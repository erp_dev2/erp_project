﻿#region Update
/*
    04/05/2017 [Ari] tambah mandatory shipment instruction[iok]
    06/06/2017 [Ari] doc SI dan DR saat save dipisah
    04/10/2017 [HAR] revisi bug tax amount dan amount saat save ke PO
    16/10/2017 [TKG] Bug fixing duplikat item saat show data dan print
    29/05/2018 [TKG] tambah status PO.
    13/07/2018 [HAR] ubah cara save PO
    07/11/2018 [HAR] bug saat generate PO docno awal bulan blm ada data PO
    04/10/2019 [TKG/TWC] ditambah approval departemen
    14/10/2019 [WED/TWC] approval per department juga berlaku di dokumen header nya
    17/10/2019 [TKG/TWC] tambah informasi site
    21/10/2019 [WED/MAI] cek approval masih bolong di site dan department
    23/10/2019 [WED/TWC] ComputeRemainingBudget() masih salah ambil kolom Total
    30/10/2019 [WED/IOK] BUG salah ambil method IsNeedApproval nya
    12/11/2019 [HAR/TWC] salah query
    18/11/2019 [DITA/TWC] ubah judul printout
    20/11/2019 [HAR/TWC] bug site tidak muncul di printout
    27/01/2020 [HAR/KMI] tambah parameter IsMRBudgetBasedOnBudgetCategory utk membedakan budget berdasarkan budget category apa tidak
    27/01/2020 [HAR/TWC] material Request routine, available budget langsung muncul 
    27/01/2020 [HAR/SRN] BUG : dokumen PO yang tergenerate titelnya MR harusnya PO
    06/04/2020 [WED/SRN] tambah ambil source dari DO
    27/04/2020 [IBL/SIER] Pemberian pembeda nomor dokumen MR Routine dg parameter IsMaterialRequest2DocNoUseDifferentAbbr
    28/04/2020 [IBL/SIER] Fromat DocNo baru dg parameter IsMaterialRequest2DocNoUseDifferentAbbr
    06/05/2020 [WED/IMS] available budget menghitung juga dari VR Budget
    06/05/2020 [WED/SIER] nomor dokumen masih error jika division short code masih kosong
    11/05/2020 [IBL/SIER] Menambah upload file
    12/05/2020 [DITA/SIER] Pengaturan Budget berdasarkan tahun dengan parametr baru mIsBudget2YearlyFormat
    29/05/2020 [VIN/IMS] Kasbon, sppd, gaji harus bisa nyambung ke budgeting operasional
    23/06/2020 [HAR/SRN] BUG budget minus bisa save : tambah function IsComputeBudgetNotValid saat insert
    30/06/2020 [HAR/SRN] BUG validasi  function IsComputeBudgetNotValid kena ke yang non budget, harusya hanya yang ytpe nya budget
    03/07/2020 [VIN/IMS] menghubungkan budget dengan cash advance settlement
    13/07/2020 [IBL/SIER] Remark pada detail tidak mandatory berdasarkan parameter IsRemarkMR2Mandatory
    20/07/2020 [DITA/SIER] Bug : saat pilh budget category --> bug di query computeavailablebudget
    21/07/2020 [VIN/SRN] Departemen belum ter otorisasi sesuai departemen user di MR Routine
    20/01/2021 [WED/SIER] tambah ambil dari Cash Advance Settlement berdasarkan parameter IsCASUsedForBudget
    22/07/2021 [WED/PHT] ComputeAvailableBudget ambil dari StdMtd
    29/09/2021 [YOG/PHT] tambah field Procurement Type berdasarkan parameter mIsMRUseProcurementType & mProcurementTypeMrRoutine 
    13/12/2021 [VIN/ALL] Bug: harusnya tidak bisa save saat nama file sama dengan yg sudah ada 
    24/03/2022 [MYA/PHT] Mengurangi available budget pada MR berdarkan transaksi receiving item form vendor - auto DO dan receiving item from vendor without PO - auto DO yang terbentuk dari MR Routine
    28/03/2022 [ICA/PHT] Menambah field available budget for MR, Available saat ini di kurangi dengan amt MR yg belum di journalkan. Jika sudah sampe PO, ambil Amt PO. 
    06/04/2022 [MYA/PHT] available budget di MR Routine bisa di simpan nilainya saat save sehingga saat show tdk dinamis
    14/04/2022 [TRI/TWC] bug angka grand total dan total tax ketukar
    18/04/2022 [ICA/PHT] bug total MR yg di ambil untuk compute budget for mr ambil dari total price bukan Qty*UPrice
    20/04/2022 [VIN/SIER] bug show data detail -> select approval terhadap materialrequest2 
    09/05/2022 [VIN/TWC] bug ComputeRemainingBudget tidak dipanggil di key down
    10/05/2022 [WED/TWC] bug print out belum false designer nya
    17/05/2022 [RIS/PHT] Validasi available budget dapat minus dengan parameter IsAvailableBudgetMRAllowMinus
    23/05/2022 [RIS/PHT] Menyangkutkan start end amount dengan parameter AmtSourceMRApproval
    07/06/2022 [RIS/PRODUCT] Cancel header ketika item cancel semua
    14/02/2023 [BRI/HEX] MR Routine menggunakan type berdasarkan param IsMRRoutineUseMRRoutineType
    21/02/2023 [IBL/HEX] Approval berdasarkan item category. Berdasarkan parameter IsDocApprovalSettingUseItemCt
    22/02/2023 [BRI/HEX] merubah param IsMRRoutineUseMRRoutineType jadi internal
    23/02/2023 [BRI/HEX] perhitungan available budget berdasarkan param FiscalYearRange
    28/02/2023 [SET/HEX] Menyesuaikan apporval di Hdr berdasar Item's Category
    01/03/2023 [SET/HEX] add field PO Sign By berdasar param IsMRRoutineUsePOSignedBy
    06/03/2023 [SET/HEX] penyesuaian tampilan Lue PO Signed By
    14/04/2023 [WED/HEX] UsedBudget ter double saat kalkulasi Remaining Budget
    18/04/2023 [WED/HEX] nilai Available MR Budget masih ke double
    28/04/2023 [WED/HEX] nilai Available MR masih belum sesuai kalau ada MR yg belum di PO kan
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        internal FrmMaterialRequest2Find FrmFind;
        internal bool
            mIsFilterBySite = false,
            mIsMREximSplitDocument = false,
            mIsShipInstructionInEximMandatory = false,
            mIsMRBudgetBasedOnBudgetCategory = false,
            mIsDORequestNeedStockValidation = false,
            mIsDORequestUseItemIssued = false,
            mIsMaterialRequest2DocNoUseDifferentAbbr = false,
            mIsBudgetActive = false,
            mIsFilterByItCt = false,
            mIsMRAllowToUploadFile = false,
            mIsBudget2YearlyFormat = false,
            mIsRemarkMR2Mandatory = false,
            mIsFilterByDept = false,
            mIsMRUseProcurementType = false,
            mIsMRUseBudgetForMR = false,
            mIsMRSaveAvailableBudget = false,
            mIsMRRoutineUseMRRoutineType = false,
            mIsMRRoutineUsePOSignedBy = false;
        private bool 
            mIsSiteMandatory = false, 
            mIsCASUsedForBudget = false,
            mIsAvailableBudgetMRAllowMinus = false,
            mIsApprovalBySiteMandatory = false,
            mIsMRApprovalByAmount = false,
            mIsDocApprovalSettingUseItemCt = false;
        private string mPOPrintOutCompanyLogo = "1", mReqTypeForNonBudget = string.Empty;
        public string docType = "1";
        private string
            mIsPrintOutUseDigitalSignature = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mProcurementTypeMrRoutine = string.Empty,
            mSourceAvailableBudgetForMR = string.Empty,
            mAmtSourceMRApproval = string.Empty,
            mFiscalYearRange = string.Empty
            ;
        private byte[] downloadedData;

        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmMaterialRequest2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Material Request";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueReqType(ref LueReqType);
                Sl.SetLueTaxCode(ref LueTaxCode1);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);
                Sl.SetLueOption(ref LueOptCode, "ProcurementType");
                Sl.SetLueOption(ref LueMRType, "MRRoutineType");
                SetLuePOSignBy(ref LuePOSignBy, string.Empty);
                LueVdContactPersonName.Visible = false;
                if (mIsShipInstructionInEximMandatory) LblShipInstruction.ForeColor = Color.Red;
                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (!mIsMRUseProcurementType)
                {
                    LueOptCode.Visible = label12.Visible = false;
                    LblMRType.Top -= 21; LueMRType.Top -= 21;
                    LblDORequestDocNo.Top -= 21; TxtDORequestDocNo.Top -= 21; BtnDORequestDocNo.Top -= 21; BtnDORequestDocNo2.Top -= 21;
                }

                if (!mIsMRRoutineUseMRRoutineType)
                {
                    LblMRType.Visible = LueMRType.Visible = false;
                    LblDORequestDocNo.Top -= 21; TxtDORequestDocNo.Top -= 21; BtnDORequestDocNo.Top -= 21; BtnDORequestDocNo2.Top -= 21;
                }

                if (!mIsMRUseBudgetForMR)
                {
                    label13.Visible = TxtBudgetForMR.Visible = false;
                    label6.Top -= 21; LueTaxCode1.Top -= 21; LueTaxCode2.Top -= 21; LueTaxCode3.Top -= 21;
                    label8.Top -= 21; TxtTaxAmt.Top -= 21;
                    label10.Top -= 21; TxtAmt.Top -= 21;
                    //LblShipInstruction.Top -= 21; TxtSIDocNo.Top -= 21; BtnSI.Top -= 21;
                    //LblDORequestDocNo.Top -= 21; TxtDORequestDocNo.Top -= 21; BtnDORequestDocNo.Top -= 21; BtnDORequestDocNo2.Top -= 21;
                    label5.Top -= 21; MeeRemark.Top -= 21;
                    label9.Top -= 21; TxtFile.Top -= 21; ChkFile.Top -= 21; BtnFile.Top -= 21; BtnDownload.Top -= 21; PbUpload.Top -= 21;
                    label17.Top -= 21; TxtFile2.Top -= 21; ChkFile2.Top -= 21; BtnFile2.Top -= 21; BtnDownload.Top -= 21; PbUpload2.Top -= 21;
                    label21.Top -= 21; TxtFile3.Top -= 21; ChkFile3.Top -= 21; BtnFile3.Top -= 21; BtnDownload3.Top -= 21; PbUpload3.Top -= 21;
                    panel2.Height -= 21;
                }
                if(!mIsMRRoutineUsePOSignedBy)
                {
                    LblPOSignBy.Visible = LuePOSignBy.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 41;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "Cancel",
                    "Old Cancel",
                    "Status",
                    "Checked By",
                    "",

                    //6-10
                    "Item's Code",
                    "", 
                    "Item's Name", 
                    "Mininum Stock",
                    "Reorder Point",

                    //11-15
                    "Quantity",
                    "UoM",
                    "Usage Date",
                    "",
                    "Quotation Number",

                    //16-20
                    "Quotation DNo",
                    "",
                    "Quotation"+Environment.NewLine+"Date",
                    "VdCode",
                    "Vendor",

                    //21-25
                    "Contact"+Environment.NewLine+"Person",
                    "Ship To",
                    "Bill To",
                    "Currency",
                    "Actual"+Environment.NewLine+"Unit Price",

                    //26-30
                    "Discount"+Environment.NewLine+"%",
                    "Discount"+Environment.NewLine+"Amount",
                    "Rounding"+Environment.NewLine+"Value",
                    "Total",
                    "Tax1",

                    //31-35
                    "Tax2",
                    "Tax3",
                    "Term of" + Environment.NewLine + "Payment",
                    "Delivery Type",
                    "Estimated"+Environment.NewLine+"Received Date",

                    //36-40
                    "Remark",
                    "ItScCode",
                    "Sub-Category",
                    "DO Request#",
                    "DO Request D#"
                },
                new int[] 
                {
                    //0
                    20,
                    //1-5
                    50, 50, 80, 80, 20,
                    //6-10
                    80, 20, 200, 100, 100,  
                    //11-15
                    80, 80, 100, 20, 150,
                    //16-20
                    80, 20, 80, 80, 200, 
                    //21-25
                    150, 150, 150, 80, 150,   
                    //26-30
                    100, 150, 100, 150, 100, 
                    //31-35
                    100, 100, 150, 150, 100, 
                    //36-40
                    200, 100, 150, 0, 0
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 25, 26, 27, 28, 29, 30, 31, 32 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 5, 7, 14, 17 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 13, 18, 35 });

            if (IsProcFormat == "1")
            {
               Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 6, 7, 16, 17, 18, 19, 26, 27, 28, 37 }, false);
            }
            else
            {
               Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 6, 7, 16, 17, 18, 19, 26, 27, 28, 37, 38 }, false);
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 7, 15, 17, 19 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueReqType, LueDeptCode, LueSiteCode, 
                        LueBCCode, LueTaxCode1, LueTaxCode2, LueTaxCode3, LueOptCode, TxtAmt, 
                        TxtSIDocNo, MeeRemark, TxtDORequestDocNo, TxtFile, TxtFile2, TxtFile3, LueMRType,
                        LuePOSignBy
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 });
                    TxtDocNo.Focus();
                    BtnSI.Enabled = false;
                    BtnDORequestDocNo.Enabled = false;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueReqType, TxtLocalDocNo, LueDeptCode, LueSiteCode, 
                        LueTaxCode1, LueTaxCode2, LueTaxCode3, TxtSIDocNo, MeeRemark, LueMRType
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LuePOSignBy
                    }, !mIsMRRoutineUsePOSignedBy);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 7, 11, 13, 14, 17, 21, 22, 23, 26, 27, 28, 35, 36 });
                    DteDocDt.Focus();
                    BtnSI.Enabled = true;
                    if (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    if (!mIsDORequestNeedStockValidation) BtnDORequestDocNo.Enabled = true;

                    if (mIsMRAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = true;
                        BtnFile3.Enabled = true;
                        BtnDownload3.Enabled = true;
                    }
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    if (!mIsDORequestNeedStockValidation)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueReqType, TxtLocalDocNo, LueDeptCode, 
                LueSiteCode, LueBCCode, LueTaxCode1, LueTaxCode2, LueTaxCode3, LueOptCode, 
                TxtSIDocNo, MeeRemark, TxtDORequestDocNo, TxtFile, TxtFile2, TxtFile3, LueMRType, LuePOSignBy
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtRemainingBudget, TxtAmt, TxtTaxAmt }, 0);
            ClearGrd();
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9, 10, 11, 25, 26, 27, 28, 29, 30, 31, 32 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMaterialRequest2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                if (mIsMRUseProcurementType)
                    Sm.SetLue(LueOptCode, mProcurementTypeMrRoutine);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string RptName = Sm.GetParameter("FormPrintOutMRExim");

            if ((RptName.Length > 0) && (Sm.GetGrdStr(Grd1, 0, 15).Length > 0)) //if MAI & has QTDocNo
            {
                var SQLVendor = new StringBuilder();
                var cVendor = new MySqlCommand();

                SQLVendor.AppendLine("Select Distinct D.DocNo ");
                SQLVendor.AppendLine("From TblMaterialRequestHdr A ");
                SQLVendor.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                SQLVendor.AppendLine("Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' ");
                SQLVendor.AppendLine("Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
                SQLVendor.AppendLine("WHERE A.DocNo = '" + TxtDocNo.Text + "';");

                //SQLVendor.AppendLine("SELECT DISTINCT A.DocNo ");
                //SQLVendor.AppendLine("FROM TblPOHdr A ");
                //SQLVendor.AppendLine("INNER JOIN TblPODtl B ON A.DocNo = B.DocNo And B.CancelInd='N' ");
                //SQLVendor.AppendLine("INNER JOIN TblPORequestDtl D ON B.PORequestDocNo = D.DocNo AND B.PORequestDNo = D.DNo And D.CancelInd='N' ");
                //SQLVendor.AppendLine("WHERE D.MaterialRequestDocNo = '" + TxtDocNo.Text + "' ");

                using (var cnVendor = new MySqlConnection(Gv.ConnectionString))
                {
                    cnVendor.Open();
                    cVendor.Connection = cnVendor;
                    cVendor.CommandText = SQLVendor.ToString();
                    var drVendor = cVendor.ExecuteReader();
                    var c = Sm.GetOrdinal(drVendor, new string[] { "DocNo" });
                    if (drVendor.HasRows)
                    {
                        if (mIsMREximSplitDocument)
                        {
                            while (drVendor.Read())
                            {
                                ParPrint(1, Sm.DrStr(drVendor, 0));
                            }
                        }
                        else
                        {
                            if (drVendor.Read())
                                ParPrint(2, Sm.DrStr(drVendor, 0));
                        }
                    }
                    else 
                        Sm.StdMsg(mMsgType.Info, "You need to process this document to Purchase Order first.");
                    drVendor.Close();
                }
            }
            else
            {
                ParPrint(0, TxtDocNo.Text);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 5 && !Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) 
                            Sm.FormShowDialog(new FrmMaterialRequest2Dlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 11, 13, 20, 35 }, e.ColIndex))
                    {
                        if (e.ColIndex == 13) Sm.DteRequestEdit(Grd1, DteUsageDt, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        if (e.ColIndex == 35) Sm.DteRequestEdit(Grd1, DteEstRecvDt, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                f.ShowDialog();
            }


            if (e.ColIndex == 14 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                  Sm.FormShowDialog(new FrmMaterialRequest2Dlg2(this, e.RowIndex));
                }
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 21 }, e.ColIndex))
            {
                Sm.LueRequestEdit(ref Grd1, ref LueVdContactPersonName, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                SetLueVdPersonCode(ref LueVdContactPersonName, Sm.GetGrdStr(Grd1, e.RowIndex, 19));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                //ComputeRemainingBudget();
                if (mIsMRUseBudgetForMR) ComputeBudgetForMR();
                ComputeAmt();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequest2Dlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                f.ShowDialog();
            }

            if (e.ColIndex == 14 && TxtDocNo.Text.Length == 0)
            {
                Sm.FormShowDialog(new FrmMaterialRequest2Dlg2(this, e.RowIndex));
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 11 }, e);

            if (e.ColIndex == 11) ComputeTotal(e.RowIndex);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 23 }, e);
            if (e.ColIndex == 1 || e.ColIndex == 11 || e.ColIndex == 26 || e.ColIndex == 27 || e.ColIndex == 28)
            {
                ComputeTotal(e.RowIndex);
                ComputeTaxAmt();
                if (mIsMRUseBudgetForMR) ComputeBudgetForMR();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 13 || e.ColIndex == 35)
            {
                if (Sm.GetGrdDate(Grd1, 0, e.ColIndex).Length != 0)
                {
                    var Dt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, e.ColIndex));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0) Grd1.Cells[Row, e.ColIndex].Value = Dt;
                }
            }
        }

        override protected void GrdRequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {
            if (e.ColIndex == 13 || e.ColIndex == 35)
                e.Text = "Double click title to copy data based on the first row's value.";
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string 
                SubCategory = Sm.GetGrdStr(Grd1, 0, 37),
                //DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr", SubCategory, 0),
                DocNo = string.Empty,
                DocNoPOR = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "PORequest", "TblPORequestHdr", SubCategory, 0),
                PODocNo = string.Empty;
            bool
                PORequestApprovalForAllDept = IsPORequestApprovalForAllDept(),
                IsApprovalByDept = IsApprovalByDeptEnabled(),
                IsFirstMR = true,
                IsFirstPOR = true,
                IsFirstPO = true,
                IsDocApprovalMR2SettingNotExisted = false; //IsDocApprovalPORSettingNotExisted();
                                                                                      //IsDocApprovalMR2SettingNotExisted = IsDocApprovalSettingNotExisted(DocNo);

            if (mIsMaterialRequest2DocNoUseDifferentAbbr) DocNo = GenerateDocNo2(Sm.GetDte(DteDocDt), "MaterialRequest2", "TblMaterialRequestHdr");
            else DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr", SubCategory, 0);

            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (mIsMREximSplitDocument)
                    IsDocApprovalMR2SettingNotExisted = IsDocApprovalSettingNotExisted(Row);
                else
                    IsDocApprovalMR2SettingNotExisted = IsDocApprovalSettingNotExistedNotSplit();
                if(IsFirstMR)
                {
                    cml.Add(SaveMaterialRequestHdr(DocNo, IsApprovalByDept, IsDocApprovalMR2SettingNotExisted));
                    IsFirstMR = false;
                }
                if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0) 
                    cml.Add(SaveMaterialRequestDtl(DocNo, Row, IsApprovalByDept, IsDocApprovalMR2SettingNotExisted));

                if (IsDocApprovalMR2SettingNotExisted)
                {
                    if(IsFirstPOR)
                    {
                        cml.Add(SavePORequestHdr(DocNoPOR));
                        IsFirstPOR = false;
                    }
                        if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0) 
                        {   
                            cml.Add(SavePORequestDtl(DocNoPOR, DocNo, Row, PORequestApprovalForAllDept));
                        }
                }

                if (IsDocApprovalMR2SettingNotExisted)
                {
                    if (mIsMREximSplitDocument)
                    {
                        PODocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "PO", "TblPOHdr", SubCategory, Row);
                        if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0 && Sm.GetGrdStr(Grd1, Row, 19).Length != 0)
                        {
                            cml.Add(SavePOHdr(PODocNo, Sm.GetGrdStr(Grd1, Row, 19), Row));
                            if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0 && Sm.GetGrdStr(Grd1, Row, 19) == Sm.GetGrdStr(Grd1, Row, 19)) // 6 itcode, 19 vdcode
                                cml.Add(SavePODtl(PODocNo, DocNoPOR, Row));
                        }
                    }
                    else
                    {
                        if (Sm.GetGrdStr(Grd1, 0, 6).Length != 0 && Sm.GetGrdStr(Grd1, 0, 19).Length != 0)
                        {
                            PODocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "PO", "TblPOHdr", SubCategory, 0);
                            if(IsFirstPO)
                            {
                                cml.Add(SavePOHdr(PODocNo, Sm.GetGrdStr(Grd1, 0, 19), 0));
                                IsFirstPO = false;
                            }
                            if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                                cml.Add(SavePODtl(PODocNo, DocNoPOR, Row));
                        }
                    }
                }
            }
           Sm.ExecCommands(cml);

           if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
               UploadFile(DocNo);
           if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
               UploadFile2(DocNo);
           if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
               UploadFile3(DocNo);
           ShowData(DocNo);

        }

        private bool IsInsertedDataNotValid()
        {
           
            IsSubCategoryNull();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueReqType, "Request type") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                IsMRUsedBugdetAndBudgetUseBudgetCategory()||
                (mIsShipInstructionInEximMandatory && Sm.IsTxtEmpty(TxtSIDocNo,"Shipment Instruction/DR", false)) ||
                (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued && Sm.IsTxtEmpty(TxtDORequestDocNo, "DO Request#", false)) ||
                (mIsMRUseProcurementType && Sm.IsLueEmpty(LueOptCode, "Procurement Type")) ||
                (mIsMRRoutineUseMRRoutineType && Sm.IsLueEmpty(LueMRType, "MR type")) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsDocApprovalPORSettingNotExisted() ||
                IsGrdExceedMaxRecords() ||
                (!mIsAvailableBudgetMRAllowMinus && IsRemainingBudgetNotValid()) ||
                (!mIsAvailableBudgetMRAllowMinus && mReqTypeForNonBudget != Sm.GetLue(LueReqType) && mIsMRBudgetBasedOnBudgetCategory && IsComputeBudgetNotValid()) ||
                IsSubcategoryDifferent() ||
                IsSubCategoryXXX() ||
                IsQuotationExpired() ||
                IsCurrencyDifferent() ||
                IsVendorDifferent() ||
                (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1" && IsFileNameAlreadyExisted()) ||
                (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1" && IsFileNameAlreadyExisted2()) ||
                (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1" && IsFileNameAlreadyExisted3())
                ;
        }

        private bool IsApprovalByDeptEnabled()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='MaterialRequest2' And DeptCode Is Not Null Limit 1; ");
        }

        private bool IsDocApprovalSettingNotExisted(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where UserCode Is not Null ");
            SQL.AppendLine("And DocType='MaterialRequest2' ");
            if (IsApprovalByDeptEnabled())
                SQL.AppendLine("And DeptCode Is Not Null And DeptCode = @DeptCode ");
            if (mIsFilterBySite)
                SQL.AppendLine("And SiteCode Is Not Null And SiteCode = @SiteCode ");
            if (mIsMRApprovalByAmount)
            {
                SQL.AppendLine("And (T.EndAmt=0 ");
                SQL.AppendLine("Or T.EndAmt>=IfNull(( ");
                if (mAmtSourceMRApproval == "2")
                    SQL.AppendLine("    Select IfNull(@TotalPrice, 0) ");
                else if (mAmtSourceMRApproval == "1")
                    SQL.AppendLine("    Select IfNull(@UPrice, 0) ");
                else if (mAmtSourceMRApproval == "3")
                    SQL.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                //SQL.AppendLine("    From TblMaterialRequestDtl ");
                //SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo ");
                SQL.AppendLine("), 0)) ");

                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                if (mAmtSourceMRApproval == "2")
                    SQL.AppendLine("    Select IfNull(@TotalPrice, 0) ");
                else if (mAmtSourceMRApproval == "1")
                    SQL.AppendLine("    Select IfNull(@UPrice, 0) ");
                else if (mAmtSourceMRApproval == "3")
                    SQL.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                //SQL.AppendLine("    From TblMaterialRequestDtl ");
                //SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo ");
                SQL.AppendLine("), 0)) ");
            }
            if (mIsDocApprovalSettingUseItemCt)
            {
                SQL.AppendLine("And T.ItCtCode In ( ");
                SQL.AppendLine("   Select ItCtCode ");
                SQL.AppendLine("   From TblItem ");
                SQL.AppendLine("   Where ItCode = @ItCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Limit 1; ");

          
            var cm = new MySqlCommand() {CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice", Sm.GetGrdDec(Grd1, Row, 29));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 25));
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", decimal.Parse(TxtAmt.Text));
            //Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            return !Sm.IsDataExist(cm);
        }

        private bool IsDocApprovalSettingNotExistedNotSplit()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where UserCode Is not Null ");
            SQL.AppendLine("And DocType='MaterialRequest2' ");
            if (IsApprovalByDeptEnabled())
                SQL.AppendLine("And DeptCode Is Not Null And DeptCode = @DeptCode ");
            if (mIsFilterBySite)
                SQL.AppendLine("And SiteCode Is Not Null And SiteCode = @SiteCode ");
            if (mIsMRApprovalByAmount && mAmtSourceMRApproval == "3")
            {
                SQL.AppendLine("And (T.EndAmt=0 ");
                SQL.AppendLine("Or T.EndAmt>=IfNull(( ");
                SQL.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                SQL.AppendLine("), 0)) ");

                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                SQL.AppendLine("), 0)) ");
            }
            if (mIsDocApprovalSettingUseItemCt)
            {
                SQL.AppendLine("And T.ItCtCode In ( ");
                SQL.AppendLine("   Select ItCtCode ");
                SQL.AppendLine("   From TblItem ");
                SQL.AppendLine("   Where ItCode = @ItCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Limit 1; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, 0, 6));
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            return !Sm.IsDataExist(cm);
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsMRUsedBugdetAndBudgetUseBudgetCategory()
        {
            if (Sm.GetLue(LueReqType) == "1")
            {
                if (mIsMRBudgetBasedOnBudgetCategory)
                {
                    if(Sm.IsLueEmpty(LueBCCode, "Budget Category"))
                        return true;
                }
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 11, true, "Quantity is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 13, false, "Usage date is empty.")||
                    Sm.IsGrdValueEmpty(Grd1, Row, 15, false, "Quotation is empty.")||
                    Sm.IsGrdValueEmpty(Grd1, Row, 35, false, "Estimated received date is empty.") ||
                    (mIsRemarkMR2Mandatory && Sm.IsGrdValueEmpty(Grd1, Row, 36, false, "Remark is empty."))
                    ) return true;
            }
            return false;
        }

        private bool IsRemainingBudgetNotValid()
        {
            decimal RemainingBudget = 0m;

            if (TxtRemainingBudget.Text.Length != 0) RemainingBudget = decimal.Parse(TxtRemainingBudget.Text);

            if (RemainingBudget<0)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid remaining budget.");
                return true;
            }
            return false;
        }

        private bool IsComputeBudgetNotValid()
        {
            decimal RemainingBudget = 0m;
            decimal GrandTotal = 0m;

            if (TxtRemainingBudget.Text.Length != 0) 
                RemainingBudget = decimal.Parse(TxtRemainingBudget.Text);

            if (TxtAmt.Text.Length != 0)
                GrandTotal = decimal.Parse(TxtAmt.Text);


            if ((RemainingBudget-GrandTotal) < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Grand Total is bigger than available budget.");
                return true;
            }
            return false;
        }

        private bool IsDocApprovalPORSettingNotExisted()
        {
            if (!Sm.IsDataExist("Select DocType From TblDocApprovalSetting Where DocType='PORequest' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "No approval setting for this PO request.");
                return true;
            }
            return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 37).Length == 0)
                {
                    Grd1.Cells[Row, 37].Value = Grd1.Cells[Row, 38].Value = "XXX";
                }
            }
        }

        private bool IsSubCategoryXXX()
        {
            if (IsProcFormat == "1")
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 37) == "XXX")
                    {
                        Msg =
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine;

                        Sm.StdMsg(mMsgType.Warning, Msg + "doesn't have Sub-Category.");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 37);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 37))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different subcategory ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsVendorDifferent()
        {
            if (Sm.GetParameter("IsMREximSplitDocument") == "N")
            {
                string VdCode = Sm.GetGrdStr(Grd1, 0, 19);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (VdCode != Sm.GetGrdStr(Grd1, Row, 19))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different vendor ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsCurrencyDifferent()
        {
            string CurCode = Sm.GetGrdStr(Grd1, 0, 24);
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (CurCode != Sm.GetGrdStr(Grd1, Row, 24))
                {
                    Sm.StdMsg(mMsgType.Warning, "Item have different currency ");
                    return true;
                }
            }
            return false;
        }

        private bool IsQuotationExpired()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                string Msg = string.Empty;
                string DateExpired = Sm.GetValue("Select ifnull(ExpiredDt, '99999999') As ExpiredDt From tblQtHdr Where DocNo = '"+Sm.GetGrdStr(Grd1, Row, 15)+"' ");
                if (Convert.ToInt32(DateExpired.Substring(0, 8)) < Convert.ToInt32(Sm.GetDte(DteDocDt).Substring(0, 8)))
                {
                    Msg =
                      "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                      "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                      "Quotation : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine;

                    Sm.StdMsg(mMsgType.Warning, Msg + "quotation has been expired.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveMaterialRequestHdr(string DocNo, bool IsApprovalByDept, bool IsDocApprovalSettingNotExisted)
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Insert Into TblMaterialRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, LocalDocNo, DeptCode, SiteCode, ReqType, BCCode, TaxCode1, TaxCode2, TaxCode3,  Amt, SIDocNo, DRDocNo, EximInd, DORequestDocNo, Remark, ");
            if (mIsMRUseProcurementType)
                SQL.AppendLine("ProcurementType, ");
            if (mIsMRRoutineUseMRRoutineType)
                SQL.AppendLine("MRType, ");
            if (mIsMRUseBudgetForMR) SQL.AppendLine("BudgetForMR, ");
            if (mIsMRSaveAvailableBudget) SQL.AppendLine("AvailableBudget, ");
            if (mIsMRRoutineUsePOSignedBy) SQL.AppendLine("POSignBy, ");
            SQL.AppendLine("CreateBy, CreateDt ) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @LocalDocNo, @DeptCode, @SiteCode, @ReqType, @BCCode, @TaxCode1, @TaxCode2, @TaxCode3, @Amt, @SIDocNo, @DRDocNo, 'Y', @DORequestDocNo, @Remark, ");
            if (mIsMRUseProcurementType)
                SQL.AppendLine("@ProcurementType, ");
            if (mIsMRRoutineUseMRRoutineType)
                SQL.AppendLine("@MRType, ");
            if (mIsMRUseBudgetForMR) SQL.AppendLine("@BudgetForMR, ");
            if (mIsMRSaveAvailableBudget) SQL.AppendLine("@AvailableBudget, ");
            if (mIsMRRoutineUsePOSignedBy) SQL.AppendLine("@POSignBy, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            if (Sm.GetParameter("IsMREximSplitDocument") == "N")
            {
                if (!IsDocApprovalSettingNotExisted)
                {
                    SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                    SQL.AppendLine("From TblDocApprovalSetting T ");
                    SQL.AppendLine("Where T.DocType='MaterialRequest2' ");
                    if (IsApprovalByDept) SQL.AppendLine("And T.DeptCode=@DeptCode ");
                    if (mIsApprovalBySiteMandatory)
                        SQL.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
                    if (mIsMRApprovalByAmount && mAmtSourceMRApproval == "3")
                    {

                        SQL.AppendLine("And (T.EndAmt=0 ");
                        SQL.AppendLine("Or T.EndAmt>=IfNull(( ");
                        SQL.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                        SQL.AppendLine("), 0)) ");

                        SQL.AppendLine("And (T.StartAmt=0 ");
                        SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                        SQL.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                        SQL.AppendLine("), 0)) ");
                    }

                    if (mIsDocApprovalSettingUseItemCt)
                    {
                        SQL.AppendLine("And T.ItCtCode In ( ");
                        SQL.AppendLine("   Select A.ItCtCode ");
                        SQL.AppendLine("   From TblItem A ");
                        SQL.AppendLine("   Where A.ItCode = @ItCode1 ");
                        SQL.AppendLine(") ");
                    }
                    SQL.AppendLine("; ");
                }
            }

            SQL.AppendLine("Update TblMaterialRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='MaterialRequest2' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2)); 
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3)); 
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            if (docType == "1")
            {
                Sm.CmParam<String>(ref cm, "@SIDocNo", TxtSIDocNo.Text);
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@DRDocNo", TxtSIDocNo.Text);
            }
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", TxtDORequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            if (mIsMRUseProcurementType)
                Sm.CmParam<String>(ref cm, "@ProcurementType", Sm.GetLue(LueOptCode));
            if (mIsMRRoutineUseMRRoutineType)
                Sm.CmParam<String>(ref cm, "@MRType", Sm.GetLue(LueMRType));
            if (mIsMRUseBudgetForMR)
                Sm.CmParam<Decimal>(ref cm, "@BudgetForMR", Decimal.Parse(TxtBudgetForMR.Text));
            if (mIsMRSaveAvailableBudget)
                Sm.CmParam<Decimal>(ref cm, "@AvailableBudget", Decimal.Parse(TxtRemainingBudget.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@ItCode1", Sm.GetGrdStr(Grd1, 0, 6));
            Sm.CmParam<String>(ref cm, "@POSignBy", Sm.GetLue(LuePOSignBy));
            return cm;
        }

        private MySqlCommand SaveMaterialRequestDtl(string DocNo, int Row, bool IsApprovalByDept, bool IsDocApprovalSettingNotExisted)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, CancelInd, Status, ItCode, Qty, UsageDt, QtDocNo, QtDNo, VdContactPerson, UPrice, Discount, DiscountAmt, ");
            SQL.AppendLine("RoundingValue, EstRecvDt, ShipTo, BillTo, DORequestDocNo, DORequestDNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @Status, @ItCode, @Qty, @UsageDt, @QtDocNo, @QtDNo, @VdContactPerson, @UPrice, @Discount, @DiscountAmt, ");
            SQL.AppendLine("@RoundingValue, @EstRecvDt, @ShipTo, @BillTo, @DORequestDocNo, @DORequestDNo, @Remark, @CreateBy, CurrentDateTime()); ");

            if (Sm.GetParameterBoo("IsMREximSplitDocument"))
            {
                if (!IsDocApprovalSettingNotExisted)
                {
                    SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
                    SQL.AppendLine("From TblDocApprovalSetting T ");
                    SQL.AppendLine("Where T.DocType='MaterialRequest2' ");
                    if (IsApprovalByDept) SQL.AppendLine("And T.DeptCode=@DeptCode ");
                    if (mIsApprovalBySiteMandatory)
                        SQL.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
                    if (mIsMRApprovalByAmount)
                    {

                        SQL.AppendLine("And (T.EndAmt=0 ");
                        SQL.AppendLine("Or T.EndAmt>=IfNull(( ");
                        if (mAmtSourceMRApproval == "2")
                            SQL.AppendLine("    Select IfNull(UPrice * Qty, 0) ");
                        else if (mAmtSourceMRApproval == "1")
                            SQL.AppendLine("    Select IfNull(UPrice, 0) ");
                        else if (mAmtSourceMRApproval == "3")
                            SQL.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                        SQL.AppendLine("    From TblMaterialRequestDtl ");
                        SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo ");
                        SQL.AppendLine("), 0)) ");

                        SQL.AppendLine("And (T.StartAmt=0 ");
                        SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                        if (mAmtSourceMRApproval == "2")
                            SQL.AppendLine("    Select IfNull(UPrice * Qty, 0) ");
                        else if (mAmtSourceMRApproval == "1")
                            SQL.AppendLine("    Select IfNull(UPrice, 0) ");
                        else if (mAmtSourceMRApproval == "3")
                            SQL.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                        SQL.AppendLine("    From TblMaterialRequestDtl ");
                        SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo ");
                        SQL.AppendLine("), 0)) ");
                    }
                    if (mIsDocApprovalSettingUseItemCt)
                    {
                        SQL.AppendLine("And T.ItCtCode In ( ");
                        SQL.AppendLine("   Select B.ItCtCode ");
                        SQL.AppendLine("   From TblMaterialRequestDtl A ");
                        SQL.AppendLine("   Inner Join TblItem B On A.ItCode = B.ItCode ");
                        SQL.AppendLine("   Where A.DocNo = @DocNo And A.DNo = @DNo ");
                        SQL.AppendLine(") ");
                    }

                    SQL.AppendLine("; ");
                }
            }

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            if (!IsDocApprovalSettingNotExisted)
                Sm.CmParam<String>(ref cm, "@Status", "O");
            else
                Sm.CmParam<String>(ref cm, "@Status", "A");
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetGrdDate(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@QtDocNo", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@QtDNo", Sm.GetGrdStr(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 25));

            Sm.CmParam<String>(ref cm, "@VdContactPerson",  Sm.GetLue(LueVdContactPersonName));
            Sm.CmParam<Decimal>(ref cm, "@Discount", Sm.GetGrdDec(Grd1, Row, 26));
            Sm.CmParam<Decimal>(ref cm, "@DiscountAmt",Sm.GetGrdDec(Grd1, Row, 27));
            Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Sm.GetGrdDec(Grd1, Row, 28));
            Sm.CmParamDt(ref cm, "@EstRecvDt", Sm.GetGrdDate(Grd1, Row, 35));
            Sm.CmParam<String>(ref cm, "@ShipTo", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@BillTo", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", Sm.GetGrdStr(Grd1, Row, 39));
            Sm.CmParam<String>(ref cm, "@DORequestDNo", Sm.GetGrdStr(Grd1, Row, 40));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 36));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", decimal.Parse(TxtAmt.Text));

            return cm;
        }

        private MySqlCommand SavePORequestHdr(string DocNo)
        {
            var SQLPOReq = new StringBuilder();

            SQLPOReq.AppendLine("Insert Into TblPORequestHdr (DocNo, LocalDocno, DocDt, SiteCode, Remark, ");
            if(mIsMRUseProcurementType)
                SQLPOReq.AppendLine("ProcurementType, ");
            SQLPOReq.AppendLine("CreateBy, CreateDt) ");
            SQLPOReq.AppendLine("Values(@DocNo, @LocalDocno, @DocDt, @SiteCode, @Remark, ");
            if(mIsMRUseProcurementType)
                SQLPOReq.AppendLine("@ProcurementType, ");
            SQLPOReq.AppendLine("@CreateBy, CurrentDateTime()); ");
            var cm = new MySqlCommand() { CommandText = SQLPOReq.ToString() };

            //var cm = new MySqlCommand()
            //{
            //    CommandText =
            //        "Insert Into TblPORequestHdr(DocNo, LocalDocno, DocDt, SiteCode, Remark, ProcurementType, CreateBy, CreateDt) " +
            //        "Values(@DocNo, @LocalDocno, @DocDt, @SiteCode, @Remark, @ProcurementType, @CreateBy, CurrentDateTime()) "
            //};
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocno", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            if (mIsMRUseProcurementType)
                Sm.CmParam<String>(ref cm, "@ProcurementType", Sm.GetLue(LueOptCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePORequestDtl(string DocNoPOR, string DocNoMR, int Row, bool PORequestApprovalForAllDept)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPORequestDtl");
            SQL.AppendLine("(DocNo, DNo, CancelInd, MaterialRequestDocNo, MaterialRequestDNo, Qty, QtDocNo, QtDNo, CreditLimit, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @MaterialRequestDocNo, @MaterialRequestDNo, @Qty, @QtDocNo, @QtDNo, 0, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, UserCode, Status, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, 'Sys', 'A', @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='PORequest' ");

            if (!PORequestApprovalForAllDept)
            {
                SQL.AppendLine("And T.DeptCode=( ");
                SQL.AppendLine("    Select B.DeptCode ");
                SQL.AppendLine("    From TblPORequestDtl A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr B On A.MaterialRequestDocNo=B.DocNo ");
                SQL.AppendLine("    Where A.DocNo=@DocNo And DNo=@DNo ");
                SQL.AppendLine(") ");
            }

            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Qty*B.UPrice*IfNull(D.Amt, 1) ");
            SQL.AppendLine("    From TblPORequestDtl A ");
            SQL.AppendLine("    Inner Join TblQtDtl B On A.QtDocNo=B.DocNo And A.QtDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblQtHdr C On A.QtDocNo=C.DocNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) D On C.CurCode=D.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo And A.DNo=@DNo ");
            SQL.AppendLine("), 0)) ");

            if (mIsDocApprovalSettingUseItemCt)
            {
                SQL.AppendLine("And T.ItCtCode In ( ");
                SQL.AppendLine("   Select B.ItCtCode ");
                SQL.AppendLine("   From TblMaterialRequestDtl A ");
                SQL.AppendLine("   Inner Join TblItem B On A.ItCode = B.ItCode ");
                SQL.AppendLine("   Where A.DocNo = @DocNo And A.DNo = @DNo ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblPORequestDtl Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo ; ");

            SQL.AppendLine("Update TblMaterialRequestDtl Set ProcessInd='F' ");
            SQL.AppendLine("Where DocNo=@MaterialRequestDocNo ");
            SQL.AppendLine("And Dno=@MaterialRequestDNo ");
            SQL.AppendLine("And Status='A' ");
            SQL.AppendLine("And CancelInd='N';");
            //SQL.AppendLine("And Not Exists(Select DocType From TblDocApproval Where DocType='PORequest' And DocNo=@DocNo And DNo=@DNo); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNoPOR);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", DocNoMR);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@QtDocNo", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@QtDNo", Sm.GetGrdStr(Grd1, Row, 16));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 36));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #region PO save
        private MySqlCommand SavePOHdr(string PODocNo, string VdCode, int CountRow)
        {
            string TaxAmt1 = "0", TaxAmt2 = "0", TaxAmt3 = "0";
            var SQLPO = new StringBuilder();

            SQLPO.AppendLine("Insert Into TblPOHdr(DocNo, DocDt, Status, DocNoSource, SiteCode, LocalDocNo, RevNo, VdCode, VdContactPerson, ShipTo, BillTo, CurCode, TaxCode1, TaxCode2, TaxCode3, TaxAmt, CustomsTaxAmt, DiscountAmt, Amt, SignBy, Remark, CreateBy, CreateDt) ");
            SQLPO.AppendLine("Values(@DocNo, @DocDt, 'A', @DocNo, @SiteCode, @LocalDocNo, 0, @VdCode, @VdContactPerson, @ShipTo, @BillTo, @CurCode, @TaxCode1, @TaxCode2, @TaxCode3, @TaxAmt, @CustomsTaxAmt, @DiscountAmt, @Amt, @SignBy, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand()
            {
                CommandText =
                    SQLPO.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", PODocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if (!mIsMREximSplitDocument)
            {
                Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
                Sm.CmParam<String>(ref cm, "@VdContactPerson", Sm.GetGrdStr(Grd1, 0, 21));
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, 0, 24));
                Sm.CmParam<String>(ref cm, "@ShipTo", Sm.GetGrdStr(Grd1, 0, 22));
                Sm.CmParam<String>(ref cm, "@BillTo", Sm.GetGrdStr(Grd1, 0, 23));
                Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
                Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
                Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
                Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Decimal.Parse(TxtTaxAmt.Text));
                Sm.CmParam<Decimal>(ref cm, "@CustomsTaxAmt", Decimal.Parse("0"));
                Sm.CmParam<Decimal>(ref cm, "@DiscountAmt", Decimal.Parse("0"));
                Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@SignBy", Sm.GetLue(LuePOSignBy));
            }
            else
            {
                if (Sm.GetLue(LueTaxCode1).Length != 0)
                {
                    TaxAmt1 = Sm.GetValue("Select ifnull(TaxRate, 0) TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode1) + "'");
                }
                if (Sm.GetLue(LueTaxCode2).Length != 0)
                {
                    TaxAmt2 = Sm.GetValue("Select ifnull(TaxRate, 0) TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode2) + "'");
                }
                if (Sm.GetLue(LueTaxCode3).Length != 0)
                {
                    TaxAmt3 = Sm.GetValue("Select ifnull(TaxRate, 0) TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode3) + "'");
                }

                decimal TaxAmt = Sm.GetGrdDec(Grd1, CountRow, 29) * Decimal.Parse(TaxAmt1) / 100m +
                                 Sm.GetGrdDec(Grd1, CountRow, 29) * Decimal.Parse(TaxAmt2) / 100m +
                                 Sm.GetGrdDec(Grd1, CountRow, 29) * Decimal.Parse(TaxAmt3) / 100m;

                decimal Amt = Sm.GetGrdDec(Grd1, CountRow, 29) + TaxAmt;

                Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
                Sm.CmParam<String>(ref cm, "@VdContactPerson", Sm.GetGrdStr(Grd1, CountRow, 21));
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, CountRow, 24));
                Sm.CmParam<String>(ref cm, "@ShipTo", Sm.GetGrdStr(Grd1, CountRow, 22));
                Sm.CmParam<String>(ref cm, "@BillTo", Sm.GetGrdStr(Grd1, CountRow, 23));
                Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
                Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
                Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
                Sm.CmParam<Decimal>(ref cm, "@TaxAmt", TaxAmt);
                Sm.CmParam<Decimal>(ref cm, "@CustomsTaxAmt", Decimal.Parse("0"));
                Sm.CmParam<Decimal>(ref cm, "@DiscountAmt", Decimal.Parse("0"));
                Sm.CmParam<Decimal>(ref cm, "@Amt", Amt);
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            }

            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }


        private MySqlCommand SavePODtl(string PODocNo, string PORDocNo, int Row)
        {
            var SQLPODtl = new StringBuilder();

            SQLPODtl.AppendLine("Insert Into TblPODtl(DocNo, DNo, CancelInd, PORequestDocNo, PORequestDNo, Qty, Discount, DiscountAmt, RoundingValue, EstRecvDt, Remark, CreateBy, CreateDt) ");
            SQLPODtl.AppendLine("Values(@DocNo, @DNo, 'N', @PORequestDocNo, @PORequestDNo, @Qty, @Discount, @DiscountAmt, @RoundingValue, @EstRecvDt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQLPODtl.AppendLine("Update TblPOhdr Set VdContactperson=@VdContactPerson, ShipTo=@ShipTo, BillTo=@BillTo Where DocNo=@DocNo; ");

            SQLPODtl.AppendLine("Update TblPORequestDtl Set ProcessInd='F' ");
            SQLPODtl.AppendLine("Where DocNo=@PORequestDocNo ");
            SQLPODtl.AppendLine("And Dno=@PORequestDNo ");
            SQLPODtl.AppendLine("And Status='A' ");
            SQLPODtl.AppendLine("And CancelInd='N';");

            var cm = new MySqlCommand()
            {
                CommandText =
                    SQLPODtl.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", PODocNo);
            if (Sm.GetParameter("IsMREximSplitDocument") == "Y")
            {
                Sm.CmParam<String>(ref cm, "@DNo", "001");
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            }
            Sm.CmParam<String>(ref cm, "@PORequestDocNo", PORDocNo);
            Sm.CmParam<String>(ref cm, "@PORequestDNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Discount", Sm.GetGrdDec(Grd1, Row, 26));
            Sm.CmParam<Decimal>(ref cm, "@DiscountAmt", Sm.GetGrdDec(Grd1, Row, 27));
            Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Sm.GetGrdDec(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@EstRecvDt", Sm.GetGrdDate(Grd1, Row, 35).Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@VdContactPerson", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@ShipTo", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@BillTo", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 36));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion


        #endregion

        #region Cancel data

        private void CancelData()
        {
            UpdateCancelledItem();

            string DNo = "'XXX'";
            int tempCancel = 0;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

                if (Sm.GetGrdBool(Grd1, Row, 1) == true)
                    tempCancel = tempCancel + 1;
            }

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelMaterialRequestDtl(TxtDocNo.Text, DNo));

            if (tempCancel == Grd1.Rows.Count - 1)
                cml.Add(CancelMaterialRequestHdr(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = 
                        "Select DNo, CancelInd From TblMaterialRequestDtl " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                IsCancelledItemNotExisted(DNo) ||
                IsCancelledItemCheckedAlready(DNo);
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsCancelledItemCheckedAlready(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblPORequestDtl ");
            SQL.AppendLine("Where MaterialRequestDocNo=@DocNo ");
            SQL.AppendLine("And (CancelInd='N' And IfNull(Status, 'O')<>'C') ");
            SQL.AppendLine("And MaterialRequestDNo In (" + DNo + ") ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been processed.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelMaterialRequestDtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And (CancelInd='N' Or Status='C') And DNo In (" + DNo + "); ");

            if (!mIsDORequestNeedStockValidation)
            {
                SQL.AppendLine("Update TblMaterialRequestDtl Set ");
                SQL.AppendLine("  DORequestDocNo = null, ");
                SQL.AppendLine("  DORequestDNo = null ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And DORequestDocNo Is Not Null ");
                SQL.AppendLine("And DORequestDNo Is Not Null ");
                SQL.AppendLine("And DNo In (" + DNo + "); ");
            }

            if (mIsMRUseBudgetForMR)
            {
                SQL.AppendLine("Update TblMaterialRequestHdr Set ");
                SQL.AppendLine("  BudgetForMR = @BudgetForMR ");
                SQL.AppendLine("Where DocNo = @DocNo; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if(mIsMRUseBudgetForMR)
                Sm.CmParam<Decimal>(ref cm, "@BudgetForMR", Decimal.Parse(TxtBudgetForMR.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand CancelMaterialRequestHdr(string Docno)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And (CancelInd='N' Or Status='C'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Docno);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateMR2File(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        private MySqlCommand UpdateMR2File2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        private MySqlCommand UpdateMR2File3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowMaterialRequestHdr(DocNo);
                ShowMaterialRequestDtl(DocNo);
                if(!mIsMRSaveAvailableBudget) ComputeRemainingBudget();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowMaterialRequestHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.DocNo, A.DocDt, A.LocalDocNo, A.DeptCode, A.SiteCode, A.BCCode, A.ReqType, A.FileName, A.FileName2, A.FileName3, ");
            SQL.AppendLine("A.TaxCode1, A.TaxCode2, A.TaxCode3,IfNull(A.SIDocNo, A.DRDocNo)As SIDocNo, A.DORequestDocNo, A.Remark ");
            if (mIsMRUseBudgetForMR) SQL.AppendLine(", A.BudgetForMR ");
            else SQL.AppendLine(", 0.00 As BudgetForMR ");
            if (mIsMRSaveAvailableBudget) SQL.AppendLine(", A.AvailableBudget ");
            else
            {
                SQL.AppendLine(", 0.00 As AvailableBudget ");
                ComputeRemainingBudget();
            }
            if (mIsMRUseProcurementType)
                SQL.AppendLine(", A.ProcurementType ");
            else
                SQL.AppendLine(", Null as ProcurementType ");
            if (mIsMRRoutineUseMRRoutineType)
                SQL.AppendLine(", A.MRType ");
            else
                SQL.AppendLine(", Null as MRType ");
            if (mIsMRRoutineUsePOSignedBy)
                SQL.AppendLine(", A.POSignBy ");
            else
                SQL.AppendLine(", Null as POSignBy ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblmaterialRequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblPORequestDtl C On B.DocNo = C.MaterialRequestDocNo And B.Dno = C.MaterialRequestDno And C.CancelInd = 'N' ");
            SQL.AppendLine("left Join TblPODtl D On C.DocNo = D.PORequestDocNo And C.Dno = D.PORequestDno And D.CancelInd = 'N' ");
            SQL.AppendLine("Left Join TblPOHdr E On D.DocNo = E.Docno ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "LocalDocNo", "ReqType", "DeptCode", "SiteCode", 
                        "BCCode", "TaxCode1", "TaxCode2", "TaxCode3", "SIDOcNo", 
                        "Remark", "DORequestDocNo", "FileName", "FileName2", "FileName3", 
                        "ProcurementType", "BudgetForMR", "AvailableBudget", "MRType", "POSignBy"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[3]));
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[4]), "N");
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[5]));
                        Sl.SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[6]), string.Empty);
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[9]));
                        TxtSIDocNo.EditValue = Sm.DrStr(dr, c[10]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                        TxtDORequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                        TxtFile.EditValue = Sm.DrStr(dr, c[13]);
                        TxtFile2.EditValue = Sm.DrStr(dr, c[14]);
                        TxtFile3.EditValue = Sm.DrStr(dr, c[15]);
                        Sm.SetLue(LueOptCode, Sm.DrStr(dr, c[16]));
                        TxtBudgetForMR.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                        if (mIsMRSaveAvailableBudget) TxtRemainingBudget.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        Sm.SetLue(LueMRType, Sm.DrStr(dr, c[19]));
                        SetLuePOSignBy(ref LuePOSignBy, Sm.DrStr(dr, c[20]));
                    }, true
                );
        }

        private void ShowMaterialRequestDtl(string DocNo)
        {
                
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc, ");
            SQL.AppendLine("(   Select T2.UserName From TblDocApproval T1, TblUser T2 ");
            SQL.AppendLine("    Where T1.DocType='MaterialRequest2' And T1.DocNo=@DocNo And T1.DNo=A.DNo And T1.UserCode=T2.UserCode And T1.UserCode Is Not Null  ");
            SQL.AppendLine("    Order By ApprovalDNo Desc Limit 1");
            SQL.AppendLine(") As UserName, ");
            SQL.AppendLine("A.ItCode, B.ItName, B.MinStock, B.ReorderStock, A.Qty, B.PurchaseUomCode,");
            SQL.AppendLine("A.UsageDt, A.QtDocNo, A.QtDNo, C.DocDt As QtDt, C.VdCode, E.Vdname, ");
            SQL.AppendLine("A.VdContactPerson, A.ShipTo, A.BillTo, C.CurCode, A.UPrice, ");
            SQL.AppendLine("(A.Qty*A.UPrice) As Total, A.Discount, A.DiscountAmt, A.RoundingValue, F.PtName, G.DtName, A.EstRecvDt, A.Remark, B.ItScCode, D.ItScName, A.DORequestDocNo, A.DORequestDNo ");
            SQL.AppendLine("From TblMaterialRequestDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblQtHdr C On A.QtDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItemSubCategory D On B.ItScCode = D.ItScCode ");
            SQL.AppendLine("Left Join TblVendor E on C.VdCode = E.VdCode");
            SQL.AppendLine("Left Join TblPaymentterm F On C.PtCode = F.PtCOde");
            SQL.AppendLine("Left Join TblDeliveryType G On C.DTCode=G.DTCode");
            //SQL.AppendLine("Left Join TblPORequestDtl H On A.Docno=H.MaterialRequestDocNo And A.Dno=H.MaterialRequestDNo And H.CancelInd = 'N' ");
            //SQL.AppendLine("Left Join TblPODtl I On H.DocNo=I.PORequestDocno And H.DNo=I.PORequestDno And I.CancelInd = 'N' ");
            //SQL.AppendLine("Left Join TblPOHdr J On I.DocNo=J.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo  ");
            SQL.AppendLine("Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "CancelInd", "StatusDesc", "UserName", "ItCode", "ItName", 
                    //6-10
                    "MinStock", "ReorderStock", "Qty", "PurchaseUomCode", "UsageDt", 
                    //11-15
                    "QtDocNo", "QtDNo", "QtDt", "VdCode", "Vdname", 
                    //16-20
                    "VdContactPerson", "ShipTo", "BillTo", "CurCode", "UPrice",  
                    //21-25
                    "Discount", "DiscountAmt", "RoundingValue", "PtName", "DtName", 
                    //26-30
                    "EstRecvDt", "Remark",  "ItScCode", "ItScName", "DORequestDocNo",
                    //31
                    "DORequestDNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);

                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 13, 10);

                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 14);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 15);

                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 17);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 18);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 19);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 25, 20);

                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 26, 21);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 27, 22);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 28, 23);
                    //Sm.SetGrdValue("N", Grd1, dr, c, Row, 29, 24);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 33, 24);

                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 34, 25);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 35, 26);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 36, 27);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 37, 28);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 38, 29);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 39, 30);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 40, 31);
                    ComputeTotal(Row);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 25, 26, 27, 28, 29, 30, 31, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
            ComputeTaxAmt();
        }

        #endregion

        #region Additional Method

        private string ItemSelection()
        {
            var SQL = new StringBuilder();

            if (Sm.CompareStr(Sm.GetLue(LueReqType), "2"))
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, Null As VdName, 0 As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("ifnull(C.Qty01, 0) As Mth01, ifnull(C.Qty03, 0) As Mth03, ifnull(C.Qty06, 0) As Mth06, ifnull(C.Qty09, 0) As Mth09, ifnull(C.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, D.ItScName, A.ItCodeInternal, A.ItGrpCode, E.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) C On C.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory D On A.ItScCode = D.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup E On A.ItGrpCode=E.ItGrpCode ");
            }
            else
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt, I.VdName, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("C.UPrice*");
                SQL.AppendLine("    Case When IfNull(C.CurCode, '')=D.ParValue Then 1 ");
                SQL.AppendLine("    Else IfNull(E.Amt, 0) ");
                SQL.AppendLine("    End As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("ifnull(F.Qty01, 0) As Mth01, ifnull(F.Qty03, 0) As Mth03, ifnull(F.Qty06, 0) As Mth06, ifnull(F.Qty09, 0) As Mth09, ifnull(F.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, G.ItScName, A.ItCodeInternal, A.ItGrpCode, H.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T1.VdCode, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                SQL.AppendLine("    From TblQtHdr T1 ");
                SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                SQL.AppendLine("        From TblQtHdr T3a, TblQtDtl T3b ");
                SQL.AppendLine("        Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' ");
                SQL.AppendLine("        Group By T3b.ItCode ");
                SQL.AppendLine("    ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                SQL.AppendLine(") C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Left Join TblParameter D On D.ParCode='MainCurCode' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                SQL.AppendLine("    From TblCurrency T1 ");
                SQL.AppendLine("    Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
                SQL.AppendLine("        From TblCurrencyRate T3a  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
                SQL.AppendLine("            From TblCurrencyRate Group By CurCode1, CurCode2 ");
                SQL.AppendLine("        ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                SQL.AppendLine("    ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                SQL.AppendLine(") E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) F On F.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory G On A.ItScCode=G.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup H On A.ItGrpCode=H.ItGrpCode ");
                SQL.AppendLine("Left Join TblVendor I On C.VdCode = I.VdCode ");

            }

            SQL.AppendLine("Where A.ActInd = 'Y' ");

            if (Sm.IsDataExist("Select ParCode From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                SQL.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");

            return SQL.ToString();
        }

        internal void ShowDORequestItem(string DocNo)
        {
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As DORequestDocNo, B.DNo As DORequestDNo, B.ItCode, ");
            SQL.AppendLine("X.ItCtName, X.PurchaseUomCode, X.DocNo, X.DNo, X.DocDt, X.VdName, X.UPrice, X.MinStock, X.ReorderStock, ");
            SQL.AppendLine("X.ItName, X.ForeignName, ");
            SQL.AppendLine("X.Mth01, X.Mth03, X.Mth06, X.Mth09, X.Mth12, ");
            SQL.AppendLine("X.ItScCode, X.ItScName, X.ItCodeInternal, X.ItGrpCode, X.ItGrpName, ");
            SQL.AppendLine("(IfNull(B.Qty, 0.00)-IfNull(D.Qty, 0.00)) As MRQty ");
            SQL.AppendLine("From TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblDORequestDeptDtl B On A.DocNo = B.DocNo And B.ProcessInd = 'O' ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine(ItemSelection());

            SQL.AppendLine(")X On B.ItCode = X.ItCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  Select ItCode, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("  From TblStockSummary ");
            SQL.AppendLine("  Where WhsCode = @WhsCode ");
            SQL.AppendLine("  And Qty>0 ");
            SQL.AppendLine("  Group By ItCode ");
            SQL.AppendLine(") D On B.ItCode = D.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And IfNull(B.Qty, 0.00)>IfNull(D.Qty, 0.00) ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetValue("Select WhsCode From TblDORequestDeptHdr Where DocNo = '" + DocNo + "'; "));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",

                    //1-5
                    "ItCodeInternal", "ItGrpName", "ItName", "ForeignName",  "ItCtName",   
                    
                    //6-7
                    "ItScCode", "ItScName", "PurchaseUomCode", "DocNo", "DNo", 

                    //11-15
                    "DocDt", "UPrice", "MinStock", "ReorderStock", "Mth01",   

                    //16-20
                    "Mth03", "Mth06", "Mth09", "Mth12", "DORequestDocNo",
                    
                    //21-23
                    "DORequestDNo", "MRQty", "VdName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 23);
                    Grd1.Cells[Row, 26].Value = Grd1.Cells[Row, 27].Value = Grd1.Cells[Row, 28].Value = 
                    Grd1.Cells[Row, 30].Value = Grd1.Cells[Row, 31].Value = Grd1.Cells[Row, 32].Value = 0;

                    if (mIsBudgetActive)
                        Grd1.Cells[Row, 8].ForeColor = Sm.GetGrdStr(Grd1, Row, 15).Length > 0 ? Color.Black : Color.Red;
                    ComputeTotal(Row);
                }, false, false, true, false
            );

            Sm.FocusGrd(Grd1, 0, 1);

            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 25, 26, 27, 28, 29, 30, 31, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void GetParameter()
        {
            //IsProcFormat = Sm.GetParameter("ProcFormatDocNo");
            //mIsMRUseProcurementType = Sm.GetParameterBoo("IsMRUseProcurementType");
            //mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            //mPOPrintOutCompanyLogo = Sm.GetParameter("POPrintOutCompanyLogo");
            //mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            //mIsShipInstructionInEximMandatory = Sm.GetParameter("IsShipInstructionInEximMandatory") == "Y";
            //mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            //mIsMRBudgetBasedOnBudgetCategory = Sm.GetParameterBoo("IsMRBudgetBasedOnBudgetCategory");
            //mIsDORequestNeedStockValidation = Sm.GetParameterBoo("IsDORequestNeedStockValidation");
            //mIsDORequestUseItemIssued = Sm.GetParameterBoo("IsDORequestUseItemIssued");
            //mIsBudgetActive = Sm.GetParameterBoo("IsBudgetActive");
            //mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            //mIsMaterialRequest2DocNoUseDifferentAbbr = Sm.GetParameterBoo("IsMaterialRequest2DocNoUseDifferentAbbr");
            //mIsMRSaveAvailableBudget = Sm.GetParameterBoo("IsMRSaveAvailableBudget");
            //mIsMRRoutineUseMRRoutineType = Sm.GetParameterBoo("IsMRRoutineUseMRRoutineType");
            //mIsMREximSplitDocument = Sm.GetParameter("IsMREximSplitDocument") == "Y";
            //mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            //mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            //mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            //mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            //mIsMRAllowToUploadFile = Sm.GetParameterBoo("IsMRAllowToUploadFile");
            //mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            //mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            //mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            //mIsRemarkMR2Mandatory = Sm.GetParameterBoo("IsRemarkMR2Mandatory");
            //mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            //mIsCASUsedForBudget = Sm.GetParameterBoo("IsCASUsedForBudget");
            //mProcurementTypeMrRoutine = Sm.GetParameter("ProcurementTypeMrRoutine");
            //mSourceAvailableBudgetForMR = Sm.GetParameter("SourceAvailableBudgetForMR");
            //mIsMRUseBudgetForMR = Sm.GetParameterBoo("IsMRUseBudgetForMR");
            //mIsAvailableBudgetMRAllowMinus = Sm.GetParameterBoo("IsAvailableBudgetMRAllowMinus");
            //mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
            //mIsMRApprovalByAmount = Sm.GetParameterBoo("IsMRApprovalByAmount");
            //mAmtSourceMRApproval = Sm.GetParameter("AmtSourceMRApproval");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'ProcFormatDocNo', 'IsMRUseProcurementType', 'IsFilterBySite', 'POPrintOutCompanyLogo', 'ReqTypeForNonBudget', ");
            SQL.AppendLine("'IsShipInstructionInEximMandatory', 'IsSiteMandatory', 'IsMRBudgetBasedOnBudgetCategory', 'IsDORequestNeedStockValidation', 'IsDORequestUseItemIssued', ");
            SQL.AppendLine("'IsBudgetActive', 'IsFilterByItCt', 'IsMaterialRequest2DocNoUseDifferentAbbr', 'IsMRSaveAvailableBudget', 'IsMRRoutineUseMRRoutineType', ");
            SQL.AppendLine("'IsMREximSplitDocument', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', ");
            SQL.AppendLine("'IsMRAllowToUploadFile', 'PortForFTPClient', 'FileSizeMaxUploadFTPClient', 'IsBudget2YearlyFormat', 'IsRemarkMR2Mandatory', ");
            SQL.AppendLine("'IsFilterByDept', 'IsCASUsedForBudget', 'ProcurementTypeMrRoutine', 'SourceAvailableBudgetForMR', 'IsMRUseBudgetForMR', 'IsMRRoutineUsePOSignedBy', ");
            SQL.AppendLine("'IsAvailableBudgetMRAllowMinus', 'IsApprovalBySiteMandatory', 'IsMRApprovalByAmount', 'AmtSourceMRApproval','IsDocApprovalSettingUseItemCt', 'FiscalYearRange' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsMRUseProcurementType": mIsMRUseProcurementType = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsShipInstructionInEximMandatory": mIsShipInstructionInEximMandatory = ParValue == "Y"; break;
                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsMRBudgetBasedOnBudgetCategory": mIsMRBudgetBasedOnBudgetCategory = ParValue == "Y"; break;
                            case "IsDORequestNeedStockValidation": mIsDORequestNeedStockValidation = ParValue == "Y"; break;
                            case "IsDORequestUseItemIssued": mIsDORequestUseItemIssued = ParValue == "Y"; break;
                            case "IsBudgetActive": mIsBudgetActive = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsMaterialRequest2DocNoUseDifferentAbbr": mIsMaterialRequest2DocNoUseDifferentAbbr = ParValue == "Y"; break;
                            case "IsMRSaveAvailableBudget": mIsMRSaveAvailableBudget = ParValue == "Y"; break;
                            case "IsMRRoutineUseMRRoutineType": mIsMRRoutineUseMRRoutineType = ParValue == "Y"; break;
                            case "IsMREximSplitDocument": mIsMREximSplitDocument = ParValue == "Y"; break;
                            case "IsMRAllowToUploadFile": mIsMRAllowToUploadFile = ParValue == "Y"; break;
                            case "IsBudget2YearlyFormat": mIsBudget2YearlyFormat = ParValue == "Y"; break;
                            case "IsRemarkMR2Mandatory": mIsRemarkMR2Mandatory = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsCASUsedForBudget": mIsCASUsedForBudget = ParValue == "Y"; break;
                            case "IsMRUseBudgetForMR": mIsMRUseBudgetForMR = ParValue == "Y"; break;
                            case "IsAvailableBudgetMRAllowMinus": mIsAvailableBudgetMRAllowMinus = ParValue == "Y"; break;
                            case "IsApprovalBySiteMandatory": mIsApprovalBySiteMandatory = ParValue == "Y"; break;
                            case "IsMRApprovalByAmount": mIsMRApprovalByAmount = ParValue == "Y"; break;
                            case "IsDocApprovalSettingUseItemCt": mIsDocApprovalSettingUseItemCt = ParValue == "Y"; break;
                            case "IsMRRoutineUsePOSignedBy": mIsMRRoutineUsePOSignedBy = ParValue == "Y"; break;
                            case "FiscalYearRange": mFiscalYearRange = ParValue; break;

                            //string
                            case "ProcFormatDocNo": IsProcFormat = ParValue; break;
                            case "POPrintOutCompanyLogo": mPOPrintOutCompanyLogo = ParValue; break;
                            case "ReqTypeForNonBudget": mReqTypeForNonBudget = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "ProcurementTypeMrRoutine": mProcurementTypeMrRoutine = ParValue; break;
                            case "SourceAvailableBudgetForMR": mSourceAvailableBudgetForMR = ParValue; break;
                            case "AmtSourceMRApproval": mAmtSourceMRApproval = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #region MR
        
        private static string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory, int nilaitambah)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = string.Empty;
            if (Sm.GetParameterBoo("IsDocSeqNoEnabled"))
                DocSeqNo = Sm.GetParameter("DocSeqNo");
            else
                DocSeqNo = "4";

            var SQL = new StringBuilder();

            if (IsProcFormat == "1")
            {
                SQL.Append("Select Concat('"+SubCategory+"', '/', ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('00000000', Convert(DocNo+1+" + nilaitambah + ", Char)), " + DocSeqNo + ") From ( ");
                SQL.Append("         Select Convert(Substring(DocNo, locate('"+DocTitle+"', DocNo)-5," + DocSeqNo + "), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5," + DocSeqNo + "), Decimal) Desc Limit 1");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '"+Sm.Right("0000000" + (nilaitambah + 1).ToString(), 4)+"') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('00000000', Convert(DocNo+1+" + nilaitambah + ", Char)), " + DocSeqNo + ") From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5," + DocSeqNo + "), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5," + DocSeqNo + "), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '" + Sm.Right("0000000" + (nilaitambah + 1).ToString(), int.Parse(DocSeqNo)) + "') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }

            return Sm.GetValue(SQL.ToString());
        }
       
        private string GenerateDocNo2(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mDocNo = string.Empty;
            string Yr = Sm.Left(DocDt, 4);
            string Mth = DocDt.Substring(4, 2);
            string DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType);
            string ShortCode = Sm.GetValue("Select B.ShortCode From TblDepartment A Inner Join TblDivision B ON A.DivisionCode = B.DivisionCode WHERE A.DeptCode = @Param; ", Sm.GetLue(LueDeptCode));

            SQL.AppendLine("Select Concat(  ");
            SQL.AppendLine("IfNull((  ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From (  ");
            SQL.AppendLine("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.AppendLine("       Where Right(DocNo, LENGTH(CONCAT(@DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr)))=Concat(@DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr)  ");
            SQL.AppendLine("       Order By Left(DocNo, 4) Desc Limit 1  ");
            SQL.AppendLine("       ) As Temp  ");
            SQL.AppendLine("   ), '0001')  ");
            SQL.AppendLine(", '/', @DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.AppendLine(") As DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@ShortCode", ShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 6) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }
        private decimal ComputeAvailableBudget()
        {
            var Amt = 0m;
            var DocDt = Sm.GetDte(DteDocDt);
            var DeptCode = Sm.GetLue(LueDeptCode);
            var BCCode = Sm.GetLue(LueBCCode);

            if (DocDt.Length>0 && DeptCode.Length>0 && BCCode.Length>0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select ");
                
                if(!mIsBudget2YearlyFormat)
                    SQL.AppendLine("A.Amt2 - ");
                else
                    SQL.AppendLine("SUM(A.Amt2) - ");

                SQL.AppendLine(" IfNull(B.Amt, 0)-IfNull(C.Amt, 0)-IfNull(D.Amt, 0)+IFNULL(Sum(E.Amt), 0) ");
                
                if (mIsCASUsedForBudget)
                    SQL.AppendLine(" - IfNull(F.Amt, 0.00) ");
                
                SQL.AppendLine("As Amt ");
                SQL.AppendLine("From TblBudgetSummary A ");
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("        Select Sum(T2.Qty*T2.UPrice) As Amt ");
                SQL.AppendLine("        From TblMaterialRequestHdr T1, TblMaterialRequestDtl T2 ");
                SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And Left(T1.DocDt, 4)=@Yr ");
                if(!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Substring(T1.DocDt, 5, 2)=@Mth ");
                SQL.AppendLine("        And T1.DeptCode=@DeptCode ");
                SQL.AppendLine("        And T1.BCCode=@BCCode ");
                SQL.AppendLine("        And T1.ReqType<>@ReqType ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.Status In ('O', 'A') ");
                SQL.AppendLine("        And T2.CancelInd='N' ");
                SQL.AppendLine("        And T2.Status In ('O', 'A') ");
                SQL.AppendLine("        And T1.DocNo<>@DocNo ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("        Select Sum(Amt) Amt ");
                SQL.AppendLine("        From TblVoucherRequestHdr ");
                SQL.AppendLine("        Where DocNo <> @DocNo ");
                SQL.AppendLine("        And Find_In_Set(DocType, ");
                SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
                SQL.AppendLine("        And CancelInd = 'N' ");
                SQL.AppendLine("        And Status In ('O', 'A') ");
                SQL.AppendLine("        And ReqType Is Not Null ");
                SQL.AppendLine("        And ReqType <> @ReqType ");
                SQL.AppendLine("        And DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine(") C On 0 = 0 ");

                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("        Select Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) Amt ");
                SQL.AppendLine("        From TblTravelRequestHdr A ");
                SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("        Where A.DocNo <> @DocNo ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.Status In ('O', 'A') ");
                SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(B.BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ) D On 0 = 0 ");

                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("       	   Select Case when A.AcType = 'D' Then IFNULL(A.Amt, 0.00) ELSE IFNULL(A.Amt*-1, 0.00) END As Amt ");
                SQL.AppendLine("           From tblvoucherhdr A  ");
                SQL.AppendLine("           Inner Join  ");
                SQL.AppendLine("             ( ");
                SQL.AppendLine("              SELECT X1.DocNo, X1.VoucherRequestDocNo   ");
                SQL.AppendLine("              From tblvoucherhdr X1  ");
                SQL.AppendLine("              INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
                SQL.AppendLine("              AND X1.DocType = '58' ");
                SQL.AppendLine("              AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("              AND X2.Status In ('O', 'A')  ");
                SQL.AppendLine("             ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("           Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("           Inner Join ");
                SQL.AppendLine("             ( ");
                SQL.AppendLine("              SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("              From tblvoucherhdr X1  ");
                SQL.AppendLine("              Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("              INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("              AND X1.DocType = '56' ");
                SQL.AppendLine("           	  AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("         	  AND X3.Status In ('O', 'A')  ");
                SQL.AppendLine("             ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("            Where A.DocNo <> @DocNo  ");
                SQL.AppendLine("    And D.DeptCode = @DeptCode  ");
                if (!mIsBudget2YearlyFormat)
                {
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr  ");
                    SQL.AppendLine("    And SUBSTRING(A.DocDt, 5, 2) = @Mth ");
                }
                else
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr  ");
                SQL.AppendLine("           And IfNull(D.BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ) E On 0 = 0 ");

                if (mIsCASUsedForBudget)
                {
                    SQL.AppendLine("Left Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select Left(A.DocDt, 4) Yr, C.BCCode, C.DeptCode, Sum(B.Amt) Amt, ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("    Substring(A.DocDt, 5, 2) As Mth ");
                    else
                        SQL.AppendLine("    '00' As Mth ");
                    SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("        And A.`Status` = 'A' And A.CancelInd = 'N' ");
                    SQL.AppendLine("        And A.DocStatus = 'F' ");
                    SQL.AppendLine("        And B.CCtCode Is Not Null ");
                    SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Substring(A.DocDt, 5, 2) = @Mth ");
                    SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                    SQL.AppendLine("        And C.CCtCode Is Not Null ");
                    SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                    SQL.AppendLine("        And C.BCCode = @BCCode ");
                    SQL.AppendLine("    Group By Left(A.DocDt, 4), ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("    Substring(A.DocDt, 5, 2), ");
                    SQL.AppendLine("    C.BCCode, C.DeptCode ");
                    SQL.AppendLine(") F On 0 = 0 ");
                }

                SQL.AppendLine("Where A.Yr=@Yr ");
                if (!mIsBudget2YearlyFormat)
                     SQL.AppendLine("And A.Mth=@Mth ");
                SQL.AppendLine("And A.DeptCode=@DeptCode ");
                SQL.AppendLine("And A.BCCode=@BCCode;");

                var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@ReqType", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
                Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
                Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length!=0)?TxtDocNo.Text:"XXX");
                Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(DocDt, 4));
                Sm.CmParam<String>(ref cm, "@Mth", DocDt.Substring(4, 2));
                
                var Value = Sm.GetValue(cm);
                if (Value.Length != 0) Amt = decimal.Parse(Value);
            }
            
            return Amt;
        }

        public void ComputeRemainingBudget()
        {
            decimal AvailableBudget = 0m, RequestedBudget = 0m;
            try
            {
                var ReqType = Sm.GetLue(LueReqType);
                if (ReqType.Length>0 && !Sm.CompareStr(ReqType, mReqTypeForNonBudget))
                {
                    AvailableBudget = mFiscalYearRange.Length == 0 ?
                        Sm.ComputeAvailableBudget(
                        (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueSiteCode), Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                        )
                        :
                        Sm.ComputeAvailableBudget2(
                        (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueSiteCode), Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                        )
                        ;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 29).Length != 0)
                            RequestedBudget += Sm.GetGrdDec(Grd1, Row, 29);
                    }

                    if (mFiscalYearRange.Length > 0)
                    {
                        var MR = new FrmMaterialRequest(mMenuCode);
                        string[] splitMth = mFiscalYearRange.Split(',');
                        string Yr = Sm.Left(Sm.GetDte(DteDocDt), 4);
                        string YrMth = Sm.Left(Sm.GetDte(DteDocDt), 6);
                        string YrMthFiscal1 = string.Concat(Yr, splitMth[0]);
                        if (Int32.Parse(YrMth) < Int32.Parse(YrMthFiscal1)) Yr = (Int32.Parse(Yr) - 1).ToString();
                        decimal UsedBudget = 0m; // used budget sudah ada di perhitungan available budget ternyata //  GetUsedBudget(Sm.GetLue(LueDeptCode), Yr, Sm.Right(YrMth, 2), Sm.GetLue(LueBCCode));
                        decimal TransferredBudget = MR.GetTransferredBudget(Sm.GetLue(LueDeptCode), Yr, Sm.GetLue(LueBCCode));
                        AvailableBudget = AvailableBudget - UsedBudget + TransferredBudget;
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            if (mSourceAvailableBudgetForMR == "2")
            {
                TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget, 0);
            }
            else
            {
                TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget - RequestedBudget, 0);
            }
        }

        public void ComputeBudgetForMR()
        {
            if (!mIsMRUseBudgetForMR) return;
            if (Sm.GetLue(LueDeptCode).Length == 0 || Sm.GetLue(LueBCCode).Length == 0 || Sm.GetDte(DteDocDt).Length == 0 || Sm.GetLue(LueReqType) != "1") return;

            decimal AvailableBudget = 0m, RequestedBudget1 = 0m, RequestedBudget2 = 0m;
            string data = string.Empty;
            var SQL = new StringBuilder();

            try
            {
                var ReqType = Sm.GetLue(LueReqType);
                if (ReqType.Length > 0 && !Sm.CompareStr(ReqType, mReqTypeForNonBudget))
                {
                    AvailableBudget = Sm.GetParameter("FiscalYearRange").Length == 0 ?
                        Sm.ComputeAvailableBudget(
                        (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueSiteCode), Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                        )
                        :
                        Sm.ComputeAvailableBudget2(
                        (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueSiteCode), Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                        )
                        ;

                    //Compute MR yg belum dijournalkan
                    #region old code // commented by web karena used budget ke double
                    //SQL.AppendLine("SELECT SUM(Amt) Amt ");
                    //SQL.AppendLine("FROM ");
                    //SQL.AppendLine("( ");
                    //SQL.AppendLine("    SELECT ");
                    //SQL.AppendLine("    Case ");
                    //SQL.AppendLine("        When B.TotalBeforeTax = B.TotalRevBeforeTax Then B.TotalBeforeTax + A.TaxAmt + A.CustomsTaxAmt - A.DiscountAmt ");
                    //SQL.AppendLine("        ELSE B.TotalRevBeforeTax + (B.TotalRevBeforeTax*(C.TaxRate/100) + (B.TotalRevBeforeTax*(D.TaxRate/100) + (B.TotalRevBeforeTax*(E.TaxRate/100)))) + A.CustomsTaxAmt - A.DiscountAmt ");
                    //SQL.AppendLine("	END Amt ");
                    //SQL.AppendLine("	FROM TblPOHdr A ");
                    //SQL.AppendLine("	INNER JOIN ( ");
                    //SQL.AppendLine("		SELECT A.DocNo, SUM(A.TotalBeforeTax) TotalBeforeTax, SUM(A.TotalRevBeforeTax) TotalRevBeforeTax ");
                    //SQL.AppendLine("		FROM ( ");
                    //SQL.AppendLine("			SELECT A.DocNo, B.DNo, ");
                    //SQL.AppendLine("			((((100-IFNULL(I.DiscountOld, B.Discount))/100)*B.Qty*IfNull(I.UPrice, H.UPrice)) - IfNull(I.DiscountAmtOld, B.DiscountAmt) + IfNull(I.RoundingValueOld, B.RoundingValue)) TotalBeforeTax, ");
                    //SQL.AppendLine("            ((((100-IfNull(J.Discount, B.Discount))/100)*(IfNull(J.Qty, B.Qty) * IfNull(J.UPrice, H.UPrice))) - IfNull(J.DiscountAmt, B.DiscountAmt) + IfNull(J.RoundingValue, B.RoundingValue)) TotalRevBeforeTax ");
                    //SQL.AppendLine("			FROM TblPOHdr A ");
                    //SQL.AppendLine("			INNER JOIN TblPODtl B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' ");
                    //SQL.AppendLine("			INNER JOIN TblPORequestDtl D ON B.PORequestDocNo = D.DocNo ");
                    //SQL.AppendLine("				AND B.PORequestDNo = D.DNo ");
                    //SQL.AppendLine("				AND D.CancelInd = 'N' ");
                    //SQL.AppendLine("			INNER JOIN TblMaterialRequestHdr E ON D.MaterialRequestDocNo = E.DocNo ");
                    //SQL.AppendLine("				AND E.CancelInd = 'N' ");
                    //SQL.AppendLine("				AND IFNULL(E.Status, 'O') <> 'C' ");
                    //SQL.AppendLine("				AND E.ReqType <> @ReqTypeForNonBudget ");
                    //SQL.AppendLine("                AND E.BCCode = @BCCode ");
                    //SQL.AppendLine("                AND E.DeptCode = @DeptCode ");
                    //SQL.AppendLine("                AND Left(E.DocDt, 6) = @YrMth ");
                    //SQL.AppendLine("            INNER JOIN TblMaterialRequestDtl F ON D.MaterialRequestDocNo = F.DocNo AND D.MaterialRequestDNo = F.DNo ");
                    //SQL.AppendLine("			Inner Join TblQtHdr G On D.QtDocNo=G.DocNo ");
                    //SQL.AppendLine("			Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo ");
                    //SQL.AppendLine("			Left Join ( ");
                    //SQL.AppendLine("                Select A.DocNo AS PoDocNo, A.DNo AS PoDNo, C.DocNo AS QtDocNo, C.UPrice, B.Discountold, B.DiscountAmtOld, B.RoundingValueOld ");
                    //SQL.AppendLine("                From TblPoDtl A ");
                    //SQL.AppendLine("			    Inner JOIN TblPoRevision B ON A.DocNo =  B.podocno AND A.DNo = B.PoDNo ");
                    //SQL.AppendLine("			    Inner Join TblQtDtl C ON B.QtDocNoOld = C.DocNo AND B.QtDNoOld = C.DNo ");
                    //SQL.AppendLine("			    Where B.docno = (Select MIN(DocNo) From TblPoRevision Where PoDocNo = B.PoDocNo And PODNo = B.PODNo And Status = 'A' ) ");
                    //SQL.AppendLine("			)I ON A.DocNo = I.PoDocNo And B.DNo = I.PoDNo ");
                    //SQL.AppendLine("			Left Join ( ");
                    //SQL.AppendLine("			    SELECT A.DocNo AS PoDoCno, A.DNo AS PoDNo, B.Qty, C.UPrice, B.Discount, B.DiscountAmt, B.RoundingValue ");
                    //SQL.AppendLine("			    FROM tblpodtl A ");
                    //SQL.AppendLine("			    Inner JOIN TblPoRevision B ON A.DocNo =  B.podocno AND A.DNo = B.PoDNo ");
                    //SQL.AppendLine("			    Inner Join TblQtDtl C ON B.QtDocNo = C.DocNo AND B.QtDNo = C.DNo ");
                    //SQL.AppendLine("			    Where B.docno = (Select max(DocNo) From TblPoRevision Where PoDocNo = B.PoDocNo And PODNo = B.PODNo And Status = 'A') ");
                    //SQL.AppendLine("			) J ON A.DocNo = J.PoDocNo AND B.DNo = J.PoDNo ");
                    //SQL.AppendLine("		)A ");
                    //SQL.AppendLine("		GROUP BY A.DocNo ");
                    //SQL.AppendLine("	) B ON A.DocNo = B.DocNo ");
                    //SQL.AppendLine("	LEFT JOIN TblTax C ON A.TaxCode1 = C.TaxCode");
                    //SQL.AppendLine("	LEFT JOIN TblTax D ON A.TaxCode2 = D.TaxCode");
                    //SQL.AppendLine("	LEFT JOIN TblTax E ON A.TaxCode3 = E.TaxCode");
                    //SQL.AppendLine("    UNION ALL ");
                    //SQL.AppendLine("	SELECT SUM(B.TotalPrice) Amt ");
                    //SQL.AppendLine("	FROM TblMaterialRequestHdr A ");
                    //SQL.AppendLine("	INNER JOIN TblMaterialRequestDtl B ON A.DocNo = B.DocNo ");
                    //SQL.AppendLine("		AND IFNULL(A.Status, 'O') <> 'C' ");
                    //SQL.AppendLine("		AND B.CancelInd = 'N' ");
                    //SQL.AppendLine("		AND A.ReqType <> @ReqTypeForNonBudget ");
                    //SQL.AppendLine("	    AND A.BCCode = @BCCode ");
                    //SQL.AppendLine("        AND A.DeptCode = @DeptCode ");
                    //SQL.AppendLine("        AND LEFT(A.DocDt, 6) = @YrMth ");
                    //SQL.AppendLine("	WHERE NOT EXISTS ( ");
                    //SQL.AppendLine("		SELECT 1 ");
                    //SQL.AppendLine("		FROM TblPODtl X1 ");
                    //SQL.AppendLine("		INNER JOIN TblPORequestDtl X2 ON X1.PORequestDocNo = X2.DocNo ");
                    //SQL.AppendLine("			AND X1.PORequestDNo = X2.DNo ");
                    //SQL.AppendLine("			AND X1.CancelInd = 'N' ");
                    //SQL.AppendLine("		INNER JOIN TblMaterialRequestDtl X3 ON X2.MaterialRequestDocNo = X3.DocNo ");
                    //SQL.AppendLine("			AND X2.MaterialRequestDNo = X3.DNo ");
                    //SQL.AppendLine("			AND X3.CancelInd = 'N' ");
                    //SQL.AppendLine("		WHERE X3.DOcNo = A.DocNo AND X3.DNo = B.DNo ");
                    //SQL.AppendLine("	) ");
                    //SQL.AppendLine(") Tbl ");
                    #endregion

                    SQL.AppendLine("Select ");
                    SQL.AppendLine("        Sum(B.Qty*B.UPrice) ");
                    SQL.AppendLine("As MRAmt ");
                    SQL.AppendLine("From TblMaterialRequestHdr A ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("    And B.CancelInd = 'N' And B.Status In('O', 'A') ");
                    SQL.AppendLine("    And A.ReqType<> @ReqTypeForNonBudget ");
                    SQL.AppendLine("    And A.BCCode = @BCCode ");
                    SQL.AppendLine("    And A.DeptCode = @DeptCode ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) = @YrMth ");
                    SQL.AppendLine("    And Concat(B.DocNo, B.DNo) Not In ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select Distinct Concat(T2.MaterialRequestDocNo, T2.MaterialRequestDNo) ");
                    SQL.AppendLine("        From TblPODtl T1 ");
                    SQL.AppendLine("        Inner Join TblPORequestDtl T2 On T1.PORequestDocNo = T2.DocNo And T1.PORequestDNo = T2.DNo ");
                    SQL.AppendLine("            And T1.CancelInd = 'N' ");
                    SQL.AppendLine("            And T2.CancelInd = 'N' ");
                    SQL.AppendLine("            And T2.Status In ('O', 'A') ");
                    SQL.AppendLine("    ); ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                    Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                    Sm.CmParam<String>(ref cm, "@YrMth", (Sm.GetDte(DteDocDt).Length > 0 ? Sm.Left(Sm.GetDte(DteDocDt), 6) : ""));

                    data = Sm.GetValue(cm);
                    if (data.Length > 0 && TxtDocNo.Text.Length == 0) RequestedBudget1 = Decimal.Parse(data);

                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (TxtDocNo.Text.Length == 0 && Sm.GetGrdStr(Grd1, Row, 29).Length != 0)
                            RequestedBudget2 += Sm.GetGrdDec(Grd1, Row, 29);
                        if (TxtDocNo.Text.Length > 0 && Sm.GetGrdBool(Grd1, Row, 1))
                            RequestedBudget2 -= Sm.GetGrdDec(Grd1, Row, 29);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

            TxtBudgetForMR.Text = Sm.FormatNum(AvailableBudget - RequestedBudget1 - RequestedBudget2, 0);
        }

        private void ParPrint(int Indicator, string PODocNo)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            switch (Indicator)
            {
                case 0:
                    var l = new List<MatReq>();
                    var ldtl = new List<MatReqDtl>();
                    string[] TableName = { "MatReq", "MatReqDtl" };
                    List<IList> myLists = new List<IList>();
                    var cm = new MySqlCommand();
                    DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

                    #region Header

                    var SQL = new StringBuilder();
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', '' As CompanyAddressCity, ");
                    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', E.SiteName AS SiteName, ");
                    SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.DeptName, C.OptDesc, A.Remark, D.UserName As CreateBy ");
                    SQL.AppendLine("From TblMaterialRequestHdr A ");
                    SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
                    SQL.AppendLine("Inner Join TblOption C On A.ReqType = C.OptCode ");
                    SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode ");
                    SQL.AppendLine("Left join TblSite E On A.SiteCode = E.SiteCode ");
                    SQL.AppendLine("Where DocNo=@DocNo And C.OptCat = 'ReqType' ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",

                         "DeptName",
                         "OptDesc",
                         "Remark",
                         "CreateBy",
                         "CompanyAddressCity",

                         "SiteName"

                        });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                l.Add(new MatReq()
                                {
                                    CompanyLogo = Sm.DrStr(dr, c[0]),

                                    CompanyName = Sm.DrStr(dr, c[1]),
                                    CompanyAddress = Sm.DrStr(dr, c[2]),
                                    CompanyPhone = Sm.DrStr(dr, c[3]),
                                    DocNo = Sm.DrStr(dr, c[4]),
                                    DocDt = Sm.DrStr(dr, c[5]),

                                    DeptName = Sm.DrStr(dr, c[6]),
                                    OptDesc = Sm.DrStr(dr, c[7]),
                                    HRemark = Sm.DrStr(dr, c[8]),
                                    CreateBy = Sm.DrStr(dr, c[9]),
                                    CompanyAddressCity = Sm.DrStr(dr, c[10]),

                                    SiteName = Sm.DrStr(dr, c[11]),
                                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                                });
                            }
                        }
                        dr.Close();
                    }
                    myLists.Add(l);
                    #endregion

                    #region Detail

                    var cmDtl = new MySqlCommand();

                    var SQLDtl = new StringBuilder();
                    using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl.Open();
                        cmDtl.Connection = cnDtl;
                        SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.Qty, B.PurchaseUomCode, B.ForeignName, ");
                        SQLDtl.AppendLine("DATE_FORMAT(A.UsageDt,'%d/%m/%Y') As UsageDt, ifnull(C.Qty01, 0) As Mth01, ");
                        SQLDtl.AppendLine("ifnull(C.Qty03, 0) As Mth03, ifnull(C.Qty06, 0) As Mth06, ifnull(C.Qty09, 0) As Mth09, ifnull(C.Qty12, 0) As Mth12, Concat('Remark : ', A.Remark) As Remark ");
                        SQLDtl.AppendLine("From TblMaterialRequestDtl A ");
                        SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                        SQLDtl.AppendLine(" Left Join ( ");
                        SQLDtl.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                        SQLDtl.AppendLine("        From ( ");
                        SQLDtl.AppendLine("        select Z1.itCode, ");
                        SQLDtl.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                        SQLDtl.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                        SQLDtl.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                        SQLDtl.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                        SQLDtl.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                        SQLDtl.AppendLine("            From ");
                        SQLDtl.AppendLine("            ( ");
                        SQLDtl.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                        SQLDtl.AppendLine("                From ");
                        SQLDtl.AppendLine("                ( ");
                        SQLDtl.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                        SQLDtl.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                        SQLDtl.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                        SQLDtl.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                        SQLDtl.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                        SQLDtl.AppendLine("                )T1 ");
                        SQLDtl.AppendLine("                Inner Join ");
                        SQLDtl.AppendLine("                ( ");
                        SQLDtl.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                        SQLDtl.AppendLine("	                From TblDODeptHdr A ");
                        SQLDtl.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQLDtl.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQLDtl.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt And last_day(@MthDocDt) ");
                        SQLDtl.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                        SQLDtl.AppendLine("	                Union ALL ");
                        SQLDtl.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                        SQLDtl.AppendLine("	                From TblDODeptHdr A ");
                        SQLDtl.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQLDtl.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQLDtl.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt2 And last_day(@MthDocDt3) ");
                        SQLDtl.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQLDtl.AppendLine("	                Union All ");
                        SQLDtl.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                        SQLDtl.AppendLine("	                From TblDODeptHdr A ");
                        SQLDtl.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQLDtl.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQLDtl.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt4 And last_day(@MthDocDt3) ");
                        SQLDtl.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQLDtl.AppendLine("	                Union All ");
                        SQLDtl.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                        SQLDtl.AppendLine("	                From TblDODeptHdr A ");
                        SQLDtl.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQLDtl.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQLDtl.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt5 And last_day(@MthDocDt3) ");
                        SQLDtl.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQLDtl.AppendLine("	                Union All ");
                        SQLDtl.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                        SQLDtl.AppendLine("	                From TblDODeptHdr A ");
                        SQLDtl.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQLDtl.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQLDtl.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt6 And last_day(@MthDocDt3) ");
                        SQLDtl.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQLDtl.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                        SQLDtl.AppendLine("            Group By T1.mth, T2.ItCode ");
                        SQLDtl.AppendLine("        )Z1 ");
                        SQLDtl.AppendLine("   )Z2 Group By Z2.ItCode ");
                        SQLDtl.AppendLine(" )C On C.ItCode = A.ItCode ");
                        SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                        SQLDtl.AppendLine("And A.CancelInd = 'N' ");
                        SQLDtl.AppendLine("Order By A.ItCode;");

                        cmDtl.CommandText = SQLDtl.ToString();

                        Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                        Sm.CmParamDt(ref cmDtl, "@MthDocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
                        Sm.CmParamDt(ref cmDtl, "@MthDocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
                        Sm.CmParamDt(ref cmDtl, "@MthDocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
                        Sm.CmParamDt(ref cmDtl, "@MthDocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
                        Sm.CmParamDt(ref cmDtl, "@MthDocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
                        Sm.CmParamDt(ref cmDtl, "@MthDocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));

                        var drDtl = cmDtl.ExecuteReader();
                        var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ItCode" ,

                     //1-5
                     "ItName" ,
                     "Qty",
                     "PurchaseUomCode",
                     "UsageDt",
                     "Mth01",
                     //6-10
                     "Mth03",
                     "Mth06",
                     "Mth09",
                     "Mth12",
                     "Remark",

                     "ForeignName"
                    });
                        if (drDtl.HasRows)
                        {
                            int nomor = 0;
                            while (drDtl.Read())
                            {
                                nomor = nomor + 1;
                                ldtl.Add(new MatReqDtl()
                                {
                                    nomor = nomor,
                                    ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                    ItName = Sm.DrStr(drDtl, cDtl[1]),
                                    Qty = Sm.DrDec(drDtl, cDtl[2]),
                                    PurchaseUomCode = Sm.DrStr(drDtl, cDtl[3]),
                                    UsageDt = Sm.DrStr(drDtl, cDtl[4]),
                                    Mth01 = Sm.DrDec(drDtl, cDtl[5]),
                                    Mth03 = Sm.DrDec(drDtl, cDtl[6]),
                                    Mth06 = Sm.DrDec(drDtl, cDtl[7]),
                                    Mth09 = Sm.DrDec(drDtl, cDtl[8]),
                                    Mth12 = Sm.DrDec(drDtl, cDtl[9]),
                                    DRemark = Sm.DrStr(drDtl, cDtl[10]),
                                    ForeignName = Sm.DrStr(drDtl, cDtl[11]),
                                });
                            }
                        }
                        drDtl.Close();
                    }
                    myLists.Add(ldtl);
                    #endregion

                    if (Sm.GetParameter("IsUseItemConsumption") == "N")
                    {
                        if (Sm.GetParameter("DocTitle") == "TWC")
                            Sm.PrintReport("MaterialRequest2TWC", myLists, TableName, false);
                        else
                            Sm.PrintReport("MaterialRequest", myLists, TableName, false);
                    }
                    else
                    {
                        Sm.PrintReport("MaterialRequest2", myLists, TableName, false);
                    }

                    break;

                case 1:
                    var l2 = new List<MatReqMAI>();
                    var ldtl2 = new List<MatReqDtlMAI>();
                    var ldtl3 = new List<MatReqDtlMAI2>();
                    var ldtl4 = new List<MatReqDtlMAI3>();
                    var ldtl5 = new List<MatReqDtlMAI4>();
                    string[] TableName2 = { "MatReqMAI", "MatReqDtlMAI", "MatReqDtlMAI2", "MatReqDtlMAI3", "MatReqDtlMAI4" };
                    List<IList> myLists2 = new List<IList>();
                    string RptName = Sm.GetValue("SELECT ParValue FROM TblParameter WHERE ParCode = 'FormPrintOutMRExim'");

                    #region Header PO

                    var cmMAI = new MySqlCommand();
                    var SQLMAI = new StringBuilder();

                    if (mIsFilterBySite)
                    {
                        SQLMAI.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, I.CompanyName, I.CompanyPhone, I.CompanyFax, I.CompanyAddress, '' As CompanyAddressCity, ");
                    }
                    else
                    {
                        SQLMAI.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
                        SQLMAI.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                        SQLMAI.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                        SQLMAI.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                        SQLMAI.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                        SQLMAI.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                    }
                    SQLMAI.AppendLine("If((Select parvalue From TblParameter Where ParCode = 'IsFilterLocalDocNo')='Y',A.LocalDocNo,A.Docno) As DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.VdContactPerson, A.ShipTo, A.BillTo, ");
                    SQLMAI.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1,IfNull(A.TaxCode2, null) As TaxCode2, IfNull(A.TaxCode3, null) As TaxCode3, A.TaxAmt, A.CustomsTaxAmt, ");
                    SQLMAI.AppendLine("A.DiscountAmt, A.Amt, A.DownPayment, A.Remark As ARemark, F.VdName, ");
                    SQLMAI.AppendLine("(Select Z.TaxName From TblTax Z ");
                    SQLMAI.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
                    SQLMAI.AppendLine("(Select Z.TaxName From TblTax Z ");
                    SQLMAI.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
                    SQLMAI.AppendLine("(Select  Z.TaxName From TblTax Z ");
                    SQLMAI.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxName3, ");
                    SQLMAI.AppendLine("G.UserName As CreateBy, ");
                    SQLMAI.AppendLine("(Select Z.TaxRate From TblTax Z ");
                    SQLMAI.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxRate1, ");
                    SQLMAI.AppendLine("(Select Z.TaxRate From TblTax Z ");
                    SQLMAI.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxRate2, ");
                    SQLMAI.AppendLine("(Select  Z.TaxRate From TblTax Z ");
                    SQLMAI.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxRate3, ");
                    SQLMAI.AppendLine("F.Address, F.Phone, F.Fax, F.Email, ");
                    SQLMAI.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', H.ContactNumber, ");
                    SQLMAI.AppendLine("(Select ParValue From TblParameter Where Parcode='IsPOSplitBasedOnTax') As SplitPO, J.SiteName, A.LocalDocNo, E.CurCode  ");
                    SQLMAI.AppendLine("From TblPOHdr A ");
                    SQLMAI.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                    SQLMAI.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                    SQLMAI.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
                    SQLMAI.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
                    SQLMAI.AppendLine("Inner Join TblVendor F On E.VdCode=F.VdCode ");
                    SQLMAI.AppendLine("Inner Join TblUser G On A.CreateBy=G.UserCode ");
                    SQLMAI.AppendLine("Left Join TblVendorContactPerson H On A.VdContactPerson=H.ContactPersonName And A.VdCode = H.VdCode ");
                    SQLMAI.AppendLine("Left Join TblSite J On A.SiteCode = J.SiteCode ");
                    if (mIsFilterBySite)
                    {
                        SQLMAI.AppendLine("Left Join (");
                        SQLMAI.AppendLine("    Select distinct A.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                        SQLMAI.AppendLine("    From TblPOhdr A  ");
                        SQLMAI.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                        SQLMAI.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                        SQLMAI.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                        SQLMAI.AppendLine("    Where A.DocNo=@DocNo And A.SiteCode is not null ");
                        SQLMAI.AppendLine(") I On A.DocNo = I.DocNo ");
                    }
                    SQLMAI.AppendLine("Where A.DocNo=@DocNo ");

                    using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn2.Open();
                        cmMAI.Connection = cn2;
                        cmMAI.CommandText = SQLMAI.ToString();
                        Sm.CmParam<String>(ref cmMAI, "@DocNo", PODocNo);
                        if (mIsFilterBySite)
                        {
                            var SQLLogo = new StringBuilder();

                            SQLLogo.AppendLine("SELECT D.EntLogoName ");
                            SQLLogo.AppendLine("FROM TblPOHdr A ");
                            SQLLogo.AppendLine("INNER JOIN TblSite B ON A.SiteCode = B.SiteCode ");
                            SQLLogo.AppendLine("INNER JOIN TblProfitCenter C ON B.ProfitCenterCode  = C.ProfitCenterCode ");
                            SQLLogo.AppendLine("INNER JOIN TblEntity D ON C.EntCode = D.EntCode ");
                            SQLLogo.AppendLine("WHERE A.DocNo= '" + PODocNo + "' ");

                            string CompanyLogo = Sm.GetValue(SQLLogo.ToString());
                            Sm.CmParam<String>(ref cmMAI, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                        }
                        else
                        {
                            Sm.CmParam<String>(ref cmMAI, "@CompanyLogo", @Sm.CompanyLogo(mPOPrintOutCompanyLogo));
                        }
                        var dr2 = cmMAI.ExecuteReader();
                        var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "CompanyName",
                         //1-5
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo",                        
                         //6-10
                         "DocDt",
                         "VdContactPerson",
                         "ShipTo" ,
                         "BillTo" ,
                         "TaxCode1",
                         //11-15
                         "TaxCode2",
                         "TaxCode3" ,
                         "TaxAmt",
                         "CustomsTaxAmt",
                         "DiscountAmt" ,
                         //16-20
                         "Amt" ,
                         "DownPayment" ,
                         "VdName",
                         "CompanyLogo",
                         "ARemark",
                         //21-25
                         "TaxName1",
                         "TaxName2",
                         "Taxname3" ,
                         "CreateBy",
                         "TaxRate1",
                         //26-30
                         "TaxRate2",
                         "TaxRate3",
                         "Address",
                         "Phone",
                         "Fax",
                         //31-35
                         "Email",
                         "ContactNumber",
                         "SplitPO",
                         "SiteName",
                         "LocalDocNo"
                        });
                        if (dr2.HasRows)
                        {
                            while (dr2.Read())
                            {
                                l2.Add(new MatReqMAI()
                                {
                                    CompanyName = Sm.DrStr(dr2, c2[0]),

                                    CompanyAddress = Sm.DrStr(dr2, c2[1]),
                                    CompanyLongAddress = Sm.DrStr(dr2, c2[2]),
                                    CompanyPhone = Sm.DrStr(dr2, c2[3]),
                                    CompanyFax = Sm.DrStr(dr2, c2[4]),
                                    DocNo = Sm.DrStr(dr2, c2[5]),

                                    DocDt = Sm.DrStr(dr2, c2[6]),
                                    VdContactPerson = Sm.DrStr(dr2, c2[7]),
                                    ShipTo = Sm.DrStr(dr2, c2[8]),
                                    BillTo = Sm.DrStr(dr2, c2[9]),
                                    TaxCode1 = Sm.DrStr(dr2, c2[10]),

                                    TaxCode2 = Sm.DrStr(dr2, c2[11]),
                                    TaxCode3 = Sm.DrStr(dr2, c2[12]),
                                    TaxAmt = Sm.DrDec(dr2, c2[13]),
                                    CustomsTaxAmt = Sm.DrDec(dr2, c2[14]),
                                    DiscountAmt = Sm.DrDec(dr2, c2[15]),

                                    Amt = Sm.DrDec(dr2, c2[16]),
                                    DownPayment = Sm.DrDec(dr2, c2[17]),
                                    VdName = Sm.DrStr(dr2, c2[18]),
                                    CompanyLogo = Sm.DrStr(dr2, c2[19]),
                                    ARemark = Sm.DrStr(dr2, c2[20]),

                                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                    TaxName1 = Sm.DrStr(dr2, c2[21]),
                                    TaxName2 = Sm.DrStr(dr2, c2[22]),
                                    TaxName3 = Sm.DrStr(dr2, c2[23]),
                                    CreateBy = Sm.DrStr(dr2, c2[24]),
                                    TaxRate1 = Sm.DrDec(dr2, c2[25]),

                                    TaxRate2 = Sm.DrDec(dr2, c2[26]),
                                    TaxRate3 = Sm.DrDec(dr2, c2[27]),
                                    Address = Sm.DrStr(dr2, c2[28]),
                                    Phone = Sm.DrStr(dr2, c2[29]),
                                    Fax = Sm.DrStr(dr2, c2[30]),

                                    Email = Sm.DrStr(dr2, c2[31]),
                                    ContactNumber = Sm.DrStr(dr2, c2[32]),
                                    SplitPO = Sm.DrStr(dr2, c2[33]),
                                    SiteName = Sm.DrStr(dr2, c2[34]),
                                    LocalDocNo = Sm.DrStr(dr2, c2[35]),
                                });
                            }
                        }
                        dr2.Close();
                    }
                    myLists2.Add(l2);

                    #endregion

                    #region Detail PO

                    var cmDtl2 = new MySqlCommand();

                    var SQLDtl2 = new StringBuilder();
                    using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl2.Open();
                        cmDtl2.Connection = cnDtl2;
                        SQLDtl2.AppendLine("Select F.ItCode, I.ItName, ");
                        SQLDtl2.AppendLine("(B.Qty-ifnull(M.Qty, 0)) As Qty, I.PurchaseUomCode, K.PtName, G.CurCode, H.UPrice, ");
                        SQLDtl2.AppendLine("B.Discount, B.DiscountAmt, B.Roundingvalue, Concat(RIGHT(B.EstRecvDt, 2), '/', RIGHT(LEFT(B.EstRecvDt, 6), 2), '/', RIGHT(LEFT(B.EstRecvDt, 4), 2)) As EstRecvDt, B.Remark As DRemark, ");
                        SQLDtl2.AppendLine("(H.Uprice * (B.Qty-ifnull(M.Qty, 0))) - (H.Uprice*(B.Qty-ifnull(M.Qty, 0))*B.Discount/100) - B.DiscountAmt + B.Roundingvalue  As Total, L.DtName, I.ForeignName, I.ItCodeInternal ");
                        SQLDtl2.AppendLine("From TblPODtl B ");
                        SQLDtl2.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                        SQLDtl2.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo and D.CancelInd='N' ");
                        SQLDtl2.AppendLine("Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
                        SQLDtl2.AppendLine("Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo And F.CancelInd='N' ");
                        SQLDtl2.AppendLine("Inner Join TblQtHdr G On D.QtDocNo=G.DocNo ");
                        SQLDtl2.AppendLine("Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo ");
                        SQLDtl2.AppendLine("Inner Join TblItem I On F.ItCode=I.ItCode ");
                        SQLDtl2.AppendLine("Inner Join TblPaymentTerm K On G.PtCode=K.PtCode ");
                        SQLDtl2.AppendLine("Left Join TblDeliveryType L On G.DtCode = L.DtCode ");
                        SQLDtl2.AppendLine("Left Join ( ");
                        SQLDtl2.AppendLine("    Select PODocNo, PODNo, Sum(Qty) Qty ");
                        SQLDtl2.AppendLine("    From TblPOQtyCancel ");
                        SQLDtl2.AppendLine("    Where CancelInd='N' ");
                        SQLDtl2.AppendLine("    And PODocNo=@DocNo ");
                        SQLDtl2.AppendLine("    Group By PODocNo, PODNo ");
                        SQLDtl2.AppendLine(") M On B.Docno = M.PODocno And B.DNo = M.PODno ");
                        SQLDtl2.AppendLine("Where B.CancelInd='N' AND B.DocNo=@DocNo;");

                        cmDtl2.CommandText = SQLDtl2.ToString();
                        Sm.CmParam<String>(ref cmDtl2, "@DocNo", PODocNo);
                        var drDtl2 = cmDtl2.ExecuteReader();
                        var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName" ,
                         "Qty",
                         "PurchaseUomCode" ,
                         "PtName",
                         "CurCode" ,

                         //6-10
                         "UPrice" ,
                         "Discount" ,
                         "DiscountAmt",
                         "RoundingValue",
                         "EstRecvDt" ,

                         //11-15
                         "DRemark" ,
                         "Total",
                         "DtName",
                         "ForeignName",
                         "ItCodeInternal"

                        });
                        if (drDtl2.HasRows)
                        {
                            while (drDtl2.Read())
                            {
                                ldtl2.Add(new MatReqDtlMAI()
                                {
                                    ItCode = Sm.DrStr(drDtl2, cDtl2[0]),
                                    ItName = Sm.DrStr(drDtl2, cDtl2[1]),

                                    Qty = Sm.DrDec(drDtl2, cDtl2[2]),
                                    PurchaseUomCode = Sm.DrStr(drDtl2, cDtl2[3]),
                                    PtName = Sm.DrStr(drDtl2, cDtl2[4]),
                                    CurCode = Sm.DrStr(drDtl2, cDtl2[5]),

                                    UPrice = Sm.DrDec(drDtl2, cDtl2[6]),
                                    Discount = Sm.DrDec(drDtl2, cDtl2[7]),
                                    DiscountAmt = Sm.DrDec(drDtl2, cDtl2[8]),
                                    RoundingValue = Sm.DrDec(drDtl2, cDtl2[9]),
                                    EstRecvDt = Sm.DrStr(drDtl2, cDtl2[10]),

                                    DRemark = Sm.DrStr(drDtl2, cDtl2[11]),
                                    DTotal = Sm.DrDec(drDtl2, cDtl2[12]),
                                    DtName = Sm.DrStr(drDtl2, cDtl2[13]),
                                    ForeignName = Sm.DrStr(drDtl2, cDtl2[14]),
                                    ItCodeInternal = Sm.DrStr(drDtl2, cDtl2[15]),
                                });
                            }
                        }
                        drDtl2.Close();
                    }
                    myLists2.Add(ldtl2);

                    #endregion

                    #region Detail PO's Signature

                    var cmDtl3 = new MySqlCommand();

                    var SQLDtl3 = new StringBuilder();
                    using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl3.Open();
                        cmDtl3.Connection = cnDtl3;

                        SQLDtl3.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                        SQLDtl3.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                        SQLDtl3.AppendLine("From ( ");
                        SQLDtl3.AppendLine("    Select Distinct ");
                        SQLDtl3.AppendLine("    '1' AS Indicator, B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                        SQLDtl3.AppendLine("    B.ApprovalDNo As DNo, E.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                        SQLDtl3.AppendLine("    From TblPODtl A ");
                        SQLDtl3.AppendLine("    Inner Join TblPORequestDtl D On A.PORequestDocNo=D.DocNo And A.PORequestDNo=D.DNo ");
                        SQLDtl3.AppendLine("    Inner Join TblDocApproval B On B.DocType='MaterialRequest2' And D.MaterialRequestDocNo=B.DocNo And D.MaterialRequestDNo=B.DNo ");
                        SQLDtl3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                        SQLDtl3.AppendLine("    Inner Join TblDocApprovalSetting E On B.ApprovalDNo=E.DNo And E.DocType = 'MaterialRequest2' ");
                        SQLDtl3.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                        SQLDtl3.AppendLine("    Union All ");
                        SQLDtl3.AppendLine("    Select Distinct ");
                        SQLDtl3.AppendLine("    '2' AS Indicator, C.CreateBy As UserCode, Concat(Upper(left(D.UserName,1)),Substring(Lower(D.UserName), 2, Length(D.UserName))) As UserName, ");
                        SQLDtl3.AppendLine("    '1' As DNo, 0 As Level, 'Created By' As Title, Left(C.CreateDt, 8) As LastUpDt ");
                        SQLDtl3.AppendLine("    From TblPODtl A ");
                        SQLDtl3.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo = B.DocNo And A.PORequestDNo = B.DNo ");
                        SQLDtl3.AppendLine("    Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo = C.DocNo And B.MaterialRequestDNo = C.DNo ");
                        SQLDtl3.AppendLine("    Inner Join TblUser D On C.CreateBy=D.UserCode ");
                        SQLDtl3.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                        SQLDtl3.AppendLine(") T1 ");
                        SQLDtl3.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                        SQLDtl3.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                        SQLDtl3.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                        SQLDtl3.AppendLine("Group By T1.Indicator, T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                        SQLDtl3.AppendLine("Order By T1.Level; ");

                        cmDtl3.CommandText = SQLDtl3.ToString();
                        Sm.CmParam<String>(ref cmDtl3, "@Space", "-------------------------");
                        Sm.CmParam<String>(ref cmDtl3, "@DocNo", PODocNo);
                        var drDtl3 = cmDtl3.ExecuteReader();
                        var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                        if (drDtl3.HasRows)
                        {
                            while (drDtl3.Read())
                            {
                                ldtl3.Add(new MatReqDtlMAI2()
                                {
                                    Signature = Sm.DrStr(drDtl3, cDtl3[0]),
                                    UserName = Sm.DrStr(drDtl3, cDtl3[1]),
                                    PosName = Sm.DrStr(drDtl3, cDtl3[2]),
                                    Space = Sm.DrStr(drDtl3, cDtl3[3]),
                                    DNo = Sm.DrStr(drDtl3, cDtl3[4]),
                                    Title = Sm.DrStr(drDtl3, cDtl3[5]),
                                    LastUpDt = Sm.DrStr(drDtl3, cDtl3[6])
                                });
                            }
                        }
                        drDtl3.Close();
                    }
                    myLists2.Add(ldtl3);

                    #endregion

                    #region Detail date

                    var cmDtl4 = new MySqlCommand();

                    var SQLDtl4 = new StringBuilder();
                    using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl4.Open();
                        cmDtl4.Connection = cnDtl4;

                        SQLDtl4.AppendLine("Select * From ( ");
                        SQLDtl4.AppendLine("Select DATE_FORMAT(SUBSTRING(LastUpDt, 1, 8),'%d %M %Y') As LastUpDt, ApprovalDNo As DNo ");
                        SQLDtl4.AppendLine("From TblDocApproval ");
                        SQLDtl4.AppendLine("Where DocType = 'PORequest' ");
                        SQLDtl4.AppendLine("And DocNo In (Select PORequestDocNo From TblPODTl Where DocNo=@DocNo) ");
                        SQLDtl4.AppendLine("Group By DocNo, ApprovalDNo ");
                        SQLDtl4.AppendLine("UNION ");
                        SQLDtl4.AppendLine("Select DATE_FORMAT(SUBSTRING(CreateDt, 1, 8),'%d %M %Y') As LastUpDt, '1' As DNo ");
                        SQLDtl4.AppendLine("From TblPOHdr ");
                        SQLDtl4.AppendLine("Where DocNo = @DocNo ");
                        SQLDtl4.AppendLine(") G1 ");
                        SQLDtl4.AppendLine("Order By G1.DNo ");

                        cmDtl4.CommandText = SQLDtl4.ToString();
                        Sm.CmParam<String>(ref cmDtl4, "@DocNo", PODocNo);
                        var drDtl4 = cmDtl4.ExecuteReader();
                        var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "LastUpdt", 
                         "DNo"
                        });
                        if (drDtl4.HasRows)
                        {
                            while (drDtl4.Read())
                            {
                                ldtl4.Add(new MatReqDtlMAI3()
                                {
                                    LastUpDt = Sm.DrStr(drDtl4, cDtl4[0]),
                                    DNo = Sm.DrStr(drDtl4, cDtl4[1])
                                });
                            }
                        }
                        drDtl4.Close();
                    }
                    myLists2.Add(ldtl4);

                    #endregion

                    #region Detail note

                    var cmDtl5 = new MySqlCommand();

                    var SQLDtl5 = new StringBuilder();
                    using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl5.Open();
                        cmDtl5.Connection = cnDtl5;

                        SQLDtl5.AppendLine("Select ParValue From TblParameter Where ParCode = 'PONotes' ");

                        cmDtl5.CommandText = SQLDtl5.ToString();
                        var drDtl5 = cmDtl5.ExecuteReader();
                        var cDtl5 = Sm.GetOrdinal(drDtl5, new string[] 
                        {
                         //0
                         "ParValue"
                        });
                        if (drDtl5.HasRows)
                        {
                            while (drDtl5.Read())
                            {
                                ldtl5.Add(new MatReqDtlMAI4()
                                {
                                    Note = Sm.DrStr(drDtl5, cDtl5[0])
                                });
                            }
                        }
                        drDtl5.Close();
                    }
                    myLists2.Add(ldtl5);

                    #endregion

                    Sm.PrintReport("MaterialRequest2" + RptName, myLists2, TableName2, false);

                    break;

                case 2:

                    var lt3 = new List<MatReqMAI>();
                    var ldtlt2 = new List<MatReqDtlMAI>();
                    var ldtlt3 = new List<MatReqDtlMAI2>();
                    var ldtlt4 = new List<MatReqDtlMAI3>();
                    var ldtlt5 = new List<MatReqDtlMAI4>();
                    string[] TableName3 = { "MatReqMAI", "MatReqDtlMAI", "MatReqDtlMAI2", "MatReqDtlMAI3", "MatReqDtlMAI4" };
                    List<IList> myLists3 = new List<IList>();

                    #region Header PO Full

                    var cmMAI2 = new MySqlCommand();
                    var SQLMAI2 = new StringBuilder();

                    if (mIsFilterBySite)
                    {
                        SQLMAI2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, I.CompanyName, I.CompanyPhone, I.CompanyFax, I.CompanyAddress, '' As CompanyAddressCity, ");
                    }
                    else
                    {
                        SQLMAI2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
                        SQLMAI2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                        SQLMAI2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                        SQLMAI2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                        SQLMAI2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                        SQLMAI2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                    }
                    SQLMAI2.AppendLine("If((Select parvalue From TblParameter Where ParCode = 'IsFilterLocalDocNo')='Y',A.LocalDocNo,A.Docno) As DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.VdContactPerson, A.ShipTo, A.BillTo, ");
                    SQLMAI2.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1,IfNull(A.TaxCode2, null) As TaxCode2, IfNull(A.TaxCode3, null) As TaxCode3, A.TaxAmt, A.CustomsTaxAmt, ");
                    SQLMAI2.AppendLine("A.DiscountAmt, A.Amt, A.DownPayment, A.Remark As ARemark, F.VdName, ");
                    SQLMAI2.AppendLine("(Select Z.TaxName From TblTax Z ");
                    SQLMAI2.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
                    SQLMAI2.AppendLine("(Select Z.TaxName From TblTax Z ");
                    SQLMAI2.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
                    SQLMAI2.AppendLine("(Select  Z.TaxName From TblTax Z ");
                    SQLMAI2.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxName3, ");
                    SQLMAI2.AppendLine("G.UserName As CreateBy, ");
                    SQLMAI2.AppendLine("(Select Z.TaxRate From TblTax Z ");
                    SQLMAI2.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxRate1, ");
                    SQLMAI2.AppendLine("(Select Z.TaxRate From TblTax Z ");
                    SQLMAI2.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxRate2, ");
                    SQLMAI2.AppendLine("(Select  Z.TaxRate From TblTax Z ");
                    SQLMAI2.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxRate3, ");
                    SQLMAI2.AppendLine("F.Address, F.Phone, F.Fax, F.Email, ");
                    SQLMAI2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', H.ContactNumber, ");
                    SQLMAI2.AppendLine("(Select ParValue From TblParameter Where Parcode='IsPOSplitBasedOnTax') As SplitPO, J.SiteName, A.LocalDocNo, E.CurCode  ");
                    SQLMAI2.AppendLine("From TblPOHdr A ");
                    SQLMAI2.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                    SQLMAI2.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                    SQLMAI2.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo And D.CancelInd='N' ");
                    SQLMAI2.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
                    SQLMAI2.AppendLine("Inner Join TblVendor F On E.VdCode=F.VdCode ");
                    SQLMAI2.AppendLine("Inner Join TblUser G On A.CreateBy=G.UserCode ");
                    SQLMAI2.AppendLine("Left Join TblVendorContactPerson H On A.VdContactPerson=H.ContactPersonName And A.VdCode = H.VdCode ");
                    SQLMAI2.AppendLine("Left Join TblSite J On A.SiteCode = J.SiteCode ");
                    if (mIsFilterBySite)
                    {
                        SQLMAI2.AppendLine("Left Join (");
                        SQLMAI2.AppendLine("    Select distinct A.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                        SQLMAI2.AppendLine("    From TblPOhdr A  ");
                        SQLMAI2.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                        SQLMAI2.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                        SQLMAI2.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                        SQLMAI2.AppendLine("    Where A.DocNo=@DocNo And A.SiteCode is not null ");
                        SQLMAI2.AppendLine(") I On A.DocNo = I.DocNo ");
                    }
                    SQLMAI2.AppendLine("Where A.DocNo=@DocNo ");

                    using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn3.Open();
                        cmMAI2.Connection = cn3;
                        cmMAI2.CommandText = SQLMAI2.ToString();
                        Sm.CmParam<String>(ref cmMAI2, "@DocNo", PODocNo);
                        if (mIsFilterBySite)
                        {
                            var SQLLogo2 = new StringBuilder();

                            SQLLogo2.AppendLine("SELECT D.EntLogoName ");
                            SQLLogo2.AppendLine("FROM TblPOHdr A ");
                            SQLLogo2.AppendLine("INNER JOIN TblSite B ON A.SiteCode = B.SiteCode ");
                            SQLLogo2.AppendLine("INNER JOIN TblProfitCenter C ON B.ProfitCenterCode  = C.ProfitCenterCode ");
                            SQLLogo2.AppendLine("INNER JOIN TblEntity D ON C.EntCode = D.EntCode ");
                            SQLLogo2.AppendLine("WHERE A.DocNo= '" + PODocNo + "' ");

                            string CompanyLogo = Sm.GetValue(SQLLogo2.ToString());
                            Sm.CmParam<String>(ref cmMAI2, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                        }
                        else
                        {
                            Sm.CmParam<String>(ref cmMAI2, "@CompanyLogo", @Sm.CompanyLogo(mPOPrintOutCompanyLogo));
                        }
                        var dr3 = cmMAI2.ExecuteReader();
                        var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                         //0
                         "CompanyName",
                         //1-5
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo",                        
                         //6-10
                         "DocDt",
                         "VdContactPerson",
                         "ShipTo" ,
                         "BillTo" ,
                         "TaxCode1",
                         //11-15
                         "TaxCode2",
                         "TaxCode3" ,
                         "TaxAmt",
                         "CustomsTaxAmt",
                         "DiscountAmt" ,
                         //16-20
                         "Amt" ,
                         "DownPayment" ,
                         "VdName",
                         "CompanyLogo",
                         "ARemark",
                         //21-25
                         "TaxName1",
                         "TaxName2",
                         "Taxname3" ,
                         "CreateBy",
                         "TaxRate1",
                         //26-30
                         "TaxRate2",
                         "TaxRate3",
                         "Address",
                         "Phone",
                         "Fax",
                         //31-35
                         "Email",
                         "ContactNumber",
                         "SplitPO",
                         "SiteName",
                         "LocalDocNo"
                        });
                        if (dr3.HasRows)
                        {
                            while (dr3.Read())
                            {
                                lt3.Add(new MatReqMAI()
                                {
                                    CompanyName = Sm.DrStr(dr3, c3[0]),

                                    CompanyAddress = Sm.DrStr(dr3, c3[1]),
                                    CompanyLongAddress = Sm.DrStr(dr3, c3[2]),
                                    CompanyPhone = Sm.DrStr(dr3, c3[3]),
                                    CompanyFax = Sm.DrStr(dr3, c3[4]),
                                    DocNo = Sm.DrStr(dr3, c3[5]),

                                    DocDt = Sm.DrStr(dr3, c3[6]),
                                    VdContactPerson = Sm.DrStr(dr3, c3[7]),
                                    ShipTo = Sm.DrStr(dr3, c3[8]),
                                    BillTo = Sm.DrStr(dr3, c3[9]),
                                    TaxCode1 = Sm.DrStr(dr3, c3[10]),

                                    TaxCode2 = Sm.DrStr(dr3, c3[11]),
                                    TaxCode3 = Sm.DrStr(dr3, c3[12]),
                                    TaxAmt = Sm.DrDec(dr3, c3[13]),
                                    CustomsTaxAmt = Sm.DrDec(dr3, c3[14]),
                                    DiscountAmt = Sm.DrDec(dr3, c3[15]),

                                    Amt = Sm.DrDec(dr3, c3[16]),
                                    DownPayment = Sm.DrDec(dr3, c3[17]),
                                    VdName = Sm.DrStr(dr3, c3[18]),
                                    CompanyLogo = Sm.DrStr(dr3, c3[19]),
                                    ARemark = Sm.DrStr(dr3, c3[20]),

                                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                    TaxName1 = Sm.DrStr(dr3, c3[21]),
                                    TaxName2 = Sm.DrStr(dr3, c3[22]),
                                    TaxName3 = Sm.DrStr(dr3, c3[23]),
                                    CreateBy = Sm.DrStr(dr3, c3[24]),
                                    TaxRate1 = Sm.DrDec(dr3, c3[25]),

                                    TaxRate2 = Sm.DrDec(dr3, c3[26]),
                                    TaxRate3 = Sm.DrDec(dr3, c3[27]),
                                    Address = Sm.DrStr(dr3, c3[28]),
                                    Phone = Sm.DrStr(dr3, c3[29]),
                                    Fax = Sm.DrStr(dr3, c3[30]),

                                    Email = Sm.DrStr(dr3, c3[31]),
                                    ContactNumber = Sm.DrStr(dr3, c3[32]),
                                    SplitPO = Sm.DrStr(dr3, c3[33]),
                                    SiteName = Sm.DrStr(dr3, c3[34]),
                                    LocalDocNo = Sm.DrStr(dr3, c3[35]),
                                });
                            }
                        }
                        dr3.Close();
                    }
                    myLists3.Add(lt3);

                    #endregion

                    #region Detail PO Full

                    var cmDtlt2 = new MySqlCommand();

                    var SQLDtlt2 = new StringBuilder();
                    using (var cnDtlt2 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtlt2.Open();
                        cmDtlt2.Connection = cnDtlt2;
                        SQLDtlt2.AppendLine("Select F.ItCode, I.ItName, ");
                        SQLDtlt2.AppendLine("(B.Qty-ifnull(M.Qty, 0)) As Qty, I.PurchaseUomCode, K.PtName, G.CurCode, H.UPrice, ");
                        SQLDtlt2.AppendLine("B.Discount, B.DiscountAmt, B.Roundingvalue, Concat(RIGHT(B.EstRecvDt, 2), '/', RIGHT(LEFT(B.EstRecvDt, 6), 2), '/', RIGHT(LEFT(B.EstRecvDt, 4), 2)) As EstRecvDt, B.Remark As DRemark, ");
                        SQLDtlt2.AppendLine("(H.Uprice * (B.Qty-ifnull(M.Qty, 0))) - (H.Uprice*(B.Qty-ifnull(M.Qty, 0))*B.Discount/100) - B.DiscountAmt + B.Roundingvalue  As Total, L.DtName, I.ForeignName, I.ItCodeInternal ");
                        SQLDtlt2.AppendLine("From TblPODtl B ");
                        SQLDtlt2.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                        SQLDtlt2.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo And D.CancelInd='N' And D.Status In ('A') ");
                        SQLDtlt2.AppendLine("Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo And E.CancelInd='N' And E.Status In ('A') ");
                        SQLDtlt2.AppendLine("Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo And F.CancelInd='N' And F.Status In ('A') ");
                        SQLDtlt2.AppendLine("Inner Join TblQtHdr G On D.QtDocNo=G.DocNo ");
                        SQLDtlt2.AppendLine("Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo ");
                        SQLDtlt2.AppendLine("Inner Join TblItem I On F.ItCode=I.ItCode ");
                        SQLDtlt2.AppendLine("Inner Join TblPaymentTerm K On G.PtCode=K.PtCode ");
                        SQLDtlt2.AppendLine("Left Join TblDeliveryType L On G.DtCode = L.DtCode ");
                        SQLDtlt2.AppendLine("Left Join ( ");
                        SQLDtlt2.AppendLine("    Select PODocNo, PODNo, Sum(Qty) Qty ");
                        SQLDtlt2.AppendLine("    From TblPOQtyCancel ");
                        SQLDtlt2.AppendLine("    Where CancelInd='N' ");
                        SQLDtlt2.AppendLine("    And PODocNo=@DocNo ");
                        SQLDtlt2.AppendLine("    Group By PODocNo, PODNo ");
                        SQLDtlt2.AppendLine(") M On B.Docno = M.PODocno And B.DNo = M.PODno ");
                        SQLDtlt2.AppendLine("Where B.CancelInd='N' AND E.DocNo=@DocNo ");

                        cmDtlt2.CommandText = SQLDtlt2.ToString();
                        Sm.CmParam<String>(ref cmDtlt2, "@DocNo", TxtDocNo.Text);
                        var drDtlt2 = cmDtlt2.ExecuteReader();
                        var cDtlt2 = Sm.GetOrdinal(drDtlt2, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName" ,
                         "Qty",
                         "PurchaseUomCode" ,
                         "PtName",
                         "CurCode" ,

                         //6-10
                         "UPrice" ,
                         "Discount" ,
                         "DiscountAmt",
                         "RoundingValue",
                         "EstRecvDt" ,

                         //11-15
                         "DRemark" ,
                         "Total",
                         "DtName",
                         "ForeignName",
                         "ItCodeInternal"

                        });
                        if (drDtlt2.HasRows)
                        {
                            while (drDtlt2.Read())
                            {
                                ldtlt2.Add(new MatReqDtlMAI()
                                {
                                    ItCode = Sm.DrStr(drDtlt2, cDtlt2[0]),
                                    ItName = Sm.DrStr(drDtlt2, cDtlt2[1]),

                                    Qty = Sm.DrDec(drDtlt2, cDtlt2[2]),
                                    PurchaseUomCode = Sm.DrStr(drDtlt2, cDtlt2[3]),
                                    PtName = Sm.DrStr(drDtlt2, cDtlt2[4]),
                                    CurCode = Sm.DrStr(drDtlt2, cDtlt2[5]),

                                    UPrice = Sm.DrDec(drDtlt2, cDtlt2[6]),
                                    Discount = Sm.DrDec(drDtlt2, cDtlt2[7]),
                                    DiscountAmt = Sm.DrDec(drDtlt2, cDtlt2[8]),
                                    RoundingValue = Sm.DrDec(drDtlt2, cDtlt2[9]),
                                    EstRecvDt = Sm.DrStr(drDtlt2, cDtlt2[10]),

                                    DRemark = Sm.DrStr(drDtlt2, cDtlt2[11]),
                                    DTotal = Sm.DrDec(drDtlt2, cDtlt2[12]),
                                    DtName = Sm.DrStr(drDtlt2, cDtlt2[13]),
                                    ForeignName = Sm.DrStr(drDtlt2, cDtlt2[14]),
                                    ItCodeInternal = Sm.DrStr(drDtlt2, cDtlt2[15]),
                                });
                            }
                        }
                        drDtlt2.Close();
                    }
                    myLists3.Add(ldtlt2);

                    #endregion

                    #region Detail PO's Signature

                    var cmDtlt3 = new MySqlCommand();

                    var SQLDtlt3 = new StringBuilder();
                    using (var cnDtlt3 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtlt3.Open();
                        cmDtlt3.Connection = cnDtlt3;

                        SQLDtlt3.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                        SQLDtlt3.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                        SQLDtlt3.AppendLine("From ( ");
                        SQLDtlt3.AppendLine("    Select Distinct ");
                        SQLDtlt3.AppendLine("    '1' AS Indicator, B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                        SQLDtlt3.AppendLine("    B.ApprovalDNo As DNo, E.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                        SQLDtlt3.AppendLine("    From TblPODtl A ");
                        SQLDtlt3.AppendLine("    Inner Join TblPORequestDtl D On A.PORequestDocNo=D.DocNo And A.PORequestDNo=D.DNo ");
                        SQLDtlt3.AppendLine("    Inner Join TblDocApproval B On B.DocType='MaterialRequest2' And D.MaterialRequestDocNo=B.DocNo And D.MaterialRequestDNo=B.DNo ");
                        SQLDtlt3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                        SQLDtlt3.AppendLine("    Inner Join TblDocApprovalSetting E On B.ApprovalDNo=E.DNo And E.DocType = 'MaterialRequest2' ");
                        SQLDtlt3.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                        SQLDtlt3.AppendLine("    Union All ");
                        SQLDtlt3.AppendLine("    Select Distinct ");
                        SQLDtlt3.AppendLine("    '2' AS Indicator, C.CreateBy As UserCode, Concat(Upper(left(D.UserName,1)),Substring(Lower(D.UserName), 2, Length(D.UserName))) As UserName, ");
                        SQLDtlt3.AppendLine("    '1' As DNo, 0 As Level, 'Created By' As Title, Left(C.CreateDt, 8) As LastUpDt ");
                        SQLDtlt3.AppendLine("    From TblPODtl A ");
                        SQLDtlt3.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo = B.DocNo And A.PORequestDNo = B.DNo ");
                        SQLDtlt3.AppendLine("    Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo = C.DocNo And B.MaterialRequestDNo = C.DNo ");
                        SQLDtlt3.AppendLine("    Inner Join TblUser D On C.CreateBy=D.UserCode ");
                        SQLDtlt3.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                        SQLDtlt3.AppendLine(") T1 ");
                        SQLDtlt3.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                        SQLDtlt3.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                        SQLDtlt3.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                        SQLDtlt3.AppendLine("Group By T1.Indicator, T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                        SQLDtlt3.AppendLine("Order By T1.Level; ");

                        cmDtlt3.CommandText = SQLDtlt3.ToString();
                        Sm.CmParam<String>(ref cmDtlt3, "@Space", "-------------------------");
                        Sm.CmParam<String>(ref cmDtlt3, "@DocNo", PODocNo);
                        var drDtlt3 = cmDtlt3.ExecuteReader();
                        var cDtlt3 = Sm.GetOrdinal(drDtlt3, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                        if (drDtlt3.HasRows)
                        {
                            while (drDtlt3.Read())
                            {
                                ldtlt3.Add(new MatReqDtlMAI2()
                                {
                                    Signature = Sm.DrStr(drDtlt3, cDtlt3[0]),
                                    UserName = Sm.DrStr(drDtlt3, cDtlt3[1]),
                                    PosName = Sm.DrStr(drDtlt3, cDtlt3[2]),
                                    Space = Sm.DrStr(drDtlt3, cDtlt3[3]),
                                    DNo = Sm.DrStr(drDtlt3, cDtlt3[4]),
                                    Title = Sm.DrStr(drDtlt3, cDtlt3[5]),
                                    LastUpDt = Sm.DrStr(drDtlt3, cDtlt3[6])
                                });
                            }
                        }
                        drDtlt3.Close();
                    }
                    myLists3.Add(ldtlt3);

                    #endregion

                    #region Detail Date Full

                    var cmDtlt4 = new MySqlCommand();

                    var SQLDtlt4 = new StringBuilder();
                    using (var cnDtlt4 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtlt4.Open();
                        cmDtlt4.Connection = cnDtlt4;

                        SQLDtlt4.AppendLine("Select * From ( ");
                        SQLDtlt4.AppendLine("Select DATE_FORMAT(SUBSTRING(LastUpDt, 1, 8),'%d %M %Y') As LastUpDt, ApprovalDNo As DNo ");
                        SQLDtlt4.AppendLine("From TblDocApproval ");
                        SQLDtlt4.AppendLine("Where DocType = 'PORequest' ");
                        SQLDtlt4.AppendLine("And DocNo In (Select PORequestDocNo From TblPODTl Where DocNo=@DocNo) ");
                        SQLDtlt4.AppendLine("Group By DocNo, ApprovalDNo ");
                        SQLDtlt4.AppendLine("UNION ");
                        SQLDtlt4.AppendLine("Select DATE_FORMAT(SUBSTRING(CreateDt, 1, 8),'%d %M %Y') As LastUpDt, '1' As DNo ");
                        SQLDtlt4.AppendLine("From TblPOHdr ");
                        SQLDtlt4.AppendLine("Where DocNo = @DocNo ");
                        SQLDtlt4.AppendLine(") G1 ");
                        SQLDtlt4.AppendLine("Order By G1.DNo ");

                        cmDtlt4.CommandText = SQLDtlt4.ToString();
                        Sm.CmParam<String>(ref cmDtlt4, "@DocNo", PODocNo);
                        var drDtlt4 = cmDtlt4.ExecuteReader();
                        var cDtlt4 = Sm.GetOrdinal(drDtlt4, new string[] 
                        {
                         //0
                         "LastUpdt", 
                         "DNo"
                        });
                        if (drDtlt4.HasRows)
                        {
                            while (drDtlt4.Read())
                            {
                                ldtlt4.Add(new MatReqDtlMAI3()
                                {
                                    LastUpDt = Sm.DrStr(drDtlt4, cDtlt4[0]),
                                    DNo = Sm.DrStr(drDtlt4, cDtlt4[1])
                                });
                            }
                        }
                        drDtlt4.Close();
                    }
                    myLists3.Add(ldtlt4);

                    #endregion

                    #region Detail Note Full

                    var cmDtlt5 = new MySqlCommand();

                    var SQLDtlt5 = new StringBuilder();
                    using (var cnDtlt5 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtlt5.Open();
                        cmDtlt5.Connection = cnDtlt5;

                        SQLDtlt5.AppendLine("Select ParValue From TblParameter Where ParCode = 'PONotes' ");

                        cmDtlt5.CommandText = SQLDtlt5.ToString();
                        var drDtlt5 = cmDtlt5.ExecuteReader();
                        var cDtlt5 = Sm.GetOrdinal(drDtlt5, new string[] 
                        {
                         //0
                         "ParValue"
                        });
                        if (drDtlt5.HasRows)
                        {
                            while (drDtlt5.Read())
                            {
                                ldtlt5.Add(new MatReqDtlMAI4()
                                {
                                    Note = Sm.DrStr(drDtlt5, cDtlt5[0])
                                });
                            }
                        }
                        drDtlt5.Close();
                    }
                    myLists3.Add(ldtlt5);

                    #endregion

                    Sm.PrintReport("MaterialRequest2Full" , myLists3, TableName3, false);

                    break;
            }
        }
        
        #endregion 

        #region POR

        private bool IsPORequestApprovalForAllDept()
        {
            return Sm.GetParameter("PORequestApprovalForAllDept") == "Y";
        }

        //internal void ComputeTotalPOR(int Row)
        //{
        //    decimal Qty = 0m, UPrice = 0m;

        //    if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 11);
        //    if (Sm.GetGrdStr(Grd1, Row, 23).Length != 0) UPrice = Sm.GetGrdDec(Grd1, Row, 23);

        //    Grd1.Cells[Row, 27].Value = Qty * UPrice;
        //}

         internal string GetSelectedQt()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 15).Length != 0 &&
                        Sm.GetGrdStr(Grd1, Row, 16).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 15) +
                            Sm.GetGrdStr(Grd1, Row, 16) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }
        #endregion

         internal void CountVd(string PORDocNo)
         {
             if (Sm.GetParameter("IsMREximSplitDocument") == "Y")
             {
                 for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                 {
                     if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0 && Sm.GetGrdStr(Grd1, Row, 19).Length != 0)
                     {
                         SavePO(Sm.GetGrdStr(Grd1, Row, 19), PORDocNo, Row);
                     }
                 }
             }
             else
             {
                 SavePO(Sm.GetGrdStr(Grd1, 0, 19), PORDocNo, 0);
             }
         }

         internal void SavePO(string VdCode, string PORDocNo, int RowX)
         {
             string SubCategory = Sm.GetGrdStr(Grd1, 0, 31);
             string DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "PO", "TblPOHdr", SubCategory, 0);

             var cml = new List<MySqlCommand>();

             cml.Add(SavePOHdr(DocNo, VdCode, RowX));

             if (Sm.GetParameter("IsMREximSplitDocument") == "Y")
             {
                 if (Sm.GetGrdStr(Grd1, RowX, 6).Length > 0 && Sm.GetGrdStr(Grd1, RowX, 19) == VdCode) 
                     cml.Add(SavePODtl(DocNo, PORDocNo, RowX));
             }
             else
             {
                 for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                 {
                     if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                     {
                         cml.Add(SavePODtl(DocNo, PORDocNo, Row));
                     }
                 }
             }
             Sm.ExecCommands(cml);
         }

         internal void ComputeTotal(int Row)
         {
             decimal Qty = 0m, UPrice = 0m, Discount = 0m,
                 DiscountAmt = 0m, RoundValue = 0m;

             if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 11);
             if (Sm.GetGrdStr(Grd1, Row, 25).Length != 0) UPrice = Sm.GetGrdDec(Grd1, Row, 25);
             if (Sm.GetGrdStr(Grd1, Row, 26).Length != 0) Discount = Sm.GetGrdDec(Grd1, Row, 26);
             if (Sm.GetGrdStr(Grd1, Row, 27).Length != 0) DiscountAmt = Sm.GetGrdDec(Grd1, Row, 27);
             if (Sm.GetGrdStr(Grd1, Row, 28 ).Length != 0) RoundValue = Sm.GetGrdDec(Grd1, Row, 28);

             Grd1.Cells[Row, 29].Value = (((100 - Discount) / 100) * (Qty * UPrice)) - DiscountAmt + RoundValue;

             ComputeTaxAmt();
         }

         internal void ComputeTaxAmt()
         {
             decimal
                 TaxAmt = 0m;

             string TaxAmt1 = "0", TaxAmt2 = "0", TaxAmt3 = "0";


             if (Sm.GetLue(LueTaxCode1).Length != 0)
             {
                 TaxAmt1 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode1) + "'");
                 if (TaxAmt1.Length != 0)
                 {
                     for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                     {
                         if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                         {
                             Grd1.Cells[Row, 30].Value = Sm.GetGrdDec(Grd1, Row, 29) * Decimal.Parse(TaxAmt1) / 100m;
                             TaxAmt += Sm.GetGrdDec(Grd1, Row, 30);
                         }
                     }
                 }
             }
             else
             {
                 for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                 {
                     Grd1.Cells[Row, 30].Value = 0;
                 }
             }

             if (Sm.GetLue(LueTaxCode2).Length != 0)
             {
                 TaxAmt2 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode2) + "'");
                 if (TaxAmt2.Length != 0)
                 {
                     for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                     {
                         if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                         {
                             Grd1.Cells[Row, 31].Value = Sm.GetGrdDec(Grd1, Row, 29) * Decimal.Parse(TaxAmt2) / 100m;
                             TaxAmt += Sm.GetGrdDec(Grd1, Row, 31);
                         }
                     }
                 }
             }
             else
             {
                 for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                 {
                     Grd1.Cells[Row, 31].Value = 0;
                 }
             }

             if (Sm.GetLue(LueTaxCode3).Length != 0)
             {
                 TaxAmt3 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode3) + "'");
                 if (TaxAmt3.Length != 0)
                 {
                     for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                     {
                         if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                         {
                             Grd1.Cells[Row, 32].Value = Sm.GetGrdDec(Grd1, Row, 29) * Decimal.Parse(TaxAmt3) / 100m;
                             TaxAmt += Sm.GetGrdDec(Grd1, Row, 32);
                         }
                     }
                 }
             }
             else
             {
                 for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                 {
                     Grd1.Cells[Row, 32].Value = 0;
                 }
             }

             TxtTaxAmt.Text = Sm.FormatNum(TaxAmt, 0);

             ComputeAmt();
         }

        internal void ComputeAmt()
        {
            decimal 
                TaxAmt = 0m, CustomsTaxAmt = 0m, DiscountAmt = 0m,
                TotalBeforeTax = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if(Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                {
                    TotalBeforeTax += Sm.GetGrdDec(Grd1, Row, 29);
                }
            }
            if (TxtTaxAmt.Text.Length != 0) TaxAmt = decimal.Parse(TxtTaxAmt.Text);

            TxtAmt.Text = Sm.FormatNum(TotalBeforeTax+TaxAmt+CustomsTaxAmt-DiscountAmt, 0);
        }

         private void SetLueVdPersonCode(ref LookUpEdit Lue, string VdCode)
         {
             Sm.SetLue1(
                 ref Lue,
                 "Select ContactPersonName As Col1 From TblVendorContactPerson Where VdCode= '" + VdCode + "' Order By ContactPersonName",
                 "Contact Person Name");
         }

        internal void SetLuePOSignBy(ref DXE.LookUpEdit Lue, string Code)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.OptCode Col1, B.UserName Col2 ");
                SQL.AppendLine("From TblOption A ");
                SQL.AppendLine("Inner Join TblUser B On A.OptCode=B.UserCode ");
                SQL.AppendLine("Where A.OptCat = 'POSignBy' ");
                if (Code.Length > 0)
                    SQL.AppendLine("AND A.OptCode = @Code; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (Code.Length > 0)
                    Sm.CmParam<String>(ref cm, "@Code", Code);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (Code.Length > 0) Sm.SetLue(Lue, Code);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
         private void SetBudgetCategory()
         {
             LueBCCode.EditValue = null;
             Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, true);

             var ReqType = Sm.GetLue(LueReqType);
             var DeptCode = Sm.GetLue(LueDeptCode);

             if (
                 mReqTypeForNonBudget.Length == 0 ||
                 ReqType.Length == 0 ||
                 Sm.CompareStr(ReqType, mReqTypeForNonBudget) ||
                 DeptCode.Length == 0
                 ) return;

             Sl.SetLueBCCode(ref LueBCCode, string.Empty, DeptCode);
             Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, false);
         }

         private void UploadFile(string DocNo)
         {
             if (IsUploadFileNotValid()) return;

             FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
             FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
             request.Method = WebRequestMethods.Ftp.UploadFile;
             request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

             Stream ftpStream = request.GetRequestStream();

             FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

             int length = 1024;
             byte[] buffer = new byte[length];
             int bytesRead = 0;

             do
             {
                 bytesRead = file.Read(buffer, 0, length);
                 ftpStream.Write(buffer, 0, bytesRead);

                 PbUpload.Invoke(
                     (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                 byte[] buffers = new byte[10240];
                 int read;
                 while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                 {
                     ftpStream.Write(buffers, 0, read);
                     PbUpload.Invoke(
                         (MethodInvoker)delegate
                         {
                             PbUpload.Value = (int)file.Position;
                         });
                 }
             }
             while (bytesRead != 0);

             Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

             file.Close();
             ftpStream.Close();

             var cml = new List<MySqlCommand>();
             cml.Add(UpdateMR2File(DocNo, toUpload.Name));
             Sm.ExecCommands(cml);
         }

         private void UploadFile2(string DocNo)
         {
             if (IsUploadFileNotValid2()) return;

             FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
             FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
             request.Method = WebRequestMethods.Ftp.UploadFile;
             request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

             Stream ftpStream = request.GetRequestStream();

             FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

             int length = 1024;
             byte[] buffer = new byte[length];
             int bytesRead = 0;

             do
             {
                 bytesRead = file.Read(buffer, 0, length);
                 ftpStream.Write(buffer, 0, bytesRead);

                 PbUpload2.Invoke(
                     (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                 byte[] buffers = new byte[10240];
                 int read;
                 while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                 {
                     ftpStream.Write(buffers, 0, read);
                     PbUpload2.Invoke(
                         (MethodInvoker)delegate
                         {
                             PbUpload2.Value = (int)file.Position;
                         });
                 }
             }
             while (bytesRead != 0);

             Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

             file.Close();
             ftpStream.Close();

             var cml = new List<MySqlCommand>();
             cml.Add(UpdateMR2File2(DocNo, toUpload.Name));
             Sm.ExecCommands(cml);
         }

         private void UploadFile3(string DocNo)
         {
             if (IsUploadFileNotValid3()) return;

             FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
             FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
             request.Method = WebRequestMethods.Ftp.UploadFile;
             request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

             Stream ftpStream = request.GetRequestStream();

             FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

             int length = 1024;
             byte[] buffer = new byte[length];
             int bytesRead = 0;

             do
             {
                 bytesRead = file.Read(buffer, 0, length);
                 ftpStream.Write(buffer, 0, bytesRead);

                 PbUpload3.Invoke(
                     (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                 byte[] buffers = new byte[10240];
                 int read;
                 while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                 {
                     ftpStream.Write(buffers, 0, read);
                     PbUpload3.Invoke(
                         (MethodInvoker)delegate
                         {
                             PbUpload3.Value = (int)file.Position;
                         });
                 }
             }
             while (bytesRead != 0);

             Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

             file.Close();
             ftpStream.Close();

             var cml = new List<MySqlCommand>();
             cml.Add(UpdateMR2File3(DocNo, toUpload.Name));
             Sm.ExecCommands(cml);
         }

         private bool IsUploadFileNotValid()
         {
             return
                 Sm.IsDteEmpty(DteDocDt, "Date") ||
                 IsFTPClientDataNotValid() ||
                 IsFileSizeNotvalid() //||
                 //IsFileNameAlreadyExisted()
                ;
         }

         private bool IsUploadFileNotValid2()
         {
             return
                 Sm.IsDteEmpty(DteDocDt, "Date") ||
                 IsFTPClientDataNotValid2() ||
                 IsFileSizeNotvalid2() //||
                 //IsFileNameAlreadyExisted2()
                ;
         }

         private bool IsUploadFileNotValid3()
         {
             return
                 Sm.IsDteEmpty(DteDocDt, "Date") ||
                 IsFTPClientDataNotValid3() ||
                 IsFileSizeNotvalid3() //||
                 //IsFileNameAlreadyExisted3()
                ;
         }

         private bool IsFTPClientDataNotValid()
         {

             if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                 return true;
             }


             if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                 return true;
             }

             if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                 return true;
             }


             if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                 return true;
             }

             return false;
         }

         private bool IsFTPClientDataNotValid2()
         {

             if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                 return true;
             }

             if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                 return true;
             }

             if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                 return true;
             }

             if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                 return true;
             }
             return false;
         }

         private bool IsFTPClientDataNotValid3()
         {

             if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                 return true;
             }


             if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                 return true;
             }


             if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                 return true;
             }


             if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
             {
                 Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                 return true;
             }
             return false;
         }

         private bool IsFileSizeNotvalid()
         {
             if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0)
             {
                 FileInfo f = new FileInfo(TxtFile.Text);

                 long bytes = 0;
                 double kilobytes = 0;
                 double megabytes = 0;
                 double gibabytes = 0;

                 if (f.Exists)
                 {
                     bytes = f.Length;
                     kilobytes = (double)bytes / 1024;
                     megabytes = kilobytes / 1024;
                     gibabytes = megabytes / 1024;
                 }


                 if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                 {
                     Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                     return true;
                 }
             }
             return false;
         }

         private bool IsFileSizeNotvalid2()
         {
             if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0)
             {
                 FileInfo f = new FileInfo(TxtFile2.Text);
                 long bytes = 0;
                 double kilobytes = 0;
                 double megabytes = 0;
                 double gibabytes = 0;

                 if (f.Exists)
                 {
                     bytes = f.Length;
                     kilobytes = (double)bytes / 1024;
                     megabytes = kilobytes / 1024;
                     gibabytes = megabytes / 1024;
                 }

                 if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                 {
                     Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                     return true;
                 }
             }
             return false;
         }

         private bool IsFileSizeNotvalid3()
         {
             if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0)
             {
                 FileInfo f = new FileInfo(TxtFile3.Text);
                 long bytes = 0;
                 double kilobytes = 0;
                 double megabytes = 0;
                 double gibabytes = 0;

                 if (f.Exists)
                 {
                     bytes = f.Length;
                     kilobytes = (double)bytes / 1024;
                     megabytes = kilobytes / 1024;
                     gibabytes = megabytes / 1024;
                 }

                 if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                 {
                     Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                     return true;
                 }
             }
             return false;
         }

         private bool IsFileNameAlreadyExisted()
         {
             if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0)
             {
                 FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                 var SQL = new StringBuilder();

                 SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                 SQL.AppendLine("Where FileName=@FileName ");
                 SQL.AppendLine("And CancelInd = 'N' ");
                 SQL.AppendLine("Limit 1; ");

                 var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                 Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                 if (Sm.IsDataExist(cm))
                 {
                     Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                     return true;
                 }
             }
             return false;
         }

         private bool IsFileNameAlreadyExisted2()
         {
             if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0)
             {
                 FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                 var SQL = new StringBuilder();

                 SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                 SQL.AppendLine("Where FileName2=@FileName ");
                 SQL.AppendLine("And CancelInd = 'N' ");
                 SQL.AppendLine("Limit 1; ");

                 var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                 Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                 if (Sm.IsDataExist(cm))
                 {
                     Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                     return true;
                 }
             }
             return false;
         }

         private bool IsFileNameAlreadyExisted3()
         {
             if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0)
             {
                 FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                 var SQL = new StringBuilder();

                 SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                 SQL.AppendLine("Where FileName3=@FileName ");
                 SQL.AppendLine("And CancelInd = 'N' ");
                 SQL.AppendLine("Limit 1; ");

                 var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                 Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                 if (Sm.IsDataExist(cm))
                 {
                     Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                     return true;
                 }
             }
             return false;
         }

         private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
         {
             downloadedData = new byte[0];

             try
             {
                 this.Text = "Connecting...";
                 Application.DoEvents();

                 FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                 this.Text = "Retrieving Information...";
                 Application.DoEvents();

                 //Get the file size first (for progress bar)
                 request.Method = WebRequestMethods.Ftp.GetFileSize;
                 request.Credentials = new NetworkCredential(username, password);
                 request.UsePassive = true;
                 request.UseBinary = true;
                 request.KeepAlive = true;

                 int dataLength = (int)request.GetResponse().ContentLength;

                 this.Text = "Downloading File...";
                 Application.DoEvents();

                 //Now get the actual data
                 request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                 request.Method = WebRequestMethods.Ftp.DownloadFile;
                 request.Credentials = new NetworkCredential(username, password);
                 request.UsePassive = true;
                 request.UseBinary = true;
                 request.KeepAlive = false;

                 //Set up progress bar
                 PbUpload.Value = 0;
                 PbUpload.Maximum = dataLength;
                 //Streams
                 FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                 Stream reader = response.GetResponseStream();

                 //Download to memory
                 MemoryStream memStream = new MemoryStream();
                 byte[] buffer = new byte[1024];

                 while (true)
                 {
                     Application.DoEvents();

                     int bytesRead = reader.Read(buffer, 0, buffer.Length);

                     if (bytesRead == 0)
                     {
                         PbUpload.Value = PbUpload.Maximum;

                         Application.DoEvents();
                         break;
                     }
                     else
                     {
                         memStream.Write(buffer, 0, bytesRead);

                         if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                         {
                             PbUpload.Value += bytesRead;

                             PbUpload.Refresh();
                             Application.DoEvents();
                         }
                     }
                 }

                 downloadedData = memStream.ToArray();

                 reader.Close();
                 memStream.Close();
                 response.Close();

                 MessageBox.Show("Downloaded Successfully");
             }
             catch (Exception)
             {
                 MessageBox.Show("There was an error connecting to the FTP Server.");
             }
         }

         private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
         {
             downloadedData = new byte[0];

             try
             {
                 this.Text = "Connecting...";
                 Application.DoEvents();

                 FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                 this.Text = "Retrieving Information...";
                 Application.DoEvents();

                 //Get the file size first (for progress bar)
                 request.Method = WebRequestMethods.Ftp.GetFileSize;
                 request.Credentials = new NetworkCredential(username, password);
                 request.UsePassive = true;
                 request.UseBinary = true;
                 request.KeepAlive = true;

                 int dataLength = (int)request.GetResponse().ContentLength;

                 this.Text = "Downloading File...";
                 Application.DoEvents();

                 //Now get the actual data
                 request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                 request.Method = WebRequestMethods.Ftp.DownloadFile;
                 request.Credentials = new NetworkCredential(username, password);
                 request.UsePassive = true;
                 request.UseBinary = true;
                 request.KeepAlive = false;

                 //Set up progress bar
                 PbUpload2.Value = 0;
                 PbUpload2.Maximum = dataLength;
                 //Streams
                 FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                 Stream reader = response.GetResponseStream();

                 //Download to memory
                 MemoryStream memStream = new MemoryStream();
                 byte[] buffer = new byte[1024];

                 while (true)
                 {
                     Application.DoEvents();

                     int bytesRead = reader.Read(buffer, 0, buffer.Length);

                     if (bytesRead == 0)
                     {
                         PbUpload2.Value = PbUpload2.Maximum;

                         Application.DoEvents();
                         break;
                     }
                     else
                     {
                         memStream.Write(buffer, 0, bytesRead);

                         if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                         {
                             PbUpload2.Value += bytesRead;

                             PbUpload2.Refresh();
                             Application.DoEvents();
                         }

                     }
                 }

                 downloadedData = memStream.ToArray();

                 reader.Close();
                 memStream.Close();
                 response.Close();

                 MessageBox.Show("Downloaded Successfully");
             }
             catch (Exception)
             {
                 MessageBox.Show("There was an error connecting to the FTP Server.");
             }
         }

         private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
         {
             downloadedData = new byte[0];

             try
             {
                 this.Text = "Connecting...";
                 Application.DoEvents();

                 FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                 this.Text = "Retrieving Information...";
                 Application.DoEvents();

                 //Get the file size first (for progress bar)
                 request.Method = WebRequestMethods.Ftp.GetFileSize;
                 request.Credentials = new NetworkCredential(username, password);
                 request.UsePassive = true;
                 request.UseBinary = true;
                 request.KeepAlive = true;

                 int dataLength = (int)request.GetResponse().ContentLength;

                 this.Text = "Downloading File...";
                 Application.DoEvents();

                 //Now get the actual data
                 request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                 request.Method = WebRequestMethods.Ftp.DownloadFile;
                 request.Credentials = new NetworkCredential(username, password);
                 request.UsePassive = true;
                 request.UseBinary = true;
                 request.KeepAlive = false;

                 //Set up progress bar
                 PbUpload3.Value = 0;
                 PbUpload3.Maximum = dataLength;
                 //Streams
                 FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                 Stream reader = response.GetResponseStream();

                 //Download to memory
                 MemoryStream memStream = new MemoryStream();
                 byte[] buffer = new byte[1024];

                 while (true)
                 {
                     Application.DoEvents();

                     int bytesRead = reader.Read(buffer, 0, buffer.Length);

                     if (bytesRead == 0)
                     {
                         PbUpload3.Value = PbUpload3.Maximum;

                         Application.DoEvents();
                         break;
                     }
                     else
                     {
                         memStream.Write(buffer, 0, bytesRead);

                         if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                         {
                             PbUpload3.Value += bytesRead;

                             PbUpload3.Refresh();
                             Application.DoEvents();
                         }

                     }
                 }

                 downloadedData = memStream.ToArray();

                 reader.Close();
                 memStream.Close();
                 response.Close();

                 MessageBox.Show("Downloaded Successfully");
             }
             catch (Exception)
             {
                 MessageBox.Show("There was an error connecting to the FTP Server.");
             }
         }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                SetBudgetCategory();
                ClearGrd();
                ComputeRemainingBudget();
                if (mIsMRUseBudgetForMR) ComputeBudgetForMR();
            }
        }

        private void LuePOSignBy_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePOSignBy, new Sm.RefreshLue2(SetLuePOSignBy), string.Empty);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(Sl.SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode));
                ComputeRemainingBudget();
                if (mIsMRUseBudgetForMR) ComputeBudgetForMR();
            }
        }

        private void LueReqType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueReqType, new Sm.RefreshLue1(Sl.SetLueReqType));
                SetBudgetCategory();
                ClearGrd();
                ComputeRemainingBudget();
                if (mIsMRUseBudgetForMR) ComputeBudgetForMR();
            }
        }

        private void DteUsageDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteUsageDt, ref fCell, ref fAccept);
        }

        private void DteUsageDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ComputeRemainingBudget();
                if (mIsMRUseBudgetForMR) ComputeBudgetForMR();
            }
        }

        private void DteEstRecvDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }


        private void DteEstRecvDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEstRecvDt, ref fCell, ref fAccept);
        }

        private void LueContact_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdContactPersonName, new Sm.RefreshLue2(SetLueVdPersonCode), Sm.GetLue(LueVdContactPersonName));
        }

        private void LueVdContactPersonName_Leave(object sender, EventArgs e)
        {
            if (LueVdContactPersonName.Visible && fAccept && fCell.ColIndex == 21)
            {
                if (Sm.GetLue(LueVdContactPersonName).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 21].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 21].Value = LueVdContactPersonName.GetColumnValue("Col1");
                }
                LueVdContactPersonName.Visible = false;
            }
        }

        private void LueVdContactPersonName_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            ComputeTaxAmt();
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            ComputeTaxAmt();
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            ComputeTaxAmt();
        }

        private void LueOptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOptCode, new Sm.RefreshLue2(Sl.SetLueOption), "ProcurementType");
        }

        private void LueMRType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMRType, new Sm.RefreshLue2(Sl.SetLueOption), "MRRoutineType");
            if (BtnSave.Enabled)
            {
                var ReqType = Sm.GetValue("Select Property1 From TblOption Where OptCode=@Param And OptCat='MRRoutineType';", Sm.GetLue(LueMRType));
                Sm.SetLue(LueReqType, ReqType);
            }
        }

        private void BtnSI_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMaterialRequest2Dlg3(this));
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }
        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }
        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        #endregion

        #region Button Event

        private void BtnDORequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueReqType, "Request Type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequest2Dlg4(this, Sm.GetLue(LueDeptCode)));
        }

        private void BtnDORequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDORequestDocNo, "DO Request#", false))
            {
                var f = new FrmDORequestDept(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDORequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed files (*.rar;*.zip)|*.rar;*.zip";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed files (*.rar;*.zip)|*.rar;*.zip";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed files (*.rar;*.zip)|*.rar;*.zip";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {

            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {

            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #endregion

        #region Report Class

        private class MatReq
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DeptName { get; set; }
            public string OptDesc { get; set; }
            public string HRemark { get; set; }
            public string CreateBy { get; set; }
            public string CompanyAddressCity { get; set; }
            public string SiteName { get; set; }
            public string PrintBy { get; set; }
        }

        private class MatReqDtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string UsageDt { get; set; }
            public decimal Mth01 { get; set; }
            public decimal Mth03 { get; set; }
            public decimal Mth06 { get; set; }
            public decimal Mth09 { get; set; }
            public decimal Mth12 { get; set; }
            public string DRemark { get; set; }
            public int nomor { get; set; }
            public string ForeignName { get; set; }
        }

        private class MatReqMAI
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyLongAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string VdContactPerson { get; set; }
            public string ShipTo { get; set; }
            public string BillTo { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal CustomsTaxAmt { get; set; }
            public decimal DiscountAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal DownPayment { get; set; }
            public string ARemark { get; set; }
            public string PrintBy { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public string CreateBy { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Email { get; set; }
            public string ContactNumber { get; set; }
            public string SplitPO { get; set; }
            public string SiteName { get; set; }
            public string LocalDocNo { get; set; }
        }

        private class MatReqDtlMAI
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string PtName { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Discount { get; set; }
            public decimal DiscountAmt { get; set; }
            public decimal RoundingValue { get; set; }
            public string EstRecvDt { get; set; }
            public string DRemark { get; set; }
            public decimal DTotal { get; set; }
            public string DtName { get; set; }
            public string ForeignName { get; set; }
            public string ItCodeInternal { get; set; }
        }

        private class MatReqDtlMAI2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MatReqDtlMAI3
        {
            public string LastUpDt { get; set; }
            public string DNo { get; set; }
        }

        private class MatReqDtlMAI4
        {
            public string Note { get; set; }
        }

        #endregion     

        
    }
}
