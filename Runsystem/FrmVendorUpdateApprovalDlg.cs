﻿#region Update
/*
    02/07/2018 [WED] tambah Website, contact person tambah KTP, sector tambah kualifikasi
    03/07/2018 [WED] tambah Head Office
    24/09/2018 [WED] tambah Establishment Year
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVendorUpdateApprovalDlg : RunSystem.FrmBase1
    {
        #region Field, Property

        private FrmVendorUpdateApproval mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty;
        private bool mIsVdCodeUseVdCt = false;

        #endregion

        #region Constructor

        public FrmVendorUpdateApprovalDlg(FrmVendorUpdateApproval FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "Vendor Update";
                base.FrmLoad(sender, e);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnDelete.Visible = BtnSave.Visible = 
                    BtnCancel.Visible = BtnPrint.Visible = false;
                BtnFind.Enabled = BtnInsert.Enabled = BtnEdit.Enabled = BtnDelete.Enabled = BtnSave.Enabled =
                    BtnCancel.Enabled = BtnPrint.Enabled = false;
                GetParameter();
                SetFormControl(mState.View);

                Sl.SetLueVdCtCode(ref LueVdCtCode);
                Sl.SetLueCityCode(ref LueCityCode);
                SetLueParent(ref LueParent);
                if (mIsVdCodeUseVdCt) LblVdCtCode.ForeColor = Color.Red;

                tabControl1.SelectTab("TpgVdSector");
                SetGrd4();

                tabControl1.SelectTab("TpgBank");
                SetGrd2();

                tabControl1.SelectTab("TpgItemCategory");
                SetGrd3();

                tabControl1.SelectTab("TpgContactPerson");
                SetGrd();

                ShowData(mVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Standard Methods

        #region Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        "Contact Person Name",
                        "ID Number",
                        "Position",
                        "Contact Number"
                    },
                     new int[] 
                    {
                        300, 120, 200, 200
                    }
                );

        }

        private void SetGrd2()
        {
            Grd2.Cols.Count = 5;
            Grd2.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] { "Code", "Bank Name", "Branch Name", "Account Name", "Account Number" },
                    new int[] { 0, 200, 150, 200, 180 }
                );
        }

        private void SetGrd3()
        {
            Grd3.Cols.Count = 3;
            Grd3.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[] 
                { 
                    "",
                    "Item's Category Code", 
                    "Item's Category Name" 
                }, new int[] 
                { 
                   20, 0, 300 
                }
             );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2 });
        }

        private void SetGrd4()
        {
            Grd4.Cols.Count = 4;
            Grd4.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd4,
                new string[] 
                { 
                    "Sector Code", 
                    "Sector Name", 
                    "QualificationCode",
                    "Qualification"
                }, new int[] 
                { 
                   0, 300, 0, 200
                }
             );
            Sm.GrdColReadOnly(Grd4, new int[] { 0, 1, 2, 3 });
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtVdCode, TxtVdName, LueVdCtCode, MeeAddress, TxtShortName, 
                        LueVilCode, LueSDCode, LueCityCode, TxtPostalCd, TxtIdentityNo, 
                        TxtTIN, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                        TxtCreditLimit, MeeRemark, ChkTaxInd, TxtWebsite, LueParent, TxtEstablishedYr
                    }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    Grd4.ReadOnly = true;
                    TxtVdCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtVdCode, TxtVdName, LueVdCtCode, MeeAddress, LueVilCode, TxtShortName, 
                LueSDCode, LueCityCode, TxtPostalCd, TxtIdentityNo, TxtTIN,
                TxtPhone, TxtFax, TxtEmail, TxtMobile, MeeRemark, TxtWebsite, LueParent, TxtEstablishedYr
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtCreditLimit }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd4, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.FocusGrd(Grd3, 0, 1);
            Sm.FocusGrd(Grd4, 0, 1);
        }

        #endregion

        #region Show Data

        public void ShowData(string VdCode)
        {
            try
            {
                ClearData();
                ShowVendor(VdCode);
                ShowVendorContactPerson(VdCode);
                ShowVendorBankAccount(VdCode);
                ShowVendorItemCategory(VdCode);
                ShowVendorSector(VdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVendor(string VdCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select * From TblVendorUpdate Where VdCode=@VdCode",
                    new string[] 
                    {
                        //0
                        "VdCode", 

                        //1-5
                        "VdName", "VdCtCode", "Address", "CityCode", "SDCode", 

                        //6-10
                        "VilCode", "PostalCd", "IdentityNo", "TIN", "Phone", 

                        //11-15
                        "Fax", "Email", "Mobile", "CreditLimit", "TaxInd", 

                        //16-20
                        "Remark", "ShortName", "Website", "Parent", "EstablishedYr"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtVdName.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetLue(LueVdCtCode, Sm.DrStr(dr, c[2]));
                        MeeAddress.EditValue = Sm.DrStr(dr, c[3]);
                        Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[4]));
                        Sl.SetLueSDCode(ref LueSDCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueSDCode, Sm.DrStr(dr, c[5]));
                        Sl.SetLueVilCode(ref LueVilCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueVilCode, Sm.DrStr(dr, c[6]));
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[7]);
                        TxtIdentityNo.EditValue = Sm.DrStr(dr, c[8]);
                        TxtTIN.EditValue = Sm.DrStr(dr, c[9]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[10]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[11]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[12]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[13]);
                        TxtCreditLimit.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                        ChkTaxInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[15]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[16]);
                        TxtShortName.EditValue = Sm.DrStr(dr, c[17]);
                        TxtWebsite.EditValue = Sm.DrStr(dr, c[18]);
                        Sm.SetLue(LueParent, Sm.DrStr(dr, c[19]));
                        TxtEstablishedYr.EditValue = Sm.DrStr(dr, c[20]);
                    }, true
                );
        }

        private void ShowVendorContactPerson(string VdCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select ContactPersonName, IDNumber, Position, ContactNumber " +
                    "From TblVendorContactPersonUpdate " +
                    "Where VdCode=@VdCode " +
                    "Order By ContactPersonName",
                    new string[] 
                    { 
                        "ContactPersonName", 
                        "IDNumber", "Position", "ContactNumber"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowVendorBankAccount(string VdCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    "Select A.BankCode, B.BankName, A.BankBranch, A.BankAcName, A.BankAcNo " +
                    "From TblVendorBankAccountUpdate A, TblBank B " +
                    "Where A.BankCode=B.BankCode And A.VdCode=@VdCode " +
                    "Order By A.DNo",
                    new string[] 
                    { 
                        "BankCode", 
                        "BankName", "BankBranch", "BankAcName", "BankAcNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowVendorItemCategory(string VdCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                    "Select A.ItCtCode, B.ItCtName from tblvendoritemcategoryUpdate A " +
                    "Inner Join Tblitemcategory B On A.ItCtCode=B.ItCtCode " +
                    "Where A.VdCode=@VdCode " +
                    "Order By B.ItCtName",
                    new string[] 
                    { 
                        "ItCtCode", 
                        "ItCtName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        //  Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        private void ShowVendorSector(string VdCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm,
                    "Select A.SectorCode, B.SectorName, A.QualificationCode, C.QualificationName from tblvendorsectorUpdate A " +
                    "Inner Join Tblsector B On A.SectorCode=B.SectorCode " +
                    "Left Join TblQualification C On A.QualificationCode = C.QualificationCode " +
                    "Where A.VdCode=@VdCode " +
                    "Order By B.SectorName",
                    new string[] 
                    { 
                        "SectorCode", 
                        "SectorName", "QualificationCode", "QualificationName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mIsVdCodeUseVdCt = Sm.GetParameter("IsVdCodeUseVdCt") == "Y";
        }

        private void SetLueParent(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.VdCode As Col1, Concat(A.VdName, ' [ ', B.CityName, ' ]') As Col2 ");
                SQL.AppendLine("From TblVendor A ");
                SQL.AppendLine("Left Join TblCity B On A.CityCode = B.CityCode ");
                SQL.AppendLine("Where A.VdCode Not In (Select VdCode From TblVendor Where (Parent Is Not Null Or Length(Parent) > 0)) ");
                if (TxtVdCode.Text.Length > 0)
                    SQL.AppendLine("And A.VdCode <> '" + TxtVdCode.Text + "' ");
                SQL.AppendLine("Order By A.VdName Desc; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region events

        #region Misc Control events

        private void LueVdCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCtCode, new Sm.RefreshLue1(Sl.SetLueVdCtCode));
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));

            LueSDCode.EditValue = null;
            LueVilCode.EditValue = null;

            if (Sm.GetLue(LueCityCode).Length != 0)
            {
                LueSDCode.EditValue = "<Refresh>";
                Sm.SetControlReadOnly(LueSDCode, (BtnSave.Enabled ? false : true));
            }
            else
                Sm.SetControlReadOnly(LueSDCode, true);
        }

        private void LueSDCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSDCode, new Sm.RefreshLue2(Sl.SetLueSDCode), Sm.GetLue(LueCityCode));
            LueVilCode.EditValue = null;
            if (Sm.GetLue(LueSDCode).Length != 0)
            {
                LueVilCode.EditValue = "<Refresh>";
                Sm.SetControlReadOnly(LueVilCode, (BtnSave.Enabled ? false : true));
            }
            else
            {
                Sm.SetControlReadOnly(LueVilCode, true);
            }
        }

        private void LueVilCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVilCode, new Sm.RefreshLue2(Sl.SetLueVilCode), Sm.GetLue(LueSDCode));
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(SetLueParent));
        }

        #endregion

        #region Button Events

        private void BtnVendor_Click(object sender, EventArgs e)
        {
            if(TxtVdCode.Text.Length > 0)
            {
                var f = new FrmVendor(string.Empty);
                f.Tag = string.Empty;
                f.Text = "Old Vendor Data";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mVdCode = TxtVdCode.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

    }
}
