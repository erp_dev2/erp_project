﻿#region Update 
/* 
    07/04/2020 [WED/SRN] new apps, CtQt pakai discount rate 5 biji
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCtQt4Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmCtQt4 mFrmParent;
        private string mSQL;
        internal bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmCtQt4Find(FrmCtQt4 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ActInd, E.CtName, G.ItCode, H.ItName, H.ItCodeInternal, H.Specification, I.CtItCode, I.CtItName, C.UPrice,  ");
            SQL.AppendLine("Case When A.Status='O' Then 'Outstanding' ");
            SQL.AppendLine("When A.Status='A' Then 'Approved' ");
            SQL.AppendLine("When A.Status='C' Then 'Cancelled' ");
            SQL.AppendLine("End As Status, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblCtQtHdr A ");
            SQL.AppendLine("Inner Join TblCtQtDtl C On A.DocNo = C.DocNo And A.DocType = 2 ");
            SQL.AppendLine("Inner Join TblCustomer E On A.CtCode=E.CtCode ");
            SQL.AppendLine("Inner Join TblItemPricehdr F On C.ItemPriceDocNo = F.DocNo ");
            SQL.AppendLine("Inner Join TblItemPriceDtl G On C.ItemPriceDocNo = G.Docno And C.ItemPriceDNo = G.Dno ");
            SQL.AppendLine("Inner Join TblItem H On G.ItCode=H.ItCode ");
            SQL.AppendLine("Left Join TblCustomerItem I On G.ItCode = I.ItCode And A.CtCode=I.CtCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=H.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Active",
                        "Status",
                        "Customer",
                        
                        //6-10
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Unit Price",
                        "Created"+Environment.NewLine+"By",
                        
                        //11-15
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",  
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",

                        //16-19
                        "Specification",
                        "Customer's"+Environment.NewLine+"Item Code",
                        "Local Code",
                        "Customer's"+Environment.NewLine+"Item Name",
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3});
            Sm.GrdColButton(Grd1, new int[] { 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Grd1.Cols[16].Move(9);
            Grd1.Cols[19].Move(9);
            Grd1.Cols[17].Move(9);
            Grd1.Cols[18].Move(8);
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 10, 11, 12, 13, 14, 15, 16, 17 }, false);
            if (!mFrmParent.mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 19 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 10, 11, 12, 13, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "G.ItCode", "H.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo, C.DNo;",
                        new string[]
                        {
                            //0
                           "DocNo",

                           //1-5
                           "DocDt", "ActInd", "Status", "CtName", "ItCode",  

                           //6-10
                           "ItName", "UPrice", "CreateBy", "CreateDt", "LastUpBy", 
                           
                           //11-15
                           "LastUpDt", "Specification", "CtItCode", "ItCodeInternal", "CtItName"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 15);
                        }, true, true, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

        }
        #endregion 
        
        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
