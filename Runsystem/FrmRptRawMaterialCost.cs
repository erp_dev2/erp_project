﻿#region Update
/*
    10/05/2017 [WED] tambah informasi rekening bank
    28/11/2018 [TKG] tambah informasi rekening bank 2
    16/12/2019 [HAR/IOK] Amount transaksi tidak dibatasi digit dibelakang koma, sedang di data dibatasi 4 digit, 
                sehingga perlu di rounding agar sesuai
    05/04/2022 [DITA/IOK] menampilkan data IdentityNo dan TIN dari Vendor
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptRawMaterialCost : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptRawMaterialCost(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueOptMonitoring(LueOptMonitoring);
                Sm.SetLue(LueOptMonitoring, "Log");
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL(string type)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, F.DocDt, D.QueueNo, ");
            SQL.AppendLine("Case A.DocType When '1' Then 'Log' When '2' Then 'Balok' End As Type, ");
            SQL.AppendLine("Case A.DocType When '1' Then K.VdName When '2' Then L.VdName End As Vendor, ");
            SQL.AppendLine("Case A.DocType When '1' Then K.IdentityNo When '2' Then L.IdentityNo End As VdIdentityNo, ");
            SQL.AppendLine("Case A.DocType When '1' Then K.TIN When '2' Then L.TIN End As VdTIN, ");
            SQL.AppendLine("G.Qty1, B.Qty As Qty2, Truncate(B.Amt, 4) As Amt, ");
            SQL.AppendLine("Truncate(A.TransportCost, 4) As TransportCost, Truncate(A.Tax, 4) As tax, Truncate(A.RoundingValue, 4) RoundingValue, ");
            SQL.AppendLine("Case When H.BankAcNo Is Null Then IfNull(H.BankAcNm, '') Else Concat(H.BankAcNo, ' [', IfNull(H.BankAcNm, ''), ']') End As BankAcNo, ");
            SQL.AppendLine("Case When J.BankAcNo Is Null Then IfNull(J.BankAcNm, '') Else Concat(J.BankAcNo, ' [', IfNull(J.BankAcNm, ''), ']') End As BankAcNo2, ");
            SQL.AppendLine("Truncate(A.Amt1, 4) As Amt1, Truncate(A.Amt2, 4) As Amt2 ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.DocNo, Sum(T3.Qty) As Qty, Sum(T3.Qty*T4.UPrice) As Amt ");
            SQL.AppendLine("    From TblPurchaseInvoiceRawMaterialHdr T1 ");
            SQL.AppendLine("    Inner Join TblVoucherhdr T2 ");
            SQL.AppendLine("        On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
            SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("        And T2.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceRawMaterialDtl T3 On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblQt2Dtl T4 On T3.QtDocNo=T4.DocNo And T3.QtDNo=T4.DNo ");
            SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            if (type.Length!=0) SQL.AppendLine("    And T1.DocType=@Type ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblRawMaterialVerify C On A.RawMaterialVerifyDocNo= C.DocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr D On C.LegaldocVerifyDocNo = D.DocNo And D.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblVoucherhdr F ");
            SQL.AppendLine("    On A.VoucherRequestDocNo=F.VoucherRequestDocNo ");
            SQL.AppendLine("    And F.CancelInd = 'N' ");
            SQL.AppendLine("    And F.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select G1.LegaldocVerifyDocNo, G1.DocType, Sum(G2.Qty) Qty1 ");
            SQL.AppendLine("    From TblRecvRawMaterialHdr G1 ");
            SQL.AppendLine("    Inner Join TblRecvRawMaterialDtl2 G2 On G1.DocNo=G2.DocNo ");
            SQL.AppendLine("    Where G1.ProcessInd='F' ");
            SQL.AppendLine("    And G1.CancelInd = 'N' ");
            if (type.Length != 0) SQL.AppendLine("    And G1.DocType=@Type ");
            SQL.AppendLine("    And G1.LegalDocVerifyDocNo In ( ");
            SQL.AppendLine("        Select Distinct T2.LegaldocVerifyDocNo ");
            SQL.AppendLine("        From TblPurchaseInvoiceRawMaterialHdr T1 ");
            SQL.AppendLine("        Inner Join TblRawMaterialVerify T2 On T1.RawMaterialVerifyDocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblVoucherhdr T3 ");
            SQL.AppendLine("            On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
            SQL.AppendLine("            And T3.CancelInd='N' ");
            SQL.AppendLine("            And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Group By G1.LegalDocVerifyDocNo, G1.DocType ");
            SQL.AppendLine(") G On D.DocNo=G.LegalDocVerifyDocNo And A.DocType=G.DocType  ");
            SQL.AppendLine("Left Join TblBankAccount H On A.BankAcCode = H.BankAcCode ");
            SQL.AppendLine("Left Join TblBankAccount J On A.BankAcCode2 = J.BankAcCode ");
            SQL.AppendLine("Left Join TblVendor K On D.VdCode1=K.VdCode ");
            SQL.AppendLine("Left Join TblVendor L On D.VdCode2=L.VdCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            if (type.Length != 0) SQL.AppendLine("And A.DocType=@Type ");
            SQL.AppendLine("Order By F.DocDt, D.QueueNo;");

            //SQL.AppendLine("Select X.QueueNo, X.DateVcr, X.Type, X.Vendor, X.Batang, X.Kubik, X.price, ");
            //SQL.AppendLine("X.transportCost, X.tax, X.RoundingValue, X.BankAcNo, X.BankAcNo2 ");
            //SQL.AppendLine("From ( ");
            //SQL.AppendLine("  Select Z.DocNo, Z.QueueNo, Z.DocDt As DateVcr, ");
            //SQL.AppendLine("  case When Z.DocType = '1' Then 'Log' ");
            //SQL.AppendLine("  When Z.DocType = '2' Then 'Balok' ");
            //SQL.AppendLine("  else Null End As Type, ");
            //SQL.AppendLine("  Case When Z.DocType = '1' Then T3.Vdname  ");
            //SQL.AppendLine("  when Z.Doctype = '2' Then T4.Vdname ");
            //SQL.AppendLine("  else Null End As vendor, ");
            //SQL.AppendLine("  Z.Qty1 As Batang, ");
            //SQL.AppendLine("  SUM(Z.Qty2) As Kubik, SUM(Z.harga) As price, Z.TransportCost, Z.Tax, Z.RoundingValue, Z.BankAcNo, Z.BankAcNo2 ");
            //SQL.AppendLine("  From ( ");
            //SQL.AppendLine("    Select A.DocNo, F.DocDt, D.QueueNo, A.DocType, G.Qty1, D.VdCode1, D.VdCode2, ");
            //SQL.AppendLine("    B.Qty As Qty2, (B.Qty*I.Uprice) As harga, A.TransportCost, A.Tax, A.RoundingValue, ");
            //SQL.AppendLine("    Case When H.BankAcNo Is Null Then IfNull(H.BankAcNm, '') Else ");
            //SQL.AppendLine("    Concat(H.BankAcNo, ' [', IfNull(H.BankAcNm, ''), ']') End As BankAcNo, ");
            //SQL.AppendLine("    Case When J.BankAcNo Is Null Then IfNull(J.BankAcNm, '') Else ");
            //SQL.AppendLine("    Concat(J.BankAcNo, ' [', IfNull(J.BankAcNm, ''), ']') End As BankAcNo2, ");
            //SQL.AppendLine("    A.Amt1, A.Amt2 ");
            //SQL.AppendLine("    From TblPurchaseInvoiceRawMaterialHdr A ");
            //SQL.AppendLine("    Inner Join TblPurchaseInvoiceRawMaterialDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join TblRawMaterialVerify C On A.RawMaterialVerifyDocNo= C.DocNo And C.CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join TblLegalDocVerifyHdr D On C.LegaldocVerifyDocNo = D.DocNo And D.CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join TblVoucherhdr F On A.VoucherRequestDocNo=F.VoucherRequestDocNo And F.CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join TblQt2Dtl I On I.DocNo = B.QtDocNo And B.QtDNo = I.DNo ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select G1.LegaldocVerifyDocNo, G1.DocType, Sum(G2.Qty) Qty1 ");
            //SQL.AppendLine("        From TblRecvRawMaterialHdr G1 ");
            //SQL.AppendLine("        Inner Join TblRecvrawMaterialDtl2 G2 On G1.DocNo = G2.DocNo ");
            //SQL.AppendLine("        Where G1.ProcessInd='F' ");
            //SQL.AppendLine("        And G1.CancelInd = 'N' ");
            //if (TypeLog != "3") SQL.AppendLine(" And  G1.DocType='" + TypeLog + "'  ");
            //SQL.AppendLine("        And G1.LegalDocVerifyDocNo In ( ");
            //SQL.AppendLine("            Select Distinct T2.LegaldocVerifyDocNo ");
            //SQL.AppendLine("            From TblPurchaseInvoiceRawMaterialHdr T1 ");
            //SQL.AppendLine("            Inner Join TblRawMaterialVerify T2 On T1.RawMaterialVerifyDocNo=T2.DocNo And T2.CancelInd='N' ");
            //SQL.AppendLine("            Inner Join TblVoucherhdr T3 ");
            //SQL.AppendLine("                On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
            //SQL.AppendLine("                And T3.CancelInd='N' ");
            //SQL.AppendLine("                And T3.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("            ) ");
            //SQL.AppendLine("        Group By G1.LegaldocVerifyDocNo, G1.DocType ");
            //SQL.AppendLine("        ) G On D.DocNo=G.LegaldocVerifyDocNo And A.DocType=G.DocType  ");
            //SQL.AppendLine("    Left Join TblBankAccount H On A.BankAcCode = H.BankAcCode ");
            //SQL.AppendLine("    Left Join TblBankAccount J On A.BankAcCode2 = J.BankAcCode ");
            //SQL.AppendLine(" Where F.DocDt Between @DocDt1 And @DocDt2 ");
            //if (TypeLog != "3") SQL.AppendLine("And A.DocType ='" + TypeLog + "'  ");
            //SQL.AppendLine(")Z ");
            //SQL.AppendLine("Left Join tblVendor T3 On Z.VdCode1 = T3.VdCode  And Z.DocType = '1' ");
            //SQL.AppendLine("Left Join tblVendor T4 On Z.VdCode2 = T4.VdCode  And Z.DocType = '2' ");
            //SQL.AppendLine("Group By Z.DocNo,Z.QueueNo, Z.DocType, Z.Qty1, Z.TransportCost, Z.tax, Z.RoundingValue, Z.BankAcNo, Z.BankAcNo2 ");
            //SQL.AppendLine(")X ");
            //SQL.AppendLine(" Where X.DateVcr Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("Order By X.DateVcr;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Queue#",
                        "Date",
                        "Type",
                        "Vendor",
                        "Bank Account 1",                        

                        //6-10
                        "Amount",
                        "Bank Account 2", 
                        "Amount",
                        "Quantity"+Environment.NewLine+"(Batang)",
                        "Volume"+Environment.NewLine+"(Kubik)",

                        //11-15
                        "Amount",
                        "Transport"+Environment.NewLine+"Cost",
                        "Tax",
                        "Rounding"+Environment.NewLine+"Value",
                        "Total",

                        //16-17
                        "Identity Number",
                        "Taxpayer Identification#"
                    },
                     new int[] 
                    {
                        40,
                        100, 100, 80, 150, 250, 
                        100, 250, 100, 100, 100, 
                        150, 100, 100, 100, 100,
                        180, 180
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Grd1.Cols[16].Move(5);
            Grd1.Cols[17].Move(6);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueOptMonitoring, "Type") ||
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                var type = string.Empty;
                switch (Sm.GetLue(LueOptMonitoring))
                {
                    case "Log":
                        type = "1"; break;
                    case "Balok":
                        type = "2"; break;
                }
                Sm.CmParam<string>(ref cm, "@Type", type);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL(type),
                        new string[]
                        {
                            //0
                            "QueueNo", 

                            //1-5
                            "DocDt", "Type", "Vendor", "BankAcNo", "Amt1", 
                            
                            //6-10
                            "BankAcNo2", "Amt2", "Qty1", "Qty2", "Amt", 
                            
                            //11-15
                            "TransportCost", "Tax", "RoundingValue", "VdIdentityNo", "VdTIN"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Grd.Cells[Row, 15].Value = 
                                Math.Round(Sm.GetGrdDec(Grd, Row, 11) - 
                                Sm.GetGrdDec(Grd, Row, 12) - 
                                Sm.GetGrdDec(Grd, Row, 13) + 
                                Sm.GetGrdDec(Grd, Row, 14), 0);
                            if (Sm.GetGrdStr(Grd, Row, 7).Length == 0) Grd.Cells[Row, 6].Value = Sm.GetGrdDec(Grd1, Row, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Additional Method

        public static void SetLueOptMonitoring(LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { "Log", "Balok", "All" });
        }

        internal static void SetLookUpEdit(LookUpEdit Lue, object ds)
        {
            try
            {
                Lue.DataBindings.Clear();
                Lue.Properties.DataSource = ds;
                Lue.Properties.PopulateColumns();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Lue.EditValue = null;
            }
        }
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
