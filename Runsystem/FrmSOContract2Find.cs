﻿#region Update
/*
    19/11/2019 [WED/IMS] amount inventory belum terhitung
    26/11/2019 [DITA/IMS] tambah kolom project code, project name, ntp docno, PONo
    23/09/2020 [IBL/IMS] hilangkan kolom amount. tambah kolom Qty Revision, UPrice Revision, Total Amt Revision
    07/10/2020 [ICA/IMS] Memberika background warna merah untuk SO Contract yang belum punya BOM
    11/11/2020 [ICA/IMS] Tata letak kolom localcode, itname, qtyrevision, unit price revision dan total price revision
    17/11/2020 [TKG/IMS] Specification diganti remark
    03/03/2021 [VIN/IMS] bug: item terdouble
    14/04/2021 [WED/IMS] bug : item masih terdouble
    27/04/2021 [VIN/IMS] bug : quantity masih terdouble
    18/05/2021 [VIN/IMS] bug : jumlah baris terdouble
    03/06/2021 [VIN/IMS] bug : jumlah baris terdouble
    09/06/2021 [TKG/IMS] bug ubah query untuk mengambil data revision
    10/06/2021 [MYA/IMS] Menambah ceklist exclude cancel ketika find soc
    10/06/2021 [TKG/IMS] bug tidak muncul remark ketika belum ada revisi
    26/08/2021 [IBL/IMS] Tambah approval
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSOContract2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmSOContract2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSOContract2Find(FrmSOContract2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approve' When 'C' Then 'Cancelled' End As StatusDesc, A.CancelInd, ");
            SQL.AppendLine("A.SAAddress, B.CtName, A.BOQDocNo, (A.Amt + A.AmtBOM) Amt, ");
            SQL.AppendLine("C.ItCode, D.ItCodeInternal, D.ItName, J.Remark, E.CtItCode, E.CtItName, C.DeliveryDt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            SQL.AppendLine("IfNull(H.ProjectCode, A.ProjectCode2) ProjectCode, IfNull(H.ProjectName, G.ProjectName) ProjectName, I.DocNo As NTPDocNo, A.PONo, ");
            SQL.AppendLine("J.RevisionUPrice, J.RevisionQty, J.RevisionAmt, K.BOMDocNo ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join tblCustomer B on A.CtCode = B.CtCode And A.DocType = '2' ");
            SQL.AppendLine("Inner Join TblSOContractDtl C On A.DocNo=C.DocNo  ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblCustomerItem E On C.ItCode=E.ItCode And A.CtCode=E.CtCode ");
            SQL.AppendLine("Inner Join TblBOQHdr F On A.BOQDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblLOphdr G On F.LOPDocNo=G.DocNO ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (G.SiteCode Is Null Or ( ");
                SQL.AppendLine("    G.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(G.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Left Join TblProjectGroup H On H.PGCode=G.PGCode ");
            SQL.AppendLine("Left Join TblNoticeToProceed I On G.DocNo = I.LOPDocNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Distinct T2.ItCode, T2.DocNo, T2.No, ");
            SQL.AppendLine("    IfNull(T4.UPrice, 0.00) RevisionUPrice, ");
            SQL.AppendLine("    IfNull(T4.Qty, 0.00) As RevisionQty, ");
            SQL.AppendLine("    IfNull(T4.Amt, 0.00) As RevisionAmt, ");
            SQL.AppendLine("    T2.Remark ");
            SQL.AppendLine("    From TblSOContractHdr T1 ");
            SQL.AppendLine("    Inner Join TblSOContractDtl4 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Max(X2.DocNo) DocNo, X2.SOCDocNo ");
            SQL.AppendLine("        From TblSOContractHdr X1, TblSOContractRevisionHdr X2 ");
            SQL.AppendLine("        Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And X1.DocNo=X2.SOCDocNo ");
            SQL.AppendLine("        Group By X2.SOCDocNo ");
            SQL.AppendLine("    ) T3 On T1.DocNo=T3.SOCDocNo ");
            SQL.AppendLine("    Left Join TblSOContractRevisionDtl4 T4 On T3.DocNo=T4.DocNo And T2.DNo=T4.DNo And T2.ItCode=T4.ItCode ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("    Select Distinct T2.ItCode, T2.DocNo, T2.No, ");
            SQL.AppendLine("    IfNull(T4.UPrice, 0.00) RevisionUPrice, ");
            SQL.AppendLine("    IfNull(T4.Qty, 0.00) As RevisionQty, ");
            SQL.AppendLine("    IfNull(T4.Amt, 0.00) As RevisionAmt, ");
            SQL.AppendLine("    T2.Remark ");
            SQL.AppendLine("    From TblSOContractHdr T1 ");
            SQL.AppendLine("    Inner Join TblSOContractDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Max(X2.DocNo) DocNo, X2.SOCDocNo ");
            SQL.AppendLine("        From TblSOContractHdr X1, TblSOContractRevisionHdr X2 ");
            SQL.AppendLine("        Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And X1.DocNo=X2.SOCDocNo ");
            SQL.AppendLine("        Group By X2.SOCDocNo ");
            SQL.AppendLine("    ) T3 On T1.DocNo=T3.SOCDocNo ");
            SQL.AppendLine("    Left Join TblSOContractRevisionDtl5 T4 On T3.DocNo=T4.DocNo And T2.DNo=T4.DNo And T2.ItCode=T4.ItCode ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");

            SQL.AppendLine(") J On A.DocNo = J.DocNo And C.No = J.No And C.ItCode=J.ItCode ");



            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select Distinct X1.ItCode, X1.DocNo, X1.No, X4.CreateDt, ");
            //SQL.AppendLine("    IfNull(X4.UPrice, 0.00) RevisionUPrice, IfNull(X4.Qty, 0.00) As RevisionQty, IfNull(X4.Amt, 0.00) As RevisionAmt, X1.Remark ");
            //SQL.AppendLine("    From TblSOContractDtl4 X1 ");
            //SQL.AppendLine("    Inner Join TblBOQDtl2 X2 On X1.BOQDocNo = X2.DocNo ");
            //SQL.AppendLine("        And X1.ItCode = X2.ItCode ");
            //SQL.AppendLine("        And X1.SeqNo = X2.SeqNo ");
            //SQL.AppendLine("        And X1.No = X2.No ");
            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select Max(DocNo) DocNo, SOCDocNo ");
            //SQL.AppendLine("        From TblSOContractRevisionHdr ");
            //SQL.AppendLine("        Group By SOCDocNo ");
            //SQL.AppendLine("    ) X3 On X1.DocNo = X3.SOCDocNo ");
            //SQL.AppendLine("    Left Join TblSOContractRevisionDtl4 X4 On X3.DocNo = X4.DocNo And X1.DNo = X4.DNo And X1.ItCode = X4.ItCode ");

            //SQL.AppendLine("    Union All ");

            //SQL.AppendLine("    Select Distinct X1.ItCode, X1.DocNo, X1.No, X4.CreateDt, ");
            //SQL.AppendLine("    IfNull(X4.UPrice, 0.00) RevisionUPrice, IfNull(X4.Qty, 0.00) As RevisionQty, IfNull(X4.Amt, 0.00) As RevisionAmt, X1.Remark ");
            //SQL.AppendLine("    From TblSOContractDtl5 X1 ");
            //SQL.AppendLine("    Inner Join TblBOQDtl3 X2 On X1.BOQDocNo = X2.DocNo ");
            //SQL.AppendLine("        And X1.ItCode = X2.ItCode ");
            //SQL.AppendLine("        And X1.SeqNo = X2.SeqNo ");
            //SQL.AppendLine("        And X1.No = X2.No ");
            //SQL.AppendLine("    Left Join ( ");
            //SQL.AppendLine("        Select Max(DocNo) DocNo, SOCDocNo ");
            //SQL.AppendLine("        From TblSOContractRevisionHdr ");
            //SQL.AppendLine("        Group By SOCDocNo ");
            //SQL.AppendLine("    ) X3 On X1.DocNo = X3.SOCDocNo ");
            //SQL.AppendLine("    Left Join TblSOContractRevisionDtl5 X4 On X3.DocNo = X4.DocNo And X1.DNo = X4.DNo And X1.ItCode = X4.ItCode ");
            //SQL.AppendLine(") J On A.DocNo = J.DocNo And C.No = J.No And C.ItCode=J.ItCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    SELECT X1.DocNo, GROUP_CONCAT(Distinct ifnull(X2.DocNo, '')) BOMDocNo ");
            SQL.AppendLine("    FROM TblSOContractHdr X1 ");
            SQL.AppendLine("    Inner Join TblBOM2Hdr X2 ON X1.DocNo = X2.SOContractDocNo ");
            SQL.AppendLine("    AND X2.CancelInd='N' AND (X2.Status='O' OR X2.Status='A') ");
            SQL.AppendLine("    AND X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    GROUP BY X1.DocNo ");
            SQL.AppendLine(")K ON K.DocNo=A.DocNo  ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 31;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Status",
                        "Cancel",
                        "Local#",
                        
                        //6-10
                        "Customer",
                        "Bill Of Quantity",
                        "Shipping"+Environment.NewLine+"Address",
                        "Amount",
                        "Item's Code",

                        //11-15
                        "Local Code",
                        "Item's Name",
                        "Customer's"+Environment.NewLine+"Item Code",
                        "Customer's"+Environment.NewLine+"Item Name",
                        "Remark",

                        //16-20
                        "Delivery Date",
                        "Created By",
                        "Created Date", 
                        "Created Time", 
                        "Last Updated By",
 
                        //21-25
                        "Last Updated Date", 
                        "Last Updated Time",
                        "Project Code",
                        "Project Name",
                        "Notice To Proceed#",

                        //26-30
                        "Customer's PO#",
                        "Quantity"+Environment.NewLine+"Revision",
                        "Unit Price"+Environment.NewLine+"Revision",
                        "Total Price"+Environment.NewLine+"Revision",
                        "BOM#"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        170, 80, 80, 60, 130, 
                        
                        //6-10
                        200, 130, 200, 120, 100,  

                        //11-15
                        100, 250, 100, 200, 250, 

                        //16-20
                        100, 130, 130, 130, 130, 

                        //21-25
                        130, 130, 120, 200, 120,

                        //26-29
                        100, 100, 120, 120, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 27, 28, 29 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 16, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 17, 18, 19, 20, 21, 22, 30 }, false);
            Grd1.Cols[23].Move(17);
            Grd1.Cols[24].Move(18);
            Grd1.Cols[25].Move(19);
            Grd1.Cols[26].Move(20);
            Grd1.Cols[27].Move(13);
            Grd1.Cols[28].Move(14);
            Grd1.Cols[29].Move(15);

            if (!mFrmParent.mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkExCancelDoc.Checked)
                {
                    var mSQL2 = new StringBuilder();

                    mSQL2.AppendLine("And A.CancelInd = 'N' ");

                    Filter += mSQL2.ToString();
                }

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "C.ItCode", "D.ItName", "D.ItCodeInternal" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {

                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "LocalDocNo", "CtName",   
                            
                            //6-10
                            "BOQDocNo", "SAAddress", "Amt", "ItCode", "ItCodeInternal", 
                            
                            //11-15
                            "ItName", "CtItCode", "CtItName", "Remark","DeliveryDt", 

                            //16-20
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "ProjectCode",

                            //21-25
                            "ProjectName", "NTPDocNo","PONo", "RevisionQty", "RevisionUPrice",

                            //26-27
                            "RevisionAmt", "BOMDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                            if (Sm.GetGrdStr(Grd1, Row, 30).Length == 0)
                                Grd1.Rows[Row].BackColor = Color.Red;
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);

        }
        #endregion 
    }
}
