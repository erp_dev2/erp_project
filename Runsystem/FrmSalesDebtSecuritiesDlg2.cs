﻿#region Update
/*
    22/06/2022 [RDA/PRODUCT] new menu
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesDebtSecuritiesDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesDebtSecurities mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesDebtSecuritiesDlg2(FrmSalesDebtSecurities FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueOption(ref LueInvestmentType, "InvestmentType");
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select T1.PortofolioId as InvestmentCode, T1.PortofolioName as InvestmentName, T1.BankAcNo, T1.BankAcNm, T1.Issuer, T1.InvestmentCtCode, ");
            SQL.AppendLine("    T1.InvestmentCtName, T1.InvestmentType, T1.InvestmentTypeNm, T1.UomCode, IfNull(T1.NominalAmt, 0.00) As NominalAmt, ");
            SQL.AppendLine("    IfNull(T2.MovingAvgPrice, 0.00) As MovingAvgPrice, IfNull(T3.MarketPrice, 0.00) As MarketPrice,  T1.BankAcCode,");
            SQL.AppendLine("    T4.InterestFreq, T4.InterestRateAmt, T4.AnnualDays, T4.MaturityDt, T4.NextCouponDt, T4.LastCouponDt, T4.CurCode, T5.Amt AS CurRt, T4.InvestmentDebtCode");
            SQL.AppendLine("	 From ( ");
            SQL.AppendLine("		Select C.PortofolioID, D.PortofolioName, A.InvestmentCode, F.BankAcNo, F.BankAcNm, D.Issuer, E.InvestmentCtCode, E.InvestmentCtName, ");
            SQL.AppendLine("      G.OptDesc As InvestmentTypeNm, Sum(IfNull(A.NominalAmt, 0.00)) As NominalAmt, C.UomCode, B.InvestmentType, F.BankAcCode");
            SQL.AppendLine("		From TblInvestmentStockSummary A ");
            SQL.AppendLine("		Inner Join TblInvestmentStockMovement B On A.Source = B.Source ");
            SQL.AppendLine("			And A.InvestmentCode = B.InvestmentCode ");
            SQL.AppendLine("		Inner Join TblInvestmentItemDebt C On A.InvestmentCode = C.InvestmentDebtCode ");
            SQL.AppendLine("		Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("			 And FIND_IN_SET(D.InvestmentCtCode, @DebtInvestmentCtCode) ");
            SQL.AppendLine("		Inner Join TblInvestmentCategory E On D.InvestmentCtCode = E.InvestmentCtCode ");
            SQL.AppendLine("		Inner Join TblBankAccount F On A.BankAcCode = F.BankAcCode ");
            SQL.AppendLine("		Inner Join TblOption G On G.OptCat = 'InvestmentType' And B.InvestmentType = G.OptCode ");
            SQL.AppendLine("		Inner Join TblInvestmentStockPrice H On A.Source = H.Source ");
            SQL.AppendLine("			Where B.DocDt <= @TradeDt ");
            SQL.AppendLine("		Group By A.InvestmentCode, D.InvestmentCtCode, B.InvestmentType, F.BankAcCode ");
            SQL.AppendLine("		  Having Sum(A.NominalAmt)<>0.00");
            SQL.AppendLine("	)T1 ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("		Select A.InvestmentCode, D.InvestmentCtCode, B.InvestmentType, ");
            SQL.AppendLine("		(Sum((IfNull(A.NominalAmt, 0.00) * (IfNull(F.UPrice, 0.00) * 0.01)))) / (Sum(IfNull(A.NominalAmt, 0.00))) As MovingAvgPrice ");
            SQL.AppendLine("		From TblInvestmentStockSummary A ");
            SQL.AppendLine("		Inner Join TblInvestmentStockMovement B On A.Source = B.Source ");
            SQL.AppendLine("			And A.InvestmentCode = B.InvestmentCode ");
            SQL.AppendLine("		INNER JOIN tblinvestmentitemdebt C On A.InvestmentCode = C.InvestmentdebtCode ");
            SQL.AppendLine("		Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("			And FIND_IN_SET(D.InvestmentCtCode, @DebtInvestmentCtCode) ");
            SQL.AppendLine("		Inner Join TblOption E On E.OptCat = 'InvestmentType' And B.InvestmentType = E.OptCode ");
            SQL.AppendLine("		Inner Join TblInvestmentStockPrice F On A.Source = F.Source ");
            SQL.AppendLine("		Where B.DocDt <= @TradeDt ");
            SQL.AppendLine("		Group By D.PortofolioId, D.InvestmentCtCode, B.InvestmentType ");
            SQL.AppendLine("	)T2 On T1.InvestmentCode = T2.InvestmentCode ");
            SQL.AppendLine("	    And T1.InvestmentCtCode = T2.InvestmentCtCode ");
            SQL.AppendLine("	    And T1.InvestmentType = T2.InvestmentType ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("    	Select B.InvestmentCode, B.InvestmentType, B.BankAcCode, D.InvestmentCtCode, B.MarketPrice ");
            SQL.AppendLine("		From TblInvestmentPortofolioClosingHdr A ");
            SQL.AppendLine("		Inner Join TblInvestmentPortofolioClosingDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblInvestmentItemDebt C On B.InvestmentCode = C.InvestmentDebtCode ");
            SQL.AppendLine("		Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("		Where A.CancelInd = 'N' ");
            SQL.AppendLine("		And A.ClosingDt = @TradeDt ");
            SQL.AppendLine("    )T3 On T1.InvestmentCode = T3.InvestmentCode ");
            SQL.AppendLine("	    And T1.InvestmentCtCode = T3.InvestmentCtCode ");
            SQL.AppendLine("	    And T1.InvestmentType = T3.InvestmentType ");
            SQL.AppendLine("	    And T1.BankAcCode = T3.BankAcCode ");
            SQL.AppendLine("	 Inner Join tblinvestmentitemdebt T4 ON T1.InvestmentCode = T4.InvestmentDebtCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("         Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("         From TblCurrencyRate D1 ");
            SQL.AppendLine("         Inner Join ( ");
            SQL.AppendLine("             Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("             From TblCurrencyRate ");
            SQL.AppendLine("             Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("             Group By CurCode1 ");
            SQL.AppendLine("         ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) T5 ON T4.CurCode=T5.CurCode1 ");
            SQL.AppendLine(") Tbl ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.ReadOnly = true;
            Grd1.FrozenArea.ColCount = 0;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Investment Code",
                        "Investment Name",
                        "Investment Bank"+Environment.NewLine+"Account (RDN)#",
                        "Investment Bank"+Environment.NewLine+"Account Name",
                        "Issuer",

                        //6-10
                        "Category Code",
                        "Category",
                        "Type Code",
                        "Type",
                        "UomCode",

                        //11-15
                        "Nominal Amount",
                        "Moving Average Price",
                        "Market Price",
                        "Bank Account Code",
                        "Interest Frequency",

                        //16-20
                        "Interest Rate Amount",
                        "Annual Days",
                        "Maturity Date",
                        "Next Coupon Dt",
                        "Last Coupon Dt",

                        //21-23
                        "Currency Code",
                        "Currency Rate",
                        "Investment Debt Code"
                    },
                     new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 250, 150, 250, 250,

                        //6-10
                        150, 150, 150, 150, 150,

                        //11-15
                        150, 150, 150, 150, 150,

                        //16-20
                        150, 150, 150, 150, 150,

                        //21-22
                        150, 150, 150
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 18, 19, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 15, 16, 22 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 6, 8, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 1 = 1";

                var cm = new MySqlCommand();

                //Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.CmParam<String>(ref cm, "@DebtInvestmentCtCode", mFrmParent.mDebtInvestmentCtCode);
                Sm.CmParam<String>(ref cm, "@TradeDt", Sm.Left(Sm.GetDte(mFrmParent.DteTradeDt), 8));

                Sm.FilterStr(ref Filter, ref cm, TxtInvestmentCode.Text, new string[] { "InvestmentCode" }); 
                Sm.FilterStr(ref Filter, ref cm, TxtInvestmentName.Text, new string[] { "InvestmentName" }); 
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueInvestmentType), "InvestmentType", true);
                Sm.FilterStr(ref Filter, ref cm, TxtInvestmentBankAcc.Text, new string[] { "BankAcNm" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "InvestmentCode", 

                            //1-5
                            "InvestmentName", "BankAcNo", "BankAcNm", "Issuer", "InvestmentCtCode", 
                            
                            //6-10
                            "InvestmentCtName", "InvestmentType", "InvestmentTypeNm", "UomCode", "NominalAmt", 
                            
                            //11-15
                            "MovingAvgPrice", "MarketPrice", "BankAcCode", "InterestFreq", "InterestRateAmt",

                            //16-20
                            "AnnualDays", "MaturityDt", "NextCouponDt", "LastCouponDt", "CurCode",

                            //21-22
                            "CurRt", "InvestmentDebtCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);

                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtInvestmentCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtInvestmentName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                Sm.SetLue(mFrmParent.LueInvestmentType, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8));
                Sm.SetLue(mFrmParent.LueInvestmentCtCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6));
                mFrmParent.TxtNominalAmt.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 11), 0);
                mFrmParent.TxtMovingAveragePrice.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 12), 0);
                mFrmParent.TxtMarketPrice.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 13), 0);
                mFrmParent.TxtInterestFrequency.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 15);
                mFrmParent.TxtCouponInterestRt.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 16), 0);
                Sm.SetLue(mFrmParent.LueAnnualDaysAssumption, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 17));
                Sm.SetDte(mFrmParent.DteMaturityDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 18));
                Sm.SetDte(mFrmParent.DteNextCouponDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 19));
                Sm.SetDte(mFrmParent.DteLastCouponDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 20));
                Sm.SetLue(mFrmParent.LueCurCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 21));
                mFrmParent.TxtCurRate.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 22), 0);

                mFrmParent.ChoosenNominalAmt = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 11);
                mFrmParent.ChoosenInvestmentDebtCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 23);
                mFrmParent.ComputeAmt();
                this.Close();
            }
        }

        #endregion

        #region Event

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        private void TxtInvestmentCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvestmentCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment Code");
        }

        private void TxtInvestmentName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvestmentName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment Name");
        }

        private void LueInvestmentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueInvestmentType, new Sm.RefreshLue2(Sl.SetLueOption), "InvestmentType");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkInvestmentType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Investment Type");
        }

        private void TxtInvestmentBankAcc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvestmentBankAcc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment Bank Account Name");
        }

        #endregion


    }
}
