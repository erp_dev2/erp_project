﻿#region Update
/*
    03/09/2021 [IBL/AMKA] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIncomingPayment3Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmIncomingPayment3 mFrmParent;
        private string 
            mSQL = string.Empty,
            mCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmIncomingPayment3Dlg3(FrmIncomingPayment3 FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Bank Code", 
                        "Bank Name", 
                        "Giro#",
                        "Due Date",

                        //6-7
                        "Currency",
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 0, 250, 150, 100,

                        //6-7
                        80, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BankCode, B.BankName, A.GiroNo, A.DueDt, A.CurCode, A.Amt ");
            SQL.AppendLine("From TblGiroSummary A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("And A.BusinessPartnerType='2' ");
            SQL.AppendLine("And A.BusinessPartnerCode=@BusinessPartnerCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                if (mFrmParent.Grd4.Rows.Count != 1)
                {
                    for (int row = 0; row < mFrmParent.Grd4.Rows.Count - 1; row++)
                        if (Sm.GetGrdStr(mFrmParent.Grd4, row, 1).Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " And ";
                            Filter += " Not(A.BankCode=@BankCode" + Sm.Right("00" + row, 3) + " And A.GiroNo=@GiroNo" + Sm.Right("00" + row, 3) + ") ";
                            Sm.CmParam<String>(ref cm, "@BankCode" + Sm.Right("00" + row, 3), Sm.GetGrdStr(mFrmParent.Grd4, row, 1));
                            Sm.CmParam<String>(ref cm, "@GiroNo" + Sm.Right("00" + row, 3), Sm.GetGrdStr(mFrmParent.Grd4, row, 3));
                        }
                }
                Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", mCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtGiroNo.Text, "A.GiroNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By B.BankName, A.GiroNo;",
                        new string[] 
                        { 
                            "BankCode", 
                            "BankName", "GiroNo", "DueDt", "CurCode", "Amt" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd4.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 6, Grd1, Row2, 7);
                        mFrmParent.Grd4.Rows.Add();                        
                        Sm.SetGrdNumValueZero(mFrmParent.Grd4, mFrmParent.Grd4.Rows.Count - 1, new int[] { 6 });
                    }
                }
                mFrmParent.ComputeGiroAmt();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account#.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string 
                BankCode = Sm.GetGrdStr(Grd1, Row, 2), 
                GiroNo = Sm.GetGrdStr(Grd1, Row, 4);
            for (int Index = 0; Index < mFrmParent.Grd4.Rows.Count - 1; Index++)
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd4, Index, 1), BankCode) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd4, Index, 3), GiroNo)
                    ) 
                    return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkGiroNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Giro#");
        }

        #endregion

        #endregion

    }
}
