﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWTG : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mWTGCode = string.Empty;
        internal FrmWTGFind FrmFind;

        #endregion

        #region Constructor

        public FrmWTG(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Working Time Group";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mWTGCode.Length != 0)
                {
                    ShowData(mWTGCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Employee Code",
                        "Employee Name",
                        "Employee's"+Environment.NewLine+"Old Code",
                        "Position",
                        "Department"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        100, 200, 80, 150, 150
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5 });
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtWTGCode, TxtWTGName, LueWTCode, LueWSRCode }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtWTGCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtWTGCode, TxtWTGName, LueWTCode, LueWSRCode }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtWTGCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtWTGName }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtWTGName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtWTGCode, TxtWTGName, LueWTCode, LueWSRCode
            });
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWTGFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                SetLueWTCode(ref LueWTCode, string.Empty);
                SetLueWSRCode(ref LueWSRCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWTGCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (!TxtWTGCode.Properties.ReadOnly)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveWTGHdr());

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveWTGDtl(Row));
            }

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWTGCode, "Working time group code", false) ||
                Sm.IsTxtEmpty(TxtWTGName, "Working time group name", false) ||
                Sm.IsLueEmpty(LueWTCode, "Working time") ||
                Sm.IsLueEmpty(LueWSRCode, "Work schedule rule") ||
                IsWTGCodeExisted() ||
                IsWTGNameExisted() ||
                IsGrdValueNotValid();
        }

        private bool IsWTGCodeExisted()
        {
            if (!TxtWTGCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select WTGCode From TblWTGHdr Where WTGCode=@WTGCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@WTGCode", TxtWTGCode.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Working time group code ( " + TxtWTGCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsWTGNameExisted()
        {

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select WTGCode From TblWTGHdr " +
                    "Where WTGName=@WTGName " +
                    (TxtWTGCode.Properties.ReadOnly ? "And WTGCode<>@WTGCode " : string.Empty) +
                    "Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@WTGName", TxtWTGName.Text);
            if (TxtWTGCode.Properties.ReadOnly)
                Sm.CmParam<String>(ref cm, "@WTGCode", TxtWTGCode.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Working time group name ( " + TxtWTGName.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Employee is empty.")) return true;       
            }
            return false;
        }

        private MySqlCommand SaveWTGHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblWTGHdr(WTGCode, WTGName, ActInd, WTCode, WSRCode, CreateBy, CreateDt) " +
                    "Values(@WTGCode, @WTGName, 'Y', @WTCode, @WSRCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@WTGCode", TxtWTGCode.Text);
            Sm.CmParam<String>(ref cm, "@WTGName", TxtWTGName.Text);
            Sm.CmParam<String>(ref cm, "@WTCode", Sm.GetLue(LueWTCode));
            Sm.CmParam<String>(ref cm, "@WSRCode", Sm.GetLue(LueWSRCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveWTGDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWTGDtl(WTGCode, EmpCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@WTGCode, @EmpCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WTGCode", TxtWTGCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditWTGHdr());

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveWTGDtl(Row));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtWTGCode.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return Sm.IsTxtEmpty(TxtWTGName, "Working time group", false);
        }

        private MySqlCommand EditWTGHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWTGHdr Set ");
            SQL.AppendLine("    ActInd=@ActInd, WTGName=@WTGName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WTGCode=@WTGCode; ");

            SQL.AppendLine("Delete From TblWTGDtl Where WTGCode=@WTGCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WTGCode", TxtWTGCode.Text);
            Sm.CmParam<String>(ref cm, "@WTGName", TxtWTGName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string WTGCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowWTGHdr(WTGCode);
                ShowWTGDtl(WTGCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWTGHdr(string WTGCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WTGCode", WTGCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select WTGCode, WTGName, ActInd, WTCode, WSRCode " +
                    "From TblWTGHdr Where WTGCode=@WTGCode;",
                    new string[] 
                    { "WTGCode", "WTGName", "ActInd", "WTCode", "WSRCode" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtWTGCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtWTGName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y") ? true : false;
                        SetLueWTCode(ref LueWTCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueWTCode, Sm.DrStr(dr, c[3]));
                        SetLueWSRCode(ref LueWSRCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueWSRCode, Sm.DrStr(dr, c[4]));
                    }, true
                );
        }

        private void ShowWTGDtl(string WTGCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WTGCode", WTGCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName ");
            SQL.AppendLine("From TblWTGDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.WTGCode=@WTGCode Order By B.ResignDt, D.DeptName, B.EmpName; ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",

                    //1-4
                    "EmpName", "EmpCodeOld", "PosName", "DeptName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmWTGDlg(this));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmWTGDlg(this));
        }

        #endregion

        #region Additional Method

        private void SetLueWTCode(ref DXE.LookUpEdit Lue, string WTCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select WTCode As Col1, WTName As Col2 From TblWT ");
                if (WTCode.Length == 0)
                    SQL.AppendLine("Where ActInd='Y' Order By WTName; ");
                else
                    SQL.AppendLine("Where WTCode=@WTCode;");

                var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
                if (WTCode.Length != 0) Sm.CmParam<String>(ref cm, "@WTCode", WTCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueWSRCode(ref DXE.LookUpEdit Lue, string WSRCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select WSRCode As Col1, WSRName As Col2 From TblWSRHdr ");
                if (WSRCode.Length == 0)
                    SQL.AppendLine("Where ActInd='Y' Order By WSRName; ");
                else
                    SQL.AppendLine("Where WSRCode=@WSRCode;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (WSRCode.Length != 0) Sm.CmParam<String>(ref cm, "@WSRCode", WSRCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedEmpCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtWTGCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWTGCode);
        }

        private void TxtWTGName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWTGName);
        }

        private void LueWTCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWTCode, new Sm.RefreshLue2(SetLueWTCode), string.Empty);
        }

        private void LueWSRCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWSRCode, new Sm.RefreshLue2(SetLueWSRCode), string.Empty);
        }

        #endregion

        #region Button Event

        private void BtnWTCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueWTCode, "Working time"))
            {
                var f = new FrmWT(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mWTCode = Sm.GetLue(LueWTCode);
                f.ShowDialog();
            }
        }

        private void BtnWSRCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueWSRCode, "Working schedule rule"))
            {
                var f = new FrmWSR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mWSRCode = Sm.GetLue(LueWSRCode);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion
    }
}
