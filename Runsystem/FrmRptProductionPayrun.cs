﻿#region Update
/*
    17/11/2017 [TKG] tambah grade level
    07/11/2018 [TKG] bug saat filter work center, work center yg kosong masih keluar.
*/ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProductionPayrun : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private decimal mDirectLaborDailyWorkHour = 0m;
        private string
            mEmpSystemTypeForProductionPayrun = string.Empty,
            mNeglectLeaveCode = string.Empty,
            mCoordinatorLeaveCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProductionPayrun(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                GetParameter();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -14);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mDirectLaborDailyWorkHour = Sm.GetParameterDec("DirectLaborDailyWorkHour");
            mEmpSystemTypeForProductionPayrun = Sm.GetParameter("EmpSystemTypeForProductionPayrun");
            mNeglectLeaveCode = Sm.GetParameter("NeglectLeaveCode");
            mCoordinatorLeaveCode = Sm.GetParameter("CoordinatorLeaveCode");
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Production Payrun */ ");

            SQL.AppendLine("Select A.DocDt, A.WorkCenterDocNo, B.DocName, ");
            SQL.AppendLine("A.EmpCode, C.EmpName, C.EmpCodeOld, D.PosName, E.DeptName, ");
            SQL.AppendLine("F.ProductionShiftName, ");
            SQL.AppendLine("Sum(A.Wages) As Wages, Sum(A.Penalty) As Penalty, Sum(A.PenaltyLeave) As PenaltyLeave, Sum(A.MinWagesInsentif) As MinWagesInsentif, Sum(A.Insentif) As Insentif, Sum(A.Incentive) As Incentive, ");
            SQL.AppendLine("Sum(A.EmpInsentif) As EmpInsentif, Sum(EmpPenalty) As EmpPenalty ");
            SQL.AppendLine("From ( ");

            SQL.AppendLine("    Select T1.DocDt, T1.WorkCenterDocNo, T2.EmpCode, T2.Wages2 As Wages, 0 As Penalty, 0 As PenaltyLeave, 0 As MinWagesInsentif, 0 As Insentif, 0 As Incentive, ");
            SQL.AppendLine("    0 As EmpInsentif, 0 As EmpPenalty ");
	        SQL.AppendLine("    From TblPWGHdr T1 ");
	        SQL.AppendLine("    Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo And T2.CoordinatorInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");

	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocDt, T1.WorkCenterDocNo, T2.EmpCode, T2.Wages2 As Wages, 0 As Penalty, 0 As PenaltyLeave, 0 As MinWagesInsentif, 0 As Insentif, 0 As Incentive, ");
            SQL.AppendLine("    0 As EmpInsentif, 0 As EmpPenalty ");
            SQL.AppendLine("    From TblPWGHdr T1 ");
            SQL.AppendLine("    Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo And T2.CoordinatorInd='Y' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocDt, T1.WorkCenterDocNo, T2.EmpCode, 0 As Wages, T2.Penalty As Penalty, 0 As PenaltyLeave, 0 As MinWagesInsentif, 0 As Insentif, 0 As Incentive, ");
            SQL.AppendLine("    0 As EmpInsentif, 0 As EmpPenalty ");
            SQL.AppendLine("    From TblPNTHdr T1 ");
            SQL.AppendLine("    Inner Join TblPNTDtl4 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocDt, T1.WorkCenterDocNo, T2.EmpCode, 0 As Wages, T2.Penalty As Penalty, 0 As PenaltyLeave, 0 As MinWagesInsentif, 0 As Insentif, 0 As Incentive, ");
            SQL.AppendLine("    0 As EmpInsentif, 0 As EmpPenalty ");
            SQL.AppendLine("    From TblPNT2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocDt, T1.WorkCenterDocNo, T2.EmpCode, 0 As Wages, 0 As Penalty, ");
            SQL.AppendLine("    If(@DirectLaborDailyWorkHour=0, 0, ((T3.DurationHour/@DirectLaborDailyWorkHour)*T2.Wages2)) As PenaltyLeave, 0 As MinWagesInsentif, 0 As Insentif, 0 As Incentive, ");
            SQL.AppendLine("    0 As EmpInsentif, 0 As EmpPenalty ");
            SQL.AppendLine("    From TblPWGHdr T1 ");
            SQL.AppendLine("    Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo And T2.CoordinatorInd='N' ");
            SQL.AppendLine("    Inner Join (");
            SQL.AppendLine("        Select EmpCode, LeaveDt, Sum(DurationHour) As DurationHour ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select X1.EmpCode, X2.LeaveDt, ");
            SQL.AppendLine("            If(X1.DurationHour=0 Or X1.DurationHour>@DirectLaborDailyWorkHour, ");
            SQL.AppendLine("            @DirectLaborDailyWorkHour, X1.DurationHour) As DurationHour ");
            SQL.AppendLine("            From TblEmpLeaveHdr X1 ");
            SQL.AppendLine("            Inner Join TblEmpLeaveDtl X2 ");
            SQL.AppendLine("                On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("                And X2.LeaveDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            Inner Join TblLeave X3 On X1.LeaveCode = X3.LeaveCode And X3.PaidInd = 'N' ");
            SQL.AppendLine("            Where X1.CancelInd='N' And IfNull(X1.Status, 'O')='A' ");
            SQL.AppendLine("        ) X Group By X.EmpCode, X.LeaveDt ");
            SQL.AppendLine("    ) T3 On T1.DocDt=T3.LeaveDt And T2.EmpCode=T3.EmpCode ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocDt, T1.WorkCenterDocNo, T2.EmpCode, 0 As Wages, 0 As Penalty, ");
            SQL.AppendLine("    If(@DirectLaborDailyWorkHour=0, 0, ((T3.DurationHour/@DirectLaborDailyWorkHour)*T2.Wages2)) As PenaltyLeave, 0 As MinWagesInsentif, 0 As Insentif,  0 As Incentive, ");
            SQL.AppendLine("    0 As EmpInsentif, 0 As EmpPenalty ");
            SQL.AppendLine("    From TblPWGHdr T1 ");
            SQL.AppendLine("    Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo And T2.CoordinatorInd='Y' ");
            SQL.AppendLine("    Inner Join (");
            SQL.AppendLine("        Select EmpCode, LeaveDt, Sum(DurationHour) As DurationHour ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select X1.EmpCode, X2.LeaveDt, ");
            SQL.AppendLine("            If(X1.DurationHour=0 Or X1.DurationHour>@DirectLaborDailyWorkHour, ");
            SQL.AppendLine("            @DirectLaborDailyWorkHour, X1.DurationHour) As DurationHour ");
            SQL.AppendLine("            From TblEmpLeaveHdr X1 ");
            SQL.AppendLine("            Inner Join TblEmpLeaveDtl X2 ");
            SQL.AppendLine("                On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("                And X2.LeaveDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            Inner Join TblLeave X3 On X1.LeaveCode = X3.LeaveCode And X3.PaidInd = 'N' ");
            SQL.AppendLine("            Where X1.CancelInd='N' And IfNull(X1.Status, 'O')='A' ");
            SQL.AppendLine("        ) X Group By X.EmpCode, X.LeaveDt ");
            SQL.AppendLine("    ) T3 On T1.DocDt=T3.LeaveDt And T2.EmpCode=T3.EmpCode ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocDt, T1.WorkCenterDocNo, T3.EmpCode, ");
            SQL.AppendLine("    0 As wages, 0 As penalty, 0 As Penaltyleave, Sum(T3.InsentifAmt) As MinWagesInsentif, 0 As Insentif, 0 As Incentive, ");
            SQL.AppendLine("    0 As EmpInsentif, 0 As EmpPenalty ");
            SQL.AppendLine("    From TblPWGHdr T1");
            SQL.AppendLine("    Inner join TblInsentifHdr T2 On T1.DocNo = T2.PWGDocNo And T2.CancelInd ='N' ");
            SQL.AppendLine("    Inner Join TblInsentifDtl T3 On T2.DocNo = T3.DocNo ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            SQL.AppendLine("    Group By T1.DocDt, T1.WorkCenterDocNo, T3.EmpCode");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.EndDt As DocDt, Null As WorkCenterDocNo, T2.EmpCode, ");
            SQL.AppendLine("    0 As wages, 0 As penalty, 0 As Penaltyleave, 0 As MinWagesInsentif, Sum(T2.InsentifAmt) As Insentif, 0 As Incentive, ");
            SQL.AppendLine("    0 As EmpInsentif, 0 As EmpPenalty ");
            SQL.AppendLine("    From TblInsentif2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblInsentif2Dtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.EndDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    Group BY T1.EndDt, T2.EmpCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocDt, Null As WorkCenterDocNo, T2.EmpCode, ");
            SQL.AppendLine("    0 As Wages, 0 As penalty, 0 As Penaltyleave, 0 As MinWagesInsentif, 0 As Insentif, Sum(T2.Incentive) As Incentive, ");
            SQL.AppendLine("    0 As EmpInsentif, 0 As EmpPenalty ");
            SQL.AppendLine("    From TblIncentiveHdr T1 ");
            SQL.AppendLine("    Inner Join TblIncentiveDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    Group BY T1.DocDt, T2.EmpCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocDt, Null As WorkCenterDocNo, T2.EmpCode, ");
            SQL.AppendLine("    0 As Wages, 0 As Penalty, 0 As Penaltyleave, 0 As MinWagesInsentif, 0 As Insentif, 0 As Incentive, ");
            SQL.AppendLine("    Sum(T1.AmtInspnt) As EmpInsentif, 0 As EmpPenalty ");
            SQL.AppendLine("    From TblEmpInsPntHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpInsPntDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblInsPnt T3 On T1.InspntCode=T3.InspntCode ");
            SQL.AppendLine("    Inner Join TblInsPntCategory T4 On T3.InspntCtCode=T4.InspntCtCode And T4.InsPntType='01' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.DocDt, T2.EmpCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocDt, Null As WorkCenterDocNo, T2.EmpCode, ");
            SQL.AppendLine("    0 As Wages, 0 As Penalty, 0 As Penaltyleave, 0 As MinWagesInsentif, 0 As Insentif, 0 As Incentive, ");
            SQL.AppendLine("    0 As EmpInsentif, Sum(T1.AmtInspnt) As EmpPenalty ");
            SQL.AppendLine("    From TblEmpInsPntHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpInsPntDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblInsPnt T3 On T1.InspntCode=T3.InspntCode ");
            SQL.AppendLine("    Inner Join TblInsPntCategory T4 On T3.InspntCtCode=T4.InspntCtCode And T4.InsPntType='02' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.DocDt, T2.EmpCode ");

            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblWorkCenterHdr B On A.WorkCenterDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode  ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocDt, T2.BomCode As EmpCode, group_concat(Distinct T3.ProductionShiftName separator ',') As ProductionShiftName ");
            SQL.AppendLine("    From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("    Inner Join TblShopFloorControl2Dtl T2 On T1.DocNo=T2.DocNo And T2.BomType='3' ");
            SQL.AppendLine("    Inner Join TblProductionShift T3 On T1.ProductionShiftCode=T3.ProductionShiftCode ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.DocDt, T2.BomCode ");
            SQL.AppendLine(") F On A.DocDt=F.DocDt And A.EmpCode=F.EmpCode ");
       
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                         //0
                        "No.",

                        //1-5
                        "Date",
                        "Employee's" + Environment.NewLine + "Code",
                        "Old Code",
                        "Employee's Name",
                        "Department", 

                        //6-10
                        "Position",
                        "Work Center",
                        "Shift",
                        "Grade",
                        "Wages",

                        //11-15
                        "Leave Name",
                        "Leave",
                        "Penalty",
                        "Incentive" + Environment.NewLine + "(Min.Wages)",
                        "Incentive"+ Environment.NewLine + "(Coordinator)",
                        
                        //16-19
                        "Incentive"+ Environment.NewLine + "(Daily)",
                        "Incentive"+ Environment.NewLine + "(Standard)",
                        "Penalty"+ Environment.NewLine + "(Standard)",
                        "Balance"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 100, 80, 250, 200, 
                        
                        //6-10
                        180, 180, 100, 180, 120,  
                        
                        //11-15
                        180, 120, 120, 120, 120, 

                        //16-19
                        120, 120, 120, 150
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 13, 14, 15, 16, 17, 18, 19 }, 2);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 8 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                bool IsExisted = false;
                var l1 = Process1();
                var l2 = Process2();

                l2.ForEach(
                    x2 =>
                    {
                        IsExisted = false;
                        foreach (var x1 in l1.Where(x1 => x1.DocDt == x2.LeaveDt && x1.EmpCode == x2.EmpCode))
                        {
                            x1.LeaveCode = x2.LeaveCode;
                            x1.LeaveName = x2.LeaveName;
                            IsExisted = true;
                        }
                        if (IsExisted == false)
                        {
                            l1.Add(new RptProductionPayrun()
                            {
                                DocDt = x2.LeaveDt,
                                DocName = string.Empty,
                                ProductionShiftName = string.Empty,
                                EmpCode = x2.EmpCode,
                                EmpName = x2.EmpName,
                                EmpCodeOld = x2.EmpCodeOld,
                                PosName = x2.PosName,
                                DeptName = x2.DeptName,
                                Wages = 0m,
                                Penalty = 0m,
                                LeaveCode = x2.LeaveCode,
                                LeaveName = x2.LeaveName,
                                PenaltyLeave = 0m,
                                MinWagesInsentif = 0m,
                                Insentif = 0m,
                                Incentive = 0m,
                                EmpInsentif = 0m,
                                EmpPenalty = 0m,
                                Balance = 0m
                            });
                        }
                    }
                );

                if (l1.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                {
                    l1.Sort(
                        delegate(RptProductionPayrun l1a, RptProductionPayrun l1b)
                        {
                            int f = l1a.DocDt.CompareTo(l1b.DocDt);
                            return f != 0 ? f : l1a.EmpCode.CompareTo(l1b.EmpCode);
                        });

                    var l3 = new List<EmployeePPS>();
                    Process3(ref l1, ref l3);
                    Process4(ref l1, ref l3);
                    int Row = 0;
                    Grd1.BeginUpdate();
                    Grd1.Rows.Count = 0;
                    bool IsProcess = false, IsExist = false;
                    foreach (RptProductionPayrun i in l1)
                    {
                        IsProcess = false;
                        if (ChkWorkCenterDocNo.Checked)
                        {
                            if (i.DocName.ToUpper().Contains(@TxtWorkCenterDocNo.Text.ToUpper()))
                                IsProcess = true;
                        }
                        else
                            IsProcess = true;
                        if (IsProcess)
                        {
                            Grd1.Rows.Add();
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd1.Cells[Row, 1].Value = Sm.ConvertDate(i.DocDt);
                            Grd1.Cells[Row, 2].Value = i.EmpCode;
                            Grd1.Cells[Row, 3].Value = i.EmpCodeOld;
                            Grd1.Cells[Row, 4].Value = i.EmpName;
                            Grd1.Cells[Row, 5].Value = i.DeptName;
                            Grd1.Cells[Row, 6].Value = i.PosName;
                            Grd1.Cells[Row, 7].Value = i.DocName;
                            Grd1.Cells[Row, 8].Value = i.ProductionShiftName;
                            Grd1.Cells[Row, 9].Value = i.GrdLvlName;
                            Grd1.Cells[Row, 10].Value = i.Wages;
                            Grd1.Cells[Row, 11].Value = i.LeaveName;
                            Grd1.Cells[Row, 12].Value = i.PenaltyLeave;
                            Grd1.Cells[Row, 13].Value = i.Penalty;
                            Grd1.Cells[Row, 14].Value = i.MinWagesInsentif;
                            Grd1.Cells[Row, 15].Value = i.Insentif;
                            Grd1.Cells[Row, 16].Value = i.Incentive;
                            Grd1.Cells[Row, 17].Value = i.EmpInsentif;
                            Grd1.Cells[Row, 18].Value = i.EmpPenalty;
                            Grd1.Cells[Row, 19].Value = i.Balance;
                            if (Sm.CompareStr(i.LeaveCode, mNeglectLeaveCode) || Sm.CompareStr(i.LeaveCode, mCoordinatorLeaveCode))
                                Grd1.Cells[Row, 19].Value = i.Balance - i.PenaltyLeave;
                            Row++;
                            IsExist = true;
                        }
                    }
                    if (IsExist)
                    {
                        iGSubtotalManager.BackColor = Color.LightSalmon;
                        iGSubtotalManager.ShowSubtotalsInCells = true;
                        iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 12, 13, 14, 15, 16, 17, 18, 19 });
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    Grd1.EndUpdate();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Data

        private List<RptProductionPayrun> Process1()
        {
            string Filter = string.Empty;
                       
            var l = new List<RptProductionPayrun>();
            var cm = new MySqlCommand();

            Sm.CmParam<Decimal>(ref cm, "@DirectLaborDailyWorkHour", mDirectLaborDailyWorkHour);
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            if (ChkWorkCenterDocNo.Checked)
            {
                Filter = " Where IfNull(B.DocName, '')<>'' ";
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterDocNo.Text, "B.DocName", false);
            }            
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName", "C.EmpCodeOld" });

            Sm.ShowDataInCtrl(
                ref cm,
                GetSQL() + Filter +
                " Group By " +
                " A.DocDt, A.WorkCenterDocNo, B.DocName, " +
                " A.EmpCode, C.EmpName, C.EmpCodeOld, D.PosName, E.DeptName, F.ProductionShiftName " +
                " Order By A.DocDt, B.DocName, C.EmpName, A.EmpCode;",
                new string[] 
                {
                    //0
                    "DocDt",  

                    //1-5
                    "DocName", "ProductionShiftName", "EmpCode", "EmpName", "EmpCodeOld", 
                    
                    //6-10
                    "PosName", "DeptName", "Wages", "Penalty", "PenaltyLeave", 
                    
                    //11-15
                    "MinWagesInsentif", "Insentif", "Incentive", "EmpInsentif", "EmpPenalty"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(new RptProductionPayrun()
                   {
                       DocDt = Sm.DrStr(dr, c[0]),
                       DocName = Sm.DrStr(dr, c[1]),
                       ProductionShiftName = Sm.DrStr(dr, c[2]),
                       EmpCode = Sm.DrStr(dr, c[3]),
                       EmpName = Sm.DrStr(dr, c[4]),
                       EmpCodeOld = Sm.DrStr(dr, c[5]),
                       PosName = Sm.DrStr(dr, c[6]),
                       DeptName = Sm.DrStr(dr, c[7]),
                       Wages = Sm.DrDec(dr, c[8]),
                       Penalty = Sm.DrDec(dr, c[9]),
                       PenaltyLeave = Sm.DrDec(dr, c[10]),
                       MinWagesInsentif = Sm.DrDec(dr, c[11]),
                       Insentif = Sm.DrDec(dr, c[12]),
                       Incentive = Sm.DrDec(dr, c[13]),
                       EmpInsentif = Sm.DrDec(dr, c[14]),
                       EmpPenalty = Sm.DrDec(dr, c[15]),
                       Balance = 0m
                   });
               }, false
            );
            l.ForEach(x => x.Balance = x.Wages - x.Penalty + x.MinWagesInsentif + x.Insentif + x.Incentive + x.EmpInsentif - x.EmpPenalty);
            return l;
        }

        private List<RptProductionPayrun2> Process2()
        {
            string Filter = " And 0=0 ";

            var l = new List<RptProductionPayrun2>();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@EmpSystemTypeForProductionPayrun", mEmpSystemTypeForProductionPayrun);
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "D.EmpCode", "D.EmpName", "D.EmpCodeOld" });

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.LeaveDt, A.EmpCode, D.EmpName, D.EmpCodeOld, E.PosName, F.DeptName, ");
            SQL.AppendLine("A.LeaveCode, C.LeaveName ");
            SQL.AppendLine("From TblEmpLeaveHdr A ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.LeaveDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblLeave C On A.LeaveCode=C.LeaveCode ");
            SQL.AppendLine("Inner Join TblEmployee D On A.EmpCode=D.EmpCode And IfNull(D.SystemType, '')=@EmpSystemTypeForProductionPayrun " + Filter);
            SQL.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            SQL.AppendLine("Left Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='A' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select C.LeaveDt, B.EmpCode, E.EmpName, E.EmpCodeOld, F.PosName, G.DeptName, ");
            SQL.AppendLine("A.LeaveCode, D.LeaveName ");
            SQL.AppendLine("From TblEmpLeave2Hdr A ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl2 C ");
            SQL.AppendLine("    On A.DocNo=C.DocNo ");
            SQL.AppendLine("    And C.LeaveDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblLeave D On A.LeaveCode=D.LeaveCode ");
            SQL.AppendLine("Inner Join TblEmployee E On B.EmpCode=E.EmpCode And IfNull(E.SystemType, '')=@EmpSystemTypeForProductionPayrun " + Filter.Replace("D.", "E."));
            SQL.AppendLine("Left Join TblPosition F On E.PosCode=F.PosCode ");
            SQL.AppendLine("Left Join TblDepartment G On E.DeptCode=G.DeptCode ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='A' ");
            SQL.AppendLine("Order By LeaveDt, EmpCode;");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "LeaveDt",  

                    //1-5
                    "EmpCode", "EmpName", "EmpCodeOld", "PosName", "DeptName", 
                    
                    //6-7
                    "LeaveCode", "LeaveName"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(new RptProductionPayrun2()
                   {
                       LeaveDt = Sm.DrStr(dr, c[0]),
                       EmpCode = Sm.DrStr(dr, c[1]),
                       EmpName = Sm.DrStr(dr, c[2]),
                       EmpCodeOld = Sm.DrStr(dr, c[3]),
                       PosName = Sm.DrStr(dr, c[4]),
                       DeptName = Sm.DrStr(dr, c[5]),
                       LeaveCode = Sm.DrStr(dr, c[6]),
                       LeaveName = Sm.DrStr(dr, c[7])
                   });
               }, false
            );
            return l;
        }

        private void Process3(ref List<RptProductionPayrun> l1, ref List<EmployeePPS> l2)
        { 
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;

            for (int i = 0; i < l1.Select(s => s.EmpCode).Distinct().ToList().Count; i++)
            {    
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), l1[i].EmpCode);
            }

            if (Filter.Length > 0) Filter = " Where (" + Filter + ") ";

            SQL.AppendLine("Select A.EmpCode, A.StartDt, IfNull(A.EndDt, '21991231') As EndDt, B.GrdLvlName ");
            SQL.AppendLine("From TblEmployeePPS A ");
            SQL.AppendLine("Left Join TblGradeLevelHdr B On A.GrdLvlCode=B.GrdLvlCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(" Order By A.EmpCode, A.StartDt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-3
                    "StartDt", "EndDt", "GrdLvlName"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new EmployeePPS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            GrdLvlName = Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<RptProductionPayrun> l1, ref List<EmployeePPS> l2)
        {
            var EmpCode = string.Empty;
            for (int i = 0; i < l1.Count; i++)
            {
                EmpCode = l1[i].EmpCode;
                foreach(var x in l2.Where(w=>Sm.CompareStr(EmpCode, w.EmpCode)).OrderBy(o=>o.StartDt))
                {
                    if (Sm.CompareDtTm(l1[i].DocDt, x.StartDt) >= 0 &&
                        Sm.CompareDtTm(l1[i].DocDt, x.EndDt) <= 0)
                    {
                        l1[i].GrdLvlName = x.GrdLvlName;
                        break;
                    }
                }   
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenterDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work Center");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion

        #endregion

        #region Class

        private class RptProductionPayrun
        {
            public string DocDt { get; set; }
            public string WorkCenterDocNo { get; set; }
            public string DocName { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }
            public string ProductionShiftName { get; set; }
            public string GrdLvlName { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveName { get; set; }
            public decimal Wages { get; set; }
            public decimal Penalty { get; set; }
            public decimal PenaltyLeave { get; set; }
            public decimal MinWagesInsentif { get; set; }
            public decimal Insentif { get; set; }
            public decimal Incentive { get; set; }
            public decimal EmpInsentif { get; set; }
            public decimal EmpPenalty { get; set; }
            public decimal Balance { get; set; }
        }

        private class RptProductionPayrun2
        {
            public string LeaveDt { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveName { get; set; }
        }

        private class EmployeePPS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string GrdLvlName { get; set; }
        }

        #endregion
    }
}
