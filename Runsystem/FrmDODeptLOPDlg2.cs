﻿#region Update
/*
    20/05/2019 [TKG] parameter IsSystemUseCostCenter diganti dengan IsDODeptUseCostCenter
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDODeptLOPDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDODeptLOP mFrmParent;
        private string mSQL = string.Empty, mCCCode = string.Empty;
        private int mCurRow;
        private bool mIsReCompute = false;

        #endregion

        #region Constructor

        public FrmDODeptLOPDlg2(FrmDODeptLOP FrmParent, int CurRow, string CCCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = CurRow;
            mCCCode = CCCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCode, A.Assetname, A.DisplayName, B.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.ActiveInd = 'Y' ");
            if (mFrmParent.mIsDODeptUseCostCenter)
            {
                SQL.AppendLine("And (A.CCCode Is Null Or ( ");
                SQL.AppendLine("A.CCCode Is Not Null ");
                SQL.AppendLine("And A.AssetCode In (Select AssetCode From TblAssetSummary Where CCCode=@CCCode) ");
                SQL.AppendLine(")) ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Asset Code",
                        "",
                        "Asset Name",
                        "Item's Code",
                        "",

                        //6-9
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's Name",
                        "Display Name",
                        "Foreign Name"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        100, 20, 300, 100, 20, 
                        
                        //6-9
                        130, 300, 300, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 5 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 6, 7, 8, 9 });
            Grd1.Cols[8].Move(4);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 6 }, false);
            if(!mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 9 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 6 }, !ChkHideInfoInGrd.Checked);
            
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "A.AssetCode", "A.AssetName" });

                mIsReCompute = false;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.AssetName;",
                        new string[] 
                        { 
                            //0
                            "AssetCode",
 
                            //1-5
                            "AssetName", "ItCode", "ItCodeInternal", "ItName", "DisplayName",

                            //6
                            "ForeignName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                mIsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 22, Grd1, Row, 1);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 23, Grd1, Row, 3);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 32, Grd1, Row, 8);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (mIsReCompute) Sm.GrdExpand(Grd1);
        }

        #endregion

        #endregion

        #region Event

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        #endregion
    }
}
