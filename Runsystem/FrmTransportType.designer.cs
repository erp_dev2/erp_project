﻿namespace RunSystem
{
    partial class FrmTransportType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtTTName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtTTCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtLoadingCost = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtUnloadingCost = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtTonase = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLoadingCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUnloadingCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTonase.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtTonase);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtUnloadingCost);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtLoadingCost);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtTTName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtTTCode);
            this.panel2.Controls.Add(this.label1);
            // 
            // TxtTTName
            // 
            this.TxtTTName.EnterMoveNextControl = true;
            this.TxtTTName.Location = new System.Drawing.Point(143, 61);
            this.TxtTTName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTTName.Name = "TxtTTName";
            this.TxtTTName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTTName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTTName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTTName.Properties.Appearance.Options.UseFont = true;
            this.TxtTTName.Properties.MaxLength = 40;
            this.TxtTTName.Size = new System.Drawing.Size(319, 20);
            this.TxtTTName.TabIndex = 16;
            this.TxtTTName.Validated += new System.EventHandler(this.TxtTTName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(95, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTTCode
            // 
            this.TxtTTCode.EnterMoveNextControl = true;
            this.TxtTTCode.Location = new System.Drawing.Point(143, 38);
            this.TxtTTCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTTCode.Name = "TxtTTCode";
            this.TxtTTCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTTCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTTCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTTCode.Properties.Appearance.Options.UseFont = true;
            this.TxtTTCode.Properties.MaxLength = 16;
            this.TxtTTCode.Size = new System.Drawing.Size(76, 20);
            this.TxtTTCode.TabIndex = 14;
            this.TxtTTCode.Validated += new System.EventHandler(this.TxtTTCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(98, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLoadingCost
            // 
            this.TxtLoadingCost.EnterMoveNextControl = true;
            this.TxtLoadingCost.Location = new System.Drawing.Point(143, 84);
            this.TxtLoadingCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLoadingCost.Name = "TxtLoadingCost";
            this.TxtLoadingCost.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLoadingCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLoadingCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLoadingCost.Properties.Appearance.Options.UseFont = true;
            this.TxtLoadingCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLoadingCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLoadingCost.Size = new System.Drawing.Size(166, 20);
            this.TxtLoadingCost.TabIndex = 42;
            this.TxtLoadingCost.Validated += new System.EventHandler(this.TxtLoadingCost_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(56, 87);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 14);
            this.label15.TabIndex = 41;
            this.label15.Text = "Loading Cost";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUnloadingCost
            // 
            this.TxtUnloadingCost.EnterMoveNextControl = true;
            this.TxtUnloadingCost.Location = new System.Drawing.Point(143, 107);
            this.TxtUnloadingCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUnloadingCost.Name = "TxtUnloadingCost";
            this.TxtUnloadingCost.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUnloadingCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUnloadingCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUnloadingCost.Properties.Appearance.Options.UseFont = true;
            this.TxtUnloadingCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUnloadingCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUnloadingCost.Size = new System.Drawing.Size(166, 20);
            this.TxtUnloadingCost.TabIndex = 44;
            this.TxtUnloadingCost.Validated += new System.EventHandler(this.TxtUnloadingCost_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(45, 109);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 14);
            this.label3.TabIndex = 43;
            this.label3.Text = "Unloading Cost";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTonase
            // 
            this.TxtTonase.EnterMoveNextControl = true;
            this.TxtTonase.Location = new System.Drawing.Point(143, 130);
            this.TxtTonase.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTonase.Name = "TxtTonase";
            this.TxtTonase.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTonase.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTonase.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTonase.Properties.Appearance.Options.UseFont = true;
            this.TxtTonase.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTonase.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTonase.Size = new System.Drawing.Size(138, 20);
            this.TxtTonase.TabIndex = 46;
            this.TxtTonase.Validated += new System.EventHandler(this.TxtTonase_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(86, 133);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 14);
            this.label4.TabIndex = 45;
            this.label4.Text = "Tonase";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmTransportType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmTransportType";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLoadingCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUnloadingCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTonase.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtTTName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtTTCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtUnloadingCost;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtLoadingCost;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtTonase;
        private System.Windows.Forms.Label label4;
    }
}