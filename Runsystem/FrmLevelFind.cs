﻿#region Update
/*
    10/09/2017 [TKG] tambah level
    23/10/2019 [HAR/SIER] tambah OT request Indicator
    21/03/2023 [WED/PHT] tambah kolom Position Status
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmLevelFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmLevel mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmLevelFind(FrmLevel FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Select A.LevelCode, A.LevelName, C.ADName, B.Amt, A.AllowOTRequestInd, A.Remark, ");
            if (mFrmParent.mIsLevelUsePositionStatus) SQL.AppendLine("D.PositionStatusName, ");
            else SQL.AppendLine("Null As PositionStatusName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt");
            SQL.AppendLine("From TblLevelHdr A ");
            SQL.AppendLine("Left Join TblLevelDtl B On A.LevelCode=B.LevelCode ");
            SQL.AppendLine("Left Join TblAllowanceDeduction C On B.ADCode=C.ADCode ");
            if (mFrmParent.mIsLevelUsePositionStatus) SQL.AppendLine("Left Join TblPositionStatus D On A.PositionStatusCode = D.PositionStatusCode ");
            
            mSQL = SQL.ToString();
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5 
                        "Code",
                        "Name",
                        "Position Status",
                        "Allowance/Deduction",
                        "Amount",

                        //6-10
                        "OT Req Ind",
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        
                        //11-13
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 100, 180, 120, 
                        
                        //6-10
                        80, 200, 100, 100, 100, 
                        
                        //11-13
                        100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 6 });
            if (!mFrmParent.mIsLevelUsePositionStatus) Sm.GrdColInvisible(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);  
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, TxtLevelCode.Text, new string[] { "A.LevelCode", "A.LevelName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.LevelName;",
                        new string[]
                        {
                              //0
                             "LevelCode",
                             
                             //1-5
                             "LevelName",
                             "PositionStatusName",
                             "ADName",
                             "Amt",
                             "AllowOTRequestInd",

                             //6-10
                             "Remark",
                             "CreateBy",
                             "CreateDt",
                             "LastUpBy",
                             "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtLevelCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLevelCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Level");
        }
        
        #endregion

        #endregion
       
    }
}
