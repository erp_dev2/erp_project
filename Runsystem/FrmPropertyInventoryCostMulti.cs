﻿#region Update
/*
    20/04/2023 [SET/BBT] Menu baru
    03/05/2023 [SET/BBT] Penyesuaian pengaruh transaksi ke Master Property Inventory
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.Xml;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryCostMulti : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        private string mReconditionDate = string.Empty;
        private bool
            mIsAutoJournalActived = false
            ;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mDeprMethod = string.Empty
            ;
        internal string 
            mPropertyCategoryCode = string.Empty,
            mSiteCode = string.Empty,
            mCCCode = string.Empty;
        private decimal mAssetAnnualPercentageRate = 0m;
        internal bool mIsFilterByDept = false;
        internal FrmPropertyInventoryCostMultiFind FrmFind;
        iGCell fCell;
        bool fAccept;
        private iGCopyPasteManager fCopyPasteManager;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmPropertyInventoryCostMulti(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Property Inventory Cost Component";

            try
            {
                ExecQuery();
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Tc1.SelectedTabPage = Tp1;
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "Document DOD#",

                        //1-5
                        "DOD DNo",
                        "",
                        "Item's Code",
                        "Item's Name",
                        "UoM",

                        //6-10
                        "Unit Price",
                        "Quantity",
                        "Amount",
                        "Outstanding Qty",
                        "CurCode",

                        //11-13
                        "Document Recv#",
                        "Remark",
                        "Cost Center",
                    },
                    new int[]
                    {
                        //0
                        150,
 
                        //1-5
                        0, 20, 120, 200, 100, 
                        
                        //6-10
                        120, 100, 130, 0, 0,

                        //11
                        150, 200, 200,
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Grd1.Cols[11].Move(1);
            //Sm.GrdColInvisible(Grd1, new int[] { 9, 10 });
            #endregion

            #region Grd2
            Grd2.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "No",

                    //1-4
                    "User",
                    "Status",
                    "Date",
                    "Remark"
                },
                new int[] { 50, 150, 100, 80, 200 }
            );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            #endregion

            #region Grd3
            Grd3.Cols.Count = 9;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "Document#",

                        //1-5
                        "",
                        "Date",
                        "DeptCode",
                        "Department",
                        "Person in Charge",

                        //6-8
                        "Currency",
                        "Amount (Cost)",
                        "Remark",
                    },
                    new int[]
                    {
                        //0
                        150,
 
                        //1-5
                        20, 100, 0, 200, 150, 
                        
                        //6-9
                        100, 150, 350
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 7 }, 0);
            Sm.GrdColButton(Grd3, new int[] { 1 });
            Sm.GrdFormatDate(Grd3, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            //Sm.GrdColInvisible(Grd3, new int[] { 9 });
            #endregion
            
            #region Grd4
            Grd4.Cols.Count = 14;
            Grd4.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Property Code",
                        "Property Name",
                        "Property Category",
                        "Cost Center",
                        "Site",

                        //6-10
                        "Remaining Stock Quantity",
                        "UoM",
                        "Property Inventory Value"+Environment.NewLine+"Before Addition",
                        "Cost Component Value",
                        "Property Inventory Value"+Environment.NewLine+"After Addition",

                        //11-13
                        "PropertyCtCode",
                        "SiteCode",
                        "CCCode",

                    },
                    new int[]
                    {
                        //0
                        20,
 
                        //1-5
                        200, 200, 200, 200, 200, 
                        
                        //6-10
                        250, 100, 250, 250, 250,

                        //11
                        0, 0, 0
                    }
                );
            Sm.GrdFormatDec(Grd4, new int[] { 6, 8, 9, 10 }, 0);
            Sm.GrdColButton(Grd4, new int[] { 0 });
            Sm.GrdFormatDate(Grd4, new int[] {  });
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            //Sm.GrdColInvisible(Grd3, new int[] { 9 });
            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, MeeCostComponenReason, TxtPropertyInventoryAfter, MeeCancelReason, TxtFile2,
                        TxtFile, TxtFile3
                    }, true);
                    ChkCancelInd.Enabled = false;
                    BtnFile.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnFile3.Enabled = false;
                    if(TxtFile.Text.Length == 0)
                        BtnDownload.Enabled = false;
                    if (TxtFile.Text.Length > 0)
                        BtnDownload.Enabled = true;
                    if(TxtFile2.Text.Length == 0)
                        BtnDownload2.Enabled = false;
                    if (TxtFile2.Text.Length > 0)
                        BtnDownload2.Enabled = true;
                    if(TxtFile3.Text.Length == 0)
                        BtnDownload3.Enabled = false;
                    if (TxtFile3.Text.Length > 0)
                        BtnDownload3.Enabled = true;
                    ChkFile.Enabled = false;
                    ChkFile2.Enabled = false;
                    ChkFile3.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                       DteDocDt, MeeCostComponenReason
                    }, false);
                    BtnFile.Enabled = true;
                    BtnFile2.Enabled = true;
                    BtnFile3.Enabled = true;
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 7 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason,
                TxtPropertyInventoryBefore, TxtRemainStockQty,
                TxtCostComponentValue, TxtPropertyInventoryAfter, MeeCostComponenReason,
                TxtFile
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtPropertyInventoryBefore, TxtRemainStockQty, 
                TxtCostComponentValue, TxtPropertyInventoryAfter
            }, 0);
            mPropertyCategoryCode = string.Empty;
            ChkCancelInd.Checked = false;
            ChkFile.Checked = false;
            ChkFile2.Checked = false;
            PbUpload.Value = 0;
            PbUpload2.Value = 0;
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7, 8, 9 });
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 7 });
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 6, 8, 9, 10 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPropertyInventoryCostMultiFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                TxtStatus.EditValue = "Outstanding";
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MultiCostComponent", "TblPropertyInventoryCostComponentHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SavePropertyInventoryCostComponentHdr(DocNo));
            if (Grd4.Rows.Count > 1)
                cml.Add(SavePropertyInventoryCostComponentDtl4(DocNo));
            if (Grd1.Rows.Count > 1)
                cml.Add(SavePropertyInventoryCostComponentDtl(DocNo));
            //for (int r = 0; r < Grd3.Rows.Count; r++)
            if (Grd3.Rows.Count > 1)
                cml.Add(SavePropertyInventoryCostComponentDtl2(DocNo));

            if (!IsDocApprovalSettingExists(DocNo))
            {
                if (mIsAutoJournalActived)
                    cml.Add(SaveJournal(DocNo));
                for (int r = 0; r < Grd4.Rows.Count - 1; r++)
                {
                    cml.Add(UpdatePropertyInventoryValue("N", DocNo, Sm.GetGrdStr(Grd4, r, 1)));
                }
            }

            Sm.ExecCommands(cml);
            if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile1(DocNo);
            if (TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);
            if (TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                UploadFile3(DocNo);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(MeeCostComponenReason, "Property Inventory Cost Component Reason", false) ||
            IsGrdEmpty() ||
            IsGrdValueNotValid() ||
            (TxtFile.Text.Length > 0 && IsUploadFileNotValid()) ||
            (TxtFile2.Text.Length > 0 && IsUploadFileNotValid2());
        }

        private bool IsGrdEmpty()
        {
            if (Sm.GetGrdStr(Grd4, 0, 1).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Property Inventory Cost Component");
                return true;
            }
            if (Grd1.Rows.Count == 1 && Sm.GetGrdStr(Grd3, 0, 0).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 DO To Department");
                return true;
            }
            if (Sm.GetGrdStr(Grd1, 0, 0).Length == 0 && Sm.GetGrdStr(Grd3, 0, 0).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Cash Advance Settlement");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var DocDt = Sm.GetDte(DteDocDt);
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 7, true, "Quantity should be bigger than 0.00.")) return true;
                if (Sm.GetGrdDec(Grd1, Row, 7) > Sm.GetGrdDec(Grd1, Row, 9))
                {
                    Sm.StdMsg(mMsgType.Warning,
                    "Quantity should not be bigger than outstanding quantity." + Environment.NewLine + Environment.NewLine +
                    "DOD Document# : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine +
                    "Item's Name   : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Quantity      : " + Sm.GetGrdDec(Grd1, Row, 7) + Environment.NewLine +
                    "Outstanding Quantity : " + Sm.GetGrdStr(Grd1, Row, 9)
                    );
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SavePropertyInventoryCostComponentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPropertyInventoryCostComponentHdr(DocNo, DocDt, Status, ValueBeforeAdd, CostComponentAmt, ValueAfterAdd, CostComponentReason, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @ValueBeforeAdd, @CostComponentAmt, @ValueAfterAdd, @CostComponentReason, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            if (IsDocApprovalSettingExists(DocNo))
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='PropertyInventoryCostComponent' ");
                SQL.AppendLine("And T.DeptCode = @DeptCode ");
                SQL.AppendLine("And T.SiteCode = @SiteCode ");
                SQL.AppendLine("And (T.StartAmt=0.00 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select CostComponentAmt ");
                SQL.AppendLine("    From TblPropertyInventoryCostComponentHdr  ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("), 0.00)) ");
                SQL.AppendLine("; ");
            }
            SQL.AppendLine("Update TblPropertyInventoryCostComponentHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType='PropertyInventoryCostComponent' ");
            SQL.AppendLine("); ");

            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@ValueBeforeAdd", Decimal.Parse(TxtPropertyInventoryBefore.Text));
            Sm.CmParam<Decimal>(ref cm, "@CostComponentAmt", Decimal.Parse(TxtCostComponentValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@ValueAfterAdd", Decimal.Parse(TxtPropertyInventoryAfter.Text));
            Sm.CmParam<String>(ref cm, "@CostComponentReason", MeeCostComponenReason.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", GetDeptCode());
            Sm.CmParam<String>(ref cm, "@SiteCode", GetSiteCode());
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //public static bool IsDataExist(MySqlCommand cm)
        //{
        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        //cm.Prepare();
        //        return !(cm.ExecuteScalar() == null);
        //    }
        //}

        private MySqlCommand UpdatePropertyInventoryValue(string CancelInd, string DocNo, string PropertyCode)
        {
            var SQL = new StringBuilder();

            #region old code. comment by IBL 13/01/2023
            //SQL.AppendLine("UPDATE tblpropertyinventoryhdr A ");
            //SQL.AppendLine("INNER JOIN ( ");
            //SQL.AppendLine("	SELECT A1.PropertyCode, SUM(A1.CostComponentAmt) as PropertyInventoryValue ");
            //SQL.AppendLine("	FROM tblpropertyinventorycostcomponenthdr A1 ");
            //SQL.AppendLine("	WHERE A1.PropertyCode = @PropertyCode AND A1.CancelInd='N' ");
            //SQL.AppendLine(") B ON A.PropertyCode = B.PropertyCode ");
            ////SQL.AppendLine("SET A.PropertyInventoryValue = B.PropertyInventoryValue ");
            //SQL.AppendLine("SET ");
            //SQL.AppendLine("A.PropertyInventoryValue = B.PropertyInventoryValue , ");
            //SQL.AppendLine("A.Uprice = B.PropertyInventoryValue / A.InventoryQty, ");
            //SQL.AppendLine("A.RemStockQty = A.InventoryQty, ");
            //SQL.AppendLine("A.RemStockValue = A.RemStockQty * (B.PropertyInventoryValue / A.InventoryQty) ");
            //SQL.AppendLine("WHERE A.PropertyCode = @PropertyCode ");
            #endregion

            SQL.AppendLine("Update TblPropertyInventoryHdr T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("	Select B.PropertyCode, ");
            if (CancelInd == "N")
            {
                SQL.AppendLine("    IfNull(C.PropertyInventoryValue,0.00) + IfNull(B.CostComponentAmt, 0.00) As NewPropInventoryeVal, ");
                SQL.AppendLine("    (IfNull(C.PropertyInventoryValue,0.00) + IfNull(B.CostComponentAmt, 0.00)) / IfNull(C.InventoryQty, 1.00) As NewUPrice, ");
            }
            else
            {
                SQL.AppendLine("    IfNull(C.PropertyInventoryValue,0.00) - IfNull(B.CostComponentAmt, 0.00) As NewPropInventoryeVal, ");
                SQL.AppendLine("    (IfNull(C.PropertyInventoryValue,0.00) - IfNull(B.CostComponentAmt, 0.00)) / IfNull(C.InventoryQty, 1.00) As NewUPrice, ");
            }
            SQL.AppendLine("	IfNull(C.RemStockQty, 0.00) As RemStockQty ");
            SQL.AppendLine("	From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("	INNER JOIN tblpropertyinventorycostcomponentdtl4 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("	Inner Join TblPropertyInventoryHdr C On B.PropertyCode = C.PropertyCode ");
            SQL.AppendLine("		And A.DocNo = @DocNo ");
            SQL.AppendLine("		And A.Status = 'A' ");
            SQL.AppendLine("		And B.PropertyCode = '" + PropertyCode + "' ");
            SQL.AppendLine(") T2 On T1.PropertyCode = T2.PropertyCode ");
            SQL.AppendLine("Set T1.PropertyInventoryValue = IfNull(T2.NewPropInventoryeVal, 0.00), ");
            SQL.AppendLine("T1.Uprice = IfNull(T2.NewUPrice, 0.00), ");
            SQL.AppendLine("T1.RemStockValue = IfNull(T1.RemStockQty, 0.00) * IfNull(T2.NewUPrice, 0.00) ");
            SQL.AppendLine("Where T1.PropertyCode = '" + PropertyCode + "'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);


            return cm;
        }

        private MySqlCommand SavePropertyInventoryCostComponentDtl4(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            SQL.AppendLine("Insert Into TblPropertyInventoryCostComponentDtl4(DocNo, DNo, PropertyCode, PropertyCategoryCode, SiteCode, CCCode, RemStockQty, ValueBeforeAdd, CostComponentAmt, ValueAfterAdd, CreateBy, CreateDt)");
            SQL.AppendLine("Values");
            for (int i = 0; i < Grd4.Rows.Count - 1; i++)
            {
                string a = Sm.GetGrdStr(Grd4, i, 0);
                if (Sm.GetGrdStr(Grd4, i, 1).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_" + i.ToString()+ ", @PropertyCode_" + i.ToString() + ", @PropertyCategoryCode_" + i.ToString() + ", @SiteCode_" + i.ToString() + ", @CCCode_" + i.ToString() + ", @RemStockQty_" + i.ToString() + ", @ValueBeforeAdd_" + i.ToString() + ", @CostComponentAmt_" + i.ToString() + ", @ValueAfterAdd_" + i.ToString() + ", @CreateBy, CurrentDateTime())");
                    Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), Sm.Right("00" + (i + 1).ToString(),3));
                    Sm.CmParam<String>(ref cm, "@PropertyCode_" + i.ToString(), Sm.GetGrdStr(Grd4, i, 1));
                    Sm.CmParam<String>(ref cm, "@PropertyCategoryCode_" + i.ToString(), Sm.GetGrdStr(Grd4, i, 11));
                    Sm.CmParam<String>(ref cm, "@SiteCode_" + i.ToString(), Sm.GetGrdStr(Grd4, i, 12));
                    Sm.CmParam<String>(ref cm, "@CCCode_" + i.ToString(), Sm.GetGrdStr(Grd4, i, 13));
                    Sm.CmParam<Decimal>(ref cm, "@RemStockQty_" + i.ToString(), Sm.GetGrdDec(Grd4, i, 6));
                    Sm.CmParam<Decimal>(ref cm, "@ValueBeforeAdd_" + i.ToString(), Sm.GetGrdDec(Grd4, i, 8));
                    Sm.CmParam<Decimal>(ref cm, "@CostComponentAmt_" + i.ToString(), Sm.GetGrdDec(Grd4, i, 9));
                    Sm.CmParam<Decimal>(ref cm, "@ValueAfterAdd_" + i.ToString(), Sm.GetGrdDec(Grd4, i, 10));
                }
            }
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "1");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();

            return cm;
        }
        
        private MySqlCommand SavePropertyInventoryCostComponentDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            SQL.AppendLine("Insert Into TblPropertyInventoryCostComponentDtl(DocNo, DNo, DocType, DODDocNo, DODDNo, CurCode, UPrice, Qty, Amt, CreateBy, CreateDt)");
            SQL.AppendLine("Values");
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 0).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_" + i.ToString()+ ", @DocType, @DODDocNo_" + i.ToString() + ", @DODDNo_" + i.ToString() + ", @CurCode_" + i.ToString() + ", @UPrice_" + i.ToString() + ", @Qty_" + i.ToString() + ", @Amt_" + i.ToString() + ", @CreateBy, CurrentDateTime())");
                    Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), Sm.Right("00" + (i + 1).ToString(),3));
                    Sm.CmParam<String>(ref cm, "@DODDocNo_" + i.ToString(), Sm.GetGrdStr(Grd1, i, 0));
                    Sm.CmParam<String>(ref cm, "@DODDNo_" + i.ToString(), Sm.GetGrdStr(Grd1, i, 1));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 7));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 8));
                    Sm.CmParam<String>(ref cm, "@CurCode_" + i.ToString(), Sm.GetGrdStr(Grd1, i, 10));
                }
            }
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "1");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();

            return cm;
        }
        
        private MySqlCommand SavePropertyInventoryCostComponentDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            SQL.AppendLine("Insert Into TblPropertyInventoryCostComponentDtl2(DocNo, DNo, DocType, CASDocNo, CurCode, CreateBy, CreateDt)");
            SQL.AppendLine("Values");
            for (int i = 0; i <= Grd3.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd3, i, 0).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_" + i.ToString() + ", @DocType, @CASDocNo_" + i.ToString() + ", @CurCode_" + i.ToString() + ", @CreateBy, CurrentDateTime())");
                    Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), Sm.Right("00" + (i + 1).ToString(),3));
                    Sm.CmParam<String>(ref cm, "@CASDocNo_" + i.ToString(), Sm.GetGrdStr(Grd3, i, 0));
                    Sm.CmParam<String>(ref cm, "@CurCode_" + i.ToString(), Sm.GetGrdStr(Grd3, i, 6));
                }
            }
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "2");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @row:= 0; ");
            SQL.AppendLine("Update TblPropertyInventoryCostComponentHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('Property Inventory Cost Component : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, B.CCCode,  A.CostComponentReason, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("Inner Join tblpropertyinventorycostcomponentdtl4 B on A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Group By A.DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, DAmt, CAmt From ( ");
            SQL.AppendLine("        Select D.AcNo1 AcNo, A.CostComponentAmt As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("		INNER JOIN tblpropertyinventorycostcomponentdtl4 B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryHdr C On B.PropertyCode = C.PropertyCode ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryCategory D On B.PropertyCategoryCode = D.PropertyCategoryCode ");
            SQL.AppendLine("		Where A.DocNo = @DocNo Group By A.DocNo ");
            SQL.AppendLine("		Union All ");
            SQL.AppendLine("		Select F.AcNo, 0.00 As DAmt, A.Amt As CAmt ");
            SQL.AppendLine("		From TblPropertyInventoryCostComponentDtl A ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryCostComponentHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblDODeptDtl C On A.DODDocNo = C.DocNo And A.DODDNo = C.DNo ");
            SQL.AppendLine("		Inner Join TblDODeptHdr D On C.DocNo = D.DocNo ");
            SQL.AppendLine("		Inner Join TblItemCostCategory E On C.ItCode = E.ItCode And D.CCCode = E.CCCode ");
            SQL.AppendLine("		Inner Join TblCostCategory F On E.CCtCode = F.CCtCode ");
            SQL.AppendLine("		Left Join TblItem G On C.ItCode = G.ItCode ");
            SQL.AppendLine("		Left Join TblItemCategory H On G.ItCtCode = H.ItCtCode ");
            SQL.AppendLine("		Where A.DocNo = @DocNo ");
            SQL.AppendLine("		Union All ");
            SQL.AppendLine("		Select F.AcNo, 0.00 As DAmt, C.Amt As CAmt ");
            SQL.AppendLine("		From TblPropertyInventoryCostComponentDtl2 A ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryCostComponentHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblCashAdvanceSettlementDtl C On A.CASDocNo = C.DocNo ");
            SQL.AppendLine("		Inner Join TblCostCategory F On C.CCtCode = F.CCtCode ");
            SQL.AppendLine("		Left Join TblItem G On C.ItCode = G.ItCode ");
            SQL.AppendLine("		Left Join TblItemCategory H On G.ItCtCode = H.ItCtCode ");
            SQL.AppendLine("		Where A.DocNo = @DocNo ");
            SQL.AppendLine("    )Tbl Where AcNo Is Not Null -- Group By AcNo ");
            SQL.AppendLine(")B On 1=1 ");
            SQL.AppendLine("Where A.DocNo = @JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.ServerCurrentDate(), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            try
            {
                if (
                    Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataInvalid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();
                var cml2 = new List<MySqlCommand>();

                cml.Add(UpdatePropertyInvnetoryCostComponent());
                
                //if (mIsAutoJournalActived)
                //    cml.Add(SaveJournal2());
                for (int r = 0; r < Grd4.Rows.Count - 1; r++)
                {
                    cml.Add(UpdatePropertyInventoryValue("Y", TxtDocNo.Text, Sm.GetGrdStr(Grd4, r, 1)));
                }

                Sm.ExecCommands(cml);

                #region old code. comment by IBL 13/01/2023
                //if (!IsCostComponentExist())
                //    cml2.Add(UpdatePropertyInventoryValueDefault());
                //Sm.ExecCommands(cml2);
                #endregion
                ShowData(TxtDocNo.Text);
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsEditedDataInvalid()
        {
            return
                IsPropertyInvnetoryCostAlreadyCancelled() ||
                IsPropCodeAlreadyProceedToOtherTrx()
                ;
        }

        private bool IsPropertyInvnetoryCostAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblPropertyInventoryCostComponentHdr " +
                "Where DocNo=@Param And CancelInd='Y';",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private bool IsPropCodeAlreadyProceedToOtherTrx()
        {
            for (int i = 0; i < Grd4.Rows.Count - 1; i++)
            {
                var SQL = new StringBuilder();
                SQL.AppendLine("Set @CreateDt := (Select CreateDt From TblPropertyInventoryCostComponentHdr Where DocNo = @DocNo);");

                SQL.AppendLine("Select Concat( ");
                SQL.AppendLine("    'Transaction'\t': ', C.MenuDesc, '\n', ");
                SQL.AppendLine("    'Document#'\t': ', A.DocNo, '\n', ");
                SQL.AppendLine("    'Date'\t\t': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
                SQL.AppendLine("    'Property Code'\t': ', @PropertyCode, '\n' ");
                SQL.AppendLine(") As Remarks ");
                SQL.AppendLine("From TblPropertyInventoryCostComponentHdr A ");
                SQL.AppendLine("INNER JOIN tblpropertyinventorycostcomponentdtl4 B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("Left Join TblMenu C On 1 = 1 And C.Param = 'FrmPropertyInventoryCost' ");
                SQL.AppendLine("Where B.PropertyCode = @PropertyCode ");
                SQL.AppendLine("And A.DocNo <> @DocNo ");
                SQL.AppendLine("And A.CreateDt >= @CreateDt ");
                SQL.AppendLine("And A.CancelInd = 'N' ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Concat( ");
                SQL.AppendLine("    'Transaction'\t': ', C.MenuDesc, '\n', ");
                SQL.AppendLine("    'Document#'\t': ', A.DocNo, '\n', ");
                SQL.AppendLine("    'Date'\t\t': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
                SQL.AppendLine("    'Property Code'\t': ', @PropertyCode, '\n' ");
                SQL.AppendLine(") As Remarks ");
                SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
                SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("Left Join TblMenu C On 1 = 1 And C.Param = 'FrmPropertyInventoryMutation' ");
                SQL.AppendLine("Where B.PropertyCode = @PropertyCode ");
                SQL.AppendLine("And A.DocNo <> @DocNo ");
                SQL.AppendLine("And A.CreateDt >= @CreateDt ");
                SQL.AppendLine("And A.CancelInd = 'N' ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Concat( ");
                SQL.AppendLine("    'Transaction'\t': ', C.MenuDesc, '\n', ");
                SQL.AppendLine("    'Document#'\t': ', A.DocNo, '\n', ");
                SQL.AppendLine("    'Date'\t\t': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
                SQL.AppendLine("    'Property Code'\t': ', @PropertyCode, '\n' ");
                SQL.AppendLine(") As Remarks ");
                SQL.AppendLine("From TblPropertyInventoryTransferHdr A ");
                SQL.AppendLine("Inner Join TblPropertyInventoryTransferDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("Left Join TblMenu C On 1 = 1 And C.Param = 'FrmPropertyInventoryTransfer' ");
                SQL.AppendLine("Where B.PropertyCode = @PropertyCode ");
                SQL.AppendLine("And A.DocNo <> @DocNo ");
                SQL.AppendLine("And A.CreateDt >= @CreateDt ");
                SQL.AppendLine("And A.CancelInd = 'N'; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<string>(ref cm, "@DocNo", TxtDocNo.Text);
                var a = Sm.GetGrdStr(Grd4, i, 1);
                Sm.CmParam<string>(ref cm, "@PropertyCode", Sm.GetGrdStr(Grd4, i, 1));

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "Remarks", });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (Sm.DrStr(dr, c[0]).Length > 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "This Property Code is already proceed to other transaction." +
                                    Environment.NewLine + Environment.NewLine +
                                    Sm.DrStr(dr, c[0]));
                                return true;
                            }
                        }
                    }
                    dr.Close();
                }
            }

            return false;
        }

        private MySqlCommand UpdatePropertyInventoryCostComponentFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPropertyInventoryCostComponentHdr Set ");
            SQL.AppendLine("    FileName1=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        
        private MySqlCommand UpdatePropertyInventoryCostComponentFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPropertyInventoryCostComponentHdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        
        private MySqlCommand UpdatePropertyInventoryCostComponentFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPropertyInventoryCostComponentHdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdatePropertyInvnetoryCostComponent()
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Update TblPropertyInventoryCostComponentHdr ");
            SQL.AppendLine("    Set CancelReason = @CancelReason, ");
            SQL.AppendLine("    CancelInd = 'Y' ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            //var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblPropertyInventoryCostComponentHdr Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y'; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            //if (IsClosingJournalUseCurrentDt)
            //    SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            //else
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblPropertyInventoryCostComponentHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblPropertyInventoryCostComponentHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowPropertyInventoryCostComponentHdr(DocNo);
                ShowPropertyInventoryCostComponentDtl(DocNo);
                ShowPropertyInventoryCostComponentDtl2(DocNo);
                ShowPropertyInventoryCostComponentDtl4(DocNo);
                ComputeRemStockQty();
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPropertyInventoryCostComponentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.CancelInd, A.CancelReason, A.DocDt, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("	When 'O' Then 'Outstanding' ");
            SQL.AppendLine("	When 'A' Then 'Approved' ");
            SQL.AppendLine("	When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As Status, ");
            SQL.AppendLine("A.ValueBeforeAdd, A.CostComponentAmt, A.ValueAfterAdd, A.CostComponentReason, A.FileName1, A.FileName2, A.FileName3 ");
            SQL.AppendLine("From TblPropertyInventoryCostComponentHdr A ");
            //SQL.AppendLine("Inner Join TblPropertyInventoryHdr B On A.PropertyCode = B.PropertyCode ");
            //SQL.AppendLine("Left Join TblPropertyInventoryCategory C On A.PropertyCategoryCode = C.PropertyCategoryCode ");
            //SQL.AppendLine("Left Join TblSite D On A.SiteCode = D.SiteCode ");
            //SQL.AppendLine("Left Join TblCostCenter E On A.CCCode = E.CCCode ");
            //SQL.AppendLine("Left Join TblProfitCenter F On E.ProfitCenterCode = F.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
            ref cm, SQL.ToString(),
            new string[]
            { 
                //0
                "DocNo",

                //1-5
                "CancelReason", "CancelInd", "DocDt", "Status", "CostComponentAmt", 

                //6-10
                "CostComponentReason", "FileName1", "FileName2", "FileName3", "ValueBeforeAdd", 

                //11-12
                "ValueAfterAdd"
            },
            (MySqlDataReader dr, int[] c) =>
            {
                TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                MeeCancelReason.EditValue = Sm.DrStr(dr, c[1]);
                ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[3]));
                TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                TxtCostComponentValue.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                MeeCostComponenReason.EditValue = Sm.DrStr(dr, c[6]);
                TxtFile.EditValue = Sm.DrStr(dr, c[7]);
                TxtFile2.EditValue = Sm.DrStr(dr, c[8]);
                TxtFile3.EditValue = Sm.DrStr(dr, c[9]);
                TxtPropertyInventoryBefore.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                TxtPropertyInventoryAfter.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
            }, true
            );
            //ComputePropertyInventoryAfter();
        }

        private void ShowPropertyInventoryCostComponentDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            var DODDocNo = Sm.GetValue("Select B.DODDocNo From TblPropertyInventoryCostComponentHdr A " +
                "Inner Join TblPropertyInventoryCostComponentDtl B On A.DocNo = B.DocNo " +
                "Where A.DocNo = @Param; ", DocNo);

            var SQL = new StringBuilder();

            if (DODDocNo.Length > 0)
            {
                SQL.AppendLine("Select A.DODDocNo, D.RecvVdDocNo, A.DODDNo, B.ItCode, C.ItName, C.InventoryUOMCode, ");
                SQL.AppendLine("A.UPrice, A.Qty, A.Amt, E.CurCode, D.Remark, F.CCName ");
                SQL.AppendLine("From TblPropertyInventoryCostComponentDtl A ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DODDocNo = B.DocNo And A.DODDNo = B.DNo ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                SQL.AppendLine("Inner Join TblDODeptHdr D On B.DocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblRecvVdHdr E On D.RecvVdDocNo = E.DocNo ");
                SQL.AppendLine("Inner Join TblCostCenter F On D.CCCode = F.CCCode ");
                SQL.AppendLine("Where A.DocNo = @DocNo Order By A.DNo; ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                    { 
                    //0
                    "DODDocNo",
                    //1-5
                    "DODDNo", "ItCode", "ItName", "InventoryUOMCode", "UPrice", 
                    //6-10
                    "Qty", "Amt", "CurCode", "RecvVdDocNo", "Remark",
                    //11
                    "CCName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    }, false, false, true, false
                );
            }
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPropertyInventoryCostComponentDtl4(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.PropertyCode, B.PropertyName, C.PropertyCategoryName, D.CCName, E.SiteName, A.RemStockQty, B.UomCode, A.ValueBeforeAdd, A.CostComponentAmt, A.ValueAfterAdd, ");
            SQL.AppendLine("A.PropertyCategoryCode, A.SiteCode, A.CCCode ");
            SQL.AppendLine("FROM TblPropertyInventoryCostComponentDtl4 A ");
            SQL.AppendLine("INNER JOIN TblPropertyInventoryHdr B ON A.PropertyCode = B.PropertyCode ");
            SQL.AppendLine("INNER JOIN TblPropertyInventoryCategory C ON A.PropertyCategoryCode = C.PropertyCategoryCode ");
            SQL.AppendLine("INNER JOIN TblCostCenter D ON A.CCCode = D.CCCode ");
            SQL.AppendLine("INNER JOIN TblSite E ON A.SiteCode = E.SiteCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "PropertyCode",
                    //1-5
                    "PropertyName", "PropertyCategoryName", "CCName", "SiteName", "RemStockQty", 
                    //6-10
                    "UomCode", "ValueBeforeAdd", "CostComponentAmt", "ValueAfterAdd", "PropertyCategoryCode",
                    //11-12
                    "SiteCode", "CCCode",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 6, 8, 9, 10 });
            Sm.FocusGrd(Grd4, 0, 1);
        }
        
        private void ShowPropertyInventoryCostComponentDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            var CASDocNo = Sm.GetValue("Select B.CASDocNo From TblPropertyInventoryCostComponentHdr A " +
                "Inner Join TblPropertyInventoryCostComponentDtl2 B On A.DocNo = B.DocNo " +
                "Where A.DocNo = @Param; ", DocNo);

            var SQL = new StringBuilder();

            if (CASDocNo.Length > 0)
            {
                SQL.AppendLine("Select A.CASDocNo, B.DocDt, C.DeptCode, D.DeptName, Ifnull(E.UserName, B.PIC) PIC, ");
                SQL.AppendLine("A.CurCode, SUM(C.Amt) Amt , B.Remark ");
                SQL.AppendLine("From TblPropertyInventoryCostComponentDtl2 A ");
                SQL.AppendLine("Inner Join TblCashAdvanceSettlementHdr B On A.CASDocNo = B.DocNo");
                SQL.AppendLine("Inner Join TblCashAdvanceSettlementDtl C On B.DocNo = C.DocNo");
                SQL.AppendLine("Left Join Tbldepartment D On B.DeptCode = D.DeptCode ");
                SQL.AppendLine("Left Join tbluser E On B.PIC = E.UserCode ");
                SQL.AppendLine("Where A.DocNo = @DocNo Group By A.DNo; ");

                Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[]
                    { 
                    //0
                    "CASDocNo",
                    
                    //1-5
                    "DocDt", "DeptCode", "DeptName", "PIC", "CurCode", 
        
                    //6-7
                    "Amt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    }, false, false, true, false
                );
            }
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 7 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, IfNull(B.UserName, A.UserCode) As UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='PropertyInventoryCostComponent' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "ApprovalDNo",
                        
                        //1-4
                        "UserName","StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd2.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        internal bool IsDifferentCCSite()
        {
            if (Grd4.Rows.Count > 0)
            {
                for (int i = 0; i < Grd4.Rows.Count - 1; i++)
                {
                    if (Sm.GetGrdStr(Grd4, i + 1, 1).Length != 0)
                    {
                        if (Sm.GetGrdStr(Grd4, i, 12) != Sm.GetGrdStr(Grd4, i + 1, 12) || Sm.GetGrdStr(Grd4, i, 13) != Sm.GetGrdStr(Grd4, i + 1, 13))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Cost Center Or Site Can't different");
                            Sm.ClearGrd(Grd4, true);
                            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 6, 8, 9, 10 });
                            Sm.FocusGrd(Grd4, 0, 0);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("ALTER TABLE `tblpropertyinventorycostcomponenthdr` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `PropertyCategoryCode` VARCHAR(30) NULL DEFAULT NULL; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            var cml = new List<MySqlCommand>();
            cml.Add(cm);

            Sm.ExecCommands(cml);

            cml.Clear();
        }

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
        }

        private bool IsDocApprovalSettingExists(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where UserCode Is Not Null ");
            SQL.AppendLine("And DocType = 'PropertyInventoryCostComponent' ");
            SQL.AppendLine("And DeptCode Is Not Null ");
            SQL.AppendLine("And DeptCode = @Param1 ");
            SQL.AppendLine("And SiteCode Is Not Null ");
            SQL.AppendLine("And SiteCode=@Param2 ");
            


            SQL.AppendLine("Limit 1; ");

            //if (Sm.IsDataExist(SQL.ToString(), GetDeptCode(), GetSiteCode(), string.Empty))
            if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd4, 0, 13), Sm.GetGrdStr(Grd4, 0, 12), string.Empty))
                return true;
            else
                return false;
        }

        private string GetDeptCode()
        {
            string CCSQL = "SELECT A.DeptCode FROM tblcostcenter A WHERE A.CCCode = @Param";
            string DeptCode = Sm.GetValue(CCSQL, Sm.GetGrdStr(Grd4, 0, 13));
            return DeptCode;
        }

        private string GetSiteCode()
        {
            string SiteCodeSQL = "Select A.SiteCode From tblsite A Where A.SiteCode = @Param";
            string SiteCode = Sm.GetValue(SiteCodeSQL, Sm.GetGrdStr(Grd4, 0, 12));
            return SiteCode;
        }

        private void ComputePropertyInventoryAfter()
        {
            decimal ValueAfterAdd = 0m;
            for(int r = 0; r < Grd4.Rows.Count - 1; r++)
            {
                Grd4.Cells[r, 10].Value = Sm.GetGrdDec(Grd4, r, 8) + Sm.GetGrdDec(Grd4, r, 9);
                ValueAfterAdd += Sm.GetGrdDec(Grd4, r, 10);
            }
            TxtPropertyInventoryAfter.EditValue = ValueAfterAdd;
            Sm.FormatNumTxt(TxtPropertyInventoryAfter, 0);
        }

        internal void ComputeCostComponentValue()
        {
            decimal CostComponentValue = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    Grd1.Cells[i, 8].Value = Sm.GetGrdDec(Grd1, i, 6) * Sm.GetGrdDec(Grd1, i, 7);
                    CostComponentValue += Sm.GetGrdDec(Grd1, i, 8);
                }
            }
            else if (Grd3.Rows.Count > 1)
            {
                for (int i = 0; i < Grd3.Rows.Count - 1; i++)
                {
                    CostComponentValue += Sm.GetGrdDec(Grd3, i, 7);
                }
            }

            for(int r = 0; r < Grd4.Rows.Count - 1; r++)
            {
                Grd4.Cells[r, 9].Value = (CostComponentValue / Decimal.Parse(TxtRemainStockQty.Text)) * Sm.GetGrdDec(Grd4, r, 6);
            }

            TxtCostComponentValue.EditValue = Sm.FormatNum(CostComponentValue, 0);
            ComputePropertyInventoryAfter();
        }

        internal void SetLueOption(ref DXE.LookUpEdit Lue, string OptCat, string OptCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat=@Param And OptCode=@OptCode Order By OptDesc; ");
            cm.CommandText = SQL.ToString();
            if (OptCat.Length > 0) Sm.CmParam<String>(ref cm, "@Param", OptCat);
            if (OptCode.Length > 0) Sm.CmParam<String>(ref cm, "@OptCode", OptCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void ComputeRemStockQty()
        {
            decimal RemStockQy = 0m;
            for(int r = 0; r < Grd4.Rows.Count - 1; r++)
            {
                RemStockQy += Sm.GetGrdDec(Grd4, r, 6);
            }
            TxtRemainStockQty.Text = Convert.ToString(RemStockQy);
            Sm.FormatNumTxt(TxtRemainStockQty, 0);
        }
        
        internal void ComputeValueBeforeAdd()
        {
            decimal Value = 0m;
            for(int r = 0; r < Grd4.Rows.Count - 1; r++)
            {
                Value += Sm.GetGrdDec(Grd4, r, 8);
            }
            TxtPropertyInventoryBefore.Text = Convert.ToString(Value);
            Sm.FormatNumTxt(TxtPropertyInventoryBefore, 0);
        }

        private void UploadFile1(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePropertyInventoryCostComponentFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }
        
        private void UploadFile2(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePropertyInventoryCostComponentFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }
        
        private void UploadFile3(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePropertyInventoryCostComponentFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }
        
        private bool IsUploadFileNotValid2()
        {
            return
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPropertyInventoryCostComponentHdr ");
                SQL.AppendLine("Where FileName1=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }
        
        private bool IsFTPClientDataNotValid2()
        {

            if (TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile2.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPropertyInventoryCostComponentHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileMandatory()
        {
            if (TxtFile.Text == "" || TxtFile.Text == "openFileDialog1")
            {
                Sm.StdMsg(mMsgType.Warning, "Supporting File is Empty");
                return true;
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = mMenuCode+"-Property Inventory Cost Component";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = mMenuCode + "-Property Inventory Cost Component";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }
        
        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = mMenuCode + "-Property Inventory Cost Component";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                //OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.PDF";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                //OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.PDF";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                //OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.PDF";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile3, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }
        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtEcoLife3_Validated(object sender, EventArgs e)
        {
            if(BtnSave.Enabled) Sm.FormatNumTxt(TxtPropertyInventoryAfter, 0);
        }

        private void TxtUPrice_Validated(object sender, EventArgs e)
        {
            //Sm.FormatNumTxt(TxtUPrice, 2);
            //ComputeFairValueAdjAmt();
            ComputeCostComponentValue();
            //TxtUPriceAfter.EditValue = Sm.FormatNum(Sm.GetDecValue(TxtPropertyInventoryAfter.Text) / Sm.GetDecValue(TxtInventoryQty.Text), 0);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 2)
                Sm.FormShowDialog(new FrmPropertyInventoryCostMultiDlg2(this));
        }


        private void Grd1_EllispsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 2)
                Sm.FormShowDialog(new FrmPropertyInventoryCostMultiDlg2(this));
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 7 && BtnSave.Enabled)
            {
                ComputeCostComponentValue();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if(BtnSave.Enabled)
                ComputeCostComponentValue();
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 2)
                Sm.FormShowDialog(new FrmPropertyInventoryCostMultiDlg3(this));
        }

        private void Grd3_EllispsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 1)
                Sm.FormShowDialog(new FrmPropertyInventoryCostMultiDlg3(this));
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled)
            {
                ComputeCostComponentValue();
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            if (BtnSave.Enabled)
                ComputeCostComponentValue();
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 0)
                Sm.FormShowDialog(new FrmPropertyInventoryCostMultiDlg(this));
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 0)
                Sm.FormShowDialog(new FrmPropertyInventoryCostMultiDlg(this));
        }
        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            if (BtnSave.Enabled)
            {
                ComputeRemStockQty();
                ComputeValueBeforeAdd();
                ComputeCostComponentValue();
            }
        }

        #endregion

        #endregion

        #region Class

        private class DepreciationAsset
        {
            public string DNo { get; set; }
            public decimal DepreciationValue { get; set; }
            public decimal AccumulationValue { get; set; }
        }

        #endregion
    }
}
