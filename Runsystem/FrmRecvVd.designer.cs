﻿namespace RunSystem
{
    partial class FrmRecvVd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRecvVd));
            this.TxtVdDONo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LblRemark = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueVdCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.DteExpiredDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.LueLot = new DevExpress.XtraEditors.LookUpEdit();
            this.LueBin = new DevExpress.XtraEditors.LookUpEdit();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TcRecvVd = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.LueProductionWorkGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.LblProductionWorkGroup = new System.Windows.Forms.Label();
            this.LueEntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblEntCode = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblDeptCode = new System.Windows.Forms.Label();
            this.LueCCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblCCCode = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.LueCustomsDocCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtKBSubmissionNo = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.ChkKBNonDocInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtKBPackaging = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtKBPackagingQty = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.DteKBRegistrationDt = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.DteKBPLDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.DteKBContractDt = new DevExpress.XtraEditors.DateEdit();
            this.BtnKBContractNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtKBRegistrationNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtKBPLNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtKBContractNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.BtnRecvExpeditionDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnRecvExpeditionDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtRecvExpeditionDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.Tp4 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblFile6 = new System.Windows.Forms.Label();
            this.lblFile3 = new System.Windows.Forms.Label();
            this.lblFile5 = new System.Windows.Forms.Label();
            this.lblFile2 = new System.Windows.Forms.Label();
            this.lblFile4 = new System.Windows.Forms.Label();
            this.lblFile1 = new System.Windows.Forms.Label();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.ChkFile6 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.BtnDownload6 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload6 = new System.Windows.Forms.ProgressBar();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile6 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile6 = new DevExpress.XtraEditors.TextEdit();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.ChkFile5 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload5 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.PbUpload5 = new System.Windows.Forms.ProgressBar();
            this.BtnFile3 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile5 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.TxtFile5 = new DevExpress.XtraEditors.TextEdit();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile4 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.BtnDownload4 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload4 = new System.Windows.Forms.ProgressBar();
            this.TxtFile4 = new DevExpress.XtraEditors.TextEdit();
            this.BtnFile4 = new DevExpress.XtraEditors.SimpleButton();
            this.Tp5 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.button2 = new System.Windows.Forms.Button();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdDONo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcRecvVd)).BeginInit();
            this.TcRecvVd.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCustomsDocCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBSubmissionNo.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkKBNonDocInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackaging.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackagingQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBRegistrationNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPLNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBContractNo.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvExpeditionDocNo.Properties)).BeginInit();
            this.Tp4.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile4.Properties)).BeginInit();
            this.Tp5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(827, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcRecvVd);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(827, 394);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.DteExpiredDt);
            this.panel3.Controls.Add(this.LueBin);
            this.panel3.Controls.Add(this.LueLot);
            this.panel3.Location = new System.Drawing.Point(0, 394);
            this.panel3.Size = new System.Drawing.Size(827, 79);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.LueLot, 0);
            this.panel3.Controls.SetChildIndex(this.LueBin, 0);
            this.panel3.Controls.SetChildIndex(this.DteExpiredDt, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(827, 79);
            this.Grd1.TabIndex = 36;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtVdDONo
            // 
            this.TxtVdDONo.EnterMoveNextControl = true;
            this.TxtVdDONo.Location = new System.Drawing.Point(94, 110);
            this.TxtVdDONo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdDONo.Name = "TxtVdDONo";
            this.TxtVdDONo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdDONo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdDONo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdDONo.Properties.Appearance.Options.UseFont = true;
            this.TxtVdDONo.Properties.MaxLength = 30;
            this.TxtVdDONo.Size = new System.Drawing.Size(216, 20);
            this.TxtVdDONo.TabIndex = 23;
            this.TxtVdDONo.Validated += new System.EventHandler(this.TxtVdDONo_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(57, 113);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 14);
            this.label12.TabIndex = 22;
            this.label12.Text = "DO#";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(94, 152);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(379, 20);
            this.MeeRemark.TabIndex = 27;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // LblRemark
            // 
            this.LblRemark.AutoSize = true;
            this.LblRemark.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRemark.Location = new System.Drawing.Point(43, 155);
            this.LblRemark.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRemark.Name = "LblRemark";
            this.LblRemark.Size = new System.Drawing.Size(47, 14);
            this.LblRemark.TabIndex = 26;
            this.LblRemark.Text = "Remark";
            this.LblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(43, 93);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 14);
            this.label3.TabIndex = 20;
            this.label3.Text = "Vendor";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCode
            // 
            this.LueVdCode.EnterMoveNextControl = true;
            this.LueVdCode.Location = new System.Drawing.Point(94, 89);
            this.LueVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCode.Name = "LueVdCode";
            this.LueVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCode.Properties.DropDownRows = 30;
            this.LueVdCode.Properties.NullText = "[Empty]";
            this.LueVdCode.Properties.PopupWidth = 400;
            this.LueVdCode.Size = new System.Drawing.Size(379, 20);
            this.LueVdCode.TabIndex = 21;
            this.LueVdCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCode.EditValueChanged += new System.EventHandler(this.LueVdCode_EditValueChanged);
            this.LueVdCode.Validated += new System.EventHandler(this.LueVdCode_Validated);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(94, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 15;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(57, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(94, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(216, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(17, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(21, 71);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 14);
            this.label4.TabIndex = 18;
            this.label4.Text = "Warehouse";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(94, 68);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 30;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 400;
            this.LueWhsCode.Size = new System.Drawing.Size(379, 20);
            this.LueWhsCode.TabIndex = 19;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.Grd2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(0, 308);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(827, 86);
            this.panel4.TabIndex = 33;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(823, 82);
            this.Grd2.TabIndex = 35;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd2_ColHdrDoubleClick);
            // 
            // DteExpiredDt
            // 
            this.DteExpiredDt.EditValue = null;
            this.DteExpiredDt.EnterMoveNextControl = true;
            this.DteExpiredDt.Location = new System.Drawing.Point(444, 21);
            this.DteExpiredDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteExpiredDt.Name = "DteExpiredDt";
            this.DteExpiredDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpiredDt.Properties.Appearance.Options.UseFont = true;
            this.DteExpiredDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpiredDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteExpiredDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteExpiredDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteExpiredDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpiredDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteExpiredDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpiredDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteExpiredDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteExpiredDt.Size = new System.Drawing.Size(125, 20);
            this.DteExpiredDt.TabIndex = 39;
            this.DteExpiredDt.Visible = false;
            this.DteExpiredDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteExpiredDt_KeyDown);
            this.DteExpiredDt.Leave += new System.EventHandler(this.DteExpiredDt_Leave);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(94, 47);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 250;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(216, 20);
            this.TxtLocalDocNo.TabIndex = 17;
            this.TxtLocalDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(47, 50);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 14);
            this.label11.TabIndex = 16;
            this.label11.Text = "Local#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLot
            // 
            this.LueLot.EnterMoveNextControl = true;
            this.LueLot.Location = new System.Drawing.Point(67, 21);
            this.LueLot.Margin = new System.Windows.Forms.Padding(5);
            this.LueLot.Name = "LueLot";
            this.LueLot.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.Appearance.Options.UseFont = true;
            this.LueLot.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLot.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLot.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLot.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLot.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLot.Properties.DropDownRows = 30;
            this.LueLot.Properties.NullText = "[Empty]";
            this.LueLot.Properties.PopupWidth = 200;
            this.LueLot.Size = new System.Drawing.Size(150, 20);
            this.LueLot.TabIndex = 37;
            this.LueLot.ToolTip = "F4 : Show/hide list";
            this.LueLot.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLot.EditValueChanged += new System.EventHandler(this.LueLot_EditValueChanged);
            this.LueLot.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueLot_KeyDown);
            this.LueLot.Leave += new System.EventHandler(this.LueLot_Leave);
            // 
            // LueBin
            // 
            this.LueBin.EnterMoveNextControl = true;
            this.LueBin.Location = new System.Drawing.Point(262, 21);
            this.LueBin.Margin = new System.Windows.Forms.Padding(5);
            this.LueBin.Name = "LueBin";
            this.LueBin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.Appearance.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBin.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBin.Properties.DropDownRows = 30;
            this.LueBin.Properties.NullText = "[Empty]";
            this.LueBin.Properties.PopupWidth = 200;
            this.LueBin.Size = new System.Drawing.Size(150, 20);
            this.LueBin.TabIndex = 38;
            this.LueBin.ToolTip = "F4 : Show/hide list";
            this.LueBin.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBin.EditValueChanged += new System.EventHandler(this.LueBin_EditValueChanged);
            this.LueBin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBin_KeyDown);
            this.LueBin.Leave += new System.EventHandler(this.LueBin_Leave);
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(62, 134);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 24;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(94, 131);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 400;
            this.LueSiteCode.Size = new System.Drawing.Size(379, 20);
            this.LueSiteCode.TabIndex = 25;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.Validated += new System.EventHandler(this.LueSiteCode_Validated);
            // 
            // TcRecvVd
            // 
            this.TcRecvVd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcRecvVd.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcRecvVd.Location = new System.Drawing.Point(0, 0);
            this.TcRecvVd.Name = "TcRecvVd";
            this.TcRecvVd.SelectedTabPage = this.Tp1;
            this.TcRecvVd.Size = new System.Drawing.Size(827, 308);
            this.TcRecvVd.TabIndex = 11;
            this.TcRecvVd.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3,
            this.Tp4,
            this.Tp5});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(821, 280);
            this.Tp1.Text = "General";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.TxtDocNo);
            this.panel5.Controls.Add(this.LblSiteCode);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.LueSiteCode);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.LueVdCode);
            this.panel5.Controls.Add(this.DteDocDt);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.LueWhsCode);
            this.panel5.Controls.Add(this.TxtLocalDocNo);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.LblRemark);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.TxtVdDONo);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(821, 280);
            this.panel5.TabIndex = 22;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.LueProductionWorkGroup);
            this.panel8.Controls.Add(this.LblProductionWorkGroup);
            this.panel8.Controls.Add(this.LueEntCode);
            this.panel8.Controls.Add(this.LblEntCode);
            this.panel8.Controls.Add(this.LueDeptCode);
            this.panel8.Controls.Add(this.LblDeptCode);
            this.panel8.Controls.Add(this.LueCCCode);
            this.panel8.Controls.Add(this.LblCCCode);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(487, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(334, 280);
            this.panel8.TabIndex = 30;
            // 
            // LueProductionWorkGroup
            // 
            this.LueProductionWorkGroup.EnterMoveNextControl = true;
            this.LueProductionWorkGroup.Location = new System.Drawing.Point(85, 68);
            this.LueProductionWorkGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueProductionWorkGroup.Name = "LueProductionWorkGroup";
            this.LueProductionWorkGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.Appearance.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProductionWorkGroup.Properties.DropDownRows = 30;
            this.LueProductionWorkGroup.Properties.NullText = "[Empty]";
            this.LueProductionWorkGroup.Properties.PopupWidth = 350;
            this.LueProductionWorkGroup.Size = new System.Drawing.Size(244, 20);
            this.LueProductionWorkGroup.TabIndex = 35;
            this.LueProductionWorkGroup.ToolTip = "F4 : Show/hide list";
            this.LueProductionWorkGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProductionWorkGroup.EditValueChanged += new System.EventHandler(this.LueProductionWorkGroup_EditValueChanged);
            // 
            // LblProductionWorkGroup
            // 
            this.LblProductionWorkGroup.AutoSize = true;
            this.LblProductionWorkGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProductionWorkGroup.ForeColor = System.Drawing.Color.Red;
            this.LblProductionWorkGroup.Location = new System.Drawing.Point(40, 71);
            this.LblProductionWorkGroup.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblProductionWorkGroup.Name = "LblProductionWorkGroup";
            this.LblProductionWorkGroup.Size = new System.Drawing.Size(40, 14);
            this.LblProductionWorkGroup.TabIndex = 34;
            this.LblProductionWorkGroup.Text = "Group";
            this.LblProductionWorkGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntCode
            // 
            this.LueEntCode.EnterMoveNextControl = true;
            this.LueEntCode.Location = new System.Drawing.Point(85, 47);
            this.LueEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntCode.Name = "LueEntCode";
            this.LueEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntCode.Properties.DropDownRows = 30;
            this.LueEntCode.Properties.NullText = "[Empty]";
            this.LueEntCode.Properties.PopupWidth = 400;
            this.LueEntCode.Size = new System.Drawing.Size(244, 20);
            this.LueEntCode.TabIndex = 33;
            this.LueEntCode.ToolTip = "F4 : Show/hide list";
            this.LueEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEntCode.EditValueChanged += new System.EventHandler(this.LueEntCode_EditValueChanged);
            // 
            // LblEntCode
            // 
            this.LblEntCode.AutoSize = true;
            this.LblEntCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEntCode.ForeColor = System.Drawing.Color.Black;
            this.LblEntCode.Location = new System.Drawing.Point(41, 50);
            this.LblEntCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEntCode.Name = "LblEntCode";
            this.LblEntCode.Size = new System.Drawing.Size(39, 14);
            this.LblEntCode.TabIndex = 32;
            this.LblEntCode.Text = "Entity";
            this.LblEntCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(85, 26);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 400;
            this.LueDeptCode.Size = new System.Drawing.Size(244, 20);
            this.LueDeptCode.TabIndex = 31;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // LblDeptCode
            // 
            this.LblDeptCode.AutoSize = true;
            this.LblDeptCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDeptCode.ForeColor = System.Drawing.Color.Red;
            this.LblDeptCode.Location = new System.Drawing.Point(7, 29);
            this.LblDeptCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDeptCode.Name = "LblDeptCode";
            this.LblDeptCode.Size = new System.Drawing.Size(73, 14);
            this.LblDeptCode.TabIndex = 30;
            this.LblDeptCode.Text = "Department";
            this.LblDeptCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCCCode
            // 
            this.LueCCCode.EnterMoveNextControl = true;
            this.LueCCCode.Location = new System.Drawing.Point(85, 5);
            this.LueCCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCCode.Name = "LueCCCode";
            this.LueCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.Appearance.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCCode.Properties.DropDownRows = 30;
            this.LueCCCode.Properties.NullText = "[Empty]";
            this.LueCCCode.Properties.PopupWidth = 400;
            this.LueCCCode.Size = new System.Drawing.Size(244, 20);
            this.LueCCCode.TabIndex = 29;
            this.LueCCCode.ToolTip = "F4 : Show/hide list";
            this.LueCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCCode.EditValueChanged += new System.EventHandler(this.LueCCCode_EditValueChanged);
            // 
            // LblCCCode
            // 
            this.LblCCCode.AutoSize = true;
            this.LblCCCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCCCode.ForeColor = System.Drawing.Color.Red;
            this.LblCCCode.Location = new System.Drawing.Point(8, 8);
            this.LblCCCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCCCode.Name = "LblCCCode";
            this.LblCCCode.Size = new System.Drawing.Size(72, 14);
            this.LblCCCode.TabIndex = 28;
            this.LblCCCode.Text = "Cost Center";
            this.LblCCCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel6);
            this.Tp2.Name = "Tp2";
            this.Tp2.PageVisible = false;
            this.Tp2.Size = new System.Drawing.Size(766, 280);
            this.Tp2.Text = "Kawasan Berikat";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.LueCustomsDocCode);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.TxtKBSubmissionNo);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.TxtKBPackaging);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.TxtKBPackagingQty);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.DteKBRegistrationDt);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.DteKBPLDt);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.DteKBContractDt);
            this.panel6.Controls.Add(this.BtnKBContractNo);
            this.panel6.Controls.Add(this.TxtKBRegistrationNo);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.TxtKBPLNo);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.TxtKBContractNo);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(766, 280);
            this.panel6.TabIndex = 22;
            // 
            // LueCustomsDocCode
            // 
            this.LueCustomsDocCode.EnterMoveNextControl = true;
            this.LueCustomsDocCode.Location = new System.Drawing.Point(160, 35);
            this.LueCustomsDocCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCustomsDocCode.Name = "LueCustomsDocCode";
            this.LueCustomsDocCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.Appearance.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCustomsDocCode.Properties.DropDownRows = 30;
            this.LueCustomsDocCode.Properties.NullText = "[Empty]";
            this.LueCustomsDocCode.Properties.PopupWidth = 400;
            this.LueCustomsDocCode.Size = new System.Drawing.Size(177, 20);
            this.LueCustomsDocCode.TabIndex = 13;
            this.LueCustomsDocCode.ToolTip = "F4 : Show/hide list";
            this.LueCustomsDocCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCustomsDocCode.Validated += new System.EventHandler(this.LueCustomsDocCode_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(7, 39);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(146, 14);
            this.label16.TabIndex = 12;
            this.label16.Text = "Customs Document Code";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBSubmissionNo
            // 
            this.TxtKBSubmissionNo.EnterMoveNextControl = true;
            this.TxtKBSubmissionNo.Location = new System.Drawing.Point(160, 127);
            this.TxtKBSubmissionNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBSubmissionNo.Name = "TxtKBSubmissionNo";
            this.TxtKBSubmissionNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBSubmissionNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBSubmissionNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBSubmissionNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBSubmissionNo.Properties.MaxLength = 30;
            this.TxtKBSubmissionNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBSubmissionNo.TabIndex = 28;
            this.TxtKBSubmissionNo.Validated += new System.EventHandler(this.TxtKBSubmissionNo_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(78, 131);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 14);
            this.label14.TabIndex = 27;
            this.label14.Text = "Submission#";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.ChkKBNonDocInd);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(645, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(121, 280);
            this.panel7.TabIndex = 33;
            // 
            // ChkKBNonDocInd
            // 
            this.ChkKBNonDocInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkKBNonDocInd.Location = new System.Drawing.Point(5, 153);
            this.ChkKBNonDocInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkKBNonDocInd.Name = "ChkKBNonDocInd";
            this.ChkKBNonDocInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkKBNonDocInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkKBNonDocInd.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ChkKBNonDocInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkKBNonDocInd.Properties.Appearance.Options.UseFont = true;
            this.ChkKBNonDocInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkKBNonDocInd.Properties.Caption = "Non Document";
            this.ChkKBNonDocInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkKBNonDocInd.Size = new System.Drawing.Size(109, 22);
            this.ChkKBNonDocInd.TabIndex = 34;
            // 
            // TxtKBPackaging
            // 
            this.TxtKBPackaging.EnterMoveNextControl = true;
            this.TxtKBPackaging.Location = new System.Drawing.Point(160, 148);
            this.TxtKBPackaging.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBPackaging.Name = "TxtKBPackaging";
            this.TxtKBPackaging.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBPackaging.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBPackaging.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBPackaging.Properties.Appearance.Options.UseFont = true;
            this.TxtKBPackaging.Properties.MaxLength = 80;
            this.TxtKBPackaging.Size = new System.Drawing.Size(206, 20);
            this.TxtKBPackaging.TabIndex = 30;
            this.TxtKBPackaging.Validated += new System.EventHandler(this.TxtKBPackaging_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(92, 150);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 14);
            this.label13.TabIndex = 29;
            this.label13.Text = "Packaging";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBPackagingQty
            // 
            this.TxtKBPackagingQty.EnterMoveNextControl = true;
            this.TxtKBPackagingQty.Location = new System.Drawing.Point(490, 148);
            this.TxtKBPackagingQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBPackagingQty.Name = "TxtKBPackagingQty";
            this.TxtKBPackagingQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBPackagingQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBPackagingQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBPackagingQty.Properties.Appearance.Options.UseFont = true;
            this.TxtKBPackagingQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtKBPackagingQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtKBPackagingQty.Size = new System.Drawing.Size(140, 20);
            this.TxtKBPackagingQty.TabIndex = 32;
            this.TxtKBPackagingQty.Validated += new System.EventHandler(this.TxtKBPackagingQty_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(375, 150);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 14);
            this.label15.TabIndex = 31;
            this.label15.Text = "Packaging Quantity";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(386, 110);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 14);
            this.label10.TabIndex = 25;
            this.label10.Text = "Registration Date";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBRegistrationDt
            // 
            this.DteKBRegistrationDt.EditValue = null;
            this.DteKBRegistrationDt.EnterMoveNextControl = true;
            this.DteKBRegistrationDt.Location = new System.Drawing.Point(490, 106);
            this.DteKBRegistrationDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBRegistrationDt.Name = "DteKBRegistrationDt";
            this.DteKBRegistrationDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteKBRegistrationDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBRegistrationDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBRegistrationDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBRegistrationDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBRegistrationDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBRegistrationDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBRegistrationDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBRegistrationDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBRegistrationDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBRegistrationDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBRegistrationDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBRegistrationDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBRegistrationDt.Size = new System.Drawing.Size(140, 20);
            this.DteKBRegistrationDt.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(387, 89);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 14);
            this.label9.TabIndex = 21;
            this.label9.Text = "Packing List Date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBPLDt
            // 
            this.DteKBPLDt.EditValue = null;
            this.DteKBPLDt.EnterMoveNextControl = true;
            this.DteKBPLDt.Location = new System.Drawing.Point(490, 85);
            this.DteKBPLDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBPLDt.Name = "DteKBPLDt";
            this.DteKBPLDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteKBPLDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBPLDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBPLDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBPLDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBPLDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBPLDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBPLDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBPLDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBPLDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBPLDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBPLDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBPLDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBPLDt.Size = new System.Drawing.Size(140, 20);
            this.DteKBPLDt.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(403, 67);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 14);
            this.label8.TabIndex = 17;
            this.label8.Text = "Contract Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBContractDt
            // 
            this.DteKBContractDt.EditValue = null;
            this.DteKBContractDt.EnterMoveNextControl = true;
            this.DteKBContractDt.Location = new System.Drawing.Point(490, 64);
            this.DteKBContractDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBContractDt.Name = "DteKBContractDt";
            this.DteKBContractDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteKBContractDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBContractDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBContractDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBContractDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBContractDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBContractDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBContractDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBContractDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBContractDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBContractDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBContractDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBContractDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBContractDt.Size = new System.Drawing.Size(140, 20);
            this.DteKBContractDt.TabIndex = 18;
            // 
            // BtnKBContractNo
            // 
            this.BtnKBContractNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnKBContractNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnKBContractNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnKBContractNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnKBContractNo.Appearance.Options.UseBackColor = true;
            this.BtnKBContractNo.Appearance.Options.UseFont = true;
            this.BtnKBContractNo.Appearance.Options.UseForeColor = true;
            this.BtnKBContractNo.Appearance.Options.UseTextOptions = true;
            this.BtnKBContractNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnKBContractNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnKBContractNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnKBContractNo.Image")));
            this.BtnKBContractNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnKBContractNo.Location = new System.Drawing.Point(342, 35);
            this.BtnKBContractNo.Name = "BtnKBContractNo";
            this.BtnKBContractNo.Size = new System.Drawing.Size(24, 21);
            this.BtnKBContractNo.TabIndex = 14;
            this.BtnKBContractNo.ToolTip = "Show Kawasan Berikat\'s Document";
            this.BtnKBContractNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnKBContractNo.ToolTipTitle = "Run System";
            this.BtnKBContractNo.Click += new System.EventHandler(this.BtnKBContractNo_Click);
            // 
            // TxtKBRegistrationNo
            // 
            this.TxtKBRegistrationNo.EnterMoveNextControl = true;
            this.TxtKBRegistrationNo.Location = new System.Drawing.Point(160, 106);
            this.TxtKBRegistrationNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBRegistrationNo.Name = "TxtKBRegistrationNo";
            this.TxtKBRegistrationNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBRegistrationNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBRegistrationNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBRegistrationNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBRegistrationNo.Properties.MaxLength = 30;
            this.TxtKBRegistrationNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBRegistrationNo.TabIndex = 24;
            this.TxtKBRegistrationNo.Validated += new System.EventHandler(this.TxtKBRegistrationNo_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(73, 110);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "Registration#";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBPLNo
            // 
            this.TxtKBPLNo.EnterMoveNextControl = true;
            this.TxtKBPLNo.Location = new System.Drawing.Point(160, 85);
            this.TxtKBPLNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBPLNo.Name = "TxtKBPLNo";
            this.TxtKBPLNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBPLNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBPLNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBPLNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBPLNo.Properties.MaxLength = 30;
            this.TxtKBPLNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBPLNo.TabIndex = 20;
            this.TxtKBPLNo.Validated += new System.EventHandler(this.TxtKBPLNo_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(74, 89);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 14);
            this.label6.TabIndex = 19;
            this.label6.Text = "Packing List#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBContractNo
            // 
            this.TxtKBContractNo.EnterMoveNextControl = true;
            this.TxtKBContractNo.Location = new System.Drawing.Point(160, 64);
            this.TxtKBContractNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBContractNo.Name = "TxtKBContractNo";
            this.TxtKBContractNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBContractNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBContractNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBContractNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBContractNo.Properties.MaxLength = 30;
            this.TxtKBContractNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBContractNo.TabIndex = 16;
            this.TxtKBContractNo.Validated += new System.EventHandler(this.TxtKBContractNo_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(90, 67);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 14);
            this.label5.TabIndex = 15;
            this.label5.Text = "Contract#";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.BtnRecvExpeditionDocNo2);
            this.Tp3.Controls.Add(this.BtnRecvExpeditionDocNo);
            this.Tp3.Controls.Add(this.TxtRecvExpeditionDocNo);
            this.Tp3.Controls.Add(this.label17);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 280);
            this.Tp3.Text = "Expedition";
            // 
            // BtnRecvExpeditionDocNo2
            // 
            this.BtnRecvExpeditionDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvExpeditionDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvExpeditionDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvExpeditionDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvExpeditionDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnRecvExpeditionDocNo2.Appearance.Options.UseFont = true;
            this.BtnRecvExpeditionDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnRecvExpeditionDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnRecvExpeditionDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvExpeditionDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvExpeditionDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecvExpeditionDocNo2.Image")));
            this.BtnRecvExpeditionDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecvExpeditionDocNo2.Location = new System.Drawing.Point(390, 7);
            this.BtnRecvExpeditionDocNo2.Name = "BtnRecvExpeditionDocNo2";
            this.BtnRecvExpeditionDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnRecvExpeditionDocNo2.TabIndex = 15;
            this.BtnRecvExpeditionDocNo2.ToolTip = "Show Expedition\'s Received Document";
            this.BtnRecvExpeditionDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvExpeditionDocNo2.ToolTipTitle = "Run System";
            this.BtnRecvExpeditionDocNo2.Click += new System.EventHandler(this.BtnRecvExpeditionDocNo2_Click);
            // 
            // BtnRecvExpeditionDocNo
            // 
            this.BtnRecvExpeditionDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvExpeditionDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvExpeditionDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvExpeditionDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvExpeditionDocNo.Appearance.Options.UseBackColor = true;
            this.BtnRecvExpeditionDocNo.Appearance.Options.UseFont = true;
            this.BtnRecvExpeditionDocNo.Appearance.Options.UseForeColor = true;
            this.BtnRecvExpeditionDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnRecvExpeditionDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvExpeditionDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvExpeditionDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecvExpeditionDocNo.Image")));
            this.BtnRecvExpeditionDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecvExpeditionDocNo.Location = new System.Drawing.Point(365, 7);
            this.BtnRecvExpeditionDocNo.Name = "BtnRecvExpeditionDocNo";
            this.BtnRecvExpeditionDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnRecvExpeditionDocNo.TabIndex = 14;
            this.BtnRecvExpeditionDocNo.ToolTip = "Find Expedition\'s Received Document";
            this.BtnRecvExpeditionDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvExpeditionDocNo.ToolTipTitle = "Run System";
            this.BtnRecvExpeditionDocNo.Click += new System.EventHandler(this.BtnRecvExpeditionDocNo_Click);
            // 
            // TxtRecvExpeditionDocNo
            // 
            this.TxtRecvExpeditionDocNo.EnterMoveNextControl = true;
            this.TxtRecvExpeditionDocNo.Location = new System.Drawing.Point(146, 8);
            this.TxtRecvExpeditionDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRecvExpeditionDocNo.Name = "TxtRecvExpeditionDocNo";
            this.TxtRecvExpeditionDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRecvExpeditionDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRecvExpeditionDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRecvExpeditionDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRecvExpeditionDocNo.Properties.MaxLength = 30;
            this.TxtRecvExpeditionDocNo.Properties.ReadOnly = true;
            this.TxtRecvExpeditionDocNo.Size = new System.Drawing.Size(216, 20);
            this.TxtRecvExpeditionDocNo.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(10, 11);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(134, 14);
            this.label17.TabIndex = 12;
            this.label17.Text = "Expedition\'s Received#";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp4
            // 
            this.Tp4.Appearance.Header.Options.UseTextOptions = true;
            this.Tp4.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp4.Controls.Add(this.splitContainer1);
            this.Tp4.Name = "Tp4";
            this.Tp4.Size = new System.Drawing.Size(766, 280);
            this.Tp4.Text = "Upload File";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(1, 1);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lblFile6);
            this.splitContainer1.Panel1.Controls.Add(this.lblFile3);
            this.splitContainer1.Panel1.Controls.Add(this.lblFile5);
            this.splitContainer1.Panel1.Controls.Add(this.lblFile2);
            this.splitContainer1.Panel1.Controls.Add(this.lblFile4);
            this.splitContainer1.Panel1.Controls.Add(this.lblFile1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile6);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload6);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile2);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload6);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload2);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile6);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile6);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile3);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile2);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile5);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload5);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile2);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload5);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile3);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile5);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload3);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile5);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload3);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile4);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload2);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload4);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile3);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload4);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile4);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile4);
            this.splitContainer1.Size = new System.Drawing.Size(819, 278);
            this.splitContainer1.SplitterDistance = 36;
            this.splitContainer1.TabIndex = 48;
            // 
            // lblFile6
            // 
            this.lblFile6.AutoSize = true;
            this.lblFile6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFile6.ForeColor = System.Drawing.Color.Black;
            this.lblFile6.Location = new System.Drawing.Point(12, 226);
            this.lblFile6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblFile6.Name = "lblFile6";
            this.lblFile6.Size = new System.Drawing.Size(24, 14);
            this.lblFile6.TabIndex = 42;
            this.lblFile6.Text = "File";
            this.lblFile6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFile3
            // 
            this.lblFile3.AutoSize = true;
            this.lblFile3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFile3.ForeColor = System.Drawing.Color.Black;
            this.lblFile3.Location = new System.Drawing.Point(12, 97);
            this.lblFile3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblFile3.Name = "lblFile3";
            this.lblFile3.Size = new System.Drawing.Size(24, 14);
            this.lblFile3.TabIndex = 24;
            this.lblFile3.Text = "File";
            this.lblFile3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFile5
            // 
            this.lblFile5.AutoSize = true;
            this.lblFile5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFile5.ForeColor = System.Drawing.Color.Black;
            this.lblFile5.Location = new System.Drawing.Point(12, 183);
            this.lblFile5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblFile5.Name = "lblFile5";
            this.lblFile5.Size = new System.Drawing.Size(24, 14);
            this.lblFile5.TabIndex = 36;
            this.lblFile5.Text = "File";
            this.lblFile5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFile2
            // 
            this.lblFile2.AutoSize = true;
            this.lblFile2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFile2.ForeColor = System.Drawing.Color.Black;
            this.lblFile2.Location = new System.Drawing.Point(12, 54);
            this.lblFile2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblFile2.Name = "lblFile2";
            this.lblFile2.Size = new System.Drawing.Size(24, 14);
            this.lblFile2.TabIndex = 18;
            this.lblFile2.Text = "File";
            this.lblFile2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFile4
            // 
            this.lblFile4.AutoSize = true;
            this.lblFile4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFile4.ForeColor = System.Drawing.Color.Black;
            this.lblFile4.Location = new System.Drawing.Point(12, 140);
            this.lblFile4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblFile4.Name = "lblFile4";
            this.lblFile4.Size = new System.Drawing.Size(24, 14);
            this.lblFile4.TabIndex = 30;
            this.lblFile4.Text = "File";
            this.lblFile4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFile1
            // 
            this.lblFile1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFile1.AutoSize = true;
            this.lblFile1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFile1.ForeColor = System.Drawing.Color.Black;
            this.lblFile1.Location = new System.Drawing.Point(12, 12);
            this.lblFile1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblFile1.Name = "lblFile1";
            this.lblFile1.Size = new System.Drawing.Size(24, 14);
            this.lblFile1.TabIndex = 12;
            this.lblFile1.Text = "File";
            this.lblFile1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(5, 8);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 250;
            this.TxtFile.Properties.ReadOnly = true;
            this.TxtFile.Size = new System.Drawing.Size(390, 20);
            this.TxtFile.TabIndex = 13;
            // 
            // ChkFile6
            // 
            this.ChkFile6.Location = new System.Drawing.Point(400, 222);
            this.ChkFile6.Name = "ChkFile6";
            this.ChkFile6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile6.Properties.Appearance.Options.UseFont = true;
            this.ChkFile6.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile6.Properties.Caption = " ";
            this.ChkFile6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile6.Size = new System.Drawing.Size(20, 22);
            this.ChkFile6.TabIndex = 44;
            this.ChkFile6.ToolTip = "Remove filter";
            this.ChkFile6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile6.ToolTipTitle = "Run System";
            this.ChkFile6.Click += new System.EventHandler(this.ChkFile6_CheckedChanged);
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(5, 31);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(390, 17);
            this.PbUpload.TabIndex = 17;
            // 
            // BtnDownload6
            // 
            this.BtnDownload6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload6.Appearance.Options.UseBackColor = true;
            this.BtnDownload6.Appearance.Options.UseFont = true;
            this.BtnDownload6.Appearance.Options.UseForeColor = true;
            this.BtnDownload6.Appearance.Options.UseTextOptions = true;
            this.BtnDownload6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload6.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload6.Image")));
            this.BtnDownload6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload6.Location = new System.Drawing.Point(459, 222);
            this.BtnDownload6.Name = "BtnDownload6";
            this.BtnDownload6.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload6.TabIndex = 46;
            this.BtnDownload6.ToolTip = "DownloadFile";
            this.BtnDownload6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload6.ToolTipTitle = "Run System";
            this.BtnDownload6.Click += new System.EventHandler(this.BtnDownload6_Click);
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile2.Image")));
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(428, 50);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(24, 21);
            this.BtnFile2.TabIndex = 21;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // PbUpload6
            // 
            this.PbUpload6.Location = new System.Drawing.Point(5, 246);
            this.PbUpload6.Name = "PbUpload6";
            this.PbUpload6.Size = new System.Drawing.Size(390, 17);
            this.PbUpload6.TabIndex = 47;
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(459, 50);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 22;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // BtnFile6
            // 
            this.BtnFile6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile6.Appearance.Options.UseBackColor = true;
            this.BtnFile6.Appearance.Options.UseFont = true;
            this.BtnFile6.Appearance.Options.UseForeColor = true;
            this.BtnFile6.Appearance.Options.UseTextOptions = true;
            this.BtnFile6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile6.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile6.Image")));
            this.BtnFile6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile6.Location = new System.Drawing.Point(428, 222);
            this.BtnFile6.Name = "BtnFile6";
            this.BtnFile6.Size = new System.Drawing.Size(24, 21);
            this.BtnFile6.TabIndex = 45;
            this.BtnFile6.ToolTip = "BrowseFile";
            this.BtnFile6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile6.ToolTipTitle = "Run System";
            this.BtnFile6.Click += new System.EventHandler(this.BtnFile6_Click);
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(428, 9);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 15;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // TxtFile6
            // 
            this.TxtFile6.EnterMoveNextControl = true;
            this.TxtFile6.Location = new System.Drawing.Point(5, 223);
            this.TxtFile6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile6.Name = "TxtFile6";
            this.TxtFile6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFile6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile6.Properties.Appearance.Options.UseFont = true;
            this.TxtFile6.Properties.MaxLength = 250;
            this.TxtFile6.Properties.ReadOnly = true;
            this.TxtFile6.Size = new System.Drawing.Size(390, 20);
            this.TxtFile6.TabIndex = 43;
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(5, 94);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 250;
            this.TxtFile3.Properties.ReadOnly = true;
            this.TxtFile3.Size = new System.Drawing.Size(390, 20);
            this.TxtFile3.TabIndex = 25;
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(400, 50);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 20;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // ChkFile5
            // 
            this.ChkFile5.Location = new System.Drawing.Point(400, 179);
            this.ChkFile5.Name = "ChkFile5";
            this.ChkFile5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile5.Properties.Appearance.Options.UseFont = true;
            this.ChkFile5.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile5.Properties.Caption = " ";
            this.ChkFile5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile5.Size = new System.Drawing.Size(20, 22);
            this.ChkFile5.TabIndex = 38;
            this.ChkFile5.ToolTip = "Remove filter";
            this.ChkFile5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile5.ToolTipTitle = "Run System";
            this.ChkFile5.Click += new System.EventHandler(this.ChkFile5_CheckedChanged);
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(459, 9);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 16;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // BtnDownload5
            // 
            this.BtnDownload5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload5.Appearance.Options.UseBackColor = true;
            this.BtnDownload5.Appearance.Options.UseFont = true;
            this.BtnDownload5.Appearance.Options.UseForeColor = true;
            this.BtnDownload5.Appearance.Options.UseTextOptions = true;
            this.BtnDownload5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload5.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload5.Image")));
            this.BtnDownload5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload5.Location = new System.Drawing.Point(459, 179);
            this.BtnDownload5.Name = "BtnDownload5";
            this.BtnDownload5.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload5.TabIndex = 40;
            this.BtnDownload5.ToolTip = "DownloadFile";
            this.BtnDownload5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload5.ToolTipTitle = "Run System";
            this.BtnDownload5.Click += new System.EventHandler(this.BtnDownload5_Click);
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(5, 51);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 250;
            this.TxtFile2.Properties.ReadOnly = true;
            this.TxtFile2.Size = new System.Drawing.Size(390, 20);
            this.TxtFile2.TabIndex = 19;
            // 
            // PbUpload5
            // 
            this.PbUpload5.Location = new System.Drawing.Point(5, 203);
            this.PbUpload5.Name = "PbUpload5";
            this.PbUpload5.Size = new System.Drawing.Size(390, 17);
            this.PbUpload5.TabIndex = 41;
            // 
            // BtnFile3
            // 
            this.BtnFile3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile3.Appearance.Options.UseBackColor = true;
            this.BtnFile3.Appearance.Options.UseFont = true;
            this.BtnFile3.Appearance.Options.UseForeColor = true;
            this.BtnFile3.Appearance.Options.UseTextOptions = true;
            this.BtnFile3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile3.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile3.Image")));
            this.BtnFile3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile3.Location = new System.Drawing.Point(428, 93);
            this.BtnFile3.Name = "BtnFile3";
            this.BtnFile3.Size = new System.Drawing.Size(24, 21);
            this.BtnFile3.TabIndex = 27;
            this.BtnFile3.ToolTip = "BrowseFile";
            this.BtnFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile3.ToolTipTitle = "Run System";
            this.BtnFile3.Click += new System.EventHandler(this.BtnFile3_Click);
            // 
            // BtnFile5
            // 
            this.BtnFile5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile5.Appearance.Options.UseBackColor = true;
            this.BtnFile5.Appearance.Options.UseFont = true;
            this.BtnFile5.Appearance.Options.UseForeColor = true;
            this.BtnFile5.Appearance.Options.UseTextOptions = true;
            this.BtnFile5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile5.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile5.Image")));
            this.BtnFile5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile5.Location = new System.Drawing.Point(428, 179);
            this.BtnFile5.Name = "BtnFile5";
            this.BtnFile5.Size = new System.Drawing.Size(24, 21);
            this.BtnFile5.TabIndex = 39;
            this.BtnFile5.ToolTip = "BrowseFile";
            this.BtnFile5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile5.ToolTipTitle = "Run System";
            this.BtnFile5.Click += new System.EventHandler(this.BtnFile5_Click);
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(5, 117);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(390, 17);
            this.PbUpload3.TabIndex = 29;
            // 
            // TxtFile5
            // 
            this.TxtFile5.EnterMoveNextControl = true;
            this.TxtFile5.Location = new System.Drawing.Point(5, 180);
            this.TxtFile5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile5.Name = "TxtFile5";
            this.TxtFile5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFile5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile5.Properties.Appearance.Options.UseFont = true;
            this.TxtFile5.Properties.MaxLength = 250;
            this.TxtFile5.Properties.ReadOnly = true;
            this.TxtFile5.Size = new System.Drawing.Size(390, 20);
            this.TxtFile5.TabIndex = 37;
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(400, 9);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 14;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload3.Image")));
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(459, 93);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload3.TabIndex = 28;
            this.BtnDownload3.ToolTip = "DownloadFile";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // ChkFile4
            // 
            this.ChkFile4.Location = new System.Drawing.Point(400, 136);
            this.ChkFile4.Name = "ChkFile4";
            this.ChkFile4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile4.Properties.Appearance.Options.UseFont = true;
            this.ChkFile4.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile4.Properties.Caption = " ";
            this.ChkFile4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile4.Size = new System.Drawing.Size(20, 22);
            this.ChkFile4.TabIndex = 32;
            this.ChkFile4.ToolTip = "Remove filter";
            this.ChkFile4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile4.ToolTipTitle = "Run System";
            this.ChkFile4.CheckedChanged += new System.EventHandler(this.ChkFile4_CheckedChanged);
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(5, 74);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(390, 17);
            this.PbUpload2.TabIndex = 23;
            // 
            // BtnDownload4
            // 
            this.BtnDownload4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload4.Appearance.Options.UseBackColor = true;
            this.BtnDownload4.Appearance.Options.UseFont = true;
            this.BtnDownload4.Appearance.Options.UseForeColor = true;
            this.BtnDownload4.Appearance.Options.UseTextOptions = true;
            this.BtnDownload4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload4.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload4.Image")));
            this.BtnDownload4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload4.Location = new System.Drawing.Point(459, 136);
            this.BtnDownload4.Name = "BtnDownload4";
            this.BtnDownload4.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload4.TabIndex = 34;
            this.BtnDownload4.ToolTip = "DownloadFile";
            this.BtnDownload4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload4.ToolTipTitle = "Run System";
            this.BtnDownload4.Click += new System.EventHandler(this.BtnDownload4_Click);
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(400, 93);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 26;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // PbUpload4
            // 
            this.PbUpload4.Location = new System.Drawing.Point(5, 160);
            this.PbUpload4.Name = "PbUpload4";
            this.PbUpload4.Size = new System.Drawing.Size(390, 17);
            this.PbUpload4.TabIndex = 35;
            // 
            // TxtFile4
            // 
            this.TxtFile4.EnterMoveNextControl = true;
            this.TxtFile4.Location = new System.Drawing.Point(5, 137);
            this.TxtFile4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile4.Name = "TxtFile4";
            this.TxtFile4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFile4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile4.Properties.Appearance.Options.UseFont = true;
            this.TxtFile4.Properties.MaxLength = 250;
            this.TxtFile4.Properties.ReadOnly = true;
            this.TxtFile4.Size = new System.Drawing.Size(390, 20);
            this.TxtFile4.TabIndex = 31;
            // 
            // BtnFile4
            // 
            this.BtnFile4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile4.Appearance.Options.UseBackColor = true;
            this.BtnFile4.Appearance.Options.UseFont = true;
            this.BtnFile4.Appearance.Options.UseForeColor = true;
            this.BtnFile4.Appearance.Options.UseTextOptions = true;
            this.BtnFile4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile4.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile4.Image")));
            this.BtnFile4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile4.Location = new System.Drawing.Point(428, 136);
            this.BtnFile4.Name = "BtnFile4";
            this.BtnFile4.Size = new System.Drawing.Size(24, 21);
            this.BtnFile4.TabIndex = 33;
            this.BtnFile4.ToolTip = "BrowseFile";
            this.BtnFile4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile4.ToolTipTitle = "Run System";
            this.BtnFile4.Click += new System.EventHandler(this.BtnFile4_Click);
            // 
            // Tp5
            // 
            this.Tp5.Appearance.Header.Options.UseTextOptions = true;
            this.Tp5.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp5.Controls.Add(this.Grd3);
            this.Tp5.Controls.Add(this.button2);
            this.Tp5.Name = "Tp5";
            this.Tp5.Size = new System.Drawing.Size(821, 280);
            this.Tp5.Text = "Upload File";
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 23);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(821, 257);
            this.Grd3.TabIndex = 49;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd3_RequestCellToolTipText);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.SteelBlue;
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.AliceBlue;
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(821, 23);
            this.button2.TabIndex = 48;
            this.button2.Text = "Attachment File";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmRecvVd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 473);
            this.Name = "FrmRecvVd";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdDONo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcRecvVd)).EndInit();
            this.TcRecvVd.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCustomsDocCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBSubmissionNo.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkKBNonDocInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackaging.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackagingQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBRegistrationNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPLNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBContractNo.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.Tp3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvExpeditionDocNo.Properties)).EndInit();
            this.Tp4.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile4.Properties)).EndInit();
            this.Tp5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        internal DevExpress.XtraEditors.TextEdit TxtVdDONo;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label LblRemark;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Panel panel4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.LookUpEdit LueBin;
        private DevExpress.XtraEditors.LookUpEdit LueLot;
        private System.Windows.Forms.Label LblSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private DevExpress.XtraTab.XtraTabControl TcRecvVd;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.TextEdit TxtKBRegistrationNo;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtKBPLNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtKBContractNo;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteKBContractDt;
        public DevExpress.XtraEditors.SimpleButton BtnKBContractNo;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.DateEdit DteKBPLDt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.DateEdit DteKBRegistrationDt;
        internal DevExpress.XtraEditors.LookUpEdit LueVdCode;
        internal DevExpress.XtraEditors.TextEdit TxtKBPackaging;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtKBPackagingQty;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.CheckEdit ChkKBNonDocInd;
        private System.Windows.Forms.Panel panel7;
        internal DevExpress.XtraEditors.TextEdit TxtKBSubmissionNo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.LookUpEdit LueCustomsDocCode;
        internal DevExpress.XtraEditors.LookUpEdit LueCCCode;
        private System.Windows.Forms.Label LblCCCode;
        private System.Windows.Forms.Panel panel8;
        internal DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label LblDeptCode;
        internal DevExpress.XtraEditors.LookUpEdit LueEntCode;
        private System.Windows.Forms.Label LblEntCode;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        public DevExpress.XtraEditors.SimpleButton BtnRecvExpeditionDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnRecvExpeditionDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtRecvExpeditionDocNo;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.DateEdit DteExpiredDt;
        private DevExpress.XtraEditors.LookUpEdit LueProductionWorkGroup;
        private System.Windows.Forms.Label LblProductionWorkGroup;
        private DevExpress.XtraTab.XtraTabPage Tp4;
        private System.Windows.Forms.Label lblFile3;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label lblFile1;
        private System.Windows.Forms.Label lblFile2;
        private System.Windows.Forms.ProgressBar PbUpload2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        private System.Windows.Forms.ProgressBar PbUpload3;
        public DevExpress.XtraEditors.SimpleButton BtnFile3;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        private System.Windows.Forms.ProgressBar PbUpload;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.Label lblFile6;
        private DevExpress.XtraEditors.CheckEdit ChkFile6;
        public DevExpress.XtraEditors.SimpleButton BtnDownload6;
        private System.Windows.Forms.ProgressBar PbUpload6;
        public DevExpress.XtraEditors.SimpleButton BtnFile6;
        internal DevExpress.XtraEditors.TextEdit TxtFile6;
        private System.Windows.Forms.Label lblFile5;
        private DevExpress.XtraEditors.CheckEdit ChkFile5;
        public DevExpress.XtraEditors.SimpleButton BtnDownload5;
        private System.Windows.Forms.ProgressBar PbUpload5;
        public DevExpress.XtraEditors.SimpleButton BtnFile5;
        internal DevExpress.XtraEditors.TextEdit TxtFile5;
        private System.Windows.Forms.Label lblFile4;
        private DevExpress.XtraEditors.CheckEdit ChkFile4;
        public DevExpress.XtraEditors.SimpleButton BtnDownload4;
        private System.Windows.Forms.ProgressBar PbUpload4;
        public DevExpress.XtraEditors.SimpleButton BtnFile4;
        internal DevExpress.XtraEditors.TextEdit TxtFile4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraTab.XtraTabPage Tp5;
        private System.Windows.Forms.Button button2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
    }
}