﻿#region Update
/*
    24/07/2017 [TKG] merubah posisi salary untuk IOK
    09/10/2017 [ARI] Tambah printout
    21/12/2017 [TKG] tambah informasi kekurangan pajak akhir tahun
    19/05/2020 [TKG/IOK] tambah informasi brutto
    03/08/2022 [HPH/IOK] Menambahkan kolom Bank Name
    05/09/2022 [HPH/IOK] kolom bank payroll diambil dari VR for Payroll
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPayrollProcessSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        internal string mSalaryInd = "1", mRptPayrollProcessSummaryVersion ="1";
        private bool 
            mIsNotFilterByAuthorization = false,
            mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmRptPayrollProcessSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd(); 
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
               
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mRptPayrollProcessSummaryVersion = Sm.GetParameter("RptPayrollProcessSummaryVersion");
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, B.PayrunName, A.EmpCode, C.EmpName, ");
            SQL.AppendLine("C.EmpCodeOld, E.PosName, D.DeptName, C.JoinDt, C.ResignDt, ");
            SQL.AppendLine("F.OptDesc As SystemTypeDesc, G.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("H.PGName, J.SiteName, A.NPWP, I.OptDesc As NonTaxableIncomeDesc, ");
            SQL.AppendLine("A.Salary, A.WorkingDay, A.PLDay, A.PLHr, ");
            SQL.AppendLine("A.PLAmt, A.ProcessPLAmt, A.UPLDay, A.UPLHr, A.UPLAmt, A.ProcessUPLAmt, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, ");
            SQL.AppendLine("A.OT1Amt, A.OT2Amt, A.OTHolidayAmt, A.TaxableFixAllowance, A.NonTaxableFixAllowance, A.FixAllowance, A.EmploymentPeriodAllowance, ");
            SQL.AppendLine("A.IncEmployee, A.IncMinWages, A.IncProduction, A.IncPerformance, A.PresenceReward, ");
            SQL.AppendLine("A.HolidayEarning, A.ExtraFooding, A.Meal, A.Transport, A.FieldAssignment, A.SSEmployerHealth, A.SSEmployerEmployment, A.SSEmployerPension, A.NonTaxableFixDeduction, A.TaxableFixDeduction, A.FixDeduction, ");
            SQL.AppendLine("A.DedEmployee, A.DedProduction, A.DedProdLeave, A.EmpAdvancePayment, A.SSEmployeeHealth, A.SSEmployeeEmployment, A.SSEmployeePension, ");
            SQL.AppendLine("A.SalaryAdjustment, A.Tax, A.EOYTax, A.Amt, A.VoucherRequestPayrollDocNo, ");
            SQL.AppendLine("(IfNull(A.OT1Hr,0)+IfNull(OT2Hr,0)+IfNull(OTHolidayHr,0))As OTTotalHr, (IfNull(A.OT1Amt,0)+IfNull(OT2Amt,0)+IfNull(OTHolidayAmt,0))As OTTotalAmt, ");
            SQL.AppendLine("(IfNull(A.Salary,0)+ IfNull(A.ProcessPLAmt,0)-IfNull(A.ProcessUPLAmt,0)+ IfNull(A.OT1Amt,0)+ IfNull(A.OT2Amt,0)+ ");
            SQL.AppendLine("IfNull(A.OTHolidayAmt,0)+ IfNull(A.TaxableFixAllowance,0)+ IfNull(A.EmploymentPeriodAllowance,0)+ IfNull(A.IncEmployee,0)+ ");
            SQL.AppendLine("IfNull(A.IncMinWages,0)+ IfNull(A.IncProduction,0)+ IfNull(A.IncPerformance,0)+ IfNull(A.PresenceReward,0)+ IfNull(A.HolidayEarning,0)+ ");
            SQL.AppendLine("IfNull(A.ExtraFooding,0)+ IfNull(A.SSEmployerHealth,0)+ IfNull(A.SSEmployerEmployment,0)+ IfNull(A.SalaryAdjustment,0)- ");
            SQL.AppendLine("IfNull(A.TaxableFixDeduction,0) - IfNull(A.DedEmployee,0)- IfNull(A.DedProduction,0)- IfNull(A.DedProdLeave,0)- IfNull(A.SSEmployeeEmployment,0)) As Bruto, N.BankName ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun B ");
            SQL.AppendLine("    On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode=E.PosCode ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Inner Join TblOption G On B.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr H On B.PGCode=H.PGCode ");
            SQL.AppendLine("Left Join TblOption I On A.PTKP=I.OptCode And I.OptCat='NonTaxableIncome' ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Left JOIN tblvoucherrequestpayrollhdr K ON K.DocNo = A.VoucherRequestPayrollDocNo");
            SQL.AppendLine("Left JOIN tblvoucherrequesthdr L ON L.DocNo = K.VoucherRequestDocNo");
            SQL.AppendLine("Left JOIN tblvoucherhdr M ON M.DocNo = L.VoucherDocNo");
            SQL.AppendLine("Left JOIN tblbank N ON K.BankCode = N.BankCode");
            SQL.AppendLine("Where 0=0 ");
            
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
           
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 65;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Payrun's Code",
                        "Payrun's Name",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",

                        //6-10
                        "Old Code",
                        "Position",
                        "Department",
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",
                        
                        //11-15
                        "Type",
                        "Period",
                        "Group",
                        "Site",
                        "NPWP",

                        //16-20
                        "PTKP",
                        "Salary", 
                        "Working Day", 
                        "Paid Leave"+Environment.NewLine+"(Day)", 
                        "Paid Leave"+Environment.NewLine+"(Hour)",  
                        
                        //21-25
                        "Paid Leave"+Environment.NewLine+"(Amount)", 
                        "Processed Paid"+Environment.NewLine+"Leave (Amount)", 
                        "Unpaid Leave"+Environment.NewLine+"(Day)", 
                        "Unpaid Leave"+Environment.NewLine+"(Hour)",  
                        "Unpaid Leave"+Environment.NewLine+"(Amount)", 
                        
                        //26-30
                        "Processed Unpaid"+Environment.NewLine+"Leave (Amount)", 
                        "OT 1"+Environment.NewLine+"(Hour)", 
                        "OT 2"+Environment.NewLine+"(Hour)", 
                        "OT Holiday"+Environment.NewLine+"(Hour)", 
                        "OT 1"+Environment.NewLine+"(Amount)", 
                        
                        //31-35
                        "OT 2"+Environment.NewLine+"(Amount)", 
                        "OT Holiday"+Environment.NewLine+"(Amount)", 
                        "Taxable Fixed"+Environment.NewLine+"Allowance", 
                        "Non Taxable Fixed"+Environment.NewLine+"Allowance", 
                        "Fix"+Environment.NewLine+"Allowance", 
                        
                        //36-40
                        "Employment"+Environment.NewLine+"Period Allowance", 
                        "Employee's"+Environment.NewLine+"Incentive",
                        "Min Wages"+Environment.NewLine+"Incentive", 
                        "Production"+Environment.NewLine+"Incentive", 
                        "Performance"+Environment.NewLine+"Incentive", 
                        
                        //41-45
                        "Premi"+Environment.NewLine+"Hadir", 
                        "Holiday"+Environment.NewLine+"Earning", 
                        "Extra"+Environment.NewLine+"Fooding", 
                        "Meal",
                        "Transport",
                        
                        //46-50
                        "Field"+Environment.NewLine+"Assignment",
                        "SS Employer"+Environment.NewLine+"Health", 
                        "SS Employer"+Environment.NewLine+"Employment",
                        "Non Taxable Employee's"+Environment.NewLine+"Deduction", 
                        "Taxable Employee's"+Environment.NewLine+"Deduction", 
                        
                        //51-55
                        "Fixed"+Environment.NewLine+"Deduction", 
                        "Employee's"+Environment.NewLine+"Deduction", 
                        "Production's"+Environment.NewLine+"Deduction", 
                        "Production's"+Environment.NewLine+"Leave Deduction", 
                        "Employee's"+Environment.NewLine+"Advance Payment",
                        
                        //56-60
                        "SS Employee"+Environment.NewLine+"Health", 
                        "SS Employee"+Environment.NewLine+"Employment", 
                        "Salary"+Environment.NewLine+"Adjustment", 
                        "Tax", 
                        "End of Year"+Environment.NewLine+"Tax", 

                        //61-64
                        "Take Home Pay",
                        "Bruto",
                        "Voucher Request#"+Environment.NewLine+"(Payroll)",
                        "Bank Payroll"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 130, 200, 80, 200, 
                        
                        //6-10
                        80, 150, 150, 100, 100, 
                        
                        //11-15
                        100, 100, 100, 150, 130, 
                        
                        //16-20
                        100, 100, 100, 100, 100,  
                        
                        //21-25
                        100, 100, 100, 100, 100, 
                        
                        //26-30
                        180, 100, 100, 100, 100, 

                        //31-35
                        100, 100, 130, 130, 100, 

                        //36-40
                        100, 100, 100, 100, 100, 

                        //41-45
                        100, 100, 100, 100, 100, 

                        //46-50
                        100, 100, 100, 130, 130, 
                        
                        //51-55
                        100, 100, 100, 100, 130, 
                        
                        //56-60
                        100, 100, 100, 100, 120, 
                        
                        //61-64
                        130, 130, 130, 180
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 
                17, 18, 19, 20, 
                21, 22, 23, 24, 25, 
                26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 
                36, 37, 38, 39, 40, 
                41, 42, 43, 44, 45, 
                46, 47, 48, 49, 50, 
                51, 52, 53, 54, 55, 
                56, 57, 58, 59, 60,
                61, 62
            }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 9, 10 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] 
            { 
                0, 2, 3, 4, 5, 
                6, 7, 8, 9, 10, 
                11, 12, 13, 14, 15, 
                16, 17, 18, 19, 20, 
                21, 22, 23, 24, 25, 
                26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 
                36, 37, 38, 39, 40, 
                41, 42, 43, 44, 45, 
                46, 47, 48, 49, 50,
                51, 52, 53, 54, 55, 
                56, 57, 58, 59, 60,
                61, 62, 63, 64
            });
            Grd1.Cols[64].Move(15);
        }

        override protected void HideInfoInGrd()
        {
            if (mRptPayrollProcessSummaryVersion == "2")
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 44, 46, 48 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtPayCod.Text, new string[] { "A.PayrunCode", "B.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By A.PayrunCode, C.EmpName;",
                        new string[]
                        {
                            //0
                            "PayrunCode",
                        
                            //1-5
                            "PayrunName",
                            "EmpCode",
                            "EmpName",
                            "EmpCodeOld",
                            "PosName",
                            
                            //6-10
                            "DeptName",
                            "JoinDt",
                            "ResignDt",
                            "SystemTypeDesc", 
                            "PayrunPeriodDesc",
                            
                            //11-15
                            "PGName",
                            "SiteName",
                            "NPWP",
                            "NonTaxableIncomeDesc",
                            "Salary", 

                            //16-20
                            "WorkingDay", 
                            "PLDay", 
                            "PLHr", 
                            "PLAmt", 
                            "ProcessPLAmt", 
                            
                            //21-25
                            "UPLDay",
                            "UPLHr", 
                            "UPLAmt", 
                            "ProcessUPLAmt",
                            "OT1Hr", 
                            
                            //26-30
                            "OT2Hr", 
                            "OTHolidayHr", 
                            "OT1Amt", 
                            "OT2Amt", 
                            "OTHolidayAmt", 
                            
                            //31-35
                            "TaxableFixAllowance",
                            "NonTaxableFixAllowance",
                            "FixAllowance", 
                            "EmploymentPeriodAllowance", 
                            "IncEmployee", 
                            
                            //36-40
                            "IncMinWages", 
                            "IncProduction",
                            "IncPerformance", 
                            "PresenceReward", 
                            "HolidayEarning", 
                            
                            //41-45
                            "ExtraFooding", 
                            "Meal",
                            "Transport",
                            "FieldAssignment", 
                            "SSEmployerHealth", 
                            
                            //46-50
                            "SSEmployerEmployment", 
                            "NonTaxableFixDeduction",
                            "TaxableFixDeduction",
                            "FixDeduction", 
                            "DedEmployee", 
                            
                            //51-55
                            "DedProduction", 
                            "DedProdLeave", 
                            "EmpAdvancePayment",
                            "SSEmployeeHealth", 
                            "SSEmployeeEmployment", 
                            
                            //56-60
                            "SalaryAdjustment", 
                            "Tax", 
                            "EOYTax",
                            "Amt",
                            "Bruto",

                            //61-62
                            "VoucherRequestPayrollDocNo",
                            "BankName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 28);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 29);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 32);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 33);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 34);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 35);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 36);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 37);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 38);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 39);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 40);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 41);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 42);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 43);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 46, 44);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 45);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 48, 46);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 49, 47);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 50, 48);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 51, 49);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 52, 50);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 53, 51);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 54, 52);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 55, 53);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 56, 54);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 57, 55);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 58, 56);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 59, 57);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 60, 58);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 61, 59);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 62, 60);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 63, 61);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 64, 62);
                        }, true, false, false, false
                    );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                    { 
                        17, 18, 19, 20, 
                        21, 22, 23, 24, 25, 
                        26, 27, 28, 29, 30, 
                        31, 32, 33, 34, 35, 
                        36, 37, 38, 39, 40, 
                        41, 42, 43, 44, 45, 
                        46, 47, 48, 49, 50, 
                        51, 52, 53, 54, 55, 
                        56, 57, 58, 59, 60,
                        61, 62
                    });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
            { 
                17, 18, 19, 20, 
                21, 22, 23, 24, 25, 
                26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 
                36, 37, 38, 39, 40, 
                41, 42, 43, 44, 45, 
                46, 47, 48, 49, 50, 
                51, 52, 53, 54, 55, 
                56, 57, 58, 59, 60,
                61, 62
            });
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length > 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    Sm.FormShowDialog(
                        new FrmRptPayrollProcessSummaryDlg(
                            this,
                            Sm.GetGrdStr(Grd1, e.RowIndex, 2),
                            Sm.GetGrdStr(Grd1, e.RowIndex, 4)
                            ));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length > 0)
            {
                Sm.FormShowDialog(
                    new FrmRptPayrollProcessSummaryDlg(
                        this,
                        Sm.GetGrdStr(Grd1, e.RowIndex, 2),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 4)
                        ));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "No data displayed");
                return true;
            }
            return false;
        }

        override protected void PrintData()
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint()
        {
            if (IsGrdEmpty() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<PayrollProcess>();
            var ldtl = new List<PayrollProcessDtl>();
            string[] TableName = { "PayrollProcess", "PayrollProcessDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where Menucode=@MenuCode) As MenuDesc ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                        "CompanyLogo",

                         //1-5
                        "CompanyName",
                        "CompanyAddress",
                        "CompanyPhone",
                        "MenuDesc",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PayrollProcess()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            MenuDesc = Sm.DrStr(dr, c[4]),
                            UserName = Gv.CurrentUserCode,
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.PayrunCode, B.PayrunName, C.EmpName, C.EmpCodeOld, D.DeptName, A.SSEmployerHealth, A.SSEmployerEmployment, A.SSEmployeeHealth, ");
                SQLDtl.AppendLine("A.SSEmployeeEmployment, A.Amt, (A.SSEmployerHealth+A.SSEmployerEmployment+A.SSEmployeeHealth+A.SSEmployeeEmployment+A.Amt)As TotalTHPBPJS, ");
                SQLDtl.AppendLine("(A.SSEmployerEmployment+A.SSEmployeeEmployment)As TotBPJSKet ");
                SQLDtl.AppendLine("From tblpayrollprocess1 A ");
                SQLDtl.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
                SQLDtl.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
                SQLDtl.AppendLine("Left Join TblDepartment D On B.DeptCode = D.DeptCode ");
                SQLDtl.AppendLine("Where A.PayrunCode in ( ");

                int x = Grd1.Rows.Count-1;
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (i == x)
                    {
                        SQLDtl.AppendLine("'" + Sm.GetGrdStr(Grd1, i, 2) + "'");

                    }
                    else
                    {
                        SQLDtl.AppendLine("'" + Sm.GetGrdStr(Grd1, i, 2) + "'");
                        SQLDtl.AppendLine(", ");
                    }
                }
                SQLDtl.AppendLine(" );");

                cmDtl.CommandText = SQLDtl.ToString();
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    //0
                    "PayrunCode",

                    //1-5
                    "PayrunName",
                    "EmpName",
                    "EmpCodeOld",
                    "DeptName",
                    "SSEmployerHealth",

                    //6-10
                    "SSEmployerEmployment",
                    "SSEmployeeHealth",
                    "SSEmployeeEmployment",
                    "Amt",
                    "TotalTHPBPJS",

                    //11
                    "TotBPJSKet",
                   
                });

                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new PayrollProcessDtl()
                        {
                            nomor = nomor,
                            PayrunCode = Sm.DrStr(drDtl, cDtl[0]),

                            PayrunName = Sm.DrStr(drDtl, cDtl[1]),
                            EmpName = Sm.DrStr(drDtl, cDtl[2]),
                            EmpCodeOld = Sm.DrStr(drDtl, cDtl[3]),
                            DeptName = Sm.DrStr(drDtl, cDtl[4]),
                            SSEmployerHealth = Sm.DrDec(drDtl, cDtl[5]),

                            SSEmployerEmployment = Sm.DrDec(drDtl, cDtl[6]),
                            SSEmployeeHealth = Sm.DrDec(drDtl, cDtl[7]),
                            SSEmployeeEmployment = Sm.DrDec(drDtl, cDtl[8]),
                            Amt = Sm.DrDec(drDtl, cDtl[9]),
                            TotalTHPBPJS = Sm.DrDec(drDtl, cDtl[10]),

                            TotBPJSKet = Sm.DrDec(drDtl, cDtl[11]),
                        });
                    }
                }

                drDtl.Close();
            }

            myLists.Add(ldtl);

            #endregion

            Sm.PrintReport("PayrollProcess", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtPayCod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayCod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        #endregion

        #endregion

        #region Report Class

        class PayrollProcess
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string MenuDesc { get; set; }
            public string UserName { get; set; }
            public string PrintBy { get; set; }

        }

        class PayrollProcessDtl
        {
            public int nomor { get; set; }
            public string PayrunCode { get; set; }
            public string PayrunName { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public decimal SSEmployerHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal Amt { get; set; }
            public decimal TotalTHPBPJS { get; set; }
            public decimal TotBPJSKet { get; set; }
        }

        #endregion
    }
}
