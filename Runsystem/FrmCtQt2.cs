﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
#endregion

namespace RunSystem
{
    public partial class FrmCtQt2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mMainCurCode = string.Empty;
        internal FrmCtQt2Find FrmFind;
        decimal mTotalAmt = 0m, mTotalMaterialCost = 0m, mTotalLaborCost = 0, mGrandTotal = 0m;

        #endregion

        #region Constructor

        public FrmCtQt2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Customer Quotation";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetGrd();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TxtDocNo, TxtExcRate, TxtTotalAmt, TxtTotalMaterialCost, TxtTotalLaborCost,
                    TxtGrandTotal
                }, true);
                SetFormControl(mState.View);

                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLuePtCode(ref LuePtCode);
                Sl.SetLueSPCode(ref LueSPCode);

                mMainCurCode = Sm.GetParameter("MainCurCode");

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    //ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "", 
                        "Job Costing#",
                        "",
                        "Item Code",
                        "",
                        
                        //6-10
                        "Item Name",
                        "Qty",
                        "UoM",
                        "Price#",
                        "",
                        
                        //11-15
                        "Price DNo",
                        "Unit Price (Org)",
                        "Unit Price",
                        "Cost 1 (Org)",
                        "Material Cost",
                        
                        //16-20
                        "Cost 2 (Org)",
                        "Labor Cost",
                        "Cost 3 (Org)",
                        "Other Cost",
                        "Remark"
                    },
                     new int[] 
                    {
                        0, 
                        20, 130, 20, 80, 20, 
                        250, 100, 100, 100, 20, 
                        0, 0, 100, 0, 100, 
                        0, 100, 0, 0, 400
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3, 5, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 12, 13, 14, 15, 16, 17, 18, 19 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5, 9, 10, 11, 12, 14, 16, 18, 19 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 9, 10 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, DteDocDt, LueCtCode, LueCtContactPersonName, LueSPCode, 
                        LuePtCode, LueCurCode, MeeRemark, TxtAdditionalCost
                    }, true);
                    BtnCtContactPersonName.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 20 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, LueSPCode, LuePtCode, LueCurCode, 
                        MeeRemark, TxtAdditionalCost
                    }, false);
                    BtnCtContactPersonName.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 20 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mTotalAmt = 0m;
            mTotalMaterialCost = 0m;
            mTotalLaborCost = 0; 
            mGrandTotal = 0m;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, ChkCancelInd, DteDocDt, LueCtCode, LueCtContactPersonName, 
                LueSPCode, LuePtCode, LueCurCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtExcRate, TxtTotalAmt, TxtTotalMaterialCost, TxtTotalLaborCost, TxtAdditionalCost,
                TxtGrandTotal
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 12, 13, 14, 15, 16, 17, 18, 19 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCtQt2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                //if (TxtDocNo.Text.Length == 0)
                //    InsertData();
                //else
                //    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1 && !Sm.IsDteEmpty(DteDocDt, "Date"))
                    {
                        e.DoDefault = false;
                        //if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmCtQt2Dlg(this, e.RowIndex, decimal.Parse(TxtExcRate.Text)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 1, 20 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 12, 13, 14, 15, 16, 17, 18, 19 });
                    }
                }
                else
                {
                    if (
                        e.ColIndex == 1 &&
                        (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmJC(mMenuCode);
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 4));
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItemPrice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            //if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 1 && !Sm.IsDteEmpty(DteDocDt, "Date"))
            //    Sm.FormShowDialog(new FrmCtQt2Dlg(this, e.RowIndex, decimal.Parse(TxtExcRate.Text)));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmJC(mMenuCode);
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 4));

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItemPrice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        #endregion

        #region Additional Method

        private void SetExcRate()
        {
            TxtExcRate.EditValue = Sm.FormatNum(0m, 0);

            if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                TxtExcRate.EditValue = Sm.FormatNum(1, 0);
                return;
            }

            if (!(mMainCurCode.Length!=0 && Sm.GetLue(LueCurCode).Length!=0 && Sm.GetDte(DteDocDt).Length!=0))
                return;
            
            string Value = string.Empty;
            try
            {
                Value = GetExcRate1();
                if (Value.Length != 0)
                    TxtExcRate.EditValue = Sm.FormatNum(decimal.Parse(Value), 0);
                else
                {
                    Value = GetExcRate2();
                    if (Value.Length != 0)
                    {
                        if (decimal.Parse(Value)!=0)
                            TxtExcRate.EditValue = Sm.FormatNum(Math.Round(1m/decimal.Parse(Value), 6), 0);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private string GetExcRate1()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select Amt From TblCurrencyRate Where CurCode1=@CurCode1 And CurCode2=@CurCode2 And RateDt<=@DocDt Order By RateDt Desc Limit 1;"
            };
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode1", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode2", Sm.GetLue(LueCurCode));

            return Sm.GetValue(cm);
        }

        private string GetExcRate2()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select Amt From TblCurrencyRate Where CurCode1=@CurCode1 And CurCode2=@CurCode2 And RateDt<=@DocDt Order By RateDt Desc Limit 1;"
            };
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode2", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode1", Sm.GetLue(LueCurCode));

            return Sm.GetValue(cm);
        }

        private void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode= '" + CtCode + "' Order By ContactPersonName",
                "Contact Person Name");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

            if (Sm.GetLue(LueCtCode).Length == 0)
            {
                LueCtContactPersonName.EditValue = null;
                Sm.SetControlReadOnly(LueCtContactPersonName, true);
            }
            else
            {
                SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode));
                Sm.SetControlReadOnly(LueCtContactPersonName, false);
            }
            Sm.ClearGrd(Grd1, true);
        }

        private void LueCtContactPersonName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtContactPersonName, new Sm.RefreshLue2(SetLueCtPersonCode), Sm.GetLue(LueCtCode));
        }

        private void BtnCtContactPersonName_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                var f = new FrmCustomer(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mMenuCode = "XXX";
                f.mCtCode = Sm.GetLue(LueCtCode);
                f.ShowDialog();
                SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode));
            }
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(Sl.SetLueSPCode));
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            SetExcRate();
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
            SetExcRate();
        }

        #endregion

        #endregion
    }
}
