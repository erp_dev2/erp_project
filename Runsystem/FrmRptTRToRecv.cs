﻿#region Update
/*
    13/07/2017 [WED] DOWhs Transfer Request yg di cancel tidak muncul
    17/03/2019 [TKG] kalau saat DO otomatis meng-complete transfer request baik quantity diproses semuanya maupun sebagian, maka reporting ini saat menampilkan quantity transfer request apabila sudah di-DO-kan akan menampilkan quantity DO.
    19/08/2021 [VIN/IMS] po service ditampilkan
    
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptTRToRecv : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mDOWhs2FulfillTransferRequestType = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private bool mIsFilterByItCt = false; 

        #endregion

        #region Constructor

        public FrmRptTRToRecv(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                GetParameter();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -3);
                Sl.SetLueWhsCode(ref LueWhsCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IFNULL(H.DocNo, A.DocNo) DocNo, A.DocDt, B.DNo, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("When 'O' Then 'Outstanding' ");
            SQL.AppendLine("When 'A' then 'Approve' ");
            SQL.AppendLine("When 'C' then 'Cancelled'  ");
            SQL.AppendLine("End As StatusTR, D.WhsName As WhsFrom, E.WhsName As WhsTo, ");
            SQL.AppendLine("B.ItCode, F.ItGrpCode, F.Itname, ifnull(C.BatchNo, '') As BatchNo, ");
            if (Sm.CompareStr(mDOWhs2FulfillTransferRequestType, "2"))
            {
                SQL.AppendLine("Case When C.DODocNo Is Null Then B.Qty Else IfNull(C.QtyDO, B.Qty) End As Qty, ");
                SQL.AppendLine("Case When C.DODocNo Is Null Then B.Qty2 Else IfNull(C.Qty2DO, B.Qty2) End As Qty2, ");
                SQL.AppendLine("Case When C.DODocNo Is Null Then B.Qty3 Else IfNull(C.Qty3DO, B.Qty3) End As Qty3, ");
            }
            else
                SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.DODocNo, ");
            SQL.AppendLine("C.QtyDO, C.Qty2DO, C.Qty3DO, ");
            SQL.AppendLine("C.RecvDocNo, ");
            SQL.AppendLine("C.QtyRecv, C.Qty2Recv, C.Qty3Recv,");
            SQL.AppendLine("F.InventoryUomCode, F.InventoryUomCode2, F.InventoryUomCode3, A.Remark ");
            SQL.AppendLine("From TblTransferRequestWhsHdr A ");
            SQL.AppendLine("Inner Join TblTransferRequestWhsDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select ifnull(T5.DocNo, T1.DocNo) As DODocNo, T1.TransferRequestWhsDocno, ");
            SQL.AppendLine("    T2.ItCode, T2.BatchNo, T2.Qty As QtyDO, T2.Qty2 As Qty2DO, T2.Qty3 As Qty3DO, ");
            SQL.AppendLine("    T4.RecvDocNo, T4.QtyRecv, T4.Qty2Recv, T4.Qty3Recv ");
            SQL.AppendLine("    From TblDOWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblTransferRequestWhsHdr T3 ");
            SQL.AppendLine("        On T1.TransferRequestWhsDocno=T3.DocNo ");
            SQL.AppendLine("        And T3.CancelInd = 'N' ");
            SQL.AppendLine("        And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Left Join ( ");
	        SQL.AppendLine("        Select X1.DoWhsDocNo, X2.Source, ");
            SQL.AppendLine("        Group_Concat(Distinct X1.DocNo Separator ', ') As RecvDocNo, ");
            SQL.AppendLine("        Sum(X1.Qty) As QtyRecv, Sum(X1.Qty2) As Qty2Recv, Sum(X1.Qty3) As Qty3Recv ");
            SQL.AppendLine("        From TblRecvWhs4Dtl X1 ");
            SQL.AppendLine("        Inner Join TblDOWhsDtl X2 On X1.DOWhsDocNo=X2.DocNo And X1.DOWhsDNo=X2.DNo And X2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblDOWhsHdr X3 On X2.DocNo=X3.DocNo ");
            SQL.AppendLine("        Inner Join TblTransferRequestWhsHdr X4 ");
            SQL.AppendLine("            On X3.TransferRequestWhsDocno=X4.DocNo ");
            SQL.AppendLine("            And X4.CancelInd = 'N' ");
            SQL.AppendLine("            And X4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where X1.CancelInd = 'N' ");
            SQL.AppendLine("        Group BY X1.DoWhsDocNo, X2.Source ");
            SQL.AppendLine("    ) T4 On T1.DocNo=T4.DOWhsDocNo And T2.Source=T4.Source ");
            SQL.AppendLine("    Left Join tbldowhs4dtl T5 ON T5.DOWhsDocNo=T2.DocNo AND T5.DOWhsDNo=T2.DNo ");

            SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            SQL.AppendLine(") C On C.TransferRequestWhsDocNo=B.DocNo And B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblWarehouse D On A.WhsCode = D.WhsCode  ");
            SQL.AppendLine("Left Join TblWarehouse E On A.WhsCode2 = E.WhsCode  ");
            SQL.AppendLine("LEFT JOIN tbldowhs4hdr G ON C.DODocNo=G.DocNo ");
            SQL.AppendLine("LEFT JOIN tbltransferrequestprojecthdr H ON G.TransferRequestProjectDocNo=H.DocNo ");
 
            SQL.AppendLine("Inner Join TblItem F On B.ItCode = F.ItCode  ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Requested#",
                    "Date",
                    "DNo",
                    "Status",
                    "",
                   
                    //6-10
                    "From",
                    "To",
                    "Item's Code",
                    "Group",
                    "Item's Name",
                   
                    //11-15
                    "Requested"+Environment.NewLine+"Quantity",
                    "UoM",
                    "Requested"+Environment.NewLine+"Quantity(2)",
                    "UoM",
                    "Requested"+Environment.NewLine+"Quantity(3)",

                    //16-20
                    "UoM",
                    "DO#",
                    "DO"+Environment.NewLine+"Quantity",
                    "UoM",
                    "DO"+Environment.NewLine+"Quantity(2)",

                    //21-25
                    "UoM",
                    "DO"+Environment.NewLine+"Quantity(3)",
                    "UoM",
                    "Received#",
                    "Received"+Environment.NewLine+"Quantity",

                    //26-30
                    "UoM",
                    "Received"+Environment.NewLine+"Quantity(2)",
                    "UoM",
                    "Received"+Environment.NewLine+"Quantity(3)",
                    "UoM",

                    //31-32
                    "Remark",
                    "Batch#"
                },
                new int[] 
                {
                    //0
                    30,

                    //1-5
                    140, 100, 60, 100, 20,  
                    
                    //6-10
                    200, 200, 100, 100, 250, 

                    //11-15
                    100, 60, 100, 60, 100, 
                    
                    //16-20
                    60, 150, 100, 60, 100, 
                    
                    //21-25
                    60, 100, 60, 150, 100, 
                    
                    //26-30
                    60, 100, 60, 100, 60,

                    //31-32
                    150, 230
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 15, 18, 20, 22, 25, 27, 29 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 });
            Grd1.Cols[32].Move(11);
            Sm.GrdColInvisible(Grd1, new int[] 
            { 
                3, 5, 8, 9, 
                13, 14, 15, 16, 
                20, 21, 22, 23,
                27, 28, 29, 30
             }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                //Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "F.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "G.TransferRequestProjectDocNo" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, string.Concat(mSQL, Filter, " Order By A.DocDt, A.DocNo;"),
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "DNo", "StatusTR", "WhsFrom", "WhsTo",  
                            
                            //6-10
                            "ItCode", "ItGrpCode", "Itname", "Qty", "Qty2",  
                            
                            //11-15
                            "Qty3", "DODocNo", "QtyDO", "Qty2DO", "Qty3DO", 
                            
                            //16-20
                            "RecvDocNo", "QtyRecv", "Qty2Recv", "Qty3Recv", "InventoryUomCode",

                            //21-24
                            "InventoryUomCode2", "InventoryUomCode3", "Remark", "BatchNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 24);
                           
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(10);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 18, 20, 22, 25, 27, 29 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                ShowApprovalInfo(e.RowIndex);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                ShowApprovalInfo(e.RowIndex);
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mDOWhs2FulfillTransferRequestType = Sm.GetParameter("DOWhs2FulfillTransferRequestType");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);

            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] 
                { 
                    13, 14, 
                    20, 21, 
                    27, 28
                }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] 
                    { 
                        13, 14, 15, 16, 
                        20, 21, 22, 23,
                        27, 28, 29, 30
                    }, true);
            }
        }

        private void ShowApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='TransferRequestWhs' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo ");
            SQL.AppendLine("Order By ApprovalDNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 1));
          
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                         new string[] 
                        { 
                            "UserCode", 
                            "StatusDesc", "LastUpDt", "Remark" 
                        }
                    );
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        #endregion
       
        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}