﻿namespace RunSystem
{
    partial class FrmRptAgingAPPaidItem2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DteDueDt1 = new DevExpress.XtraEditors.DateEdit();
            this.DteDueDt2 = new DevExpress.XtraEditors.DateEdit();
            this.ChkDueDt = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkVdCode = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueVdCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.ChkStatus = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueType = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkType = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkSiteCode = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkProfitCenterCode = new DevExpress.XtraEditors.CheckEdit();
            this.CcbProfitCenterCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.DtePaymentDt1 = new DevExpress.XtraEditors.DateEdit();
            this.DtePaymentDt2 = new DevExpress.XtraEditors.DateEdit();
            this.ChkPaymentDt = new DevExpress.XtraEditors.CheckEdit();
            this.LblPaymentDt = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ChkBalanceDt = new DevExpress.XtraEditors.CheckEdit();
            this.DteBalanceDt = new DevExpress.XtraEditors.DateEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.ChkVdCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueVdCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkDocDt = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPaymentDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBalanceDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBalanceDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBalanceDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVdCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocDt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkDocDt);
            this.panel2.Controls.Add(this.ChkVdCtCode);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.LueVdCtCode);
            this.panel2.Controls.Add(this.ChkBalanceDt);
            this.panel2.Controls.Add(this.DteBalanceDt);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.DtePaymentDt1);
            this.panel2.Controls.Add(this.DtePaymentDt2);
            this.panel2.Controls.Add(this.ChkPaymentDt);
            this.panel2.Controls.Add(this.LblPaymentDt);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.DteDueDt1);
            this.panel2.Controls.Add(this.DteDueDt2);
            this.panel2.Controls.Add(this.ChkDueDt);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.ChkVdCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueVdCode);
            this.panel2.Size = new System.Drawing.Size(772, 142);
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ReadOnly = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 331);
            this.Grd1.TabIndex = 25;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 142);
            this.panel3.Size = new System.Drawing.Size(772, 331);
            // 
            // DteDueDt1
            // 
            this.DteDueDt1.EditValue = null;
            this.DteDueDt1.EnterMoveNextControl = true;
            this.DteDueDt1.Location = new System.Drawing.Point(115, 26);
            this.DteDueDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt1.Name = "DteDueDt1";
            this.DteDueDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt1.Size = new System.Drawing.Size(110, 20);
            this.DteDueDt1.TabIndex = 12;
            this.DteDueDt1.EditValueChanged += new System.EventHandler(this.DteDueDt1_EditValueChanged);
            // 
            // DteDueDt2
            // 
            this.DteDueDt2.EditValue = null;
            this.DteDueDt2.EnterMoveNextControl = true;
            this.DteDueDt2.Location = new System.Drawing.Point(237, 26);
            this.DteDueDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt2.Name = "DteDueDt2";
            this.DteDueDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt2.Size = new System.Drawing.Size(109, 20);
            this.DteDueDt2.TabIndex = 13;
            this.DteDueDt2.EditValueChanged += new System.EventHandler(this.DteDueDt2_EditValueChanged);
            // 
            // ChkDueDt
            // 
            this.ChkDueDt.Location = new System.Drawing.Point(351, 25);
            this.ChkDueDt.Name = "ChkDueDt";
            this.ChkDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDueDt.Properties.Appearance.Options.UseFont = true;
            this.ChkDueDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDueDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDueDt.Properties.Caption = " ";
            this.ChkDueDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDueDt.Size = new System.Drawing.Size(19, 22);
            this.ChkDueDt.TabIndex = 14;
            this.ChkDueDt.ToolTip = "Remove filter";
            this.ChkDueDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDueDt.ToolTipTitle = "Run System";
            this.ChkDueDt.CheckedChanged += new System.EventHandler(this.ChkDueDt_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(50, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Due Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(226, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "-";
            // 
            // ChkVdCode
            // 
            this.ChkVdCode.Location = new System.Drawing.Point(351, 68);
            this.ChkVdCode.Name = "ChkVdCode";
            this.ChkVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkVdCode.Properties.Appearance.Options.UseFont = true;
            this.ChkVdCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkVdCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkVdCode.Properties.Caption = " ";
            this.ChkVdCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkVdCode.Size = new System.Drawing.Size(19, 22);
            this.ChkVdCode.TabIndex = 10;
            this.ChkVdCode.ToolTip = "Remove filter";
            this.ChkVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkVdCode.ToolTipTitle = "Run System";
            this.ChkVdCode.CheckedChanged += new System.EventHandler(this.ChkVdCode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(62, 74);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Vendor";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCode
            // 
            this.LueVdCode.EnterMoveNextControl = true;
            this.LueVdCode.Location = new System.Drawing.Point(115, 70);
            this.LueVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCode.Name = "LueVdCode";
            this.LueVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCode.Properties.DropDownRows = 30;
            this.LueVdCode.Properties.NullText = "[Empty]";
            this.LueVdCode.Properties.PopupWidth = 300;
            this.LueVdCode.Size = new System.Drawing.Size(231, 20);
            this.LueVdCode.TabIndex = 9;
            this.LueVdCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCode.EditValueChanged += new System.EventHandler(this.LueVdCode_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(34, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 14);
            this.label7.TabIndex = 18;
            this.label7.Text = "Invoice Date";
            // 
            // LueStatus
            // 
            this.LueStatus.EnterMoveNextControl = true;
            this.LueStatus.Location = new System.Drawing.Point(83, 26);
            this.LueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueStatus.Name = "LueStatus";
            this.LueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.Appearance.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStatus.Properties.DropDownRows = 30;
            this.LueStatus.Properties.NullText = "[Empty]";
            this.LueStatus.Properties.PopupWidth = 350;
            this.LueStatus.Size = new System.Drawing.Size(279, 20);
            this.LueStatus.TabIndex = 23;
            this.LueStatus.ToolTip = "F4 : Show/hide list";
            this.LueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged);
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(115, 4);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(110, 20);
            this.DteDocDt1.TabIndex = 19;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // ChkStatus
            // 
            this.ChkStatus.Location = new System.Drawing.Point(370, 24);
            this.ChkStatus.Name = "ChkStatus";
            this.ChkStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkStatus.Properties.Appearance.Options.UseFont = true;
            this.ChkStatus.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkStatus.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkStatus.Properties.Caption = " ";
            this.ChkStatus.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkStatus.Size = new System.Drawing.Size(19, 22);
            this.ChkStatus.TabIndex = 24;
            this.ChkStatus.ToolTip = "Remove filter";
            this.ChkStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkStatus.ToolTipTitle = "Run System";
            this.ChkStatus.CheckedChanged += new System.EventHandler(this.ChkStatus_CheckedChanged);
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(237, 4);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt2.TabIndex = 20;
            this.DteDocDt2.EditValueChanged += new System.EventHandler(this.DteDocDt2_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(36, 30);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 14);
            this.label5.TabIndex = 22;
            this.label5.Text = "Status";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(226, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 14);
            this.label6.TabIndex = 18;
            this.label6.Text = "-";
            // 
            // LueType
            // 
            this.LueType.EnterMoveNextControl = true;
            this.LueType.Location = new System.Drawing.Point(83, 4);
            this.LueType.Margin = new System.Windows.Forms.Padding(5);
            this.LueType.Name = "LueType";
            this.LueType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.Appearance.Options.UseFont = true;
            this.LueType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueType.Properties.DropDownRows = 30;
            this.LueType.Properties.NullText = "[Empty]";
            this.LueType.Properties.PopupWidth = 350;
            this.LueType.Size = new System.Drawing.Size(279, 20);
            this.LueType.TabIndex = 16;
            this.LueType.ToolTip = "F4 : Show/hide list";
            this.LueType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueType.EditValueChanged += new System.EventHandler(this.LueType_EditValueChanged);
            // 
            // ChkType
            // 
            this.ChkType.Location = new System.Drawing.Point(370, 3);
            this.ChkType.Name = "ChkType";
            this.ChkType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkType.Properties.Appearance.Options.UseFont = true;
            this.ChkType.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkType.Properties.Caption = " ";
            this.ChkType.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkType.Size = new System.Drawing.Size(19, 22);
            this.ChkType.TabIndex = 17;
            this.ChkType.ToolTip = "Remove filter";
            this.ChkType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkType.ToolTipTitle = "Run System";
            this.ChkType.CheckedChanged += new System.EventHandler(this.ChkType_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(43, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Type";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(83, 48);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 350;
            this.LueSiteCode.Size = new System.Drawing.Size(279, 20);
            this.LueSiteCode.TabIndex = 27;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // ChkSiteCode
            // 
            this.ChkSiteCode.Location = new System.Drawing.Point(370, 47);
            this.ChkSiteCode.Name = "ChkSiteCode";
            this.ChkSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSiteCode.Properties.Appearance.Options.UseFont = true;
            this.ChkSiteCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSiteCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSiteCode.Properties.Caption = " ";
            this.ChkSiteCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSiteCode.Size = new System.Drawing.Size(19, 22);
            this.ChkSiteCode.TabIndex = 28;
            this.ChkSiteCode.ToolTip = "Remove filter";
            this.ChkSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSiteCode.ToolTipTitle = "Run System";
            this.ChkSiteCode.CheckedChanged += new System.EventHandler(this.ChkSiteCode_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(50, 52);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 14);
            this.label8.TabIndex = 26;
            this.label8.Text = "Site";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkProfitCenterCode);
            this.panel4.Controls.Add(this.CcbProfitCenterCode);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.LueType);
            this.panel4.Controls.Add(this.LueSiteCode);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.ChkSiteCode);
            this.panel4.Controls.Add(this.ChkType);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.ChkStatus);
            this.panel4.Controls.Add(this.LueStatus);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(376, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(392, 138);
            this.panel4.TabIndex = 29;
            // 
            // ChkProfitCenterCode
            // 
            this.ChkProfitCenterCode.Location = new System.Drawing.Point(370, 68);
            this.ChkProfitCenterCode.Name = "ChkProfitCenterCode";
            this.ChkProfitCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProfitCenterCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProfitCenterCode.Properties.Caption = " ";
            this.ChkProfitCenterCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProfitCenterCode.Size = new System.Drawing.Size(18, 22);
            this.ChkProfitCenterCode.TabIndex = 31;
            this.ChkProfitCenterCode.ToolTip = "Remove filter";
            this.ChkProfitCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProfitCenterCode.ToolTipTitle = "Run System";
            this.ChkProfitCenterCode.CheckedChanged += new System.EventHandler(this.ChkProfitCenterCode_CheckedChanged);
            // 
            // CcbProfitCenterCode
            // 
            this.CcbProfitCenterCode.Location = new System.Drawing.Point(83, 70);
            this.CcbProfitCenterCode.Name = "CcbProfitCenterCode";
            this.CcbProfitCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbProfitCenterCode.Properties.DropDownRows = 30;
            this.CcbProfitCenterCode.Properties.IncrementalSearch = true;
            this.CcbProfitCenterCode.Size = new System.Drawing.Size(279, 20);
            this.CcbProfitCenterCode.TabIndex = 30;
            this.CcbProfitCenterCode.EditValueChanged += new System.EventHandler(this.CcbProfitCenterCode_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(2, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 14);
            this.label9.TabIndex = 29;
            this.label9.Text = "Profit Center";
            // 
            // DtePaymentDt1
            // 
            this.DtePaymentDt1.EditValue = null;
            this.DtePaymentDt1.EnterMoveNextControl = true;
            this.DtePaymentDt1.Location = new System.Drawing.Point(115, 114);
            this.DtePaymentDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePaymentDt1.Name = "DtePaymentDt1";
            this.DtePaymentDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaymentDt1.Properties.Appearance.Options.UseFont = true;
            this.DtePaymentDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaymentDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePaymentDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePaymentDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePaymentDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePaymentDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePaymentDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePaymentDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePaymentDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePaymentDt1.Size = new System.Drawing.Size(110, 20);
            this.DtePaymentDt1.TabIndex = 31;
            this.DtePaymentDt1.EditValueChanged += new System.EventHandler(this.DtePaymentDt1_EditValueChanged);
            // 
            // DtePaymentDt2
            // 
            this.DtePaymentDt2.EditValue = null;
            this.DtePaymentDt2.EnterMoveNextControl = true;
            this.DtePaymentDt2.Location = new System.Drawing.Point(237, 114);
            this.DtePaymentDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePaymentDt2.Name = "DtePaymentDt2";
            this.DtePaymentDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaymentDt2.Properties.Appearance.Options.UseFont = true;
            this.DtePaymentDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaymentDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePaymentDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePaymentDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePaymentDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePaymentDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePaymentDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePaymentDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePaymentDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePaymentDt2.Size = new System.Drawing.Size(109, 20);
            this.DtePaymentDt2.TabIndex = 33;
            this.DtePaymentDt2.EditValueChanged += new System.EventHandler(this.DtePaymentDt2_EditValueChanged);
            // 
            // ChkPaymentDt
            // 
            this.ChkPaymentDt.Location = new System.Drawing.Point(351, 112);
            this.ChkPaymentDt.Name = "ChkPaymentDt";
            this.ChkPaymentDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPaymentDt.Properties.Appearance.Options.UseFont = true;
            this.ChkPaymentDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPaymentDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPaymentDt.Properties.Caption = " ";
            this.ChkPaymentDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPaymentDt.Size = new System.Drawing.Size(19, 22);
            this.ChkPaymentDt.TabIndex = 34;
            this.ChkPaymentDt.ToolTip = "Remove filter";
            this.ChkPaymentDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPaymentDt.ToolTipTitle = "Run System";
            this.ChkPaymentDt.CheckedChanged += new System.EventHandler(this.ChkPaymentDt_CheckedChanged);
            // 
            // LblPaymentDt
            // 
            this.LblPaymentDt.AutoSize = true;
            this.LblPaymentDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPaymentDt.Location = new System.Drawing.Point(24, 115);
            this.LblPaymentDt.Name = "LblPaymentDt";
            this.LblPaymentDt.Size = new System.Drawing.Size(85, 14);
            this.LblPaymentDt.TabIndex = 30;
            this.LblPaymentDt.Text = "Payment Date";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(226, 116);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(11, 14);
            this.label10.TabIndex = 32;
            this.label10.Text = "-";
            // 
            // ChkBalanceDt
            // 
            this.ChkBalanceDt.Location = new System.Drawing.Point(231, 47);
            this.ChkBalanceDt.Name = "ChkBalanceDt";
            this.ChkBalanceDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBalanceDt.Properties.Appearance.Options.UseFont = true;
            this.ChkBalanceDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBalanceDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBalanceDt.Properties.Caption = " ";
            this.ChkBalanceDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBalanceDt.Size = new System.Drawing.Size(19, 22);
            this.ChkBalanceDt.TabIndex = 40;
            this.ChkBalanceDt.ToolTip = "Remove filter";
            this.ChkBalanceDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBalanceDt.ToolTipTitle = "Run System";
            this.ChkBalanceDt.CheckedChanged += new System.EventHandler(this.ChkBalanceDt_CheckedChanged);
            // 
            // DteBalanceDt
            // 
            this.DteBalanceDt.EditValue = null;
            this.DteBalanceDt.EnterMoveNextControl = true;
            this.DteBalanceDt.Location = new System.Drawing.Point(115, 48);
            this.DteBalanceDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBalanceDt.Name = "DteBalanceDt";
            this.DteBalanceDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBalanceDt.Properties.Appearance.Options.UseFont = true;
            this.DteBalanceDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBalanceDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBalanceDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBalanceDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBalanceDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBalanceDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBalanceDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBalanceDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBalanceDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBalanceDt.Size = new System.Drawing.Size(110, 20);
            this.DteBalanceDt.TabIndex = 39;
            this.DteBalanceDt.EditValueChanged += new System.EventHandler(this.DteBalanceDt_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(27, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 14);
            this.label11.TabIndex = 38;
            this.label11.Text = "Balance As Of";
            // 
            // ChkVdCtCode
            // 
            this.ChkVdCtCode.Location = new System.Drawing.Point(351, 89);
            this.ChkVdCtCode.Name = "ChkVdCtCode";
            this.ChkVdCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkVdCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkVdCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkVdCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkVdCtCode.Properties.Caption = " ";
            this.ChkVdCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkVdCtCode.Size = new System.Drawing.Size(19, 22);
            this.ChkVdCtCode.TabIndex = 43;
            this.ChkVdCtCode.ToolTip = "Remove filter";
            this.ChkVdCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkVdCtCode.ToolTipTitle = "Run System";
            this.ChkVdCtCode.CheckedChanged += new System.EventHandler(this.ChkVdCtCode_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(2, 93);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 14);
            this.label12.TabIndex = 41;
            this.label12.Text = "Vendor\'s Category";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCtCode
            // 
            this.LueVdCtCode.EnterMoveNextControl = true;
            this.LueVdCtCode.Location = new System.Drawing.Point(115, 92);
            this.LueVdCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCtCode.Name = "LueVdCtCode";
            this.LueVdCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCtCode.Properties.DropDownRows = 30;
            this.LueVdCtCode.Properties.NullText = "[Empty]";
            this.LueVdCtCode.Properties.PopupWidth = 250;
            this.LueVdCtCode.Size = new System.Drawing.Size(231, 20);
            this.LueVdCtCode.TabIndex = 42;
            this.LueVdCtCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCtCode.EditValueChanged += new System.EventHandler(this.LueVdCtCode_EditValueChanged);
            // 
            // ChkDocDt
            // 
            this.ChkDocDt.Location = new System.Drawing.Point(351, 3);
            this.ChkDocDt.Name = "ChkDocDt";
            this.ChkDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocDt.Properties.Appearance.Options.UseFont = true;
            this.ChkDocDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocDt.Properties.Caption = " ";
            this.ChkDocDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocDt.Size = new System.Drawing.Size(19, 22);
            this.ChkDocDt.TabIndex = 44;
            this.ChkDocDt.ToolTip = "Remove filter";
            this.ChkDocDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocDt.ToolTipTitle = "Run System";
            this.ChkDocDt.CheckedChanged += new System.EventHandler(this.ChkDocDt_CheckedChanged);
            // 
            // FrmRptAgingAPPaidItem2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "FrmRptAgingAPPaidItem2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPaymentDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBalanceDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBalanceDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBalanceDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVdCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocDt.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDueDt1;
        internal DevExpress.XtraEditors.DateEdit DteDueDt2;
        private DevExpress.XtraEditors.CheckEdit ChkDueDt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckEdit ChkVdCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueVdCode;
        private DevExpress.XtraEditors.LookUpEdit LueStatus;
        private DevExpress.XtraEditors.CheckEdit ChkStatus;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueType;
        private DevExpress.XtraEditors.CheckEdit ChkType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private DevExpress.XtraEditors.CheckEdit ChkSiteCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.DateEdit DtePaymentDt1;
        internal DevExpress.XtraEditors.DateEdit DtePaymentDt2;
        private DevExpress.XtraEditors.CheckEdit ChkPaymentDt;
        private System.Windows.Forms.Label LblPaymentDt;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.CheckEdit ChkProfitCenterCode;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbProfitCenterCode;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.CheckEdit ChkBalanceDt;
        internal DevExpress.XtraEditors.DateEdit DteBalanceDt;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.CheckEdit ChkVdCtCode;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueVdCtCode;
        private DevExpress.XtraEditors.CheckEdit ChkDocDt;
    }
}