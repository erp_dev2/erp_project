﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDOCt4 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptDOCt4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
                BtnPrint.Hide();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.CtName, B.ItCode, D.ItName, A.SAAddress, B.Qty, ");
            SQL.AppendLine("D.InventoryUOMCode, F.CurCode, Round(E.UPrice,2)As UPrice, B.Qty*E.UPrice As Amt, A.Remark ");
            SQL.AppendLine("From TblDOCt4Hdr A ");
            SQL.AppendLine("Inner Join TblDOCt4Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblCustomer C on A.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblItem D on B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblCtQt2Dtl E On B.CtQt2DocNo=E.DocNo And B.CtQt2DNo=E.DNo ");
            SQL.AppendLine("Inner Join TblCtQt2Hdr F On E.DocNo=F.DocNo ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5      
                        "Document#",
                        "Date",
                        "Customer",
                        "Shipping Address",
                        "Item's Code",

                        //6-10
                        "Item's Name",
                        "Quantity",
                        "UoM",
                        "Currency",
                        "Price",

                        //11-12
                        "Amt",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 200, 250, 100,
                        
                        //6-10
                        250, 100, 100, 80, 180,

                        //11-12
                        180, 200,
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + "Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[]
                        {
                            //0
                            "DocNo",  

                            //1-5
                            "DocDt", "CtName", "SAAddress", "ItCode", "ItName",

                            //6-10
                            "Qty", "InventoryUomCode", "CurCode", "UPrice", "Amt",
                            
                            //11
                            "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 11 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional

        private void BtnPrint2_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            {
                var l = new List<DoToCustomerSRIA>();

                string[] TableName = { "DoToCustomerSRIA" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d/%m/%Y')As DocDt, C.CtName, B.ItCode, D.ItName, A.SAAddress, B.Qty, ");
                SQL.AppendLine("D.InventoryUOMCode, F.CurCode, Round(E.UPrice,2)As UPrice, B.Qty*E.UPrice As Amt, A.Remark, ");
                SQL.AppendLine("Date_Format(@DocDt1,'%d %M %Y')As DocDt1, Date_Format(@DocDt2,'%d %M %Y')As DocDt2 ");
                SQL.AppendLine("From TblDOCt4Hdr A ");
                SQL.AppendLine("Inner Join TblDOCt4Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                SQL.AppendLine("Inner Join TblCustomer C on A.CtCode=C.CtCode ");
                SQL.AppendLine("Inner Join TblItem D on B.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblCtQt2Dtl E On B.CtQt2DocNo=E.DocNo And B.CtQt2DNo=E.DNo ");
                SQL.AppendLine("Inner Join TblCtQt2Hdr F On E.DocNo=F.DocNo ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
                if (Sm.GetLue(LueCtCode).Length > 0)
                    SQL.AppendLine("And A.CtCode=@CtCode");
                SQL.AppendLine("Order By A.DocDt, A.DocNo, B.DNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "DocNo",
                         "DocDt",
                         "CtName",
                         "ItCode",
                         "ItName",
                         
                         //11-15
                         "SAAddress",
                         "Qty",
                         "InventoryUOMCode",
                         "CurCode",
                         "UPrice",

                         //16-19
                         "Amt",
                         "Remark",
                         "DocDt1",
                         "DocDt2"

                        });
                    if (dr.HasRows)
                    {
                        int nomor = 0;
                        while (dr.Read())
                        {
                            nomor = nomor + 1;
                            l.Add(new DoToCustomerSRIA()
                            {
                                nomor = nomor,
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressFull = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),

                                DocNo = Sm.DrStr(dr, c[6]),
                                DocDt = Sm.DrStr(dr, c[7]),
                                CtName = Sm.DrStr(dr, c[8]),
                                ItCode = Sm.DrStr(dr, c[9]),
                                ItName = Sm.DrStr(dr, c[10]),

                                SAAddress = Sm.DrStr(dr, c[11]),
                                Qty = Sm.DrDec(dr, c[12]),
                                InventoryUOMCode = Sm.DrStr(dr, c[13]),
                                CurCode = Sm.DrStr(dr, c[14]),
                                UPrice = Sm.DrDec(dr, c[15]),

                                Amt = Sm.DrDec(dr, c[16]),
                                Remark = Sm.DrStr(dr, c[17]),
                                DocDt1 = Sm.DrStr(dr, c[18]),
                                DocDt2 = Sm.DrStr(dr, c[19]),
                                Customer = LueCtCode.Text,

                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                Sm.PrintReport("DoToCustomerSRIA", myLists, TableName, false);

            }
        }


        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #region Class

        private class DoToCustomerSRIA
        {
            public int nomor { get; set; }
            public string CompanyLogo { set; get; }

            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }

            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string SAAddress { get; set; }
            public string ItCode { get; set; }

            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUOMCode { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }

            public decimal Amt { get; set; }
            public string Remark { get; set; }
            public string DocDt1 { get; set; }
            public string DocDt2 { get; set; }
            public string Customer { get; set; }
        }

        #endregion

     
       

    }
}
