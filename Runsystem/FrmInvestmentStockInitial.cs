﻿#region Update
/* 
    12/04/2022 [RDA/PRODUCT] rombak untuk menu investment initial
    18/04/2022 [IBL/PRODUCT] Simpan InvestmentType di StockMovement, dan simpan Base Price di StockPrice utk keperluan perhitungan di reporting
    27/04/2022 [RDA/PRODUCT] tambah param BankAccountTypeForInvestment
    11/05/2022 [RDA/PRODUCT] bug warning 'Column count doesn't match value count at row 1'
    13/05/2022 [RDA/PRODUCT] penyesuaian show data untuk kolom Investment Code
    25/05/2022 [IBL/PRODUCT] Memisahkan item equity dan item debt dengan tab
    05/07/2022 [SET/PRODUCT] tambah field Last Coupon Date & Next Coupon Date pada tab Debt Securities
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentStockInitial : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmInvestmentStockInitialFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string 
            mMainCurCode = string.Empty,
            mAcNoForInitialStockOpeningBalance = string.Empty,
            mDocType = "01",
            mEntCode = string.Empty;
        internal bool 
            mIsItGrpCodeShow = false,
            mIsShowForeignName = false,
            mIsInvTrnShowItSpec = false,
            mIsUseProductionWorkGroup = false;
        private bool 
            mIsAutoJournalActived = false, 
            mIsBatchNoUseDocDtIfEmpty = false, 
            mIsMovingAvgEnabled = false,
            mIsJournalValidationStockInitialEnabled = false;
        internal string
            mBankAccountTypeForInvestment = string.Empty;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmInvestmentStockInitial(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                
                Tc1.SelectedTab = Tp2;
                Tc1.SelectedTab = Tp1;
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCurCode(ref LueCurCode);
                SetLueLot(ref LueLot);
                LueLot.Visible = false;
                SetLueBin(ref LueBin);
                LueBin.Visible = false;
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                SetLueInvestmentSekuritasCode(ref LueWhsCode, string.Empty);
                SetLueInvestmentType(ref LueInvestmentType, String.Empty);
                SetLueInvestmentType(ref LueInvestmentType2, String.Empty);
                label6.Visible = false;
                label11.Visible = false;
                label8.Visible = false;
                LueCurCode.Visible = false;
                TxtExcRate.Visible = false;
                LueWhsCode.Visible = false;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Equity Securities

            Grd3.Cols.Count = 35;
            Grd3.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Investment's"+Environment.NewLine+"Equity Code",
                        "Investment's"+Environment.NewLine+"Name",
                        
                        //6-10
                        "Local Code",
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",

                        //11-15
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Acquisition Price", 
                        "Remark",
                        "Group",
                        "Foreign Name",

                        //21-25
                        "Specification",
                        "",
                        "Investment Bank"+Environment.NewLine+"Account Code (RDN)",
                        "Investment Bank"+Environment.NewLine+"Account Name",
                        "Investment Category",

                        //26-30
                        "Currency",
                        "Issuer",
                        "Type",
                        "Base Price",
                        "Acquisition Cost",

                        //31-34
                        "OptCode",
                        "CurCode",
                        "Investment Bank"+Environment.NewLine+"Account (RDN)",
                        "Investment's"+Environment.NewLine+"Code",
                    },
                     new int[] 
                    {
                        //0
                        20,
                        //1-5
                        50, 0, 20, 120, 200, 
                        //6-10
                        130, 200, 180, 100, 100, 
                        //11-15
                        80, 60, 80, 60, 80, 
                        //16-20
                        60, 150, 300, 100, 150,
                        //21-25
                        200, 20, 150, 250, 150, 
                        //26-30
                        80, 200, 150, 150, 150,
                        //31-34
                        80, 80, 150, 120
                    }
                );
            Sm.GrdColCheck(Grd3, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd3, new int[] { 11, 13, 15, 17, 29, 30 }, 0);
            Sm.GrdColButton(Grd3, new int[] { 3, 22 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 4, 7, 8, 9, 10, 13, 14, 15, 16, 18, 19, 21, 23, 6, 8, 9, 18, 31, 32 }, false);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd3, new int[] { 20 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 4, 5, 6, 8, 12, 14, 16, 19, 20, 21, 23, 24, 25, 26, 27, 30, 31, 32, 33, 34});
            if (mIsItGrpCodeShow)
            {
                Grd3.Cols[19].Visible = true;
                Grd3.Cols[19].Move(7);
            }
            Grd3.Cols[20].Move(8);
            if (mIsInvTrnShowItSpec)
            {
                Grd3.Cols[21].Visible = true;
                Grd3.Cols[21].Move(9);
            }

            Grd3.Cols[22].Move(6);
            Grd3.Cols[23].Move(7);
            Grd3.Cols[24].Move(8);
            Grd3.Cols[25].Move(9);
            Grd3.Cols[26].Move(10);
            Grd3.Cols[27].Move(11);
            Grd3.Cols[28].Move(12);
            Grd3.Cols[29].Move(25);
            Grd3.Cols[31].Move(13);
            Grd3.Cols[33].Move(7);
            Grd3.Cols[34].Move(4);

            #endregion
            
            #region Debt Securities

            Grd4.Cols.Count = 26;
            Grd4.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                Grd4,
                new string[]
                {
                    //0
                    "DNo",
                        
                    //1-5
                    "Cancel",
                    "",
                    "Investment's"+Environment.NewLine+"Debt Code",
                    "Investment's"+Environment.NewLine+"Code",
                    "Investment's"+Environment.NewLine+"Name",

                    //6-10
                    "",
                    "Investment"+Environment.NewLine+"Bank Account Code",
                    "Investment"+Environment.NewLine+"Bank Account (RDN)",
                    "Investment"+Environment.NewLine+"Bank Account Name",
                    "Investment's Category",

                    //11-15 
                    "Currency",
                    "Issuer",
                    "Investment Type Code",
                    "Type",
                    "Nominal Amount",
                    
                    //16-20                    
                    "Acquisition Price (%)",
                    "Acquisition Cost",
                    "Local Code",
                    "Batch#",
                    "Source",
                        
                    //21-25
                    "Lot",
                    "Bin",
                    "Remark",
                    "Last Coupon"+Environment.NewLine+"Date",
                    "Next Coupon"+Environment.NewLine+"Date",
                },
                    new int[]
                {
                    //0
                    20,
                    //1-5
                    50, 20, 120, 120, 200, 
                    //6-10
                    20, 150, 250, 150, 150, 
                    //11-15
                    80, 200, 80, 150, 150, 
                    //16-20
                    150, 150, 150, 150, 150,
                    //21-25
                    150, 150, 250, 100, 100,
                }
            );
            Sm.GrdFormatDate(Grd4, new int[] { 24, 25 });
            Sm.GrdColCheck(Grd4, new int[] { 1 });
            Sm.GrdColButton(Grd4, new int[] { 2, 6 });
            Sm.GrdFormatDec(Grd4, new int[] { 15, 16, 17 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 0, 3, 7, 13, 18, 19, 20, 21, 22 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
            Grd4.Cols[24].Move(18);
            Grd4.Cols[25].Move(19);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        "Total Quantity 1", "Total Quantity 2", "Total Quantity 3"
                    },
                    new int[] 
                    {
                        130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 0, 1, 2 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, false);
            Grd2.Visible = false;

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd3, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd4, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueCurCode, TxtExcRate, MeeRemark, LueWhsCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 3, 7, 9, 10, 11, 13, 15, 17, 18, 28, 29 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 1, 2, 6, 14, 15, 16, 23, 24, 25 });
                    TxtDocNo.Focus();
                    LueInvestmentType.Visible = false;
                    LueInvestmentType2.Visible = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCurCode, TxtExcRate, MeeRemark, LueWhsCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 3, 7, 9, 10, 11, 13, 15, 17, 18, 28, 29 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 2, 6, 14, 15, 16, 23 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueCurCode, TxtExcRate, MeeRemark, LueWhsCode
            });
            ClearGrd();
            Sm.FocusGrd(Grd3, 0, 1);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdBoolValueFalse(ref Grd3, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 11, 13, 15, 17, 29, 30 });

            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdBoolValueFalse(ref Grd4, 0, new int[] { 1 });
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 15, 16, 17 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 0, 1, 2 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInvestmentStockInitialFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                //Sm.SetLue(LueCurCode, mMainCurCode);
                //TxtExcRate.EditValue = Sm.FormatNum(1, 0);
                Sm.SetLue(LueCurCode, string.Empty);
                TxtExcRate.EditValue = Sm.FormatNum(0, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();   
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            ParPrint(TxtDocNo.Text, (int)mNumberOfInventoryUomCode);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "InvestmentStockInitial", "TblInvestmentStockInitialHdr");
            

            var cml = new List<MySqlCommand>();

            cml.Add(SaveInvestmentStockInitialHdr(DocNo));
            cml.Add(SaveInvestmentStockInitialDtl(DocNo));
            cml.Add(SaveInvestmentStockInitialDtl2(DocNo));

            cml.Add(SaveInvestmentStock(DocNo));

            //if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            if (Sm.StdMsgYN("Print", "") == DialogResult.No)
                BtnInsertClick(sender, e);
            else
            {
                ShowData(DocNo);
                //ParPrint(DocNo, (int)mNumberOfInventoryUomCode);
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdValueNotValid2() ||
                IsGrdExceedMaxRecords() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt)) ||
                IsJournalSettingInvalid();
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsJournalValidationStockInitialEnabled) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter

            if (mAcNoForInitialStockOpeningBalance.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForInitialStockOpeningBalance is empty.");
                return true;
            }

            //Table
            if (IsJournalSettingInvalid_InvestmentCategory(Msg)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_InvestmentCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string InvestmentCode = string.Empty, InvestmentCtName = string.Empty;

            SQL.AppendLine("Select B.InvestmentCtName From TblInvestmentItem A, TblInvestmentCategory B ");
            SQL.AppendLine("Where A.InvestmentCtCode=B.InvestmentCtCode And B.AcNo Is Null ");
            SQL.AppendLine("And A.InvestmentCode In (");
            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                InvestmentCode = Sm.GetGrdStr(Grd3, r, 4);
                if (InvestmentCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@InvestmentCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@InvestmentCode_" + r.ToString(), InvestmentCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            InvestmentCtName = Sm.GetValue(cm);
            if (InvestmentCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Investment category's COA account# ("+ InvestmentCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd3.Rows.Count == 1 && Grd4.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd3.Rows.Count >100000)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Equity Securities data entered (" + (Grd3.Rows.Count-1).ToString() + ") exceeds the maximum limit (99.999).");
                Tc1.SelectedTab = Tp1;
                return true;
            }
            if (Grd4.Rows.Count > 100000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Debt Securities data entered (" + (Grd4.Rows.Count - 1).ToString() + ") exceeds the maximum limit (99.999).");
                Tc1.SelectedTab = Tp2;
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsInventoryQtyMandatory = Sm.GetParameter("IsInventoryQtyMandatory")=="Y";
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                Msg = 
                    "Investment's Code : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine +
                    "Investment's Name : " + Sm.GetGrdStr(Grd3, Row, 5) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd3, Row, 7) + Environment.NewLine + Environment.NewLine 
                    ;

                if (Sm.IsGrdValueEmpty(Grd3, Row, 23, false, "Investment bank account is empty."))
                {
                    Tc1.SelectedTab = Tp1;
                    Sm.FocusGrd(Grd3, Row, 33);
                    return true;
                }

                if (Sm.IsGrdValueEmpty(Grd3, Row, 28, false, "Type is empty.") || Sm.IsGrdValueEmpty(Grd3, Row, 31, false, "Type is empty."))
                {
                    Tc1.SelectedTab = Tp1;
                    Sm.FocusGrd(Grd3, Row, 28);
                    return true;
                }

                //if (
                //    Sm.IsGrdValueEmpty(Grd3, Row, 4, false, "Item is empty.") ||
                //    (!IsInventoryQtyMandatory &&
                //    (
                //        Sm.IsGrdValueEmpty(Grd3, Row, 11, true, Msg + "Quantity (1) is empty.") ||
                //        (Sm.GetGrdStr(Grd3, Row, 14).Length > 0 && mNumberOfInventoryUomCode >= 2 && Sm.IsGrdValueEmpty(Grd3, Row, 13, true, Msg + "Quantity (2) is empty.")) ||
                //        (Sm.GetGrdStr(Grd3, Row, 16).Length > 0 && mNumberOfInventoryUomCode == 3 && Sm.IsGrdValueEmpty(Grd3, Row, 15, true, Msg + "Quantity (3) is empty."))
                //    ))
                //    )
                //    return true;

                if (Sm.IsGrdValueEmpty(Grd3, Row, 4, false, "Item is empty.") || (!IsInventoryQtyMandatory && Sm.IsGrdValueEmpty(Grd3, Row, 11, true, Msg + "Quantity is empty.")))
                {
                    Tc1.SelectedTab = Tp1;
                    return true;
                }

                for (int Row2 = 0; Row2 < Grd3.Rows.Count - 1; Row2++)
                {
                    if (Row != Row2 &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd3, Row, 4) + Sm.GetGrdStr(Grd3, Row, 7), Sm.GetGrdStr(Grd3, Row2, 4) + Sm.GetGrdStr(Grd3, Row2, 7)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Investment's Code : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine +
                            "Investment's Name : " + Sm.GetGrdStr(Grd3, Row, 5) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd3, Row, 7) + Environment.NewLine + Environment.NewLine +
                            "Duplicate item in the list."
                            );
                        Tc1.SelectedTab = Tp1;
                        Sm.FocusGrd(Grd3, Row, 1);
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid2()
        {
            bool IsInventoryQtyMandatory = Sm.GetParameter("IsInventoryQtyMandatory") == "Y";
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                Msg =
                    "Investment's Code : " + Sm.GetGrdStr(Grd4, Row, 4) + Environment.NewLine +
                    "Investment's Name : " + Sm.GetGrdStr(Grd4, Row, 5) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd4, Row, 19) + Environment.NewLine + Environment.NewLine
                    ;

                if (Sm.IsGrdValueEmpty(Grd4, Row, 8, false, "Investment bank account is empty."))
                {
                    Tc1.SelectedTab = Tp2;
                    Sm.FocusGrd(Grd4, Row, 8);
                    return true;
                }

                if (Sm.IsGrdValueEmpty(Grd4, Row, 13, false, "Type is empty.") || Sm.IsGrdValueEmpty(Grd4, Row, 14, false, "Type is empty."))
                {
                    Tc1.SelectedTab = Tp2;
                    Sm.FocusGrd(Grd4, Row, 14);
                    return true;
                }

                if (Sm.IsGrdValueEmpty(Grd4, Row, 4, false, "Item is empty.") || (!IsInventoryQtyMandatory && Sm.IsGrdValueEmpty(Grd4, Row, 15, true, Msg + "Nominal Amount is empty.")))
                {
                    Tc1.SelectedTab = Tp2;
                    return true;
                }

                for (int Row2 = 0; Row2 < Grd4.Rows.Count - 1; Row2++)
                {
                    if (Row != Row2 &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd4, Row, 4) + Sm.GetGrdStr(Grd4, Row, 19), Sm.GetGrdStr(Grd4, Row2, 4) + Sm.GetGrdStr(Grd4, Row2, 19)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Investment's Code : " + Sm.GetGrdStr(Grd4, Row, 4) + Environment.NewLine +
                            "Investment's Name : " + Sm.GetGrdStr(Grd4, Row, 5) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd4, Row, 19) + Environment.NewLine + Environment.NewLine +
                            "Duplicate investment item in the list."
                            );
                        Tc1.SelectedTab = Tp2;
                        Sm.FocusGrd(Grd3, Row, 1);
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveInvestmentStockInitialHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblInvestmentStockInitialHdr(DocNo, DocDt, WhsCode, CurCode, ExcRate, ProductionWorkGroup, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @CurCode, @ExcRate, @ProductionWorkGroup, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtExcRate.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveInvestmentStockInitialDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* StockInitial - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblInvestmentStockInitialDtl(DocNo, DNo, CancelInd, InvestmentCode, BankAcCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, " +
                            "InvestmentType, BasePrice, AqcuisitionCost, CurCode, Issuer, " +
                            "CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@DocNo, @DNo_" + r.ToString() +
                        ", 'N', @InvestmentCode_" + r.ToString() + 
                        ", ");
                    SQL.AppendLine("@BankAcCode_" + r.ToString() + ", ");
                    SQL.AppendLine("IfNull(@BatchNo_" + r.ToString() + ", ");
                    SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
                    SQL.AppendLine("), Concat(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() +
                        "), IfNull(@Lot_" + r.ToString() + 
                        ", '-'), IfNull(@Bin_" + r.ToString() + 
                        ", '-'), ");
                    SQL.AppendLine("@Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @Qty3_" + r.ToString() +
                        ", @UPrice_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @InvestmentType_" + r.ToString() +
                        ", @BasePrice_" + r.ToString() +
                        ", @AqcuisitionCost_" + r.ToString() +
                        ", @CurCode_" + r.ToString() +
                        ", @Issuer_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00000" + (r + 1).ToString(), 5));
                    Sm.CmParam<String>(ref cm, "@InvestmentCode_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 4)); 
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 7));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 9));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 11));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 13));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 17));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 18));
                    Sm.CmParam<String>(ref cm, "@BankAcCode_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 23));
                    Sm.CmParam<String>(ref cm, "@InvestmentType_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 31));
                    Sm.CmParam<Decimal>(ref cm, "@BasePrice_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 29));
                    Sm.CmParam<Decimal>(ref cm, "@AqcuisitionCost_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 30));
                    Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 32));
                    Sm.CmParam<String>(ref cm, "@Issuer_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 27));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveInvestmentStockInitialDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            int DNo = GetInitDNo2();

            SQL.AppendLine("/* StockInitial - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                DNo = DNo + 1;
                if (Sm.GetGrdStr(Grd4, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblInvestmentStockInitialDtl2(DocNo, DNo, CancelInd, InvestmentCode, BankAcCode, BatchNo, Source, " +
                                       "Lot, Bin, NominalAmt, UPrice, Remark, InvestmentType, AcquisitionCost, CurCode, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", 'N', @InvestmentCode_" + r.ToString() + ", ");
                    SQL.AppendLine("@BankAcCode_" + r.ToString() + ", ");
                    SQL.AppendLine("IfNull(@BatchNo_" + r.ToString() + ", ");
                    SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
                    SQL.AppendLine("), Concat(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString()+ ") ");
                    SQL.AppendLine(", IfNull(@Lot_" + r.ToString() + ", '-') ");
                    SQL.AppendLine(", IfNull(@Bin_" + r.ToString() + ", '-') ");
                    SQL.AppendLine(", @NominalAmt_" + r.ToString() +
                        ", @UPrice_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @InvestmentType_" + r.ToString() +
                        ", @AqcuisitionCost_" + r.ToString() +
                        ", @CurCode_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00000" + DNo.ToString(), 5));
                    Sm.CmParam<String>(ref cm, "@InvestmentCode_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 3));
                    Sm.CmParam<String>(ref cm, "@BankAcCode_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 7));
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 19));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 21));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 22));
                    Sm.CmParam<Decimal>(ref cm, "@NominalAmt_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 16));
                    Sm.CmParam<Decimal>(ref cm, "@AqcuisitionCost_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 17));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 23));                    
                    Sm.CmParam<String>(ref cm, "@InvestmentType_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 13));
                    Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 11));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");
            
                var a = Sm.GetGrdStr(Grd4, 0, 3);
                var b = Sm.GetGrdStr(Grd4, 0, 4);
                var c = Sm.GetGrdDate(Grd4, 0, 24);
                var d = Sm.GetGrdDate(Grd4, 0, 25);

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 4).Length > 0)
                {
                    SQL.AppendLine("UPDATE TblInvestmentItemDebt A ");
                    SQL.AppendLine("SET A.LastCouponDt = @LastCouponDt_" + r.ToString() + ", A.NextCouponDt = @NextCouponDt_" + r.ToString() + ", ");
                    SQL.AppendLine("A.LastUpBy = @UserCode, A.LastUpDt = @Dt ");
                    SQL.AppendLine("WHERE A.InvestmentDebtCode = @InvestmentDebtCode_" + r.ToString() + " AND A.PortofolioId = @InvestmentCode2_" + r.ToString() + "; ");

                    Sm.CmParam<String>(ref cm, "@InvestmentDebtCode_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 3));
                    Sm.CmParam<String>(ref cm, "@InvestmentCode2_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 4));
                    Sm.CmParamDt(ref cm, "@LastCouponDt_" + r.ToString(), Sm.GetGrdDate(Grd4, r, 24));
                    Sm.CmParamDt(ref cm, "@NextCouponDt_" + r.ToString(), Sm.GetGrdDate(Grd4, r, 25));
                }
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        private MySqlCommand SaveStockInitialDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblInvestmentStockInitialDtl(DocNo, DNo, CancelInd, InvestmentCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @InvestmentCode, ");
            SQL.AppendLine("IfNull(@BatchNo, ");
            SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty?"@DocDt":"'-'");
            SQL.AppendLine("), Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-') , ");
            SQL.AppendLine("@Qty, @Qty2, @Qty3, @UPrice, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@InvestmentCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd3, Row, 10));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd3, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd3, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd3, Row, 17));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 18));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        private MySqlCommand SaveInvestmentStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Tbl Stock Movement */");
            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, InvestmentType, BatchNo, Source, Qty, Qty2, Qty3, NominalAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, Null As WhsCode, B.BankAcCode, B.Lot, B.Bin, B.InvestmentCode, B.InvestmentType, B.BatchNo, B.Source, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, 0.00 As NominalAmt, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("  Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("  Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, Null As WhsCode, B.BankAcCode, B.Lot, B.Bin, B.InvestmentCode, B.InvestmentType,  B.BatchNo, B.Source, ");
            SQL.AppendLine("0.00 As Qty, 0.00 As Qty2, 0.00 As Qty3, B.NominalAmt, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("  Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("  Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblInvestmentStockInitialDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("/* Tbl Stock Summary */");
            SQL.AppendLine("Insert Into TblInvestmentStockSummary(BankAcCode, WhsCode, Lot, Bin, InvestmentCode, BatchNo, Source, Qty, Qty2, Qty3, NominalAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.BankAcCode, Null As WhsCode, B.Lot, B.Bin, B.InvestmentCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, 0.00 As NominalAmt, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.BankAcCode, Null As WhsCode, B.Lot, B.Bin, B.InvestmentCode, B.BatchNo, B.Source, 0.00 As Qty, 0.00 As Qty2, 0.00 As Qty3, B.NominalAmt, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblInvestmentStockInitialDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("/* Tbl Stock Price */");
            SQL.AppendLine("Insert Into TblInvestmentStockPrice(InvestmentCode, BatchNo, Source, CurCode, UPrice, BasePrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.InvestmentCode, B.BatchNo, B.Source, B.CurCode, B.UPrice, B.BasePrice, C.Amt, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblCurrencyRate C On B.CurCode=C.CurCode1 AND C.RateDt = (SELECT Max(RateDt) FROM tblcurrencyrate WHERE CurCode1 = B.CurCode) ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.InvestmentCode, B.BatchNo, B.Source, B.CurCode, B.UPrice, 0.00 As BasePrice, C.Amt, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblInvestmentStockInitialDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblCurrencyRate C On B.CurCode=C.CurCode1 AND C.RateDt = (SELECT Max(RateDt) FROM tblcurrencyrate WHERE CurCode1 = B.CurCode) ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblInvestmentStockInitialHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Investment Stock Initial : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("null as CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, B.RemarkJournal, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt, Remark As RemarkJournal From (");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblInvestmentStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblInvestmentStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblInvestmentItem D On C.InvestmentCode=D.InvestmentCode ");
                SQL.AppendLine("        Inner Join TblInvestmentCategory E On D.InvestmentCtCode=E.InvestmentCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo2 As AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblInvestmentStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblInvestmentStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblInvestmentItem D On C.InvestmentCode=D.InvestmentCode ");
                SQL.AppendLine("        Inner Join TblInvestmentCategory E On D.InvestmentCtCode=E.InvestmentCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblInvestmentStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblInvestmentStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblInvestmentItem D On C.InvestmentCode=D.InvestmentCode ");
                SQL.AppendLine("        Inner Join TblInvestmentCategory E On D.InvestmentCtCode=E.InvestmentCtCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select  '2.01.07.02' As AcNo, 0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblInvestmentStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblInvestmentStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblInvestmentItem D On C.InvestmentCode=D.InvestmentCode ");
                SQL.AppendLine("        Inner Join TblInvestmentCategory E On D.InvestmentCtCode=E.InvestmentCtCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@AcNoForInitialStockOpeningBalance", mAcNoForInitialStockOpeningBalance);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@Remark", DocNo);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "'XXXXX'", DNo2 = "'XXXXX'";

            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd3, Row, 1) && !Sm.GetGrdBool(Grd3, Row, 2) && Sm.GetGrdStr(Grd3, Row, 4).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd3, Row, 0) + "'";

            for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd4, Row, 1) && Sm.GetGrdStr(Grd4, Row, 3).Length > 0)
                    DNo2 = DNo2 + ",'" + Sm.GetGrdStr(Grd4, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid(DNo, DNo2)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd3, Row, 1) && !Sm.GetGrdBool(Grd3, Row, 2) && Sm.GetGrdStr(Grd3, Row, 4).Length > 0)
                    cml.Add(EditInvestmentStockInitialDtl(Row));
            }

            for (int Row = 0; Row < Grd4.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd4, Row, 1) && Sm.GetGrdStr(Grd4, Row, 3).Length > 0)
                    cml.Add(EditInvestmentStockInitialDtl2(Row));
            }

            //if (mIsAutoJournalActived) cml.Add(SaveJournal2(DNo));
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblInvestmentStockInitialDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd3.ProcessTab = true;
                    Grd3.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd3, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd3, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd3, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd3.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsEditedDataNotValid(string DNo, string DNo2)
        {
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo, DNo2) ||
                IsStockNotEnough() ||
                IsJournalSettingInvalid();
        }

        private bool IsCancelledItemNotExisted(string DNo, string DNo2)
        {
            if (Sm.CompareStr(DNo, "'XXXXX'") && Sm.CompareStr(DNo2, "'XXXXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false; 
        }

        private bool IsStockNotEnough()
        {
            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd3, r, 1) && !Sm.GetGrdBool(Grd3, r, 2) && Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                    if (IsStockNotEnough(r)) return true;
            }

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd4, r, 1) && Sm.GetGrdStr(Grd3, r, 20).Length > 0)
                    if (IsStockNotEnough2(r)) return true;
            }
            return false;
        }

        private bool IsStockNotEnough(int r)
        {
            decimal Qty = 0m, Qty2 = 0m, Qty3 = 0m;
            decimal Stock = 0m, Stock2 = 0m, Stock3 = 0m;

            if (Sm.GetGrdStr(Grd3, r, 11).Length > 0) Qty = Sm.GetGrdDec(Grd3, r, 11);
            if (Sm.GetGrdStr(Grd3, r, 13).Length > 0) Qty2 = Sm.GetGrdDec(Grd3, r, 13);
            if (Sm.GetGrdStr(Grd3, r, 15).Length > 0) Qty3 = Sm.GetGrdDec(Grd3, r, 15);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Qty, Qty2, Qty3 From TblInvestmentStockSummary ");
            SQL.AppendLine("Where Source=@Source ");
            SQL.AppendLine("And BankAcCode=@BankAcCode ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin Limit 1;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd3, r, 8));
                Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd3, r, 9));
                Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd3, r, 10));
                Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetGrdStr(Grd3, r, 23));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Qty", "Qty2", "Qty3" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Stock = Sm.DrDec(dr, 0);
                        Stock2 = Sm.DrDec(dr, 1);
                        Stock3 = Sm.DrDec(dr, 2);
                    }
                }
                dr.Close();
            }

            if (Qty < Stock)
            {
                Sm.StdMsg(mMsgType.Warning,
                     "Item's Code : " + Sm.GetGrdStr(Grd3, r, 34) + Environment.NewLine +
                     "Item's Name : " + Sm.GetGrdStr(Grd3, r, 6) + Environment.NewLine +
                     "Batch# : " + Sm.GetGrdStr(Grd3, r, 7) + Environment.NewLine +
                     "Source : " + Sm.GetGrdStr(Grd3, r, 8) + Environment.NewLine + 
                     "Lot : " + Sm.GetGrdStr(Grd3, r, 9) + Environment.NewLine + 
                     "Bin : " + Sm.GetGrdStr(Grd3, r, 10) + Environment.NewLine + 
                     "Stock : " + Sm.FormatNum(Stock, 0) + Environment.NewLine +
                     "Cancelled Quantity : " + Sm.FormatNum(Qty, 0) + Environment.NewLine + Environment.NewLine +
                     "Not enough stock.");
                return true;
            }

            if (mNumberOfInventoryUomCode>=2 && Qty2 < Stock2)
            {
                Sm.StdMsg(mMsgType.Warning,
                     "Item's Code : " + Sm.GetGrdStr(Grd3, r, 34) + Environment.NewLine +
                     "Item's Name : " + Sm.GetGrdStr(Grd3, r, 6) + Environment.NewLine +
                     "Batch# : " + Sm.GetGrdStr(Grd3, r, 7) + Environment.NewLine +
                     "Source : " + Sm.GetGrdStr(Grd3, r, 8) + Environment.NewLine +
                     "Lot : " + Sm.GetGrdStr(Grd3, r, 9) + Environment.NewLine +
                     "Bin : " + Sm.GetGrdStr(Grd3, r, 10) + Environment.NewLine +
                     "Stock : " + Sm.FormatNum(Stock2, 0) + Environment.NewLine +
                     "Cancelled Quantity : " + Sm.FormatNum(Qty2, 0) + Environment.NewLine + Environment.NewLine +
                     "Not enough stock.");
                return true;
            }

            if (mNumberOfInventoryUomCode >= 3 && Qty3 < Stock3)
            {
                Sm.StdMsg(mMsgType.Warning,
                     "Item's Code : " + Sm.GetGrdStr(Grd3, r, 34) + Environment.NewLine +
                     "Item's Name : " + Sm.GetGrdStr(Grd3, r, 6) + Environment.NewLine +
                     "Batch# : " + Sm.GetGrdStr(Grd3, r, 7) + Environment.NewLine +
                     "Source : " + Sm.GetGrdStr(Grd3, r, 8) + Environment.NewLine +
                     "Lot : " + Sm.GetGrdStr(Grd3, r, 9) + Environment.NewLine +
                     "Bin : " + Sm.GetGrdStr(Grd3, r, 10) + Environment.NewLine +
                     "Stock : " + Sm.FormatNum(Stock3, 0) + Environment.NewLine +
                     "Cancelled Quantity : " + Sm.FormatNum(Qty3, 0) + Environment.NewLine + Environment.NewLine +
                     "Not enough stock.");
                return true;
            }
            return false;
        }

        private bool IsStockNotEnough2(int r)
        {
            decimal NominalAmt = 0m;
            decimal Stock = 0m;

            if (Sm.GetGrdStr(Grd3, r, 15).Length > 0) NominalAmt = Sm.GetGrdDec(Grd3, r, 15);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select NominalAmt From TblInvestmentStockSummary ");
            SQL.AppendLine("Where Source=@Param1 ");
            SQL.AppendLine("And BankAcCode=@Param2 ");
            SQL.AppendLine("And Lot=@Param3 ");
            SQL.AppendLine("And Bin=@Param4 Limit 1;");

            Stock = Sm.GetValueDec(SQL.ToString(), Sm.GetGrdStr(Grd4, r, 20), Sm.GetGrdStr(Grd4, r, 7), Sm.GetGrdStr(Grd4, r, 21), Sm.GetGrdStr(Grd4, r, 22));

            if (NominalAmt < Stock)
            {
                Sm.StdMsg(mMsgType.Warning,
                     "Investment's Code : " + Sm.GetGrdStr(Grd4, r, 4) + Environment.NewLine +
                     "Investment's Name : " + Sm.GetGrdStr(Grd4, r, 5) + Environment.NewLine +
                     "Batch# : " + Sm.GetGrdStr(Grd4, r, 19) + Environment.NewLine +
                     "Source : " + Sm.GetGrdStr(Grd4, r, 20) + Environment.NewLine +
                     "Lot : " + Sm.GetGrdStr(Grd4, r, 21) + Environment.NewLine +
                     "Bin : " + Sm.GetGrdStr(Grd4, r, 22) + Environment.NewLine +
                     "Stock : " + Sm.FormatNum(Stock, 0) + Environment.NewLine +
                     "Cancelled Nominal Amount : " + Sm.FormatNum(NominalAmt, 0) + Environment.NewLine + Environment.NewLine +
                     "Not enough stock.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditInvestmentStockInitialDtl(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblInvestmentStockInitialDtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo=@DNo; ");

            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, InvestmentType, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, A.WhsCode, B.BankAcCode, B.Lot, B.Bin, B.InvestmentCode, B.InvestmentType, B.BatchNo, B.Source, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblInvestmentStockSummary T Set ");
            SQL.AppendLine("    T.Qty=0, T.Qty2=0, T.Qty3=0, T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime()  ");
            SQL.AppendLine("Where BankAcCode=@BankAcCode ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (r + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd3, r, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd3, r, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd3, r, 8));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetGrdStr(Grd3, r, 23));

            return cm;
        }

        private MySqlCommand EditInvestmentStockInitialDtl2(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblInvestmentStockInitialDtl2 Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo=@DNo; ");

            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, InvestmentType, BatchNo, Source, NominalAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, Null As WhsCode, B.BankAcCode, B.Lot, B.Bin, B.InvestmentCode, B.InvestmentType, B.BatchNo, B.Source, ");
            SQL.AppendLine("-1*B.NominalAmt, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblInvestmentStockInitialDtl2 B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblInvestmentStockSummary T Set ");
            SQL.AppendLine("    T.NominalAmt=0, T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime()  ");
            SQL.AppendLine("Where BankAcCode=@BankAcCode ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd4, r, 0));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetGrdStr(Grd4, r, 7));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd4, r, 20));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd4, r, 21));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd4, r, 22));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(string DNo)
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblInvestmentStockInitialDtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo In (" + DNo + "); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Cancelling Investment Stock Initial : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("null, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()); ");
            
            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, B.RemarkJournal, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt, RemarkJournal From (");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark As RemarkJournal");
                SQL.AppendLine("        From TblInvestmentStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo And B.DNo In (" + DNo + ") ");
                SQL.AppendLine("        Inner Join TblInvestmentStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblInvestmentItem D On C.InvestmentCode=D.InvestmentCode ");
                SQL.AppendLine("        Inner Join TblInvestmentCategory E On D.InvestmentCtCode=E.InvestmentCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblInvestmentStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo And B.DNo In (" + DNo + ") ");
                SQL.AppendLine("        Inner Join TblInvestmentStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblInvestmentItem D On C.InvestmentCode=D.InvestmentCode ");
                SQL.AppendLine("        Inner Join TblInvestmentCategory E On D.InvestmentCtCode=E.InvestmentCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark As RemarkJournal");
                SQL.AppendLine("        From TblInvestmentStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo And B.DNo In (" + DNo + ") ");
                SQL.AppendLine("        Inner Join TblInvestmentStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblInvestmentStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblInvestmentStockInitialDtl B On A.DocNo=B.DocNo And B.DNo In (" + DNo + ") ");
                SQL.AppendLine("        Inner Join TblInvestmentStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblInvestmentItem D On C.InvestmentCode=D.InvestmentCode ");
                SQL.AppendLine("        Inner Join TblInvestmentCategory E On D.InvestmentCtCode=E.InvestmentCtCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@AcNoForInitialStockOpeningBalance", mAcNoForInitialStockOpeningBalance);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowInvestmentStockInitialHdr(DocNo);
                ShowInvestmentStockInitialDtl(DocNo);
                ShowInvestmentStockInitialDtl2(DocNo);
                ComputeTotalQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowInvestmentStockInitialHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WhsCode, CurCode, ExcRate, ProductionWorkGroup, Remark From TblInvestmentStockInitialHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "WhsCode", "CurCode", "ExcRate", "Remark",
                        "ProductionWorkGroup"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        SetLueInvestmentSekuritasCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[3]));
                        TxtExcRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    }, true
                );
        }

        private void ShowInvestmentStockInitialDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            #region Comment By RDA
            //SQL.AppendLine("Select A.DNo, A.CancelInd, A.InvestmentCode, B.InvestmentName, B.InvestmentCodeInternal, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            //SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, ");
            //SQL.AppendLine("A.UPrice, A.Remark, B.ItGrpCode, B.ForeignName, B.Specification, "); 
            //SQL.AppendLine("A.InvestmentCtCode, D.OptDesc as InvestmentType, A.BasePrice, A.AqcuisitionCost, A.BankAcCode, C.BankAcNm, E.CurName, A.Issuer ");
            //SQL.AppendLine("From TblInvestmentStockInitialDtl A ");
            //SQL.AppendLine("Left Join TblInvestmentItem B On A.InvestmentCode=B.InvestmentCode ");
            //SQL.AppendLine("LEFT JOIN tblbankaccount C ON C.BankAcCode = A.BankAcCode ");
            //SQL.AppendLine("Left Join TblOption D On A.InvestmentType=D.OptCode and D.OptCat='InvestmentType' ");
            //SQL.AppendLine("LEFT JOIN tblcurrency E ON A.CurCode = E.CurCode ");
            //SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");
            #endregion

            #region Comment By IBL
            /*
            SQL.AppendLine("Select A.DNo, A.CancelInd, A.InvestmentCode, B.PortofolioName, A.BatchNo, A.Source, A.Lot, A.Bin,  ");
            SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, B.UomCode,  ");
            SQL.AppendLine("A.UPrice, A.Remark, B.ForeignName, B.Specification,   ");
            SQL.AppendLine("B.InvestmentCtName, D.OptDesc as InvestmentType, A.BasePrice, A.AqcuisitionCost, A.BankAcCode, C.BankAcNm, E.CurName, A.Issuer, C.BankAcNo, B.PortofolioId  ");
            SQL.AppendLine("From TblInvestmentStockInitialDtl A  ");
            SQL.AppendLine("INNER JOIN ( ");
            SQL.AppendLine("	SELECT X1.InvestmentEquityCode as InvestmentCode, X2.PortofolioName, X1.ForeignName, X3.InvestmentCtName,  ");
            SQL.AppendLine("	X1.UomCode, X4.CurName, X2.Issuer, X1.Specification, X2.InvestmentCtCode, X1.PortofolioId ");
            SQL.AppendLine("	FROM tblinvestmentitemequity X1 ");
            SQL.AppendLine("	INNER JOIN tblinvestmentportofolio X2 ON X1.PortofolioId = X2.PortofolioId  ");
            SQL.AppendLine("	LEFT JOIN tblinvestmentcategory X3 ON X2.InvestmentCtCode = X3.InvestmentCtCode  ");
            SQL.AppendLine("	LEFT JOIN tblcurrency X4 ON X2.CurCode = X4.CurCode ");
            SQL.AppendLine("	UNION ALL  ");
            SQL.AppendLine("	SELECT X1.InvestmentDebtCode as InvestmentCode, X2.PortofolioName, X1.ForeignName, X3.InvestmentCtName,  ");
            SQL.AppendLine("	X1.UomCode, X4.CurName, X2.Issuer, X1.Specification, X2.InvestmentCtCode, X1.PortofolioId  ");
            SQL.AppendLine("	FROM tblinvestmentitemdebt X1  ");
            SQL.AppendLine("	INNER JOIN tblinvestmentportofolio X2 ON X1.PortofolioId = X2.PortofolioId  ");
            SQL.AppendLine("	LEFT JOIN tblinvestmentcategory X3 ON X2.InvestmentCtCode = X3.InvestmentCtCode  ");
            SQL.AppendLine("	LEFT JOIN tblcurrency X4 ON X2.CurCode = X4.CurCode ");
            SQL.AppendLine(")B ON A.InvestmentCode = B.InvestmentCode ");
            SQL.AppendLine("LEFT JOIN tblbankaccount C ON C.BankAcCode = A.BankAcCode  ");
            SQL.AppendLine("Left Join TblOption D On A.InvestmentType=D.OptCode and D.OptCat='InvestmentType'  ");
            SQL.AppendLine("LEFT JOIN tblcurrency E ON A.CurCode = E.CurCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");
            */
            #endregion

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.InvestmentCode, C.PortofolioName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, B.UomCode, A.UPrice, A.Remark, B.ForeignName, B.Specification, D.InvestmentCtName,  ");
            SQL.AppendLine("G.OptDesc As InvestmentType, A.BasePrice, A.AqcuisitionCost, A.BankAcCode, F.BankAcNm, E.CurName, A.Issuer, ");
            SQL.AppendLine("F.BankAcNo, B.PortofolioId ");
            SQL.AppendLine("From TblInvestmentStockInitialDtl A ");
            SQL.AppendLine("Inner Join TblInvestmentItemEquity B On A.InvestmentCode = B.InvestmentEquityCode ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio C On B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("Left Join TblInvestmentCategory D On C.InvestmentCtCode = D.InvestmentCtCode ");
            SQL.AppendLine("Left Join TblCurrency E On A.CurCode = E.CurCode ");
            SQL.AppendLine("Left Join TblBankAccount F On A.BankAcCode = F.BankAcCode ");
            SQL.AppendLine("Left Join TblOption G On A.InvestmentType = G.OptCode And G.OptCat = 'InvestmentType' ");
            SQL.AppendLine("Where A.DocNo = @DocNo Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] { 
                    //0
                    "DNo",
 
                    //1-5
                    "CancelInd", "InvestmentCode", "PortofolioName", "BatchNo", "Source",
                    
                    //6-10
                    "Lot", "Bin", "Qty", "UomCode", "UPrice", 

                    //11-15
                    "Remark", "ForeignName", "Specification", "InvestmentCtName", "InvestmentType",
 
                    //16-20
                    "BasePrice", "AqcuisitionCost", "BankAcCode", "BankAcNm", "CurName",

                    //21-23
                    "Issuer", "BankAcNo", "PortofolioId"
                     
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);  //DNo
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);  //CancelInd
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);  //CancelInd
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);  //InvestmentCode
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3); //InvestmentName -> PortofolioName
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4); //InvestmentCodeInternal
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4); //BatchNo
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5); //Source
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6); //Lot
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7); //Bin
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8); //Qty
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9); //InventoryUomCode
                    //Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11); //Qty2
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12); //InventoryUomCode2
                    //Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);  //Qty3
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14); //InventoryUomCode3
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 10); //UPrice
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 11); //Remark
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17); //ItGrpCode
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 12); //ForeignName
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 13); //Specification
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 14); //InvestmentCtName
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 15); //InvestmentType
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 16); //BasePrice
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 17); //AqcuisitionCost
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 18); //BankAcCode
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 19); //BankAcNm
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20); //CurName
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 21); //Issuer
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 22); //BankAcNo
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 23); //PortofolioId
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd3, Grd3.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 11, 13, 15, 17 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowInvestmentStockInitialDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, B.InvestmentDebtCode, C.PortofolioId As InvestmentCode, C.PortofolioName As InvestmentName, ");
            SQL.AppendLine("F.BankAcCode, F.BankAcNo, F.BankAcNm, D.InvestmentCtName, A.CurCode, C.Issuer, A.InvestmentType, G.OptDesc InvestmentTypeNm, ");
            SQL.AppendLine("A.NominalAmt, A.UPrice As AcquisitionPrice, A.AcquisitionCost, A.BatchNo, A.Source, A.Lot, A.Bin, A.Remark, B.LastCouponDt, B.NextCouponDt ");
            SQL.AppendLine("From TblInvestmentStockInitialDtl2 A ");
            SQL.AppendLine("Inner Join TblInvestmentItemDebt B On A.InvestmentCode = B.InvestmentDebtCode ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio C On B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("Left Join TblInvestmentCategory D On C.InvestmentCtCode = D.InvestmentCtCode ");
            SQL.AppendLine("Left Join TblCurrency E On A.CurCode = E.CurCode ");
            SQL.AppendLine("Left Join TblBankAccount F on A.BankAcCode = F.BankAcCode ");
            SQL.AppendLine("Left Join TblOption G On A.InvestmentType = G.OptCode And G.OptCat = 'InvestmentType' ");
            SQL.AppendLine("Where A.DocNo = @DocNo Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[] { 
                    //0
                    "DNo",
 
                    //1-5
                    "CancelInd", "InvestmentDebtCode", "InvestmentCode", "InvestmentName", "BankAcCode",
                    
                    //6-10
                    "BankAcNo", "BankAcNm", "InvestmentCtName", "CurCode", "Issuer", 

                    //11-15
                    "InvestmentType", "InvestmentTypeNm", "NominalAmt", "AcquisitionPrice", "AcquisitionCost",
 
                    //16-20
                    "BatchNo", "Source", "Lot", "Bin", "Remark",

                    //21-22
                    "LastCouponDt", "NextCouponDt",

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 22);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd4, Grd4.Rows.Count - 1, new int[] { 1 });
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 15, 16, 17});
            Sm.FocusGrd(Grd4, 0, 1);
        }

        #endregion

        #region Additional Method

        private int GetInitDNo2()
        {
            int DNo = 0;

            if (Grd3.Rows.Count == 1)
                DNo = 0;
            else
                DNo = Grd3.Rows.Count - 1;

            return DNo;
        }

        internal void SetLueInvestmentCtCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.InvestmentCtCode As Col1, T.InvestmentCtName As Col2 From TblInvestmentCategory T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (T.InvestmentCtCode=@Code Or ");
                SQL.AppendLine("(T.ActInd='Y' ");
                SQL.AppendLine(")) ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
            }
            SQL.AppendLine("Order By T.InvestmentCtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        internal void SetLueInvestmentSekuritasCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.sekuritasCode As Col1, T.sekuritasName As Col2 From TblInvestmentSekuritas T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where T.sekuritasCode=@Code");
            }
            SQL.AppendLine("Order By T.sekuritasName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetLueInvestmentType(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.OptCode As Col1, T.OptDesc As Col2 From TblOption T ");
            SQL.AppendLine("Where OptCat = 'InvestmentType' ");
            if (Code.Length > 0) SQL.AppendLine("And T.OptCode=@Code");
            SQL.AppendLine("Order By T.OptDesc;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueLot(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Lot As Col1 From TblLotHdr Where ActInd='Y' Order By Lot",
                "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' Order By Bin;",
                "Bin");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 4).Length != 0)
                        SQL += ((SQL.Length != 0?",":"") + "'" + Sm.GetGrdStr(Grd3, Row, 4) + "'");
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void ParPrint(string DocNo, int parValue)
        {
            var l = new List<IS>();
            var ldtl = new List<ISDtl>();

            string[] TableName = { "IS", "ISDtl" };
            List<IList> myLists = new List<IList>();

            #region Header
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, A.Remark ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",

                         "WhsName",
                         "Remark",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new IS()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            WhsName = Sm.DrStr(dr, c[6]),
                            HRemark = Sm.DrStr(dr, c[7]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.InvestmentCode, B.InvestmentName, A.BatchNo,  A.Source, A.Lot, A.Bin, A.Qty, B.InventoryUomCode, ");
                SQLDtl.AppendLine("A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, ");
                SQLDtl.AppendLine("A.UPrice, C.CurCode, A.Remark, B.ItGrpCode ");
                SQLDtl.AppendLine("From TblInvestmentStockInitialDtl A ");
                SQLDtl.AppendLine("Inner Join TblInvestmentItem B On A.InvestmentCode = B.InvestmentCode ");
                SQLDtl.AppendLine("Inner Join TblInvestmentStockInitialHdr C On A.DocNo = C.DocNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And A.CancelInd = 'N' ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "InvestmentCode" ,

                         //1-5
                         "InvestmentName" ,
                         "BatchNo",
                         "Source",
                         "Lot",
                         "Bin",

                         //6-10
                         "Qty" ,
                         "InventoryUomCode",
                         "Qty2" ,
                         "InventoryUomCode2",
                         "Qty3" ,

                         //11-15
                         "InventoryUomCode3",
                         "UPrice",
                         "CurCode",
                         "Remark",
                         "ItGrpCode"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new ISDtl()
                        {
                            InvestmentCode = Sm.DrStr(drDtl, cDtl[0]),

                            InvestmentName = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                            Source = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[7]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[8]),

                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[9]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[10]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            UPrice = Sm.DrDec(drDtl, cDtl[12]),
                            CurCode = Sm.DrStr(drDtl, cDtl[13]),

                            DRemark = Sm.DrStr(drDtl, cDtl[14]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[15]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            switch (parValue)
            {
                case 1:
                    Sm.PrintReport("InitialStock", myLists, TableName, false);
                    break;
                case 2:
                    Sm.PrintReport("InitialStock2", myLists, TableName, false);
                    break;
                case 3:
                    Sm.PrintReport("InitialStock3", myLists, TableName, false);
                    break;
            }
        }

        internal void ComputeTotalQty()
        {
            decimal Total = 0m;
            int col = 11;
            int col2 = 0;
            while (col <= 15)
            {
                Total = 0m;
                for (int row = 0; row <= Grd3.Rows.Count - 1; row++)
                    if (Sm.GetGrdStr(Grd3, row, col).Length != 0) Total += Sm.GetGrdDec(Grd3, row, col);
                
                if (col == 11) col2 = 0;
                if (col == 13) col2 = 1;
                if (col == 15) col2 = 2;
                Grd3.Cells[0, col2].Value = Total;
                col += 2;
            }
        }

        private void GetParameter()
        {
            //SetNumberOfInventoryUomCode();
            //mMainCurCode = Sm.GetParameter("MainCurCode");
            //mAcNoForInitialStockOpeningBalance = Sm.GetParameter("AcNoForInitialStockOpeningBalance");
            //mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            //mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            //mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            //mIsBatchNoUseDocDtIfEmpty = Sm.GetParameterBoo("IsBatchNoUseDocDtIfEmpty");
            //mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            //mIsInvTrnShowItSpec = Sm.GetParameterBoo("IsInvTrnShowItSpec");
            //mIsUseProductionWorkGroup = Sm.GetParameterBoo("IsUseProductionWorkGroup");
            //mIsJournalValidationStockInitialEnabled = Sm.GetParameterBoo("IsJournalValidationStockInitialEnabled");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsItGrpCodeShow', 'MainCurCode', 'IsAutoJournalActived', 'AcNoForInitialStockOpeningBalance', 'IsShowForeignName', ");
            SQL.AppendLine("'IsBatchNoUseDocDtIfEmpty', 'IsMovingAvgEnabled', 'IsInvTrnShowItSpec', 'IsUseProductionWorkGroup', 'IsJournalValidationStockInitialEnabled', ");
            SQL.AppendLine("'NumberOfInventoryUomCode', 'BankAccountTypeForInvestment' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsItGrpCodeShow": mIsItGrpCodeShow = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsBatchNoUseDocDtIfEmpty": mIsBatchNoUseDocDtIfEmpty = ParValue == "Y"; break;
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                            case "IsInvTrnShowItSpec": mIsInvTrnShowItSpec = ParValue == "Y"; break;
                            case "IsUseProductionWorkGroup": mIsUseProductionWorkGroup = ParValue == "Y"; break;
                            case "IsJournalValidationStockInitialEnabled": mIsJournalValidationStockInitialEnabled = ParValue == "Y"; break;

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "AcNoForInitialStockOpeningBalance": mAcNoForInitialStockOpeningBalance = ParValue; break;
                            case "BankAccountTypeForInvestment": mBankAccountTypeForInvestment = ParValue; break;

                            //Integer
                            case "NumberOfInventoryUomCode":
                                if (ParValue.Length == 0)
                                    mNumberOfInventoryUomCode = 1;
                                else
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueInvestmentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueInvestmentType, new Sm.RefreshLue2(SetLueInvestmentType), string.Empty);
        }
        
        private void LueInvestmentType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueInvestmentType_Leave(object sender, EventArgs e)
        {
            if (LueInvestmentType.Visible && fAccept && fCell.ColIndex == 28)
            {
                if (Sm.GetLue(LueInvestmentType).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 31].Value =
                    Grd3.Cells[fCell.RowIndex, 28].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 31].Value = Sm.GetLue(LueInvestmentType);
                    Grd3.Cells[fCell.RowIndex, 28].Value = LueInvestmentType.GetColumnValue("Col2");
                }
                LueInvestmentType.Visible = false;
            }
        }

        private void LueInvestmentType2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueInvestmentType2, new Sm.RefreshLue2(SetLueInvestmentType), string.Empty);
        }

        private void LueInvestmentType2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueInvestmentType2_Leave(object sender, EventArgs e)
        {
            if (LueInvestmentType2.Visible && fAccept && fCell.ColIndex == 14)
            {
                if (Sm.GetLue(LueInvestmentType2).Length == 0)
                    Grd4.Cells[fCell.RowIndex, 13].Value =
                    Grd4.Cells[fCell.RowIndex, 14].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 13].Value = Sm.GetLue(LueInvestmentType2);
                    Grd4.Cells[fCell.RowIndex, 14].Value = LueInvestmentType2.GetColumnValue("Col2");
                }
                LueInvestmentType2.Visible = false;
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(SetLueInvestmentSekuritasCode), string.Empty);
            ClearGrd();
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(SetLueBin));
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 10)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 10].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 10].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
            
            //if (LueBin.Visible && fAccept && fCell.ColIndex == 10)
            //{
            //    if (Sm.GetLue(LueBin).Length == 0)
            //        Grd3.Cells[fCell.RowIndex, 21].Value =
            //        Grd3.Cells[fCell.RowIndex, 10].Value = null;
            //    else
            //    {
            //        Grd3.Cells[fCell.RowIndex, 21].Value = Sm.GetLue(LueBin);
            //        Grd3.Cells[fCell.RowIndex, 10].Value = LueBin.GetColumnValue("Col2");
            //    }
            //    LueBin.Visible = false;
            //    Sm.SetGrdNumValueZero(ref Grd3, (fCell.RowIndex + 1), new int[] { 11, 13, 15, 17 });
            //}
        }
        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue1(SetLueLot));
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 9)
            {
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 9].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 9].Value = LueLot.GetColumnValue("Col1");
                }
                LueLot.Visible = false;
            }
            
            //if (LueLot.Visible && fAccept && fCell.ColIndex == 9)
            //{
            //    if (Sm.GetLue(LueLot).Length == 0)
            //        Grd3.Cells[fCell.RowIndex, 20].Value =
            //        Grd3.Cells[fCell.RowIndex, 9].Value = null;
            //    else
            //    {
            //        Grd3.Cells[fCell.RowIndex, 20].Value = Sm.GetLue(LueLot);
            //        Grd3.Cells[fCell.RowIndex, 9].Value = LueLot.GetColumnValue("Col2");
            //    }
            //    LueLot.Visible = false;
            //    Sm.SetGrdNumValueZero(ref Grd3, (fCell.RowIndex + 1), new int[] { 11, 13, 15, 17 });
            //}
        }

        private void DteLastCouponDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd4, ref fAccept, e);
        }

        private void DteLastCouponDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteLastCouponDt, ref fCell, ref fAccept);
        }

        private void DteNextCouponDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd4, ref fAccept, e);
        }

        private void DteNextCouponDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteNextCouponDt, ref fCell, ref fAccept);
        }

        #endregion

        #region Grid Event

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                ComputeTotalQty();
            }
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmInvestmentStockInitialDlg(this, 1));

            if (e.ColIndex == 22 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0)
                    Sm.FormShowDialog(new FrmInvestmentStockInitialDlg2(this, 1));
                else
                    Sm.StdMsg(mMsgType.Warning, "Please choose investment item.");
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmInvestmentItem("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mInvestmentCode = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd3, new int[] { 13, 15 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd3, new int[] { 7, 9, 10, 18 }, e);

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd3, e.RowIndex, 12), Sm.GetGrdStr(Grd3, e.RowIndex, 14)))
                Sm.CopyGrdValue(Grd3, e.RowIndex, 13, Grd3, e.RowIndex, 11);

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd3, e.RowIndex, 12), Sm.GetGrdStr(Grd3, e.RowIndex, 16)))
                Sm.CopyGrdValue(Grd3, e.RowIndex, 15, Grd3, e.RowIndex, 11);

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd3, e.RowIndex, 14), Sm.GetGrdStr(Grd3, e.RowIndex, 16)))
                Sm.CopyGrdValue(Grd3, e.RowIndex, 15, Grd3, e.RowIndex, 13);

            if (Sm.IsGrdColSelected(new int[] { 11, 13, 15 }, e.ColIndex)) ComputeTotalQty();

            if ((e.ColIndex == 11 || e.ColIndex == 17) && Grd3.Rows.Count > 1)
            {
                if (e.ColIndex == 11 && Sm.GetGrdDec(Grd3, e.RowIndex, 11) <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Quantity should not less than 1.");
                    Grd3.Cells[e.RowIndex, e.ColIndex].Value = 1;
                }
                else if (e.ColIndex == 17 && Sm.GetGrdDec(Grd3, e.RowIndex, 17) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Aqcuisition price should not less than 0.");
                    Grd3.Cells[e.RowIndex, e.ColIndex].Value = 0;
                }
                else
                    Grd3.Cells[Grd3.CurRow.Index, 30].Value = Sm.GetGrdDec(Grd3, e.RowIndex, 11) * Sm.GetGrdDec(Grd3, e.RowIndex, 17);
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmInvestmentStockInitialDlg(this, 1));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 7, 9, 10, 11, 13, 15, 17, 18, 28, 29 }, e.ColIndex))
                    {
                        if (Sm.IsGrdColSelected(new int[] { 9 }, e.ColIndex))
                        {
                            LueRequestEdit(Grd3, LueLot, ref fCell, ref fAccept, e);
                            //SetLueLot(ref LueLot);
                        }

                        if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex))
                        {
                            LueRequestEdit(Grd3, LueBin, ref fCell, ref fAccept, e);
                            //SetLueBin(ref LueBin);
                        }

                        if (e.ColIndex == 28)
                        {
                            Sm.LueRequestEdit(ref Grd3, ref LueInvestmentType, ref fCell, ref fAccept, e);
                        }

                        Sm.GrdRequestEdit(Grd3, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd3, Grd3.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 11, 13, 15, 17, 29 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd3, e.RowIndex, 2) || Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmInvestmentItem("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mInvestmentCode = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        private void Grd3_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 11, 13, 15 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd3, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmInvestmentStockInitialDlg(this, 2));

            if (e.ColIndex == 6 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 3).Length != 0)
                    Sm.FormShowDialog(new FrmInvestmentStockInitialDlg2(this, 2));
                else
                    Sm.StdMsg(mMsgType.Warning, "Please choose investment item.");
            }
        }

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd4, new int[] { 15, 16, 17 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd4, new int[] { 23 }, e);

            if (e.ColIndex == 15 || e.ColIndex == 16)
                Grd4.Cells[Grd4.CurRow.Index, 17].Value = Sm.GetGrdDec(Grd4, e.RowIndex, 15) * Sm.GetGrdDec(Grd4, e.RowIndex, 16) * 0.01m;
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 2)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmInvestmentStockInitialDlg(this, 2));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 1, 14 }, e.ColIndex))
                    {
                        if (e.ColIndex == 14)
                            Sm.LueRequestEdit(ref Grd4, ref LueInvestmentType2, ref fCell, ref fAccept, e);

                        Sm.GrdRequestEdit(Grd4, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd4, Grd4.Rows.Count - 1, new int[] { 1 });
                        Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 15, 16, 17 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && Sm.GetGrdStr(Grd4, e.RowIndex, 3).Length == 0)
                        e.DoDefault = false;
                }

                if (e.ColIndex == 24) Sm.DteRequestEdit(Grd4, DteLastCouponDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 25) Sm.DteRequestEdit(Grd4, DteNextCouponDt, ref fCell, ref fAccept, e);
            }
        }

        private void Grd4_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 15 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd4, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #endregion

        #region Class

        private class IS
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string HRemark { get; set; }
            public string PrintBy { get; set; }
        }

        private class ISDtl
        {
            public string InvestmentCode { get; set; }
            public string InvestmentName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoryUomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public decimal UPrice { get; set; }
            public string CurCode { get; set; }
            public string DRemark { get; set; }
            public string ItGrpCode { get; set; }
        }

        #endregion

    }
}
