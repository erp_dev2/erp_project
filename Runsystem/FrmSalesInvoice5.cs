﻿#region Update
/*
    21/08/2018 [WED] update yang project implementation nya sudah approved
    16/10/2018 [DITA] ketentuan settled di project delivery bisa masuk ke sales invoice for project
    23/10/2018 [TKG] tambah proses journal
    27/11/2018 [MEY] Penambahan nama project di sales invoice for project berdasarkan customer dan project implementation yang sudah di settle.
    17/12/2018 [WED] BobotPercentage PRJI
    11/09/2019 [WED] alur nya diubah ke SOCOntractRevision
    25/09/2019 [WED/YK] journal Sales Invoice dirubah
    02/12/2019 [WED/YK] print out sales invoice for project (class : Invoice5Hdr, Invoice5Dtl)
    12/12/2019 [WED/IMS] tambah fasilitas upload file per DO nya, berdasarkan parameter IsSalesInvoice5AllowToUploadFile
    25/02/2020 [HAR/VIR] Jurnal Sales invoice for project blm sesuai berdasarkan param : mProjectAcNoFormula, 1 ambil dari projectCode, 2 ambil dari project code 2
    29/04/2020 [HAR/YK] untuk save journl terhadap pajak keluaran akan ambil dari master tax (output tax COA) untuk parameter VAT sdh tidak dipakai 
    18/08/2020 [HAR/YK] tambah validasi tidak bisa cancel jika sdh kepakai di AR settlement waktu edit data
    26/08/2020 [WED/YK] nilai journal piutang nya ambil dari nilai amount yg sudah kena pajak invoice nya
    23/09/2020 [WED/IMS] nilai journal tambah dari SOContractHdr.AmtBOM IMS
    02/10/2020 [TKG/YK] ubah journal dan tambah validasi apakah project ada 0 atau > 1
    23/11/2020 [DITA/VIR] base on param IsSLIForProjectTotalAmountBasedOnEstimatedPrice amount ambil dari estimated price PRJI
    12/12/2020 [WED/VIR] journal piutang usaha nilainya belum dikurangi DP
    17/01/2021 [TKG/PHT] ubah GenerateReceipt
    19/01/2021 [WED/YK] perhitungan Invoice Amount berdasarkan parameter SLI5InvoiceAmtCalculationFormat
    03/10/2022 [VIN/VIR] BUG Journal Piutang, total - DP, bukan per detail
    28/11/2022 [TYO/YK] print out merubah source tax amt ambil dari SLI
    07/12/2022 [BRI/YK] penyesuaian journal berdasarkan param SalesInvoice5JournalFormula
    09/12/2022 [VIN/YK] BUG Project Name
    19/12/2022 [RDA/MNET] penambahan remark (mandatory) pada tab list of task
    21/12/2022 [VIN/YK] BUG Project
    06/01/2023 [ICA/MNET] penyesuaian printout MNET
    02/02/2023 [RDA/MNET] tambah kolom tax berdasarkan param SLIPTaxFormat
    12/02/2023 [MAU/MNET] penyesuaian cost center pada journal transaksi SLIP, dengan param IsSLIPJournalUseCC
    02/03/2023 [MYA/MNET] Penyesuaian menu Sales Invoice for Project ( dampak frm baru prji + jurnal yang terbentuk+reprocess+jurnal after approve)
    04/03/2023 [WED/MNET] query PRJI masih ambil dari dtl pertama, harusnya dtl5 berdasarkan parameter IsWBS2SummarizedWBS1
    09/03/2023 [MYA/MNET] Penambahan alamat customer pada Sales Invoice for Project
    09/03/2023 [MYA/MNET] Menambahkan jabatan user yang menandatangani di print out Sales Invoice for Project
    23/04/2023 [MAU/MNET] Lue Project Name menjadi mandatory berdasarkan parameter IsProjectNameSLIPMandatory
*/
#endregion

#region Namespace

using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sl = RunSystem.SetLue;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice5 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty, //if this application is called from other application;
            mIsFormPrintOutInvoice = string.Empty,
            mSLIPTaxFormat = string.Empty,
            TblProjectImplementationDtl = "TblProjectImplementationDtl"
            ;
        private string 
            mMainCurCode = string.Empty,
            mEmpCodeSI = string.Empty,
            mEmpCodeTaxCollector = string.Empty,
            mStateIndicator = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mProjectAcNoFormula = string.Empty,
            mSLI5InvoiceAmtCalculationFormat = string.Empty,
            mSalesInvoice5JournalFormula = string.Empty
            ;
        private bool
            mIsAutoJournalActived = false,
            mIsRemarkForJournalMandatory = false,
            mIsTaxAliasMandatory = false,
            mIsSalesInvoice5AllowToUploadFile = false,
            mIsSLIPJournalUseCC = false;
        internal bool 
            mIsFilterBySite = false,
            mIsCustomerItemNameMandatory = false,
            mIsStageShouldBeSettled = false,
            mIsSLIForProjectTotalAmountBasedOnEstimatedPrice = false,
            mIsWBS2SummarizedWBS1 = false,
            mIsProjectNameSLIPMandatory = false;
        internal FrmSalesInvoice5Find FrmFind;
        iGCell fCell;
        bool fAccept;
        private byte[] downloadedData;
        
        #endregion

        #region Constructor

        public FrmSalesInvoice5(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmSalesInvoice5");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                BtnDueDt.Visible = BtnLocalDocNo.Visible = false;
                BtnDueDt.Enabled = BtnLocalDocNo.Enabled = false;
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                TcVoucher.SelectedTabPage = Tp5;
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueTaxCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueTaxCode1, LueTaxCode2, LueTaxCode3 });

                TcVoucher.SelectedTabPage = Tp4;
                SetLueOptionCode(ref LueOption);
                LueOption.Visible = false;

                TcVoucher.SelectedTabPage = Tp1;
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                SetLueSPCode(ref LueSPCode);
                //SetLueProjectName(ref LueProjectName, string.Empty);
                SetGrd();
                SetFormControl(mState.View);
                if (mIsRemarkForJournalMandatory) LblRemark.ForeColor = Color.Red;
                if (mIsProjectNameSLIPMandatory) label26.ForeColor = Color.Red;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "",
                    "Project Implementation#",
                    "Project Implementation D#",
                    "",
                    "Stage",

                    //6-10
                    "Task",
                    "Bobot (%)",
                    "Amount",
                    "FileName",
                    "",

                    //11-15
                    "",
                    "FileName2",
                    "Tax 1",
                    "Tax 2",
                    "Tax 3",

                    //16-19
                    "Tax 1 Amount",
                    "Tax 2 Amount",
                    "Tax 3 Amount",
                    "Remark"
                },
                 new int[] 
                {
                    //0
                    0, 

                    //1-5
                    20, 150, 0, 20, 150,

                    //6-10
                    150, 100, 150, 100, 20,

                    //11-15
                    20, 100, 50, 50, 50,
                    
                    //16-19
                    150, 150, 150, 400
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 1, 4, 10, 11 });
            Sm.GrdColCheck(Grd1, new int[] { 13, 14, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 16, 17, 18 }, 0);
            if(mSLIPTaxFormat=="1")
                Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 4, 12, 13, 14, 15, 16, 17, 18 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 4, 12, 16, 17, 18 }, false);
            if (!mIsSalesInvoice5AllowToUploadFile) Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 12, 13, 14, 15, 16, 17, 18 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(Grd2, new string[] { "Currency", "Deposit Amount" }, new int[] { 100, 200 });
            Sm.GrdFormatDec(Grd2, new int[] { 1 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 11;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "",
                        "Debit"+Environment.NewLine+"Amount",
                        "",
                        //6-10
                        "Credit"+Environment.NewLine+"Amount",
                        "OptCode",
                        "Option",
                        "Remark",
                        ""
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 20, 100, 20, 
                        //6-10
                        100, 50, 150, 400, 20
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 3, 5, 10 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 7, 10 }, false);

            #endregion
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, DteDocDt, LueCtCode, DteDueDt, TxtLocalDocNo, LueSPCode,
                        LueCurCode, TxtDownpayment, LueBankAcCode, MeeRemark, TxtTotalWithTax, TxtCurTaxRateAmt, MeeCancelReason,
                        TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3, LueTaxCode1, LueTaxCode2, LueTaxCode3, 
                        DteTaxInvoiceDt, DteTaxInvoiceDt2, DteTaxInvoiceDt3, TxtAlias1, TxtAlias2, TxtAlias3, TxtTaxAmtM1, 
                        TxtTaxAmtM2, TxtTaxAmtM3, LueProjectName
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 13, 14, 15, 19 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 11 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, DteDueDt, TxtLocalDocNo, 
                        LueCurCode, TxtDownpayment, MeeRemark,  LueBankAcCode, LueSPCode, TxtDownpayment, TxtCurTaxRateAmt,
                        TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3, LueTaxCode1, LueTaxCode2, LueTaxCode3, 
                        DteTaxInvoiceDt, DteTaxInvoiceDt2, DteTaxInvoiceDt3, TxtAlias1, TxtAlias2, TxtAlias3, LueProjectName
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 13, 14, 15, 19 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 8, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd, MeeCancelReason }, false);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 13, 14, 15 });
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mStateIndicator = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, LueCtCode, DteDueDt, TxtLocalDocNo, 
                 LueCurCode, MeeRemark, LueSPCode, LueBankAcCode,
                 TxtJournalDocNo, TxtJournalDocNo2, TxtReceiptNo,
                 TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3,
                 LueTaxCode1, LueTaxCode2, LueTaxCode3, TxtAlias1, TxtAlias2, TxtAlias3, MeeCancelReason,
                 DteTaxInvoiceDt, DteTaxInvoiceDt2, DteTaxInvoiceDt3, LueProjectName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtTotalAmt, TxtTotalTax, TxtDownpayment, TxtAmt, TxtTotalWithTax, TxtCurTaxRateAmt,
                TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, TxtTaxAmtM1, TxtTaxAmtM2, TxtTaxAmtM3
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearTabTax()
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtTotalAmt, TxtTotalTax, TxtDownpayment, TxtAmt, TxtTotalWithTax, TxtCurTaxRateAmt,
                TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, TxtTaxAmtM1, TxtTaxAmtM2, TxtTaxAmtM3
            }, 0);
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3,
                 LueTaxCode1, LueTaxCode2, LueTaxCode3, TxtAlias1, TxtAlias2, TxtAlias3,
                 DteTaxInvoiceDt, DteTaxInvoiceDt2, DteTaxInvoiceDt3
            });
        }

        #region Clear Grid

        private void ClearGrd()
        {
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
        }

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 8, 16, 17, 18 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 1 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 6 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesInvoice5Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                TxtCurTaxRateAmt.EditValue = Sm.FormatNum(1m, 0);
                SetLueCtCode(ref LueCtCode, string.Empty);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Order by CreateDt limit 1;"));
                mStateIndicator = "I";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mStateIndicator = "E";
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesInvoice5", "TblSalesInvoice5Hdr");
            string ReceiptNo = GenerateReceipt(Sm.GetDte(DteDocDt));
            var lDepositSummary = new List<DepositSummary>();

            ProcessDepositSummary(ref lDepositSummary);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalesInvoice5Hdr(DocNo, ReceiptNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    cml.Add(SaveSalesInvoice5Dtl(DocNo, Row));
                    cml.Add(UpdateProjectImplementationInvoicedInd(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3), "N"));
                }
            }

            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveSalesInvoice5Dtl2(DocNo, Row));
            }

            if (decimal.Parse(TxtDownpayment.Text) != 0m)
            {
                cml.Add(SaveCustomerDeposit(DocNo));
                if (lDepositSummary.Count > 0)
                {
                    for (int i = 0; i < lDepositSummary.Count; i++)
                        cml.Add(SaveSalesInvoice5Dtl3(DocNo, lDepositSummary[i]));
                }
            }

            if (mIsAutoJournalActived) 
                cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            if (mIsSalesInvoice5AllowToUploadFile)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    {
                        if (mIsSalesInvoice5AllowToUploadFile && Sm.GetGrdStr(Grd1, Row, 9).Length > 0 && Sm.GetGrdStr(Grd1, Row, 9) != "openFileDialog1")
                        {
                            UploadFile(DocNo, Row, Sm.GetGrdStr(Grd1, Row, 9));
                            // IsFile = true;
                        }
                    }
                }
            }

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsDteEmpty(DteDueDt, "Due date") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsRemarkEmpty() ||
                IsGrdValueNotValid() ||
                IsDownpaymentNotValid() ||
                IsJournalAmtNotBalanced() ||
                //IsLocalDocNoNotValid() ||
                IsTaxInfoInvalid() ||
                IsTaxAlias() ||
                IsDocHasMoreThan1ProjectCode() ||
                IsNumberofProjectInvalid() || 
                (mIsProjectNameSLIPMandatory && Sm.IsLueEmpty(LueProjectName, "Project Name"));
        }




        private bool IsDocHasMoreThan1ProjectCode()
        {
            var cm = new MySqlCommand(); 
            var SQL = new StringBuilder();
            string DocNo = string.Empty, Filter = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                DocNo = Sm.GetGrdStr(Grd1, r, 1);
                if (DocNo.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.DocNo=@DocNo0" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select Count(1) ");
            SQL.AppendLine("From (");
            if(mProjectAcNoFormula == "2")
                SQL.AppendLine("    Select Distinct ProjectCode2 ");
            else
                SQL.AppendLine("    Select Distinct ProjectCode ");
            SQL.AppendLine("    From TblProjectImplementationHdr A ");
            SQL.AppendLine("    Inner Join TblSocontractRevisionHdr A1 On A.SOContractDocNo = A1.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractHdr B On A1.SOCDocNo=B.DocNo ");
            if(mProjectAcNoFormula == "2")
                SQL.AppendLine("    And B.ProjectCode2 Is Not Null ");
            else
                SQL.AppendLine("    And B.ProjectCode Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(") T; ");
            
            var value = Sm.GetValue(SQL.ToString());
            if (int.Parse(value) > 1)
            {
                Sm.StdMsg(mMsgType.Info, "This document should not have more than 1 project code.");
                return true;
            }
            return false;
        }

        private bool IsTaxInfoInvalid()
        {
            string
                TaxCode1 = Sm.GetLue(LueTaxCode1),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3);

            if (TxtTaxInvoiceNo.Text.Length > 0 || Sm.GetDte(DteTaxInvoiceDt).Length > 0 || Sm.GetLue(LueTaxCode1).Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtTaxInvoiceNo, "Tax invoice#", false)) { TcVoucher.SelectedTabPage = Tp5; return true; }
                if (Sm.IsDteEmpty(DteTaxInvoiceDt, "Tax invoice date")) { TcVoucher.SelectedTabPage = Tp5; return true; }
                if (Sm.IsLueEmpty(LueTaxCode1, "Tax")) { TcVoucher.SelectedTabPage = Tp5; return true; }
            }

            if (TxtTaxInvoiceNo2.Text.Length > 0 || Sm.GetDte(DteTaxInvoiceDt2).Length > 0 || Sm.GetLue(LueTaxCode2).Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtTaxInvoiceNo2, "Tax invoice#", false)) { TcVoucher.SelectedTabPage = Tp5; return true; }
                if (Sm.IsDteEmpty(DteTaxInvoiceDt2, "Tax invoice date")) { TcVoucher.SelectedTabPage = Tp5; return true; }
                if (Sm.IsLueEmpty(LueTaxCode2, "Tax")) { TcVoucher.SelectedTabPage = Tp5; return true; }
            }

            if (TxtTaxInvoiceNo3.Text.Length > 0 || Sm.GetDte(DteTaxInvoiceDt3).Length > 0 || Sm.GetLue(LueTaxCode3).Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtTaxInvoiceNo3, "Tax invoice#", false)) { TcVoucher.SelectedTabPage = Tp5; return true; }
                if (Sm.IsDteEmpty(DteTaxInvoiceDt3, "Tax invoice date")) { TcVoucher.SelectedTabPage = Tp5; return true; }
                if (Sm.IsLueEmpty(LueTaxCode3, "Tax")) { TcVoucher.SelectedTabPage = Tp5; return true; }
            }

            if (TaxCode1.Length == 0 && (TaxCode2.Length > 0 || TaxCode3.Length > 0))
            {
                Sm.StdMsg(mMsgType.Warning, "Tax is empty.");
                TcVoucher.SelectedTabPage = Tp5;
                return true;
            }

            if (TaxCode1.Length > 0)
            {
                var TaxRateAmt = decimal.Parse(TxtCurTaxRateAmt.Text);
                if (Sm.CompareStr(Sm.GetLue(LueCurCode), mMainCurCode))
                {
                    if (TaxRateAmt != 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid currency tax's rate.");
                        TcVoucher.SelectedTabPage = Tp5;
                        return true;
                    }
                }
                else
                {
                    if (TaxRateAmt == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid currency tax's rate.");
                        TcVoucher.SelectedTabPage = Tp5;
                        return true;
                    }
                }

                if (Sm.IsDataExist("Select 1 From TblTax Where TaxCode=@Param And TaxInvoiceInd='Y';", TaxCode1))
                {
                    if (Sm.IsTxtEmpty(TxtTaxInvoiceNo, "Tax invoice#", false)) { TcVoucher.SelectedTabPage = Tp5; return true; }
                    if (Sm.IsDteEmpty(DteTaxInvoiceDt, "Tax invoice date")) { TcVoucher.SelectedTabPage = Tp5; return true; }
                }
            }

            if (TaxCode1.Length > 0 && TaxCode2.Length > 0 && Sm.CompareStr(TaxCode1, TaxCode2))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Tax (1) : " + LueTaxCode1.GetColumnValue("Col2") + Environment.NewLine +
                    "Tax (2) : " + LueTaxCode2.GetColumnValue("Col2") + Environment.NewLine +
                    "Sales Invoice should not have the same taxes."
                    );
                TcVoucher.SelectedTabPage = Tp5;
                return true;
            }

            if (TaxCode1.Length > 0 && TaxCode3.Length > 0 && Sm.CompareStr(TaxCode1, TaxCode3))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Tax (1) : " + LueTaxCode1.GetColumnValue("Col2") + Environment.NewLine +
                    "Tax (3) : " + LueTaxCode3.GetColumnValue("Col2") + Environment.NewLine +
                    "Sales invoice should not have the same taxes."
                    );
                TcVoucher.SelectedTabPage = Tp5;
                return true;
            }

            if (TaxCode2.Length > 0 && TaxCode3.Length > 0 && Sm.CompareStr(TaxCode2, TaxCode3))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Tax (2) : " + LueTaxCode2.GetColumnValue("Col2") + Environment.NewLine +
                    "Tax (3) : " + LueTaxCode3.GetColumnValue("Col2") + Environment.NewLine +
                    "Sales invoice should not have the same taxes."
                    );
                TcVoucher.SelectedTabPage = Tp5;
                return true;
            }
            return false;
        }

        private bool IsTaxAlias()
        {
            if (mIsTaxAliasMandatory && Sm.GetLue(LueTaxCode1).Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtAlias1, "Tax alias", false)) { TcVoucher.SelectedTabPage = Tp5; return true; }
            }

            if (mIsTaxAliasMandatory && Sm.GetLue(LueTaxCode2).Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtAlias2, "Tax alias", false)) { TcVoucher.SelectedTabPage = Tp5; return true; }
            }

            if (mIsTaxAliasMandatory && Sm.GetLue(LueTaxCode3).Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtAlias3, "Tax alias", false)) { TcVoucher.SelectedTabPage = Tp5; return true; }
            }
            return false;
        }

        private bool IsLocalDocNoNotValid()
        {
            //var LocalDocNo = TxtLocalDocNo.Text;
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //{
            //    if (!Sm.CompareStr(LocalDocNo, Sm.GetGrdStr(Grd1, Row, 29)))
            //    {
            //        return (Sm.StdMsgYN("Question", 
            //            "Sales invoice's local document should be the same as all existing PL/DR local document." + 
            //            Environment.NewLine +
            //            "Do you want to continue ?"
            //            )== DialogResult.No);
            //    }
            //}
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Task.");
                TcVoucher.SelectedTabPage = Tp2;
                return true;
            }
            return false;
        }

        private bool IsRemarkEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 19).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Remark is empty");
                    TcVoucher.SelectedTabPage = Tp2;
                    return true;
                }
            }

            
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Task entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                TcVoucher.SelectedTabPage = Tp1;
                return true;
            }
            if (Grd3.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Additional cost, discount information entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                TcVoucher.SelectedTabPage = Tp4;
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                var AcType = string.Empty;

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "COA's account is empty.")) { TcVoucher.SelectedTabPage = Tp4; return true; }
                    if (Sm.GetGrdDec(Grd3, Row, 4) == 0m && Sm.GetGrdDec(Grd3, Row, 6) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount can't be 0.");
                        TcVoucher.SelectedTabPage = Tp4;
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd3, Row, 4) != 0m && Sm.GetGrdDec(Grd3, Row, 6) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount both can't be bigger than 0.");
                        TcVoucher.SelectedTabPage = Tp4;
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsDownpaymentNotValid()
        {
            decimal Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (Downpayment == 0m) return false;

            //Recompute Deposit
            ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));

            //Get Currency
            decimal Deposit = 0m;
            string CurCode = Sm.GetLue(LueCurCode);

            //Get Deposit Amount Based on currency
            if (Grd2.Rows.Count > 0)
            {
                for (int row = 0; row < Grd2.Rows.Count - 1; row++)
                {
                    if (Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, row, 0)))
                    {
                        Deposit = Sm.GetGrdDec(Grd2, row, 1);
                        break;
                    }
                }
            }

            if (Downpayment > Deposit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Currency : " + CurCode + Environment.NewLine +
                    "Deposit Amount: " + Sm.FormatNum(Deposit, 0) + Environment.NewLine +
                    "Downpayment Amount: " + Sm.FormatNum(Downpayment, 0) + Environment.NewLine + Environment.NewLine +
                    "Downpayment is bigger than existing deposit."
                    );
                TcVoucher.SelectedTabPage = Tp1;
                return true;
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) Debit += Sm.GetGrdDec(Grd3, Row, 4);
                if (Sm.GetGrdStr(Grd3, Row, 6).Length > 0) Credit += Sm.GetGrdDec(Grd3, Row, 6);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveCustomerDeposit(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, Case @CancelInd When 'Y' Then '04' Else '03' End As DocType, ");
            SQL.AppendLine("DocDt, CtCode, CurCode, (Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesInvoice5Hdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @CtCode, @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+((Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt), LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoice5Hdr(string DocNo, string ReceiptNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoice5Hdr(DocNo, ReceiptNo, DocDt, CancelInd, ProcessInd, CBDInd, CtCode, ProjectImplementationDocNo, ");
            SQL.AppendLine("LocalDocNo, TaxInvoiceNo, TaxInvoiceNo2, TaxInvoiceNo3, TaxInvoiceDt, TaxInvoiceDt2, TaxInvoiceDt3, ");
            SQL.AppendLine("DueDt, BankAcCode, CurCode, CurTaxRateAmt, TaxCode1, TaxCode2, TaxCode3, TaxAlias1, TaxAlias2, TaxAlias3, ");
            SQL.AppendLine("TaxAmt1, TaxAmt2, TaxAmt3, TaxAmtM1, TaxAmtM2, TaxAmtM3, ");
            SQL.AppendLine("TotalAmt, TotalTax, Downpayment, Amt, MInd, SalesName, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @ReceiptNo, @DocDt, 'N', 'O', 'N', @CtCode, @ProjectImplementationDocNo, ");
            SQL.AppendLine("@LocalDocNo, @TaxInvoiceNo, @TaxInvoiceNo2, @TaxInvoiceNo3, @TaxInvoiceDt, @TaxInvoiceDt2, @TaxInvoiceDt3, ");
            SQL.AppendLine("@DueDt, @BankAcCode, @CurCode, @CurTaxRateAmt, @TaxCode1, @TaxCode2, @TaxCode3, @TaxAlias1, @TaxAlias2, @TaxAlias3, ");
            SQL.AppendLine("@TaxAmt1, @TaxAmt2, @TaxAmt3, @TaxAmtM1, @TaxAmtM2, @TaxAmtM3, ");
            SQL.AppendLine("@TotalAmt, @TotalTax, @Downpayment, @Amt, 'N', @SalesName, @Remark, @CreateBy, CurrentDateTime()); ");

            string Doctitle = Sm.GetParameter("DocTitle");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            if(Doctitle == "KIM")
                Sm.CmParam<String>(ref cm, "@ReceiptNo", ReceiptNo);
            else
                Sm.CmParam<String>(ref cm, "@ReceiptNo", string.Empty);
            
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<String>(ref cm, "@TaxAlias1", TxtAlias1.Text);
            Sm.CmParam<String>(ref cm, "@TaxAlias2", TxtAlias2.Text);
            Sm.CmParam<String>(ref cm, "@TaxAlias3", TxtAlias3.Text);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", decimal.Parse(TxtTaxAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmtM1", decimal.Parse(TxtTaxAmtM1.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmtM2", decimal.Parse(TxtTaxAmtM2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmtM3", decimal.Parse(TxtTaxAmtM3.Text));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CurTaxRateAmt", decimal.Parse(TxtCurTaxRateAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", decimal.Parse(TxtTotalTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@Downpayment", decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@SalesName", Sm.GetLue(LueSPCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ProjectImplementationDocNo", Sm.GetLue(LueProjectName));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoice5Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoice5Dtl(DocNo, DNo, ProjectImplementationDocNo, ProjectImplementationDNo, ProcessInd, Amt, TaxInd1, TaxInd2, TaxInd3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ProjectImplementationDocNo, @ProjectImplementationDNo, 'O', @Amt, @TaxInd1, @TaxInd2, @TaxInd3, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ProjectImplementationDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ProjectImplementationDNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@TaxInd1", Sm.GetGrdBool(Grd1, Row, 13) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@TaxInd2", Sm.GetGrdBool(Grd1, Row, 14) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@TaxInd3", Sm.GetGrdBool(Grd1, Row, 15) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 19));

            return cm;
        }

        private MySqlCommand SaveSalesInvoice5Dtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSalesInvoice5Dtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc,  AcInd,  Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @OptAcDesc,  @AcInd, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@OptAcDesc", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@AcInd", Sm.GetGrdBool(Grd3, Row, 10) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSalesInvoice5Dtl3(string DocNo, DepositSummary i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoice5Dtl3 ");
            SQL.AppendLine("(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @CurCode, @ExcRate, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesInvoice5Hdr Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblCustomerDepositSummary2 Set ");
            SQL.AppendLine("    Amt=Amt-@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where CtCode=@CtCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And ExcRate=@ExcRate; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", i.ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", i.UsedAmt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateProjectImplementationInvoicedInd(string DocNo, string DNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update "+ TblProjectImplementationDtl +" ");
            SQL.AppendLine("     Set InvoicedInd = @InvoicedInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo And SettledInd = 'Y' ");
            SQL.AppendLine("And Exists (Select DocNo From TblProjectImplementationHdr Where DocNo = @DocNo And CancelInd = 'N' And Status = 'A'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@InvoicedInd", CancelInd == "N" ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private bool IsNumberofProjectInvalid()
        {
            if (mProjectAcNoFormula != "2") return false;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, DocNo = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                for (int r=0; r<Grd1.Rows.Count - 1;r++)
                {
                    DocNo = Sm.GetGrdStr(Grd1, r, 2);
                    if (DocNo.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.DocNo=@DocNo" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + r.ToString(), DocNo);
                    }
                }
            }
            if (Filter.Length == 0)
                Filter = " Where 0=1 ";
            else
                Filter = " Where (" + Filter + ")";

            SQL.AppendLine("Select Cast(Count(1) As Decimal(5,0)) From (");
            SQL.AppendLine("    Select Distinct C.ProjectCode2 ");
            SQL.AppendLine("    From TblProjectImplementationHdr A ");
            SQL.AppendLine("    Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractHdr C On B.SOCDocNo=C.DocNo And C.ProjectCode2 Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(") T ");

            cm.CommandText = SQL.ToString();
            var n = Sm.GetValueDec(cm);

            if (n == 0m || n > 1m)
            {
                Sm.StdMsg(mMsgType.Warning, "Each document should only has 1 project.");
                return true;    
            }
            return false;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var CurCode = Sm.GetLue(LueCurCode);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblSalesInvoice5Hdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            if (mIsSLIPJournalUseCC)
                SQL.AppendLine(",CCCode ");
            SQL.AppendLine(")");

            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Sales Invoice For Project : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime() ");
            if (mIsSLIPJournalUseCC)
                SQL.AppendLine(",@CCCode ");
            SQL.AppendLine(" ); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            //Piutang Uninvoice
            if (Sm.CompareStr(mMainCurCode, CurCode))
            {
                if (mProjectAcNoFormula == "2") //YK
                {
                    //SQL.AppendLine("       Select Concat(B.ParValue, G.ProjectCode2) As AcNo, ");
                    //SQL.AppendLine("       0.00 As DAmt, ");
                    //SQL.AppendLine("       ((D.BobotPercentage * 0.01 * G.Amt2) + (D.BobotPercentage * 0.01 * G.AmtBOM)) As CAmt ");
                    //SQL.AppendLine("       From TblSalesInvoice5Hdr A ");
                    //SQL.AppendLine("       Inner Join TblParameter B On B.ParCode = 'CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                    //SQL.AppendLine("       Inner Join TblSalesInvoice5Dtl C On A.DocNo = C.DocNo ");
                    //SQL.AppendLine("       Inner Join TblProjectImplementationDtl D On C.ProjectImplementationDocNo = D.DocNo And C.ProjectImplementationDNo = D.DNo ");
                    //SQL.AppendLine("       Inner Join TblProjectImplementationHdr E On D.DocNo = E.DocNo ");
                    //SQL.AppendLine("       Inner Join TblSOContractRevisionHdr F On E.SOContractDocNo = F.DocNo ");
                    //SQL.AppendLine("       Inner Join TblSOContractHdr G On F.SOCDocNo = G.DocNo ");
                    //SQL.AppendLine("       Where A.DocNo=@DocNo ");

                    SQL.AppendLine("        Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        A.TotalAmt As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoice5Hdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join ( ");
                    SQL.AppendLine("            Select Distinct T1.DocNo, T5.ProjectCode2 ");
                    SQL.AppendLine("            From TblSalesInvoice5Hdr T1 ");
                    SQL.AppendLine("            Inner Join TblSalesInvoice5Dtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("            Inner Join TblProjectImplementationHdr T3 On T2.ProjectImplementationDocNo=T3.DocNo ");
                    SQL.AppendLine("            Inner Join TblSOContractRevisionHdr T4 On T3.SOContractDocNo=T4.DocNo ");
                    SQL.AppendLine("            Inner Join TblSOContractHdr T5 On T4.SOCDocNo=T5.DocNo And T5.ProjectCode2 Is Not Null ");
                    SQL.AppendLine("            Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("       ) C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("       Where A.DocNo=@DocNo ");
                }
                else
                {
                    if (mIsWBS2SummarizedWBS1)
                    {
                        SQL.AppendLine("       Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        SQL.AppendLine("       0.00 As DAmt, ");
                        if (mIsSLIForProjectTotalAmountBasedOnEstimatedPrice)
                        {
                            SQL.AppendLine("       D.EstimatedAmt As CAmt ");
                            SQL.AppendLine("       From TblSalesInvoice5Hdr A ");
                            SQL.AppendLine("       Inner Join TblParameter B On B.ParCode = 'CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                            SQL.AppendLine("       Inner Join TblSalesInvoice5Dtl C On A.DocNo = C.DocNo And A.DocNo = @DocNo ");
                            SQL.AppendLine("       Inner Join "+TblProjectImplementationDtl+" D On C.ProjectImplementationDocNo = D.DocNo And C.ProjectImplementationDNo = D.DNo ");
                            SQL.AppendLine("       Inner Join TblProjectImplementationHdr E On D.DocNo = E.DocNo ");
                            SQL.AppendLine("       Inner Join TblSOContractRevisionHdr F On E.SOContractDocNo = F.DocNo ");
                            SQL.AppendLine("       Inner Join TblSOContractHdr G On F.SOCDocNo = G.DocNo ");
                            SQL.AppendLine("       Inner Join TblProjectDeliveryHdr H On E.DocNo=H.PRJIDocNo ");
                        }
                        else
                        {
                            SQL.AppendLine("       A.TotalAmt As CAmt ");
                            SQL.AppendLine("       From TblSalesInvoice5Hdr A ");
                            SQL.AppendLine("       Inner Join TblParameter B On B.ParCode = 'CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                            SQL.AppendLine("       Where A.DocNo = @DocNo ");
                        }
                        
                    }
                    else
                    {
                        SQL.AppendLine("       Select Concat(B.ParValue, G.ProjectCode) As AcNo, ");
                        SQL.AppendLine("       0.00 As DAmt, ");
                        if (mIsSLIForProjectTotalAmountBasedOnEstimatedPrice)
                            SQL.AppendLine("       D.EstimatedAmt As CAmt ");
                        else
                            SQL.AppendLine("       ((D.BobotPercentage * 0.01 * G.Amt2) + (D.BobotPercentage * 0.01 * G.AmtBOM)) As CAmt ");
                        SQL.AppendLine("       From TblSalesInvoice5Hdr A ");
                        SQL.AppendLine("       Inner Join TblParameter B On B.ParCode = 'CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                        SQL.AppendLine("       Inner Join TblSalesInvoice5Dtl C On A.DocNo = C.DocNo And A.DocNo = @DocNo ");
                        SQL.AppendLine("       Inner Join "+TblProjectImplementationDtl +" D On C.ProjectImplementationDocNo = D.DocNo And C.ProjectImplementationDNo = D.DNo ");
                        SQL.AppendLine("       Inner Join TblProjectImplementationHdr E On D.DocNo = E.DocNo ");
                        SQL.AppendLine("       Inner Join TblSOContractRevisionHdr F On E.SOContractDocNo = F.DocNo ");
                        SQL.AppendLine("       Inner Join TblSOContractHdr G On F.SOCDocNo = G.DocNo ");
                    }
                    
                }
                
            }
            else
            {
                if (mProjectAcNoFormula == "2")
                {
                    SQL.AppendLine("       Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                    SQL.AppendLine("       0.00 As DAmt, ");
                    SQL.AppendLine("       IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("       ), 0.00) * ");
                    SQL.AppendLine("       A.TotalAmt As CAmt ");
                    SQL.AppendLine("       From TblSalesInvoice5Hdr A ");
                    SQL.AppendLine("       Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                    SQL.AppendLine("       Inner Join ( ");
                    SQL.AppendLine("            Select Distinct T1.DocNo, T5.ProjectCode2 ");
                    SQL.AppendLine("            From TblSalesInvoice5Hdr T1 ");
                    SQL.AppendLine("            Inner Join TblSalesInvoice5Dtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("            Inner Join TblProjectImplementationHdr T3 On T2.ProjectImplementationDocNo=T3.DocNo ");
                    SQL.AppendLine("            Inner Join TblSOContractRevisionHdr T4 On T3.SOContractDocNo=T4.DocNo ");
                    SQL.AppendLine("            Inner Join TblSOContractHdr T5 On T4.SOCDocNo=T5.DocNo And T5.ProjectCode2 Is Not Null ");
                    SQL.AppendLine("            Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("       ) C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("       Where A.DocNo=@DocNo ");
                }
                else
                {
                    if (mIsWBS2SummarizedWBS1)
                    {
                        SQL.AppendLine("       Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        SQL.AppendLine("       0.00 As DAmt, ");
                        SQL.AppendLine("       IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00) * ");
                        if (mIsSLIForProjectTotalAmountBasedOnEstimatedPrice)
                        {
                            SQL.AppendLine("       D.EstimatedAmt As CAmt ");
                            SQL.AppendLine("       From TblSalesInvoice5Hdr A ");
                            SQL.AppendLine("       Inner Join TblParameter B On B.ParCode = 'CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                            SQL.AppendLine("       Inner Join TblSalesInvoice5Dtl C On A.DocNo = C.DocNo And A.DocNo = @DocNo ");
                            SQL.AppendLine("       Inner Join "+TblProjectImplementationDtl+" D On C.ProjectImplementationDocNo=D.DocNo And C.ProjectImplementationDNo=D.DNo ");
                            SQL.AppendLine("       Inner Join TblProjectImplementationHdr E On D.DocNo=E.DocNo ");
                            SQL.AppendLine("       Inner Join TblSOContractRevisionHdr F On E.SOContractDocNo=F.DocNo ");
                            SQL.AppendLine("       Inner Join TblSOContractHdr G On F.SOCDocNo=G.DocNo ");
                            SQL.AppendLine("       Inner Join TblProjectDeliveryHdr H On E.DocNo=H.PRJIDocNo ");
                        }
                        else
                        {
                            //SQL.AppendLine("       ((D.BobotPercentage * 0.01 * G.Amt2) + (D.BobotPercentage * 0.01 * G.AmtBOM)) As CAmt ");
                            SQL.AppendLine("       A.TotalAmt As CAmt ");
                            SQL.AppendLine("       From TblSalesInvoice5Hdr A ");
                            SQL.AppendLine("       Inner Join TblParameter B On B.ParCode = 'CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                            SQL.AppendLine("       Inner Join TblSalesInvoice5Dtl C On A.DocNo = C.DocNo And A.DocNo = @DocNo ");
                            SQL.AppendLine("       Inner Join "+TblProjectImplementationDtl+" D On C.ProjectImplementationDocNo=D.DocNo And C.ProjectImplementationDNo=D.DNo ");
                            SQL.AppendLine("       Inner Join TblProjectImplementationHdr E On D.DocNo=E.DocNo ");
                            SQL.AppendLine("       Inner Join TblSOContractRevisionHdr F On E.SOContractDocNo=F.DocNo ");
                            SQL.AppendLine("       Inner Join TblSOContractHdr G On F.SOCDocNo=G.DocNo ");
                            SQL.AppendLine("       Inner Join TblProjectDeliveryHdr H On E.DocNo=H.PRJIDocNo ");
                        }
                    }
                    else
                    {
                        SQL.AppendLine("       Select Concat(B.ParValue, G.ProjectCode) As AcNo, ");
                        SQL.AppendLine("       0.00 As DAmt, ");
                        SQL.AppendLine("       IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00) * ");
                        if (mIsSLIForProjectTotalAmountBasedOnEstimatedPrice)
                            SQL.AppendLine("       D.EstimatedAmt As CAmt ");
                        else
                            SQL.AppendLine("       ((D.BobotPercentage * 0.01 * G.Amt2) + (D.BobotPercentage * 0.01 * G.AmtBOM)) As CAmt ");
                        SQL.AppendLine("       From TblSalesInvoice5Hdr A ");
                        SQL.AppendLine("       Inner Join TblParameter B On B.ParCode = 'CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                        SQL.AppendLine("       Inner Join TblSalesInvoice5Dtl C On A.DocNo = C.DocNo And A.DocNo = @DocNo ");
                        SQL.AppendLine("       Inner Join "+TblProjectImplementationDtl+" D On C.ProjectImplementationDocNo=D.DocNo And C.ProjectImplementationDNo=D.DNo ");
                        SQL.AppendLine("       Inner Join TblProjectImplementationHdr E On D.DocNo=E.DocNo ");
                        SQL.AppendLine("       Inner Join TblSOContractRevisionHdr F On E.SOContractDocNo=F.DocNo ");
                        SQL.AppendLine("       Inner Join TblSOContractHdr G On F.SOCDocNo=G.DocNo ");
                    }
                }
            }

            // Piutang Usaha
            if (Sm.CompareStr(mMainCurCode, CurCode))
            {
                SQL.AppendLine("    Union All ");
                if (mProjectAcNoFormula == "2")
                {
                    //SQL.AppendLine("        Select Concat(B.ParValue, F.ProjectCode2) As AcNo, ");
                    //SQL.AppendLine("        ( ");
                    //SQL.AppendLine("        ((D.BobotPercentage*0.01)*(F.Amt2+(F.Amt2*0.01*IfNull(G.TaxRate, 0.00))+(F.Amt2*0.01*IfNull(H.TaxRate, 0.00))+(F.Amt2*0.01*IfNull(I.TaxRate, 0.00)))) + ");
                    //SQL.AppendLine("        ((D.BobotPercentage*0.01)*(F.AmtBOM+(F.AmtBOM*0.01*IfNull(G.TaxRate, 0.00))+(F.AmtBOM*0.01*IfNull(H.TaxRate, 0.00))+(F.AmtBOM*0.01*IfNull(I.TaxRate, 0.00)))) ");
                    //SQL.AppendLine("        )  As DAmt, ");
                    //SQL.AppendLine("        0.00 As CAmt ");
                    //SQL.AppendLine("        From TblSalesInvoice5Hdr A ");
                    //SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                    //SQL.AppendLine("        Inner Join TblSalesInvoice5Dtl C On A.DocNo=C.DocNo ");
                    //SQL.AppendLine("        Inner Join "+TblProjectImplementationDtl+" D On C.ProjectImplementationDocNo=D.DocNo And C.ProjectImplementationDNo=D.DNo ");
                    //SQL.AppendLine("        Inner Join TblProjectImplementationHdr E On D.DocNo=E.DocNo ");
                    //SQL.AppendLine("        Inner Join TblSOCOntractRevisionHdr E1 On E.SOContractDocNo = E1.DocNo ");
                    //SQL.AppendLine("        Inner Join TblSOContractHdr F On E1.SOCDocNo=F.DocNo ");
                    //SQL.AppendLine("        Left Join TblTax G On A.TaxCode1=G.TaxCode ");
                    //SQL.AppendLine("        Left Join TblTax H On A.TaxCode2=H.TaxCode ");
                    //SQL.AppendLine("        Left Join TblTax I On A.TaxCode3=I.TaxCode ");
                    //SQL.AppendLine("        Where A.DocNo=@DocNo ");

                    SQL.AppendLine("        Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                    SQL.AppendLine("        A.Amt As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoice5Hdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join ( ");
                    SQL.AppendLine("            Select Distinct T1.DocNo, T5.ProjectCode2 ");
                    SQL.AppendLine("            From TblSalesInvoice5Hdr T1 ");
                    SQL.AppendLine("            Inner Join TblSalesInvoice5Dtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("            Inner Join TblProjectImplementationHdr T3 On T2.ProjectImplementationDocNo=T3.DocNo ");
                    SQL.AppendLine("            Inner Join TblSOContractRevisionHdr T4 On T3.SOContractDocNo=T4.DocNo ");
                    SQL.AppendLine("            Inner Join TblSOContractHdr T5 On T4.SOCDocNo=T5.DocNo And T5.ProjectCode2 Is Not Null ");
                    SQL.AppendLine("            Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("       ) C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    if (mIsWBS2SummarizedWBS1)
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        SQL.AppendLine("        ( ");
                        if (mIsSLIForProjectTotalAmountBasedOnEstimatedPrice)
                        {
                            SQL.AppendLine("        Sum((D.EstimatedAmt + (D.EstimatedAmt*0.01*IfNull(G.TaxRate, 0.00)) + (D.EstimatedAmt*0.01*IfNull(H.TaxRate, 0.00)) + (D.EstimatedAmt*0.01*IfNull(I.TaxRate, 0.00)) )) - A.Downpayment ");
                        }
                        else
                        {
                            SQL.AppendLine("        A.Amt ");
                        }
                        SQL.AppendLine("        )  As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoice5Hdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                        SQL.AppendLine("        Inner Join TblSalesInvoice5Dtl C On A.DocNo=C.DocNo ");
                        SQL.AppendLine("        Inner Join "+TblProjectImplementationDtl+" D On C.ProjectImplementationDocNo=D.DocNo And C.ProjectImplementationDNo=D.DNo ");
                        SQL.AppendLine("        Inner Join TblProjectImplementationHdr E On D.DocNo=E.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOCOntractRevisionHdr E1 On E.SOContractDocNo = E1.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractHdr F On E1.SOCDocNo=F.DocNo ");
                        SQL.AppendLine("        Left Join TblTax G On A.TaxCode1=G.TaxCode ");
                        SQL.AppendLine("        Left Join TblTax H On A.TaxCode2=H.TaxCode ");
                        SQL.AppendLine("        Left Join TblTax I On A.TaxCode3=I.TaxCode ");
                        SQL.AppendLine("        Inner Join TblProjectDeliveryHdr J On E.DocNo=J.PRJIDocNo ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        SQL.AppendLine("        Group By A.DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, F.ProjectCode) As AcNo, ");
                        SQL.AppendLine("        ( ");
                        if (mIsSLIForProjectTotalAmountBasedOnEstimatedPrice)
                        {
                            SQL.AppendLine("        Sum((D.EstimatedAmt + (D.EstimatedAmt*0.01*IfNull(G.TaxRate, 0.00)) + (D.EstimatedAmt*0.01*IfNull(H.TaxRate, 0.00)) + (D.EstimatedAmt*0.01*IfNull(I.TaxRate, 0.00)) )) - A.Downpayment ");
                        }
                        else
                        {
                            SQL.AppendLine("        ((D.BobotPercentage*0.01)*(F.Amt2+(F.Amt2*0.01*IfNull(G.TaxRate, 0.00))+(F.Amt2*0.01*IfNull(H.TaxRate, 0.00))+(F.Amt2*0.01*IfNull(I.TaxRate, 0.00)))) + ");
                            SQL.AppendLine("        ((D.BobotPercentage*0.01)*(F.AmtBOM+(F.AmtBOM*0.01*IfNull(G.TaxRate, 0.00))+(F.AmtBOM*0.01*IfNull(H.TaxRate, 0.00))+(F.AmtBOM*0.01*IfNull(I.TaxRate, 0.00)))) ");
                            SQL.AppendLine("        - A.Downpayment ");
                        }
                        SQL.AppendLine("        )  As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoice5Hdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                        SQL.AppendLine("        Inner Join TblSalesInvoice5Dtl C On A.DocNo=C.DocNo ");
                        SQL.AppendLine("        Inner Join "+TblProjectImplementationDtl+" D On C.ProjectImplementationDocNo=D.DocNo And C.ProjectImplementationDNo=D.DNo ");
                        SQL.AppendLine("        Inner Join TblProjectImplementationHdr E On D.DocNo=E.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOCOntractRevisionHdr E1 On E.SOContractDocNo = E1.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractHdr F On E1.SOCDocNo=F.DocNo ");
                        SQL.AppendLine("        Left Join TblTax G On A.TaxCode1=G.TaxCode ");
                        SQL.AppendLine("        Left Join TblTax H On A.TaxCode2=H.TaxCode ");
                        SQL.AppendLine("        Left Join TblTax I On A.TaxCode3=I.TaxCode ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        SQL.AppendLine("        Group By A.DocNo ");
                    }
                }
            }
            else
            {
                SQL.AppendLine("    Union All ");
                if (mProjectAcNoFormula == "2")
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                    SQL.AppendLine("        A.Amt As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoice5Hdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join ( ");
                    SQL.AppendLine("            Select Distinct T1.DocNo, T5.ProjectCode2 ");
                    SQL.AppendLine("            From TblSalesInvoice5Hdr T1 ");
                    SQL.AppendLine("            Inner Join TblSalesInvoice5Dtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("            Inner Join TblProjectImplementationHdr T3 On T2.ProjectImplementationDocNo=T3.DocNo ");
                    SQL.AppendLine("            Inner Join TblSOContractRevisionHdr T4 On T3.SOContractDocNo=T4.DocNo ");
                    SQL.AppendLine("            Inner Join TblSOContractHdr T5 On T4.SOCDocNo=T5.DocNo And T5.ProjectCode2 Is Not Null ");
                    SQL.AppendLine("            Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("       ) C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    if (mIsWBS2SummarizedWBS1)
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00)* ");
                        SQL.AppendLine("        ( ");
                        if (mIsSLIForProjectTotalAmountBasedOnEstimatedPrice)
                        {
                            //SQL.AppendLine("        Sum(D.EstimatedAmt + (D.EstimatedAmt*0.01*IfNull(G.TaxRate, 0.00)) + (D.EstimatedAmt*0.01*IfNull(H.TaxRate, 0.00)) + (D.EstimatedAmt*0.01*IfNull(I.TaxRate, 0.00)) ) - A.Downpayment ");
                            SQL.AppendLine("        Sum(J.Amt + (J.Amt*0.01*IfNull(G.TaxRate, 0.00)) + (J.Amt*0.01*IfNull(H.TaxRate, 0.00)) + (J.Amt*0.01*IfNull(I.TaxRate, 0.00)) ) - A.Downpayment ");
                        }
                        else
                        {
                            SQL.AppendLine("        ((D.BobotPercentage*0.01)*(F.Amt2+(F.Amt2*0.01*IfNull(G.TaxRate, 0.00))+(F.Amt2*0.01*IfNull(H.TaxRate, 0.00))+(F.Amt2*0.01*IfNull(I.TaxRate, 0.00)))) + ");
                            SQL.AppendLine("        ((D.BobotPercentage*0.01)*(F.AmtBOM+(F.AmtBOM*0.01*IfNull(G.TaxRate, 0.00))+(F.AmtBOM*0.01*IfNull(H.TaxRate, 0.00))+(F.AmtBOM*0.01*IfNull(I.TaxRate, 0.00)))) ");
                            SQL.AppendLine("        - A.Downpayment ");
                        }
                        SQL.AppendLine("        ) As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoice5Hdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                        SQL.AppendLine("        Inner Join TblSalesInvoice5Dtl C On A.DocNo=C.DocNo ");
                        SQL.AppendLine("        Inner Join "+TblProjectImplementationDtl+" D On C.ProjectImplementationDocNo=D.DocNo And C.ProjectImplementationDNo=D.DNo ");
                        SQL.AppendLine("        Inner Join TblProjectImplementationHdr E On D.DocNo=E.DocNo ");
                        SQL.AppendLine("        INner JOin TblSOContractRevisionHdr E1 On E.SOCOntractDocNo = E1.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractHdr F On E1.SOCDocNo=F.DocNo ");
                        SQL.AppendLine("        Left Join TblTax G On A.TaxCode1=G.TaxCode ");
                        SQL.AppendLine("        Left Join TblTax H On A.TaxCode2=H.TaxCode ");
                        SQL.AppendLine("        Left Join TblTax I On A.TaxCode3=I.TaxCode ");
                        SQL.AppendLine("        Inner Join TblProjectDeliveryHdr J On E.DocNo=J.PRJIDocNo ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        SQL.AppendLine("        Group By A.DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, F.ProjectCode) As AcNo, ");
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00)* ");
                        SQL.AppendLine("        ( ");
                        if (mIsSLIForProjectTotalAmountBasedOnEstimatedPrice)
                        {
                            SQL.AppendLine("        Sum(D.EstimatedAmt + (D.EstimatedAmt*0.01*IfNull(G.TaxRate, 0.00)) + (D.EstimatedAmt*0.01*IfNull(H.TaxRate, 0.00)) + (D.EstimatedAmt*0.01*IfNull(I.TaxRate, 0.00)) ) - A.Downpayment ");
                        }
                        else
                        {
                            SQL.AppendLine("        ((D.BobotPercentage*0.01)*(F.Amt2+(F.Amt2*0.01*IfNull(G.TaxRate, 0.00))+(F.Amt2*0.01*IfNull(H.TaxRate, 0.00))+(F.Amt2*0.01*IfNull(I.TaxRate, 0.00)))) + ");
                            SQL.AppendLine("        ((D.BobotPercentage*0.01)*(F.AmtBOM+(F.AmtBOM*0.01*IfNull(G.TaxRate, 0.00))+(F.AmtBOM*0.01*IfNull(H.TaxRate, 0.00))+(F.AmtBOM*0.01*IfNull(I.TaxRate, 0.00)))) ");
                            SQL.AppendLine("        - A.Downpayment ");
                        }
                        SQL.AppendLine("        ) As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoice5Hdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                        SQL.AppendLine("        Inner Join TblSalesInvoice5Dtl C On A.DocNo=C.DocNo ");
                        SQL.AppendLine("        Inner Join "+TblProjectImplementationDtl+" D On C.ProjectImplementationDocNo=D.DocNo And C.ProjectImplementationDNo=D.DNo ");
                        SQL.AppendLine("        Inner Join TblProjectImplementationHdr E On D.DocNo=E.DocNo ");
                        SQL.AppendLine("        INner JOin TblSOContractRevisionHdr E1 On E.SOCOntractDocNo = E1.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractHdr F On E1.SOCDocNo=F.DocNo ");
                        SQL.AppendLine("        Left Join TblTax G On A.TaxCode1=G.TaxCode ");
                        SQL.AppendLine("        Left Join TblTax H On A.TaxCode2=H.TaxCode ");
                        SQL.AppendLine("        Left Join TblTax I On A.TaxCode3=I.TaxCode ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        SQL.AppendLine("        Group By A.DocNo ");
                    }
                }
            }

            //Downpayment
            if (TxtDownpayment.Text.Length > 0 && decimal.Parse(TxtDownpayment.Text) != 0)
            {
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("    Union All ");
                    if (mProjectAcNoFormula == "2")
                    {
                        //SQL.AppendLine("    Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                        //SQL.AppendLine("    A.Downpayment As DAmt, 0.00 As CAmt ");
                        //SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                        //SQL.AppendLine("    Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' And B.ParValue Is Not Null ");
                        //SQL.AppendLine("    Inner Join ( ");
                        //SQL.AppendLine("        Select T4.ProjectCode2, T4.ProjectCode ");
                        //SQL.AppendLine("        From TblSalesInvoice5Dtl T1 ");
                        //SQL.AppendLine("        Inner Join "+TblProjectImplementationDtl+" T2 On T1.ProjectImplementationDocNo=T2.DocNo And T1.ProjectImplementationDNo=T2.DNo ");
                        //SQL.AppendLine("        Inner Join TblProjectImplementationHdr T3 On T2.DocNo=T3.DocNo ");
                        //SQL.AppendLine("        Inner JOin TblSOContractRevisionHdr T31 On T3.SOContractDocNo = T31.Docno ");
                        //SQL.AppendLine("        Inner Join TblSOContractHdr T4 On T31.SOCDocNo=T4.DocNo ");
                        //SQL.AppendLine("        Where T1.DocNo=@DocNo Limit 1");
                        //SQL.AppendLine("    ) C On 1=1 ");
                        //SQL.AppendLine("    Where A.DocNo=@DocNo ");

                        SQL.AppendLine("    Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                        SQL.AppendLine("    A.Downpayment As DAmt, ");
                        SQL.AppendLine("    0.00 As CAmt ");
                        SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                        SQL.AppendLine("    Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' And B.ParValue Is Not Null ");
                        SQL.AppendLine("    Inner Join ( ");
                        SQL.AppendLine("        Select Distinct T1.DocNo, T5.ProjectCode2 ");
                        SQL.AppendLine("        From TblSalesInvoice5Hdr T1 ");
                        SQL.AppendLine("        Inner Join TblSalesInvoice5Dtl T2 On T1.DocNo=T2.DocNo ");
                        SQL.AppendLine("        Inner Join TblProjectImplementationHdr T3 On T2.ProjectImplementationDocNo=T3.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractRevisionHdr T4 On T3.SOContractDocNo=T4.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractHdr T5 On T4.SOCDocNo=T5.DocNo And T5.ProjectCode2 Is Not Null ");
                        SQL.AppendLine("        Where T1.DocNo=@DocNo ");
                        SQL.AppendLine("    ) C On A.DocNo=C.DocNo ");
                        SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("    Select Concat(B.ParValue, C.ProjectCode) As AcNo, ");
                        SQL.AppendLine("    A.Downpayment As DAmt, 0.00 As CAmt ");
                        SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                        SQL.AppendLine("    Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' And B.ParValue Is Not Null ");
                        SQL.AppendLine("    Inner Join ( ");
                        SQL.AppendLine("        Select T4.ProjectCode2, T4.ProjectCode ");
                        SQL.AppendLine("        From TblSalesInvoice5Dtl T1 ");
                        SQL.AppendLine("        Inner Join "+TblProjectImplementationDtl+" T2 On T1.ProjectImplementationDocNo=T2.DocNo And T1.ProjectImplementationDNo=T2.DNo ");
                        SQL.AppendLine("        Inner Join TblProjectImplementationHdr T3 On T2.DocNo=T3.DocNo ");
                        SQL.AppendLine("        Inner JOin TblSOContractRevisionHdr T31 On T3.SOContractDocNo = T31.Docno ");
                        SQL.AppendLine("        Inner Join TblSOContractHdr T4 On T31.SOCDocNo=T4.DocNo ");
                        SQL.AppendLine("        Where T1.DocNo=@DocNo Limit 1");
                        SQL.AppendLine("    ) C On 1=1 ");
                        SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    }
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    if (mProjectAcNoFormula == "2")
                    {
                        SQL.AppendLine("    Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                        SQL.AppendLine("    IfNull(( ");
                        SQL.AppendLine("        Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("    ), 0.00) * ");
                        SQL.AppendLine("    A.Downpayment As DAmt, ");
                        SQL.AppendLine("    0.00 As CAmt ");
                        SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                        SQL.AppendLine("    Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' And B.ParValue Is Not Null ");
                        SQL.AppendLine("    Inner Join ( ");
                        SQL.AppendLine("        Select Distinct T1.DocNo, T5.ProjectCode2 ");
                        SQL.AppendLine("        From TblSalesInvoice5Hdr T1 ");
                        SQL.AppendLine("        Inner Join TblSalesInvoice5Dtl T2 On T1.DocNo=T2.DocNo ");
                        SQL.AppendLine("        Inner Join TblProjectImplementationHdr T3 On T2.ProjectImplementationDocNo=T3.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractRevisionHdr T4 On T3.SOContractDocNo=T4.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractHdr T5 On T4.SOCDocNo=T5.DocNo And T5.ProjectCode2 Is Not Null ");
                        SQL.AppendLine("        Where T1.DocNo=@DocNo ");
                        SQL.AppendLine("    ) C On A.DocNo=C.DocNo ");
                        SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("    Select Concat(B.ParValue, C.ProjectCode) As AcNo, ");
                        SQL.AppendLine("    D.ExcRate*D.Amt As DAmt As DAmt, 0.00 As CAmt ");
                        SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                        SQL.AppendLine("    Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' And B.ParValue Is Not Null ");
                        SQL.AppendLine("    Inner Join ( ");
                        SQL.AppendLine("        Select T4.ProjectCode2, T4.ProjectCode ");
                        SQL.AppendLine("        From TblSalesInvoice5Dtl T1 ");
                        SQL.AppendLine("        Inner Join "+TblProjectImplementationDtl+" T2 On T1.ProjectImplementationDocNo=T2.DocNo And T1.ProjectImplementationDNo=T2.DNo ");
                        SQL.AppendLine("        Inner Join TblProjectImplementationHdr T3 On T2.DocNo=T3.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractRevisionHdr T31 On T3.SOContractDocNo = T31.Docno ");
                        SQL.AppendLine("        Inner Join TblSOContractHdr T4 On T31.SOCDocNo=T4.DocNo ");
                        SQL.AppendLine("        Where T1.DocNo=@DocNo Limit 1");
                        SQL.AppendLine("    ) C On 1=1 ");
                        SQL.AppendLine("    Inner Join TblSalesInvoice5Dtl3 D On A.DocNo=D.DocNo ");
                        SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    }
                }
            }
            
            //PPN Keluaran
            if (Sm.CompareStr(mMainCurCode, CurCode))
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    A.TaxAmt1 As CAmt ");
                SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                SQL.AppendLine("    Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo2 Is Not Null ");
                SQL.AppendLine("    Where A.TaxCode1 Is Not Null And A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    A.TaxAmt2 As CAmt ");
                SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                SQL.AppendLine("    Inner Join TblTax B On A.TaxCode2 = B.TaxCode And B.AcNo2 Is Not Null ");
                SQL.AppendLine("    Where A.TaxCode2 Is Not Null And A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    A.TaxAmt3 As CAmt ");
                SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                SQL.AppendLine("    Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 Is Not Null ");
                SQL.AppendLine("    Where A.TaxCode3 Is Not Null And A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    A.CurTaxRateAmt*A.TaxAmt1 As CAmt ");
                SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                SQL.AppendLine("    Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo2 Is Not Null ");
                SQL.AppendLine("    Where A.TaxCode1 Is Not Null And A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    A.CurTaxRateAmt*A.TaxAmt2 As CAmt ");
                SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                SQL.AppendLine("    Inner Join TblTax B On A.TaxCode2 = B.TaxCode And B.AcNo2 Is Not Null ");
                SQL.AppendLine("    Where A.TaxCode2 Is Not Null And A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    A.CurTaxRateAmt*A.TaxAmt3 As CAmt ");
                SQL.AppendLine("    From TblSalesInvoice5Hdr A ");
                SQL.AppendLine("    Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 Is Not Null ");
                SQL.AppendLine("    Where A.TaxCode3 Is Not Null And A.DocNo=@DocNo ");
            }

            if (mSalesInvoice5JournalFormula == "1")
            {
                //List of COA
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo, ");
                if (!Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0) * ");
                }
                SQL.AppendLine("        B.DAmt, ");
                if (!Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0) * ");
                }
                SQL.AppendLine("        B.CAmt ");
                SQL.AppendLine("        From TblSalesInvoice5Hdr A ");
                SQL.AppendLine("        Inner Join TblSalesInvoice5Dtl2 B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            //Laba rugi selisih kurs
            if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignExchange' And ParValue Is Not Null ");
            }

            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo  ");
            SQL.AppendLine(") T;  ");

            if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0.00 End, ");
                SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0.00 End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignExchange' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");
            }

            if (mSalesInvoice5JournalFormula == "2")
            {
                //update COA Exists
                SQL.AppendLine("UPDATE TblJournalDtl A ");
                SQL.AppendLine("INNER JOIN ( ");
                SQL.AppendLine("	SELECT B.AcNo, ");
                if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                }
                SQL.AppendLine("	C.DAmt + Case ");
                SQL.AppendLine("		When ColType = 'D' AND B.DAmt > 0 then B.DAmt ");
                SQL.AppendLine("		When ColType = 'D' AND B.CAmt > 0 Then B.CAmt*-1 ");
                SQL.AppendLine("		ELSE 0 ");
                SQL.AppendLine("	END DAmtNew, ");
                if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                }
                SQL.AppendLine("	C.CAmt + Case ");
                SQL.AppendLine("		When ColType = 'C' AND B.CAmt > 0 then B.CAmt ");
                SQL.AppendLine("		When ColType = 'C' AND B.DAmt > 0 then B.DAmt*-1 ");
                SQL.AppendLine("		ELSE 0 ");
                SQL.AppendLine("	END CAmtNew ");
                SQL.AppendLine("	FROM TblSalesInvoice5Hdr A ");
                SQL.AppendLine("	INNER JOIN TblSalesInvoice5Dtl2 B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("	INNER JOIN ( ");
                SQL.AppendLine("		select DocNo, AcNo, DAmt, CAmt, ");
                SQL.AppendLine("		Case ");
                SQL.AppendLine("			When DAmt > 0 then 'D' ");
                SQL.AppendLine("			ELSE 'C' ");
                SQL.AppendLine("		END ColType ");
                SQL.AppendLine("		FROM TblJournalDtl ");
                SQL.AppendLine("		WHERE DocNo = @JournalDocNo ");
                SQL.AppendLine("	)C ON B.AcNo = C.AcNo ");
                SQL.AppendLine("	WHERE A.DocNo = @DocNo ");
                SQL.AppendLine(")B ON A.AcNo = B.AcNo AND A.DocNo = @JournalDocNo ");
                SQL.AppendLine("SET A.DAmt = B.DAmtNew, A.CAmt = B.CAmtNew; ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, @EntCode As EntCode, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("        Select B.AcNo, ");
                if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                }
                SQL.AppendLine("        B.DAmt DAmt, ");
                if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                }
                SQL.AppendLine("        B.CAmt CAmt ");
                SQL.AppendLine("	FROM TblSalesInvoice5Hdr A ");
                SQL.AppendLine("	INNER JOIN TblSalesInvoice5Dtl2 B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("	WHERE A.DocNo = @DocNo ");
                SQL.AppendLine("	AND B.AcNo NOT IN ( ");
                SQL.AppendLine("		SELECT AcNo ");
                SQL.AppendLine("		FROM TblJournalDtl ");
                SQL.AppendLine("		WHERE DocNo = @JournalDocNo ");
                SQL.AppendLine("	) ");
                SQL.AppendLine(")T; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            if (mIsSLIPJournalUseCC)
                Sm.CmParam<String>(ref cm, "@CCCode", GetCCCode(DocNo));
            var testcc = GetCCCode(DocNo);
            return cm;
        }

        private MySqlCommand UpdateSalesInvoice5File(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoice5Dtl Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);
            var lDepositSummary = new List<DepositSummary>();

            if (ChkCancelInd.Checked &&
              decimal.Parse(TxtDownpayment.Text) != 0 &&
              !Sm.CompareStr(Sm.GetLue(LueCurCode), mMainCurCode)
              )
                GetDepositSummary2(ref lDepositSummary);

            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesInvoice5Hdr());

            if (ChkCancelInd.Checked &&
                decimal.Parse(TxtDownpayment.Text) != 0)
            {
                cml.Add(SaveCustomerDeposit(TxtDocNo.Text));
                if (lDepositSummary.Count > 0)
                {
                    for (int i = 0; i < lDepositSummary.Count; i++)
                        cml.Add(SaveCustomerDepositSummary2(lDepositSummary[i]));
                }
            }

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    cml.Add(UpdateProjectImplementationInvoicedInd(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3), "Y"));
                }
            }

            if (mIsAutoJournalActived) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private MySqlCommand SaveCustomerDepositSummary2(DepositSummary i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCustomerDepositSummary2 Set ");
            SQL.AppendLine("    Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where CtCode=@CtCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And ExcRate=@ExcRate; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", i.ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", i.UsedAmt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDocumentNotCancelled() ||
                IsDataAlreadyUsedOnARSettlement()||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToIncomingPayment() ||
                (ChkCancelInd.Checked && IsVoucherRequestPPNExisted()) ||
                (ChkCancelInd.Checked && IsVATSettlementExisted());
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist("Select DocNo From TblSalesInvoice5Hdr Where CancelInd='Y' And DocNo=@Param;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyUsedOnARSettlement()
        {
            if (Sm.IsDataExist("Select DocNo From TblARSHdr Where SalesinvoiceDocNo=@Param And CancelInd = 'N' Limit 1;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already used on AR settlement.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyProcessedToIncomingPayment()
        {
            if (Sm.IsDataExist("Select DocNo From TblSalesInvoice5Hdr Where ProcessInd<>'O' And DocNo=@Param Limit 1;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to incoming payment.");
                return true;
            }
            return false;
        }

        private bool IsVoucherRequestPPNExisted()
        {
            if (Sm.IsDataExist(
                "SELECT DocNo FROM TblSalesInvoice5Hdr " +
                "WHERE DocNo=@Param AND VoucherRequestPPNDocNo IS NOT NULL;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Voucher Request VAT.");
                return true;
            }
            return false;
        }

        private bool IsVATSettlementExisted()
        {
            if (Sm.IsDataExist(
                "Select DocNo From TblSalesInvoice5Hdr " +
                "Where DocNo=@Param And VATSettlementDocNo Is Not Null;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to VAT settlement.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditSalesInvoice5Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoice5Hdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason = @CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @JournalDocNo:=");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine(Sm.GetNewJournalDocNo(CurrentDt, 1));
            else
                SQL.AppendLine(Sm.GetNewJournalDocNo(DocDt, 1));
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalesInvoice5Hdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblSalesInvoice5Hdr Where DocNo=@DocNo And CancelInd='Y');");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblSalesInvoice5Hdr Where DocNo=@DocNo And CancelInd='Y');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSalesInvoice5Hdr(DocNo);
                ShowSalesInvoice5Dtl(DocNo);
                ShowSalesInvoice5Dtl2(DocNo);
                ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesInvoice5Hdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select * From TblSalesInvoice5Hdr Where DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "ReceiptNo", "DocDt", "CancelReason", "CancelInd", "CtCode", 
                    
                    //6-10
                    "DueDt", "LocalDocNo", "SalesName", "TotalAmt", "TotalTax", 
                    
                    //11-15
                    "DownPayment", "Amt", "BankAcCode", "Remark", "JournalDocNo", 
                    
                    //16-20
                    "JournalDocNo2", "CurCode", "CurTaxRateAmt", "TaxInvoiceNo", "TaxInvoiceNo2", 

                    //21-25
                    "TaxInvoiceNo3", "TaxInvoiceDt", "TaxInvoiceDt2", "TaxInvoiceDt3", "TaxCode1", 

                    //26-30
                    "TaxCode2", "TaxCode3", "TaxAmt1", "TaxAmt2", "TaxAmt3", 
                    
                    //31-35
                    "TaxAlias1", "TaxAlias2", "TaxAlias3", "TaxAmtM1", "TaxAmtM2", 
                    
                    //36
                    "TaxAmtM3", "ProjectImplementationDocNo"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtReceiptNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y") ? true : false;
                    SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));

                    SetLueProjectName(ref LueProjectName, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueProjectName, Sm.DrStr(dr, c[37]));
                    Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[6]));
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    SetLueSPCode(ref LueSPCode);
                    Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[8]));
                    TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    TxtTotalTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    TxtTotalWithTax.EditValue = Sm.FormatNum((Sm.DrDec(dr, c[9]) + Sm.DrDec(dr, c[10])), 0);
                    TxtDownpayment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[13]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[14]);
                    TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[15]);
                    TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[16]);
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[17]));
                    TxtCurTaxRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                    TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[19]);
                    TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[20]);
                    TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[21]);
                    Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[22]));
                    Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[23]));
                    Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[24]));
                    Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[25]));
                    Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[26]));
                    Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[27]));
                    TxtTaxAmt1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[28]), 0);
                    TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[29]), 0);
                    TxtTaxAmt3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[30]), 0);
                    TxtAlias1.EditValue = Sm.DrStr(dr, c[31]);
                    TxtAlias2.EditValue = Sm.DrStr(dr, c[32]);
                    TxtAlias3.EditValue = Sm.DrStr(dr, c[33]);
                    TxtTaxAmtM1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[34]), 0);
                    TxtTaxAmtM2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[35]), 0);
                    TxtTaxAmtM3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[36]), 0);
                    
                  //  LueProjectName.EditValue = Sm.DrStr(dr, c[37]);
                }, true
            );
        }

        private void ShowSalesInvoice5Dtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            decimal TaxRate1 = 0m, TaxRate2 = 0m, TaxRate3 = 0m;
            if (LueTaxCode1.Text.Length > 0 && LueTaxCode1.Text != "[Empty]") TaxRate1 = decimal.Parse(Sm.GetValue("Select TaxRate*0.01 From TblTax Where TaxCode=@Param;", Sm.GetLue(LueTaxCode1)));
            if (LueTaxCode2.Text.Length > 0 && LueTaxCode2.Text != "[Empty]") TaxRate2 = decimal.Parse(Sm.GetValue("Select TaxRate*0.01 From TblTax Where TaxCode=@Param;", Sm.GetLue(LueTaxCode2)));
            if (LueTaxCode3.Text.Length > 0 && LueTaxCode3.Text != "[Empty]") TaxRate3 = decimal.Parse(Sm.GetValue("Select TaxRate*0.01 From TblTax Where TaxCode=@Param;", Sm.GetLue(LueTaxCode3)));

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ProjectImplementationDocNo, A.ProjectImplementationDNo, C.StageName, D.TaskName, ");
            SQL.AppendLine("B.BobotPercentage, A.Amt, A.FileName, A.TaxInd1, A.TaxInd2, A.TaxInd3, A.Remark ");
            SQL.AppendLine("From TblSalesInvoice5Dtl A ");
            SQL.AppendLine("Inner Join "+TblProjectImplementationDtl+" B On A.ProjectImplementationDocNo = B.DocNo  ");
            SQL.AppendLine("    And A.ProjectImplementationDNo = B.DNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblProjectStage C On B.StageCode = C.StageCode ");
            SQL.AppendLine("Inner Join TblProjectTask D On B.TaskCode = D.TaskCode ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "ProjectImplementationDocNo", "ProjectImplementationDNo", "StageName", "TaskName", "BobotPercentage", 
                    
                    //6-10
                    "Amt", "FileName", "TaxInd1", "TaxInd2", "TaxInd3", 
                    
                    //11
                    "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 7);

                    Sm.SetGrdValue("B", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 15, 10);
                    Grd1.Cells[Row, 16].Value = Sm.GetGrdDec(Grd1, Row, 8) * TaxRate1;
                    Grd1.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 8) * TaxRate2;
                    Grd1.Cells[Row, 18].Value = Sm.GetGrdDec(Grd1, Row, 8) * TaxRate3;

                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 11);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 8 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSalesInvoice5Dtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, A.OptAcDesc, C.OptDesc, A.Remark ");
            SQL.AppendLine("From TblSalesInvoice5Dtl2 A "); 
            SQL.AppendLine("Inner Join TblCOA B ");
            SQL.AppendLine("Left Join TblOption C On A.OptAcDesc = C.OptCode And OptCat='AccountDescriptionOnSalesInvoice'  ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "AcInd", "DAmt", "CAmt", "OptAcDesc",
                    "OptDesc", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);

                    if (Sm.GetGrdBool(Grd3, Row, 10).ToString() == "True")
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        }
                        else
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 2);
                        }
                    }
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowCustomerDepositSummary(string CtCode)
        {
            ClearGrd2();

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select CurCode, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary ");
            SQL.AppendLine("Where CtCode=@CtCode Order By CurCode;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                }
            }
            while (bytesRead != 0);

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateSalesInvoice5File(DocNo, Row, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {
            if (mIsSalesInvoice5AllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsSalesInvoice5AllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsSalesInvoice5AllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsSalesInvoice5AllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsSalesInvoice5AllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private void ProcessDepositSummary(ref List<DepositSummary> l)
        {
            if (Sm.CompareStr(Sm.GetLue(LueCurCode), mMainCurCode)) return;

            decimal Downpayment = 0m;

            if (TxtDownpayment.Text.Length > 0)
                Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (Downpayment <= 0m) return;

            GetDepositSummary1(ref l);

            if (l.Count > 0m)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    if (Downpayment > 0m)
                    {
                        if (Downpayment >= l[i].Amt)
                        {
                            l[i].UsedAmt = l[i].Amt;
                            Downpayment -= l[i].Amt;
                        }
                        else
                        {
                            l[i].UsedAmt = Downpayment;
                            Downpayment = 0m;
                            break;
                        }
                    }
                    else
                        break;
                }

                for (int i = l.Count - 1; i >= 0; i--)
                    if (l[i].UsedAmt == 0m) l.RemoveAt(i);
            }
        }

        private void GetDepositSummary1(ref List<DepositSummary> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ExcRate, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary2 ");
            SQL.AppendLine("Where CtCode=@CtCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And Amt>0.00 ");
            SQL.AppendLine("Order By CreateDt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ExcRate", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DepositSummary()
                        {
                            ExcRate = Sm.DrDec(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1]),
                            UsedAmt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetDepositSummary2(ref List<DepositSummary> l)
        { 
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ExcRate, Amt ");
            SQL.AppendLine("From TblSalesInvoice5Dtl3 ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By CreateDt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ExcRate", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DepositSummary()
                        {
                            ExcRate = Sm.DrDec(dr, c[0]),
                            UsedAmt = Sm.DrDec(dr, c[1]),
                            Amt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        //private MySqlCommand SaveExcRate(ref List<Rate> lR, int i, string DocNo, decimal DP)
        //{
        //    var SQL1 = new StringBuilder();

        //    decimal x = ((DP - lR[i].Amt) >= 0) ? lR[i].Amt : DP;

        //    SQL1.AppendLine("Select @DP:=(Case @CancelInd When 'Y' Then -1 Else 1 End * " + x + "); ");

        //    SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
        //    SQL1.AppendLine("    Set Amt = Amt-@DP ");
        //    SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

        //    SQL1.AppendLine("Insert Into TblSalesInvoice5Dtl3(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
        //    SQL1.AppendLine("Select @DocNo, @CurCode, @ExcRate, @DP, @CreateBy, CurrentDateTime() ");
        //    SQL1.AppendLine("On Duplicate Key ");
        //    SQL1.AppendLine("   Update Amt=Amt-@DP, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

        //    var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
        //    Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
        //    Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
        //    Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
        //    Sm.CmParam<String>(ref cm1, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

        //    return cm1;
        //}

        //private MySqlCommand UpdateSummary2(ref List<Rate> lR, int i)
        //{
        //    var SQL1 = new StringBuilder();

        //    SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
        //    SQL1.AppendLine("    Set Amt = Amt + @Amt, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
        //    SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

        //    var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
        //    Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
        //    Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
        //    Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
        //    Sm.CmParam<Decimal>(ref cm1, "@Amt", lR[i].Amt);
        //    Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

        //    return cm1;
        //}

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsFormPrintOutInvoice = Sm.GetParameter("FormPrintOutInvoice");
            mIsCustomerItemNameMandatory = Sm.GetParameter("IsCustomerItemNameMandatory") == "Y";
            mIsRemarkForJournalMandatory = Sm.GetParameter("IsRemarkForJournalMandatory") == "Y";
            mEmpCodeSI = Sm.GetParameter("EmpCodeSI");
            mEmpCodeTaxCollector = Sm.GetParameter("EmpCodeTaxCollector");
            mIsTaxAliasMandatory = Sm.GetParameterBoo("IsTaxAliasMandatory");
            mIsStageShouldBeSettled = Sm.GetParameter("IsStageShouldBeSettled") == "Y";
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsSalesInvoice5AllowToUploadFile = Sm.GetParameterBoo("IsSalesInvoice5AllowToUploadFile");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mProjectAcNoFormula = Sm.GetParameter("ProjectAcNoFormula");
            mIsSLIForProjectTotalAmountBasedOnEstimatedPrice = Sm.GetParameterBoo("IsSLIForProjectTotalAmountBasedOnEstimatedPrice");
            mSLI5InvoiceAmtCalculationFormat = Sm.GetParameter("SLI5InvoiceAmtCalculationFormat");
            mSalesInvoice5JournalFormula = Sm.GetParameter("SalesInvoice5JournalFormula");
            mSLIPTaxFormat = Sm.GetParameter("SLIPTaxFormat");
            mIsSLIPJournalUseCC = Sm.GetParameterBoo("IsSLIPJournalUseCC");
            mIsWBS2SummarizedWBS1 = Sm.GetParameterBoo("IsWBS2SummarizedWBS1");
            mIsProjectNameSLIPMandatory = Sm.GetParameterBoo("IsProjectNameSLIPMandatory");

            if (mSLI5InvoiceAmtCalculationFormat.Length == 0) mSLI5InvoiceAmtCalculationFormat = "1";
            if (mSalesInvoice5JournalFormula.Length == 0) mSalesInvoice5JournalFormula = "1";
            if (mSLIPTaxFormat.Length == 0) mSLIPTaxFormat = "1";

            if (mIsWBS2SummarizedWBS1) TblProjectImplementationDtl += "5";
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union ALL Select 'All' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueProjectName(ref LookUpEdit Lue, string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select distinct A.DocNo As Col1, E.ProjectName as Col2");
            SQL.AppendLine("From " + TblProjectImplementationDtl + " A ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.DocNo = B.DocNo  ");
            if (BtnSave.Enabled)
            {
                SQL.AppendLine("And B.CancelInd = 'N' And B.Status = 'A' And A.SettledInd = 'Y' And A.InvoicedInd = 'N'  ");
                SQL.AppendLine("And Not Exists (Select 1 From " + TblProjectImplementationDtl + " T  ");
                SQL.AppendLine("Where T.SettledInd = 'N' and T.InvoicedInd = 'N' and T.StageCode=A.StageCode ");
                SQL.AppendLine("And T.DocNo = A.DocNo) ");
            }
            SQL.AppendLine("INner JOin TblSoCOntractRevisionHdr B1 On B.SOContractDocNo = B1.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B1.SOCDocNo=C.DocNo And C.CtCode=@CtCode  ");
            SQL.AppendLine("Inner Join TblBOQHdr D On C.BOQDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo=E.DocNo ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (E.SiteCode Is Null Or ( ");
                SQL.AppendLine("    E.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(E.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }
        private void LueRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.LookUpEdit Lue,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();

                if (CtCode.Length == 0)
                {
                    SQL.AppendLine("Select T1.CtCode As Col1, T2.CtName As Col2 ");
                    SQL.AppendLine("From ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select Distinct C.CtCode ");
                    SQL.AppendLine("    From TblProjectImplementationHdr A ");
                    SQL.AppendLine("    Inner Join " + TblProjectImplementationDtl + " B On A.DocNo = B.DocNo And A.CancelInd = 'N' And Status = 'A' And B.SettledInd = 'Y' ");
                    SQL.AppendLine("    Inner Join TblSOContractRevisionHdr B1 On A.SOCOntractDocNo = B1.DocNo ");
                    SQL.AppendLine("    Inner Join TblSOContractHdr C On B1.SOCDocNo = C.DocNo ");
                    SQL.AppendLine(") T1 ");
                    SQL.AppendLine("Inner Join TblCustomer T2 On T1.CtCode = T2.CtCode ");
                    SQL.AppendLine("Order By T2.CtName; ");
                }
                else
                    SQL.AppendLine("Select CtCode As Col1, CtName As Col2 From TblCustomer Where CtCode='" + CtCode + "'");
                
                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueOptionCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='AccountDescriptionOnSalesInvoice' ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeAmt()
        { 
            decimal Amt = 0m, TotalTax = 0m, Downpayment = 0m;
            
            //var CustomerAcNoAR = 
            //    Sm.GetValue(
            //            "Select (Select Concat(ParValue, A.CtCode) From TblParameter Where ParCode='CustomerAcNoAR' Limit 1) As AcNo " +
            //            "From TblCustomer A, TblCustomerCategory B " +
            //            "Where A.CtCtCode is Not Null " +
            //            "And A.CtCtCode=B.CtCtCode " +
            //            "And A.CtCode='"+Sm.GetLue(LueCtCode)+"' Limit 1; "
            //            );

            if(Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if(Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                    {
                        Amt += Sm.GetGrdDec(Grd1, i, 8);
                    }
                }
            }
            
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", Sm.GetGrdStr(Grd3, Row, 1));
                if (Sm.GetGrdBool(Grd3, Row, 3))
                {
                    if (AcType == "D")
                        Amt += Sm.GetGrdDec(Grd3, Row, 4);
                    else
                        Amt -= Sm.GetGrdDec(Grd3, Row, 4);
                }
                if (Sm.GetGrdBool(Grd3, Row, 5))
                {
                    if (AcType == "C")
                        Amt += Sm.GetGrdDec(Grd3, Row, 6);
                    else
                        Amt -= Sm.GetGrdDec(Grd3, Row, 6);
                }
            }

            if (TxtDownpayment.Text.Length > 0) Downpayment = decimal.Parse(TxtDownpayment.Text);

            ComputeTax(Amt, Downpayment);

            TotalTax = Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text);

            TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
            TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
            TxtTotalWithTax.EditValue = Sm.FormatNum((TotalTax + Amt), 0);
            TxtAmt.EditValue = Sm.FormatNum((Amt + TotalTax) - Downpayment, 0);
        }

        private void ComputeTax(decimal Amt, decimal Downpayment)
        {
            decimal TaxAmt1 = 0m, TaxAmt2 = 0m, TaxAmt3 = 0m, CurTaxRateAmt = 0m;

            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { 
                TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, 
                TxtTaxAmtM1, TxtTaxAmtM2, TxtTaxAmtM3
            }, 0);

            if (TxtCurTaxRateAmt.Text.Length != 0) CurTaxRateAmt = decimal.Parse(TxtCurTaxRateAmt.Text);

            if (mSLIPTaxFormat == "1")
            {
                if (Sm.GetLue(LueTaxCode1).Length != 0)
                {
                    if (mSLI5InvoiceAmtCalculationFormat == "1")
                        TaxAmt1 = (GetTaxRate(Sm.GetLue(LueTaxCode1)) / 100 * Amt);
                    else
                        TaxAmt1 = (GetTaxRate(Sm.GetLue(LueTaxCode1)) / 100 * (Amt - Downpayment));
                    TxtTaxAmt1.Text = Sm.FormatNum(TaxAmt1, 0);
                    TxtTaxAmtM1.Text = Sm.FormatNum(TaxAmt1 * CurTaxRateAmt, 0);
                }

                if (Sm.GetLue(LueTaxCode2).Length != 0)
                {
                    if (mSLI5InvoiceAmtCalculationFormat == "1")
                        TaxAmt2 = (GetTaxRate(Sm.GetLue(LueTaxCode2)) / 100 * Amt);
                    else
                        TaxAmt2 = (GetTaxRate(Sm.GetLue(LueTaxCode2)) / 100 * (Amt - Downpayment));
                    TxtTaxAmt2.Text = Sm.FormatNum(TaxAmt2, 0);
                    TxtTaxAmtM2.Text = Sm.FormatNum(TaxAmt2 * CurTaxRateAmt, 0);
                }

                if (Sm.GetLue(LueTaxCode3).Length != 0)
                {
                    if (mSLI5InvoiceAmtCalculationFormat == "1")
                        TaxAmt3 = (GetTaxRate(Sm.GetLue(LueTaxCode3)) / 100 * Amt);
                    else
                        TaxAmt3 = (GetTaxRate(Sm.GetLue(LueTaxCode3)) / 100 * (Amt - Downpayment));
                    TxtTaxAmt3.Text = Sm.FormatNum(TaxAmt3, 0);
                    TxtTaxAmtM3.Text = Sm.FormatNum(TaxAmt3 * CurTaxRateAmt, 0);
                }
            }
            else if (mSLIPTaxFormat == "2")
            {
                decimal TaxRate1 = 0m, TaxRate2 = 0m, TaxRate3 = 0m;

                if (LueTaxCode1.Text.Length > 0 && LueTaxCode1.Text != "[Empty]") TaxRate1 = decimal.Parse(Sm.GetValue("Select TaxRate*0.01 From TblTax Where TaxCode=@Param;", Sm.GetLue(LueTaxCode1)));
                if (LueTaxCode2.Text.Length > 0 && LueTaxCode2.Text != "[Empty]") TaxRate2 = decimal.Parse(Sm.GetValue("Select TaxRate*0.01 From TblTax Where TaxCode=@Param;", Sm.GetLue(LueTaxCode2)));
                if (LueTaxCode3.Text.Length > 0 && LueTaxCode3.Text != "[Empty]") TaxRate3 = decimal.Parse(Sm.GetValue("Select TaxRate*0.01 From TblTax Where TaxCode=@Param;", Sm.GetLue(LueTaxCode3)));

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 13) == true)
                        Grd1.Cells[Row, 16].Value = Sm.GetGrdDec(Grd1, Row, 8) * TaxRate1;
                    else
                        Grd1.Cells[Row, 16].Value = 0m;

                    if (Sm.GetGrdBool(Grd1, Row, 14) == true)
                        Grd1.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 8) * TaxRate2;
                    else
                        Grd1.Cells[Row, 17].Value = 0m;

                    if (Sm.GetGrdBool(Grd1, Row, 15) == true)
                        Grd1.Cells[Row, 18].Value = Sm.GetGrdDec(Grd1, Row, 8) * TaxRate3;
                    else
                        Grd1.Cells[Row, 18].Value = 0m;

                    TaxAmt1 += Sm.GetGrdDec(Grd1, Row, 16);
                    TaxAmt2 += Sm.GetGrdDec(Grd1, Row, 17);
                    TaxAmt3 += Sm.GetGrdDec(Grd1, Row, 18);
                }

                TxtTaxAmt1.Text = Sm.FormatNum(TaxAmt1, 0);
                TxtTaxAmtM1.Text = Sm.FormatNum(TaxAmt1 * CurTaxRateAmt, 0);

                TxtTaxAmt2.Text = Sm.FormatNum(TaxAmt2, 0);
                TxtTaxAmtM2.Text = Sm.FormatNum(TaxAmt2 * CurTaxRateAmt, 0);

                TxtTaxAmt3.Text = Sm.FormatNum(TaxAmt3, 0);
                TxtTaxAmtM3.Text = Sm.FormatNum(TaxAmt3 * CurTaxRateAmt, 0);
            }

            
        }

        private decimal GetTaxRate(string TaxCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select TaxRate from TblTax Where TaxCode=@TaxCode"
            };
            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);
            string TaxRate = Sm.GetValue(cm);

            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedProjectImplementation()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0 && Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##" + Sm.GetGrdStr(Grd1, Row, 3) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##XXX##" : SQL);
        }

        private string GetCCCode(string DocNo)
        {

            var mSIPRDocNo = DocNo;
            var mPRJIDocNo = Sm.GetGrdStr(Grd1, 0, 2);
            if (mSIPRDocNo.Length == 0) return string.Empty;
            return
                Sm.GetValue(
                    "SELECT G.CCCode FROM tblsalesinvoice5hdr A  " +
                    "Left JOIN tblsalesinvoice5dtl B ON A.DocNo = B.DocNo " +
                    "Left JOIN tblprojectimplementationHDR C ON B.ProjectImplementationDocNo = C.DocNo  " +
                    "Left JOIN tblsocontractrevisionhdr D ON C.SOContractDocNo = D.DocNo  " +
                    "Left JOIN tblsocontracthdr E ON D.SOCDocNo = E.DocNo  " +
                    "Left JOIN tblboqhdr F ON E.BOQDocNo = F.DocNo  " +
                    "Left JOIN tbllophdr G ON F.LOPDocNo = G.DocNo  " +
                    "WHERE C.DocNo = @Param  " +
                    "GROUP BY A.DocNo AND G.CCCode  ", mPRJIDocNo
                    );
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };



        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            var l = new List<Invoice5Hdr>();
            var l2 = new List<Invoice5Hdr2>();
            var ldtl = new List<Invoice5Dtl>();
            var lpayment = new List<Invoice5Payment>();
            var ls = new List<Invoice5Summary>();

            string[] TableName = { "Invoice5Hdr", "Invoice5Hdr2", "Invoice5Dtl", "Invoice5Summary" };

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, G.ProjectName, H.ParValue AS Consultant, E.LocalDocNo ContractLocalDocNo, Date_Format(E.DocDt, '%d %M %Y') ContractDt, E.Amt ContractAmt, ");
            SQL.AppendLine("J.OptDesc AS ProjectResource, DATE_FORMAT(A.DocDt, '%M %Y') InvoicePeriod, K.SiteName, UPPER(I.UserName) UserName, ");
            SQL.AppendLine("L.PosName ");
            SQL.AppendLine("FROM TblSalesInvoice5Hdr A ");
            SQL.AppendLine("INNER JOIN (SELECT DISTINCT ProjectImplementationDocNo, DocNo FROM TblSalesInvoice5Dtl WHERE DocNo = @DocNo) B ON A.DocNo = B.DocNo AND A.DocNo = @DocNo ");
            SQL.AppendLine("INNER JOIN TblProjectImplementationHdr C ON B.ProjectImplementationDocNo = C.DocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractRevisionHdr D ON C.SOContractDocNo = D.DocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractHdr E ON D.SOCDocNo = E.DocNo ");
            SQL.AppendLine("INNER JOIN TblBOQHdr F ON E.BOQDocNo = F.DocNo ");
            SQL.AppendLine("INNER JOIN TblLOPHdr G ON F.LOPDocNo = G.DocNo ");
            SQL.AppendLine("INNER JOIN TblParameter H ON H.ParCode = 'ReportTitle1' ");
            SQL.AppendLine("INNER JOIN TblUser I ON A.CreateBy = I.UserCode ");
            SQL.AppendLine("INNER JOIN TblOption J ON G.ProjectResource = J.OptCode AND J.OptCat = 'ProjectResource' ");
            SQL.AppendLine("INNER JOIN TblSite K ON G.SiteCode = K.SiteCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    SELECT T1.DocNo, T2.EmpName, T3.PosName ");
	        SQL.AppendLine("    FROM TblSalesInvoice5Hdr T1 ");
	        SQL.AppendLine("    INNER JOIN TblEmployee T2 ON T1.CreateBy = T2.UserCode ");
	        SQL.AppendLine("    LEFT JOIN TblPosition T3 ON T2.PosCode = T3.PosCode ");
	        SQL.AppendLine("    WHERE T1.DocNo = @DocNo ");
            SQL.AppendLine(") L ON A.DocNo = L.DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                        //0
                        "DocNo",

                        //1-5
                        "ProjectName",
                        "Consultant",
                        "ContractLocalDocNo",
                        "ContractDt",
                        "ContractAmt",

                        //6-10
                        "ProjectResource",
                        "InvoicePeriod",
                        "SiteName",
                        "UserName",
                        "PosName",
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Invoice5Hdr()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),

                            ProjectName = Sm.DrStr(dr, c[1]),
                            Consultant = Sm.DrStr(dr, c[2]),
                            ContractLocalDocNo = Sm.DrStr(dr, c[3]),
                            ContractDt = Sm.DrStr(dr, c[4]),
                            ContractAmt = Sm.DrDec(dr, c[5]),

                            ProjectResource = Sm.DrStr(dr, c[6]),
                            InvoicePeriod = Sm.DrStr(dr, c[7]),
                            SiteName = Sm.DrStr(dr, c[8]),
                            UserName = Sm.DrStr(dr, c[9]),
                            PosName = Sm.DrStr(dr, c[10]),

                            PaymentNo = "1",
                        });
                    }
                }
                dr.Close();
            }

            #region Calculate Payment No

            string PRJIDocNo = Sm.GetValue("Select Distinct ProjectImplementationDocNo From TblSalesInvoice5Dtl Where DocNo = @Param Limit 1; ", TxtDocNo.Text);
            var SQLp = new StringBuilder();
            var cmp = new MySqlCommand();

            SQLp.AppendLine("Select A.DocNo ");
            SQLp.AppendLine("From TblSalesInvoice5Hdr A ");
            SQLp.AppendLine("Inner Join (Select Distinct DocNo From TblSalesInvoice5Dtl Where ProjectImplementationDocNo = @Param) B On A.DocNo = B.DocNo ");
            SQLp.AppendLine("Where A.CancelInd = 'N' ");
            SQLp.AppendLine("Order By A.DocDt, A.DocNo; ");

            using (var cnp = new MySqlConnection(Gv.ConnectionString))
            {
                cnp.Open();
                cmp.Connection = cnp;
                cmp.CommandText = SQLp.ToString();
                Sm.CmParam<String>(ref cmp, "@Param", PRJIDocNo);
                var drp = cmp.ExecuteReader();
                var cp = Sm.GetOrdinal(drp, new string[] { "DocNo" });
                decimal mNo = 1;
                if (drp.HasRows)
                {
                    while (drp.Read())
                    {
                        lpayment.Add(new Invoice5Payment()
                        {
                            DocNo = Sm.DrStr(drp, cp[0]),
                            PaymentNo = mNo
                        });
                        mNo += 1;
                    }
                }
                drp.Close();
            }

            if (lpayment.Count > 0)
            {
                for (int i = 0; i < l.Count; ++i)
                {
                    for (int j = 0; j < lpayment.Count; ++j)
                    {
                        if (l[i].DocNo == lpayment[j].DocNo)
                        {
                            l[i].PaymentNo = string.Concat(lpayment[j].PaymentNo.ToString(), " (", Sm.Terbilang(lpayment[j].PaymentNo).ToUpper().Replace(" RUPIAH", ""), ")");
                            break;
                        }
                    }
                }
            }

            #endregion

            myLists.Add(l);
            #endregion

            #region Header
            var SQL2 = new StringBuilder();
            var cm2 = new MySqlCommand();

            SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',  ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',  ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull',  ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',  ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitleSLIP1') As 'Email',  ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitleSLIP2') As 'AddressTTD',  ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitleSLIP3') As 'PIC',  ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitleSLIP4') As 'PICPosition',  ");
            SQL2.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt, '%d %M %Y') As DocDt, B.CtName, A.Amt, A.Downpayment, A.TaxAmt1, A.TaxAmt2, A.TaxAmt3,  ");
            SQL2.AppendLine("(Select Z.TaxName From TblTax Z  ");
            SQL2.AppendLine("Inner Join TblSalesInvoice5Hdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
            SQL2.AppendLine("(Select Z.TaxName From TblTax Z  ");
            SQL2.AppendLine("Inner Join TblSalesInvoice5Hdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
            SQL2.AppendLine("(Select Z.TaxName From TblTax Z  ");
            SQL2.AppendLine("Inner Join TblSalesInvoice5Hdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxName3, ");
            SQL2.AppendLine("D.BankName, C.BankAcNo, C.BankAcNm, A.CurCode, B.Address ");
            SQL2.AppendLine("FROM TblSalesInvoice5Hdr A ");
            SQL2.AppendLine("INNER JOIN TblCustomer B ON A.CtCode = B.CtCode  ");
            SQL2.AppendLine("LEFT JOIN TblBankAccount C ON A.BankAcCode = C.BankAcCode ");
            SQL2.AppendLine("LEFT JOIN TblBank D ON C.BankCode = D.BankCode ");
            SQL2.AppendLine("WHERE A.DocNo = @DocNo ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[]
                    {
                        //0
                        "CompanyLogo",

                        //1-5
                        "CompanyName",
                        "CompanyAddress",
                        "CompanyAddressFull",
                        "CompanyPhone",
                        "Email",

                        //6-10
                        "AddressTTD",
                        "PIC",
                        "CtName",
                        "DocNo",
                        "DocDt",

                        //11-15
                        "Amt",
                        "Downpayment",
                        "TaxName1",
                        "TaxName2",
                        "TaxName3",

                        //16-20
                        "TaxAmt1",
                        "TaxAmt2",
                        "TaxAmt3",
                        "BankName",
                        "BankAcNo",

                        //21
                        "BankAcNm",
                        "CurCode",
                        "Address",
                        "PICPosition"

                    });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new Invoice5Hdr2()
                        {
                            CompanyLogo = Sm.DrStr(dr2, c2[0]),

                            CompanyName = Sm.DrStr(dr2, c2[1]),
                            CompanyAddress = Sm.DrStr(dr2, c2[2]),
                            CompanyAddressFull = Sm.DrStr(dr2, c2[3]),
                            CompanyPhone = Sm.DrStr(dr2, c2[4]),
                            Email = Sm.DrStr(dr2, c2[5]),

                            AddressTTD = Sm.DrStr(dr2, c2[6]),
                            PIC = Sm.DrStr(dr2, c2[7]),
                            CtName = Sm.DrStr(dr2, c2[8]),
                            DocNo = Sm.DrStr(dr2, c2[9]),
                            DocDt = Sm.DrStr(dr2, c2[10]),

                            Amt = Sm.DrDec(dr2, c2[11]),
                            Downpayment = Sm.DrDec(dr2, c2[12]),
                            TaxName = Sm.DrStr(dr2, c2[13]),
                            TaxName2 = Sm.DrStr(dr2, c2[14]),
                            TaxName3 = Sm.DrStr(dr2, c2[15]),

                            TaxAmt = Sm.DrDec(dr2, c2[16]),
                            TaxAmt2 = Sm.DrDec(dr2, c2[17]),
                            TaxAmt3 = Sm.DrDec(dr2, c2[18]),
                            BankName = Sm.DrStr(dr2, c2[19]),
                            BankAcNo = Sm.DrStr(dr2, c2[20]),

                            BankAcName = Sm.DrStr(dr2, c2[21]),
                            CurCode = Sm.DrStr(dr2, c2[22]),
                            Address = Sm.DrStr(dr2, c2[23]),
                            PICPosition = Sm.DrStr(dr2, c2[24]),
                            Terbilang = Sm.Terbilang(Decimal.Parse(TxtAmt.Text)),


                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr2.Close();
            }

            myLists.Add(l2);
            
            #endregion

            #region Detail
            var SQLDtl = new StringBuilder();
            var cmDtl = new MySqlCommand();

            SQLDtl.AppendLine("SELECT A.DocNo, A.DNo, C.TaskName, B.BobotPercentage, (B.BobotPercentage * 0.01 * F.Amt) ContractAmt, ");
            SQLDtl.AppendLine("IFNULL(G.OpeningBalanceAmt, 0.00) OpeningBalanceAmt, A.Amt, (IFNULL(G.OpeningBalanceAmt, 0.00) + A.Amt) OngoingAmt, ");
            SQLDtl.AppendLine("(((B.BobotPercentage * 0.01 * F.Amt2) + (B.BobotPercentage * 0.01 * F.AmtBOM)) - (IFNULL(G.OpeningBalanceAmt, 0.00) + A.Amt)) OutstandingAmt, A.Remark ");
            SQLDtl.AppendLine("FROM TblSalesInvoice5Dtl A ");
            SQLDtl.AppendLine("INNER JOIN "+ TblProjectImplementationDtl +" B ON A.ProjectImplementationDocNo = B.DocNo AND A.ProjectImplementationDNo = B.DNo ");
            SQLDtl.AppendLine("INNER JOIN TblProjectTask C ON B.TaskCode = C.TaskCode ");
            SQLDtl.AppendLine("INNER JOIN TblProjectImplementationHdr D ON B.DocNo = D.DocNo ");
            SQLDtl.AppendLine("INNER JOIN TblSOContractRevisionHdr E ON D.SOContractDocNo = E.DocNo ");
            SQLDtl.AppendLine("INNER JOIN TblSOContractHdr F ON E.SOCDocNo = F.DocNo ");
            SQLDtl.AppendLine("LEFT JOIN ");
            SQLDtl.AppendLine("( ");
	        SQLDtl.AppendLine("    SELECT T1.ProjectImplementationDocNo, T1.ProjectImplementationDNo, SUM(T1.Amt) OpeningBalanceAmt ");
	        SQLDtl.AppendLine("    FROM TblSalesInvoice5Dtl T1 ");
	        SQLDtl.AppendLine("    INNER JOIN "+ TblProjectImplementationDtl +" T2 ON T1.ProjectImplementationDocNo = T2.DocNo AND T1.ProjectImplementationDNo = T2.DNo ");
	        SQLDtl.AppendLine("        AND CONCAT(T1.ProjectImplementationDocNo, T1.ProjectImplementationDNo) IN ");
	        SQLDtl.AppendLine("        ( ");
	    	SQLDtl.AppendLine("            SELECT CONCAT(ProjectImplementationDocNo, ProjectImplementationDNo) ");
	    	SQLDtl.AppendLine("            FROM TblSalesInvoice5Dtl ");
	    	SQLDtl.AppendLine("            WHERE DocNo = @DocNo ");
	        SQLDtl.AppendLine("        ) ");
	        SQLDtl.AppendLine("    AND T1.DocNo != @DocNo ");
	        SQLDtl.AppendLine("    INNER JOIN TblSalesInvoice5Hdr T3 ON T1.DocNo = T3.DocNo AND T3.DOcDt < (SELECT DocDt FROM TblSalesInvoice5Hdr WHERE DocNo = @DocNo) ");
	        SQLDtl.AppendLine("    GROUP BY T1.ProjectImplementationDocNo, T1.ProjectImplementationDNo ");
            SQLDtl.AppendLine(") G ON A.ProjectImplementationDocNo = G.ProjectImplementationDocNo AND A.ProjectImplementationDNo = G.ProjectImplementationDNo ");
            SQLDtl.AppendLine("WHERE A.DocNo = @DocNo; ");

            using (var cnd = new MySqlConnection(Gv.ConnectionString))
            {
                cnd.Open();
                cmDtl.Connection = cnd;
                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                {
                    //0
                    "DocNo",

                    //1-5
                    "DNo",
                    "TaskName",
                    "BobotPercentage",
                    "ContractAmt",
                    "OpeningBalanceAmt",

                    //6-7
                    "Amt",
                    "OngoingAmt",
                    "OutstandingAmt",
                    "Remark"
                });
                if (drDtl.HasRows)
                {
                    decimal mNo = 1;
                    while (drDtl.Read())
                    {
                        ldtl.Add(new Invoice5Dtl()
                        {
                            No = mNo,
                            DocNo = Sm.DrStr(drDtl, cDtl[0]),

                            DNo = Sm.DrStr(drDtl, cDtl[1]),
                            TaskName = Sm.DrStr(drDtl, cDtl[2]),
                            BobotPercentage = Sm.DrDec(drDtl, cDtl[3]),
                            ContractAmt = Sm.DrDec(drDtl, cDtl[4]),
                            OpeningBalanceAmt = Sm.DrDec(drDtl, cDtl[5]),

                            Amt = Sm.DrDec(drDtl, cDtl[6]),
                            OngoingAmt = Sm.DrDec(drDtl, cDtl[7]),
                            OutstandingAmt = Sm.DrDec(drDtl, cDtl[8]),
                            Remark = Sm.DrStr(drDtl, cDtl[9]) 
                        });
                        mNo += 1;
                    }
                }
                drDtl.Close();
            }

            myLists.Add(ldtl);

            #endregion

            #region Summary
            var SQLSum = new StringBuilder();
            var cmSum = new MySqlCommand();


            if (ldtl.Count > 0)
            {

                string mDocNo = TxtDocNo.Text,
                       mTaxName1 = LueTaxCode1.Text,
                       mTaxName2 = LueTaxCode2.Text,
                       mTaxName3 = LueTaxCode3.Text;


                decimal mContractAmt = 0m, mOpeningBalanceAmt = 0m, mAmt = 0m, mOngoingAmt = 0m, mOutstandingAmt = 0m,
                    mContractAmt1 = 0m, mOpeningBalanceAmt1 = 0m, mAmt1 = 0m, mOngoingAmt1 = 0m, mOutstandingAmt1 = 0m,
                    mContractAmt2 = 0m, mOpeningBalanceAmt2 = 0m, mAmt2 = 0m, mOngoingAmt2 = 0m, mOutstandingAmt2 = 0m,
                    mBobotPercentage = 0m, mDownpayment = 0m, mTaxAmt1 = 0m, mTaxAmt2 = 0m, mTaxAmt3 = 0m;

                for (int i = 0; i < ldtl.Count; ++i)
                {
                    mContractAmt += ldtl[i].ContractAmt;
                    mOpeningBalanceAmt += ldtl[i].OpeningBalanceAmt;
                    mAmt += ldtl[i].Amt;
                    mOngoingAmt += ldtl[i].OngoingAmt;
                    mOutstandingAmt += ldtl[i].OutstandingAmt;
                    mBobotPercentage += ldtl[i].BobotPercentage;
                }

                mContractAmt1 = mContractAmt * 0.1m;
                mOpeningBalanceAmt1 = mOpeningBalanceAmt * 0.1m;
                mAmt1 = mAmt * 0.1m;
                mOngoingAmt1 = mOngoingAmt * 0.1m;
                mOutstandingAmt1 = mOutstandingAmt * 0.1m;

                mContractAmt2 = mContractAmt + mContractAmt1;
                mOpeningBalanceAmt2 = mOpeningBalanceAmt + mOpeningBalanceAmt1;
                mAmt2 = mAmt + mAmt1;
                mOngoingAmt2 = mOngoingAmt + mOngoingAmt1;
                mOutstandingAmt2 = mOutstandingAmt + mOutstandingAmt1;

                mDownpayment = decimal.Parse(TxtDownpayment.Text);
                mTaxAmt1 = decimal.Parse(TxtTaxAmt1.Text);
                mTaxAmt2 = decimal.Parse(TxtTaxAmt2.Text);
                mTaxAmt3 = decimal.Parse(TxtTaxAmt3.Text);

                ls.Add(new Invoice5Summary() 
                {
                    DocNo = mDocNo,
                    ContractAmt = mContractAmt,
                    OpeningBalanceAmt = mOpeningBalanceAmt,
                    Amt = mAmt,
                    OngoingAmt = mOngoingAmt,
                    OutstandingAmt = mOutstandingAmt,
                    BobotPercentage = mBobotPercentage,
                    
                    ContractAmt1 = mContractAmt1,
                    OpeningBalanceAmt1 = mOpeningBalanceAmt1,
                    Amt1 = mAmt1,
                    OngoingAmt1 = mOngoingAmt1,
                    OutstandingAmt1 = mOutstandingAmt1,

                    ContractAmt2 = mContractAmt2,
                    OpeningBalanceAmt2 = mOpeningBalanceAmt2,
                    Amt2 = mAmt2,
                    OngoingAmt2 = mOngoingAmt2,
                    OutstandingAmt2 = mOutstandingAmt2,

                    Terbilang = Sm.Terbilang(mAmt2).ToUpper(),
                    Downpayment = mDownpayment,
                    TaxName1 = mTaxName1,
                    TaxName2 = mTaxName2,
                    TaxName3 = mTaxName3,

                    TaxAmt1 = mTaxAmt1,
                    TaxAmt2 = mTaxAmt2,
                    TaxAmt3 = mTaxAmt3,
                });
            }

            myLists.Add(ls);

            #endregion

            if(Sm.GetParameter("DocTitle") == "MNET")
                Sm.PrintReport("Invoice5MNET", myLists, TableName, false);
            else
                Sm.PrintReport("Invoice5", myLists, TableName, false);

            l.Clear(); ldtl.Clear(); lpayment.Clear(); ls.Clear(); l2.Clear();
        }

        internal string GenerateReceipt(string DocDt)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblSalesInvoiceHdr ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoiceHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As ReceiptNo");

            return Sm.GetValue(SQL.ToString());
        }

        //internal string GenerateReceipt(string DocDt)
        //{
        //    string
        //        Yr = DocDt.Substring(2, 2),
        //        Mth = DocDt.Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle");
        //      //  DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

        //    var SQL = new StringBuilder();

        //    SQL.Append("Select Concat( ");
        //    SQL.Append("IfNull(( ");
        //    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
        //    SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoice5Hdr ");
        //    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
        //    SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
        //    SQL.Append("       ) As Temp ");
        //    SQL.Append("   ), '0001') ");
        //    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
        //    SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
        //    SQL.Append(") As ReceiptNo");

        //    return Sm.GetValue(SQL.ToString());
        //}

        private void ComputeFromTaxInd(iGAfterCommitEditEventArgs e)
        {
            decimal TaxRate1=0m, TaxRate2=0m, TaxRate3=0m;

            if(LueTaxCode1.Text.Length > 0 && LueTaxCode1.Text!="[Empty]") TaxRate1 = decimal.Parse(Sm.GetValue("Select TaxRate*0.01 From TblTax Where TaxCode=@Param;", Sm.GetLue(LueTaxCode1)));
            if (LueTaxCode2.Text.Length > 0 && LueTaxCode2.Text != "[Empty]") TaxRate2 = decimal.Parse(Sm.GetValue("Select TaxRate*0.01 From TblTax Where TaxCode=@Param;", Sm.GetLue(LueTaxCode2)));
            if (LueTaxCode3.Text.Length > 0 && LueTaxCode3.Text != "[Empty]") TaxRate3 = decimal.Parse(Sm.GetValue("Select TaxRate*0.01 From TblTax Where TaxCode=@Param;", Sm.GetLue(LueTaxCode3)));

            //Tax 1
            if (e.ColIndex == 13 && Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex) == true)
            {
                //compute tax 1 amount
                Grd1.Cells[e.RowIndex, 16].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 8) * TaxRate1;
            }
            else if (e.ColIndex == 13 && Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex) == false)
            {
                Grd1.Cells[e.RowIndex, 16].Value = 0m;
            }

            //Tax 2
            if (e.ColIndex == 14 && Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex) == true)
            {
                //compute tax 2 amount
                Grd1.Cells[e.RowIndex, 17].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 8) * TaxRate2;
            }
            else if (e.ColIndex == 14 && Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex) == false)
            {
                Grd1.Cells[e.RowIndex, 17].Value = 0m;
            }

            //Tax 3
            if (e.ColIndex == 15 && Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex) == true)
            {
                //compute tax 3 amount
                Grd1.Cells[e.RowIndex, 18].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 8) * TaxRate3;
            }
            else if (e.ColIndex == 15 && Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex) == false)
            {
                Grd1.Cells[e.RowIndex, 18].Value = 0m;
            }
        }

        private void ComputeFromTaxGrd()
        {
            decimal Tax1Grd = 0m, Tax2Grd = 0m, Tax3Grd = 0m, CurTaxRateAmt = 0m;
            if (TxtCurTaxRateAmt.Text.Length != 0) CurTaxRateAmt = decimal.Parse(TxtCurTaxRateAmt.Text);

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Tax1Grd += Sm.GetGrdDec(Grd1, Row, 16);
                Tax2Grd += Sm.GetGrdDec(Grd1, Row, 17);
                Tax3Grd += Sm.GetGrdDec(Grd1, Row, 18);
            }

            TxtTaxAmt1.EditValue = Sm.FormatNum(Tax1Grd,0);
            TxtTaxAmt2.EditValue = Sm.FormatNum(Tax2Grd,0);
            TxtTaxAmt3.EditValue = Sm.FormatNum(Tax3Grd,0);

            TxtTaxAmtM1.EditValue = Sm.FormatNum(Tax1Grd * CurTaxRateAmt, 0);
            TxtTaxAmtM2.EditValue = Sm.FormatNum(Tax2Grd * CurTaxRateAmt, 0);
            TxtTaxAmtM3.EditValue = Sm.FormatNum(Tax3Grd * CurTaxRateAmt, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            DteDueDt.EditValue = null;
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);
            if (BtnSave.Enabled)
            {
                DteDueDt.EditValue = null;
                ClearGrd();
                if (Sm.GetLue(LueCtCode).Length > 0)
                    ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));

                //Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                //    {
                //         LueProjectName
                //    });
                SetLueProjectName(ref LueProjectName, Sm.GetLue(LueCtCode));
            }
            else
            {
                SetLueProjectName(ref LueProjectName, Sm.GetLue(LueCtCode));
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
            if (BtnSave.Enabled)
            {
                if (mStateIndicator == "I")
                {
                    DteDueDt.EditValue = null;
                    ClearGrd();
                    ClearTabTax();
                    TxtCurTaxRateAmt.EditValue = Sm.FormatNum(1m, 0);
                }
                if (Sm.GetLue(LueCtCode).Length > 0)
                    ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));
            }
        }

        private void TxtDownpayment_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDownpayment, 0);
            if(BtnSave.Enabled && mStateIndicator == "I")
                ComputeAmt();
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue1(SetLueOptionCode));
        }

        private void LueOption_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value =
                    Grd3.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueOption);
                    Grd3.Cells[fCell.RowIndex, 8].Value = LueOption.GetColumnValue("Col2");
                }
                LueOption.Visible = false;
            }
        }


        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mStateIndicator == "I")
                    ComputeAmt();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mStateIndicator == "I")
                    ComputeAmt();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mStateIndicator == "I")
                    ComputeAmt();
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

       

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtCurTaxRateAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtCurTaxRateAmt, 0);
                if (mStateIndicator == "I")
                    ComputeAmt();
            }
        }

        private void LueProjectName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, true);
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProjectName, new Sm.RefreshLue2(SetLueProjectName), Sm.GetLue(LueCtCode));
            }
        }

        #endregion

        #region Button Event

        private void BtnDueDt_Click(object sender, EventArgs e)
        {
            //DteDueDt.EditValue = null;
            //var DocDt = Sm.GetDte(DteDocDt);
            //if (DocDt.Length > 0 && Grd1.Rows.Count > 1)
            //{
            //    decimal TOP = Sm.GetGrdDec(Grd1, 0, 28);
            //    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    {
            //        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
            //        {
            //            if (TOP > Sm.GetGrdDec(Grd1, Row, 28))
            //                TOP = Sm.GetGrdDec(Grd1, Row, 28);
            //        }
            //    }
                
            //    DteDueDt.EditValue = new DateTime(
            //       Int32.Parse(DocDt.Substring(0, 4)),
            //       Int32.Parse(DocDt.Substring(4, 2)),
            //       Int32.Parse(DocDt.Substring(6, 2)),
            //       0, 0, 0).AddDays((double)TOP);
            
            //}

        }

        private void BtnLocalDocNo_Click(object sender, EventArgs e)
        {
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd1, Row, 29).Length > 0)
            //    {
            //        TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 29);
            //        break;
            //    }
            //}
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid

        #region Grd1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProjectImplementation(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer") && !Sm.IsLueEmpty(LueCurCode, "Currency"))
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmSalesInvoice5Dlg(this, Sm.GetLue(LueCtCode), Sm.GetLue(LueProjectName)));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (mStateIndicator == "I") ComputeAmt();
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1 && !Sm.IsLueEmpty(LueCtCode, "Customer") && !Sm.IsLueEmpty(LueCurCode, "Currency") && 
                !(mIsProjectNameSLIPMandatory && Sm.IsLueEmpty(LueProjectName, "Project Name")))
            {
                // if (mIsProjectNameSLIPMandatory) !Sm.IsLueEmpty(LueProjectName, "Project Name");
                Sm.FormShowDialog(new FrmSalesInvoice5Dlg(this, Sm.GetLue(LueCtCode), Sm.GetLue(LueProjectName)));
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmProjectImplementation(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            //download file
            if (e.ColIndex == 11)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 9), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 9, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            //upload file
            if (BtnSave.Enabled && e.ColIndex == 10)
            {
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();
                Grd1.Cells[e.RowIndex, 9].Value = OD.FileName;
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if(mSLIPTaxFormat=="2")
            {
                if (e.ColIndex == 13 || e.ColIndex == 14 || e.ColIndex == 15)
                {
                    ComputeFromTaxInd(e);
                    ComputeFromTaxGrd();
                    ComputeAmt();
                }
            }
           
        }

        #endregion

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdDec(Grd3, e.RowIndex, 4)>0)
            {
                Grd3.Cells[e.RowIndex, 6].Value = 0;
            }

            if (e.ColIndex == 6 && Sm.GetGrdDec(Grd3, e.RowIndex, 6)>0)
            {
                Grd3.Cells[e.RowIndex, 4].Value = 0;
            }

            if (e.ColIndex == 3 || e.ColIndex == 5)
                Grd3.Cells[e.RowIndex, 10].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6 }, e.ColIndex))
                if (mStateIndicator == "I") 
                    ComputeAmt();
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesInvoice5Dlg2(this));
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LueOption, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
                SetLueOptionCode(ref LueOption);
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmSalesInvoice5Dlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            if (mStateIndicator == "I") ComputeAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #endregion

        #region Class

        #region Report Class

        private class Invoice5Hdr
        {
            public string DocNo { get; set; }
            public string ProjectName { get; set; }
            public string Consultant { get; set; }
            public string ContractLocalDocNo { get; set; }
            public string ContractDt { get; set; }
            public decimal ContractAmt { get; set; }
            public string ProjectResource { get; set; }
            public string InvoicePeriod { get; set; }
            public string SiteName { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string PaymentNo { get; set; }
        }

        private class Invoice5Hdr2
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string Terbilang { get; set; }
            public string Email { get; set; }
            public string BankName { get; set; }
            public string BankAcNo { get; set; }
            public string BankAcName { get; set; }
            public decimal Downpayment { get; set; }
            public string TaxName { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal TaxAmt2 { get; set; }
            public decimal TaxAmt3 { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public string AddressTTD { get; set; }
            public string PIC { get; set; }
            public string Address { get; set; }
            public string PICPosition { get; set; }
            public string PrintBy { get; set; }
        }

            private class Invoice5Dtl
        {
            public decimal No { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string TaskName { get; set; }
            public decimal BobotPercentage { get; set; }
            public decimal ContractAmt { get; set; }
            public decimal OpeningBalanceAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal OngoingAmt { get; set; }
            public decimal OutstandingAmt { get; set; }
            public string Remark { get; set; }
        }

        private class Invoice5Payment
        {
            public string DocNo { get; set; }
            public decimal PaymentNo { get; set; }
        }

        private class Invoice5Summary
        {
            public string DocNo { get; set; }
            public decimal ContractAmt { get; set; }
            public decimal OpeningBalanceAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal OngoingAmt { get; set; }
            public decimal OutstandingAmt { get; set; }
            public decimal BobotPercentage { get; set; }
            public decimal ContractAmt1 { get; set; }
            public decimal OpeningBalanceAmt1 { get; set; }
            public decimal Amt1 { get; set; }
            public decimal OngoingAmt1 { get; set; }
            public decimal OutstandingAmt1 { get; set; }
            public decimal ContractAmt2 { get; set; }
            public decimal OpeningBalanceAmt2 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal OngoingAmt2 { get; set; }
            public decimal OutstandingAmt2 { get; set; }
            public string Terbilang { get; set; }
            public decimal Downpayment { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxAmt1 { get; set; }
            public decimal TaxAmt2 { get; set; }
            public decimal TaxAmt3 { get; set; }
        }

        #endregion       

        private class DepositSummary
        {
            public decimal ExcRate { get; set; }
            public decimal Amt { get; set; }
            public decimal UsedAmt { get; set; }
        }

        private class EmployeeTaxCollector
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Mobile { get; set; }
        }

        //class Rate
        //{
        //    public decimal Downpayment { get; set; }
        //    public decimal Amt { get; set; }
        //    public string CurCode { get; set; }
        //    public decimal ExcRate { get; set; }
        //    public string CtCode { get; set; }
        //}

        #endregion
    }
}
