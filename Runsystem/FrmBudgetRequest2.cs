﻿#region Update
/* 
    07/08/2019 [DITA] Tambah Approval Budget Request
    24/10/2019 [WED/YK] validasi deactivate dokumen
    30/01/2020 [HAR/VIR] Budget Request tidak bisa edit 
    13/03/2020 [HAR/TWC] actual sama usage dimunculin waktu show
    04/05/2020 [DITA/VIR] tambah informasi Budget Category Code internal 
    05/05/2020 [WED/IMS] usage juga hitung dari VR Budget
    11/05/2020 [WED/SIER] budget tahunan berdasarkan parameter IsBudget2YearlyFormat
    27/05/2020 [WED/IMS] usage juga hitung dari Travel Request
    01/07/2020 [IBL/IMS] budget bisa menyimpan tipe cash advance di voucher request param --> VoucherDocTypeBudget
    01/07/2020 [IBL/IMS] menghubungkan budget dengan cash advance settlement
    07/10/2020 [IBL/PHT] menambah field Entity (header), kolom item dan quantity (detail) Based on param -> IsBudgetRequestUseItemAndQty
    20/01/2021 [WED/SIER] tambah ambil dari Cash Advance Settlement berdasarkan parameter IsCASUsedForBudget
    29/01/2021 [TKG/SIER] tambah copy previous year
    07/05/2021 [BRI/SIER] doc approval tidak melihat start amt berdasarkan parameter IsBudgetRequestApprovalNoNeedStartAmt
    18/04/2022 [TRI/TWC] bug, kolom usage belum mengambil dari MR Routine
    20/09/2022 [ICA/PRODUCT] menambahkan param IsBudgetUseMRProcessInd, agar yg ditarik hanya data dari MR yg sudah final
    11/10/2022 [VIN/SIER] BUG Used budget CAS berdasarkan bc code 
    22/11/2022 [VIN/SIER] BUG : tambah param IsCASUseBudgetCategory
    17/02/2023 [IBL/MNET] Auto create Budget Maintenance saat save Budget Request berdasarkan param IsBudgetRequest2AutoCreateBudgetMaintenance
    22/02/2023 [BRI/HEX] Usage melihat nilai PO berdasarkan param MRAvailableBudgetSubtraction
    22/02/2023 [BRI/HEX] range budget berdasarkan param FiscalYearRange
    10/04/2023 [WED/HEX] tambah fitur import CSV
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetRequest2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmBudgetRequest2Find FrmFind;
        internal bool mIsMRShowEstimatedPrice = true;
        internal bool
            mIsBudgetCalculateFromEstimatedPrice = true,
            mIsBudget2YearlyFormat = false,
            mIsBudgetRequestApprovalNoNeedStartAmt = false,
            mIsBudgetUseMRProcessInd = false,
            mIsCASUseBudgetCategory = false,
            mIsBudgetRequest2AutoCreateBudgetMaintenance = false,
            mIsBudgetMaintenanceYearlyActived = false
            ;
        private string mReqTypeForNonBudget = string.Empty,
            mMRAvailableBudgetSubtraction = string.Empty,
            mFiscalYearRange = string.Empty
            ;
        internal bool mIsBudgetRequestUseItemAndQty = false;
        private bool mIsCASUsedForBudget = false;
        string[] mFiscalYear;

        #endregion

        #region Constructor

        public FrmBudgetRequest2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Budget Request";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();

                LueEntCode.Visible = mIsBudgetRequestUseItemAndQty;
                label7.Visible = mIsBudgetRequestUseItemAndQty;

                if (mIsBudget2YearlyFormat)
                {
                    LblMth.ForeColor = Color.Black;
                    BtnCopyPrevMth.Text = "Copy Prev. Year";
                    BtnCopyPrevMth.ToolTip = "Copy from previous year";
                }
                SetFormControl(mState.View);
                Sl.SetLueYr(LueYr, string.Empty);
                if (!mIsBudget2YearlyFormat) Sl.SetLueMth(LueMth);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "Budget"+Environment.NewLine+"Category",
                        "Budget"+Environment.NewLine+"Category",
                        "Previous"+Environment.NewLine+"Requested"+Environment.NewLine+"Amount",
                        "Requested"+Environment.NewLine+"Amount",
                        "Actual"+Environment.NewLine+"Budget",

                        //6-10
                        "Usage",
                        "Budget Category" + Environment.NewLine + "Code Internal",
                        "Remark",
                        "",
                        "Item's Code",

                        //11-13
                        "Item's Name",
                        "",
                        "Quantity"
                    },
                     new int[]
                    {
                        //0
                        0,
 
                        //1-5
                        0, 200, 130, 130, 130,
                        
                        //6-10
                        130, 130, 300, 20, 100,

                        //11-13
                        150, 20, 120
                    }
                );

            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 13 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 9, 12 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 10, 11 });
            Grd1.Cols[8].Move(13);

            if (mIsBudgetRequestUseItemAndQty)
                Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 3, 5, 6, 10 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 3, 5, 6, 9, 10, 11, 12, 13 }, false);

        }

        override protected void HideInfoInGrd()
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6 }, !ChkHideInfoInGrd.Checked);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt,
                        MeeCancelReason, ChkCancelInd, LueYr, LueMth, LueDeptCode,
                        MeeRemark, LueEntCode
                    }, true);
                    BtnImport.Enabled =
                    BtnRefreshData.Enabled = 
                    BtnCopyPrevMth.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 4, 8, 9, 12, 13 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6 }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueYr, LueMth, LueDeptCode, MeeRemark, LueEntCode }, false);
                    
                    BtnImport.Enabled =
                    BtnRefreshData.Enabled = 
                    BtnCopyPrevMth.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, 8, 9, 12, 13 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6 }, !ChkHideInfoInGrd.Checked);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        MeeCancelReason, ChkCancelInd
                    }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo,
                DteDocDt, MeeCancelReason, LueYr, LueMth, LueDeptCode, TxtStatus,
                TxtBudgetDocNo, MeeRemark, LueEntCode
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6, 13 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBudgetRequest2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueEntCode(ref LueEntCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "")) return;
            SetFormControl(mState.Edit);
        }


        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!(BtnSave.Enabled && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0 && Sm.IsGrdColSelected(new int[] { 4, 8, 9, 12, 13 }, e.ColIndex)))
            {
                e.DoDefault = false;
                //Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 4, 13 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 8 }, e);
            if (e.ColIndex == 4) ComputeTotalAmount();
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BudgetRequest", "TblBudgetRequestHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBudgetRequestHdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveBudgetRequestDtl(DocNo, r));

            if (mIsBudgetRequest2AutoCreateBudgetMaintenance)
            {
                string BudgetDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Budget", "TblBudgetHdr");
                cml.Add(SaveBudgetMaintenance(DocNo, BudgetDocNo));
            }

            Sm.ExecCommands(cml);

            DeactivateOtherBudgetRequest(DocNo);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                (!mIsBudget2YearlyFormat && Sm.IsLueEmpty(LueMth, "Month")) ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mIsBudgetRequestUseItemAndQty && Sm.IsLueEmpty(LueEntCode, "Entity")) ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsDocExists() ||
                IsApprovalOngoing();
        }

        private bool IsApprovalOngoing()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblBudgetRequestHdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And Status = 'O' ");
            SQL.AppendLine("And DeptCode = @Param1 ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("And Mth = @Param2 ");
            SQL.AppendLine("And Yr = @Param3 ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueMth), Sm.GetLue(LueYr)))
            {
                Sm.StdMsg(mMsgType.Warning, "You can't create this request. Another request is still on approval process. Make sure this document#" + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueMth), Sm.GetLue(LueYr)) + " is approved by all level, or cancelled via document approval.");
                return true;
            }

            return false;
        }

        private bool IsDocExists()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblBudgetRequestHdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And Status = 'A' ");
            SQL.AppendLine("And DeptCode = @Param1 ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("And Mth = @Param2 ");
            SQL.AppendLine("And Yr = @Param3 ");
            SQL.AppendLine("And BudgetDocNo Is Not NULL ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueMth), Sm.GetLue(LueYr)))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already requested on " + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueMth), Sm.GetLue(LueYr)) + " and has been processed to Budget Maintenance.");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 budget category.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            //ReComputeStock();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Budget category is empty.")) return true;
                Msg = "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine + Environment.NewLine;
            }

            if (mIsBudgetRequestUseItemAndQty)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 10, false, "Item is empty.")) return true;
                    Msg = "Item : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine + Environment.NewLine;
                }

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 13, true, "Quantity should not be 0,00.")) return true;
                    Msg = "Quantity : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;
                }
            }

            return false;
        }

        private MySqlCommand SaveBudgetRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Yr, Mth, DeptCode, Amt, BudgetDocNo, Remark, Status, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', @Yr, @Mth, @DeptCode, @Amt, Null, @Remark, 'O', @EntCode, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='BudgetRequest2' ");
            SQL.AppendLine("And T.DeptCode Is Not Null And T.DeptCode=@DeptCode ");
            if (!mIsBudgetRequestApprovalNoNeedStartAmt)
            {
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.Amt*1 ");
                SQL.AppendLine("    From TblBudgetRequestHdr A ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)) ");
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblBudgetRequestHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'BudgetRequest2' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", (mIsBudget2YearlyFormat) ? "00" : Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBudgetRequestDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBudgetRequestDtl(DocNo, DNo, BCCode, Amt, ItCode, Qty, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @BCCode, @Amt, @ItCode, @Qty, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        internal MySqlCommand SaveBudgetMaintenance(string DocNo, string BudgetDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetHdr(DocNo, DocDt, Status, BudgetRequestDocNo, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @BudgetDocNo, DocDt, 'A', DocNo, IfNull(Amt, 0.00), Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblBudgetRequestHdr ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Status = 'A'; ");

            SQL.AppendLine("Update TblBudgetRequestHdr Set BudgetDocNo = @BudgetDocNo ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Status = 'A'; ");

            SQL.AppendLine("Set @row:=0;");
            SQL.AppendLine("Insert Into TblBudgetDtl(DocNo, DNo, BudgetRequestDocNo, BudgetRequestDNo, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @BudgetDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3), A.DocNo, B.DNo, B.Amt, B.Remark, B.CreateBy, B.CreateDt ");
            SQL.AppendLine("From TblBudgetRequestHdr A ");
            SQL.AppendLine("Inner Join TblBudgetRequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.Status = 'A'; ");

            if (!mIsBudgetMaintenanceYearlyActived)
            {
                SQL.AppendLine("Insert Into TblBudgetSummary(Yr, Mth, DeptCode, BCCode, Amt1, Amt2, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.Yr, " + (mIsBudget2YearlyFormat ? "00" : "A.Mth") + ", A.DeptCode, B.BCCode, IfNull(B.Amt, 0.00), IfNull(B.Amt, 0.00), A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblBudgetRequestHdr A ");
                SQL.AppendLine("Inner Join TblBudgetRequestDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("Where A.DocNo = @DocNo ");
                SQL.AppendLine("And A.Status = 'A' ");
                SQL.AppendLine("On Duplicate Key  ");
                SQL.AppendLine("    Update Amt1=IfNull(B.Amt, 0.00), Amt2=IfNull(B.Amt, 0.00), LastUpBy=A.CreateBy, LastUpDt=A.CreateDt; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@BudgetDocNo", BudgetDocNo);

            return cm;
        }

        private void DeactivateOtherBudgetRequest(string DocNo)
        {
            if (Sm.IsDataExist("Select 1 From TblBudgetRequestHdr Where DocNo = @Param And Status ='A' Limit 1;", DocNo))
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Update TblBudgetRequestHdr Set CancelInd='Y', CancelReason='Auto cancelled by system.' ");
                SQL.AppendLine("Where CancelInd='N' ");
                SQL.AppendLine("And Yr=@Yr ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("And Mth=@Mth ");
                SQL.AppendLine("And DeptCode=@DeptCode ");
                SQL.AppendLine("And DocNo <> @DocNo ");
                SQL.AppendLine("And BudgetDocNo Is Null; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

                Sm.ExecCommand(cm);
            }
        }

        private bool IsNeedApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblDocApprovalSetting T Where T.DocType='BudgetRequest2' ");
            SQL.AppendLine("And T.DeptCode Is Not Null And T.DeptCode=@Param1 ");
            if (!mIsBudgetRequestApprovalNoNeedStartAmt)
            {
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(@Param2, 0.00)) ");
            }
            SQL.AppendLine("; ");

            return Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueDeptCode), TxtAmt.Text);
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditBudgetRequestHdr());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblBudgetRequestHdr Where DocNo=@Param And CancelInd='Y';",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private bool IsDataProcessedAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblBudgetRequestHdr Where DocNo=@Param And BudgetDocNo Is Not Null;",
                TxtDocNo.Text,
                "This document already processed to budget.");
        }


        private MySqlCommand EditBudgetRequestHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBudgetRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBudgetRequestHdr(DocNo);
                ShowBudgetRequestDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBudgetRequestHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As Status, " +
                    "DocDt, CancelReason, CancelInd, Yr, Mth, DeptCode, Amt, BudgetDocNo, Remark, EntCode " +
                    "From TblBudgetRequestHdr Where DocNo=@DocNo;",
                    new string[]
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "Yr", "Mth", 
                        //6-10
                        "DeptCode", "Amt", "BudgetDocNo", "Remark", "Status",
                        //11
                        "EntCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueMth, Sm.DrStr(dr, c[5]));
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[6]));
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        TxtBudgetDocNo.EditValue = Sm.DrStr(dr, c[8]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[10]);
                        Sl.SetLueEntCode(ref LueEntCode, Sm.DrStr(dr, c[11]));
                    }, true
                );
        }

        private void ShowBudgetRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));


            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.BCCode, B.BCName, A.Amt, D.Amt2, (IfNull(E.Amt3, 0) + IfNull(F.Amt, 0) + IfNull(G.Amt, 0) + IfNull(H.Amt, 0) ");
            if (mIsCASUsedForBudget)
                SQL.AppendLine("+ IfNull(J.Amt, 0.00) ");
            SQL.AppendLine(") Amt3, A.Remark, B.LocalCode, ");
            SQL.AppendLine("I.ItCode, I.ItName, A.Qty ");
            SQL.AppendLine("From TblBudgetRequestDtl A ");
            SQL.AppendLine("Inner Join TblBudgetCategory B On A.BCCode=B.BCCode ");
            SQL.AppendLine("Inner Join TblBudgetRequesthdr C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Left Join TblBudgetSummary D On C.Yr = D.Yr And C.Mth = D.Mth");
            SQL.AppendLine("    And D.DeptCode = C.DeptCode And D.BCCode = B.BCCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.BCCode, A.DeptCode, Left(A.DocDt, 4) As Yr, ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("    '00' As Mth, ");
            else
                SQL.AppendLine("    Substring(A.DocDt, 5, 2) As Mth, ");
            if (mMRAvailableBudgetSubtraction == "1")
            {
                if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                    SQL.AppendLine("    Sum(B.Qty * Case When A.EximInd = 'Y' Then B.UPrice Else B.EstPrice End) As Amt3 ");
                else
                    SQL.AppendLine("    Sum(B.Qty*B.UPrice) As Amt3  ");
            }
            if (mMRAvailableBudgetSubtraction == "2")
                SQL.AppendLine("        ((((100.00 - D.Discount) * 0.01) * (D.Qty * G.UPrice)) - D.DiscountAmt + D.RoundingValue) As Amt3 ");
            SQL.AppendLine("    From TblMaterialRequestHdr A ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
            if (mMRAvailableBudgetSubtraction == "2")
            {
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo = C.MaterialRequestDocNo And B.DNo = C.MaterialRequestDNo And C.CancelInd = 'N' And C.Status = 'A' ");
                SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo = D.PORequestDocNo And C.DNo = D.PORequestDNo And D.CancelInd = 'N' ");
                SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo = E.DocNo And E.Status <> 'C' ");
                SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo = F.DocNo ");
                SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo = G.DocNo And C.QtDNo = G.DNo ");
            }
            SQL.AppendLine("    Where B.cancelind = 'N'  ");
            if (mFiscalYearRange.Length == 0)
                SQL.AppendLine("    And Left(A.DocDt, 4)=@Yr ");
            else
                SQL.AppendLine("    And Left(A.DocDt, 6) Between Concat(@Yr,'" + mFiscalYear[0] + "') And Concat(@Yr+1,'" + mFiscalYear[1] + "') ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2)=@Mth ");
            if (mIsBudgetUseMRProcessInd)
                SQL.AppendLine("    And A.ProcessInd = 'F' ");
            SQL.AppendLine("    Group By A.BCCode, A.DeptCode ");
            SQL.AppendLine(") E On A.BCCode=E.BCCode And C.DeptCode=E.DeptCode And C.Yr=E.Yr ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("    And C.Mth=E.Mth ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T3.DNo, T3.BCCode, Sum(T1.Amt) Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr T2 On T2.DocNo = @DocNo ");
            SQL.AppendLine("        And T1.ReqType Is Not Null ");
            SQL.AppendLine("        And T1.ReqType <> @ReqTypeForNonBudget ");
            SQL.AppendLine("        And T1.DeptCode = T2.DeptCode ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(T1.DocDt, 4) = T2.Yr ");
            else
                SQL.AppendLine("        And Left(T1.DocDt, 6) = Concat(T2.Yr, T2.Mth) ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.Status In ('O', 'A') ");
            SQL.AppendLine("        And Find_In_Set(DocType, ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'VoucherDocTypeBudget'), '')) ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl T3 On T2.DocNo = T3.DocNo ");
            SQL.AppendLine("        And T1.BCCode = T3.BCCode ");
            SQL.AppendLine("    Group By T3.DNo, T3.BCCode ");
            SQL.AppendLine(") F On A.DNo = F.DNo And A.BCCode = F.BCCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T5.DNo, T1.BCCode, Sum(T1.Amt1 + T1.Amt2 + T1.Amt3 + T1.Amt4 + T1.Amt5 + T1.Amt6 + T1.Detasering) Amt ");
            SQL.AppendLine("    From TblTravelRequestDtl7 T1 ");
            SQL.AppendLine("    Inner Join TblTravelRequestHdr T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("        And T2.Status In ('O', 'A') ");
            SQL.AppendLine("        And T1.BCCode Is Not Null ");
            SQL.AppendLine("    Inner Join TblBudgetCategory T3 On T1.BCCode = T3.BCCode ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr T4 On T4.DocNo = @DocNo ");
            SQL.AppendLine("        And T3.DeptCode = T4.DeptCode ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(T2.DocDt, 4) = T4.Yr ");
            else
                SQL.AppendLine("        And Left(T2.DocDt, 6) = Concat(T4.Yr, T4.Mth) ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl T5 On T4.DocNo = T5.DocNo ");
            SQL.AppendLine("        And T5.BCCode = T1.BCCode ");
            SQL.AppendLine("    Group By T5.DNo, T1.BCCode ");
            SQL.AppendLine(") G On A.DNo = G.DNo And A.BCCode = G.BCCode ");

            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T.DNo, T.BCCode, Sum(T.Amt) Amt From ( ");
            SQL.AppendLine("    Select F.DNo, D.BCCode, Case when A.AcType = 'D' Then IFNULL(A.Amt*-1, 0.00) ELSE IFNULL(A.Amt, 0.00) END As Amt ");
            SQL.AppendLine("    From tblvoucherhdr A ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X1.DocNo, X1.VoucherRequestDocNo ");
            SQL.AppendLine("        From tblvoucherhdr X1 ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
            SQL.AppendLine("        AND X1.DocType = '58' ");
            SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            SQL.AppendLine("        AND X2.Status In ('O', 'A') ");
            SQL.AppendLine("     ) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("        From tblvoucherhdr X1 ");
            SQL.AppendLine("        Inner Join tblcashadvancesettlementdtl X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("        AND X1.DocType = '56' ");
            SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            SQL.AppendLine("       AND X3.Status In ('O', 'A') ");
            SQL.AppendLine("    ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr E On E.DocNo = @DocNo ");
            SQL.AppendLine("        And D.DeptCode = E.DeptCode ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(A.DocDt, 4) = E.Yr ");
            else
                SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(E.Yr, E.Mth) ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl F On E.DocNo = F.DocNo ");
            SQL.AppendLine("        And F.BCCode = D.BCCode ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.DNo, T.BCCode ");
            SQL.AppendLine(") H On A.DNo = H.DNo And A.BCCode = H.BCCode ");
            SQL.AppendLine("Left Join TblItem I On A.ItCode = I.ItCode ");
            if (mIsCASUsedForBudget)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select E.DNo, C.BCCode, Sum(B.Amt) Amt ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        And A.`Status` = 'A' And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.DocStatus = 'F' ");
                SQL.AppendLine("        And B.CCtCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode And B.BCCode=C.BCCode ");
                SQL.AppendLine("        And C.CCtCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetRequestHdr D On D.DocNo = @DocNo And D.DeptCode = C.DeptCode ");
                SQL.AppendLine("    Inner Join TblBudgetRequestDtl E On D.DocNo = E.DocNo ");
                if (mIsCASUseBudgetCategory) SQL.AppendLine(" And C.BCCode = E.BCCode ");
                SQL.AppendLine("    Group By E.DNo, C.BCCode ");
                SQL.AppendLine(") J On A.DNo = J.DNo And A.BCCode = J.BCCode ");
            }

            SQL.AppendLine("Where A.DocNo = @DocNo Order By A.DNo");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                {
                    "DNo",
                    "BCCode", "BCName", "Amt", "Amt2", "Amt3",
                    "LocalCode", "Remark", "ItCode", "ItName",
                    "Qty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Grd.Cells[Row, 3].Value = 0m;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 13 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        internal void ShowItemInfo(string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ItCode, ItName ");
            SQL.AppendLine("From TblItem ");
            SQL.AppendLine("Where ItCode = @ItCode ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                {
                    "ItCode", "ItName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 1);
                }, false, false, true, false
            );
        }

        #endregion

        #region Additional Method

        private string BrowseCSV()
        {
            OD.InitialDirectory = "c:";
            OD.Filter = "CSV files (*.csv)|*.CSV";
            OD.FilterIndex = 2;
            OD.ShowDialog();

            return OD.FileName;
        }

        private void ReadCSV(string FileName)
        {
            var l = new List<Detail>();

            try
            {
                bool IsFirst = true;
                using (var rd = new StreamReader(FileName))
                {
                    while (!rd.EndOfStream)
                    {
                        var splits = rd.ReadLine().Split(',');
                        if (splits.Count() != 3)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Make sure this budget category code " + splits[0].Trim() + " have a correct column count data.");
                            l.Clear();
                            Sm.ClearGrd(Grd1, true);
                            return;
                        }
                        else
                        {
                            if (IsFirst)
                                IsFirst = false;
                            else
                            {
                                var arr = splits.ToArray();
                                var mCount = Sm.GetValue("Select Max(Length(BCCode)) BCCode From TblBudgetCategory Where ActInd = 'Y'; ");
                                if (arr[0].Trim().Length > 0)
                                {
                                    l.Add(new Detail()
                                    {
                                        BCCode = (mCount.Length > 0) ? Sm.Right(string.Concat("00000000", splits[0].Trim()), Int32.Parse(mCount)) : splits[0].Trim(),
                                        BCName = string.Empty,
                                        BCLocalCode = string.Empty,
                                        Amt = Decimal.Parse(splits[1].Trim()),
                                        Amt1 = 0m,
                                        Amt2 = 0m,
                                        Remark = splits[2].Trim(),
                                        RemoveInd = false
                                    });
                                }
                            }
                        }
                    }

                    if (l.Count > 0) ProcessCSV(ref l);
                }
            }
            catch (Exception e)
            {
                Sm.StdMsg(mMsgType.Warning, e.Message);
            }
            finally
            {
                l.Clear();
            }
        }

        private void ProcessCSV(ref List<Detail> l)
        {
            string BCCode = string.Empty;
            foreach (var x in l)
            {
                if (BCCode.Length > 0) BCCode += ",";
                BCCode += x.BCCode;
            }

            var invalidBCCode = ProcessInvalidBCCode(ref l, BCCode);

            if (invalidBCCode.Count() != 0)
            {
                RemoveInvalidBCCode(ref invalidBCCode, ref BCCode, ref l);
                Sm.StdMsg(mMsgType.Info, "Some of budget category from your imported file was dropped due to incorrect relation to selected Department.");
            }

            var b = new List<BudgetCategory>();
            GetBudgetCategoryData(ref BCCode, ref b);
            ProcessBudgetCategoryData(ref l, ref b);
            ProcessToGrid(ref l);
            ComputeTotalAmount();

            b.Clear();
        }

        private List<string> ProcessInvalidBCCode(ref List<Detail> l, string BCCode)
        {
            var SQL = new StringBuilder();
            var invalidBCCode = new List<string>();

            SQL.AppendLine("Select BCCode ");
            SQL.AppendLine("From TblBudgetCategory T ");
            SQL.AppendLine("Where Find_In_Set(T.BCCode, @BCCode) ");
            SQL.AppendLine("And Not Exists ( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblBudgetCategory A ");
            SQL.AppendLine("    Where A.BCCode = T.BCCode ");
            SQL.AppendLine("    And A.DeptCode = @DeptCode ");
            SQL.AppendLine("); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (Sm.DrStr(dr, c[0]).Length > 0)
                        {
                            invalidBCCode.Add(Sm.DrStr(dr, c[0]));
                        }
                    }
                }
                dr.Close();
            }

            return invalidBCCode;
        }

        private void RemoveInvalidBCCode(ref List<string> invalidBCCode, ref string BCCode, ref List<Detail> l)
        {
            foreach(var x in invalidBCCode)
            {
                foreach (var y in l.Where(w => w.BCCode == x))
                {
                    y.RemoveInd = true;
                }
            }

            l.RemoveAll(w => w.RemoveInd);

            BCCode = string.Empty;

            foreach (var x in l)
            {
                if (BCCode.Length > 0) BCCode += ",";
                BCCode += x.BCCode;
            }
        }

        private void GetBudgetCategoryData(ref string BCCode, ref List<BudgetCategory> b)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode, BCName, LocalCode ");
            SQL.AppendLine("From TblBudgetCategory ");
            SQL.AppendLine("Where Find_In_Set(BCCode, @BCCode); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "BCName", "LocalCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        b.Add(new BudgetCategory()
                        {
                            BCCode = Sm.DrStr(dr, c[0]),
                            BCName = Sm.DrStr(dr, c[1]),
                            BCLocalCode = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessBudgetCategoryData(ref List<Detail> l, ref List<BudgetCategory> b)
        {
            foreach (var x in b)
            {
                foreach (var y in l.Where(w => w.BCCode == x.BCCode))
                {
                    y.BCName = x.BCName;
                    y.BCLocalCode = x.BCLocalCode;
                }
            }
        }

        private void ProcessToGrid(ref List<Detail> l)
        {
            Sm.ClearGrd(Grd1, true);

            Grd1.BeginUpdate();
            int Row = 0;
            foreach (var x in l)
            {
                Grd1.Rows.Add();
                Grd1.Cells[Row, 1].Value = x.BCCode;
                Grd1.Cells[Row, 2].Value = x.BCName;
                Grd1.Cells[Row, 3].Value = Grd1.Cells[Row, 5].Value = Grd1.Cells[Row, 6].Value = 0m;
                Grd1.Cells[Row, 4].Value = x.Amt;
                Grd1.Cells[Row, 7].Value = x.BCLocalCode;
                Grd1.Cells[Row, 8].Value = x.Remark;
                Grd1.Cells[Row, 13].Value = 0m;

                Row++;
            }
            Grd1.EndUpdate();
        }

        internal void GetParameter()
        {
            mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            mIsBudgetRequestUseItemAndQty = Sm.GetParameterBoo("IsBudgetRequestUseItemAndQty");
            mIsCASUsedForBudget = Sm.GetParameterBoo("IsCASUsedForBudget");
            mIsBudgetRequestApprovalNoNeedStartAmt = Sm.GetParameterBoo("IsBudgetRequestApprovalNoNeedStartAmt");
            mIsBudgetUseMRProcessInd = Sm.GetParameterBoo("IsBudgetUseMRProcessInd");
            mIsCASUseBudgetCategory = Sm.GetParameterBoo("IsCASUseBudgetCategory");
            mIsBudgetRequest2AutoCreateBudgetMaintenance = Sm.GetParameterBoo("IsBudgetRequest2AutoCreateBudgetMaintenance");
            mIsBudgetMaintenanceYearlyActived = Sm.GetParameterBoo("IsBudgetMaintenanceYearlyActived");
            mMRAvailableBudgetSubtraction = Sm.GetParameter("MRAvailableBudgetSubtraction");
            if (mMRAvailableBudgetSubtraction.Length == 0) mMRAvailableBudgetSubtraction = "1";
            mFiscalYearRange = Sm.GetParameter("FiscalYearRange");
            mFiscalYear = mFiscalYearRange.Split(',');
        }

        private void ComputeTotalAmount()
        {
            decimal Amt = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 4).Length != 0)
                    Amt += Sm.GetGrdDec(Grd1, r, 4);
            TxtAmt.Text = Sm.FormatNum(Amt, 0);
        }

        private void RefreshData()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BCCode, A.BCName, IfNull(B.Amt1, 0) As Amt1, IfNull(B.Amt2, 0) As Amt2, IfNull(C.Amt, 0) As Amt, A.LocalCode ");
            SQL.AppendLine("From TblBudgetCategory A ");
            SQL.AppendLine("Left Join TblBudgetSummary B On A.BCCode=B.BCCode And B.DeptCode=@DeptCode And B.Yr=@Yr ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("    And B.Mth=@Mth ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.BCCode, Sum(T.Amt) Amt ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.BCCode, ");
            if (mMRAvailableBudgetSubtraction == "1")
            {
                if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                    SQL.AppendLine("    (B.Qty * Case When A.EximInd = 'Y' Then B.UPrice Else B.EstPrice End) As Amt ");
                else
                    SQL.AppendLine("    (B.Qty*B.UPrice) As Amt  ");
            }
            if (mMRAvailableBudgetSubtraction == "2")
                SQL.AppendLine("        ((((100.00 - D.Discount) * 0.01) * (D.Qty * G.UPrice)) - D.DiscountAmt + D.RoundingValue) As Amt ");
            SQL.AppendLine("        From TblMaterialRequestHdr A ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
            if (mMRAvailableBudgetSubtraction == "2")
            {
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo = C.MaterialRequestDocNo And B.DNo = C.MaterialRequestDNo And C.CancelInd = 'N' And C.Status = 'A' ");
                SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo = D.PORequestDocNo And C.DNo = D.PORequestDNo And D.CancelInd = 'N' ");
                SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo = E.DocNo And E.Status <> 'C' ");
                SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo = F.DocNo ");
                SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo = G.DocNo And C.QtDNo = G.DNo ");
            }
            if (mFiscalYearRange.Length == 0)
                SQL.AppendLine("        Where Left(A.DocDt, 4)=@Yr ");
            else
                SQL.AppendLine("        Where Left(A.DocDt, 6) Between Concat(@Yr,'" + mFiscalYear[0] + "') And Concat(@Yr+1,'" + mFiscalYear[1] + "') ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2)=@Mth ");
            if (mIsBudgetUseMRProcessInd)
                SQL.AppendLine("    And A.ProcessInd = 'F' ");
            SQL.AppendLine("        And A.DeptCode=@DeptCode ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select BCCode, Amt ");
            SQL.AppendLine("        From TblVoucherRequestHdr ");
            SQL.AppendLine("        Where ReqType Is Not null ");
            SQL.AppendLine("        And ReqType <> @ReqTypeForNonBudget ");
            SQL.AppendLine("        And CancelInd = 'N' ");
            SQL.AppendLine("        And Status In ('O', 'A') ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(DocDt, 4) = @Yr ");
            else
                SQL.AppendLine("        And Left(DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("        And Find_In_Set(DocType, ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'VoucherDocTypeBudget'), '')) ");
            SQL.AppendLine("        And DeptCode = @DeptCode ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select T1.BCCode, (T1.Amt1 + T1.Amt2 + T1.Amt3 + T1.Amt4 + T1.Amt5 + T1.Amt6 + T1.Detasering) Amt ");
            SQL.AppendLine("        From TblTravelRequestDtl7 T1 ");
            SQL.AppendLine("        Inner Join TblTravelRequestHdr T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T2.CancelInd = 'N' ");
            SQL.AppendLine("            And T2.Status In ('O', 'A') ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("            And Left(T2.DocDt, 4) = Concat(@Yr) ");
            else
                SQL.AppendLine("            And Left(T2.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("            And T1.BCCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblBudgetCategory T3 On T1.BCCode = T3.BCCode ");
            SQL.AppendLine("            And T3.DeptCode = @DeptCode ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select D.BCCode, Case when A.AcType = 'D' Then IFNULL(A.Amt*-1, 0.00) ELSE IFNULL(A.Amt, 0.00) END As Amt ");
            SQL.AppendLine("        From tblvoucherhdr A  ");
            SQL.AppendLine("        Inner Join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            SELECT X1.DocNo, X1.VoucherRequestDocNo   ");
            SQL.AppendLine("            From tblvoucherhdr X1  ");
            SQL.AppendLine("            INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo  ");
            SQL.AppendLine("            AND X1.DocType = '58' ");
            SQL.AppendLine("            AND X1.CancelInd = 'N'  ");
            SQL.AppendLine("            AND X2.Status In ('O', 'A')  ");
            SQL.AppendLine("        ) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("        Inner Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            SELECT X2.DocNo, X2.VoucherDocNo, X3.DocNo VRDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("            From tblvoucherhdr X1  ");
            SQL.AppendLine("            Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("            INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("                AND X1.DocType = '56' ");
            SQL.AppendLine("                AND X1.CancelInd = 'N'  ");
            SQL.AppendLine("                AND X3.Status In ('O', 'A') ");
            SQL.AppendLine("                And X3.BCCode Is Not null ");
            SQL.AppendLine("        ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        Where D.DeptCode = @DeptCode  ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
            else
                SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(@Yr, @Mth) ");

            if (mIsCASUsedForBudget)
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select C.BCCode, B.Amt ");
                SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            And A.`Status` = 'A' And A.CancelInd = 'N' ");
                SQL.AppendLine("            And A.DocStatus = 'F' ");
                SQL.AppendLine("            And B.CCtCode Is Not Null ");
                SQL.AppendLine("            And B.CCtCode Is Not Null ");
                SQL.AppendLine("            And Left(A.DocDt, 4) = @Yr ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("            And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                SQL.AppendLine("            And C.CCtCode Is Not Null ");
                SQL.AppendLine("            And C.DeptCode = @DeptCode ");
                if (mIsCASUseBudgetCategory) SQL.AppendLine(" And B.BCCode = C.BCCode ");

            }

            SQL.AppendLine("   ) T ");
            SQL.AppendLine("   Group By T.BCCode ");
            SQL.AppendLine(") C On A.BCCode=C.BCCode ");

            #region Prepare Code For show item and qty in refreshdata by IBL
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select B.BCCode, B.Qty, C.ItCode, C.ItName ");
            //SQL.AppendLine("    From TblBudgetRequestHdr A ");
            //SQL.AppendLine("    Inner Join TblBudgetRequestDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    Inner Join TblItem C On B.ItCode = C.ItCode ");
            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select Max(Concat(Yr,Mth)) As MaxDocDate ");
            //SQL.AppendLine("        From TblBudgetRequestHdr ");
            //SQL.AppendLine("    ) D On 0=0 ");
            //SQL.AppendLine("    Where A.Status In ('O','A') ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            //SQL.AppendLine("    And A.DeptCode = @DeptCode ");
            //SQL.AppendLine("    And D.MaxDocDate <= Concat(@Yr,@Mth) ");
            //SQL.AppendLine(") D On A.BCCode = D.BCCode ");
            #endregion

            SQL.AppendLine("Where A.DeptCode=@DeptCode ");
            SQL.AppendLine("And A.ActInd='Y'; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                { 
                     //0
                    "BCCode", 

                    //1-5
                    "BCName", "Amt1", "Amt2", "Amt", "LocalCode",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Grd.Cells[Row, 8].Value = null;
                    Grd.Cells[Row, 13].Value = 0m;

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 13 });
            Sm.FocusGrd(Grd1, 0, 2);
            ComputeTotalAmount();
        }

        private void CopyPrevMth()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length != 0)
                    Grd1.Cells[r, 4].Value = 0m;

            string BCCode = string.Empty;
            decimal Amt = 0m;

            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);

            if (Mth == "01")
            {
                Yr = (int.Parse(Yr) - 1).ToString();
                Mth = "12";
            }
            else
            {
                if (!mIsBudget2YearlyFormat)
                    Mth = Sm.Right(string.Concat("0", (int.Parse(Mth) - 1).ToString()), 2);
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode, IfNull(Amt2, Amt1) As Amt ");
            SQL.AppendLine("From TblBudgetSummary ");
            SQL.AppendLine("Where Yr=@Yr ");
            if (!mIsBudget2YearlyFormat) SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And DeptCode=@DeptCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        BCCode = Sm.DrStr(dr, 0);
                        Amt = Sm.DrDec(dr, 1);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), BCCode))
                            {
                                Grd1.Cells[r, 4].Value = Amt;
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                dr.Close();
            }
            ComputeTotalAmount();
        }

        private void CopyPrevYr()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length != 0)
                    Grd1.Cells[r, 4].Value = 0m;

            string BCCode = string.Empty;
            decimal Amt = 0m;


            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode, IfNull(Amt2, Amt1) As Amt ");
            SQL.AppendLine("From TblBudgetSummary ");
            SQL.AppendLine("Where Yr=@Yr ");
            SQL.AppendLine("And DeptCode=@DeptCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Yr", (int.Parse(Sm.GetLue(LueYr)) - 1).ToString());
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        BCCode = Sm.DrStr(dr, 0);
                        Amt = Sm.DrDec(dr, 1);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), BCCode))
                            {
                                Grd1.Cells[r, 4].Value = Amt;
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                dr.Close();
            }
            ComputeTotalAmount();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode), string.Empty);
            if (BtnSave.Enabled)
            {
                TxtAmt.EditValue = Sm.FormatNum(0m, 0);
                ClearGrd();
            }
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue2(Sl.SetLueEntCode), string.Empty);
            if (BtnSave.Enabled)
            {
                TxtAmt.EditValue = Sm.FormatNum(0m, 0);
                ClearGrd();
            }
        }

        private void BtnCopyPrevMth_Click(object sender, EventArgs e)
        {
            if (mIsBudget2YearlyFormat)
            {
                if (Sm.StdMsgYN("Question", "Do you want to copy amount from previous year's data ?") == DialogResult.No) return;
            }
            else
            {
                if (Sm.StdMsgYN("Question", "Do you want to copy amount from previous month's data ?") == DialogResult.No) return;
            }

            if (!(Sm.IsLueEmpty(LueYr, "Year") ||
                (!mIsBudget2YearlyFormat && Sm.IsLueEmpty(LueMth, "Month")) ||
                Sm.IsLueEmpty(LueDeptCode, "Department")))
            {
                try
                {
                    if (mIsBudget2YearlyFormat)
                        CopyPrevYr();
                    else
                        CopyPrevMth();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!(Sm.IsLueEmpty(LueYr, "Year") ||
                        (!mIsBudget2YearlyFormat && Sm.IsLueEmpty(LueMth, "Month")) ||
                        Sm.IsLueEmpty(LueDeptCode, "Department")))
                {
                    string FileName = BrowseCSV();
                    if (FileName.Length == 0 || FileName == "openFileDialog1") return;
                    ReadCSV(FileName);
                }
            }
        }
        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to refresh data ?") == DialogResult.No) return;
            if (!(Sm.IsLueEmpty(LueYr, "Year") ||
                (!mIsBudget2YearlyFormat && Sm.IsLueEmpty(LueMth, "Month")) ||
                Sm.IsLueEmpty(LueDeptCode, "Department")))
                RefreshData();
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                TxtAmt.EditValue = Sm.FormatNum(0m, 0);
                ClearGrd();
            }
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                TxtAmt.EditValue = Sm.FormatNum(0m, 0);
                ClearGrd();
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeTrim(MeeCancelReason);
            ChkCancelInd.Checked = (MeeCancelReason.Text.Length > 0);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkCancelInd.Checked)
                {
                    if (MeeCancelReason.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Reason for cancellation is empty.");
                        ChkCancelInd.Checked = false;
                    }
                }
                else
                    MeeCancelReason.EditValue = null;
            }
        }

        #endregion

        #region Grid Control Event

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 9) Sm.FormShowDialog(new FrmBudgetRequest2Dlg(this, e.RowIndex));

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 9)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmBudgetRequest2Dlg(this, e.RowIndex));
            }

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        private class Detail
        {
            public string BCCode { get; set; }
            public string BCName { get; set; }
            public string BCLocalCode { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
            public bool RemoveInd { get; set; }
        }

        private class BudgetCategory
        {
            public string BCCode { get; set; }
            public string BCName { get; set; }
            public string BCLocalCode { get; set; }
        }

        #endregion
    }
}
