﻿#region Update
/*
    16/08/2019 [TKG] filter by site
*/
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpDecree : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mEmpCode = string.Empty;
        internal bool mIsFilterBySiteHR = false;
        internal FrmEmpDecreeFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmpDecree(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Employee Decree";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "",
                        "Old Code", 
                        "Employee's Name",
                        "Department",

                        //6-9
                        "Position",
                        "Latest Price"+Environment.NewLine+"Factor Value",
                        "Price"+Environment.NewLine+"Factor Value",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        100, 20, 100, 200, 200,
 
                        //6-10
                        200, 130, 130, 300
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8 }, 0);
        }

      
        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueSiteCode, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueSiteCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 2, 8, 9 });
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueSiteCode, MeeRemark
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 8 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpDecreeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpDecree", "TblEmpDecreeHdr");

                var cml = new List<MySqlCommand>();

                cml.Add(SaveEmpDecreeHdr(DocNo));
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveEmpDecreeDtl(DocNo, Row));

                Sm.ExecCommands(cml);

                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        public void InsertData()
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetDteCurrentDate(DteDocDt);
            Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR?"Y":"N");
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
               if (Sm.IsGrdValueEmpty(Grd1, Row, 8, true, "Price factor value is empty.")) return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpDecreeHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpDecreeHdr(DocNo, DocDt, SiteCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @SiteCode, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveEmpDecreeDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpDecreeDtl Set ActInd = 'N' Where EmpCode = @EmpCode ;");
            SQL.AppendLine("Insert Into TblEmpDecreeDtl(DocNo, DNo, EmpCode, ActInd, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, 'Y', @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowEmpDecreeHdr(DocNo);
                ShowEmpDecreeDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpDecreeHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, SiteCode, Remark From TblEmpDecreeHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "SiteCode", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[2]), "N");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowEmpDecreeDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpCodeOld, B.EmpName, C.DeptName, D.PosName, A.Amt, A.Remark ");
            SQL.AppendLine("From TblEmpDecreeDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join tblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode = D.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "EmpCode", 
                    "EmpCodeOld", "EmpName", "DeptName", "PosName", "Amt", 
                    "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                    Grd1.Cells[Row, 7].Value = 0m;
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 8 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 &&
                    !Sm.IsLueEmpty(LueSiteCode, "Site"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                       Sm.FormShowDialog(new FrmEmpDecreeDlg(this, Sm.GetLue(LueSiteCode)));
                }

            }
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 &&
                !Sm.IsLueEmpty(LueSiteCode, "Site"))
                Sm.FormShowDialog(new FrmEmpDecreeDlg(this, Sm.GetLue(LueSiteCode)));

                if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    var f = new FrmEmployee(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
        }
      
        #endregion

        #region Additional Method

        internal string GetSelectedEmployee()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR?"Y":"N");
        }

        #endregion

        #endregion
    }
}
