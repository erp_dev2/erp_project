﻿#region Update
    //22/06/2017 [TKG] Tambah fasilitas untuk ubah lot
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBinUpdateStatus : RunSystem.FrmBase5
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mSQL = string.Empty,
            mDocType="32";
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmBinUpdateStatus(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
                Sl.SetLueOption(ref LueStatus, "BinStatus");
                LueLot.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Bin, A.Status, B.OptDesc As StatusDesc, IfNull(A.Lot, '-') As Lot ");
            SQL.AppendLine("From TblBin A ");
            SQL.AppendLine("Inner Join TblOption B On B.OptCat='BinStatus' And A.Status=B.OptCode ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("And A.Bin<>'-' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Bin",
                        "Status", 
                        "Status",
                        "Old Lot",
                        "New Lot"
                    },
                    new int[] 
                    {
                        //0
                        20,

                        //1-5
                        130, 100, 150, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "Lot", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.Bin;",
                        new string[] { "Bin", "Status", "StatusDesc", "Lot" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        }, true, false, true, false
                    );
                if (Grd1.Rows.Count > 1) Grd1.Rows.RemoveAt(Grd1.Rows.Count - 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No || 
                    IsEditedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();
                string
                    DocNo = string.Empty,
                    DocTitle = Sm.GetParameter("DocTitle"),
                    DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='LotTransfer';");

                string DtTm = Sm.ServerCurrentDateTime();
                string DocDt = Sm.Left(DtTm, 8);
                    
                string
                    Yr = DocDt.Substring(2, 2),
                    Mth = DocDt.Substring(4, 2);
                int i = 0;
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdBool(Grd1, r, 0))
                    {
                        cml.Add(SaveBinStatus(r, DtTm));
                        if (Sm.GetGrdStr(Grd1, r, 4) != Sm.GetGrdStr(Grd1, r, 5))
                        {
                            i++;
                            DocNo = GenerateDocNo(Yr, Mth, DocTitle, DocAbbr, i);
                            cml.Add(SaveLotTransfer(DocNo, DocDt, DtTm, r));
                        }
                    }
                }
                
                Sm.ExecCommands(cml);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private string GenerateDocNo(string Yr, string Mth, string DocTitle, string DocAbbr, int i)
        {
            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("    Select Right(Concat('0000', Convert(DocNoTemp+Convert(" + i.ToString() + ", Decimal), Char)), 4) ");
            SQL.Append("    From ( ");
            SQL.Append("        Select Convert(Left(DocNo, 4), Decimal) As DocNoTemp ");
            SQL.Append("        From TblLotTransfer ");
            SQL.Append("        Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("        Order By Left(DocNo, 4) Desc Limit 1 ");
            SQL.Append("    ) As Temp ");
            SQL.Append("), Right(Concat('0000', '"+ i.ToString() + "'), 4)) ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? string.Empty : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        private bool IsEditedDataNotValid()
        {
            SetLot();
            return
                Sm.IsLueEmpty(LueStatus, "Bin's status") ||
                IsEditedDataNotExisted() ||
                IsGrdValueNotValid() ||
                IsBinNotValid(); 
        }

        private bool IsBinNotValid()
        {
            var Bin = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 0))
                {
                    if (Bin.Length > 0) Bin += ",";
                    Bin += Sm.GetGrdStr(Grd1, r, 1);
                }
            }
            if (Bin.Length == 0) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Bin From ( ");
            SQL.AppendLine("    Select Bin, WhsCode, Count(Bin) ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct Bin, WhsCode ");
            SQL.AppendLine("        From TblStockSummary ");
            SQL.AppendLine("        Where Bin<>'-' ");
            SQL.AppendLine("        And Qty<>0 ");
            SQL.AppendLine("        And Find_In_Set(Bin, @Bin) ");
            SQL.AppendLine("    ) T "); 
            SQL.AppendLine("    Group By Bin, WhsCode ");
            SQL.AppendLine("    Having Count(Bin)>1 ");
            SQL.AppendLine(") Tbl Order By Bin Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Bin", Bin);

            Bin = Sm.GetValue(cm);
            if (Bin.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Bin : " + Bin + Environment.NewLine +
                    "It has more than 1 warehouse.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 0))
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 5, false, 
                        "Bin : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine + 
                        "New lot is empty."
                        )) return true;
                }
            }
            return false;
        }

        private bool IsEditedDataNotExisted()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 0)) return false;

            Sm.StdMsg(mMsgType.Warning, "You need to edit minimum 1 record.");
            return true;
        }

        private MySqlCommand SaveBinStatus(int r, string DtTm)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblBin Set " +
                    "   Status=@Status, Lot=@Lot, " +
                    "   LastUpBy=@UserCode, LastUpDt=@CurrentDateTime " +
                    "Where Bin=@Bin; "
            };
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 5));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CurrentDateTime", DtTm);

            return cm;
        }

        private MySqlCommand SaveLotTransfer(string DocNo, string DocDt, string DtTm, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLotTransfer(DocNo, DocDt, Bin, Lot1, Lot2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Bin, @Lot1, @Lot2, @UserCode, @CurrentDateTime); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, CancelInd, DocDt, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, @DocNo, ");
            SQL.AppendLine("Right(Concat('00000', Cast((@row:=@row+1) As Char(5))), 5) As DNo, ");
            SQL.AppendLine("'N', @DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("@UserCode, @CurrentDateTime ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, ");
            SQL.AppendLine("    -1*Abs(Qty) As Qty, -1*Abs(Qty2) As Qty2, -1*Abs(Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where Bin=@Bin ");
            SQL.AppendLine("    And Lot=@Lot1 ");
            SQL.AppendLine("    And Qty<>0 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select WhsCode, @Lot2 As Lot, Bin, ItCode, PropCode, BatchNo, Source, ");
            SQL.AppendLine("    Abs(Qty) As Qty, Abs(Qty2) As Qty2, Abs(Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where Bin=@Bin ");
            SQL.AppendLine("    And Lot=@Lot1 ");
            SQL.AppendLine("    And Qty<>0 ");
            SQL.AppendLine(") T;");

            SQL.AppendLine("Insert Into TblStockSummary ");
            SQL.AppendLine("(WhsCode, Lot, Bin, Source, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.WhsCode, @Lot2, T.Bin, T.Source, T.ItCode, T.PropCode, T.BatchNo, ");
            SQL.AppendLine("0 As Qty, 0 As Qty2, 0 As Qty3, @UserCode, @CurrentDateTime ");
            SQL.AppendLine("From TblStockSummary T ");
            SQL.AppendLine("Where T.Bin=@Bin ");
            SQL.AppendLine("And T.Lot=@Lot1 ");
            SQL.AppendLine("And T.Qty<>0 ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select WhsCode From TblStockSummary ");
            SQL.AppendLine("    Where WhsCode=T.WhsCode ");
            SQL.AppendLine("    And Lot=@Lot2 ");
            SQL.AppendLine("    And Bin=@Bin ");
            SQL.AppendLine("    And Source=T.Source ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblStockSummary A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select WhsCode, @Lot2 As Lot, Bin, Source, Qty, Qty2, Qty3 ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where Bin=@Bin ");
            SQL.AppendLine("    And Lot=@Lot1 ");
            SQL.AppendLine("    And Qty<>0 ");
            SQL.AppendLine(") B On A.WhsCode=B.WhsCode And A.Lot=B.Lot And A.Bin=B.Bin And A.Source=B.Source ");
            SQL.AppendLine("Set A.Qty=A.Qty+B.Qty, A.Qty2=A.Qty2+B.Qty2, A.Qty3=A.Qty3+B.Qty3,  ");
            SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=@CurrentDateTime; ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=0, Qty2=0, Qty3=0, LastUpBy=@UserCode, LastUpDt=@CurrentDateTime ");
            SQL.AppendLine("Where Bin=@Bin ");
            SQL.AppendLine("And Lot=@Lot1 ");
            SQL.AppendLine("And Qty<>0;");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@Lot1", Sm.GetGrdStr(Grd1, r, 4));
            Sm.CmParam<String>(ref cm, "@Lot2", Sm.GetGrdStr(Grd1, r, 5));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CurrentDateTime", DtTm);

            return cm;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length == 0)
                e.DoDefault = false;
            else
            {
                if (e.ColIndex == 0)
                    Grd1.Cells[e.RowIndex, 5].Value = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                if (e.ColIndex == 5)
                {
                    if (!Sm.GetGrdBool(Grd1, e.RowIndex, 0))
                        e.DoDefault = false;
                    else
                        LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e);
                }
            }
        }

        #endregion

        #region Additional Method

        private void SetLueLot(ref LookUpEdit Lue, string Lot)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Col1 From ( ");
            SQL.AppendLine("Select Lot As Col1 from tblLotHdr Where ActInd = 'Y' ");
            if (Lot.Length>0) SQL.AppendLine("Union All Select @Lot As Col1 ");
            SQL.AppendLine(") T Order By Col1;");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Lot", Lot);

            Sm.SetLue1(ref Lue, ref cm, "Lot");
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            var Lot = Sm.GetGrdStr(Grd, fCell.RowIndex, 5);
            if (Lot.Length == 0)
                Lue.EditValue = null;
            else
            {
                SetLueLot(ref LueLot, Lot); 
                Sm.SetLue(Lue, Lot);
            }
            Lue.Visible = true;
            Lue.Focus();
            fAccept = true;
        }

        private void SetLot() 
        {
            var Bin = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 0))
                {
                    if (Bin.Length > 0) Bin += ",";
                    Bin += Sm.GetGrdStr(Grd1, r, 1);
                }
            }
            if (Bin.Length == 0) return;

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Bin", Bin);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = 
                    "Select Bin, IfNull(Lot, '-') As Lot From TblBin " +
                    "Where Find_In_Set(Bin, @Bin) " +
                    "Order By Bin;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "Bin", "Lot" });
                
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Bin = Sm.DrStr(dr, c[0]);
                        for (int r = 0; r < Grd1.Rows.Count; r++)
                        {
                            if (Sm.CompareStr(Bin, Sm.GetGrdStr(Grd1, r, 1)))
                            {
                                Grd1.Cells[r, 4].Value = Sm.DrStr(dr, c[1]);
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue2(Sl.SetLueOption), "BinStatus");
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue2(SetLueLot), string.Empty);
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueLot);
            }
        }

        private void LueLot_Validated(object sender, EventArgs e)
        {
            LueLot.Visible = false;
        }

        #endregion

        #endregion
    }
}
