﻿#region Update
/*
    16/10/2021 [TKG/IMS] new app.
    26/11/2021 [TKG/IMS] filter menggunakan item category (kodenya) dan tanggal akhir dokumen yg diproses
    26/11/2021 [TKG/IMS] recv vendor, stock initial, mutasi (to) tetap menggunakan harga aktual di movement.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmUpdateMAP : RunSystem.FrmBase12
    {
        #region Field, Property

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string mMainCurCode = string.Empty;
        private bool mIsMovingAvgEnabled = false;

        #endregion

        #region Constructor

        public FrmUpdateMAP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                GetParameter();
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Button Click

        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date")) return;

            if (Sm.StdMsgYN("Process", string.Empty, mMenuCode) == DialogResult.No) return;

            try
            {
                var l  = new List<Result>();
                string ItCode = string.Empty, Query = string.Empty;
                bool IsFirst = true;
                
                ProcessData1(ref l);
                if (l.Count > 0)
                {
                    foreach (var i in l)
                    {
                        if (!Sm.CompareStr(i.ItCode, ItCode))
                        {
                            Query = string.Empty;
                            IsFirst = true;
                        }
                        else
                            IsFirst = false;

                        ProcessData2(i, ref Query, IsFirst);
                        if (Query.Length > 0) Query += " Or ";
                        Query += (string.Concat(
                            " (",
                            "DocType='" + i.DocType + "' And ",
                            "DocNo='" + i.DocNo + "' And ",
                            "DNo='" + i.DNo + "' And ",
                            "CancelInd='" + i.CancelInd + "' ",
                            ")"));
                        
                        ItCode = i.ItCode;
                    }
                    Sm.StdMsg(mMsgType.Info, "Process is completed.");
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                TxtItCtCode.Focus();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, "Error : " + Exc.Message.ToString());
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MainCurCode', 'IsMovingAvgEnabled' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue=="Y"; break;                            

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;                            
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessData1(ref List<Result> l)
        { 
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DNo, A.DocDt, A.DocType, A.ItCode, C.MovingAvgInd As ItMovingAvgInd, D.MovingAvgInd As WhsMovingAvgInd, A.Qty, A.CreateBy, A.CreateDt, A.CancelInd ");
            SQL.AppendLine("From TblStockMovement A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            if (TxtItCtCode.Text.Length > 0) 
            {
                SQL.AppendLine(" And B.ItCtCode=@ItCtCode ");
                Sm.CmParam<string>(ref cm, "@ItCtCode", TxtItCtCode.Text);
            }
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode And C.MovingAvgInd='Y' ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("Where A.DocDt<=@DocDt ");
            SQL.AppendLine("Order By A.ItCode, A.DocDt, A.DocType, A.DocNo, A.DNo, A.CancelInd; "); //docdt buat jaga2 do dan recv whs

            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocNo", 

                    //1-5
                    "DNo", 
                    "DocDt", 
                    "ItCode", 
                    "ItMovingAvgInd", 
                    "WhsMovingAvgInd",

                    //6-10
                    "DocType",
                    "Qty",
                    "CreateBy",
                    "CreateDt",
                    "CancelInd"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DNo = Sm.DrStr(dr, c[1]),
                            DocDt = Sm.DrStr(dr, c[2]),
                            ItCode = Sm.DrStr(dr, c[3]),
                            ItMovingAvgInd = Sm.DrStr(dr, c[4]),
                            WhsMovingAvgInd= Sm.DrStr(dr, c[5]),
                            DocType = Sm.DrStr(dr, c[6]),
                            Qty = Sm.DrDec(dr, c[7]),
                            CreateBy = Sm.DrStr(dr, c[8]),
                            CreateDt = Sm.DrStr(dr, c[9]),
                            CancelInd = Sm.DrStr(dr, c[10])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessData2(Result i, ref string Query, bool IsFirst)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var subSQL = string.Empty;

            if (Query.Length == 0)
                subSQL = " 0=1 ";
            else
                subSQL = " (" + Query + ") ";
            Sm.CmParam<string>(ref cm, "@IsMovingAvgEnabled", mIsMovingAvgEnabled?"Y":"N");
            Sm.CmParam<string>(ref cm, "@IsItCtMovingAvgEnabled", i.ItMovingAvgInd);
            Sm.CmParam<string>(ref cm, "@IsWhsMovingAvgEnabled", i.WhsMovingAvgInd);
            Sm.CmParam<string>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<string>(ref cm, "@ItCode", i.ItCode);
            Sm.CmParam<string>(ref cm, "@DocNo", i.DocNo);
            Sm.CmParam<string>(ref cm, "@DNo", i.DNo);
            Sm.CmParam<string>(ref cm, "@DocType", i.DocType);
            Sm.CmParam<decimal>(ref cm, "@Qty", i.Qty);
            Sm.CmParam<string>(ref cm, "@CreateBy", i.CreateBy);
            Sm.CmParam<string>(ref cm, "@CreateDt", i.CreateDt);
            Sm.CmParam<string>(ref cm, "@CancelInd", i.CancelInd);


            SQL.AppendLine("Update TblStockMovement Set ");
            SQL.AppendLine("    MovingAvgCurCode=Null, ");
            SQL.AppendLine("    MovingAvgPrice=0.00 ");
            SQL.AppendLine("Where DocType=@DocType And DocNo=@DocNo And DNo=@DNo And CancelInd=@CancelInd; ");

            if (IsFirst)
            {
                SQL.AppendLine("Update TblItemMovingAvg Set ");
                SQL.AppendLine("    MovingAvgCurCode=Null, ");
                SQL.AppendLine("    MovingAvgPrice=0.00 ");
                SQL.AppendLine("Where itCode=@ItCode; ");
            }

            SQL.AppendLine("Set @Qty1:=IfNull((Select Sum(Qty) From TblStockMovement Where Qty>0.00 And ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("), 0.00); ");
	        SQL.AppendLine("Set @UPrice1:=IfNull((Select MovingAvgPrice From TblItemMovingAvg Where ItCode=@ItCode), 0.00); ");
	        SQL.AppendLine("Set @Qty2:=@Qty; ");
	        SQL.AppendLine("Set @UPrice2:=0.00; ");
            SQL.AppendLine("Set @MovingAvgPrice:=0.00; ");

            if (i.DocType == "03" ||
                i.DocType == "06" ||
                i.DocType == "08" ||
                i.DocType == "10" ||
                i.DocType == "17" ||
                i.DocType == "18" ||
                i.DocType == "29")
            {
                SQL.AppendLine("    Set @UPrice2=@UPrice1; ");
            }

            if (i.DocType=="01")
            {
                SQL.AppendLine("    Set @UPrice2=IfNull(( ");
                SQL.AppendLine("        Select ");
                SQL.AppendLine("        (((A.QtyPurchase/A.Qty)*F.UPrice)- ");
                SQL.AppendLine("        (((A.QtyPurchase/A.Qty)*F.UPrice)*C.Discount/100.00)- ");
                SQL.AppendLine("        ((1.00/A.Qty)*(A.QtyPurchase/C.Qty)*C.DiscountAmt)+ ");
                SQL.AppendLine("        ((1.00/A.Qty)*(A.QtyPurchase/C.Qty)*C.RoundingValue)) ");
                SQL.AppendLine("        * ");
                SQL.AppendLine("        Case When E.CurCode=@MainCurCode Then ");
                SQL.AppendLine("            1.00 ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            IfNull(( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=G.DocDt ");
                SQL.AppendLine("                And CurCode1=E.CurCode ");
                SQL.AppendLine("                And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ), 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("        From TblRecvVdDtl A ");
                SQL.AppendLine("        Inner Join TblPOHdr B On A.PODocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl C On A.PODocNo=C.DocNo And A.PODNo=C.DNo ");
                SQL.AppendLine("        Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
                SQL.AppendLine("        Inner Join TblQtDtl F On D.QtDocNo=F.DocNo And D.QtDNo=F.DNo ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr G On A.DocNo=G.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DNo=@DNo ");
                SQL.AppendLine("    ), 0.00); ");
            }

            if (i.DocType == "04")
            {
                SQL.AppendLine("    Set @UPrice2=IfNull(( ");
                SQL.AppendLine("        Select B.UPrice*A.ExcRate ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo AND B.DNo=@DNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ), 0.00); ");
            }

            if (i.DocType == "12")
            {
                SQL.AppendLine("Set @UPrice2=IfNull(( ");
                SQL.AppendLine("    Select ");
                SQL.AppendLine("    B.UPrice* ");
                SQL.AppendLine("    Case When B.CurCode=@MainCurCode Then ");
                SQL.AppendLine("    1.00 ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("        Select Amt From TblCurrencyRate ");
                SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("    From TblMutationsHdr A, TblMutationsDtl2 B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And A.DocNo=@DocNo ");
                SQL.AppendLine("    And B.DNo=@DNo ");
                SQL.AppendLine("    ), 0.00); ");
            }

            if (i.DocType == "13")
            {
                SQL.AppendLine("    Set @UPrice2=IfNull(( ");
                SQL.AppendLine("        Select ");
                SQL.AppendLine("        (B.UPrice- ");
                SQL.AppendLine("        (B.UPrice*B.Discount/100.00)+ ");
                SQL.AppendLine("        ((1.00/B.Qty)*B.RoundingValue)) * ");
                SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            IfNull(( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=A.DocDt ");
                SQL.AppendLine("                And CurCode1=A.CurCode ");
                SQL.AppendLine("                And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ), 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("        From TblRecvVdHdr A ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ), 0.00); ");
            }

            if (i.DocType == "23")
            {
                SQL.AppendLine("    Set @UPrice2=IfNull(( ");
                SQL.AppendLine("        Select C.UPrice ");
                SQL.AppendLine("        From TblRecvProductionHdr A ");
                SQL.AppendLine("        Inner Join TblRecvProductionDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
                SQL.AppendLine("        Inner Join TblShopFloorControlDtl C On B.ShopFloorControlDocNo=C.DocNo And B.ShopFloorControlDNo=C.DNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ), 0.00); ");
            }

            if (i.DocType == "25")
            {
                SQL.AppendLine("    Set @UPrice2=IfNull(( ");
                SQL.AppendLine("        Select A.UPrice2*B.ExcRate ");
                SQL.AppendLine("        From TblStockOpname2Dtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DNo=@DNo And A.Source2 Is Not Null ");
                SQL.AppendLine("    ), 0.00); ");
            }


            if (i.DocType == "55")
            {
                SQL.AppendLine("    Set @UPrice2=IfNull(( ");
                SQL.AppendLine("        Select ");
                SQL.AppendLine("        B.UPrice* ");
                SQL.AppendLine("        Case When B.CurCode=@MainCurCode Then ");
                SQL.AppendLine("        1.00 ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            IfNull(( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=A.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ), 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("        From TblDOProjectHdr A, TblDOProjectDtl2 B ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.DocNo=@DocNo ");
                SQL.AppendLine("        And B.DNo=@DNo ");
                SQL.AppendLine("    ), 0.00); ");
            }

            if (i.DocType == "01" || 
                i.DocType == "03" || 
                i.DocType == "04" ||
                i.DocType == "06" ||
                i.DocType == "08" ||
                i.DocType == "12" ||
                i.DocType == "13" ||
                i.DocType == "17" ||
                i.DocType == "18" ||
                i.DocType == "23" ||
                i.DocType == "25" ||
                i.DocType == "29" ||
                i.DocType == "55")
            {
                SQL.AppendLine("If (@Qty>0.00 ");
                SQL.AppendLine("And @IsMovingAvgEnabled='Y' ");
                SQL.AppendLine("And @IsItCtMovingAvgEnabled='Y' ");
                SQL.AppendLine("And @IsWhsMovingAvgEnabled='Y' ");
                SQL.AppendLine("And @CancelInd='N' ");
                SQL.AppendLine(") Then ");

                SQL.AppendLine("    Set @MovingAvgPrice=((@Qty1*@UPrice1)+(@Qty2*@UPrice2))/(@Qty1+@Qty2); ");

                if (i.DocType == "01" || i.DocType == "04" || i.DocType == "12" || i.DocType == "13")
                {
                    SQL.AppendLine("    Update TblStockMovement Set ");
                    SQL.AppendLine("        MovingAvgCurCode=@MainCurCode, ");
                    SQL.AppendLine("        MovingAvgPrice=@UPrice2 ");
                    SQL.AppendLine("    Where DocType=@DocType And DocNo=@DocNo And DNo=@DNo And CancelInd=@CancelInd; ");
                }
                else
                {
                    SQL.AppendLine("    Update TblStockMovement Set ");
                    SQL.AppendLine("        MovingAvgCurCode=@MainCurCode, ");
                    SQL.AppendLine("        MovingAvgPrice=@MovingAvgPrice ");
                    SQL.AppendLine("    Where DocType=@DocType And DocNo=@DocNo And DNo=@DNo And CancelInd=@CancelInd; ");
                }

                SQL.AppendLine("    Insert Into TblItemMovingAvg(ItCode, CreateBy, CreateDt) ");
                SQL.AppendLine("    Select A.ItCode, @CreateBy, @CreateDt ");
                SQL.AppendLine("    From TblItem A ");
                SQL.AppendLine("    Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode And B.MovingAvgInd='Y' ");
                SQL.AppendLine("    Where A.ItCode=@ItCode And ItCode Not In (Select ItCode From TblItemMovingAvg); ");

                SQL.AppendLine("    Update TblItemMovingAvg Set ");
                SQL.AppendLine("        MovingAvgCurCode=@MainCurCode, ");
                SQL.AppendLine("        MovingAvgPrice=@MovingAvgPrice ");
                SQL.AppendLine("    Where itCode=@ItCode; ");
                
                SQL.AppendLine("End If; ");
            }

            if (i.DocType == "02" ||
                i.DocType == "05" ||
                i.DocType == "09" ||
                i.DocType == "10" ||
                i.DocType == "11" ||
                i.DocType == "26" ||
                i.DocType == "27" ||
                i.DocType == "31" ||
                i.DocType == "54")
            {
                SQL.AppendLine("If (@IsWhsMovingAvgEnabled='N' ");
                SQL.AppendLine("And @IsMovingAvgEnabled='Y' ");
                SQL.AppendLine("And @IsItCtMovingAvgEnabled='Y' ");
                SQL.AppendLine("And @CancelInd='N' ");
                SQL.AppendLine(") Then ");
                SQL.AppendLine("    Update TblStockMovement Set ");
                SQL.AppendLine("        MovingAvgCurCode=@MainCurCode, ");
                SQL.AppendLine("        MovingAvgPrice=@UPrice1 ");
                SQL.AppendLine("    Where DocType=@DocType And DocNo=@DocNo And DNo=@DNo And CancelInd=@CancelInd; ");
                SQL.AppendLine("End If; ");
            }

            SQL.AppendLine("If (@IsMovingAvgEnabled='Y' And @CancelInd='Y') Then ");
            SQL.AppendLine("    Update TblStockMovement A ");
            SQL.AppendLine("    Inner Join TblStockMovement B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.DocNo=B.DocNo ");
            SQL.AppendLine("    And A.DNo=B.DNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    Set ");
            SQL.AppendLine("        A.MovingAvgCurCode=B.MovingAvgCurCode, ");
            SQL.AppendLine("        A.MovingAvgPrice=B.MovingAvgPrice ");
            SQL.AppendLine("    Where A.DocType=@DocType And A.DocNo=@DocNo And A.DNo=@DNo And A.CancelInd=@CancelInd; ");
            SQL.AppendLine("End If; ");

            cm.CommandText = SQL.ToString();

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item's category code");
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DocDt { get; set; }
            public string ItCode { get; set; }
            public string ItMovingAvgInd { get; set; }
            public string WhsMovingAvgInd { get; set; }
            public string DocType { get; set; }
            public decimal Qty { get; set; }
            public string CreateBy { get; set; }
            public string CreateDt { get; set; }
            public string CancelInd { get; set; }
        }

        #endregion
    }
}
