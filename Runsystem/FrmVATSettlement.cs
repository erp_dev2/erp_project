﻿#region Update
/*
    11/06/2017 [TKG] VAT settlement
    30/07/2017 [TKG] tambah tax, ubah journal berdasarkan tax.
    22/08/2017 [TKG] menggunakan tax group
    03/09/2017 [TKG] tambah informasi outgoing payment dan date, incoming payment# dan date
    03/09/2017 [TKG] vr tax hanya yg sudah divoucherkan
    12/11/2017 [TKG] VR Tax bisa in atau out berdasarkan debit/creditnya.
    28/11/2017 [TKG] perubahan perhitungan vr tax
    13/05/2019 [TKG] tambah PI raw material
    07/06/2022 [DITA/IOK] save taxcode PI untuk keperluan jika ada update tax di PI 
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVATSettlement : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mPurchaseInvoiceRawMaterialTaxGrpCode = string.Empty,
            mMainCurCode = string.Empty;
        private bool mIsAutoJournalActived = false;
        internal FrmVATSettlementFind FrmFind;

        #endregion

        #region Constructor

        public FrmVATSettlement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "VAT Settlement";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueDocType(ref LueDocType, string.Empty);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueTaxGrpCode(ref LueTaxGrpCode);
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Type",
                        "Type",
                        "Document#",
                        "Date",
                        
                        //6-10
                        "Description",
                        "Tax Invoice#",
                        "Tax Invoice Date",
                        "Amount",
                        "TaxCode"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 0, 180, 130, 80,
                        
                        //6-10
                        400, 130, 120, 120, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 10 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueDocType, LueTaxGrpCode, 
                        LueCurCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDocType, LueTaxGrpCode, LueCurCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, LueDocType, 
                LueTaxGrpCode, LueCurCode, MeeRemark, TxtJournalDocNo, TxtJournalDocNo2
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd1();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVATSettlementFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueDocType, "Document type") && !Sm.IsLueEmpty(LueCurCode, "Currency") && !Sm.IsLueEmpty(LueTaxGrpCode, "Tax") && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) 
                    Sm.FormShowDialog(new FrmVATSettlementDlg(this, Sm.GetLue(LueDocType), Sm.GetLue(LueCurCode)));
            }            
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueDocType, "Document type") && !Sm.IsLueEmpty(LueCurCode, "Currency") && !Sm.IsLueEmpty(LueTaxGrpCode, "Tax") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmVATSettlementDlg(this, Sm.GetLue(LueDocType), Sm.GetLue(LueCurCode)));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VATSettlement", "TblVATSettlementHdr");
            
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveVATSettlementHdr(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0) 
                    cml.Add(SaveVATSettlementDtl(DocNo, r));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueDocType, "Document type") ||
                Sm.IsLueEmpty(LueTaxGrpCode, "Tax group") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var SQL = new StringBuilder();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Document# is empty.")) return true;
                if (Sm.GetLue(LueDocType) == "I")
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2) == "1")
                    {
                        SQL.Length = 0;
                        SQL.Capacity = 0;

                        SQL.AppendLine("Select 1 ");
                        SQL.AppendLine("From TblPurchaseInvoiceHdr ");
                        SQL.AppendLine("Where CancelInd='N' ");
                        SQL.AppendLine("And DocNo=@Param1 ");
                        SQL.AppendLine("And ( ");
                        SQL.AppendLine("   (IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And (VoucherRequestPPNDocNo Is Not Null Or VATSettlementDocNo Is Not Null)) Or ");
                        SQL.AppendLine("   (IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And (VoucherRequestPPNDocNo2 Is Not Null Or VATSettlementDocNo2 Is Not Null)) Or ");
                        SQL.AppendLine("   (IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And (VoucherRequestPPNDocNo3 Is Not Null Or VATSettlementDocNo3 Is Not Null)) ");
                        SQL.AppendLine(");");

                        if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd1, Row, 4), Sm.GetLue(LueTaxGrpCode), string.Empty))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Type : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                                "Document# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine +
                                "This document already processed to voucher request VAT/VAT settlement."
                                );
                            return true;
                        }
                    }
                    if (Sm.GetGrdStr(Grd1, Row, 2) == "2")
                    {
                        SQL.Length = 0;
                        SQL.Capacity = 0;

                        SQL.AppendLine("Select 1 ");
                        SQL.AppendLine("From TblPurchaseReturnInvoiceHdr ");
                        SQL.AppendLine("Where CancelInd='N' ");
                        SQL.AppendLine("And DocNo=@Param1 ");
                        SQL.AppendLine("And ( ");
                        SQL.AppendLine("   (IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And (VoucherRequestPPNDocNo Is Not Null Or VATSettlementDocNo Is Not Null)) Or ");
                        SQL.AppendLine("   (IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And (VoucherRequestPPNDocNo2 Is Not Null Or VATSettlementDocNo2 Is Not Null)) Or ");
                        SQL.AppendLine("   (IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And (VoucherRequestPPNDocNo3 Is Not Null Or VATSettlementDocNo3 Is Not Null)) ");
                        SQL.AppendLine(");");

                        if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd1, Row, 4), Sm.GetLue(LueTaxGrpCode), string.Empty))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Type : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                                "Document# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine +
                                "This document already processed to voucher request VAT/VAT settlement."
                                );
                            return true;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 2) == "3")
                    {
                        SQL.Length = 0;
                        SQL.Capacity = 0;

                        SQL.AppendLine("Select 1 ");
                        SQL.AppendLine("From TblVoucherRequestTax T ");
                        SQL.AppendLine("Where T.CancelInd='N' ");
                        SQL.AppendLine("And T.DocNo=@Param ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 ");
                        SQL.AppendLine("    From TblVoucherHdr ");
                        SQL.AppendLine("    Where DocType='21' ");
                        SQL.AppendLine("    And CancelInd='N' ");
                        SQL.AppendLine("    And VoucherRequestDocNo=T.VoucherRequestDocNo ");
                        SQL.AppendLine(");");
                        if (!Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd1, Row, 4)))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Type : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                                "Document# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine +
                                "This document haven't processed to voucher."
                                );
                            return true;
                        }    

                        SQL.Length = 0;
                        SQL.Capacity = 0;

                        SQL.AppendLine("Select 1 ");
                        SQL.AppendLine("From TblVoucherRequestTax ");
                        SQL.AppendLine("Where CancelInd='N' ");
                        SQL.AppendLine("And DocNo=@Param1 ");
                        SQL.AppendLine("And TaxCode In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) ");
                        SQL.AppendLine("And (VoucherRequestPPNDocNo Is Not Null Or VATSettlementDocNo Is Not Null); ");
                        
                        if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd1, Row, 4), Sm.GetLue(LueTaxGrpCode), string.Empty))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Type : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                                "Document# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine +
                                "This document already processed to voucher request VAT/VAT settlement."
                                );
                            return true;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 2) == "7")
                    {
                        SQL.Length = 0;
                        SQL.Capacity = 0;

                        SQL.AppendLine("Select 1 ");
                        SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr T ");
                        SQL.AppendLine("Where T.CancelInd='N' ");
                        SQL.AppendLine("And T.TaxInd='Y' ");
                        SQL.AppendLine("And T.DocNo=@Param ");
                        SQL.AppendLine(";");
                        if (!Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd1, Row, 4)))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Type : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                                "Document# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine +
                                "This document not ready to processed."
                                );
                            return true;
                        }

                        SQL.Length = 0;
                        SQL.Capacity = 0;

                        SQL.AppendLine("Select 1 ");
                        SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr ");
                        SQL.AppendLine("Where CancelInd='N' ");
                        SQL.AppendLine("And TaxInd='Y' ");
                        SQL.AppendLine("And DocNo=@Param ");
                        SQL.AppendLine("And (VoucherRequestPPNDocNo Is Not Null Or VATSettlementDocNo Is Not Null); ");

                        if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd1, Row, 4)))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Type : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                                "Document# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine +
                                "This document already processed to voucher request VAT/VAT settlement."
                                );
                            return true;
                        }
                    }

                }
                if (Sm.GetLue(LueDocType)=="O")
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2) == "4")
                    {
                        if (Sm.IsDataExist(
                            "Select 1 From TblSalesInvoiceHdr " +
                            "Where CancelInd='N' " +
                            "And DocNo=@Param " +
                            "And (VoucherRequestPPNDocNo Is Not Null Or VATSettlementDocNo Is Not Null);",
                            Sm.GetGrdStr(Grd1, Row, 4)))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Type : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                                "Document# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine +
                                "This document already processed to voucher request VAT/VAT settlement."
                                );
                            return true;
                        }
                    }
                    if (Sm.GetGrdStr(Grd1, Row, 2) == "5")
                    {
                        if (Sm.IsDataExist(
                            "Select 1 From TblSalesReturnInvoiceHdr " +
                            "Where CancelInd='N' " +
                            "And DocNo=@Param " +
                            "And (VoucherRequestPPNDocNo Is Not Null Or VATSettlementDocNo Is Not Null);",
                            Sm.GetGrdStr(Grd1, Row, 4)))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Type : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                                "Document# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine +
                                "This document already processed to voucher request VAT/VAT settlement."
                                );
                            return true;
                        }
                    }
                    if (Sm.GetGrdStr(Grd1, Row, 2) == "6")
                    {
                        if (Sm.IsDataExist(
                            "Select 1 From TblPOSVat " +
                            "Where CancelInd='N' " +
                            "And DocNo=@Param " +
                            "And (VoucherRequestPPNDocNo Is Not Null Or VATSettlementDocNo Is Not Null);",
                            Sm.GetGrdStr(Grd1, Row, 4)))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Type : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                                "Document# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine +
                                "This document already processed to voucher request VAT/VAT settlement."
                                );
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveVATSettlementHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVATSettlementHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, DocType, TaxGrpCode, CurCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', @DocType, @TaxGrpCode, @CurCode, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParam<String>(ref cm, "@TaxGrpCode", Sm.GetLue(LueTaxGrpCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVATSettlementDtl(string DocNo, int r)
        {
            var SQL = new StringBuilder();
            var DocType = Sm.GetGrdStr(Grd1, r, 2);


            SQL.AppendLine("Insert Into TblVATSettlementDtl ");
            SQL.AppendLine("(DocNo, DNo, DocType, DocNoInOut, Amt, TaxInvoiceNo, TaxInvoiceDt, TaxCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values");
            SQL.AppendLine("(@DocNo, @DNo, @DocType, @DocNoInOut, @Amt, @TaxInvoiceNo, @TaxInvoiceDt, @TaxCode, @Remark, @CreateBy, CurrentDateTime()); ");

            if (DocType == "1")
            {
                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
                SQL.AppendLine("    VATSettlementDocNo=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And VATSettlementDocNo Is Null; ");

                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
                SQL.AppendLine("    VATSettlementDocNo2=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNDocNo2 Is Null ");
                SQL.AppendLine("And VATSettlementDocNo2 Is Null; ");

                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
                SQL.AppendLine("    VATSettlementDocNo3=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNDocNo3 Is Null ");
                SQL.AppendLine("And VATSettlementDocNo3 Is Null; ");
            }

            if (DocType == "2")
            {
                SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
                SQL.AppendLine("    VATSettlementDocNo=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And VATSettlementDocNo Is Null; ");

                SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
                SQL.AppendLine("    VATSettlementDocNo2=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNDocNo2 Is Null ");
                SQL.AppendLine("And VATSettlementDocNo2 Is Null; ");

                SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
                SQL.AppendLine("    VATSettlementDocNo3=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNDocNo3 Is Null ");
                SQL.AppendLine("And VATSettlementDocNo3 Is Null; ");
            }

            if (DocType == "3")
            {
                SQL.AppendLine("Update TblVoucherRequestTax Set ");
                SQL.AppendLine("    VATSettlementDocNo=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And IfNull(TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And VATSettlementDocNo Is Null; ");
            }

            if (DocType == "7")
            {
                SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set ");
                SQL.AppendLine("    VATSettlementDocNo=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And TaxInd='Y' ");
                SQL.AppendLine("And VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And VATSettlementDocNo Is Null; ");
            }


            if (DocType == "4")
            {
                SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
                SQL.AppendLine("    VATSettlementDocNo=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And VATSettlementDocNo Is Null; ");
            }

            if (DocType == "5")
            {
                SQL.AppendLine("Update TblSalesReturnInvoiceHdr Set ");
                SQL.AppendLine("    VATSettlementDocNo=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And VATSettlementDocNo Is Null; ");
            }

            if (DocType == "6")
            {
                SQL.AppendLine("Update TblPOSVat Set ");
                SQL.AppendLine("    VATSettlementDocNo=@DocNo ");
                SQL.AppendLine("Where DocNo=@DocNoInOut ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VoucherRequestPPNDocNo Is Null ");
                SQL.AppendLine("And VATSettlementDocNo Is Null; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (r + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@DocNoInOut", Sm.GetGrdStr(Grd1, r, 4));
            Sm.CmParam<String>(ref cm, "@TaxGrpCode", Sm.GetLue(LueTaxGrpCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, r, 9));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, r, 6));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", Sm.GetGrdStr(Grd1, r, 7));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetGrdDate(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetGrdStr(Grd1, r, 10));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var CurCode = Sm.GetLue(LueCurCode);
            var DocType = Sm.GetLue(LueDocType);

            SQL.AppendLine("Update TblVATSettlementHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, Concat('VAT Settlement : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblVATSettlementHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAmt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, DAmt, CAmt From (");

            if (DocType == "I")
            {
                SQL.AppendLine("Select C.ParValue As AcNo, ");
                if (Sm.CompareStr(mMainCurCode, CurCode))
                    SQL.AppendLine("A.Amt As DAmt, 0.00 As CAmt ");
                else
                    SQL.AppendLine("A.Amt*IfNull(B.Amt, 0.00) As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("From TblVATSettlementHdr A ");
                if (!Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine(") B On 0=0 ");
                }
                SQL.AppendLine("Inner Join TblParameter C On C.ParCode='AcNoForFinancialCosts' And C.ParValue Is Not Null ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select C.AcNo1 As AcNo, ");
                if (Sm.CompareStr(mMainCurCode, CurCode))
                    SQL.AppendLine("0.00 As DAmt, A.Amt As CAmt ");
                else
                    SQL.AppendLine("0.00 As DAmt, A.Amt*IfNull(B.Amt, 0.00) As CAmt ");
                SQL.AppendLine("From TblVATSettlementHdr A ");
                if (!Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine(") B On 0=0 ");
                }
                SQL.AppendLine("Inner Join TblTaxGrp C On A.TaxGrpCode=C.TaxGrpCode And C.AcNo1 Is Not Null ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
            }

            if (DocType == "O")
            {
                SQL.AppendLine("Select C.AcNo2 As AcNo, ");
                if (Sm.CompareStr(mMainCurCode, CurCode))
                    SQL.AppendLine("A.Amt As DAmt, 0.00 As CAmt ");
                else
                    SQL.AppendLine("A.Amt*IfNull(B.Amt, 0.00) As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("From TblVATSettlementHdr A ");
                if (!Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine(") B On 0=0 ");
                }
                SQL.AppendLine("Inner Join TblTaxGrp C On A.TaxGrpCode=C.TaxGrpCode And C.AcNo2 Is Not Null ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select C.ParValue As AcNo, ");
                if (Sm.CompareStr(mMainCurCode, CurCode))
                    SQL.AppendLine("0.00 As DAmt, A.Amt As CAmt ");
                else
                    SQL.AppendLine("0.00 As DAmt, A.Amt*IfNull(B.Amt, 0.00) As CAmt ");
                SQL.AppendLine("From TblVATSettlementHdr A ");
                if (!Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine(") B On 0=0 ");
                }
                SQL.AppendLine("Inner Join TblParameter C On C.ParCode='AcNoForFinancialCosts' And C.ParValue Is Not Null ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", CurCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditVATSettlementHdr());

            for (int r=0; r<Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0) 
                    cml.Add(EditVATSettlementDtl(r));

            if (mIsAutoJournalActived) cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(Sm.ServerCurrentDate()) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblVATSettlementHdr " +
                "Where CancelInd='Y' And DocNo=@Param;", 
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditVATSettlementHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVATSettlementHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditVATSettlementDtl(int r)
        {
            var SQL = new StringBuilder();
            var DocType = Sm.GetGrdStr(Grd1, r, 2);

            if (DocType=="1")
            {
                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
                SQL.AppendLine("   VATSettlementDocNo = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VATSettlementDocNo Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo=@VATSettlementDocNo; ");

                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
                SQL.AppendLine("   VATSettlementDocNo2 = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VATSettlementDocNo2 Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo2=@VATSettlementDocNo; ");

                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
                SQL.AppendLine("   VATSettlementDocNo3 = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VATSettlementDocNo3 Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo3=@VATSettlementDocNo; ");
            }

            if (DocType == "2")
            {
                SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
                SQL.AppendLine("   VATSettlementDocNo = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VATSettlementDocNo Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo=@VATSettlementDocNo; ");

                SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
                SQL.AppendLine("   VATSettlementDocNo2 = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VATSettlementDocNo2 Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo2=@VATSettlementDocNo; ");

                SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
                SQL.AppendLine("   VATSettlementDocNo3 = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VATSettlementDocNo3 Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo3=@VATSettlementDocNo; ");
            }

            if (DocType == "3")
            {
                SQL.AppendLine("Update TblVoucherRequestTax Set ");
                SQL.AppendLine("   VATSettlementDocNo = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VATSettlementDocNo Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo=@VATSettlementDocNo; ");
            }

            if (DocType == "7")
            {
                SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set ");
                SQL.AppendLine("   VATSettlementDocNo = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And TaxInd='Y' ");
                SQL.AppendLine("And VATSettlementDocNo Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo=@VATSettlementDocNo; ");
            }

            if (DocType == "4")
            {
                SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
                SQL.AppendLine("   VATSettlementDocNo = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VATSettlementDocNo Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo=@VATSettlementDocNo; ");
            }
            if (DocType == "5")
            {
                SQL.AppendLine("Update TblSalesReturnInvoiceHdr Set ");
                SQL.AppendLine("   VATSettlementDocNo = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VATSettlementDocNo Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo=@VATSettlementDocNo; ");
            }
            if (DocType == "6")
            {
                SQL.AppendLine("Update TblPOSVAT Set ");
                SQL.AppendLine("   VATSettlementDocNo = Null ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VATSettlementDocNo Is Not Null ");
                SQL.AppendLine("And VATSettlementDocNo=@VATSettlementDocNo; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@VATSettlementDocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 4));

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVATSettlementHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Replace(CurDate(), '-', ''), Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblVATSettlementHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblVATSettlementHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowVATSettlementHdr(DocNo);
                ShowVATSettlementDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVATSettlementHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, DocDt, CancelInd, CancelReason, ");
            SQL.AppendLine("DocType, TaxGrpCode, CurCode, Amt, Remark, JournalDocNo, JournalDocNo2 ");
            SQL.AppendLine("From TblVAtSettlementHdr Where DocNo=@DocNo;");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "DocType", "TaxGrpCode", 
                    
                    //6-10
                    "CurCode", "Amt", "Remark", "JournalDocNo", "JournalDocNo2"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[3])=="Y";
                     Sm.SetLue(LueDocType, Sm.DrStr(dr, c[4]));
                     Sm.SetLue(LueTaxGrpCode, Sm.DrStr(dr, c[5]));
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[6]));
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                     TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[9]);
                     TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[10]);
                 }, true
             );
        }

        private void ShowVATSettlementDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DocType, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then 'Purchase Invoice' ");
            SQL.AppendLine("    When '2' Then 'Purchase Return Invoice' ");
            SQL.AppendLine("    When '3' Then 'Voucher Request Tax' ");
            SQL.AppendLine("    When '4' Then 'Sales Invoice' ");
            SQL.AppendLine("    When '5' Then 'Sales Return Invoice' ");
            SQL.AppendLine("    When '6' Then 'POS VAT' ");
            SQL.AppendLine("    When '7' Then 'Purchase Invoice Raw Material' ");
            SQL.AppendLine("End As DocTypeDesc, ");
            SQL.AppendLine("A.DocNoInOut, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then B.DocDt ");
            SQL.AppendLine("    When '2' Then C.DocDt ");
            SQL.AppendLine("    When '3' Then D.DocDt ");
            SQL.AppendLine("    When '4' Then E.DocDt ");
            SQL.AppendLine("    When '5' Then F.DocDt ");
            SQL.AppendLine("    When '6' Then G.DocDt ");
            SQL.AppendLine("    When '7' Then H.DocDt ");
            SQL.AppendLine("End As DocDt, ");
            SQL.AppendLine("A.Amt, A.TaxInvoiceNo, A.TaxInvoiceDt, A.Remark ");
            SQL.AppendLine("From TblVATSettlementDtl A ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceHdr B On A.DocNoInOut=B.DocNo ");
            SQL.AppendLine("Left Join TblPurchaseReturnInvoiceHdr C On A.DocNoInOut=C.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestTax D On A.DocNoInOut=D.DocNo ");
            SQL.AppendLine("Left Join TblSalesInvoiceHdr E On A.DocNoInOut=E.DocNo ");
            SQL.AppendLine("Left Join TblSalesReturnInvoiceHdr F On A.DocNoInOut=F.DocNo ");
            SQL.AppendLine("Left Join TblPOSVAT G On A.DocNoInOut=G.DocNo ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr H On A.DocNoInOut=H.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",
                        
                        //1-5
                        "DocType", "DocTypeDesc", "DocNoInOut", "DocDt", "Remark", 
                        
                        //6-8
                        "TaxInvoiceNo", "TaxInvoiceDt", "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MaincurCode");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mPurchaseInvoiceRawMaterialTaxGrpCode = Sm.GetParameter("PurchaseInvoiceRawMaterialTaxGrpCode");
        }

        private void SetLueDocType(ref DXE.LookUpEdit Lue, string DocType)
        {
            try
            {
                var cm = new MySqlCommand() 
                { 
                    CommandText = 
                        "Select 'I' As Col1, 'In' As Col2 " +
                        "Union All Select 'O' As Col1, 'Out' As Col2;"
                };
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m;
            for (int r=0;r<Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 9).Length>0)
                    Amt+=Sm.GetGrdDec(Grd1, r, 9); 
            }
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDocType, new StdMtd.RefreshLue2(SetLueDocType), string.Empty);
                ClearGrd1();
                TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new StdMtd.RefreshLue1(Sl.SetLueCurCode));
                ClearGrd1();
                TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            }
        }

        private void LueTaxGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd1, true);
                Sm.RefreshLookUpEdit(LueTaxGrpCode, new Sm.RefreshLue1(Sl.SetLueTaxGrpCode));
            }
        }

        #endregion

        #endregion
    }
}
