﻿#region Update
/*
     03/06/2020 [VIN/TWC] tambah Lue cost center group
*/
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGradeLevelGroup : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmGradeLevelGroupFind FrmFind;

        #endregion

        #region Constructor

        public FrmGradeLevelGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueCCGrpCode, "CostCenterGroup");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
          //  if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtGrdLvlGrpCode, TxtGrdLvlGrpName, LueCCGrpCode
                    }, true);
                    TxtGrdLvlGrpCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtGrdLvlGrpCode, TxtGrdLvlGrpName, LueCCGrpCode
                    }, false);
                    TxtGrdLvlGrpCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtGrdLvlGrpCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtGrdLvlGrpName, LueCCGrpCode
                    }, false);
                    TxtGrdLvlGrpName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtGrdLvlGrpCode, TxtGrdLvlGrpName, LueCCGrpCode
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGradeLevelGroupFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtGrdLvlGrpCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtGrdLvlGrpCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblGradeLevelGroup Where GrdLvlGrpCode=@GrdLvlGrpCode" };
                Sm.CmParam<String>(ref cm, "@GrdLvlGrpCode", TxtGrdLvlGrpCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblGradeLevelGroup(GrdLvlGrpCode, GrdLvlGrpName, CCGrpCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@GrdLvlGrpCode, @GrdLvlGrpName, @CCGrpCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update GrdLvlGrpName=@GrdLvlGrpName, CCGrpCode=@CCGrpCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@GrdLvlGrpCode", TxtGrdLvlGrpCode.Text);
                Sm.CmParam<String>(ref cm, "@GrdLvlGrpName", TxtGrdLvlGrpName.Text);
                Sm.CmParam<String>(ref cm, "@CCGrpCode", Sm.GetLue(LueCCGrpCode));

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtGrdLvlGrpCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CurCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@GrdLvlGrpCode", CurCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select GrdLvlGrpCode,GrdLvlGrpName, CCGrpCode From TblGradeLevelGroup Where GrdLvlGrpCode=@GrdLvlGrpCode ",
                        new string[] { "GrdLvlGrpCode", "GrdLvlGrpName", "CCGrpCode" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtGrdLvlGrpCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtGrdLvlGrpName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueCCGrpCode, Sm.DrStr(dr, c[2]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtGrdLvlGrpCode, "Grade level group code", false) ||
                Sm.IsTxtEmpty(TxtGrdLvlGrpName, "Grade level group name", false) ||
                IsCurCodeExisted();
        }

        private bool IsCurCodeExisted()
        {
            if (!TxtGrdLvlGrpCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select 1 From TblGradeLevelGroup Where GrdLvlGrpCode=@Param Limit 1;", TxtGrdLvlGrpCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Grade Level Group code ( " + TxtGrdLvlGrpCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtGrdLvlCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGrdLvlGrpCode);
        }

        private void TxtGrdLvlName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGrdLvlGrpName);
        }

        private void LueCCGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCCGrpCode, new Sm.RefreshLue2(Sl.SetLueOption), "CostCenterGroup");
        }        

        #endregion

        #endregion
    }
}
