﻿#region Update
/*
    07/05/2021 [WED/PHT] new apps
    18/06/2021 [TKG/PHT] 
        profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
        cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetTransferCostCenterFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmBudgetTransferCostCenter mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBudgetTransferCostCenterFind(FrmBudgetTransferCostCenter FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueYr(LueYr, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("    Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc, ");
            SQL.AppendLine("    A.CancelInd, A.Yr, A.Mth, A.Mth2, A.ProfitCenterCode, B.ProfitCenterName, A.ProfitCenterCode2, ");
            SQL.AppendLine("    C.ProfitCenterName As ProfitCenterName2, A.CCCode, D.CCName, A.CCCode2, E.CCName As CCName2, ");
            SQL.AppendLine("    A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    From TblBudgetTransferCostCenterHdr A ");
            SQL.AppendLine("    Inner Join TblProfitCenter B On A.ProfitCenterCode = B.ProfitCenterCode ");
            SQL.AppendLine("    Inner Join TblProfitCenter C On A.ProfitCenterCode2 = C.ProfitCenterCode ");
            SQL.AppendLine("    Inner Join TblCostCenter D On A.CCCode = D.CCCode ");
            SQL.AppendLine("    Inner Join TblCostCenter E On A.CCCode2 = E.CCCode ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.ProfitCenterCode In (");
            SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Status",
                    "Cancel",
                    "Year",
                    
                    //6-10
                    "From Month",
                    "To Month",
                    "Profit Center Code", 
                    "From"+Environment.NewLine+"Profit Center",
                    "Profit Center Code",
                    
                    //11-15
                    "To"+Environment.NewLine+"Profit Center",
                    "Cost Center Code",
                    "From"+Environment.NewLine+"Cost Center",
                    "Cost Center Code",
                    "To"+Environment.NewLine+"Cost Center",
                    
                    //16-20
                    "Remark",
                    "Created By",
                    "Created Date", 
                    "Created Time", 
                    "Last Updated By",

                    //21-22
                    "Last Updated Date",
                    "Last Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 80, 80, 60, 60, 
                    
                    //6-10
                    80, 80, 100, 180, 100, 
                    
                    //11-15
                    180, 100, 180, 100, 180, 

                    //16-20
                    200, 100, 100, 100, 100, 

                    //21-22
                    100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 10, 12, 14, 17, 18, 19, 20, 21, 22 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 10, 12, 14, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProfitCenterCode.Text, new string[] { "T.ProfitCenterCode", "T.ProfitCenterName" });
                Sm.FilterStr(ref Filter, ref cm, TxtProfitCenterCode2.Text, new string[] { "T.ProfitCenterCode2", "T.ProfitCenterName2" });
                Sm.FilterStr(ref Filter, ref cm, TxtCCCode.Text, new string[] { "T.CCCode", "T.CCName" });
                Sm.FilterStr(ref Filter, ref cm, TxtCCCode2.Text, new string[] { "T.CCCode2", "T.CCName2" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.CreateDt Desc; ",
                    new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "StatusDesc", "CancelInd", "Yr", "Mth", 
                        
                        //6-10
                        "Mth2", "ProfitCenterCode", "ProfitCenterName", "ProfitCenterCode2", "ProfitCenterName2", 
                        
                        //11-15
                        "CCCode", "CCName",  "CCCode2", "CCName2", "Remark", 
                        
                        //16-19
                        "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 19);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtProfitCenterCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "From Profit Center");
        }

        private void TxtProfitCenterCode2_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "To Profit Center");
        }

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "From Cost Center");
        }

        private void TxtCCCode2_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "To Cost Center");
        }

        #endregion

        #endregion
    }
}
