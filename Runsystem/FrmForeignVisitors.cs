﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmForeignVisitors : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmForeignVisitorsFind FrmFind;

        #endregion

        #region Constructor

        public FrmForeignVisitors(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length <= 0) this.Text = Sm.GetMenuDesc("FrmForeignVisitors");
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtForeignVisitors, TxtTotalForeignVisitors
                    }, true);
                    TxtDocNo.Focus();
                    ChkActiveInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtForeignVisitors, TxtTotalForeignVisitors
                    }, false);
                    TxtForeignVisitors.Focus();
                    ChkActiveInd.Checked = true;
                    break;
                case mState.Edit:
                    if (ChkActiveInd.Checked)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            TxtForeignVisitors, TxtTotalForeignVisitors
                        }, false);
                    }
                    TxtForeignVisitors.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtForeignVisitors, TxtTotalForeignVisitors
            }, 0);
            ChkActiveInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmForeignVisitorsFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;
            if(ChkActiveInd.Checked)
                SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                var cml = new List<MySqlCommand>();

                string DocNo = string.Empty;

                if (TxtDocNo.Text.Length > 0)
                    DocNo = TxtDocNo.Text;
                else
                    DocNo = Sm.GenerateDocNo(Sm.Left(Sm.ServerCurrentDateTime(), 8), "ForeignVisitors", "TblForeignVisitors");

                SQL.AppendLine("Insert Into TblForeignVisitors(DocNo, ActInd, ForeignVisitors, TotalForeignVisitors, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @ActInd, @ForeignVisitors, @TotalForeignVisitors, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ForeignVisitors=@ForeignVisitors, TotalForeignVisitors=@TotalForeignVisitors, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                SQL.AppendLine("Update TblForeignVisitors T1 ");
                SQL.AppendLine("    Set T1.ActInd = 'N' ");
                SQL.AppendLine("Where T1.DocNo <> @DocNo ");
                SQL.AppendLine("And Exists (Select T2.DocNo From (Select * From TblForeignVisitors Where DocNo = @DocNo And ActInd = 'Y') T2 ); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@ActInd", (ChkActiveInd.Checked) ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@ForeignVisitors", decimal.Parse(TxtForeignVisitors.Text));
                Sm.CmParam<Decimal>(ref cm, "@TotalForeignVisitors", decimal.Parse(TxtTotalForeignVisitors.Text));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                cml.Add(cm);
                Sm.ExecCommands(cml);

                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select * From TblForeignVisitors Where DocNo = @DocNo; ");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            "DocNo", "ActInd", "ForeignVisitors", "TotalForeignVisitors"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            ChkActiveInd.Checked = Sm.DrStr(dr, c[1]) == "Y";
                            TxtForeignVisitors.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                            TxtTotalForeignVisitors.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtForeignVisitors, "Foreign Visitors", true) ||
                Sm.IsTxtEmpty(TxtTotalForeignVisitors, "Total Foreign Visitors", true);
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control events

        private void TxtForeignVisitors_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtForeignVisitors, 0);
        }

        private void TxtTotalForeignVisitors_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalForeignVisitors, 0);
        }

        #endregion

        #endregion

    }
}
