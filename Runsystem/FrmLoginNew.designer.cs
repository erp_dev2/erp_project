﻿namespace RunSystem
{
    partial class FrmLoginNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnSetting = new System.Windows.Forms.Button();
            this.PnlSetting = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.TxtServer = new System.Windows.Forms.TextBox();
            this.LblServer = new System.Windows.Forms.Label();
            this.LblDb = new System.Windows.Forms.Label();
            this.TxtDatabase = new System.Windows.Forms.TextBox();
            this.LblVersionNo = new System.Windows.Forms.Label();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.LblCompanyName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtPwd = new System.Windows.Forms.TextBox();
            this.TxtUserCode = new System.Windows.Forms.TextBox();
            this.ChkKeepUserCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.PnlSetting.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkKeepUserCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.BtnSetting);
            this.panel1.Controls.Add(this.PnlSetting);
            this.panel1.Controls.Add(this.LblVersionNo);
            this.panel1.Controls.Add(this.BtnLogin);
            this.panel1.Controls.Add(this.LblCompanyName);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 76);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel1.Size = new System.Drawing.Size(1018, 409);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::RunSystem.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(710, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(265, 78);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            // 
            // BtnSetting
            // 
            this.BtnSetting.BackColor = System.Drawing.Color.Transparent;
            this.BtnSetting.BackgroundImage = global::RunSystem.Properties.Resources.Setting;
            this.BtnSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtnSetting.FlatAppearance.BorderSize = 0;
            this.BtnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSetting.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSetting.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnSetting.Location = new System.Drawing.Point(976, 371);
            this.BtnSetting.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnSetting.Name = "BtnSetting";
            this.BtnSetting.Size = new System.Drawing.Size(27, 28);
            this.BtnSetting.TabIndex = 21;
            this.BtnSetting.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnSetting.UseVisualStyleBackColor = false;
            this.BtnSetting.Click += new System.EventHandler(this.BtnSetting_Click);
            // 
            // PnlSetting
            // 
            this.PnlSetting.Controls.Add(this.panel9);
            this.PnlSetting.Controls.Add(this.panel8);
            this.PnlSetting.Controls.Add(this.TxtServer);
            this.PnlSetting.Controls.Add(this.LblServer);
            this.PnlSetting.Controls.Add(this.LblDb);
            this.PnlSetting.Controls.Add(this.TxtDatabase);
            this.PnlSetting.Location = new System.Drawing.Point(683, 263);
            this.PnlSetting.Name = "PnlSetting";
            this.PnlSetting.Size = new System.Drawing.Size(302, 78);
            this.PnlSetting.TabIndex = 22;
            this.PnlSetting.Visible = false;
            // 
            // panel9
            // 
            this.panel9.BackgroundImage = global::RunSystem.Properties.Resources.garis1;
            this.panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel9.Location = new System.Drawing.Point(83, 60);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(217, 12);
            this.panel9.TabIndex = 10;
            // 
            // panel8
            // 
            this.panel8.BackgroundImage = global::RunSystem.Properties.Resources.garis1;
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel8.Location = new System.Drawing.Point(83, 35);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(217, 12);
            this.panel8.TabIndex = 9;
            // 
            // TxtServer
            // 
            this.TxtServer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtServer.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtServer.Location = new System.Drawing.Point(95, 47);
            this.TxtServer.Name = "TxtServer";
            this.TxtServer.Size = new System.Drawing.Size(166, 15);
            this.TxtServer.TabIndex = 8;
            this.TxtServer.Validated += new System.EventHandler(this.TxtServer_Validated);
            this.TxtServer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtServer_KeyDown);
            // 
            // LblServer
            // 
            this.LblServer.AutoSize = true;
            this.LblServer.BackColor = System.Drawing.Color.Transparent;
            this.LblServer.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblServer.ForeColor = System.Drawing.Color.DodgerBlue;
            this.LblServer.Location = new System.Drawing.Point(26, 48);
            this.LblServer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblServer.Name = "LblServer";
            this.LblServer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblServer.Size = new System.Drawing.Size(52, 16);
            this.LblServer.TabIndex = 7;
            this.LblServer.Text = "Server";
            this.LblServer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblDb
            // 
            this.LblDb.AutoSize = true;
            this.LblDb.BackColor = System.Drawing.Color.Transparent;
            this.LblDb.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDb.ForeColor = System.Drawing.Color.DodgerBlue;
            this.LblDb.Location = new System.Drawing.Point(8, 18);
            this.LblDb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblDb.Name = "LblDb";
            this.LblDb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblDb.Size = new System.Drawing.Size(70, 16);
            this.LblDb.TabIndex = 5;
            this.LblDb.Text = "Database";
            this.LblDb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDatabase
            // 
            this.TxtDatabase.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtDatabase.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDatabase.Location = new System.Drawing.Point(98, 21);
            this.TxtDatabase.Name = "TxtDatabase";
            this.TxtDatabase.Size = new System.Drawing.Size(201, 15);
            this.TxtDatabase.TabIndex = 6;
            this.TxtDatabase.Validated += new System.EventHandler(this.TxtDatabase_Validated);
            this.TxtDatabase.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtDatabase_KeyDown);
            // 
            // LblVersionNo
            // 
            this.LblVersionNo.AutoSize = true;
            this.LblVersionNo.BackColor = System.Drawing.Color.Transparent;
            this.LblVersionNo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVersionNo.ForeColor = System.Drawing.Color.SteelBlue;
            this.LblVersionNo.Location = new System.Drawing.Point(686, 377);
            this.LblVersionNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblVersionNo.Name = "LblVersionNo";
            this.LblVersionNo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblVersionNo.Size = new System.Drawing.Size(97, 16);
            this.LblVersionNo.TabIndex = 20;
            this.LblVersionNo.Text = "Version 9.999";
            this.LblVersionNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnLogin
            // 
            this.BtnLogin.BackColor = System.Drawing.Color.DodgerBlue;
            this.BtnLogin.FlatAppearance.BorderSize = 0;
            this.BtnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnLogin.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLogin.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnLogin.Location = new System.Drawing.Point(803, 224);
            this.BtnLogin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(95, 31);
            this.BtnLogin.TabIndex = 3;
            this.BtnLogin.Text = "&Log in";
            this.BtnLogin.UseVisualStyleBackColor = false;
            this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // LblCompanyName
            // 
            this.LblCompanyName.AutoSize = true;
            this.LblCompanyName.BackColor = System.Drawing.Color.Transparent;
            this.LblCompanyName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCompanyName.ForeColor = System.Drawing.Color.SteelBlue;
            this.LblCompanyName.Location = new System.Drawing.Point(684, 360);
            this.LblCompanyName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblCompanyName.Name = "LblCompanyName";
            this.LblCompanyName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblCompanyName.Size = new System.Drawing.Size(191, 16);
            this.LblCompanyName.TabIndex = 19;
            this.LblCompanyName.Text = "RUN Sytem Sample Company";
            this.LblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.TxtPwd);
            this.panel2.Controls.Add(this.TxtUserCode);
            this.panel2.Controls.Add(this.ChkKeepUserCode);
            this.panel2.Location = new System.Drawing.Point(690, 104);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(313, 120);
            this.panel2.TabIndex = 29;
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::RunSystem.Properties.Resources.pwd;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Location = new System.Drawing.Point(17, 54);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(16, 22);
            this.panel5.TabIndex = 3;
            // 
            // panel7
            // 
            this.panel7.BackgroundImage = global::RunSystem.Properties.Resources.garis1;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel7.Location = new System.Drawing.Point(36, 73);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(259, 12);
            this.panel7.TabIndex = 6;
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::RunSystem.Properties.Resources.usercode;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Location = new System.Drawing.Point(14, 20);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(19, 22);
            this.panel4.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = global::RunSystem.Properties.Resources.garis1;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Location = new System.Drawing.Point(36, 37);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(259, 12);
            this.panel6.TabIndex = 5;
            // 
            // TxtPwd
            // 
            this.TxtPwd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtPwd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPwd.Location = new System.Drawing.Point(55, 59);
            this.TxtPwd.Name = "TxtPwd";
            this.TxtPwd.PasswordChar = '*';
            this.TxtPwd.Size = new System.Drawing.Size(229, 15);
            this.TxtPwd.TabIndex = 2;
            this.TxtPwd.UseSystemPasswordChar = true;
            this.TxtPwd.Validated += new System.EventHandler(this.TxtPwd_Validated);
            this.TxtPwd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtPwd_KeyDown);
            // 
            // TxtUserCode
            // 
            this.TxtUserCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtUserCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUserCode.Location = new System.Drawing.Point(55, 23);
            this.TxtUserCode.Name = "TxtUserCode";
            this.TxtUserCode.Size = new System.Drawing.Size(229, 15);
            this.TxtUserCode.TabIndex = 1;
            this.TxtUserCode.Validated += new System.EventHandler(this.TxtUserCode_Validated);
            this.TxtUserCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtUserCode_KeyDown);
            // 
            // ChkKeepUserCode
            // 
            this.ChkKeepUserCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkKeepUserCode.Location = new System.Drawing.Point(28, 85);
            this.ChkKeepUserCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkKeepUserCode.Name = "ChkKeepUserCode";
            this.ChkKeepUserCode.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkKeepUserCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkKeepUserCode.Properties.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
            this.ChkKeepUserCode.Properties.Appearance.Options.UseBackColor = true;
            this.ChkKeepUserCode.Properties.Appearance.Options.UseFont = true;
            this.ChkKeepUserCode.Properties.Appearance.Options.UseForeColor = true;
            this.ChkKeepUserCode.Properties.Caption = "Keep user id";
            this.ChkKeepUserCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkKeepUserCode.Size = new System.Drawing.Size(115, 22);
            this.ChkKeepUserCode.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightGray;
            this.panel3.BackgroundImage = global::RunSystem.Properties.Resources.BGLogin;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(177, 60);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(516, 441);
            this.panel3.TabIndex = 30;
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1047, 539);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "FrmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Run System";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.PnlSetting.ResumeLayout(false);
            this.PnlSetting.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkKeepUserCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel PnlSetting;
        private System.Windows.Forms.Label LblDb;
        private System.Windows.Forms.Label LblServer;
        private System.Windows.Forms.Button BtnSetting;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.Label LblCompanyName;
        private System.Windows.Forms.Label LblVersionNo;
        private DevExpress.XtraEditors.CheckEdit ChkKeepUserCode;
        private System.Windows.Forms.TextBox TxtDatabase;
        private System.Windows.Forms.TextBox TxtServer;
        private System.Windows.Forms.TextBox TxtUserCode;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox TxtPwd;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;

    }
}