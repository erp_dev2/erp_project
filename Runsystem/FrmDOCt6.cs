﻿#region Update
/*
    22/11/2019 [TKG/SIER] New application
    04/02/2020 [TKG/SIER] bug saat proses stock untuk do yg baru
    17/06/2020 [DITA/SIER] Tambah save procesind ke dotocthdr
    11/12/2020 [WED/SIER] ganti parameter IsDOCtUseCtQtPrice ke IsDOCtUseProcessInd untuk save ke Stock nya
    28/05/2021 [RDH/SIER] Tambah validasi hanya document DO to Customer yang ACTIVE saja yang bisa dipilih. Semua Document DO to Customer selama itu aktif.
    17/09/2021 [TKG/ALL] tambah validasi setting journal
    29/03/2022 [SET/SIER] Field Customer Category dan Customer terfilter berdasarkan group setting berdasar parameter IsFilterByCtCt
    11/08/2022 [TYO/SIER] tambah kolom total amount
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt6 : RunSystem.FrmBase15
    {
        #region Field

        internal string mMenuCode = string.Empty, mMainCurCode = string.Empty;
        private bool
            mIsCheckCOAJournalNotExists = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDOCtAmtRounded = false,
            mIsAutoJournalActived = false,
            mIsDOCtApprovalActived = false,
            mIsDOCtUseCtQtPrice = false,
            mIsDOCtUseProcessInd = false;
        internal string mNoOfDayDOCt6ToChoose = string.Empty;
        internal bool mIsDOCt6UseBackDateOnly = false,
            mIsCopyingDOUseActiveDoc =false,
            mIsFilterByCtCt = false;
            
        #endregion

        #region Constructor

        public FrmDOCt6(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();
                Sm.SetDteCurrentDate(DteDocDt);
                ClearGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsDOCtApprovalActived = Sm.IsDataExist("Select 1 from TblDocApprovalSetting Where DocType='DOCt' Limit 1;");
            
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'IsCheckCOAJournalNotExists', 'MainCurCode', 'NoOfDayDOCt6ToChoose', 'IsDOCtAmtRounded', ");
            SQL.AppendLine("'IsAcNoForSaleUseItemCategory', 'IsDOCtUseCtQtPrice', 'IsDOCt6UseBackDateOnly', 'IsDOCtUseProcessInd', 'IsCopyingDOUseActiveDoc', ");
            SQL.AppendLine("'IsFilterByCtCt' );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            case "IsAcNoForSaleUseItemCategory": mIsAcNoForSaleUseItemCategory = ParValue == "Y"; break;
                            case "IsDOCtUseCtQtPrice": mIsDOCtUseCtQtPrice = ParValue == "Y"; break;
                            case "IsDOCt6UseBackDateOnly": mIsDOCt6UseBackDateOnly = ParValue == "Y"; break;
                            case "IsDOCtUseProcessInd": mIsDOCtUseProcessInd = ParValue == "Y"; break;
                            case "IsCopyingDOUseActiveDoc": mIsCopyingDOUseActiveDoc = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            
                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "NoOfDayDOCt6ToChoose": mNoOfDayDOCt6ToChoose = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Processed",
                        "Warning",
                        "Document#", 
                        "",
                        "Date", 

                        //6-10
                        "Customer",
                        "Warehouse",
                        "Shipping",
                        "Remark",
                        "Total Amount"
                    },
                    new int[] 
                    {
                        //0
                        20,

                        //1-5
                        80, 200, 130, 20, 100,
                        
                        //6-10
                        200, 200, 300, 250, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 0, 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 5, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Grd1.Cols[10].Move(9);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Grd1.Rows.AutoHeight();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            var l1 = new List<Result1>();
            var l2 = new List<Result2>();
            var l3 = new List<Result3>();
            var IsCompleted = true;

            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 3).Length > 0 && !Sm.GetGrdBool(Grd1, r, 1))
                    {
                        if (!Process(r, ref l1, ref l2, ref l3))
                        {
                            Grd1.Cells[r, 1].Value = false;
                            IsCompleted = false;
                            break;
                        }
                        else
                        {
                            Grd1.Cells[r, 1].Value = true;
                            Grd1.Cells[r, 2].Value = string.Empty;
                        }
                    }
                }
                if (IsCompleted)
                {
                    Sm.StdMsg(mMsgType.Info, "Process is completed.");
                    ClearGrd();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l1.Clear();
                l2.Clear();
                l3.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOCt6Dlg(this));
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
            
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmDOCt(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmDOCt6Dlg(this));
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmDOCt(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }


        #endregion

        #region Additional Method

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt));
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    if (Sm.IsGrdValueEmpty(Grd1, r, 3, false, "DO# is empty.")) return true;
            }
            return false;
        }

        private bool Process(int r, ref List<Result1> l1, ref List<Result2> l2, ref List<Result3> l3)
        {
            var DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt", "TblDOCtHdr");
            
            l1.Clear();
            l2.Clear();
            l3.Clear();

            Process1(ref r, ref l1);
            if (l1.Count > 0)
            {
                Process2(ref l1, ref l2);
                Process3(ref l1, ref l2, ref l3);
                if (IsInvalid(r, ref l1))
                    return false;
                else
                {
                    if (mIsAutoJournalActived && mIsCheckCOAJournalNotExists)
                    {
                        if (IsJournalSettingInvalid(ref l3)) return false;
                    }
                    Process4(DocNo, ref l3, r);
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        private bool IsJournalSettingInvalid(ref List<Result3> l)
        {
            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter

            if (!mIsAcNoForSaleUseItemCategory)
            {
                if (Sm.GetParameter("AcNoForCOGS").Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForCOGS is empty.");
                    return true;
                }
                if (Sm.GetParameter("AcNoForSaleOfFinishedGoods").Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForSaleOfFinishedGoods is empty.");
                    return true;
                }
            }
            if (Sm.GetParameter("CustomerAcNoNonInvoice").Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                return true;
            }


            //Table
            if (IsJournalSettingInvalid_ItemCategory(Msg, ref l)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, ref List<Result3> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCtName = string.Empty;
            int i = 0;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("And (B.AcNo Is Null ");
            if (mIsAcNoForSaleUseItemCategory)
                SQL.AppendLine(" Or B.AcNo4 Is Null Or B.AcNo5 Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.ItCode In (");
            foreach (var x in l)
            {
                if (x.ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + i.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + i.ToString(), x.ItCode);
                    i++;
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private void Process1(ref int r, ref List<Result1> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
           
            SQL.AppendLine("Select B.DNo, A.WhsCode, B.ItCode, C.ItName, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, B.UPrice ");
            SQL.AppendLine("From TblDOCtHdr A, TblDOCtDtl B, TblItem C ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.CancelInd='N' ");
            SQL.AppendLine("And B.Qty>0.00 ");
            SQL.AppendLine("And B.ItCode=C.ItCode ");
            SQL.AppendLine("Order By B.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 3));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DNo", 
                    "WhsCode", "ItCode", "ItName", "Qty", "Qty2", 
                    "Qty3", "UPrice" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result1()
                        {
                            DNo = Sm.DrStr(dr, c[0]),
                            WhsCode = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            ItName = Sm.DrStr(dr, c[3]),
                            Qty = Sm.DrDec(dr, c[4]),
                            Qty2 = Sm.DrDec(dr, c[5]),
                            Qty3 = Sm.DrDec(dr, c[6]),
                            UPrice = Sm.DrDec(dr, c[7]),
                            Stock = 0m,
                            Stock2 = 0m,
                            Stock3 = 0m

                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result1> l1, ref List<Result2> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;
            var Filter = string.Empty;

            SQL.AppendLine("Select Lot, Bin, ItCode, PropCode, BatchNo, Source, ");
            SQL.AppendLine("Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where Qty>0 ");
            SQL.AppendLine("And WhsCode=@WhsCode ");
            foreach (var x in l1)
            {
                if (i==0) Sm.CmParam<String>(ref cm, "@WhsCode", x.WhsCode);
                if (Filter.Length > 0) Filter += " Or ";
                Filter += " ItCode=@ItCode0" + i.ToString();
                Sm.CmParam<String>(ref cm, "@ItCode0" + i.ToString(), x.ItCode);
                ++i;
            }
            SQL.AppendLine(" And (" + Filter + "); ");

            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "Lot", 
                    "Bin", "ItCode", "PropCode", "BatchNo", "Source",
                    "Qty", "Qty2", "Qty3" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Result2()
                        {
                            Lot = Sm.DrStr(dr, c[0]),
                            Bin = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            PropCode = Sm.DrStr(dr, c[3]),
                            BatchNo = Sm.DrStr(dr, c[4]),
                            Source = Sm.DrStr(dr, c[5]),
                            Qty = Sm.DrDec(dr, c[6]),
                            Qty2 = Sm.DrDec(dr, c[7]),
                            Qty3 = Sm.DrDec(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Result1> l1, ref List<Result2> l2, ref List<Result3> l3)
        {
            decimal QtyTemp = 0m, QtyTemp2 = 0m, QtyTemp3 = 0m;
            foreach (var i in l1)
            {
                QtyTemp = i.Qty;
                QtyTemp2 = i.Qty2;
                QtyTemp3 = i.Qty3;
                foreach (var j in l2.Where(w=>Sm.CompareStr(w.ItCode, i.ItCode) && w.Qty>0m))
                {
                    if (j.Qty >= QtyTemp)
                    {
                        l3.Add(new Result3()
                        {
                            Lot = j.Lot,
                            Bin = j.Bin,
                            ItCode = j.ItCode,
                            PropCode = j.PropCode,
                            BatchNo = j.BatchNo,
                            Source = j.Source,
                            Qty = QtyTemp,
                            Qty2 = QtyTemp2,
                            Qty3 = QtyTemp3,
                            UPrice = i.UPrice
                        });
                        j.Qty -= QtyTemp;
                        i.Stock += QtyTemp;
                        QtyTemp = 0m;

                        j.Qty2 -= QtyTemp2;
                        i.Stock2 += QtyTemp2;
                        QtyTemp2 = 0m;

                        j.Qty3 -= QtyTemp3;
                        i.Stock3 += QtyTemp3;
                        QtyTemp3 = 0m;
                    }
                    else
                    {
                        l3.Add(new Result3()
                        {
                            Lot = j.Lot,
                            Bin = j.Bin,
                            ItCode = j.ItCode,
                            PropCode = j.PropCode,
                            BatchNo = j.BatchNo,
                            Source = j.Source,
                            Qty = j.Qty,
                            Qty2 = j.Qty2,
                            Qty3 = j.Qty3,
                            UPrice = i.UPrice
                        });
                        i.Stock += j.Qty;
                        QtyTemp -= j.Qty;
                        j.Qty = 0m;

                        i.Stock2 += j.Qty2;
                        QtyTemp2 -= j.Qty2;
                        j.Qty2 = 0m;

                        i.Stock3 += j.Qty3;
                        QtyTemp3 -= j.Qty3;
                        j.Qty3 = 0m;
                    }
                    if (QtyTemp <= 0m) break;
                }                
            }
        }

        private void Process4(string DocNo, ref List<Result3> l3, int r)
        {
            var SQL = new StringBuilder();
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();
            int i = 0;

            SQL.AppendLine("Set @CurrentDt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblDOCtHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, ProcessInd, DOCtDocNoSource, WhsCode, CtCode, DocNoInternal, ResiNo, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, CustomsDocCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @DocDt, @Status, @ProcessInd, @DOCtDocNo, WhsCode, CtCode, DocNoInternal, ResiNo, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, CustomsDocCode, Remark, @UserCode, @CurrentDt ");
            SQL.AppendLine("From TblDOCtHdr Where DocNo=@DOCtDocNo; ");

            if (mIsDOCtApprovalActived)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, @CurrentDt ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='DOCt'; ");

                SQL.AppendLine("Update TblDOCtHdr Set Status='A' ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApproval ");
                SQL.AppendLine("    Where DocType='DOCt' ");
                SQL.AppendLine("    And DocNo=@DocNo ");
                SQL.AppendLine("    ); ");
            }

            foreach (var x in l3)
            {
                ++i;

                SQL.AppendLine("Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, ProcessInd, Source, Lot, Bin, QtyPackagingUnit, PackagingUnitUomCode, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @DNo"+i.ToString()+", 'N', @ItCode" + i.ToString() + ", @PropCode" + i.ToString() + ", @BatchNo" + i.ToString() + ", 'O', @Source" + i.ToString() + ", @Lot" + i.ToString() + ", @Bin" + i.ToString() + ", 0.00, Null, @Qty" + i.ToString() + ", @Qty2" + i.ToString() + ", @Qty3" + i.ToString() + ", @UPrice" + i.ToString() + ", Null, @UserCode, @CurrentDt); ");

                Sm.CmParam<String>(ref cm, "@DNo" + i.ToString(), Sm.Right("00" + (i + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@ItCode" + i.ToString(), x.ItCode);
                Sm.CmParam<String>(ref cm, "@PropCode" + i.ToString(), x.PropCode);
                Sm.CmParam<String>(ref cm, "@BatchNo" + i.ToString(), x.BatchNo);
                Sm.CmParam<String>(ref cm, "@Source" + i.ToString(), x.Source);
                Sm.CmParam<String>(ref cm, "@Lot" + i.ToString(), x.Lot);
                Sm.CmParam<String>(ref cm, "@Bin" + i.ToString(), x.Bin);
                Sm.CmParam<Decimal>(ref cm, "@Qty" + i.ToString(), x.Qty);
                Sm.CmParam<Decimal>(ref cm, "@Qty2" + i.ToString(), x.Qty2);
                Sm.CmParam<Decimal>(ref cm, "@Qty3" + i.ToString(), x.Qty3);
                Sm.CmParam<Decimal>(ref cm, "@UPrice" + i.ToString(), x.UPrice);
            }

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", mIsDOCtApprovalActived ? "O" : "A");
            Sm.CmParam<String>(ref cm, "@ProcessInd", mIsDOCtUseCtQtPrice ? "D" : "F");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DOCtDocNo", Sm.GetGrdStr(Grd1, r, 3));
            Sm.CmParam<String>(ref cm, "@DocType", "07");

            if (!mIsDOCtUseProcessInd) //if(!mIsDOCtUseCtQtPrice) comment by wed 11/12/2020
            {
                SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                SQL.AppendLine("@UserCode, @CurrentDt ");
                SQL.AppendLine("From TblDOCtHdr A ");
                SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And A.Status='A'; ");

                SQL.AppendLine("Update TblStockSummary As A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select T1.WhsCode, T2.Lot, T2.Bin, T2.Source, ");
                SQL.AppendLine("    Sum(T2.Qty) As Qty, ");
                SQL.AppendLine("    Sum(T2.Qty2) As Qty2, ");
                SQL.AppendLine("    Sum(T2.Qty3) As Qty3 ");
                SQL.AppendLine("    From TblDOCtHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
                SQL.AppendLine("    Where T1.DocNo=@DocNo ");
                SQL.AppendLine("    And T1.Status='A' ");
                SQL.AppendLine("    Group By T1.WhsCode, T2.Lot, T2.Bin, T2.Source ");
                SQL.AppendLine(") B ");
                SQL.AppendLine("    On A.WhsCode=B.WhsCode ");
                SQL.AppendLine("    And A.Lot=B.Lot ");
                SQL.AppendLine("    And A.Bin=B.Bin ");
                SQL.AppendLine("    And A.Source=B.Source ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.Qty=A.Qty-IfNull(B.Qty, 0.00), ");
                SQL.AppendLine("    A.Qty2=A.Qty2-IfNull(B.Qty2, 0.00), ");
                SQL.AppendLine("    A.Qty3=A.Qty3-IfNull(B.Qty3, 0.00), ");
                SQL.AppendLine("    A.LastUpBy=@UserCode, ");
                SQL.AppendLine("    A.LastUpDt=@CurrentDt; ");

                if (mIsAutoJournalActived)
                {
                    SQL.AppendLine("Set @CurCode:=(Select CurCode From TblDOCtHdr Where DocNo=@DocNo); ");
                    SQL.AppendLine("Set @EntCode:=(");
                    SQL.AppendLine("    Select C.EntCode ");
                    SQL.AppendLine("    From TblWarehouse A, TblCostCenter B, TblProfitCenter C ");
                    SQL.AppendLine("    Where A.CCCode=B.CCCode And B.ProfitCenterCode=C.ProfitCenterCode ");
                    SQL.AppendLine("    And A.WhsCode In (Select WhsCode From TblDOCtHdr Where DocNo=@DocNo) ");
                    SQL.AppendLine("); ");

                    SQL.AppendLine("Update TblDOCtHdr Set ");
                    SQL.AppendLine("    JournalDocNo=");
                    SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
                    SQL.AppendLine(" Where DocNo=@DocNo And Status='A'; ");

                    SQL.AppendLine("Insert Into TblJournalHdr ");
                    SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select A.JournalDocNo, ");
                    SQL.AppendLine("A.DocDt, ");
                    SQL.AppendLine("Concat('DO To Customer : ', A.DocNo) As JnDesc, ");
                    SQL.AppendLine("@MenuCode As MenuCode, ");
                    SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
                    SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblDOCtHdr A ");
                    SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
                    SQL.AppendLine("Where A.DocNo=@DocNo ");
                    SQL.AppendLine("And A.Status='A'; ");

                    SQL.AppendLine("Set @Row:=0; ");

                    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                    SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblJournalHdr A ");
                    SQL.AppendLine("Inner Join ( ");
                    SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                    if (mIsAcNoForSaleUseItemCategory)
                    {
                        SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("        Sum(Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty)) As DAmt, ");
                        else
                            SQL.AppendLine("        Sum(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblDOCtHdr A ");
                        SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        SQL.AppendLine("        Group By E.AcNo5 ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select D.ParValue As AcNo, ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                        else
                            SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblDOCtHdr A ");
                        SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }

                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
                    else
                        SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
                    SQL.AppendLine("        From TblDOCtHdr A ");
                    SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");

                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Sum(Floor(X1.Amt)) As DAmt, ");
                    else
                        SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From ( ");
                    SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
                    SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
                    SQL.AppendLine("            From TblDOCtHdr A ");
                    SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            Left Join ( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ) C On 0=0 ");
                    SQL.AppendLine("            Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        ) X1 ");
                    SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
                    SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
                    SQL.AppendLine("        Union All ");

                    if (mIsAcNoForSaleUseItemCategory)
                    {
                        SQL.AppendLine("        Select X.AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("        Sum(Floor(X.Amt)) As CAmt ");
                        else
                            SQL.AppendLine("        Sum(X.Amt) As CAmt ");
                        SQL.AppendLine("        From ( ");
                        SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                        SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 ");
                        SQL.AppendLine("            Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty ");
                        SQL.AppendLine("            As Amt ");
                        SQL.AppendLine("            From TblDOCtHdr A ");
                        SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("            Left Join ( ");
                        SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("            ) C On 0=0 ");
                        SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                        SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                        SQL.AppendLine("            Where A.DocNo=@DocNo ");
                        SQL.AppendLine("        ) X ");
                        SQL.AppendLine("        Group By X.AcNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("        Sum(Floor(X1.Amt)) As CAmt ");
                        else
                            SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
                        SQL.AppendLine("        From ( ");
                        SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
                        SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
                        SQL.AppendLine("            From TblDOCtHdr A ");
                        SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("            Left Join ( ");
                        SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("            ) C On 1=1 ");
                        SQL.AppendLine("            Where A.DocNo=@DocNo ");
                        SQL.AppendLine("        ) X1 ");
                        SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                        SQL.AppendLine("        Group By X2.ParValue ");
                    }

                    SQL.AppendLine("    ) Tbl Group By AcNo ");
                    SQL.AppendLine(") B On 1=1 ");
                    SQL.AppendLine("Where A.DocNo In ( ");
                    SQL.AppendLine("    Select JournalDocNo ");
                    SQL.AppendLine("    From TblDOCtHdr ");
                    SQL.AppendLine("    Where DocNo=@DocNo ");
                    SQL.AppendLine("    And JournalDocNo Is Not Null ");
                    SQL.AppendLine("    ); ");
            }
            

                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            }

            cm.CommandText = SQL.ToString();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        private bool IsInvalid(int r, ref List<Result1> l)
        {
            var Msg = string.Empty;
            var SQL = new StringBuilder();

            Grd1.Cells[r, 2].Value = string.Empty;

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDOCtHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select Distinct T1.DOCtDocNoSource ");
            SQL.AppendLine("    From TblDOCtHdr T1, TblDOCtDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    And T1.DOCtDocNoSource Is Not Null ");
            SQL.AppendLine("    And T2.CancelInd='N' ");
            SQL.AppendLine(") And DocNo=@Param;");

            if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd1, r, 3)))
            {
                Msg =
                    "Document# : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Warehouse : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                    "Customer : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine + Environment.NewLine +
                    "This DO already copied.";
                Sm.StdMsg(mMsgType.Warning, Msg);
                Grd1.Cells[r, 2].Value = Msg;
                Grd1.Rows.AutoHeight();
                Sm.FocusGrd(Grd1, r, 2);
                return true;
            }
            foreach (var i in l)
            {
                if (i.Stock<i.Qty)
                {
                    Msg =
                        "Document# : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                        "Warehouse : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                        "Customer : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                        "Item's Code : " + i.ItCode + Environment.NewLine +
                        "Item's Name : " + i.ItName + Environment.NewLine +
                        "DO's Quantity : " + Sm.FormatNum(i.Qty, 0) + Environment.NewLine +
                        "Stock : " + Sm.FormatNum(i.Stock, 0) + Environment.NewLine +
                        "Not enough stock.";
                    Sm.StdMsg(mMsgType.Warning,Msg);
                    Msg =
                        "Item's Code : " + i.ItCode + Environment.NewLine +
                        "Item's Name : " + i.ItName + Environment.NewLine +
                        "DO's Quantity : " + Sm.FormatNum(i.Qty, 0) + Environment.NewLine +
                        "Stock : " + Sm.FormatNum(i.Stock, 0) + Environment.NewLine +
                        "Not enough stock.";
                    Grd1.Cells[r, 2].Value = Msg;
                    Grd1.Rows.AutoHeight();
                    Sm.FocusGrd(Grd1, r, 2);
                    return true;
                }
            }
            return false;
        }

        #endregion
        #endregion

        #region Event
        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if(mIsDOCt6UseBackDateOnly) ClearGrd();
        }
        #endregion
    }

    #region Class

    class Result1
    {
        public string DocNo { set; get; }
        public string DNo { set; get; }
        public string WhsCode { set; get; }
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public decimal Stock { get; set; }
        public decimal Stock2 { get; set; }
        public decimal Stock3 { get; set; }
        public decimal UPrice { get; set; }
    }

    class Result2
    {
        public string WhsCode { set; get; }
        public string Lot { set; get; }
        public string Bin { set; get; }
        public string Source { get; set; }
        public string ItCode { get; set; }
        public string BatchNo { get; set; }
        public string PropCode { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
    }

    class Result3
    {
        public string Lot { set; get; }
        public string Bin { set; get; }
        public string Source { get; set; }
        public string ItCode { get; set; }
        public string BatchNo { get; set; }
        public string PropCode { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public decimal UPrice { get; set; }
    }

    #endregion
}
