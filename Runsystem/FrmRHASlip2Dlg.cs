﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRHASlip2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRHASlip2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRHASlip2Dlg(FrmRHASlip2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode(ref LueSiteCode, "");
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.HolidayDt, A.DeptCode,  ");
            SQL.AppendLine("B.Deptname, A.SiteCode, C.Sitename, A.Remark  ");
            SQL.AppendLine("From TblRHAHdr A  ");
            SQL.AppendLine("Left Join tblDepartment b on A.DeptCode = B.DeptCode   ");
            SQL.AppendLine("Left Join tblSite C On A.SiteCode = C.SiteCode  ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='A' And A.VoucherRequestDocNo is not null ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document",
                        "Date",
                        "Holiday Date",
                        "Department"+Environment.NewLine+"Code",
                        "Department",
                        
                        //6-8
                        "Site Code",
                        "Site",
                        "Remark"
                    },
                    new int[]
                    {
                        40, 
                        150, 100, 100, 100, 120,
                        100, 180, 250
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6});
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.DocDt ;",
                        new string[] 
                        { 
                            "DocNo",
                            "DocDt", "HolidayDt", "DeptCode", "DeptName", "SiteCode", 
                            "SiteName", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                int Row = Grd1.CurRow.Index;
                mFrmParent.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtDeptName.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtSiteName.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                mFrmParent.MeeRemark.EditValue = Sm.GetGrdStr(Grd1, Row, 8);
                Sm.SetDte(mFrmParent.DteDocDt, Sm.GetGrdDate(Grd1, Row, 2));
                Sm.SetDte(mFrmParent.DteHolidayDt, Sm.GetGrdDate(Grd1, Row, 3));
                Sm.ClearGrd(mFrmParent.Grd1, true);
                this.Close();
            }

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4,6 }, !ChkHideInfoInGrd.Checked);
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }


        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }


        #endregion

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender); 
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode)," ");
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

       
       
    }
}
