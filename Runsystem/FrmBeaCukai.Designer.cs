﻿namespace RunSystem
{
    partial class FrmBeaCukai
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBeaCukai));
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtPabean = new DevExpress.XtraEditors.TextEdit();
            this.TxtJnsEkspor = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtNoPengajuan = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtKodePabean = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtKategEkspor = new DevExpress.XtraEditors.TextEdit();
            this.TxtCaraBayar = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtCaraDagang = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtNoPendaftaran = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtNoBC = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtSubPos = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtMerkKemasan = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtJmlKemasan = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.DteBCDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtJnsKemasan = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.DtePendaftaranDt = new DevExpress.XtraEditors.DateEdit();
            this.DtePPJKDt = new DevExpress.XtraEditors.DateEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.TxtKantorPemeriksaan = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtValutaAsing = new DevExpress.XtraEditors.TextEdit();
            this.TxtPemeriksaan = new DevExpress.XtraEditors.TextEdit();
            this.TxtBankDevisa = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.MeeAlamat = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtNoPokokPPJK = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtNmNPWP = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtNPWP = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtNoPeti = new DevExpress.XtraEditors.TextEdit();
            this.TxtMerkPetiKemas = new System.Windows.Forms.Label();
            this.TxtJmlPeti = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtStatusPeti = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtPetiKemas = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtPLDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.BtnPLDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.TxtBenderaPengangkut = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtNoPengangkut = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.TxtSaranaPengangkut = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.TxtPengangkutan = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.DteTglEksporDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtCntName = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPabean.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJnsEkspor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoPengajuan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKodePabean.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKategEkspor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCaraBayar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCaraDagang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoPendaftaran.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoBC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubPos.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMerkKemasan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJmlKemasan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBCDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBCDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJnsKemasan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePendaftaranDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePendaftaranDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePPJKDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePPJKDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKantorPemeriksaan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtValutaAsing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPemeriksaan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankDevisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAlamat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoPokokPPJK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNmNPWP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoPeti.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJmlPeti.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatusPeti.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPetiKemas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBenderaPengangkut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoPengangkut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSaranaPengangkut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPengangkutan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTglEksporDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTglEksporDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCntName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(829, 0);
            this.panel1.Size = new System.Drawing.Size(70, 565);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtCntName);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.DteTglEksporDt);
            this.panel2.Controls.Add(this.label43);
            this.panel2.Controls.Add(this.TxtBenderaPengangkut);
            this.panel2.Controls.Add(this.TxtCtCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.TxtNoPengangkut);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.TxtSaranaPengangkut);
            this.panel2.Controls.Add(this.BtnPLDocNo);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.TxtPengangkutan);
            this.panel2.Controls.Add(this.TxtPLDocNo);
            this.panel2.Controls.Add(this.label42);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.TxtKategEkspor);
            this.panel2.Controls.Add(this.TxtCaraBayar);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtCaraDagang);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtPabean);
            this.panel2.Controls.Add(this.TxtJnsEkspor);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtNoPengajuan);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtNoPeti);
            this.panel2.Controls.Add(this.TxtKodePabean);
            this.panel2.Controls.Add(this.TxtMerkPetiKemas);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtJmlPeti);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.TxtStatusPeti);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.TxtPetiKemas);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(829, 493);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 493);
            this.panel3.Size = new System.Drawing.Size(829, 72);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 543);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(829, 72);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(351, 6);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.ShowToolTips = false;
            this.ChkCancelInd.Size = new System.Drawing.Size(73, 22);
            this.ChkCancelInd.TabIndex = 12;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(165, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(120, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(68, 30);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "Document Date";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(165, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(183, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(51, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document Number";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPabean
            // 
            this.TxtPabean.EnterMoveNextControl = true;
            this.TxtPabean.Location = new System.Drawing.Point(165, 111);
            this.TxtPabean.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPabean.Name = "TxtPabean";
            this.TxtPabean.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPabean.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPabean.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPabean.Properties.Appearance.Options.UseFont = true;
            this.TxtPabean.Size = new System.Drawing.Size(249, 20);
            this.TxtPabean.TabIndex = 23;
            // 
            // TxtJnsEkspor
            // 
            this.TxtJnsEkspor.EnterMoveNextControl = true;
            this.TxtJnsEkspor.Location = new System.Drawing.Point(165, 174);
            this.TxtJnsEkspor.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJnsEkspor.Name = "TxtJnsEkspor";
            this.TxtJnsEkspor.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtJnsEkspor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJnsEkspor.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJnsEkspor.Properties.Appearance.Options.UseFont = true;
            this.TxtJnsEkspor.Size = new System.Drawing.Size(249, 20);
            this.TxtJnsEkspor.TabIndex = 29;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(89, 177);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 14);
            this.label8.TabIndex = 28;
            this.label8.Text = "Jenis Ekspor";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNoPengajuan
            // 
            this.TxtNoPengajuan.EnterMoveNextControl = true;
            this.TxtNoPengajuan.Location = new System.Drawing.Point(165, 153);
            this.TxtNoPengajuan.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoPengajuan.Name = "TxtNoPengajuan";
            this.TxtNoPengajuan.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNoPengajuan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoPengajuan.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoPengajuan.Properties.Appearance.Options.UseFont = true;
            this.TxtNoPengajuan.Size = new System.Drawing.Size(249, 20);
            this.TxtNoPengajuan.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(58, 156);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 14);
            this.label7.TabIndex = 26;
            this.label7.Text = "Nomor Pengajuan";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKodePabean
            // 
            this.TxtKodePabean.EnterMoveNextControl = true;
            this.TxtKodePabean.Location = new System.Drawing.Point(165, 132);
            this.TxtKodePabean.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKodePabean.Name = "TxtKodePabean";
            this.TxtKodePabean.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKodePabean.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKodePabean.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKodePabean.Properties.Appearance.Options.UseFont = true;
            this.TxtKodePabean.Size = new System.Drawing.Size(249, 20);
            this.TxtKodePabean.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(24, 135);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 14);
            this.label5.TabIndex = 24;
            this.label5.Text = "Kode  Pabean / SPKPBC";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(75, 114);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 14);
            this.label17.TabIndex = 22;
            this.label17.Text = "Kantor Pabean";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKategEkspor
            // 
            this.TxtKategEkspor.EnterMoveNextControl = true;
            this.TxtKategEkspor.Location = new System.Drawing.Point(165, 195);
            this.TxtKategEkspor.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKategEkspor.Name = "TxtKategEkspor";
            this.TxtKategEkspor.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKategEkspor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKategEkspor.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKategEkspor.Properties.Appearance.Options.UseFont = true;
            this.TxtKategEkspor.Size = new System.Drawing.Size(249, 20);
            this.TxtKategEkspor.TabIndex = 31;
            // 
            // TxtCaraBayar
            // 
            this.TxtCaraBayar.EnterMoveNextControl = true;
            this.TxtCaraBayar.Location = new System.Drawing.Point(165, 237);
            this.TxtCaraBayar.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCaraBayar.Name = "TxtCaraBayar";
            this.TxtCaraBayar.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCaraBayar.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCaraBayar.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCaraBayar.Properties.Appearance.Options.UseFont = true;
            this.TxtCaraBayar.Size = new System.Drawing.Size(249, 20);
            this.TxtCaraBayar.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(62, 240);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 14);
            this.label3.TabIndex = 34;
            this.label3.Text = "Cara Pembayaran";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCaraDagang
            // 
            this.TxtCaraDagang.EnterMoveNextControl = true;
            this.TxtCaraDagang.Location = new System.Drawing.Point(165, 216);
            this.TxtCaraDagang.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCaraDagang.Name = "TxtCaraDagang";
            this.TxtCaraDagang.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCaraDagang.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCaraDagang.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCaraDagang.Properties.Appearance.Options.UseFont = true;
            this.TxtCaraDagang.Size = new System.Drawing.Size(249, 20);
            this.TxtCaraDagang.TabIndex = 33;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(57, 219);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 14);
            this.label6.TabIndex = 32;
            this.label6.Text = "Cara Perdagangan";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(70, 198);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 14);
            this.label9.TabIndex = 30;
            this.label9.Text = "Kategori Ekspor";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNoPendaftaran
            // 
            this.TxtNoPendaftaran.EnterMoveNextControl = true;
            this.TxtNoPendaftaran.Location = new System.Drawing.Point(162, 88);
            this.TxtNoPendaftaran.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoPendaftaran.Name = "TxtNoPendaftaran";
            this.TxtNoPendaftaran.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNoPendaftaran.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoPendaftaran.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoPendaftaran.Properties.Appearance.Options.UseFont = true;
            this.TxtNoPendaftaran.Size = new System.Drawing.Size(234, 20);
            this.TxtNoPendaftaran.TabIndex = 63;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(92, 154);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 14);
            this.label10.TabIndex = 64;
            this.label10.Text = "Tanggal BC";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNoBC
            // 
            this.TxtNoBC.EnterMoveNextControl = true;
            this.TxtNoBC.Location = new System.Drawing.Point(162, 130);
            this.TxtNoBC.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoBC.Name = "TxtNoBC";
            this.TxtNoBC.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNoBC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoBC.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoBC.Properties.Appearance.Options.UseFont = true;
            this.TxtNoBC.Size = new System.Drawing.Size(234, 20);
            this.TxtNoBC.TabIndex = 67;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(99, 133);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 14);
            this.label11.TabIndex = 62;
            this.label11.Text = "Nomor BC";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(40, 113);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 14);
            this.label12.TabIndex = 60;
            this.label12.Text = "Tanggal Pendaftaran";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(47, 91);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 14);
            this.label13.TabIndex = 58;
            this.label13.Text = "Nomor Pendaftaran";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(159, 69);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 14);
            this.label14.TabIndex = 57;
            this.label14.Text = "Bea Cukai";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSubPos
            // 
            this.TxtSubPos.EnterMoveNextControl = true;
            this.TxtSubPos.Location = new System.Drawing.Point(162, 172);
            this.TxtSubPos.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubPos.Name = "TxtSubPos";
            this.TxtSubPos.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSubPos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubPos.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubPos.Properties.Appearance.Options.UseFont = true;
            this.TxtSubPos.Size = new System.Drawing.Size(234, 20);
            this.TxtSubPos.TabIndex = 71;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(81, 175);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 14);
            this.label15.TabIndex = 66;
            this.label15.Text = "Pos/ Sub Pos";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.TxtMerkKemasan);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.TxtJmlKemasan);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.DteBCDt);
            this.panel4.Controls.Add(this.TxtJnsKemasan);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.DtePendaftaranDt);
            this.panel4.Controls.Add(this.DtePPJKDt);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.TxtKantorPemeriksaan);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.TxtValutaAsing);
            this.panel4.Controls.Add(this.TxtPemeriksaan);
            this.panel4.Controls.Add(this.TxtBankDevisa);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.MeeAlamat);
            this.panel4.Controls.Add(this.TxtNoPokokPPJK);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.TxtNmNPWP);
            this.panel4.Controls.Add(this.TxtNoPendaftaran);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.TxtSubPos);
            this.panel4.Controls.Add(this.TxtNPWP);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.TxtNoBC);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(422, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(407, 493);
            this.panel4.TabIndex = 88;
            // 
            // TxtMerkKemasan
            // 
            this.TxtMerkKemasan.EnterMoveNextControl = true;
            this.TxtMerkKemasan.Location = new System.Drawing.Point(162, 457);
            this.TxtMerkKemasan.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMerkKemasan.Name = "TxtMerkKemasan";
            this.TxtMerkKemasan.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMerkKemasan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMerkKemasan.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMerkKemasan.Properties.Appearance.Options.UseFont = true;
            this.TxtMerkKemasan.Size = new System.Drawing.Size(234, 20);
            this.TxtMerkKemasan.TabIndex = 94;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(68, 459);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(92, 14);
            this.label38.TabIndex = 86;
            this.label38.Text = "Merek Kemasan";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(159, 395);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(84, 14);
            this.label39.TabIndex = 81;
            this.label39.Text = "Data Kemasan";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJmlKemasan
            // 
            this.TxtJmlKemasan.EnterMoveNextControl = true;
            this.TxtJmlKemasan.Location = new System.Drawing.Point(162, 436);
            this.TxtJmlKemasan.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJmlKemasan.Name = "TxtJmlKemasan";
            this.TxtJmlKemasan.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtJmlKemasan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJmlKemasan.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJmlKemasan.Properties.Appearance.Options.UseFont = true;
            this.TxtJmlKemasan.Size = new System.Drawing.Size(234, 20);
            this.TxtJmlKemasan.TabIndex = 92;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(64, 439);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(96, 14);
            this.label40.TabIndex = 84;
            this.label40.Text = "Jumlah Kemasan";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteBCDt
            // 
            this.DteBCDt.EditValue = null;
            this.DteBCDt.EnterMoveNextControl = true;
            this.DteBCDt.Location = new System.Drawing.Point(162, 151);
            this.DteBCDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBCDt.Name = "DteBCDt";
            this.DteBCDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBCDt.Properties.Appearance.Options.UseFont = true;
            this.DteBCDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBCDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBCDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBCDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBCDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBCDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBCDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBCDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBCDt.Properties.MaxLength = 8;
            this.DteBCDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBCDt.Size = new System.Drawing.Size(120, 20);
            this.DteBCDt.TabIndex = 69;
            // 
            // TxtJnsKemasan
            // 
            this.TxtJnsKemasan.EnterMoveNextControl = true;
            this.TxtJnsKemasan.Location = new System.Drawing.Point(162, 415);
            this.TxtJnsKemasan.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJnsKemasan.Name = "TxtJnsKemasan";
            this.TxtJnsKemasan.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtJnsKemasan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJnsKemasan.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJnsKemasan.Properties.Appearance.Options.UseFont = true;
            this.TxtJnsKemasan.Size = new System.Drawing.Size(234, 20);
            this.TxtJnsKemasan.TabIndex = 90;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(75, 418);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(85, 14);
            this.label41.TabIndex = 82;
            this.label41.Text = "Jenis Kemasan";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DtePendaftaranDt
            // 
            this.DtePendaftaranDt.EditValue = null;
            this.DtePendaftaranDt.EnterMoveNextControl = true;
            this.DtePendaftaranDt.Location = new System.Drawing.Point(162, 109);
            this.DtePendaftaranDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePendaftaranDt.Name = "DtePendaftaranDt";
            this.DtePendaftaranDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePendaftaranDt.Properties.Appearance.Options.UseFont = true;
            this.DtePendaftaranDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePendaftaranDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePendaftaranDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePendaftaranDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePendaftaranDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePendaftaranDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePendaftaranDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePendaftaranDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePendaftaranDt.Properties.MaxLength = 8;
            this.DtePendaftaranDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePendaftaranDt.Size = new System.Drawing.Size(120, 20);
            this.DtePendaftaranDt.TabIndex = 65;
            // 
            // DtePPJKDt
            // 
            this.DtePPJKDt.EditValue = null;
            this.DtePPJKDt.EnterMoveNextControl = true;
            this.DtePPJKDt.Location = new System.Drawing.Point(162, 302);
            this.DtePPJKDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePPJKDt.Name = "DtePPJKDt";
            this.DtePPJKDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePPJKDt.Properties.Appearance.Options.UseFont = true;
            this.DtePPJKDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePPJKDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePPJKDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePPJKDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePPJKDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePPJKDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePPJKDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePPJKDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePPJKDt.Properties.MaxLength = 8;
            this.DtePPJKDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePPJKDt.Size = new System.Drawing.Size(120, 20);
            this.DtePPJKDt.TabIndex = 82;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(126, 305);
            this.label35.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(33, 14);
            this.label35.TabIndex = 76;
            this.label35.Text = "Date";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(159, 328);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(150, 14);
            this.label32.TabIndex = 77;
            this.label32.Text = "Data Tempat Pemeriksaan";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKantorPemeriksaan
            // 
            this.TxtKantorPemeriksaan.EnterMoveNextControl = true;
            this.TxtKantorPemeriksaan.Location = new System.Drawing.Point(162, 369);
            this.TxtKantorPemeriksaan.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKantorPemeriksaan.Name = "TxtKantorPemeriksaan";
            this.TxtKantorPemeriksaan.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKantorPemeriksaan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKantorPemeriksaan.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKantorPemeriksaan.Properties.Appearance.Options.UseFont = true;
            this.TxtKantorPemeriksaan.Size = new System.Drawing.Size(234, 20);
            this.TxtKantorPemeriksaan.TabIndex = 87;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(2, 372);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(158, 14);
            this.label33.TabIndex = 80;
            this.label33.Text = "Kantor Pabean Pemeriksaan";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtValutaAsing
            // 
            this.TxtValutaAsing.EnterMoveNextControl = true;
            this.TxtValutaAsing.Location = new System.Drawing.Point(162, 44);
            this.TxtValutaAsing.Margin = new System.Windows.Forms.Padding(5);
            this.TxtValutaAsing.Name = "TxtValutaAsing";
            this.TxtValutaAsing.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtValutaAsing.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtValutaAsing.Properties.Appearance.Options.UseBackColor = true;
            this.TxtValutaAsing.Properties.Appearance.Options.UseFont = true;
            this.TxtValutaAsing.Size = new System.Drawing.Size(234, 20);
            this.TxtValutaAsing.TabIndex = 60;
            // 
            // TxtPemeriksaan
            // 
            this.TxtPemeriksaan.EnterMoveNextControl = true;
            this.TxtPemeriksaan.Location = new System.Drawing.Point(162, 348);
            this.TxtPemeriksaan.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPemeriksaan.Name = "TxtPemeriksaan";
            this.TxtPemeriksaan.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPemeriksaan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPemeriksaan.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPemeriksaan.Properties.Appearance.Options.UseFont = true;
            this.TxtPemeriksaan.Size = new System.Drawing.Size(234, 20);
            this.TxtPemeriksaan.TabIndex = 85;
            // 
            // TxtBankDevisa
            // 
            this.TxtBankDevisa.EnterMoveNextControl = true;
            this.TxtBankDevisa.Location = new System.Drawing.Point(162, 23);
            this.TxtBankDevisa.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankDevisa.Name = "TxtBankDevisa";
            this.TxtBankDevisa.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankDevisa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankDevisa.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankDevisa.Properties.Appearance.Options.UseFont = true;
            this.TxtBankDevisa.Size = new System.Drawing.Size(234, 20);
            this.TxtBankDevisa.TabIndex = 58;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(50, 351);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(110, 14);
            this.label34.TabIndex = 78;
            this.label34.Text = "Lokasi Pemeriksaan";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAlamat
            // 
            this.MeeAlamat.EnterMoveNextControl = true;
            this.MeeAlamat.Location = new System.Drawing.Point(162, 260);
            this.MeeAlamat.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAlamat.Name = "MeeAlamat";
            this.MeeAlamat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAlamat.Properties.Appearance.Options.UseFont = true;
            this.MeeAlamat.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAlamat.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAlamat.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAlamat.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAlamat.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAlamat.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAlamat.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAlamat.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAlamat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAlamat.Properties.MaxLength = 400;
            this.MeeAlamat.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAlamat.Properties.ShowIcon = false;
            this.MeeAlamat.Size = new System.Drawing.Size(234, 20);
            this.MeeAlamat.TabIndex = 78;
            this.MeeAlamat.ToolTip = "F4 : Show/hide text";
            this.MeeAlamat.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAlamat.ToolTipTitle = "Run System";
            // 
            // TxtNoPokokPPJK
            // 
            this.TxtNoPokokPPJK.EnterMoveNextControl = true;
            this.TxtNoPokokPPJK.Location = new System.Drawing.Point(162, 281);
            this.TxtNoPokokPPJK.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoPokokPPJK.Name = "TxtNoPokokPPJK";
            this.TxtNoPokokPPJK.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNoPokokPPJK.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoPokokPPJK.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoPokokPPJK.Properties.Appearance.Options.UseFont = true;
            this.TxtNoPokokPPJK.Size = new System.Drawing.Size(234, 20);
            this.TxtNoPokokPPJK.TabIndex = 80;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(50, 284);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(110, 14);
            this.label25.TabIndex = 74;
            this.label25.Text = "Nomor Pokok PPJK";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(116, 263);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(44, 14);
            this.label26.TabIndex = 72;
            this.label26.Text = "Alamat";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(159, 198);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(33, 14);
            this.label27.TabIndex = 67;
            this.label27.Text = "PPJK";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNmNPWP
            // 
            this.TxtNmNPWP.EnterMoveNextControl = true;
            this.TxtNmNPWP.Location = new System.Drawing.Point(162, 239);
            this.TxtNmNPWP.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNmNPWP.Name = "TxtNmNPWP";
            this.TxtNmNPWP.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNmNPWP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNmNPWP.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNmNPWP.Properties.Appearance.Options.UseFont = true;
            this.TxtNmNPWP.Size = new System.Drawing.Size(234, 20);
            this.TxtNmNPWP.TabIndex = 76;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(123, 242);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(37, 14);
            this.label28.TabIndex = 70;
            this.label28.Text = "Nama";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNPWP
            // 
            this.TxtNPWP.EnterMoveNextControl = true;
            this.TxtNPWP.Location = new System.Drawing.Point(162, 218);
            this.TxtNPWP.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNPWP.Name = "TxtNPWP";
            this.TxtNPWP.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNPWP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNPWP.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNPWP.Properties.Appearance.Options.UseFont = true;
            this.TxtNPWP.Size = new System.Drawing.Size(234, 20);
            this.TxtNPWP.TabIndex = 74;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(119, 221);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 14);
            this.label29.TabIndex = 68;
            this.label29.Text = "NPWP";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(159, 3);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(125, 14);
            this.label19.TabIndex = 53;
            this.label19.Text = "Data Transaksi Ekspor";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(89, 25);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 14);
            this.label18.TabIndex = 54;
            this.label18.Text = "Bank Devisa";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(86, 46);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 14);
            this.label16.TabIndex = 56;
            this.label16.Text = "Valuta Asing";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNoPeti
            // 
            this.TxtNoPeti.EnterMoveNextControl = true;
            this.TxtNoPeti.Location = new System.Drawing.Point(165, 467);
            this.TxtNoPeti.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoPeti.Name = "TxtNoPeti";
            this.TxtNoPeti.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNoPeti.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoPeti.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoPeti.Properties.Appearance.Options.UseFont = true;
            this.TxtNoPeti.Size = new System.Drawing.Size(249, 20);
            this.TxtNoPeti.TabIndex = 55;
            // 
            // TxtMerkPetiKemas
            // 
            this.TxtMerkPetiKemas.AutoSize = true;
            this.TxtMerkPetiKemas.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMerkPetiKemas.ForeColor = System.Drawing.Color.Black;
            this.TxtMerkPetiKemas.Location = new System.Drawing.Point(13, 470);
            this.TxtMerkPetiKemas.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TxtMerkPetiKemas.Name = "TxtMerkPetiKemas";
            this.TxtMerkPetiKemas.Size = new System.Drawing.Size(149, 14);
            this.TxtMerkPetiKemas.TabIndex = 52;
            this.TxtMerkPetiKemas.Text = "Merk && Nomor Peti Kemas";
            this.TxtMerkPetiKemas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJmlPeti
            // 
            this.TxtJmlPeti.EnterMoveNextControl = true;
            this.TxtJmlPeti.Location = new System.Drawing.Point(165, 446);
            this.TxtJmlPeti.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJmlPeti.Name = "TxtJmlPeti";
            this.TxtJmlPeti.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtJmlPeti.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJmlPeti.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJmlPeti.Properties.Appearance.Options.UseFont = true;
            this.TxtJmlPeti.Size = new System.Drawing.Size(249, 20);
            this.TxtJmlPeti.TabIndex = 53;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(54, 449);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(108, 14);
            this.label24.TabIndex = 50;
            this.label24.Text = "Jumlah Peti Kemas";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(165, 386);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 14);
            this.label20.TabIndex = 45;
            this.label20.Text = "Data Peti Kemas";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatusPeti
            // 
            this.TxtStatusPeti.EnterMoveNextControl = true;
            this.TxtStatusPeti.Location = new System.Drawing.Point(165, 425);
            this.TxtStatusPeti.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatusPeti.Name = "TxtStatusPeti";
            this.TxtStatusPeti.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtStatusPeti.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatusPeti.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatusPeti.Properties.Appearance.Options.UseFont = true;
            this.TxtStatusPeti.Size = new System.Drawing.Size(249, 20);
            this.TxtStatusPeti.TabIndex = 51;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(56, 428);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(106, 14);
            this.label21.TabIndex = 48;
            this.label21.Text = "Status Peti Kemas";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPetiKemas
            // 
            this.TxtPetiKemas.EnterMoveNextControl = true;
            this.TxtPetiKemas.Location = new System.Drawing.Point(165, 404);
            this.TxtPetiKemas.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPetiKemas.Name = "TxtPetiKemas";
            this.TxtPetiKemas.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPetiKemas.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPetiKemas.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPetiKemas.Properties.Appearance.Options.UseFont = true;
            this.TxtPetiKemas.Size = new System.Drawing.Size(249, 20);
            this.TxtPetiKemas.TabIndex = 49;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(95, 407);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 14);
            this.label22.TabIndex = 46;
            this.label22.Text = "Peti Kemas";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPLDocNo
            // 
            this.TxtPLDocNo.EnterMoveNextControl = true;
            this.TxtPLDocNo.Location = new System.Drawing.Point(165, 48);
            this.TxtPLDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPLDocNo.Name = "TxtPLDocNo";
            this.TxtPLDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPLDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPLDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPLDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPLDocNo.Properties.MaxLength = 16;
            this.TxtPLDocNo.Size = new System.Drawing.Size(183, 20);
            this.TxtPLDocNo.TabIndex = 16;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Location = new System.Drawing.Point(92, 52);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 14);
            this.label36.TabIndex = 15;
            this.label36.Text = "Packing List";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPLDocNo
            // 
            this.BtnPLDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPLDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPLDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPLDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPLDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPLDocNo.Appearance.Options.UseFont = true;
            this.BtnPLDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPLDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPLDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPLDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPLDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPLDocNo.Image")));
            this.BtnPLDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPLDocNo.Location = new System.Drawing.Point(352, 46);
            this.BtnPLDocNo.Name = "BtnPLDocNo";
            this.BtnPLDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPLDocNo.TabIndex = 17;
            this.BtnPLDocNo.ToolTip = "Find Packing List";
            this.BtnPLDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPLDocNo.ToolTipTitle = "Run System";
            this.BtnPLDocNo.Click += new System.EventHandler(this.BtnPLDocNo_Click);
            // 
            // TxtCtCode
            // 
            this.TxtCtCode.EnterMoveNextControl = true;
            this.TxtCtCode.Location = new System.Drawing.Point(165, 69);
            this.TxtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtCode.Name = "TxtCtCode";
            this.TxtCtCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtCode.Properties.MaxLength = 16;
            this.TxtCtCode.Size = new System.Drawing.Size(249, 20);
            this.TxtCtCode.TabIndex = 19;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(103, 72);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(59, 14);
            this.label30.TabIndex = 18;
            this.label30.Text = "Customer";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBenderaPengangkut
            // 
            this.TxtBenderaPengangkut.EnterMoveNextControl = true;
            this.TxtBenderaPengangkut.Location = new System.Drawing.Point(165, 342);
            this.TxtBenderaPengangkut.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBenderaPengangkut.Name = "TxtBenderaPengangkut";
            this.TxtBenderaPengangkut.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBenderaPengangkut.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBenderaPengangkut.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBenderaPengangkut.Properties.Appearance.Options.UseFont = true;
            this.TxtBenderaPengangkut.Size = new System.Drawing.Size(249, 20);
            this.TxtBenderaPengangkut.TabIndex = 44;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(0, 345);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 14);
            this.label2.TabIndex = 42;
            this.label2.Text = "Bendera Sarana Pengangkut";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNoPengangkut
            // 
            this.TxtNoPengangkut.EnterMoveNextControl = true;
            this.TxtNoPengangkut.Location = new System.Drawing.Point(165, 321);
            this.TxtNoPengangkut.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoPengangkut.Name = "TxtNoPengangkut";
            this.TxtNoPengangkut.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNoPengangkut.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoPengangkut.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoPengangkut.Properties.Appearance.Options.UseFont = true;
            this.TxtNoPengangkut.Size = new System.Drawing.Size(249, 20);
            this.TxtNoPengangkut.TabIndex = 42;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(66, 324);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(96, 14);
            this.label23.TabIndex = 40;
            this.label23.Text = "No. Pengangkut";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(165, 261);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(115, 14);
            this.label31.TabIndex = 35;
            this.label31.Text = "Data Pengangkutan";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSaranaPengangkut
            // 
            this.TxtSaranaPengangkut.EnterMoveNextControl = true;
            this.TxtSaranaPengangkut.Location = new System.Drawing.Point(165, 300);
            this.TxtSaranaPengangkut.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSaranaPengangkut.Name = "TxtSaranaPengangkut";
            this.TxtSaranaPengangkut.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSaranaPengangkut.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSaranaPengangkut.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSaranaPengangkut.Properties.Appearance.Options.UseFont = true;
            this.TxtSaranaPengangkut.Size = new System.Drawing.Size(249, 20);
            this.TxtSaranaPengangkut.TabIndex = 40;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(15, 303);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(147, 14);
            this.label37.TabIndex = 38;
            this.label37.Text = "Nama Sarana Pengangkut";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPengangkutan
            // 
            this.TxtPengangkutan.EnterMoveNextControl = true;
            this.TxtPengangkutan.Location = new System.Drawing.Point(165, 279);
            this.TxtPengangkutan.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPengangkutan.Name = "TxtPengangkutan";
            this.TxtPengangkutan.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPengangkutan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPengangkutan.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPengangkutan.Properties.Appearance.Options.UseFont = true;
            this.TxtPengangkutan.Size = new System.Drawing.Size(249, 20);
            this.TxtPengangkutan.TabIndex = 38;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Red;
            this.label42.Location = new System.Drawing.Point(49, 282);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(113, 14);
            this.label42.TabIndex = 36;
            this.label42.Text = "Cara Pengangkutan";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(19, 366);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(143, 14);
            this.label43.TabIndex = 44;
            this.label43.Text = "Tanggal Perkiraan Ekspor";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTglEksporDt
            // 
            this.DteTglEksporDt.EditValue = null;
            this.DteTglEksporDt.EnterMoveNextControl = true;
            this.DteTglEksporDt.Location = new System.Drawing.Point(165, 363);
            this.DteTglEksporDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTglEksporDt.Name = "DteTglEksporDt";
            this.DteTglEksporDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTglEksporDt.Properties.Appearance.Options.UseFont = true;
            this.DteTglEksporDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTglEksporDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTglEksporDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTglEksporDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTglEksporDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTglEksporDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTglEksporDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTglEksporDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTglEksporDt.Properties.MaxLength = 8;
            this.DteTglEksporDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTglEksporDt.Size = new System.Drawing.Size(120, 20);
            this.DteTglEksporDt.TabIndex = 46;
            // 
            // TxtCntName
            // 
            this.TxtCntName.EnterMoveNextControl = true;
            this.TxtCntName.Location = new System.Drawing.Point(165, 90);
            this.TxtCntName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCntName.Name = "TxtCntName";
            this.TxtCntName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCntName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCntName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCntName.Properties.Appearance.Options.UseFont = true;
            this.TxtCntName.Properties.MaxLength = 16;
            this.TxtCntName.Size = new System.Drawing.Size(249, 20);
            this.TxtCntName.TabIndex = 21;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(103, 93);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(59, 14);
            this.label44.TabIndex = 20;
            this.label44.Text = "Container";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmBeaCukai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 565);
            this.Name = "FrmBeaCukai";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPabean.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJnsEkspor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoPengajuan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKodePabean.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKategEkspor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCaraBayar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCaraDagang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoPendaftaran.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoBC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubPos.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMerkKemasan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJmlKemasan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBCDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBCDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJnsKemasan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePendaftaranDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePendaftaranDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePPJKDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePPJKDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKantorPemeriksaan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtValutaAsing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPemeriksaan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankDevisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAlamat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoPokokPPJK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNmNPWP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoPeti.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJmlPeti.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatusPeti.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPetiKemas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBenderaPengangkut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoPengangkut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSaranaPengangkut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPengangkutan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTglEksporDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTglEksporDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCntName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtPabean;
        internal DevExpress.XtraEditors.TextEdit TxtJnsEkspor;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtNoPengajuan;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtKodePabean;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtKategEkspor;
        internal DevExpress.XtraEditors.TextEdit TxtCaraBayar;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtCaraDagang;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtNoPendaftaran;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtNoBC;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtSubPos;
        private System.Windows.Forms.Label label15;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtStatusPeti;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtPetiKemas;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtNoPeti;
        private System.Windows.Forms.Label TxtMerkPetiKemas;
        internal DevExpress.XtraEditors.TextEdit TxtJmlPeti;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtNoPokokPPJK;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtNmNPWP;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtNPWP;
        private System.Windows.Forms.Label label29;
        private DevExpress.XtraEditors.MemoExEdit MeeAlamat;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit TxtKantorPemeriksaan;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtPemeriksaan;
        private System.Windows.Forms.Label label34;
        internal DevExpress.XtraEditors.TextEdit TxtValutaAsing;
        internal DevExpress.XtraEditors.TextEdit TxtBankDevisa;
        internal DevExpress.XtraEditors.DateEdit DtePPJKDt;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtJmlKemasan;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtJnsKemasan;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label36;
        public DevExpress.XtraEditors.SimpleButton BtnPLDocNo;
        internal DevExpress.XtraEditors.DateEdit DteBCDt;
        internal DevExpress.XtraEditors.DateEdit DtePendaftaranDt;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.TextEdit TxtMerkKemasan;
        public DevExpress.XtraEditors.TextEdit TxtPLDocNo;
        public DevExpress.XtraEditors.TextEdit TxtCtCode;
        internal DevExpress.XtraEditors.TextEdit TxtBenderaPengangkut;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtNoPengangkut;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.TextEdit TxtSaranaPengangkut;
        private System.Windows.Forms.Label label37;
        internal DevExpress.XtraEditors.TextEdit TxtPengangkutan;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        internal DevExpress.XtraEditors.DateEdit DteTglEksporDt;
        public DevExpress.XtraEditors.TextEdit TxtCntName;
        private System.Windows.Forms.Label label44;
    }
}