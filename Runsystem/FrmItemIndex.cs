﻿#region Update
/*
    16/07/2018 [WED] Item Code yang combobox diganti jadi lup
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemIndex : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string mStateInd = string.Empty;
        internal FrmItemIndexFind FrmFind;

        #endregion

        #region Constructor

        public FrmItemIndex(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
            SetLueItPlanningGrp(ref LueItPlanningGrp);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItCode, TxtItName, TxtItIndex, LueItPlanningGrp
                    }, true);
                    BtnItCode.Enabled = false;
                    TxtItCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItIndex, LueItPlanningGrp
                    }, false);
                    BtnItCode.Enabled = true;
                    TxtItCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItIndex, LueItPlanningGrp
                    }, false);
                  //  TxtItIndex.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mStateInd = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtItCode, TxtItName, LueItPlanningGrp });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtItIndex }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemIndexFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            mStateInd = "I";
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItCode, "Item Code", false)) return;
            SetFormControl(mState.Edit);
            mStateInd = "E";
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItCode, "Item Code", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblItemIndex Where ItCode=@ItCode" };
                Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblItemIndex(ItCode, ItIndex, ItPlanningGrpCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ItCode, @ItIndex, @ItPlanningGrpCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ItIndex=@ItIndex, ItPlanningGrpCode=@ItPlanningGrpCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
                Sm.CmParam<Decimal>(ref cm, "@ItIndex", decimal.Parse(TxtItIndex.Text));
                Sm.CmParam<String>(ref cm, "@ItPlanningGrpCode", Sm.GetLue(LueItPlanningGrp));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtItCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ItCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.ItCode, B.ItName, A.ItIndex, A.ItPlanningGrpCode " +
                        "From TblItemIndex A " +
                        "Inner Join TblItem B On A.ItCode=B.ItCode " +
                        "Where A.ItCode=@ItCode",
                        new string[] 
                        {
                            "ItCode", 
                            "ItName", "ItIndex", "ItPlanningGrpCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtItCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtItName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtItIndex.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                            Sm.SetLue(LueItPlanningGrp, Sm.DrStr(dr, c[3]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItCode, "Item code", false) ||
                //Sm.IsTxtEmpty(TxtItIndex, "Index", true) ||
                 Sm.IsLueEmpty(LueItPlanningGrp, "Planning group") ||
                IsItCodeExisted();
        }

        private bool IsItCodeExisted()
        {
            if (mStateInd == "I")
            {
                var cm = new MySqlCommand() { CommandText = "Select ItCode From TblItemIndex Where ItCode=@ItCode Limit 1" };
                Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Item code ( " + TxtItCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;

        }

        #endregion

        #region Additional Method

        private void SetLueItPlanningGrp(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                 ref Lue,
                 "Select ItPlanningGrpCode As Col1, ItPlanningGrpName As Col2 From TblItemPlanningGroup where ActInd='Y';",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Button Click

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            if(BtnItCode.Enabled)
            {
                Sm.FormShowDialog(new FrmItemIndexDlg(this));
            }
        }

        #endregion

        #region Misc Control Event

        private void TxtItIndex_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtItIndex, 0);
        }

        private void LueItPlanningGrp_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItPlanningGrp, new Sm.RefreshLue1(SetLueItPlanningGrp));
        }

        #endregion

        #endregion

     
    }
}
