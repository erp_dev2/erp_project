﻿#region Update
/*
    05/06/2017 [TKG] bug fixing data yg ditampilkan salah ketika menggunakan filter berdasarkan bank account.
    27/11/2018 [TKG] tambah function proses excel untuk kolom yg dimulai dengan 0
    27/07/2020 [IBL/MMM] tambah kolom Voucher's Description ambil dari remark voucher header
    09/08/2020 [WED/SRN] amount nya ngeliat curcode di bank account
    09/11/2020 [TKG/PHT] tambah entity
    26/11/2020 [TKG/IOK] bug amount nya ngeliat curcode di bank account
    13/01/2021 [IBL/IMS] Tambah filter Date berdasarkan parameter IsRptCashBookUseEndDt
    15/01/2021 [IBL/PHT] Mengganti filter multi entity menjadi multi profit center berdasarkan parameter IsFicoUseMultiProfitCenterfilter
    25/01/2020 [TKG/PHT] ubah SetCcbEntCode divalidasi berdasarkan cost center group user
    29/01/2021 [IBL/PHT] Menambah kolom bank code, account number, site bank berdasarkan parameter IsRptCashBookShowDetailBankAccount
    05/02/2021 [TKG/PHT] ubah proses mengambil profit center
    04/03/2021 [DITA/IMS] menampilkan loop ketransaksi voucher berdasarkan parameter : IsRptShowVoucherLoop
    13/03/2021 [TKG/PHT] Berdasarkan parameter mIsRptCashBookBankAcCode2FilterByBankAccountDisabled, apakah filter bank account untuk bank account penerima diaktifkan atau tidak.
    07/03/2021 [TKG/PHT] profit center divalidasi berdasarkan parent juga.
    16/03/2021 [TKG/PHT] divalidasi berdasarkan group profit center.
    16/03/2021 [TKG/PHT] Untuk voucher dengan switching bank account, yg juga divalidasi adalah bank penerimanya juga.
    12/04/2021 [TKG/PHT] filter multi profit center diurutkan berdasarkan kodenya.
    29/10/2021 [TYO/AMKA] Menambah kolom CashType dan CashType2
    11/11/2021 [VIN/ALL] BUG: Redundant data krn penambahan cashtype
    21/12/2021 [VIN/ALL] BUG: Redundant data krn penambahan cashtype dan CashType2
    10/03/2022 [DITA/PHT] Closing balance in cash di pht based on profit center -> param : IsClosingBalanceInCashBasedOnMultiProfitCenter
    18/03/2022 [DITA/PHT] saat menampilkan data opening balance, tambah dari voucher yg transfer -> bankaccode2
    23/03/2022 [DITA/AMKA] Angka di Closing Balance In Cash/Bank Acc tidak sama dengan cashbook
    30/03/2022 [DITA/PHT] masih ada bug saat validasi closing balance in cash profit center aktif setelah penambahan voucher yg transfer -> bankaccode2
    27/12/2022 [VIN/PHT] BUG Excel Document
    24/03/2023 [WED/PHT] bank account type yang ada di parameter AccTypeCashAndBankAccNotForCashBook gak muncul di cashbook
    04/04/2023 [MYA/BBT] Menghubungkan Voucher (switching bank acc) with additional cost and others ke Reporting Cashbook agar additional cost and others tidak masuk pada Cashbook Bank Debit To
    05/03/2023 [WED/PHT] bug fix filter parameter belum masuk
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptCashBook : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mMainCurCode = string.Empty,
            mAccTypeCashAndBankAccNotForCashBook = string.Empty
            ;
        private bool
            mIsFilterByBankAccount = false,
            mIsRptCashBookUseEndDt = false,
            mIsFicoUseMultiProfitCenterFilter = false,
            mIsFicoUseMultiEntityFilter = false,
            mIsRptCashBookShowDetailBankAccount = false,
            mIsRptCashBookUseProfitCenter = false,
            mIsRptShowVoucherLoop = false,
            mIsRptCashBookBankAcCode2FilterByBankAccountDisabled = false,
            mIsAllProfitCenterSelected = false,
            mIsRptCashBookShowCashTypeInfo = false,
            mIsClosingBalanceInCashBasedOnMultiProfitCenter = false,
            mIsVoucherUseAdditionalCost = false;
        internal bool mVoucherAccessInd = false;
        private decimal mClosingBalanceInCashProfitCenterLevelToBeValidated = 0m;
        private List<String> mlProfitCenter = null;

        #endregion

        #region Constructor

        public FrmRptCashBook(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetLueDt(LueDt);
                Sm.SetLue(LueDt, CurrentDateTime.Substring(6, 2));
                SetLueBankAcCode(ref LuebankAcCode);
                if (!mIsRptCashBookUseEndDt)
                {
                    LblDt.Visible = LueDt.Visible = false;
                    MoveComponent(ref label1, 8, 6); MoveComponent(ref LueMth, 55, 4);
                    MoveComponent(ref label4, 18, 29); MoveComponent(ref LueYr, 55, 26);
                }
                if (mIsRptCashBookUseProfitCenter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            //mMainCurCode = Sm.GetParameter("MainCurCode");
            //mIsRptCashBookUseEndDt = Sm.GetParameterBoo("IsRptCashBookUseEndDt");
            //mIsFicoUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiProfitCenterFilter");
            //mIsFicoUseMultiEntityFilter = Sm.GetParameterBoo("IsFicoUseMultiEntityFilter");
            //mIsRptCashBookShowDetailBankAccount = Sm.GetParameterBoo("IsRptCashBookShowDetailBankAccount");
            //mIsRptCashBookUseProfitCenter = Sm.GetParameterBoo("IsRptCashBookUseProfitCenter");
            //mIsRptShowVoucherLoop = Sm.GetParameterBoo("IsRptShowVoucherLoop");
            //mIsFilterByBankAccount = Sm.GetParameterBoo("IsFilterByBankAccount");
            mIsRptCashBookShowCashTypeInfo = Sm.GetParameterBoo("IsRptCashBookShowCashTypeInfo");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MainCurCode', 'IsRptCashBookUseEndDt', 'IsFicoUseMultiProfitCenterFilter', 'IsFicoUseMultiEntityFilter', 'IsRptCashBookShowDetailBankAccount', ");
            SQL.AppendLine("'IsRptCashBookUseProfitCenter', 'IsRptShowVoucherLoop', 'IsFilterByBankAccount', 'IsRptCashBookBankAcCode2FilterByBankAccountDisabled', ");
            SQL.AppendLine("'IsClosingBalanceInCashBasedOnMultiProfitCenter', 'ClosingBalanceInCashProfitCenterLevelToBeValidated', 'AccTypeCashAndBankAccNotForCashBook', ");
            SQL.AppendLine("'IsVoucherUseAdditionalCost');");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptCashBookUseEndDt": mIsRptCashBookUseEndDt = ParValue == "Y"; break;
                            case "IsFicoUseMultiProfitCenterFilter": mIsFicoUseMultiProfitCenterFilter = ParValue == "Y"; break;
                            case "IsFicoUseMultiEntityFilter": mIsFicoUseMultiEntityFilter = ParValue == "Y"; break;
                            case "IsRptCashBookShowDetailBankAccount": mIsRptCashBookShowDetailBankAccount = ParValue == "Y"; break;
                            case "IsRptCashBookUseProfitCenter": mIsRptCashBookUseProfitCenter = ParValue == "Y"; break;
                            case "IsRptShowVoucherLoop": mIsRptShowVoucherLoop = ParValue == "Y"; break;
                            case "IsFilterByBankAccount": mIsFilterByBankAccount = ParValue == "Y"; break;
                            case "IsRptCashBookBankAcCode2FilterByBankAccountDisabled": mIsRptCashBookBankAcCode2FilterByBankAccountDisabled = ParValue == "Y"; break;
                            case "IsClosingBalanceInCashBasedOnMultiProfitCenter": mIsClosingBalanceInCashBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsVoucherUseAdditionalCost": mIsVoucherUseAdditionalCost = ParValue == "Y"; break;

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;

                            case "ClosingBalanceInCashProfitCenterLevelToBeValidated":
                                if (ParValue.Length > 0)
                                    mClosingBalanceInCashProfitCenterLevelToBeValidated = decimal.Parse(ParValue);
                                break;

                            case "AccTypeCashAndBankAccNotForCashBook": mAccTypeCashAndBankAccNotForCashBook = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Bank Account",
                        "Document#",
                        "Date", 
                        "Month",
                        "Description", 
                        
                        //6-10
                        "Debet", 
                        "Credit",
                        "Remark",
                        "Opening",
                        "Balanced",

                        //11-15
                        "PIC",
                        "BankAcCode",
                        "DocDt",
                        "CurCode",
                        "ExcRate",

                        //16-20
                        "DNo",
                        "Entity",
                        "Bank Code",
                        "Account Number",
                        "Site Bank",

                        //21-23
                        "",
                        "Cash Type",
                        "Cash Type 2"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 130, 100, 100, 300, 
                        
                        //6-10
                        150, 150, 150, 150, 150,

                        //11-15
                        150, 0, 0, 0, 0, 

                        //16-20
                        0, 200, 100, 200, 200,

                        //21-23
                        20, 150, 150
                    }
                );


            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 9, 10, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 9, 12, 13, 14, 15, 16, 18, 19, 20, 23 }, false);
            Sm.GrdColButton(Grd1, new int[] { 21 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16, 17, 18, 19,20, 22, 23 });

            if (!mIsRptShowVoucherLoop) Sm.GrdColInvisible(Grd1, new int[] { 21 }, false);
            Grd1.Cols[21].Move(4);

            if (mIsRptCashBookShowDetailBankAccount)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20 }, true);
                Grd1.Cols[18].Move(4);
                Grd1.Cols[19].Move(5);
                Grd1.Cols[20].Move(6);
            }

            mVoucherAccessInd = GetAccessInd("FrmVoucher");

            if (!mVoucherAccessInd)
                Sm.GrdColInvisible(Grd1, new int[] { 21 });

            if (!mIsRptCashBookShowCashTypeInfo)
                Sm.GrdColInvisible(Grd1, new int[] { 22, 23 }, false);

            Grd1.Cols[22].Move(2);
            Grd1.Cols[23].Move(3);
        }

        override protected void HideInfoInGrd()
        {
            if (mIsRptCashBookShowCashTypeInfo)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 23 }, !ChkHideInfoInGrd.Checked);
            }
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueDt, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueYr, "Month")
                ) return;

            string
                Year = Sm.GetLue(LueYr),
                Month = Sm.GetLue(LueMth),
                Dt = Sm.GetLue(LueDt),
                Filter = string.Empty,
                Query = string.Empty;
            var SQL = new StringBuilder();
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                SetProfitCenter();

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@MonthFilter", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Year", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Dt", Sm.GetLue(LueDt));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuebankAcCode), "BankAcCode", true);
                Sm.CmParam<Decimal>(ref cm, "@ClosingBalanceInCashProfitCenterLevelToBeValidated", mClosingBalanceInCashProfitCenterLevelToBeValidated);

                if (mIsRptCashBookUseProfitCenter)
                {
                    //if (ChkProfitCenterCode.Checked)
                    //{
                    //    Query = "    And C.SiteCode Is Not Null ";
                    //    Query += "    And C.SiteCode In ( ";
                    //    Query += "        Select Distinct SiteCode ";
                    //    Query += "        From TblSite ";
                    //    Query += "        Where ProfitCenterCode Is Not Null ";
                    //    Query += "        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                    //    Query += "    ) ";
                    //}

                    Query = "    And C.SiteCode Is Not Null ";
                    Query += "    And C.SiteCode In ( ";
                    Query += "        Select Distinct SiteCode ";
                    Query += "        From TblSite ";
                    Query += "        Where ProfitCenterCode Is Not Null ";

                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter2 = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += " (ProfitCenterCode=@ProfitCenter" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter" + i.ToString(), x);
                            i++;
                        }
                        if (Filter2.Length == 0)
                            Query +=  "    And 1=0 ";
                        else
                            Query += "    And (" + Filter2 + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            Query += "    And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); 
                        }
                        else
                        {
                            Query += "    And ProfitCenterCode In ( ";
                            Query += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            Query += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            Query += "    ) ";
                        }
                    }

                    Query += "    ) ";

                    if (!ChkProfitCenterCode.Checked)
                    {
                        Query = "    And (C.SiteCode Is Null Or (C.SiteCode Is Not Null And C.SiteCode In ( ";
                        Query += "        Select Distinct SiteCode From TblGroupSite ";
                        Query += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        Query += "        ))) ";
                    }
                }


                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("Select Distinct T.DocDt, T2.CurCode, T2.BankAcTp, T.ExcRate, T.BankAcCode, Concat(T2.BankAcNm, Case When T2.BankAcNo Is Null Then '' Else Concat('-', T2.BankAcNo) End) As BankAcNm, ");
                SQL.AppendLine("T.DocNo, T.DNo, T.VcYear, T.VcDate, T.VcMonth, T3.EntName, T2.BankAcNo, T4.SiteName, ");
                if(mIsRptCashBookShowCashTypeInfo)
                    SQL.AppendLine("T5.CashTypeName, T6.CashTypeName as CashTypeName2, ");
                else
                    SQL.AppendLine("NULL As CashTypeName, NULL As CashTypeName2, ");

                SQL.AppendLine("Case T.VCMonth When '01' Then 'January' ");
                SQL.AppendLine("        When '02' Then 'February' ");
                SQL.AppendLine("        When '03' Then 'March'  ");
                SQL.AppendLine("        When '04' Then 'April' ");
                SQL.AppendLine("        When '05' Then 'May' ");
                SQL.AppendLine("        When '06' Then 'June' ");
                SQL.AppendLine("        When '07' Then 'July' ");
                SQL.AppendLine("        When '08' Then 'August' ");
                SQL.AppendLine("        When '09' Then 'September' ");
                SQL.AppendLine("        When '10' Then 'October' ");
                SQL.AppendLine("        When '11' Then 'November' ");
                SQL.AppendLine("        When '12' Then 'December' End As Month, ");
                SQL.AppendLine("T.Description,  ");
                SQL.AppendLine("T.Debit, T.Credit, T.Opening, ");
                SQL.AppendLine("if(@prev != T.BankAcCode, @bal:=((T.Opening+T.Debit)-T.Credit), @bal:= @bal+(Debit-Credit)) As Balanced, ");
                SQL.AppendLine("@prev:=T.BankAcCode, T1.Username, T.CreateDt, T.VoucherRemark  ");
                SQL.AppendLine("From(  ");
                SQL.AppendLine("    Select A.DocDt, A.ExcRate, A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, B.Description, ");
                SQL.AppendLine("    0.00 As Debit, B.Amt As Credit, 0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                SQL.AppendLine(Query);
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
                SQL.AppendLine("    Where Substring(A.DocDt, 5, 2) = @MonthFilter ");
                SQL.AppendLine("    And Left(A.DocDt, 4)=@Year ");
                if (mIsRptCashBookUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.BankAcCode Is Not Null ");
                SQL.AppendLine("    And A.AcType='C' ");
                if (mIsVoucherUseAdditionalCost)
                    SQL.AppendLine("      AND B.ExpensesInd = 'N' ");
                if (mIsFilterByBankAccount)
                {
                    SQL.AppendLine("And A.BankAcCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("      Union All ");
                SQL.AppendLine("      Select A.DocDt, A.ExcRate, ");
                SQL.AppendLine("      A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
                SQL.AppendLine("      B.Description, B.Amt As Debit, ");
                SQL.AppendLine("      0.00 As Credit, 0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
                SQL.AppendLine("      From TblVoucherHdr A ");
                SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                SQL.AppendLine(Query);
                SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
                SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter And Left(A.DocDt, 4)=@Year And A.CancelInd='N' ");
                if (mIsRptCashBookUseEndDt)
                    SQL.AppendLine("      And Right(A.DocDt, 2) <= @Dt ");
                SQL.AppendLine("      And A.BankAcCode Is Not Null ");
                SQL.AppendLine("      And A.AcType='D' ");
                if (mIsVoucherUseAdditionalCost)
                    SQL.AppendLine("      AND B.ExpensesInd = 'N' ");
                if (mIsFilterByBankAccount)
                {
                    SQL.AppendLine("And A.BankAcCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mIsRptCashBookBankAcCode2FilterByBankAccountDisabled)
                {
                    //////////////////
                    SQL.AppendLine("      Union All ");
                    SQL.AppendLine("      Select A.DocDt, A.ExcRate, A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
                    SQL.AppendLine("      B.Description, 0.00 As Debit, ");
                    SQL.AppendLine("      (B.Amt*A.ExcRate) As Credit, ");
                    SQL.AppendLine("      0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
                    SQL.AppendLine("      From TblVoucherHdr A  ");
                    SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode2=C.BankAcCode ");
                    SQL.AppendLine(Query);
                    SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
                    SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter And Left(A.DocDt, 4) = @Year And A.CancelInd='N' ");
                    if (mIsRptCashBookUseEndDt)
                        SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
                    SQL.AppendLine("      And A.AcType2 Is Not Null ");
                    SQL.AppendLine("      And A.AcType2='C' ");
                    if (mIsVoucherUseAdditionalCost)
                        SQL.AppendLine("      AND B.ExpensesInd = 'N' ");
                    if (mIsFilterByBankAccount)
                    {
                        SQL.AppendLine("And A.BankAcCode2 Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                        SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode2, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    SQL.AppendLine("      Union All ");
                    SQL.AppendLine("      Select A.DocDt, A.ExcRate, A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
                    SQL.AppendLine("      B.Description, (B.Amt*A.ExcRate) As Debit, ");
                    SQL.AppendLine("      0.00 As Credit, 0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
                    SQL.AppendLine("      From TblVoucherHdr A ");
                    SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode2=C.BankAcCode ");
                    SQL.AppendLine(Query);
                    SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
                    SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter ");
                    SQL.AppendLine("      And Left(A.DocDt, 4)=@Year ");
                    if (mIsRptCashBookUseEndDt)
                        SQL.AppendLine("      And Right(A.DocDt, 2) <= @Dt ");
                    SQL.AppendLine("      And A.CancelInd='N' ");
                    SQL.AppendLine("      And A.AcType2 Is Not Null ");
                    SQL.AppendLine("      And A.AcType2='D' ");
                    if (mIsVoucherUseAdditionalCost)
                        SQL.AppendLine("      AND B.ExpensesInd = 'N' ");
                    if (mIsFilterByBankAccount)
                    {
                        SQL.AppendLine("And A.BankAcCode2 Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                        SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode2, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    /////////////////
                }
                else
                {
                    SQL.AppendLine("      Union All ");
                    SQL.AppendLine("      Select A.DocDt, A.ExcRate, A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
                    SQL.AppendLine("      B.Description, 0.00 As Debit, ");
                    SQL.AppendLine("      (B.Amt*A.ExcRate) As Credit, ");
                    SQL.AppendLine("      0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
                    SQL.AppendLine("      From TblVoucherHdr A  ");
                    SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                    SQL.AppendLine(Query);
                    SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
                    SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter And Left(A.DocDt, 4) = @Year And A.CancelInd='N' ");
                    if (mIsRptCashBookUseEndDt)
                        SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
                    SQL.AppendLine("      And A.Actype2 Is Not Null ");
                    SQL.AppendLine("      And A.Actype2='C' ");
                    if (mIsVoucherUseAdditionalCost)
                        SQL.AppendLine("      AND B.ExpensesInd = 'N' ");
                    if (mIsFilterByBankAccount)
                    {
                        SQL.AppendLine("And A.BankAcCode2 Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                        SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode2, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    SQL.AppendLine("      Union All ");
                    SQL.AppendLine("      Select A.DocDt, A.ExcRate, A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
                    SQL.AppendLine("      B.Description, (B.Amt*A.ExcRate) As Debit, ");
                    SQL.AppendLine("      0.00 As Credit, 0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
                    SQL.AppendLine("      From TblVoucherHdr A ");
                    SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                    SQL.AppendLine(Query);
                    SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
                    SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter ");
                    SQL.AppendLine("      And Left(A.DocDt, 4)=@Year ");
                    if (mIsRptCashBookUseEndDt)
                        SQL.AppendLine("      And Right(A.DocDt, 2) <= @Dt ");
                    SQL.AppendLine("      And A.CancelInd='N' ");
                    SQL.AppendLine("      And A.Actype2 Is Not Null ");
                    SQL.AppendLine("      And A.Actype2='D' ");
                    if (mIsVoucherUseAdditionalCost)
                        SQL.AppendLine("      AND B.ExpensesInd = 'N' ");
                    if (mIsFilterByBankAccount)
                    {
                        SQL.AppendLine("And A.BankAcCode2 Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                        SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode2, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");   
                    }
                }
                if (mIsVoucherUseAdditionalCost)
                {
                    SQL.AppendLine("UNION ALL  ");
                    SQL.AppendLine("		Select A.DocDt, A.ExcRate, B.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear,  ");
                    SQL.AppendLine("      E.Remark As Description, ");
                    SQL.AppendLine("      case ");
                    SQL.AppendLine("		when A.AcType = 'D' AND A.CurCode != 'IDR' then (E.Amt*A.ExcRate) ");
                    SQL.AppendLine("		when A.AcType = 'D' AND A.CurCode = 'IDR' then E.Amt ");
                    SQL.AppendLine("		ELSE 0.00 ");
                    SQL.AppendLine("		END AS Debit,  ");
                    SQL.AppendLine("      case ");
                    SQL.AppendLine("		when A.AcType = 'C' AND A.CurCode != 'IDR' then (E.Amt*A.ExcRate) ");
                    SQL.AppendLine("		when A.AcType = 'C' AND A.CurCode = 'IDR' then E.Amt ");
                    SQL.AppendLine("		ELSE 0.00 ");
                    SQL.AppendLine("		END AS Credit, ");
                    SQL.AppendLine("		0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2  ");
                    SQL.AppendLine("      From TblVoucherHdr A  ");
                    SQL.AppendLine("      Inner Join tblvoucherdtl4 B On A.DocNo = B.DocNo AND A.BankAcCode = B.BankAcCode ");
                    SQL.AppendLine("      Inner Join TblBankAccount C ON B.BankAcCode = C.BankAcCode  ");
                    SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode'  ");
                    SQL.AppendLine("      INNER JOIN tblexpensestypedtl E ON B.ExpensesCode = E.ExpensesCode AND B.ExpensesDNo = E.DNo ");
                    SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter  ");
                    SQL.AppendLine("      And Left(A.DocDt, 4)=@Year  ");
                    SQL.AppendLine("      And A.CancelInd='N'  ");
                    SQL.AppendLine("      And A.Actype Is Not NULL ");
                    SQL.AppendLine("And A.BankAcCode Is Not Null  ");
                    SQL.AppendLine("And Exists(  ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount  ");
                    SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode, '')  ");
                    SQL.AppendLine("    And GrpCode In (  ");
                    SQL.AppendLine("        Select GrpCode From TblUser  ");
                    SQL.AppendLine("        Where UserCode=@UserCode  ");
                    SQL.AppendLine("    )  ");
                    SQL.AppendLine(")  ");
                    SQL.AppendLine("		UNION ALL ");
                    SQL.AppendLine("		Select A.DocDt, A.ExcRate, B.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear,  ");
                    SQL.AppendLine("      E.Remark As Description, ");
                    SQL.AppendLine("      case ");
                    SQL.AppendLine("		when A.AcType2 = 'D' AND A.CurCode2 != 'IDR' then (E.Amt*A.ExcRate) ");
                    SQL.AppendLine("		when A.AcType2 = 'D' AND A.CurCode2 = 'IDR' then E.Amt ");
                    SQL.AppendLine("		ELSE 0.00 ");
                    SQL.AppendLine("		END AS Debit,  ");
                    SQL.AppendLine("      case ");
                    SQL.AppendLine("		when A.AcType2 = 'C' AND A.CurCode2 != 'IDR' then (E.Amt*A.ExcRate) ");
                    SQL.AppendLine("		when A.AcType2 = 'C' AND A.CurCode2 = 'IDR' then E.Amt ");
                    SQL.AppendLine("		ELSE 0.00 ");
                    SQL.AppendLine("		END AS Credit, ");
                    SQL.AppendLine("		0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2  ");
                    SQL.AppendLine("      From TblVoucherHdr A  ");
                    SQL.AppendLine("      Inner Join tblvoucherdtl4 B On A.DocNo = B.DocNo AND A.BankAcCode2 = B.BankAcCode ");
                    SQL.AppendLine("      Inner Join TblBankAccount C ON B.BankAcCode = C.BankAcCode  ");
                    SQL.AppendLine(" ");
                    SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode'  ");
                    SQL.AppendLine("      INNER JOIN tblexpensestypedtl E ON B.ExpensesCode = E.ExpensesCode AND B.ExpensesDNo = E.DNo ");
                    SQL.AppendLine("       ");
                    SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter  ");
                    SQL.AppendLine("      And Left(A.DocDt, 4)=@Year  ");
                    SQL.AppendLine("      And A.CancelInd='N'  ");
                    SQL.AppendLine("      And A.Actype2 Is Not NULL ");
                    SQL.AppendLine("And A.BankAcCode2 Is Not Null  ");
                    SQL.AppendLine("And Exists(  ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount  ");
                    SQL.AppendLine("    Where BankAcCode2=IfNull(A.BankAcCode2, '')  ");
                    SQL.AppendLine("    And GrpCode In (  ");
                    SQL.AppendLine("        Select GrpCode From TblUser  ");
                    SQL.AppendLine("        Where UserCode=@UserCode  ");
                    SQL.AppendLine("    )  ");
                    SQL.AppendLine(")  ");
                }
                SQL.AppendLine("      Union All ");
                SQL.AppendLine("      Select A.DocDt, 0.00 As ExcRate, B.BankAcCode, A.DocNo, '001', '01',  @MonthFilter As VcMonth,  @Year,  ");
                SQL.AppendLine("      'Opening Balance', 0.00 As Debit, 0.00 As Credit, ifnull(B.Amt, 0) As Opening, A.Createby, concat(@year,@MonthFilter,'01','0002') As CreateDt, ");
                SQL.AppendLine("      A.Remark AS VoucherRemark, D.CashTypeCode, D.CashTypeCode2 ");
                SQL.AppendLine("      From tblClosingBalanceIncashHdr A  ");
                SQL.AppendLine("      Inner Join TblClosingBalanceIncashDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' And CancelReason Is Null ");
                SQL.AppendLine("      Inner Join TblVoucherHdr D On B.BankAcCode = D.BankAcCode  ");
                if (mIsFilterByBankAccount)
                {
                    SQL.AppendLine("And B.BankAcCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=IfNull(B.BankAcCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("      Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode ");
                if (!mIsClosingBalanceInCashBasedOnMultiProfitCenter)
                    SQL.AppendLine(Query);
                else
                {
                    if (mIsRptCashBookUseProfitCenter)
                    {
                        SQL.AppendLine(" And A.ProfitCenterCode Is Not Null ");

                        if (!mIsAllProfitCenterSelected)
                        {
                            var Filter3 = string.Empty;
                            int j = 0;
                            foreach (var x in mlProfitCenter.Distinct())
                            {
                                if (Filter3.Length > 0) Filter3 += " Or ";
                                Filter3 += " (A.ProfitCenterCode=@ProfitCenterA" + j.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@ProfitCenterA" + j.ToString(), x);
                                j++;
                            }
                            if (Filter3.Length == 0)
                                SQL.AppendLine("    And 1=0 ");
                            else
                                SQL.AppendLine("    And (" + Filter3 + ") ");
                        }
                        else
                        {
                            if (ChkProfitCenterCode.Checked)
                            {
                                SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                                if (ChkProfitCenterCode.Checked)
                                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                            }
                            else
                            {
                                SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                                SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                SQL.AppendLine("    ) ");
                            }

                        }
                        SQL.AppendLine("    And A.ProfitCenterCode In (Select ProfitCenterCode From TblProfitCenter Where Level>=@ClosingBalanceInCashProfitCenterLevelToBeValidated)  ");



                        if (!ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("   And (C.SiteCode Is Null Or (C.SiteCode Is Not Null And C.SiteCode In ( ");
                            SQL.AppendLine("        Select Distinct SiteCode From TblGroupSite ");
                            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        ))) ");
                        }
                    }
                }
                SQL.AppendLine("      where ");
                if (Sm.GetLue(LueMth) == "01")
                {
                    int Yr = Convert.ToInt32(Sm.GetLue(LueYr)) - 1;
                    SQL.AppendLine(" A.yr = '" + Yr + "' ");
                    SQL.AppendLine(" And A.Mth = '12' ");
                }
                else
                {
                    SQL.AppendLine(" A.yr = @year and A.Mth =   Cast(@MonthFilter As Decimal(10,2)) -1 ");
                }
                SQL.AppendLine("Group By B.BankAcCode ");

                if (mIsClosingBalanceInCashBasedOnMultiProfitCenter)
                {
                    SQL.AppendLine("      Union All ");
                    SQL.AppendLine("      Select A.DocDt, 0.00 As ExcRate, B.BankAcCode, A.DocNo, '001', '01',  @MonthFilter As VcMonth,  @Year,  ");
                    SQL.AppendLine("      'Opening Balance', 0.00 As Debit, 0.00 As Credit, ifnull(B.Amt, 0) As Opening, A.Createby, concat(@year,@MonthFilter,'01','0002') As CreateDt, ");
                    SQL.AppendLine("      A.Remark AS VoucherRemark, D.CashTypeCode, D.CashTypeCode2 ");
                    SQL.AppendLine("      From tblClosingBalanceIncashHdr A  ");
                    SQL.AppendLine("      Inner Join TblClosingBalanceIncashDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' And CancelReason Is Null ");
                    SQL.AppendLine("      Inner Join TblVoucherHdr D On B.BankAcCode = D.BankAcCode2  ");
                    if (mIsFilterByBankAccount)
                    {
                        SQL.AppendLine("And B.BankAcCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                        SQL.AppendLine("    Where BankAcCode=IfNull(B.BankAcCode, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    SQL.AppendLine("      Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode ");
                    if (!mIsClosingBalanceInCashBasedOnMultiProfitCenter)
                        SQL.AppendLine(Query);
                    else
                    {
                        if (mIsRptCashBookUseProfitCenter)
                        {
                            SQL.AppendLine(" And A.ProfitCenterCode Is Not Null ");

                            if (!mIsAllProfitCenterSelected)
                            {
                                var Filter3 = string.Empty;
                                int j = 0;
                                foreach (var x in mlProfitCenter.Distinct())
                                {
                                    if (Filter3.Length > 0) Filter3 += " Or ";
                                    Filter3 += " (A.ProfitCenterCode=@ProfitCenterB" + j.ToString() + ") ";
                                    Sm.CmParam<String>(ref cm, "@ProfitCenterB" + j.ToString(), x);
                                    j++;
                                }
                                if (Filter3.Length == 0)
                                    SQL.AppendLine("    And 1=0 ");
                                else
                                    SQL.AppendLine("    And (" + Filter3 + ") ");
                            }
                            else
                            {
                                if (ChkProfitCenterCode.Checked)
                                {
                                    SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                                    if (ChkProfitCenterCode.Checked)
                                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                                }
                                else
                                {
                                    SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                                    SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                    SQL.AppendLine("    ) ");
                                }

                            }
                            SQL.AppendLine("    And A.ProfitCenterCode In (Select ProfitCenterCode From TblProfitCenter Where Level>=@ClosingBalanceInCashProfitCenterLevelToBeValidated)  ");



                            if (!ChkProfitCenterCode.Checked)
                            {
                                SQL.AppendLine("   And (C.SiteCode Is Null Or (C.SiteCode Is Not Null And C.SiteCode In ( ");
                                SQL.AppendLine("        Select Distinct SiteCode From TblGroupSite ");
                                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                SQL.AppendLine("        ))) ");
                            }
                        }
                    }
                    SQL.AppendLine("      where ");
                    if (Sm.GetLue(LueMth) == "01")
                    {
                        int Yr = Convert.ToInt32(Sm.GetLue(LueYr)) - 1;
                        SQL.AppendLine(" A.yr = '" + Yr + "' ");
                        SQL.AppendLine(" And A.Mth = '12' ");
                    }
                    else
                    {
                        SQL.AppendLine(" A.yr = @year and A.Mth =   Cast(@MonthFilter As Decimal(10,2)) -1 ");
                    }
                    SQL.AppendLine("Group By B.BankAcCode ");
                }

                SQL.AppendLine("      Order By BankAcCode, VcDate, VcMonth, CreateDt ");

                SQL.AppendLine(") T ");
                SQL.AppendLine("Left Join TblUser T1 On T.Pic = T1.UserCode ");
                SQL.AppendLine("Inner Join TblBankAccount T2 On T.BankAcCode=T2.BankAcCode ");
                SQL.AppendLine("Left Join TblEntity T3 On T2.EntCode=T3.EntCode ");
                SQL.AppendLine("Left Join TblSite T4 On T2.SiteCode=T4.SiteCode ");
                SQL.AppendLine("Left Join TblCashType T5 On T.CashTypeCode = T5.CashTypeCode ");
                SQL.AppendLine("Left Join TblCashType T6 On T.CashTypeCode2 = T6.CashTypeCode ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("       Select @bal := 0, @prev:='' ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine(") Tbl ");
                SQL.AppendLine(Filter);

                if (mAccTypeCashAndBankAccNotForCashBook.Length > 0)
                {
                    SQL.AppendLine(FilterBankAccountType(Filter, mAccTypeCashAndBankAccNotForCashBook));
                }

                SQL.AppendLine(" Order By BankAcCode, VcDate, DocNo, Dno, VcMonth, CreateDt; ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "BankAcNm", 

                        //1-5
                        "DocNo", "VcDate", "Month", "Description", "Debit",

                        //6-10
                        "Credit", "VoucherRemark", "Opening", "Balanced", "UserName", 

                        //11-15
                        "BankAcCode", "DocDt", "CurCode", "ExcRate", "DNo", 
                        
                        //16-20
                        "EntName", "BankAcNo", "SiteName", "CashTypeName", "CashTypeName2" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    }, true, false, false, false
                    );

                //ProcessCurrencyAmt();
                Bal();
                //Grd1.GroupObject.Add(1);
                //Grd1.Group();
                Grd1.Rows.CollapseAll();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
            
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 7 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
            //Bal();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Contains("VC"))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Contains("VC"))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        public string FilterBankAccountType(string Filter, string mAccTypeCashAndBankAccNotForCashBook)
        {
            var SQL = new StringBuilder();

            string bankAcTp = string.Empty;
            if (mAccTypeCashAndBankAccNotForCashBook.Contains(","))
            {
                string[] bankAcTps = mAccTypeCashAndBankAccNotForCashBook.Split(',');
                foreach (var x in bankAcTps)
                {
                    if (bankAcTp.Length > 0) bankAcTp += ", ";
                    bankAcTp += string.Concat("'", x, "'");
                }
            }
            else
            {
                bankAcTp = string.Concat("'", mAccTypeCashAndBankAccNotForCashBook, "'");
            }

            if (Filter.Length == 0) SQL.AppendLine("Where ");
            else SQL.AppendLine("And ");
            SQL.AppendLine("Tbl.BankAcTp Not In (" + bankAcTp + ") ");

            return SQL.ToString();
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptCashBookUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                    SQL.AppendLine("    And (" + Filter + ") ");
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        internal bool GetAccessInd(string FormName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(Left(AccessInd, 1), 'N') ");
            SQL.AppendLine("From TblGroupMenu ");
            SQL.AppendLine("Where MenuCode In ( Select MenuCode From TblMenu Where Param = @Param1 ) ");
            SQL.AppendLine("And GrpCode In ");
            SQL.AppendLine("(Select GrpCode From TblUser Where UserCode = @Param2) ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), FormName, Gv.CurrentUserCode, string.Empty) == "Y";
        }

        private void Bal()
        {
            string b = string.Empty;
            decimal balance = 0m;
            for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
            {
                if (Grd1.Rows[row].Type == iGRowType.Normal)
                {
                    if (b != Sm.GetGrdStr(Grd1, row, 1))
                    {
                        balance = 0m;
                        b = Sm.GetGrdStr(Grd1, row, 1);
                    }
                    Grd1.Cells[row, 10].Value = balance + Sm.GetGrdDec(Grd1, row, 6) + Sm.GetGrdDec(Grd1, row, 9) - Sm.GetGrdDec(Grd1, row, 7);
                    balance = Sm.GetGrdDec(Grd1, row, 10);
                }
            }
        }

        private void SetLueBankAcCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select BankAcCode As Col1, BankAcDesc As Col2 From ( ");
                SQL.AppendLine("    Select BankAcCode, ");
                SQL.AppendLine("    Concat( ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(B.BankName, ' : ') Else '' End, ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End ");
                SQL.AppendLine("    ) As BankAcDesc ");
                SQL.AppendLine("    From TblBankAccount A ");
                SQL.AppendLine("    Left Join TblBank B On A.BankCode=B.BankCode ");
                if (mIsFilterByBankAccount)
                {
                    SQL.AppendLine("    Where A.BankAcCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("        Where BankAcCode=IfNull(A.BankAcCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                }

                if (mAccTypeCashAndBankAccNotForCashBook.Length > 0)
                {
                    string Filter = " ";
                    SQL.AppendLine(FilterBankAccountType(Filter,mAccTypeCashAndBankAccNotForCashBook).Replace("Tbl.", "A."));
                }

                SQL.AppendLine(") T Order By BankAcDesc; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                if (mIsFilterByBankAccount)
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 3].Value = "'" + Sm.GetGrdStr(Grd1, Row, 3);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                //Grd1.Cells[Row, 3].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(Grd1, Row, 3).Length - 1);
            }
            Grd1.EndUpdate();
        }

        private void ProcessCurrencyAmt()
        {
            var l = new List<Voucher>();
            var l2 = new List<CurrencyRate>();
            string VCDocNo = string.Empty;

            VCDocNo = GetVoucher(ref l);
            GetCurrencyRate(ref l2, VCDocNo);

            if (l.Count > 0)
            {
                l.RemoveAll(w => w.CurCode == mMainCurCode); // delete all IDR curcode
                l.RemoveAll(w => w.ExcRate != 0m); // delete non-IDR with ExcRate non other than 0

                GetExchageRate(ref l, ref l2);
                ShowAmountAfterRate(ref l);
            }
        }

        private string GetVoucher(ref List<Voucher> l)
        {
            string DocNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (DocNo.Length > 0) DocNo += ",";
                    DocNo += Sm.GetGrdStr(Grd1, i, 2);

                    l.Add(new Voucher()
                    {
                        DocNo = Sm.GetGrdStr(Grd1, i, 2),
                        DNo = Sm.GetGrdStr(Grd1, i, 16),
                        DocDt = Sm.GetGrdStr(Grd1, i, 13),
                        BankAcCode = Sm.GetGrdStr(Grd1, i, 11),
                        CurCode = Sm.GetGrdStr(Grd1, i, 14),
                        ExcRate = Sm.GetGrdDec(Grd1, i, 15),
                        Debit = Sm.GetGrdDec(Grd1, i, 6),
                        Credit = Sm.GetGrdDec(Grd1, i, 7),
                        DebitAfterRate = Sm.GetGrdDec(Grd1, i, 6),
                        CreditAfterRate = Sm.GetGrdDec(Grd1, i, 7),
                    });
                }
            }

            return DocNo;
        }

        private void GetCurrencyRate(ref List<CurrencyRate> l2, string VCDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select CurCode1 CurCode, RateDt, Amt ");
            SQL.AppendLine("From TblCurrencyRate ");
            SQL.AppendLine("Where CurCode1 != @MainCurCode ");
            SQL.AppendLine("And CurCode2 = @MainCurCode ");
            SQL.AppendLine("And Left(RateDt, 6) = @YrMth ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T1.CurCode, T1.DocDt RateDt, T2.RateAmt Amt ");
            SQL.AppendLine("From TblVoucherHdr T1 ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentHdr T2 On T1.VoucherRequestDocNo = T2.VoucherRequestDocNo ");
            SQL.AppendLine("Where T1.DocType = '03' ");
            SQL.AppendLine("And Find_In_set(T1.DocNo, @VCDocNo); ");

            Sm.CmParam<string>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<string>(ref cm, "@VCDocNo", VCDocNo);
            Sm.CmParam<string>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CurCode", "RateDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new CurrencyRate()
                        {
                            CurCode = Sm.DrStr(dr, c[0]),
                            RateDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetExchageRate(ref List<Voucher> l, ref List<CurrencyRate> l2)
        {
            if (l2.Count > 0)
            {
                foreach (var x in l)
                {
                    foreach (var y in l2
                        .Where(
                            w =>
                                w.CurCode == x.CurCode &&
                                Decimal.Parse(x.DocDt) >= Decimal.Parse(w.RateDt)
                            )
                        .OrderByDescending(o => o.RateDt)
                        .Take(1)
                        )
                    {
                        x.ExcRate = y.Amt;
                    }
                }
            }
        }

        private void ShowAmountAfterRate(ref List<Voucher> l)
        {
            foreach(var x in l)
            {
                if (x.ExcRate != 0m)
                {
                    x.DebitAfterRate = x.Debit / x.ExcRate;
                    x.CreditAfterRate = x.Credit / x.ExcRate;
                }
            }

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    foreach(var x in l.Where(w => 
                        w.DocNo == Sm.GetGrdStr(Grd1, i, 2) &&
                        w.DNo == Sm.GetGrdStr(Grd1, i, 16)
                        ))
                    {
                        Grd1.Cells[i, 6].Value = Sm.FormatNum(x.DebitAfterRate, 0);
                        Grd1.Cells[i, 7].Value = Sm.FormatNum(x.CreditAfterRate, 0);
                    }
                }
            }
        }

        private void SetLueDt(LookUpEdit Lue)
        {
            Sl.SetLookUpEdit(Lue,
                new string[] {
                    null,
                    "01", "02", "03", "04", "05",
                    "06", "07", "08", "09", "10",
                    "11", "12", "13", "14", "15",
                    "16", "17", "18", "19", "20",
                    "21", "22", "23", "24", "25",
                    "26", "27", "28", "29", "30",
                    "31"
                });
        }

        private void MoveComponent(ref Label Lbl, int x, int y)
        {
            Lbl.Location = new System.Drawing.Point(x, y);
        }

        private void MoveComponent(ref DXE.LookUpEdit Lue, int x, int y)
        {
            Lue.Location = new System.Drawing.Point(x, y);
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }


        #endregion

        #endregion

        #region Event

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Bank account");
        }

        private void LuebankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuebankAcCode, new Sm.RefreshLue1(SetLueBankAcCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit Center");   
        }

        #endregion

        #region Class

        private class Voucher
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DocDt { get; set; }
            public string BankAcCode { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public decimal Debit { get; set; }
            public decimal Credit { get; set; }
            public decimal DebitAfterRate { get; set; }
            public decimal CreditAfterRate { get; set; }
        }

        private class CurrencyRate
        {
            public string CurCode { get; set; }
            public string RateDt { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion

        
    }
}
