﻿#region Update
/*
    29/11/2019 [TKG/PHT] New Reporting
    02/12/2020 [TKG/PHT] tambah kolom non bpjs
    03/12/2020 [HAR/PHT] bpjs BNI dari SSEeBNILife diganti SSEmployeeBNIlife & SSErBNILife diganti SSEmployerBNIlife
    10/12/2020 [TKG/PHT] tambah komponen brutto
    10/12/2020 [TKG/PHT] tambah komponen netto
    15/12/2020 [TKG/PHT] column DPLK dihilangkan.
    16/12/2020 [TKG/PHT] tambah ssemployeepension dan ssemployerpension di netto dan brutto
    20/12/2020 [TKG/PHT] tambah warning letter
    21/12/2020 [WED/PHT] tambah informasi level, ambil dari master employee. 
                         tambah informasi UMK, ambil dari regional wages
                         tambah informasi Tunjangan kemahalan, ambil dari nilai allowance untuk tunjangan kemahalan
                         tambah UMPP, ambil dari parameter UMPP
    22/12/2020 [WED/PHT] tambah allowance work period
    22/12/2020 [TKG/PHT] ubah Brutto+Netto diambil langsung dari TblPayrollProcess1, Tambah kolom Netto Actual, Netto Adjustment 
    01/01/2021 [TKG/PHT] tambah tunjangan masa kerja
    02/01/2021 [TKG/PHT] tambah indikator karyawan telah meninggal.
    02/01/2021 [TKG/PHT] tambah OT Holiday meal
    25/02/2021 [TKG/PHT] menambah lup untuk menampilkan informasi advance payment
    28/04/2021 [VIN/PHT] menambah ExportToExcel
    30/04/2021 [ICA/PHT] menambah kolom pemilikan rumah, premi kesehatan, taspen, wanaarta, dapen pns, dapen pht
    05/05/2021 [ICA/PHT] bug amount SSP yg muncul disesuaikan dengan EmployeeSS
    11/05/2021 [ICA/PHT] Pemilikan Rumah, Dapen PHT, Dapen PNS, Premi Kesehatan, Taspen, dan Wanaartha dibuat menjadi employer dan employee
    18/05/2021 [TKG/PHT] ubah proses menampilkan nilai ss program menjadi lbh fleksible
    24/05/2021 [TKG/PHT] tambah informasi ss program
    03/02/2022 [BRI/PHT] merubah validasi date payrun & RMW
    15/02/2022 [TKG/PHT] merubah GetParameter(), merubah GetParameter()
    23/02/2022 [ICA/PHT] Breakdown Allowance menjadi kolom, rombak urutan kolom, ubah rumus (brutto, netto, THP) dan penambahan beberapa kolom baru
    24/02/2022 [ICA/PHT] Bug ketika filter site
    02/03/2022 [ICA/PHT] rumus THP seharusnya netto - Fix deduction - employee Advance payment (ada trellonya) 
    08/03/2022 [ICA/PHT] penyesuaian printout
    14/03/2022 [DEV/PHT] Menambahkan field netto actual di payroll process summary karena netto actual untuk menghitung otomatisasi TPJ dan BPJS
    14/03/2022 [DEV/PHT] Menambah komponen warning letter di reporting PPSummary
    17/03/2022 [VIN/PHT] Level tidak di Hide
    18/03/2022 [ICA/PHT] Source brutto dan THP langsung ambil dari PayrollProcess1, karena perhitungan brutto dan THP menggunakan payrollprocessformula
    21/03/2022 [MYA/PHT] Menambah tab komponen tunjangan yang diperoeh ketika karyawan meninggal dunia
    23/03/2022 [MYA/PHT] Feedback : Menambah tab komponen tunjangan yang diperoeh ketika karyawan meninggal dunia
    23/03/2022 [ICA/PHT] Source netto,netto-actual, netto adjustment ambil dari PayrollProcess1, karena perhitungan netto, netto-actual, netto adjustment menggunakan payrollprocessformula
    25/03/2022 [VIN/PHT] BUG warning saat meng excelkan dokumen 
    30/03/2022 [ICA/PHT] memunculkan dan move kolom Voucher Request Payroll
    30/03/2022 [ICA/PHT] menghilangkan passaway ind ketika menampilkan Allowance meninggal dunia
                         kolom total subsidi tidak lagi berdasarkan passaway, Subsidi Hidup + TotalAmtMD (karena kalo meninggal subsidi hidup pasti 0)
    11/05/2022 [ICA/PHT] Detail employee yg muncul sesuai dengan pps terakhir sebelum startdate payrun berdasarkan parameter IsPayslipNotUseCurrentPPS
    15/06/2022 [TYO/PHT] BUG : validasi nilai netto adjustment
    16/06/2022 [DITA/PHT] based on param : IsPayrollAmtRounded semua amount di bulatkan kebawah
    24/06/2022 [TRI/PHT] BUG : printout payslip, semester dan triwulan belum sesuai dengan Bulan
    27/07/2022 [VIN/PHT] IsPayslipNotUseCurrentPPS pakai ifnull 
    23/08/2022 [ICA/PHT] Printout : Membebankan Warning Letter pada Allowance yg tercapture di menu Warning Letter, mengambil nilai BPJS dari PayrollProcess1 jika param IsPayrollProcessingCalculateBPJSAutomatic aktif
                            Memunculkan Tunjangan untuk yg meninggal dunia. 
    25/11/2022 [DITA/PHT] fix allowance ada yg tidak bulat 375.000, bisa kelebihan 1-2 rupiah
    03/01/2022 [WED/PHT] brutto, netto, total subsidi selisih 1 rupiah
    16/03/2023 [WED/PHT] kolom Total Fix Component dirubah langsung ambil dari PayrollProcessAD kode AL012
    28/03/2023 [MAU/PHT] penyesuaian isi kolom month of services berdasarkan join date dan create date
    11/04/2023 [MAU/PHT] penyesuaian kolom monthofservice dan yearofservice berdasarkan VoucherRequestPayrollDocDt
    12/04/2023 [MAU/PHT] selisih month dan year of service berdasarkan antara bulan join date , dan bulan VoucherRequestPayroll DocDt 
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPayrollProcessSummary18 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool
             mIsFilterBySiteHR = false,
             mIsFilterByDeptHR = false,
             mIsRptPensionUseSSRetiredMaxAge2 = false,
             mIsPayslipNotUseCurrentPPS = false,
            mIsPayrollAmtRounded = false;
        private string
            mPensionCalculationFormat = string.Empty,
            mSSRetiredMaxAge = string.Empty,
            mSSRetiredMaxAge2 = string.Empty,
            mADCodePositionAdjustment = string.Empty;
        private List<SSP> mlSSP = null;
        private int
            mLatestCol = 132,
            mADCount = 14,
            mMDCount = 21;

        #endregion

        #region Constructor

        public FrmRptPayrollProcessSummary18(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSSProgram();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParCode In (");
            SQL.AppendLine("'IsFilterBySiteHR', 'IsFilterByDeptHR', 'PensionCalculationFormat', 'SSRetiredMaxAge', 'SSRetiredMaxAge2', ");
            SQL.AppendLine("'IsRptPensionUseSSRetiredMaxAge2', 'ADCodePositionAdjustment', 'IsPayslipNotUseCurrentPPS', 'IsPayrollAmtRounded' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsFilterByDeptHR ": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsRptPensionUseSSRetiredMaxAge2": mIsRptPensionUseSSRetiredMaxAge2 = ParValue == "Y"; break;
                            case "IsPayslipNotUseCurrentPPS": mIsPayslipNotUseCurrentPPS = ParValue == "Y"; break;
                            case "IsPayrollAmtRounded": mIsPayrollAmtRounded = ParValue == "Y"; break;

                            //string
                            case "SSRetiredMaxAge":
                                mSSRetiredMaxAge = ParValue;
                                if (mSSRetiredMaxAge.Length == 0) mSSRetiredMaxAge = "56";
                                break;
                            case "SSRetiredMaxAge2":
                                mSSRetiredMaxAge2 = ParValue;
                                if (mSSRetiredMaxAge2.Length == 0) mSSRetiredMaxAge2 = "56";
                                break;
                            case "PensionCalculationFormat":
                                mPensionCalculationFormat = ParValue;
                                if (mPensionCalculationFormat.Length == 0) mPensionCalculationFormat = "1";
                                break;
                            case "ADCodePositionAdjustment":
                                mADCodePositionAdjustment = ParValue;
                                if (mADCodePositionAdjustment.Length == 0) mADCodePositionAdjustment = "AL015";
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            //wed
            //ada TotalMDAmtT -> Truncated, untuk ngakalin TotSubsidi yg selisih 1 rupiah terus karena perhitungan truncated dan untrancated

            SQL.AppendLine("Select Tbl.*, ");

            //SQL.AppendLine(" (FixAllowance -  ");
            //if (mIsPayrollAmtRounded)
            //    SQL.AppendLine(" Truncate(TotalADAmt, 0) -  Truncate(TotalMDAmt, 0) ");
            //else
            //    SQL.AppendLine(" TotalADAmt -  TotalMDAmt ");

            //SQL.AppendLine(" ) FixAllowanceNew, (Salary + (FixAllowance - ");

            //if (mIsPayrollAmtRounded)
            //    SQL.AppendLine(" Truncate(TotalADAmt, 0) - Truncate(TotalMDAmt, 0) ");
            //else
            //    SQL.AppendLine(" TotalADAmt - TotalMDAmt ");

            //SQL.AppendLine(" )) TotFixComponent, ");

            SQL.AppendLine("IfNull(Tbl2.Amt, 0.00) FixAllowanceNew, (Salary + IfNull(Tbl2.Amt, 0.00)) TotFixComponent, ");

            SQL.AppendLine("(SSBNILife + SSPKES + SSPRMH + SSPension + BPJSEmployment + BPJSHealth + BPJSPension + SSEmployeeDPNPNS + SSEmployeeDPNPHT + SSEmployeeTSP + Tax) TotDeduction, ");
            SQL.AppendLine("(TotalSubsidi + ");

            if (mIsPayrollAmtRounded)
                SQL.AppendLine(" Truncate(TotalMDAmtT, 0) ");
            else
                SQL.AppendLine(" TotalMDAmtT ");

            SQL.AppendLine(" ) TotSubsidi ");

            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select A.PayrunCode, B.PayrunName, A.EmpCode, C.EmpName, C.EmpCodeOld, B.DeptCode, D.DeptName, C.JoinDt, ");
            if (mIsPayslipNotUseCurrentPPS)
                SQL.AppendLine("ifnull(E2.PosName, E.PosName) PosName, ifnull(M2.LevelName, M.LevelName) LevelNameAdd, ");
            else
                SQL.AppendLine("E.PosName, M.LevelName LevelNameAdd, ");
            SQL.AppendLine("C.ResignDt, F.OptDesc As SystemTypeDesc, G.OptDesc As PayrunPeriodDesc, H.PGName, B.SiteCode, J.SiteName, A.NPWP, ");
            
            if (mIsPayrollAmtRounded)
            {
                SQL.AppendLine("I.OptDesc As NonTaxableIncomeDesc, A.Salary, A.WorkingDay, A.PLDay, A.PLHr, A.PLAmt, A.ProcessPLAmt, ");
                SQL.AppendLine("A.UPLDay, A.UPLHr, A.UPLAmt, A.ProcessUPLAmt, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, A.OT1Amt, A.OT2Amt, ");
                SQL.AppendLine("A.OTHolidayAmt, A.TaxableFixAllowance, A.NonTaxableFixAllowance, A.FixAllowance, A.PerformanceValue, ");
                SQL.AppendLine("A.VarAllowance, A.IncEmployee, A.Transport, A.TaxAllowance, A.Brutto, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerHealth, 0.00), 0.00) SSEmployerHealth, A.SSEmployeeHealth, IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerEmployment, 0.00), 0.00) SSEmployerEmployment, A.SSEmployeeEmployment, IF(A.PassAwayInd = 'N', IFNULL(A.SSErPension, 0.00), 0.00) SSErPension, A.SSEePension, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerPension, 0.00), 0.00) SSEmployerPension, A.SSEmployeePension, A.NonTaxableFixDeduction, A.TaxableFixDeduction, A.FixDeduction, A.DedEmployee, ");
                SQL.AppendLine("A.DedProduction, A.DedProdLeave, A.EmpAdvancePayment, A.SalaryAdjustment, ");
                SQL.AppendLine("IF(A.Netto != A.NettoActual, A.Netto-A.NettoActual+A.NettoActual, A.Netto) AS Netto, A.NettoActual, IF(A.NettoActual < IfNull(Truncate(N.MinRegionalWagesAmt,0),0.00) OR A.NettoActual < IfNull(Truncate(P.ParValue,0),0.00), A.Netto-A.NettoActual, 0.00) As NettoAdjustment, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', A.Tax, 0.00) Tax, A.EOYTax, A.Amt, IF(L.Amt Is Null, A.Amt, (A.Amt-Truncate(L.Amt, 0))) As PaidAmt, A.VoucherRequestPayrollDocNo, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerBNILife, 0.00), 0.00) SSEmployerBNILife, A.SSEmployeeBNILife, A.SSEmployerNonBPJS, A.SSEmployeeNonBPJS, IfNull(Truncate(N.MinRegionalWagesAmt, 0), 0.00) MinRegionalWagesAmt, IfNull(Truncate(O.Amt, 0), 0.00) ExpensiveAllowance, A.WarningLetter, IfNull(Truncate(P.ParValue, 0), 0.00) UMPP, ");
                SQL.AppendLine("IfNull(Truncate(Q.WorkPeriodAmt, 0), 0.00) WorkPeriodAmt, A.ServiceYearAllowance, A.PassAwayInd, A.OTMeal, ");
                SQL.AppendLine("A.SSEmployerDPNPHT, A.SSEmployeeDPNPHT, ");
                SQL.AppendLine("A.SSEmployerDPNPNS, A.SSEmployeeDPNPNS, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerPKES, 0.00), 0.00) SSEmployerPKES, A.SSEmployeePKES, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerPRMH, 0.00), 0.00) SSEmployerPRMH, A.SSEmployeePRMH, ");
                SQL.AppendLine("A.SSEmployerTSP, A.SSEmployeeTSP, ");
                SQL.AppendLine("(A.SSEmployerBNILife - A.SSEmployeeBNILife) SSBNILife, (A.SSEmployerPKES - A.SSEmployeePKES) SSPKES, (A.SSEmployerPRMH - A.SSEmployeePRMH) SSPRMH, (A.SSEmployerPension - A.SSEmployeePension) SSPension, ");
                SQL.AppendLine("(A.SSEmployerEmployment+A.SSEmployeeEmployment) BPJSEmployment, (A.SSEmployerHealth+A.SSEmployeeHealth) BPJSHealth, (A.SSErPension+A.SSEePension) BPJSPension, ");
                SQL.AppendLine("(A.SSEmployerBNILife + A.SSEmployerPKES + A.SSEmployerPRMH + A.SSEmployerPension + A.SSEmployerEmployment + A.SSEmployerHealth + A.SSErPension + A.TaxAllowance) TotalSubsidi, ");
                SQL.AppendLine("A.SSEmployerWNR, A.SSEmployeeWNR, R.TMTLevel, TIMESTAMPDIFF(YEAR, C.JoinDt, CURDATE()) AS YearOfService, ");
                SQL.AppendLine("Concat(TIMESTAMPDIFF(YEAR, C.JoinDt, CURDATE()), ' | ', (TIMESTAMPDIFF(MONTH, C.JoinDt, CONCAT(SUBSTRING(A.CreateDt,1,4),'-',SUBSTRING(A.CreateDt,5,2),'-',SUBSTRING(A.CreateDt,7,2)))%12)) AS MonthOfService, ");
                if (mPensionCalculationFormat != "2")
                    SQL.AppendLine("Date_Format(C.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d')As Pension, ");
                else
                {
                    SQL.AppendLine("case ");
                    SQL.AppendLine("	when Right(C.BirthDt, 2)='01' then Date_Format(C.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d') ");
                    SQL.AppendLine("    ELSE concat(Date_Format(C.BirthDt + INTERVAL '1' MONTH + INTERVAL @SSRetiredMaxAge Year, '%Y%m'), '01') ");
                    SQL.AppendLine("END AS Pension, ");
                }

                SQL.AppendLine("IfNull(Truncate(S1.Amt, 0), 0.00) TJabatan, IfNull(Truncate(S2.Amt, 0), 0.00) TPengPejabat, IfNull(Truncate(S3.Amt, 0), 0.00) TPerwakilan, IfNull(Truncate(S4.Amt, 0), 0.00) TSMKDinamis, ");
                SQL.AppendLine("IfNull(Truncate(S5.Amt, 0), 0.00) TSMKStatis, IfNull(Truncate(S6.Amt, 0), 0.00) TPerumahan, IfNull(Truncate(S7.Amt, 0), 0.00) TWilayah, IfNull(Truncate(S8.Amt, 0), 0.00) TUJT, IfNull(Truncate(S9.Amt, 0), 0.00) TRepresentatif, ");
                SQL.AppendLine("IfNull(Truncate(S10.Amt, 0), 0.00) TObat, IfNull(Truncate(S11.Amt, 0), 0.00) TTransport, IfNull(Truncate(S12.Amt, 0), 0.00) TPengStaff, IfNull(Truncate(S13.Amt, 0), 0.00) TPenJabatan, IfNull(Truncate(S14.Amt, 0), 0.00) TMasaPensiun, ");

                //MDAllowance Source ER
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerBNILife, 0.00), 0.00) DPNPemKerMD,  ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerPKES, 0.00), 0.00) PreKesMD, ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerPRMH, 0.00), 0.00) TPerumMD, ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerPension, 0.00), 0.00) DPLKMD,  ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerEmployment, 0.00), 0.00) BPJSEmploymentMD,  ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerHealth, 0.00), 0.00) BPJSHealthMD,  ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSErPension, 0.00), 0.00) BPJSPensionMD,  ");

                //MDAllowance Source Allowance Deduction
                SQL.AppendLine("IfNull(Truncate(S15.Amt, 0), 0.00) DPNPemKerMD,  ");
                SQL.AppendLine("IfNull(Truncate(S16.Amt, 0), 0.00) PreKesMD, ");
                SQL.AppendLine("IfNull(Truncate(S17.Amt, 0), 0.00) TPerumMD, ");
                SQL.AppendLine("IfNull(Truncate(S18.Amt, 0), 0.00) DPLKMD,  ");
                SQL.AppendLine("IfNull(Truncate(S19.Amt, 0), 0.00) BPJSEmploymentMD,  ");
                SQL.AppendLine("IfNull(Truncate(S20.Amt, 0), 0.00) BPJSHealthMD,  ");
                SQL.AppendLine("IfNull(Truncate(S21.Amt, 0), 0.00) BPJSPensionMD,  ");

                //ADAmt
                for (int i = 1; i <= mADCount; i++)
                {
                    //if (i == 1)
                    //    SQL.AppendLine("(IfNull(Truncate(S" + i.ToString() + ".Amt, 0), 0.00)");
                    //else
                    //    SQL.AppendLine("+ IfNull(Truncate(S" + i.ToString() + ".Amt, 0), 0.00) ");

                    if (i == 1)
                        SQL.AppendLine("(IfNull(S" + i.ToString() + ".Amt, 0.00)");
                    else
                        SQL.AppendLine("+ IfNull(S" + i.ToString() + ".Amt, 0.00) ");
                }
                SQL.AppendLine(") TotalADAmt, ");

                //MDAmt
                for (int i = 15; i <= mMDCount; i++)
                {
                    if (i == 15)
                        SQL.AppendLine("(IfNull(S" + i.ToString() + ".Amt, 0.00)");
                    else
                        SQL.AppendLine("+ IfNull(S" + i.ToString() + ".Amt, 0.00) ");
                }
                SQL.AppendLine(") TotalMDAmt, ");

                //MDAmt Truncated
                for (int i = 15; i <= mMDCount; i++)
                {
                    if (i == 15)
                        SQL.AppendLine("(IfNull(Truncate(S" + i.ToString() + ".Amt, 0), 0.00)");
                    else
                        SQL.AppendLine("+ IfNull(Truncate(S" + i.ToString() + ".Amt, 0), 0.00) ");
                }
            }
            else
            {
                SQL.AppendLine("I.OptDesc As NonTaxableIncomeDesc, A.Salary, A.WorkingDay, A.PLDay, A.PLHr, A.PLAmt, A.ProcessPLAmt, ");
                SQL.AppendLine("A.UPLDay, A.UPLHr, A.UPLAmt, A.ProcessUPLAmt, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, A.OT1Amt, A.OT2Amt, ");
                SQL.AppendLine("A.OTHolidayAmt, A.TaxableFixAllowance, A.NonTaxableFixAllowance, A.FixAllowance, A.PerformanceValue, ");
                SQL.AppendLine("A.VarAllowance, A.IncEmployee, A.Transport, A.TaxAllowance, A.Brutto, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerHealth, 0.00), 0.00) SSEmployerHealth, A.SSEmployeeHealth, IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerEmployment, 0.00), 0.00) SSEmployerEmployment, A.SSEmployeeEmployment, IF(A.PassAwayInd = 'N', IFNULL(A.SSErPension, 0.00), 0.00) SSErPension, A.SSEePension, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerPension, 0.00), 0.00) SSEmployerPension, A.SSEmployeePension, A.NonTaxableFixDeduction, A.TaxableFixDeduction, A.FixDeduction, A.DedEmployee, ");
                SQL.AppendLine("A.DedProduction, A.DedProdLeave, A.EmpAdvancePayment, A.SalaryAdjustment, ");
                SQL.AppendLine("IF(A.Netto != A.NettoActual, A.Netto-A.NettoActual+A.NettoActual, A.Netto) AS Netto, A.NettoActual, IF(A.NettoActual < N.MinRegionalWagesAmt OR A.NettoActual < P.ParValue, A.Netto-A.NettoActual, 0.00) As NettoAdjustment, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', A.Tax, 0.00) Tax, A.EOYTax, A.Amt, IF(L.Amt Is Null, A.Amt, (A.Amt-L.Amt)) As PaidAmt, A.VoucherRequestPayrollDocNo, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerBNILife, 0.00), 0.00) SSEmployerBNILife, A.SSEmployeeBNILife, A.SSEmployerNonBPJS, A.SSEmployeeNonBPJS, IfNull(N.MinRegionalWagesAmt, 0.00) MinRegionalWagesAmt, IfNull(O.Amt, 0.00) ExpensiveAllowance, A.WarningLetter, IfNull(P.ParValue, 0.00) UMPP, ");
                SQL.AppendLine("IfNull(Q.WorkPeriodAmt, 0.00) WorkPeriodAmt, A.ServiceYearAllowance, A.PassAwayInd, A.OTMeal, ");
                SQL.AppendLine("A.SSEmployerDPNPHT, A.SSEmployeeDPNPHT, ");
                SQL.AppendLine("A.SSEmployerDPNPNS, A.SSEmployeeDPNPNS, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerPKES, 0.00), 0.00) SSEmployerPKES, A.SSEmployeePKES, ");
                SQL.AppendLine("IF(A.PassAwayInd = 'N', IFNULL(A.SSEmployerPRMH, 0.00), 0.00) SSEmployerPRMH, A.SSEmployeePRMH, ");
                SQL.AppendLine("A.SSEmployerTSP, A.SSEmployeeTSP, ");
                SQL.AppendLine("(A.SSEmployerBNILife - A.SSEmployeeBNILife) SSBNILife, (A.SSEmployerPKES - A.SSEmployeePKES) SSPKES, (A.SSEmployerPRMH - A.SSEmployeePRMH) SSPRMH, (A.SSEmployerPension - A.SSEmployeePension) SSPension, ");
                SQL.AppendLine("(A.SSEmployerEmployment+A.SSEmployeeEmployment) BPJSEmployment, (A.SSEmployerHealth+A.SSEmployeeHealth) BPJSHealth, (A.SSErPension+A.SSEePension) BPJSPension, ");
                SQL.AppendLine("(A.SSEmployerBNILife + A.SSEmployerPKES + A.SSEmployerPRMH + A.SSEmployerPension + A.SSEmployerEmployment + A.SSEmployerHealth + A.SSErPension + A.TaxAllowance) TotalSubsidi, ");
                SQL.AppendLine("A.SSEmployerWNR, A.SSEmployeeWNR, R.TMTLevel, TIMESTAMPDIFF(YEAR, C.JoinDt, CURDATE()) AS YearOfService, ");
                SQL.AppendLine("Concat(TIMESTAMPDIFF(YEAR, C.JoinDt, CURDATE()), ' | ', (TIMESTAMPDIFF(MONTH, C.JoinDt, CURDATE())%12)) AS MonthOfService, ");
                if (mPensionCalculationFormat != "2")
                    SQL.AppendLine("Date_Format(C.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d')As Pension, ");
                else
                {
                    SQL.AppendLine("case ");
                    SQL.AppendLine("	when Right(C.BirthDt, 2)='01' then Date_Format(C.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d') ");
                    SQL.AppendLine("    ELSE concat(Date_Format(C.BirthDt + INTERVAL '1' MONTH + INTERVAL @SSRetiredMaxAge Year, '%Y%m'), '01') ");
                    SQL.AppendLine("END AS Pension, ");
                }

                SQL.AppendLine("IfNull(S1.Amt, 0.00) TJabatan, IfNull(S2.Amt, 0.00) TPengPejabat, IfNull(S3.Amt, 0.00) TPerwakilan, IfNull(S4.Amt, 0.00) TSMKDinamis, ");
                SQL.AppendLine("IfNull(S5.Amt, 0.00) TSMKStatis, IfNull(S6.Amt, 0.00) TPerumahan, IfNull(S7.Amt, 0.00) TWilayah, IfNull(S8.Amt, 0.00) TUJT, IfNull(S9.Amt, 0.00) TRepresentatif, ");
                SQL.AppendLine("IfNull(S10.Amt, 0.00) TObat, IfNull(S11.Amt, 0.00) TTransport, IfNull(S12.Amt, 0.00) TPengStaff, IfNull(S13.Amt, 0.00) TPenJabatan, IfNull(S14.Amt, 0.00) TMasaPensiun, ");

                //MDAllowance Source ER
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerBNILife, 0.00), 0.00) DPNPemKerMD,  ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerPKES, 0.00), 0.00) PreKesMD, ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerPRMH, 0.00), 0.00) TPerumMD, ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerPension, 0.00), 0.00) DPLKMD,  ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerEmployment, 0.00), 0.00) BPJSEmploymentMD,  ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSEmployerHealth, 0.00), 0.00) BPJSHealthMD,  ");
                //SQL.AppendLine("IF(A.PassAwayInd = 'Y', IFNULL(A.SSErPension, 0.00), 0.00) BPJSPensionMD,  ");

                //MDAllowance Source Allowance Deduction
                SQL.AppendLine("IfNull(S15.Amt, 0.00) DPNPemKerMD,  ");
                SQL.AppendLine("IfNull(S16.Amt, 0.00) PreKesMD, ");
                SQL.AppendLine("IfNull(S17.Amt, 0.00) TPerumMD, ");
                SQL.AppendLine("IfNull(S18.Amt, 0.00) DPLKMD,  ");
                SQL.AppendLine("IfNull(S19.Amt, 0.00) BPJSEmploymentMD,  ");
                SQL.AppendLine("IfNull(S20.Amt, 0.00) BPJSHealthMD,  ");
                SQL.AppendLine("IfNull(S21.Amt, 0.00) BPJSPensionMD,  ");

                //ADAmt
                for (int i = 1; i <= mADCount; i++)
                {
                    if (i == 1)
                        SQL.AppendLine("(IfNull(S" + i.ToString() + ".Amt, 0.00)");
                    else
                        SQL.AppendLine("+ IfNull(S" + i.ToString() + ".Amt, 0.00) ");
                }
                SQL.AppendLine(") TotalADAmt, ");

                //MDAmt
                for (int i = 15; i <= mMDCount; i++)
                {
                    if (i == 15)
                        SQL.AppendLine("(IfNull(S" + i.ToString() + ".Amt, 0.00)");
                    else
                        SQL.AppendLine("+ IfNull(S" + i.ToString() + ".Amt, 0.00) ");
                }
                SQL.AppendLine(") TotalMDAmt, ");

                //MDAmt Truncated
                for (int i = 15; i <= mMDCount; i++)
                {
                    if (i == 15)
                        SQL.AppendLine("(IfNull(S" + i.ToString() + ".Amt, 0.00)");
                    else
                        SQL.AppendLine("+ IfNull(S" + i.ToString() + ".Amt, 0.00) ");
                }
            }
            SQL.AppendLine(") TotalMDAmtT, ");

            // Year Of Service
            //SQL.AppendLine("TIMESTAMPDIFF(YEAR, C.JoinDt, CONCAT(SUBSTRING(T.DocDt,1,4),'-',SUBSTRING(T.DocDt,5,2),'-',SUBSTRING(T.DocDt,7,2))) AS YearOfServiceNew, ");
            SQL.AppendLine("TIMESTAMPDIFF(YEAR, REPLACE(C.JoinDt, Right(C.JoinDt,2), '01'), CONCAT(SUBSTRING(T.DocDt,1,4),'-',SUBSTRING(T.DocDt,5,2),'-',SUBSTRING(T.DocDt,7,2))) AS YearOfServiceNew, ");

            // Month Of Service
            //SQL.AppendLine(" Concat(TIMESTAMPDIFF(YEAR, C.JoinDt, CONCAT(SUBSTRING(T.DocDt,1,4),'-',SUBSTRING(T.DocDt,5,2),'-',SUBSTRING(T.DocDt,7,2))), ' | ', (TIMESTAMPDIFF(MONTH, C.JoinDt, CONCAT(SUBSTRING(T.DocDt,1,4),'-',SUBSTRING(T.DocDt,5,2),'-',SUBSTRING(T.DocDt,7,2)))%12)) AS MonthOfServiceNew ");
            SQL.AppendLine(" Concat(TIMESTAMPDIFF(YEAR, REPLACE(C.JoinDt, Right(C.JoinDt,2), '01'), CONCAT(SUBSTRING(T.DocDt,1,4),'-',SUBSTRING(T.DocDt,5,2),'-',SUBSTRING(T.DocDt,7,2))), ' | ', (TIMESTAMPDIFF(MONTH, REPLACE(C.JoinDt, Right(C.JoinDt,2), '01'), CONCAT(SUBSTRING(T.DocDt,1,4),'-',SUBSTRING(T.DocDt,5,2),'-',SUBSTRING(T.DocDt,7,2)))%12)) AS MonthOfServiceNew ");

            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun B ");
            SQL.AppendLine("    On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            if(mIsPayslipNotUseCurrentPPS)
            {
                SQL.AppendLine("Left Join TblEmployeePPS C2 On C2.EmpCode = A.EmpCode ");
                SQL.AppendLine("    And C2.StartDt = ( ");
                SQL.AppendLine("        Select Max(StartDt) ");
                SQL.AppendLine("        From TblEmployeePPS ");
                SQL.AppendLine("        Where EmpCode = A.EmpCode ");
                SQL.AppendLine("            And StartDt <= B.StartDt ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Left Join TblPosition E2 On C2.PosCode=E2.PosCode ");
                SQL.AppendLine("Left Join TblLevelHdr M2 On C2.LevelCode = M2.LevelCode ");

            }
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode=E.PosCode ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Inner Join TblOption G On B.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr H On B.PGCode=H.PGCode ");
            SQL.AppendLine("Left Join TblOption I On A.PTKP=I.OptCode And I.OptCat='NonTaxableIncome' ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X.EmpCode, X.payruncode, X.Amt001, X.Amt004  ");
            SQL.AppendLine("    from  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.EmpCode, A.PayrunCode, A.Amt As Amt001, ifnull(B.Amt004, 0) Amt004  ");
            SQL.AppendLine("        From TblPayrollProcessAd A ");
            SQL.AppendLine("        Left Join  ");
            SQL.AppendLine("        (  ");
            SQL.AppendLine("            Select A.EmpCode, A.PayrunCode, A.Amt As Amt004  ");
            SQL.AppendLine("            From TblPayrollProcessAd A ");
            SQL.AppendLine("            Where A.AdCode ='065' ");
            SQL.AppendLine("        )B On A.PayrunCode = B.payrunCode And A.EmpCode = B.EmpCode ");
            SQL.AppendLine("        Where A.AdCode ='001' ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine(")K On A.payruncode = K.PayrunCode And  A.EmpCode = K.EmpCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.EmpCode, A.PayrunCode, A.Amt From tblpayrollprocessad A ");
            SQL.AppendLine("    Inner Join tblemployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("    Where A.ADCode = '003' And B.PosCode = '026' ");
            SQL.AppendLine(")L On A.payruncode = L.PayrunCode And A.EmpCode = L.EmpCode ");
            SQL.AppendLine("Left Join TblLevelHdr M On C.LevelCode = M.LevelCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.StartDt, T2.EndDt, T1.SiteCode, T1.CityCode, T1.Amt MinRegionalWagesAmt ");
            SQL.AppendLine("    From TblRegionalMinimumWagesDtl T1 ");
            SQL.AppendLine("    Inner Join TblRegionalMinimumWagesHdr T2 On T1.DocNo = T2.Docno ");
            SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine(") N On C.SiteCode = N.SiteCode And J.CityCode = N.CityCode And (B.StartDt >= N.StartDt And N.EndDt >= B.EndDt) ");
            SQL.AppendLine("Left Join TblPayrollProcessAD O On A.PayrunCode = O.PayrunCode And A.EmpCode = O.EmpCode And O.ADCode In (Select ParValue From TblParameter Where ParCode = 'ADCodeExpensiveAllowance') ");
            SQL.AppendLine("Left Join Tblparameter P On P.ParCode = 'UMPP' ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.StartDt, T1.EndDt, T2.WorkPeriod, Sum(T2.Amt) WorkPeriodAmt ");
            SQL.AppendLine("    From TblAllowanceWorkPeriodHDr T1 ");
            SQL.AppendLine("    Inner Join TblAllowanceWorkPeriodDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        ANd T1.CancelInd = 'N' ");
            SQL.AppendLine("    Group By T1.StartDt, T1.EndDt, T2.WorkPeriod ");
            SQL.AppendLine(") Q On Q.WorkPeriod = TimeStampdiff(Year, C.JoinDt, left(Replace(CurDate(), '-', ''), 8)) And (Q.StartDt >= B.StartDt Or B.EndDt >= Q.EndDt) ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select EmpCode, Max(DocDt) TMTLevel ");
            SQL.AppendLine("	From TblPPS ");
            SQL.AppendLine("	Where CancelInd = 'N' And Status = 'A' ");
            SQL.AppendLine("	And Ifnull(LevelCodeOld, '') <> LevelCodeNew ");
            SQL.AppendLine("	Group By EmpCode ");
            SQL.AppendLine(")R On C.EmpCode = R.EmpCode ");

            //Breakdown lup FixAllowance
            #region Query Breakdown Allowance

            SQL.AppendLine("Left Join TblPayrollProcessAD S1 On A.PayrunCode = S1.PayrunCode And A.EmpCode = S1.EmpCode And S1.ADCode = 'AL008' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S2 On A.PayrunCode = S2.PayrunCode And A.EmpCode = S2.EmpCode And S2.ADCode = 'AL011' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S3 On A.PayrunCode = S3.PayrunCode And A.EmpCode = S3.EmpCode And S3.ADCode = 'AL007' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S4 On A.PayrunCode = S4.PayrunCode And A.EmpCode = S4.EmpCode And S4.ADCode = 'AL004' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S5 On A.PayrunCode = S5.PayrunCode And A.EmpCode = S5.EmpCode And S5.ADCode = 'AL025' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S6 On A.PayrunCode = S6.PayrunCode And A.EmpCode = S6.EmpCode And S6.ADCode = 'AL010' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S7 On A.PayrunCode = S7.PayrunCode And A.EmpCode = S7.EmpCode And S7.ADCode = 'AL002' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S8 On A.PayrunCode = S8.PayrunCode And A.EmpCode = S8.EmpCode And S8.ADCode = 'AL013' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S9 On A.PayrunCode = S9.PayrunCode And A.EmpCode = S9.EmpCode And S9.ADCode = 'AL014' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S10 On A.PayrunCode = S10.PayrunCode And A.EmpCode = S10.EmpCode And S10.ADCode = 'AL009' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S11 On A.PayrunCode = S11.PayrunCode And A.EmpCode = S11.EmpCode And S11.ADCode = '012' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S12 On A.PayrunCode = S12.PayrunCode And A.EmpCode = S12.EmpCode And S12.ADCode = 'AL024' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S13 On A.PayrunCode = S13.PayrunCode And A.EmpCode = S13.EmpCode And S13.ADCode = '"+ mADCodePositionAdjustment + "' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S14 On A.PayrunCode = S14.PayrunCode And A.EmpCode = S14.EmpCode And S14.ADCode = 'AL016' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S15 On A.PayrunCode = S15.PayrunCode And A.EmpCode = S15.EmpCode And S15.ADCode = 'AL017' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S16 On A.PayrunCode = S16.PayrunCode And A.EmpCode = S16.EmpCode And S16.ADCode = 'AL018' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S17 On A.PayrunCode = S17.PayrunCode And A.EmpCode = S17.EmpCode And S17.ADCode = 'AL023' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S18 On A.PayrunCode = S18.PayrunCode And A.EmpCode = S18.EmpCode And S18.ADCode = 'AL019' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S19 On A.PayrunCode = S19.PayrunCode And A.EmpCode = S19.EmpCode And S19.ADCode = 'AL020' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S20 On A.PayrunCode = S20.PayrunCode And A.EmpCode = S20.EmpCode And S20.ADCode = 'AL021' ");
            SQL.AppendLine("Left Join TblPayrollProcessAD S21 On A.PayrunCode = S21.PayrunCode And A.EmpCode = S21.EmpCode And S21.ADCode = 'AL022' ");

            #endregion

            SQL.AppendLine("LEFT JOIN tblvoucherrequestpayrollhdr T ON A.voucherrequestpayrolldocno = T.DocNo ");

            SQL.AppendLine(") Tbl ");
            SQL.AppendLine("Left Join TblPayrollProcessAD Tbl2 On Tbl2.PayrunCode = Tbl.PayrunCode And Tbl2.EmpCode = Tbl.EmpCode And Tbl2.ADCode In (Select ParValue From TblParameter Where ParCode = 'ADCodeFixedAllowance') ");
            SQL.AppendLine("Where 1=1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 133;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Payrun Code",
                        "Payrun Name",
                        "Employee's Code",
                        "Employee's Name",

                        //6-10
                        "Old Code",
                        "Position",
                        "Department",
                        "Level",
                        "Join",
                        
                        //11-15
                        "Resign",
                        "Type",
                        "Period",
                        "Group",
                        "Site",

                        //16-20
                        "NPWP",
                        "PTKP",
                        "TMT Grade",
                        "TMT Salary",
                        "TMT Level",

                        //21-25
                        "TMT Pension",
                        "Year of Service",
                        "Month of Service",
                        "UMK",
                        "Potongan atas"+Environment.NewLine+"Tunjangan Kemahalan",

                        //26-30
                        "Warning Letter",
                        "UMPP",
                        "Allowance"+Environment.NewLine+"Work Period",
                        "Salary",
                        "Working Day",

                        //31-35
                        "Service Year Allowance",
                        "Taxable Fixed"+Environment.NewLine+"Allowance",
                        "Non Taxable Fixed"+Environment.NewLine+"Allowance",
                        "Fixed"+Environment.NewLine+"Allowance",
                        "Variable"+Environment.NewLine+"Allowance",

                        //36-40
                        "",
                        "",
                        "Total"+Environment.NewLine+"Fix Component",
                        "Tunj. Jabatan",
                        "Tunj. Penyesuaian"+Environment.NewLine+"Penghasilan Pejabat",

                        //41-45
                        "Tunj. Perwakilan",
                        "Tunj. SMK"+Environment.NewLine+"Dinamis",
                        "Tunj. SMK"+Environment.NewLine+"Statis",
                        "Tunj. Perumahan",
                        "Tunj. Wilayah",

                        //46-50
                        "Tunj. UJT",
                        "Tunj. Representatif",
                        "Tunj. Obat",
                        "Tunj. Transportasi",
                        "Tunj. Penyesuaian"+Environment.NewLine+"Penghasilan Staff",

                        //51-55
                        "Tunj. Penyesuaian"+Environment.NewLine+"Jabatan",
                        "Tunj. Masa"+Environment.NewLine+"Persiapan Pensiun",
                        "Total"+Environment.NewLine+"Variable Component",
                        "Employer"+Environment.NewLine+"Dapen"+Environment.NewLine+"Pemberi Kerja",
                        "Employee"+Environment.NewLine+"Dapen"+Environment.NewLine+"Pemberi Kerja",

                        //56-60
                        "",
                        "Premi Kesehatan"+Environment.NewLine +"Er",
                        "Premi Kesehatan"+Environment.NewLine +"Ee",
                        "Pemilikan Rumah"+Environment.NewLine +"Er",
                        "Pemilikan Rumah"+Environment.NewLine +"Ee",

                        //61-65
                        "Employer"+Environment.NewLine+"DPLK",
                        "Employee"+Environment.NewLine+"DPLK",
                        "BPJS Employer"+Environment.NewLine+"Employment",
                        "BPJS Employee"+Environment.NewLine+"Employment",
                        "BPJS Employer"+Environment.NewLine+"Health",

                        //66-70
                        "BPJS Employee"+Environment.NewLine+"Health",
                        "BPJS Employer"+Environment.NewLine+"Pension",
                        "BPJS Employee"+Environment.NewLine+"Pension",
                        "Tax"+Environment.NewLine+"Allowance",
                        "Total Subsidi",

                        //71-75
                        "Brutto",
                        "Dapen"+Environment.NewLine+" Pemberi Kerja",
                        "Premi"+Environment.NewLine+"Kesehatan",
                        "Pemilikan"+Environment.NewLine+"Rumah",
                        "DPLK",

                        //76-80
                        "BPJS Employment",
                        "BPJS Health",
                        "BPJS Pension",
                        "Dana Pensiun PNS"+Environment.NewLine +"Er", 
                        "Dana Pensiun PNS"+Environment.NewLine +"Ee",

                        //81-85
                        "Dana Pensiun PHT"+Environment.NewLine +"Er",
                        "Dana Pensiun PHT"+Environment.NewLine +"Ee",
                        "Taspen"+Environment.NewLine +"Er",
                        "Taspen"+Environment.NewLine +"Ee",
                        "Wanaarta"+Environment.NewLine +"Er",

                        //86-90
                        "Wanaarta"+Environment.NewLine +"Ee",
                        "Tax",
                        "End of Year"+Environment.NewLine+"Tax",
                        "Total Deduction",
                        "Netto",

                        //91-95
                        "Netto-Actual",
                        "Netto Adjustment",
                        "Non Taxable Employee's"+Environment.NewLine+"Deduction",
                        "Taxable Employee's"+Environment.NewLine+"Deduction",
                        "Fixed"+Environment.NewLine+"Deduction",

                        //96-100
                        "Employee's"+Environment.NewLine+"Advance Payment",
                        "",
                        "Salary"+Environment.NewLine+"Adjustment",
                        "Salary Before"+Environment.NewLine+"Adjustment",
                        "Take Home Pay",

                        //101-105
                        "Paid Amount",
                        "Voucher Request#"+Environment.NewLine+"(Payroll)",
                        "Paid Leave"+Environment.NewLine+"(Day)",
                        "Paid Leave"+Environment.NewLine+"(Hour)", 
                        "Paid Leave"+Environment.NewLine+"(Amount)",

                        //106-110
                        "Processed Paid"+Environment.NewLine+"Leave (Amount)",
                        "Unpaid Leave"+Environment.NewLine+"(Day)",
                        "Unpaid Leave"+Environment.NewLine+"(Hour)",
                        "Unpaid Leave"+Environment.NewLine+"(Amount)", 
                        "Processed Unpaid"+Environment.NewLine+"Leave (Amount)",

                        //111-115
                        "OT 1"+Environment.NewLine+"(Hour)",
                        "OT 2"+Environment.NewLine+"(Hour)",
                        "OT Holiday"+Environment.NewLine+"(Hour)",
                        "OT 1"+Environment.NewLine+"(Amount)", 
                        "OT 2"+Environment.NewLine+"(Amount)",

                        //116-120
                        "OT Holiday"+Environment.NewLine+"(Amount)",
                        "Incentive",
                        "Transport", 
                        "Employee"+Environment.NewLine+"Deduction",
                        "Employer"+Environment.NewLine+"Non BPJS",

                        //121-125
                        "",
                        "Employee"+Environment.NewLine+"Non BPJS",
                        "",
                        "Pass Away",
                        "OT Meal",

                        //126-130
                        "Dapen"+Environment.NewLine+"Pemberi Kerja"+Environment.NewLine+"(MD)",
                        "Premi"+Environment.NewLine+"Kesehatan"+Environment.NewLine+"(MD)",
                        "Pemilikan"+Environment.NewLine+"Rumah (MD)",
                        "DPLK (MD)",
                        "BPJS"+Environment.NewLine+"Employment"+Environment.NewLine+"(MD)",

                        //131-132
                        "BPJS Health"+Environment.NewLine+"(MD)",
                        "BPJS Pension"+Environment.NewLine+"(MD)",
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 300, 110, 200, 
                        
                        //6-10
                        100, 250, 150, 100, 100, 
                        
                        //11-15
                        100, 100, 100, 100, 250, 
                        
                        //16-20
                        130, 200, 100, 100, 100,  
                        
                        //21-25
                        100, 100, 100, 120, 150, 
                        
                        //26-30
                        150, 150, 150, 100, 100, 

                        //31-35
                        150, 100, 100, 100, 100, 
                        
                        //36-40
                        20, 20, 150, 130, 130,
                        
                        //41-45
                        130, 130, 130, 130, 130, 
                        
                        //46-50
                        130, 130, 130, 130, 130,
                        
                        //51-55
                        130, 130, 150, 130, 130,
                        
                        //56-60
                        20, 130, 130, 130, 130,

                        //61-65
                        130, 130, 130, 130, 130,
                        
                        //66-70
                        130, 130, 130, 130, 150,

                        //71-75
                        130, 130, 130, 130, 130,

                        //76-80
                        130, 130, 130, 130, 130,

                        //81-85
                        130, 130, 130, 130, 130,

                        //86-90
                        130, 130, 130, 150, 130,

                        //91-95
                        130, 130, 150, 130, 130,

                        //96-100
                        130, 20, 130, 130, 130,

                        //101-105
                        130, 170, 100, 100, 100,

                        //106-110
                        100, 100, 100, 100, 100, 

                        //111-115
                        100, 100, 100, 100, 100, 

                        //116-120
                        100, 100, 100, 100, 100, 

                        //121-125
                        20, 100, 20, 100, 100,

                        //126-130
                        130, 130, 130, 130, 130,

                        //131-132
                        130, 130,
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 36, 37, 56, 97, 121, 123 });
            Sm.GrdFormatDec(Grd1, new int[] {
                24, 25, 26, 27, 28, 29, 30,
                31, 32, 33, 34, 35, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 
                51, 52, 53, 54, 55, 57, 58, 59, 60, 
                61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 
                71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 
                81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 
                91, 92, 93, 94, 95, 96, 98, 99, 100, 
                101, 103, 104, 105, 106, 107, 108, 109, 110, 
                111, 112, 113,  114, 115, 116, 117, 118, 119, 120, 
                122, 125, 126, 127, 128, 129, 130, 131, 132
            }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 10, 11, 18, 19, 20, 21 });
            Sm.GrdColCheck(Grd1, new int[] { 124 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[]
            {
                0,
                2, 3, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                31, 32, 33, 34, 35, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                51, 52, 53, 54, 55, 57, 58, 59, 60,
                61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
                71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
                91, 92, 93, 94, 95, 96, 98, 99, 100,
                101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
                111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
                122, 124, 125, 126, 127, 128, 129, 130, 131, 132
            });

            Sm.GrdColInvisible(Grd1, new int[] {
                
                12, 14,
                25, 28, 30,
                31, 32, 33, 35, 36,
                55, 56, 58, 60,
                62, 64, 66, 68,
                79,
                81, 83, 85, 86, 88,
                93, 94, 98, 99,
                101, 103, 104, 105, 106, 107, 108, 109, 110,
                111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
                121, 122, 123, 124, 125
               }, false);

            Grd1.Cols[37].Move(95);
            Grd1.Cols[26].Move(99);
            Grd1.Cols[91].Move(88);
            Grd1.Cols[126].Move(67);
            Grd1.Cols[127].Move(68);
            Grd1.Cols[128].Move(69);
            Grd1.Cols[129].Move(70);
            Grd1.Cols[130].Move(71);
            Grd1.Cols[131].Move(72);
            Grd1.Cols[132].Move(73);
            Grd1.Cols[102].Move(4);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 14, 36 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowData()
        {
            if (Sm.IsTxtEmpty(TxtPayCod, "Payrun", false)) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<string>(ref cm, "@SSRetiredMaxAge", mIsRptPensionUseSSRetiredMaxAge2 ? mSSRetiredMaxAge2 : mSSRetiredMaxAge);
                Sm.FilterStr(ref Filter, ref cm, TxtPayCod.Text, new string[] { "Tbl.PayrunCode", "Tbl.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "Tbl.EmpCode", "Tbl.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "Tbl.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "Tbl.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By Tbl.PayrunCode, Tbl.EmpName;",
                    new string[]
                    {
                            //0
                            "PayrunCode",
                        
                            //1-5
                            "PayrunName",
                            "EmpCode",
                            "EmpName",
                            "EmpCodeOld",
                            "PosName",
                            
                            //6-10
                            "DeptName",
                            "LevelNameAdd",
                            "JoinDt",
                            "ResignDt",
                            "SystemTypeDesc", 
                            
                            //11-15
                            "PayrunPeriodDesc",
                            "PGName",
                            "SiteName",
                            "NPWP",
                            "NonTaxableIncomeDesc",

                            //16-20
                            "TMTLevel",
                            "Pension",
                            "YearOfServiceNew",
                            "MonthOfServiceNew",
                            "MinRegionalWagesAmt", 
                            
                            //21-25
                            "ExpensiveAllowance",
                            "WarningLetter",
                            "UMPP",
                            "WorkPeriodAmt",
                            "Salary", 
                            
                            //26-30
                            "WorkingDay",
                            "ServiceYearAllowance",
                            "TaxableFixAllowance",
                            "NonTaxableFixAllowance",
                            "FixAllowanceNew",
                            
                            //31-35
                            "VarAllowance",
                            "TotFixComponent",
                            "TJabatan",
                            "TPengPejabat",
                            "TPerwakilan",

                            //36-40
                            "TSMKDinamis",
                            "TSMKStatis",
                            "TPerumahan",
                            "TWilayah",
                            "TUJT",

                            //41-45
                            "TRepresentatif",
                            "TObat",
                            "TTransport",
                            "TPengStaff",
                            "TPenJabatan",

                            //46-50
                            "TMasaPensiun",
                            "TotalADAmt",
                            "SSEmployerBNILife",
                            "SSEmployeeBNILife",
                            "SSEmployerPKES",

                            //51-55
                            "SSEmployeePKES",
                            "SSEmployerPRMH",
                            "SSEmployeePRMH",
                            "SSEmployerPension",
                            "SSEmployeePension",

                            //56-60
                            "SSEmployerEmployment",
                            "SSEmployeeEmployment",
                            "SSEmployerHealth",
                            "SSEmployeeHealth",
                            "SSErPension",

                            //61-65
                            "SSEePension",
                            "TaxAllowance",
                            "TotSubsidi",
                            "Brutto",
                            "SSBNILife",

                            //66-70
                            "SSPKES",
                            "SSPRMH",
                            "SSPension",
                            "BPJSEmployment",
                            "BPJSHealth",

                            //71-75
                            "BPJSPension",
                            "SSEmployerDPNPNS",
                            "SSEmployeeDPNPNS",
                            "SSEmployerDPNPHT",
                            "SSEmployeeDPNPHT",

                            //76-80
                            "SSEmployerTSP",
                            "SSEmployeeTSP",
                            "SSEmployerWNR",
                            "SSEmployeeWNR",
                            "Tax",

                            //81-85
                            "EOYTax",
                            "TotDeduction",
                            "Netto",
                            "NettoActual",
                            "NettoAdjustment",

                            //86-90
                            "NonTaxableFixDeduction",
                            "TaxableFixDeduction",
                            "FixDeduction",
                            "EmpAdvancePayment",
                            "SalaryAdjustment",

                            //91-95
                            "Amt",
                            "PaidAmt",
                            "VoucherRequestPayrollDocNo",
                            "PLDay",
                            "PLHr",

                            //96-100
                            "PLAmt",
                            "ProcessPLAmt",
                            "UPLDay",
                            "UPLHr",
                            "UPLAmt",

                            //101-105
                            "ProcessUPLAmt",
                            "OT1Hr",
                            "OT2Hr",
                            "OTHolidayHr",
                            "OT1Amt",

                            //106-110
                            "OT2Amt",
                            "OTHolidayAmt",
                            "IncEmployee",
                            "Transport",
                            "DedEmployee",

                            //111-115
                            "SSEmployerNonBPJS",
                            "SSEmployeeNonBPJS",
                            "PassAwayInd",
                            "OTMeal",
                            "DPNPemKerMD",

                            //116-120
                            "PreKesMD",
                            "TPerumMD",
                            "DPLKMD",
                            "BPJSEmploymentMD",
                            "BPJSHealthMD",

                            //121
                            "BPJSPensionMD",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 20);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 22);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 23);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 25);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 26);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 27);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 28);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 29);
                        //Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 30);
                        Grd.Cells[Row, 34].Value = Sm.DrDec(dr, c[30]) == 0m ? 0m : Math.Round(Sm.DrDec(dr, c[30]) / 100m) * 100;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 31);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 32);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 33);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 34);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 35);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 36);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 37);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 38);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 39);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 46, 40);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 41);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 48, 42);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 49, 43);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 50, 44);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 51, 45);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 52, 46);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 53, 47);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 54, 48);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 55, 49);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 57, 50);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 58, 51);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 59, 52);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 60, 53);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 61, 54);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 62, 55);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 63, 56);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 64, 57);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 65, 58);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 66, 59);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 67, 60);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 68, 61);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 69, 62);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 70, 63);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 71, 64);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 72, 65);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 73, 66);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 74, 67);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 75, 68);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 76, 69);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 77, 70);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 78, 71);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 79, 72);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 80, 73);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 81, 74);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 82, 75);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 83, 76);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 84, 77);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 85, 78);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 86, 79);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 87, 80);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 88, 81);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 89, 82);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 90, 83); 
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 91, 84);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 92, 85);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 93, 86);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 94, 87);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 95, 88);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 96, 89);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 98, 90);

                        if (Sm.GetGrdDec(Grd1, Row, 98) < 0)
                            Grd1.Cells[Row, 99].Value = Sm.GetGrdDec(Grd1, Row, 98) + Sm.GetGrdDec(Grd1, Row, 71);
                        else if (Sm.GetGrdDec(Grd1, Row, 98) > 0)
                            Grd1.Cells[Row, 99].Value = Sm.GetGrdDec(Grd1, Row, 71) - Sm.GetGrdDec(Grd1, Row, 98);
                        else
                            Grd1.Cells[Row, 99].Value = 0m;

                        Sm.SetGrdValue("N", Grd, dr, c, Row, 100, 91); 

                        //Netto adjustment sorce baru
                        //if (Sm.GetGrdDec(Grd1, Row, 90) < Sm.GetGrdDec(Grd1, Row, 24))
                        //{
                        //    if (Sm.GetGrdDec(Grd1, Row, 24) < Sm.GetGrdDec(Grd1, Row, 27))
                        //        Grd1.Cells[Row, 101].Value = Sm.GetGrdDec(Grd1, Row, 27) - Sm.GetGrdDec(Grd1, Row, 90);
                        //    else
                        //        Grd1.Cells[Row, 101].Value = Sm.GetGrdDec(Grd1, Row, 24) - Sm.GetGrdDec(Grd1, Row, 90);
                        //}
                        //else
                        //    Grd1.Cells[Row, 101].Value = 0m;

                        Sm.SetGrdValue("N", Grd, dr, c, Row, 101, 92);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 102, 93);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 103, 94);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 104, 95);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 105, 96);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 106, 97);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 107, 98);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 108, 99);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 109, 100);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 110, 101);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 111, 102);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 112, 103);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 113, 104);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 114, 105);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 115, 106);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 116, 107);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 117, 108);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 118, 109);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 119, 110);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 120, 111);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 122, 112);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 124, 113);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 125, 114);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 126, 115);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 127, 116);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 128, 117);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 129, 118);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 130, 119);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 131, 120);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 132, 121);

                        for (int Col = mLatestCol + 1; Col < Grd1.Cols.Count; Col++)
                            Grd1.Cells[Row, Col].Value = 0m;
                    }, true, false, false, false
                );

                Grd1.BeginUpdate();
                SetSSPAmt();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]
                {
                    24, 25, 26, 27, 28, 29, 30,
                    31, 32, 33, 34, 35, 38, 39, 40,
                    41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                    51, 52, 53, 54, 55, 57, 58, 59, 60,
                    61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
                    71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                    81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
                    91, 92, 93, 94, 95, 96, 98, 99, 100,
                    101, 103, 104, 105, 106, 107, 108, 109, 110,
                    111, 112, 113,  114, 115, 116, 117, 118, 119, 120,
                    122, 125, 126, 127, 128, 129, 130, 131, 132
                });
                for (int Col = mLatestCol + 1; Col < Grd1.Cols.Count; Col++)
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { Col });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    Sm.FormShowDialog(
                        new FrmRptPayrollProcessSummary18Dlg(
                            this,
                            Sm.GetGrdStr(Grd1, r, 2),
                            Sm.GetGrdStr(Grd1, r, 4)
                            ));
                }
            }
            if (e.ColIndex == 36 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 34) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessAD("A", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (e.ColIndex == 37 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 95) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessAD("D", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (e.ColIndex == 56 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                ShowBasicSalaryComponent(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 121 && Sm.GetGrdStr(Grd1, r, 120).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 120) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessSSProgram("R", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (e.ColIndex == 123 && Sm.GetGrdStr(Grd1, r, 122).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 122) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessSSProgram("E", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (e.ColIndex == 97 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 96) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowCredit(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            {
                Sm.FormShowDialog(
                    new FrmRptPayrollProcessSummary18Dlg(
                        this,
                        Sm.GetGrdStr(Grd1, r, 2),
                        Sm.GetGrdStr(Grd1, r, 4)
                        ));
            }
            if (e.ColIndex == 36 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 34) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessAD("A", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 37 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 95) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessAD("D", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 56 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
                ShowBasicSalaryComponent(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            if (e.ColIndex == 121 && Sm.GetGrdStr(Grd1, r, 66).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 120) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessSSProgram("R", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 123 && Sm.GetGrdStr(Grd1, r, 68).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 122) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessSSProgram("E", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 97 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 96) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowCredit(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
        }

        #endregion

        #region Button Method

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        override protected void PrintData()
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return true;
            }
            return false;
        }

        private void ShowPayrollProcessSSProgram(string SSType, string PayrunCode, string EmpCode)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            SQL.AppendLine("Select SSPName, Amt From (");
            SQL.AppendLine("    Select B.SSPName, A.E" + SSType + "Amt As Amt ");
            SQL.AppendLine("    From TblPayrollProcessSSProgram A ");
            SQL.AppendLine("    Inner Join TblSSProgram B On A.SSPCode=B.SSPCode ");
            SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("    And A.EmpCode=@EmpCode ");
            SQL.AppendLine("    And A.BPJSInd='N' ");
            SQL.AppendLine(") T Where Amt<>0.00 Order By SSPName;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "SSPName", "Amt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append(Sm.DrStr(dr, c[0]));
                            Msg.Append(" : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ShowPayrollProcessAD(string ADType, string PayrunCode, string EmpCode)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            SQL.AppendLine("Select B.ADName, A.Amt ");
            SQL.AppendLine("From TblPayrollProcessAD A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType=@ADType And B.AmtType='1' ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("And A.Amt<>0.00 ");
            SQL.AppendLine("Order By B.ADName;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                    Sm.CmParam<String>(ref cm, "@ADType", ADType);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ADName", "Amt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append(Sm.DrStr(dr, c[0]));
                            Msg.Append(" : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ShowBasicSalaryComponent(string PayrunCode, string EmpCode)
        {
            var SQL = new StringBuilder();
            var Msg = new StringBuilder();

            SQL.AppendLine("Select Concat('Salary : ', Convert(Format(Salary, 2) Using utf8)) As Components ");
            SQL.AppendLine("From TblPayrollProcess1 ");
            SQL.AppendLine("Where EmpCode=@EmpCode ");
            SQL.AppendLine("And PayrunCode=@PayrunCode ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat(B.ADName, ' : ', Convert(Format(A.Amt, 2) Using utf8)) As Components ");
            SQL.AppendLine("From TblPayrollProcessAD A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And Find_In_Set(A.ADCode, IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='ADCodeBasicSalaryComponent'), '')) ;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);


                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "Components" });
                    if (dr.HasRows)
                    {
                        while (dr.Read()) Msg.AppendLine(Sm.DrStr(dr, c[0]));
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ShowCredit(string PayrunCode, string EmpCode)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            for (int i = 1; i <= 10; i++)
            {
                if (i != 1) SQL.AppendLine("Union All ");
                SQL.AppendLine("Select B.CreditName, A.CreditAdvancePayment" + i.ToString() + " As Amt ");
                SQL.AppendLine("From TblPayrollProcess1 A ");
                SQL.AppendLine("Inner Join TblCredit B On A.CreditCode" + i.ToString() + "=B.CreditCode ");
                SQL.AppendLine("Where A.CreditCode" + i.ToString() + " Is Not Null ");
                SQL.AppendLine("And A.CreditAdvancePayment" + i.ToString() + " >0.00 ");
                SQL.AppendLine("And A.PayrunCode=@PayrunCode ");
                SQL.AppendLine("And A.EmpCode=@EmpCode ");
            }
            SQL.AppendLine(";");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "CreditName", "Amt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append(Sm.DrStr(dr, c[0]));
                            Msg.Append(" : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 4].Value = "'" + Sm.GetGrdStr(Grd1, Row, 4);

            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
               // Grd1.Cells[Row, 4].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 4).Length - 1);

            }
            Grd1.EndUpdate();
        }

        private void SetSSPAmt()
        {
            if (mlSSP.Count == 0 || Grd1.Rows.Count == 0 || (Grd1.Rows.Count > 0 && Sm.GetGrdStr(Grd1, 0, 2).Length == 0)) return;

            var SQL = new StringBuilder();
            string Filter = " ", PayrunCode = string.Empty, EmpCode = string.Empty;
            var cm = new MySqlCommand();
            var l = new List<SSProgram>();

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.FilterStr(ref Filter, ref cm, TxtPayCod.Text, new string[] { "B.PayrunCode", "B.PayrunName" });
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

            SQL.AppendLine("Select A.SSPCode, A.EmpCode, A.PayrunCode, A.ErAmt, A.EeAmt ");
            SQL.AppendLine("From TblPayrollProcessSSProgram A ");
            SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode And B.CancelInd = 'N' ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Where Find_In_Set(A.SSPCode, ");
            SQL.AppendLine("    IfNull( ");
            SQL.AppendLine("    (Select ParValue from TblParameter Where ParValue Is Not Null And ParCode='SSProgramNew'), ");
            SQL.AppendLine("    '***')) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    "PayrunCode", "EmpCode", "SSPCode", "ErAmt", "EeAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SSProgram()
                        {
                            PayrunCode = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            SSPCode = Sm.DrStr(dr, c[2]),
                            ErAmt = Sm.DrDec(dr, c[3]),
                            EeAmt = Sm.DrDec(dr, c[4]),
                            ColEr = 0,
                            ColEe = 0
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach (var i in mlSSP)
                {
                    foreach (var j in l.Where(w => Sm.CompareStr(w.SSPCode, i.SSPCode)))
                    {
                        j.ColEr = i.ColEr;
                        j.ColEe = i.ColEe;
                    }
                }

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    PayrunCode = Sm.GetGrdStr(Grd1, r, 2);
                    EmpCode = Sm.GetGrdStr(Grd1, r, 4);
                    foreach (var x in l.Where(w => Sm.CompareStr(w.EmpCode, EmpCode) && Sm.CompareStr(w.PayrunCode, PayrunCode)))
                    {
                        Grd1.Cells[r, x.ColEr].Value = Sm.GetGrdDec(Grd1, r, x.ColEr) + x.ErAmt;
                        Grd1.Cells[r, x.ColEe].Value = Sm.GetGrdDec(Grd1, r, x.ColEe) + x.EeAmt;
                    }
                }
            }
            l.Clear();
        }

        private void SetSSProgram()
        {
            mlSSP = new List<SSP>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            int TempCol = 0;
            int Col = mLatestCol;

            SQL.AppendLine("Select SSPCode, SSPName From TblSSProgram ");
            SQL.AppendLine("Where Find_In_Set(SSPCode, ");
            SQL.AppendLine("    IfNull( ");
            SQL.AppendLine("    (Select ParValue from TblParameter Where ParValue Is Not Null And ParCode='SSProgramNew'), ");
            SQL.AppendLine("    '***')) Order By SSPName;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SSPCode", "SSPName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mlSSP.Add(new SSP()
                        {
                            SSPCode = Sm.DrStr(dr, c[0]),
                            SSPName = Sm.DrStr(dr, c[1]),
                            ColEr = Col + 1,
                            ColEe = Col + 2
                        });
                        Col += 2;
                    }
                }
                dr.Close();
            }
            if (mlSSP.Count > 0)
            {
                foreach (var x in mlSSP)
                {
                    Grd1.Cols.Count += 2;
                    TempCol = Grd1.Cols.Count;

                    Grd1.Cols[TempCol - 2].Text = x.SSPName + Environment.NewLine + "Er";
                    Grd1.Cols[TempCol - 2].Width = 150;
                    Grd1.Header.Cells[0, TempCol - 2].TextAlign = iGContentAlignment.TopCenter;
                    Grd1.Cols[TempCol - 2].CellStyle.ValueType = typeof(string);

                    Grd1.Cols[TempCol - 1].Text = x.SSPName + Environment.NewLine + "Ee";
                    Grd1.Cols[TempCol - 1].Width = 150;
                    Grd1.Header.Cells[0, TempCol - 1].TextAlign = iGContentAlignment.TopCenter;
                    Grd1.Cols[TempCol - 1].CellStyle.ValueType = typeof(string);

                    Sm.GrdFormatDec(Grd1, new int[] { TempCol - 2, TempCol - 1 }, 0);
                    Sm.GrdColReadOnly(true, false, Grd1, new int[] { TempCol - 2, TempCol - 1 });
                }
            }
        }

        private void ParPrint()
        {
            if (IsGrdEmpty() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var lHeader = new List<Header>();
            var l = new List<PaySlipPHT>();
            var l1 = new List<PHTTunaiGaji>();
            var l2 = new List<PHTTunaiTunjTidakTetap>();
            var l3 = new List<PHTNonTunai>();
            var l4 = new List<PHTPotonganNonTunai>();
            var l5 = new List<PHTPotonganLain>();
            var l6 = new List<PHTPotonganTunai>();
            string[] mBulan = { "JANUARI", "FEBRUARI", "MARET", "APRIL", "MEI", "JUNI", "JULI", "AGUSTUS", "SEPTEMBER", "OKTOBER", "NOVEMBER", "DESEMBER", "JANUARI" };

            string[] TableName = { "Header", "PaySlipPHT", "PHTTunaiGaji", "PHTTunaiTunjTidakTetap", "PHTNonTunai", "PHTPotonganNonTunai", "PHTPotonganLain", "PHTPotonganTunai" };
            List<IList> myLists = new List<IList>();

            #region Var FilterBy
            string FilterBySiteHR = string.Empty,
                FilterByDeptHR = string.Empty;

            if(mIsFilterBySiteHR)
            {
                FilterBySiteHR = "And B.SiteCode Is Not Null " + Environment.NewLine +
                                    "    And Exists( " + Environment.NewLine +
                                    "        Select 1 From TblGroupSite " + Environment.NewLine +
                                    "        Where SiteCode=IfNull(B.SiteCode, '') " + Environment.NewLine +
                                    "        And GrpCode In ( " + Environment.NewLine +
                                    "            Select GrpCode From TblUser " + Environment.NewLine +
                                    "            Where UserCode=@UserCode " + Environment.NewLine +
                                    "        ) " + Environment.NewLine +
                                    "    ) ";
            }
            if(mIsFilterByDeptHR)
            {
                FilterByDeptHR = "And B.DeptCode Is Not Null " + Environment.NewLine +
                                    "    And Exists( " + Environment.NewLine +
                                    "        Select 1 From TblGroupDepartment " + Environment.NewLine +
                                    "        Where DeptCode=IfNull(B.DeptCode, '') " + Environment.NewLine +
                                    "        And GrpCode In ( " + Environment.NewLine +
                                    "            Select GrpCode From TblUser " + Environment.NewLine +
                                    "            Where UserCode=@UserCode " + Environment.NewLine +
                                    "        ) " + Environment.NewLine +
                                    "    ) ";
            }
            #endregion

            #region Header 

            lHeader.Add(new Header()
            {
                PrintDt = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
            });

            #endregion

            #region Header2

            string Yr = string.Empty,
                Mth = string.Empty,
                Semester = string.Empty,
                Triwulan = string.Empty,
                DivisionName = string.Empty;

            if(TxtPayCod.Text.Length >= 6)
            {
                Mth = (Decimal.Parse(TxtPayCod.Text.Substring(4, 2)) + 1).ToString();
                Yr = (Mth == "13" ? (Decimal.Parse(Sm.Left(TxtPayCod.Text, 4)) + 1).ToString() : Sm.Left(TxtPayCod.Text, 4));
            }
            else if(TxtPayCod.Text.Length >= 4)
            {
                Yr = Sm.Left(TxtPayCod.Text, 4);
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "Please fill payrun code more then 4 digits.");
            }

            if(Mth.Length > 0)
            {
                if (Decimal.Parse(Mth) <= 3)
                    Triwulan = "I";
                else if (Decimal.Parse(Mth) >= 4 && Decimal.Parse(Mth) <= 6)
                    Triwulan = "II";
                else if (Decimal.Parse(Mth) >= 7 && Decimal.Parse(Mth) <= 9)
                    Triwulan = "III";
                else
                    Triwulan = "IV";

                Semester = Decimal.Parse(Mth) <= 6 ? "I" : "II";
            }

            DivisionName =
                Sm.GetValue(
                    "Select Group_Concat(Distinct D.DivisionName separator '\n') " +
                    "From TblSite A " +
                    "Inner Join TblCostCenter B On A.ProfitCenterCode = B.ProfitCenterCode " +
                    "Inner Join TblDepartment C On B.DeptCode = C.DeptCode " +
                    "Inner Join TblDivision D On C.DivisionCode = D.DivisionCode "+
                    "Where A.SiteCode = @Param "+
                    "Group By A.SiteCode; ", Sm.GetLue(LueSiteCode));

            l.Add(new PaySlipPHT()
            {
                PayrunCode = TxtPayCod.Text,
                Yr = Yr,
                Mth = (Mth.Length == 0 ? "" : mBulan[Int32.Parse(Mth) - 1]),
                Semester = Semester,
                Triwulan = Triwulan,
                SiteName = (Sm.GetLue(LueSiteCode).Length == 0 ? "" : LueSiteCode.Text),
                DivisionName = DivisionName,
            }) ;

            #endregion

            #region Komponen Tetap

            var SQL1 = new StringBuilder();
            var cm1 = new MySqlCommand();
            string Filter1 = " ";

            Sm.CmParam<String>(ref cm1, "@UserCode", Gv.CurrentUserCode);
            Sm.FilterStr(ref Filter1, ref cm1, TxtPayCod.Text, new string[] { "A.PayrunCode", "B.PayrunName" });
            Sm.FilterStr(ref Filter1, ref cm1, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName" });
            Sm.FilterStr(ref Filter1, ref cm1, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter1, ref cm1, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

            SQL1.AppendLine("SELECT 'Gaji Pokok' As Col1, SUM(A.Salary) Col2, 1 as SeqNo ");
            SQL1.AppendLine("From TblPayrollProcess1 A ");
            SQL1.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL1.AppendLine("   And B.CancelInd = 'N' ");
            SQL1.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL1.AppendLine("WHERE 0 = 0 ");
            SQL1.AppendLine(FilterBySiteHR);
            SQL1.AppendLine(FilterByDeptHR);
            SQL1.AppendLine(Filter1);
            SQL1.AppendLine("GROUP BY Col1, SeqNo");

            SQL1.AppendLine("Union All ");

            SQL1.AppendLine("Select 'Tunjangan Tetap' As Col1, SUM(D.Amt) Col2, 2 as SeqNo ");
            SQL1.AppendLine("From TblPayrollProcess1 A ");
            SQL1.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL1.AppendLine("   And B.CancelInd = 'N' ");
            SQL1.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL1.AppendLine("Left Join ");
            SQL1.AppendLine("( ");
            SQL1.AppendLine("   Select T1.PayrunCode, T1.EmpCode, Sum(T1.Amt) Amt ");
            SQL1.AppendLine("   From TblPayrollProcessAD T1 ");
            SQL1.AppendLine("   Inner Join TblParameter T2 On T2.ParCode = 'ADCodeFixedAllowance' ");
            SQL1.AppendLine("       And T2.ParValue Is Not Null ");
            SQL1.AppendLine("       And Find_In_Set(T1.ADCode, T2.Parvalue) ");
            SQL1.AppendLine("       And T1.Amt != 0.00 ");
            SQL1.AppendLine("   Inner Join TblAllowanceDeduction T3 On T1.ADCode = T3.ADCode ");
            SQL1.AppendLine("       And T3.ADType = 'A' ");
            SQL1.AppendLine("   Group By T1.PayrunCode, T1.EmpCode ");
            SQL1.AppendLine(")D On A.PayrunCode = D.PayrunCode And A.EmpCode = D.EmpCode ");
            SQL1.AppendLine("WHERE 0 = 0 ");
            SQL1.AppendLine(FilterBySiteHR);
            SQL1.AppendLine(FilterByDeptHR);
            SQL1.AppendLine(Filter1);
            SQL1.AppendLine("GROUP BY Col1, SeqNo");

            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandText = SQL1.ToString();

                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] { "Col1", "Col2", "SeqNo" });

                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        l1.Add(new PHTTunaiGaji()
                        {
                            Col1 = Sm.DrStr(dr1, c1[0]),
                            Col2 = Sm.DrDec(dr1, c1[1]),
                            PayrunCode = TxtPayCod.Text,
                            SeqNo = Sm.DrInt(dr1, c1[2])
                        });
                    }
                }
                dr1.Close();
            }

            #endregion

            #region Komponen Variabel

            var SQL2 = new StringBuilder();
            var cm2 = new MySqlCommand();
            string Filter2 = " ";

            Sm.CmParam<String>(ref cm2, "@UserCode", Gv.CurrentUserCode);
            Sm.FilterStr(ref Filter2, ref cm2, TxtPayCod.Text, new string[] { "A.PayrunCode", "B.PayrunName" });
            Sm.FilterStr(ref Filter2, ref cm2, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName" });
            Sm.FilterStr(ref Filter2, ref cm2, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter2, ref cm2, Sm.GetLue(LueSiteCode), "B.SiteCode", true);


            #region Var Query WL

            string QueryWL = string.Empty;

            QueryWL = "Left Join ( " + Environment.NewLine +
                        "   SELECT A.EmpCode, D.PayrunCode, C.ADCode, E.WarningLetter " + Environment.NewLine +
                        "    FROM TblEmpWL A " + Environment.NewLine +
                        "    INNER JOIN TblWarningLetter B ON A.WLCode = B.WLCode AND A.CancelInd = 'N' " + Environment.NewLine +
                        "    INNER JOIN TblEmployeeAllowanceDeduction C ON A.EmpCode = C.EmpCode AND B.ADCode = C.ADCode " + Environment.NewLine +
                        "    INNER JOIN TblPayrun D ON 1=1 " + Filter2.Replace("B.", "D.").Replace("A.", "D.") + Environment.NewLine +
                        "    INNER JOIN TblPayrollProcess1 E ON A.EmpCode = E.EmpCode AND D.PayrunCode = E.PayrunCode " + Environment.NewLine +
                        "    WHERE ( " + Environment.NewLine +
                        "        (C.StartDt Is Null And C.EndDt Is Null) Or " + Environment.NewLine +
                        "        (C.StartDt Is Not Null And C.EndDt Is Null And C.StartDt<=D.EndDt) Or " + Environment.NewLine +
                        "        (C.StartDt Is Null And C.EndDt Is Not Null AND D.EndDt<=C.EndDt) Or " + Environment.NewLine +
                        "        (C.StartDt Is Not Null And C.EndDt Is Not Null And C.StartDt<=D.EndDt AND D.EndDt<=C.EndDt) " + Environment.NewLine +
                        "    ) And ( " + Environment.NewLine +
                        "        (A.StartDt Is Null And A.EndDt Is Null) Or " + Environment.NewLine +
                        "	    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=D.EndDt) Or " + Environment.NewLine +
                        "	    (A.StartDt Is Null And A.EndDt Is Not Null And D.EndDt<=A.EndDt) Or " + Environment.NewLine +
                        "	    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=D.EndDt And D.EndDt<=A.EndDt) " + Environment.NewLine +
                        "    ) " + Environment.NewLine +
                        ") Tbl On Tbl.PayrunCode=A.PayrunCode And Tbl.EmpCode = A.EmpCode And Tbl.ADCode = A.ADCode ";

            #endregion

            #region Old

            //SQL2.AppendLine("SELECT H.ADName Col1, if(Tbl.ADCode is not Null, Sum(IfNull(A.Amt, 0.00))-Tbl.WarningLetter, Sum(IfNull(A.Amt, 0.00))) Col2, H.SeqNo, Null As Parent ");
            //SQL2.AppendLine("From TblPayrollProcessAD A ");
            //SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            //SQL2.AppendLine("	And B.CancelInd = 'N'");
            //SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            //SQL2.AppendLine("INNER JOIN TblParameter D ON D.ParCode = 'ADCodeVariableAllowance' ");
            //SQL2.AppendLine("INNER JOIN TblParameter E ON E.ParCode = 'ADCodePositionalVariableAllowance' ");
            //SQL2.AppendLine("INNER JOIN TblParameter F ON F.ParCode = 'ADCodePositionalVariableAllowance2' ");
            //SQL2.AppendLine("INNER JOIN TblParameter G ON G.ParCode = 'ADCodeFacilitiesVariableAllowance' ");
            //SQL2.AppendLine("   And Find_In_Set(A.ADCode, D.ParValue)  ");
            //SQL2.AppendLine("   And A.Amt != 0.00 ");
            //SQL2.AppendLine("   AND !FIND_IN_SET(A.ADCode, IfNull(E.ParValue, '')) ");
            //SQL2.AppendLine("   AND !FIND_IN_SET(A.ADCode, IfNull(F.ParValue, '')) ");
            //SQL2.AppendLine("   AND !FIND_IN_SET(A.ADCode, IfNull(G.ParValue, '')) ");
            //SQL2.AppendLine("Inner Join TblAllowanceDeduction H On A.ADCode = H.ADCode ");
            //SQL2.AppendLine("       And H.ADType = 'A' ");
            //SQL2.AppendLine(QueryWL);
            //SQL2.AppendLine("Where 0 = 0 ");
            //SQL2.AppendLine(FilterBySiteHR);
            //SQL2.AppendLine(FilterByDeptHR);
            //SQL2.AppendLine(Filter2);
            //SQL2.AppendLine("Group By Col1, SeqNo, Parent");
            //SQL2.AppendLine("UNION ALL ");
            //SQL2.AppendLine("SELECT 'Apresiasi Jabatan' AS Col1, 0.00 Col2, 1 AS SeqNo, Null As Parent ");
            //SQL2.AppendLine("FROM TblPayrollProcess1 A ");
            //SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            //SQL2.AppendLine("	And B.CancelInd = 'N'");
            //SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            //SQL2.AppendLine("Where EXISTS( ");
            //SQL2.AppendLine("   SELECT 1 ");
            //SQL2.AppendLine("   From TblPayrollProcessAD X1 ");
            //SQL2.AppendLine("    INNER JOIN TblParameter X2 ON X2.ParCode = 'ADCodePositionalVariableAllowance' ");
            //SQL2.AppendLine("   INNER JOIN TblParameter X3 ON X3.ParCode = 'ADCodePositionalVariableAllowance2' ");
            //SQL2.AppendLine("       AND (FIND_IN_SET(X1.ADCode, X2.ParValue) OR FIND_IN_SET(X1.ADCode, X3.ParValue)) ");
            //SQL2.AppendLine("       AND X1.Amt != 0.00 ");
            //SQL2.AppendLine("   WHERE X1.EmpCode = A.EmpCode And X1.PayrunCode = A.PayrunCode ");
            //SQL2.AppendLine(") ");
            //SQL2.AppendLine(Filter2);
            //SQL2.AppendLine("Group By Col1, SeqNo, Parent ");
            //SQL2.AppendLine("UNION ALL ");
            //SQL2.AppendLine("SELECT 'Fasilitas' AS Col1, 0.00 Col2, 10 AS SeqNo, Null As Parent ");
            //SQL2.AppendLine("FROM TblPayrollProcess1 A ");
            //SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            //SQL2.AppendLine("	And B.CancelInd = 'N'");
            //SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            //SQL2.AppendLine("Where EXISTS( ");
            //SQL2.AppendLine("   SELECT 1 ");
            //SQL2.AppendLine("   From TblPayrollProcessAD X1  ");
            //SQL2.AppendLine("   INNER JOIN TblParameter X2 ON X2.ParCode = 'ADCodeFacilitiesVariableAllowance' ");
            //SQL2.AppendLine("       And FIND_IN_SET(X1.ADCode, X2.ParValue) ");
            //SQL2.AppendLine("       AND X1.Amt != 0.00 ");
            //SQL2.AppendLine("   WHERE X1.EmpCode = A.EmpCode And X1.PayrunCode = A.PayrunCode ");
            //SQL2.AppendLine(") ");
            //SQL2.AppendLine(Filter2);
            //SQL2.AppendLine("Group By Col1, SeqNo, Parent ");
            //SQL2.AppendLine("UNION ALL ");
            //SQL2.AppendLine("SELECT 'Pejabat' Col1, 0.00 Col2, 2 AS SeqNo, 'Apresiasi Jabatan' AS Parent ");
            //SQL2.AppendLine("FROM TblPayrollProcess1 A ");
            //SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            //SQL2.AppendLine("	And B.CancelInd = 'N'");
            //SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            //SQL2.AppendLine("Where EXISTS( ");
            //SQL2.AppendLine("   SELECT 1 ");
            //SQL2.AppendLine("   From TblPayrollProcessAD X1  ");
            //SQL2.AppendLine("   INNER JOIN TblParameter X2 ON X2.ParCode = 'ADCodePositionalVariableAllowance' ");
            //SQL2.AppendLine("       And FIND_IN_SET(X1.ADCode, X2.ParValue) ");
            //SQL2.AppendLine("       AND X1.Amt != 0.00 ");
            //SQL2.AppendLine("   WHERE X1.EmpCode = A.EmpCode And X1.PayrunCode = A.PayrunCode ");
            //SQL2.AppendLine(") ");
            //SQL2.AppendLine(Filter2);
            //SQL2.AppendLine("Group By Col1, SeqNo, Parent ");
            //SQL2.AppendLine("UNION ALL ");
            //SQL2.AppendLine("SELECT 'Staff' Col1, 0.00 Col2, 6 AS SeqNo, 'Apresiasi Jabatan' AS Parent ");
            //SQL2.AppendLine("FROM TblPayrollProcess1 A ");
            //SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            //SQL2.AppendLine("	And B.CancelInd = 'N'");
            //SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            //SQL2.AppendLine("Where EXISTS( ");
            //SQL2.AppendLine("   SELECT 1 ");
            //SQL2.AppendLine("   From TblPayrollProcessAD X1  ");
            //SQL2.AppendLine("   INNER JOIN TblParameter X2 ON X2.ParCode = 'ADCodePositionalVariableAllowance2' ");
            //SQL2.AppendLine("       And FIND_IN_SET(X1.ADCode, X2.ParValue) ");
            //SQL2.AppendLine("       AND X1.Amt != 0.00 ");
            //SQL2.AppendLine("   WHERE X1.EmpCode = A.EmpCode And X1.PayrunCode = A.PayrunCode ");
            //SQL2.AppendLine(") ");
            //SQL2.AppendLine(Filter2);
            //SQL2.AppendLine("Group By Col1, SeqNo, Parent ");
            //SQL2.AppendLine("UNION ALL ");
            //SQL2.AppendLine("SELECT E.ADName Col1, if(Tbl.ADCode is not Null, Sum(IfNull(A.Amt-Tbl.WarningLetter, 0.00)), Sum(IfNull(A.Amt, 0.00))) Col2, E.SeqNo, 'Fasilitas' As Parent ");
            //SQL2.AppendLine("FROM TblPayrollProcessAD A ");
            //SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            //SQL2.AppendLine("	And B.CancelInd = 'N'");
            //SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            //SQL2.AppendLine("INNER JOIN TblParameter D ON D.ParCode = 'ADCodeFacilitiesVariableAllowance' ");
            //SQL2.AppendLine("   And Find_In_Set(A.ADCode, D.ParValue)  ");
            //SQL2.AppendLine("   And A.Amt != 0.00 ");
            //SQL2.AppendLine("Inner Join TblAllowanceDeduction E On A.ADCode = E.ADCode ");
            //SQL2.AppendLine("       And E.ADType = 'A' ");
            //SQL2.AppendLine(QueryWL);
            //SQL2.AppendLine("Where 0 = 0 ");
            //SQL2.AppendLine(FilterBySiteHR);
            //SQL2.AppendLine(FilterByDeptHR);
            //SQL2.AppendLine(Filter2);
            //SQL2.AppendLine("Group By Col1, SeqNo, Parent ");
            //SQL2.AppendLine("UNION ALL ");
            //SQL2.AppendLine("SELECT E.ADName Col1, if(Tbl.ADCode is not Null, Sum(IfNull(A.Amt-Tbl.WarningLetter, 0.00)), Sum(IfNull(A.Amt, 0.00))) Col2, E.SeqNo, 'Pejabat' As Parent ");
            //SQL2.AppendLine("FROM TblPayrollProcessAD A ");
            //SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            //SQL2.AppendLine("	And B.CancelInd = 'N'");
            //SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            //SQL2.AppendLine("INNER JOIN TblParameter D ON D.ParCode = 'ADCodePositionalVariableAllowance' ");
            //SQL2.AppendLine("   And Find_In_Set(A.ADCode, D.ParValue)  ");
            //SQL2.AppendLine("   And A.Amt != 0.00 ");
            //SQL2.AppendLine("Inner Join TblAllowanceDeduction E On A.ADCode = E.ADCode ");
            //SQL2.AppendLine("       And E.ADType = 'A' ");
            //SQL2.AppendLine(QueryWL);
            //SQL2.AppendLine("Where 0 = 0 ");
            //SQL2.AppendLine(FilterBySiteHR);
            //SQL2.AppendLine(FilterByDeptHR);
            //SQL2.AppendLine(Filter2);
            //SQL2.AppendLine("Group By Col1, SeqNo, Parent ");
            //SQL2.AppendLine("UNION ALL ");
            //SQL2.AppendLine("SELECT E.ADName Col1, if(Tbl.ADCode is not Null, Sum(IfNull(A.Amt-Tbl.WarningLetter, 0.00)), Sum(IfNull(A.Amt, 0.00))) Col2, E.SeqNo, 'Staff' As Parent ");
            //SQL2.AppendLine("FROM TblPayrollProcessAD A ");
            //SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            //SQL2.AppendLine("	And B.CancelInd = 'N'");
            //SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            //SQL2.AppendLine("INNER JOIN TblParameter D ON D.ParCode = 'ADCodePositionalVariableAllowance2' ");
            //SQL2.AppendLine("   And Find_In_Set(A.ADCode, D.ParValue)  ");
            //SQL2.AppendLine("   And A.Amt != 0.00 ");
            //SQL2.AppendLine("Inner Join TblAllowanceDeduction E On A.ADCode = E.ADCode ");
            //SQL2.AppendLine("       And E.ADType = 'A' ");
            //SQL2.AppendLine(QueryWL);
            //SQL2.AppendLine("Where 0 = 0 ");
            //SQL2.AppendLine(FilterBySiteHR);
            //SQL2.AppendLine(FilterByDeptHR);
            //SQL2.AppendLine(Filter2);
            //SQL2.AppendLine("Group By Col1, SeqNo, Parent ");

            #endregion

            SQL2.AppendLine("Select Col1, Sum(Col2) Col2, SeqNo, Parent ");
            SQL2.AppendLine("From ( ");
            SQL2.AppendLine("SELECT A.EmpCode, H.ADName Col1, if(Tbl.ADCode is not Null, IfNull(A.Amt, 0.00)-Tbl.WarningLetter, IfNull(A.Amt, 0.00)) Col2, H.SeqNo, Null As Parent ");
            SQL2.AppendLine("From TblPayrollProcessAD A ");
            SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL2.AppendLine("	And B.CancelInd = 'N'");
            SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL2.AppendLine("INNER JOIN TblParameter D ON D.ParCode = 'ADCodeVariableAllowance' ");
            SQL2.AppendLine("INNER JOIN TblParameter E ON E.ParCode = 'ADCodePositionalVariableAllowance' ");
            SQL2.AppendLine("INNER JOIN TblParameter F ON F.ParCode = 'ADCodePositionalVariableAllowance2' ");
            SQL2.AppendLine("INNER JOIN TblParameter G ON G.ParCode = 'ADCodeFacilitiesVariableAllowance' ");
            SQL2.AppendLine("   And Find_In_Set(A.ADCode, D.ParValue)  ");
            SQL2.AppendLine("   And A.Amt != 0.00 ");
            SQL2.AppendLine("   AND !FIND_IN_SET(A.ADCode, IfNull(E.ParValue, '')) ");
            SQL2.AppendLine("   AND !FIND_IN_SET(A.ADCode, IfNull(F.ParValue, '')) ");
            SQL2.AppendLine("   AND !FIND_IN_SET(A.ADCode, IfNull(G.ParValue, '')) ");
            SQL2.AppendLine("Inner Join TblAllowanceDeduction H On A.ADCode = H.ADCode ");
            SQL2.AppendLine("       And H.ADType = 'A' ");
            SQL2.AppendLine(QueryWL);
            SQL2.AppendLine("Where 0 = 0 ");
            SQL2.AppendLine(FilterBySiteHR);
            SQL2.AppendLine(FilterByDeptHR);
            SQL2.AppendLine(Filter2);
            SQL2.AppendLine("UNION ALL ");
            SQL2.AppendLine("SELECT A.EmpCode, 'Apresiasi Jabatan' AS Col1, 0.00 Col2, 1 AS SeqNo, Null As Parent ");
            SQL2.AppendLine("FROM TblPayrollProcess1 A ");
            SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL2.AppendLine("	And B.CancelInd = 'N'");
            SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL2.AppendLine("Where EXISTS( ");
            SQL2.AppendLine("   SELECT 1 ");
            SQL2.AppendLine("   From TblPayrollProcessAD X1 ");
            SQL2.AppendLine("    INNER JOIN TblParameter X2 ON X2.ParCode = 'ADCodePositionalVariableAllowance' ");
            SQL2.AppendLine("   INNER JOIN TblParameter X3 ON X3.ParCode = 'ADCodePositionalVariableAllowance2' ");
            SQL2.AppendLine("       AND (FIND_IN_SET(X1.ADCode, X2.ParValue) OR FIND_IN_SET(X1.ADCode, X3.ParValue)) ");
            SQL2.AppendLine("       AND X1.Amt != 0.00 ");
            SQL2.AppendLine("   WHERE X1.EmpCode = A.EmpCode And X1.PayrunCode = A.PayrunCode ");
            SQL2.AppendLine(") ");
            SQL2.AppendLine(Filter2);
            SQL2.AppendLine("UNION ALL ");
            SQL2.AppendLine("SELECT A.EmpCode, 'Fasilitas' AS Col1, 0.00 Col2, 10 AS SeqNo, Null As Parent ");
            SQL2.AppendLine("FROM TblPayrollProcess1 A ");
            SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL2.AppendLine("	And B.CancelInd = 'N'");
            SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL2.AppendLine("Where EXISTS( ");
            SQL2.AppendLine("   SELECT 1 ");
            SQL2.AppendLine("   From TblPayrollProcessAD X1  ");
            SQL2.AppendLine("   INNER JOIN TblParameter X2 ON X2.ParCode = 'ADCodeFacilitiesVariableAllowance' ");
            SQL2.AppendLine("       And FIND_IN_SET(X1.ADCode, X2.ParValue) ");
            SQL2.AppendLine("       AND X1.Amt != 0.00 ");
            SQL2.AppendLine("   WHERE X1.EmpCode = A.EmpCode And X1.PayrunCode = A.PayrunCode ");
            SQL2.AppendLine(") ");
            SQL2.AppendLine(Filter2);
            SQL2.AppendLine("UNION ALL ");
            SQL2.AppendLine("SELECT A.EmpCode, 'Pejabat' Col1, 0.00 Col2, 2 AS SeqNo, 'Apresiasi Jabatan' AS Parent ");
            SQL2.AppendLine("FROM TblPayrollProcess1 A ");
            SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL2.AppendLine("	And B.CancelInd = 'N'");
            SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL2.AppendLine("Where EXISTS( ");
            SQL2.AppendLine("   SELECT 1 ");
            SQL2.AppendLine("   From TblPayrollProcessAD X1  ");
            SQL2.AppendLine("   INNER JOIN TblParameter X2 ON X2.ParCode = 'ADCodePositionalVariableAllowance' ");
            SQL2.AppendLine("       And FIND_IN_SET(X1.ADCode, X2.ParValue) ");
            SQL2.AppendLine("       AND X1.Amt != 0.00 ");
            SQL2.AppendLine("   WHERE X1.EmpCode = A.EmpCode And X1.PayrunCode = A.PayrunCode ");
            SQL2.AppendLine(") ");
            SQL2.AppendLine(Filter2);
            SQL2.AppendLine("UNION ALL ");
            SQL2.AppendLine("SELECT A.EmpCode, 'Staff' Col1, 0.00 Col2, 6 AS SeqNo, 'Apresiasi Jabatan' AS Parent ");
            SQL2.AppendLine("FROM TblPayrollProcess1 A ");
            SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL2.AppendLine("	And B.CancelInd = 'N'");
            SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL2.AppendLine("Where EXISTS( ");
            SQL2.AppendLine("   SELECT 1 ");
            SQL2.AppendLine("   From TblPayrollProcessAD X1  ");
            SQL2.AppendLine("   INNER JOIN TblParameter X2 ON X2.ParCode = 'ADCodePositionalVariableAllowance2' ");
            SQL2.AppendLine("       And FIND_IN_SET(X1.ADCode, X2.ParValue) ");
            SQL2.AppendLine("       AND X1.Amt != 0.00 ");
            SQL2.AppendLine("   WHERE X1.EmpCode = A.EmpCode And X1.PayrunCode = A.PayrunCode ");
            SQL2.AppendLine(") ");
            SQL2.AppendLine(Filter2);
            SQL2.AppendLine("UNION ALL ");
            SQL2.AppendLine("SELECT A.EmpCode, E.ADName Col1, if(Tbl.ADCode is not Null, IfNull(A.Amt, 0.00)-Tbl.WarningLetter, IfNull(A.Amt, 0.00)) Col2, E.SeqNo, 'Fasilitas' As Parent ");
            SQL2.AppendLine("FROM TblPayrollProcessAD A ");
            SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL2.AppendLine("	And B.CancelInd = 'N'");
            SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL2.AppendLine("INNER JOIN TblParameter D ON D.ParCode = 'ADCodeFacilitiesVariableAllowance' ");
            SQL2.AppendLine("   And Find_In_Set(A.ADCode, D.ParValue)  ");
            SQL2.AppendLine("   And A.Amt != 0.00 ");
            SQL2.AppendLine("Inner Join TblAllowanceDeduction E On A.ADCode = E.ADCode ");
            SQL2.AppendLine("       And E.ADType = 'A' ");
            SQL2.AppendLine(QueryWL);
            SQL2.AppendLine("Where 0 = 0 ");
            SQL2.AppendLine(FilterBySiteHR);
            SQL2.AppendLine(FilterByDeptHR);
            SQL2.AppendLine(Filter2);
            SQL2.AppendLine("UNION ALL ");
            SQL2.AppendLine("SELECT A.EmpCode, E.ADName Col1, if(Tbl.ADCode is not Null, IfNull(A.Amt, 0.00)-Tbl.WarningLetter, IfNull(A.Amt, 0.00)) Col2, E.SeqNo, 'Pejabat' As Parent ");
            SQL2.AppendLine("FROM TblPayrollProcessAD A ");
            SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL2.AppendLine("	And B.CancelInd = 'N'");
            SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL2.AppendLine("INNER JOIN TblParameter D ON D.ParCode = 'ADCodePositionalVariableAllowance' ");
            SQL2.AppendLine("   And Find_In_Set(A.ADCode, D.ParValue)  ");
            SQL2.AppendLine("   And A.Amt != 0.00 ");
            SQL2.AppendLine("Inner Join TblAllowanceDeduction E On A.ADCode = E.ADCode ");
            SQL2.AppendLine("       And E.ADType = 'A' ");
            SQL2.AppendLine(QueryWL);
            SQL2.AppendLine("Where 0 = 0 ");
            SQL2.AppendLine(FilterBySiteHR);
            SQL2.AppendLine(FilterByDeptHR);
            SQL2.AppendLine(Filter2);
            SQL2.AppendLine("UNION ALL ");
            SQL2.AppendLine("SELECT A.EmpCode, E.ADName Col1, if(Tbl.ADCode is not Null, IfNull(A.Amt, 0.00)-Tbl.WarningLetter, IfNull(A.Amt, 0.00)) Col2, E.SeqNo, 'Staff' As Parent ");
            SQL2.AppendLine("FROM TblPayrollProcessAD A ");
            SQL2.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL2.AppendLine("	And B.CancelInd = 'N'");
            SQL2.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL2.AppendLine("INNER JOIN TblParameter D ON D.ParCode = 'ADCodePositionalVariableAllowance2' ");
            SQL2.AppendLine("   And Find_In_Set(A.ADCode, D.ParValue)  ");
            SQL2.AppendLine("   And A.Amt != 0.00 ");
            SQL2.AppendLine("Inner Join TblAllowanceDeduction E On A.ADCode = E.ADCode ");
            SQL2.AppendLine("       And E.ADType = 'A' ");
            SQL2.AppendLine(QueryWL);
            SQL2.AppendLine("Where 0 = 0 ");
            SQL2.AppendLine(FilterBySiteHR);
            SQL2.AppendLine(FilterByDeptHR);
            SQL2.AppendLine(Filter2);
            SQL2.AppendLine(")Tbl ");
            SQL2.AppendLine("Group By Col1, SeqNo, Parent ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();

                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] { "Col1", "Col2", "SeqNo", "Parent" });

                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new PHTTunaiTunjTidakTetap()
                        {
                            PayrunCode = TxtPayCod.Text,
                            Col1 = Sm.DrStr(dr2, c2[0]),
                            Col2 = Sm.DrDec(dr2, c2[1]),
                            SeqNo = Sm.DrInt(dr2, c2[2]),
                            Parent = Sm.DrStr(dr2, c2[3])
                        });
                    }
                }
                dr2.Close();
            }

            #endregion

            #region Subsudi

            var SQL3 = new StringBuilder();
            var cm3 = new MySqlCommand();
            string Filter3 = " ";

            Sm.CmParam<String>(ref cm3, "@UserCode", Gv.CurrentUserCode);
            Sm.FilterStr(ref Filter3, ref cm3, TxtPayCod.Text, new string[] { "A.PayrunCode", "B.PayrunName" });
            Sm.FilterStr(ref Filter3, ref cm3, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName" });
            Sm.FilterStr(ref Filter3, ref cm3, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter3, ref cm3, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

            SQL3.AppendLine("Select D.SSPName Col1, ");
            //SQL3.AppendLine("if(D.SSPCode = 'BPJS-KTNK', Sum(A.ErAmt)-Sum(E.SSerPension), Sum(A.ErAmt)) Col2, ");
            SQL3.AppendLine("Case ");
            SQL3.AppendLine("	When H.Parvalue = 'Y' Then ");
            SQL3.AppendLine("		Case ");
            SQL3.AppendLine("			when D.SSPCode = 'BPJS-KTNK' then Sum(E.SSEmployerEmployment) ");
            SQL3.AppendLine("			when D.SSPCode = 'BPJS-KES' then Sum(E.SSEmployerHealth) ");
            SQL3.AppendLine("			else Sum(A.ErAmt) ");
            SQL3.AppendLine("		End ");
            SQL3.AppendLine("	Else ");
            SQL3.AppendLine("		if(D.SSPCode = 'BPJS-KTNK', Sum(A.ErAmt)-Sum(E.SSerPension), Sum(A.ErAmt)) ");
            SQL3.AppendLine("End Col2, ");
            SQL3.AppendLine("F.EmployerPerc 'perc', D.SeqNo Sequence, if(A.SSPCode Like 'BPJS%', 'BPJS', NULL) Parent ");
            SQL3.AppendLine("From TblPayrollProcessSSProgram A ");
            SQL3.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL3.AppendLine("   And B.CancelInd = 'N' ");
            SQL3.AppendLine("   And A.ErAmt != 0.00 ");
            SQL3.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL3.AppendLine("Inner Join TblSSProgram D On A.SSPCode = D.SSPCode ");
            SQL3.AppendLine("Inner Join TblPayrollProcess1 E On A.EmpCode = E.EmpCode And E.PayrunCode = A.PayrunCode ");
            SQL3.AppendLine("INNER JOIN ( ");
            SQL3.AppendLine("	Select SSCode, SSPCode, Sum(EmployerPerc) EmployerPerc ");
            SQL3.AppendLine("	From tblss ");
            SQL3.AppendLine("	Where SScode !='Jampen'");
            SQL3.AppendLine("	Group By SSPCode ");
            SQL3.AppendLine(")F ON D.SSPCode = F.SSPCode ");
            SQL3.AppendLine("INNER JOIN TblParameter G ON G.ParCode = 'SSProgramSubsidyForPayslip' ");
            SQL3.AppendLine("   	AND FIND_IN_SET(D.SSPCode, G.ParValue) ");
            SQL3.AppendLine("Inner Join TblParameter H On H.ParCode = 'IsPayrollProcessingCalculateBPJSAutomatic' and H.Parvalue is not null ");
            SQL3.AppendLine("WHERE F.SScode !='Jampen' And A.ErAmt-E.SSerPension != 0");
            SQL3.AppendLine(FilterBySiteHR);
            SQL3.AppendLine(FilterByDeptHR);
            SQL3.AppendLine(Filter3);
            SQL3.AppendLine("GROUP BY col1, Perc, Sequence, Parent ");

            SQL3.AppendLine("UNION ALL ");
            SQL3.AppendLine("SELECT 'Pensiun' Col1, Sum(IfNull(E.SSerPension, 0.00)) Col2, F.EmployerPerc 'perc', 8 As Sequence, 'BPJS' Parent ");
            SQL3.AppendLine("FROM TblPayrollProcessSSProgram A ");
            SQL3.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL3.AppendLine("   And B.CancelInd = 'N' ");
            SQL3.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL3.AppendLine("INNER JOIN TblSSProgram D ON A.SSPCode = D.SSPCode ");
            SQL3.AppendLine("INNER JOIN TblPayrollProcess1 E ON A.EmpCode = E.EmpCode AND E.PayrunCode = A.PayrunCode ");
            SQL3.AppendLine("   And E.SSerPension !=0 ");
            SQL3.AppendLine("INNER JOIN tblss F ON D.SSPCode = F.SSPCode ");
            SQL3.AppendLine("WHERE F.SScode ='Jampen' AND D.SSPCode = 'BPJS-KTNK' ");
            SQL3.AppendLine(FilterBySiteHR);
            SQL3.AppendLine(FilterByDeptHR);
            SQL3.AppendLine(Filter3);
            SQL3.AppendLine("GROUP BY col1, Perc, Sequence, Parent ");

            SQL3.AppendLine("Union All ");
            SQL3.AppendLine("Select 'Tunjangan Pajak' Col1, Sum(TaxAllowance) Col2, '' Perc, 12 As Sequence, NULL Parent ");
            SQL3.AppendLine("From TblPayrollProcess1 A ");
            SQL3.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL3.AppendLine("   And B.CancelInd = 'N' ");
            SQL3.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL3.AppendLine("Where A.TaxAllowance != 0.00 ");
            SQL3.AppendLine(FilterBySiteHR);
            SQL3.AppendLine(FilterByDeptHR);
            SQL3.AppendLine(Filter3);
            SQL3.AppendLine("GROUP BY col1, Perc, Sequence, Parent ");

            SQL3.AppendLine("Union All ");
            SQL3.AppendLine("SELECT C.ADName Col1, Sum(IfNull(A.Amt, 0.00)) Col2, D.EmployerPerc Perc, C.SeqNo As SEQUENCE, NULL As Parent  ");
            SQL3.AppendLine("From TblPayrollProcessAD A ");
            SQL3.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL3.AppendLine("INNER JOIN tblallowancededuction C ON A.ADCode = C.ADCode ");
            SQL3.AppendLine("Inner Join TblSS D On C.SSCode = D.SSCode ");
            SQL3.AppendLine("Inner Join TblParameter E On E.ParCode='SSProgramForPayslipMD' And E.ParValue Is Not Null ");
            SQL3.AppendLine("Where A.Amt<>0.00 ");
            SQL3.AppendLine("   And FIND_IN_SET(A.ADCode , E.ParValue) ");
            SQL3.AppendLine(FilterBySiteHR);
            SQL3.AppendLine(FilterByDeptHR);
            SQL3.AppendLine(Filter3);
            SQL3.AppendLine("GROUP BY Col1, Perc, Sequence, Parent ");

            SQL3.AppendLine("Union All");
            SQL3.AppendLine("SELECT C.ADName Col1, Sum(IfNull(A.Amt, 0.00)) Col2, D.TotalPerc Perc, C.SeqNo As SEQUENCE, 'BPJS' Parent  ");
            SQL3.AppendLine("From TblPayrollProcessAD A ");
            SQL3.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL3.AppendLine("INNER JOIN tblallowancededuction C ON A.ADCode = C.ADCode ");
            SQL3.AppendLine("Inner Join ( ");
            SQL3.AppendLine("   SELECT A.SSCode, Sum(EmployerPerc) TotalPerc ");
            SQL3.AppendLine("   From tblallowancededuction A ");
            SQL3.AppendLine("   INNER JOIN tblss B ON FIND_IN_SET(B.SSCode, A.SSCode) ");
            SQL3.AppendLine("   Group By SSCode ");
            SQL3.AppendLine(")D ON C.SSCode = D.SSCode ");
            SQL3.AppendLine("Inner Join TblParameter E On E.ParCode='SSProgramForPayslipBPJSMD' And E.ParValue Is Not Null ");
            SQL3.AppendLine("Where A.Amt<>0.00 ");
            SQL3.AppendLine("   And FIND_IN_SET(A.ADCode , E.ParValue) ");
            SQL3.AppendLine(FilterBySiteHR);
            SQL3.AppendLine(FilterByDeptHR);
            SQL3.AppendLine(Filter3);
            SQL3.AppendLine("GROUP By Col1, Perc, Sequence, Parent ");

            SQL3.AppendLine("Union All ");
            SQL3.AppendLine("SELECT 'BPJS' Col1, 0.00 Col2, '' Perc, 5 AS Sequence, NULL Parent ");
            SQL3.AppendLine("FROM TblPayrollProcessSSProgram A ");
            SQL3.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL3.AppendLine("   And B.CancelInd = 'N' ");
            SQL3.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL3.AppendLine("AND ( ");
            SQL3.AppendLine("   EXISTS ( ");
            SQL3.AppendLine("	    SELECT 1 ");
            SQL3.AppendLine("       FROM TblPayrollProcessSSProgram X1 ");
            SQL3.AppendLine("       INNER JOIN TblSSProgram X2 ON X1.SSPCode = X2.SSPCode ");
            SQL3.AppendLine("       INNER JOIN TblPayrollProcess1 X3 ON X1.EmpCode = X3.EmpCode And X3.PayrunCode = X1.PayrunCode ");
            SQL3.AppendLine("           AND (X1.ErAmt != 0.00 AND X1.ErAmt-X3.SSerPension != 0) ");
            SQL3.AppendLine("           OR X3.SSerPension !=0 ");
            SQL3.AppendLine("       INNER JOIN TblSS X4 ON X2.SSPCode = X4.SSPCode ");
            SQL3.AppendLine("       INNER JOIN TblPayrun X5 On X1.PayrunCode = X5.PayrunCode ");
            SQL3.AppendLine("       And X5.CancelInd = 'N' ");
            SQL3.AppendLine("       Where (X2.SSPCode LIKE 'BPJS%' OR (X4.SSCode = 'Jampen' AND X2.sspcode = 'BPJS-KTNK')) ");
            SQL3.AppendLine(Filter3.Replace("A.", "X1.").Replace("B.", "X5.").Replace("C.", "X1."));
            SQL3.AppendLine("   ) ");
            SQL3.AppendLine("   OR EXISTS ( ");
            SQL3.AppendLine("		SELECT 1 ");
            SQL3.AppendLine("		From tblpayrollprocessad Y1  ");
            SQL3.AppendLine("		INNER JOIN tblallowancededuction Y2 ON Y1.ADCode = Y2.ADCode  ");
            SQL3.AppendLine("		Inner Join (  ");
            SQL3.AppendLine("			SELECT Y1.SSCode, SUM(Y2.TotalPerc) TotalPerc  ");
            SQL3.AppendLine("			From tblallowancededuction Y1 ");
            SQL3.AppendLine("			INNER JOIN tblss Y2 ON FIND_IN_SET(Y2.SSCode, Y1.SSCode)  ");
            SQL3.AppendLine("			Group By SSCode  ");
            SQL3.AppendLine("		)Y3 ON Y2.SSCode = Y3.SSCode  ");
            SQL3.AppendLine("		Inner Join tblparameter Y4 ON Y4.ParCode='SSProgramForPayslipBPJSMD' AND Y4.ParValue Is Not Null ");
            SQL3.AppendLine("       Inner Join TblPayrun Y5 On Y1.PayrunCode = Y5.PayrunCode ");
            SQL3.AppendLine("		WHERE Y1.Amt<>0.00 ");
            SQL3.AppendLine("		AND FIND_IN_SET(Y1.ADCode , Y4.ParValue)  ");
            SQL3.AppendLine(Filter3.Replace("A.", "Y1.").Replace("B.", "Y5.").Replace("C.", "Y1."));
            SQL3.AppendLine("	)");
            SQL3.AppendLine(") ");
            SQL3.AppendLine(Filter3);
            SQL3.AppendLine("Group By Col1, Perc, Sequence, Parent ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();

                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] { "Col1", "Col2", "Perc", "Sequence", "Parent" });

                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new PHTNonTunai()
                        {
                            PayrunCode = TxtPayCod.Text,
                            Col1 = Sm.DrStr(dr3, c3[0]),
                            Col2 = Sm.DrDec(dr3, c3[1]),
                            Perc = Sm.DrStr(dr3, c3[2]),
                            SeqNo = Sm.DrInt(dr3, c3[3]),
                            Parent = Sm.DrStr(dr3, c3[4])
                        });
                    }
                }
                dr3.Close();
            }

            #endregion

            #region Potongan Non Tunai

            var SQL4= new StringBuilder();
            var cm4 = new MySqlCommand();
            string Filter4 = " ";

            Sm.CmParam<String>(ref cm4, "@UserCode", Gv.CurrentUserCode);
            Sm.FilterStr(ref Filter4, ref cm4, TxtPayCod.Text, new string[] { "A.PayrunCode", "B.PayrunName" });
            Sm.FilterStr(ref Filter4, ref cm4, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName" });
            Sm.FilterStr(ref Filter4, ref cm4, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter4, ref cm4, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

            SQL4.AppendLine("SELECT D.SeqNo As Sequence, D.SSPName Col1, Sum(A.EeAmt) Col2, F.TotalPerc 'perc', Null As Parent ");
            SQL4.AppendLine("From TblPayrollProcessSSProgram A ");
            SQL4.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL4.AppendLine("   And B.CancelInd = 'N' ");
            SQL4.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL4.AppendLine("Inner Join TblSSProgram D On A.SSPCode = D.SSPCode ");
            SQL4.AppendLine("INNER Join TblParameter E On E.ParCode='SSProgramForPayslipBPJS' And E.ParValue Is Not Null ");
            SQL4.AppendLine("   And !Find_In_Set(A.SSPCode, E.ParValue) ");
            SQL4.AppendLine("INNER JOIN tblss F ON A.SSPCode = F.SSPCode");
            SQL4.AppendLine("Where A.EeAmt<>0.00 ");
            SQL4.AppendLine("AND !FIND_IN_SET(A.SSPCode, 'WNR') ");
            SQL4.AppendLine(Filter4);
            SQL4.AppendLine(FilterBySiteHR);
            SQL4.AppendLine(FilterByDeptHR);
            SQL4.AppendLine("GROUP BY Sequence, Col1, Perc, Parent ");

            SQL4.AppendLine("Union All ");

            SQL4.AppendLine("SELECT D.SeqNo As Sequence, Concat(E.ParValue, ' ', D.SSPName) Col1, Sum(A.ErAmt) Col2, G.TotalPerc Perc, Null As Parent ");
            SQL4.AppendLine("From TblPayrollProcessSSProgram A ");
            SQL4.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL4.AppendLine("   And B.CancelInd = 'N' ");
            SQL4.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL4.AppendLine("Inner Join TblSSProgram D On A.SSPCode = D.SSPCode ");
            SQL4.AppendLine("Inner Join TblParameter E On E.ParCode='AdditionalDescForPayslipSSPContribution' And E.ParValue Is Not Null ");
            SQL4.AppendLine("Inner Join TblParameter F On F.ParCode='SSProgramForPayslipContribution' And F.ParValue Is Not Null ");
            SQL4.AppendLine("   And Find_In_Set(A.SSPCode, F.ParValue) ");
            SQL4.AppendLine("INNER JOIN tblss G ON D.SSPCode = G.SSPCode");
            SQL4.AppendLine("Where A.ErAmt<>0.00 ");
            SQL4.AppendLine(Filter4);
            SQL4.AppendLine(FilterBySiteHR);
            SQL4.AppendLine(FilterByDeptHR);
            SQL4.AppendLine("GROUP BY Sequence, Col1, Perc, Parent ");

            SQL4.AppendLine("Union All ");

            SQL4.AppendLine("SELECT D.SeqNo As Sequence, D.SSPName Col1, ");
            //SQL4.AppendLine("if(A.SSPCode = 'BPJS-KTNK', Sum(A.ErAmt+A.EeAmt)-Sum(F.SSerPension+F.SSeePension), Sum(A.ErAmt+A.EeAmt)) Col2, ");
            SQL4.AppendLine("Case ");
            SQL4.AppendLine("	When H.Parvalue = 'Y' Then ");
            SQL4.AppendLine("		Case ");
            SQL4.AppendLine("			when D.SSPCode = 'BPJS-KTNK' then Sum(F.SSEmployerEmployment+F.SSEmployeeEmployment) ");
            SQL4.AppendLine("			when D.SSPCode = 'BPJS-KES' then Sum(F.SSEmployerHealth+F.SSEmployeeHealth) ");
            SQL4.AppendLine("			else Sum(A.ErAmt+A.EeAmt) ");
            SQL4.AppendLine("		End ");
            SQL4.AppendLine("	Else ");
            SQL4.AppendLine("		if(A.SSPCode = 'BPJS-KTNK', Sum(A.ErAmt+A.EeAmt)-Sum(F.SSerPension+F.SSeePension), Sum(A.ErAmt+A.EeAmt)) ");
            SQL4.AppendLine("End Col2, ");
            SQL4.AppendLine("G.TotalPerc Perc, 'BPJS' As Parent ");
            SQL4.AppendLine("From TblPayrollProcessSSProgram A ");
            SQL4.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL4.AppendLine("   And B.CancelInd = 'N' ");
            SQL4.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL4.AppendLine("Inner Join TblSSProgram D On A.SSPCode = D.SSPCode ");
            SQL4.AppendLine("Inner Join TblParameter E On E.ParCode='SSProgramForPayslipBPJS' And E.ParValue Is Not Null ");
            SQL4.AppendLine("   And Find_In_Set(A.SSPCode, E.ParValue) ");
            SQL4.AppendLine("Inner Join TblPayrollProcess1 F On A.EmpCode = F.EmpCode And F.PayrunCode = A.PayrunCode ");
            SQL4.AppendLine("INNER JOIN ( ");
            SQL4.AppendLine("	SELECT Group_Concat(SSCode) SSCode, SSPCode, SUM(TotalPerc) TotalPerc ");
            SQL4.AppendLine("	FROM TblSS ");
            SQL4.AppendLine("	WHERE SSCode <> 'Jampen' ");
            SQL4.AppendLine("	GROUP BY SSPCode ");
            SQL4.AppendLine(")G ON D.SSPCode = G.SSPCode ");
            SQL4.AppendLine("Inner Join TblParameter H On H.ParCode = 'IsPayrollProcessingCalculateBPJSAutomatic' and H.Parvalue is not null ");
            SQL4.AppendLine("Where A.ErAmt+A.EeAmt<>0.00 ");
            SQL4.AppendLine("   AND G.SSCode != 'Jampen'");
            SQL4.AppendLine(FilterBySiteHR);
            SQL4.AppendLine(FilterByDeptHR);
            SQL4.AppendLine(Filter4);
            SQL4.AppendLine("GROUP BY Sequence, Col1, Perc, Parent ");

            SQL4.AppendLine("Union ALL ");
            SQL4.AppendLine("SELECT 8 As Sequence, 'Pensiun' Col1, SUM(A.SSerPension+A.SSEePension) Col2, F.TotalPerc Perc, 'BPJS' As Parent ");
            SQL4.AppendLine("From TblPayrollProcess1 A  ");
            SQL4.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL4.AppendLine("   And B.CancelInd = 'N' ");
            SQL4.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL4.AppendLine("Inner Join TblPayrollProcessSSProgram D ON D.PayrunCode = A.PayrunCode And A.EmpCode = D.EmpCode ");
            SQL4.AppendLine("Inner Join TblSSProgram E On D.SSPCode = E.SSPCode ");
            SQL4.AppendLine("Inner Join TblSS F On D.SSPCode = F.SSPCode ");
            SQL4.AppendLine("Where E.SSPCode = 'BPJS-KTNK' AND F.SSCode = 'Jampen' ");
            SQL4.AppendLine(FilterBySiteHR);
            SQL4.AppendLine(FilterByDeptHR);
            SQL4.AppendLine(Filter4);
            SQL4.AppendLine("GROUP BY Sequence, Col1, Perc, Parent ");

            SQL4.AppendLine("Union All ");
            SQL4.AppendLine("SELECT 17 As Sequence, 'Potongan Pajak' Col1, Sum(A.Tax) Col2, '' Perc, Null As Parent ");
            SQL4.AppendLine("From TblPayrollProcess1 A ");
            SQL4.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL4.AppendLine("   And B.CancelInd = 'N' ");
            SQL4.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL4.AppendLine("Where A.Tax != 0.00 ");
            SQL4.AppendLine(FilterBySiteHR);
            SQL4.AppendLine(FilterByDeptHR);
            SQL4.AppendLine(Filter4);
            SQL4.AppendLine("GROUP BY Sequence, Col1, Perc, Parent ");

            SQL4.AppendLine("UNION ALL ");
            SQL4.AppendLine("SELECT 5 AS Sequence, 'BPJS' Col1, 0.00 AS Col2, NULL Perc, Null As Parent ");
            SQL4.AppendLine("FROM TblPayrollProcessSSProgram A ");
            SQL4.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
            SQL4.AppendLine("   And B.CancelInd = 'N' ");
            SQL4.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL4.AppendLine("WHERE EXISTS ( ");
            SQL4.AppendLine("   SELECT 1 ");
            SQL4.AppendLine("   FROM TblPayrollProcessSSProgram X1 ");
            SQL4.AppendLine("   Inner Join TblSSProgram X2 ON X1.SSPCode = X2.SSPCode ");
            SQL4.AppendLine("   Inner Join TblParameter X3 ON X3.ParCode='SSProgramForPayslipBPJS' AND X3.ParValue Is Not Null ");
            SQL4.AppendLine("       And FIND_IN_SET(X1.SSPCode, X3.ParValue) ");
            SQL4.AppendLine("   Inner Join TblPayrollProcess1 X4 ON X1.EmpCode = X4.EmpCode AND X4.PayrunCode = X1.PayrunCode ");
            SQL4.AppendLine("   INNER JOIN tblss X5 ON X2.SSPCode = X5.SSPCode ");
            SQL4.AppendLine("   Inner Join TblPayrun X6 On X1.PayrunCode = X6.PayrunCode And X6.CancelInd = 'N' ");
            SQL4.AppendLine("   WHERE X1.EmpCode = A.EmpCode ");
            SQL4.AppendLine("       AND (X1.ErAmt+X1.EeAmt<>0.00 OR X4.SSerPension+SSeePension <> 0.00) ");
            SQL4.AppendLine("       And (UPPER(X1.PayrunCode) Like @PayrunCode1 OR UPPER(X6.PayrunName) Like @PayrunCode1)");
            SQL4.AppendLine("   ) ");
            SQL4.AppendLine(Filter4);
            SQL4.AppendLine("GROUP BY Sequence, Col1, Perc, Parent ");

            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQL4.ToString();

                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] { "Col1", "Col2", "perc", "Sequence", "Parent" });

                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        l4.Add(new PHTPotonganNonTunai()
                        {
                            PayrunCode = TxtPayCod.Text,
                            Col1 = Sm.DrStr(dr4, c4[0]),
                            Col2 = Sm.DrDec(dr4, c4[1]),
                            perc = Sm.DrStr(dr4, c4[2]),
                            SeqNo = Sm.DrInt(dr4, c4[3]),
                            Parent = Sm.DrStr(dr4, c4[4])
                        });
                    }
                }
                dr4.Close();
            }

            #endregion

            #region Add to List

            myLists.Add(lHeader);
            myLists.Add(l);
            myLists.Add(l1);
            myLists.Add(l2);
            myLists.Add(l3);
            myLists.Add(l4);

            #endregion

            Sm.PrintReport("RptPayrollProcessSummary18", myLists, TableName, false);

            l.Clear(); l1.Clear(); l2.Clear(); l3.Clear(); l4.Clear(); l5.Clear(); l6.Clear(); 

        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPayCod_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPayCod);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

        #region Report Class

        class PayrollProcess
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string MenuDesc { get; set; }
            public string UserName { get; set; }
            public string PrintBy { get; set; }

        }

        class PayrollProcessDtl
        {
            public int nomor { get; set; }
            public string PayrunCode { get; set; }
            public string PayrunName { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public decimal SSEmployerHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal Amt { get; set; }
            public decimal TotalTHPBPJS { get; set; }
            public decimal TotBPJSKet { get; set; }
        }

        class SSProgram
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string SSPCode { get; set; }
            public decimal ErAmt { get; set; }
            public decimal EeAmt { get; set; }
            public int ColEr { get; set; }
            public int ColEe { get; set; }
        }

        class SSP
        {
            public string SSPCode { get; set; }
            public string SSPName { get; set; }
            public int ColEr { get; set; }
            public int ColEe { get; set; }
        }

        #region Printout PHT

        class Header
        {
            public string PrintDt { get; set; }
        }

        class PaySlipPHT
        {
            public string PayrunCode { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string Semester { get; set; }
            public string Triwulan { get; set; }
            public string SiteName { get; set; }
            public string DivisionName { get; set; }
        }

        class PHTTunaiGaji
        {
            public string PayrunCode { get; set; }
            public string Col1 { get; set; }
            public decimal Col2 { get; set; }
            public int SeqNo { get; set; }
        }

        class PHTTunaiTunjTidakTetap
        {
            public string PayrunCode { get; set; }
            public string Col1 { get; set; }
            public decimal Col2 { get; set; }
            public int SeqNo { get; set; }
            public string Parent { get; set; }
        }

        class PHTNonTunai
        {
            public string PayrunCode { get; set; }
            public string Perc { get; set; }
            public string Col1 { get; set; }
            public decimal Col2 { get; set; }
            public int SeqNo { get; set; }
            public string Parent { get; set; }
        }

        class PHTPotonganNonTunai
        {
            public string PayrunCode { get; set; }
            public string perc { get; set; }
            public string Col1 { get; set; }
            public decimal Col2 { get; set; }
            public int SeqNo { get; set; }
            public string Parent { get; set; }
        }

        class PHTPotonganLain
        {
            public string EmpCode { get; set; }
            public string Col1 { get; set; }
            public decimal Col2 { get; set; }
        }

        class PHTPotonganTunai
        {
            public string EmpCode { get; set; }
            public string Col1 { get; set; }
            public decimal Col2 { get; set; }
        }

        #endregion

        #endregion
    }
}
