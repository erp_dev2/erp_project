﻿#region Update
/*
    24/03/2020 [WED/YK] new apps
    01/07/2020 [WED/YK] salah join kolom
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSummaryProductRealization : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mCOAForSummaryRealization = string.Empty,
            mDescForSummaryRealization = string.Empty,
            mSiteCodeForAuctionInfo = string.Empty;

        private int[] mColDec = { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };

        #endregion

        #region Constructor

        public FrmRptSummaryProductRealization(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Project Description",
                    "Site Code",
                    "Site Name", 
                    "RKAP Amount",
                    "Estimated Value", 
                    
                    //6-10
                    "Realization"+Environment.NewLine+"January",
                    "Realization"+Environment.NewLine+"February",
                    "Realization"+Environment.NewLine+"March",
                    "Total 1st Quarterly",
                    "Realization"+Environment.NewLine+"April",

                    //11-15
                    "Realization"+Environment.NewLine+"May",
                    "Realization"+Environment.NewLine+"June",
                    "Total 2nd Quarterly",
                    "Realization"+Environment.NewLine+"July",
                    "Realization"+Environment.NewLine+"August",

                    //16-20
                    "Realization"+Environment.NewLine+"September",
                    "Total 3rd Quaterly",
                    "Realization"+Environment.NewLine+"October",
                    "Realization"+Environment.NewLine+"November",
                    "Realization"+Environment.NewLine+"December",

                    //21-24
                    "Total 4th Quarterly",
                    "Total Production",                    
                    "Percentage"+Environment.NewLine+"towards Target",
                    "Balance"
                },
                new int[] 
                {
                    //0
                    50, 

                    //1-5
                    130, 100, 150, 150, 150,  
                    
                    //6-10
                    150, 150, 150, 150, 150,

                    //11-15
                    150, 150, 150, 150, 150, 

                    //16-20
                    150, 150, 150, 150, 150, 

                    //21-25
                    150, 150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, mColDec, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;

            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ProjectSite>();

                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Process3(ref l);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }

                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mCOAForSummaryRealization = Sm.GetParameter("COAForSummaryRealization");
            mDescForSummaryRealization = Sm.GetParameter("DescForSummaryRealization");
            mSiteCodeForAuctionInfo = Sm.GetParameter("SiteCodeForAuctionInfo");

            if (mCOAForSummaryRealization.Length == 0) mCOAForSummaryRealization = "4.00,4.10";
            if (mDescForSummaryRealization.Length == 0) mDescForSummaryRealization = "PROYEK DITANGAN,PROYEK MEMBER DALAM KSO";
        }

        private void Process1(ref List<ProjectSite> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string[] mProjectDesc = mDescForSummaryRealization.Split(',');
            string[] mCOA = mCOAForSummaryRealization.Split(',');

            SQL.AppendLine("SELECT T.ProjectDesc, T.SiteCode, T.SiteName, T.RKAPAmt, T.EstValue, T.Amt1, T.Amt2, T.Amt3, T.Amt4, T.Amt5, T.Amt6, T.Amt7, T.Amt8, T.Amt9, T.Amt10, T.Amt11, T.Amt12 ");
            SQL.AppendLine("FROM ( ");

            for (int i = 0; i < mProjectDesc.Count(); ++i)
            {
                SQL.AppendLine("SELECT '" + mProjectDesc[i] + "' ProjectDesc, A.SiteCode, A.SiteName, IFNULL(C.Amt, 0.00) RKAPAmt, IfNull(B.EstValue, 0.00) EstValue, IFNULL(D.Amt1, 0.00) Amt1, ");
                SQL.AppendLine("IFNULL(D.Amt2, 0.00) Amt2, IFNULL(D.Amt3, 0.00) Amt3, IFNULL(D.Amt4, 0.00) Amt4, IFNULL(D.Amt5, 0.00) Amt5, ");
                SQL.AppendLine("IFNULL(D.Amt6, 0.00) Amt6, IFNULL(D.Amt7, 0.00) Amt7, IFNULL(D.Amt8, 0.00) Amt8, IFNULL(D.Amt9, 0.00) Amt9, ");
                SQL.AppendLine("IFNULL(D.Amt10, 0.00) Amt10, IFNULL(D.Amt11, 0.00) Amt11, IFNULL(D.Amt12, 0.00) Amt12 ");
                SQL.AppendLine("FROM (Select SiteCode, SiteName From TblSite Where FIND_IN_SET(SiteCode, @SiteCode) ) A ");
                SQL.AppendLine("Left JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select SiteCode, Sum(EstValue) EstValue ");
                SQL.AppendLine("    From TblLOPHdr ");
                SQL.AppendLine("    Where Find_In_set(SiteCode, @SiteCode) ");
                SQL.AppendLine("    And LEFT(DocDt, 4) = @Yr ");
                SQL.AppendLine("    AND CancelInd = 'N' ");
                SQL.AppendLine("    AND Status = 'A' ");
                SQL.AppendLine("    Group By SiteCode ");
                SQL.AppendLine(") B On A.SiteCode = B.SiteCode ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT T2.AcNo, SUM(T2.Amt) Amt ");
                SQL.AppendLine("    FROM TblCompanyBudgetPlanHdr T1 ");
                SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl T2 ON T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        AND T1.Yr = @Yr ");
                SQL.AppendLine("        AND T1.CancelInd = 'N' ");
                SQL.AppendLine("        And T2.Amt != 0.00 ");
                SQL.AppendLine("        AND  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("             T2.AcNo LIKE '" + mCOA[i] + "%' ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    INNER JOIN TblCOA T3 ON T2.AcNo = T3.AcNo ");
                SQL.AppendLine("        AND T3.ActInd = 'Y' ");
                SQL.AppendLine("    INNER JOIN TblParameter T4 ON T4.ParCode = 'COALevelFicoSettingJournalToCBP' ");
                SQL.AppendLine("        AND T4.ParValue IS NOT NULL ");
                SQL.AppendLine("        AND T3.Level = CONVERT(T4.ParValue, DECIMAL)+1 ");
                SQL.AppendLine("    Inner Join TblSite T5 On Right(T2.AcNo, 3) = T5.SiteCode ");
                SQL.AppendLine("        And Find_In_Set(T5.SiteCode, @SiteCode) ");
                SQL.AppendLine("    GROUP BY T2.AcNo ");
                SQL.AppendLine(") C ON A.SiteCode = RIGHT(C.AcNo, 3) ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT T.SiteCode, SUM(T.Amt1) Amt1, SUM(T.Amt2) Amt2, SUM(T.Amt3) Amt3, SUM(T.Amt4) Amt4, SUM(T.Amt5) Amt5, ");
                SQL.AppendLine("    SUM(T.Amt6) Amt6, SUM(T.Amt7) Amt7, SUM(T.Amt8) Amt8, SUM(T.Amt9) Amt9, SUM(T.Amt10) Amt10, SUM(T.Amt11) Amt11, ");
                SQL.AppendLine("    SUM(T.Amt12) Amt12 ");
                SQL.AppendLine("    FROM ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        SELECT T1.SiteCode, If(SUBSTR(T7.DocDt, 5, 2) = '01', T7.Amt, 0.00) Amt1, If(SUBSTR(T7.DocDt, 5, 2) = '02', T7.Amt, 0.00) Amt2, ");
                SQL.AppendLine("        If(SUBSTR(T7.DocDt, 5, 2) = '03', T7.Amt, 0.00) Amt3, If(SUBSTR(T7.DocDt, 5, 2) = '04', T7.Amt, 0.00) Amt4, ");
                SQL.AppendLine("        If(SUBSTR(T7.DocDt, 5, 2) = '05', T7.Amt, 0.00) Amt5, If(SUBSTR(T7.DocDt, 5, 2) = '06', T7.Amt, 0.00) Amt6, ");
                SQL.AppendLine("        If(SUBSTR(T7.DocDt, 5, 2) = '07', T7.Amt, 0.00) Amt7, If(SUBSTR(T7.DocDt, 5, 2) = '08', T7.Amt, 0.00) Amt8, ");
                SQL.AppendLine("        If(SUBSTR(T7.DocDt, 5, 2) = '09', T7.Amt, 0.00) Amt9, If(SUBSTR(T7.DocDt, 5, 2) = '10', T7.Amt, 0.00) Amt10, ");
                SQL.AppendLine("        If(SUBSTR(T7.DocDt, 5, 2) = '11', T7.Amt, 0.00) Amt11, If(SUBSTR(T7.DocDt, 5, 2) = '12', T7.Amt, 0.00) Amt12 ");
                SQL.AppendLine("        FROM TblSite T1 ");
                SQL.AppendLine("        INNER JOIN TblLOPHdr T2 ON T1.SiteCode = T2.SiteCode ");
                SQL.AppendLine("            AND FIND_IN_SET(T1.SiteCode, @SiteCode) ");
                SQL.AppendLine("            AND T2.CancelInd = 'N' ");
                SQL.AppendLine("            AND T2.Status = 'A' ");
                SQL.AppendLine("        INNER JOIN TblBOQHdr T3 ON T2.DocNo = T3.LOPDocNo ");
                SQL.AppendLine("        INNER JOIN TblSOContractHdr T4 ON T3.DocNo = T4.BOQDocNo ");
                SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr T5 ON T4.DocNo = T5.SOCDocNo ");
                SQL.AppendLine("        INNER JOIN TblProjectImplementationHdr T6 ON T5.DocNo = T6.SOContractDocNo ");
                SQL.AppendLine("        INNER JOIN TblProjectDeliveryHdr T7 ON T6.DocNo = T7.PRJIDocNo ");
                SQL.AppendLine("            AND T7.CancelInd = 'N' ");
                SQL.AppendLine("            AND T7.Status = 'A' ");
                SQL.AppendLine("            AND T7.DocType = '1' ");
                SQL.AppendLine("            AND LEFT(T7.DocDt, 4) = @Yr ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine("    GROUP BY T.SiteCode ");
                SQL.AppendLine(") D ON A.SiteCode = D.SiteCode ");

                if (i != (mProjectDesc.Count() - 1))
                    SQL.AppendLine("    UNION ALL ");
            }

            SQL.AppendLine(") T ");
            SQL.AppendLine("ORDER BY T.ProjectDesc, T.SiteCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCodeForAuctionInfo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "ProjectDesc",
                    //1-5
                    "SiteCode", "SiteName", "RKAPAmt", "EstValue", "Amt1", 
                    //6-10
                    "Amt2", "Amt3", "Amt4", "Amt5", "Amt6", 
                    //11-15
                    "Amt7", "Amt8", "Amt9", "Amt10", "Amt11", 
                    //16
                    "Amt12"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProjectSite()
                        {
                            ProjectDesc = Sm.DrStr(dr, c[0]),
                            SiteCode = Sm.DrStr(dr, c[1]),
                            SiteName = Sm.DrStr(dr, c[2]),
                            RKAPAmt = Sm.DrDec(dr, c[3]),
                            EstValue = Sm.DrDec(dr, c[4]),
                            Amt1 = Sm.DrDec(dr, c[5]),
                            Amt2 = Sm.DrDec(dr, c[6]),
                            Amt3 = Sm.DrDec(dr, c[7]),
                            Total1 = Sm.DrDec(dr, c[5]) + Sm.DrDec(dr, c[6]) + Sm.DrDec(dr, c[7]),
                            Amt4 = Sm.DrDec(dr, c[8]),
                            Amt5 = Sm.DrDec(dr, c[9]),
                            Amt6 = Sm.DrDec(dr, c[10]),
                            Total2 = Sm.DrDec(dr, c[8]) + Sm.DrDec(dr, c[9]) + Sm.DrDec(dr, c[10]),
                            Amt7 = Sm.DrDec(dr, c[11]),
                            Amt8 = Sm.DrDec(dr, c[12]),
                            Amt9 = Sm.DrDec(dr, c[13]),
                            Total3 = Sm.DrDec(dr, c[11]) + Sm.DrDec(dr, c[12]) + Sm.DrDec(dr, c[13]),
                            Amt10 = Sm.DrDec(dr, c[14]),
                            Amt11 = Sm.DrDec(dr, c[15]),
                            Amt12 = Sm.DrDec(dr, c[16]),
                            Total4 = Sm.DrDec(dr, c[14]) + Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16]),
                            TotalNewContract = 0m,
                            PercentageTowardsTarget = 0m,
                            Balance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<ProjectSite> l)
        {
            foreach (var x in l)
            {
                x.TotalNewContract = x.Total1 + x.Total2 + x.Total3 + x.Total4;
                if (x.RKAPAmt != 0m) x.PercentageTowardsTarget = (x.TotalNewContract / x.RKAPAmt) * 100m;
                x.Balance = x.TotalNewContract - x.RKAPAmt;
            }
        }

        private void Process3(ref List<ProjectSite> l)
        {
            int Row = 0;
            foreach (var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 1].Value = x.ProjectDesc;
                Grd1.Cells[Row, 2].Value = x.SiteCode;
                Grd1.Cells[Row, 3].Value = x.SiteName;
                Grd1.Cells[Row, 4].Value = x.RKAPAmt;
                Grd1.Cells[Row, 5].Value = x.EstValue;
                Grd1.Cells[Row, 6].Value = x.Amt1;
                Grd1.Cells[Row, 7].Value = x.Amt2;
                Grd1.Cells[Row, 8].Value = x.Amt3;
                Grd1.Cells[Row, 9].Value = x.Total1;
                Grd1.Cells[Row, 10].Value = x.Amt4;
                Grd1.Cells[Row, 11].Value = x.Amt5;
                Grd1.Cells[Row, 12].Value = x.Amt6;
                Grd1.Cells[Row, 13].Value = x.Total2;
                Grd1.Cells[Row, 14].Value = x.Amt7;
                Grd1.Cells[Row, 15].Value = x.Amt8;
                Grd1.Cells[Row, 16].Value = x.Amt9;
                Grd1.Cells[Row, 17].Value = x.Total3;
                Grd1.Cells[Row, 18].Value = x.Amt10;
                Grd1.Cells[Row, 19].Value = x.Amt11;
                Grd1.Cells[Row, 20].Value = x.Amt12;
                Grd1.Cells[Row, 21].Value = x.Total4;
                Grd1.Cells[Row, 22].Value = x.TotalNewContract;
                Grd1.Cells[Row, 23].Value = x.PercentageTowardsTarget;
                Grd1.Cells[Row, 24].Value = x.Balance;

                Row += 1;
            }

            Grd1.GroupObject.Add(1);
            Grd1.Group();
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, mColDec);
        }

        #endregion

        #endregion

        #region Class

        private class ProjectSite
        {
            public string ProjectDesc { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public decimal RKAPAmt { get; set; }
            public decimal EstValue { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal Total1 { get; set; }
            public decimal Amt4 { get; set; }
            public decimal Amt5 { get; set; }
            public decimal Amt6 { get; set; }
            public decimal Total2 { get; set; }
            public decimal Amt7 { get; set; }
            public decimal Amt8 { get; set; }
            public decimal Amt9 { get; set; }
            public decimal Total3 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }
            public decimal Total4 { get; set; }
            public decimal TotalNewContract { get; set; }
            public decimal PercentageTowardsTarget { get; set; }
            public decimal Balance { get; set; }
        }

        #endregion
    }
}
