﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSalesQtFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmSalesQt mFrmParent;
        private string mSQL;

        #endregion

        #region Constructor

        public FrmSalesQtFind(FrmSalesQt FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ActInd, E.CtName, G.ItCode, H.ItName, H.ItCodeInternal, H.Specification, I.CtItCode, I.CtItName, C.UPrice,  ");
            //SQL.AppendLine("Case When A.Status='O' Then 'Outstanding' ");
            //SQL.AppendLine("When A.Status='A' Then 'Approved' ");
            //SQL.AppendLine("When A.Status='C' Then 'Cancelled' ");
            //SQL.AppendLine("End As Status, ");
            SQL.AppendLine("Null As Status, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblSalesQtHdr A ");
            SQL.AppendLine("Inner Join TblSalesQtDtl C On A.DocNo = C.DocNo   ");
            SQL.AppendLine("Inner Join TblCustomer E On A.CtCode=E.CtCode ");
            SQL.AppendLine("Inner Join TblItemPricehdr F On C.ItemPriceDocNo = F.DocNo ");
            SQL.AppendLine("Inner Join TblItemPriceDtl G On C.ItemPriceDocNo = G.Docno And C.ItemPriceDNo = G.Dno ");
            SQL.AppendLine("Inner Join TblItem H On G.ItCode=H.ItCode ");
            SQL.AppendLine("Left Join TblCustomerItem I On G.ItCode = I.ItCode And A.CtCode=I.CtCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Active",
                    "Status",
                    "Customer",
                    
                    //6-10
                    "Item's Code",
                    "",
                    "Local Code",//18
                    "Item's Name",//8
                    "Customer's"+Environment.NewLine+"Item Code",//17
                    
                    //11-15
                    "Customer's"+Environment.NewLine+"Item Name",//19
                    "Specification",//16
                    "Unit Price", //9
                    "Created"+Environment.NewLine+"By", //10
                    "Created"+Environment.NewLine+"Date", //11

                    //16-19
                    "Created"+Environment.NewLine+"Time",  //12
                    "Last"+Environment.NewLine+"Updated By", //13
                    "Last"+Environment.NewLine+"Updated Date", //14
                    "Last"+Environment.NewLine+"Updated Time",//15
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    120, 80, 60, 80, 180,

                    //6-10
                    100, 20, 100, 250, 100, 

                    //11-15
                    150, 150, 120, 100, 100, 

                    //16-19
                    100, 100, 100, 100,
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3});
            Sm.GrdColButton(Grd1, new int[] { 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 7, 14, 15, 16, 17, 18, 19, 12, 10 }, false);
            if (!mFrmParent.mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 11 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 14, 15, 16, 17, 18, 19, 12, 10 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "G.ItCode", "H.ItName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocNo, C.DNo;",
                    new string[]
                    {
                        //0
                       "DocNo",

                       //1-5
                       "DocDt", "ActInd", "Status", "CtName", "ItCode",  

                       //6-10
                       "ItName", "UPrice", "CreateBy", "CreateDt", "LastUpBy", 
                       
                       //11-15
                       "LastUpDt", "Specification", "CtItCode", "ItCodeInternal", "CtItName"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 14);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 13);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 15);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 12);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 8);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 9);
                        Sm.SetGrdValue("T", Grd1, dr, c, Row, 16, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 10);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 11);
                        Sm.SetGrdValue("T", Grd1, dr, c, Row, 19, 11);                        
                    }, true, true, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion
        
        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
