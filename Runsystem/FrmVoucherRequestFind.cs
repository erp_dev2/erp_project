﻿#region Update
/*
    15/08/2017 [TKG] menambah informasi paid to
    07/03/2018 [ARI] tambah kolom status approve
    19/03/2018 [TKG] tambah informasi local document#
    04/04/2018 [ARI] TAMBAH FILTER DEPARTMEN
    25/09/2018 [DITA] tambah kolom entity
    06/09/2019 [DITA] Bug saat refresh
    17/03/2020 [VIN/KBN] fambah filter bank account type
    17/03/2022 [VIN/SIER] Filter departemen dibatasi group
    27/07/2022 [TYO/SIER] Group filter doc Type berdasarkan parameter IsGroupFilterByType
    05/08/2022 [TYO/SIER] Penyesuaian filter by group
    09/08/2022 [BRI/PHT] Penyesuaian filter by group bank acc
    15/09/2022 [VIN/ALL] BUG VR Budget menampilkan VR biasa
    09/03/2022 [VIN/ALL] MenuCodeForVRBudget.Length < 0, maka tidak liat punya bccode atau tidak
    23/03/2023 [MAU/PHT] penyesuaian filter Type berdasarkan group user
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmVoucherRequest mFrmParent;
        private string mSQL = string.Empty, mMInd = "N", 
            mMenuCode =string.Empty,
            mMenuCodeForVRBudget = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestFind(FrmVoucherRequest FrmParent, string MInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMInd = MInd;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                mMenuCode = mFrmParent.mMenuCode;
                mMenuCodeForVRBudget = mFrmParent.mMenuCodeForVRBudget;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueVoucherDocType(ref LueDocType, mFrmParent.mIsGroupFilterByType ? "Y" : "N");
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                if (mFrmParent.mIsFilterByDept || mFrmParent.mIsVoucherRequestFilteredByDept)
                    Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                else
                    Sl.SetLueDeptCode(ref LueDeptCode);

                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd,I.EntName, D.BankAcTp, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.LocalDocNo, B.OptDesc As DocType, ");
            SQL.AppendLine("A.VoucherDocNo, D.BankAcNm, E.OptDesc As PaymentType, A.GiroNo, F.BankName, ");
            SQL.AppendLine("Case A.AcType ");
            SQL.AppendLine("  When 'C' Then 'CREDIT' ");
            SQL.AppendLine("  When 'D' Then 'DEBIT' ");
            SQL.AppendLine("End As AcType, ");
            SQL.AppendLine("A.DueDt, G.UserName As PIC, H.DeptName, A.Amt, A.Remark, ");
            SQL.AppendLine("PaidToBankCode, A.PaidToBankBranch, A.PaidToBankAcNo, A.PaidToBankAcName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Left Join TblOption B On A.DocType=B.OptCode and B.OptCat='VoucherDocType' ");
            SQL.AppendLine("Left Join TblOption C On A.AcType=C.OptCode and C.OptCat='AccountType' ");
            SQL.AppendLine("Left Join TblBankAccount D On A.BankAcCode=D.BankAcCode ");
            SQL.AppendLine("Left Join TblOption E On A.PaymentType=E.OptCode and E.OptCat='VoucherPaymentType' ");
            SQL.AppendLine("Left Join TblBank F On A.BankCode=F.BankCode ");
            SQL.AppendLine("Left Join TblUser G On A.PIC=G.UserCode ");
            SQL.AppendLine("Left Join TblEntity I On A.EntCode=I.EntCode ");
            SQL.AppendLine("Left Join TblDepartment H On A.DeptCode=H.DeptCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mMenuCodeForVRBudget.Length > 0)
            {
                if (mMenuCode != mMenuCodeForVRBudget)
                    SQL.AppendLine("And A.BCCode Is Null ");
                else
                    SQL.AppendLine("And A.BCCode Is Not Null ");
            }

            if (mFrmParent.mIsFilterByDept || mFrmParent.mIsVoucherRequestFilteredByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null ");
                SQL.AppendLine("    Or (A.DeptCode Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        )))) ");
            }
            if (mFrmParent.mIsVoucherRequestBankAccountFilteredByGrp)
            {
                SQL.AppendLine("And (A.BankAcCode Is Null ");
                SQL.AppendLine("    Or (A.BankAcCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");

                SQL.AppendLine("And (A.BankAcCode2 Is Null ");
                SQL.AppendLine("    Or (A.BankAcCode2 Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode2 ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ))) Or Find_In_Set(A.DocType,'16,73') ");
                SQL.AppendLine("    ) ");
            }

            if (mFrmParent.mIsGroupFilterByType)
            {
                SQL.AppendLine("And EXISTS");
                SQL.AppendLine("(  ");
                SQL.AppendLine("	SELECT 1   ");
                SQL.AppendLine("	FROM TblGroupType  ");
                SQL.AppendLine("	WHERE TypeCode =A.DocType  ");
                SQL.AppendLine("	AND GrpCode IN   ");
                SQL.AppendLine("	(  ");
                SQL.AppendLine("		SELECT GrpCode FROM tbluser  ");
                SQL.AppendLine("		WHERE UserCode = @UserCode  ");
                SQL.AppendLine("	)  ");
                SQL.AppendLine(")  ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel", 
                        "Status",
                        "Local#",
                        
                        //6-10
                        "Type",
                        "Voucher#",
                        "Bank Account",
                        "Payment"+Environment.NewLine+"Type",
                        "Giro#",
                        
                        //11-15
                        "Bank",
                        "Account"+Environment.NewLine+"Type",
                        "Due"+Environment.NewLine+"Date",
                        "PIC",
                        "Department",
                        
                        //16-20
                        "Amount",
                        "Paid To" + Environment.NewLine + "(Bank)",
                        "Paid To" + Environment.NewLine + "(Branch)",
                        "Paid To" + Environment.NewLine + "(Account#)",
                        "Paid To" + Environment.NewLine + "(Account Name)",
                        
                        //21-25
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 

                        //26-29
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",
                        "Entity",
                        "Bank"+Environment.NewLine+"Account Type"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 50, 100, 120, 
                        
                        //6-10
                        120, 130, 200, 150, 150, 

                        //11-15
                        350, 80, 80, 150, 180, 

                        //16-20
                        100, 200, 200, 200, 200, 
                        
                        //21-25
                        300, 100, 100, 100, 100, 
                         
                        //26-29
                        100, 100,200, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 16 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 13, 23, 26 });
            Sm.GrdFormatTime(Grd1, new int[] { 24, 27 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 }, false);
            Grd1.Cols[28].Move(21);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[29].Move(9);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = mFrmParent.mIsUseMInd ? " And A.MInd=@MInd " : " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@MInd", mMInd);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtAcTp.Text, "D.BankAcTp", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "DocType", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter +" Order By A.CreateDt Desc; " ,
                        new string[]
                        {
                            //0
                            "DocNo",
                            
                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "LocalDocNo", "DocType", 
                            
                            //6-10
                            "VoucherDocNo", "BankAcNm", "PaymentType", "GiroNo", "BankName", 
                            
                            //11-15
                            "AcType", "DueDt", "PIC", "DeptName", "Amt", 
                            
                            //16-20
                            "PaidToBankCode", "PaidToBankBranch", "PaidToBankAcNo", "PaidToBankAcName", "Remark", 
                            
                            //21-26
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "EntName",

                            //26
                            "BankAcTp"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            if (Sm.DrStr(dr, 2) == "Y")
                            {
                                Grd.Rows[Row].BackColor = Color.Red;
                                Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            }
                            else
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                        }, true, false,false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
          
          private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }
          private void TxtAcTp_Validated(object sender, EventArgs e)
          {
              Sm.FilterTxtSetCheckEdit(this, sender);
          }
          private void ChkAcTp_CheckedChanged(object sender, EventArgs e)
          {
              Sm.FilterSetTextEdit(this, sender, "Account Type#");
          }
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue2(Sl.SetLueVoucherDocType), mFrmParent.mIsGroupFilterByType ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Document type");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mFrmParent.mIsFilterByDept || mFrmParent.mIsVoucherRequestFilteredByDept)
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode), string.Empty);
            else
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));

            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

       
        #endregion

        
    }
}
