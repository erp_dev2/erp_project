﻿#region Update
/*
    25/02/2019 [TKG] New reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptKB_BC40RecvVd : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mStartDt = string.Empty, mEndDt = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRptKB_BC40RecvVd(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.KBRegistrationNo, A.KBRegistrationDt, A.KBContractNo, A.DocDt, C.VdName, D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status In ('O', 'A') And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.KBContractNo Is Not Null ");
           
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 1;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;
            Grd1.Header.Cells[1, 1].Value = "Dokumen Pabean";
            Grd1.Header.Cells[1, 1].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 1].SpanCols = 3;
            Grd1.Header.Cells[0, 1].Value = "Jenis";
            Grd1.Header.Cells[0, 1].SpanRows = 1;
            Grd1.Header.Cells[0, 2].Value = "Nomor";
            Grd1.Header.Cells[0, 2].SpanRows = 1;
            Grd1.Header.Cells[0, 3].Value = "Tanggal";
            Grd1.Header.Cells[0, 3].SpanRows = 1;
            Grd1.Header.Cells[1, 4].Value = "Bukti Penerimaan Barang";
            Grd1.Header.Cells[1, 4].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 4].SpanCols = 2;
            Grd1.Header.Cells[0, 4].Value = "Nomor";
            Grd1.Header.Cells[0, 4].SpanRows = 1;
            Grd1.Header.Cells[0, 5].Value = "Tanggal";
            Grd1.Header.Cells[0, 5].SpanRows = 1;
            Grd1.Header.Cells[0, 6].Value = "Supplier";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;
            Grd1.Header.Cells[0, 7].Value = "Kode Barang";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;
            Grd1.Header.Cells[0, 8].Value = "Uraian Barang";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 2;
            Grd1.Header.Cells[0, 9].Value = "Satuan";
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 9].SpanRows = 2;
            Grd1.Header.Cells[0, 10].Value = "Jumlah";
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 10].SpanRows = 2;
            Grd1.Header.Cells[0, 11].Value = "Valas";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 2;
            Grd1.Header.Cells[0, 12].Value = "Nilai Barang";
            Grd1.Header.Cells[0, 12].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 12].SpanRows = 2;
            
            Sm.GrdFormatDate(Grd1, new int[] { 3, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12 }, 0);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void ShowData()
        {
            mStartDt = string.Empty; mEndDt = string.Empty;
            Sm.ClearGrd(Grd1, true);
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                mStartDt = Sm.GetDte(DteDocDt1).Substring(0, 8); 
                mEndDt = Sm.GetDte(DteDocDt2).Substring(0, 8);

                Sm.CmParamDt(ref cm, "@DocDt1", mStartDt);
                Sm.CmParamDt(ref cm, "@DocDt2", mEndDt);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.KBContractNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.KBContractNo, D.ItGrpCode, D.ItName;",
                        new string[]
                        {
                            //0
                            "KBRegistrationNo", 

                            //1-5
                            "KBRegistrationDt", "KBContractNo", "DocDt", "VdName", "ItGrpCode", 

                            //6-10
                            "ItName", "InventoryUomCode", "Qty", "CurCode", "Amt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = "BC.40";
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            try
            {
                if (Grd1.Rows.Count == 0 || mStartDt.Length==0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }

                iGSubtotalManager.ForeColor = Color.Black;

                string StartDt = string.Concat(Sm.Right(mStartDt, 2), "/", mStartDt.Substring(4, 2), "/", Sm.Left(mStartDt, 4));
                string EndDt = string.Concat(Sm.Right(mEndDt, 2), "/", mEndDt.Substring(4, 2), "/", Sm.Left(mEndDt, 4));

                PM1.PageHeader.MiddleSection.Text =
                    "LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine +
                    Gv.CompanyName;
                PM1.PageHeader.LeftSection.Text = Environment.NewLine + string.Concat("Periode : ", StartDt, " s/d ", EndDt);
                Sm.SetPrintManagerProperty(PM1, Grd1, ChkAutoWidth.Checked);
                iGSubtotalManager.ForeColor = Color.White;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
