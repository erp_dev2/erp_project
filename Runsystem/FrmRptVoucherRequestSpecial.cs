﻿#region Update
/*
    03/12/2020 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptVoucherRequestSpecial : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptVoucherRequestSpecial(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueOption(ref LueDocType, "VRSpecialDocType");
                SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueOption(ref LueCostOfGroup, "EmpCostGroup");
                Sl.SetLueGrdLvlCode(ref LueGrdLvlCode);
                Sl.SetLuePayrollGrpCode(ref LuePayrollGrpCode);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.OptDesc As DocType, D.EmpCode, E.EmpName, F.DeptName, ");
            SQL.AppendLine("G.OptDesc As CostGroup, H.GrdLvlName, I.PGName, D.Amt, J.VoucherDocNo ");
            SQL.AppendLine("From TblVoucherRequestSpecialHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestSpecialDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.Status In ('A', 'O') ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And A.DocType = @DocType ");
            SQL.AppendLine("Inner Join TblOption C On A.DocType = C.OptCode And C.OptCat = 'VRSpecialDocType' ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T2.DNo, T3.PGCode, T4.EmpCode, T4.Amt ");
            SQL.AppendLine("    From TblVoucherRequestSpecialHdr T1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.Status In ('A', 'O') ");
            SQL.AppendLine("        And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And T1.DocType = @DocType ");
            SQL.AppendLine("    Inner Join TblRHAHdr T3 On T2.DocNo2 = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblRHADtl T4 On T3.DocNo = T4.DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocNo, T2.DNo, T3.PGCode, T4.EmpCode, T4.Amount Amt ");
            SQL.AppendLine("    From TblVoucherRequestSpecialHdr T1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.Status In ('A', 'O') ");
            SQL.AppendLine("        And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And T1.DocType = @DocType ");
            SQL.AppendLine("    Inner Join TblEmpIncentiveHdr T3 On T2.DocNo2 = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblEmpIncentiveDtl T4 On T3.DocNo = T4.DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocNo, T2.DNo, T3.PGCode, T4.EmpCode, T4.Amt ");
            SQL.AppendLine("    From TblVoucherRequestSpecialHdr T1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.Status In ('A', 'O') ");
            SQL.AppendLine("        And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And T1.DocType = @DocType ");
            SQL.AppendLine("    Inner Join TblOTVerificationHdr T3 On T2.DocNo2 = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblOTVerificationDtl T4 On T3.DocNo = T4.DocNo ");
            SQL.AppendLine(") D On A.DocNo = D.DocNo And B.DNo = D.DNo ");
            SQL.AppendLine("Inner Join TblEmployee E On D.EmpCode = E.EmpCode ");
            SQL.AppendLine("Inner Join TblDepartment F On E.DeptCode = F.DeptCode ");
            SQL.AppendLine("Left Join TblOption G On E.CostGroup = G.OptCode And G.OptCat = 'EmpCostGroup' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr H On E.GrdLvlCode = H.GrdLvlCode ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On D.PGCode = I.PGCode ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr J On A.VoucherRequestDocNo = J.DocNo ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Date",
                    "Type",
                    "Employee's Code",
                    "Employee",
                    
                    //6-10
                    "Department",
                    "Cost of Group",
                    "Grade",
                    "Payroll's Group",
                    "Amount",
                    
                    //11
                    "Voucher#"
                   
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 130, 120, 220, 
                    
                    //6-10
                    200, 180, 180, 180, 150, 

                    //11
                    150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                Sm.IsLueEmpty(LueDocType, "Type VR Special")
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0=0 ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "D.EmpCode", "E.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "E.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCostOfGroup), "E.CostGroup", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueGrdLvlCode), "E.GrdLvlCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrollGrpCode), "D.PGCode", true);


                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SetSQL() + Filter + " Order By A.DocDt, A.DocNo, E.EmpName;",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "DocType", "EmpCode", "EmpName", "DeptName", 
                        
                        //6-10
                        "CostGroup", "GrdLvlName", "PGName", "Amt", "VoucherDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    }, true, false, false, false
                );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueDeptCode(ref DXE.LookUpEdit Lue, string Code, string IsFilterByDept)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T Where 0 = 0 ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("And DeptCode=@Code  ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("Or Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            else
            {
                SQL.AppendLine("And ActInd = 'Y' ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By T.DeptName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue2(Sl.SetLueOption), "VRSpecialDocType");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueCostOfGroup_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCostOfGroup, new Sm.RefreshLue2(Sl.SetLueOption), "EmpCostGroup");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCostOfGroup_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost of Group");
        }

        private void LueGrdLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGrdLvlCode, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkGrdLvlCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Grade");
        }

        private void LuePayrollGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrollGrpCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrollGrpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payroll Group");
        }

        #endregion

        #endregion
    }
}
