﻿#region Update
/*
    01/11/2019 [DITA/IMS] tambah informasi Specifications
    28/11/2019 [WED/IMS] tambah kolom Project code, project name, customer PO#, spmk#
    13/05/2020 [DITA+/IMS] ubah query set sql (karna ganti alur)
    18/05/2020 [IBL/IMS] Tambah kolom local document
    27/05/2020 [TKG/IMS] Tambah finished good dan component
    21/07/2020 [TKG/IMS] Tambah filter item's local code
 *  13/10/2020 [ICA/IMS] Menambah kolom cancellation MR based on parameter ISBOMShowSpecifications
    05/11/2020 [WED/IMS] query from dibedakan berdasarkan parameter MenuCodeForPRService
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest3Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmMaterialRequest3 mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmMaterialRequest3Find(FrmMaterialRequest3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDept?"Y":"N");
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mFrmParent.mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, C.DeptName, B.CancelInd, ");
            SQL.AppendLine("Case B.Status When 'A' Then 'Approved' When 'C' Then 'Cancel' When 'O' Then 'Outstanding' Else 'Unknown' End As StatusDesc, ");
            SQL.AppendLine("B.ItCode, B.Qty, D.PurchaseUomCode, ");
            SQL.AppendLine("D.ItName, E.SiteName, B.UsageDt, B.Remark As DRemark, A.Remark As HRemark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt, D.ForeignName, D.ItCodeInternal, F.UserName, D.Specification, ");
            SQL.AppendLine("H.ProjectCode, H.ProjectName, G.PONo, A.NTPDocNo, B.FinishedGood, B.Component, I.Qty as CancelledQty ");
            if (mFrmParent.mMenuCodeForPRService)
            {
                SQL.AppendLine("From TblMaterialRequestServiceHdr A ");
                SQL.AppendLine("Inner Join TblMaterialRequestServiceDtl B On A.DocNo=B.DocNo And A.DocType = '2' ");
            }
            else
            {
                SQL.AppendLine("From TblMaterialRequestHdr A ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And A.DocType = '2' ");
            }
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblItem D ");
            SQL.AppendLine("    On B.ItCode=D.ItCode ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblUser F On A.PICCode=F.UserCode ");
            SQL.AppendLine("Left Join TblSOContractHdr G On A.SOCDocNo = G.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup H On A.PGCode = H.PGCode ");
            SQL.AppendLine("Left Join TblMRQtyCancel I ON A.DocNo=I.MRDocNo");
            SQL.AppendLine("    AND B.DNo=I.MRDNo ");
            SQL.AppendLine("    AND I.CancelInd='N' ");
            SQL.AppendLine("Where A.WODocNo Is Null ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or ( ");
                SQL.AppendLine("    A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Local Document",
                    "Cancel",
                    "Status",

                    //6-10
                    "Department",
                    "Site",
                    "Item's"+Environment.NewLine+"Code",
                    "",
                    "Item's Name",

                    //11-15
                    "Local Code",
                    "Foreign Name",
                    "Quantity",
                    "UoM",
                    "Usage"+Environment.NewLine+"Date",
                    
                    
                    //16-20
                    "Item's Remark",
                    "Document's Remark",
                    "Created By",
                    "Created Date", 
                    "Created Time", 

                    //21-25
                    "Last Updated By",
                    "Last Updated Date",
                    "Last Updated Time",
                    "PIC",
                    "Specification",

                    //26-30
                    "Project's Code",
                    "Project's Name",
                    "Customer PO#",
                    "Notice To Proceed#",
                    "Finished Good",

                    //31
                    "Component", 
                    "Cancelled"+Environment.NewLine+"Quantity"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 150, 60, 100, 
                    
                    //6-10
                    200, 150, 80, 20, 250,
                    
                    //11-15
                    100, 200, 100, 80, 100, 

                    //16-20
                    300, 300, 130, 130, 130, 
                    
                    //21-25
                    130, 130, 130, 150, 300, 

                    //26-30
                    100, 200, 120, 120, 200,

                    //31-32
                    200, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdColButton(Grd1, new int[] { 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 32 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 15, 19, 22 });
            Sm.GrdFormatTime(Grd1, new int[] { 20, 23 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 18, 19, 20, 21, 22, 23 }, false);
            if (!mFrmParent.mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 12 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 25, 26, 27, 28, 29,32 });
            if (!mIsFilterBySite) Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
            Grd1.Cols[24].Move(7);
            Grd1.Cols[25].Move(13);
            Grd1.Cols[30].Move(14);
            Grd1.Cols[31].Move(15);
            Grd1.Cols[32].Move(18);
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 17, 18, 19, 20, 21, 22, 23 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                if (ChkExcludedCancelledItem.Checked)
                    Filter = " And A.Status In ('O', 'A') And A.CancelInd='N' And B.Status In ('O', 'A') And B.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItName", "D.ForeignName", "D.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "LocalDocNo", "CancelInd", "StatusDesc", "DeptName", 

                        //6-10
                        "SiteName", "ItCode", "ItName", "ItCodeInternal", "ForeignName", 
                        
                        //11-15
                        "Qty", "PurchaseUomCode", "UsageDt", "DRemark", "HRemark", 
                        
                        //16-20
                        "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "UserName", 

                        //21-25
                        "Specification", "ProjectCode", "ProjectName", "PONo", "NTPDocNo",

                        //26-28
                        "FinishedGood", "Component", "CancelledQty"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 28);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(
                Sl.SetLueDeptCode), 
                string.Empty, 
                mFrmParent.mIsFilterByDept ? "Y" : "N"
                );
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local document#");
        }

        #endregion

        #endregion
    }
}
