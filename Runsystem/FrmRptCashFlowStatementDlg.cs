﻿#region update
/*
    27/11/2020 [IBL/IMS] New App
    04/05/2021 [HAR/PHT] Cashflow statement  nilai loop detail tidak sama
    14/10/2021 [ICA/AMKA] menambahkan cashtype2 berdasarkan parameter IsVoucherUseCashType2
    25/10/2021 [VIN/IMS] voucher non IDR * Rate 
    23/11/2021 [ICA/AMKA] bug detail cashtype2 belum muncul
    13/01/2022 [DITA/PHT] bug belum adanya inisialisasi isi data untuk param @UserCode yg menyebabkan waktu lup di klik muncul 'No Data'
    19/01/2022 [SET/PHT] menambahkan kolom type source beneficiary di list of voucher di cashflow statement
    21/01/2022 [SET/PHT] Nilai voucher dari transaksi Multi VR type "CASBA" tercatat "0" di cashflow statement
    23/01/2022 [SET/PHT] Menampilkan nilai VC dari transaksi Manual VR type Switching Bank Account
    25/01/2022 [SET/PHT] Menampilkan nilai VC dari transaksi Multi VR type Switching Bank Account
    02/02/2022 [VIN/AMKA] BUG Menampilkan cash type 2 voucher
    09/02/2022 [SET/PHT] Feedback : Nilai voucher dari transaksi Multi VR type "CASBA" tercatat "0" di cashflow statement
    17/02/2022 [SET/PHT] feedback perhitungan cashflow statement belum sesuai
    02/03/2022 [VIN/AMKA] BUG Group blm sesuai 
    14/03/2022 [IBL/PHT] Nilai loop detail tidak sesuai dengan voucher
    15/12/2022 [IBL/BBT] Untuk VC type Cash Advance dan Cash Advance Settlement yg ditarik hanya yg BankAcTp nya tidak terdaftar di param BankAccountTypeForVRCA1
                         Berdasarkan parameter IsVoucherCASAllowMultipleBankAccount
    30/03/2023 [RDA/BBT] tambah informasi additional amount berdasarkan parameter mIsVoucherUseAdditionalCost
    06/04/2023 [MYA/PHT] Amount di cashflow statement ketika transaksi non IDR otomatis mengalikan dari currency rate terdekat sampai dengan tanggal transaksi 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptCashFlowStatementDlg : RunSystem.FrmBase4
    {
        #region Field
        private FrmRptCashFlowStatement mFrmParent;
        internal string
            mCashTypeCode = string.Empty,
            mQueryProfitCenter = string.Empty,
            mQueryProfitCenter2 = string.Empty;
        internal bool
            mVoucherAccessInd = false;
        private List<String> mlProfitCenter = null;

        #endregion

        #region Constructor

        public FrmRptCashFlowStatementDlg(FrmRptCashFlowStatement FrmParent, List<string> lProfitCenter)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mlProfitCenter = lProfitCenter;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                  Grd1,
                  new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "Voucher#",
                        "",
                        "Type",
                        "Source",
                        "Beneficiary",

                        //6-7
                        mFrmParent.mThisYear,
                        mFrmParent.mThisYear,
                    },
                   new int[] 
                    {
                        //0
                        50,
 
                        //1-4
                        120, 20, 160, 350, 350,
                        
                        //6-7
                        0, 200,
                    }
              );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7 });
            Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 2);

            mVoucherAccessInd = GetAccessInd("FrmVoucher");
            if (mVoucherAccessInd)
                Sm.GrdColInvisible(Grd1, new int[] { 6 });
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 6 });

            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {


            return string.Empty; //SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {

        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;


                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                if (mFrmParent.mIsRptCashFlowStatementUseProfitCenter)
                {
                    mQueryProfitCenter = "    And C.SiteCode Is Not Null ";
                    mQueryProfitCenter += "    And C.SiteCode In ( ";
                    mQueryProfitCenter += "        Select Distinct SiteCode ";
                    mQueryProfitCenter += "        From TblSite ";
                    mQueryProfitCenter += "        Where ProfitCenterCode Is Not Null ";

                    mQueryProfitCenter2 = "    And D.SiteCode Is Not Null ";
                    mQueryProfitCenter2 += "    And D.SiteCode In ( ";
                    mQueryProfitCenter2 += "        Select Distinct SiteCode ";
                    mQueryProfitCenter2 += "        From TblSite ";
                    mQueryProfitCenter2 += "        Where ProfitCenterCode Is Not Null ";

                    if (!mFrmParent.mIsAllProfitCenterSelected)
                    {
                        var Filter2 = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += " (ProfitCenterCode=@ProfitCenter" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter" + i.ToString(), x);
                            i++;
                        }
                        if (Filter2.Length == 0)
                        {
                            mQueryProfitCenter += "    And 1=0 ";
                            mQueryProfitCenter2 += "    And 1=0 ";
                        }
                        else
                        {
                            mQueryProfitCenter += "    And (" + Filter2 + ") ";
                            mQueryProfitCenter2 += "    And (" + Filter2 + ") ";
                        }
                    }
                    else
                    {
                        if (mFrmParent.ChkProfitCenterCode.Checked)
                        {
                            mQueryProfitCenter += "    And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                            mQueryProfitCenter2 += "    And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                            if (mFrmParent.ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", mFrmParent.GetCcbProfitCenterCode());
                        }
                        else
                        {
                            mQueryProfitCenter += "    And ProfitCenterCode In ( ";
                            mQueryProfitCenter += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            mQueryProfitCenter += "    ) ";

                            mQueryProfitCenter2 += "    And ProfitCenterCode In ( ";
                            mQueryProfitCenter2 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            mQueryProfitCenter2 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            mQueryProfitCenter2 += "    ) ";
                        }
                    }

                    mQueryProfitCenter += "    ) ";
                    mQueryProfitCenter2 += "    ) ";

                    if (!mFrmParent.ChkProfitCenterCode.Checked)
                    {
                        mQueryProfitCenter = "    And (C.SiteCode Is Null Or (C.SiteCode Is Not Null And C.SiteCode In ( ";
                        mQueryProfitCenter += "        Select Distinct SiteCode From TblGroupSite ";
                        mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter += "        ))) ";

                        mQueryProfitCenter2 = "    And (D.SiteCode Is Null Or (D.SiteCode Is Not Null And D.SiteCode In ( ";
                        mQueryProfitCenter2 += "        Select Distinct SiteCode From TblGroupSite ";
                        mQueryProfitCenter2 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter2 += "        ))) ";
                    }
                }

                #region Old Code
                //SQL.AppendLine("Select DocNo, (ThisYearAmt+ThisYearAmt2) ThisYearAmt ");
                //SQL.AppendLine("From ");
                //SQL.AppendLine("( ");
                //SQL.AppendLine("    Select A.DocNo, ");
                //SQL.AppendLine("    IfNull(A.Amt, 0.00) * Case AcType ");
                //SQL.AppendLine("        When 'C' Then -1 ");
                //SQL.AppendLine("        Else 1 ");
                //SQL.AppendLine("        End As ThisYearAmt, ");
                //SQL.AppendLine("    IfNull(A.Amt, 0.00) * Case AcType2 ");
                //SQL.AppendLine("        When 'C' Then -1 ");
                //SQL.AppendLine("        When 'D' Then 1 ");
                //SQL.AppendLine("        Else 0 ");
                //SQL.AppendLine("        End As ThisYearAmt2, ");
                //SQL.AppendLine("        0.00 As LastYearAmt, 0.00 As LastYearAmt2 ");
                //SQL.AppendLine("    From TblVoucherHdr A ");
                //if (mFrmParent.mIsRptCashFlowStatementUseProfitCenter)
                //{
                //    SQL.AppendLine("            Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
                //    SQL.AppendLine(mQueryProfitCenter);
                //}
                //SQL.AppendLine("    Where DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2) ");
                //SQL.AppendLine("    And CashTypeCode = @CashTypeCode ");
                //SQL.AppendLine("    And CancelInd = 'N' ");
                //SQL.AppendLine(") Tbl ");
                #endregion

                SQL.AppendLine("SELECT DocNo, CashTypeCode, IfNull(ThisYearAmt,0.00)+IfNull(ThisYearAmt2,0.00) As ThisYearAmt, Type, Source, Beneficiary ");
                SQL.AppendLine("FROM ( ");
                SQL.AppendLine("    Select A.DocNo, A.CashTypeCode, A.Type, A.Source, A.Beneficiary, ");
                SQL.AppendLine("    IfNull(A.Amt, 0.00) * Case AcType ");
                SQL.AppendLine("        When 'C' Then -1 ");
                SQL.AppendLine("        Else 1 ");
                SQL.AppendLine("        End As ThisYearAmt, ");
                if (mFrmParent.mIsVoucherUseCashType2)
                    SQL.AppendLine("0.00 as ThisYearAmt2, ");
                else
                {
                    SQL.AppendLine("    IfNull(A.Amt, 0.00) * Case AcType2 ");
                    SQL.AppendLine("        When 'C' Then -1 ");
                    SQL.AppendLine("        When 'D' Then 1 ");
                    SQL.AppendLine("        Else 0 ");
                    SQL.AppendLine("        End As ThisYearAmt2, ");
                }
                SQL.AppendLine("    0.00 As LastYearAmt, 0.00 As LastYearAmt2 ");
                SQL.AppendLine("    FROM( ");
                SQL.AppendLine("        SELECT A.DocNo, A.CashTypeCode, A.AcType, A.AcType2,  ");
                SQL.AppendLine("		Case  ");
                SQL.AppendLine("		When A.CurCode != @MainCurCode then  ");
                SQL.AppendLine("		ifnull ");
                SQL.AppendLine("		(( ");
                SQL.AppendLine("		Select Amt From TblCurrencyRate ");
                SQL.AppendLine("		Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("		Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("		),1)*A.Amt Else A.Amt End As Amt,  ");
                SQL.AppendLine("        B.OptDesc AS Type,  ");
                SQL.AppendLine("        TRIM(CONCAT( ");
                SQL.AppendLine("        Case when C.BankAcNo IS NOT NULL ");
                SQL.AppendLine("        then CONCAT(C.BankAcNo, ' [', IFNULL(C.BankAcNm, ''), ']') ");
                SQL.AppendLine("        ELSE IFNULL(C.BankAcNm, '') END, ");
                SQL.AppendLine("        case when E.BankName IS NOT NULL then CONCAT(' ', E.BankName) ELSE '' END)) AS Source, ");
                SQL.AppendLine("        TRIM(CONCAT( ");
                SQL.AppendLine("        Case when D.BankAcNo IS NOT NULL ");
                SQL.AppendLine("        then CONCAT(D.BankAcNo, ' [', IFNULL(D.BankAcNm, ''), ']') ");
                SQL.AppendLine("        ELSE IFNULL(D.BankAcNm, '') END, ");
                SQL.AppendLine("        case when F.BankName IS NOT NULL then CONCAT(' ', F.BankName) ELSE '' END)) Beneficiary ");
                SQL.AppendLine("        From TblVoucherHdr A ");
                SQL.AppendLine("        INNER JOIN tbloption B ON A.DocType = B.OptCode AND B.OptCat = 'VoucherDocType' ");
                SQL.AppendLine("        INNER Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
                if (mFrmParent.mIsRptCashFlowStatementUseProfitCenter)
                {
                    if (mFrmParent.mIsRptCashFlowStatementYearToDate)
                        SQL.AppendLine("        AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                    SQL.AppendLine(mQueryProfitCenter);
                }
                SQL.AppendLine("        LEFT Join TblBankAccount D On A.BankAcCode2=D.BankAcCode ");
                if (mFrmParent.mIsRptCashFlowStatementUseProfitCenter)
                {
                    if (mFrmParent.mIsRptCashFlowStatementYearToDate)
                        SQL.AppendLine("        AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                }
                SQL.AppendLine("        LEFT JOIN TblBank E ON C.BankCode = E.BankCode ");
                SQL.AppendLine("        LEFT JOIN tblbank F ON D.BankCode = F.BankCode ");
                SQL.AppendLine("        Where DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2) ");
                SQL.AppendLine("        And CashTypeCode = @CashTypeCode ");
                SQL.AppendLine("        And CancelInd = 'N' ");
                SQL.AppendLine("        And A.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");
                if(mFrmParent.mIsCashFlowStatementShowSwitchingBankAccountDetail)
                    SQL.AppendLine("        And A.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '16') ");
                if (mFrmParent.mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("        And A.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '70') ");
                if(mFrmParent.mIsVoucherCASAllowMultipleBankAccount)
                    SQL.AppendLine("        And A.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType in('56','58')) ");
                if (mFrmParent.mIsVoucherUseCashType2)
                {
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        SELECT A.DocNo, A.CashTypeCode2 CashTypeCode, A.AcType2 as AcType, A.AcType2, Case When A.CurCode != 'IDR' then ifnull(A.ExcRate,1)*A.Amt Else A.Amt End As Amt, B.OptDesc,  ");
                    SQL.AppendLine("        TRIM(CONCAT( ");
                    SQL.AppendLine("        Case when C.BankAcNo IS NOT NULL ");
                    SQL.AppendLine("        then CONCAT(C.BankAcNo, ' [', IFNULL(C.BankAcNm, ''), ']') ");
                    SQL.AppendLine("        ELSE IFNULL(C.BankAcNm, '') END, ");
                    SQL.AppendLine("        case when E.BankName IS NOT NULL then CONCAT(' ', E.BankName) ELSE '' END)) AS Source, ");

                    SQL.AppendLine("        TRIM(CONCAT( ");
                    SQL.AppendLine("        Case when D.BankAcNo IS NOT NULL ");
                    SQL.AppendLine("        then CONCAT(D.BankAcNo, ' [', IFNULL(D.BankAcNm, ''), ']') ");
                    SQL.AppendLine("        Else IfNull(D.BankAcNm, '') END, ");
                    SQL.AppendLine("        case when F.BankName IS NOT NULL then CONCAT(' ', F.BankName) ELSE '' END)) Beneficiary ");

                    SQL.AppendLine("        From TblVoucherHdr A ");
                    SQL.AppendLine("        Inner Join tbloption B ON A.DocType= B.OptCode AND B.OptCat = 'VoucherDocType' ");
                    SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode2=C.BankAcCode ");
                    if (mFrmParent.mIsRptCashFlowStatementUseProfitCenter)
                    {
                        if (mFrmParent.mIsRptCashFlowStatementYearToDate)
                            SQL.AppendLine("        And C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                        SQL.AppendLine(mQueryProfitCenter);
                    }
                    SQL.AppendLine("        Left Join TblBankAccount D On A.BankAcCode2=D.BankAcCode ");
                    if (mFrmParent.mIsRptCashFlowStatementUseProfitCenter)
                    {
                        if (mFrmParent.mIsRptCashFlowStatementYearToDate)
                            SQL.AppendLine("        AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                    }
                    SQL.AppendLine("        Left Join TblBank E ON C.BankCode = E.BankCode ");
                    SQL.AppendLine("        Left Join tblbank F ON D.BankCode = F.BankCode ");
                    SQL.AppendLine("        Where DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2) ");
                    SQL.AppendLine("        And CashTypeCode2 = @CashTypeCode ");
                    SQL.AppendLine("        And CancelInd = 'N' ");
                    SQL.AppendLine("        And A.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");
                    if (mFrmParent.mIsVoucherCASAllowMultipleBankAccount)
                        SQL.AppendLine("        And A.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType in('56','58')) ");
                }
                SQL.AppendLine("    ) A ");
                SQL.AppendLine(")Tbl ");

                //PHT
                if (mFrmParent.mIsCashFlowStatementShowSwitchingBankAccountDetail || mFrmParent.mIsCashFlowStatementShowCASBADetail)
                {
                    SQL.AppendLine("UNION ALL ");
                    SQL.AppendLine("    SELECT DocNo, CashtypeCode, ThisYearAmt, Type, Source, Beneficiary ");
                    SQL.AppendLine("    FROM ( ");
                    SQL.AppendLine("        SELECT A.DocNo,  A.CashTypeCode, ");
                    SQL.AppendLine("        IFNULL(A.Amt, 0.00) * case A.AcType when 'C' then -1 ELSE 1 END AS ThisYearAmt, ");
                    SQL.AppendLine("        B.OptDesc AS Type, ");
                    SQL.AppendLine("        TRIM(CONCAT( ");
                    SQL.AppendLine("        Case when C2.BankAcNo IS NOT NULL ");
                    SQL.AppendLine("        then CONCAT(C2.BankAcNo, ' [', IFNULL(C2.BankAcNm, ''), ']') ");
                    SQL.AppendLine("        ELSE IFNULL(C2.BankAcNm, '') END, ");
                    SQL.AppendLine("        case when E.BankName IS NOT NULL then CONCAT(' ', E.BankName) ELSE '' END)) AS Source, ");
                    SQL.AppendLine("        TRIM(CONCAT( ");
                    SQL.AppendLine("        Case when D2.BankAcNo IS NOT NULL ");
                    SQL.AppendLine("        then CONCAT(D2.BankAcNo, ' [', IFNULL(D2.BankAcNm, ''), ']') ");
                    SQL.AppendLine("        ELSE IFNULL(D2.BankAcNm, '') END, ");
                    SQL.AppendLine("        case when F.BankName IS NOT NULL then CONCAT(' ', F.BankName) ELSE '' END)) Beneficiary ");
                    SQL.AppendLine("        FROM TblVoucherHdr A ");
                    SQL.AppendLine("        INNER JOIN tbloption B ON A.DocType = B.OptCode AND B.OptCat = 'VoucherDocType' ");
                    if (mFrmParent.mIsRptCashFlowStatementUseProfitCenter)
                    {
                        SQL.AppendLine("        INNER JOIN TblBankAccount C On A.BankAcCode=C.BankAcCode ");
                        if (mFrmParent.mIsRptCashFlowStatementYearToDate)
                            SQL.AppendLine("        AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                        SQL.AppendLine(mQueryProfitCenter);
                        SQL.AppendLine("        LEFT Join TblBankAccount D On A.BankAcCode2=D.BankAcCode ");
                        if (mFrmParent.mIsRptCashFlowStatementYearToDate)
                            SQL.AppendLine("        AND D.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                        SQL.AppendLine(mQueryProfitCenter2);
                    }
                    SQL.AppendLine("            INNER Join TblBankAccount C2 On A.BankAcCode=C2.BankAcCode ");
                    SQL.AppendLine("            INNER Join TblBankAccount D2 On A.BankAcCode2=D2.BankAcCode ");
                    SQL.AppendLine("        LEFT JOIN TblBank E ON C2.BankCode = E.BankCode ");
                    SQL.AppendLine("        LEFT JOIN tblbank F ON D2.BankCode = F.BankCode ");
                    SQL.AppendLine("        WHERE DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2) ");
                    SQL.AppendLine("        AND CashTypeCode = @CashTypeCode ");
                    SQL.AppendLine("        AND CancelInd = 'N' ");
                    SQL.AppendLine("            And ");
                    if (mFrmParent.mIsCashFlowStatementShowSwitchingBankAccountDetail && mFrmParent.mIsCashFlowStatementShowCASBADetail)
                        SQL.AppendLine("        (A.DocType = '16' OR A.DocType = '70') ");
                    else if (mFrmParent.mIsCashFlowStatementShowSwitchingBankAccountDetail)
                        SQL.AppendLine("        A.DocType = '16' ");
                    else if (mFrmParent.mIsCashFlowStatementShowCASBADetail)
                        SQL.AppendLine("        A.DocType = '70' ");
                    SQL.AppendLine("    UNION ALL ");
                    SQL.AppendLine("        SELECT B.DocNo, B.CashTypeCode, ");
                    SQL.AppendLine("        IfNull(B.Amt, 0.00) * Case AcType2 ");
                    SQL.AppendLine("        When 'C' Then -1 ");
                    SQL.AppendLine("        When 'D' Then 1 ");
                    SQL.AppendLine("        Else 0 End As ThisYearAmt, ");
                    SQL.AppendLine("        B.Type, B.Source, B.Beneficiary ");
                    SQL.AppendLine("        FROM ( ");
                    SQL.AppendLine("                SELECT A.DocNo,  A.CashTypeCode, B.OptDesc AS Type, A.AcType, A.AcType2,  ");
                    SQL.AppendLine("				Case When A.CurCode != 'IDR' then  ");
                    SQL.AppendLine("				IFNULL(( ");
                    SQL.AppendLine("				Select Amt  ");
                    SQL.AppendLine("				From tblcurrencyrate  ");
                    SQL.AppendLine("				Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("				Order By RateDt Desc LIMIT 1),1)*A.Amt  ");
                    SQL.AppendLine("				Else A.Amt End As Amt,  ");
                    SQL.AppendLine("            TRIM(CONCAT( ");
                    SQL.AppendLine("            Case when C2.BankAcNo IS NOT NULL ");
                    SQL.AppendLine("            then CONCAT(C2.BankAcNo, ' [', IFNULL(C2.BankAcNm, ''), ']') ");
                    SQL.AppendLine("            ELSE IFNULL(C2.BankAcNm, '') END, ");
                    SQL.AppendLine("            case when E.BankName IS NOT NULL then CONCAT(' ', E.BankName) ELSE '' END)) AS Source, ");
                    SQL.AppendLine("            TRIM(CONCAT( ");
                    SQL.AppendLine("            Case when D2.BankAcNo IS NOT NULL ");
                    SQL.AppendLine("            then CONCAT(D2.BankAcNo, ' [', IFNULL(D2.BankAcNm, ''), ']') ");
                    SQL.AppendLine("            ELSE IFNULL(D2.BankAcNm, '') END, ");
                    SQL.AppendLine("            case when F.BankName IS NOT NULL then CONCAT(' ', F.BankName) ELSE '' END)) Beneficiary ");
                    SQL.AppendLine("            From TblVoucherHdr A ");
                    SQL.AppendLine("            INNER JOIN tbloption B ON A.DocType = B.OptCode AND B.OptCat = 'VoucherDocType' ");
                    if (mFrmParent.mIsRptCashFlowStatementUseProfitCenter)
                    {
                        SQL.AppendLine("            LEFT Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
                        if (mFrmParent.mIsRptCashFlowStatementYearToDate)
                            SQL.AppendLine("            AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                        SQL.AppendLine(mQueryProfitCenter);
                        SQL.AppendLine("            INNER Join TblBankAccount D On A.BankAcCode2=D.BankAcCode ");
                        if (mFrmParent.mIsRptCashFlowStatementYearToDate)
                            SQL.AppendLine("            AND D.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                        SQL.AppendLine(mQueryProfitCenter2);
                    }
                    SQL.AppendLine("            INNER Join TblBankAccount C2 On A.BankAcCode=C2.BankAcCode ");
                    SQL.AppendLine("            INNER Join TblBankAccount D2 On A.BankAcCode2=D2.BankAcCode ");
                    SQL.AppendLine("            LEFT JOIN TblBank E ON C2.BankCode = E.BankCode ");
                    SQL.AppendLine("            LEFT JOIN tblbank F ON D2.BankCode = F.BankCode ");
                    SQL.AppendLine("            WHERE DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2) ");
                    SQL.AppendLine("            And CashTypeCode = @CashTypeCode ");
                    SQL.AppendLine("            And CancelInd = 'N' ");
                    SQL.AppendLine("            And ");
                    if (mFrmParent.mIsCashFlowStatementShowSwitchingBankAccountDetail && mFrmParent.mIsCashFlowStatementShowCASBADetail)
                        SQL.AppendLine("        (A.DocType = '16' OR A.DocType = '70') ");
                    else if(mFrmParent.mIsCashFlowStatementShowSwitchingBankAccountDetail)
                        SQL.AppendLine("        A.DocType = '16' ");
                    else if (mFrmParent.mIsCashFlowStatementShowCASBADetail)
                        SQL.AppendLine("        A.DocType = '70' ");
                    SQL.AppendLine("        ) B ");
                    SQL.AppendLine("    ) Tbl2 ");
                }

                //BBT
                if (mFrmParent.mIsVoucherCASAllowMultipleBankAccount)
                {
                    SQL.AppendLine("UNION ALL ");
                    SQL.AppendLine("SELECT DOcNo, CashTypeCode, IfNull(ThisYearAmt,0.00)+IfNull(ThisYearAmt2,0.00) As ThisYearAmt, Type, Source, Beneficiary ");
                    SQL.AppendLine("FROM ( ");
                    SQL.AppendLine("	Select A.DocNo, A.CashTypeCode, A.Type, A.Source, A.Beneficiary,  ");
                    SQL.AppendLine("	IfNull(A.Amt, 0.00) * Case AcType ");
                    SQL.AppendLine("		When 'C' Then -1 ");
                    SQL.AppendLine("		When 'D' Then 1 ");
                    SQL.AppendLine("		Else 0 ");
                    SQL.AppendLine("	End As ThisYearAmt, 0.00 As ThisYearAmt2, 0.00 As LastYearAmt, 0.00 As LastYearAmt2 ");
                    SQL.AppendLine("	From ( ");
                    SQL.AppendLine("		Select A.DocNo, A.CashTypeCode, A.AcType, Null As AcType2, ");
                    SQL.AppendLine("		Case When A.CurCode != 'IDR' then ifnull(A.ExcRate,1)*A.Amt Else A.Amt End As Amt, B.OptDesc As Type, ");
                    SQL.AppendLine("		TRIM(CONCAT( ");
                    SQL.AppendLine("		Case when C.BankAcNo IS NOT NULL ");
                    SQL.AppendLine("		then CONCAT(C.BankAcNo, ' [', IFNULL(C.BankAcNm, ''), ']') ");
                    SQL.AppendLine("		ELSE IFNULL(C.BankAcNm, '') END, ");
                    SQL.AppendLine("		case when D.BankName IS NOT NULL then CONCAT(' ', D.BankName) ELSE '' END)) AS Source, Null As Beneficiary ");
                    SQL.AppendLine("		From TblVoucherHdr A ");
                    SQL.AppendLine("		Inner Join TblOption B ON A.DocType = B.OptCode AND B.OptCat = 'VoucherDocType' ");
                    SQL.AppendLine("		Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
                    if (mFrmParent.mIsRptCashFlowStatementUseProfitCenter)
                    {
                        if (mFrmParent.mIsRptCashFlowStatementYearToDate)
                            SQL.AppendLine("        And C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                        SQL.AppendLine(mQueryProfitCenter);
                    }
                    SQL.AppendLine("			And Not Find_In_Set(C.BankAcTp, @BankAccountTypeForVRCA1) ");
                    SQL.AppendLine("		Left Join TblBank D On C.BankCode = D.BankCode ");
                    SQL.AppendLine("		Where DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2)  ");
                    SQL.AppendLine("		And CashTypeCode = @CashTypeCode ");
                    SQL.AppendLine("		And CancelInd = 'N' ");
                    SQL.AppendLine("		And A.DocType In ('56','58') ");
                    SQL.AppendLine("	) A ");
                    SQL.AppendLine("	Union All ");
                    SQL.AppendLine("	Select A.DocNo, A.CashTypeCode, A.Type, A.Source, A.Beneficiary,  ");
                    SQL.AppendLine("	IfNull(A.Amt, 0.00) * Case AcType ");
                    SQL.AppendLine("		When 'C' Then -1 ");
                    SQL.AppendLine("		When 'D' Then 1 ");
                    SQL.AppendLine("		Else 0 ");
                    SQL.AppendLine("	End As ThisYearAmt, 0.00 As ThisYearAmt2, 0.00 As LastYearAmt, 0.00 As LastYearAmt2 ");
                    SQL.AppendLine("	From ( ");
                    SQL.AppendLine("		Select A.DocNo, A.CashTypeCode, A.AcType2 As AcType, Null As AcType2, ");
                    SQL.AppendLine("		Case When A.CurCode != 'IDR' then ifnull(A.ExcRate,1)*A.Amt Else A.Amt End As Amt, B.OptDesc As Type, ");
                    SQL.AppendLine("		TRIM(CONCAT( ");
                    SQL.AppendLine("		Case when C.BankAcNo IS NOT NULL ");
                    SQL.AppendLine("		then CONCAT(C.BankAcNo, ' [', IFNULL(C.BankAcNm, ''), ']') ");
                    SQL.AppendLine("		ELSE IFNULL(C.BankAcNm, '') END, ");
                    SQL.AppendLine("		case when D.BankName IS NOT NULL then CONCAT(' ', D.BankName) ELSE '' END)) AS Source, Null As Beneficiary ");
                    SQL.AppendLine("		From TblVoucherHdr A ");
                    SQL.AppendLine("		Inner Join TblOption B ON A.DocType = B.OptCode AND B.OptCat = 'VoucherDocType' ");
                    SQL.AppendLine("		Inner Join TblBankAccount C On A.BankAcCode2=C.BankAcCode ");
                    if (mFrmParent.mIsRptCashFlowStatementUseProfitCenter)
                    {
                        if (mFrmParent.mIsRptCashFlowStatementYearToDate)
                            SQL.AppendLine("        And C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                        SQL.AppendLine(mQueryProfitCenter);
                    }
                    SQL.AppendLine("			And Not Find_In_Set(C.BankAcTp, @BankAccountTypeForVRCA1) ");
                    SQL.AppendLine("		Left Join TblBank D On C.BankCode = D.BankCode ");
                    SQL.AppendLine("		Where DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2)  ");
                    SQL.AppendLine("		And CashTypeCode = @CashTypeCode ");
                    SQL.AppendLine("		And CancelInd = 'N' ");
                    SQL.AppendLine("		And A.DocType In ('56','58') ");
                    SQL.AppendLine("	) A ");
                    SQL.AppendLine(") Tbl3 ");
                }

                if (mFrmParent.mIsVoucherUseAdditionalCost)
                {
                    SQL.AppendLine("UNION All ");
                    SQL.AppendLine("SELECT DocNo, CashTypeCode, IfNull(ThisYearAmt,0.00)+IfNull(ThisYearAmt2,0.00) As ThisYearAmt, Type, Source, Beneficiary  ");
                    SQL.AppendLine("FROM (  ");
                    SQL.AppendLine("    Select A.DocNo, A.CashTypeCode, A.Type, A.Source, A.Beneficiary,  ");
                    SQL.AppendLine("    IFNULL(A.Amt, 0.00) As ThisYearAmt, 0.00 AS ThisYearAmt2, 0.00 As LastYearAmt, 0.00 As LastYearAmt2  ");
                    SQL.AppendLine("    FROM( ");
                    SQL.AppendLine("        SELECT A.DocNo, A.CashTypeCode, A.AcType, A.AcType2, Case When A.CurCode != 'IDR' then ifnull(A.ExcRate,1)*D.Amt ELSE D.Amt End As Amt,  ");
                    SQL.AppendLine("        B.OptDesc AS Type,  ");
                    SQL.AppendLine("        TRIM(CONCAT(  ");
                    SQL.AppendLine("        Case when E.BankAcNo IS NOT NULL  ");
                    SQL.AppendLine("        then CONCAT(E.BankAcNo, ' [', IFNULL(E.BankAcNm, ''), ']')  ");
                    SQL.AppendLine("        ELSE IFNULL(E.BankAcNm, '') END,  ");
                    SQL.AppendLine("        case when F.BankName IS NOT NULL then CONCAT(' ', F.BankName) ELSE '' END)) AS Source,  ");
                    SQL.AppendLine("        Null As Beneficiary  ");
                    SQL.AppendLine("        From TblVoucherHdr A  ");
                    SQL.AppendLine("        INNER JOIN tbloption B ON A.DocType = B.OptCode AND B.OptCat = 'VoucherDocType'  ");
                    SQL.AppendLine("        INNER JOIN tblvoucherdtl4 C ON A.DocNo=C.DocNo ");
                    SQL.AppendLine("        Inner Join tblexpensestypedtl D ON C.ExpensesCode = D.ExpensesCode  ");
                    SQL.AppendLine("					AND C.ExpensesDNo = D.DNo  ");
                    SQL.AppendLine("        INNER Join TblBankAccount E ON C.BankAcCode=E.BankAcCode  ");
                    SQL.AppendLine("        LEFT JOIN TblBank F ON E.BankCode = F.BankCode  ");
                    SQL.AppendLine("        WHERE A.DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2)  ");
                    //SQL.AppendLine("        AND A.CashTypeCode = @CashTypeCode  ");
                    SQL.AppendLine("        AND D.CashTypeCode = @CashTypeCode  ");
                    SQL.AppendLine("        AND A.CancelInd = 'N'  ");
                    SQL.AppendLine("    ) A  ");
                    SQL.AppendLine(")Tbl4  ");
                }

                if (mFrmParent.mIsCashFlowStatementShowSwitchingBankAccountDetail || mFrmParent.mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("ORDER BY DocNo ");
                else
                    SQL.AppendLine("GROUP BY DocNo, CashTypeCode ORDER BY DocNo ");

                string Filter = string.Empty;

                Sm.CmParam<string>(ref cm, "@Yr", mFrmParent.mYr);
                Sm.CmParam<string>(ref cm, "@Dt1", mFrmParent.mDt1);
                Sm.CmParam<string>(ref cm, "@Dt2", mFrmParent.mDt2);
                Sm.CmParam<string>(ref cm, "@CashTypeCode", mCashTypeCode);
                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@AcNoForCashFlowStatementOpeningBalance", mFrmParent.mAcNoForCashFlowStatementOpeningBalance + "%");
                Sm.CmParam<String>(ref cm, "@BankAccountTypeForVRCA1", mFrmParent.mBankAccountTypeForVRCA1);
                Sm.CmParam<String>(ref cm, "@MainCurCode", Sm.GetParameter("MainCurCode"));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString() + Filter,
                        //ref Grd1, ref cm, SQL.ToString() + Filter + "GROUP BY /*DocNo, */CashTypeCode ORDER BY DocNo ",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                            //1
                            "ThisYearAmt",
                            "Type",
                            "Source",
                            "Beneficiary"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        }, true, false, false, false
                    );
                ProcessNegativeBalance();
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private void ProcessNegativeBalance()
        {
            decimal TotalAmtYear = 0m;
            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        Grd1.Cells[i, 7].Value = Sm.GetGrdDec(Grd1, i, 6);

                        if (Sm.GetGrdDec(Grd1, i, 6) < 0m) Grd1.Cells[i, 7].Value = string.Concat("(", Sm.FormatNum((Sm.GetGrdDec(Grd1, i, 6) * -1m), 0).ToString(), ")");
                        TotalAmtYear +=  Sm.GetGrdDec(Grd1, i, 6);
                    }
                }
            }
            Grd1.Rows.Add();
            Grd1.Cells[(Grd1.Rows.Count-1), 7].Value = TotalAmtYear;
        }

        internal bool GetAccessInd(string FormName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(Left(AccessInd, 1), 'N') ");
            SQL.AppendLine("From TblGroupMenu ");
            SQL.AppendLine("Where MenuCode In ( Select MenuCode From TblMenu Where Param = @Param1 ) ");
            SQL.AppendLine("And GrpCode In ");
            SQL.AppendLine("(Select GrpCode From TblUser Where UserCode = @Param2) ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), FormName, Gv.CurrentUserCode, string.Empty) == "Y";
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #region Grid Control Events

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                var f = new FrmVoucher(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                var f = new FrmVoucher(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

    }
}
