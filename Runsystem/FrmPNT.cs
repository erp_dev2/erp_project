﻿#region Update
/*
    10/07/2018 [TKG] merubah query untuk mempercepat proses setelah menghapus data.
    28/07/2022 [DITA/IOK] generate docno di hardcode 8 digit nya
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPNT : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmPNTFind FrmFind;
        internal int mNumberOfPlanningUomCode = 1;
        private decimal mHolidayWagesIndex = 1, mNotHolidayWagesIndex = 1;

        #endregion

        #region Constructor

        public FrmPNT(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Direct Labor's Production Penalty";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetParameter();

                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sl.SetLueWagesFormulationCode(ref LueWagesFormulationCode);
                Sl.SetLueCurCode(ref LueCurCode);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "SFC#",
                        "SFC DNo",
                        "Date",
                        "Shift",

                        //6-10
                        "Direct"+Environment.NewLine+"Labor's"+Environment.NewLine+"Code",
                        "Direct Labor's"+Environment.NewLine+"Name",
                        "Position",
                        "Department",
                        "Item Code",

                        //11-15
                        "Item Name",
                        "Index",
                        "Batch#",
                        "Quantity",
                        "UoM",
                        
                        //16-20
                        "Quantity 2",
                        "Uom 2",
                        "Holiday",
                        "Holiday"+Environment.NewLine+"Index", 
                        "Value",

                        //21-22
                        "Holiday Description",
                        "Shift Code"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 130, 0, 100, 150, 
                    
                        //6-10
                        100, 250, 150, 150, 80,

                        //11-15
                        250, 70, 180, 80, 70,  

                        //16-20
                        80, 70, 80, 70, 70,

                        //21-22
                        200, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 18 });
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16, 19, 20 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 8, 9, 10, 16, 17, 21, 22 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 17, 19, 20, 21, 22 });
            Grd1.Cols[21].Move(18);

            #endregion

            #region Grd2

            Grd2.Cols.Count = 13;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Date",
                        "Shift Code",
                        "Shift",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        
                        //6-10
                        "Position",
                        "Department",
                        "Value From",
                        "Value To",
                        "Value",
                        
                        //11-12
                        "Penalty/Unit",
                        "Penalty"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        80, 0, 150, 100, 200, 
                        
                        //6-10
                        150, 150, 100, 100, 100, 
                        
                        //11-12
                        130, 130                    
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 1 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 4, 6, 7, 8, 9 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDec(Grd2, new int[] { 8, 9, 10, 11, 12 }, 0);

            #endregion

            #region Grd3

            Grd3.Cols.Count = 18;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Date",
                        "Shift Code",
                        "Shift",
                        "Coordinator's"+Environment.NewLine+"Code",
                        "Coordinator's Name",
                        
                        //6-10
                        "Position",
                        "Department",
                        "Grade"+Environment.NewLine+"Level",
                        "Grade"+Environment.NewLine+"Index",
                        "Penalty (Before"+Environment.NewLine+"Holiday)",
                        
                        //11-15
                        "Holiday",
                        "Holiday"+Environment.NewLine+"Index", 
                        "Penalty",
                        "Number of"+Environment.NewLine+"NC (All)",
                        "Number of"+Environment.NewLine+"NC (Penalty)",
                        
                        //16-17
                        "NC"+Environment.NewLine+"Penalty",
                        "Grade"+Environment.NewLine+"Level Code",
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        80, 0, 150, 100, 200, 
                    
                        //6-10
                        150, 150, 150, 120, 150, 

                        //11-15
                        150, 150, 100, 150, 150,

                        //16-17
                        130, 80
                    }
                );
            Sm.GrdFormatDate(Grd3, new int[] { 1 });
            Sm.GrdFormatDec(Grd3, new int[] { 9, 10, 12, 13, 16 }, 0);
            Sm.GrdFormatDec(Grd3, new int[] { 14, 15 }, 1);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 4, 6, 7, 8, 11, 14, 15, 16, 17 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            
            #endregion

            #region Grd4

            Grd4.Cols.Count = 7;
            Grd4.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Penalty",

                        //6
                        "Payrun Code"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 200, 150, 150, 150,

                        //6
                        100
                    }
                );
            Sm.GrdColInvisible(Grd4, new int[] { 0, 1, 3, 4 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDec(Grd4, new int[] { 5 }, 0);

            #endregion

            #region Grd5

            Grd5.Cols.Count = 7;
            Grd5.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Penalty",

                        //6
                        "Payrun Code"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 200, 150, 150, 150,

                        //6
                        100
                    }
                );
            Sm.GrdColInvisible(Grd5, new int[] { 0, 1, 3, 4 }, false);
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDec(Grd5, new int[] { 5 }, 0);

            #endregion

            #region Grd6

            Grd6.Cols.Count = 14;
            Grd6.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "SFC#",
                        "DNo",
                        "Item's Code",
                        "Item's Name",
                        "Batch#",

                        //6-10
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "Processed"+Environment.NewLine+"Quantity",
                        "Balance",
                        "UoM",
                        "Outstanding"+Environment.NewLine+"Quantity 2",
                        
                        //11-13
                        "Processed"+Environment.NewLine+"Quantity 2",
                        "Balance 2",
                        "UoM 2"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        130, 0, 80, 250, 200,   

                        //6-10
                        100, 100, 100, 100, 100,

                        //11-13
                        100, 100, 100
                    }
                );
            Sm.GrdColInvisible(Grd6, new int[] { 0, 1, 2, 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdFormatDec(Grd6, new int[] { 6, 7, 8, 10, 11, 12 }, 0);

            #endregion

            ShowPlanningUomCode();
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 6, 8, 9, 10, 21 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 4, 6, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 4, 6, 7, 8, 11, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd4, new int[] { 1, 3, 4 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd5, new int[] { 1, 3, 4 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd6, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowPlanningUomCode()
        {
            if (mNumberOfPlanningUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17 }, true);
                Sm.GrdColInvisible(Grd6, new int[] { 10, 11, 12, 13 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkCancelInd, DteDocDt, LueWorkCenterDocNo, LueWagesFormulationCode, 
                        TxtUomCode, LueCurCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 14, 16, 18 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueWorkCenterDocNo, LueCurCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 14, 16, 18 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, LueWorkCenterDocNo, LueWagesFormulationCode, TxtUomCode, 
                 LueCurCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        #region Clear Grid

        private void ClearGrd()
        { 
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            ClearGrd4();
            ClearGrd5();
            ClearGrd6();
        }

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 18 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 12, 14, 16, 19, 20 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 8, 9, 10, 11, 12 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 9, 10, 12, 13, 14, 15, 16 });
        }

        private void ClearGrd4()
        {
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 5 });
        }

        private void ClearGrd5()
        {
            Grd5.Rows.Clear();
            Grd5.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 5 });
        }

        private void ClearGrd6()
        {
            Grd6.Rows.Clear();
            Grd6.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 6, 7, 8, 10, 11, 12 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPNTFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sl.SetLueWorkCenterDocNo(ref LueWorkCenterDocNo, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo =  GenerateDocNo(Sm.GetDte(DteDocDt), "PNT", "TblPNTHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SavePNTHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SavePNTDtl(DocNo, Row));

            //cml.Add(DeletePNT(DocNo));
            
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SavePNTDtl2(DocNo, Row));

            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SavePNTDtl3(DocNo, Row));

            int DNo = 1;
            for (int Row = 0; Row < Grd4.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                {
                    cml.Add(SavePNTDtl4a(DocNo, Sm.Right("00" + DNo, 3), Row));
                    DNo += 1;
                }
            }

            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
                {
                    cml.Add(SavePNTDtl4b(DocNo, Sm.Right("00" + DNo, 3), Row));
                    DNo += 1;
                }
            }

            for (int Row = 0; Row < Grd6.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd6, Row, 1).Length > 0) cml.Add(SavePNTDtl5(DocNo, Row));

            cml.Add(UpdateSFCProcessInfo(DocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWorkCenterDocNo, "Work center") ||
                Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsSFCAlreadyCancelled() ||
                IsSFCAlreadyFulfilled() ||
                IsGrdValueNotValid();
        }

        private void RecomputeOutstanding()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, ");
            SQL.AppendLine("B.Qty-IfNull(C.Qty, 0)-IfNull(D.Qty, 0) As Qty, ");
            SQL.AppendLine("B.Qty2-IfNull(C.Qty2, 0)-IfNull(D.Qty2, 0) As Qty2 ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Locate(Concat('##', B.DocNo, B.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNTHdr T1  ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.DNo ");
            SQL.AppendLine("        And Locate(Concat('##', T3.DocNo, T3.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNT2Hdr T1  ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.DNo ");
            SQL.AppendLine("        And Locate(Concat('##', T3.DocNo, T3.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo And B.DNo=D.DNo ");
            SQL.AppendLine("Order By A.DocNo, B.DNo;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@SelectedSFC", GetSelectedSFC2());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "Qty", "Qty2" });

                if (dr.HasRows)
                {
                    Grd6.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0;Row < Grd6.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd6, Row, 1), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd6, Row, 2), Sm.DrStr(dr, 1))
                                )
                            {
                                Sm.SetGrdValue("N", Grd6, dr, c, Row, 6, 2);
                                Sm.SetGrdValue("N", Grd6, dr, c, Row, 10, 3);

                                Grd6.Cells[Row, 8].Value = Sm.GetGrdDec(Grd6, Row, 6) - Sm.GetGrdDec(Grd6, Row, 7);
                                Grd6.Cells[Row, 12].Value = Sm.GetGrdDec(Grd6, Row, 10) - Sm.GetGrdDec(Grd6, Row, 11);
                                break;
                            }

                        }
                    }
                    Grd6.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsSFCAlreadyCancelled()
        {
            string DocNo = string.Empty;
            return Sm.IsDataNotValid(
                "Select DocNo From TblShopFloorControlHdr " +
                "Where CancelInd='Y' And Locate(Concat('##', DocNo, '##'), @Param)>0 Limit 1;",
                GetSelectedSFC2(), ref DocNo,
                "SFC# : " + DocNo + Environment.NewLine + "This document already cancelled.");
        }

        private bool IsSFCAlreadyFulfilled()
        {
            string Msg = string.Empty;
            if (Grd6.Rows.Count > 1)
            {
                for (int Row = 0;Row < Grd6.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd6, Row, 1).Length != 0)
                    {
                        Msg =
                            "SFC# : " + Sm.GetGrdStr(Grd6, Row, 1) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd6, Row, 4) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd6, Row, 5) + Environment.NewLine + Environment.NewLine;

                        if (IsSFCAlreadyFulfilled(Row))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "This document already fulfilled.");
                            Sm.FocusGrd(Grd6, Row, 1);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsSFCAlreadyFulfilled(int Row)
        {
            var cm = new MySqlCommand() 
            { 
                CommandText = 
                    "Select DocNo From TblShopFloorControlDtl " +
                    "Where ProcessInd2='F' And DocNo=@DocNo And DNo=@DNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd6, Row, 1));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd6, Row, 2));
            return Sm.IsDataExist(cm);
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            if (Grd1.Rows.Count>1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    Msg = 
                        "SFC# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Direct Labor's Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "SFC# should not be empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 14, true, Msg + "Quantity should be greater than 0.")) return true;
                    if (Grd1.Cols[16].Visible && Sm.IsGrdValueEmpty(Grd1, Row, 16, true, Msg + "Quantity (2) should be greater than 0.")) return true;
                }
            }

            if (Grd6.Rows.Count>1)
            {
                RecomputeOutstanding();

                for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd6, Row, 1).Length != 0)
                    {
                        Msg =
                            "SFC# : " + Sm.GetGrdStr(Grd6, Row, 1) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd6, Row, 4) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd6, Row, 5) + Environment.NewLine + Environment.NewLine;

                        if (Sm.GetGrdStr(Grd6, Row, 8).Length != 0 &&
                            decimal.Parse(Sm.GetGrdStr(Grd6, Row, 8)) < 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Balance should not be less than 0.");
                            Sm.FocusGrd(Grd6, Row, 8);
                            return true;
                        }

                        if (Grd6.Cols[12].Visible &&
                            Sm.GetGrdStr(Grd6, Row, 12).Length != 0 &&
                             decimal.Parse(Sm.GetGrdStr(Grd6, Row, 12)) < 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Balance (2) should not be less than 0.");
                            Sm.FocusGrd(Grd6, Row, 12);
                            return true;
                        }
                    }
                
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 SFC.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        //private MySqlCommand DeletePNT(string DocNo)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Delete From TblPNTDtl2 Where DocNo=@DocNo; " +
        //            "Delete From TblPNTDtl3 Where DocNo=@DocNo; " +
        //            "Delete From TblPNTDtl4 Where DocNo=@DocNo; " +
        //            "Delete From TblPNTDtl5 Where DocNo=@DocNo; "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    return cm;
        //}

        private MySqlCommand SavePNTHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPNTHdr(DocNo, DocDt, CancelInd, WorkCenterDocNo, WagesFormulationCode, UomCode, CurCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @WorkCenterDocNo, @WagesFormulationCode, @UomCode, @CurCode, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetLue(LueWorkCenterDocNo));
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@UomCode", TxtUomCode.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePNTDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNTDtl(DocNo, DNo, ShopFloorControlDocNo, ShopFloorControlDNo, EmpCode, Qty, Qty2, ItIndex, HolInd, HolIndex, Value, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ShopFloorControlDocNo, @ShopFloorControlDNo, @EmpCode, @Qty, @Qty2, @ItIndex, @HolInd, @HolIndex, @Value, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@ItIndex", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@HolInd", Sm.GetGrdBool(Grd1, Row, 18) ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@HolIndex", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNTDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNTDtl2(DocNo, DNo, DocDt, ProductionShiftCode, EmpCode, ValueFrom, ValueTo, Value, Penalty, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @DocDt, @ProductionShiftCode, @EmpCode, @ValueFrom, @ValueTo, @Value, @Penalty, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetGrdDate(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@ProductionShiftCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@ValueFrom", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@ValueTo", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNTDtl3(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNTDtl3(DocNo, DNo, DocDt, ProductionShiftCode, EmpCode, GrdLvlCode, GrdLvlIndex, HolIndex, NoOfEmp, NoOfEmp2, Amt, Amt1, Amt2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @DocDt, @ProductionShiftCode, @EmpCode, @GrdLvlCode, @GrdLvlIndex, @HolIndex, @NoOfEmp, @NoOfEmp2, @Amt, @Amt1, @Amt2, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetGrdDate(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@ProductionShiftCode", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd3, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex", Sm.GetGrdDec(Grd3, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@HolIndex", Sm.GetGrdDec(Grd3, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@NoOfEmp", Sm.GetGrdDec(Grd3, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@NoOfEmp2", Sm.GetGrdDec(Grd3, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd3, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Sm.GetGrdDec(Grd3, Row, 13));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNTDtl4a(string DocNo, string DNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNTDtl4(DocNo, DNo, ProcessInd, CoordinatorInd, EmpCode, Penalty, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'O', 'N', @EmpCode, @Penalty, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd4, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNTDtl4b(string DocNo, string DNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNTDtl4(DocNo, DNo, ProcessInd, CoordinatorInd, EmpCode, Penalty, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'O', 'Y', @EmpCode, @Penalty, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd5, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd5, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNTDtl5(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNTDtl5(DocNo, DNo, ShopFloorControlDocNo, ShopFloorControlDNo, Qty, Qty2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ShopFloorControlDocNo, @ShopFloorControlDNo, @Qty, @Qty2, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", Sm.GetGrdStr(Grd6, Row, 1));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDNo", Sm.GetGrdStr(Grd6, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd6, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd6, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateSFCProcessInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblShopFloorControlDtl A ");
            SQL.AppendLine("Inner Join TblPNTDtl5 B ");
            SQL.AppendLine("    On A.DocNo=B.ShopFloorControlDocNo ");
            SQL.AppendLine("    And A.DNo=B.ShopFloorControlDNo ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNTHdr T1  ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.ShopFloorControlDocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNT2Hdr T1  ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.ShopFloorControlDocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            //SQL.AppendLine("Left Join (");
            //SQL.AppendLine("    Select T1.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            //SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            //SQL.AppendLine("    From TblProductionPenaltyHdr T1 ");
            //SQL.AppendLine("    Inner Join TblProductionPenaltyDtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblPNTDtl5 T3 ");
            //SQL.AppendLine("        On T1.ShopFloorControlDocNo=T3.ShopFloorControlDocNo ");
            //SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            //SQL.AppendLine("        And T3.DocNo=@DocNo ");
            //SQL.AppendLine("    Where T1.CancelInd='N'  ");
            //SQL.AppendLine("    Group By T1.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            //SQL.AppendLine(") D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("Set A.ProcessInd2=");
            SQL.AppendLine("    If(A.Qty+A.Qty2<>0 And IfNull(C.Qty, 0)+IfNull(C.Qty2, 0)+IfNull(D.Qty, 0)+IfNull(D.Qty2, 0)=0, 'O', ");
            SQL.AppendLine("        If(A.Qty+A.Qty2-IfNull(C.Qty, 0)-IfNull(C.Qty2, 0)-IfNull(D.Qty, 0)-IfNull(D.Qty2, 0)=0, 'F', 'P') ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Update TblShopFloorControlHdr T ");
            SQL.AppendLine("Set T.ProcessInd2=");
            SQL.AppendLine("    Case When Not Exists(Select DocNo From TblShopFloorControlDtl Where DocNo=T.DocNo And ProcessInd2<>'O' Limit 1) Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If( ");
            SQL.AppendLine("        Not Exists(Select DocNo From TblShopFloorControlDtl Where DocNo=T.DocNo And ProcessInd2<>'F' Limit 1), ");
            SQL.AppendLine("            'F', 'P') ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select ShopFloorControlDocNo From TblPNTDtl5 Where DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditPNTHdr());
            cml.Add(UpdateSFCProcessInfo(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToPPR();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPNTHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyProcessedToPPR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPNTDtl4 ");
            SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to production payrun.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditPNTHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPNTHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowPNTHdr(DocNo);
                ShowPNTDtl(DocNo);
                ShowPNTDtl2(DocNo);
                ShowPNTDtl3(DocNo);
                ShowPNTDtl4a(DocNo);
                ShowPNTDtl4b(DocNo);
                ShowPNTDtl5(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPNTHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, WorkCenterDocNo, WagesFormulationCode, UomCode, CurCode, Remark " +
                    "From TblPNTHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "WorkCenterDocNo", "WagesFormulationCode", "UomCode", 

                        //6-7
                        "CurCode", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = (Sm.DrStr(dr, c[2]) == "Y");
                        Sl.SetLueWorkCenterDocNo(ref LueWorkCenterDocNo, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueWorkCenterDocNo, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueWagesFormulationCode, Sm.DrStr(dr, c[4]));
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[6]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowPNTDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ShopFloorControlDocNo, A.ShopFloorControlDNo, B.DocDt, C.ProductionShiftName, ");
            SQL.AppendLine("A.EmpCode, D.EmpName, E.PosName, F.DeptName, G.ItCode, H.ItName, A.ItIndex, G.BatchNo, ");
            SQL.AppendLine("A.Qty, H.PlanningUomCode, A.Qty2, H.PlanningUomCode2, ");
            SQL.AppendLine("I.HolName, A.HolInd, A.HolIndex, A.Value, B.ProductionShiftCode ");
            SQL.AppendLine("From TblPNTDtl A ");
            SQL.AppendLine("Inner Join TblShopFloorControlHdr B On A.ShopFloorControlDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblProductionShift C On B.ProductionShiftCode=C.ProductionShiftCode ");
            SQL.AppendLine("Left Join TblEmployee D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            SQL.AppendLine("Left Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl G On A.ShopFloorControlDocNo=G.DocNo And A.ShopFloorControlDNo=G.DNo ");
            SQL.AppendLine("Inner Join TblItem H On G.ItCode=H.ItCode ");
            SQL.AppendLine("Left Join TblHoliday I On B.DocDt=I.HolDt ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "ShopFloorControlDocNo", "ShopFloorControlDNo", "DocDt", "ProductionShiftName", "EmpCode", 
                    
                    //6-10
                    "EmpName", "PosName", "DeptName", "ItCode", "ItName", 
                    
                    //11-15
                    "ItIndex", "BatchNo", "Qty", "PlanningUomCode", "Qty2", 
                    
                    //16-20
                    "PlanningUomCode2", "HolInd", "HolIndex", "Value", "HolName",

                    //21
                    "ProductionShiftCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 18 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 14, 16, 19, 20 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPNTDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DocDt, A.ProductionShiftCode, E.ProductionShiftName, A.EmpCode, B.EmpName, C.PosName, D.DeptName, ");
            SQL.AppendLine("A.ValueFrom, A.ValueTo, A.Value, A.Penalty, A.Amt ");
            SQL.AppendLine("From TblPNTDtl2 A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblProductionShift E On A.ProductionShiftCode=E.ProductionShiftCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "DocDt", "ProductionShiftCode", "ProductionShiftName", "EmpCode", "EmpName", 
                    
                    //6-10
                    "PosName", "DeptName", "ValueFrom", "ValueTo", "Value",  
                    
                    //11-12
                    "Penalty", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 8, 9, 10, 11, 12 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowPNTDtl3(string DocNo)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select A.DNo, A.DocDt, A.ProductionShiftCode, B.ProductionShiftName, ");
            SQL.AppendLine("A.EmpCode, C.EmpName, D.PosName, E.DeptName, F.GrdLvlName, ");
            SQL.AppendLine("A.GrdLvlIndex, A.Amt1, G.HolName, A.HolIndex, A.Amt2, A.NoOfEmp, A.NoOfEmp2, A.Amt ");
            SQL.AppendLine("From TblPNTDtl3 A ");
            SQL.AppendLine("Left Join TblProductionShift B On A.ProductionShiftCode=B.ProductionShiftCode ");
            SQL.AppendLine("Left Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr F On A.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("Left Join TblHoliday G On A.DocDt=G.HolDt ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "DocDt", "ProductionShiftCode", "ProductionShiftName", "EmpCode", "EmpName", 
                    
                    //6-10
                    "PosName", "DeptName", "GrdLvlName", "GrdLvlIndex", "Amt1", 
 
                    //11-15
                    "HolName", "HolIndex", "Amt2", "NoOfEmp", "NoOfEmp2", 
                    
                    //16
                    "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 16);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 9, 10, 12, 13, 14, 15, 16 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowPNTDtl4a(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, A.Penalty, A.PayrunCode ");
            SQL.AppendLine("From TblPNTDtl4 A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.CoordinatorInd='N' And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "EmpCode", "EmpName", "PosName", "DeptName", "Penalty",

                    //6
                    "PayrunCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowPNTDtl4b(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, A.Penalty, A.PayrunCode ");
            SQL.AppendLine("From TblPNTDtl4 A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.CoordinatorInd='Y' And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd5, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "EmpCode", "EmpName", "PosName", "DeptName", "Penalty",

                    //6
                    "PayrunCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ShowPNTDtl5(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ShopFloorControlDocNo, A.ShopFloorControlDNo, ");
            SQL.AppendLine("C.ItCode, D.ItName, C.BatchNo, ");
            SQL.AppendLine("C.Qty-IfNull(F.Qty, 0)-IfNull(G.Qty, 0)+if(E.CancelInd='N', A.Qty, 0) As OutstandingQty, ");
            SQL.AppendLine("A.Qty, D.PlanningUomCode, ");
            SQL.AppendLine("C.Qty2-IfNull(F.Qty2, 0)-IfNull(G.Qty2, 0)+if(E.CancelInd='N', A.Qty2, 0) As OutstandingQty2, ");
            SQL.AppendLine("A.Qty2, D.PlanningUomCode2 ");
            SQL.AppendLine("From TblPNTDtl5 A ");
            SQL.AppendLine("Inner Join TblShopFloorControlHdr B On A.ShopFloorControlDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl C On A.ShopFloorControlDocNo=C.DocNo And A.ShopFloorControlDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblPNTHdr E On A.DocNo=E.DocNo ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNTHdr T1 ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.ShopFloorControlDocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N'  ");
            SQL.AppendLine("    And T1.DocNo<>@DocNo ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") F On A.ShopFloorControlDocNo=F.DocNo And A.ShopFloorControlDNo=F.DNo ");

            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNT2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.ShopFloorControlDocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N'  ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") G On A.ShopFloorControlDocNo=G.DocNo And A.ShopFloorControlDNo=G.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd6, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "ShopFloorControlDocNo", "ShopFloorControlDNo", "ItCode", "ItName", "BatchNo", 
                    
                    //6-10
                    "OutstandingQty", "Qty", "PlanningUomCode", "OutstandingQty2", "Qty2", 
                    
                    //11
                    "PlanningUomCode2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Grd.Cells[Row, 8].Value = Sm.GetGrdDec(Grd, Row, 6) - Sm.GetGrdDec(Grd, Row, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Grd.Cells[Row, 12].Value = Sm.GetGrdDec(Grd, Row, 10) - Sm.GetGrdDec(Grd, Row, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 6, 7, 8, 10, 11, 12 });
            Sm.FocusGrd(Grd6, 0, 1);
        }

        #endregion

        #region Additional Method

        public string GenerateDocNo(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            bool
                IsDocNoFormatUseFullYear = Sm.GetParameterBoo("IsDocNoFormatUseFullYear"),
                IsDocNoResetByDocAbbr = Sm.GetParameterBoo("IsDocNoResetByDocAbbr"),
                IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = "8";

            //if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");
            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
                SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
                SQL.Append("       From " + Tbl);
                //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                if (IsDocNoResetByDocAbbr)
                    SQL.Append("       And DocNo Like '%/" + DocAbbr + "/%' ");
                //SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-9," + DocSeqNo + "), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
                //SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
                SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
                SQL.Append("       From " + Tbl);
                //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                if (IsDocNoResetByDocAbbr)
                    SQL.Append("       And DocNo Like '%/" + DocAbbr + "/%' ");
                //SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-9," + DocSeqNo + "), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
                //SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }

            return Sm.GetValue(SQL.ToString());

        }
        private void SetParameter()
        {
            var Param = string.Empty;

            Param = Sm.GetParameter("NumberOfPlanningUomCode");
            if (Param.Length != 0) mNumberOfPlanningUomCode = int.Parse(Param);

            Param = Sm.GetParameter("HolidayIndex");
            if (Param.Length != 0) mHolidayWagesIndex = Decimal.Parse(Param)/10m;

            Param = Sm.GetParameter("NotHolidayWagesIndex");
            if (Param.Length != 0) mNotHolidayWagesIndex = Decimal.Parse(Param);
        }

        private void ShowWagesFormulationUom()
        {
            try
            {
                TxtUomCode.EditValue = null;
                var WagesFormulationCode = Sm.GetLue(LueWagesFormulationCode);

                if (WagesFormulationCode.Length != 0)
                {
                    var cm = new MySqlCommand()
                    {
                        CommandText =
                            "Select UomCode From TblWagesFormulationHdr " +
                            "Where WagesFormulationCode=@WagesFormulationCode;"
                    };
                    Sm.CmParam<String>(ref cm, "@WagesFormulationCode", WagesFormulationCode);
                    TxtUomCode.EditValue = Sm.GetValue(cm);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        //private void ComputeQtyBasedOnConvertionFormula(
        //    string ConvertType, iGrid Grd, int Row, int ColItCode,
        //    int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        //{
        //    try
        //    {
        //        if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //        {
        //            decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //            if (Convert != 0)
        //                Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        internal string GetSelectedData()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            Sm.GetGrdStr(Grd1, Row, 6) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedSFC()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedSFC2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ShowDocInfo()
        {
            try
            {
                ClearGrd2();
                ClearGrd3();
                ClearGrd4();
                ClearGrd5();
                ClearGrd6();

                ShowPenaltyBasedOnFormula();
                ShowSCFItem();
                ShowCoordinatorSCF();
                ShowNonCoordinatorPenalty();
                ShowCoordinatorPenalty();

                ComputeItemProcessed();

                for (int Row1 = 0; Row1 < Grd1.Rows.Count; Row1++)
                    if (Sm.GetGrdStr(Grd1, Row1, 4).Length != 0)
                        ComputePenaltyBasedOnFormula(
                            Sm.GetGrdDate(Grd1, Row1, 4),
                            Sm.GetGrdStr(Grd1, Row1, 22),
                            Sm.GetGrdStr(Grd1, Row1, 6)
                            );

                ComputeCoordinatorSCFPenalty();
                ComputeNonCoordinatorPenalty();
                ComputeCoordinatorPenalty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowPenaltyBasedOnFormula()
        {
            var SQL = new StringBuilder();

            var cm = new MySqlCommand();
            string Filter = string.Empty, Filter2 = string.Empty, Filter3 = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T1.DocNo=@DocNo" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No, Sm.GetGrdStr(Grd1, Row, 2));
                    }
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                    {
                        if (Filter2.Length > 0) Filter2 += " Or ";
                        Filter2 += "(T2.BomCode=@BomCode" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@BomCode" + No, Sm.GetGrdStr(Grd1, Row, 6));
                    }
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                    {
                        if (Filter3.Length > 0) Filter3 += " Or ";
                        Filter3 += "(A.DocDt=@DocDt" + No + " And A.ProductionShiftCode=@ProductionShiftCode" + No + " And A.EmpCode=@EmpCode" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@DocDt" + No, Sm.GetGrdDate(Grd1, Row, 4).Substring(0, 8));
                        Sm.CmParam<String>(ref cm, "@ProductionShiftCode" + No, Sm.GetGrdStr(Grd1, Row, 22));
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No, Sm.GetGrdStr(Grd1, Row, 6));
                    }
                    No += 1;
                }
            }

            if (Filter.Length == 0) Filter = " 0=1 ";
            if (Filter2.Length == 0) Filter2 = " 0=0 ";
            if (Filter3.Length == 0) Filter3 = " 0=0 ";

            SQL.AppendLine("Select A.DocDt, A.ProductionShiftCode, F.ProductionShiftName, ");
            SQL.AppendLine("A.EmpCode, B.EmpName, C.PosName, D.DeptName, ");
            SQL.AppendLine("E.ValueFrom, E.ValueTo, E.Penalty ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select Distinct T1.DocDt, T1.ProductionShiftCode, T2.BomCode As EmpCode ");
            SQL.AppendLine("    From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("    Inner Join TblShopFloorControl2Dtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.BomType='3' ");
            SQL.AppendLine("        And T2.CoordinatorInd='N' ");
            SQL.AppendLine("        And (" + Filter2 + ") ");
            //SQL.AppendLine("        And Locate(Concat('##', T2.BomCode, '##'), @GetSelectedEmployee)>0 ");
            //SQL.AppendLine("    Where Locate(Concat('##', T1.DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    Where (" + Filter + ") ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl E On E.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Left Join TblProductionShift F On A.ProductionShiftCode=F.ProductionShiftCode ");
            //SQL.AppendLine("Where Locate(Concat('##', A.DocDt, A.ProductionShiftCode, A.EmpCode, '##'), @GetSelectedData)>0 ");
            SQL.AppendLine("Where (" + Filter3 + ") ");
            SQL.AppendLine("Order By A.DocDt, A.ProductionShiftCode, B.EmpName, A.EmpCode, E.ValueFrom;");


            //var cm = new MySqlCommand();

            //Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC2());
            //Sm.CmParam<String>(ref cm, "@GetSelectedEmployee", GetSelectedEmployee());
            //Sm.CmParam<String>(ref cm, "@GetSelectedData", GetSelectedData2());
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));

            Sm.ShowDataInGrid(
                   ref Grd2, ref cm, SQL.ToString(),
                   new string[] 
                   { 
                       //0
                       "DocDt", 
                       
                       //1-5
                       "ProductionShiftCode", "ProductionShiftName", "EmpCode", "EmpName", "PosName", 
                       
                       //6-9
                       "DeptName", "ValueFrom", "ValueTo", "Penalty"
                   },
                   (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                   {
                       Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                       Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                       Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                       Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                       Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                       Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                       Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                       Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                       Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                       Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                       Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 10, 12 });
                   }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 8, 9, 10, 11, 12 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private string GetSelectedEmployee()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 6) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedData2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL += "##" + 
                            Sm.GetGrdDate(Grd1, Row, 4).Substring(0, 8) +
                            Sm.GetGrdStr(Grd1, Row, 22) +
                            Sm.GetGrdStr(Grd1, Row, 6) + 
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ComputeValue(int Row)
        {
            string UomCode = TxtUomCode.Text;

            Decimal Qty = 0m, ItemIndex = 0m, HolidayIndex = 0m;

            if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd1, Row, 15)))
            {
                if (Sm.GetGrdStr(Grd1, Row, 14).Length > 0) Qty = Sm.GetGrdDec(Grd1, Row, 14);
            }
            else
            {
                if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd1, Row, 17)))
                {
                    if (Sm.GetGrdStr(Grd1, Row, 16).Length > 0) Qty = Sm.GetGrdDec(Grd1, Row, 16);
                }
            }

            if (Sm.GetGrdStr(Grd1, Row, 12).Length > 0) ItemIndex = Sm.GetGrdDec(Grd1, Row, 12);
            if (Sm.GetGrdStr(Grd1, Row, 19).Length > 0) HolidayIndex = Sm.GetGrdDec(Grd1, Row, 19);
            Grd1.Cells[Row, 20].Value = Qty * ItemIndex * HolidayIndex;
        }

        private void ComputePenaltyBasedOnFormula(string DocDt, string ProductionShiftCode, string EmpCode)
        {
            decimal Value = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row1 = 0; Row1 < Grd1.Rows.Count; Row1++)
                    if (
                        Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd1, Row1, 4)) &&
                        Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd1, Row1, 22)) &&
                        Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, Row1, 6))
                        )
                        Value += Sm.GetGrdDec(Grd1, Row1, 20);
            }

            int Row = -1;
            if (Grd2.Rows.Count > 1)
            {
                for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                {
                    if (
                        Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd2, Row2, 1)) &&
                        Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd2, Row2, 2)) &&
                        Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd2, Row2, 4))
                        )
                    {
                        Row = Row2;
                        break;
                    }
                }

                if (Row >= 0)
                {
                    //set 0 utk value dan amount 
                    for (int Row2 = Row; Row2 < Grd2.Rows.Count; Row2++)
                    {
                        if (
                            Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd2, Row2, 1)) &&
                            Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd2, Row2, 2)) &&
                            Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd2, Row2, 4))
                            )
                        {
                            Grd2.Cells[Row2, 10].Value = 0m;
                            Grd2.Cells[Row2, 12].Value = 0m;
                        }
                        else
                            break;
                    }

                    decimal Temp = Value, To = 0m;

                    for (int Row2 = Row; Row2 < Grd2.Rows.Count; Row2++)
                    {
                        if (
                            Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd2, Row2, 1)) &&
                            Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd2, Row2, 2)) &&
                            Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd2, Row2, 4))
                            )
                        {
                            if (Row2 == Row && Value < Sm.GetGrdDec(Grd2, Row2, 8))
                            {
                                Temp = 0m;
                                break;
                            }

                            To = Sm.GetGrdDec(Grd2, Row2, 9);

                            if (Temp <= To)
                            {
                                Grd2.Cells[Row2, 10].Value = Temp;
                                Grd2.Cells[Row2, 12].Value = Temp * Sm.GetGrdDec(Grd2, Row2, 11);
                                Temp = 0m;
                            }
                            else
                            {
                                Grd2.Cells[Row2, 10].Value = To;
                                Grd2.Cells[Row2, 12].Value = To * Sm.GetGrdDec(Grd2, Row2, 11);
                                Temp -= To;
                            }

                            if (Temp == 0m) break;
                        }
                    }
                }
            }
        }

        private void ShowSCFItem()
        {
            var SQL = new StringBuilder();

            var cm = new MySqlCommand();
            var Filter = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (Tbl.DocNo=@DocNo" + No + " And Tbl.DNo=@DNo" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No, Sm.GetGrdStr(Grd1, Row, 2));
                        Sm.CmParam<String>(ref cm, "@DNo" + No, Sm.GetGrdStr(Grd1, Row, 3));
                        No += 1;
                    }
                }
            }

            if (Filter.Length == 0) Filter = " 0=1 ";

            SQL.AppendLine("Select A.DocNo, B.DNo, ");
            SQL.AppendLine("B.ItCode, B.BatchNo, E.ItName, E.PlanningUomCode, E.PlanningUomCode2, ");
            SQL.AppendLine("B.Qty-IfNull(C.Qty, 0)-IfNull(D.Qty, 0) As Qty, ");
            SQL.AppendLine("B.Qty2-IfNull(C.Qty2, 0)-IfNull(D.Qty2, 0) As Qty2 ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            //SQL.AppendLine("    And Locate(Concat('##', B.DocNo, B.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    And ("+Filter.Replace("Tbl.","B.")+") ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNTHdr T1  ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.DNo ");
            //SQL.AppendLine("        And Locate(Concat('##', T3.DocNo, T3.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("        And (" + Filter.Replace("Tbl.", "T3.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNT2Hdr T1  ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.DNo ");
            //SQL.AppendLine("        And Locate(Concat('##', T3.DocNo, T3.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("        And (" + Filter.Replace("Tbl.", "T3.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo And B.DNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On B.ItCode=E.ItCode");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocNo, B.ItCode;");

            //var cm = new MySqlCommand();
            //Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC());
            Sm.ShowDataInGrid(
                ref Grd6, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DNo", "ItCode", "ItName", "BatchNo", "Qty", 
                    
                    //6-8
                    "PlanningUomCode", "Qty2", "PlanningUomCode2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Grd.Cells[Row, 7].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Grd.Cells[Row, 11].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 6, 7, 8, 10, 11, 12 });
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ComputeItemProcessed()
        {
            decimal Qty = 0m, Qty2 = 0m;
            string key = string.Empty;
            if (Grd6.Rows.Count > 1)
            {
                for (int Row6 = 0; Row6 < Grd6.Rows.Count; Row6++)
                {
                    if (Sm.GetGrdStr(Grd6, Row6, 1).Length != 0)
                    {
                        key = Sm.GetGrdStr(Grd6, Row6, 1)+Sm.GetGrdStr(Grd6, Row6, 2);
                        Qty = 0m;
                        Qty2 = 0m;
                        for (int Row1 = 0; Row1 < Grd1.Rows.Count; Row1++)
                        {
                            if (
                                Sm.GetGrdStr(Grd1, Row1, 2).Length != 0 &&
                                Sm.CompareStr(key, Sm.GetGrdStr(Grd1, Row1, 2)+Sm.GetGrdStr(Grd1, Row1, 3)) 
                                )
                            {
                                Qty += Sm.GetGrdDec(Grd1, Row1, 14);
                                Qty2 += Sm.GetGrdDec(Grd1, Row1, 16);
                            }
                        }
                        Grd6.Cells[Row6, 7].Value = Qty;
                        Grd6.Cells[Row6, 8].Value = Sm.GetGrdDec(Grd6, Row6, 6) - Qty;
                        Grd6.Cells[Row6, 11].Value = Qty2;
                        Grd6.Cells[Row6, 12].Value = Sm.GetGrdDec(Grd6, Row6, 10) - Qty2;
                    }
                }
            }
        }

        private void ShowNonCoordinatorPenalty()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, Filter2 = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(DocNo=@DocNo" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No, Sm.GetGrdStr(Grd1, Row, 2));
                    }
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                    {
                        if (Filter2.Length > 0) Filter2 += " Or ";
                        Filter2 += "(BomCode=@BomCode" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@BomCode" + No, Sm.GetGrdStr(Grd1, Row, 6));
                    }
                    No += 1;
                }
            }

            if (Filter.Length == 0) Filter = " 0=0 ";
            if (Filter2.Length == 0) Filter2 = " 0=0 ";

            SQL.AppendLine("Select Distinct A.EmpCode, B.EmpName, C.PosName, D.DeptName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct BomCode As EmpCode ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where BomType='3' ");
            SQL.AppendLine("    And CoordinatorInd='N' ");
            SQL.AppendLine("    And ("+Filter+") ");
            SQL.AppendLine("    And ("+Filter2+") ");
            //SQL.AppendLine("    And Locate(Concat('##', BomCode, '##'), @GetSelectedEmployee)>0 ");
            //SQL.AppendLine("    And Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Order By D.DeptName, B.EmpName, A.EmpCode;");

            //var cm = new MySqlCommand();
            //Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC2());
            //Sm.CmParam<String>(ref cm, "@GetSelectedEmployee", GetSelectedEmployee());
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "EmpCode",

                               //1-3
                               "EmpName", "PosName", "DeptName"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 5 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowCoordinatorPenalty()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(DocNo=@DocNo" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No, Sm.GetGrdStr(Grd1, Row, 2));
                    }
                    No += 1;
                }
            }

            if (Filter.Length == 0) Filter = " 0=0 ";

            SQL.AppendLine("Select Distinct A.EmpCode, B.EmpName, C.PosName, D.DeptName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct BomCode As EmpCode ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where BomType='3' ");
            SQL.AppendLine("    And CoordinatorInd='Y' ");
            //SQL.AppendLine("    And Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    And ("+Filter+") ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Order By D.DeptName, B.EmpName, A.EmpCode;");

            //var cm = new MySqlCommand();
            //Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC2());
            Sm.ShowDataInGrid(
                    ref Grd5, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "EmpCode",

                               //1-3
                               "EmpName", "PosName", "DeptName"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 5 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ComputeNonCoordinatorPenalty()
        {
            decimal Penalty = 0m;
            string key = string.Empty;
            if (Grd4.Rows.Count > 1)
            {
                for (int Row4 = 0; Row4 < Grd4.Rows.Count; Row4++)
                {
                    if (Sm.GetGrdStr(Grd4, Row4, 1).Length != 0)
                    {
                        key = Sm.GetGrdStr(Grd4, Row4, 1);
                        Penalty = 0m;
                        for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                        {
                            if (
                                Sm.GetGrdStr(Grd2, Row2, 4).Length != 0 &&
                                Sm.CompareStr(key, Sm.GetGrdStr(Grd2, Row2, 4))
                                )
                                Penalty = Penalty + Sm.GetGrdDec(Grd2, Row2, 12);
                        }
                        Grd4.Cells[Row4, 5].Value = Penalty;
                    }
                }
            }
        }

        private void ComputeCoordinatorPenalty()
        {
            decimal Penalty = 0m;
            string key = string.Empty;

            if (Grd5.Rows.Count > 1)
            {
                for (int Row5 = 0; Row5 < Grd5.Rows.Count; Row5++)
                {
                    if (Sm.GetGrdStr(Grd5, Row5, 1).Length != 0)
                    {
                        key = Sm.GetGrdStr(Grd5, Row5, 1);
                        Penalty = 0m;
                        
                        for (int Row3 = 0; Row3 < Grd3.Rows.Count; Row3++)
                            if (Sm.CompareStr(key, Sm.GetGrdStr(Grd3, Row3, 4)))
                                Penalty += Sm.GetGrdDec(Grd3, Row3, 13);
                        
                        Grd5.Cells[Row5, 5].Value = Penalty;
                    }
                }
            }
        }

        private void ShowCoordinatorSCF()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<Decimal>(ref cm, "@HolidayWagesIndex", mHolidayWagesIndex);

            if (Grd1.Rows.Count != 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(Tbl.DocNo=@DocNo" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No, Sm.GetGrdStr(Grd1, Row, 2));
                    }
                    No += 1;
                }
            }

            if (Filter.Length != 0) Filter = " Where (" + Filter + ")";

            #region Old Code

            //SQL.AppendLine("Select Distinct A.DocDt, A.ProductionShiftCode, I.ProductionShiftName, ");
            //SQL.AppendLine("B.BomCode As EmpCode, C.EmpName, D.PosName, E.DeptName, F.GrdLvlName, ");
            //SQL.AppendLine("IfNull(G.GrdLvlIndexP, 1) As GrdLvlIndex, ");
            //SQL.AppendLine("H.HolName, if(IfNull(H.HolName, '')='', 1, @HolidayWagesIndex) As HolidayPenaltyIndex, ");
            //SQL.AppendLine("IfNull(J.NoOfEmployee, 0) As NoOfEmployee ");
            //SQL.AppendLine("From TblShopFloorControlHdr A ");
            //SQL.AppendLine("Inner Join TblShopFloorControl2Dtl B ");
            //SQL.AppendLine("    On B.BomType='3' ");
            //SQL.AppendLine("    And B.CoordinatorInd='Y' ");
            //SQL.AppendLine("    And A.DocNo=B.DocNo ");
            //SQL.AppendLine("Inner Join TblEmployee C On B.BomCode=C.EmpCode ");
            //SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            //SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            //SQL.AppendLine("Left Join TblGradeLevelHdr F On C.GrdLvlCode=F.GrdLvlCode ");
            //SQL.AppendLine("Left Join TblWagesFormulationDtl2 G ");
            //SQL.AppendLine("    On C.GrdLvlCode=G.GrdLvlCode ");
            //SQL.AppendLine("    And G.WagesFormulationCode=@WagesFormulationCode ");
            //SQL.AppendLine("Left Join TblHoliday H On A.DocDt=H.HolDt ");
            //SQL.AppendLine("Left Join TblProductionShift I On A.ProductionShiftCode=I.ProductionShiftCode ");
            //SQL.AppendLine("Inner Join ( ");
            //SQL.AppendLine("    Select DocDt, ProductionShiftCode, Count(EmpCode) As NoOfEmployee ");
            //SQL.AppendLine("    From ( ");
            //SQL.AppendLine("        Select Distinct T1.DocDt, T1.ProductionShiftCode, T2.BomCode As EmpCode ");
            //SQL.AppendLine("        From TblShopFloorControlHdr T1 ");
            //SQL.AppendLine("        Inner Join TblShopFloorControl2Dtl T2 ");
            //SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("            And T2.BomType='3' ");
            //SQL.AppendLine("            And T2.CoordinatorInd='N' ");
            //SQL.AppendLine("        Where (" + Filter.Replace("Tbl.", "T1.") + ") ");
            //SQL.AppendLine("    ) T ");
            //SQL.AppendLine("    Group By DocDt, ProductionShiftCode ");
            //SQL.AppendLine(") J On A.DocDt=J.DocDt And A.ProductionShiftCode=J.ProductionShiftCode ");
            //SQL.AppendLine("Where (" + Filter.Replace("Tbl.", "A.") + ") ");
            //SQL.AppendLine("Order By A.DocNo, E.DeptName, C.EmpName, B.BomCode;");

            #endregion

            SQL.AppendLine("Select A.DocDt, A.ProductionShiftCode, C.ProductionShiftName, ");
            SQL.AppendLine("A.EmpCode, D.EmpName, E.PosName, F.DeptName, ");
            SQL.AppendLine("G.GrdLvlName, IfNull(H.GrdLvlIndexP, 1) As GrdLvlIndex, ");
            SQL.AppendLine("B.HolName, if(IfNull(B.HolName, '')='', 1, @HolidayWagesIndex) As HolidayPenaltyIndex, ");
            SQL.AppendLine("IfNull(I.NoOfEmployee, 0) As NoOfEmployee, A.GrdLvlCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.DocDt, T.ProductionShiftCode, T.EmpCode, ");
            SQL.AppendLine("    (");
            SQL.AppendLine("        Select GrdLvlCode From TblEmployeePPS ");
            SQL.AppendLine("        Where EmpCode=T.EmpCode ");
            SQL.AppendLine("        And StartDt<=T.DocDt And (EndDt Is Null Or EndDt>=T.DocDt) ");
            SQL.AppendLine("        Order By StartDt Desc Limit 1 "); //sepertinya tidak perlu
            SQL.AppendLine("    ) As GrdLvlCode ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct T1.DocDt, T1.ProductionShiftCode, T2.BomCode As EmpCode ");
            SQL.AppendLine("        From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("        Inner Join TblShopFloorControl2Dtl T2 ");
            SQL.AppendLine("            On T2.BomType='3' ");
            SQL.AppendLine("            And T2.CoordinatorInd='Y' ");
            SQL.AppendLine("            And T1.DocNo=T2.DocNo ");
            SQL.AppendLine(Filter.Replace("Tbl.", "T1."));
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblHoliday B On A.DocDt=B.HolDt ");
            SQL.AppendLine("Left Join TblProductionShift C On A.ProductionShiftCode=C.ProductionShiftCode ");
            SQL.AppendLine("Inner Join TblEmployee D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            SQL.AppendLine("Left Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr G On A.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 H ");
            SQL.AppendLine("    On A.GrdLvlCode=H.GrdLvlCode ");
            SQL.AppendLine("    And H.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.DocDt, T.ProductionShiftCode, Count(T.EmpCode) As NoOfEmployee ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct T1.DocDt, T1.ProductionShiftCode, T2.BomCode As EmpCode ");
            SQL.AppendLine("        From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("        Inner Join TblShopFloorControl2Dtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And T2.BomType='3' ");
            SQL.AppendLine("            And T2.CoordinatorInd='N' ");
            SQL.AppendLine(Filter.Replace("Tbl.", "T1."));
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.DocDt, T.ProductionShiftCode ");
            SQL.AppendLine(") I On A.DocDt=I.DocDt And A.ProductionShiftCode=I.ProductionShiftCode ");
            SQL.AppendLine("Order By A.DocDt, A.ProductionShiftCode, D.EmpName, A.EmpCode;");

            Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                       //0
                       "DocDt",

                       //1-5
                       "ProductionShiftCode", "ProductionShiftName", "EmpCode", "EmpName", "PosName", 
                       
                       //6-10
                       "DeptName", "GrdLvlName", "GrdLvlIndex", "HolName", "HolidayPenaltyIndex",

                       //11-12
                       "NoOfEmployee", "GrdLvlCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 10, 13, 15, 16 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 9, 10, 12, 13, 14, 15, 16 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ComputeCoordinatorSCFPenalty(string DocDt, string ProductionShiftCode)
        {
            decimal Amt = 0m, Penalty = 0m, EmpCount = 0m, EmpCount2 = 0m, GrdLvlIndex = 0m, HolidayIndex = 1m;
            string EmpCode = string.Empty;
            bool IsValid = false;

            if (Grd3.Rows.Count > 1)
            {
                for (int Row3 = 0; Row3 < Grd3.Rows.Count; Row3++)
                {
                    if (
                        Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd3, Row3, 1)) &&
                        Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd3, Row3, 2)) 
                        )
                    {
                        Penalty = 0m;
                        Amt = 0m;
                        EmpCount = 0m;
                        EmpCount2 = 0m;
                        GrdLvlIndex = 0m;
                        HolidayIndex = 1m;

                        for (int Row4 = 0; Row4 < Grd4.Rows.Count; Row4++)
                        {
                            if (Sm.GetGrdStr(Grd4, Row4, 1).Length > 0)
                            {
                                EmpCode = Sm.GetGrdStr(Grd4, Row4, 1);
                                IsValid = false;
                                for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                                {
                                    if (
                                        Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd2, Row2, 4)) &&
                                        Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd2, Row2, 1)) &&
                                        Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd2, Row2, 2))
                                        )
                                    {
                                        if (!IsValid)
                                        {
                                            EmpCount2 += 1;
                                            IsValid = true;
                                        }
                                        Amt += Sm.GetGrdDec(Grd2, Row2, 12);
                                    }
                                }
                            }
                        }

                        if (Sm.GetGrdStr(Grd3, Row3, 9).Length > 0) GrdLvlIndex = Sm.GetGrdDec(Grd3, Row3, 9);
                        if (Sm.GetGrdStr(Grd3, Row3, 14).Length > 0) EmpCount = Sm.GetGrdDec(Grd3, Row3, 14);
                        if (Sm.GetGrdStr(Grd3, Row3, 12).Length > 0) HolidayIndex = Sm.GetGrdDec(Grd3, Row3, 12);

                        if (EmpCount != 0) Penalty = (Amt / EmpCount) * GrdLvlIndex;

                        Grd3.Cells[Row3, 10].Value = Penalty;
                        Grd3.Cells[Row3, 13].Value = Penalty * HolidayIndex;
                        Grd3.Cells[Row3, 15].Value = EmpCount2;
                        Grd3.Cells[Row3, 16].Value = Amt;
                    }
                }
            }
        }

        private void ComputeCoordinatorSCFPenalty()
        {
            decimal Amt = 0m, Penalty = 0m, EmpCount = 0m, EmpCount2 = 0m, GrdLvlIndex = 0m, HolidayIndex = 1m;
            string EmpCode = string.Empty;
            bool IsValid = false;

            if (Grd3.Rows.Count > 1)
            {
                for (int Row3 = 0; Row3 < Grd3.Rows.Count; Row3++)
                {
                    if (Sm.GetGrdStr(Grd3, Row3, 1).Length>0)
                    {
                        Penalty = 0m;
                        Amt = 0m;
                        EmpCount = 0m;
                        EmpCount2 = 0m;
                        GrdLvlIndex = 0m;
                        HolidayIndex = 1m;

                        for (int Row4 = 0; Row4 < Grd4.Rows.Count; Row4++)
                        {
                            if (Sm.GetGrdStr(Grd4, Row4, 1).Length > 0)
                            {
                                EmpCode = Sm.GetGrdStr(Grd4, Row4, 1);
                                IsValid = false;
                                for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                                {
                                    if (
                                        Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd2, Row2, 4)) &&
                                        Sm.CompareStr(Sm.GetGrdDate(Grd3, Row3, 1), Sm.GetGrdDate(Grd2, Row2, 1)) &&
                                        Sm.CompareStr(Sm.GetGrdStr(Grd3, Row3, 2), Sm.GetGrdStr(Grd2, Row2, 2))
                                        )
                                    {
                                        if (!IsValid)
                                        {
                                            EmpCount2 += 1;
                                            IsValid = true;
                                        }
                                        Amt += Sm.GetGrdDec(Grd2, Row2, 12);
                                    }
                                }
                            }
                        }

                        if (Sm.GetGrdStr(Grd3, Row3, 9).Length > 0) GrdLvlIndex = Sm.GetGrdDec(Grd3, Row3, 9);
                        if (Sm.GetGrdStr(Grd3, Row3, 14).Length > 0) EmpCount = Sm.GetGrdDec(Grd3, Row3, 14);
                        if (Sm.GetGrdStr(Grd3, Row3, 12).Length > 0) HolidayIndex = Sm.GetGrdDec(Grd3, Row3, 12);

                        if (EmpCount != 0) Penalty = (Amt / EmpCount) * GrdLvlIndex;

                        Grd3.Cells[Row3, 10].Value = Penalty;
                        Grd3.Cells[Row3, 13].Value = Penalty * HolidayIndex;
                        Grd3.Cells[Row3, 15].Value = EmpCount2;
                        Grd3.Cells[Row3, 16].Value = Amt;
                    }
                }
            }
        }

        private void SetWagesFormulationCode()
        {
            LueWagesFormulationCode.EditValue = null;

            try
            {
                string WorkCenterDocNo = Sm.GetLue(LueWorkCenterDocNo);

                if (WorkCenterDocNo.Length > 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select WagesFormulationCode ");
                    SQL.AppendLine("From TblWorkCenterWagesFormulation ");
                    SQL.AppendLine("Where WorkCenterCode=@WorkCenterDocNo ");
                    SQL.AppendLine("Order By Date Desc Limit 1;");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", WorkCenterDocNo);

                    string WagesFormulationCode = Sm.GetValue(cm);
                    if (WagesFormulationCode.Length > 0)
                        Sm.SetLue(LueWagesFormulationCode, WagesFormulationCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
                Grd3.Font = TheFont;
                Grd4.Font = TheFont;
                Grd5.Font = TheFont;
                Grd6.Font = TheFont;
            }
        }

        private void LueWorkCenterDocNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWorkCenterDocNo, new Sm.RefreshLue2(Sl.SetLueWorkCenterDocNo), string.Empty);
            SetWagesFormulationCode();
            ClearGrd();
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueWagesFormulationCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWagesFormulationCode, new Sm.RefreshLue1(Sl.SetLueWagesFormulationCode));
            ShowWagesFormulationUom();
            ClearGrd();
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (
                BtnSave.Enabled &&
                TxtDocNo.Text.Length == 0 &&
                e.ColIndex == 1 &&
                !Sm.IsLueEmpty(LueWorkCenterDocNo, "Work Center") &&
                !Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation")
                )
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmPNTDlg(
                        this, Sm.GetLue(LueWorkCenterDocNo), mHolidayWagesIndex, mNotHolidayWagesIndex 
                        ));
            }

            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (!(
                    Sm.IsGrdColSelected(new int[] { 14, 16 }, e.ColIndex) ||
                    (e.ColIndex == 18 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length > 0)
                   ))
                    e.DoDefault = false;
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length==0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete) ShowDocInfo();
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled &&
                TxtDocNo.Text.Length == 0 &&
                e.ColIndex == 1 &&
                !Sm.IsLueEmpty(LueWorkCenterDocNo, "Work Center") &&
                !Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation")
                )
                Sm.FormShowDialog(new FrmPNTDlg(this, Sm.GetLue(LueWorkCenterDocNo), mHolidayWagesIndex, mNotHolidayWagesIndex));
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 14, 16 }, e);

                if (e.ColIndex == 14)
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 10, 14, 16, 15, 17);

                if (e.ColIndex == 16)
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 10, 16, 14, 17, 15);

                if (e.ColIndex == 14 || e.ColIndex == 16 || (e.ColIndex == 18 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length > 0))
                {
                    if (e.ColIndex == 18)
                        Grd1.Cells[e.RowIndex, 19].Value =
                            Sm.GetGrdBool(Grd1, e.RowIndex, 18) ?
                                mHolidayWagesIndex : mNotHolidayWagesIndex;
                    ComputeValue(e.RowIndex);
                    ComputeItemProcessed();
                    ComputePenaltyBasedOnFormula(
                        Sm.GetGrdDate(Grd1, e.RowIndex, 4),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 22),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 6)
                        );
                    ComputeCoordinatorSCFPenalty(                       
                        Sm.GetGrdDate(Grd1, e.RowIndex, 4),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 22)
                        );
                    ComputeNonCoordinatorPenalty();
                    ComputeCoordinatorPenalty();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion
    }
}
