﻿#region Update

#region before 2023
/*
    13/04/2017 [WED] tambahan GetSQLApprovalNotify() untuk Receiving Item From Other Warehouse (Without DO).
    26/04/2017 [WED] tambahan GetSQLApprovalNotify() untuk Make To Stock.
    08/05/2017 [TKG] tambah journal untuk POS.
    17/05/2017 [WED] tambah GetSQLApprovalNotify() untuk Voucher Request For Tax.
    29/05/2017 [WED] tambah GetSQLApprovalNotify() untuk Voucher Request PPN.
    08/06/2017 [TKG] POS VAT (manual).
    04/08/2017 [TKG] Tambah PosNo di proses dayend closing POS.
    07/08/2017 [TKG] POS Online.
    14/08/2017 [TKG] Pos retur.
    14/08/2017 [TKG] Pos retur.
    09/09/2017 [TKG] Pos retur dalam 1 business date.
    11/09/2017 [TKG] bug fixing apabila tidak ada pos retur.
    28/09/2017 [TKG] tambah pos number di nomor dokumen saat masuk ke stok
    10/10/2017 [HAR] Travel Request
    13/10/2017 [TKG] no rekening journal untuk pos diambil dari pos setting
    24/10/2017 [HAR] document contract notify berdasarkan user yang didaftarkan di user notify
                     background image customer (Main_load)
    25/10/2017 [HAR] contract notify validasi resigndate di ubah
    14/10/2017 [HAR] background image customer berdasarkan param DocTiTle
    28/11/2017 [WED] notifikasi approval Annual Leave Allowance
    29/11/2017 [HAR] notifikasi approval Employee Severance
    08/12/2017 [HAR] notifikasi approval Employee Reward
    15/02/2018 [WED] notifikasi approval Return ARDownpayment
    15/02/2018 [WED] notifikasi approval Service Charge Deduction
    14/04/2018 [TKG] menggunakan format khusus voucher atau format standard dokumen run system berdasarkan parameter VoucherCodeFormatType
    04/05/2018 [TKG] notifikasi wo request
    09/05/2018 [TKG] ubah setting notifikasi approval travel request menjadi bisa per department.
    29/05/2018 [TKG] notifikasi PO
    24/07/2018 [WED] notifikasi approval Request for Loan to Partner
    25/07/2018 [WED] notifikasi approval Survey
    06/08/2018 [WED] notifikasi approval Partner's Loan Payment
    10/08/2018 [WED] notifikasi approval DO To Customer
    14/08/2018 [WED] notifikasi approval BOQ
    14/08/2018 [WED] notifikasi approval LOP
    21/08/2018 [WED] notifikasi approval ProjectImplementation
    27/08/2018 [TKG] journal pos
    28/08/2018 [HAR] notifikasi approval Training Request
    03/09/2018 [HAR] validasi training request kurang validasi site dan departmment employee claim notif belum dibuat
    06/09/2018 [WED] notifikasi approval Bonus
    31/09/2018 [TKG] tambah validasi site di approval LOP dan RLP
    12/11/2018 [WED] notifikasi approval SO Contract Revision
    03/12/2018 [TKG] ubah event click supaya tidak lama saat lihat2 menu
    11/01/2019 [HAR] notifikasi approval ProjectImplementation tambah validasi hanya yang processind = 'F' ngikut doc approval
    29/05/2019 [TKG] journal pos tambah cost center
    14/08/2019 [DITA] tambah notif approval count untuk Dropping Request, Budget Request, Budget
    23/09/2019 [DITA/IMS] tambah notif approval count untuk Notice To Proceed
    24/09/2019 [WED/YK] tambah notif approval count untuk Dropping Payment
    03/10/2019 [WED/YK] tambah notif approval count untuk Voucher Request External Payroll
    10/10/2019 [WED/IMS] notif employee contract berdasarkan parameter EmpEndContractBenchmark dan GrpCodeForEmpEndContractNotification
    14/10/2019 [WED/TWC] notif untuk MR dan POR masih belom bener
    16/10/2019 [TKG/YK] notification untuk Cash Advance Settlement
    21/10/2019 [WED/MAI] notifikasi Material Request Exim belom bener di Department dan Site nya
    23/10/2019 [WED/YK] Approval Project implementation Revision
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
    23/12/2019 [WED/YK] approval Project Delivery
    03/01/2020 [TKG/TWC] Berdasarkan parameter IsPOSUpdateStockEnabled Pada saat day end closing, tidak akan mengupdate data2 stock (Movement/Summary).
    15/01/2020 [WED/SIER] approval Asset Transfer
    15/01/2020 [WED/SIER] approval Stock Inbound
    15/01/2020 [WED/SIER] approval Stock Outbound
    05/03/2020 [WED/SIER] parameter IsStockInboundApprovalBasedOnWhs & IsStockOutboundApprovalBasedOnWhs
    01/04/2020 [DITA/ALL] tambah self on boarding
    29/04/2020 [IBL/KSM] tambah GetSQLApprovalNotify() untuk Sales Memo.
    28/05/2020 [DITA/Runmarket] jika runmarket --> background diganti putih
    07/06/2020 [TKG/IMS] Bug SelfOnBoarding
    09/06/2020 [DITA/IMS] self on boarding bisa menumpuk background
    29/06/2020 [DITA/IMS] tambah notif approval count untuk Drawing Approval
    04/08/2020 [IBL/ALL] self on boarding tambah panel News dan widget
    31/08/2020 [IBL/IOK] Notifikasi approval tidak muncul
    08/09/2020 [TKG/MSI] ubah proses pos retur
    17/09/2020 [WED/SRN] leave melihat docapprovalsetting kolom LeaveCode, berdasarkan parameter IsDocApprovalSettingUseLeaveCode
    26/10/2020 [ICA/IMS] Notifikasi SO Contract yang belum di BOM kan
    05/11/2020 [VIN/PHT] Notifikasi karyawan yang akan pensiun tahun depan
    26/11/2020 [TKG/IOK] bug notifikasi label status 
    02/12/2020 [DITA/IMS] tambah notif approval count untuk Project Budget Resource
    07/12/2020 [WED/IMS] tambah notif approval count untuk BOM3
    05/01/2021 [IBL/IMS] tambah notif count cancel approval document
    12/01/2021 [DITA/PHT] tampil page change password ketika usercode dan password sama
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    03/03/2021 [ICA/PHT] informasi Employee Pre Pension dibuat 6 bulan 
    05/03/2021 [ICA/PHT] Menambah notifikasi merit increase
    19/03/2021 [ICA/PHT] informasi employee Pre Pension dibuat berdasarkan tanggal pension (bukan resigndt)
    05/04/2021 [ICA/PHT] informasi employee pre pension, tanggal pension berdasarkan param SSRetiredMaxAge2
                         informasi employee pre pension untuk employee di menu Pre Pension Notification Setting
    30/09/2021 [BRI/ALL] tambah notifikasi Journal Memorial
    08/10/2021 [ICA/PHT] tambah GetSQLApprovalNotify() CBP Profit Loss Revision
    25/10/2021 [TKG/TWC] untuk format vr+voucher di pos berdasarkan docseqno
    25/10/2021 [TKG/TWC] menggunakan parameter IsVoucherDocSeqNoEnabled saat membuat Voucher Request#+Voucher#
    21/12/2021 [MYA/ALL] Adanya Approval setelah mengisi data pada menu KPI
    22/03/2022 [VIN/YK] BUG: ShowCancelledDocCount
    12/05/2022 [TYO/HIN] Adanya approval setelah mengisi data pada menu Goals Setting
    19/05/2022 [TYO/HIN] BUG : Adanya approval setelah mengisi data pada menu Goals Setting
    23/05/2022 [MYA/HIN] Menambahkan type document approval menu baru untuk approval menu  Performance Review
    24/05/2022 [ICA/TWC] Bug mengubah muncul warning tidak ada actind, seharusnya cancelInd di notify docapproval NewPerformanceReview
    07/06/2022 [MYA/PHT] Setting background resolution
    27/07/2022 [IBL/SIER] tambahan GetSQLApprovalNotify() untuk Purchase Invoice.
    26/10/2022 [MAU/PRODUCT] tambah GetSQLApprovalNotify() untuk VR Petty Cash
    08/11/2022 [IBL/SIER] tambahan GetSQLApprovalNotify() untuk petty cash disbursement
    09/11/2022 [RDA/SIER] mempercepat load saat login
    15/11/2022 [RDA/TWC] bug perubahan bentuk pemanggilan param (untuk mempercepat load)
    15/11/2022 [RDA/TWC] typo pemanggilan param
    29/12/2022 [ICA/MNET] tambah notofy docapproval net off payment
    29/12/2022 [DITA/BBT] tambah notify docapproval property inventory
 */
#endregion

/*    
    04/01/2023 [DITA/BBT] tambah notify docapproval property inventory cost component
    09/01/2023 [IBL/BBT] tambah notify docapproval property inventory mutation
    10/01/2023 [SET/BBT] tambah notify docapproval property inventory transfer
    12/01/2023 [VIN/ALL] BUG kurang union all saat reccount
    08/02/2023 [WED/MNET] tambah notify docapproval Service Delivery
    09/02/2023 [IBL/BBT] tambah notify docapproval Yearly Closing Journal Entry
    15/02/2023 [WED/HEX] tambah notify docapproval Budget Transfer
    17/02/2023 [WED/PHT] tambah param baru untuk R1
    04/03/2023 [WED/ALL] tambah panjang karakter SysVer di TblLog untuk persiapan versioning dengan Semantic Version 2.0
    17/03/2023 [HAR/PHT] memanggil variabel IsPwdAlreadyExpired untuk membuka form change password
    30/03/2023 [WED/PHT] tambah query alter dan insert baru untuk VR dan VC
    06/04/2023 [HAR/PHT] perubahan saat ShowFormChangePassword 
    06/04/2023 [SET/MNET] penyesuaian length data qty
    11/04/2023 [HAR/TWC] tambahan parameter IsFormMainShowChangePwd, di function FrmMain_Load
    17/04/2023 [WED/ALL] tambah parameter IsUsePreparedStatement
    20/04/2023 [WED/ALL] tambah parameter IsActivityLogStored
*/
#endregion

#region namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Text;
using MySql.Data.MySqlClient;
using System.Drawing.Printing;
using System.Net;
using System.IO;
//using System.Threading.Tasks;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Wa = RunSystem.WinAPI;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmMain : Form
    {
        #region Field

        public string UserName = string.Empty;
        public ArrayList ListOfEmlNo = new ArrayList();

        #region POS

        private PrintDocument pdoc = null;
        private string CurrentShift = string.Empty;
        private string CurrentBsDate = string.Empty;
        private bool mIsAutoJournalActived = false;

        #endregion

        private bool
            mPORequestApprovalForAllDept = false,
            mIsApprovalNotify = false,
            mIsOutstandingCancellationPONotify = false,
            mIsPORevisionApprovalUseDept = false,
            mIsApprovalBySiteMandatory = false,
            mIsAPDownpaymentApprovalBasedOnDept = false,
            mIsRecvVdApprovalBasedOnWhs = false,
            mIsEmpContractDocNotify = false,
            mIsFilterByDept = false,
            mIsPPSApprovalBasedOnDept = false,
            mIsRecvWhs2ApprovalBasedOnWhs = false,
            mIsVoucherRequestTaxApprovalBasedOnDept = false,
            mIsVoucherRequestPPNApprovalBasedOnDept = false,
            mIsReturnAPDownpaymentApprovalBasedOnDept = false,
            mIsReturnAPDownpaymentUseEntity = false,
            mIsReturnARDownpaymentApprovalBasedOnDept = false,
            mIsReturnARDownpaymentUseEntity = false,
            mIsAutoLogOffActived = false,
            mIsDOCtApprovalActived = false,
            mIsProjectImplementationApprovalBySiteMandatory = false,
            mIsPOSEnabled = false,
            mIsProductionOrderNotifyEnabled = false,
            mLockInd = false,
            mIsFilterBySite = false,
            mIsMaterialRequestApprovalByDeptEnabled = false,
            mIsUseSelfOnBoarding = false,
            mIsShow = true,
            mIsDocApprovalSettingUseLeaveCode = false,
            mIsOutstandingSOContractNotify = false,
            mUserCode = false,
            mIsPasswordForLoginNeedToEncode = false,
            mIsUserNeedToChangeDefaultPassword = false,
            mIsFormMainShowChangePwd = false
            ;

        private string
            mSQLProductionOrderNotify = string.Empty,
            mSQLApprovalNotify = string.Empty,
            mSQLOutstandingCancellationPONotify = string.Empty,
            mSQLEmpContractDocNotify = string.Empty,
            mMenuCodeForShiftClosing = string.Empty,
            mMenuCodeForDayEndClosing = string.Empty,
            mLockDt = string.Empty,
            mWarningMsg = string.Empty,
            mEmpEndContractBenchmark = string.Empty,
            mGrpCodeForEmpEndContractNotification = string.Empty,
            mAccessInd = string.Empty,
            mSelfOnboardingNewsLimit = string.Empty,
            mSelfOnboardingFontSize = string.Empty,
            mSQLOutstandingSOContractNotify = string.Empty,
            mUserCodeForSelfOnBoardingPrePensionNotification = string.Empty,
            mApprovalDocTypeForNotification = string.Empty, 
            mDocTitle = string.Empty
            ;

        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mUserPassword = string.Empty,
            mUserCodeEncode = string.Empty;


        private byte[] downloadedData;
        private byte[] image = null;

        #endregion

        #region Constructor

        public FrmMain()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Self On Boarding

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "","","","","",

                        //6
                        ""
                        
                    },
                    new int[] 
                    {
                        //0
                        200,

                        //1-5
                        50, 50, 70, 20, 0,
 
                        //6
                        0
                    }
                );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
        }

        protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<TempSelfOnBoarding>();
                var l2 = new List<SelfOnboarding>();

                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l, ref l2);
                    Process3(ref l2);
                }

                l.Clear(); l2.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private decimal GetValue(string Param, string Ind)
        {
            decimal mNumber = 0m;
            if (Ind == "N") mNumber = Decimal.Parse(Param);
            else mNumber = Decimal.Parse(Sm.GetValue(Param));
            return mNumber;
        }

        private void Process1(ref List<TempSelfOnBoarding> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT * FROM TblSelfOnBoarding ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "MenuCode",
                    //1-5
                    "Desc", "Param", "Query1", "Query1Ind", "Query2", 
                    //6
                    "Query2Ind"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TempSelfOnBoarding()
                        {
                            MenuCode = Sm.DrStr(dr, c[0]),
                            Desc = Sm.DrStr(dr, c[1]),
                            Param = Sm.DrStr(dr, c[2]),
                            Query1 = Sm.DrStr(dr, c[3]),
                            Query1Ind = Sm.DrStr(dr, c[4]),
                            Query2 = Sm.DrStr(dr, c[5]),
                            Query2Ind = Sm.DrStr(dr, c[6]),

                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<TempSelfOnBoarding> l, ref List<SelfOnboarding> l2)
        {
            foreach (var x in l)
            {
                l2.Add(new SelfOnboarding()
                {
                    Desc = x.Desc,
                    Param = x.Param,
                    MenuCode = x.MenuCode,
                    Target = x.Query1.Length == 0 ? 0m : GetValue(x.Query1, x.Query1Ind),
                    Existing = x.Query2.Length == 0 ? 0m : GetValue(x.Query2, x.Query2Ind),
                    Percentage = 0m,
                });
            }

            foreach (var y in l2)
            {
                if (y.Target != 0m) y.Percentage = (y.Existing / y.Target) * 100m;
            }
        }

        private void Process3(ref List<SelfOnboarding> l)
        {
            int Row = 0;
            foreach (var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Row, 0].Value = x.Desc;
                Grd1.Cells[Row, 1].Value = x.Target;
                Grd1.Cells[Row, 2].Value = x.Existing;
                Grd1.Cells[Row, 3].Value = Sm.FormatNum(x.Percentage, 0) + '%';
                Grd1.Cells[Row, 5].Value = x.MenuCode;
                Grd1.Cells[Row, 6].Value = x.Param;

                if (x.Percentage >= 90m)
                {
                    Grd1.Rows[Row].ForeColor = Color.Green;
                }
                else if (x.Percentage < 90m && x.Percentage > 40m)
                {
                    Grd1.Rows[Row].ForeColor = Color.Orange;
                }
                else if (x.Percentage <= 40m)
                {
                    Grd1.Rows[Row].ForeColor = Color.Red;
                }
                Row += 1;
            }
        }

        #region News Panel

        private void SetGrd2()
        {
            Grd2.Cols.Count = 4;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd2, new string[] 
                    {
                        //0
                        "",

                        //1-3
                        "", "", ""
                        
                    },
                    new int[] 
                    {
                        //0
                        0,

                        //1-3
                        370, 20, 0
                    }
                );
            Sm.GrdColReadOnly(true, false, Grd2, new int[] { 0, 1, 3 });
            Sm.GrdColInvisible(Grd2, new int[] { 0 });
            Sm.GrdColButton(Grd2, new int[] { 2 });
        }

        private void ShowNews()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Id, Title, Picture From TblSelfOnBoarding2 ");
            SQL.AppendLine("Order By CreateDt Desc ");
            SQL.AppendLine("Limit " + mSelfOnboardingNewsLimit);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "Id",

                    //1-2
                    "Title", "Picture"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
        }

        private string GetMenuCode(string FrmName)
        {
            return
                Sm.GetValue("Select MenuCode From TblMenu Where Param = @Param", FrmName);
        }

        private string GetAccessInd(string FormName)
        {
            return
                Sm.GetValue("Select Left(C.AccessInd, 1) " +
                            "From TblUser A, TblGroup B, TblGroupMenu C, TblMenu D " +
                            "Where A.GrpCode=B.GrpCode " +
                            "And B.GrpCode=C.GrpCode " +
                            "And C.MenuCode=D.MenuCode " +
                            "And D.StdInd='Y' " +
                            "And D.Visible='Y' " +
                            "And D.MenuCat='D' " +
                            "And D.Param=@Param1 " +
                            "And A.UserCode=@Param2 " +
                            "And 1=@Param3 ", FormName, Gv.CurrentUserCode, "1");
        }

        #endregion

        #region Employee Pre Pension

        private void SetGrd3()
        {
            Grd3.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(
                    Grd3, new string[] 
                    {
                        //0
                        "Employee",

                        //1
                        "" 
                        
                    },
                    new int[] 
                    {
                        //0
                        100,

                        //1
                        20
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0 });
            Sm.GrdFormatDec(Grd3, new int[] { 0 }, 0);
            Sm.GrdColButton(Grd3, new int[] { 1 });
        }

        private void ShowEmployeeCount()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT COUNT(A.empcode) Employee ");
            SQL.AppendLine("FROM tblemployee A ");
            if (mDocTitle == "PHT")
            {
                SQL.AppendLine("INNER JOIN ( ");
                SQL.AppendLine("    SELECT A.EmpCode, ");
                SQL.AppendLine("    case ");
                SQL.AppendLine("        when Right(A.BirthDt, 2)='01' then Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge2 Year, '%Y%m%d')");
                SQL.AppendLine("        ELSE concat(Date_Format(A.BirthDt + INTERVAL '1' MONTH + INTERVAL @SSRetiredMaxAge2 Year, '%Y%m'), '01') ");
                SQL.AppendLine("    END AS Pension ");
                SQL.AppendLine("    From tblemployee A ");
                SQL.AppendLine("    WHERE BirthDt IS NOT NULL ");
                SQL.AppendLine(") B ON A.EmpCode = B.EmpCode");
                SQL.AppendLine("WHERE Left(B.Pension, 4) = Year(INTERVAL 6 MONTH + CURRENT_TIMESTAMP) ");
                SQL.AppendLine("    AND SUBSTRING(B.Pension, 5, 2) = MONTH(INTERVAL 6 Month + CURRENT_TIMESTAMP) ");
                SQL.AppendLine("    AND (A.ResignDt IS NULL OR A.ResignDt > Left(CurrentDateTime(), 8)) ");
            }
            else
            {
                SQL.AppendLine("WHERE A.resigndt IS NOT NULL ");
                SQL.AppendLine("    AND (LEFT(A.resigndt, 4)) = (YEAR(CURRENT_TIMESTAMP))+1 ");
                SQL.AppendLine("    AND (SUBSTRING(A.resigndt, 5, 2)) = MONTH(CURRENT_TIMESTAMP) ");
            }

            Sm.CmParam<String>(ref cm, "@SSRetiredMaxAge2", Sm.GetValue("Select parvalue From TblParameter Where Parcode = 'SSRetiredMaxAge2'"));

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "Employee"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 0, 0);
                }, false, false, true, false
            );

            Grd3.Rows.RemoveAt(Grd3.Rows.Count - 1);
        }

        #endregion

        #region

        private void ShowMeritIncreaseNotification()
        {
            if (Sm.GetParameter("DocTitle") == "PHT")
            {
                bool mIsShowMeritIncreaseNotification = 
                Sm.IsDataExist("select 1 " +
                "from TblEmpMeritIncreaseNotifSetting A " +
                "Inner Join TblEmployee B On A.EmpCode = B.EmpCode And A.SiteCode = B.SiteCode " +
                "where B.UserCode = '" + Gv.CurrentUserCode +"' "+
                "   And (FIND_IN_SET(Substring(Currentdatetime(), 5, 2), (Select parvalue from tblparameter where parcode = 'MeritIncreaseNotificationPeriod1')) " +
                "   Or FIND_IN_SET(Substring(Currentdatetime(), 5, 2), (Select parvalue from tblparameter where parcode = 'MeritIncreaseNotificationPeriod2'))); ");

                if (mIsShowMeritIncreaseNotification) LblMeritIncreaseNotification.Text = "Please, fill the employee merit increace request.";
                else panel8.Hide();
            }
        }

        #endregion

        #region Cancelled Document

        private void SetGrd4()
        {
            Grd4.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(
                    Grd4, new string[] 
                    {
                        //0
                        "",

                        //1
                        "" 
                        
                    },
                    new int[] 
                    {
                        //0
                        365,

                        //1
                        20
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0 });
            Sm.GrdColButton(Grd4, new int[] { 1 });
        }

        private void ShowCancelledDocCount()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(IfNull(Count(*), 0), ' Cancelled Document.') As CountDocument ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select DocNo ");
	        SQL.AppendLine("    From TblDocApproval ");
	        SQL.AppendLine("    Where UserCode Is Not Null ");
	        SQL.AppendLine("    And Status = 'C' ");
	        SQL.AppendLine("    And Find_In_Set (DocType, @DocType) ");
	        SQL.AppendLine("    And CreateBy = @UserCode ");
	        SQL.AppendLine("    Group By DocNo ");
            SQL.AppendLine(") T; ");
            
            Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<string>(ref cm, "@DocType", mApprovalDocTypeForNotification);

            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "CountDocument"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                }, false, false, true, false
            );

            Grd4.Rows.RemoveAt(Grd4.Rows.Count - 1);
        }

        #endregion

        #region Merit Increase

        private void ShowNotifMeritIncrease()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(IfNull(Count(*), 0), ' Cancelled Document.') As CountDocument ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select * ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where UserCode Is Not Null ");
            SQL.AppendLine("    And Status = 'C' ");
            SQL.AppendLine("    And Find_In_Set (DocType, @DocType) ");
            SQL.AppendLine("    And CreateBy = @UserCode ");
            SQL.AppendLine("    Group By DocNo ");
            SQL.AppendLine(") T; ");
        }

        #endregion

        #region Widget Panel

        protected void ShowWidget()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<TempSelfOnBoarding3>();
                var l2 = new List<SelfOnBoarding3>();

                Process_1(ref l);
                if (l.Count > 0)
                {
                    Process_2(ref l, ref l2);
                    Process_3(ref l2);
                }

                l.Clear(); l2.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process_1(ref List<TempSelfOnBoarding3> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select * ");
            SQL.AppendLine("From TblSelfOnboarding3 ");
            SQL.AppendLine("Where GrpCode Is Not Null ");
            SQL.AppendLine("And Find_In_Set ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    (Select GrpCode From TblUser Where UserCode = @UserCode) ");
            SQL.AppendLine("    , GrpCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By CreateDt Desc ");
            SQL.AppendLine("Limit 2; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "Id",
                    
                    //1-3
                    "Desc", "Query", "QueryInd",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TempSelfOnBoarding3()
                        {
                            Id = Sm.DrStr(dr, c[0]),
                            Desc = Sm.DrStr(dr, c[1]),
                            Query = Sm.DrStr(dr, c[2]),
                            QueryInd = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process_2(ref List<TempSelfOnBoarding3> l, ref List<SelfOnBoarding3> l2)
        {
            foreach (var x in l)
            {
                l2.Add(new SelfOnBoarding3()
                {
                    Id = x.Id,
                    Desc = x.Desc,
                    Number = x.Query.Length == 0 ? 0m : GetValue(x.Query, x.QueryInd),
                });
            }
        }

        private void Process_3(ref List<SelfOnBoarding3> l2)
        {
            #region old code 14/08/2020
            /*
                int x = 10;
                foreach (var y in l2)
                {
                    Panel panel = new Panel();
                    Label LblWidget = new Label();
                    RichTextBox RtbKet = new RichTextBox();

                    this.PnlWidget.Controls.Add(panel);
                    panel.Location = new System.Drawing.Point(9, x);
                    x += 87;
                    panel.Name = panel + y.Id.ToString();
                    panel.Size = new System.Drawing.Size(308, 75);
                    panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                    panel.SuspendLayout();

                    panel.Controls.Add(LblWidget);
                    LblWidget.AutoSize = true;
                    LblWidget.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    LblWidget.Location = new System.Drawing.Point(2, 14);
                    LblWidget.Name = LblWidget + y.Id.ToString();
                    LblWidget.Size = new System.Drawing.Size(40, 45);
                    LblWidget.TabIndex = 1;
                    LblWidget.Text = Sm.FormatNum(y.Number, 1).ToString();

                    panel.Controls.Add(RtbKet);
                    RtbKet.BorderStyle = System.Windows.Forms.BorderStyle.None;
                    RtbKet.Dock = System.Windows.Forms.DockStyle.Right;
                    RtbKet.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    RtbKet.Location = new System.Drawing.Point(154, 0);
                    RtbKet.Name = "RtbKet" + y.Id.ToString();
                    RtbKet.Size = new System.Drawing.Size(222, 73);
                    RtbKet.TabIndex = 3;
                    RtbKet.ReadOnly = true;
                    RtbKet.Text = y.Desc;
                }
                */
            #endregion

            foreach (var x in l2.Select((value, i) => new { i, value }))
            {
                var index = x.i;

                if (index == 0)
                {
                    LblWidget1.Text = x.value.Number.ToString();
                    RtbWidget1.Text = x.value.Desc;
                }
                else
                {
                    LblWidget2.Text = x.value.Number.ToString();
                    RtbWidget2.Text = x.value.Desc;
                }
            }

        }

        #endregion

        #region Grid Method
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4)
            {
                e.DoDefault = false;
                OpenForm(Sm.GetGrdStr(Grd1, e.RowIndex, 6), Sm.GetGrdStr(Grd1, e.RowIndex, 5));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4)
            {
                OpenForm(Sm.GetGrdStr(Grd1, e.RowIndex, 6), Sm.GetGrdStr(Grd1, e.RowIndex, 5));
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                if (image != null) image = null;
                if (Sm.GetGrdStr(Grd2, e.RowIndex, 3).Length > 0)
                    image = DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd2, e.RowIndex, 3), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);

                var f = new FrmSelfOnBoardingContent("xxx");
                f.Tag = "xxx";
                f.Text = "Self On Boarding Content";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mContentId = Sm.GetGrdStr(Grd2, e.RowIndex, 0);
                f.ImageByte = image;
                f.ShowDialog();
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                if (image != null) image = null;
                if (Sm.GetGrdStr(Grd2, e.RowIndex, 3).Length > 0)
                    image = DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd2, e.RowIndex, 3), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);

                var f = new FrmSelfOnBoardingContent("xxx");
                f.Tag = "xxx";
                f.Text = "Self On Boarding Content";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mContentId = Sm.GetGrdStr(Grd2, e.RowIndex, 0);
                f.ImageByte = image;
                f.ShowDialog();
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                var f = new FrmRptEmpPrePension("");
                f.Tag = "";
                f.Text = "Employee Pre-Pension Notification";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.SetMainGrd();
                f.SetMainSQL();
                f.ShowDialog();
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                var f = new FrmRptCancelledApproval("");
                f.Tag = "";
                f.Text = "Cancelled Document Notification";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }

        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                var f = new FrmRptCancelledApproval("");
                f.Tag = "";
                f.Text = "Cancelled Document Notification";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }

        }

        #endregion

        #endregion

        #region Auto Show FrmChangePassword

        private void ShowFormChangePassword(bool statusPwd)
        {
            var f = new FrmChangePassword("");
            f.Tag = "";
            f.Text = "Change Password";
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.mIsPwdAlreadyExpired = statusPwd;
            f.ShowDialog();
        }

        #endregion

        void FormOpenClick(object sender, EventArgs e)
        {
            try
            {
                PropertyInfo info = sender.GetType().GetProperty("Tag");
                object o = info.GetValue(sender, null);

                string FormName = string.Empty, MenuCode = string.Empty;
                int pointer = 0;

                pointer = o.ToString().IndexOf(";");

                if (pointer > 0) FormName = o.ToString().Substring(0, pointer);

                if (pointer >= 0) MenuCode = o.ToString().Substring(o.ToString().Length - (o.ToString().Length - pointer - 1), o.ToString().Length - pointer - 1);

                if (MenuCode.Length != 0)
                {
                    if (FormName.Length != 0)
                    {
                        if (mLockInd && FormName.Length > 3 && Sm.CompareStr(Sm.Right(FormName, 3), "***"))
                            Sm.StdMsg(mMsgType.Warning, mWarningMsg);
                        else
                        {
                            if (Gv.IsSaveAppUsageHistoryActivated) SaveAppUsageHistory(MenuCode);
                            //OpenForm(FormName.Replace("Temp", string.Empty).Replace("FrmRHA1", "FrmRHA"), MenuCode);
                            OpenForm(FormName.Replace("Temp", string.Empty), MenuCode);
                        }
                    }
                    else
                    {
                        if (Sm.Left(MenuCode, 2) == "05")
                        {
                            switch (MenuCode)
                            {
                                case "0501":
                                    this.LayoutMdi(MdiLayout.TileVertical);
                                    break;
                                case "0502":
                                    this.LayoutMdi(MdiLayout.TileHorizontal);
                                    break;
                                case "0503":
                                    this.LayoutMdi(MdiLayout.Cascade);
                                    break;
                            }
                        }
                        else
                        {
                            if (mIsPOSEnabled)
                            {
                                if (Sm.CompareStr(MenuCode, mMenuCodeForShiftClosing))
                                {
                                    ShiftClosing();
                                    return;
                                }
                                if (Sm.CompareStr(MenuCode, mMenuCodeForDayEndClosing))
                                {
                                    DayEndClosing();
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void OpenForm(string FormName, string MenuCode)
        {
            try
            {
                Assembly asm = Assembly.GetExecutingAssembly();
                Type FormType = asm.GetType(asm.GetName().Name + "." + FormName);
                if (FormType == null)
                    Sm.StdMsg(mMsgType.Warning, "Under Construction");
                else
                {
                    Form f = Activator.CreateInstance(
                            FormType, new object[] { MenuCode }
                        ) as Form;
                    f.Tag = MenuCode;
                    f.Text = Sm.GetValue(
                        "Select Concat(MenuCode ,'-', MenuDesc) From TblMenu " +
                        "Where StdInd='Y' And MenuCode=@Param Limit 1;", MenuCode);
                    f.MdiParent = this;
                    f.WindowState = FormWindowState.Normal;
                    f.Left = 0;
                    f.Top = 0;
                    f.BringToFront();
                    f.Show();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.Refresh();
        }

        private static void ExecQuery()
        {
            var SQL = new StringBuilder();

            #region create db

            //SQL.AppendLine("CREATE DATABASE IF NOT EXISTS `runsystemlog`; ");
            //SQL.AppendLine("USE `runsystemlog`; ");

            //SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tbllogactivity` ( ");
            //SQL.AppendLine("  `Id` varchar(255) NOT NULL DEFAULT uuid(), ");
            //SQL.AppendLine("  `CtCode` varchar(10) NOT NULL, ");
            //SQL.AppendLine("  `DbName` varchar(50) NOT NULL, ");
            //SQL.AppendLine("  `DbHost` varchar(255) NOT NULL, ");
            //SQL.AppendLine("  `UserCode` varchar(255) NOT NULL, ");
            //SQL.AppendLine("  `MenuCode` varchar(255) NOT NULL, ");
            //SQL.AppendLine("  `MenuDesc` varchar(255) NOT NULL, ");
            //SQL.AppendLine("  `Action` char(1) NOT NULL DEFAULT '', ");
            //SQL.AppendLine("  `CreateDt` timestamp NOT NULL DEFAULT current_timestamp(), ");
            //SQL.AppendLine("  PRIMARY KEY(`Id`), ");
            //SQL.AppendLine("  KEY `CtCode` (`CtCode`,`DbName`,`DbHost`), ");
            //SQL.AppendLine("  KEY `UserCode` (`UserCode`,`MenuCode`,`Action`,`MenuDesc`), ");
            //SQL.AppendLine("  KEY `CreateDt` (`CreateDt`), ");
            //SQL.AppendLine("  KEY `Id` (`Id`) ");
            //SQL.AppendLine(") ENGINE = InnoDB DEFAULT CHARSET = latin1; ");

            //SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tbloption` ( ");
            //SQL.AppendLine("    `OptCat` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci', ");
            //SQL.AppendLine("    `OptCode` VARCHAR(10) NOT NULL DEFAULT '' COLLATE 'latin1_swedish_ci', ");
            //SQL.AppendLine("    `OptDesc` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'latin1_swedish_ci', ");
            //SQL.AppendLine("    PRIMARY KEY(`OptCat`, `OptCode`) USING BTREE, ");
            //SQL.AppendLine("   INDEX `OptCat` (`OptCat`, `OptCode`, `OptDesc`) USING BTREE ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            //SQL.AppendLine("ENGINE = InnoDB ");
            //SQL.AppendLine("; ");

            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`) VALUES ");
            //SQL.AppendLine("('ActionInd', 'C', 'Cancel'), ");
            //SQL.AppendLine("('ActionInd', 'D', 'Delete'), ");
            //SQL.AppendLine("('ActionInd', 'E', 'Edit'), ");
            //SQL.AppendLine("('ActionInd', 'F', 'Find'), ");
            //SQL.AppendLine("('ActionInd', 'I', 'Insert'), ");
            //SQL.AppendLine("('ActionInd', 'N', 'Print'), ");
            //SQL.AppendLine("('ActionInd', 'O', 'Process'), ");
            //SQL.AppendLine("('ActionInd', 'R', 'Refresh'), ");
            //SQL.AppendLine("('ActionInd', 'S', 'Save'), ");
            //SQL.AppendLine("('ActionInd', 'V', 'CSV'), ");
            //SQL.AppendLine("('ActionInd', 'X', 'Excel'); ");


            //SQL.AppendLine("USE `" + Gv.Database + "`; ");

            #endregion

            #region create tables

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblservicedeliveryhdr` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DocDt` VARCHAR(8) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Status` VARCHAR(1) NOT NULL DEFAULT 'A' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CtCode` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DocNoInternal` VARCHAR(80) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DOCtDocNo` VARCHAR(80) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CCCode` VARCHAR(80) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CurCode` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `SAName` VARCHAR(40) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `SAAddress` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `SACityCode` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `SACntCode` VARCHAR(16) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `SAPostalCd` VARCHAR(20) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `SAPhone` VARCHAR(40) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `SAFax` VARCHAR(40) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `SAEmail` VARCHAR(40) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `SAMobile` VARCHAR(40) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `ResiNo` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `JournalDocNo` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Amt` DECIMAL(20, 6) NOT NULL DEFAULT '0.000000', ");
            SQL.AppendLine("    `Remark` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(16) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNo` (`DocNo`, `DocDt`, `Status`, `CtCode`, `CreateDt`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DOCtDocNo` (`DOCtDocNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `CCCode` (`CCCode`) USING BTREE, ");
            SQL.AppendLine("   INDEX `JournalDocNo` (`JournalDocNo`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblservicedeliverydtl` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DNo` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CancelInd` VARCHAR(1) NOT NULL DEFAULT 'N' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CancelReason` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `ItCode` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Qty` DECIMAL(12, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `Qty2` DECIMAL(12, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `Qty3` DECIMAL(12, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `UPrice` DECIMAL(18, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `JournalDocNo` VARCHAR(40) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Remark` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(16) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`, `DNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNo` (`DocNo`, `DNo`, `CancelInd`, `CreateDt`) USING BTREE, ");
            SQL.AppendLine("   INDEX `ItCode` (`ItCode`) USING BTREE, ");
            SQL.AppendLine("   INDEX `JournalDocNo` (`JournalDocNo`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblbudgettransferhdr` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DocDt` VARCHAR(8) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Status` VARCHAR(1) NOT NULL DEFAULT 'A' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CancelInd` VARCHAR(1) NOT NULL DEFAULT 'N' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CancelReason` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DocNoInternal` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DeptCode` VARCHAR(60) NOT NULL COMMENT 'Department From' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DeptCode2` VARCHAR(60) NOT NULL COMMENT 'Department To' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Amt` DECIMAL(20, 6) NOT NULL DEFAULT '0.000000', ");
            SQL.AppendLine("    `Remark` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNoInternal` (`DocNoInternal`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNo` (`DocNo`, `DocDt`, `Status`, `DeptCode`, `DeptCode2`, `CreateDt`, `CancelInd`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblbudgettransferdtl` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DNo` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `BCCode` VARCHAR(60) NOT NULL COMMENT 'Budget Category From' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `BCCode2` VARCHAR(60) NOT NULL COMMENT 'Budget Category To' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Amt` DECIMAL(20, 6) NOT NULL DEFAULT '0.000000' COMMENT 'Total Transferred', ");
            SQL.AppendLine("    `Remark` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`, `DNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNo` (`DocNo`, `DNo`, `BCCode`, `BCCode2`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblbudgettransferfile` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DNo` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `FileName` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`, `DNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNo` (`DocNo`, `DNo`, `FileName`, `CreateDt`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tbllogerror` ( ");
            SQL.AppendLine("    `UserCode` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LogIn` VARCHAR(14) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LogOut` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `IP` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Machine` VARCHAR(80) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `SysVer` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `Remark` VARCHAR(80) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(16) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");


            #endregion

            #region insert data

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('IsServiceDeliveryApprovalInSequence', 'Indikator untuk Service Delivery approval apakah harus berurutan atau acak.', 'Y', NULL, 'Approval', 'Y', 'WEDHA', '202302080000', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `Property1`, `Property2`, `Property3`, `Property4`, `Property5`, `Property6`, `Property7`, `Property8`, `Property9`, `Property10`, `Remark`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('ApprovalType', 'ServiceDelivery', 'Service Delivery', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WEDHA', '202302080000', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('IsBudgetTransferAllowToUploadFile', 'Apakah Budget Transfer di ijinkan untuk upload file ? [Y = Ya, N = Tidak]', 'N', 'HEX', NULL, 'Y', 'WEDHA', '202302131400', 'HAR', '201810031151'); ");
            SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `Property1`, `Property2`, `Property3`, `Property4`, `Property5`, `Property6`, `Property7`, `Property8`, `Property9`, `Property10`, `Remark`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('ApprovalType', 'BudgetTransfer', 'Budget Transfer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WEDHA', '202302150000', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('IsBudgetTransferApprovalInSequence', 'Indikator untuk Budget Transfer approval apakah harus berurutan atau acak.', 'Y', NULL, 'Approval', 'Y', 'WEDHA', '202302150000', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsUseR1', '[R1] apakah menggunakan R1 ?', 'N', NULL, NULL, 'Y', 'WEDHA', '202302171500', NULL, NULL); ");
            SQL.AppendLine("Insert Ignore Into `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `CreateBy`, `CreateDt`) VALUES('RecvvdJournalFormat', 'Apakah journal recvvd  menggunakan param DefferedChargesAcNo? 1 = Default ; 2 = menggunakan param DefferedChargesAcNo', '1', 'MNET', 'RDA', '202302240000'); ");
            SQL.AppendLine("Insert Ignore Into `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `CreateBy`, `CreateDt`) VALUES('Recvvd2JournalFormat', 'Apakah journal recvvd 2 menggunakan param DefferedChargesAcNo? 1 = Default ; 2 = menggunakan param DefferedChargesAcNo', '1', 'MNET', 'RDA', '202302240000'); ");
            SQL.AppendLine("Insert Ignore Into `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('DefferedChargesAcNo', 'AcNo untuk biaya ditangguhkan project', NULL, 'MNET', NULL, 'Y', 'RDA', '202302230000', NULL, NULL); ");
            SQL.AppendLine("Insert Ignore Into `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `CreateBy`, `CreateDt`) VALUES('IsVoucherRequestExternalPayrollNoJournal', 'apakah voucher request external  payroll tidak membentuk jurnal? Y=ya, N=tidak', 'N', 'MNET', 'MYA', '202302210000'); ");
            SQL.AppendLine("Insert Ignore Into `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `CreateBy`, `CreateDt`) VALUES('IsVoucherRequestExternalPayrollHideVendor', 'Apakah Voucher Request for External Payroll tidak menampilkan Vendor ? [Y=iya, N=tidak]', 'N', 'MNET', 'MYA', '202302210000'); ");
            SQL.AppendLine("Insert Ignore Into `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `CreateBy`, `CreateDt`) VALUES('IsVoucherRequestExternalPayrollUseBankAcc', 'apakah voucher request external payroll menampilkan bank account? y=ya, n =tidak', 'N', 'MNET', 'MYA', '202302210000'); ");
            SQL.AppendLine("Insert Ignore Into `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsBOQUploadFileMandatory', 'Apakah Upload File pada menu Bill of Quantity Mandatory? [Y:Ya, N:Tidak]', 'N', 'MNET', NULL, 'Y', 'MYA', '202302070000', NULL, NULL); ");            
            SQL.AppendLine("Insert Ignore Into `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsDroppingRequestUseUsageDate', 'Apakah menu dropping request menggunakan field usage date? [ Y = Ya ; N = Tidak ]', 'N', 'MNET', NULL, 'Y', 'RDA', '202302270000', NULL, NULL); ");
            SQL.AppendLine("Insert Ignore Into `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsDroppingRequestUsePurpose', 'Apakah menu dropping request menggunakan field purpose? [ Y = Ya ; N = Tidak ]', 'N', 'MNET', NULL, 'Y', 'RDA', '202302270000', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `CreateBy`, `CreateDt`) VALUES('IsRecvWhsUseApproval', 'apakah transaksi receiving item from other warehouse menggunakan approval? [ N = Tidak ; Y = Ya ]', 'N', 'MNET', NULL, 'RDA', '202303010000'); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `CreateBy`, `CreateDt`) VALUES('IsRecvWhsApprovalBasedOnWhs', 'Approval receiving item from warehouse berdasarkan warehouse atau tidak ? (Y=Berdasarkan, N=Tdk berdasarkan)', 'N', 'MNET', 'RDA', '202303020000'); ");
            SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `CreateBy`, `CreateDt`) VALUES('ApprovalType', 'RecvWhs', 'Receiving Item From Other Warehouse', 'RDA', '202303030000'); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('ChangePassValidateByHistoricalPass', 'Apakah user saat change password perlu melihat password historical (Y = Yes ; N= No)', 'N', 'PHT', NULL, 'Y', 'HAR', '202303170000', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('SystemAdministratorContact', 'nomor / account sys admin yang bisa dihubungi ', NULL, 'PHT', NULL, 'Y', 'HAR', '202303170000', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsVoucherRequestUploadFileMandatory', 'Apakah upload File 1 di VR mandatory? [Y: Mandatory; N: Optional]', 'N', 'BBT', NULL, 'Y', 'MAU', '202303170000', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsVRCashAdvanceUseEmployeePIC', 'Menggunakan Indikator Cash Advance PIC ?  [Y:Ya, N:Tidak]', 'N', 'PHT', NULL, 'Y', 'MAU', '202303290000', 'DITA', '202303300935'); ");
            SQL.AppendLine("Insert Ignore Into `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Editable`, `CreateBy`, `CreateDt`) VALUES('IsUsePreparedStatement', 'Apakah eksekusi query menggunakan prepared statement checking ?', 'N', 'N', 'WEDHA', '202304171056'); ");
            SQL.AppendLine("INSERT Ignore INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Editable`, `CreateBy`, `CreateDt`) VALUES ('IsActivityLogStored', 'Apakah activity log perlu disimpan ?', 'N', 'N', 'WEDHA', '202304192214'); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('FormPrintOutGenerateDOCt2', 'set nama file print out GenerateDOCt2', NULL, NULL, NULL, 'Y', 'WEDHA', '202304242254', NULL, NULL); ");


            #endregion

            #region alter table

            SQL.AppendLine("ALTER TABLE `tbllog` ");
            SQL.AppendLine("    CHANGE COLUMN `SysVer` `SysVer` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci' AFTER `Machine`; ");

            SQL.AppendLine("ALTER TABLE `tbllogerror` ");
            SQL.AppendLine("    CHANGE COLUMN `SysVer` `SysVer` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci' AFTER `Machine`; ");

            SQL.AppendLine("ALTER TABLE `tbldroppingrequesthdr` ");
            SQL.AppendLine("    Add Column If Not Exists `UsageDt` VARCHAR(8) NULL DEFAULT NULL AFTER `DocType`, ");
            SQL.AppendLine("    Add Column If Not Exists `Purpose` VARCHAR(300) NULL DEFAULT NULL AFTER `UsageDt`; ");

            SQL.AppendLine("ALTER TABLE `tblrecvwhsdtl`  ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `Status` VARCHAR(1) NOT NULL DEFAULT 'A' AFTER `CancelInd`; ");

            SQL.AppendLine("ALTER TABLE `tblexpensestypedtl` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `CashTypeGrpCode` VARCHAR(16) NULL DEFAULT NULL AFTER `Formula`, ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `CashTypeCode` VARCHAR(16) NULL DEFAULT NULL AFTER `CashTypeGrpCode`; ");

            SQL.AppendLine("ALTER TABLE `tblposition` ");
            SQL.AppendLine("    ADD COLUMN if NOT exists `CashAdvPIC` VARCHAR(1) NOT NULL DEFAULT 'N' AFTER `TeritoryCode`; ");

            SQL.AppendLine("ALTER TABLE `tblvoucherrequesthdr` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `CashAdvanceTypeCode` VARCHAR(3) NULL DEFAULT NULL AFTER `Remark`, ");
            SQL.AppendLine("    ADD INDEX IF NOT EXISTS `CashAdvanceTypeCode` (`CashAdvanceTypeCode`); ");

            SQL.AppendLine("ALTER TABLE `tblvoucherrequesthdr` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `EmpCodeCashAdv` VARCHAR(50) NULL DEFAULT NULL AFTER `PIC`, ");
            SQL.AppendLine("    ADD INDEX IF NOT EXISTS `EmpCodeCashAdv` (`EmpCodeCashAdv`); ");

            #endregion

            Sm.ExecQuery(SQL.ToString());

            //await Task.Run(() =>
            //{
            //    Sm.ExecQuery(SQL.ToString());
            //});
        }

        private void GetParameter()
        {
            #region Old Code
            /*
            mUserCodeForSelfOnBoardingPrePensionNotification = Sm.GetParameter("UserCodeForSelfOnBoardingPrePensionNotification");
            mMenuCodeForShiftClosing = Sm.GetParameter("MenuCodeForShiftClosing");
            mMenuCodeForDayEndClosing = Sm.GetParameter("MenuCodeForDayEndClosing");
            mIsAutoLogOffActived = Sm.GetParameterBoo("IsAutoLogOffActived");
            mIsPPSApprovalBasedOnDept = Sm.GetParameterBoo("IsPPSApprovalBasedOnDept");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsDOCtApprovalActived = Sm.IsDataExist("Select 1 from TblDocApprovalSetting Where DocType='DOCt' Limit 1;");
            mIsProjectImplementationApprovalBySiteMandatory = Sm.GetParameterBoo("IsProjectImplementationApprovalBySiteMandatory");
            mIsPOSEnabled = mMenuCodeForShiftClosing.Length > 0 && mMenuCodeForDayEndClosing.Length > 0;
            mEmpEndContractBenchmark = Sm.GetParameter("EmpEndContractBenchmark");
            mGrpCodeForEmpEndContractNotification = Sm.GetParameter("GrpCodeForEmpEndContractNotification");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsMaterialRequestApprovalByDeptEnabled = IsMaterialRequestApprovalByDeptEnabled();
            mIsUseSelfOnBoarding = Sm.GetParameterBoo("IsUseSelfOnBoarding");
            mSelfOnboardingNewsLimit = Sm.GetParameter("SelfOnboardingNewsLimit");
            mSelfOnboardingFontSize = Sm.GetParameter("SelfOnboardingFontSize");
            mIsDocApprovalSettingUseLeaveCode = Sm.GetParameterBoo("IsDocApprovalSettingUseLeaveCode");
            mApprovalDocTypeForNotification = Sm.GetParameter("ApprovalDocTypeForNotification");
            mDocTitle = Sm.GetParameter("DocTitle");

            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mIsPasswordForLoginNeedToEncode = Sm.GetParameterBoo("IsPasswordForLoginNeedToEncode");
            mIsUserNeedToChangeDefaultPassword = Sm.GetParameterBoo("IsUserNeedToChangeDefaultPassword");
            */
            #endregion

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'UserCodeForSelfOnBoardingPrePensionNotification','MenuCodeForShiftClosing','MenuCodeForDayEndClosing','EmpEndContractBenchmark','GrpCodeForEmpEndContractNotification', ");
            SQL.AppendLine("'SelfOnboardingNewsLimit','SelfOnboardingFontSize','ApprovalDocTypeForNotification','DocTitle','HostAddrForFTPClient', ");
            SQL.AppendLine("'SharedFolderForFTPClient','UsernameForFTPClient','PasswordForFTPClient','PortForFTPClient','IsAutoLogOffActived', ");
            SQL.AppendLine("'IsPPSApprovalBasedOnDept','IsAutoJournalActived','IsProjectImplementationApprovalBySiteMandatory','IsFiltverBySite','IsUseSelfOnBoarding', ");
            SQL.AppendLine("'IsDocApprovalSettingUseLeaveCode','IsPasswordForLoginNeedToEncode','IsUserNeedToChangeDefaultPassword', 'IsFormMainShowChangePwd' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "UserCodeForSelfOnBoardingPrePensionNotification": mUserCodeForSelfOnBoardingPrePensionNotification = ParValue; break;
                            case "MenuCodeForShiftClosing": mMenuCodeForShiftClosing = ParValue; break;
                            case "MenuCodeForDayEndClosing": mMenuCodeForDayEndClosing = ParValue; break;
                            case "EmpEndContractBenchmark": mEmpEndContractBenchmark = ParValue; break;
                            case "GrpCodeForEmpEndContractNotification": mGrpCodeForEmpEndContractNotification = ParValue; break;
                            case "SelfOnboardingNewsLimit": mSelfOnboardingNewsLimit = ParValue; break;
                            case "SelfOnboardingFontSize": mSelfOnboardingFontSize = ParValue; break;
                            case "ApprovalDocTypeForNotification": mApprovalDocTypeForNotification = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;

                            //boolean
                            case "IsAutoLogOffActived": mIsAutoLogOffActived = ParValue == "Y"; break;
                            case "IsPPSApprovalBasedOnDept": mIsPPSApprovalBasedOnDept = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsProjectImplementationApprovalBySiteMandatory": mIsProjectImplementationApprovalBySiteMandatory = ParValue == "Y"; break;
                            case "IsFiltverBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsUseSelfOnBoarding": mIsUseSelfOnBoarding = ParValue == "Y"; break;
                            case "IsDocApprovalSettingUseLeaveCode": mIsDocApprovalSettingUseLeaveCode = ParValue == "Y"; break;
                            case "IsPasswordForLoginNeedToEncode": mIsPasswordForLoginNeedToEncode = ParValue == "Y"; break;
                            case "IsUserNeedToChangeDefaultPassword": mIsUserNeedToChangeDefaultPassword = ParValue == "Y"; break;
                            case "IsFormMainShowChangePwd": mIsFormMainShowChangePwd = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
            
            mIsDOCtApprovalActived = Sm.IsDataExist("Select 1 from TblDocApprovalSetting Where DocType='DOCt' Limit 1;");
            mIsPOSEnabled = mMenuCodeForShiftClosing.Length > 0 && mMenuCodeForDayEndClosing.Length > 0;
            mIsMaterialRequestApprovalByDeptEnabled = IsMaterialRequestApprovalByDeptEnabled();

            var GrpCodeForProductionOrderNotify = Sm.GetParameter("GrpCodeForProductionOrderNotify");
            if (GrpCodeForProductionOrderNotify.Length > 0)
            {
                mIsProductionOrderNotifyEnabled = Sm.IsDataExist(
                    "Select 1 From TblUser Where GrpCode Is Not Null And UserCode=@Param1 And Find_In_Set(GrpCode, @Param2); ",
                    Gv.CurrentUserCode, GrpCodeForProductionOrderNotify, string.Empty);
            }

            mLockInd = Sm.IsDataExist("Select LockInd From TblLockProcess Where LockInd='Y';");
            if (mLockInd)
            {
                mLockDt = Sm.GetValue("Select LockDt From TblLockProcess Where LockInd='Y' Limit 1;");
                mWarningMsg = Sm.GetValue("Select WarningMsg2 From TblLockProcess Where LockInd='Y' Limit 1;");

                if (mLockDt.Length > 0)
                {
                    string WarningMsg = Sm.GetValue("Select WarningMsg1 From TblLockProcess Where LockInd='Y' Limit 1;");
                    string CurrentDt = Sm.ServerCurrentDate();

                    DateTime Dt1 = new DateTime(
                       Int32.Parse(mLockDt.Substring(0, 4)),
                       Int32.Parse(mLockDt.Substring(4, 2)),
                       Int32.Parse(mLockDt.Substring(6, 2)),
                       0, 0, 0
                       );
                    DateTime Dt2 = new DateTime(
                        Int32.Parse(CurrentDt.Substring(0, 4)),
                        Int32.Parse(CurrentDt.Substring(4, 2)),
                        Int32.Parse(CurrentDt.Substring(6, 2)),
                        0, 0, 0
                        );

                    int d = ((TimeSpan)(Dt1 - Dt2)).Days;

                    if (d > 0)
                    {
                        string Msg = string.Empty;
                        Msg += "Expiration date : " +
                            Sm.Right(mLockDt, 2) + "/" +
                            mLockDt.Substring(4, 2) + "/" +
                            Sm.Left(mLockDt, 4) + Environment.NewLine;
                        if (d == 1)
                            Msg += "Status : " + d.ToString() + " day remaining" + Environment.NewLine;
                        else
                            Msg += "Status : " + d.ToString() + " days remaining" + Environment.NewLine;
                        if (WarningMsg.Length > 0)
                            Msg += Environment.NewLine + d.ToString() + WarningMsg;
                        Sm.StdMsg(mMsgType.Warning, Msg);
                    }
                }
            }
        }

        private bool IsAllMenuShowed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Case When Exists( ");
            SQL.AppendLine("    Select 1 From TblParameter ");
            SQL.AppendLine("    Where ParCode='ShowAllMenu' And ParValue='Y' ");
            SQL.AppendLine(") Or  ");
            SQL.AppendLine("Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblUser ");
            SQL.AppendLine("    Where UserCode=@UserCode And GrpCode='SysAdm' ");
            SQL.AppendLine(") Then '1' Else '0' End As IsAllMenuShowed; ");

            var cm = new MySqlCommand { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return (Sm.GetValue(cm) == "1");
        }

        private void SetMenuData(ref List<Menu> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            if (IsAllMenuShowed())
            {
                SQL.AppendLine("Select T.MenuCode, T.MenuDesc, T.Parent, T.Param, T.Icon, ");
                SQL.AppendLine("Case When Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblUser A, TblGroup B, TblGroupMenu C ");
                SQL.AppendLine("    Where A.GrpCode=B.GrpCode And B.GrpCode=C.GrpCode ");
                SQL.AppendLine("    And C.MenuCode=T.MenuCode And A.UserCode=@UserCode Limit 1 ");
                SQL.AppendLine("    ) Then 'Y' Else 'N' End As Enabled ");
                SQL.AppendLine("From TblMenu T ");
                SQL.AppendLine("Where T.StdInd='Y' ");
                SQL.AppendLine("And T.Visible='Y' ");
                SQL.AppendLine("And T.MenuCat = 'D' ");
                SQL.AppendLine("Order By T.MenuCode; ");
            }
            else
            {
                SQL.AppendLine("Select D.MenuCode, D.MenuDesc, D.Parent, D.Param, D.Icon, 'Y' As Enabled ");
                SQL.AppendLine("From TblUser A, TblGroup B, TblGroupMenu C, TblMenu D ");
                SQL.AppendLine("Where A.GrpCode=B.GrpCode ");
                SQL.AppendLine("And B.GrpCode=C.GrpCode ");
                SQL.AppendLine("And C.MenuCode=D.MenuCode ");
                SQL.AppendLine("And D.StdInd='Y' ");
                SQL.AppendLine("And D.Visible='Y' ");
                SQL.AppendLine("And D.MenuCat='D' ");
                SQL.AppendLine("And A.UserCode=@UserCode;");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "MenuCode",

                         //1-5
                         "MenuDesc", "Parent", "Icon", "Param", "Enabled"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Menu()
                        {
                            MenuCode = Sm.DrStr(dr, c[0]),
                            MenuDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Icon = Sm.DrStr(dr, c[3]),
                            Param = Sm.DrStr(dr, c[4]),
                            Enabled = Sm.DrStr(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetMenuParent()
        {
            var lm = new List<Menu>();
            var ms = new MenuStrip();
            var lt = new List<ToolStripMenuItem>();

            SetMenuData(ref lm);

            var lp =
               (
                    from m in lm
                    where m.Parent.Length == 0
                    select m
                ).ToList();

            lp.ForEach(m =>
            {
                lt.Add(new ToolStripMenuItem
                {
                    Name = m.MenuCode,
                    Text = m.MenuDesc,
                    Enabled = Sm.CompareStr(m.Enabled, "Y"),
                    Image = (m.Icon.Length == 0) ? null : (Image)Properties.Resources.ResourceManager.GetObject(m.Icon)
                });
            });

            lt.ForEach(t =>
            {
                SetMenuChild(lm, t);
                ms.Items.Add(t);
            }
            );
            this.Controls.Add(ms);
        }

        private void SetMenuChild(List<Menu> lm, ToolStripMenuItem t)
        {
            string Parent = t.Name;
            var lt = new List<ToolStripMenuItem>();

            var lp =
               (
                    from m in lm
                    where m.Parent == Parent
                    select m
                ).ToList();

            lp.ForEach(m =>
            {
                lt.Add(new ToolStripMenuItem
                {
                    Name = m.MenuCode,
                    Text = m.MenuCode + "-" + m.MenuDesc,
                    Tag = m.Param + ';' + m.MenuCode,
                    Enabled = Sm.CompareStr(m.Enabled, "Y"),
                    Image = (m.Icon.Length == 0) ? null : (Image)Properties.Resources.ResourceManager.GetObject(m.Icon)
                });
            });

            lt.ForEach(m =>
            {
                if (m.Tag.ToString().Length != 0) m.Click += FormOpenClick;
                SetMenuChild(lm, m);
                t.DropDownItems.Add(m);
            });
        }

        private void UpdateLog()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblLog Set LogOut=CurrentDateTime(), LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where UserCode=@UserCode And LogIn=@LogIn;"
            };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@LogIn", Gv.LogIn);

            Sm.ExecCommand(cm);
        }

        private void SaveAppUsageHistory(string MenuCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAppUsageHistory(MenuCode, MenuDesc, CreateBy, CreateDt) " +
                    "Select MenuCode, MenuDesc, @UserCode, CurrentDateTime() " +
                    "From Tblmenu Where MenuCode=@MenuCode;"
            };
            Sm.CmParam<String>(ref cm, "@MenuCode", MenuCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        private string[] GetFontSize(string FontSizes)
        {
            string[] FontSize = FontSizes.Split(',');
            return FontSize;
        }

        #region Notification

        private void RunNotification()
        {
            mIsApprovalNotify = IsUserNeedApprovalNotification();
            mIsOutstandingCancellationPONotify = IsUserNeedOutstandingCancellationPONotification();
            mIsEmpContractDocNotify = IsUserNeedEmpContractDocNotification();
            mIsOutstandingSOContractNotify = IsOutstandingSOContractNotify();


            if (mIsApprovalNotify || mIsOutstandingCancellationPONotify || mIsEmpContractDocNotify || mIsProductionOrderNotifyEnabled || mIsOutstandingSOContractNotify)
            {
                statusStrip1.Visible = true;
                toolStripStatusLabel1.Text = string.Empty;
                toolStripStatusLabel1.BackColor = Color.Transparent;

                if (mIsApprovalNotify)
                {
                    mPORequestApprovalForAllDept = Sm.GetParameterBoo("PORequestApprovalForAllDept");
                    mIsPORevisionApprovalUseDept = Sm.GetParameterBoo("IsPORevisionApprovalUseDept");
                    mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
                    mIsAPDownpaymentApprovalBasedOnDept = Sm.GetParameterBoo("IsAPDownpaymentApprovalBasedOnDept");
                    mIsRecvVdApprovalBasedOnWhs = Sm.GetParameterBoo("IsRecvVdApprovalBasedOnWhs");
                    mIsRecvWhs2ApprovalBasedOnWhs = Sm.GetParameterBoo("IsRecvWhs2ApprovalBasedOnWhs");
                    mIsVoucherRequestTaxApprovalBasedOnDept = Sm.GetParameterBoo("IsVoucherRequestTaxApprovalBasedOnDept");
                    mIsVoucherRequestPPNApprovalBasedOnDept = Sm.GetParameterBoo("IsVoucherRequestPPNApprovalBasedOnDept");
                    mSQLApprovalNotify = GetSQLApprovalNotify();
                    mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
                    mIsReturnAPDownpaymentApprovalBasedOnDept = Sm.GetParameterBoo("IsReturnAPDownpaymentApprovalBasedOnDept");
                    mIsReturnAPDownpaymentUseEntity = Sm.GetParameterBoo("IsReturnAPDownpaymentUseEntity");
                    mIsReturnARDownpaymentApprovalBasedOnDept = Sm.GetParameterBoo("IsReturnARDownpaymentApprovalBasedOnDept");
                    mIsReturnARDownpaymentUseEntity = Sm.GetParameterBoo("IsReturnARDownpaymentUseEntity");
                }

                if (mIsOutstandingCancellationPONotify) mSQLOutstandingCancellationPONotify = GetSQLOutstandingCancellationPONotify();
                if (mIsEmpContractDocNotify) mSQLEmpContractDocNotify = GetSQLEmpContractDocNotify();
                if (mIsProductionOrderNotifyEnabled) mSQLProductionOrderNotify = GetSQLProductionOrderNotify();
                if (mIsOutstandingSOContractNotify)
                    mSQLOutstandingSOContractNotify = GetSQLOutstandingSOContractNotify();

                ProcessNotification();

                Timer Tm = new Timer();
                Tm.Interval = GetTimerIntervalNotification();
                Tm.Tick += new EventHandler(UpdateNotification);
                Tm.Start();
            }
        }

        private bool IsUserNeedApprovalNotification()
        {
            return Sm.IsDataExist("Select 1 From TblUser Where UserCode=@Param And NotifyInd='Y';", Gv.CurrentUserCode);
        }

        private bool IsUserNeedOutstandingCancellationPONotification()
        {
            return Sm.GetParameterBoo("OutstandingCancellationPONotify");
        }

        private bool IsUserNeedEmpContractDocNotification()
        {
            return Sm.GetParameterBoo("EmpContractDocNotify");
        }

        private bool IsOutstandingSOContractNotify()
        {
            return Sm.GetParameterBoo("IsOutstandingSOContractNotify");
        }

        private int GetTimerIntervalNotification()
        {
            string Param = Sm.GetParameter("TimerIntervalNotification");
            if (Param.Length > 0)
                return Int32.Parse(Param);
            else
                return 3600000;
        }

        private string GetSQLApprovalNotify()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sum(RecCount) RecCount From (");

            #region Material Request

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("    And B.DeptCode In (Select DeptCode From TblMaterialRequestHdr Where DocNo=A.DocNo) ");

            if (mIsApprovalBySiteMandatory)
            {
                SQL.AppendLine("    And B.SiteCode In ( ");
                SQL.AppendLine("        Select SiteCode From TblMaterialRequestHdr ");
                SQL.AppendLine("        Where DocNo=A.DocNo ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("Where A.DocType='MaterialRequest' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblMaterialRequestDtl Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo And DNo=A.DNo) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsMaterialRequestApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Material Request 2

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            if (mIsMaterialRequestApprovalByDeptEnabled)
                SQL.AppendLine("    And B.DeptCode Is Not Null And B.DeptCode In (Select DeptCode From TblMaterialRequestHdr Where DocNo = A.DocNo) ");
            if (mIsFilterBySite)
                SQL.AppendLine("    And B.SiteCode Is Not Null And B.SiteCode In (Select SiteCode From TblMaterialRequestHdr Where DocNo = A.DocNo) ");
            SQL.AppendLine("Where A.DocType='MaterialRequest2' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select 1 From TblMaterialRequestDtl Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo And DNo=A.DNo) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsMaterialRequest2ApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region PO Request

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            if (!mPORequestApprovalForAllDept)
            {
                SQL.AppendLine("And B.DeptCode=( ");
                SQL.AppendLine("    Select T2.DeptCode ");
                SQL.AppendLine("    From TblPORequestDtl T1 ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr T2 On T1.MaterialRequestDocNo=T2.DocNo ");
                SQL.AppendLine("    Where T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Where A.DocType='PORequest' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblPORequestDtl Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo And DNo=A.DNo) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPORequestApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region PO

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='PO' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select 1 From TblPOHdr Where IfNull(Status, 'O')='O' And DocNo=A.DocNo) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPOApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region PO Revision

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");

            if (mIsPORevisionApprovalUseDept)
            {
                SQL.AppendLine("       And B.DeptCode In ( ");
                SQL.AppendLine("           Select Distinct T4.DeptCode ");
                SQL.AppendLine("           From TblPORevision T1 ");
                SQL.AppendLine("           Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo ");
                SQL.AppendLine("           Inner Join TblPORequestDtl T3 On T2.PORequestDocNo=T3.DocNo And T2.PORequestDNo=T3.DNo ");
                SQL.AppendLine("           Inner Join TblMaterialRequestHdr T4 On T3.MaterialRequestDocNo=T4.DocNo ");
                SQL.AppendLine("           Where T1.DocNo=A.DocNo ");
                SQL.AppendLine(") ");
            }

            SQL.AppendLine("Where A.DocType='PORevision' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select 1 From TblPORevision Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");

            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPORevisionApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Outgoing Payment

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");

            if (Sm.GetParameter("IsOutgoingPaymentApprovalNeedDept") == "Y")
                SQL.AppendLine("    And B.DeptCode = (Select DeptCode From TblOutgoingPaymentHdr Where DocNo=A.DocNo) ");

            if (Sm.GetParameter("IsOutgoingPaymentApprovalNeedPaymentType") == "Y")
                SQL.AppendLine("    And B.PaymentType = (Select PaymentType From TblOutgoingPaymentHdr Where DocNo=A.DocNo) ");

            SQL.AppendLine("Where A.DocType='OutgoingPayment' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblOutgoingPaymentHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsOutgoingPaymentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Incoming Payment

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='IncomingPayment' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblIncomingPaymentHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsIncomingPaymentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region AP Downpayment

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            if (mIsAPDownpaymentApprovalBasedOnDept)
                SQL.AppendLine("    And B.DeptCode In (Select DeptCode From TblAPDownpayment Where DocNo=A.DocNo And CancelInd='N') ");
            SQL.AppendLine("Where A.DocType='APDownpayment' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblAPDownpayment Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsAPDownpaymentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region AR Downpayment

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='ARDownpayment' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblARDownpayment Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsARDownpaymentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region OT Adjustment

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='OTAdjustment' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblOTAdjustment Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsOTAdjustmentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Voucher Request

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("    And B.DeptCode = (Select DeptCode From TblVoucherRequestHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='VoucherRequest' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblVoucherRequestHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsVoucherRequestApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Voucher Request Social Security

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Inner Join TblVoucherRequestSSHdr C On A.DocNo=C.DocNo And C.CancelInd='N' ");
            SQL.AppendLine("Where A.DocType='VoucherRequestSS' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsVoucherRequestSSApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Voucher Request Payroll

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollHdr C On A.DocNo=C.DocNo And C.CancelInd='N' ");
            SQL.AppendLine("Where A.DocType='VoucherRequestPayroll' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsVoucherRequestPayrollApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Employee's Leave Request

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("    And B.DeptCode In ( ");
            SQL.AppendLine("        Select T2.DeptCode ");
            SQL.AppendLine("        From TblEmpLeaveHdr T1 ");
            SQL.AppendLine("        Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
            SQL.AppendLine("        Where T1.DocNo=A.DocNo And T1.CancelInd='N' ");
            SQL.AppendLine("    ) ");

            if (mIsApprovalBySiteMandatory)
            {
                SQL.AppendLine("    And B.SiteCode In ( ");
                SQL.AppendLine("        Select T2.SiteCode ");
                SQL.AppendLine("        From TblEmpLeaveHdr T1 ");
                SQL.AppendLine("        Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
                SQL.AppendLine("        Where T1.DocNo=A.DocNo And T1.CancelInd='N' ");
                SQL.AppendLine("    ) ");
            }

            if (mIsDocApprovalSettingUseLeaveCode) SQL.AppendLine("    And B.LeaveCode Is Not Null ");

            SQL.AppendLine("Where A.DocType='EmpLeave' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblEmpLeaveHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            if (mIsDocApprovalSettingUseLeaveCode)
            {
                SQL.AppendLine("    And Find_In_Set(LeaveCode, B.LeaveCode) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsEmpLeaveApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Group of Employee's Leave Request

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("    And B.DeptCode In ( ");
            SQL.AppendLine("        Select DeptCode From TblEmpLeave2Hdr ");
            SQL.AppendLine("        Where DocNo=A.DocNo And CancelInd='N' ");
            SQL.AppendLine("    ) ");

            if (mIsApprovalBySiteMandatory)
            {
                SQL.AppendLine("    And B.SiteCode In ( ");
                SQL.AppendLine("        Select SiteCode From TblEmpLeave2Hdr ");
                SQL.AppendLine("        Where DocNo=A.DocNo And CancelInd='N' ");
                SQL.AppendLine("    ) ");
            }

            if (mIsDocApprovalSettingUseLeaveCode) SQL.AppendLine("    And B.LeaveCode Is Not Null ");

            SQL.AppendLine("Where A.DocType='EmpLeave2' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblEmpLeave2Hdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            if (mIsDocApprovalSettingUseLeaveCode)
            {
                SQL.AppendLine("    And Find_In_Set(LeaveCode, B.LeaveCode) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsEmpLeave2ApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region OT Request

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("    And B.DeptCode In ( ");
            SQL.AppendLine("        Select DeptCode From TblOTRequestHdr ");
            SQL.AppendLine("        Where DocNo=A.DocNo And CancelInd='N' ");
            SQL.AppendLine("    ) ");

            if (mIsApprovalBySiteMandatory)
            {
                SQL.AppendLine("    And B.SiteCode In ( ");
                SQL.AppendLine("        Select SiteCode From TblOTRequestHdr ");
                SQL.AppendLine("        Where DocNo=A.DocNo And CancelInd='N' ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("Where A.DocType='OTRequest' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblOTRequestHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsOTRequestApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Receiving From Vendor With PO

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            if (mIsRecvVdApprovalBasedOnWhs)
            {
                SQL.AppendLine("    And B.WhsCode In ( ");
                SQL.AppendLine("        Select Distinct T1.WhsCode ");
                SQL.AppendLine("        From TblRecvVdHdr T1, TblRecvVdDtl T2 ");
                SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T1.DocNo=A.DocNo ");
                SQL.AppendLine("        And T2.DNo=A.DNo ");
                SQL.AppendLine("        And T2.CancelInd='N' ");
                SQL.AppendLine("        And IfNull(T2.Status, 'O')='O' ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where A.DocType='RecvVd' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblRecvVdDtl Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo And DNo=A.DNo) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsRecvVdApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Receiving From Vendor Without PO

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            if (mIsRecvVdApprovalBasedOnWhs)
            {
                SQL.AppendLine("    And B.WhsCode In ( ");
                SQL.AppendLine("        Select Distinct T1.WhsCode ");
                SQL.AppendLine("        From TblRecvVdHdr T1, TblRecvVdDtl T2 ");
                SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T1.DocNo=A.DocNo ");
                SQL.AppendLine("        And T2.DNo=A.DNo ");
                SQL.AppendLine("        And T2.CancelInd='N' ");
                SQL.AppendLine("        And IfNull(T2.Status, 'O')='O' ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where A.DocType='RecvVd2' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblRecvVdDtl Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo And DNo=A.DNo) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsRecvVd2ApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Vendor's Quotation

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='QT' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblQtHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsVdQtApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");


            SQL.AppendLine("Union All ");

            #endregion

            #region Transfer Request

            SQL.AppendLine("Select Count(A.DocNo) As RecCount  ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType  ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo  ");
            SQL.AppendLine("    And B.UserCode Like @UserCode  ");
            SQL.AppendLine("    And B.WhsCode = (Select WhsCode2 From TblTransferRequestWhsHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='TransferRequestWHs' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblTransferRequestWhsHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsTransferRequestWhsApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("    ) Then 1 ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("        Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("        And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("        And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("    ) Then 0 Else 1 End ");
            SQL.AppendLine("End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region DO WHS 2 (With Transfer Request)

            SQL.AppendLine("Select Count(A.DocNo) As RecCount  ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType  ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo  ");
            SQL.AppendLine("    And B.UserCode Like @UserCode  ");
            SQL.AppendLine("    And B.WhsCode = (Select WhsCode From TblDOWhsHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='DOWhs2' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblDOWhsHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsDOWhs2ApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("    ) Then 1 ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("        Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("        And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("        And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("    ) Then 0 Else 1 End ");
            SQL.AppendLine("End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Employee's Request

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");

            #region 21/02/2017

            if (mIsFilterByDept)
            {
                SQL.AppendLine("    And B.DeptCode In ( ");
                SQL.AppendLine("        Select T1.DeptCode ");
                SQL.AppendLine("        From TblEmployeeRequest T1 ");
                SQL.AppendLine("        Where T1.DocNo=A.DocNo And T1.CancelInd='N' ");
                SQL.AppendLine("    ) ");
            }

            if (mIsApprovalBySiteMandatory)
            {
                SQL.AppendLine("    And B.SiteCode In ( ");
                SQL.AppendLine("        Select T1.SiteCode ");
                SQL.AppendLine("        From TblEmployeeRequest T1 ");
                SQL.AppendLine("        Where T1.DocNo=A.DocNo And T1.CancelInd='N' ");
                SQL.AppendLine("    ) ");
            }

            #endregion

            SQL.AppendLine("Where A.DocType='EmployeeRequest' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblEmployeeRequest Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsEmployeeRequestApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Employee's Working Schedule Amendment

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("    And B.DeptCode In ( ");
            SQL.AppendLine("        Select T1.DeptCode ");
            SQL.AppendLine("        From TblEmpWSHdr T1 ");
            SQL.AppendLine("        Where T1.DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Where A.DocType='EmpWS' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblEmpWSHdr Where IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsEmployeeRequestApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Customer Quotation

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='CtQt' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblCtQtHdr Where IfNull(Status, 'O')='O' And DocNo=A.DocNo) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsCtQtApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Production Order Revision

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='ProductionOrderRevision' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblProductionOrderRevision Where IfNull(Status, 'O')='O' And DocNo=A.DocNo) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsProductionOrderRevisionApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Residual Leave Payment

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            if (Sm.IsApprovalNeedXCode("RLP", "Dept"))
                SQL.AppendLine("And B.DeptCode Is Not Null And And B.DeptCode In (Select DeptCode From TblRLPHdr Where DocNo=A.DocNo) ");
            if (Sm.IsApprovalNeedXCode("RLP", "Site"))
                SQL.AppendLine("And B.SiteCode Is Not Null And And B.SiteCode In (Select SiteCode From TblRLPHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='RLP' ");
            SQL.AppendLine("And A.UserCode Is Null ");
            SQL.AppendLine("And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo From TblRLPHdr ");
            SQL.AppendLine("    Where IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsRLPApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Return APDownpayment

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            if (mIsReturnARDownpaymentApprovalBasedOnDept)
                SQL.AppendLine("    And B.DeptCode in (Select DeptCode From TblReturnApDownpayment Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='ReturnAPDownpayment' ");
            SQL.AppendLine("And A.UserCode Is Null ");
            SQL.AppendLine("And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo From TblReturnAPDownpayment ");
            SQL.AppendLine("    Where IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsReturnAPDownpaymentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Stock Opname

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='StockOpname' ");
            SQL.AppendLine("And A.UserCode Is Null ");
            SQL.AppendLine("And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo From TblStockOpnameHdr ");
            SQL.AppendLine("    Where IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsStockOpnameApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region PPS

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");

            if (mIsPPSApprovalBasedOnDept)
                SQL.AppendLine("    And B.DeptCode in (Select DeptCodeNew From TblPPS Where DocNo=A.DocNo) ");

            SQL.AppendLine("Where A.DocType='PPS' ");
            SQL.AppendLine("And A.UserCode Is Null ");
            SQL.AppendLine("And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo From TblPPS ");
            SQL.AppendLine("    Where IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPPSApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Receiving Item From Other Warehouse (Without DO)

            SQL.AppendLine("   Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("   From TblDocApproval A ");
            SQL.AppendLine("   Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("       On A.DocType=B.DocType ");
            SQL.AppendLine("       And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("       And B.UserCode Like @UserCode ");

            if (mIsRecvWhs2ApprovalBasedOnWhs)
            {
                SQL.AppendLine("        And B.WhsCode In ( ");
                SQL.AppendLine("            Select WhsCode ");
                SQL.AppendLine("            From TblRecvWhs2Hdr ");
                SQL.AppendLine("            Where DocNo=A.DocNo ");
                SQL.AppendLine("        ) ");
            }

            SQL.AppendLine("   Where A.DocType='RecvWhs2' And A.UserCode Is Null And A.Status Is Null ");

            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsRecvWhs2ApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Make To Stock

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='MakeToStock' ");
            SQL.AppendLine("And A.UserCode Is Null ");
            SQL.AppendLine("And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo From TblMakeToStockHdr ");
            SQL.AppendLine("    Where IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsMakeToStockApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Voucher Request For Tax

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");

            if (mIsVoucherRequestTaxApprovalBasedOnDept)
                SQL.AppendLine("    And B.DeptCode = (Select DeptCode From TblVoucherRequestTax Where DocNo = A.DocNo) ");

            SQL.AppendLine("Where A.DocType='VoucherRequestTax' ");
            SQL.AppendLine("And A.UserCode Is Null ");
            SQL.AppendLine("And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo From TblVoucherRequestTax ");
            SQL.AppendLine("    Where IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsVoucherRequestTaxApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Voucher Request PPN

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");

            if (mIsVoucherRequestPPNApprovalBasedOnDept)
                SQL.AppendLine("    And B.DeptCode = (Select DeptCode From TblVoucherRequestPPNHdr Where DocNo = A.DocNo) ");

            SQL.AppendLine("Where A.DocType='VoucherRequestPPN' ");
            SQL.AppendLine("And A.UserCode Is Null ");
            SQL.AppendLine("And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo From TblVoucherRequestPPNHdr ");
            SQL.AppendLine("    Where IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsVoucherRequestPPNApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Travel Request

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");

            if (Sm.IsDataExist(
                    "Select 1 From TblDocApprovalSetting " +
                    "Where DocType='TravelRequest' And DeptCode Is Not Null Limit 1;"))
            {
                SQL.AppendLine("   And B.DeptCode In ( ");
                SQL.AppendLine("       Select Distinct T2.DeptCode ");
                SQL.AppendLine("       From TblTravelRequestHdr T1 ");
                SQL.AppendLine("       Inner Join TblEmployee T2 On T1.PICCode=T2.EmpCode ");
                SQL.AppendLine("       Where T1.DocNo=A.DocNo ");
                SQL.AppendLine("       And T1.CancelInd='N' ");
                SQL.AppendLine("       And T1.Status='O' ");
                SQL.AppendLine("   ) ");
            }

            SQL.AppendLine("Where A.DocType='TravelRequest' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblTravelRequestHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsTravelRequestApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Annual Leave Allowance

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='AnnualLeaveAllowance' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblAnnualLeaveAllowanceHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsAnnualLeaveAllowanceApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Employee Severance

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='EmpSeverance' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblEmpSeverance Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsEmpSeveranceApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Employee Reward

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='EmpReward' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblEmpReward Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsEmpRewardApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Return ARDownpayment 15/02/2018

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            if (mIsReturnARDownpaymentApprovalBasedOnDept)
                SQL.AppendLine("    And B.DeptCode in (Select DeptCode From TblReturnARDownpayment Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='ReturnARDownpayment' ");
            SQL.AppendLine("And A.UserCode Is Null ");
            SQL.AppendLine("And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo From TblReturnARDownpayment ");
            SQL.AppendLine("    Where IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsReturnARDownpaymentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Service Charge Deduction 15/02/2018

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("    And B.SiteCode In (Select SiteCode From TblSCIDeductionHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='SCIDeduction' ");
            SQL.AppendLine("And A.UserCode Is Null ");
            SQL.AppendLine("And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select DocNo From TblSCIDeductionHdr ");
            SQL.AppendLine("    Where IfNull(Status, 'O')='O' ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsSCIDeductionApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region WO Request

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            if (Sm.GetParameterBoo("IsWORApprovalBySiteMandatory"))
            {
                SQL.AppendLine("        And B.SiteCode In ( ");
                SQL.AppendLine("            Select T3.SiteCode ");
                SQL.AppendLine("            From TblWOR T1 ");
                SQL.AppendLine("            Inner Join TblTOHdr T2 On T1.TOCOde=T2.AssetCode ");
                SQL.AppendLine("            Inner Join TblLocation T3 On T2.LocCode=T3.LocCode ");
                SQL.AppendLine("            Where T1.DocNo=A.DocNo ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("Where A.DocType='WORequest' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblWOR ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And Status Is Not Null ");
            SQL.AppendLine("    And Status='O' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsWORequestApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Request for Loan to Partner

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='RequestLP' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblRequestLP ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And Status Is Not Null ");
            SQL.AppendLine("    And Status='O' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsRequestLPApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Survey

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='Survey' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblSurvey ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And Status Is Not Null ");
            SQL.AppendLine("    And Status='O' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsSurveyApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Partner's Loan Payment

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='PLP' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblPLPHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And Status Is Not Null ");
            SQL.AppendLine("    And Status='O' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPLPApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region DO To Customer

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='DOCt' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblDOCtHdr ");
            SQL.AppendLine("    Where Status Is Not Null ");
            SQL.AppendLine("    And Status='O' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsDOCtApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region BOQ

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("And B.SiteCode In ( ");
            SQL.AppendLine("    Select Distinct T2.SiteCode ");
            SQL.AppendLine("    From TblBOQHdr T1 ");
            SQL.AppendLine("    Inner Join TblLOPHdr T2 On T1.LOPDocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.DocNo=A.DocNo ");
            SQL.AppendLine("    And T1.ActInd='Y' ");
            SQL.AppendLine("    And T1.Status='O' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Inner Join TblBOQHdr C On A.DocNo=C.DocNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.DocType='BOQ' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblBOQHdr ");
            SQL.AppendLine("    Where Status Is Not Null ");
            SQL.AppendLine("    And Status='O' ");
            SQL.AppendLine("    And ActInd = 'Y' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsBOQApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region LOP

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            if (Sm.IsApprovalNeedXCode("LOP", "Site"))
                SQL.AppendLine("And B.SiteCode Is Not Null And B.SiteCode In (Select SiteCode From TblLOPHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Inner Join TblLOPHdr C On A.DocNo=C.DocNo And C.CancelInd = 'N' And C.ProcessInd = 'P' ");
            SQL.AppendLine("Where A.DocType='LOP' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblLOPHdr ");
            SQL.AppendLine("    Where Status Is Not Null ");
            SQL.AppendLine("    And Status = 'O' ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And ProcessInd = 'P' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsLOPApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Project Implementation

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            if (mIsProjectImplementationApprovalBySiteMandatory)
            {
                SQL.AppendLine("        And B.SiteCode Is Not Null ");
                SQL.AppendLine("        And B.SiteCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X4.SiteCode ");
                SQL.AppendLine("            From TblProjectImplementationHdr X1 ");
                SQL.AppendLine("            Inner Join TblSOContractRevisionHdr X11 On X1.SOContractDocNo = X11.DocNo ");
                SQL.AppendLine("            Inner Join TblSOContractHdr X2 On X11.SOCDocNo = X2.DocNo ");
                SQL.AppendLine("            Inner Join TblBOQHdr X3 On X2.BOQDocNo = X3.DocNo ");
                SQL.AppendLine("            Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo ");
                SQL.AppendLine("            Where X1.DocNo = A.DocNo ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("Inner Join TblProjectImplementationHdr C On A.DocNo = C.DocNo And C.CancelInd = 'N' And processInd = 'F' ");
            SQL.AppendLine("Where A.DocType='ProjectImplementation' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblProjectImplementationHdr ");
            SQL.AppendLine("    Where Status Is Not Null ");
            SQL.AppendLine("    And Status = 'O' ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsProjectImplementationApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Employee Claim 03/09/2018

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            if (Sm.IsDataExist(
                    "Select 1 From TblDocApprovalSetting " +
                    "Where DocType='EmpClaim' And DeptCode Is Not Null Limit 1;"))
            {
                SQL.AppendLine("   And B.DeptCode In ( ");
                SQL.AppendLine("       Select Distinct T2.DeptCode ");
                SQL.AppendLine("       From TblEmpClaimHdr T1 ");
                SQL.AppendLine("       Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
                SQL.AppendLine("       Where T1.DocNo=A.DocNo ");
                SQL.AppendLine("       And T1.CancelInd='N' ");
                SQL.AppendLine("       And T1.Status='O' ");
                SQL.AppendLine("   ) ");
            }
            if (Sm.IsDataExist(
                    "Select 1 From TblDocApprovalSetting " +
                    "Where DocType='EmpClaim' And SiteCode Is Not Null Limit 1;"))
            {
                SQL.AppendLine("   And B.SiteCode In ( ");
                SQL.AppendLine("       Select Distinct T2.SiteCode ");
                SQL.AppendLine("       From TblEmpClaimHdr T1 ");
                SQL.AppendLine("       Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
                SQL.AppendLine("       Where T1.DocNo=A.DocNo ");
                SQL.AppendLine("       And T1.CancelInd='N' ");
                SQL.AppendLine("       And T1.Status='O' ");
                SQL.AppendLine("   ) ");
            }
            SQL.AppendLine("Inner Join TblEmpClaimHdr C On A.DocNo=C.DocNo And C.CancelInd = 'N' And C.Status = 'O' ");
            SQL.AppendLine("Where A.DocType='EmpClaim' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("Case When Exists( ");
            SQL.AppendLine("   Select ParValue From TblParameter ");
            SQL.AppendLine("   Where ParCode='IsEmpClaimApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("   ) Then 1 ");
            SQL.AppendLine("Else   ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("       Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("       And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("       And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("    ) Then 0 Else 1 End ");
            SQL.AppendLine("End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region 27/08/2018 Training Request

            SQL.AppendLine("Select Count(1) As RecCount  ");
            SQL.AppendLine("From TblDocApproval A  ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode  ");
            if (Sm.IsDataExist(
                    "Select 1 From TblDocApprovalSetting " +
                    "Where DocType='TrainingRequest' And DeptCode Is Not Null Limit 1;"))
            {
                SQL.AppendLine("   And B.DeptCode In ( ");
                SQL.AppendLine("       Select Distinct T2.DeptCode ");
                SQL.AppendLine("       From TblTrainingRequestDtl2 T1 ");
                SQL.AppendLine("       Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
                SQL.AppendLine("       Where T1.DocNo=A.DocNo ");
                SQL.AppendLine("       And T1.CancelInd='N' ");
                SQL.AppendLine("       And T1.Status='O' ");
                SQL.AppendLine("   ) ");
            }
            if (Sm.IsDataExist(
                    "Select 1 From TblDocApprovalSetting " +
                    "Where DocType='TrainingRequest' And SiteCode Is Not Null Limit 1;"))
            {
                SQL.AppendLine("   And B.SiteCode In ( ");
                SQL.AppendLine("       Select Distinct T2.SiteCode ");
                SQL.AppendLine("       From TblTrainingRequestDtl2 T1 ");
                SQL.AppendLine("       Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
                SQL.AppendLine("       Where T1.DocNo=A.DocNo ");
                SQL.AppendLine("       And T1.CancelInd='N' ");
                SQL.AppendLine("       And T1.Status='O' ");
                SQL.AppendLine("   ) ");
            }
            SQL.AppendLine("Inner Join TblTrainingRequestHdr C On A.DocNo=C.DocNo And C.CancelInd = 'N'  ");
            SQL.AppendLine("Where A.DocType='TrainingRequest' And A.UserCode Is Null And A.Status Is Null  ");
            SQL.AppendLine("And Exists(  ");
            SQL.AppendLine("    Select 1 From TblTrainingRequestHdr A ");
            SQL.AppendLine("     Inner Join TblTrainingRequestDtl2 B On A.Docno = B.DocNo ");
            SQL.AppendLine("    Where B.Status Is Not Null  ");
            SQL.AppendLine("    And B.Status = 'O'  ");
            SQL.AppendLine("    And A.CancelInd = 'N'  ");
            SQL.AppendLine("    And B.DocNo=A.DocNo  ");
            SQL.AppendLine("    )  ");
            SQL.AppendLine("   And 1=  ");
            SQL.AppendLine("   Case When Exists(  ");
            SQL.AppendLine("       Select 1 From TblParameter  ");
            SQL.AppendLine("       Where ParCode='IstrainingRequestApprovalInSequence' And ParValue='N'  ");
            SQL.AppendLine("       ) Then 1  ");
            SQL.AppendLine("   Else    ");
            SQL.AppendLine("       Case When Exists(  ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2  ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo  ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level  ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1  ");
            SQL.AppendLine("        ) Then 0 Else 1 End  ");
            SQL.AppendLine("   End  ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Bonus

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("        And B.SiteCode Is Not Null ");
            SQL.AppendLine("        And B.SiteCode In ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select X1.SiteCode ");
            SQL.AppendLine("            From TblBonusHdr X1 ");
            SQL.AppendLine("            Where X1.DocNo = A.DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("Inner Join TblBonusHdr C On A.DocNo = C.DocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("Where A.DocType='Bonus' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblBonusHdr ");
            SQL.AppendLine("    Where Status Is Not Null ");
            SQL.AppendLine("    And Status = 'O' ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsBonusApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region SOCRev 12/11/2018

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Where A.DocType='SOCRev' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblSOContractRevisionHdr ");
            SQL.AppendLine("    Where Status Is Not Null ");
            SQL.AppendLine("    And Status = 'O' ");
            SQL.AppendLine("    And DocNo=A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsSOCRevApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Dropping Request 28/06/2019

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("        On A.DocType=B.DocType ");
            SQL.AppendLine("        And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        And B.UserCode Like @UserCode ");
            SQL.AppendLine("    Inner Join TblDroppingRequestHdr C On A.DOcNo = C.DOcNo And C.CancelInd = 'N' ");
            SQL.AppendLine("    Where A.DocType='DroppingRequest' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("    And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsDroppingRequestApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("        ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("Union All ");

            #endregion

            #region Budget Request 08/08/2019

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("        On A.DocType=B.DocType ");
            SQL.AppendLine("        And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        And B.UserCode Like @UserCode ");
            SQL.AppendLine("        And B.DeptCode Is Not Null ");
            SQL.AppendLine("        And B.DeptCode In (Select DeptCode From TblBudgetRequestHdr Where DocNo = A.DocNo) ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr C On A.DOcNo = C.DOcNo And C.CancelInd = 'N' ");
            SQL.AppendLine("    Where A.DocType='BudgetRequest2' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("    And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsBudgetRequest2ApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("        ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Budget 08/08/2019

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("        On A.DocType=B.DocType ");
            SQL.AppendLine("        And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        And B.UserCode Like @UserCode ");
            SQL.AppendLine("        And B.DeptCode Is Not Null ");
            SQL.AppendLine("        And B.DeptCode In (Select X2.DeptCode From TblBudgetHdr X1 Inner Join TblBudgetRequestHdr X2 On X2.DocNo = X1.BudgetRequestDocNo Where X1.DocNo = A.DocNo) ");
            SQL.AppendLine("    Inner Join TblBudgetHdr C On A.DOcNo = C.DOcNo  ");
            SQL.AppendLine("    Where A.DocType='Budget2' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("    And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsBudget2ApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("        ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region NTP 23/09/2019

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("        On A.DocType=B.DocType ");
            SQL.AppendLine("        And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        And B.UserCode Like @UserCode ");
            SQL.AppendLine("    Inner Join TblNoticeToProceed C On A.DocNo = C.DOcNo And C.CancelInd = 'N'  ");
            SQL.AppendLine("    Where A.DocType='NTP' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("    And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsNtpApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("        ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Dropping Payment 24/09/2019

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct A.DocNo ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("        On A.DocType=B.DocType ");
            SQL.AppendLine("        And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        And B.UserCode Like @UserCode ");
            SQL.AppendLine("    Inner Join TblDroppingPaymentHdr C On A.DocNo = C.DOcNo And C.CancelInd = 'N' And C.Status = 'O' ");
            SQL.AppendLine("    Inner Join TblDroppingRequestHdr D On C.DRQDocNo = D.DocNo ");
            SQL.AppendLine("    Inner Join TblProjectImplementationHdr E On D.PRJIDocNo = E.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractRevisionHdr F On E.SOContractDocNo = F.DocNo ");
            SQL.AppendLine("    Inner JOin TblSOContractHdr G On F.SOCDocNo = G.DocNo ");
            SQL.AppendLine("    Inner Join TblBOQHdr H On G.BOQDocNo = H.DocNo ");
            SQL.AppendLine("    Inner Join TblLOPHdr I On H.LOPDocNo = I.DocNo ");
            SQL.AppendLine("        And B.SiteCode Is Not Null ");
            SQL.AppendLine("        And B.SiteCode = I.SiteCode ");
            SQL.AppendLine("    Where A.DocType='DroppingPayment' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("    And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsDroppingPaymentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("        ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine(" Union All ");

            SQL.AppendLine("    Select Distinct A.DocNo ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("        On A.DocType=B.DocType ");
            SQL.AppendLine("        And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        And B.UserCode Like @UserCode ");
            SQL.AppendLine("    Inner Join TblDroppingPaymentHdr C On A.DocNo = C.DOcNo And C.CancelInd = 'N' And C.Status = 'O' ");
            SQL.AppendLine("    Inner Join TblDroppingRequestHdr D On C.DRQDocNo = D.DocNo ");
            SQL.AppendLine("        And D.DeptCode Is Not Null ");
            SQL.AppendLine("        And B.DeptCode Is Not Null ");
            SQL.AppendLine("        And B.DeptCode = D.DeptCode ");
            SQL.AppendLine("    Where A.DocType='DroppingPayment' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("    And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsDroppingPaymentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("        ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine(") T ");

            SQL.AppendLine("Union All ");

            #endregion

            #region NTP 23/09/2019

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("        On A.DocType=B.DocType ");
            SQL.AppendLine("        And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        And B.UserCode Like @UserCode ");
            SQL.AppendLine("    Inner Join TblVoucherRequestExternalPayrollHdr C On A.DocNo = C.DocNo And C.CancelInd = 'N' And C.Status = 'O' ");
            SQL.AppendLine("    Where A.DocType='VoucherRequestExternalPayroll' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("    And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsVoucherRequestExternalPayrollApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("        ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Cash Advance Settlement

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='CashAdvanceSettlement' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select 1 From TblCashAdvanceSettlementHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsCashAdvanceSettlementApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Project Implementation Revision

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            if (mIsProjectImplementationApprovalBySiteMandatory)
            {
                SQL.AppendLine("        And B.SiteCode Is Not Null ");
                SQL.AppendLine("        And B.SiteCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X4.SiteCode ");
                SQL.AppendLine("            From TblProjectImplementationRevisionHdr X0 ");
                SQL.AppendLine("            Inner Join TblProjectImplementationHdr X1 On X0.PRJIDocNo = X1.DocNo ");
                SQL.AppendLine("            Inner Join TblSOContractRevisionHdr X11 On X1.SOContractDocNo = X11.DocNo ");
                SQL.AppendLine("            Inner Join TblSOContractHdr X2 On X11.SOCDocNo = X2.DocNo ");
                SQL.AppendLine("            Inner Join TblBOQHdr X3 On X2.BOQDocNo = X3.DocNo ");
                SQL.AppendLine("            Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo ");
                SQL.AppendLine("            Where X0.DocNo = A.DocNo ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("Inner Join TblProjectImplementationRevisionHdr C On A.DocNo = C.DocNo And C.Status = 'O' ");
            SQL.AppendLine("Where A.DocType='ProjectImplementationRev' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsProjectImplementationRevApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Project Delivery

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("        And B.SiteCode Is Not Null ");
            SQL.AppendLine("        And B.SiteCode In ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select X4.SiteCode ");
            SQL.AppendLine("            From TblProjectDeliveryHdr X0 ");
            SQL.AppendLine("            Inner Join TblProjectImplementationHdr X1 On X0.PRJIDocNo = X1.DocNo ");
            SQL.AppendLine("            Inner Join TblSOContractRevisionHdr X11 On X1.SOContractDocNo = X11.DocNo ");
            SQL.AppendLine("            Inner Join TblSOContractHdr X2 On X11.SOCDocNo = X2.DocNo ");
            SQL.AppendLine("            Inner Join TblBOQHdr X3 On X2.BOQDocNo = X3.DocNo ");
            SQL.AppendLine("            Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo ");
            SQL.AppendLine("            Where X0.DocNo = A.DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("Inner Join TblProjectDeliveryHdr C On A.DocNo = C.DocNo And C.Status = 'O' And C.CancelInd = 'N' ");
            SQL.AppendLine("Where A.DocType='ProjectDelivery' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsProjectDeliveryApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Asset Transfer

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Inner Join TblAssetTransfer C On A.DocNo = C.DocNo And C.Status = 'O' ");
            SQL.AppendLine("Where A.DocType='AssetTransfer' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsAssetTransferApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Stock Inbound

            bool mIsStockInboundApprovalBasedOnWhs = Sm.GetParameterBoo("IsStockInboundApprovalBasedOnWhs");

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Inner Join TblStockInboundHdr C On A.DocNo = C.DocNo And C.Status = 'O' ");
            if (mIsStockInboundApprovalBasedOnWhs)
                SQL.AppendLine("    And C.WhsCode = B.WhsCode ");
            SQL.AppendLine("Where A.DocType='StockInbound' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsStockInboundApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Stock Outbound

            bool mIsStockOutboundApprovalBasedOnWhs = Sm.GetParameterBoo("IsStockOutboundApprovalBasedOnWhs");

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Inner Join TblStockOutboundHdr C On A.DocNo = C.DocNo And C.Status = 'O' ");
            SQL.AppendLine("Where A.DocType='StockOutbound' And A.UserCode Is Null And A.Status Is Null ");
            if (mIsStockOutboundApprovalBasedOnWhs)
                SQL.AppendLine("    And C.WhsCode = B.WhsCode ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsStockOutboundApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Sales Memo 28/04/2020

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("   From TblDocApproval A ");
            SQL.AppendLine("   Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("       On A.DocType=B.DocType ");
            SQL.AppendLine("       And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("       And B.UserCode Like @UserCode ");
            SQL.AppendLine("       And B.SiteCode In (Select SiteCode From TblSalesMemoHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("   Where A.DocType='SalesMemo' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsSalesMemoApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("Union All ");

            #endregion

            #region Drawing Approval 29/06/2020

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("        On A.DocType=B.DocType ");
            SQL.AppendLine("        And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        And B.UserCode Like @UserCode ");
            SQL.AppendLine("    Inner Join TblDrawingApprovalHdr C On A.DocNo = C.DocNo And C.CancelInd = 'N' And C.Status = 'O' ");
            SQL.AppendLine("    Where A.DocType='DrawingApproval' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("    And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select 1 From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsDrawingApprovalApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("        ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("Union All ");

            #endregion

            #region Project Budget Resource 02/12/2020

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("        On A.DocType=B.DocType ");
            SQL.AppendLine("        And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        And B.UserCode Like @UserCode ");
            SQL.AppendLine("    Inner Join TblProjectBudgetResourceHdr C On A.DocNo = C.DOcNo And C.CancelInd = 'N' And C.Status = 'O' ");
            SQL.AppendLine("    Where A.DocType='ProjectBudgetResource' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("    And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsProjectBudgetResourceApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("        ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("Union All ");

            #endregion

            #region BOM3 07/12/2020

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("    From TblDocApproval A ");
            SQL.AppendLine("    Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("        On A.DocType=B.DocType ");
            SQL.AppendLine("        And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("        And B.DeptCode Is Not Null ");
            SQL.AppendLine("        And B.UserCode Like @UserCode ");
            SQL.AppendLine("    Inner Join TblBOm2Hdr C On A.DocNo = C.DOcNo And C.CancelInd = 'N' And C.Status = 'O' And B.DeptCode = C.DeptCode ");
            SQL.AppendLine("    Where A.DocType='ProjectBudgetResource' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("    And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='IsBom3ApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("        ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("Union All ");

            #endregion

            #region DO Request From Department

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("On A.DocType=B.DocType ");
            SQL.AppendLine("   And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("   And B.UserCode Like @UserCode ");
            SQL.AppendLine("   And B.DeptCode = (Select DeptCode From TblDORequestDeptHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='DORequestDept' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblDORequestDeptDtl Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo And DNo=A.DNo) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsDORequestDeptApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("Union All ");

            #endregion           

            #region CBP Profit Loss Revision 08/10/2021

            SQL.AppendLine("Select Count(1) As RecCount ");
            SQL.AppendLine("   From TblDocApproval A ");
            SQL.AppendLine("   Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("       On A.DocType=B.DocType ");
            SQL.AppendLine("       And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("       And B.UserCode Like @UserCode ");
            SQL.AppendLine("   Where A.DocType='CompanyBudgetPlanProfitLossRev' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("   And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsCompanyBudgetPlanProfitLossRevInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("Union All ");

            #endregion

            #region Journal Memorial

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("On A.DocType=B.DocType ");
            SQL.AppendLine("   And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("   And B.UserCode Like @UserCode ");
            SQL.AppendLine("   And B.DeptCode = (Select DeptCode From TblJournalMemorialHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='JournalMemorial' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblJournalMemorialDtl Where DocNo=A.DocNo And DNo=A.DNo) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsJournalMemorialApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region KPI

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='KPI' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select 1 From TblKPIHdr Where IfNull(Status, 'O')='O' And DocNo=A.DocNo And ActInd = 'Y') ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsKPIApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Goals Setting

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='GoalsSetting' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select 1 From TblGoalsSettingHdr Where IfNull(Status, 'O')='O' And DocNo=A.DocNo And ActInd = 'Y') ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsGoalsSettingInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Goals Performance Review

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='PerformanceReview2' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select 1 From TblNewPerformanceReviewHdr Where IfNull(Status, 'O')='O' And DocNo=A.DocNo And CancelInd = 'Y') ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select 1 From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPerformanceReviewInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Purchase Invoice

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType = B.DocType And A.ApprovalDNo = B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType = 'PurchaseInvoice' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select 1 From TblPurchaseInvoiceHdr Where IfNull(Status, 'O')='O' And DocNo = A.DocNo And CancelInd = 'N') ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("    Case When Exists( ");
            SQL.AppendLine("        Select 1 From TblParameter ");
            SQL.AppendLine("        Where ParCode = 'IsPurchaseInvoiceInSequence' And ParValue ='N' ");
            SQL.AppendLine("    ) Then 1 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select 1 From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("            Where T1.DocType= A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("            And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("            And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Union All ");
            #endregion

            #region Voucher Request Petty Cash
            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("    And B.DeptCode = (Select DeptCode From TblVoucherRequestHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='VoucherRequestPettyCash' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblVoucherRequestHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsVoucherRequestPettyCashApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("Union All ");

            #endregion

            #region Net Off Payment 
            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='NetOffPayment' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblNetOffPaymentHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsNetOffPaymentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");
            #endregion

            #region Petty Cash Disbursement
            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("--    And B.DeptCode = (Select DeptCode From TblPettyCashDisbursementHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='PettyCashDisbursement' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblPettyCashDisbursementHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPettyCashDisbursementApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");

            #endregion

            #region Property Inventory
            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("--    And B.DeptCode = (Select DeptCode From TblPropertyInventoryHdr Where PropertyCode=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='PropertyInventory' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblPropertyInventoryHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPropertyInventoryApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            SQL.AppendLine("Union All ");
            #endregion

            #region Property Inventory Cost Component
            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("--    And B.DeptCode = (Select DeptCode From TblPropertyInventoryCostComponentHdr Where DocNo=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='PropertyInventoryCostComponent' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblPropertyInventoryCostComponentHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPropertyInventoryCostComponentApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("Union All ");

            #endregion

            #region Property Inventory Mutation

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='PropertyInventoryMutation' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblPropertyInventoryMutationHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPropertyInventoryMutationApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("   Union All ");

            #endregion

            #region Service Delivery

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Inner Join TblServiceDeliveryHdr C On A.DocNo = C.DocNo And C.Status = 'O' ");
            SQL.AppendLine("Where A.DocType='ServiceDelivery' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsServiceDeliveryApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("   Union All ");

            #endregion

            #region Budget Transfer

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Inner Join TblBudgetTransferHdr C On A.DocNo = C.DocNo And C.Status = 'O' And C.Status = 'N' ");
            SQL.AppendLine("Where A.DocType='BudgetTransfer' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsBudgetTransferApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("   Union All ");

            #endregion

            #region Yearly Closing Journal Entry

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("Where A.DocType='YearlyClosingJournalEntry' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblYearlyClosingJournalEntryHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsYearlyClosingJournalEntryApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");
            SQL.AppendLine("   Union All ");

            #endregion

            #region Property Inventory Transfer
            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B ");
            SQL.AppendLine("    On A.DocType=B.DocType ");
            SQL.AppendLine("    And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("    And B.UserCode Like @UserCode ");
            SQL.AppendLine("--    And B.DeptCode = (Select DeptCode From TblPropertyInventoryTransferHdr Where PropertyCode=A.DocNo) ");
            SQL.AppendLine("Where A.DocType='PropertyInventoryTransfer' And A.UserCode Is Null And A.Status Is Null ");
            SQL.AppendLine("And Exists(Select DocNo From TblPropertyInventoryTransferHdr Where CancelInd='N' And IfNull(Status, 'O')='O' And DocNo=A.DocNo ) ");
            SQL.AppendLine("And 1= ");
            SQL.AppendLine("   Case When Exists( ");
            SQL.AppendLine("       Select ParValue From TblParameter ");
            SQL.AppendLine("       Where ParCode='IsPropertyInventoryTransferApprovalInSequence' And ParValue='N' ");
            SQL.AppendLine("       ) Then 1 ");
            SQL.AppendLine("   Else   ");
            SQL.AppendLine("       Case When Exists( ");
            SQL.AppendLine("           Select T1.DocType From TblDocApproval T1, TblDocApprovalSetting T2 ");
            SQL.AppendLine("           Where T1.DocType=A.DocType And T1.DocNo=A.DocNo And T1.DNo=A.DNo ");
            SQL.AppendLine("           And T1.DocType=T2.DocType And T1.ApprovalDNo=T2.DNo And T2.Level<B.Level ");
            SQL.AppendLine("           And IfNull(T1.Status, 'X')<>'A' Limit 1 ");
            SQL.AppendLine("        ) Then 0 Else 1 End ");
            SQL.AppendLine("   End ");

            #endregion

            SQL.AppendLine(") T;");

            return SQL.ToString();
        }

        private string GetSQLOutstandingCancellationPONotify()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Count(A.DocNo) As RecCount ");
            SQL.AppendLine("From TblPOQtyCancel A");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On C.MaterialRequestDocNo=E.DocNo And C.MaterialRequestDNo=E.DNo And E.CreateBy=@UserCode ");
            SQL.AppendLine("Where A.CancelInd='N' And A.NewMRInd='Y' And A.ProcessInd='O';");

            return SQL.ToString();
        }

        private string GetSQLEmpContractDocNotify()
        {
            string DtNow = Sm.ServerCurrentDateTime().Substring(0, 8);
            string Interval = String.Concat("-", Sm.GetParameter("EmpContractIntervalDay"));

            var SQL = new StringBuilder();

            if (mEmpEndContractBenchmark == "1")
            {
                SQL.AppendLine("Select Count(DocNo) As RecCount ");
                SQL.AppendLine("From TblEmpContractDoc A ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("    And (B.ResignDt Is Null Or (B.ResignDt Is Not Null And B.ResignDt>'" + DtNow + "')) ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select EmpCode, MAX(CreateDt) As CreateDt ");
                SQL.AppendLine("    from TblEmpContractDoc ");
                SQL.AppendLine("    Where ActInd = 'Y' ");
                SQL.AppendLine("    Group By EmpCode ");
                SQL.AppendLine("    ) C On A.EmpCode =  C.EmpCode And A.CreateDt = C.CreateDt ");
                SQL.AppendLine("Inner Join TblUserNotify D On D.DocCode = '01' And D.UserCode=@UserCode ");
                SQL.AppendLine("Inner Join TblUser E On D.UserCode=E.UserCode And E.NotifyInd = 'Y' ");
                SQL.AppendLine("Where A.ActInd = 'Y' And ('" + DtNow + "' between DATE_FORMAT(DATE_ADD(A.EndDt, INTERVAL " + Interval + " DAY), '%Y%m%d') And A.EndDt); ");
            }
            else
            {
                SQL.AppendLine("SELECT COUNT(A.DocNo) AS RecCount ");
                SQL.AppendLine("FROM TblEmpContractDoc A ");
                SQL.AppendLine("INNER JOIN TblParameter B ON B.ParCode = 'EmpContractIntervalDay' AND LENGTH(B.ParValue) > 0 AND B.ParValue > 0 ");
                SQL.AppendLine("INNER JOIN TblParameter C ON C.ParCode = 'GrpCodeForEmpEndContractNotification' AND C.ParValue IS NOT NULL ");
                SQL.AppendLine("WHERE A.EndDt IS NOT NULL AND A.ActInd = 'Y' ");
                SQL.AppendLine("AND (A.EndDt BETWEEN DATE_FORMAT(NOW(), '%Y%m%d') AND DATE_FORMAT(DATE_ADD(NOW(), INTERVAL B.ParValue DAY), '%Y%m%d')) ");
                SQL.AppendLine("AND EXISTS ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT 1 ");
                SQL.AppendLine("    FROM TblUser ");
                SQL.AppendLine("    WHERE UserCode = '" + Gv.CurrentUserCode + "' ");
                SQL.AppendLine("    AND FIND_IN_SET(GrpCode, C.ParValue) ");
                SQL.AppendLine("); ");
            }

            return SQL.ToString();
        }

        private string GetSQLProductionOrderNotify()
        {
            var SQL = new StringBuilder();
            var NoOfDaysForProdOrderNotify = Sm.GetParameter("NoOfDaysForProdOrderNotify");
            if (NoOfDaysForProdOrderNotify.Length == 0) NoOfDaysForProdOrderNotify = "30";

            SQL.AppendLine("Select Count(*) As Val ");
            SQL.AppendLine("From TblProductionOrderHdr A ");
            SQL.AppendLine("Inner Join (Select " + NoOfDaysForProdOrderNotify + " As NoOfDaysForProdOrderNotify) B On 1=1 ");
            SQL.AppendLine("Inner Join (Select Replace(CurDate(), '-', '') As CurrentDt) C On Replace(DATE_ADD(STR_TO_DATE(A.UsageDt,'%Y%m%d'), INTERVAL -1*B.NoOfDaysForProdOrderNotify DAY), '-', '')<=C.CurrentDt ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.UsageDt Is Not Null ");
            SQL.AppendLine("And A.ProcessInd='O'; ");

            return SQL.ToString();
        }

        private string GetSQLOutstandingSOContractNotify()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT COUNT(A.Docno) countOutstandingSOC");
            SQL.AppendLine("FROM TblSOContractHdr A ");
            SQL.AppendLine("WHERE A.DocNo NOT IN ( ");
            SQL.AppendLine("    SELECT SOContractDocno  ");
            SQL.AppendLine("    FROM tblBOM2Hdr ");
            SQL.AppendLine("    Where CancelInd='N' AND (`Status`='O' OR `Status`='A') ");
            SQL.AppendLine(")");

            return SQL.ToString();
        }

        private string GetApprovalNotify()
        {
            var cm = new MySqlCommand() { CommandText = mSQLApprovalNotify };
            Sm.CmParam<String>(ref cm, "@UserCode", "%#" + Gv.CurrentUserCode + "#%");

            return Sm.GetValue(cm);
        }

        private string GetOutstandingCancellationPONotify()
        {
            var cm = new MySqlCommand() { CommandText = mSQLOutstandingCancellationPONotify };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return Sm.GetValue(cm);
        }

        private string GetEmpContractDocNotify()
        {
            var cm = new MySqlCommand() { CommandText = mSQLEmpContractDocNotify };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return Sm.GetValue(cm);
        }

        private string GetOutstandingSOContractNotify()
        {
            var cm = new MySqlCommand() { CommandText = mSQLOutstandingSOContractNotify };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return Sm.GetValue(cm);
        }

        private bool IsMaterialRequestApprovalByDeptEnabled()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='MaterialRequest2' And DeptCode Is Not Null Limit 1; ");
        }

        private void ProcessNotification()
        {
            toolStripStatusLabel1.Text = string.Empty;

            var RecCountApproval = "0";
            var RecCountOutstandingCancellationPO = "0";
            var RecCountEmpContractDoc = "0";
            var RecCountProductionOrder = "0";
            var RecCountOutstandingSOContract = "0";

            if (mIsApprovalNotify)
                RecCountApproval = GetApprovalNotify();
            if (mIsOutstandingCancellationPONotify) RecCountOutstandingCancellationPO = GetOutstandingCancellationPONotify();
            if (mIsEmpContractDocNotify) RecCountEmpContractDoc = GetEmpContractDocNotify();
            if (mIsProductionOrderNotifyEnabled) RecCountProductionOrder = Sm.GetValue(mSQLProductionOrderNotify);
            if (mIsOutstandingSOContractNotify)
                RecCountOutstandingSOContract = GetOutstandingSOContractNotify();

            bool IsShowApproval = (RecCountApproval.Length > 0 && RecCountApproval != "0");
            bool IsShowOutstandingCancellationPO = (RecCountOutstandingCancellationPO.Length > 0 && RecCountOutstandingCancellationPO != "0");
            bool IsShowEmpContractDoc = (RecCountEmpContractDoc.Length > 0 && RecCountEmpContractDoc != "0");
            bool IsShowOutstandingSOContract = (RecCountOutstandingSOContract.Length > 0 && RecCountOutstandingSOContract != "0");

            var Msg = string.Empty;

            if (IsShowApproval)
                Msg = "Outstanding Approval: " + (Int32.Parse(RecCountApproval)).ToString() + ".";

            if (IsShowOutstandingCancellationPO)
            {
                if (Msg.Length > 0) Msg += " ";
                Msg += " Outstanding Cancellation PO: " + (Int32.Parse(RecCountOutstandingCancellationPO)).ToString() + ".";
            }

            if (IsShowEmpContractDoc)
            {
                if (Msg.Length > 0) Msg += " ";
                Msg += " Employee Contract End : " + (Int32.Parse(RecCountEmpContractDoc)).ToString() + ".";
            }

            if (mIsProductionOrderNotifyEnabled && RecCountProductionOrder.Length > 0 && RecCountProductionOrder != "0")
            {
                if (Msg.Length > 0) Msg += " ";
                Msg += " Outstanding Production Order : " + (Int32.Parse(RecCountProductionOrder)).ToString() + ".";
            }

            if (mIsOutstandingSOContractNotify && RecCountOutstandingSOContract.Length > 0 && RecCountOutstandingSOContract != "0")
            {
                if (Msg.Length > 0) Msg += " ";
                Msg += " Outstanding SO Contract : " + (Int32.Parse(RecCountOutstandingSOContract)).ToString() + ".";
            }

            if (Msg.Length > 0)
            {
                toolStripStatusLabel1.BackColor = Color.Red;
                toolStripStatusLabel1.Text =
                    String.Format("{0:dd-MMM-yyyy HH:mm}", DateTime.Now) + "->" + Msg;
                Console.Beep(1000, 500);
            }
            else
            {
                toolStripStatusLabel1.BackColor = Color.Transparent;
                toolStripStatusLabel1.Text = string.Empty;
            }
            statusStrip1.Refresh();
        }

        private void UpdateNotification(object sender, EventArgs e)
        {
            ProcessNotification();
        }

        internal void StartTimerAutoLogOff()
        {
            TmAutoLogOff.Enabled = true;
            TmAutoLogOff.Start();
        }

        private void SetTmAutoLogOffInterval()
        {
            TmAutoLogOff.Interval = 1000;
        }

        #endregion

        #region POS

        private void ShiftClosing()
        {
            if (Sm.GetPosSetting("StsShift") == "C")
                Sm.StdMsg(mMsgType.Warning, "Unable to Perform Shift Closing. " + Environment.NewLine +
                    "There''s no Opened Shift");
            else
            {
                string CurrentShiftOpen = Sm.GetPosSetting("CurrentShift");
                string CurrentUserLog = Sm.GetPosSetting("CurrentUser");
                if (CurrentUserLog != Gv.CurrentUserCode)
                    Sm.StdMsg(mMsgType.Warning, "Unable to Perform Shift Closing. " + Environment.NewLine +
                        "You are not authorized to do shift closing." + Environment.NewLine +
                        "Current Open Shift is " + CurrentShiftOpen + ", Logged By " + CurrentUserLog);
                else
                {
                    if (Sm.StdMsgYN("Question",
                        "Current Open Shift is " + CurrentShiftOpen + Environment.NewLine +
                        "Do you really want to perform Shift Closing ?") == DialogResult.Yes)
                        DoShiftClosing();
                }
            }
        }

        private void DayEndClosing()
        {
            string CurrentBusinessDate = Sm.GetPosSetting("CurrentBnsDate");

            if (IsDataPosAlreadyProcessed(CurrentBusinessDate))
            {
                return;
            }

            string CurrentShiftOpen = Sm.GetPosSetting("CurrentShift");
            string CurrentUserLog = Sm.GetPosSetting("CurrentUser");
            if (Sm.GetPosSetting("StsDayEnd") == "C")
                Sm.StdMsg(mMsgType.Warning, "Unable to Perform Day End Closing. " + Environment.NewLine +
                    "There''s no Active Business Date.");
            else
                if (Sm.GetPosSetting("StsShift") == "O")
                    Sm.StdMsg(mMsgType.Warning, "Unable to Perform Day End Closing. " + Environment.NewLine +
                        "There's an Opened Shift. Please Perform Shift Closing first." + Environment.NewLine +
                        "Current Open Shift is " + CurrentShiftOpen + ", Logged By " + CurrentUserLog);
                else
                {
                    if (Sm.StdMsgYN("Question",
                        "Current Business Date is " + CurrentBusinessDate.Substring(6, 2) + "-" + CurrentBusinessDate.Substring(4, 2) + "-" + CurrentBusinessDate.Substring(0, 4) + Environment.NewLine +
                        "Do you really want to perform Day End Closing ?") == DialogResult.Yes)
                        DoDayEndClosing();
                }
        }

        private bool IsDataPosAlreadyProcessed(string CurrentBusinessDate)
        {
            var cm = new MySqlCommand() { CommandText = "Select BsDate From TblPosProcess Where BsDate=@BsDate And PosNo=@PosNo Limit 1;" };
            Sm.CmParam<String>(ref cm, "@BsDate", CurrentBusinessDate);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);

            if (Sm.IsDataExist(cm))
            {
                CurrentBusinessDate = CurrentBusinessDate.Substring(6, 2) + "-" + Sm.MonthName(CurrentBusinessDate.Substring(4, 2)) + "-" + Sm.Left(CurrentBusinessDate, 4);
                Sm.StdMsg(mMsgType.Warning,
                    "Business Date : " + CurrentBusinessDate + Environment.NewLine +
                    "This data already processed.");
                return true;
            }
            return false;
        }

        private void PrintShiftClosing(string ShiftNumber)
        {
            try
            {
                CurrentShift = ShiftNumber;
                CurrentBsDate = Sm.GetPosSetting("CurrentBnsDate");

                PrintDialog pd = new PrintDialog();
                pdoc = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();
                Font font = new Font("Courier New", 15);


                PaperSize psize = new PaperSize("Custom", 100, 200);
                //ps.DefaultPageSettings.PaperSize = psize;

                pd.Document = pdoc;
                pd.Document.DefaultPageSettings.PaperSize = psize;
                //pdoc.DefaultPageSettings.PaperSize.Height =320;
                pdoc.DefaultPageSettings.PaperSize.Height = 820;

                pdoc.DefaultPageSettings.PaperSize.Width = 520;

                pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintShiftClosingPage);

                pdoc.PrinterSettings.PrinterName = Sm.GetPosSetting("PrinterName");
                pdoc.Print();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void DoShiftClosing()
        {
            try
            {
                string MyShift = Sm.GetPosSetting("CurrentShift");

                Sm.SetPosSetting("StsShift", "C");
                Sm.SetPosSetting("CurrentUser", "");

                Sm.StdMsg(mMsgType.Info, "Successfully Perform Shift Closing " + Environment.NewLine +
                    "Please Turn on the printer and press OK Button to start printing");

                PrintShiftClosing(MyShift);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void PrintDayEndClosing(string BusinessDate)
        {
            try
            {
                CurrentBsDate = BusinessDate;

                PrintDialog pd = new PrintDialog();
                pdoc = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();
                Font font = new Font("Courier New", 15);

                PaperSize psize = new PaperSize("Custom", 100, 200);
                //ps.DefaultPageSettings.PaperSize = psize;

                pd.Document = pdoc;
                pd.Document.DefaultPageSettings.PaperSize = psize;
                //pdoc.DefaultPageSettings.PaperSize.Height =320;
                pdoc.DefaultPageSettings.PaperSize.Height = 820;

                pdoc.DefaultPageSettings.PaperSize.Width = 520;

                pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintDayEndClosingPage);

                pdoc.PrinterSettings.PrinterName = Sm.GetPosSetting("PrinterName");
                pdoc.Print();

                //this.BeginInvoke(new MethodInvoker(this.Close));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void DoDayEndClosing()
        {
            try
            {
                ProcessDayEndClosing();

                string MyBsDate = Sm.GetPosSetting("CurrentBnsDate");

                Sm.SetPosSetting("StsDayEnd", "C");
                Sm.SetPosSetting("CurrentShift", "0");
                Sm.SetPosSetting("CurrentBnsDate", "");

                Sm.StdMsg(mMsgType.Info, "Successfully Perform Day End Closing " + Environment.NewLine +
                    "Please Turn on the printer and press OK Button to start printing");

                PrintDayEndClosing(MyBsDate);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void PrintLine(Graphics graphics, string BusinessDate, string LineStr, int startX, int startY, int spaceY, ref int Offset)
        {
            Sm.AddToAuditRollFile(BusinessDate, LineStr);
            graphics.DrawString(LineStr, new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + spaceY;
        }

        void pdoc_PrintShiftClosingPage(object sender, PrintPageEventArgs e)
        {

            Sm.AddToAuditRollFile(CurrentBsDate, "Printing Shift Closing Report...");

            Graphics graphics = e.Graphics;
            Font font = new Font("Courier New", 10);
            float fontHeight = font.GetHeight();
            int startX = 3;
            int startY = 0;
            int Offset = 0;


            Decimal CashAmount = 0;
            Decimal FloatAmount = Sm.GetDecValue(Sm.GetPosSetting("FloatAmount"));
            Decimal CashIn = Sm.GetDecValue(Sm.GetValue("SELECT Sum(CIAmt) As NumValue FROM tblposcashin Where PosNo='" + Gv.PosNo + "' And ShiftNo='" + CurrentShift + "' And BsDate='" + CurrentBsDate + "' "));
            Decimal CashOut = Sm.GetDecValue(Sm.GetValue("SELECT Sum(COAmt) As NumValue FROM tblposcashout Where PosNo='" + Gv.PosNo + "' And ShiftNo='" + CurrentShift + "' And BsDate='" + CurrentBsDate + "' "));

            PrintLine(graphics, CurrentBsDate, Sm.PadCenter(Gv.CompanyName, Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadCenter("Shift Closing Report", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.ServerCurrentDateTime().Substring(6, 2) + "-" + Sm.ServerCurrentDateTime().Substring(4, 2) + "-" + Sm.ServerCurrentDateTime().Substring(0, 4) + " " + Sm.ServerCurrentDateTime().Substring(8, 2) + ":" + Sm.ServerCurrentDateTime().Substring(10, 2), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, "Pos No : " + Gv.PosNo, startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, "Shift No : " + CurrentShift, startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, "Cashier : " + Gv.CurrentUserCode, startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Tender Summary", Gv.MaxCharLinePrint - 15) + Sm.PadRight("Count", 5) + Sm.PadRight("Amount", 10), startX, startY, 15, ref Offset);

            // read from database
            try
            {
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText =
                        " Select D.SlsType, Case When D.SlsType='R' then concat(C.PayTpNm, ' Return') else C.PayTpNm end As PayTpNm, C.IsCash, Count(B.PayTpNo) As TCount, " +
                        " Sum(Case when D.SlsType = 'R' then -1*B.PayAmtNett else B.PayAmtNett end) As TPayAmt from tblPosTrnHdr A " +
                        " Inner Join tblPosTrnPay B On A.TrnNo=B.TrnNo And A.BsDate=B.BsDate And A.PosNo=B.PosNo " +
                        " Inner Join tblpospaybytype C on B.PayTpNo=C.PayTpNo " +
                        " Inner join tblpostrnhdr D on A.TrnNo=D.TrnNo And A.BsDate=D.BsDate And A.PosNo=D.PosNo " +
                        " Where A.PosNo='" + Gv.PosNo + "' And A.ShiftNo='" + CurrentShift + "' And A.BsDate='" + CurrentBsDate + "' " +
                        " Group By D.SlsType, C.PayTpNm, C.IsCash ";
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        dr.GetOrdinal("PayTpNm"),
                        dr.GetOrdinal("IsCash"),
                        dr.GetOrdinal("TCount"),
                        dr.GetOrdinal("TPayAmt"),
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (Sm.DrStr(dr, c[1]) == "Y")
                                CashAmount += Sm.DrDec(dr, c[3]);
                            PrintLine(graphics, CurrentBsDate, Sm.PadLeft(Sm.DrStr(dr, c[0]), Gv.MaxCharLinePrint - 15) + Sm.PadRight(Sm.DrStr(dr, c[2]), 5) + Sm.PadRight(String.Format("{0:###,###,###,##0}", Sm.DrDec(dr, c[3])), 10), startX, startY, 15, ref Offset);
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, "Unable to print." + Exc.Message);
            }
            string TotalTransCount = Sm.GetValue("SELECT Count(*) As NumValue FROM tblPosTrnHdr Where PosNo='" + Gv.PosNo + "' And ShiftNo='" + CurrentShift + "' And BsDate='" + CurrentBsDate + "' ");
            PrintLine(graphics, CurrentBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Transaction Count", Gv.MaxCharLinePrint - 10) + Sm.PadRight(String.Format("{0:###,###,###,##0}", TotalTransCount), 10), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Float Amount", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", FloatAmount), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Cash In", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", CashIn), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Cash Out", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", CashOut), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Total Cash", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", FloatAmount + CashAmount + CashIn - CashOut), 15), startX, startY, 15, ref Offset);

            Sm.StdMsg(mMsgType.Info, "Shift Report is successfully done.");
        }

        void pdoc_PrintDayEndClosingPage(object sender, PrintPageEventArgs e)
        {

            Sm.AddToAuditRollFile(CurrentBsDate, "Printing Day End Closing Report...");

            Graphics graphics = e.Graphics;
            Font font = new Font("Courier New", 10);
            float fontHeight = font.GetHeight();
            int startX = 3;
            int startY = 0;
            int Offset = 0;


            Decimal CashAmount = 0;
            Decimal FloatAmount = Sm.GetDecValue(Sm.GetPosSetting("FloatAmount"));
            Decimal CashIn = Sm.GetDecValue(Sm.GetValue("SELECT Sum(CIAmt) As NumValue FROM tblposcashin Where PosNo='" + Gv.PosNo + "' And BsDate='" + CurrentBsDate + "' "));
            Decimal CashOut = Sm.GetDecValue(Sm.GetValue("SELECT Sum(COAmt) As NumValue FROM tblposcashout Where PosNo='" + Gv.PosNo + "' And BsDate='" + CurrentBsDate + "' "));

            PrintLine(graphics, CurrentBsDate, Sm.PadCenter(Gv.CompanyName, Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadCenter("Day End Closing Report", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.ServerCurrentDateTime().Substring(6, 2) + "-" + Sm.ServerCurrentDateTime().Substring(4, 2) + "-" + Sm.ServerCurrentDateTime().Substring(0, 4) + " " + Sm.ServerCurrentDateTime().Substring(8, 2) + ":" + Sm.ServerCurrentDateTime().Substring(10, 2), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, "Pos No : " + Gv.PosNo, startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, "Business Date : " + CurrentBsDate, startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, "Cashier : " + Gv.CurrentUserCode, startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Tender Summary", Gv.MaxCharLinePrint - 15) + Sm.PadRight("Count", 5) + Sm.PadRight("Amount", 10), startX, startY, 15, ref Offset);


            // read from database
            try
            {
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText =
                        " Select D.SlsType, Case When D.SlsType='R' then concat(C.PayTpNm, ' Return') else C.PayTpNm end As PayTpNm, " +
                        " C.IsCash, Count(B.PayTpNo) As TCount, " +
                        " Sum(Case when D.SlsType = 'R' then -1*B.PayAmtNett else B.PayAmtNett end) As TPayAmt from tblPosTrnHdr A " +
                        " Inner Join tblPosTrnPay B On A.TrnNo=B.TrnNo And A.BsDate=B.BsDate And A.PosNo=B.PosNo " +
                        " Inner Join tblpospaybytype C on B.PayTpNo=C.PayTpNo " +
                        " Inner join tblpostrnhdr D on A.TrnNo=D.TrnNo And A.BsDate=D.BsDate And A.PosNo=D.PosNo " +
                        " Where A.PosNo='" + Gv.PosNo + "' And A.BsDate='" + CurrentBsDate + "' " +
                        " Group By D.SlsType, C.PayTpNm, C.IsCash ";
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        dr.GetOrdinal("PayTpNm"),
                        dr.GetOrdinal("IsCash"),
                        dr.GetOrdinal("TCount"),
                        dr.GetOrdinal("TPayAmt"),
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (Sm.DrStr(dr, c[1]) == "Y")
                                CashAmount += Sm.DrDec(dr, c[3]);
                            PrintLine(graphics, CurrentBsDate, Sm.PadLeft(Sm.DrStr(dr, c[0]), Gv.MaxCharLinePrint - 15) + Sm.PadRight(Sm.DrStr(dr, c[2]), 5) + Sm.PadRight(String.Format("{0:###,###,###,##0}", Sm.DrDec(dr, c[3])), 10), startX, startY, 15, ref Offset);
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, "Unable to print." + Exc.Message);
            }
            string TotalTransCount = Sm.GetValue("SELECT Count(*) As NumValue FROM tblPosTrnHdr Where PosNo='" + Gv.PosNo + "' And BsDate='" + CurrentBsDate + "' ");
            PrintLine(graphics, CurrentBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Transaction Count", Gv.MaxCharLinePrint - 10) + Sm.PadRight(String.Format("{0:###,###,###,##0}", TotalTransCount), 10), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Float Amount", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", FloatAmount), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Cash In", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", CashIn), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Cash Out", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", CashOut), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, CurrentBsDate, Sm.PadLeft("Total Cash", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", FloatAmount + CashAmount + CashIn - CashOut), 15), startX, startY, 15, ref Offset);

            Sm.StdMsg(mMsgType.Info, "Day End Report is successfully done.");

        }

        #region TKG

        private void ProcessDayEndClosing()
        {
            var DocSeqNo = "4";
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            if (IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");
            var IsPOSUpdateStockEnabled = Sm.GetParameterBoo("IsPOSUpdateStockEnabled");
            var POSUpdatedStockPosNo = Sm.GetParameter("POSUpdatedStockPosNo");
            if (!IsPOSUpdateStockEnabled && POSUpdatedStockPosNo.Length > 0)
            {
                IsPOSUpdateStockEnabled = Sm.IsDataExist(
                    "Select 1 From TblPosNo Where PosNo=@Param And Find_In_Set(IfNull(PosNo, ''), IfNull((Select ParValue From TblParameter Where ParCode='POSUpdatedStockPosNo'), '') );",
                    Gv.PosNo);
            }
            var CurrentBnsDate = Sm.GetValue("Select SetValue From TblPosSetting Where SetCode='CurrentBnsDate' And PosNo=@Param;", Gv.PosNo);
            var WhsCode = Sm.GetValue("Select SetValue From TblPosSetting Where SetCode='StoreWhsCode' And PosNo=@Param;", Gv.PosNo);
            var CurCode = Sm.GetParameter("MainCurCode");
            var DocType = "28";
            var DocType2 = "29";
            var DocTitle = Sm.GetParameter("DocTitle");
            var VoucherRequestDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'");
            var VoucherDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Voucher'");
            var DeptCode = Sm.GetParameter("IncomingPaymentDeptCode");
            string
                StockJournalDocNo = string.Empty,
                VoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");

            if (CurrentBnsDate.Length > 0 && WhsCode.Length > 0)
            {
                var lPosTrn = new List<PosTrn>();
                var lPosTrnPay = new List<PosTrnPay>();
                var lPosStock = new List<PosStock>();
                var lPosItem = new List<PosItem>();
                var lPosMovement = new List<PosMovement>();
                var lRetur = new List<Retur>();

                ProcessDayEndClosing1(CurrentBnsDate, ref lPosTrn);
                ProcessDayEndClosing2(CurrentBnsDate, ref lPosTrnPay);

                foreach (var x in
                    from value in lPosTrn
                    where value.SlsType == "S"
                    group value.Qty by new { value.ItCode } into grouped
                    select new PosItem()
                    {
                        ItCode = grouped.Key.ItCode,
                        Qty = grouped.Sum()
                    })
                {
                    lPosItem.Add(new PosItem()
                    {
                        ItCode = x.ItCode,
                        Qty = x.Qty
                    });
                };

                ProcessDayEndClosing3(WhsCode, ref lPosItem, ref lPosStock);

                var ItCode = new List<String>();

                foreach (var x in
                    from i in lPosItem
                    where !(from s in lPosStock
                            select s.ItCode)
                           .Contains(i.ItCode)
                    select i.ItCode
                    )
                {
                    ItCode.Add(x);
                }

                ProcessDayEndClosing4(WhsCode, ref ItCode, ref lPosStock);
                ProcessDayEndClosing5(CurrentBnsDate, ref lPosTrn, ref lPosStock, ref lPosMovement);
                ProcessDayEndClosing6(
                    CurrentBnsDate,
                    DocTitle,
                    VoucherRequestDocAbbr,
                    VoucherDocAbbr,
                    VoucherCodeFormatType,
                    ref lPosTrnPay,
                    ref StockJournalDocNo,
                    IsPOSUpdateStockEnabled,
                    IsVoucherDocSeqNoEnabled, 
                    DocSeqNo
                );
                ProcessDayEndClosing7(DocType, ref lPosTrn, ref lRetur);
                ProcessDayEndClosing8(DocType, CurrentBnsDate, WhsCode, ref lPosTrn, ref lPosMovement, ref lRetur);
                ProcessDayEndClosing9(CurrentBnsDate, ref lPosTrn, ref lRetur);
                ProcessDayEndClosing10(
                    CurrentBnsDate,
                    DocType,
                    DocType2,
                    WhsCode,
                    DeptCode,
                    CurCode,
                    ref lPosMovement,
                    ref lPosTrnPay,
                    ref lRetur,
                    ref StockJournalDocNo,
                    IsPOSUpdateStockEnabled
                    );
            }
        }

        private void ProcessDayEndClosing1(string CurrentBnsDate, ref List<PosTrn> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.TrnNo, A.TrnDtTm, B.DtlNo, B.ItCode, B.Qty, A.CreateBy, A.CreateDt, A.SlsType, ");
            SQL.AppendLine("B.RTrnNo, B.RBsDate, B.RDtlNo ");
            SQL.AppendLine("From TblPosTrnHdr A, TblPosTrnDtl B ");
            SQL.AppendLine("Where A.TrnNo=B.TrnNo ");
            SQL.AppendLine("And A.BsDate=B.BsDate ");
            SQL.AppendLine("And A.PosNo=B.PosNo ");
            SQL.AppendLine("And A.BsDate=@BsDate ");
            SQL.AppendLine("And A.PosNo=@PosNo ");

            Sm.CmParam<string>(ref cm, "@BsDate", CurrentBnsDate);
            Sm.CmParam<string>(ref cm, "@PosNo", Gv.PosNo);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "TrnNo", 
                    "TrnDtTm", "DtlNo", "ItCode", "Qty", "CreateBy", 
                    "CreateDt", "SlsType", "RTrnNo", "RBsDate", "RDtlNo"  
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PosTrn()
                        {
                            TrnNo = Sm.DrStr(dr, c[0]),
                            TrnDtTm = Sm.Left(Sm.DrStr(dr, c[1]), 8),
                            DtlNo = Sm.Right("00" + Sm.DrStr(dr, c[2]), 3),
                            ItCode = Sm.DrStr(dr, c[3]),
                            Qty = Sm.DrDec(dr, c[4]),
                            CreateBy = Sm.DrStr(dr, c[5]),
                            CreateDt = Sm.DrStr(dr, c[6]),
                            SlsType = Sm.DrStr(dr, c[7]),
                            RTrnNo = Sm.DrStr(dr, c[8]),
                            RBsDate = Sm.DrStr(dr, c[9]),
                            RDtlNo = Sm.DrStr(dr, c[10]),
                            BsDate = CurrentBnsDate
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessDayEndClosing2(string CurrentBnsDate, ref List<PosTrnPay> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayTpNo, B.PaymentType, B.BankAcCode, C.AutoNoDebit, ");
            SQL.AppendLine("Sum(Case When D.SlsType='S' Then 1.00 Else -1.00 End*A.PayAmtNett) As PayAmtNett, ");
            SQL.AppendLine("Sum(Case When D.SlsType='S' Then 1.00 Else -1.00 End*(A.PayAmtNett/((100.00+IfNull(E.TaxRate, 0.00))/100.00))) As AmtBefTax, ");
            SQL.AppendLine("Sum(Case When D.SlsType='S' Then 1.00 Else -1.00 End*(A.PayAmtNett-(A.PayAmtNett/((100.00+IfNull(E.TaxRate, 0.00))/100.00)))) As TaxAmt ");
            SQL.AppendLine("From TblPosTrnPay A ");
            SQL.AppendLine("Inner Join TblPosPayByType B On A.PayTpNo=B.PayTpNo ");
            SQL.AppendLine("Left Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
            SQL.AppendLine("Inner Join TblPosTrnHdr D ");
            SQL.AppendLine("    On A.TrnNo=D.TrnNo ");
            SQL.AppendLine("    And A.BsDate=D.BsDate ");
            SQL.AppendLine("    And A.PosNo=D.PosNo ");
            SQL.AppendLine("Left Join TblTax E on D.Tax1Code=E.TaxCode ");
            SQL.AppendLine("Where A.BsDate=@BsDate And A.PosNo=@PosNo ");
            SQL.AppendLine("Group By A.PayTpNo, B.PaymentType, B.BankAcCode, C.AutoNoDebit;");

            Sm.CmParam<string>(ref cm, "@BsDate", CurrentBnsDate);
            Sm.CmParam<string>(ref cm, "@PosNo", Gv.PosNo);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "PayTpNo", 

                    //1-5
                    "PaymentType", "BankAcCode", "AutoNoDebit", "PayAmtNett", "AmtBefTax",
 
                    //6
                    "TaxAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PosTrnPay()
                        {
                            PayTpNo = Sm.DrStr(dr, c[0]),
                            PaymentType = Sm.DrStr(dr, c[1]),
                            BankAcCode = Sm.DrStr(dr, c[2]),
                            AutoNoDebit = Sm.DrStr(dr, c[3]),
                            PayAmt = Sm.DrDec(dr, c[4]),
                            AmtBefTax = Sm.DrDec(dr, c[5]),
                            TaxAmt = Sm.DrDec(dr, c[6]),
                            BsDate = CurrentBnsDate,
                            VoucherRequestDocNo = string.Empty,
                            VoucherDocNo = string.Empty,
                            JournalDocNo = string.Empty,
                            POSVATDocNo = string.Empty
                            //VoucherRequestPPNDocNo = string.Empty,
                            //VoucherRequestDocNo2 = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessDayEndClosing3(string WhsCode, ref List<PosItem> l, ref List<PosStock> l2)
        {
            if (l.Count <= 0) return;

            string Filter = string.Empty;
            var No = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            for (var i = 0; i < l.Count; i++)
            {
                No = i.ToString();
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(ItCode=@ItCode" + No + ") ";
                Sm.CmParam<String>(ref cm, "@ItCode" + No, l[i].ItCode);
            }
            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);

            SQL.AppendLine("Select Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Qty>0 ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine(";");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "Lot", 
                    "Bin", "ItCode", "PropCode", "BatchNo", "Source", 
                    "Qty" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new PosStock()
                        {
                            Lot = Sm.DrStr(dr, c[0]),
                            Bin = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            PropCode = Sm.DrStr(dr, c[3]),
                            BatchNo = Sm.DrStr(dr, c[4]),
                            Source = Sm.DrStr(dr, c[5]),
                            Qty = Sm.DrDec(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }

        }

        private void ProcessDayEndClosing4(string WhsCode, ref List<String> l, ref List<PosStock> l2)
        {
            if (l.Count <= 0) return;

            var No = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            for (var i = 0; i < l.Count; i++)
            {
                No = i.ToString();
                if (SQL.Length > 0) SQL.AppendLine(" Union All ");
                SQL.AppendLine(" Select Lot, Bin, ItCode, PropCode, BatchNo, Source From ( ");
                SQL.AppendLine(" Select Lot, Bin, ItCode, PropCode, BatchNo, Source From TblStockSummary ");
                SQL.AppendLine(" Where WhsCode=@WhsCode And ItCode=@ItCode" + No + " Order By CreateDt Desc Limit 1 ");
                SQL.AppendLine(") T ");
                Sm.CmParam<String>(ref cm, "@ItCode" + No, l[i]);
            }
            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);

            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "Lot", 
                    "Bin", "ItCode", "PropCode", "BatchNo", "Source"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new PosStock()
                        {
                            Lot = Sm.DrStr(dr, c[0]),
                            Bin = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            PropCode = Sm.DrStr(dr, c[3]),
                            BatchNo = Sm.DrStr(dr, c[4]),
                            Source = Sm.DrStr(dr, c[5]),
                            Qty = 0m
                        });
                    }
                }
                dr.Close();
            }

        }

        private void ProcessDayEndClosing5(string BnsDate, ref List<PosTrn> l, ref List<PosStock> l2, ref List<PosMovement> l3)
        {
            var ItCode = string.Empty;
            bool IsFind = false;
            decimal QtyTemp = 0m, ProcessedQty = 0m;

            l2.Sort((x, y) => x.ItCode.CompareTo(y.ItCode));

            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].SlsType == "S")
                {
                    ItCode = l[i].ItCode;
                    QtyTemp = l[i].Qty;
                    IsFind = false;

                    for (var j = 0; j < l2.Count; j++)
                    {
                        if (string.Compare(ItCode, l2[j].ItCode) == 0)
                        {
                            IsFind = true;
                            if (l2[j].Qty > 0)
                            {
                                if (QtyTemp >= l2[j].Qty)
                                {
                                    ProcessedQty = l2[j].Qty;
                                    QtyTemp -= l2[j].Qty;
                                    l2[j].Qty = 0m;
                                }
                                else
                                {
                                    ProcessedQty = QtyTemp;
                                    l2[j].Qty -= QtyTemp;
                                    QtyTemp = 0m;
                                }
                                l3.Add(new PosMovement()
                                {
                                    DocNo = BnsDate + "/" + l[i].TrnNo,
                                    DocDt = l[i].TrnDtTm,
                                    DNo = l[i].DtlNo,
                                    ItCode = l[i].ItCode,
                                    PropCode = l2[j].PropCode,
                                    BatchNo = l2[j].BatchNo,
                                    Source = l2[j].Source,
                                    Lot = l2[j].Lot,
                                    Bin = l2[j].Bin,
                                    Qty = ProcessedQty,
                                    CreateBy = l[i].CreateBy,
                                    CreateDt = l[i].CreateDt
                                });
                            }
                        }
                        else
                        {
                            if (IsFind)
                            {
                                if (l3.Count > 0 && string.Compare(ItCode, l3[l3.Count - 1].ItCode) == 0)
                                    l3[l3.Count - 1].Qty += QtyTemp;
                                else
                                {
                                    if (j > 0 && string.Compare(ItCode, l2[j - 1].ItCode) == 0)
                                    {
                                        l3.Add(new PosMovement()
                                        {
                                            DocNo = BnsDate + "/" + l[i].TrnNo,
                                            DocDt = l[i].TrnDtTm,
                                            DNo = l[i].DtlNo,
                                            ItCode = l[i].ItCode,
                                            PropCode = l2[j - 1].PropCode,
                                            BatchNo = l2[j - 1].BatchNo,
                                            Source = l2[j - 1].Source,
                                            Lot = l2[j - 1].Lot,
                                            Bin = l2[j - 1].Bin,
                                            Qty = QtyTemp,
                                            CreateBy = l[i].CreateBy,
                                            CreateDt = l[i].CreateDt
                                        });
                                    }
                                }
                                QtyTemp = 0m;
                            }
                        }
                        if (QtyTemp == 0m) break;
                    }
                }
            }
        }

        private void ProcessDayEndClosing6(
            string CurrentBnsDate,
            string DocTitle,
            string VoucherRequestDocAbbr,
            string VoucherDocAbbr,
            string VoucherCodeFormatType,
            ref List<PosTrnPay> l,
            ref string StockJournalDocNo,
            bool IsPOSUpdateStockEnabled,
            bool IsVoucherDocSeqNoEnabled, 
            string DocSeqNo)
        {
            string
                 Yr = CurrentBnsDate.Substring(2, 2),
                 Mth = CurrentBnsDate.Substring(4, 2);
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<string>(ref cm, "@Yr", Yr);
            Sm.CmParam<string>(ref cm, "@Mth", Mth);

            if (VoucherCodeFormatType == "2")
            {
                SQL.Append("Select Convert(Left(DocNo, 4), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                SQL.Append("Where Right(DocNo, 5)= Concat(@Mth,'/', @Yr) ");
                SQL.Append("Order By Left(DocNo, 4) Desc Limit 1;");
            }
            else
            {
                if (IsVoucherDocSeqNoEnabled)
                {
                    SQL.Append("Select Convert(Substring(DocNo, 7, " + DocSeqNo + "), Decimal) As DocNoTemp ");
                    SQL.Append("From TblVoucherRequestHdr ");
                    SQL.Append("Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
                    SQL.Append("Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1;");
                }
                else
                {
                    SQL.Append("Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNoTemp ");
                    SQL.Append("From TblVoucherRequestHdr ");
                    SQL.Append("Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
                    SQL.Append("Order By Substring(DocNo, 7, 4) Desc Limit 1;");
                }
            }
            cm.CommandText = SQL.ToString();
            var VoucherRequestNo = Sm.GetValue(cm);
            VoucherRequestNo = (VoucherRequestNo.Length == 0) ? "0" : VoucherRequestNo;

            SQL.Length = 0;
            if (VoucherCodeFormatType == "2")
            {
                SQL.Append("Select Convert(Left(DocNo, 4), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherHdr ");
                SQL.Append("Where Right(DocNo, 5)= Concat(@Mth,'/', @Yr) ");
                SQL.Append("Order By Left(DocNo, 4) Desc Limit 1;");   
            }
            else
            {
                if (IsVoucherDocSeqNoEnabled)
                {
                    SQL.Append("Select Convert(Substring(DocNo, 7, " + DocSeqNo + "), Decimal) As DocNoTemp ");
                    SQL.Append("From TblVoucherHdr ");
                    SQL.Append("Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
                    SQL.Append("Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1;");
                }
                else
                {
                    SQL.Append("Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNoTemp ");
                    SQL.Append("From TblVoucherHdr ");
                    SQL.Append("Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
                    SQL.Append("Order By Substring(DocNo, 7, 4) Desc Limit 1;");
                }
            }
            cm.CommandText = SQL.ToString();

            var VoucherNo = Sm.GetValue(cm);
            VoucherNo = (VoucherNo.Length == 0) ? "0" : VoucherNo;

            SQL.Length = 0;
            SQL.Append("Select Convert(Left(DocNo, 4), Decimal) As DocNo ");
            SQL.Append("From TblPOSVAT ");
            SQL.Append("Where Right(DocNo, 5)=Concat(@Mth,'/',@Yr) ");
            SQL.Append("Order By Left(DocNo, 4) Desc Limit 1 ");
            cm.CommandText = SQL.ToString();
            var POSVATNo = Sm.GetValue(cm);
            POSVATNo = (POSVATNo.Length == 0) ? "0" : POSVATNo;
            string POSVATDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='POSVAT'");

            int VoucherRequest = int.Parse(VoucherRequestNo);
            int Voucher = int.Parse(VoucherNo);
            int Journal = 0;
            int POSVAT = int.Parse(POSVATNo);
            //int VoucherRequestPPN = int.Parse(VoucherRequestPPNNo);

            for (var i = 0; i < l.Count; i++)
            {
                VoucherRequest += 1;
                Voucher += 1;
                if (mIsAutoJournalActived) Journal += 1;
                if (VoucherCodeFormatType == "2")
                {
                    l[i].VoucherRequestDocNo = string.Concat(Sm.Right("0000" + VoucherRequest.ToString(), 4), "/", DocTitle, "/", VoucherRequestDocAbbr, "/", Mth, "/", Yr);
                    l[i].VoucherDocNo = string.Concat(Sm.Right("0000" + Voucher.ToString(), 4), "/", DocTitle, "/", VoucherDocAbbr, "/", Mth, "/", Yr);
                }
                else
                {
                    if (IsVoucherDocSeqNoEnabled)
                    {
                        l[i].VoucherRequestDocNo = Yr + "/" + Mth + "/" + Sm.Right(String.Empty.PadRight(int.Parse(DocSeqNo), '0') + VoucherRequest, int.Parse(DocSeqNo)) + "/" + VoucherRequestDocAbbr;
                        l[i].VoucherDocNo = Yr + "/" + Mth + "/" + Sm.Right(String.Empty.PadRight(int.Parse(DocSeqNo), '0') + Voucher, int.Parse(DocSeqNo)) + "/" + VoucherDocAbbr;
                    }
                    else
                    {
                        l[i].VoucherRequestDocNo = Yr + "/" + Mth + "/" + Sm.Right("000" + VoucherRequest, 4) + "/" + VoucherRequestDocAbbr;
                        l[i].VoucherDocNo = Yr + "/" + Mth + "/" + Sm.Right("000" + Voucher, 4) + "/" + VoucherDocAbbr;
                    }
                    if (l[i].AutoNoDebit.Length > 0)
                    {
                        l[i].VoucherRequestDocNo = l[i].VoucherRequestDocNo + "/" + l[i].AutoNoDebit;
                        l[i].VoucherDocNo = l[i].VoucherDocNo + "/" + l[i].AutoNoDebit;
                    }
                }
                if (mIsAutoJournalActived) l[i].JournalDocNo = Sm.GenerateDocNoJournal(l[i].BsDate, Journal);
                if (l[i].TaxAmt != 0)
                {
                    POSVAT += 1;
                    l[i].POSVATDocNo = string.Concat(Sm.Right("0000" + POSVAT.ToString(), 4), "/", DocTitle, "/", POSVATDocAbbr, "/", Mth, "/", Yr);
                }
            }
            VoucherRequest += 1;
            if (mIsAutoJournalActived && IsPOSUpdateStockEnabled)
            {
                Journal += 1;
                StockJournalDocNo = Sm.GenerateDocNoJournal(CurrentBnsDate, Journal);
            }
        }

        private void ProcessDayEndClosing7(string DocType, ref List<PosTrn> l, ref List<Retur> l2)
        {
            if (l.Count <= 0) return;
            l2.Clear();
            string Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            int x = 0;

            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].SlsType == "R")
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(DocNo=@DocNo" + x.ToString() + " And ItCode=@ItCode" + x.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DocNo" + x.ToString(), string.Concat(Gv.PosNo, "/", l[i].RBsDate, "/", l[i].RTrnNo));
                    Sm.CmParam<String>(ref cm, "@ItCode" + x.ToString(), l[i].ItCode);
                    Sm.CmParam<Decimal>(ref cm, "@Qty" + x.ToString(), l[i].Qty);
                    x += 1;
                }
            }
            Sm.CmParam<String>(ref cm, "@DocType", DocType);

            if (Filter.Length > 0) Filter = " ( " + Filter + ") ";

            SQL.AppendLine("Select DocNo, DNo, WhsCode, Lot, Bin, ");
            SQL.AppendLine("ItCode, PropCode, BatchNo, Source, ");
            if (Filter.Length > 0)
            {
                SQL.AppendLine("Case ");
                for (int i = 0; i < x; i++)
                    SQL.AppendLine("    When DocNo=@DocNo" + i.ToString() + " And ItCode=@ItCode" + i.ToString() + " Then Abs(@Qty" + i.ToString() + ") ");
                SQL.AppendLine("Else 0.00 End ");
            }
            else
                SQL.AppendLine("0.00 ");
            SQL.AppendLine("As Qty ");
            SQL.AppendLine("From TblStockMovement ");
            if (Filter.Length == 0) Filter = " 1=0 ";
            SQL.AppendLine("Where DocType=@DocType And " + Filter + ";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocNo", 
                    "DNo", "WhsCode", "Lot", "Bin", "ItCode", 
                    "PropCode", "BatchNo", "Source", "Qty" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Retur()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DNo = Sm.DrStr(dr, c[1]),
                            WhsCode = Sm.DrStr(dr, c[2]),
                            Lot = Sm.DrStr(dr, c[3]),
                            Bin = Sm.DrStr(dr, c[4]),
                            ItCode = Sm.DrStr(dr, c[5]),
                            PropCode = Sm.DrStr(dr, c[6]),
                            BatchNo = Sm.DrStr(dr, c[7]),
                            Source = Sm.DrStr(dr, c[8]),
                            Qty = Sm.DrDec(dr, c[9]),
                            Qty2 = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessDayEndClosing8(string DocType, string BnsDate, string WhsCode, ref List<PosTrn> l, ref List<PosMovement> l2, ref List<Retur> l3)
        {
            if (l.Count <= 0) return;

            string DocNo = string.Empty, ItCode = string.Empty;

            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].SlsType == "R")
                {
                    DocNo = string.Concat(Gv.PosNo, "/", l[i].RBsDate, "/", l[i].RTrnNo);
                    ItCode = l[i].ItCode;
                    for (var j = 0; j < l2.Count; j++)
                    {
                        if (Sm.CompareStr(l2[j].DocNo, DocNo) &&
                            Sm.CompareStr(l2[j].ItCode, ItCode))
                            l3.Add(new Retur()
                            {
                                DocNo = l2[j].DocNo,
                                DNo = l2[j].DNo,
                                WhsCode = WhsCode,
                                Lot = l2[j].Lot,
                                Bin = l2[j].Bin,
                                ItCode = l2[j].ItCode,
                                PropCode = l2[j].PropCode,
                                BatchNo = l2[j].BatchNo,
                                Source = l2[j].Source,
                                Qty = l[i].Qty < 0 ? -1 * l[i].Qty : l[i].Qty,
                                Qty2 = 0m
                            });
                    }
                }
            }
        }

        private void ProcessDayEndClosing9(string BnsDate, ref List<PosTrn> l, ref List<Retur> l2)
        {
            if (l2.Count <= 0) return;

            decimal Qty = 0m;
            var DocNo = string.Empty;
            int DNo = 0;
            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].SlsType == "R")
                {
                    Qty = l[i].Qty;

                    foreach (var x in l2.OrderBy(o => o.DocNo))
                    {
                        if (
                            Qty > 0m &&
                            string.Compare(string.Concat(Gv.PosNo, "/", l[i].RBsDate, "/", l[i].RTrnNo), x.DocNo) == 0 &&
                            string.Compare(l[i].ItCode, x.ItCode) == 0
                            )
                        {
                            if (Sm.CompareStr(DocNo, string.Concat(Gv.PosNo, "/", BnsDate, "/", l[i].TrnNo)))
                            {
                                x.DocNoRetur = DocNo;
                                DNo += 1;
                            }
                            else
                            {
                                x.DocNoRetur = string.Concat(Gv.PosNo, "/", BnsDate, "/", l[i].TrnNo);
                                DNo = 1;
                            }
                            x.DNoRetur = Sm.Right("00" + DNo, 3);
                            x.DocDt = Sm.Left(l[i].TrnDtTm, 8);
                            x.UserCode = l[i].CreateBy;
                            x.Dt = l[i].CreateDt;
                            if (x.Qty >= Qty)
                            {
                                x.Qty2 = Qty;
                                Qty = 0m;
                                break;
                            }
                            else
                            {
                                x.Qty2 = x.Qty;
                                Qty -= x.Qty2;
                            }
                            DocNo = x.DocNoRetur;
                        }
                    }
                }
            }
        }

        private void ProcessDayEndClosing10(
            string CurrentBnsDate,
            string DocType,
            string DocType2,
            string WhsCode,
            string DeptCode,
            string CurCode,
            ref List<PosMovement> l,
            ref List<PosTrnPay> l2,
            ref List<Retur> l3,
            ref string StockJournalDocNo,
            bool IsPOSUpdateStockEnabled
            )
        {
            var cml = new List<MySqlCommand>();
            var SQL = new StringBuilder();
            var Query = string.Empty;

            if (IsPOSUpdateStockEnabled)
            {
                cml.Add(DelStockMovementPOS(CurrentBnsDate));

                #region Stock

                SQL.AppendLine("Insert Into TblStockMovement ");
                SQL.AppendLine("(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@DocType, Concat(@PosNo, '/',@DocNo), @DNo, 'N', @DocDt, @WhsCode, @Lot, @Bin, @ItCode, @PropCode, @BatchNo, @Source, -1*@Qty, -1*@Qty, -1*@Qty, @CreateBy, @CreateDt); ");

                SQL.AppendLine("Insert Into TblStockMovementPOS (PosNo, DocType, DocDt, ItCode, Source, Qty) ");
                SQL.AppendLine("Values (@PosNo, @DocType, @DocDt, @ItCode, @Source, @Qty); ");

                SQL.AppendLine("Update TblStockSummary Set ");
                SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty, Qty3=Qty3-@Qty, LastUpBy=@CreateBy, LastUpDt=@CreateDt ");
                SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

                Query = SQL.ToString();

                for (var i = 0; i < l.Count; i++)
                    cml.Add(SaveStock(Query, DocType, WhsCode, ref l, i));

                if (l3.Count > 0)
                {
                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Insert Into TblStockMovement ");
                    SQL.AppendLine("(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@DocType, @DocNo, @DNo, 'N', @DocDt, ");
                    SQL.AppendLine("@WhsCode, @Lot, @Bin, @ItCode, @PropCode, @BatchNo, @Source, ");
                    SQL.AppendLine("@Qty, @Qty, @Qty, @CreateBy, @CreateDt); ");

                    SQL.AppendLine("Insert Into TblStockMovementPOS (PosNo, DocType, DocDt, ItCode, Source, Qty) ");
                    SQL.AppendLine("Values (@PosNo, @DocType, @DocDt, @ItCode, @Source, @Qty); ");

                    SQL.AppendLine("Update TblStockSummary Set ");
                    SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty, Qty3=Qty3+@Qty, LastUpBy=@CreateBy, LastUpDt=@CreateDt ");
                    SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

                    Query = SQL.ToString();

                    for (var i = 0; i < l3.Count; i++)
                    {
                        if (l3[i].Qty2 != 0) cml.Add(SaveStock2(Query, DocType2, ref l3, i));
                    }
                }
            }

                #endregion

            #region Voucher

            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, CurCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'A', 'N', @DeptCode, '10', @VoucherDocNo, ");
            SQL.AppendLine("Case When @Amt>=0 Then 'D' Else 'C' End, @PaymentType, @BankAcCode, @CurCode, Abs(@Amt), @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', @Remark, Abs(@Amt), @UserCode, CurrentDateTime()); ");

            SQL.Append("Insert Into TblVoucherHdr (DocNo, DocDt, CancelInd, MInd, VoucherRequestDocNo, DocType, ");
            SQL.Append("AcType, BankAcCode, PaymentType, CurCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.Append("Values(@VoucherDocNo, @DocDt, 'N', 'N', @VoucherRequestDocNo, '10', ");
            SQL.Append("Case When @Amt>=0 Then 'D' Else 'C' End, @BankAcCode, @PaymentType, @CurCode, Abs(@Amt), @Remark, @UserCode, CurrentDateTime());");

            SQL.AppendLine("Insert Into TblVoucherDtl(DocNo, DNo, Description, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherDocNo, '001', @Remark, Abs(@Amt), @UserCode, CurrentDateTime()); ");

            if (mIsAutoJournalActived)
            {
                SQL.AppendLine("Update TblVoucherHdr Set ");
                SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
                SQL.AppendLine("Where DocNo=@VoucherDocNo;");

                SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Select JournalDocNo, ");
                if (Sm.IsClosingJournalUseCurrentDt(CurrentBnsDate))
                    SQL.AppendLine("Replace(CurDate(), '-', ''), ");
                else
                    SQL.AppendLine("DocDt, ");
                SQL.AppendLine("Concat('Voucher (POS) : ', DocNo) As JnDesc, ");
                SQL.AppendLine("@MenuCode As MenuCode, ");
                SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
                SQL.AppendLine("(Select CCCode From TblPosNo Where PosNo=@PosNo), ");
                SQL.AppendLine("CreateBy, CreateDt ");
                SQL.AppendLine("From TblVoucherHdr Where DocNo=@VoucherDocNo;");

                SQL.AppendLine("Set @Row:=0; ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("B.AcNo, B.DAmt, B.CAmt, E.EntCode, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
                SQL.AppendLine("        Select B.COAAcNo As AcNo, @Amt As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblVoucherHdr A ");
                SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select IfNull(C.SetValue, B.ParValue) As AcNo, 0.00 As DAmt, @AmtBefTax As CAmt ");
                SQL.AppendLine("        From TblVoucherHdr A ");
                SQL.AppendLine("        Left Join TblParameter B On B.ParCode='AcNoForStoreSales' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Left Join TblPosSetting C On C.SetCode='AcNoForStoreSales' And C.SetValue Is Not Null And C.PosNo=@PosNo ");
                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.ParValue As AcNo, 0.00 As DAmt, @TaxAmt As CAmt ");
                SQL.AppendLine("        From TblVoucherHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForVATOut' ");
                SQL.AppendLine("        Where A.DocNo=@VoucherDocNo ");
                SQL.AppendLine("        And Case When @TaxAmt<>0 Then 1 Else 0 End=1 ");
                SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Left Join TblPosNo C On C.PosNo=@PosNo ");
                SQL.AppendLine("Left Join TblSite D On C.SiteCode=D.SiteCode ");
                SQL.AppendLine("Left Join TblProfitCenter E On D.ProfitCenterCode=E.ProfitCenterCode ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
            }

            Query = SQL.ToString();

            for (var i = 0; i < l2.Count; i++)
                cml.Add(SaveVoucher(Query, DeptCode, CurCode, ref l2, i));

            for (var i = 0; i < l2.Count; i++)
                if (l2[i].TaxAmt != 0) cml.Add(SavePOSVAT(CurCode, ref l2, i));

            //for (var i = 0; i < l2.Count; i++) if (l2[i].TaxAmt!=0) cml.Add(SaveVoucherRequestPPN(DeptCode, CurCode, ref l2, i));

            #endregion

            cml.Add(SavePOSProcess(CurrentBnsDate, StockJournalDocNo, IsPOSUpdateStockEnabled));

            if (IsPOSUpdateStockEnabled)
            {
                #region Journal->Stock

                if (mIsAutoJournalActived) cml.Add(SaveStockJournal(StockJournalDocNo, CurrentBnsDate, DocType, DocType2));

                #endregion

                cml.Add(DelStockMovementPOS(CurrentBnsDate));
            }

            Sm.ExecCommands(cml);
        }

        private MySqlCommand SaveStock(string Query, string DocType, string WhsCode, ref List<PosMovement> l, int i)
        {
            var cm = new MySqlCommand() { CommandText = Query.ToString() };

            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<String>(ref cm, "@DocType", DocType);
            Sm.CmParam<String>(ref cm, "@DocNo", l[i].DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", l[i].DNo);
            Sm.CmParam<String>(ref cm, "@DocDt", l[i].DocDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
            Sm.CmParam<String>(ref cm, "@Lot", l[i].Lot);
            Sm.CmParam<String>(ref cm, "@Bin", l[i].Bin);
            Sm.CmParam<String>(ref cm, "@ItCode", l[i].ItCode);
            Sm.CmParam<String>(ref cm, "@PropCode", l[i].PropCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", l[i].BatchNo);
            Sm.CmParam<String>(ref cm, "@Source", l[i].Source);
            Sm.CmParam<Decimal>(ref cm, "@Qty", l[i].Qty);
            Sm.CmParam<String>(ref cm, "@CreateBy", l[i].CreateBy);
            Sm.CmParam<String>(ref cm, "@CreateDt", l[i].CreateDt);

            return cm;
        }

        private MySqlCommand SaveStock2(string Query, string DocType, ref List<Retur> l, int i)
        {
            var cm = new MySqlCommand() { CommandText = Query.ToString() };

            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<String>(ref cm, "@DocType", DocType);
            Sm.CmParam<String>(ref cm, "@DocNo", l[i].DocNoRetur);
            Sm.CmParam<String>(ref cm, "@DNo", l[i].DNoRetur);
            Sm.CmParam<String>(ref cm, "@DocDt", l[i].DocDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", l[i].WhsCode);
            Sm.CmParam<String>(ref cm, "@Lot", l[i].Lot);
            Sm.CmParam<String>(ref cm, "@Bin", l[i].Bin);
            Sm.CmParam<String>(ref cm, "@ItCode", l[i].ItCode);
            Sm.CmParam<String>(ref cm, "@PropCode", l[i].PropCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", l[i].BatchNo);
            Sm.CmParam<String>(ref cm, "@Source", l[i].Source);
            Sm.CmParam<Decimal>(ref cm, "@Qty", l[i].Qty2);
            Sm.CmParam<String>(ref cm, "@CreateBy", l[i].UserCode);
            Sm.CmParam<String>(ref cm, "@CreateDt", l[i].Dt);

            return cm;
        }

        private MySqlCommand SaveVoucher(string Query, string DeptCode, string CurCode, ref List<PosTrnPay> l, int i)
        {
            var cm = new MySqlCommand() { CommandText = Query.ToString() };

            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", l[i].VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", l[i].VoucherDocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", l[i].BsDate);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", l[i].BankAcCode);
            Sm.CmParam<String>(ref cm, "@PaymentType", l[i].PaymentType);
            Sm.CmParam<String>(ref cm, "@CurCode", CurCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", l[i].PayAmt);
            Sm.CmParam<String>(ref cm, "@Remark", "Point of Sale (" + Gv.PosNo + ") : " + l[i].BsDate);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            if (mIsAutoJournalActived)
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", l[i].JournalDocNo);
                Sm.CmParam<Decimal>(ref cm, "@AmtBefTax", l[i].AmtBefTax);
                Sm.CmParam<Decimal>(ref cm, "@TaxAmt", l[i].TaxAmt);
                Sm.CmParam<String>(ref cm, "@MenuCode", Sm.GetParameter("MenuCodeForDayEndClosing"));
            }
            return cm;
        }

        private MySqlCommand SavePOSProcess(string CurrentBnsDate, string StockJournalDocNo, bool IsPOSUpdateStockEnabled)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPOSProcess(BSDate, PosNo, JournalDocNo, CreateBy, CreateDt) " +
                    "Values(@BSDate, @PosNo, @JournalDocNo, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@BSDate", CurrentBnsDate);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            if (mIsAutoJournalActived && IsPOSUpdateStockEnabled)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", StockJournalDocNo);
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", string.Empty);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockJournal(string StockJournalDocNo, string CurrentBsDate, string DocType1, string DocType2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            if (Sm.IsClosingJournalUseCurrentDt(CurrentBsDate))
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('POS (Stock ', @PosNo, ') : ', @DocDt), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("Null, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, E.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, DAmt, CAmt From (");
            SQL.AppendLine("        Select AcNo, 0 As DAmt, Sum(CAmt) As CAmt ");
            SQL.AppendLine("        From (");
            SQL.AppendLine("            Select D.AcNo, IfNull(B.UPrice, 0)*IfNull(B.Excrate, 0)*A.Qty As CAmt ");
            SQL.AppendLine("            From TblStockMovementPOS A ");
            SQL.AppendLine("            Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("            Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("            Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("            Where A.DocDt=@DocDt ");
            SQL.AppendLine("            And A.DocType=@DocType1 ");
            SQL.AppendLine("        ) T Group By AcNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select AcNo, Sum(DAmt) As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From (");
            SQL.AppendLine("            Select C.ParValue As AcNo, IfNull(B.UPrice, 0)*IfNull(B.Excrate, 0)*A.Qty As DAmt ");
            SQL.AppendLine("            From TblStockMovementPOS A ");
            SQL.AppendLine("            Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("            Inner Join TblParameter C On C.ParCode='AcNoForCOGS' And C.ParValue Is Not Null ");
            SQL.AppendLine("            Where A.DocDt=@DocDt ");
            SQL.AppendLine("            And A.DocType=@DocType1 ");
            SQL.AppendLine("        ) T Group By AcNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select AcNo, Sum(DAmt) As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From (");
            SQL.AppendLine("            Select D.AcNo, IfNull(B.UPrice, 0)*IfNull(B.Excrate, 0)*A.Qty As DAmt ");
            SQL.AppendLine("            From TblStockMovementPOS A ");
            SQL.AppendLine("            Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("            Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("            Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("            Where A.DocDt=@DocDt ");
            SQL.AppendLine("            And A.DocType=@DocType2 ");
            SQL.AppendLine("        ) T Group By AcNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select AcNo, 0 As DAmt, Sum(CAmt) As CAmt ");
            SQL.AppendLine("        From (");
            SQL.AppendLine("            Select C.ParValue As AcNo, IfNull(B.UPrice, 0)*IfNull(B.Excrate, 0)*A.Qty As CAmt ");
            SQL.AppendLine("            From TblStockMovementPOS A ");
            SQL.AppendLine("            Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("            Inner Join TblParameter C On C.ParCode='AcNoForCOGS' And C.ParValue Is Not Null ");
            SQL.AppendLine("            Where A.DocDt=@DocDt ");
            SQL.AppendLine("            And A.DocType=@DocType2 ");
            SQL.AppendLine("        ) T Group By AcNo ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Left Join TblPosNo C On C.PosNo=@PosNo ");
            SQL.AppendLine("Left Join TblSite D On C.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter E On D.ProfitCenterCode=E.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", Sm.GetParameter("MenuCodeForDayEndClosing"));
            Sm.CmParam<String>(ref cm, "@JournalDocNo", StockJournalDocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", CurrentBsDate);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocType1", DocType1);
            Sm.CmParam<String>(ref cm, "@DocType2", DocType2);

            return cm;
        }

        private MySqlCommand DelStockMovementPOS(string CurrentBnsDate)
        {
            var cm = new MySqlCommand() { CommandText = "Delete From TblStockMovementPOS Where DocDt=@DocDate And PosNo=@PosNo;" };
            Sm.CmParam<String>(ref cm, "@DocDate", CurrentBnsDate);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            return cm;
        }

        private MySqlCommand SavePOSVAT(string CurCode, ref List<PosTrnPay> l, int i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPOSVAT ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, CancelReason, VoucherRequestPPNDocNo, ");
            SQL.AppendLine("CurCode, Amt, TaxInvoiceNo, TaxInvoiceDt, PaymentType, BankAcCode, PosNo, CtCode, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', Null, Null, @CurCode, @Amt, @TaxInvoiceNo, @DocDt, ");
            SQL.AppendLine("@PaymentType, @BankAcCode, @PosNo, ");
            SQL.AppendLine("(Select SetValue From TblPosSetting Where PosNo=@PosNo And SetCode='StoreCtCode'), ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", l[i].POSVATDocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", l[i].BsDate);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", l[i].VoucherDocNo);
            Sm.CmParam<String>(ref cm, "@BankAcCode", l[i].BankAcCode);
            Sm.CmParam<String>(ref cm, "@PaymentType", l[i].PaymentType);
            Sm.CmParam<String>(ref cm, "@CurCode", CurCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", l[i].TaxAmt);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<String>(ref cm, "@Remark", "Point of Sale " + l[i].BsDate);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private string GenerateVoucherRequestPPNDocNo(string DocDt, int Value)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequestPPN'"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");


            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+"+Value.ToString()+", Char)), " + DocSeqNo + ")  From ( ");
            SQL.Append("Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+" + Value + ", Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo ");
            SQL.Append("       From TblVoucherRequestPPNHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, "+DocSeqNo+") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '"+Value.ToString()+"'), " + DocSeqNo + ") ");
            //SQL.Append("   ), '000" + Value + "' ");
            SQL.Append("), '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        #endregion

        #endregion

        #endregion

        #region FTP

        private byte[] DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }

            return downloadedData;
        }

        #endregion

        #region Event

        #region Form Event

        private void FrmMain_Load(object sender, EventArgs e)
        {
            try
            {
                var frmLogin = new FrmLogin(this);

                if (frmLogin.ShowDialog(this) == DialogResult.OK)
                {
                    if (Sm.GetParameter("DocTitle") == "RM")
                    {
                        this.Text = "Run Market ";
                        this.Icon = (Icon)Properties.Resources.ResourceManager.GetObject("RMIcon");
                    }

                    this.Text +=
                        Gv.VersionNo +
                        " [" +
                        Gv.Database + "-" +
                        Gv.CompanyName + "-" +
                        Gv.CurrentUserCode +
                        "]";
                    mIsMaterialRequestApprovalByDeptEnabled = IsMaterialRequestApprovalByDeptEnabled();
                    ExecQuery();
                    GetParameter();
                    SetMenuParent();
                    RunNotification();

                    if (mIsUseSelfOnBoarding)
                    {
                        
                        SetGrd();
                        SetGrd2();
                        SetGrd3();
                        SetGrd4();
                        ShowData();
                        ShowWidget();
                        ShowCancelledDocCount();
                    }

                    mAccessInd = GetAccessInd("FrmSelfOnBoardingContent");
                    mUserPassword = Sm.GetValue("Select Pwd From TblUser Where Usercode=@Param ", Gv.CurrentUserCode);
                    mUserCodeEncode = Sm.GetHashBase64(Sm.GetHashSha256(Gv.CurrentUserCode.ToUpper()));
                   

                    string[] mFontSizes = GetFontSize(mSelfOnboardingFontSize);

                    Sm.SetLookUpEdit(LueFontSize, mFontSizes);
                    Sm.SetLue(LueFontSize, mFontSizes[0]);

                    if (mAccessInd == "Y")
                    {
                        ShowNews();
                        Grd2.Rows.RemoveAt(Grd2.Rows.Count - 1);
                    }
                    else
                    {
                        //PnlNews.Hide();

                        if (mDocTitle == "PHT")
                            mUserCode = Sm.IsDataExist(
                                "Select 1 From TblEmpPrePensionNotifSetting A " +
                                "Inner Join TblEmployee B On A.EmpCode = B.EmpCode " +
                                "Where B.UserCode = @Param", Gv.CurrentUserCode);
                        else
                            mUserCode = Sm.Find_In_Set(Gv.CurrentUserCode, mUserCodeForSelfOnBoardingPrePensionNotification);

                        panel4.Hide();
                        if (!mUserCode)
                            panel6.Hide();
                        if (mIsUseSelfOnBoarding) ShowEmployeeCount();
                        ShowMeritIncreaseNotification();
                    }

                    foreach (Control c in this.Controls)
                        if (c is MdiClient)
                        {
                            if (Sm.GetParameter("DocTitle") == "RUNMarket" ||
                                Sm.GetParameter("DocTitle") == "RM")
                                c.BackColor = Color.White;
                            else
                                c.BackColor = Color.LightSteelBlue;
                        }

                    if (mIsAutoLogOffActived)
                    {
                        SetTmAutoLogOffInterval();
                        StartTimerAutoLogOff();
                    }

                    if (!mIsUseSelfOnBoarding)
                    {
                        PnlBase.Visible = false;
                        if (Sm.GetParameter("DocTitle").Length > 0)
                        {
                            this.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject(Sm.GetParameter("DocTitle"));
                            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                        }
                        else
                        {
                            this.BackgroundImage = null;
                        }

                        if (Sm.GetParameter("DocTitle") == "PHT" || Sm.GetParameter("DocTitle") == "BBT")
                        {
                            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
                        }
                    }
                    else
                    {
                        this.BackgroundImage = null;
                        LogoSelf.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject(string.Concat(Sm.GetParameter("DocTitle"), "Self"));
                        if (LogoSelf.BackgroundImage == null) LogoSelf.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject("runsystem");
                        LogoSelf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
                        if (Sm.GetParameter("DocTitle") == "IMS" ||
                            Sm.GetParameter("DocTitle") == "RM")
                            this.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject(Sm.GetParameter("DocTitle"));
                    }

                    if (mIsUserNeedToChangeDefaultPassword)
                    {
                        if (mIsPasswordForLoginNeedToEncode)
                        {
                            if (mUserPassword == mUserCodeEncode) ShowFormChangePassword(false);
                        }
                        else
                        {
                            if (mUserPassword.ToUpper() == Gv.CurrentUserCode.ToUpper()) ShowFormChangePassword(false);
                        }
                    }
                    
                    if(frmLogin.IsPwdAlreadyExpired && mIsFormMainShowChangePwd)
                    {
                        ShowFormChangePassword(frmLogin.IsPwdAlreadyExpired);
                    }
                  
                }
                else
                    Application.Exit();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if ((Gv.LogIn ?? "").Length != 0) UpdateLog();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
                Application.Exit();
            }
        }

        #endregion

        #region Misc Control Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                LblWidget1.Font = new Font(
                    LblWidget1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                LblWidget2.Font = new Font(
                    LblWidget1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                RtbWidget1.Font = new Font(
                    LblWidget1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                RtbWidget2.Font = new Font(
                    LblWidget1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void TmAutoLogOff_Tick(object sender, EventArgs e)
        {
            if (Wa.GetIdleTime() > Gv.AutoLogOff)
            {
                TmAutoLogOff.Stop();
                TmAutoLogOff.Enabled = false;
                Sm.SetFormAdditionalSetting(new FrmAutoLogOff(this));
            }
        }

        private void BtnShowHide_Click(object sender, EventArgs e)
        {
            if (mIsShow)
            {
                mIsShow = false;
                TmShowHide.Start();
            }
            else
            {
                mIsShow = true;
                TmShowHide.Start();
            }
        }

        private void TmShowHide_Tick(object sender, EventArgs e)
        {
            if (mIsShow)
            {
                if (PnlBase.Width >= 415)
                {
                    TmShowHide.Stop();
                    BtnShowHide.Text = "HIDE";
                }
                else
                {
                    PnlBase.Width += 66;
                }
            }
            else
            {
                if (PnlBase.Width <= 19)
                {
                    TmShowHide.Stop();
                    BtnShowHide.Text = "SHOW";
                }
                else
                {
                    PnlBase.Width -= 66;
                }
            }
        }

        #endregion

        #endregion

        #region Class

        private class Menu
        {
            public string MenuCode { get; set; }
            public string MenuDesc { get; set; }
            public string Parent { get; set; }
            public string Param { get; set; }
            public string Icon { get; set; }
            public string Enabled { get; set; }
        }

        #region POS

        private class PosTrn
        {
            public string TrnNo { get; set; }
            public string BsDate { get; set; }
            public string TrnDtTm { get; set; }
            public string DtlNo { get; set; }
            public string SlsType { get; set; }
            public string ItCode { get; set; }
            public decimal Qty { get; set; }
            public string CreateBy { get; set; }
            public string CreateDt { get; set; }
            public string RTrnNo { get; set; }
            public string RBsDate { get; set; }
            public string RDtlNo { get; set; }
        }

        private class PosTrnPay
        {
            public string BsDate { get; set; }
            public string PayTpNo { get; set; }
            public string PaymentType { get; set; }
            public string BankAcCode { get; set; }
            public string AutoNoDebit { get; set; }
            public decimal PayAmt { get; set; }
            public decimal AmtBefTax { get; set; }
            public decimal TaxAmt { get; set; }
            public string VoucherRequestDocNo { get; set; }
            public string VoucherDocNo { get; set; }
            public string JournalDocNo { get; set; }
            public string POSVATDocNo { get; set; }
            //public string VoucherRequestPPNDocNo { get; set; }
            //public string VoucherRequestDocNo2 { get; set; }
        }

        private class PosItem
        {
            public string ItCode { get; set; }
            public decimal Qty { get; set; }
        }

        private class PosStock
        {
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public decimal Qty { get; set; }
        }

        private class PosMovement
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DocDt { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public decimal Qty { get; set; }
            public string CreateBy { get; set; }
            public string CreateDt { get; set; }
        }

        private class Retur
        {
            public string DocNoRetur { get; set; }
            public string DNoRetur { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string WhsCode { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public string DocDt { get; set; }
            public string UserCode { get; set; }
            public string Dt { get; set; }
        }

        #endregion

        #region Self On Boarding

        private class TempSelfOnBoarding
        {
            public string MenuCode { get; set; }
            public string Desc { get; set; }
            public string Param { get; set; }
            public string Query1 { get; set; }
            public string Query1Ind { get; set; }
            public string Query2 { get; set; }
            public string Query2Ind { get; set; }
        }

        private class SelfOnboarding
        {
            // hasil olahan dari TempSelfOnboarding
            public string Desc { get; set; }
            public string MenuCode { get; set; }
            public string Param { get; set; }
            public decimal Target { get; set; }
            public decimal Existing { get; set; }
            public decimal Percentage { get; set; }
        }

        private class TempSelfOnBoarding3
        {
            public string Id { set; get; }
            public string Desc { set; get; }
            public string Query { set; get; }
            public string QueryInd { set; get; }
        }

        private class SelfOnBoarding3
        {
            public string Id { set; get; }
            public string Desc { set; get; }
            public decimal Number { set; get; }
        }

        #endregion

        private void PnlWidget_Paint(object sender, PaintEventArgs e)
        {

        }

        #endregion

    }
}