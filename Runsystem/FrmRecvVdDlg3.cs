﻿#region Update
/*
    03/02/2017 [TKG/IMS] New application
    26/03/2020 [TKG/IMS] Receiving item from vendor bisa memproses data receiving expedition secara partial.
    02/10/2020 [TKG/IMS] receiving expedition ketika diproses di recv vd sisanya akan hangus kalau tidak diproses, dan harus diproses kembali di recv expedition
    14/12/2020 [WED/IMS] hanya ambil data expedition yg approved
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvVdDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvVd mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRecvVdDlg3(FrmRecvVd FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "",
                        "Date",
                        "Local Document#",
                        "Warehouse",

                        //6-8
                        "Vendor",
                        "Vendor's DO#",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 20, 80, 150, 200,
                        
                        //6-8
                        200, 130, 250
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 8 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DocDt, B.LocalDocNo, ");
            SQL.AppendLine("D.WhsName, C.VdName, B.VdDONo, B.Remark ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct T1.DocNo ");
            SQL.AppendLine("    From TblRecvExpeditionHdr T1, TblRecvExpeditionDtl T2, TblItem T3 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.CancelInd='N' ");
            SQL.AppendLine("    And T2.Status = 'A' ");
            SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.VdCode=@VdCode ");
            SQL.AppendLine("    And T2.QtyPurchase>0.00 ");
            SQL.AppendLine("    And T2.RecvVdQtyPurchase<=0.00 ");
            SQL.AppendLine("    And T2.ItCode=T3.ItCode ");
            //SQL.AppendLine("    And T3.ServiceItemInd='N' ");
            //SQL.AppendLine("    And T2.QtyPurchase-T2.RecvVdQtyPurchase>0.00 ");
            //SQL.AppendLine("    And T1.DocNo In ( ");
            //SQL.AppendLine("        Select Distinct X1.DocNo ");
            //SQL.AppendLine("        From TblRecvExpeditionHdr X1, TblRecvExpeditionDtl X2 ");
            //SQL.AppendLine("        Where X1.DocNo=X2.DocNo And X2.CancelInd='N' ");
            //SQL.AppendLine("        And X1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("        And X1.VdCode=@VdCode ");
            //SQL.AppendLine("    ) ");
            SQL.AppendLine("    And Not Exists( ");
            SQL.AppendLine("        Select 1 ");
            SQL.AppendLine("        From TblRecvVdHdr X1, TblRecvVdDtl X2 ");
            SQL.AppendLine("        Where X1.DocNo=X2.DocNo ");
            SQL.AppendLine("        And X2.CancelInd='N' And X2.Status In ('O', 'A') ");
            SQL.AppendLine("        And X1.VdCode=@VdCode ");
            SQL.AppendLine("        And X1.RecvExpeditionDocNo Is Not Null ");
            SQL.AppendLine("        And X2.RecvExpeditionDocNo=T2.DocNo ");
            SQL.AppendLine("        And X2.RecvExpeditionDNo=T2.DNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblRecvExpeditionHdr B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblWarehouse D ");
            SQL.AppendLine("On B.WhsCode=D.WhsCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("    Where WhsCode=D.WhsCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "B.DocNo", "B.LocalDocNo" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.DocDt Desc, B.DocNo;",
                        new string[] 
                        { 
                             //0
                             "DocNo", 
                             
                             //1-5
                             "DocDt", 
                             "LocalDocNo", 
                             "WhsName",
                             "VdName",
                             "VdDONo",

                             //6
                             "Remark"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtRecvExpeditionDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.ClearGrd();
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRecvExpedition(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRecvExpedition(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
