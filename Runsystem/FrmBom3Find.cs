﻿#region Update
/*
    15/01/2020 [TKG/IMS] New Application
    05/02/2020 [TKG/IMS] tambah cancel, revision
    18/03/2020 [TKG/IMS] tambah filter document date.
    06/06/2020 [TKG/IMS] tambah customer's PO#.
    19/06/2020 [HAR/IMS] BUG : saat show data kolom revision ambil kolom 19 (createby) haruse kolom 3 (revisi)
    07/12/2020 [WED/IMS] tambah informasi status dan department
    14/07/2021 [VIN/IMS] Order By CreateDt bukan DocDt
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmBom3Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmBom3 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBom3Find(FrmBom3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (");
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.SOContractDocNo, F.ProjectCode, F.ProjectName, B.PONo, ");
            SQL.AppendLine("A.ItCode, C.ItName, C.ItCodeInternal, A.Qty, C.PurchaseUomCode, C.Specification, A.Remark, G.SOContractRemark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, A.RevNo, A.Revision, A.CancelInd, H.DeptName, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc ");
            SQL.AppendLine("From TblBom2Hdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblBOQHdr D On B.BOQDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo=E.DocNO ");
            SQL.AppendLine("Inner Join TblProjectGroup F On E.PGCode=F.PGCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T1.ItCode, Group_Concat(Distinct T3.Remark Separator ' ; ') As SOContractRemark ");
            SQL.AppendLine("    From TblBom2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblSOContractHdr T2 On T1.SOContractDocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl T3 On T2.DocNo=T3.DocNo And T3.Remark is Not Null ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T1.DocNo, T1.ItCode ");
            SQL.AppendLine(") G On A.DocNo=G.DocNo And A.ItCode=G.ItCode ");
            SQL.AppendLine("Left Join TblDepartment H On A.DeptCode = H.DeptCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsForRevision)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select A.DocNo, A.DocDt, A.SOContractDocNo, F.ProjectCode, F.ProjectName, ");
                SQL.AppendLine("A.ItCode, C.ItName, C.ItCodeInternal, A.Qty, C.PurchaseUomCode, C.Specification, A.Remark, G.SOContractRemark, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, A.RevNo, A.Revision, A.CancelInd, H.DeptName, ");
                SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc ");
                SQL.AppendLine("From TblBom2RevisionHdr A ");
                SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Inner Join TblBOQHdr D On B.BOQDocNo=D.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo=E.DocNO ");
                SQL.AppendLine("Inner Join TblProjectGroup F On E.PGCode=F.PGCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T1.ItCode, Group_Concat(Distinct T3.Remark Separator ' ; ') As SOContractRemark ");
                SQL.AppendLine("    From TblBom2RevisionHdr T1 ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T2 On T1.SOContractDocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblSOContractDtl T3 On T2.DocNo=T3.DocNo And T3.Remark is Not Null ");
                SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("    Group By T1.DocNo, T1.ItCode ");
                SQL.AppendLine(") G On A.DocNo=G.DocNo And A.ItCode=G.ItCode ");
                SQL.AppendLine("Left Join TblDepartment H On A.DeptCode = H.DeptCode ");
                SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine(Filter);

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Revision",
                        "Cancel",
                        "SO Contract#",

                        //6-10
                        "Project's Code",
                        "Project's Name",
                        "Customer's PO#",
                        "Finished Good' Code",
                        "Finished Good' Name",
                        
                        //11-15
                        "Local Code",
                        "Quantity",
                        "UoM",
                        "Specification",
                        "SO Contract's remark",
                        
                        //16-20
                        "Remark",
                        "Created By",
                        "Created Date", 
                        "Created Time", 
                        "Last Updated By",
                        
                        //21-25
                        "Last Updated Date", 
                        "Last Updated Time",
                        "RevNo",
                        "Department",
                        "Status"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 100, 100, 100, 130, 
                        
                        //6-10
                        130, 200, 180, 130, 200, 
                        
                        //11-15
                        130, 100, 100, 200, 200, 
                        
                        //16-20
                        200, 130, 130, 130, 130, 
                        
                        //21-25
                        130, 130, 0, 200, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 23 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, false);
            Grd1.Cols[25].Move(5);
            Grd1.Cols[24].Move(6);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, new string[] { "T.ProjectCode", "T.ProjectName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter) + " Order By T.CreateDt Desc, T.DocNo Desc, T.RevNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "Revision", "CancelInd", "SOContractDocNo", "ProjectCode", 
                            
                            //6-10
                            "ProjectName", "PONo", "ItCode", "ItName", "ItCodeInternal", 
                            
                            //11-15
                            "Qty", "PurchaseUomCode", "Specification", "SOContractRemark", "Remark", 
                            
                            //16-20
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "RevNo",

                            //21-22
                            "DeptName", "StatusDesc"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 23));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        #endregion

        #endregion
    }
}
