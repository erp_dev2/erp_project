﻿namespace RunSystem
{
    partial class FrmSP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSP));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.LueHSCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel19 = new System.Windows.Forms.Panel();
            this.BtnPlaceDelivery = new DevExpress.XtraEditors.SimpleButton();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtNameReceipt = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtNameDelivery = new DevExpress.XtraEditors.TextEdit();
            this.BtnReceipt = new DevExpress.XtraEditors.SimpleButton();
            this.MeeDelivery = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeReceipt = new DevExpress.XtraEditors.MemoExEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueDischarge = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueLoading = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.DtePEBDt = new DevExpress.XtraEditors.DateEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.DteVLegalDt = new DevExpress.XtraEditors.DateEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtVLegal = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtKpbc = new DevExpress.XtraEditors.TextEdit();
            this.TxtSPName = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtSize = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtMark = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblLocalDoc = new System.Windows.Forms.Label();
            this.BtnSource = new DevExpress.XtraEditors.SimpleButton();
            this.label12 = new System.Windows.Forms.Label();
            this.MeeConsignee = new DevExpress.XtraEditors.MemoExEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.DteStfDt = new DevExpress.XtraEditors.DateEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.BtnCtNotify = new DevExpress.XtraEditors.SimpleButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtBL = new DevExpress.XtraEditors.TextEdit();
            this.TxtPEB = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSource = new DevExpress.XtraEditors.TextEdit();
            this.LueNotify = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeNotify = new DevExpress.XtraEditors.MemoExEdit();
            this.DteRouteDt = new DevExpress.XtraEditors.DateEdit();
            this.LueType = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.SFD1 = new System.Windows.Forms.SaveFileDialog();
            this.OD1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueHSCode.Properties)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNameReceipt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNameDelivery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDelivery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReceipt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDischarge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLoading.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePEBDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePEBDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVLegalDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVLegalDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVLegal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKpbc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSPName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeConsignee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStfDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStfDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPEB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueNotify.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNotify.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRouteDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRouteDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 473);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.BtnDownload);
            this.splitContainer1.Panel1.Controls.Add(this.LueHSCode);
            this.splitContainer1.Panel1.Controls.Add(this.panel19);
            this.splitContainer1.Panel1.Controls.Add(this.TxtLocalDocNo);
            this.splitContainer1.Panel1.Controls.Add(this.LblLocalDoc);
            this.splitContainer1.Panel1.Controls.Add(this.BtnSource);
            this.splitContainer1.Panel1.Controls.Add(this.label12);
            this.splitContainer1.Panel1.Controls.Add(this.MeeConsignee);
            this.splitContainer1.Panel1.Controls.Add(this.label18);
            this.splitContainer1.Panel1.Controls.Add(this.label11);
            this.splitContainer1.Panel1.Controls.Add(this.DteStfDt);
            this.splitContainer1.Panel1.Controls.Add(this.label16);
            this.splitContainer1.Panel1.Controls.Add(this.BtnCtNotify);
            this.splitContainer1.Panel1.Controls.Add(this.label10);
            this.splitContainer1.Panel1.Controls.Add(this.label21);
            this.splitContainer1.Panel1.Controls.Add(this.TxtBL);
            this.splitContainer1.Panel1.Controls.Add(this.TxtPEB);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.label19);
            this.splitContainer1.Panel1.Controls.Add(this.LueCtCode);
            this.splitContainer1.Panel1.Controls.Add(this.label17);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.DteDocDt);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.TxtDocNo);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.LueStatus);
            this.splitContainer1.Panel1.Controls.Add(this.TxtSource);
            this.splitContainer1.Panel1.Controls.Add(this.LueNotify);
            this.splitContainer1.Panel1.Controls.Add(this.MeeNotify);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.DteRouteDt);
            this.splitContainer1.Panel2.Controls.Add(this.LueType);
            this.splitContainer1.Panel2.Controls.Add(this.Grd1);
            this.splitContainer1.Size = new System.Drawing.Size(772, 473);
            this.splitContainer1.SplitterDistance = 295;
            this.splitContainer1.TabIndex = 0;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(346, 51);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 140;
            this.BtnDownload.ToolTip = "Download File";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // LueHSCode
            // 
            this.LueHSCode.EnterMoveNextControl = true;
            this.LueHSCode.Location = new System.Drawing.Point(111, 227);
            this.LueHSCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueHSCode.Name = "LueHSCode";
            this.LueHSCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHSCode.Properties.Appearance.Options.UseFont = true;
            this.LueHSCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHSCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueHSCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHSCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueHSCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHSCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueHSCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHSCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueHSCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueHSCode.Properties.DropDownRows = 30;
            this.LueHSCode.Properties.NullText = "[Empty]";
            this.LueHSCode.Properties.PopupWidth = 300;
            this.LueHSCode.Size = new System.Drawing.Size(234, 20);
            this.LueHSCode.TabIndex = 30;
            this.LueHSCode.ToolTip = "F4 : Show/hide list";
            this.LueHSCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueHSCode.EditValueChanged += new System.EventHandler(this.LueHSCode_EditValueChanged);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel19.Controls.Add(this.BtnPlaceDelivery);
            this.panel19.Controls.Add(this.label25);
            this.panel19.Controls.Add(this.TxtNameReceipt);
            this.panel19.Controls.Add(this.label24);
            this.panel19.Controls.Add(this.TxtNameDelivery);
            this.panel19.Controls.Add(this.BtnReceipt);
            this.panel19.Controls.Add(this.MeeDelivery);
            this.panel19.Controls.Add(this.MeeReceipt);
            this.panel19.Controls.Add(this.label9);
            this.panel19.Controls.Add(this.LueDischarge);
            this.panel19.Controls.Add(this.label8);
            this.panel19.Controls.Add(this.label3);
            this.panel19.Controls.Add(this.LueLoading);
            this.panel19.Controls.Add(this.label2);
            this.panel19.Controls.Add(this.DtePEBDt);
            this.panel19.Controls.Add(this.label22);
            this.panel19.Controls.Add(this.DteVLegalDt);
            this.panel19.Controls.Add(this.label20);
            this.panel19.Controls.Add(this.TxtVLegal);
            this.panel19.Controls.Add(this.label14);
            this.panel19.Controls.Add(this.TxtKpbc);
            this.panel19.Controls.Add(this.TxtSPName);
            this.panel19.Controls.Add(this.label23);
            this.panel19.Controls.Add(this.label13);
            this.panel19.Controls.Add(this.TxtSize);
            this.panel19.Controls.Add(this.label7);
            this.panel19.Controls.Add(this.TxtMark);
            this.panel19.Controls.Add(this.label15);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel19.Location = new System.Drawing.Point(382, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(390, 295);
            this.panel19.TabIndex = 139;
            // 
            // BtnPlaceDelivery
            // 
            this.BtnPlaceDelivery.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPlaceDelivery.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPlaceDelivery.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPlaceDelivery.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPlaceDelivery.Appearance.Options.UseBackColor = true;
            this.BtnPlaceDelivery.Appearance.Options.UseFont = true;
            this.BtnPlaceDelivery.Appearance.Options.UseForeColor = true;
            this.BtnPlaceDelivery.Appearance.Options.UseTextOptions = true;
            this.BtnPlaceDelivery.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPlaceDelivery.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPlaceDelivery.Image = ((System.Drawing.Image)(resources.GetObject("BtnPlaceDelivery.Image")));
            this.BtnPlaceDelivery.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPlaceDelivery.Location = new System.Drawing.Point(345, 135);
            this.BtnPlaceDelivery.Name = "BtnPlaceDelivery";
            this.BtnPlaceDelivery.Size = new System.Drawing.Size(24, 21);
            this.BtnPlaceDelivery.TabIndex = 69;
            this.BtnPlaceDelivery.ToolTip = "Copy Port of Discharge to Place of Delivery";
            this.BtnPlaceDelivery.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPlaceDelivery.ToolTipTitle = "Run System";
            this.BtnPlaceDelivery.Click += new System.EventHandler(this.BtnPlaceDelivery_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(18, 51);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(98, 14);
            this.label25.TabIndex = 70;
            this.label25.Text = "Name of Receipt";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNameReceipt
            // 
            this.TxtNameReceipt.EnterMoveNextControl = true;
            this.TxtNameReceipt.Location = new System.Drawing.Point(121, 48);
            this.TxtNameReceipt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNameReceipt.Name = "TxtNameReceipt";
            this.TxtNameReceipt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNameReceipt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNameReceipt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNameReceipt.Properties.Appearance.Options.UseFont = true;
            this.TxtNameReceipt.Size = new System.Drawing.Size(248, 20);
            this.TxtNameReceipt.TabIndex = 71;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(17, 95);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(99, 14);
            this.label24.TabIndex = 63;
            this.label24.Text = "Name of Delivery";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNameDelivery
            // 
            this.TxtNameDelivery.EnterMoveNextControl = true;
            this.TxtNameDelivery.Location = new System.Drawing.Point(121, 92);
            this.TxtNameDelivery.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNameDelivery.Name = "TxtNameDelivery";
            this.TxtNameDelivery.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNameDelivery.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNameDelivery.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNameDelivery.Properties.Appearance.Options.UseFont = true;
            this.TxtNameDelivery.Size = new System.Drawing.Size(248, 20);
            this.TxtNameDelivery.TabIndex = 64;
            // 
            // BtnReceipt
            // 
            this.BtnReceipt.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnReceipt.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnReceipt.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnReceipt.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnReceipt.Appearance.Options.UseBackColor = true;
            this.BtnReceipt.Appearance.Options.UseFont = true;
            this.BtnReceipt.Appearance.Options.UseForeColor = true;
            this.BtnReceipt.Appearance.Options.UseTextOptions = true;
            this.BtnReceipt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnReceipt.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnReceipt.Image = ((System.Drawing.Image)(resources.GetObject("BtnReceipt.Image")));
            this.BtnReceipt.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnReceipt.Location = new System.Drawing.Point(345, 68);
            this.BtnReceipt.Name = "BtnReceipt";
            this.BtnReceipt.Size = new System.Drawing.Size(24, 21);
            this.BtnReceipt.TabIndex = 65;
            this.BtnReceipt.ToolTip = "Copy Port of Loading to Place Receipt";
            this.BtnReceipt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnReceipt.ToolTipTitle = "Run System";
            this.BtnReceipt.Click += new System.EventHandler(this.BtnReceipt_Click);
            // 
            // MeeDelivery
            // 
            this.MeeDelivery.EnterMoveNextControl = true;
            this.MeeDelivery.Location = new System.Drawing.Point(121, 136);
            this.MeeDelivery.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDelivery.Name = "MeeDelivery";
            this.MeeDelivery.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDelivery.Properties.Appearance.Options.UseFont = true;
            this.MeeDelivery.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDelivery.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDelivery.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDelivery.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDelivery.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDelivery.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDelivery.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeDelivery.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDelivery.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDelivery.Properties.MaxLength = 400;
            this.MeeDelivery.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeDelivery.Properties.ShowIcon = false;
            this.MeeDelivery.Size = new System.Drawing.Size(221, 20);
            this.MeeDelivery.TabIndex = 68;
            this.MeeDelivery.ToolTip = "F4 : Show/hide text";
            this.MeeDelivery.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDelivery.ToolTipTitle = "Run System";
            // 
            // MeeReceipt
            // 
            this.MeeReceipt.EnterMoveNextControl = true;
            this.MeeReceipt.Location = new System.Drawing.Point(121, 70);
            this.MeeReceipt.Margin = new System.Windows.Forms.Padding(5);
            this.MeeReceipt.Name = "MeeReceipt";
            this.MeeReceipt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReceipt.Properties.Appearance.Options.UseFont = true;
            this.MeeReceipt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReceipt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeReceipt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReceipt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeReceipt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReceipt.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeReceipt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeReceipt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeReceipt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeReceipt.Properties.MaxLength = 400;
            this.MeeReceipt.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeReceipt.Properties.ShowIcon = false;
            this.MeeReceipt.Size = new System.Drawing.Size(221, 20);
            this.MeeReceipt.TabIndex = 73;
            this.MeeReceipt.ToolTip = "F4 : Show/hide text";
            this.MeeReceipt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeReceipt.ToolTipTitle = "Run System";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(20, 139);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 14);
            this.label9.TabIndex = 67;
            this.label9.Text = "Place of Delivery";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDischarge
            // 
            this.LueDischarge.EnterMoveNextControl = true;
            this.LueDischarge.Location = new System.Drawing.Point(121, 114);
            this.LueDischarge.Margin = new System.Windows.Forms.Padding(5);
            this.LueDischarge.Name = "LueDischarge";
            this.LueDischarge.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDischarge.Properties.Appearance.Options.UseFont = true;
            this.LueDischarge.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDischarge.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDischarge.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDischarge.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDischarge.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDischarge.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDischarge.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDischarge.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDischarge.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDischarge.Properties.DropDownRows = 30;
            this.LueDischarge.Properties.NullText = "[Empty]";
            this.LueDischarge.Properties.PopupWidth = 300;
            this.LueDischarge.Size = new System.Drawing.Size(248, 20);
            this.LueDischarge.TabIndex = 66;
            this.LueDischarge.ToolTip = "F4 : Show/hide list";
            this.LueDischarge.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(15, 117);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 14);
            this.label8.TabIndex = 65;
            this.label8.Text = "Port of Discharge";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(21, 73);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 14);
            this.label3.TabIndex = 72;
            this.label3.Text = "Place of Receipt";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLoading
            // 
            this.LueLoading.EnterMoveNextControl = true;
            this.LueLoading.Location = new System.Drawing.Point(121, 26);
            this.LueLoading.Margin = new System.Windows.Forms.Padding(5);
            this.LueLoading.Name = "LueLoading";
            this.LueLoading.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLoading.Properties.Appearance.Options.UseFont = true;
            this.LueLoading.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLoading.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLoading.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLoading.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLoading.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLoading.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLoading.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLoading.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLoading.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLoading.Properties.DropDownRows = 30;
            this.LueLoading.Properties.NullText = "[Empty]";
            this.LueLoading.Properties.PopupWidth = 300;
            this.LueLoading.Size = new System.Drawing.Size(248, 20);
            this.LueLoading.TabIndex = 69;
            this.LueLoading.ToolTip = "F4 : Show/hide list";
            this.LueLoading.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(25, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 14);
            this.label2.TabIndex = 68;
            this.label2.Text = "Port of Loading";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DtePEBDt
            // 
            this.DtePEBDt.EditValue = null;
            this.DtePEBDt.EnterMoveNextControl = true;
            this.DtePEBDt.Location = new System.Drawing.Point(121, 268);
            this.DtePEBDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePEBDt.Name = "DtePEBDt";
            this.DtePEBDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePEBDt.Properties.Appearance.Options.UseFont = true;
            this.DtePEBDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePEBDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePEBDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePEBDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePEBDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePEBDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePEBDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePEBDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePEBDt.Properties.MaxLength = 8;
            this.DtePEBDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePEBDt.Size = new System.Drawing.Size(120, 20);
            this.DtePEBDt.TabIndex = 81;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(58, 271);
            this.label22.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(58, 14);
            this.label22.TabIndex = 80;
            this.label22.Text = "PEB Date";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteVLegalDt
            // 
            this.DteVLegalDt.EditValue = null;
            this.DteVLegalDt.EnterMoveNextControl = true;
            this.DteVLegalDt.Location = new System.Drawing.Point(121, 246);
            this.DteVLegalDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteVLegalDt.Name = "DteVLegalDt";
            this.DteVLegalDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVLegalDt.Properties.Appearance.Options.UseFont = true;
            this.DteVLegalDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVLegalDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteVLegalDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteVLegalDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteVLegalDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteVLegalDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteVLegalDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteVLegalDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteVLegalDt.Properties.MaxLength = 8;
            this.DteVLegalDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteVLegalDt.Size = new System.Drawing.Size(120, 20);
            this.DteVLegalDt.TabIndex = 79;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(39, 250);
            this.label20.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 14);
            this.label20.TabIndex = 78;
            this.label20.Text = "V-Legal Date";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVLegal
            // 
            this.TxtVLegal.EnterMoveNextControl = true;
            this.TxtVLegal.Location = new System.Drawing.Point(121, 224);
            this.TxtVLegal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVLegal.Name = "TxtVLegal";
            this.TxtVLegal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVLegal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVLegal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVLegal.Properties.Appearance.Options.UseFont = true;
            this.TxtVLegal.Properties.MaxLength = 30;
            this.TxtVLegal.Size = new System.Drawing.Size(248, 20);
            this.TxtVLegal.TabIndex = 77;
            this.TxtVLegal.Validated += new System.EventHandler(this.TxtVLegal_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(69, 227);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 14);
            this.label14.TabIndex = 76;
            this.label14.Text = "V-Legal";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKpbc
            // 
            this.TxtKpbc.EnterMoveNextControl = true;
            this.TxtKpbc.Location = new System.Drawing.Point(121, 4);
            this.TxtKpbc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKpbc.Name = "TxtKpbc";
            this.TxtKpbc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKpbc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKpbc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKpbc.Properties.Appearance.Options.UseFont = true;
            this.TxtKpbc.Size = new System.Drawing.Size(248, 20);
            this.TxtKpbc.TabIndex = 36;
            // 
            // TxtSPName
            // 
            this.TxtSPName.EnterMoveNextControl = true;
            this.TxtSPName.Location = new System.Drawing.Point(121, 202);
            this.TxtSPName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSPName.Name = "TxtSPName";
            this.TxtSPName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSPName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSPName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSPName.Properties.Appearance.Options.UseFont = true;
            this.TxtSPName.Properties.MaxLength = 50;
            this.TxtSPName.Size = new System.Drawing.Size(248, 20);
            this.TxtSPName.TabIndex = 75;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(81, 7);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 14);
            this.label23.TabIndex = 35;
            this.label23.Text = "KPBC";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(41, 205);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 14);
            this.label13.TabIndex = 74;
            this.label13.Text = "Sales Person";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSize
            // 
            this.TxtSize.EnterMoveNextControl = true;
            this.TxtSize.Location = new System.Drawing.Point(121, 180);
            this.TxtSize.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSize.Name = "TxtSize";
            this.TxtSize.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSize.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSize.Properties.Appearance.Options.UseFont = true;
            this.TxtSize.Size = new System.Drawing.Size(248, 20);
            this.TxtSize.TabIndex = 73;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(32, 183);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 14);
            this.label7.TabIndex = 72;
            this.label7.Text = "Container Size";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMark
            // 
            this.TxtMark.EnterMoveNextControl = true;
            this.TxtMark.Location = new System.Drawing.Point(121, 158);
            this.TxtMark.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMark.Name = "TxtMark";
            this.TxtMark.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMark.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMark.Properties.Appearance.Options.UseFont = true;
            this.TxtMark.Size = new System.Drawing.Size(248, 20);
            this.TxtMark.TabIndex = 71;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(84, 161);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 14);
            this.label15.TabIndex = 70;
            this.label15.Text = "Mark";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(111, 51);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtLocalDocNo.TabIndex = 12;
            // 
            // LblLocalDoc
            // 
            this.LblLocalDoc.AutoSize = true;
            this.LblLocalDoc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLocalDoc.Location = new System.Drawing.Point(12, 54);
            this.LblLocalDoc.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblLocalDoc.Name = "LblLocalDoc";
            this.LblLocalDoc.Size = new System.Drawing.Size(95, 14);
            this.LblLocalDoc.TabIndex = 11;
            this.LblLocalDoc.Text = "Local Document";
            this.LblLocalDoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSource
            // 
            this.BtnSource.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSource.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSource.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSource.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSource.Appearance.Options.UseBackColor = true;
            this.BtnSource.Appearance.Options.UseFont = true;
            this.BtnSource.Appearance.Options.UseForeColor = true;
            this.BtnSource.Appearance.Options.UseTextOptions = true;
            this.BtnSource.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSource.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSource.Image = ((System.Drawing.Image)(resources.GetObject("BtnSource.Image")));
            this.BtnSource.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSource.Location = new System.Drawing.Point(351, 138);
            this.BtnSource.Name = "BtnSource";
            this.BtnSource.Size = new System.Drawing.Size(24, 21);
            this.BtnSource.TabIndex = 21;
            this.BtnSource.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSource.ToolTipTitle = "Run System";
            this.BtnSource.Click += new System.EventHandler(this.BtnSource_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(62, 141);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 14);
            this.label12.TabIndex = 19;
            this.label12.Text = "Source";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeConsignee
            // 
            this.MeeConsignee.EnterMoveNextControl = true;
            this.MeeConsignee.Location = new System.Drawing.Point(111, 249);
            this.MeeConsignee.Margin = new System.Windows.Forms.Padding(5);
            this.MeeConsignee.Name = "MeeConsignee";
            this.MeeConsignee.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConsignee.Properties.Appearance.Options.UseFont = true;
            this.MeeConsignee.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConsignee.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeConsignee.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConsignee.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeConsignee.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConsignee.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeConsignee.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeConsignee.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeConsignee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeConsignee.Properties.MaxLength = 400;
            this.MeeConsignee.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeConsignee.Properties.ShowIcon = false;
            this.MeeConsignee.Size = new System.Drawing.Size(234, 20);
            this.MeeConsignee.TabIndex = 32;
            this.MeeConsignee.ToolTip = "F4 : Show/hide text";
            this.MeeConsignee.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeConsignee.ToolTipTitle = "Run System";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(44, 252);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 14);
            this.label18.TabIndex = 31;
            this.label18.Text = "Consignee";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(42, 186);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 14);
            this.label11.TabIndex = 25;
            this.label11.Text = "Also Notify";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteStfDt
            // 
            this.DteStfDt.EditValue = null;
            this.DteStfDt.EnterMoveNextControl = true;
            this.DteStfDt.Location = new System.Drawing.Point(111, 73);
            this.DteStfDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStfDt.Name = "DteStfDt";
            this.DteStfDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStfDt.Properties.Appearance.Options.UseFont = true;
            this.DteStfDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStfDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStfDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStfDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStfDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStfDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStfDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStfDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStfDt.Properties.MaxLength = 8;
            this.DteStfDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStfDt.Size = new System.Drawing.Size(120, 20);
            this.DteStfDt.TabIndex = 14;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(27, 75);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 14);
            this.label16.TabIndex = 13;
            this.label16.Text = "Stuffing Date";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCtNotify
            // 
            this.BtnCtNotify.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtNotify.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtNotify.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtNotify.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtNotify.Appearance.Options.UseBackColor = true;
            this.BtnCtNotify.Appearance.Options.UseFont = true;
            this.BtnCtNotify.Appearance.Options.UseForeColor = true;
            this.BtnCtNotify.Appearance.Options.UseTextOptions = true;
            this.BtnCtNotify.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtNotify.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtNotify.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtNotify.Image")));
            this.BtnCtNotify.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtNotify.Location = new System.Drawing.Point(351, 160);
            this.BtnCtNotify.Name = "BtnCtNotify";
            this.BtnCtNotify.Size = new System.Drawing.Size(24, 21);
            this.BtnCtNotify.TabIndex = 24;
            this.BtnCtNotify.ToolTip = "Add Costumer Notify Party";
            this.BtnCtNotify.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtNotify.ToolTipTitle = "Run System";
            this.BtnCtNotify.Click += new System.EventHandler(this.BtnCtNotify_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(65, 97);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 14);
            this.label10.TabIndex = 15;
            this.label10.Text = "Status";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(53, 229);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 14);
            this.label21.TabIndex = 29;
            this.label21.Text = "HS Code";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBL
            // 
            this.TxtBL.EnterMoveNextControl = true;
            this.TxtBL.Location = new System.Drawing.Point(111, 205);
            this.TxtBL.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBL.Name = "TxtBL";
            this.TxtBL.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBL.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBL.Properties.Appearance.Options.UseFont = true;
            this.TxtBL.Size = new System.Drawing.Size(234, 20);
            this.TxtBL.TabIndex = 28;
            // 
            // TxtPEB
            // 
            this.TxtPEB.EnterMoveNextControl = true;
            this.TxtPEB.Location = new System.Drawing.Point(111, 271);
            this.TxtPEB.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPEB.Name = "TxtPEB";
            this.TxtPEB.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPEB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPEB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPEB.Properties.Appearance.Options.UseFont = true;
            this.TxtPEB.Size = new System.Drawing.Size(234, 20);
            this.TxtPEB.TabIndex = 34;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(60, 274);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 14);
            this.label6.TabIndex = 33;
            this.label6.Text = "PEB No";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(12, 207);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(95, 14);
            this.label19.TabIndex = 27;
            this.label19.Text = "Booking / BL No";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(111, 117);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(234, 20);
            this.LueCtCode.TabIndex = 18;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(48, 119);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 14);
            this.label17.TabIndex = 17;
            this.label17.Text = "Customer";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(36, 163);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 14);
            this.label5.TabIndex = 22;
            this.label5.Text = "Notify Party";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(111, 29);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(120, 20);
            this.DteDocDt.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(74, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 14);
            this.label4.TabIndex = 9;
            this.label4.Text = "Date";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(111, 7);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtDocNo.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(34, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 7;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueStatus
            // 
            this.LueStatus.EnterMoveNextControl = true;
            this.LueStatus.Location = new System.Drawing.Point(111, 95);
            this.LueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueStatus.Name = "LueStatus";
            this.LueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.Appearance.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStatus.Properties.DropDownRows = 30;
            this.LueStatus.Properties.NullText = "[Empty]";
            this.LueStatus.Properties.PopupWidth = 300;
            this.LueStatus.Size = new System.Drawing.Size(234, 20);
            this.LueStatus.TabIndex = 16;
            this.LueStatus.ToolTip = "F4 : Show/hide list";
            this.LueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged);
            // 
            // TxtSource
            // 
            this.TxtSource.EnterMoveNextControl = true;
            this.TxtSource.Location = new System.Drawing.Point(111, 139);
            this.TxtSource.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSource.Name = "TxtSource";
            this.TxtSource.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSource.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSource.Properties.Appearance.Options.UseFont = true;
            this.TxtSource.Size = new System.Drawing.Size(234, 20);
            this.TxtSource.TabIndex = 20;
            // 
            // LueNotify
            // 
            this.LueNotify.EnterMoveNextControl = true;
            this.LueNotify.Location = new System.Drawing.Point(111, 161);
            this.LueNotify.Margin = new System.Windows.Forms.Padding(5);
            this.LueNotify.Name = "LueNotify";
            this.LueNotify.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.Appearance.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueNotify.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueNotify.Properties.DropDownRows = 30;
            this.LueNotify.Properties.NullText = "[Empty]";
            this.LueNotify.Properties.PopupWidth = 300;
            this.LueNotify.Size = new System.Drawing.Size(234, 20);
            this.LueNotify.TabIndex = 23;
            this.LueNotify.ToolTip = "F4 : Show/hide list";
            this.LueNotify.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueNotify.EditValueChanged += new System.EventHandler(this.LueNotify_EditValueChanged_1);
            // 
            // MeeNotify
            // 
            this.MeeNotify.EnterMoveNextControl = true;
            this.MeeNotify.Location = new System.Drawing.Point(111, 183);
            this.MeeNotify.Margin = new System.Windows.Forms.Padding(5);
            this.MeeNotify.Name = "MeeNotify";
            this.MeeNotify.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.Appearance.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeNotify.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeNotify.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeNotify.Properties.MaxLength = 400;
            this.MeeNotify.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeNotify.Properties.ShowIcon = false;
            this.MeeNotify.Size = new System.Drawing.Size(234, 20);
            this.MeeNotify.TabIndex = 26;
            this.MeeNotify.ToolTip = "F4 : Show/hide text";
            this.MeeNotify.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeNotify.ToolTipTitle = "Run System";
            // 
            // DteRouteDt
            // 
            this.DteRouteDt.EditValue = null;
            this.DteRouteDt.EnterMoveNextControl = true;
            this.DteRouteDt.Location = new System.Drawing.Point(93, 22);
            this.DteRouteDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteRouteDt.Name = "DteRouteDt";
            this.DteRouteDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRouteDt.Properties.Appearance.Options.UseFont = true;
            this.DteRouteDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRouteDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteRouteDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteRouteDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteRouteDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRouteDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteRouteDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRouteDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteRouteDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteRouteDt.Size = new System.Drawing.Size(138, 20);
            this.DteRouteDt.TabIndex = 32;
            this.DteRouteDt.Visible = false;
            this.DteRouteDt.Leave += new System.EventHandler(this.DteRouteDt_Leave);
            this.DteRouteDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteRouteDt_KeyDown);
            // 
            // LueType
            // 
            this.LueType.EnterMoveNextControl = true;
            this.LueType.Location = new System.Drawing.Point(238, 22);
            this.LueType.Margin = new System.Windows.Forms.Padding(5);
            this.LueType.Name = "LueType";
            this.LueType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.Appearance.Options.UseFont = true;
            this.LueType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueType.Properties.DropDownRows = 30;
            this.LueType.Properties.NullText = "[Empty]";
            this.LueType.Properties.PopupWidth = 350;
            this.LueType.Size = new System.Drawing.Size(182, 20);
            this.LueType.TabIndex = 31;
            this.LueType.ToolTip = "F4 : Show/hide list";
            this.LueType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueType.EditValueChanged += new System.EventHandler(this.LueType_EditValueChanged);
            this.LueType.Leave += new System.EventHandler(this.LueType_Leave);
            this.LueType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueType_KeyDown);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(772, 174);
            this.Grd1.TabIndex = 63;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            // 
            // OD1
            // 
            this.OD1.FileName = "NamaFile";
            // 
            // FrmSP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmSP";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueHSCode.Properties)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNameReceipt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNameDelivery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDelivery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReceipt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDischarge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLoading.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePEBDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePEBDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVLegalDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVLegalDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVLegal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKpbc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSPName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeConsignee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStfDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStfDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPEB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueNotify.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNotify.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRouteDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRouteDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label LblLocalDoc;
        public DevExpress.XtraEditors.SimpleButton BtnSource;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.MemoExEdit MeeConsignee;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.DateEdit DteStfDt;
        private System.Windows.Forms.Label label16;
        public DevExpress.XtraEditors.SimpleButton BtnCtNotify;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtBL;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueStatus;
        internal DevExpress.XtraEditors.TextEdit TxtSource;
        private DevExpress.XtraEditors.LookUpEdit LueNotify;
        private DevExpress.XtraEditors.MemoExEdit MeeNotify;
        protected System.Windows.Forms.Panel panel19;
        internal DevExpress.XtraEditors.DateEdit DtePEBDt;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.DateEdit DteVLegalDt;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtVLegal;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtSPName;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtSize;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtKpbc;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtPEB;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtMark;
        private System.Windows.Forms.Label label15;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private DevExpress.XtraEditors.LookUpEdit LueType;
        internal DevExpress.XtraEditors.DateEdit DteRouteDt;
        private DevExpress.XtraEditors.LookUpEdit LueHSCode;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtNameReceipt;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtNameDelivery;
        public DevExpress.XtraEditors.SimpleButton BtnReceipt;
        public DevExpress.XtraEditors.MemoExEdit MeeDelivery;
        public DevExpress.XtraEditors.MemoExEdit MeeReceipt;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueDischarge;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueLoading;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.SimpleButton BtnPlaceDelivery;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.SaveFileDialog SFD1;
        private System.Windows.Forms.OpenFileDialog OD1;


    }
}