﻿#region Update
/* 
    07/08/2019 [DITA] Tambah Approval Budget
    27/01/2020 [HAR/TWC] : ketika buat budget tidak mengupdate kolom budget di budget request
    04/03/2020 [HAR/IMS] : bisa di input berkali kali
    09/03/2020 [HAR/VIR] : BUG informasi total amount di header tidak muncul
    05/04/2020 [IBL/VIR] : Tambah informasi Budget Category Code internal
    05/05/2020 [WED/IMS] usage juga hitung dari VR Budget
    11/05/2020 [WED/SIER] budget tahunan berdasarkan parameter IsBudget2YearlyFormat
    27/05/2020 [WED/IMS] usage juga hitung dari Travel Request
    03/07/2020 [VIN/IMS] menghubungkan budget dengan cash advance settlement
    22/07/2020 [HAR/SRN] nilai used ambil dari MR Dtl qty * price / qty * estimated price  (showbudget & recompute)
    15/08/2020 [HAR/YK] bug : show data ketika amount used 0 tidk terbaca
    01/09/2020 [DITA/IMS] parameter IsBudgetShowPreviousAmt untuk menampilkan kolom previous amount
    07/10/2020 [IBL/PHT] menambah field Entity (header), kolom item dan quantity (detail) Based on param -> IsBudgetRequestUseItemAndQty
    20/01/2021 [WED/SIER] tambah ambil dari Cash Advance Settlement berdasarkan parameter IsCASUsedForBudget
    05/02/2021 [WED/SIER] tambah bisa cancel dokumen berdasarkan parameter IsBudget2Cancellable
    24/02/2021 [IBL/SIER] budget amount ambil dari budget request amt berdasarkan parameter IsBudgetAmtAutoFill
    30/04/2021 [WED/PHT] budget tidak menyimpan ke Budget Summary berdasarkan parameter IsBudgetMaintenanceYearlyActived
    22/06/2021 [WED/IMS] Budget Maintenance bisa di cancel berdasarkan parameter IsBudget2Cancellable dan Budget2CancelFormat (default : 1 untuk SIER, 2 : untuk yg otomatis bikin dokumen maintenance baru)
    10/11/2021 [ARI/AMKA] menambah parameter IsBudgetMaintenanceDeptFilteredByGroup untuk filter data di dialog da find
    18/04/2022 [TRI/TWC] bug, kolom usage belum mengambil dari MR Routine
    25/08/2022 [IBL/SIER] Nilai MR yg terpakai akan ditambahkan ke kolom Usage ketika process ind = 'F'. Berdasarkan parameter IsBudgetUseMRProcessInd.
    29/08/2022 [IBL/SIER] Bug : Warning unkown column A.ProcessInd saat memilih budget request.
    14/09/2022 [DITA/SIER] saat ShowBudgetDtl tambah show data VR for budget di kolom usage
    20/09/2022 [ICA/PRODUCT] menambahkan param IsBudgetUseMRProcessInd di method ShowBudgetRequestInfo
    11/10/2022 [VIN/SIER] BUG Used budget CAS berdasarkan bc code 
    22/11/2022 [VIN/SIER] BUG : tambah param IsCASUseBudgetCategory
    23/11/2022 [VIN/SIER] BUG : recompute Usage
    22/02/2023 [BRI/HEX] Usage melihat nilai PO berdasarkan param MRAvailableBudgetSubtraction
    22/02/2023 [BRI/HEX] range budget berdasarkan param FiscalYearRange
    27/02/2023 [IBL/HEX] Bug : recompute usage (column deptcode in where clause is ambiguous)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmBudget2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty, 
            mDeptCode = string.Empty,
            mReqTypeForNonBudget = string.Empty
            ;
        internal FrmBudget2Find FrmFind;
        private string            
            mBudget2CancelFormat = string.Empty,
            mMRAvailableBudgetSubtraction = string.Empty,
            mFiscalYearRange = string.Empty
            ;
        internal bool
            mIsBudget2YearlyFormat = false,
            mIsBudget2Cancellable = false,
            mIsBudgetAmtAutoFill = false,
            mIsBudgetCalculateFromEstimatedPrice = false,
            mIsMRShowEstimatedPrice = false,
            mIsBudgetShowPreviousAmt = false,
            mIsCASUsedForBudget = false,
            mIsBudgetMaintenanceYearlyActived = false,
            mIsBudgetMaintenanceDeptFilteredByGroup = false,
            mIsCASUseBudgetCategory = false
            ;
        internal bool
            mIsBudgetRequestUseItemAndQty = false,
            mIsBudgetUseMRProcessInd = false;
        string[] mFiscalYear;

        #endregion

        #region Constructor

        public FrmBudget2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Budget";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (!mIsBudget2Cancellable)
                {
                    LblCancelReason.Visible = MeeCancelReason.Visible = ChkCancelInd.Visible = false;
                }
                SetGrd();
                LueEntCode.Visible = mIsBudgetRequestUseItemAndQty;
                label9.Visible = mIsBudgetRequestUseItemAndQty;
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "Budget Request DNo",

                    //1-5
                    "Budget"+Environment.NewLine+"Category",
                    "Budget"+Environment.NewLine+"Category",
                    "Requested"+Environment.NewLine+"Amount",
                    "Previous" + Environment.NewLine + "Budget"+Environment.NewLine+"Amount",
                    "Budget"+Environment.NewLine+"Amount",

                    //6-10
                    "Usage",
                    "Remark",
                    "Local"+Environment.NewLine+"Code",
                    "Item's Code",
                    "Item's Name",

                    //11-12
                    "",
                    "Quantity",
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    0, 200, 130, 130, 130,

                    //6-10
                    130, 300, 100, 120, 150,

                    //11-12
                    20, 100
                }
            );
            Grd1.Cols[8].Move(0);
            Grd1.Cols[7].Move(12);

            Sm.GrdColButton(Grd1, new int[] { 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 4, 6, 9 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 8, 9, 10, 12 });

            if(!mIsBudgetRequestUseItemAndQty)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12}, false);
        }

        override protected void HideInfoInGrd()
        {
            if (BtnSave.Enabled) Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteDocDt, MeeRemark, LueEntCode, MeeCancelReason, ChkCancelInd }, true);
                    BtnBudgetRequestDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 5, 7, 11 });
                    Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteDocDt, MeeRemark }, false);
                    BtnBudgetRequestDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 7, 11 });
                    Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, !ChkHideInfoInGrd.Checked);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    if (mIsBudget2Cancellable)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd, MeeCancelReason}, false);
                        MeeCancelReason.Focus();
                    }
                    break;
            }
        }

        private void ClearData()
        {
            mDeptCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, 
                DteDocDt, TxtBudgetRequestDocNo, TxtYr, TxtMth, TxtDeptCode,TxtStatus, 
                MeeRemark, LueEntCode, MeeCancelReason
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtBudgetRequestAmt, TxtAmt }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBudget2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (mIsBudget2Cancellable)
                {
                    if (TxtDocNo.Text.Length == 0) InsertData(sender, e);
                    else
                    {
                        if (mBudget2CancelFormat == "1") EditData();
                        else ProcessEdit();
                    }
                }
                else
                    InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (mIsBudget2Cancellable)
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
                SetFormControl(mState.Edit);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!(BtnSave.Enabled && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0 && Sm.IsGrdColSelected(new int[] { 5, 7 }, e.ColIndex)))
                e.DoDefault = false;
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 5 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 7 }, e);
            if (e.ColIndex == 5) ComputeTotalAmount();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Budget", "TblBudgetHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBudgetHdr(DocNo));
            for (int r = 0;r< Grd1.Rows.Count - 1;r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveBudgetDtl(DocNo, r));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtBudgetRequestDocNo, "Budget request#", false) ||
                IsBudgetRequestAlreadyCancelled() ||
                IsBudgetRequestAlreadyProcessed() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 budget category.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            decimal BudgetRequestAmt = 0m, BudgetAmt = 0m, UsageAmt = 0m;
            
            RecomputeUsage();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Budget category is empty.")) return true;
                Msg = "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine;

                BudgetRequestAmt = Sm.GetGrdDec(Grd1, Row, 3);
                BudgetAmt = Sm.GetGrdDec(Grd1, Row, 5);
                UsageAmt = Sm.GetGrdDec(Grd1, Row, 6);

                //if (BudgetAmt > BudgetRequestAmt)
                //{
                //    Sm.StdMsg(mMsgType.Warning, 
                //        Msg +
                //        "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                //        "Budget amount : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                //        "Requested amount : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine + Environment.NewLine +
                //        "Budget amount is bigger than requested amount."
                //        );
                //}

                if (BudgetAmt < UsageAmt)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Budget amount : " + Sm.FormatNum(BudgetAmt, 0) + Environment.NewLine +
                        "Used amount : " + Sm.FormatNum(UsageAmt, 0) + Environment.NewLine + Environment.NewLine +
                        "Budget amount is smaller than used amount."
                        );
                    return true;
                }

            }
            return false;
        }

        private bool IsBudgetRequestAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblBudgetRequestHdr Where DocNo=@Param And CancelInd='Y';", 
                TxtBudgetRequestDocNo.Text, 
                "Budget Request# : " + TxtBudgetRequestDocNo.Text + Environment.NewLine + 
                "This budget request already cancelled."
                );
        }

        private bool IsBudgetRequestAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblBudgetRequestHdr Where DocNo=@Param And BudgetDocNo is Not Null;",
                TxtBudgetRequestDocNo.Text,
                "Budget Request# : " + TxtBudgetRequestDocNo.Text + Environment.NewLine +
                "This budget request already processed."
                );
        }

        private MySqlCommand SaveBudgetHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetHdr(DocNo, DocDt, Status, BudgetRequestDocNo, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @BudgetRequestDocNo, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='Budget2' ");
                SQL.AppendLine("And T.DeptCode Is Not Null And T.DeptCode = (Select DeptCode From TblBudgetRequestHdr Where DocNo = @BudgetRequestDocNo) ");
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.Amt*1 ");
                SQL.AppendLine("    From TblBudgetHdr A ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)); ");
            }

            SQL.AppendLine("Update TblBudgetHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'Budget2' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblBudgetRequestHdr Set BudgetDocNo = @DocNo ");
            SQL.AppendLine("Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'Budget2' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBudgetDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetDtl(DocNo, DNo, BudgetRequestDocNo, BudgetRequestDNo, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @BudgetRequestDocNo, @BudgetRequestDNo, @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            if (!mIsBudgetMaintenanceYearlyActived)
            {
                SQL.AppendLine("Insert Into TblBudgetSummary(Yr, Mth, DeptCode, BCCode, Amt1, Amt2, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@Yr, @Mth, @DeptCode, @BCCode, @Amt1, @Amt, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("    Update Amt1=@Amt1, Amt2=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            }

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BudgetRequestDNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
            Sm.CmParam<String>(ref cm, "@Mth", (mIsBudget2YearlyFormat) ? "00" : TxtMth.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd1, Row, 3));
            
            return cm;
        }

        #endregion

        #region Edit Data

        #region Default

        private void EditData()
        {
            if (!mIsBudget2Cancellable) return;
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditBudgetHdr(TxtDocNo.Text));
            cml.Add(UpdateBudgetRequest(TxtBudgetRequestDocNo.Text));
            cml.Add(DeleteBudgetSummary(TxtBudgetRequestDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsDataNotCancelled() ||
                IsDataAlreadyCancelled()
                ;
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblBudgetHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C') ");
            SQL.AppendLine("; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditBudgetHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBudgetHdr Set ");
            SQL.AppendLine("    CancelInd = 'Y', CancelReason = @CancelReason, ");
            SQL.AppendLine("    LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateBudgetRequest(string BudgetRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBudgetRequestHdr Set ");
            SQL.AppendLine("    BudgetDocNo = Null, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("And BudgetDocNo Is Not Null ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", BudgetRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteBudgetSummary(string BudgetRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblBudgetSummary Where Concat(Yr, ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("Mth, ");
            SQL.AppendLine("DeptCode, BCCode) In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Concat(T1.Yr, ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("    T1.Mth, ");
            SQL.AppendLine("    T1.DeptCode, T2.BCCode) ");
            SQL.AppendLine("    From TblBudgetRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl T2 On T1.DocNo = T2.DOcNo ");
            SQL.AppendLine("        And T1.DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", BudgetRequestDocNo);

            return cm;
        }

        #endregion

        #region Process Edit

        private void ProcessEdit()
        {
            bool IsUsageExists = GetUsageBudget();
            if (ChkCancelInd.Checked)
            {
                if (IsUsageExists)
                {
                    Sm.FormShowDialog(new FrmBudget2Dlg2(this));
                    ShowData(TxtDocNo.Text);
                }
                else
                    EditData();
            }
        }

        private bool GetUsageBudget()
        {
            RecomputeUsage();
            bool IsExists = false;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdDec(Grd1, i, 6) != 0m)
                {
                    IsExists = true;
                    break;
                }
            }

            return IsExists;
        }

        #endregion

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBudgetHdr(DocNo);
                ShowBudgetDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBudgetHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.BudgetRequestDocNo, B.Yr, B.Mth, ");
            SQL.AppendLine("B.DeptCode, C.DeptName, B.Amt As BudgetRequestAmt, A.Amt, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As Status, B.EntCode, ");
            if (mIsBudget2Cancellable)
                SQL.AppendLine("A.CancelReason, A.CancelInd ");
            else
                SQL.AppendLine("Null As CancelReason, 'N' As CancelInd ");
            SQL.AppendLine("From TblBudgetHdr A ");
            SQL.AppendLine("Inner Join TblBudgetRequestHdr B On A.BudgetRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");


            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "BudgetRequestDocNo", "Yr", "Mth", "DeptCode", 
                        //6-10
                        "DeptName", "BudgetRequestAmt", "Amt", "Remark","Status",

                        //11-13
                        "EntCode", "CancelReason", "CancelInd"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtBudgetRequestDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        TxtYr.EditValue = Sm.DrStr(dr, c[3]);
                        TxtMth.EditValue = Sm.DrStr(dr, c[4]);
                        mDeptCode = Sm.DrStr(dr, c[5]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtBudgetRequestAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[10]);
                        Sl.SetLueEntCode(ref LueEntCode, Sm.DrStr(dr, c[11]));
                        if (mIsBudget2Cancellable)
                        {
                            MeeCancelReason.EditValue = Sm.DrStr(dr, c[12]);
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[13]) == "Y";
                        }
                    }, true
                );
        }

        private void ShowBudgetDtl(string DocNo)
        {
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BudgetRequestDNo, B.BCCode, C.LocalCode, C.BCName, B.Amt As BudgetRequestAmt, A.Amt, A.Remark, IfNull(E.Amt, 0) Amt2, IfNull(F.Amt, 0.00) PrevAmt,  ");
            SQL.AppendLine("G.ItCode, G.ItName, B.Qty ");
            SQL.AppendLine("From TblBudgetDtl A ");
            SQL.AppendLine("Inner Join TblBudgetRequestDtl B On A.BudgetRequestDocNo=B.DocNo And A.BudgetRequestDNo=B.DNo ");
            SQL.AppendLine("Inner Join TblBudgetCategory C On B.BCCode=C.BCCode ");

            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select T.Dno, T.BcCode, SUM(T.Amt) As Amt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("    Select F.DNo, D.BCCode, Case when A.AcType = 'D' Then IFNULL(A.Amt*-1, 0.00) ELSE IFNULL(A.Amt, 0.00) END Amt ");
            SQL.AppendLine("    From tblvoucherhdr A ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X1.DocNo, X1.VoucherRequestDocNo ");
            SQL.AppendLine("        From tblvoucherhdr X1 ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
            SQL.AppendLine("        AND X1.DocType = '58' ");
            SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            SQL.AppendLine("        AND X2.Status In ('O', 'A') ");
            SQL.AppendLine("     ) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("        From tblvoucherhdr X1 ");
            SQL.AppendLine("        Inner Join tblcashadvancesettlementdtl X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("        AND X1.DocType = '56' ");
            SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            SQL.AppendLine("       AND X3.Status In ('O', 'A') ");
            SQL.AppendLine("    ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr E On E.DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        And D.DeptCode = @DeptCode ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
            else
                SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl F On E.DocNo = F.DocNo ");
            SQL.AppendLine("        And F.BCCode = D.BCCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select E.DNo, C.BCCode, Sum( ");
            if (mMRAvailableBudgetSubtraction == "1")
            {
                if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                    SQL.AppendLine("    B.Qty * Case When A.EximInd = 'Y' Then B.UPrice Else B.EstPrice End ");
                else
                    SQL.AppendLine("    B.Qty * B.UPrice ");
            }
            if (mMRAvailableBudgetSubtraction == "2")
                SQL.AppendLine("        ((((100.00 - G.Discount) * 0.01) * (G.Qty * J.UPrice)) - G.DiscountAmt + G.RoundingValue) ");
            SQL.AppendLine("    ) Amt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And B.`Status` In ('A', 'O') And B.CancelInd = 'N' ");
            if (mIsBudget2YearlyFormat)
            {
                if (mFiscalYearRange.Length == 0)
                    SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 6) Between Concat(@Yr,'" + mFiscalYear[0] + "') And Concat(@Yr+1,'" + mFiscalYear[1] + "') ");
            }
            else
                SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(@Yr, @Mth) ");
            if (mIsBudgetUseMRProcessInd)
                SQL.AppendLine("        And A.ProcessInd = 'F' ");
            SQL.AppendLine("    Inner Join TblBudgetCategory C On A.BCCode = C.BCCode ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr D On D.DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl E On D.DocNo = E.DocNo And C.BCCode = E.BCCode ");
            if (mMRAvailableBudgetSubtraction == "2")
            {
                SQL.AppendLine("        Inner Join TblPORequestDtl F On B.DocNo = F.MaterialRequestDocNo And B.DNo = F.MaterialRequestDNo And F.CancelInd = 'N' And F.Status = 'A' ");
                SQL.AppendLine("        Inner Join TblPODtl G On F.DocNo = G.PORequestDocNo And F.DNo = G.PORequestDNo And G.CancelInd = 'N' ");
                SQL.AppendLine("        Inner Join TblPOHdr H On G.DocNo = H.DocNo And H.Status <> 'C' ");
                SQL.AppendLine("        Inner Join TblQtHdr I On F.QtDocNo = I.DocNo ");
                SQL.AppendLine("        Inner Join TblQtDtl J On F.QtDocNo = J.DocNo And F.QtDNo = J.DNo ");
            }
            SQL.AppendLine("    Group By E.DNo, C.BCCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select D.DNo, A.BCCode, Sum(A.Amt) Amt ");
            SQL.AppendLine("        From TblVoucherRequestHdr A ");
            SQL.AppendLine("        Inner Join TblBudgetCategory B On A.BCCode = B.BCCode ");
            SQL.AppendLine("        Inner Join TblBudgetRequestHdr C On C.DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Inner Join TblBudgetRequestDtl D On C.DocNo = D.DocNo And B.BCCode = D.BCCode ");
            SQL.AppendLine("        Where A.ReqType Is Not null ");
            SQL.AppendLine("        And A.CancelInd = 'N' ");
            SQL.AppendLine("        And A.Status In ('O', 'A') ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(A.DocDt, 4) = Concat(@Yr) ");
            else
                SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("        And Find_In_Set(A.DocType, ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
            SQL.AppendLine("    Group By D.DNo, A.BCCode ");

            if (mIsCASUsedForBudget)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select E.DNo, C.BCCode, Sum(B.Amt) Amt ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        And A.`Status` = 'A' And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.DocStatus = 'F' ");
                SQL.AppendLine("        And B.CCtCode Is Not Null ");
                if (mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(@Yr, @Mth) ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode And B.BCCode=C.BCCode ");
                SQL.AppendLine("        And C.CCtCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetRequestHdr D On D.DocNo = @BudgetRequestDocNo And D.DeptCode = C.DeptCode ");
                SQL.AppendLine("    Inner Join TblBudgetRequestDtl E On D.DocNo = E.DocNo And C.BCCode = E.BCCode ");
                SQL.AppendLine("    Group By E.DNo, C.BCCode ");
            }

            SQL.AppendLine("        )T ");
            SQL.AppendLine("Group By T.DNo, T.BCCode ");
            SQL.AppendLine(") E On  B.BCCode = E.BCCode ");

            #region Old Code
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select X2.DocNo, X2.DNo, Sum(X1.Amt) Amt ");
            //SQL.AppendLine("    From TblBudgetDtl X1 ");
            //SQL.AppendLine("    Inner Join TblBudgetRequestDtl X2 On X1.BudgetRequestDocNo = X2.DocNo And X1.BudgetRequestDNo = X2.DNo ");
            //SQL.AppendLine("        And X1.DocNo != @DocNo ");
            //SQL.AppendLine("        And X1.CreateDt <= (Select CreateDt From TblBudgetHdr Where DocNo = @DocNo) ");
            //SQL.AppendLine("    Inner Join TblBudgetHdr X3 On X1.DocNo = X3.DocNo And X3.Status In ('O', 'A') ");
            //SQL.AppendLine("    Group By X2.DocNo, X2.DNo ");
            //SQL.AppendLine(") F On A.BudgetRequestDocNo=F.DocNo And A.BudgetRequestDNo=F.DNo ");
            #endregion

            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T3.BudgetRequestDocNo, T3.BudgetRequestDNo, T3.Amt ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Max(Concat(X3.DocDt, Left(X3.DocNo, 4))) DocNo ");
            SQL.AppendLine("        From TblBudgetDtl X1 ");
            SQL.AppendLine("        Inner Join TblBudgetRequestDtl X2 On X1.BudgetRequestDocNo = X2.DocNo And X1.BudgetRequestDNo = X2.DNo ");
            SQL.AppendLine("            And X1.DocNo != @DocNo ");
            SQL.AppendLine("            And X1.CreateDt <= (Select CreateDt From TblBudgetHdr Where DocNo = @DocNo) ");
            SQL.AppendLine("        Inner Join TblBudgetHdr X3 On X1.DocNo = X3.DocNo And X3.Status In ('O', 'A') ");
            if (mIsBudget2Cancellable)
                SQL.AppendLine("            And X3.CancelInd = 'N' ");
            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    Inner Join TblBudgetHdr T2 On T1.DocNo = Concat(T2.DocDt, Left(T2.DocNo, 4)) ");
            if (mIsBudget2Cancellable)
                SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblBudgetDtl T3 On T2.DocNo = T3.DocNo ");
            SQL.AppendLine(") F On A.BudgetRequestDocNo=F.BudgetRequestDocNo And A.BudgetRequestDNo=F.BudgetRequestDNo ");
            SQL.AppendLine("Left Join TblItem G On B.ItCode = G.ItCode ");
            if (mIsBudgetShowPreviousAmt)
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");
            else
            {
                SQL.AppendLine("Where B.DocNo=@BudgetRequestDocNo ");
                if (mIsBudget2Cancellable)
                    SQL.AppendLine("And A.DocNo In (Select DocNo From TblBudgetHdr Where CancelInd = 'N') ");
                SQL.AppendLine("Order By A.DNo; ");
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
            Sm.CmParam<String>(ref cm, "@Mth", TxtMth.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "BudgetRequestDNo", 
                    "BCCode", "BCName", "BudgetRequestAmt", "Amt", "Remark",
                    "LocalCode", "Amt2", "PrevAmt", "ItCode", "ItName",
                    "Qty",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    if (mIsBudgetShowPreviousAmt) Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 8);
                    else Grd.Cells[Row, 4].Value = 0m; 
                    
                    //Grd.Cells[Row, 6].Value = 0m;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 12 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            mIsBudgetShowPreviousAmt = Sm.GetParameterBoo("IsBudgetShowPreviousAmt");
            mIsBudgetRequestUseItemAndQty = Sm.GetParameterBoo("IsBudgetRequestUseItemAndQty");
            mIsCASUsedForBudget = Sm.GetParameterBoo("IsCASUsedForBudget");
            mIsBudget2Cancellable = Sm.GetParameterBoo("IsBudget2Cancellable");
            mIsBudgetAmtAutoFill = Sm.GetParameterBoo("IsBudgetAmtAutoFill");
            mIsBudgetMaintenanceYearlyActived = Sm.GetParameterBoo("IsBudgetMaintenanceYearlyActived");
            mBudget2CancelFormat = Sm.GetParameter("Budget2CancelFormat");
            mIsBudgetMaintenanceDeptFilteredByGroup = Sm.GetParameterBoo("IsBudgetMaintenanceDeptFilteredByGroup");
            mIsBudgetUseMRProcessInd = Sm.GetParameterBoo("IsBudgetUseMRProcessInd");
            mIsCASUseBudgetCategory = Sm.GetParameterBoo("IsCASUseBudgetCategory");
            if (mBudget2CancelFormat.Length == 0) mBudget2CancelFormat = "1";
            mMRAvailableBudgetSubtraction = Sm.GetParameter("MRAvailableBudgetSubtraction");
            if (mMRAvailableBudgetSubtraction.Length == 0) mMRAvailableBudgetSubtraction = "1";
            mFiscalYearRange = Sm.GetParameter("FiscalYearRange");
            mFiscalYear = mFiscalYearRange.Split(',');
        }

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where Doctype = 'Budget2' And DeptCode Is Not Null Limit 1; ");
        }
        
        private void ComputeTotalAmount()
        {
            decimal Amt = 0m;
            decimal Amt2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 3).Length != 0)
                    Amt += Sm.GetGrdDec(Grd1, r, 3);
            TxtBudgetRequestAmt.Text = Sm.FormatNum(Amt, 0);

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 5).Length != 0)
                    Amt2 += Sm.GetGrdDec(Grd1, r, 5);
            TxtAmt.Text = Sm.FormatNum(Amt2, 0);
        }

        internal void ShowBudgetAmt()
        {
            if (BtnSave.Enabled && Grd1.Rows.Count > 1)
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    Sm.CopyGrdValue(Grd1, row, 5, Grd1, row, 3);
                }
                ComputeTotalAmount();
            }
        }

        internal void ShowBudgetRequestInfo()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.BCCode, B.BCName, A.Amt, B.LocalCode, IfNull(C.Amt2, 0) As Amt2, (IfNull(D.Amt, 0)+ IfNull(E.Amt, 0) ");
            if (mIsCASUsedForBudget)
                SQL.AppendLine("+ IfNull(G.Amt, 0.00) ");
            SQL.AppendLine(") As Used, ");
            SQL.AppendLine("IfNull(A.Qty, 0) Qty, F.ItCode, F.ItName ");
            SQL.AppendLine("From TblBudgetRequestDtl A ");
            SQL.AppendLine("Inner Join TblBudgetCategory B On A.BCCode=B.BCCode ");
            SQL.AppendLine("Left Join TblBudgetSummary C On A.BCCode=C.BCCode And C.DeptCode=@DeptCode And C.Yr=@Yr ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("    And C.Mth=@Mth ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.BCCode, Sum(T.Amt) Amt ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select BCCode, Amt ");
            //SQL.AppendLine("        From TblMaterialRequestHdr ");
            //SQL.AppendLine("        Where Left(DocDt, 4)=@Yr ");
            //if (!mIsBudget2YearlyFormat)
            //    SQL.AppendLine("        And Substring(DocDt, 5, 2)=@Mth ");
            //SQL.AppendLine("        And DeptCode=@DeptCode ");
            SQL.AppendLine("    Select A.BCCode,   ");
            if (mMRAvailableBudgetSubtraction == "1")
            {
                if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                    SQL.AppendLine("    (B.Qty * B.EstPrice) As Amt ");
                else
                    SQL.AppendLine("    (B.Qty * B.UPrice) As Amt ");
            }
            if (mMRAvailableBudgetSubtraction == "2")
                SQL.AppendLine("        ((((100.00 - D.Discount) * 0.01) * (D.Qty * G.UPrice)) - D.DiscountAmt + D.RoundingValue) As Amt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo  ");
            if (mMRAvailableBudgetSubtraction == "2")
            {
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo = C.MaterialRequestDocNo And B.DNo = C.MaterialRequestDNo And C.CancelInd = 'N' And C.Status = 'A' ");
                SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo = D.PORequestDocNo And C.DNo = D.PORequestDNo And D.CancelInd = 'N' ");
                SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo = E.DocNo And E.Status <> 'C' ");
                SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo = F.DocNo ");
                SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo = G.DocNo And C.QtDNo = G.DNo ");
            }
            SQL.AppendLine("    Where B.cancelind = 'N' ");
            if (mFiscalYearRange.Length == 0)
                SQL.AppendLine("    And  Left(A.DocDt, 4)=@Yr  ");
            else
                SQL.AppendLine("    And Left(A.DocDt, 6) Between Concat(@Yr,'" + mFiscalYear[0] + "') And Concat(@Yr+1,'" + mFiscalYear[1] + "') ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2)=@Mth  ");
            if (mIsBudgetUseMRProcessInd)
                SQL.AppendLine("        And A.ProcessInd = 'F' ");
            SQL.AppendLine("        And A.DeptCode=@DeptCode ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select BCCode, Amt ");
            SQL.AppendLine("        From TblVoucherRequestHdr ");
            SQL.AppendLine("        Where ReqType Is Not null ");
            SQL.AppendLine("        And ReqType <> @ReqTypeForNonBudget ");
            SQL.AppendLine("        And CancelInd = 'N' ");
            SQL.AppendLine("        And Status In ('O', 'A') ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(DocDt, 4) = Concat(@Yr) ");
            else
                SQL.AppendLine("        And Left(DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("        And Find_In_Set(DocType, ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
            
            SQL.AppendLine("        And DeptCode = @DeptCode ");
            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select T1.BCCode, (T1.Amt1 + T1.Amt2 + T1.Amt3 + T1.Amt4 + T1.Amt5 + T1.Amt6 + T1.Detasering) Amt ");
            SQL.AppendLine("        From TblTravelRequestDtl7 T1 ");
            SQL.AppendLine("        Inner Join TblTravelRequestHdr T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T2.CancelInd = 'N' ");
            SQL.AppendLine("            And T2.Status In ('O', 'A') ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("            And Left(T2.DocDt, 4) = Concat(@Yr) ");
            else
                SQL.AppendLine("            And Left(T2.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("            And T1.BCCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblBudgetCategory T3 On T1.BCCode = T3.BCCode ");
            SQL.AppendLine("            And T3.DeptCode = @DeptCode ");

            SQL.AppendLine("    ) T Group By T.BCCode ");
            SQL.AppendLine(") D On B.BCCode=D.BCCode ");


            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select F.DNo, D.BCCode, Case when A.AcType = 'D' Then IFNULL(SUM(A.Amt)*-1, 0.00) ELSE IFNULL(SUM(A.Amt), 0.00) END Amt ");
            SQL.AppendLine("    From tblvoucherhdr A ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X1.DocNo, X1.VoucherRequestDocNo ");
            SQL.AppendLine("        From tblvoucherhdr X1 ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
            SQL.AppendLine("        AND X1.DocType = '58' ");
            SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            SQL.AppendLine("        AND X2.Status In ('O', 'A') ");
            SQL.AppendLine("     ) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("        From tblvoucherhdr X1 ");
            SQL.AppendLine("        Inner Join tblcashadvancesettlementdtl X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("        AND X1.DocType = '56' ");
            SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            SQL.AppendLine("       AND X3.Status In ('O', 'A') ");
            SQL.AppendLine("    ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr E On E.DocNo = @DocNo ");
            SQL.AppendLine("        And D.DeptCode = E.DeptCode ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(A.DocDt, 4) = E.Yr ");
            else
                SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(E.Yr, E.Mth) ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl F On E.DocNo = F.DocNo ");
            SQL.AppendLine("        And F.BCCode = D.BCCode ");
            SQL.AppendLine("    Group By F.DNo, D.BCCode ");
            SQL.AppendLine(") E On A.DNo = E.DNo And A.BCCode = E.BCCode ");
            SQL.AppendLine("Left Join TblItem F On A.ItCode = F.ItCode ");
            if (mIsCASUsedForBudget)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select E.DNo, C.BCCode, Sum(B.Amt) Amt ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        And A.`Status` = 'A' And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.DocStatus = 'F' ");
                SQL.AppendLine("        And B.CCtCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                SQL.AppendLine("        And C.CCtCode Is Not Null ");
                if(mIsCASUseBudgetCategory)
                    SQL.AppendLine("        And C.BCCode = B.BCCode ");

                SQL.AppendLine("    Inner Join TblBudgetRequestHdr D On D.DocNo = @DocNo And D.DeptCode = C.DeptCode ");
                SQL.AppendLine("    Inner Join TblBudgetRequestDtl E On D.DocNo = E.DocNo And C.BCCode = E.BCCode ");
                SQL.AppendLine("    Group By E.DNo, C.BCCode ");
                SQL.AppendLine(") G On A.DNo = G.DNo And A.BCCode = G.BCCode ");
            }

            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
            Sm.CmParam<String>(ref cm, "@Mth", TxtMth.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
            
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    
                    //1-5
                    "BCCode", "BCName", "Amt", "Amt2", "Used",

                    //6-9
                    "LocalCode", "ItCode", "ItName", "Qty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Grd.Cells[Row, 7].Value = null;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 12 });
            Sm.FocusGrd(Grd1, 0, 2);
            ComputeTotalAmount();
        }

        private void RecomputeUsage()
        {
            var BCCode = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.BCCode, Sum(T.Amt) Amt ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            //SQL.AppendLine("    Select BCCode, Amt ");
            //SQL.AppendLine("    From TblMaterialRequestHdr ");
            //SQL.AppendLine("    Where Left(DocDt, 4)=@Yr ");
            //if (!mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    And Substring(DocDt, 5, 2)=@Mth ");
            //SQL.AppendLine("    And DeptCode=@DeptCode ");

            SQL.AppendLine("    Select BCCode,   ");
            if (mMRAvailableBudgetSubtraction == "1")
            {
                if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                    SQL.AppendLine("    (B.Qty * B.EstPrice) As Amt ");
                else
                    SQL.AppendLine("    (B.Qty*B.UPrice) As Amt ");
            }
            if (mMRAvailableBudgetSubtraction == "2")
                SQL.AppendLine("        ((((100.00 - D.Discount) * 0.01) * (D.Qty * G.UPrice)) - D.DiscountAmt + D.RoundingValue) As Amt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo  ");
            if (mMRAvailableBudgetSubtraction == "2")
            {
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo = C.MaterialRequestDocNo And B.DNo = C.MaterialRequestDNo And C.CancelInd = 'N' And C.Status = 'A' ");
                SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo = D.PORequestDocNo And C.DNo = D.PORequestDNo And D.CancelInd = 'N' ");
                SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo = E.DocNo And E.Status <> 'C' ");
                SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo = F.DocNo ");
                SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo = G.DocNo And C.QtDNo = G.DNo ");
            }
            SQL.AppendLine("    Where B.cancelind = 'N' And  Left(A.DocDt, 4)=@Yr  ");
            if (mFiscalYearRange.Length == 0)
                SQL.AppendLine("    And  Left(A.DocDt, 4)=@Yr  ");
            else
                SQL.AppendLine("    And Left(A.DocDt, 6) Between Concat(@Yr,'" + mFiscalYear[0] + "') And Concat(@Yr+1,'" + mFiscalYear[1] + "') ");
            if (!mIsBudget2YearlyFormat)
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2)=@Mth  ");
            SQL.AppendLine("    And A.DeptCode=@DeptCode ");
            if (mIsBudgetUseMRProcessInd) SQL.AppendLine("And A.ProcessInd = 'F' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select BCCode, Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr ");
            SQL.AppendLine("    Where ReqType Is Not null ");
            SQL.AppendLine("    And ReqType <> @ReqTypeForNonBudget ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And Status In ('O', 'A') ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("    And Left(DocDt, 4) = Concat(@Yr) ");
            else
                SQL.AppendLine("    And Left(DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("        And Find_In_Set(DocType, ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
            SQL.AppendLine("    And DeptCode = @DeptCode ");

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select T1.BCCode, (T1.Amt1 + T1.Amt2 + T1.Amt3 + T1.Amt4 + T1.Amt5 + T1.Amt6 + T1.Detasering) Amt ");
            SQL.AppendLine("    From TblTravelRequestDtl7 T1 ");
            SQL.AppendLine("    Inner Join TblTravelRequestHdr T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("        And T2.Status In ('O', 'A') ");
            if (mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(T2.DocDt, 4) = Concat(@Yr) ");
            else
                SQL.AppendLine("        And Left(T2.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("        And T1.BCCode Is Not Null ");
            SQL.AppendLine("    Inner Join TblBudgetCategory T3 On T1.BCCode = T3.BCCode ");
            SQL.AppendLine("        And T3.DeptCode = @DeptCode ");
            
            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    SELECT D.BCCode AS BCCode,  Case when A.AcType = 'D' Then IFNULL(SUM(A.Amt)*-1, 0.00) ELSE IFNULL(SUM(A.Amt), 0.00) END As Amt ");
            SQL.AppendLine("    From tblvoucherhdr A  ");
            SQL.AppendLine("    Inner Join  ");
            SQL.AppendLine("      ( ");
            SQL.AppendLine("       SELECT X1.DocNo, X1.VoucherRequestDocNo   ");
            SQL.AppendLine("       From tblvoucherhdr X1  ");
            SQL.AppendLine("	    INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo  ");
            SQL.AppendLine("	    AND X1.DocType = '58' ");
            SQL.AppendLine("	    AND X1.CancelInd = 'N'  ");
            SQL.AppendLine("	    AND X2.Status In ('O', 'A')  ");
            SQL.AppendLine("      ) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("      ( ");
            SQL.AppendLine("       SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("       From tblvoucherhdr X1  ");
            SQL.AppendLine("       Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("       INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("       AND X1.DocType = '56' ");
            SQL.AppendLine("       AND X1.CancelInd = 'N'  ");
            SQL.AppendLine("       AND X3.Status In ('O', 'A')  ");
            SQL.AppendLine("      ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    And D.DeptCode = @DeptCode  ");
            if (!mIsBudget2YearlyFormat)
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr  ");
                SQL.AppendLine("    And SUBSTRING(A.DocDt, 5, 2) = @Mth ");
            }
            else
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr  ");

            if (mIsCASUsedForBudget)
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select C.BCCode, B.Amt ");
                SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            And A.`Status` = 'A' And A.CancelInd = 'N' ");
                SQL.AppendLine("            And A.DocStatus = 'F' ");
                SQL.AppendLine("            And B.CCtCode Is Not Null ");
                SQL.AppendLine("            And Left(A.DocDt, 4) = @Yr ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("            And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                if(mIsCASUseBudgetCategory) SQL.AppendLine(" And B.BCCode=C.BCCode ");
                SQL.AppendLine("            And C.CCtCode Is Not Null ");
                SQL.AppendLine("            And C.DeptCode = @DeptCode ");
            }

            SQL.AppendLine(") T Group By T.BCCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
                Sm.CmParam<String>(ref cm, "@Mth", TxtMth.Text);
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "BCCode", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        BCCode = Sm.DrStr(dr, 0);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), BCCode))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 6, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsBudget2Cancellable) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsBudget2Cancellable) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Button Event

        private void BtnBudgetRequestDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBudget2Dlg(this));
        }

        private void BtnBudgetRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBudgetRequestDocNo, "Budget request#", false))
            {
                var f1 = new FrmBudgetRequest2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtBudgetRequestDocNo.Text;
                f1.ShowDialog();
            }
        }
       
        #endregion

        #region Grid Event Control

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #endregion

        #region Class

        private class NewBudget2Hdr
        {
        }

        private class NewBudget2Dtl
        {

        }

        #endregion
    }
}
