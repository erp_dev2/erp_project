﻿#region Update
/*
    17/11/2020 [WED/PHT] Severance type belum ikut ke save
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSeverance : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmSeveranceFind FrmFind;

        #endregion

        #region Constructor

        public FrmSeverance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetLueSeveranceType(ref LueSeveranceType);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSeveranceCode, TxtSeveranceName, LueSeveranceType, TxtYearsWorked, TxtAmt
                    }, true);
                    TxtSeveranceCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSeveranceCode, TxtSeveranceName, LueSeveranceType, TxtYearsWorked, TxtAmt
                    }, false);
                    TxtSeveranceCode.Focus();
                    break;
                case mState.Edit:
                   
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSeveranceCode, TxtSeveranceName
            });

            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>
            {
                TxtYearsWorked, TxtAmt
            }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSeveranceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            //if (Sm.IsTxtEmpty(TxtCntCode, "", false)) return;
            //SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblSeverance(SeveranceCode, SeveranceName, SeveranceType, YearsWorked, Amt, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@SeveranceCode, @SeveranceName, @SeveranceType, @YearsWorked, @Amt, @UserCode, CurrentDateTime()); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@SeveranceCode", TxtSeveranceCode.Text);
                Sm.CmParam<String>(ref cm, "@SeveranceName", TxtSeveranceName.Text);
                Sm.CmParam<String>(ref cm, "@SeveranceType", Sm.GetLue(LueSeveranceType));
                Sm.CmParam<Decimal>(ref cm, "@YearsWorked", Decimal.Parse(TxtYearsWorked.Text));
                Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtSeveranceCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string SeveranceCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@SeveranceCode", SeveranceCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblSeverance Where SeveranceCode=@SeveranceCode",
                        new string[] 
                        {
                            "SeveranceCode", 
                            //1-3
                            "SeveranceName", "SeveranceType", "YearsWorked", "Amt"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtSeveranceCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtSeveranceName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueSeveranceType, Sm.DrStr(dr, c[2]));
                            TxtYearsWorked.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[3]), 0);
                            TxtAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[4]), 0);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSeveranceCode, "Severance code", false) ||
                Sm.IsTxtEmpty(TxtSeveranceName, "Severance name", false) ||
                Sm.IsLueEmpty(LueSeveranceType, "Severance type") ||
                Sm.IsTxtEmpty(TxtYearsWorked, "Number of years worked", true)||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                IsSeveranceCodeExisted();
        }

        private bool IsSeveranceCodeExisted()
        {
            if (!TxtSeveranceCode.Properties.ReadOnly && Sm.IsDataExist("Select SeveranceCode From TblSeverance Where SeveranceCode='" + TxtSeveranceCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Severance code ( " + TxtSeveranceCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private void SetLueSeveranceType(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='SeveranceType' ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

       
        #endregion

        #region Event

        private void TxtYearsWorked_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtYearsWorked, 0);
        }

        private void TxtAmt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void LueSeveranceType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSeveranceType, new Sm.RefreshLue1(SetLueSeveranceType));
        }
        #endregion

       

    }
}
