﻿#region Update

/*
    02/02/2021 [ICA/SIER] Saat menampilkan Downpayment auto dikurangi dengan returnAmt dan SettledAmt Based On Param IsReturnARDPUseSettledAmt
 *  18/02/2021 [ICA/SIER] Tidak Jadi Auto Insert Grd2 baris 1
 *  31/03/2021 [ICA/KSM] Menampilkan kolom customer category berdasarakan param IsShowCustomerCategory
 *  12/04/2021 [BRI/SIER] Menampilkan kolom customer category berdasarakan param IsShowCustomerCategory
 *  28/10/2021 [RIS/AMKA] Menampilkan kolom custumer terfilter berdasarkan group dan param IsFilterByCtCt
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmReturnARDownpaymentDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmReturnARDownpayment mFrmParent;

        #endregion

        #region Constructor

        public FrmReturnARDownpaymentDlg(FrmReturnARDownpayment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Customer Deposit Summary";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Customer",
                    "Customer",
                    "Customer Category",
                    "Entity",
                    "Entity",

                    //6
                    "Currency",
                    "Amount",
                },  
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    0, 200, 200, 200, 200, 
                    
                    //6-7
                    80, 150
                }
            );

            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4 }, false);
            if (mFrmParent.mIsShowCustomerCategory)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3 }, (Sm.GetParameter("DocTitle") == "KSM" || Sm.GetParameter("DocTitle") == "SIER"));
            }
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode, B.CtName, A.EntCode, C.EntName, A.CurCode, E.CtCtName, ");
            if (mFrmParent.mIsReturnARDPUseSettledAmt)
                SQL.AppendLine("A.Amt-(IfNull(D.Amt, 0)+ IfNull(D.SettledAmt, 0)) As Amt ");
            else
                SQL.AppendLine("A.Amt-IfNull(D.Amt, 0) As Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode " + Filter);
            SQL.AppendLine("Left Join TblEntity C On A.EntCode=C.EntCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.CtCode, IfNull(T1.EntCode, '') As EntCode, T1.CurCode, Sum(T1.Amt) As Amt, ");
            if (mFrmParent.mIsReturnARDPUseSettledAmt)
                SQL.AppendLine("Sum(T1.SettledAmt) As SettledAmt ");
            else
                SQL.AppendLine("0.00 As SettledAmt ");
            SQL.AppendLine("    From TblReturnARDownpayment T1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr T2 ");
            SQL.AppendLine("        On T1.VoucherRequestDocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status<>'C' ");
            SQL.AppendLine("        And T2.VoucherDocNo Is Null ");
            SQL.AppendLine("    Where T1.Status<>'C' And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.CtCode, IfNull(T1.EntCode, ''), T1.CurCode ");
            SQL.AppendLine(") D ");
            SQL.AppendLine("    On A.CtCode=D.CtCode ");
            SQL.AppendLine("    And IfNull(A.EntCode, '')=IfNull(D.EntCode, '') ");
            SQL.AppendLine("    And A.CurCode=D.CurCode ");
            SQL.AppendLine("Left Join TblCustomerCategory E On B.CtCtCode = E.CtCtCode ");
            if (mFrmParent.mIsReturnARDPUseSettledAmt)
                SQL.AppendLine("Where A.Amt-(IfNull(D.Amt, 0)+ IfNull(D.SettledAmt, 0))>0 ");
            else
                SQL.AppendLine("Where A.Amt-IfNull(D.Amt, 0)>0 ");            
            if (mFrmParent.mIsFilterByCtCt)
            {
                SQL.AppendLine("And Exists ( ");
                SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("    Where CtCtCode=B.CtCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where ");
                SQL.AppendLine("    UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");

            }
            SQL.AppendLine("Order By B.CtName; ");

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtCtCode.Text, "B.CtName", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[] 
                        { 
                             //0
                             "CtCode", 
                             
                             //1-5
                             "CtName", 
                             "CtCtName",
                             "EntCode", 
                             "EntName", 
                             "CurCode", 

                             //6-7
                             "Amt",
                             //"AcNo",
                             //"AcDesc"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            //Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            //Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                mFrmParent.mCtCode = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtCtCtName.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.mEntCode = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtEntCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtCurCode.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                mFrmParent.TxtSummary.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 7), 0);
                mFrmParent.mSummary = Sm.GetGrdDec(Grd1, Row, 7);
                mFrmParent.TxtReturnAmt.EditValue = Sm.FormatNum(0m, 0);

                //if (mFrmParent.mIsReturnARDPUseSettledAmt)
                //{
                //    Sm.CopyGrdValue(mFrmParent.Grd2, 0, 1, Grd1, Row, 7);
                //    Sm.CopyGrdValue(mFrmParent.Grd2, 0, 2, Grd1, Row, 8);
                //    Sm.CopyGrdValue(mFrmParent.Grd2, 0, 3, Grd1, Row, 6);
                //    Sm.SetGrdNumValueZero(mFrmParent.Grd2, 0, new int[] { 4 });

                //    mFrmParent.Grd2.Rows.Add();
                //    Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 3, 4 });
                //}
               
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtCtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer");
        }

        #endregion

        #endregion

    }
}
