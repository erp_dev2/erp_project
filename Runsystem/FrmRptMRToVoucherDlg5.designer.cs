﻿namespace RunSystem
{
    partial class FrmRptMRToVoucherDlg5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtWhsName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtVdName = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWhsName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 138);
            this.panel3.Size = new System.Drawing.Size(438, 335);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(438, 335);
            this.Grd1.TabIndex = 23;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtWhsName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtVdName);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Size = new System.Drawing.Size(438, 138);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(54, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "Item Code";
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(124, 90);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 30;
            this.TxtItCode.Size = new System.Drawing.Size(208, 20);
            this.TxtItCode.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(50, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Warehouse";
            // 
            // TxtWhsName
            // 
            this.TxtWhsName.EnterMoveNextControl = true;
            this.TxtWhsName.Location = new System.Drawing.Point(124, 69);
            this.TxtWhsName.Name = "TxtWhsName";
            this.TxtWhsName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWhsName.Properties.Appearance.Options.UseFont = true;
            this.TxtWhsName.Properties.MaxLength = 30;
            this.TxtWhsName.Size = new System.Drawing.Size(208, 20);
            this.TxtWhsName.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(72, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Vendor";
            // 
            // TxtVdName
            // 
            this.TxtVdName.EnterMoveNextControl = true;
            this.TxtVdName.Location = new System.Drawing.Point(124, 48);
            this.TxtVdName.Name = "TxtVdName";
            this.TxtVdName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdName.Properties.Appearance.Options.UseFont = true;
            this.TxtVdName.Properties.MaxLength = 30;
            this.TxtVdName.Size = new System.Drawing.Size(208, 20);
            this.TxtVdName.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document Number";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(124, 6);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Size = new System.Drawing.Size(208, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(51, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 14);
            this.label5.TabIndex = 21;
            this.label5.Text = "Item Name";
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(124, 111);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 30;
            this.TxtItName.Size = new System.Drawing.Size(208, 20);
            this.TxtItName.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(77, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 14);
            this.label6.TabIndex = 13;
            this.label6.Text = "Status";
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(124, 27);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Size = new System.Drawing.Size(208, 20);
            this.TxtStatus.TabIndex = 14;
            // 
            // FrmRptMRToVoucherDlg5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 473);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmRptMRToVoucherDlg5";
            this.Text = "Receiving Status";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWhsName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.TextEdit TxtWhsName;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.TextEdit TxtVdName;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.TextEdit TxtStatus;
    }
}