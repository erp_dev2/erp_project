﻿#region update
/*
    09/09/2021 [WED/PADI] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

// download file
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmQtDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmQt mFrmParent;
        private string mDocNo = string.Empty,
            mDNo = string.Empty,
            mSQL = string.Empty
            ;

        #endregion

        #region Constructor

        public FrmQtDlg2(FrmQt FrmParent, string DocNo, string DNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNo = DocNo;
            mDNo = DNo;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = BtnPrint.Visible = BtnExcel.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo2, FileLocation ");
            SQL.AppendLine("From TblQtDtl2 ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And DNo = @DNo ");
            SQL.AppendLine("Order by DNo2; ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-3
                    "DNo2",
                    "File",
                    ""
                },
                new int[] 
                {
                    //0
                    50,

                    //1-3
                    0, 200, 20
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
                Sm.CmParam<String>(ref cm, "@DNo", mDNo);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL,
                    new string[] 
                    { 
                        "DNo2", "FileLocation"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length > 0)
                {
                    var PbUpload = new System.Windows.Forms.ProgressBar();
                    mFrmParent.DownloadFile(mFrmParent.mHostAddrForFTPClient, mFrmParent.mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 2), mFrmParent.mUsernameForFTPClient, mFrmParent.mPasswordForFTPClient, mFrmParent.mSharedFolderForFTPClient, PbUpload);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 2, false, "File name is empty") && mFrmParent.downloadedData != null && mFrmParent.downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(mFrmParent.downloadedData, 0, mFrmParent.downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

    }
}
