﻿#region Update
/*
    06/12/2017 [TKG] New Reporting
    21/12/2017 [TKG] perubahan proses perhitungan
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptYearlyTax : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool mIsNotFilterByAuthorization = false;
        private decimal 
            mFunctionalExpenses = 0m,
            mFunctionalExpensesMaxAmt = 0m
            ;
        List<TI> lTI = new List<TI>();
        List<NTI> lNTI = new List<NTI>();

        #endregion

        #region Constructor

        public FrmRptYearlyTax(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
                ProcessTI(ref lTI);
                ProcessNTI(ref lNTI);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            mFunctionalExpensesMaxAmt = Sm.GetParameterDec("FunctionalExpensesMaxAmt");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Join Date",
                        "NPWP",

                        //6-10
                        "Non"+Environment.NewLine+"Incoming Tax",
                        "Department",
                        "Position",
                        "Site",
                        "Paid",

                        //11-12
                        "Recomputed",
                        "Balance"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 100, 100, 120, 
                        
                        //6-10
                        200, 180, 180, 180, 120,

                        //11-12
                        120, 120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6, 8, 9 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6, 8, 9 }, !ChkHideInfoInGrd.Checked);

        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year")) return;

            Cursor.Current = Cursors.WaitCursor;

            var l = new List<Data>();
            try
            {
                Process1(ref l);
                if (l.Count > 0)
                {
                    //Process2(ref l);
                    Process3(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional

        private void ProcessNTI(ref List<NTI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.ActInd='Y' And A.UsedFor='01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process1(ref List<Data> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, A.JoinDt, A.NPWP, A.PTKP, B.OptDesc As PTKPDesc, ");
            SQL.AppendLine("C.DeptName, D.PosName, E.SiteName, ");
            SQL.AppendLine("IfNull(F.Amt, 0.00) As Amt, IfNull(F.Tax, 0.00) As Tax, IfNull(F.Tax2, 0.00) As Tax2 ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat='NonTaxableIncome' And A.PTKP=B.OptCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On A.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As Amt, Sum(T.Tax) As Tax, Sum(T.Tax2) As Tax2 From (");
            SQL.AppendLine("    Select T1.EmpCode, ");

            SQL.AppendLine("        (IfNull(T1.Salary,0)+ IfNull(T1.ProcessPLAmt,0)-IfNull(T1.ProcessUPLAmt,0)+ IfNull(T1.OT1Amt,0)+ IfNull(T1.OT2Amt,0)+ ");
            SQL.AppendLine("        IfNull(T1.OTHolidayAmt,0)+ IfNull(T1.TaxableFixAllowance,0)+ IfNull(T1.EmploymentPeriodAllowance,0)+ IfNull(T1.IncEmployee,0)+ ");
            SQL.AppendLine("        IfNull(T1.IncMinWages,0)+ IfNull(T1.IncProduction,0)+ IfNull(T1.IncPerformance,0)+ IfNull(T1.PresenceReward,0)+ IfNull(T1.HolidayEarning,0)+ ");
            SQL.AppendLine("        IfNull(T1.ExtraFooding,0)+ IfNull(T1.SSEmployerHealth,0)+ IfNull(T1.SSEmployerEmployment,0)+ IfNull(T1.SalaryAdjustment,0)- ");
            SQL.AppendLine("        IfNull(T1.TaxableFixDeduction,0) - IfNull(T1.DedEmployee,0)- IfNull(T1.DedProduction,0)- IfNull(T1.DedProdLeave,0)- IfNull(T1.SSEmployeeEmployment,0)) As Amt, ");
            
            //SQL.AppendLine("    (T1.Amt+T1.Tax)-Case When (T1.Amt+T1.Tax)*@FunctionalExpenses*0.01>@FunctionalExpensesMaxAmt Then ");
            //SQL.AppendLine("        @FunctionalExpensesMaxAmt ");
            //SQL.AppendLine("    Else (T1.Amt+T1.Tax)*@FunctionalExpenses*0.01 ");
            //SQL.AppendLine("    End As Amt, ");
            SQL.AppendLine("    T1.Tax, ");
            SQL.AppendLine("    T1.Tax+T1.EOYTax As Tax2 ");
            SQL.AppendLine("    From TblPayrollProcess1 T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 " );
            SQL.AppendLine("        On T1.EmpCode=T2.EmpCode ");
            SQL.AppendLine("        And T1.ResignDt Is Null ");
            SQL.AppendLine("        And T1.NPWP Is Not Null ");
            SQL.AppendLine("        And T1.PTKP Is Not Null ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblEmployee ");
                SQL.AppendLine("        Where EmpCode=T1.EmpCode ");
                SQL.AppendLine("        And GrdLvlCode In ( ");
                SQL.AppendLine("            Select X2.GrdLvlCode ");
                SQL.AppendLine("            From TblPPAHdr X1 ");
                SQL.AppendLine("            Inner Join TblPPADtl X2 On X1.DocNo=X2.DocNo ");
                SQL.AppendLine("            Where X1.ActInd='Y' And X1.UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Inner Join TblPayrun T3 ");
            SQL.AppendLine("        On T1.PayrunCode=T3.PayrunCode ");
            SQL.AppendLine("        And T3.CancelInd='N' ");
            SQL.AppendLine("        And Left(T3.EndDt, 4)=@Yr ");
            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select T2.EmpCode, T2.Value As Amt, ");
            //SQL.AppendLine("    T2.Value-Case When T2.Value*@FunctionalExpenses*0.01>@FunctionalExpensesMaxAmt Then ");
            //SQL.AppendLine("        @FunctionalExpensesMaxAmt ");
            //SQL.AppendLine("    Else T2.Value*@FunctionalExpenses*0.01 ");
            //SQL.AppendLine("    End As Amt, ");
            SQL.AppendLine("    T2.Tax, 0.00 As Tax2 ");
            SQL.AppendLine("    From TblRHAHdr T1 ");
            SQL.AppendLine("    Inner Join TblRHADtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    And Left(T1.HolidayDt, 4)=@Yr ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And T2.EmpCode In ( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") T Group By T.EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");
            SQL.AppendLine("Where A.ResignDt Is Null ");
            SQL.AppendLine("And A.NPWP Is Not Null ");
            SQL.AppendLine("And A.PTKP Is Not Null ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By A.EmpName;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandTimeout = 600,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                //Sm.CmParam<Decimal>(ref cm, "@FunctionalExpenses", mFunctionalExpenses);
                //Sm.CmParam<Decimal>(ref cm, "@FunctionalExpensesMaxAmt", mFunctionalExpensesMaxAmt);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "EmpCode", 
                    "EmpName", "EmpCodeOld", "JoinDt", "NPWP", "PTKP",
                    "PTKPDesc", "DeptName", "PosName", "SiteName", "Amt",
                    "Tax", "Tax2"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            EmpCodeOld = Sm.DrStr(dr, c[2]),
                            JoinDt = Sm.DrStr(dr, c[3]),
                            NPWP = Sm.DrStr(dr, c[4]),
                            PTKP = Sm.DrStr(dr, c[5]),
                            PTKPDesc = Sm.DrStr(dr, c[6]),
                            DeptName = Sm.DrStr(dr, c[7]),
                            PosName = Sm.DrStr(dr, c[8]),
                            SiteName = Sm.DrStr(dr, c[9]),
                            TotalBrutto = Sm.DrDec(dr, c[10]),
                            TotalTax1 = Sm.DrDec(dr, c[11]),
                            TotalTax2 = Sm.DrDec(dr, c[12])
                        });
                    }
                }
                dr.Close();

            }
        }

        private void Process2(ref List<Data> l)
        {
            decimal
                Tax = 0m,
                NTIAmt = 0m,
                TaxTemp = 0m,
                Amt2Temp = 0m,
                FunctionalExpenses = 0m;
            string NTI = string.Empty, Yr = Sm.GetLue(LueYr);

            for (var i = 0; i < l.Count; i++)
            {
                Tax = l[i].TotalBrutto;
                NTIAmt = 0m;
                TaxTemp = 0m; 
                Amt2Temp = 0m;
                NTI = l[i].PTKP;

                FunctionalExpenses = (Tax * (mFunctionalExpenses / 100m));

                if (FunctionalExpenses > mFunctionalExpensesMaxAmt * 12m)
                    FunctionalExpenses = mFunctionalExpensesMaxAmt * 12m;

                Tax = Tax - FunctionalExpenses;

                foreach (var x in lNTI.Where(x => Sm.CompareStr(x.Status, NTI)))
                    NTIAmt = x.Amt;

                if (Tax > NTIAmt)
                    Tax -= NTIAmt;
                else
                    Tax = 0m;

                if (Tax > 0 && lTI.Count > 0)
                {
                    TaxTemp = Tax;
                    Amt2Temp = 0m;
                    Tax = 0m;

                    foreach (TI t in lTI.OrderBy(x => x.SeqNo))
                    {
                        if (TaxTemp > 0m)
                        {
                            if (TaxTemp <= (t.Amt2 - Amt2Temp))
                            {
                                Tax += (TaxTemp * t.TaxRate / 100m);
                                TaxTemp = 0m;
                            }
                            else
                            {
                                Tax += ((t.Amt2 - Amt2Temp) * t.TaxRate / 100m);
                                TaxTemp -= (t.Amt2 - Amt2Temp);
                            }
                        }
                        Amt2Temp = t.Amt2;
                    }
                    Tax = decimal.Truncate(Tax);
                    Tax = Tax - (Tax % 100);
                    Tax = decimal.Truncate(Tax);
                }
                if (Tax <= 0) Tax = 0m;
                l[i].TotalTax2 = Tax;
            }
        }

        private void Process3(ref List<Data> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].TotalTax1 != 0m || l[i].TotalTax2 != 0m)
                {
                    r = Grd1.Rows.Add();
                    r.Cells[0].Value = i + 1;
                    r.Cells[1].Value = l[i].EmpCode;
                    r.Cells[2].Value = l[i].EmpName;
                    r.Cells[3].Value = l[i].EmpCodeOld;
                    if (l[i].JoinDt.Length > 0) r.Cells[4].Value = Sm.ConvertDate(l[i].JoinDt);
                    r.Cells[5].Value = l[i].NPWP;
                    r.Cells[6].Value = l[i].PTKPDesc;
                    r.Cells[7].Value = l[i].DeptName;
                    r.Cells[8].Value = l[i].PosName;
                    r.Cells[9].Value = l[i].SiteName;
                    r.Cells[10].Value = l[i].TotalTax1;
                    r.Cells[11].Value = l[i].TotalTax2;
                    r.Cells[12].Value = l[i].TotalTax1 - l[i].TotalTax2;
                }
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 12 });
            Grd1.EndUpdate();
            if (Grd1.Rows.Count == 0) Sm.StdMsg(mMsgType.NoData, string.Empty);
        }

        #endregion

        #endregion

        #region Event

        private void FrmRptYearlyTax_FormClosing(object sender, FormClosingEventArgs e)
        {
            lTI.Clear();
            lNTI.Clear();
        }

        #endregion

        #region Class

        private class Data
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string JoinDt { get; set; }
            public string NPWP { get; set; }
            public string PTKP { get; set; }
            public string PTKPDesc { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string SiteName { get; set; }
            public decimal TotalBrutto { get; set; }
            public decimal TotalTax1 { get; set; }
            public decimal TotalTax2 { get; set; }
        }

        private class NTI
        {
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        #endregion
    }
}
