﻿namespace RunSystem
{
    partial class FrmAcquisitionDebtSecurities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAcquisitionDebtSecurities));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpInvestmentDetails = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this.LueInvestmentType = new DevExpress.XtraEditors.LookUpEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.DteNextCouponDt = new DevExpress.XtraEditors.DateEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.DteLastCouponDt = new DevExpress.XtraEditors.DateEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.DteMaturityDt = new DevExpress.XtraEditors.DateEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.DteListingDt = new DevExpress.XtraEditors.DateEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtInterestFrequency = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtAnnualDays = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtCouponInterestRate = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcquisitionPrice = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.TxtCurRate = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtCtInvestment = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtInvestmentName = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnInvestmentName = new DevExpress.XtraEditors.SimpleButton();
            this.TxtInvestmentCode = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.BtnInvestmentCode = new DevExpress.XtraEditors.SimpleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.DteTradeDt = new DevExpress.XtraEditors.DateEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtTotalAcquisition = new DevExpress.XtraEditors.TextEdit();
            this.DteSettlementDt = new DevExpress.XtraEditors.DateEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtTotalExpenses = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtAccruedInterest = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtInvestmentCost = new DevExpress.XtraEditors.TextEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.LueFinancialInstitution = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtInvestmentBankAcc = new DevExpress.XtraEditors.TextEdit();
            this.LblInvestBankAcc = new System.Windows.Forms.Label();
            this.BtnInvestmentBankAcc = new DevExpress.XtraEditors.SimpleButton();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtDocNoVoucher = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.TpExpensesOtherDetail = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.DteExpensesDocDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.BtnRefreshData = new DevExpress.XtraEditors.SimpleButton();
            this.TpApproval = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.label30 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TpInvestmentDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestFrequency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAnnualDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCouponInterestRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcquisitionPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtInvestment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTradeDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTradeDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAcquisition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSettlementDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSettlementDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalExpenses.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAccruedInterest.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFinancialInstitution.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentBankAcc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoVoucher.Properties)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.TpExpensesOtherDetail.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpensesDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpensesDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel9.SuspendLayout();
            this.TpApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(797, 0);
            this.panel1.Size = new System.Drawing.Size(70, 625);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Click += new System.EventHandler(this.BtnInsert_Click);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Click += new System.EventHandler(this.BtnFind_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl2);
            this.panel2.Controls.Add(this.TxtDocNoVoucher);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.LueFinancialInstitution);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.MeeCancelReason);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtInvestmentBankAcc);
            this.panel2.Controls.Add(this.LblInvestBankAcc);
            this.panel2.Controls.Add(this.BtnInvestmentBankAcc);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Size = new System.Drawing.Size(797, 625);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpInvestmentDetails);
            this.tabControl1.Location = new System.Drawing.Point(0, 157);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 294);
            this.tabControl1.TabIndex = 0;
            // 
            // TpInvestmentDetails
            // 
            this.TpInvestmentDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpInvestmentDetails.Controls.Add(this.label30);
            this.TpInvestmentDetails.Controls.Add(this.label29);
            this.TpInvestmentDetails.Controls.Add(this.LueInvestmentType);
            this.TpInvestmentDetails.Controls.Add(this.label28);
            this.TpInvestmentDetails.Controls.Add(this.DteNextCouponDt);
            this.TpInvestmentDetails.Controls.Add(this.label27);
            this.TpInvestmentDetails.Controls.Add(this.DteLastCouponDt);
            this.TpInvestmentDetails.Controls.Add(this.label26);
            this.TpInvestmentDetails.Controls.Add(this.DteMaturityDt);
            this.TpInvestmentDetails.Controls.Add(this.label25);
            this.TpInvestmentDetails.Controls.Add(this.DteListingDt);
            this.TpInvestmentDetails.Controls.Add(this.label24);
            this.TpInvestmentDetails.Controls.Add(this.TxtInterestFrequency);
            this.TpInvestmentDetails.Controls.Add(this.label23);
            this.TpInvestmentDetails.Controls.Add(this.TxtAnnualDays);
            this.TpInvestmentDetails.Controls.Add(this.label22);
            this.TpInvestmentDetails.Controls.Add(this.TxtCouponInterestRate);
            this.TpInvestmentDetails.Controls.Add(this.TxtAcquisitionPrice);
            this.TpInvestmentDetails.Controls.Add(this.label21);
            this.TpInvestmentDetails.Controls.Add(this.TxtAmt);
            this.TpInvestmentDetails.Controls.Add(this.TxtCurRate);
            this.TpInvestmentDetails.Controls.Add(this.label8);
            this.TpInvestmentDetails.Controls.Add(this.TxtCurCode);
            this.TpInvestmentDetails.Controls.Add(this.label20);
            this.TpInvestmentDetails.Controls.Add(this.TxtCtInvestment);
            this.TpInvestmentDetails.Controls.Add(this.label7);
            this.TpInvestmentDetails.Controls.Add(this.label19);
            this.TpInvestmentDetails.Controls.Add(this.label18);
            this.TpInvestmentDetails.Controls.Add(this.TxtInvestmentName);
            this.TpInvestmentDetails.Controls.Add(this.label17);
            this.TpInvestmentDetails.Controls.Add(this.BtnInvestmentName);
            this.TpInvestmentDetails.Controls.Add(this.TxtInvestmentCode);
            this.TpInvestmentDetails.Controls.Add(this.label16);
            this.TpInvestmentDetails.Controls.Add(this.BtnInvestmentCode);
            this.TpInvestmentDetails.Controls.Add(this.label3);
            this.TpInvestmentDetails.Controls.Add(this.label14);
            this.TpInvestmentDetails.Controls.Add(this.DteTradeDt);
            this.TpInvestmentDetails.Controls.Add(this.MeeRemark);
            this.TpInvestmentDetails.Controls.Add(this.label4);
            this.TpInvestmentDetails.Controls.Add(this.TxtTotalAcquisition);
            this.TpInvestmentDetails.Controls.Add(this.DteSettlementDt);
            this.TpInvestmentDetails.Controls.Add(this.label12);
            this.TpInvestmentDetails.Controls.Add(this.TxtTotalExpenses);
            this.TpInvestmentDetails.Controls.Add(this.label11);
            this.TpInvestmentDetails.Controls.Add(this.TxtAccruedInterest);
            this.TpInvestmentDetails.Controls.Add(this.label9);
            this.TpInvestmentDetails.Controls.Add(this.label10);
            this.TpInvestmentDetails.Controls.Add(this.TxtInvestmentCost);
            this.TpInvestmentDetails.Location = new System.Drawing.Point(4, 26);
            this.TpInvestmentDetails.Name = "TpInvestmentDetails";
            this.TpInvestmentDetails.Padding = new System.Windows.Forms.Padding(3);
            this.TpInvestmentDetails.Size = new System.Drawing.Size(792, 264);
            this.TpInvestmentDetails.TabIndex = 0;
            this.TpInvestmentDetails.Text = "Investment Details";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(340, 113);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(19, 14);
            this.label29.TabIndex = 73;
            this.label29.Text = "%";
            // 
            // LueInvestmentType
            // 
            this.LueInvestmentType.EnterMoveNextControl = true;
            this.LueInvestmentType.Location = new System.Drawing.Point(145, 46);
            this.LueInvestmentType.Margin = new System.Windows.Forms.Padding(5);
            this.LueInvestmentType.Name = "LueInvestmentType";
            this.LueInvestmentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentType.Properties.DropDownRows = 30;
            this.LueInvestmentType.Properties.NullText = "[Empty]";
            this.LueInvestmentType.Properties.PopupWidth = 300;
            this.LueInvestmentType.Size = new System.Drawing.Size(193, 20);
            this.LueInvestmentType.TabIndex = 72;
            this.LueInvestmentType.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInvestmentType.EditValueChanged += new System.EventHandler(this.LueInvestmentType_EditValueChanged);
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(431, 112);
            this.label28.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(109, 14);
            this.label28.TabIndex = 58;
            this.label28.Text = "Next Coupon Date";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteNextCouponDt
            // 
            this.DteNextCouponDt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DteNextCouponDt.EditValue = null;
            this.DteNextCouponDt.EnterMoveNextControl = true;
            this.DteNextCouponDt.Location = new System.Drawing.Point(542, 109);
            this.DteNextCouponDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteNextCouponDt.Name = "DteNextCouponDt";
            this.DteNextCouponDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteNextCouponDt.Properties.Appearance.Options.UseFont = true;
            this.DteNextCouponDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteNextCouponDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteNextCouponDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteNextCouponDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteNextCouponDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteNextCouponDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteNextCouponDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteNextCouponDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteNextCouponDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteNextCouponDt.Size = new System.Drawing.Size(244, 20);
            this.DteNextCouponDt.TabIndex = 59;
            this.DteNextCouponDt.Validated += new System.EventHandler(this.DteNextCouponDt_Validated);
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(435, 91);
            this.label27.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(105, 14);
            this.label27.TabIndex = 56;
            this.label27.Text = "Last Coupon Date";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteLastCouponDt
            // 
            this.DteLastCouponDt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DteLastCouponDt.EditValue = null;
            this.DteLastCouponDt.EnterMoveNextControl = true;
            this.DteLastCouponDt.Location = new System.Drawing.Point(542, 88);
            this.DteLastCouponDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLastCouponDt.Name = "DteLastCouponDt";
            this.DteLastCouponDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastCouponDt.Properties.Appearance.Options.UseFont = true;
            this.DteLastCouponDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastCouponDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLastCouponDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLastCouponDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLastCouponDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastCouponDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLastCouponDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastCouponDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLastCouponDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLastCouponDt.Size = new System.Drawing.Size(244, 20);
            this.DteLastCouponDt.TabIndex = 57;
            this.DteLastCouponDt.Validated += new System.EventHandler(this.DteLastCouponDt_Validated);
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(459, 70);
            this.label26.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(81, 14);
            this.label26.TabIndex = 54;
            this.label26.Text = "Maturity Date";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteMaturityDt
            // 
            this.DteMaturityDt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DteMaturityDt.EditValue = null;
            this.DteMaturityDt.EnterMoveNextControl = true;
            this.DteMaturityDt.Location = new System.Drawing.Point(542, 67);
            this.DteMaturityDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteMaturityDt.Name = "DteMaturityDt";
            this.DteMaturityDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteMaturityDt.Properties.Appearance.Options.UseFont = true;
            this.DteMaturityDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteMaturityDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteMaturityDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteMaturityDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteMaturityDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteMaturityDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteMaturityDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteMaturityDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteMaturityDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteMaturityDt.Size = new System.Drawing.Size(244, 20);
            this.DteMaturityDt.TabIndex = 55;
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(471, 49);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 14);
            this.label25.TabIndex = 52;
            this.label25.Text = "Listing Date";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteListingDt
            // 
            this.DteListingDt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DteListingDt.EditValue = null;
            this.DteListingDt.EnterMoveNextControl = true;
            this.DteListingDt.Location = new System.Drawing.Point(542, 46);
            this.DteListingDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteListingDt.Name = "DteListingDt";
            this.DteListingDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteListingDt.Properties.Appearance.Options.UseFont = true;
            this.DteListingDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteListingDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteListingDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteListingDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteListingDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteListingDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteListingDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteListingDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteListingDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteListingDt.Size = new System.Drawing.Size(244, 20);
            this.DteListingDt.TabIndex = 53;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(31, 196);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(112, 14);
            this.label24.TabIndex = 46;
            this.label24.Text = "Interest Frequency";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInterestFrequency
            // 
            this.TxtInterestFrequency.EnterMoveNextControl = true;
            this.TxtInterestFrequency.Location = new System.Drawing.Point(145, 193);
            this.TxtInterestFrequency.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInterestFrequency.Name = "TxtInterestFrequency";
            this.TxtInterestFrequency.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtInterestFrequency.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInterestFrequency.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInterestFrequency.Properties.Appearance.Options.UseFont = true;
            this.TxtInterestFrequency.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInterestFrequency.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInterestFrequency.Properties.MaxLength = 30;
            this.TxtInterestFrequency.Properties.ReadOnly = true;
            this.TxtInterestFrequency.Size = new System.Drawing.Size(193, 20);
            this.TxtInterestFrequency.TabIndex = 47;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(3, 175);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(140, 14);
            this.label23.TabIndex = 44;
            this.label23.Text = "Annual Days Assumption";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAnnualDays
            // 
            this.TxtAnnualDays.EnterMoveNextControl = true;
            this.TxtAnnualDays.Location = new System.Drawing.Point(145, 172);
            this.TxtAnnualDays.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAnnualDays.Name = "TxtAnnualDays";
            this.TxtAnnualDays.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtAnnualDays.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAnnualDays.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAnnualDays.Properties.Appearance.Options.UseFont = true;
            this.TxtAnnualDays.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAnnualDays.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAnnualDays.Properties.MaxLength = 30;
            this.TxtAnnualDays.Properties.ReadOnly = true;
            this.TxtAnnualDays.Size = new System.Drawing.Size(193, 20);
            this.TxtAnnualDays.TabIndex = 45;
            this.TxtAnnualDays.EditValueChanged += new System.EventHandler(this.TxtAnnualDays_EditValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(16, 154);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(126, 14);
            this.label22.TabIndex = 42;
            this.label22.Text = "Coupon Interest Rate";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCouponInterestRate
            // 
            this.TxtCouponInterestRate.EnterMoveNextControl = true;
            this.TxtCouponInterestRate.Location = new System.Drawing.Point(145, 151);
            this.TxtCouponInterestRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCouponInterestRate.Name = "TxtCouponInterestRate";
            this.TxtCouponInterestRate.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtCouponInterestRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCouponInterestRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCouponInterestRate.Properties.Appearance.Options.UseFont = true;
            this.TxtCouponInterestRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCouponInterestRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCouponInterestRate.Properties.MaxLength = 30;
            this.TxtCouponInterestRate.Properties.ReadOnly = true;
            this.TxtCouponInterestRate.Size = new System.Drawing.Size(193, 20);
            this.TxtCouponInterestRate.TabIndex = 43;
            this.TxtCouponInterestRate.EditValueChanged += new System.EventHandler(this.TxtCouponInterestRate_EditValueChanged);
            // 
            // TxtAcquisitionPrice
            // 
            this.TxtAcquisitionPrice.EnterMoveNextControl = true;
            this.TxtAcquisitionPrice.Location = new System.Drawing.Point(145, 109);
            this.TxtAcquisitionPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcquisitionPrice.Name = "TxtAcquisitionPrice";
            this.TxtAcquisitionPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcquisitionPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcquisitionPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcquisitionPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtAcquisitionPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAcquisitionPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAcquisitionPrice.Properties.MaxLength = 16;
            this.TxtAcquisitionPrice.Properties.ReadOnly = true;
            this.TxtAcquisitionPrice.Size = new System.Drawing.Size(193, 20);
            this.TxtAcquisitionPrice.TabIndex = 39;
            this.TxtAcquisitionPrice.EditValueChanged += new System.EventHandler(this.TxtAcquisitionPrice_EditValueChanged);
            this.TxtAcquisitionPrice.Validated += new System.EventHandler(this.TxtAcquisitionPrice_Validated);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(47, 112);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(95, 14);
            this.label21.TabIndex = 38;
            this.label21.Text = "Acquisition Price";
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(145, 88);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.MaxLength = 16;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(193, 20);
            this.TxtAmt.TabIndex = 37;
            this.TxtAmt.EditValueChanged += new System.EventHandler(this.TxtAmt_EditValueChanged);
            this.TxtAmt.Validated += new System.EventHandler(this.TxtAmt_Validated);
            // 
            // TxtCurRate
            // 
            this.TxtCurRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCurRate.EnterMoveNextControl = true;
            this.TxtCurRate.Location = new System.Drawing.Point(542, 151);
            this.TxtCurRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurRate.Name = "TxtCurRate";
            this.TxtCurRate.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtCurRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurRate.Properties.Appearance.Options.UseFont = true;
            this.TxtCurRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCurRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCurRate.Properties.MaxLength = 30;
            this.TxtCurRate.Properties.ReadOnly = true;
            this.TxtCurRate.Size = new System.Drawing.Size(242, 20);
            this.TxtCurRate.TabIndex = 63;
            this.TxtCurRate.EditValueChanged += new System.EventHandler(this.TxtCurRate_EditValueChanged);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(456, 154);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 14);
            this.label8.TabIndex = 62;
            this.label8.Text = "Currency Rate";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(542, 130);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 30;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(242, 20);
            this.TxtCurCode.TabIndex = 61;
            this.TxtCurCode.EditValueChanged += new System.EventHandler(this.TxtCurCode_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(45, 91);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 14);
            this.label20.TabIndex = 36;
            this.label20.Text = "Nominal Amount";
            // 
            // TxtCtInvestment
            // 
            this.TxtCtInvestment.EnterMoveNextControl = true;
            this.TxtCtInvestment.Location = new System.Drawing.Point(145, 67);
            this.TxtCtInvestment.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtInvestment.Name = "TxtCtInvestment";
            this.TxtCtInvestment.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtInvestment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtInvestment.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtInvestment.Properties.Appearance.Options.UseFont = true;
            this.TxtCtInvestment.Properties.MaxLength = 16;
            this.TxtCtInvestment.Properties.ReadOnly = true;
            this.TxtCtInvestment.Size = new System.Drawing.Size(193, 20);
            this.TxtCtInvestment.TabIndex = 35;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(485, 133);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 14);
            this.label7.TabIndex = 60;
            this.label7.Text = "Currency";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(19, 70);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(123, 14);
            this.label19.TabIndex = 34;
            this.label19.Text = "Investment Category\r\n";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(40, 49);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(102, 14);
            this.label18.TabIndex = 32;
            this.label18.Text = "Investment Type";
            // 
            // TxtInvestmentName
            // 
            this.TxtInvestmentName.EnterMoveNextControl = true;
            this.TxtInvestmentName.Location = new System.Drawing.Point(145, 25);
            this.TxtInvestmentName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentName.Name = "TxtInvestmentName";
            this.TxtInvestmentName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentName.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentName.Properties.MaxLength = 16;
            this.TxtInvestmentName.Properties.ReadOnly = true;
            this.TxtInvestmentName.Size = new System.Drawing.Size(193, 20);
            this.TxtInvestmentName.TabIndex = 30;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(38, 28);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 14);
            this.label17.TabIndex = 29;
            this.label17.Text = "Investment Name";
            // 
            // BtnInvestmentName
            // 
            this.BtnInvestmentName.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnInvestmentName.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInvestmentName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInvestmentName.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInvestmentName.Appearance.Options.UseBackColor = true;
            this.BtnInvestmentName.Appearance.Options.UseFont = true;
            this.BtnInvestmentName.Appearance.Options.UseForeColor = true;
            this.BtnInvestmentName.Appearance.Options.UseTextOptions = true;
            this.BtnInvestmentName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInvestmentName.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnInvestmentName.Image = ((System.Drawing.Image)(resources.GetObject("BtnInvestmentName.Image")));
            this.BtnInvestmentName.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnInvestmentName.Location = new System.Drawing.Point(340, 25);
            this.BtnInvestmentName.Name = "BtnInvestmentName";
            this.BtnInvestmentName.Size = new System.Drawing.Size(24, 21);
            this.BtnInvestmentName.TabIndex = 31;
            this.BtnInvestmentName.ToolTip = "Show WO Information";
            this.BtnInvestmentName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnInvestmentName.ToolTipTitle = "Run System";
            this.BtnInvestmentName.Click += new System.EventHandler(this.BtnInvestmentName_Click);
            // 
            // TxtInvestmentCode
            // 
            this.TxtInvestmentCode.EnterMoveNextControl = true;
            this.TxtInvestmentCode.Location = new System.Drawing.Point(145, 4);
            this.TxtInvestmentCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentCode.Name = "TxtInvestmentCode";
            this.TxtInvestmentCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCode.Properties.MaxLength = 16;
            this.TxtInvestmentCode.Properties.ReadOnly = true;
            this.TxtInvestmentCode.Size = new System.Drawing.Size(193, 20);
            this.TxtInvestmentCode.TabIndex = 27;
            this.TxtInvestmentCode.EditValueChanged += new System.EventHandler(this.TxtInvestmentCode_EditValueChanged);
            this.TxtInvestmentCode.Validated += new System.EventHandler(this.TxtInvestmentCode_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(40, 7);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 14);
            this.label16.TabIndex = 26;
            this.label16.Text = "Investment Code";
            // 
            // BtnInvestmentCode
            // 
            this.BtnInvestmentCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnInvestmentCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInvestmentCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInvestmentCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInvestmentCode.Appearance.Options.UseBackColor = true;
            this.BtnInvestmentCode.Appearance.Options.UseFont = true;
            this.BtnInvestmentCode.Appearance.Options.UseForeColor = true;
            this.BtnInvestmentCode.Appearance.Options.UseTextOptions = true;
            this.BtnInvestmentCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInvestmentCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnInvestmentCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnInvestmentCode.Image")));
            this.BtnInvestmentCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnInvestmentCode.Location = new System.Drawing.Point(340, 4);
            this.BtnInvestmentCode.Name = "BtnInvestmentCode";
            this.BtnInvestmentCode.Size = new System.Drawing.Size(24, 21);
            this.BtnInvestmentCode.TabIndex = 28;
            this.BtnInvestmentCode.ToolTip = "Show WO Information";
            this.BtnInvestmentCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnInvestmentCode.ToolTipTitle = "Run System";
            this.BtnInvestmentCode.Click += new System.EventHandler(this.BtnInvestmentCode_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(471, 7);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 14);
            this.label3.TabIndex = 48;
            this.label3.Text = "Trade Date";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(488, 237);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 14);
            this.label14.TabIndex = 70;
            this.label14.Text = "Remarks";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTradeDt
            // 
            this.DteTradeDt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DteTradeDt.EditValue = null;
            this.DteTradeDt.EnterMoveNextControl = true;
            this.DteTradeDt.Location = new System.Drawing.Point(542, 4);
            this.DteTradeDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTradeDt.Name = "DteTradeDt";
            this.DteTradeDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTradeDt.Properties.Appearance.Options.UseFont = true;
            this.DteTradeDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTradeDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTradeDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTradeDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTradeDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTradeDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTradeDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTradeDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTradeDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTradeDt.Size = new System.Drawing.Size(244, 20);
            this.DteTradeDt.TabIndex = 49;
            this.DteTradeDt.EditValueChanged += new System.EventHandler(this.DteTradeDt_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(542, 234);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(242, 20);
            this.MeeRemark.TabIndex = 71;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(441, 28);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 14);
            this.label4.TabIndex = 50;
            this.label4.Text = "Settlement Date";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalAcquisition
            // 
            this.TxtTotalAcquisition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTotalAcquisition.EnterMoveNextControl = true;
            this.TxtTotalAcquisition.Location = new System.Drawing.Point(542, 213);
            this.TxtTotalAcquisition.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalAcquisition.Name = "TxtTotalAcquisition";
            this.TxtTotalAcquisition.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtTotalAcquisition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalAcquisition.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalAcquisition.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalAcquisition.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalAcquisition.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalAcquisition.Properties.MaxLength = 30;
            this.TxtTotalAcquisition.Properties.ReadOnly = true;
            this.TxtTotalAcquisition.Size = new System.Drawing.Size(242, 20);
            this.TxtTotalAcquisition.TabIndex = 69;
            this.TxtTotalAcquisition.EditValueChanged += new System.EventHandler(this.TxtTotalAcquisition_EditValueChanged);
            // 
            // DteSettlementDt
            // 
            this.DteSettlementDt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DteSettlementDt.EditValue = null;
            this.DteSettlementDt.EnterMoveNextControl = true;
            this.DteSettlementDt.Location = new System.Drawing.Point(542, 25);
            this.DteSettlementDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteSettlementDt.Name = "DteSettlementDt";
            this.DteSettlementDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteSettlementDt.Properties.Appearance.Options.UseFont = true;
            this.DteSettlementDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteSettlementDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteSettlementDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteSettlementDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteSettlementDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteSettlementDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteSettlementDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteSettlementDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteSettlementDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteSettlementDt.Size = new System.Drawing.Size(244, 20);
            this.DteSettlementDt.TabIndex = 51;
            this.DteSettlementDt.EditValueChanged += new System.EventHandler(this.DteSettlementDt_EditValueChanged);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(415, 216);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 14);
            this.label12.TabIndex = 68;
            this.label12.Text = "Total Acquisition Cost";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalExpenses
            // 
            this.TxtTotalExpenses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTotalExpenses.EnterMoveNextControl = true;
            this.TxtTotalExpenses.Location = new System.Drawing.Point(542, 192);
            this.TxtTotalExpenses.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalExpenses.Name = "TxtTotalExpenses";
            this.TxtTotalExpenses.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtTotalExpenses.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalExpenses.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalExpenses.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalExpenses.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalExpenses.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalExpenses.Properties.MaxLength = 30;
            this.TxtTotalExpenses.Properties.ReadOnly = true;
            this.TxtTotalExpenses.Size = new System.Drawing.Size(242, 20);
            this.TxtTotalExpenses.TabIndex = 67;
            this.TxtTotalExpenses.EditValueChanged += new System.EventHandler(this.TxtTotalExpenses_EditValueChanged);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(356, 195);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(184, 14);
            this.label11.TabIndex = 66;
            this.label11.Text = "Total Expenses and Other Detail";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAccruedInterest
            // 
            this.TxtAccruedInterest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtAccruedInterest.EnterMoveNextControl = true;
            this.TxtAccruedInterest.Location = new System.Drawing.Point(542, 171);
            this.TxtAccruedInterest.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAccruedInterest.Name = "TxtAccruedInterest";
            this.TxtAccruedInterest.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtAccruedInterest.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAccruedInterest.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAccruedInterest.Properties.Appearance.Options.UseFont = true;
            this.TxtAccruedInterest.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAccruedInterest.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAccruedInterest.Properties.MaxLength = 30;
            this.TxtAccruedInterest.Properties.ReadOnly = true;
            this.TxtAccruedInterest.Size = new System.Drawing.Size(242, 20);
            this.TxtAccruedInterest.TabIndex = 65;
            this.TxtAccruedInterest.EditValueChanged += new System.EventHandler(this.TxtAccruedInterest_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(45, 133);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 14);
            this.label9.TabIndex = 40;
            this.label9.Text = "Investment Cost";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(440, 174);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 14);
            this.label10.TabIndex = 64;
            this.label10.Text = "Accrued Interest";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentCost
            // 
            this.TxtInvestmentCost.EnterMoveNextControl = true;
            this.TxtInvestmentCost.Location = new System.Drawing.Point(145, 130);
            this.TxtInvestmentCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentCost.Name = "TxtInvestmentCost";
            this.TxtInvestmentCost.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtInvestmentCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCost.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvestmentCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInvestmentCost.Properties.MaxLength = 30;
            this.TxtInvestmentCost.Properties.ReadOnly = true;
            this.TxtInvestmentCost.Size = new System.Drawing.Size(193, 20);
            this.TxtInvestmentCost.TabIndex = 41;
            this.TxtInvestmentCost.EditValueChanged += new System.EventHandler(this.TxtInvestmentCost_EditValueChanged);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(192, 108);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(193, 20);
            this.TxtStatus.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(148, 111);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 14);
            this.label6.TabIndex = 22;
            this.label6.Text = "Status";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFinancialInstitution
            // 
            this.LueFinancialInstitution.EnterMoveNextControl = true;
            this.LueFinancialInstitution.Location = new System.Drawing.Point(192, 66);
            this.LueFinancialInstitution.Margin = new System.Windows.Forms.Padding(5);
            this.LueFinancialInstitution.Name = "LueFinancialInstitution";
            this.LueFinancialInstitution.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitution.Properties.Appearance.Options.UseFont = true;
            this.LueFinancialInstitution.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitution.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFinancialInstitution.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitution.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFinancialInstitution.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitution.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFinancialInstitution.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitution.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFinancialInstitution.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFinancialInstitution.Properties.DropDownRows = 30;
            this.LueFinancialInstitution.Properties.NullText = "[Empty]";
            this.LueFinancialInstitution.Properties.PopupWidth = 300;
            this.LueFinancialInstitution.Size = new System.Drawing.Size(193, 20);
            this.LueFinancialInstitution.TabIndex = 18;
            this.LueFinancialInstitution.ToolTip = "F4 : Show/hide list";
            this.LueFinancialInstitution.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFinancialInstitution.EditValueChanged += new System.EventHandler(this.LueFinancialInstitution_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(79, 69);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "Financial Institution";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(192, 45);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 500;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(193, 20);
            this.MeeCancelReason.TabIndex = 15;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(386, 45);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(60, 22);
            this.ChkCancelInd.TabIndex = 16;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(54, 49);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 14);
            this.label13.TabIndex = 14;
            this.label13.Text = "Reason For Cancellation";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentBankAcc
            // 
            this.TxtInvestmentBankAcc.EnterMoveNextControl = true;
            this.TxtInvestmentBankAcc.Location = new System.Drawing.Point(192, 87);
            this.TxtInvestmentBankAcc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentBankAcc.Name = "TxtInvestmentBankAcc";
            this.TxtInvestmentBankAcc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentBankAcc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentBankAcc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentBankAcc.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentBankAcc.Properties.MaxLength = 16;
            this.TxtInvestmentBankAcc.Properties.ReadOnly = true;
            this.TxtInvestmentBankAcc.Size = new System.Drawing.Size(193, 20);
            this.TxtInvestmentBankAcc.TabIndex = 20;
            // 
            // LblInvestBankAcc
            // 
            this.LblInvestBankAcc.AutoSize = true;
            this.LblInvestBankAcc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInvestBankAcc.ForeColor = System.Drawing.Color.Red;
            this.LblInvestBankAcc.Location = new System.Drawing.Point(3, 90);
            this.LblInvestBankAcc.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblInvestBankAcc.Name = "LblInvestBankAcc";
            this.LblInvestBankAcc.Size = new System.Drawing.Size(187, 14);
            this.LblInvestBankAcc.TabIndex = 19;
            this.LblInvestBankAcc.Text = "Investment Bank Account (RDN)";
            // 
            // BtnInvestmentBankAcc
            // 
            this.BtnInvestmentBankAcc.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnInvestmentBankAcc.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInvestmentBankAcc.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInvestmentBankAcc.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInvestmentBankAcc.Appearance.Options.UseBackColor = true;
            this.BtnInvestmentBankAcc.Appearance.Options.UseFont = true;
            this.BtnInvestmentBankAcc.Appearance.Options.UseForeColor = true;
            this.BtnInvestmentBankAcc.Appearance.Options.UseTextOptions = true;
            this.BtnInvestmentBankAcc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInvestmentBankAcc.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnInvestmentBankAcc.Image = ((System.Drawing.Image)(resources.GetObject("BtnInvestmentBankAcc.Image")));
            this.BtnInvestmentBankAcc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnInvestmentBankAcc.Location = new System.Drawing.Point(387, 87);
            this.BtnInvestmentBankAcc.Name = "BtnInvestmentBankAcc";
            this.BtnInvestmentBankAcc.Size = new System.Drawing.Size(24, 21);
            this.BtnInvestmentBankAcc.TabIndex = 21;
            this.BtnInvestmentBankAcc.ToolTip = "Show WO Information";
            this.BtnInvestmentBankAcc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnInvestmentBankAcc.ToolTipTitle = "Run System";
            this.BtnInvestmentBankAcc.Click += new System.EventHandler(this.BtnInvestmentBankAcc_Click);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(192, 24);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(193, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(96, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Document Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(192, 3);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(193, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(115, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNoVoucher
            // 
            this.TxtDocNoVoucher.EnterMoveNextControl = true;
            this.TxtDocNoVoucher.Location = new System.Drawing.Point(192, 129);
            this.TxtDocNoVoucher.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNoVoucher.Name = "TxtDocNoVoucher";
            this.TxtDocNoVoucher.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtDocNoVoucher.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNoVoucher.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNoVoucher.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNoVoucher.Properties.MaxLength = 30;
            this.TxtDocNoVoucher.Properties.ReadOnly = true;
            this.TxtDocNoVoucher.Size = new System.Drawing.Size(193, 20);
            this.TxtDocNoVoucher.TabIndex = 25;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(128, 132);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 14);
            this.label15.TabIndex = 24;
            this.label15.Text = "Voucher#";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl2.Controls.Add(this.TpExpensesOtherDetail);
            this.tabControl2.Controls.Add(this.TpApproval);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl2.Location = new System.Drawing.Point(0, 453);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(797, 172);
            this.tabControl2.TabIndex = 26;
            // 
            // TpExpensesOtherDetail
            // 
            this.TpExpensesOtherDetail.Controls.Add(this.panel3);
            this.TpExpensesOtherDetail.Controls.Add(this.panel9);
            this.TpExpensesOtherDetail.Location = new System.Drawing.Point(4, 26);
            this.TpExpensesOtherDetail.Name = "TpExpensesOtherDetail";
            this.TpExpensesOtherDetail.Padding = new System.Windows.Forms.Padding(3);
            this.TpExpensesOtherDetail.Size = new System.Drawing.Size(789, 142);
            this.TpExpensesOtherDetail.TabIndex = 0;
            this.TpExpensesOtherDetail.Text = "Expenses & Other Detail";
            this.TpExpensesOtherDetail.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.DteExpensesDocDt);
            this.panel3.Controls.Add(this.Grd1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 29);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(783, 110);
            this.panel3.TabIndex = 146;
            // 
            // DteExpensesDocDt
            // 
            this.DteExpensesDocDt.EditValue = null;
            this.DteExpensesDocDt.EnterMoveNextControl = true;
            this.DteExpensesDocDt.Location = new System.Drawing.Point(107, 24);
            this.DteExpensesDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteExpensesDocDt.Name = "DteExpensesDocDt";
            this.DteExpensesDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpensesDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteExpensesDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpensesDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteExpensesDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteExpensesDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteExpensesDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpensesDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteExpensesDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpensesDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteExpensesDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteExpensesDocDt.Size = new System.Drawing.Size(125, 20);
            this.DteExpensesDocDt.TabIndex = 144;
            this.DteExpensesDocDt.Visible = false;
            this.DteExpensesDocDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteExpensesDocDt_KeyDown);
            this.DteExpensesDocDt.Leave += new System.EventHandler(this.DteExpensesDocDt_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(783, 110);
            this.Grd1.TabIndex = 143;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.BtnRefreshData);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(783, 26);
            this.panel9.TabIndex = 145;
            // 
            // BtnRefreshData
            // 
            this.BtnRefreshData.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRefreshData.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefreshData.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefreshData.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnRefreshData.Appearance.Options.UseBackColor = true;
            this.BtnRefreshData.Appearance.Options.UseFont = true;
            this.BtnRefreshData.Appearance.Options.UseForeColor = true;
            this.BtnRefreshData.Appearance.Options.UseTextOptions = true;
            this.BtnRefreshData.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRefreshData.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRefreshData.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnRefreshData.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRefreshData.Location = new System.Drawing.Point(0, 0);
            this.BtnRefreshData.Name = "BtnRefreshData";
            this.BtnRefreshData.Size = new System.Drawing.Size(87, 26);
            this.BtnRefreshData.TabIndex = 13;
            this.BtnRefreshData.Text = "Refresh Data";
            this.BtnRefreshData.ToolTip = "Refresh Data";
            this.BtnRefreshData.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRefreshData.ToolTipTitle = "Run System";
            this.BtnRefreshData.Click += new System.EventHandler(this.BtnRefreshData_Click);
            // 
            // TpApproval
            // 
            this.TpApproval.Controls.Add(this.Grd2);
            this.TpApproval.Location = new System.Drawing.Point(4, 26);
            this.TpApproval.Name = "TpApproval";
            this.TpApproval.Padding = new System.Windows.Forms.Padding(3);
            this.TpApproval.Size = new System.Drawing.Size(764, 142);
            this.TpApproval.TabIndex = 1;
            this.TpApproval.Text = "Approval Info";
            this.TpApproval.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(3, 3);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(758, 136);
            this.Grd2.TabIndex = 144;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(340, 154);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(19, 14);
            this.label30.TabIndex = 74;
            this.label30.Text = "%";
            // 
            // FrmAcquisitionDebtSecurities
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 625);
            this.Name = "FrmAcquisitionDebtSecurities";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.TpInvestmentDetails.ResumeLayout(false);
            this.TpInvestmentDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestFrequency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAnnualDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCouponInterestRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcquisitionPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtInvestment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTradeDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTradeDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAcquisition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSettlementDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSettlementDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalExpenses.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAccruedInterest.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFinancialInstitution.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentBankAcc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoVoucher.Properties)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.TpExpensesOtherDetail.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteExpensesDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpensesDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel9.ResumeLayout(false);
            this.TpApproval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpInvestmentDetails;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueFinancialInstitution;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentBankAcc;
        private System.Windows.Forms.Label LblInvestBankAcc;
        public DevExpress.XtraEditors.SimpleButton BtnInvestmentBankAcc;
        internal DevExpress.XtraEditors.DateEdit DteSettlementDt;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteTradeDt;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtDocNoVoucher;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        internal DevExpress.XtraEditors.TextEdit TxtTotalAcquisition;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtTotalExpenses;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtAccruedInterest;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCost;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtCurRate;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCode;
        private System.Windows.Forms.Label label16;
        public DevExpress.XtraEditors.SimpleButton BtnInvestmentCode;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.DateEdit DteListingDt;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtInterestFrequency;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtAnnualDays;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtCouponInterestRate;
        internal DevExpress.XtraEditors.TextEdit TxtAcquisitionPrice;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtCtInvestment;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentName;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.SimpleButton BtnInvestmentName;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.DateEdit DteNextCouponDt;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.DateEdit DteLastCouponDt;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.DateEdit DteMaturityDt;
        private DevExpress.XtraEditors.LookUpEdit LueInvestmentType;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage TpExpensesOtherDetail;
        private System.Windows.Forms.TabPage TpApproval;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Panel panel9;
        public DevExpress.XtraEditors.SimpleButton BtnRefreshData;
        private System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.DateEdit DteExpensesDocDt;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
    }
}