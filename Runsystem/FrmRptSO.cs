﻿#region Update
/*
    06/04/2021 [IBL/KSM] : menarik transaksi Sales Contract. Menambah kolom local document (SC)
    21/04/2021 [IBL/KSM] : menarik SC berdasarkan parameter IsSalesContractEnabled
    16/11/2021 [RIS/IOK] : Menambah kolom item actual name dengan menggunakan param IsSO2UseActualItem
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSO : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsSalesContratcEnabled = false,
            mIsSO2UseActualItem = false;


        #endregion

        #region Constructor

        public FrmRptSO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( "); 
            SQL.AppendLine("    Select T.DocNo, T.DocDt, T.DNo, T.ItCode, T2.ItName, T.QtySales, T2.SalesuomCode, ");
            SQL.AppendLine("    T.QtyProdOrder, T2.PlanningUomCode, (T.QtySales-T.QtyProdOrder) As Outstanding, T.ContainerGroup, T.LocalDocNo, T3.ItName As ItName2 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.DocNo, T1.DocDt, T2.DNo, T4.ItCode, T2.Qty As QtySales, 0.00 As QtyProdOrder, ContainerGroup, Null As LocalDocNo, ");
            if (mIsSO2UseActualItem) 
            {
                SQL.AppendLine("ItCode2 ");
            }
            else
            {
                SQL.AppendLine("Null As ItCode2 ");
            }
            SQL.AppendLine("        From TblSOHdr T1 ");
            SQL.AppendLine("        Inner Join TblSODtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Left Join TblCtQtDtl T3 On T1.CtQtDocNo=T3.DocNo And T2.CtQtDNo=T3.DNo ");
            SQL.AppendLine("        Left Join TblItemPriceDtl T4 On T3.ItemPriceDocNo=T4.DocNo And T3.ItemPriceDNo=T4.DNo ");
            SQL.AppendLine("        Left Join TblItemPriceHdr T4A On T3.ItemPriceDocNo=T4A.DocNo ");
            SQL.AppendLine("        Where T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        select A.SODOcNo As DocNo, B.DocDt, A.SODno As DNo, A.ItCode, C.Qty As QtySales, SUM(ifnull(A.Qty, 0)) As QtyProdOrder, Null As ContainerGroup, Null As LocalDocNo, ");
            if (mIsSO2UseActualItem)
            {
                SQL.AppendLine("C.ItCode2 ");
            }
            else
            {
                SQL.AppendLine("Null As ItCode2 ");
            }
            SQL.AppendLine("        From TblProductionOrderhdr A ");
            SQL.AppendLine("        Inner Join TblSOHdr B On A.SODocNo = B.DocNo ");
            SQL.AppendLine("        And B.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Inner Join TblSODtl C On A.SODocNo = C.DocNo And A.SoDNo = C.Dno And C.ProcessInd3 <> 'F' ");
            SQL.AppendLine("        Where A.DocNo In ");
            SQL.AppendLine("        ( ");
	        SQL.AppendLine("            Select B.ProductionOrderDocNo ");
	        SQL.AppendLine("            From TblPPHdr A ");
	        SQL.AppendLine("            Inner Join TblPPdtl B On A.DocNo = B.DocNo ");
	        SQL.AppendLine("            Where A.CancelInd = 'N' ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.DocType = '1' and A.CancelInd = 'N' ");
            SQL.AppendLine("        Group By A.SODOcNo, B.DocDt, A.SODno, A.ItCode, C.Qty ");
            if (mIsSalesContratcEnabled)
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select A.DocNo, A.DocDt, C.DNo, C.ItCode, C.Qty As QtySales, 0.00 As QtyProdOrder, Null As ContainerGroup, A.LocalDocNo, Null As ItCode2  ");
                SQL.AppendLine("        From TblSalesContract A ");
                SQL.AppendLine("        Inner Join TblSalesMemoHdr B On A.SalesMemoDocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblSalesMemoDtl C On B.DocNo = C.DocNo ");
                SQL.AppendLine("        Where A.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And B.CancelInd = 'N' ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Inner Join TblItem T2 On T.ItCode = T2.ItCode ");
            SQL.AppendLine("    Left Join TblItem T3 On T.ItCode2 = T3.ItCode ");
            SQL.AppendLine("    Where T.QtySales-T.QtyProdOrder <> 0.00 ");
            SQL.AppendLine(") X ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Local Document#",
                        "",
                        "Date",
                        "Item's"+Environment.NewLine+"Code",
                        
                        //6-10
                        "",
                        "Item's Name",
                        "Item's Actual Name",
                        "Sales Order"+Environment.NewLine+"Quantity",
                        "UoM"+Environment.NewLine+"Sales",
                        
                        
                        //11-14
                        "Production Order"+Environment.NewLine+"Quantity",
                        "UoM"+Environment.NewLine+"Planning",
                        "Outstanding",
                        "Container Group"
                    },
                    new int[] 
                    {
                        //0
                        30,

                        //1-5
                        180, 180, 20, 100, 80,
                        
                        //6-10
                        20, 200, 200, 100, 80, 

                        //11-13
                        100, 80, 100, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6 }, false);
            if (!mIsSO2UseActualItem)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 8 }, false);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 1=1 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order by X.DocDt, X.DocNo; ",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "LocalDocNo", "DocDt", "ItCode", "ItName", "ItName2", 
                            
                            //6-10
                            "QtySales", "SalesUomCode", "QtyProdOrder", "PlanningUomCode", "Outstanding", 

                            //11
                            "ContainerGroup"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        }, true, false, false, false
                    );
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }


        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 11, 13 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsSalesContratcEnabled = Sm.GetParameterBoo("IsSalesContractEnabled");
            mIsSO2UseActualItem = Sm.GetParameterBoo("IsSO2UseActualItem");
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
