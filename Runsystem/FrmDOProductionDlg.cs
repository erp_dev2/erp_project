﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOProductionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOProduction mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOProductionDlg(FrmDOProduction FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
                //ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

           SQL.AppendLine("Select Y.DocNo, Y.DocDt, Y.StatusDesc, Y.ProdStartDt, Y.ProdEndDt, Y.DocValue, Y.ItCode, Y.ItName, Y.Qty, Y.Uom, Y.ProductionRoutingDocNo, Y.DocType From ( ");
           SQL.AppendLine("Select Distinct A.DocNo, A.DocDt, ");
           SQL.AppendLine("Case A.Status When 'P' Then 'Planned' When 'R' Then 'Released' When 'C' Then 'Cancelled' End As StatusDesc, ");
           SQL.AppendLine("A.ProdStartDt, A.ProdEndDt,  ");
           SQL.AppendLine("Case When A.DocType = '1' then A.SODOcNo ");
           SQL.AppendLine("When A.DocType = '2' then A.MakeToStockDocNo ");
           SQL.AppendLine("End As DocValue, ");
           SQL.AppendLine("Case When A.DocType = '1' then C.ItCode ");
           SQL.AppendLine("When A.DocType = '2' then D.ItCode ");
           SQL.AppendLine("End As ItCode, ");
           SQL.AppendLine("Case When A.DocType = '1' then C.ItName ");
           SQL.AppendLine("When A.DocType = '2' then D.ItName ");
           SQL.AppendLine("End As ItName, A.Qty, ");
           SQL.AppendLine("Case When A.DocType = '1' then C.PlanningUomCode ");
           SQL.AppendLine("When A.DocType = '2' then D.PlanningUomCode ");
           SQL.AppendLine("End As Uom, ");
           SQL.AppendLine("A.ProductionRoutingDocNo, A.DocType  ");
           SQL.AppendLine("From TblProductionOrderHdr A  ");
           SQL.AppendLine("Inner Join TblProductionOrderDtl B On A.DocNo=B.DocNo ");
           SQL.AppendLine("Left Join (   ");
           SQL.AppendLine("    Select A.DocNo, A.Dno, A.ItCode, B.Itname, B.PlanningUomCode  ");
           SQL.AppendLine("    From TblSODtl A  ");
           SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode) C On A.SODocNo = C.DocNo And A.SODno = C.Dno  ");
           SQL.AppendLine("Left Join (  ");
           SQL.AppendLine("    Select A.DocNo, A.Dno, A.ItCode, B.Itname, B.PlanningUomCode  ");
           SQL.AppendLine("    From TblMakeToStockDtl A  ");
           SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode) D On A.MakeToStockDocNo = D.DocNo And A.MakeToStockDno = D.Dno  ");
           SQL.AppendLine("Inner Join TblProductionRoutingDtl E On A.ProductionRoutingDocNo=E.DocNo And B.ProductionRoutingDNo=E.DNo   ");
           SQL.AppendLine("Inner Join TblWorkCenterHdr F On E.WorkCenterDocNo=F.DocNo   ");
           SQL.AppendLine("Inner Join TblBomHdr G On B.BomDocNo=G.DocNo Where A.Status <> 'C' ");
           SQL.AppendLine(")Y ");

           mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Doc Type",
                        "Document", 
                        "",
                        "Date",
                        "Status", 
                        
                        //6-10
                        "Production Start", 
                        "Production End",
                        "Document Value",
                        "",
                        "Item Code",

                        //11-15
                        "Item Name",
                        "",
                        "Quantity",
                        "Uom (Planning)",
                        "Production Routing",
                        //16
                        "",
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3, 9, 12, 16 });
            Sm.GrdFormatDate(Grd1, new int[] { 4, 6, 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 10, 11, 13, 14, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 10 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "Y.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "Y.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[]{"Y.ItCode", "Y.ItName"} );

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By Y.DocDt, Y.DocNo;",
                        new string[]
                        {
                            //0
                            "DocType", 

                            //1-5
                            "DocNo", "DocDt", "StatusDesc", "ProdStartDt", "ProdEndDt", 
                            
                            //6-10
                            "DocValue", "ItCode", "ItName", "Qty", "UoM",
 
                            //11-15
                            "ProductionRoutingDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    mFrmParent.ClearGrd();
                    int Row = Grd1.CurRow.Index;
                    
                    mFrmParent.TxtPODocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                    mFrmParent.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, Row, 10);
                    mFrmParent.TxtItName.EditValue = Sm.GetGrdStr(Grd1, Row, 11);
                    mFrmParent.TxtQty.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 13), 0);
                    mFrmParent.TxtPlanningUomCode.EditValue = Sm.GetGrdStr(Grd1, Row, 14);
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionOrder(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog(); 
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "1")
                {
                    e.DoDefault = false;
                    var f = new FrmSO2(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f.ShowDialog();
                }
                else
                {
                    e.DoDefault = false;
                    var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                    var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                    bool IsMTSStandard = Sm.IsDataExist(
                        "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;",
                        Sm.GetGrdStr(Grd1, e.RowIndex, 8));

                    var f = new FrmMakeToStock(mFrmParent.mMenuCode);
                    if (IsMTSStandard)
                    {
                        if (MenuCodeForMakeToStockStandard.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockStandard;
                            f.Tag = MenuCodeForMakeToStockStandard;
                        }
                    }
                    else
                    {
                        if (MenuCodeForMakeToStockMaklon.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockMaklon;
                            f.Tag = MenuCodeForMakeToStockMaklon;
                        }
                    }
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
            
            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmProductionOrder(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "1")
                {
                    var f = new FrmSO2(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f.ShowDialog();
                }
                else
                {
                    var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                    var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                    bool IsMTSStandard = Sm.IsDataExist(
                        "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;",
                        Sm.GetGrdStr(Grd1, e.RowIndex, 8));

                    var f = new FrmMakeToStock(mFrmParent.mMenuCode);
                    if (IsMTSStandard)
                    {
                        if (MenuCodeForMakeToStockStandard.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockStandard;
                            f.Tag = MenuCodeForMakeToStockStandard;
                        }
                    }
                    else
                    {
                        if (MenuCodeForMakeToStockMaklon.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockMaklon;
                            f.Tag = MenuCodeForMakeToStockMaklon;
                        }
                    }
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

       

    }
}
