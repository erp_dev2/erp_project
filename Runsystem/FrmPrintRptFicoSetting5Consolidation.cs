﻿#region Update
/*
    02/11/2022 [WED/PHT] tambah tarik data dari consolidation
    17/02/2023 [WED/PHT] ketika print, kop surat nya ambil dari entity inhutani nya berdasarkan parameter IsUseR1
    16/03/2023 [WED/PHT] group site di Fico Setting berdasarkan parameter IsFicoRptSettingNotUseSite
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPrintRptFicoSetting5Consolidation : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mCOAAssetStartYr = string.Empty,
            mCOAAssetAcNo = string.Empty,
            mAccountingRptStartFrom = string.Empty,
            mPrintRptFicoSettingParenthesesFormat = string.Empty,
            mPrintRptFicoSettingCOAParentheses = string.Empty,
            mFicoSettingCodeForPrintOutAccounting = string.Empty,
            mTypeRuleFormat = string.Empty;

        private bool 
            mIsCOAAssetUseStartYr = false,
            mIsEntityMandatory = false,
            mIsReportingFilterByEntity = false,
            mIsFilterByEnt = false,
            mIsFilterBySite = false,
            mIsPrintRptFicoSettingShowPercentage = false,
            mIsPrintRptFicoSettingYrAscOrder = false,
            mIsPrintRptFicoSettingYr2DisplayMth = false,
            mPrintFicoSettingLastYearBasedOnFilterMonth = false,
            mIsPrintRptFicoSettingUseParentheses = false,
            mIsPrintRptFicoSettingDisplayDate = false,
            //mIsPrintRptFicoSettingUseMultiProfitCenter = false,
            mIsPrintRptFicoSettingUseProfitCenter = false,
            mIsAllProfitCenterSelected = false,
            mIsPrintRptFicoSettingUseEntity = false,
            mIsPrintRptFicoSettingDisplayPrintDate = false,
            mIsPrinOutAccountingUseBalanceInformation = false,
            mIsPrintOutAccountingShowBeginningAndEndingInventory = false,
            mIsUseR1 = false,
            mIsFicoRptSettingNotUseSite = false
            ;
            
        #endregion

        #region Constructor

        public FrmPrintRptFicoSetting5Consolidation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnPrint.Visible = false;
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueYr(LueYr2, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                //Sm.SetLue(LueYr2, CurrentDateTime.Substring(0, 4));
                Sm.SetDte(DteDocDt1, Sm.GetLue(LueYr)+"0101");
                Sm.SetDte(DteDocDt2, Sm.GetLue(LueYr)+"1231");
                Sm.SetDte(DteDocDt3, string.Empty);
                Sm.SetDte(DteDocDt4, string.Empty);
                SetLueFicoFormula(ref LueOptionCode);
                if (!mIsPrinOutAccountingUseBalanceInformation)
                {
                    LblBalance1.Visible = false;
                    LblBalance2.Visible = false;
                    TxtBalance1.Visible = false;
                    TxtBalance2.Visible = false;
                }
                //SetLueEntCode(ref LueEntCode, string.Empty, mIsFilterByEnt ? "Y" : "N");
                //if (mIsPrintRptFicoSettingUseEntity) 
                //    LblEntCode.ForeColor = Color.Red;
                //else
                //{
                //    LblEntCode.Visible = false;
                //    LueEntCode.Visible = false;
                //    LblEntCode.ForeColor = Color.Black;
                //}

                LblMultiProfitCenterCode.Visible = CcbProfitCenterCode.Visible = ChkProfitCenterCode.Visible = mIsPrintRptFicoSettingUseProfitCenter;
                if (mIsPrintRptFicoSettingUseProfitCenter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        //private void SetLueEntCode(ref LookUpEdit Lue, string Code, string IsFilterByEnt)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
        //    SQL.AppendLine("Union All ");
        //    SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T ");
        //    SQL.AppendLine("Where T.ActInd='Y' ");
        //    if (IsFilterByEnt == "Y")
        //    {
        //        SQL.AppendLine("And Exists( ");
        //        SQL.AppendLine("    Select 1 From TblGroupEntity ");
        //        SQL.AppendLine("    Where EntCode=T.EntCode ");
        //        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
        //        SQL.AppendLine("    ) ");
        //    }
        //    SQL.AppendLine("Order By Col2;");
            

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    if (Code.Length > 0)
        //        Sm.CmParam<String>(ref cm, "@Code", Code);
        //    else
        //        Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //    if (Code.Length > 0) Sm.SetLue(Lue, Code);
        //}

        private void GetParameter()
        {
            //mAcNoForCurrentEarning = Sm.GetParameter("AcNoForCurrentEarning");
            //mAcNoForIncome = Sm.GetParameter("AcNoForIncome");
            //mAcNoForCost = Sm.GetParameter("AcNoForCost");
            //mCOAAssetStartYr = Sm.GetParameter("COAAssetStartYr");
            //mCOAAssetAcNo = Sm.GetParameter("COAAssetAcNo");
            //mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
            //mPrintRptFicoSettingCOAParentheses = Sm.GetParameter("PrintRptFicoSettingCOAParentheses");
            //mPrintRptFicoSettingParenthesesFormat = Sm.GetParameter("PrintRptFicoSettingParenthesesFormat");

            //mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            //mIsCOAAssetUseStartYr = Sm.GetParameterBoo("IsCOAAssetUseStartYr");
            //mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
            //mIsFilterByEnt = Sm.GetParameterBoo("IsFilterByEnt");
            //mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            //mIsPrintRptFicoSettingShowPercentage = Sm.GetParameterBoo("IsPrintRptFicoSettingShowPercentage");
            //mIsPrintRptFicoSettingYr2DisplayMth = Sm.GetParameterBoo("IsPrintRptFicoSettingYr2DisplayMth");
            //mIsPrintRptFicoSettingYrAscOrder = Sm.GetParameterBoo("IsPrintRptFicoSettingYrAscOrder");
            //mPrintFicoSettingLastYearBasedOnFilterMonth = Sm.GetParameterBoo("PrintFicoSettingLastYearBasedOnFilterMonth");
            //mIsPrintRptFicoSettingUseParentheses = Sm.GetParameterBoo("IsPrintRptFicoSettingUseParentheses");
            //mIsPrintRptFicoSettingDisplayDate = Sm.GetParameterBoo("IsPrintRptFicoSettingDisplayDate");
            //mIsPrintRptFicoSettingUseProfitCenter = Sm.GetParameterBoo("IsPrintRptFicoSettingUseProfitCenter");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'AcNoForCurrentEarning', 'AcNoForIncome', 'AcNoForCost', 'COAAssetStartYr', 'COAAssetAcNo', ");
            SQL.AppendLine("'AccountingRptStartFrom', 'PrintRptFicoSettingCOAParentheses', 'PrintRptFicoSettingParenthesesFormat', 'IsEntityMandatory', 'IsCOAAssetUseStartYr', ");
            SQL.AppendLine("'IsReportingFilterByEntity', 'IsFilterByEnt', 'IsFilterBySite', 'IsPrintRptFicoSettingShowPercentage', 'IsPrintRptFicoSettingYr2DisplayMth', ");
            SQL.AppendLine("'IsPrintRptFicoSettingYrAscOrder', 'PrintFicoSettingLastYearBasedOnFilterMonth', 'IsPrintRptFicoSettingUseParentheses', 'IsPrintRptFicoSettingDisplayDate', 'IsPrintRptFicoSettingUseProfitCenter', ");
            SQL.AppendLine("'IsPrintRptFicoSettingUseEntity', 'IsPrintRptFicoSettingDisplayPrintDate', 'FicoSettingCodeForPrintOutAccounting', 'TypeRuleFormat', 'IsPrinOutAccountingUseBalanceInformation', 'IsPrintOutAccountingShowBeginningAndEndingInventory', ");
            SQL.AppendLine("'IsUseR1', 'IsFicoRptSettingNotUseSite' ");
            SQL.AppendLine(");");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsCOAAssetUseStartYr": mIsCOAAssetUseStartYr = ParValue == "Y"; break;
                            case "IsReportingFilterByEntity": mIsReportingFilterByEntity = ParValue == "Y"; break;
                            case "IsFilterByEnt": mIsFilterByEnt = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingShowPercentage": mIsPrintRptFicoSettingShowPercentage = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingYr2DisplayMth": mIsPrintRptFicoSettingYr2DisplayMth = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingYrAscOrder": mIsPrintRptFicoSettingYrAscOrder = ParValue == "Y"; break;
                            case "PrintFicoSettingLastYearBasedOnFilterMonth": mPrintFicoSettingLastYearBasedOnFilterMonth = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingUseParentheses": mIsPrintRptFicoSettingUseParentheses = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingDisplayDate": mIsPrintRptFicoSettingDisplayDate = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingUseProfitCenter": mIsPrintRptFicoSettingUseProfitCenter = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingUseEntity": mIsPrintRptFicoSettingUseEntity = ParValue == "Y"; break;
                            case "IsPrintRptFicoSettingDisplayPrintDate": mIsPrintRptFicoSettingDisplayPrintDate = ParValue == "Y"; break;
                            case "IsPrinOutAccountingUseBalanceInformation": mIsPrinOutAccountingUseBalanceInformation = ParValue == "Y"; break;
                            case "IsPrintOutAccountingShowBeginningAndEndingInventory": mIsPrintOutAccountingShowBeginningAndEndingInventory = ParValue == "Y"; break;
                            case "IsUseR1": mIsUseR1 = ParValue == "Y"; break;
                            case "IsFicoRptSettingNotUseSite": mIsFicoRptSettingNotUseSite = ParValue == "Y"; break;

                            //string
                            case "AcNoForCurrentEarning": mAcNoForCurrentEarning = ParValue; break;
                            case "AcNoForIncome": mAcNoForIncome = ParValue; break;
                            case "AcNoForCost": mAcNoForCost = ParValue; break;
                            case "COAAssetStartYr": mCOAAssetStartYr = ParValue; break;
                            case "COAAssetAcNo": mCOAAssetAcNo = ParValue; break;
                            case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                            case "PrintRptFicoSettingCOAParentheses": mPrintRptFicoSettingCOAParentheses = ParValue; break;
                            case "PrintRptFicoSettingParenthesesFormat": mPrintRptFicoSettingParenthesesFormat = ParValue; break;
                            case "FicoSettingCodeForPrintOutAccounting": mFicoSettingCodeForPrintOutAccounting = ParValue; break;
                            case "TypeRuleFormat": mTypeRuleFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            #region Grd1
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;

            
            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Description#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Balance 1";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 250;
            Grd1.Header.Cells[0, 3].Value = "Balance 1";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 250;
            Grd1.Header.Cells[0, 4].Value = "Balance 2";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Cols[5].Width = 250;
            Grd1.Header.Cells[0, 5].Value = "Balance 2";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5 }, 2);
            if (mIsPrintRptFicoSettingUseParentheses) Sm.GrdColInvisible(Grd1, new int[] { 2, 4 });
            else Sm.GrdColInvisible(Grd1, new int[] { 3, 5 });
            #endregion

            #region Grd2
            Grd2.ReadOnly = true;
            Grd2.Header.Rows.Count = 2;
            Grd2.Cols.Count = 4;
            Grd2.FrozenArea.ColCount = 2;


            Grd2.Cols[0].Width = 250;
            Grd2.Header.Cells[0, 0].Value = "Account#";
            Grd2.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd2.Header.Cells[0, 0].SpanRows = 2;

            Grd2.Cols[1].Width = 250;
            Grd2.Header.Cells[0, 1].Value = "Description#";
            Grd2.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd2.Header.Cells[0, 1].SpanRows = 2;

            Grd2.Cols[2].Width = 250;
            Grd2.Header.Cells[0, 2].Value = "Balance#";
            Grd2.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd2.Header.Cells[0, 2].SpanRows = 2;

            Grd2.Cols[3].Width = 250;
            Grd2.Header.Cells[0, 3].Value = "Balance#";
            Grd2.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd2.Header.Cells[0, 3].SpanRows = 2;

            Sm.GrdFormatDec(Grd2, new int[] { 2, 3 }, 2);
            if (mIsPrintRptFicoSettingUseParentheses) Sm.GrdColInvisible(Grd2, new int[] { 2 });
            else Sm.GrdColInvisible(Grd2, new int[] { 3 });
            #endregion
        }

        //private bool IsProfitCenterInvalid()
        //{
        //    if (!mIsPrintRptFicoSettingUseMultiProfitCenter) return false;

        //    if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
        //    {
        //        Sm.StdMsg(mMsgType.Warning, "Multi profit center is empty.");
        //        CcbProfitCenterCode.Focus();
        //        return true;
        //    }

        //    var v = GetCcbProfitCenterCode();
        //    if (v.Contains("Consolidate"))
        //    {
        //        var split = v.Split(',');
        //        var mFlag = false;

        //        if (split.Length > 1)
        //        {
        //            foreach (var x in split)
        //            {
        //                if (x.Length == 0)
        //                {
        //                    mFlag = true;
        //                    break;
        //                }
        //            }

        //            if (!mFlag)
        //            {
        //                Sm.StdMsg(mMsgType.Warning, "if consolidate already choosen, You can't choose another profit center.");
        //                CcbProfitCenterCode.Focus();
        //                return true;
        //            }
        //        }
        //    }
        //    return false;
        //}

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsPrintRptFicoSettingUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked)
                mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || 
                (LblStartDt.ForeColor == Color.Red && Sm.IsDteEmpty(DteDocDt1, "Start Date")) ||
                Sm.IsDteEmpty(DteDocDt2, "End Date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                Sm.IsFilterByDateInvalid(ref DteDocDt3, ref DteDocDt4) ||
                Sm.IsLueEmpty(LueOptionCode, "Type") ||
                IsDateNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var lCOATemp = new List<COA2>();
            var lJournalTemp = new List<Journal2>();
            var lJournalCurrentMonthTemp = new List<Journal3>();
            var lCOAOpeningBalanceTemp = new List<COAOpeningBalance2>();
            var lUsedAcNo = new List<string>();
            string mListAcNo = string.Empty, mUsedAcNo = string.Empty, mUsedProfitCenter = string.Empty;

            mListAcNo = Sm.GetValue("SET SESSION group_concat_max_len = 1000000;Select Group_Concat(Distinct Replace(Replace(AcNo, '+', ','), '-', ',')) From TblRptFicoSettingDtl Where RptFicoCode = @Param And AcNo Is Not Null; ", Sm.GetLue(LueOptionCode));

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtBalance1, TxtBalance2 }, 0);
            var Yr = Sm.GetLue(LueYr);
            var Yr2 = Sm.GetLue(LueYr2);
            var Mth = Sm.GetDte(DteDocDt2).Substring(4,2);
            var Mth2 = Sm.GetDte(DteDocDt4).Length > 0 ? Sm.GetDte(DteDocDt4).Substring(4, 2) : Convert.ToString(8);
            var StartFrom = Sm.GetLue(LueYr);
            var StartFrom2 = Sm.GetLue(LueYr2);
            var StartDt = Sm.GetDte(DteDocDt1);
            var StartDt2 = Sm.GetDte(DteDocDt3);
            var EndDt = Sm.GetDte(DteDocDt2);
            var EndDt2 = Sm.GetDte(DteDocDt4);

            GetAllCOA(ref lCOATemp, mListAcNo,ref lUsedAcNo);
            string UsedAcNo = string.Join(",", lUsedAcNo.ToArray());
            SetProfitCenter();

            GetAllCOAOpeningBalance(ref lCOAOpeningBalanceTemp, mUsedAcNo, StartFrom, StartFrom2, ref lUsedAcNo);
            GetAllJournal(ref lJournalTemp, mUsedAcNo, Yr, Mth, StartFrom, Yr2, Mth2.ToString(), StartFrom2, StartDt, StartDt2, ref lUsedAcNo);
            GetAllJournalCurrentMonth(ref lJournalCurrentMonthTemp, mUsedAcNo, Yr, Mth, Yr2, Mth2.ToString(), EndDt, EndDt2, ref lUsedAcNo);

            try
            {
                #region grid 1
                Sm.ClearGrd(Grd1, true);
                Sm.ClearGrd(Grd2, true);
                var lCOA = new List<COA>();
                
                var lEntityCOA = new List<EntityCOA>();
                var lRptFicoSetting = new List<RptFicoSetting>();
                var lRptFicoSetting2 = new List<RptFicoSetting>();
                var lRptFicoSetting3 = new List<COAFicoSetting>();
                var lRptFicoSetting4 = new List<RptFicoSetting2>();

                
                foreach(var x in lCOATemp)
                {
                    lCOA.Add(new COA()
                    {
                        AcNo = x.AcNo,
                        AcDesc = x.AcDesc,
                        AcType = x.AcType,
                        HasChild = x.HasChild,
                        Level = x.Level,
                        Parent = x.Parent,
                        CurrentMonthBalance = x.CurrentMonthBalance,
                        CurrentMonthCAmt = x.CurrentMonthCAmt,
                        CurrentMonthDAmt = x.CurrentMonthDAmt,
                        MonthToDateBalance = x.MonthToDateBalance,
                        MonthToDateCAmt = x.MonthToDateCAmt,
                        MonthToDateDAmt = x.MonthToDateDAmt,
                        MonthToDateBeginningCAmt = 0m,
                        MonthToDateBeginningDAmt = 0m,
                        MonthToDateEndingCAmt = 0m,
                        MonthToDateEndingDAmt = 0m,
                        OpeningBalanceCAmt = x.OpeningBalanceCAmt,
                        OpeningBalanceDAmt = x.OpeningBalanceDAmt,
                        YearToDateBalance = x.YearToDateBalance,
                        YearToDateCAmt = x.YearToDateCAmt,
                        YearToDateDAmt = x.YearToDateDAmt,
                        YearToDateBeginningDAmt = 0m,
                        YearToDateBeginningCAmt = 0m,
                        YearToDateEndingDAmt = 0m,
                        YearToDateEndingCAmt = 0m,
                        YearToDateBeginningBalance = 0m,
                        YearToDateEndingBalance = 0m
                    });
                }

                if (lCOA.Count > 0)
                {
                    Process2(ref lCOAOpeningBalanceTemp, ref lCOA, "New", UsedAcNo);
                    Process3(ref lJournalTemp, ref lCOA, "New", UsedAcNo);
                    Process4(ref lJournalCurrentMonthTemp, ref lCOA, "New", UsedAcNo);
                    Process5(ref lCOA);
                    Process6(ref lCOA);
                    Process9(ref lCOA);

                    ProcessFicoCOA(ref lRptFicoSetting);
                    if (lRptFicoSetting.Count > 0)
                    {
                        ProcessFicoCOA2(ref lRptFicoSetting, ref lRptFicoSetting2);
                    }

                    ProcessFicoCOA3(ref lRptFicoSetting3);

                    if (lCOA.Count > 0)
                    {
                        Grd1.BeginUpdate();

                        foreach (var x in lRptFicoSetting2.OrderBy(o => o.AcNo))
                        {
                            foreach (var y in lCOA.OrderBy(o => o.AcNo).Where(a => a.AcNo == x.AcNo))
                            {
                                decimal mBalance = y.YearToDateBalance;

                                if (x.BeginningInvInd || x.EndingInvInd)
                                {
                                    if (x.BeginningInvInd) mBalance = y.YearToDateBeginningBalance;
                                    if (x.EndingInvInd) mBalance = y.YearToDateEndingBalance;
                                }

                                lRptFicoSetting4.Add(new RptFicoSetting2()
                                {
                                    FicoCode = x.FicoCode,
                                    SettingCode = x.SettingCode,
                                    SettingDesc = x.SettingDesc,
                                    Sequence = x.Sequence,
                                    Formula = x.Formula,
                                    AcNo = x.AcNo,
                                    Balance = mBalance
                                });
                                break;
                            }
                        }

                        ProcessFicoCOA4(ref lRptFicoSetting3, ref lRptFicoSetting4, Grd1);
                        Grd1.EndUpdate();
                    }

                    lCOA.Clear();
                    lEntityCOA.Clear();
                }
                #endregion

                #region grid 2
                var lCOAGrd2 = new List<COA>();
                var lEntityCOAGrd2 = new List<EntityCOA>();
                var lRptFicoSettingGrd2 = new List<RptFicoSetting>();
                var lRptFicoSetting2Grd2 = new List<RptFicoSetting>();
                var lRptFicoSetting3Grd2 = new List<COAFicoSetting>();
                var lRptFicoSetting4Grd2 = new List<RptFicoSetting2>();
                string mAcNo = string.Empty;

                foreach (var x in lCOATemp)
                {
                    lCOAGrd2.Add(new COA()
                    {
                        AcNo = x.AcNo,
                        AcDesc = x.AcDesc,
                        AcType = x.AcType,
                        HasChild = x.HasChild,
                        Level = x.Level,
                        Parent = x.Parent,
                        CurrentMonthBalance = x.CurrentMonthBalance,
                        CurrentMonthCAmt = x.CurrentMonthCAmt,
                        CurrentMonthDAmt = x.CurrentMonthDAmt,
                        MonthToDateBalance = x.MonthToDateBalance,
                        MonthToDateCAmt = x.MonthToDateCAmt,
                        MonthToDateDAmt = x.MonthToDateDAmt,
                        MonthToDateBeginningCAmt = 0m,
                        MonthToDateBeginningDAmt = 0m,
                        MonthToDateEndingCAmt = 0m,
                        MonthToDateEndingDAmt = 0m,
                        OpeningBalanceCAmt = x.OpeningBalanceCAmt,
                        OpeningBalanceDAmt = x.OpeningBalanceDAmt,
                        YearToDateBalance = x.YearToDateBalance,
                        YearToDateCAmt = x.YearToDateCAmt,
                        YearToDateDAmt = x.YearToDateDAmt,
                        YearToDateBeginningDAmt = 0m,
                        YearToDateBeginningCAmt = 0m,
                        YearToDateEndingDAmt = 0m,
                        YearToDateEndingCAmt = 0m,
                        YearToDateBeginningBalance = 0m,
                        YearToDateEndingBalance = 0m
                    });
                }
                if (lCOAGrd2.Count > 0)
                {
                    Process2(ref lCOAOpeningBalanceTemp, ref lCOAGrd2, "Old", UsedAcNo);
                    Process3(ref lJournalTemp, ref lCOAGrd2, "Old", UsedAcNo);
                    Process4(ref lJournalCurrentMonthTemp, ref lCOAGrd2, "Old", UsedAcNo);
                    Process5(ref lCOAGrd2);
                    Process6(ref lCOAGrd2);
                    Process9(ref lCOAGrd2);

                    ProcessFicoCOA(ref lRptFicoSettingGrd2);
                    if (lRptFicoSettingGrd2.Count > 0)
                    {
                        ProcessFicoCOA2(ref lRptFicoSettingGrd2, ref lRptFicoSetting2Grd2);
                    }

                    ProcessFicoCOA3(ref lRptFicoSetting3Grd2);

                    if (lCOAGrd2.Count > 0)
                    {
                        Grd2.BeginUpdate();

                        foreach (var i in lRptFicoSetting2Grd2.OrderBy(o => o.AcNo))
                        {
                            foreach (var j in lCOAGrd2.OrderBy(o => o.AcNo).Where(x => x.AcNo == i.AcNo))
                            {
                                decimal mBalance = j.YearToDateBalance;

                                if (i.BeginningInvInd || i.EndingInvInd)
                                {
                                    if (i.BeginningInvInd) mBalance = j.YearToDateBeginningBalance;
                                    if (i.EndingInvInd) mBalance = j.YearToDateEndingBalance;
                                }

                                lRptFicoSetting4Grd2.Add(new RptFicoSetting2()
                                {
                                    FicoCode = i.FicoCode,
                                    SettingCode = i.SettingCode,
                                    SettingDesc = i.SettingDesc,
                                    Sequence = i.Sequence,
                                    Formula = i.Formula,
                                    AcNo = i.AcNo,
                                    Balance = mBalance
                                });
                                break;
                            }
                        }

                        ProcessFicoCOA4(ref lRptFicoSetting3Grd2, ref lRptFicoSetting4Grd2, Grd2);
                        Grd2.EndUpdate();
                    }

                    lCOAGrd2.Clear();
                    lEntityCOAGrd2.Clear();
                }
                #endregion

                CopyBalance();
                GetTotalBalance();
                lCOATemp.Clear();
                lJournalCurrentMonthTemp.Clear();
                lJournalTemp.Clear();
                lCOAOpeningBalanceTemp.Clear();
                lUsedAcNo.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        //private void SetLueEntCode(ref LookUpEdit Lue)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
        //    SQL.AppendLine("Union All ");
        //    if (mIsReportingFilterByEntity)
        //    {
        //        SQL.AppendLine("    Select A.EntCode, B.EntName  From TblGroupEntity A ");
        //        SQL.AppendLine("    Inner Join TblEntity B On A.EntCode = B.EntCode ");
        //        SQL.AppendLine("    Where A.GrpCode In ( ");
        //        SQL.AppendLine("        Select GrpCode From TblUser ");
        //        SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
        //        SQL.AppendLine("    ) ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T; ");
        //    }

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}

        //private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select Col From (");
        //    //SQL.AppendLine("    Select 'Consolidate' Col, '01' Col2  ");
        //    //SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("    Select ProfitCenterName AS Col ");
        //    SQL.AppendLine("    From TblProfitCenter ");
        //    SQL.AppendLine("    Where ProfitCenterCode In ( ");
        //    SQL.AppendLine("        Select ProfitCenterCode ");
        //    SQL.AppendLine("        From TblCostCenter ");
        //    SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
        //    SQL.AppendLine("        And ActInd='Y' ");
        //    SQL.AppendLine("        And CCCode In ( ");
        //    SQL.AppendLine("            Select Distinct CCCode From TblGroupCostCenter T ");
        //    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
        //    SQL.AppendLine("        ) ");
        //    SQL.AppendLine("    ) ");
        //    SQL.AppendLine(") Tbl Order By Col; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    Sm.SetCcb(ref Ccb, cm);
        //}

        //private string GetCcbProfitCenterCode()
        //{
        //    var Value = Sm.GetCcb(CcbProfitCenterCode);
        //    if (Value.Length == 0) return string.Empty;
        //    return GetProfitCenterCode(Value).Replace(", ", ",");
        //}

        //private string ProcessCcbProfitCenterCode(string Value)
        //{
        //    if (Value.Length == 0) return string.Empty;
        //    return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        //}

        //private string GetProfitCenterCode(string Value)
        //{
        //    if (Value.Length == 0) return string.Empty;

        //    string Parent = string.Empty;
        //    string ParentTemp = string.Empty;
        //    string ProfitCenterCode = string.Empty;
        //    var SQLPr = new StringBuilder();
        //    var SQLCh = new StringBuilder();

        //    SQLPr.AppendLine("Select Group_Concat(T.Code Separator ', ') As Code ");
        //    SQLPr.AppendLine("From ");
        //    SQLPr.AppendLine("( ");
        //    SQLPr.AppendLine("    Select ProfitCenterCode As Code ");
        //    SQLPr.AppendLine("    From TblProfitCenter ");
        //    SQLPr.AppendLine("    Where Find_In_Set(ProfitCenterName, @Param) ");
        //    SQLPr.AppendLine(") T ");
        //    SQLPr.AppendLine("; ");

        //    SQLCh.AppendLine("Select Group_Concat(ProfitCenterCode Separator ', ') As Code ");
        //    SQLCh.AppendLine("From TblProfitCenter ");
        //    SQLCh.AppendLine("Where Parent Is Not Null ");
        //    SQLCh.AppendLine("And Find_In_Set(Parent, @Param) ");
        //    SQLCh.AppendLine("; ");

        //    Parent = Sm.GetValue(SQLPr.ToString(), Value.Replace(", ", ","));
        //    if (Parent.Length > 0)
        //    {
        //        ProfitCenterCode = Parent;
        //        ParentTemp = Parent;
        //    }

        //    while (ParentTemp.Length > 0)
        //    {
        //        Parent = Sm.GetValue(SQLCh.ToString(), ParentTemp.Replace(", ", ","));
        //        if (Parent.Length > 0)
        //        {
        //            if (ProfitCenterCode.Length > 0) ProfitCenterCode += ", ";
        //            ProfitCenterCode += Parent;
        //        }
        //        ParentTemp = Parent;
        //    }

        //    return ProfitCenterCode;

        //    //return
        //    //    Sm.GetValue(
        //    //        "Select Group_Concat(T.Code Separator ', ') As Code " +
        //    //        "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
        //    //        Value.Replace(", ", ","));

        //    //var IsIncludeConsolidate = Value.Contains("Consolidate");
        //    //return string.Concat(
        //    //    (IsIncludeConsolidate?"Consolidate, ":string.Empty), 
        //    //    Sm.GetValue(
        //    //        "Select Group_Concat(T.Code Separator ', ') As Code " +
        //    //        "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ", 
        //    //        Value.Replace(", ", ",")));
        //}

        private void SetLueFicoFormula(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.RptFicoCode As Col1, T.RptFicoName As Col2 ");
            SQL.AppendLine("From TblRptFicoSettingHdr T ");
            if (!mIsFicoRptSettingNotUseSite && mIsFilterBySite)
            {
                SQL.AppendLine("Where (T.SiteCode Is Null Or ");
                SQL.AppendLine("(T.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetAllCOA(ref List<COA2> l, string ListAcNo,ref List<string> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;
            string[] AcNo = ListAcNo.Split(',');

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                //if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                //{
                //    SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo=B.AcNo ");
                //    SQL.AppendLine("And B.EntCode=@EntCode ");

                //    if (mIsReportingFilterByEntity)
                //    {
                //        SQL.AppendLine("And B.EntCode Is Not Null ");
                //        SQL.AppendLine("And Exists( ");
                //        SQL.AppendLine("    Select 1 From TblGroupEntity ");
                //        SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                //        SQL.AppendLine("    And GrpCode In ( ");
                //        SQL.AppendLine("        Select GrpCode From TblUser ");
                //        SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                //        SQL.AppendLine("    ) ");
                //        SQL.AppendLine(") ");
                //    }
                //}
                SQL.AppendLine("Where A.ActInd='Y' ");
                SQL.AppendLine("And ( ");
                for (int x = 0; x < AcNo.Count(); ++x)
                {
                    SQL.AppendLine("(A.AcNo Like '" + AcNo[x] + "%') ");
                    if (x != AcNo.Count() - 1)
                        SQL.AppendLine(" Or ");
                }
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                //Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode).Length > 0 ? Sm.GetLue(LueEntCode) == "Consolidate" ? "" : Sm.GetLue(LueEntCode) : Sm.GetLue(LueEntCode));

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA2()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });

                        l2.Add(Sm.DrStr(dr, c[0]));
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournal(ref List<Journal2> l, string ListAcNo,
            string Yr, string Mth, string StartFrom,
            string Yr2, string Mth2, string StartFrom2,
            string StartDt, string StartDt2, ref List<string> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            string Filter2 = string.Empty;
            int i = 0;
            int j = 0;

            foreach (var x in mlProfitCenter.Distinct())
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "ProfitCenterCode=@ProfitCenter_" + i.ToString();
                Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                i++;
            }

            //foreach (var y in l2.Distinct())
            //{
            //    if (Filter2.Length > 0) Filter2 += " Or ";
            //    Filter2 += "B.AcNo=@AcNo_" + j.ToString();
            //    Sm.CmParam<String>(ref cm, "@AcNo_" + j.ToString(), y);
            //    j++;
            //}

            if (mIsPrintOutAccountingShowBeginningAndEndingInventory)
            {
                // prep all 3 use cases (beginning checked, ending checked, and both unchecked
                #region Beginning & Ending Inventory

                SQL.AppendLine("Select T.DocType, T.AcNo, Sum(T.DAmt) DAmt, Sum(T.CAmt) CAmt, ");
                SQL.AppendLine("Sum(T.BeginningDAmt) BeginningDAmt, Sum(T.BeginningCAmt) BeginningCAmt, ");
                SQL.AppendLine("Sum(T.EndingDAmt) EndingDAmt, Sum(T.EndingCAmt) EndingCAmt ");
                SQL.AppendLine("From ( ");

                #region Periode 1

                // both beginning and ending unchecked
                SQL.AppendLine("    Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt, 0.00 BeginningDAmt, 0.00 BeginningCAmt, 0.00 EndingDAmt, 0.00 EndingCAmt ");
                SQL.AppendLine("    From TblJournalConsolidationHdr A ");
                SQL.AppendLine("    Inner Join TblJournalConsolidationDtl B ON A.DocNo=B.DocNo ");
                if (Filter2.Length > 0)
                    SQL.AppendLine("    And (" + Filter2 + ") ");
                SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
                SQL.AppendLine("    Where 1=1 ");
                SQL.AppendLine("        And A.DocDt >= @StartFrom ");
                SQL.AppendLine("        And Left(A.DocDt, 6) < @YrMth ");

                #region Profit Center Filter
                if (mIsPrintRptFicoSettingUseProfitCenter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("            ) ");
                        if (Filter.Length > 0)
                            SQL.AppendLine("    And (" + Filter + ") ");
                        SQL.AppendLine("        ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                            SQL.AppendLine("    ) ");
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In (");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        )))) ");
                        }
                    }
                }
                #endregion

                if (Sm.Right(Sm.Left(StartDt, 8), 4) != "0101") //if start date isn't equal to 01 Jan
                {
                    SQL.AppendLine("    Union All ");

                    // beginning checked
                    SQL.AppendLine("    Select 'New' As DocType, B.AcNo, 0.00 As DAmt, 0.00 As CAmt, B.DAmt BeginningDAmt, B.CAmt BeginningCAmt, 0.00 EndingDAmt, 0.00 EndingCAmt ");
                    SQL.AppendLine("    From TblJournalConsolidationHdr A ");
                    SQL.AppendLine("    Inner Join TblJournalConsolidationDtl B ON A.DocNo=B.DocNo ");
                    if (Filter2.Length > 0)
                        SQL.AppendLine("    And (" + Filter2 + ") ");
                    SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
                    SQL.AppendLine("    Where 1=1 ");
                    SQL.AppendLine("        And A.DocDt Between Concat(@Yr, '0101') And Date_Format(Date_Sub(@StartDt, Interval 1 Day), '%Y%m%d') ");

                    #region Profit Center Filter
                    if (mIsPrintRptFicoSettingUseProfitCenter)
                    {
                        if (!mIsAllProfitCenterSelected)
                        {
                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In ( ");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("            ) ");
                            if (Filter.Length > 0)
                                SQL.AppendLine("    And (" + Filter + ") ");
                            SQL.AppendLine("        ) ");
                        }
                        else
                        {
                            if (ChkProfitCenterCode.Checked)
                            {
                                SQL.AppendLine("    And A.CCCode Is Not Null ");
                                SQL.AppendLine("    And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                                SQL.AppendLine("    ) ");
                            }
                            else
                            {
                                SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And ProfitCenterCode In (");
                                SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                SQL.AppendLine("        )))) ");
                            }
                        }
                    }
                    #endregion
                }

                SQL.AppendLine("    Union All ");

                // ending checked
                SQL.AppendLine("    Select 'New' As DocType, B.AcNo, 0.00 As DAmt, 0.00 As CAmt, 0.00 BeginningDAmt, 0.00 BeginningCAmt, B.DAmt EndingDAmt, B.CAmt EndingCAmt ");
                SQL.AppendLine("    From TblJournalConsolidationHdr A ");
                SQL.AppendLine("    Inner Join TblJournalConsolidationDtl B ON A.DocNo=B.DocNo ");
                if (Filter2.Length > 0)
                    SQL.AppendLine("    And (" + Filter2 + ") ");
                SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
                SQL.AppendLine("    Where 1=1 ");
                SQL.AppendLine("        And A.DocDt Between Concat(@Yr, '0101') And @EndDt ");

                #region Profit Center Filter
                if (mIsPrintRptFicoSettingUseProfitCenter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("            ) ");
                        if (Filter.Length > 0)
                            SQL.AppendLine("    And (" + Filter + ") ");
                        SQL.AppendLine("        ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                            SQL.AppendLine("    ) ");
                        }
                        else
                        {
                            SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In (");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        )))) ");
                        }
                    }
                }
                #endregion

                #endregion

                if (StartDt2.Trim().Length > 0)
                {
                    SQL.AppendLine("    Union All ");

                    #region Periode 2

                    // both beginning and ending unchecked
                    SQL.AppendLine("    Select 'Old' As DocType, B.AcNo, B.DAmt, B.CAmt, 0.00 BeginningDAmt, 0.00 BeginningCAmt, 0.00 EndingDAmt, 0.00 EndingCAmt ");
                    SQL.AppendLine("    From TblJournalConsolidationHdr A ");
                    SQL.AppendLine("    Inner Join TblJournalConsolidationDtl B ON A.DocNo=B.DocNo ");
                    if (Filter2.Length > 0)
                        SQL.AppendLine("    And (" + Filter2 + ") ");
                    SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
                    SQL.AppendLine("    Where 1=1 ");
                    SQL.AppendLine("        And A.DocDt >= @StartFrom2 ");
                    SQL.AppendLine("        And Left(A.DocDt, 6) < @YrMth2 ");

                    #region Profit Center Filter
                    if (mIsPrintRptFicoSettingUseProfitCenter)
                    {
                        if (!mIsAllProfitCenterSelected)
                        {
                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In ( ");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("            ) ");
                            if (Filter.Length > 0)
                                SQL.AppendLine("    And (" + Filter + ") ");
                            SQL.AppendLine("        ) ");
                        }
                        else
                        {
                            if (ChkProfitCenterCode.Checked)
                            {
                                SQL.AppendLine("    And A.CCCode Is Not Null ");
                                SQL.AppendLine("    And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                                SQL.AppendLine("    ) ");
                            }
                            else
                            {
                                SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And ProfitCenterCode In (");
                                SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                SQL.AppendLine("        )))) ");
                            }
                        }
                    }
                    #endregion

                    if (Sm.Right(Sm.Left(StartDt2, 8), 4) != "0101") //if start date isn't equal to 01 Jan
                    {
                        SQL.AppendLine("    Union All ");

                        // beginning checked
                        SQL.AppendLine("    Select 'Old' As DocType, B.AcNo, 0.00 As DAmt, 0.00 As CAmt, B.DAmt BeginningDAmt, B.CAmt BeginningCAmt, 0.00 EndingDAmt, 0.00 EndingCAmt ");
                        SQL.AppendLine("    From TblJournalConsolidationHdr A ");
                        SQL.AppendLine("    Inner Join TblJournalConsolidationDtl B ON A.DocNo=B.DocNo ");
                        if (Filter2.Length > 0)
                            SQL.AppendLine("    And (" + Filter2 + ") ");
                        SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
                        SQL.AppendLine("    Where 1=1 ");
                        SQL.AppendLine("        And A.DocDt Between Concat(@Yr2, '0101') And Date_Format(Date_Sub(@StartDt2, Interval 1 Day), '%Y%m%d') ");

                        #region Profit Center Filter
                        if (mIsPrintRptFicoSettingUseProfitCenter)
                        {
                            if (!mIsAllProfitCenterSelected)
                            {
                                SQL.AppendLine("    And A.CCCode Is Not Null ");
                                SQL.AppendLine("    And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And ProfitCenterCode In ( ");
                                SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                SQL.AppendLine("            ) ");
                                if (Filter.Length > 0)
                                    SQL.AppendLine("    And (" + Filter + ") ");
                                SQL.AppendLine("        ) ");
                            }
                            else
                            {
                                if (ChkProfitCenterCode.Checked)
                                {
                                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                                    SQL.AppendLine("    And A.CCCode In ( ");
                                    SQL.AppendLine("        Select Distinct CCCode ");
                                    SQL.AppendLine("        From TblCostCenter ");
                                    SQL.AppendLine("        Where ActInd='Y' ");
                                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                    SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                                    SQL.AppendLine("    ) ");
                                }
                                else
                                {
                                    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                                    SQL.AppendLine("        Select Distinct CCCode ");
                                    SQL.AppendLine("        From TblCostCenter ");
                                    SQL.AppendLine("        Where ActInd='Y' ");
                                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                    SQL.AppendLine("        And ProfitCenterCode In (");
                                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                    SQL.AppendLine("        )))) ");
                                }
                            }
                        }
                        #endregion
                    }

                    SQL.AppendLine("    Union All ");

                    // ending checked
                    SQL.AppendLine("    Select 'Old' As DocType, B.AcNo, 0.00 As DAmt, 0.00 As CAmt, 0.00 BeginningDAmt, 0.00 BeginningCAmt, B.DAmt EndingDAmt, B.CAmt EndingCAmt ");
                    SQL.AppendLine("    From TblJournalConsolidationHdr A ");
                    SQL.AppendLine("    Inner Join TblJournalConsolidationDtl B ON A.DocNo=B.DocNo ");
                    if (Filter2.Length > 0)
                        SQL.AppendLine("    And (" + Filter2 + ") ");
                    SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
                    SQL.AppendLine("    Where 1=1 ");
                    SQL.AppendLine("        And A.DocDt Between Concat(@Yr2, '0101') And @EndDt2 ");

                    #region Profit Center Filter
                    if (mIsPrintRptFicoSettingUseProfitCenter)
                    {
                        if (!mIsAllProfitCenterSelected)
                        {
                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In ( ");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("            ) ");
                            if (Filter.Length > 0)
                                SQL.AppendLine("    And (" + Filter + ") ");
                            SQL.AppendLine("        ) ");
                        }
                        else
                        {
                            if (ChkProfitCenterCode.Checked)
                            {
                                SQL.AppendLine("    And A.CCCode Is Not Null ");
                                SQL.AppendLine("    And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                                SQL.AppendLine("    ) ");
                            }
                            else
                            {
                                SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And ProfitCenterCode In (");
                                SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                SQL.AppendLine("        )))) ");
                            }
                        }
                    }
                    #endregion

                    #endregion

                }

                SQL.AppendLine(") T ");
                SQL.AppendLine("Group By T.DocType, T.AcNo ");
                SQL.AppendLine("Order By T.DocType, T.AcNo; ");

                #endregion
            }
            else
            {
                #region Default

                SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt, ");
                SQL.AppendLine("0.00 BeginningDAmt, 0.00 BeginningCAmt, 0.00 EndingDAmt, 0.00 EndingCAmt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
                SQL.AppendLine("    From TblJournalConsolidationHdr A ");
                SQL.AppendLine("    Inner Join TblJournalConsolidationDtl B ON A.DocNo=B.DocNo ");
                //SQL.AppendLine("    And A.CancelInd = 'N' ");
                SQL.AppendLine("        And Find_In_Set(B.AcNo, @AcNo) ");
                //if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                //{
                //    SQL.AppendLine("    And B.EntCode=@EntCode ");

                //    if (mIsReportingFilterByEntity)
                //    {
                //        SQL.AppendLine("And B.EntCode Is Not Null ");
                //        SQL.AppendLine("And Exists( ");
                //        SQL.AppendLine("    Select 1 From TblGroupEntity ");
                //        SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                //        SQL.AppendLine("    And GrpCode In ( ");
                //        SQL.AppendLine("        Select GrpCode From TblUser ");
                //        SQL.AppendLine("        Where UserCode=@UserCode ");
                //        SQL.AppendLine("    ) ");
                //        SQL.AppendLine(") ");
                //    }
                //}

                //if (mIsPrintRptFicoSettingUseMultiProfitCenter)
                //{
                //    if (ChkProfitCenterCode.Checked)
                //    {
                //        SQL.AppendLine("    And A.CCCode Is Not Null ");
                //        SQL.AppendLine("    And A.CCCode In ( ");
                //        SQL.AppendLine("        Select Distinct CCCode ");
                //        SQL.AppendLine("        From TblCostCenter ");
                //        SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
                //        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                //        SQL.AppendLine("    ) ");
                //    }
                //    else
                //    {
                //        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                //        SQL.AppendLine("        Select Distinct CCCode From TblGroupCostCenter ");
                //        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                //        SQL.AppendLine("        ))) ");
                //    }
                //}

                SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
                SQL.AppendLine("Where 1=1 ");
                SQL.AppendLine("    And A.DocDt >= @StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                if (mIsPrintRptFicoSettingUseProfitCenter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        Filter = string.Empty;
                        i = 0;

                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        ) ");
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter.Length == 0)
                            SQL.AppendLine("    And 1=0 ");
                        else
                            SQL.AppendLine("    And (" + Filter + ") ");
                        SQL.AppendLine("    ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                            SQL.AppendLine("    ) ");
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In (");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        )))) ");
                        }
                    }
                }

                if (StartDt2.Trim().Length > 0)
                {

                    SQL.AppendLine("Union All ");

                    SQL.AppendLine("Select 'Old' As DocType, B.AcNo, B.DAmt, B.CAmt ");
                    SQL.AppendLine("From TblJournalConsolidationHdr A ");
                    SQL.AppendLine("Inner Join TblJournalConsolidationDtl B ON A.DocNo = B.DocNo ");
                    //SQL.AppendLine("    And A.CancelInd = 'N' ");
                    SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
                    //if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                    //{
                    //    SQL.AppendLine("    And B.EntCode=@EntCode ");

                    //    if (mIsReportingFilterByEntity)
                    //    {
                    //        SQL.AppendLine("And B.EntCode Is Not Null ");
                    //        SQL.AppendLine("And Exists( ");
                    //        SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    //        SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    //        SQL.AppendLine("    And GrpCode In ( ");
                    //        SQL.AppendLine("        Select GrpCode From TblUser ");
                    //        SQL.AppendLine("        Where UserCode=@UserCode ");
                    //        SQL.AppendLine("    ) ");
                    //        SQL.AppendLine(") ");
                    //    }
                    //}

                    //if (mIsPrintRptFicoSettingUseMultiProfitCenter)
                    //{
                    //    if (ChkProfitCenterCode.Checked)
                    //    {
                    //        SQL.AppendLine("    And A.CCCode Is Not Null ");
                    //        SQL.AppendLine("    And A.CCCode In ( ");
                    //        SQL.AppendLine("        Select Distinct CCCode ");
                    //        SQL.AppendLine("        From TblCostCenter ");
                    //        SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
                    //        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    //        SQL.AppendLine("    ) ");
                    //    }
                    //    else
                    //    {
                    //        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    //        SQL.AppendLine("        Select Distinct CCCode From TblGroupCostCenter ");
                    //        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    //        SQL.AppendLine("        ))) ");
                    //    }
                    //}

                    SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
                    SQL.AppendLine("Where 1=1 ");
                    SQL.AppendLine("And A.DocDt >= @StartFrom2 ");
                    SQL.AppendLine("And Left(A.DocDt, 6) < @YrMth2 ");
                    if (mIsPrintRptFicoSettingUseProfitCenter)
                    {
                        if (!mIsAllProfitCenterSelected)
                        {
                            Filter = string.Empty;
                            i = 0;

                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In ( ");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        ) ");
                            foreach (var x in mlProfitCenter.Distinct())
                            {
                                if (Filter.Length > 0) Filter += " Or ";
                                Filter += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                                //Sm.CmParam<String>(ref cm, "@ProfitCenterCode2_" + i.ToString(), x);
                                i++;
                            }
                            if (Filter.Length == 0)
                                SQL.AppendLine("    And 1=0 ");
                            else
                                SQL.AppendLine("    And (" + Filter + ") ");
                            SQL.AppendLine("    ) ");
                        }
                        else
                        {
                            if (ChkProfitCenterCode.Checked)
                            {
                                SQL.AppendLine("    And A.CCCode Is Not Null ");
                                SQL.AppendLine("    And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                                SQL.AppendLine("    ) ");
                                //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                            }
                            else
                            {
                                SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And ProfitCenterCode In (");
                                SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                SQL.AppendLine("        )))) ");
                            }
                        }
                    }
                }

                SQL.AppendLine(") T ");
                SQL.AppendLine("Group By T.DocType, T.AcNo ");
                SQL.AppendLine("Order By T.DocType, T.AcNo; ");
                #endregion
            }

            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, StartDt.Substring(4, 4)));
            //Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Yr2", Yr2);
            Sm.CmParam<String>(ref cm, "@YrMth2", string.Concat(Yr2, Mth2));
            if (StartFrom2.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom2", string.Concat(StartFrom2, StartDt2.Substring(4, 4)));
            if (mIsPrintOutAccountingShowBeginningAndEndingInventory)
            {
                Sm.CmParam<String>(ref cm, "@StartDt", Sm.Left(StartDt, 8));
                if (StartDt2.Trim().Length > 0) Sm.CmParam<String>(ref cm, "@StartDt2", Sm.Left(StartDt2, 8));
                Sm.CmParam<String>(ref cm, "@EndDt", Sm.Left(Sm.GetDte(DteDocDt2), 8));
                if (StartDt2.Trim().Length > 0) Sm.CmParam<String>(ref cm, "@EndDt2", Sm.Left(Sm.GetDte(DteDocDt4), 8));
            }

            //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); 

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 10000;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocType", 
                    "AcNo", "DAmt", "CAmt", "BeginningDAmt", "BeginningCAmt",
                    "EndingDAmt", "EndingCAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                            BeginningDAmt = Sm.DrDec(dr, c[4]),
                            BeginningCAmt = Sm.DrDec(dr, c[5]),
                            EndingDAmt = Sm.DrDec(dr, c[6]),
                            EndingCAmt = Sm.DrDec(dr, c[7]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournalCurrentMonth(ref List<Journal3> l, string ListAcNo, string Yr, string Mth, string Yr2, string Mth2, string EndDt, string EndDt2, ref List<string> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            string Filter2 = string.Empty;
            int i = 0;
            int j = 0;

            foreach (var x in mlProfitCenter.Distinct())
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "ProfitCenterCode=@ProfitCenter_" + i.ToString();
                Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                i++;
            }

            //foreach (var y in l2.Distinct())
            //{
            //    if (Filter2.Length > 0) Filter2 += " Or ";
            //    Filter2 += "B.AcNo=@AcNo_" + j.ToString();
            //    Sm.CmParam<String>(ref cm, "@AcNo_" + j.ToString(), y);
            //    j++;
            //}

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalConsolidationHdr A ");
            SQL.AppendLine("Inner Join TblJournalConsolidationDtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("    And A.CancelInd = 'N' ");
            if (Filter2.Length > 0)
                SQL.AppendLine("    And (" + Filter2 + ") ");
            //if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            //{
            //    SQL.AppendLine("    And B.EntCode=@EntCode ");

            //    if (mIsReportingFilterByEntity)
            //    {
            //        SQL.AppendLine("And B.EntCode Is Not Null ");
            //        SQL.AppendLine("And Exists( ");
            //        SQL.AppendLine("    Select 1 From TblGroupEntity ");
            //        SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
            //        SQL.AppendLine("    And GrpCode In ( ");
            //        SQL.AppendLine("        Select GrpCode From TblUser ");
            //        SQL.AppendLine("        Where UserCode=@UserCode ");
            //        SQL.AppendLine("    ) ");
            //        SQL.AppendLine(") ");
            //    }
            //}

            //if (mIsPrintRptFicoSettingUseMultiProfitCenter)
            //{
            //    if (ChkProfitCenterCode.Checked)
            //    {
            //        SQL.AppendLine("    And A.CCCode Is Not Null ");
            //        SQL.AppendLine("    And A.CCCode In ( ");
            //        SQL.AppendLine("        Select Distinct CCCode ");
            //        SQL.AppendLine("        From TblCostCenter ");
            //        SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
            //        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
            //        SQL.AppendLine("    ) ");
            //    }
            //    else
            //    {
            //        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
            //        SQL.AppendLine("        Select Distinct CCCode From TblGroupCostCenter ");
            //        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            //        SQL.AppendLine("        ))) ");
            //    }
            //}

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where Left(A.DocDt, 6) = @YrMth And Right(A.DocDt, 2) <= Right(@EndDt, 2) ");
            if (mIsPrintRptFicoSettingUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("            ) ");
                    if (Filter.Length > 0)
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("        ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            if(Yr2.Trim().Length > 0)
            {
                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select 'Old' As DocType, B.AcNo, B.DAmt, B.CAmt ");
                SQL.AppendLine("From TblJournalConsolidationHdr A ");
                SQL.AppendLine("Inner Join TblJournalConsolidationDtl B ON A.DocNo = B.DocNo ");
                //SQL.AppendLine("    And A.CancelInd = 'N' ");
                if (Filter2.Length > 0)
                    SQL.AppendLine("    And (" + Filter2 + ") ");
                //if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                //{
                //    SQL.AppendLine("    And B.EntCode=@EntCode ");

                //    if (mIsReportingFilterByEntity)
                //    {
                //        SQL.AppendLine("And B.EntCode Is Not Null ");
                //        SQL.AppendLine("And Exists( ");
                //        SQL.AppendLine("    Select 1 From TblGroupEntity ");
                //        SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                //        SQL.AppendLine("    And GrpCode In ( ");
                //        SQL.AppendLine("        Select GrpCode From TblUser ");
                //        SQL.AppendLine("        Where UserCode=@UserCode ");
                //        SQL.AppendLine("    ) ");
                //        SQL.AppendLine(") ");
                //    }
                //}
                //if (mIsPrintRptFicoSettingUseMultiProfitCenter)
                //{
                //    if (ChkProfitCenterCode.Checked)
                //    {
                //        SQL.AppendLine("    And A.CCCode Is Not Null ");
                //        SQL.AppendLine("    And A.CCCode In ( ");
                //        SQL.AppendLine("        Select Distinct CCCode ");
                //        SQL.AppendLine("        From TblCostCenter ");
                //        SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
                //        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                //        SQL.AppendLine("    ) ");
                //    }
                //    else
                //    {
                //        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                //        SQL.AppendLine("        Select Distinct CCCode From TblGroupCostCenter ");
                //        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                //        SQL.AppendLine("        ))) ");
                //    }
                //}
                SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
                SQL.AppendLine("Where Left(A.DocDt, 6) = @YrMth2 And Right(A.DocDt, 2) <= Right(@EndDt2, 2) ");
                if (mIsPrintRptFicoSettingUseProfitCenter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("            ) ");
                        if (Filter.Length > 0)
                            SQL.AppendLine("    And (" + Filter + ") ");
                        SQL.AppendLine("        ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                            SQL.AppendLine("    ) ");
                        }
                        else
                        {
                            SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In (");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        )))) ");
                        }
                    }
                }
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@YrMth2", string.Concat(Yr2, Mth2));
            if (Yr.Length > 0) Sm.CmParam<String>(ref cm, "@EndDt", EndDt.Substring(6, 2));
            if (Yr2.Length > 0) Sm.CmParam<String>(ref cm, "@EndDt2", EndDt2.Substring(6, 2));
            //Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); 

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal3()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllCOAOpeningBalance(ref List<COAOpeningBalance2> l, string ListAcNo, string Yr, string Yr2, ref List<string> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            string Filter2 = string.Empty;
            int i = 0;
            int j = 0;

            foreach (var x in mlProfitCenter.Distinct())
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "A.ProfitCenterCode=@ProfitCenter_" + i.ToString();
                Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                i++;
            }

            //foreach (var y in l2.Distinct())
            //{
            //    if (Filter2.Length > 0) Filter2 += " Or ";
            //    Filter2 += "B.AcNo=@AcNo_" + j.ToString();
            //    Sm.CmParam<String>(ref cm, "@AcNo_" + j.ToString(), y);
            //    j++;
            //}

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceConsolidationHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceConsolidationDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            if (Filter2.Length > 0)
                SQL.AppendLine("    And (" + Filter2 + ") ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.Yr=@Yr ");
            SQL.AppendLine("And A.CancelInd='N' ");
            if (mIsPrintRptFicoSettingUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    if (Filter.Length > 0)
                        SQL.AppendLine("    And (" + Filter + ") ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                        SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                }
            }
            if(Yr2.Trim().Length > 0)
            {
                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select 'Old' As DocType, B.AcNo, ");
                SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
                SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
                SQL.AppendLine("From TblCOAOpeningBalanceConsolidationHdr A ");
                SQL.AppendLine("Inner Join TblCOAOpeningBalanceConsolidationDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                if (Filter2.Length > 0)
                    SQL.AppendLine("    And (" + Filter2 + ") ");
                SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd='Y' ");
                SQL.AppendLine("Where A.Yr = @Yr2 ");
                SQL.AppendLine("And A.CancelInd = 'N' ");
                if (mIsPrintRptFicoSettingUseProfitCenter)
                {
                    SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                    if (!mIsAllProfitCenterSelected)
                    {
                        if (Filter.Length > 0)
                            SQL.AppendLine("    And (" + Filter + ") ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                            SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                        else
                        {
                            SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                            SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("    ) ");
                        }
                    }
                }
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Yr2", Yr2);
            //Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); // multiEntCode.Contains("Consolidate") ? "" : multiEntCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COAOpeningBalance2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COAOpeningBalance2> l, ref List<COA> lCOA, string DocType, string UsedAcNo)
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo).Where(a => a.Year == DocType))
            {
                if(Sm.Find_In_Set(x.AcNo, UsedAcNo))
                {
                    lJournal.Add(new Journal()
                    {
                        AcNo = x.AcNo,
                        DAmt = x.DAmt,
                        CAmt = x.CAmt
                    });
                }
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) &&
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.')
                            ||
                            lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.')) &&
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.')
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<Journal2> l, ref List<COA> lCOA, string DocType, string UsedAcNo)
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo).Where(a => a.Year == DocType))
            {
                if (Sm.Find_In_Set(x.AcNo, UsedAcNo))
                {
                    lJournal.Add(new Journal()
                    {
                        AcNo = x.AcNo,
                        DAmt = x.DAmt,
                        CAmt = x.CAmt,
                        BeginningDAmt = x.BeginningDAmt,
                        BeginningCAmt = x.BeginningCAmt,
                        EndingDAmt = x.EndingDAmt,
                        EndingCAmt = x.EndingCAmt
                    });
                }
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            lCOA[j].MonthToDateBeginningDAmt += lJournal[i].BeginningDAmt;
                            lCOA[j].MonthToDateBeginningCAmt += lJournal[i].BeginningCAmt;
                            lCOA[j].MonthToDateEndingDAmt += lJournal[i].EndingDAmt;
                            lCOA[j].MonthToDateEndingCAmt += lJournal[i].EndingCAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                        
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<Journal3> l, ref List<COA> lCOA, string DocType, string UsedAcNo)
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo).Where(p => p.Year == DocType))
            {
                if (Sm.Find_In_Set(x.AcNo, UsedAcNo))
                {
                    lJournal.Add(new Journal()
                    {
                        AcNo = x.AcNo,
                        DAmt = x.DAmt,
                        CAmt = x.CAmt
                    });
                }
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<COA> lCOA)
        {
            return;
            if (
                mAcNoForCurrentEarning.Length == 0 ||
                mAcNoForIncome.Length == 0 ||
                mAcNoForCost.Length == 0
                ) return;

            int 
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed==3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }

            //var CurrentProfiLossParentIndex = -1;
            //var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;

            //for (var i = 0; i < lCOA.Count; i++)
            //{
            //    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //    {
            //        CurrentProfiLossParentIndex = i;
            //        break;
            //    }
            //}

            decimal Amt = 0m;
            
            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;
            //    (lCOA[IncomeIndex].OpeningBalanceCAmt - lCOA[IncomeIndex].OpeningBalanceDAmt)-
            //    (lCOA[CostIndex].OpeningBalanceDAmt - lCOA[CostIndex].OpeningBalanceCAmt);

            //lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt = 0m;
            //lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt = Amt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            CurrentProfiLossParentIndex = i;
            //            lCOA[CurrentProfiLossParentIndex].OpeningBalanceDAmt += 0m;
            //            lCOA[CurrentProfiLossParentIndex].OpeningBalanceCAmt += Amt;
                        
            //            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //            f = CurrentProfiLossParentAcNo.Length > 0;
            //            break;
            //        }
            //    }
            //}

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);
                
            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;
            
            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                        lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;
                        
                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);
                
            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                        lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;
                        
                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Year To Date
            //Amt =
            //    (lCOA[IncomeIndex].YearToDateCAmt - lCOA[IncomeIndex].YearToDateDAmt) -
            //    (lCOA[CostIndex].YearToDateDAmt - lCOA[CostIndex].YearToDateCAmt);
                
            //lCOA[CurrentProfiLossIndex].YearToDateDAmt = 0m;
            //lCOA[CurrentProfiLossIndex].YearToDateCAmt = Amt;
            //CurrentProfiLossParentIndex = -1;
            //CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //f = true;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            CurrentProfiLossParentIndex = i;
            //            lCOA[CurrentProfiLossParentIndex].YearToDateDAmt += 0m;
            //            lCOA[CurrentProfiLossParentIndex].YearToDateCAmt += Amt;
                        
            //            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //            f = CurrentProfiLossParentAcNo.Length > 0;
            //            break;
            //        }
            //    }
            //}
        }

        private void Process6(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt = 
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt +
                    lCOA[i].CurrentMonthDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt +
                    lCOA[i].CurrentMonthCAmt;

                if (mIsPrintOutAccountingShowBeginningAndEndingInventory)
                {
                    lCOA[i].YearToDateBeginningDAmt =
                        lCOA[i].OpeningBalanceDAmt +
                        lCOA[i].MonthToDateBeginningDAmt;

                    lCOA[i].YearToDateEndingDAmt =
                        lCOA[i].OpeningBalanceDAmt +
                        lCOA[i].MonthToDateEndingDAmt;

                    lCOA[i].YearToDateBeginningCAmt =
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateBeginningCAmt;

                    lCOA[i].YearToDateEndingCAmt =
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateEndingCAmt;
                }
            }
        }

        private void Process9(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].AcType == "D")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceDAmt +
                        lCOA[i].MonthToDateDAmt -
                        lCOA[i].MonthToDateCAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthDAmt -
                        lCOA[i].CurrentMonthCAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (mIsPrintOutAccountingShowBeginningAndEndingInventory)
                    {
                        lCOA[i].YearToDateBeginningBalance = lCOA[i].YearToDateBeginningDAmt - lCOA[i].YearToDateBeginningCAmt;
                        lCOA[i].YearToDateEndingBalance = lCOA[i].YearToDateEndingDAmt - lCOA[i].YearToDateEndingCAmt;
                    }
                }
                if (lCOA[i].AcType == "C")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateCAmt-
                        lCOA[i].MonthToDateDAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthCAmt -
                        lCOA[i].CurrentMonthDAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                    if (mIsPrintOutAccountingShowBeginningAndEndingInventory)
                    {
                        lCOA[i].YearToDateBeginningBalance = lCOA[i].YearToDateBeginningCAmt - lCOA[i].YearToDateBeginningDAmt;
                        lCOA[i].YearToDateEndingBalance = lCOA[i].YearToDateEndingCAmt - lCOA[i].YearToDateEndingDAmt;
                    }
                }
            }
        }

        private void ProcessFicoCOA(ref List<RptFicoSetting> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            //var lRptFicoSetting = new List<RptFicoSetting>();

            SQL.AppendLine("Select B.RptFicoCode, B.SettingCode, B.SettingDesc, B.Sequence, B.Formula ");
            if (mIsPrintOutAccountingShowBeginningAndEndingInventory) SQL.AppendLine(", B.BeginningInvInd, B.EndingInvInd ");
            else SQL.AppendLine(", 'N' As BeginningInvInd, 'N' As EndingInvInd ");
            SQL.AppendLine("From TblRptFicoSettingHdr A ");
            SQL.AppendLine("Inner Join TblRptFicoSettingDtl B On A.RptFicoCode = B.RptFicoCode ");
            SQL.AppendLine("Where A.RptFicoCode = @FicoCode ");
            if (!mIsFicoRptSettingNotUseSite && mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order by Cast(sequence As UNSIGNED); ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "SettingDesc", "Sequence", "Formula", "BeginningInvInd", "EndingInvInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RptFicoSetting()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            SettingDesc = Sm.DrStr(dr, c[2]),
                            Sequence = Sm.DrStr(dr, c[3]),
                            Formula = Sm.DrStr(dr, c[4]),
                            Balance = 0,
                            AcNo = string.Empty,
                            BeginningInvInd = Sm.DrStr(dr, c[5]) == "Y",
                            EndingInvInd = Sm.DrStr(dr, c[6]) == "Y",
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessFicoCOA2(ref List<RptFicoSetting> l, ref List<RptFicoSetting>  lRptFicoSetting)
        {
             lRptFicoSetting.Clear();

            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].SettingDesc.Length > 0)
                {
                    var cm = new MySqlCommand();
                    var SQL = new StringBuilder();

                    SQL.AppendLine("SET SESSION group_concat_max_len = 1000000; ");
                    SQL.AppendLine("Select AcNo From TblCOA ");
                    SQL.AppendLine("Where Find_In_Set(AcNo,( ");
                    SQL.AppendLine("    Select Group_Concat(Acno) As AcNo ");
                    SQL.AppendLine("    From TblRptFicoSettingDtl ");
                    SQL.AppendLine("    Where SettingCode=@SettingCode ");
                    SQL.AppendLine("    And RptFicoCode=@FicoCode ");
                    SQL.AppendLine("    Order by Cast(sequence As UNSIGNED) ");
                    SQL.AppendLine(")) Order By AcNo;");
                    Sm.CmParam<String>(ref cm, "@SettingCode", l[i].SettingCode);
                    Sm.CmParam<String>(ref cm, "@FicoCode", l[i].FicoCode);
                   
                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandTimeout = 600;
                        cm.CommandText = SQL.ToString();
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                lRptFicoSetting.Add(new RptFicoSetting()
                                {
                                    FicoCode = l[i].FicoCode,
                                    SettingCode = l[i].SettingCode,
                                    SettingDesc = l[i].SettingDesc,
                                    Sequence = l[i].Sequence,
                                    Formula = l[i].Formula,
                                    AcNo = Sm.DrStr(dr, c[0]),
                                    BeginningInvInd = l[i].BeginningInvInd,
                                    EndingInvInd = l[i].EndingInvInd,
                                    Balance = 0,
                                });
                            }
                        }
                        dr.Close();
                    }
                }
            }
        }

        private void ProcessFicoCOA3(ref List<COAFicoSetting> lCOASet)
        {
            lCOASet.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            //var lRptFicoSetting = new List<RptFicoSetting>();

            SQL.AppendLine("Select B.RptFicoCode, B.SettingCode, B.SettingDesc, B.Sequence, B.Formula ");
            SQL.AppendLine("From TblRptFicoSettinghdr A ");
            SQL.AppendLine("Inner Join TblRptFicoSettingDtl B On A.RptFicoCode = B.RptFicoCode ");
            SQL.AppendLine("Where A.RptFicoCode = @FicoCode ");
            if (!mIsFicoRptSettingNotUseSite && mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order By Cast(B.sequence As UNSIGNED);");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "SettingDesc", "Sequence", "Formula" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        lCOASet.Add(new COAFicoSetting()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            SettingDesc = Sm.DrStr(dr, c[2]),
                            Sequence = Sm.DrStr(dr, c[3]),
                            Formula = Sm.DrStr(dr, c[4]),
                            Balance = 0,
                        });
                }
                dr.Close();
            }
        }

        private void ProcessFicoCOA4(ref List<COAFicoSetting> lCOASet, ref List<RptFicoSetting2> l,iGrid Grdxx)
        {
            Sm.ClearGrd(Grdxx, true);
            iGRow r;


            for (var i = 0; i < lCOASet.Count; i++)
            {
                decimal bal = 0;
                r = Grdxx.Rows.Add();
                r.Cells[0].Value = lCOASet[i].SettingCode;
                r.Cells[1].Value = lCOASet[i].SettingDesc;

               
                if (lCOASet[i].Formula.Length == 0)
                {
                    l.OrderBy(o => o.Sequence).ToList();
                    for (var j = 0; j < l.Count; j++)
                    {
                        if (lCOASet[i].SettingCode == l[j].SettingCode)
                        {
                            r.Cells[2].Value = bal + l[j].Balance;
                            if (mIsPrintRptFicoSettingUseParentheses)
                            {
                                r.Cells[3].Value = bal + l[j].Balance;
                                if ((bal + l[j].Balance) < 0)
                                {
                                    r.Cells[3].Value = string.Concat("(", Sm.FormatNum(((bal + l[j].Balance) * -1m).ToString(), 0), ")");
                                }
                            }

                            bal = bal + l[j].Balance;
                        }
                    }
                }
                else
                {
                    char[] delimiters = {'+', '-' };
                    string SQLFormula = lCOASet[i].Formula;
                    string[] ArraySQLFormula = lCOASet[i].Formula.Split(delimiters);
                    for (int ind = 0; ind < ArraySQLFormula.Count(); ind++)
                    {
                        for (var h = 0; h < Grdxx.Rows.Count; h++)
                        {
                            if (ArraySQLFormula[ind].ToString() == Sm.GetGrdStr(Grdxx, h, 0))
                            {
                                string oldS = ArraySQLFormula[ind].ToString();
                                string newS = Sm.GetGrdDec(Grdxx, h, 2).ToString();
                                
                                SQLFormula = SQLFormula.Replace(oldS, newS);
                            }
                        }
                    }
                    decimal baltemp = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));
                    r.Cells[2].Value = baltemp;
                    r.Cells[3].Value = baltemp;
                    if (mIsPrintRptFicoSettingUseParentheses)
                    {
                        if (baltemp < 0)
                            r.Cells[3].Value = string.Concat("(", Sm.FormatNum((baltemp * -1m).ToString(), 0), ")");
                    }
                }
            }
        }

        private void ParPrint()
        {
            int nomor = 0;
            var l = new List<TB>();
            var l2 = new List<TBDtl>();

            string Doctitle = Sm.GetParameter("DocTitle");
            string[] TableName = { "TB", "TBDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            string ProfitCenterCode = string.Empty;
            foreach (var x in mlProfitCenter.Take(1)) ProfitCenterCode = x;

            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddressCity', ");
            SQL.AppendLine("A.RptFicoCode, A.Remark, A.Createby ");
            if (mIsUseR1 && ProfitCenterCode.Length > 0)
            {
                SQL.AppendLine(", B.EntName, B.EntAddress, B.EntPhone, B.EntFax ");
            }
            else
            {
                SQL.AppendLine(", Null As EntName, Null As EntAddress, Null As EntPhone, Null as EntFax ");
            }
            SQL.AppendLine("From TblRptFicoSettingHdr A ");
            if (mIsUseR1 && ProfitCenterCode.Length > 0)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select EntName, EntAddress, EntPhone, EntFax  ");
                SQL.AppendLine("    From TblEntity ");
                SQL.AppendLine("    Where EntCode In (Select EntCode From TblProfitCenter Where ProfitCenterCode = '" + ProfitCenterCode + "') ");
                SQL.AppendLine(") B On 0 = 0 ");
            }
            SQL.AppendLine("Where A.RptFIcoCode=@RptFicoCode  ");
            if (!mIsFicoRptSettingNotUseSite && mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@RptFicoCode", Sm.GetLue(LueOptionCode));
                Sm.CmParam<String>(ref cm, "@CompanyLogo", Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "CompanyName",
                    "CompanyAddress",
                    "CompanyPhone",
                    "CompanyAddressCity",
                    "Remark",

                    //6-10
                    "CreateBy",
                    "EntName",
                    "EntAddress",
                    "EntPhone",
                    "EntFax"
                });
                if (dr.HasRows)
                {
                    int month = Convert.ToInt32(Sm.GetDte(DteDocDt1).Substring(4, 2));;
                    string mth = string.Empty;
                    switch (month)
                    {
                        case 1:
                            mth = "January";
                            break;
                        case 2:
                            mth = "February";
                            break;
                        case 3:
                            mth = "March";
                            break;
                        case 4:
                            mth = "April";
                            break;
                        case 5:
                            mth = "May";
                            break;
                        case 6:
                            mth = "June";
                            break;
                        case 7:
                            mth = "July";
                            break;
                        case 8:
                            mth = "August";
                            break;
                        case 9:
                            mth = "September";
                            break;
                        case 10:
                            mth = "October";
                            break;
                        case 11:
                            mth = "November";
                            break;
                        case 12:
                            mth = "December";
                            break;
                    }
                    while (dr.Read())
                    {
                        l.Add(new TB()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = (mIsUseR1 && ProfitCenterCode.Length > 0) ? Sm.DrStr(dr, c[7]) : Sm.DrStr(dr, c[1]),
                            CompanyAddress = (mIsUseR1 && ProfitCenterCode.Length > 0) ? Sm.DrStr(dr, c[8]) : Sm.DrStr(dr, c[2]),
                            CompanyPhone = (mIsUseR1 && ProfitCenterCode.Length > 0) ? Sm.DrStr(dr, c[9]) : Sm.DrStr(dr, c[3]),
                            CompanyAddressCity = (mIsUseR1 && ProfitCenterCode.Length > 0) ? Sm.DrStr(dr, c[10]) : Sm.DrStr(dr, c[4]),
                            Yr = (!mIsPrintRptFicoSettingYrAscOrder) ? Sm.GetLue(LueYr) : (!mIsPrintRptFicoSettingYr2DisplayMth) ? (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString() : "Dec " + (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString(),
                            Mth = mth,
                            //Entity = Sm.GetValue("select EntName From tblentity Where EntCode = '"+Sm.GetLue(LueEntCode)+"' ") ,
                            HRemark = Sm.DrStr(dr, c[5]),
                            Type = LueOptionCode.Text,
                            Yr2 = (mIsPrintRptFicoSettingYrAscOrder) ? Sm.GetLue(LueYr) : (!mIsPrintRptFicoSettingYr2DisplayMth) ? (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString() : "Dec " + (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString(),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            IsPrintRptFicoSettingShowPercentage = mIsPrintRptFicoSettingShowPercentage,
                            IsPrintRptFicoSettingDisplayDate = mIsPrintRptFicoSettingDisplayDate,
                            IsPrintRptFicoSettingDisplayPrintDate= mIsPrintRptFicoSettingDisplayPrintDate,
                            Date = System.DateTime.DaysInMonth(Convert.ToInt32(Sm.GetLue(LueYr)), month),
                            ProfitCenter = CcbProfitCenterCode.Text,
                            PeriodYear2 = DteDocDt3.Text + "-" + DteDocDt4.Text,
                            Period = (Doctitle == "PHT" ? DteDocDt1.Text + " - " + DteDocDt2.Text : (!mIsPrintRptFicoSettingYrAscOrder) ? Sm.GetLue(LueYr) : (!mIsPrintRptFicoSettingYr2DisplayMth) ? (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString() : "Dec " + (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString()),
                            Period2 = (Doctitle == "PHT" ? DteDocDt3.Text + " - " + DteDocDt4.Text : (mIsPrintRptFicoSettingYrAscOrder) ? Sm.GetLue(LueYr) : (!mIsPrintRptFicoSettingYr2DisplayMth) ? (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString() : "Dec " + (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString()),
                            DocTitle = Doctitle
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select B.AcNo AcNo2, A.SettingCode, A.SettingDesc, A.Sequence, A.AcNo, A.Formula, A.Font,  A.Line, A.Align, A.IndentInd  ");
                SQLDtl.AppendLine("From TblRptFicoSettingDtl A   ");
                SQLDtl.AppendLine("LEFT JOIN  ");
                SQLDtl.AppendLine("(SELECT AcNo FROM tblcoa WHERE FIND_IN_SET (Acno, '" + mPrintRptFicoSettingCOAParentheses + "')) B ON A.AcNo=B.AcNo ");
                SQLDtl.AppendLine("Where A.RptFIcoCode=@RptFicoCode And PrintInd = 'Y'  order by Cast(sequence As UNSIGNED) ");

                cmDtl.CommandText = SQLDtl.ToString();
                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@RptFicoCode", Sm.GetLue(LueOptionCode));


                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "SettingCode" ,

                         //1-5
                         "SettingDesc",
                         "Sequence",
                         "Formula",
                         "Font",
                         "Line",

                         //6-9
                         "AcNo",
                         "AcNo2",
                         "Align",
                         "IndentInd"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        decimal bal = 0;
                        decimal bal2 = 0;
                        decimal percentage = 0;
                        for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0 && Sm.GetGrdStr(Grd1, Row, 0) == Sm.DrStr(drDtl, cDtl[0]))
                            {
                                bal =  Sm.GetGrdDec(Grd1, Row, 2);
                            }
                        }
                        for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0 && Sm.GetGrdStr(Grd2, Row, 0) == Sm.DrStr(drDtl, cDtl[0]))
                            {
                                bal2 = Sm.GetGrdDec(Grd2, Row, 2);
                            }
                        }

                        if(bal2 != 0) 
                        {
                            percentage = Sm.Round((bal / bal2 )*100 ,2);
                        }
                       
                        nomor = nomor + 1;
                        l2.Add(new TBDtl()
                        {
                            nomor = nomor,
                            SettingCode = Sm.DrStr(drDtl, cDtl[0]),
                            SettingDesc = Sm.DrStr(drDtl, cDtl[1]),
                            Sequence = Sm.DrStr(drDtl, cDtl[2]),
                            Formula = Sm.DrStr(drDtl, cDtl[3]),
                            Font = Sm.DrStr(drDtl, cDtl[4]),
                            Line = Sm.DrStr(drDtl, cDtl[5]) == "P" ? ""+Environment.NewLine+""+Environment.NewLine+"" : Sm.DrStr(drDtl, cDtl[5]),
                            Balance = (!mIsPrintRptFicoSettingYrAscOrder)? bal : bal2,
                            Balance2 = (!mIsPrintRptFicoSettingYrAscOrder) ? bal2 : bal,
                            Percentage = percentage,
                            mIsPrintRptFicoSettingUseParentheses = mIsPrintRptFicoSettingUseParentheses,
                            mPrintRptFicoSettingParenthesesFormat = mPrintRptFicoSettingParenthesesFormat,
                            AcNo = Sm.DrStr(drDtl, cDtl[6]),
                            AcNo2 = Sm.DrStr(drDtl, cDtl[7]),
                            Align = Sm.DrStr(drDtl, cDtl[8]),
                            IndentInd = Sm.DrStr(drDtl, cDtl[9]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(l2);
            #endregion

            Sm.PrintReport("RptFicoSetting5", myLists, TableName, false);
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            //SQL.AppendLine("    Select 'Consolidate' Col, '01' Col2  ");
            //SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select ProfitCenterName AS Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));

            //var IsIncludeConsolidate = Value.Contains("Consolidate");
            //return string.Concat(
            //    (IsIncludeConsolidate?"Consolidate, ":string.Empty), 
            //    Sm.GetValue(
            //        "Select Group_Concat(T.Code Separator ', ') As Code " +
            //        "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ", 
            //        Value.Replace(", ", ",")));
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void GetTotalBalance()
        {
            decimal Balance1 = 0m;
            decimal Balance2 = 0m;
            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    Balance1 += Sm.GetGrdDec(Grd1, i, 2);
                    Balance2 += Sm.GetGrdDec(Grd1, i, 4);
                }
                TxtBalance1.EditValue = Sm.FormatNum(Balance1, 0);
                TxtBalance2.EditValue = Sm.FormatNum(Balance2, 0);
            }
        }

        private void CopyBalance()
        {
            if (Grd2.Rows.Count > 0)
            {
                for (int i = 0; i < Grd2.Rows.Count; ++i)
                {
                    string AcNo1 = Sm.GetGrdStr(Grd2, i, 0);
                    for (int j = 0; j < Grd1.Rows.Count; ++j)
                    {
                        string AcNo2 = Sm.GetGrdStr(Grd1, j, 0);
                        if (AcNo1 == AcNo2)
                        {
                            Sm.CopyGrdValue(Grd1, j, 4, Grd2, i, 2);
                            Sm.CopyGrdValue(Grd1, j, 5, Grd2, i, 3);
                        }
                    }
                }
            }
        }

        private bool IsDateNotValid()
        {
            if (Sm.GetLue(LueYr).Length > 0)
            {
                if (!Sm.CompareStr(Sm.GetLue(LueYr), Sm.GetDte(DteDocDt1).Substring(0, 4)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                            "Date should be in range with year.");
                    DteDocDt1.Focus();
                    return true;
                }
                if (!Sm.CompareStr(Sm.GetLue(LueYr), Sm.GetDte(DteDocDt2).Substring(0, 4)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                            "Date should be in range with year.");
                    DteDocDt2.Focus();
                    return true;
                }
            }
            if (Sm.GetLue(LueYr2).Length > 0)
            {
                if (!Sm.CompareStr(Sm.GetLue(LueYr2), Sm.GetDte(DteDocDt3).Substring(0, 4)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                            "Date should be in range with year.");
                    DteDocDt3.Focus();
                    return true;
                }
                if (!Sm.CompareStr(Sm.GetLue(LueYr2), Sm.GetDte(DteDocDt4).Substring(0, 4)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                            "Date should be in range with year.");
                    DteDocDt4.Focus();
                    return true;
                }
            }
            return false;
        }

            #endregion

            #endregion

        #region Event

            #region Misc Control Event

        private void LueOptionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOptionCode, new Sm.RefreshLue1(SetLueFicoFormula));
            if (Sm.Find_In_Set(Sm.GetLue(LueOptionCode), mFicoSettingCodeForPrintOutAccounting))
            {
                LblStartDt.ForeColor = Color.Black;
                Sm.SetDte(DteDocDt1, Sm.GetLue(LueYr)+"0101");
                DteDocDt1.Enabled = false;
                if (Sm.GetLue(LueYr2).Length > 0)
                {
                    Sm.SetDte(DteDocDt3, Sm.GetLue(LueYr2)+"0101");
                    DteDocDt3.Enabled = false;
                }
            }
            else
            {
                LblStartDt.ForeColor = Color.Red;
                DteDocDt1.Enabled = true;
                DteDocDt3.Enabled = true;
            }
        }

        private void BtnPrint2_Click(object sender, EventArgs e)
        {
             ParPrint();
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueYr).Length > 0)
            {
                Sm.SetDte(DteDocDt1, Sm.GetLue(LueYr)+"0101");
                Sm.SetDte(DteDocDt2, Sm.GetLue(LueYr)+"1231");
            }
        }

        private void LueYr2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueYr2).Length > 0)
            {
                Sm.SetDte(DteDocDt3, Sm.GetLue(LueYr2)+"0101");
                Sm.SetDte(DteDocDt4, Sm.GetLue(LueYr2)+"1231");
            }
            else
            {
                Sm.SetDte(DteDocDt3, string.Empty);
                Sm.SetDte(DteDocDt4, string.Empty);
            }
            if (Sm.GetLue(LueOptionCode).Length > 0)
                if (Sm.Find_In_Set(Sm.GetLue(LueOptionCode), mFicoSettingCodeForPrintOutAccounting))
                    DteDocDt3.Enabled = false;
            else
                    DteDocDt3.Enabled = true;
        }
       
        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        private void TypeRule_Click(object sender, EventArgs e)
        {
            Sm.StdMsg(mMsgType.Info, mTypeRuleFormat);
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBeginningDAmt { get; set; }
            public decimal MonthToDateBeginningCAmt { get; set; }
            public decimal MonthToDateEndingDAmt { get; set; }
            public decimal MonthToDateEndingCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBeginningDAmt { get; set; }
            public decimal YearToDateBeginningCAmt { get; set; }
            public decimal YearToDateEndingDAmt { get; set; }
            public decimal YearToDateEndingCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
            public decimal YearToDateBeginningBalance { get; set; }
            public decimal YearToDateEndingBalance { get; set; }
        }

        private class COA2
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal BeginningDAmt { get; set; }
            public decimal BeginningCAmt { get; set; }
            public decimal EndingDAmt { get; set; }
            public decimal EndingCAmt { get; set; }
        }

        private class Journal2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal BeginningDAmt { get; set; }
            public decimal BeginningCAmt { get; set; }
            public decimal EndingDAmt { get; set; }
            public decimal EndingCAmt { get; set; }
        }

        private class Journal3
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COAOpeningBalance2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        private class COAFicoSetting
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
            public decimal Balance2 { get; set; }
        }

        private class RptFicoSetting
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string AcNo { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
            public bool BeginningInvInd { get; set; }
            public bool EndingInvInd { get; set; }
        }

        private class RptFicoSetting2
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string AcNo { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
            public bool BeginningInvInd { get; set; }
            public bool EndingInvInd { get; set; }
        }

        private class TB
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyAddressCity { get; set; }
            public string Yr { get; set; }
            public string Yr2 { get; set; }
            public string Mth { get; set; }
            //public string Entity { get; set; }
            public string HRemark { get; set; }
            public string Type { get; set; }
            public string PrintBy { get; set; }
            public bool IsPrintRptFicoSettingShowPercentage { get; set; }
            public bool IsPrintRptFicoSettingDisplayDate { get; set; }
            public bool IsPrintRptFicoSettingDisplayPrintDate { get; set; }
            public int Date { get; set; }
            public string ProfitCenter { get; set; }
            public string Period { get; set; }
            public string Period2 { get; set; }
            public string PeriodYear { get; set; }
            public string PeriodYear2 { get; set; }
            public string DocTitle { get; set; }
        }

        private class TBDtl
        {
            public int nomor { set; get; }
            public string SettingCode { set; get; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
            public decimal Balance2 { get; set; }
            public string Font { get; set; }
            public string Line { get; set; }
            public string Align { get; set; }
            public string IndentInd { get; set; }
            public decimal Percentage { get; set; }
            public bool mIsPrintRptFicoSettingUseParentheses { get; set; }
            public string mPrintRptFicoSettingParenthesesFormat { get; set; }
            public string AcNo { get; set; }
            public string AcNo2 { get; set; }
        }
        #endregion
    }
}
