﻿#region Update
/*
    20/06/2017 [TKG] Update remark di journal
    11/07/2017 [WED] EntCode save ke journalDtl
    11/07/2017 [WED] Remark Journal Hdr ambil dari remark dokumen transaksi 
    01/10/2018 [TKG] untuk KIM menggunakan document date sebagai batch# kalau kosong
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    07/02/2021 [TKG/GSS] ubah GetParameter dan proses save.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmMutation : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmMutationFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal bool mIsShowMutationPriceInfo = false;
        private bool 
            mIsAutoJournalActived = false,
            mIsBatchNoUseDocDtIfEmpty = false;
        private string mEntCode = string.Empty;

        #endregion

        #region Constructor

        public FrmMutation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                SetIsShowMutationPriceInfo();
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 41;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Cancel", 
                        "Old Cancel", 
                        "",
                        "Mutation From" + Environment.NewLine + "Item Code",
                        "",
                        
                        //6-10
                        "Mutation From" + Environment.NewLine + "Item Name",
                        "Mutation From" + Environment.NewLine + "Batch#",
                        "Source",
                        "Lot",
                        "Bin",
                        
                        //11-15
                        "Mutation From" + Environment.NewLine + "Stock 1",
                        "Mutation From" + Environment.NewLine + "Quantity 1",
                        "Mutation From" + Environment.NewLine + "Uom 1",
                        "Mutation From" + Environment.NewLine + "Stock 2",
                        "Mutation From" + Environment.NewLine + "Quantity 2",

                        //16-20
                        "Mutation From" + Environment.NewLine + "Uom 2",
                        "Mutation From" + Environment.NewLine + "Stock 3",
                        "Mutation From" + Environment.NewLine + "Quantity 3",
                        "Mutation From" + Environment.NewLine + "Uom 3",
                        "",
                        
                        //21-25
                        "Mutation To" + Environment.NewLine + "Item Code",
                        "",
                        "Mutation To" + Environment.NewLine + "Item Name",
                        "Mutation To" + Environment.NewLine + "Batch#",
                        "Mutation To" + Environment.NewLine + "Source",
                        
                        //26-30
                        "Mutation To" + Environment.NewLine + "Quantity 1",
                        "Mutation To" + Environment.NewLine + "Uom 1",
                        "Mutation To" + Environment.NewLine + "Quantity 2",
                        "Mutation To" + Environment.NewLine + "Uom 2",
                        "Mutation To" + Environment.NewLine + "Quantity 3",
                        
                        //31-35
                        "Mutation To" + Environment.NewLine + "Uom 3",
                        "Item Cost#",
                        "",
                        "Item Cost"+Environment.NewLine+"D No",
                        "Currency",
                        
                        //36-40
                        "Cost",
                        "Remark",
                        "Stock" + Environment.NewLine + "(1)",
                        "Stock" + Environment.NewLine + "(2)",
                        "Stock" + Environment.NewLine + "(3)"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        50, 0, 20, 100, 20,

                        //6-10
                        150, 200, 180, 100, 100, 

                        //11-15
                        100, 100, 100, 100, 100,

                        //16-20
                        100, 100, 100, 100, 20, 
                        
                        //21-25
                        100, 20, 150, 200, 170, 
                        
                        //26-30
                        100, 100, 100, 100, 100, 
                        
                        //31-35
                        100, 130, 20, 0, 80,  
                        
                        //36-40
                        100, 300, 100, 100, 100
                    }
                );

            for (int Col = 3; Col <= 19; Col++)
                Grd1.Cols[Col].ColHdrStyle.BackColor = Color.LightSalmon;

            for (int Col = 20; Col <= 31; Col++)
                Grd1.Cols[Col].ColHdrStyle.BackColor = Color.LightGreen;
            

            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 6, 7, 8, 9, 10, 11, 13, 14, 16, 17, 19, 21, 23, 25, 27, 29, 31, 32, 33, 34, 35, 36, 38, 39, 40 });
            Sm.GrdColButton(Grd1, new int[] { 3, 5, 20, 22, 33 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 14, 15, 17, 18, 26, 28, 30, 36, 38, 39, 40 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 4, 5, 8, 9, 10, 14, 15, 16, 17, 18, 19, 21, 22, 25, 28, 29, 30, 31, 38, 39, 40 }, false);
            if (!mIsShowMutationPriceInfo) Sm.GrdColInvisible(Grd1, new int[] { 32, 33, 34, 35, 36 }, false);
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 8, 9, 10, 22, 25 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 28, 29 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19, 28, 29, 30, 31 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueWhsCode, MeeRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         DteDocDt, LueWhsCode, MeeRemark
                    }, true);
                    Grd1.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, LueWhsCode, MeeRemark
            });
            mEntCode = string.Empty;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 12, 14, 15, 17, 18, 26, 28, 30, 36, 38, 39, 40 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMutationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mEntCode = Sm.GetValue("Select C.EntCode " +
                            "From TblWarehouse A " +
                            "Inner Join TblCostCenter B on A.CCCode = B.CCCode  " +
                            "INner Join TblProfitCenter C on B.ProfitCenterCode = C.ProfitCenterCode " +
                            "Where A.WhsCode = '" + Sm.GetLue(LueWhsCode) + "' limit 1;");
                
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string par = Sm.GetParameter("NumberOfInventoryUomCode");
            if (par == "1")
            {
                ParPrint(1);
            }
            else if (par == "2")
            {
                ParPrint(2);
            }
            else
                ParPrint(3);

        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMutationDlg(this, Sm.GetLue(LueWhsCode)));
            }

            if (e.ColIndex == 20
                && TxtDocNo.Text.Length == 0
                && !Sm.IsDteEmpty(DteDocDt, "Document date")
                && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 4, false, "Item is empty"))
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmMutationDlg2(this, Sm.GetDte(DteDocDt), Sm.GetGrdStr(Grd1, e.RowIndex, 4), e.RowIndex));
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 22 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 21);
                f.ShowDialog();
            }

            if (e.ColIndex == 33 && Sm.GetGrdStr(Grd1, e.RowIndex, 32).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItemCost(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 32);
                f.ShowDialog();
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0 &&
                ((e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 2)) || e.ColIndex != 1))
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && !IsItemEmpty(e) && Sm.IsGrdColSelected(new int[] { 3, 12, 15, 18, 20, 24, 26, 28, 30, 41 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmMutationDlg(this, Sm.GetLue(LueWhsCode)));

            if (e.ColIndex == 20 
                && TxtDocNo.Text.Length == 0 
                && !Sm.IsDteEmpty(DteDocDt, "Document date") 
                && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 4, false, "Item is empty"))

                Sm.FormShowDialog(new FrmMutationDlg2(this, Sm.GetDte(DteDocDt), Sm.GetGrdStr(Grd1, e.RowIndex, 4), e.RowIndex));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 22 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 21);
                f.ShowDialog();
            }

            if (e.ColIndex == 33 && Sm.GetGrdStr(Grd1, e.RowIndex, 32).Length != 0)
            {
                var f = new FrmItemCost(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 32);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 12, 15, 18, 26, 28, 30 }, e.ColIndex) &&
                Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0;

            if (Sm.IsGrdColSelected(new int[] { 24, 37 }, e.ColIndex) &&
                Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length != 0)
                Grd1.Cells[e.RowIndex, e.ColIndex].Value = Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Trim();

            if (e.ColIndex == 12 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 13), Sm.GetGrdStr(Grd1, e.RowIndex, 16)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 15, Grd1, e.RowIndex, 12);

            if (e.ColIndex == 12 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 13), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 12);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 26 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 27), Sm.GetGrdStr(Grd1, e.RowIndex, 29)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 28, Grd1, e.RowIndex, 26);

            if (e.ColIndex == 26 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 27), Sm.GetGrdStr(Grd1, e.RowIndex, 31)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 30, Grd1, e.RowIndex, 26);

            if (e.ColIndex == 28 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 29), Sm.GetGrdStr(Grd1, e.RowIndex, 31)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 30, Grd1, e.RowIndex, 28);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            
            if (Sm.IsGrdColSelected(new int[] { 12, 15, 18, 26, 28, 30 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Mutation", "TblMutationHdr");

            var JournalDocNo = string.Empty;

            if (mIsAutoJournalActived)
                JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveMutationHdr(DocNo, JournalDocNo));
            cml.Add(SaveMutationDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) 
            //        cml.Add(SaveMutationDtl(DocNo, Row));

            cml.Add(SaveStock(DocNo));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo, JournalDocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            ReComputeStock();

            return
                Sm.IsTxtEmpty(DteDocDt, "Date", false) ||
                Sm.IsTxtEmpty(LueWhsCode, "WareHouse", false) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsItCodeNotValid() ||
                IsStockNotValid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt));
        }

        private bool IsStockNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 11) < Sm.GetGrdDec(Grd1, Row, 12))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code (From) : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                            "Item Name (From) : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                            "Batch Number (From) : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Source (From) : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine + Environment.NewLine +
                            "Lot : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                            "Bin : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                            "Stock : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 11), 0) + Environment.NewLine +
                            "Quantity (Inventory 1) : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0) + Environment.NewLine + Environment.NewLine +
                            "Quantity (Inventory 1) is greater than stock.");
                        return true;
                    }

                    if (mNumberOfInventoryUomCode>=2 && Sm.GetGrdDec(Grd1, Row, 14) < Sm.GetGrdDec(Grd1, Row, 15))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code (From) : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                            "Item Name (From) : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                            "Batch Number (From) : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Source (From) : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine + Environment.NewLine +
                            "Lot : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                            "Bin : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                            "Stock : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 14), 0) + Environment.NewLine +
                            "Quantity (Inventory 2) : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 15), 0) + Environment.NewLine + Environment.NewLine +
                            "Quantity (Inventory 2) is greater than stock.");
                        return true;
                    }

                    if (mNumberOfInventoryUomCode==3 && Sm.GetGrdDec(Grd1, Row, 17) < Sm.GetGrdDec(Grd1, Row, 18))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code (From) : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                            "Item Name (From) : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                            "Batch Number (From) : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Source (From) : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine + Environment.NewLine +
                            "Lot : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                            "Bin : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                            "Stock : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 17), 0) + Environment.NewLine +
                            "Quantity (Inventory 3) : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 18), 0) + Environment.NewLine + Environment.NewLine +
                            "Quantity (Inventory 3) is greater than stock.");
                        return true;
                    }
                }
            }
            return false;
        }

        internal string GetSelectedItem(int Col)
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, Col).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, Col) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private bool IsItCodeNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 4)+Sm.GetGrdStr(Grd1, Row, 7), Sm.GetGrdStr(Grd1, Row, 21)+Sm.GetGrdStr(Grd1, Row, 24)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Lot : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + 
                        "Bin : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine + Environment.NewLine + 

                        "Item Code (From) : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + 
                        "Item Name (From) : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine + 
                        "Batch Number (From) : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + 
                        "Source (From) : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine + Environment.NewLine + 

                        "Item Code (To) : " + Sm.GetGrdStr(Grd1, Row, 21) + Environment.NewLine + 
                        "Item Name (To) : " + Sm.GetGrdStr(Grd1, Row, 23) + Environment.NewLine +
                        "Batch Number (To) : " + Sm.GetGrdStr(Grd1, Row, 24) + Environment.NewLine + Environment.NewLine + 

                        "Items should not be the same.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item in list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item from is empty.") ||
                    //Sm.IsGrdValueEmpty(Grd1, Row, 12, true, "Quantity from (Inventory 1) should be greater than 0.") ||
                    //(mNumberOfInventoryUomCode >= 2 && Sm.IsGrdValueEmpty(Grd1, Row, 15, true, "Quantity from (Inventory 2) should be greater than 0.")) ||
                    //(mNumberOfInventoryUomCode==3 && Sm.IsGrdValueEmpty(Grd1, Row, 18, true, "Quantity from (Inventory 3) should be greater than 0.")) ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 21, false, "Item to is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 24, false, "Batch number to is empty.") 
                    //Sm.IsGrdValueEmpty(Grd1, Row, 26, true, "Quantity to (Inventory 1) should be greater than 0.") ||
                    //(mNumberOfInventoryUomCode >= 2 && Sm.IsGrdValueEmpty(Grd1, Row, 28, true, "Quantity to (Inventory 2) should be greater than 0.")) ||
                    //(mNumberOfInventoryUomCode==3 && Sm.IsGrdValueEmpty(Grd1, Row, 30, true, "Quantity to (Inventory 3) should be greater than 0.")) 
                    
                    //|| Sm.IsGrdValueEmpty(Grd1, Row, 31, false, "Item cost's document number is empty.")
                    ) 
                    return true;
            }
            return false;
        }

        private MySqlCommand SaveMutationHdr(string DocNo, string JournalDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblMutationHdr (DocNo, DocDt, WhsCode, JournalDocNo, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DocDt, @WhsCode, @JournalDocNo, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMutationDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Mutation - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblMutationDtl(DocNo, DNo, CancelInd, ItCodeFrom, BatchNoFrom, SourceFrom, Lot, Bin, QtyFrom, Qty2From, Qty3From, ItCodeTo, BatchNoTo, SourceTo, QtyTo, Qty2To, Qty3To, ItemCostDocNo, ItemCostDNo, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", 'N', @ItCodeFrom_" + r.ToString() +
                        ", @BatchNoFrom_" + r.ToString() +
                        ", @SourceFrom_" + r.ToString() +
                        ", @Lot_" + r.ToString() +
                        ", @Bin_" + r.ToString() +
                        ", @QtyFrom_" + r.ToString() +
                        ", @Qty2From_" + r.ToString() +
                        ", @Qty3From_" + r.ToString() +
                        ", @ItCodeTo_" + r.ToString() +
                        ", IfNull(@BatchNoTo_" + r.ToString() +
                        ", ");
                    SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
                    SQL.AppendLine(
                        "), Concat('12*', @DocNo, '*', @DNo_" + r.ToString() +
                        "), @QtyTo_" + r.ToString() +
                        ", @Qty2To_" + r.ToString() +
                        ", @Qty3To_" + r.ToString() +
                        ", @ItemCostDocNo_" + r.ToString() +
                        ", @ItemCostDNo_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ItCodeFrom_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<String>(ref cm, "@BatchNoFrom_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@SourceFrom_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@QtyFrom_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 12));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2From_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3From_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
                    Sm.CmParam<String>(ref cm, "@ItCodeTo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 21));
                    Sm.CmParam<String>(ref cm, "@BatchNoTo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 24));
                    Sm.CmParam<Decimal>(ref cm, "@QtyTo_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 26));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2To_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 28));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3To_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 30));
                    Sm.CmParam<String>(ref cm, "@ItemCostDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 32));
                    Sm.CmParam<String>(ref cm, "@ItemCostDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 34));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 37));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();


            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveMutationDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblMutationDtl(DocNo, DNo, CancelInd, ItCodeFrom, BatchNoFrom, SourceFrom, Lot, Bin, QtyFrom, Qty2From, Qty3From, ItCodeTo, BatchNoTo, SourceTo, QtyTo, Qty2To, Qty3To, ItemCostDocNo, ItemCostDNo, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, 'N', @ItCodeFrom, @BatchNoFrom, @SourceFrom, @Lot, @Bin, @QtyFrom, @Qty2From, @Qty3From, @ItCodeTo, IfNull(@BatchNoTo,");
        //    SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
        //    SQL.AppendLine("), Concat('12*', @DocNo, '*', @DNo), @QtyTo, @Qty2To, @Qty3To, @ItemCostDocNo, @ItemCostDNo, @Remark, @CreateBy, CurrentDateTime());");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@ItCodeFrom", Sm.GetGrdStr(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@BatchNoFrom", Sm.GetGrdStr(Grd1, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@SourceFrom", Sm.GetGrdStr(Grd1, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyFrom", Sm.GetGrdDec(Grd1, Row, 12));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2From", Sm.GetGrdDec(Grd1, Row, 15));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty3From", Sm.GetGrdDec(Grd1, Row, 18));
        //    Sm.CmParam<String>(ref cm, "@ItCodeTo", Sm.GetGrdStr(Grd1, Row, 21));
        //    Sm.CmParam<String>(ref cm, "@BatchNoTo", Sm.GetGrdStr(Grd1, Row, 24));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyTo", Sm.GetGrdDec(Grd1, Row, 26));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2To", Sm.GetGrdDec(Grd1, Row, 28));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty3To", Sm.GetGrdDec(Grd1, Row, 30));
        //    Sm.CmParam<String>(ref cm, "@ItemCostDocNo", Sm.GetGrdStr(Grd1, Row, 32));
        //    Sm.CmParam<String>(ref cm, "@ItemCostDNo", Sm.GetGrdStr(Grd1, Row, 34));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 37));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType1, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCodeFrom, B.BatchNoFrom, B.SourceFrom, -1*B.QtyFrom, -1*B.Qty2From, -1*B.Qty3From, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary As T1  ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select WhsCode, ItCode, BatchNo, Source, Lot, Bin, Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            SQL.AppendLine("    From TblStockMovement ");
            SQL.AppendLine("    Where DocType=@DocType1 And DocNo=@DocNo ");
            SQL.AppendLine("    Group By WhsCode, ItCode, BatchNo, Source, Lot, Bin ");
            SQL.AppendLine(") T2 On T1.WhsCode=T2.WhsCode And T1.ItCode=T2.ItCode And T1.BatchNo=T2.BatchNo And T1.Source=T2.Source And T1.Lot=T2.Lot And T1.Bin=T2.Bin ");
            SQL.AppendLine("Set T1.Qty=T1.Qty+T2.Qty, T1.Qty2=T1.Qty2+T2.Qty2, T1.Qty3=T1.Qty3+T2.Qty3, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCodeTo, B.BatchNoTo, B.SourceTo, B.QtyTo, B.Qty2To, B.Qty3To, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblMutationHdr A, TblMutationDtl B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DocNo=B.DocNo; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCodeTo, B.BatchNoTo, B.SourceTo, B.QtyTo, B.Qty2To, B.Qty3To, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCodeTo, B.BatchNoTo, B.SourceTo, ");
            SQL.AppendLine("Case When IfNull(C.CurCode, '')='' Then E.CurCode ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    (Select ParValue from TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("End As CurCode, ");
            SQL.AppendLine("Case When IfNull(C.CurCode, '')='' Then (E.UPrice*E.ExcRate) ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    (E.UPrice*E.ExcRate)+(D.Cost *");
            SQL.AppendLine("        Case When C.CurCode=(Select ParValue from TblParameter Where ParCode='MainCurCode') Then 1 Else ");
            SQL.AppendLine("            IfNull(( ");
            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=C.CurCode ");
            SQL.AppendLine("                And CurCode2=(Select ParValue from TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ), 0) End ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("End As UPrice, ");
            SQL.AppendLine("Case When IfNull(C.CurCode, '')='' Then E.ExcRate Else 1 End As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblItemCostHdr C On B.ItemCostDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItemCostDtl D On B.ItemCostDocNo=D.DocNo And B.ItemCostDNo=D.DNo ");
            SQL.AppendLine("Left Join TblStockPrice E On B.ItCodeFrom=E.ItCode And B.BatchNoFrom=E.BatchNo And B.SourceFrom=E.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType1", "11");
            Sm.CmParam<String>(ref cm, "@DocType2", "12");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, string JournalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Mutation : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblMutationHdr Where DocNo=@DocNo;");
            

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, @EntCode, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo, A.QtyTo*B.UPrice*B.ExcRate As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblMutationDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.SourceTo=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCodeTo=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select D.AcNo, 0 As DAmt, A.QtyFrom*B.UPrice*B.ExcRate As CAmt ");
            SQL.AppendLine("        From TblMutationDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.SourceFrom=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCodeFrom=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") T Where T.AcNo Is Not Null;  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }
        
        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledMutation();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var JournalDocNo = string.Empty;
            
            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    cml.Add(EditMutationDtl(Row));
            }

            if (mIsAutoJournalActived)
                cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledMutation()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = 
                        "Select DNo, CancelInd From TblMutationDtl " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Index = 0; Index < Grd1.Rows.Count - 1; Index++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Index, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Index, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Index, 2, 1);
                                    break;
                                }
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            ReComputeStock2();
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledMutationNotExisted(DNo) ||
                IsCancelledStockNotEnough();
        }

        private bool IsCancelledMutationNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsCancelledStockNotEnough()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 25).Length > 0)
                {
                    Msg =
                        "Result" + Environment.NewLine + Environment.NewLine +
                        "Item Code : " + Sm.GetGrdStr(Grd1, Row, 21) + Environment.NewLine +
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 23) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd1, Row, 24) + Environment.NewLine +
                        "Source : " + Sm.GetGrdStr(Grd1, Row, 25) + Environment.NewLine +
                        "Lot : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                        "Bin : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine;

                    if (Sm.GetGrdDec(Grd1, Row, 26) > Sm.GetGrdDec(Grd1, Row, 38))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            Msg +
                            "Result (1) : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 26), 0) + Environment.NewLine +
                            "Existing stock (1) : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 38), 0) + Environment.NewLine + Environment.NewLine +
                            "Not enough stock."
                            );
                        return true;
                    }

                    if (Grd1.Cols[28].Visible)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 28) > Sm.GetGrdDec(Grd1, Row, 39))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Result (2) : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 28), 0) + Environment.NewLine +
                                "Existing stock (2) : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 39), 0) + Environment.NewLine + Environment.NewLine +
                                "Not enough stock."
                                );
                            return true;
                        }
                    }

                    if (Grd1.Cols[30].Visible)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 30) > Sm.GetGrdDec(Grd1, Row, 40))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Result (3) : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 30), 0) + Environment.NewLine +
                                "Existing stock (3) : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 40), 0) + Environment.NewLine + Environment.NewLine +
                                "Not enough stock."
                                );
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand EditMutationDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMutationDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@QtyTo, Qty2=Qty2-@Qty2To, Qty3=Qty3-@Qty3To, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@SourceTo;");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@QtyFrom, Qty2=Qty2+@Qty2From, Qty3=Qty3+@Qty3From, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@SourceFrom;");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType1, A.DocNo, B.DNo, 'Y', A.DocDt, ");
            SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCodeFrom, B.PropCodeFrom, B.BatchNoFrom, B.SourceFrom, ");
            SQL.AppendLine("B.QtyFrom, B.Qty2From, B.Qty3From, B.Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, 'Y', A.DocDt, ");
            SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCodeTo, B.PropCodeTo, B.BatchNoTo, B.SourceTo, ");
            SQL.AppendLine("-1*B.QtyTo, -1*B.Qty2To, -1*B.Qty3To, B.Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DocType1", "11");
            Sm.CmParam<String>(ref cm, "@DocType2", "12");
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@SourceFrom", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@SourceTo", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<Decimal>(ref cm, "@QtyFrom", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty2From", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty3From", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@QtyTo", Sm.GetGrdDec(Grd1, Row, 26));
            Sm.CmParam<Decimal>(ref cm, "@Qty2To", Sm.GetGrdDec(Grd1, Row, 28));
            Sm.CmParam<Decimal>(ref cm, "@Qty3To", Sm.GetGrdDec(Grd1, Row, 30));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);
            int No = 1;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2))
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.DNo=@DNo0" + No.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo0" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 0));
                    No += 1;
                }
            }
            Filter = " And (" + Filter + ")";

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblMutationDtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine(Filter.Replace("A.", string.Empty));
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Cancelling Mutation : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, @EntCode, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo, 0 As DAmt, A.QtyTo*B.UPrice*B.ExcRate As CAmt ");
            SQL.AppendLine("        From TblMutationDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.SourceTo=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCodeTo=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter);
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select D.AcNo, A.QtyFrom*B.UPrice*B.ExcRate As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblMutationDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.SourceFrom=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCodeFrom=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter);
            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") T Where T.AcNo Is Not Null;  ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowMutationHdr(DocNo);
                ShowMutationDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowMutationHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                         ref cm,
                         "Select DocNo, DocDt, WhsCode, Remark From TblMutationHdr Where DocNo=@DocNo ",
                         new string[]{ "DocNo","DocDt","WhsCode","Remark" },
                         (MySqlDataReader dr, int[] c) =>
                         {
                             TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                             Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                             Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                             MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                         }, true
                     );
        }

        private void ShowMutationDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.ItCodeFrom, B.ItName As ItNameFrom, A.BatchNoFrom, A.SourceFrom, A.Lot, A.Bin, ");
            SQL.AppendLine("A.QtyFrom, B.InventoryUomCode As InventoryUomCodeFrom, A.Qty2From, B.InventoryUomCode2 As InventoryUomCode2From, A.Qty3From, B.InventoryUomCode3 As InventoryUomCode3From, ");
            SQL.AppendLine("A.ItCodeTo, C.ItName As ItNameTo, A.BatchNoTo, A.SourceTo, ");
            SQL.AppendLine("A.QtyTo, C.InventoryUomCode As InventoryUomCodeTo, A.Qty2To, C.InventoryUomCode2 As InventoryUomCode2To, A.Qty3To, B.InventoryUomCode3 As InventoryUomCode3To, ");
            SQL.AppendLine("A.ItemCostDocNo, A.ItemCostDNo, D.CurCode, E.Cost, A.Remark ");  
            SQL.AppendLine("From TblMutationDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCodeFrom=B.ItCode ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCodeTo=C.ItCode ");
            SQL.AppendLine("Left Join TblItemCostHdr D On A.ItemCostDocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblItemCostDtl E On A.ItemCostDocNo=E.DocNo And A.ItemCostDNo=E.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo ");

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                       "DNo",

                        //1-5
                        "CancelInd", "ItCodeFrom", "ItNameFrom", "BatchNoFrom", "SourceFrom", 
                    
                        //6-10
                        "Lot", "Bin", "QtyFrom", "InventoryUomCodeFrom", "Qty2From", 
                        
                        //11-15
                        "InventoryUomCode2From", "Qty3From", "InventoryUomCode3From", "ItCodeTo", "ItNameTo", 
                        
                        //16-20
                        "BatchNoTo", "SourceTo", "QtyTo", "InventoryUomCodeTo", "Qty2To",  
                        
                        //21-25
                        "InventoryUomCode2To", "Qty3To", "InventoryUomCode3To", "ItemCostDocNo", "ItemCostDNo",  
                        
                        //26-28
                        "CurCode", "Cost", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                        Grd1.Cells[Row, 11].Value = 0m;
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 8);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 9);
                        Grd1.Cells[Row, 14].Value = 0m;
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 10);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 11);
                        Grd1.Cells[Row, 17].Value = 0m;
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 12);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 13);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 14);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 15);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 16);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 25, 17);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 26, 18);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 19);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 28, 20);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 29, 21);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 30, 22);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 31, 23);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 33, 24);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 34, 25);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 35, 26);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 36, 27);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 37, 28);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 14, 15, 17, 18, 26, 28, 30, 36, 38, 39, 40 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsBatchNoUseDocDtIfEmpty', 'IsAutoJournalActived', 'NumberOfInventoryUomCode' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsBatchNoUseDocDtIfEmpty": mIsBatchNoUseDocDtIfEmpty = ParValue == "Y"; break;

                            //Integer
                            case "NumberOfInventoryUomCode":
                                if (ParValue.Length == 0)
                                    mNumberOfInventoryUomCode = 1;
                                else
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetIsShowMutationPriceInfo()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select ParValue From TblParameter ");
            SQL.AppendLine("Where ParCode='MutationPriceInfoGroup' ");
            SQL.AppendLine("And ParValue Like Concat('%#', (Select GrpCode From TblUser Where UserCode=@UserCode),'#%') ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            mIsShowMutationPriceInfo = Sm.IsDataExist(cm);
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private bool IsItemEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        internal string GetSelectedStockSummary()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 7).Replace("'", "''") +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 8, 9, 10);
                            No += 1;
                        }
                    }
                }
                cm.CommandText = SQL.ToString() + " And (" + Filter + ")";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 8), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 9), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 10), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 11, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 17, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ReComputeStock2()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 25).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 25);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 25), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 9), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 10), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 38, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 39, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 40, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ParPrint(int parValue)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<SMut>();
            var ldtl = new List<SMutDtl>();

            string[] TableName = { "SMut", "SMutDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();


            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, A.Remark ");
            SQL.AppendLine("From TblMutationhdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",

                         "WhsName",
                         "Remark",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SMut()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            WhsName = Sm.DrStr(dr, c[6]),
                            HRemark = Sm.DrStr(dr, c[7]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select  A.ItCodeFrom, B.ItName As ItNameFrom, A.BatchNoFrom, A.SourceFrom As Source, A.Lot, A.Bin,  ");
                SQLDtl.AppendLine("A.QtyFrom, B.InventoryUomCode As InventoryUomCodeFrom, ");
                SQLDtl.AppendLine("A.Qty2From, B.InventoryUomCode2 As InventoryUomCode2From, ");
                SQLDtl.AppendLine("A.Qty3From, B.InventoryUomCode3 As InventoryUomCode3From, ");

                SQLDtl.AppendLine("A.ItCodeTo, C.ItName As ItNameTo, A.BatchNoTo, ");
                SQLDtl.AppendLine("A.QtyTo, C.InventoryUomCode As InventoryUomCodeTo, ");
                SQLDtl.AppendLine("A.Qty2To, C.InventoryUomCode2 As InventoryUomCode2To, ");
                SQLDtl.AppendLine("A.Qty3To, C.InventoryUomCode3 As InventoryUomCode3To, ");
                SQLDtl.AppendLine("D.CurCode, E.Cost, A.Remark ");
                SQLDtl.AppendLine("From TblMutationDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCodeFrom=B.ItCode ");
                SQLDtl.AppendLine("Inner Join TblItem C On A.ItCodeTo=C.ItCode ");
                SQLDtl.AppendLine("Left Join TblItemCostHdr D On A.ItemCostDocNo=D.DocNo ");
                SQLDtl.AppendLine("Left Join TblItemCostDtl E On A.ItemCostDocNo=E.DocNo And A.ItemCostDNo=E.DNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And A.CancelInd != 'Y' Order By A.DNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCodeFrom" ,

                         //1-5
                         "ItNameFrom" ,
                         "BatchNoFrom",
                         "Source",
                         "Lot",
                         "Bin",

                         "QtyFrom" ,
                         "InventoryUomCodeFrom",
                         "Qty2From" ,
                         "InventoryUomCode2From" ,
                         "Qty3From" ,

                         "InventoryUomCode3From",
                         "ItCodeTo" ,
                         "ItNameTo" ,
                         "BatchNoTo" ,
                         "QtyTo" ,

                         "InventoryUomCodeTo" ,
                         "Qty2To" ,
                         "InventoryUomCode2To" ,
                         "Qty3To" ,
                         "InventoryUomCode3To",

                         "CurCode" ,
                         "Cost" ,
                         "Remark" ,
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new SMutDtl()
                        {
                            ItCodeFrom = Sm.DrStr(drDtl, cDtl[0]),

                            ItNameFrom = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNoFrom = Sm.DrStr(drDtl, cDtl[2]),
                            Source = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),

                            QtyFrom = Sm.DrDec(drDtl, cDtl[6]),
                            UomCodeFrom = Sm.DrStr(drDtl, cDtl[7]),
                            Qty2From = Sm.DrDec(drDtl, cDtl[8]),
                            UomCode2From = Sm.DrStr(drDtl, cDtl[9]),
                            Qty3From = Sm.DrDec(drDtl, cDtl[10]),

                            UomCode3From = Sm.DrStr(drDtl, cDtl[11]),
                            ItCodeTo = Sm.DrStr(drDtl, cDtl[12]),
                            ItNameTo = Sm.DrStr(drDtl, cDtl[13]),
                            BatchNoTo = Sm.DrStr(drDtl, cDtl[14]),
                            QtyTo = Sm.DrDec(drDtl, cDtl[15]),

                            UomCodeTo = Sm.DrStr(drDtl, cDtl[16]),
                            Qty2To = Sm.DrDec(drDtl, cDtl[17]),
                            UomCode2To = Sm.DrStr(drDtl, cDtl[18]),
                            Qty3To = Sm.DrDec(drDtl, cDtl[19]),
                            UomCode3To = Sm.DrStr(drDtl, cDtl[20]),

                            CurCode = Sm.DrStr(drDtl, cDtl[21]),
                            Cost = Sm.DrDec(drDtl, cDtl[22]),
                            DRemark = Sm.DrStr(drDtl, cDtl[23]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            switch (parValue)
            {
                case 1:
                    Sm.PrintReport("StockMutation", myLists, TableName, false);
                    break;
                case 2:
                    Sm.PrintReport("StockMutation2", myLists, TableName, false);
                    break;
                case 3:
                    Sm.PrintReport("StockMutation3", myLists, TableName, false);
                    break;
            }
        }

        #endregion

        #endregion 
        
        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
        }

        private void MeeRemark_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(MeeRemark);
        }

        #endregion

        #endregion
    }

    #region Report Class

    class SMut
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string WhsName { get; set; }
        public string HRemark { get; set; }
        public string PrintBy { get; set; }
    }

    class SMutDtl
    {
        public string ItCodeFrom { get; set; }
        public string ItNameFrom { get; set; }
        public string BatchNoFrom { get; set; }
        public string Source { get; set; }
        public string Lot { get; set; }
        public string Bin { get; set; }
        public decimal QtyFrom { get; set; }
        public string UomCodeFrom { get; set; }
        public decimal Qty2From { get; set; }
        public string UomCode2From { get; set; }
        public decimal Qty3From { get; set; }
        public string UomCode3From { get; set; }

        public string ItCodeTo { get; set; }
        public string ItNameTo { get; set; }
        public string BatchNoTo { get; set; }
        public decimal QtyTo { get; set; }
        public string UomCodeTo { get; set; }
        public decimal Qty2To { get; set; }
        public string UomCode2To { get; set; }
        public decimal Qty3To { get; set; }
        public string UomCode3To { get; set; }

        public string CurCode { get; set; }
        public decimal Cost { get; set; }
        public string DRemark { get; set; }
    }

    #endregion
}
