﻿#region Update
/*
    13/03/2018 [HAR/HIN] feedback cara hitung PPH 21.
    10/09/2018 [TKG/HIN] menggunakan PKP untuk pesangon.
    27/03/2019 [TKG/HIN] perubahan perhitungan
    09/11/2020 [WED/PHT] tambah tab baru deduction berdasarkan parameter IsEmpSeveranceUseDeduction
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    12/03/2021 [IBL/PHT] Tambah tab baru untuk download attachment
    10/06/2021 [RDH/PHT] tambah filter level code untuk approval setting
 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.Net;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmEmpSeverance : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "",  mDocNo = "";
        internal FrmEmpSeveranceFind FrmFind;
        decimal Retirement = 0, EmployeeFee = 0, EmployerFee = 0; 
        internal string mSeveranceProsentaseCompensationRights = string.Empty,
            mSeveranceDivisorValueLeave = string.Empty, mCorporateFinancialAssistanceAmt = string.Empty, mHIIMInDateCorporateFinancial = string.Empty,
            mHIIPensionCode = string.Empty, mHIIPensionEmployeeProsentase = string.Empty, mHIIPensionEmployerProsentase = string.Empty,
            mNATPensionCode = string.Empty, mNATPensionEmployeeProsentase = string.Empty, mNATPensionEmployerProsentase = string.Empty,
            mRetirementCapitalValue = string.Empty;
        private string mMainCurCode = string.Empty;
        private bool mIsEmpSeveranceUseDeduction = false,
            mIsApprovalEmpSeveranceByLevelCode = false;
        private decimal mInitAccruedExpAmt = 0m;
        internal string
            mHostAddrForFTPClient = string.Empty,
            mPortForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmEmpSeverance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee Severance";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter();

            SetFormControl(mState.View);

            Tpg.SelectTab("TpgDeduction");
            if (!mIsEmpSeveranceUseDeduction) Tpg.TabPages.Remove(TpgDeduction);

            Tpg.SelectTab("TpgSeverance");
            SetLueKoefisien(ref LueDtlKoefisien1);
            SetLueKoefisien(ref LueDtlKoefisien2);

            Tpg.SelectTab("TpgSalary");
            

            base.FrmLoad(sender, e);

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, ChkCancelInd, TxtEmpCode, TxtEmpName, TxtProcess, MeeRemark,
                        TxtSiteName, TxtIDNumber, TxtDeptName, TxtPosition, TxtBirthPlace, DteBirthDt, DteRetiredDt,
                        DteJoinDt, DteLeaveStartDt, TxtYearsWorked, TxtHdrSalary, TxtHdrRetirement, TxtVRDocNo,
                        TxtHdrSeverance, TxtHdrAccruedExp, TxtHdrReceivedPay, TxtHdrRecvDirect,
                        TxtDtlRetirementCapital, TxtDtlSalary, TxtDtlPension, TxtDtlNonPension, TxtDtlPensionType, TxtDtlRetirement, TxtDtlEmpFee, TxtDtlEmprFee,
                        LueDtlKoefisien1, LueDtlKoefisien2, TxtDtlSevGrp1, TxtDtlSevGrp2, TxtDtlSalary1, TxtDtlSalary2,
                        TxtDtlResidual, TxtDtlDivider, TxtDtlSalary3, TxtDtlProsentase, TxtDtlSevLongService, TxtDtlSevPay4,
                        TxtDtlSevPay1, TxtDtlSevPay2, TxtDtlResidual, TxtDtlDivider,TxtDtlSalary3, TxtDtlSevPay3,
                        TxtDtlProsentase, TxtDtlSevLongService, TxtDtlSevPay4, TxtDtlSevPay5, TxtDtlSevPay6,
                        TxtDtlEmprFee2, TxtDtlCorporateFinancial, TxtDtlAccruedExp,TxtDtlCorporateFinancial,
                        TxtDtlEmpFee2, TxtDtlEmprFee3, TxtDtlCorporateFinancial2, TxtDtlCorporateDonation, TxtDtlAccruedExp2, TxtDtlReceivedPay,
                        TxtDtlAccruedExp3, TxtDtlCorporateFinancial3, TxtDtlCorporateDonation2, TxtDtlAmtBeforeTax,
                        TxtDtlTax, TxtDtlTax2, TxtDtlAmtAfterTax,  TxtDtlRetirementCapital3, TxtDtlRetirementCapital2, TxtDtlRetirementCapital,
                        TxtDeductionAmt, MeeDeductionRemark
                    }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { 
                        DteDocDt, TxtDtlRetirement, TxtDtlResidual,
                        LueDtlKoefisien1, LueDtlKoefisien2, MeeRemark, TxtDeductionAmt, MeeDeductionRemark
                    }, false);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { 
                        MeeCancelReason 
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        public void ClearData()
        {
            mInitAccruedExpAmt = 0m;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtDtlTax, TxtDtlTax2, TxtDtlAmtAfterTax, TxtDtlRetirementCapital, TxtDtlRetirementCapital2, TxtDtlRetirementCapital,
                TxtDtlAccruedExp3, TxtDtlCorporateFinancial3, TxtDtlCorporateDonation2, TxtDtlAmtBeforeTax,
                TxtDtlEmpFee2, TxtDtlEmprFee3, TxtDtlCorporateFinancial2, TxtDtlCorporateDonation, TxtDtlAccruedExp2, TxtDtlReceivedPay,
                TxtDtlEmprFee2, TxtDtlCorporateFinancial, TxtDtlAccruedExp,
                TxtDtlProsentase, TxtDtlSevLongService, TxtDtlSevPay4, TxtDtlSevPay5, TxtDtlSevPay6,
                TxtDtlResidual, TxtDtlDivider,TxtDtlSalary3, TxtDtlSevPay3,TxtDtlRetirementCapital3,
                TxtDtlSevPay1, TxtDtlSevPay2, TxtDtlSevPay5,TxtDtlSevGrp1, TxtDtlSevGrp2,
                TxtDtlRetirementCapital, TxtDtlSalary, TxtDtlSalary1, TxtDtlSalary2, TxtDtlSalary3,
                TxtDtlPension, TxtDtlNonPension, TxtDtlRetirement, TxtDtlEmpFee, TxtDtlEmprFee,
                TxtHdrSeverance, TxtHdrAccruedExp, TxtHdrReceivedPay, TxtHdrRecvDirect, 
                TxtYearsWorked, TxtHdrSalary, TxtHdrRetirement, TxtDeductionAmt
            }, 0);

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, TxtEmpCode, TxtEmpName,
                TxtSiteName, TxtIDNumber, TxtDeptName, TxtPosition, TxtBirthPlace, DteBirthDt,
                DteJoinDt, DteLeaveStartDt, DteRetiredDt, LueDtlKoefisien1, LueDtlKoefisien2, 
                TxtVRDocNo, MeeRemark, MeeDeductionRemark
            });
        }

        public void ClearData2()
        {
            mInitAccruedExpAmt = 0m;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtDtlTax, TxtDtlTax2, TxtDtlAmtAfterTax, TxtDtlRetirementCapital, TxtDtlRetirementCapital2, TxtDtlRetirementCapital,
                TxtDtlAccruedExp3, TxtDtlCorporateFinancial3, TxtDtlCorporateDonation2, TxtDtlAmtBeforeTax,
                TxtDtlEmpFee2, TxtDtlEmprFee3, TxtDtlCorporateFinancial2, TxtDtlCorporateDonation, TxtDtlAccruedExp2, TxtDtlReceivedPay,
                TxtDtlEmprFee2, TxtDtlCorporateFinancial, TxtDtlAccruedExp,
                TxtDtlProsentase, TxtDtlSevLongService, TxtDtlSevPay4, TxtDtlSevPay5, TxtDtlSevPay6,
                TxtDtlResidual, TxtDtlDivider,TxtDtlSalary3, TxtDtlSevPay3,
                TxtDtlSevPay1, TxtDtlSevPay2, TxtDtlSevPay5,TxtDtlSevGrp1, TxtDtlSevGrp2,
                TxtDtlRetirementCapital, TxtDtlSalary, TxtDtlSalary1, TxtDtlSalary2, TxtDtlSalary3,
                TxtDtlPension, TxtDtlNonPension, TxtDtlRetirement, TxtDtlEmpFee, TxtDtlEmprFee,
                TxtHdrSeverance, TxtHdrAccruedExp, TxtHdrReceivedPay, TxtHdrRecvDirect, 
                TxtYearsWorked, TxtHdrSalary, TxtHdrRetirement, TxtDeductionAmt
            }, 0);

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                MeeCancelReason, TxtEmpCode, TxtEmpName,
                TxtSiteName, TxtIDNumber, TxtDeptName, TxtPosition, TxtBirthPlace, DteBirthDt,
                DteJoinDt, DteLeaveStartDt, DteRetiredDt, LueDtlKoefisien1, LueDtlKoefisien2, 
                TxtVRDocNo, MeeRemark, MeeDeductionRemark
            });
        }

    
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpSeveranceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.Text = "Outstanding";
                TxtDtlProsentase.EditValue = Sm.FormatNum(Decimal.Parse(mSeveranceProsentaseCompensationRights), 0);
                TxtDtlDivider.EditValue = Sm.FormatNum(Decimal.Parse(mSeveranceDivisorValueLeave), 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    UpdateData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View); 
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            string[] FileNames;

            FileNames = TxtFile.Text.Split(',');

            foreach (var FileName in FileNames)
            {
                DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, FileName, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                SFD.FileName = FileName;
                SFD.DefaultExt = "pdf";
                SFD.AddExtension = true;

                if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
                {

                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Application.DoEvents();

                        //Write the bytes to a file
                        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                        newFile.Write(downloadedData, 0, downloadedData.Length);
                        newFile.Close();
                        MessageBox.Show("Saved Successfully");
                    }
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpSeverance", "TblEmpSeverance");
            bool NoNeedApproval = IsDocApprovalSettingNotExisted();
            string VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpSeverance(DocNo, VoucherRequestDocNo, NoNeedApproval));

            cml.Add(SaveVoucherRequest(VoucherRequestDocNo, DocNo));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document's date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee", false) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark")
                ;
        }

        private bool IsDocApprovalSettingNotExisted()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocType From TblDocApprovalSetting Where UserCode Is not Null And DocType='EmpSeverance'");

            if (mIsApprovalEmpSeveranceByLevelCode)
            {
                SQL.AppendLine("AND LevelCode is not null");
                SQL.AppendLine("AND FIND_IN_SET(");
                SQL.AppendLine("levelCode,(select levelCode from tblemployee where EmpCode = @EmpCode)");
                SQL.AppendLine(" )");
            }
            
            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);


            if (!Sm.IsDataExist(cm))
                return true;
            else
                return false;
        }

        private MySqlCommand SaveEmpSeverance(string DocNo, string VoucherRequestDocNo, bool NoNeedApproval)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmpSeverance ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, CancelReason, Status, Process, ");
            SQL.AppendLine("EmpCode, NumberWorkYr, InitialDt, RetiredDt, PensionBasicSalary, PensionNonBasicSalary, ");
            SQL.AppendLine("BasicSalary, PensionType, RetirementBenefit, EmpFee, EmprFee, RetirementCapital,  ");
            SQL.AppendLine("Coefficient, Coefficient2, SevGrp, SevGrp2, ResidualLeave, ");
            SQL.AppendLine("Divider, ResidualAmt, SevPay, LongSevpay, CompensationRightsPay, Prosentase, ");
            SQL.AppendLine("SevAmt, AccruedExp, AccruedExpBfDeduction, DeductionAmt, DeductionRemark, CorporateFinancial, CorporateDonation, TaxAmt1, ");
            SQL.AppendLine("RecvPay, AmtBfTax, TaxAmt2, AmtAfTax, VoucherRequestDocNo, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt )");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @CancelInd, @CancelReason, 'O', @Process, ");
            SQL.AppendLine("@EmpCode, @NumberWorkYr, @InitialDt, @RetiredDt,  @PensionBasicSalary, @PensionNonBasicSalary, ");
            SQL.AppendLine("@BasicSalary, @PensionType, @RetirementBenefit, @EmpFee, @EmprFee, @RetirementCapital, ");
            SQL.AppendLine("@Coefficient, @Coefficient2, @SevGrp, @SevGrp2, @ResidualLeave, ");
            SQL.AppendLine("@Divider, @ResidualAmt, @SevPay, @LongSevpay, @CompensationRightsPay, @Prosentase, ");
            SQL.AppendLine("@SevAmt, @AccruedExp, @AccruedExpBfDeduction, @DeductionAmt, @DeductionRemark, @CorporateFinancial, @CorporateDonation, @TaxAmt1, ");
            SQL.AppendLine("@RecvPay, @AmtBfTax, @TaxAmt2, @AmtAfTax, @VoucherRequestDocNo, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='EmpSeverance' ");

                if (mIsApprovalEmpSeveranceByLevelCode)
                {
                    SQL.AppendLine("And LevelCode is not null");
                    SQL.AppendLine("And FIND_IN_SET(");
                    SQL.AppendLine("    LevelCode,(Select LevelCode From TblEmployee Where EmpCode = @EmpCode)");
                    SQL.AppendLine(" )");
                }
            }

            SQL.AppendLine("Update TblEmpSeverance Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpSeverance' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@Process", TxtProcess.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@NumberWorkYr", decimal.Parse(TxtYearsWorked.Text));
            Sm.CmParamDt(ref cm, "@InitialDt", Sm.GetDte(DteLeaveStartDt));
            Sm.CmParamDt(ref cm, "@RetiredDt", Sm.GetDte(DteRetiredDt));
            Sm.CmParam<Decimal>(ref cm, "@PensionBasicSalary", decimal.Parse(TxtDtlPension.Text));
            Sm.CmParam<Decimal>(ref cm, "@PensionNonBasicSalary", decimal.Parse(TxtDtlNonPension.Text));
            Sm.CmParam<Decimal>(ref cm, "@BasicSalary", decimal.Parse(TxtDtlSalary.Text));
            Sm.CmParam<String>(ref cm, "@PensionType", TxtDtlPensionType.Text);
            Sm.CmParam<Decimal>(ref cm, "@RetirementBenefit", decimal.Parse(TxtDtlRetirement.Text));
            Sm.CmParam<Decimal>(ref cm, "@EmpFee", decimal.Parse(TxtDtlEmpFee.Text));
            Sm.CmParam<Decimal>(ref cm, "@EmprFee", decimal.Parse(TxtDtlEmprFee.Text));
            Sm.CmParam<Decimal>(ref cm, "@RetirementCapital", decimal.Parse(TxtDtlRetirementCapital.Text));
            Sm.CmParam<Decimal>(ref cm, "@Coefficient", Sm.GetLue(LueDtlKoefisien1).Length > 0 ? decimal.Parse(Sm.GetLue(LueDtlKoefisien1)) : 0m);
            Sm.CmParam<Decimal>(ref cm, "@Coefficient2", Sm.GetLue(LueDtlKoefisien2).Length > 0 ? decimal.Parse(Sm.GetLue(LueDtlKoefisien2)) : 0m);
            Sm.CmParam<Decimal>(ref cm, "@SevGrp", decimal.Parse(TxtDtlSevGrp1.Text));
            Sm.CmParam<Decimal>(ref cm, "@SevGrp2", decimal.Parse(TxtDtlSevGrp2.Text));
            Sm.CmParam<Decimal>(ref cm, "@ResidualLeave", decimal.Parse(TxtDtlResidual.Text));
            Sm.CmParam<Decimal>(ref cm, "@Divider", decimal.Parse(TxtDtlDivider.Text));
            Sm.CmParam<Decimal>(ref cm, "@ResidualAmt", decimal.Parse(TxtDtlSevPay3.Text));
            Sm.CmParam<Decimal>(ref cm, "@SevPay", decimal.Parse(TxtDtlSevPay1.Text));
            Sm.CmParam<Decimal>(ref cm, "@LongSevpay", decimal.Parse(TxtDtlSevPay2.Text));
            Sm.CmParam<Decimal>(ref cm, "@CompensationRightsPay", decimal.Parse(TxtDtlSevPay4.Text));
            Sm.CmParam<Decimal>(ref cm, "@Prosentase", decimal.Parse(TxtDtlProsentase.Text));
            Sm.CmParam<Decimal>(ref cm, "@SevAmt", decimal.Parse(TxtDtlSevPay5.Text));
            Sm.CmParam<Decimal>(ref cm, "@AccruedExp", decimal.Parse(TxtDtlAccruedExp.Text));
            Sm.CmParam<Decimal>(ref cm, "@AccruedExpBfDeduction", mIsEmpSeveranceUseDeduction ? mInitAccruedExpAmt : decimal.Parse(TxtDtlAccruedExp.Text));
            Sm.CmParam<Decimal>(ref cm, "@DeductionAmt", mIsEmpSeveranceUseDeduction ? decimal.Parse(TxtDeductionAmt.Text) : 0m);
            Sm.CmParam<String>(ref cm, "@DeductionRemark", mIsEmpSeveranceUseDeduction ? MeeDeductionRemark.Text : string.Empty);
            Sm.CmParam<Decimal>(ref cm, "@CorporateFinancial", decimal.Parse(TxtDtlCorporateFinancial.Text));
            Sm.CmParam<Decimal>(ref cm, "@CorporateDonation", decimal.Parse(TxtDtlCorporateDonation.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", decimal.Parse(TxtDtlTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtDtlTax2.Text));
            Sm.CmParam<Decimal>(ref cm, "@RecvPay", decimal.Parse(TxtDtlReceivedPay.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtBfTax", decimal.Parse(TxtDtlAmtBeforeTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtAfTax", decimal.Parse(TxtDtlAmtAfterTax.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty,
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    string
        //        Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
        //        Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle"),
        //        DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
        //        type = string.Empty;

        //    var SQL = new StringBuilder();

        //    if (type == string.Empty)
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
        //    }
        //    else
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
        //    }
        //    return Sm.GetValue(SQL.ToString());
        //}


        private MySqlCommand SaveVoucherRequest(string VRDocNo, string EmpSeveranceDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, ");
            SQL.AppendLine("DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, OpeningDt, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='EmpSeveranceDeptCode'), '25', Null, 'C', Null, null, null, null, null, null, @CreateBy, 0, @CurCode, @Amt, Null, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', @Remark, @Amt, Null, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpSeverance' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", EmpSeveranceDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VRDocNo);
            Sm.CmParam<String>(ref cm, "@CurCode", mMainCurCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<decimal>(ref cm, "@Amt", decimal.Parse(TxtDtlAmtAfterTax.Text));
            Sm.CmParam<String>(ref cm, "@Remark", string.Concat("Employee Severance : ", "" + EmpSeveranceDocNo + "", " ", MeeRemark.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void UpdateData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsUpdateDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateEmpSeverance());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsUpdateDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsEmpSeveranceNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataCancelledByApproval() ||
                IsDataProcessedAlready(); 
                ;
        }

        private bool IsDataCancelledByApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblEmpSeverance ");
            SQL.AppendLine("Where Status='C' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblEmpSeverance");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsEmpSeveranceNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVRDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private MySqlCommand UpdateEmpSeverance()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpSeverance Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo AND Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVRDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpSeverance(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpSeverance(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select  ");
            SQL.AppendLine("A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, case When A.Status = 'O' then 'Outstanding' ");
            SQL.AppendLine("When A.Status = 'A' then 'Approve' When A.Status = 'C' Then 'cancelled' End As StatusDocNo, ");
            SQL.AppendLine("A.Process, B.EmpCode, B.EmpName, C.SiteName, B.Idnumber, D.Deptname,  ");
            SQL.AppendLine("E.PosName, B.BirthPlace, B.BirtHDt, B.JoinDt, A.InitialDt, A.RetiredDt, A.NumberWorkYr, A.VoucherRequestDocNo, A.Remark, ");
            SQL.AppendLine("A.PensionBasicSalary, A.PensionNonBasicSalary, A.basicSalary, A.PensionType,   ");
            SQL.AppendLine("A.RetirementBenefit, A.EmpFee, A.EmprFee, A.RetirementCapital, A.Coefficient, A.SevGrp, A.SevPay,  ");
            SQL.AppendLine("A.Coefficient2, A.SevGrp2, A.LongSevpay, A.ResidualLeave, A.Divider, A.ResidualAmt,");
            SQL.AppendLine("(A.Sevpay+A.LongSevpay) As TotalSev, A.Prosentase, A.CompensationRightsPay, A.SevAmt, ");
            SQL.AppendLine("A.CorporateFinancial, A.AccruedExp, A.AccruedExpBfDeduction, A.DeductionAmt, A.DeductionRemark, A.CorporateDonation, A.RecvPay, A.AmtBfTax, A.TaxAmt1, A.TaxAmt2, AmtAfTax, A.FileName  ");
            SQL.AppendLine("from tblEmpseverance A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("left Join tblSite C On B.SiteCode = C.SiteCode ");
            SQL.AppendLine("inner Join tblDepartment D On B.DeptCode = D.DeptCode ");
            SQL.AppendLine("left Join TblPosition E On B.PosCode = E.PosCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CancelInd", "CancelReason", "StatusDocNo", "EmpCode", 
                        //6-10
                        "EmpName", "SiteName", "Idnumber", "Deptname", "PosName", 
                        //11-15
                        "BirthPlace", "BirtHDt", "JoinDt", "InitialDt", "RetiredDt", 
                        //16-20
                        "NumberWorkYr", "VoucherRequestDocNo", "Remark", "PensionBasicSalary", "PensionNonBasicSalary", 
                        //21-25
                        "basicSalary", "PensionType", "RetirementBenefit", "EmpFee", "EmprFee", 
                        //26-30
                        "RetirementCapital", "Coefficient", "SevGrp", "SevPay", "Coefficient2", 
                        //31-35
                        "SevGrp2", "LongSevpay", "ResidualLeave", "Divider", "ResidualAmt",
                        //36-40
                        "TotalSev", "Prosentase", "CompensationRightsPay", "SevAmt","CorporateFinancial", 
                        //41-45
                        "AccruedExp", "CorporateDonation", "RecvPay", "AmtBfTax", "TaxAmt1", 
                        //46-50
                        "TaxAmt2", "AmtAfTax", "Process", "AccruedExpBfDeduction", "DeductionAmt",
                        //51-52
                        "DeductionRemark", "FileName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[6]);
                        TxtSiteName.EditValue = Sm.DrStr(dr, c[7]);
                        TxtIDNumber.EditValue = Sm.DrStr(dr, c[8]);
                        TxtDeptName.EditValue = Sm.DrStr(dr, c[9]);
                        TxtPosition.EditValue = Sm.DrStr(dr, c[10]);
                        TxtBirthPlace.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[12]));
                        Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[13]));
                        Sm.SetDte(DteLeaveStartDt, Sm.DrStr(dr, c[14]));
                        Sm.SetDte(DteRetiredDt, Sm.DrStr(dr, c[15]));
                        TxtYearsWorked.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                        TxtVRDocNo.EditValue =Sm.DrStr(dr, c[17]); 
                        MeeRemark.EditValue = Sm.DrStr(dr, c[18]);
                        //tab 1
                        TxtDtlPension.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                        TxtDtlNonPension.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[20]), 0);
                        TxtDtlSalary.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[21]), 0);
                        //tab 2
                        TxtDtlPensionType.EditValue = Sm.DrStr(dr, c[22]);
                        TxtDtlRetirement.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[23]), 0);
                        TxtDtlEmpFee.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[24]), 0);
                        TxtDtlEmprFee.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[25]), 0);
                        TxtDtlRetirementCapital.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[26]), 0);
                        //tab 3
                        SetLueKoefisien(ref LueDtlKoefisien1);
                        Sm.SetLue(LueDtlKoefisien1, Sm.DrStr(dr, c[27]));
                        TxtDtlSevGrp1.EditValue = Sm.DrStr(dr, c[28]);
                        TxtDtlSalary1.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[21]), 0);
                        TxtDtlSevPay1.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[29]), 0);
                        SetLueKoefisien(ref LueDtlKoefisien2);
                        Sm.SetLue(LueDtlKoefisien2, Sm.DrStr(dr, c[30]));
                        TxtDtlSevGrp2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[31]), 0);
                        TxtDtlSalary2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[21]), 0);
                        TxtDtlSevPay2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[32]), 0);
                        TxtDtlResidual.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[33]), 0);
                        TxtDtlDivider.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[34]), 0);
                        TxtDtlSalary3.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[21]), 0);
                        TxtDtlSevPay3.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[35]), 0);
                        TxtDtlSevLongService.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[36]), 0);
                        TxtDtlProsentase.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[37]), 0);
                        TxtDtlSevPay4.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[38]), 0);
                        TxtDtlSevPay5.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[39]), 0);
                        //tab 4
                        TxtDtlSevPay6.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[39]), 0);
                        TxtDtlEmprFee2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[25]), 0);
                        TxtDtlCorporateFinancial.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[40]), 0);
                        TxtDtlRetirementCapital2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[26]), 0);
                        TxtDtlAccruedExp.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[41]), 0);
                        //tab 5
                        TxtDtlEmpFee2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[24]), 0);
                        TxtDtlEmprFee3.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[25]), 0);
                        TxtDtlCorporateFinancial2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[40]), 0);
                        TxtDtlCorporateDonation.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[42]), 0);
                        TxtDtlAccruedExp2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[41]), 0);
                        TxtDtlRetirementCapital3.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[26]), 0);
                        TxtDtlReceivedPay.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[43]), 0);
                        //tab 6
                        TxtDtlAccruedExp3.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[41]), 0);
                        TxtDtlCorporateFinancial3.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[40]), 0);
                        TxtDtlCorporateDonation2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[42]), 0);
                        TxtDtlAmtBeforeTax.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[44]), 0);
                        TxtDtlTax.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[45]), 0);
                        TxtDtlTax2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[46]), 0);
                        TxtDtlAmtAfterTax.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[47]), 0);
                        //Hdr
                        TxtHdrSalary.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[21]), 0);
                        TxtHdrRetirement.EditValue =  Sm.FormatNum(Sm.DrStr(dr, c[23]), 0);
                        TxtHdrSeverance.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[39]), 0);
                        TxtHdrAccruedExp.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[41]), 0);
                        TxtHdrReceivedPay.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[43]), 0);
                        TxtHdrRecvDirect.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[47]), 0);
                        TxtProcess.EditValue = Sm.DrStr(dr, c[48]);
                        if (mIsEmpSeveranceUseDeduction)
                        {
                            // tab 7 deduction
                            mInitAccruedExpAmt = Sm.DrDec(dr, c[49]);
                            TxtDeductionAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[50]), 0);
                            MeeDeductionRemark.EditValue = Sm.DrStr(dr, c[51]);
                        }

                        //tab attachment
                        TxtFile.EditValue = Sm.DrStr(dr, c[52]);
                    }, true
                );
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mSeveranceProsentaseCompensationRights = Sm.GetParameter("SeveranceProsentaseCompensationRights");
            mSeveranceDivisorValueLeave = Sm.GetParameter("SeveranceDivisorValueLeave");
            mCorporateFinancialAssistanceAmt = Sm.GetParameter("CorporateFinancialAssistanceAmt");
            mHIIPensionCode =  Sm.GetParameter("HIIPensionCode");
            mHIIPensionEmployeeProsentase = Sm.GetParameter("HIIPensionEmployeeProsentase");
            mHIIPensionEmployerProsentase = Sm.GetParameter("HIIPensionEmployerProsentase");
            mNATPensionCode = Sm.GetParameter("NATPensionCode");
            mNATPensionEmployeeProsentase = Sm.GetParameter("NATPensionEmployeeProsentase");
            mNATPensionEmployerProsentase = Sm.GetParameter("NATPensionEmployerProsentase");
            mRetirementCapitalValue = Sm.GetParameter("RetirementCapitalValue");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mHIIMInDateCorporateFinancial = Sm.GetParameter("HIIMInDateCorporateFinancial");
            mIsEmpSeveranceUseDeduction = Sm.GetParameterBoo("IsEmpSeveranceUseDeduction");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mIsApprovalEmpSeveranceByLevelCode = Sm.GetParameterBoo("IsApprovalEmpSeveranceByLevelCode");
        }


        public void ShowEmpData(string EmpCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.EmpCode, A.EmpName, D.SiteName, A.IDNumber,  B.DeptName, C.PosName, A.BirthPlace, A.BirthDt, A.JoinDt, A.LeaveStartDt, E.SSname, A.ResignDt  " +
                    "From TblEmployee A " +
                    "Inner Join TblDepartment B On A.DeptCode = B.DeptCode "+
                    "Left Join TblPosition C On A.PosCode = C.PosCode "+
                    "Left Join TblSite D on A.SiteCode = D.SiteCode "+
                    "Left Join ( "+
                    "   Select A.EmpCode, B.SSName  "+
                    "   From tblemployeess A "+
                    "   Inner Join TblSS B On A.SSCode = B.SSCode "+
                    "   Where A.SSCode In  "+
                    "   ( "+
                    "       Select SSCode From TblSS "+
                    "       Where SSpCode=(Select Parvalue From Tblparameter Where parCode='SSProgramForPension')  "+
                    "   ) "+
                    ")E On A.EmpCode=E.EmpCode "+
                    "Where A.EmpCode=@EmpCode ",
                    new string[] 
                    { 
                        //0
                        "EmpCode", 

                        //1-5
                        "EmpName", "SiteName", "IDNUmber", "DeptName", "PosName",  
                        
                        //6-10
                        "BirthPlace", "BirthDt", "JoinDt", "LeaveStartDt", "SSname",
                        //11
                        "ResignDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtSiteName.EditValue = Sm.DrStr(dr, c[2]);
                        TxtIDNumber.EditValue = Sm.DrStr(dr, c[3]);
                        TxtDeptName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPosition.EditValue = Sm.DrStr(dr, c[5]);
                        TxtBirthPlace.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[7]));
                        Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[8]));
                        Sm.SetDte(DteLeaveStartDt, Sm.DrStr(dr, c[9]));
                        TxtDtlPensionType.EditValue = Sm.DrStr(dr, c[10]);
                        Sm.SetDte(DteRetiredDt, Sm.DrStr(dr, c[11]));
                    }, true
                );
        }

        #region tab 1
        public void SetServeranceGrp()
        {
            decimal servGrp = 0;
            decimal servGrp2 = 0;
            decimal YrWorked = 0;

            if (TxtYearsWorked.Text.Length > 0)
            {
                decimal MaxUP = 0,MaxUPH = 0;
                YrWorked = decimal.Parse(TxtYearsWorked.Text);
                string MaxUPStr = Sm.GetValue("Select MAX(Amt) From TblSeverance Where SeveranceType = 'UP' ");
                string MaxUPHStr = Sm.GetValue("Select MAX(Amt) From TblSeverance Where SeveranceType = 'UPH' ");
                
                if(MaxUPStr.Length>0)
                     MaxUP = decimal.Parse(MaxUPStr);

                if(MaxUPHStr.Length>0)
                     MaxUPH = decimal.Parse(MaxUPHStr);


                if (YrWorked > MaxUP)
                {
                    servGrp = MaxUP;
                }
                else
                {
                     string ServGrpString = Sm.GetValue("Select Amt From TblSeverance Where YearsWorked = '" + YrWorked + "' And SeveranceType = 'UP' ");
                     if (ServGrpString.Length > 0)
                     {
                        servGrp = Sm.Round(Decimal.Parse(ServGrpString), 2);
                     }
                }

                if (YrWorked > MaxUPH)
                {
                    servGrp2 = MaxUPH;
                }
                else
                {
                    string ServGrpString2 = Sm.GetValue("Select Amt From TblSeverance Where YearsWorked = '" + YrWorked + "' And SeveranceType = 'UPH' ");
                    if (ServGrpString2.Length > 0)
                    {
                        servGrp2 = Sm.Round(Decimal.Parse(ServGrpString2), 2);
                    }
                }
                
                TxtDtlSevGrp1.EditValue = Sm.FormatNum(servGrp, 0);
                TxtDtlSevGrp2.EditValue = Sm.FormatNum(servGrp2, 0);
            }
        }
        #endregion

        #region tab 2
        private void ComputeBenefit()
        {
            string SSCode = string.Empty;
            decimal DtlRetirementCapital = 0, DtlPension = 0;

            if (TxtDtlPensionType.Text.Length > 0)
                SSCode = Sm.GetValue("Select SSCode From TblSS Where SSname = '" + TxtDtlPensionType.Text + "'");

            if (SSCode == mHIIPensionCode && TxtDtlRetirement.Text.Length > 0)
            {
                Retirement = Decimal.Parse(TxtDtlRetirement.Text);

                EmployeeFee = Decimal.Parse(mHIIPensionEmployeeProsentase)*0.01m * Retirement;
                TxtDtlEmpFee.EditValue = Sm.FormatNum(EmployeeFee, 0);
                TxtDtlEmpFee2.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlEmpFee.Text), 0);

                EmployerFee = Decimal.Parse(mHIIPensionEmployerProsentase) *0.01m * Retirement;
                TxtDtlEmprFee.EditValue = Sm.FormatNum(EmployerFee, 0);
                TxtDtlEmprFee2.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlEmprFee.Text), 0);
                TxtDtlEmprFee3.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlEmprFee.Text), 0);

                DtlRetirementCapital = Decimal.Parse(mRetirementCapitalValue);
                DtlPension = Decimal.Parse(TxtDtlPension.Text);

                TxtDtlRetirementCapital.EditValue = Sm.FormatNum((DtlRetirementCapital * DtlPension), 0);
                TxtDtlRetirementCapital2.EditValue = Sm.FormatNum((DtlRetirementCapital * DtlPension), 0);
                TxtDtlRetirementCapital3.EditValue = Sm.FormatNum((DtlRetirementCapital * DtlPension), 0);
            }
            else if (SSCode == mNATPensionCode && TxtDtlRetirement.Text.Length > 0)
            {
                Retirement = Decimal.Parse(TxtDtlRetirement.Text);

                EmployeeFee = Decimal.Parse(mNATPensionEmployeeProsentase) *0.01m * Retirement;
                TxtDtlEmpFee.EditValue = Sm.FormatNum(EmployeeFee, 0);
                TxtDtlEmpFee2.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlEmpFee.Text), 0);

                EmployerFee = Decimal.Parse(mNATPensionEmployerProsentase) *0.01m * Retirement;
                TxtDtlEmprFee.EditValue = Sm.FormatNum(EmployerFee, 0);
                TxtDtlEmprFee2.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlEmprFee.Text), 0);
                TxtDtlEmprFee3.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlEmprFee.Text), 0);
            }

            TxtHdrRetirement.EditValue = Sm.FormatNum(Retirement,0);
        }
        #endregion

        #region tab 3
        private void ComputeSeverance1()
        {
            decimal koefisien = 0;
            if (Sm.GetLue(LueDtlKoefisien1).Length > 0)
                koefisien = Decimal.Parse(Sm.GetLue(LueDtlKoefisien1));

            TxtDtlSevPay1.EditValue = Sm.FormatNum((koefisien * Decimal.Parse(TxtDtlSevGrp1.Text) * Decimal.Parse(TxtDtlSalary1.Text)), 0);
            TxtDtlSevLongService.EditValue = Sm.FormatNum((Decimal.Parse(TxtDtlSevPay1.Text) + Decimal.Parse(TxtDtlSevPay2.Text)), 0);

            ComputeSeverance4();
            ComputeSeverance5();
            ComputeAmtBeforeTax();
            ComputeAmtAfterTax();
        }

        private void ComputeSeverance2()
        {
            decimal koefisien = 0;
            if (Sm.GetLue(LueDtlKoefisien2).Length > 0)
                koefisien = Decimal.Parse(Sm.GetLue(LueDtlKoefisien2));

            TxtDtlSevPay2.EditValue = Sm.FormatNum((koefisien * Decimal.Parse(TxtDtlSevGrp2.Text) * Decimal.Parse(TxtDtlSalary2.Text)), 0);
            TxtDtlSevLongService.EditValue = Sm.FormatNum((Decimal.Parse(TxtDtlSevPay1.Text) + Decimal.Parse(TxtDtlSevPay2.Text)), 0);
            
            ComputeSeverance4(); 
            ComputeSeverance5();
        }
        
        private void ComputeSeverance3()
        {
            //decimal residuLeave = 0;
            decimal residuAmt = 0;
            decimal divider = 0;
            //string ResLeave = Sm.GetValue("Select ifnull(Balance, 0) As balance From tblLeaveSummary Where EmpCode = '"+TxtEmpCode.Text+"' And Yr = '"+Sm.Left(Sm.ServerCurrentDateTime(), 4)+"' ;");
            //if (ResLeave.Length > 0)
            //{
            //residuLeave = Decimal.Parse(ResLeave);
            TxtDtlResidual.EditValue = Sm.FormatNum(0m, 0);
            //}

            string div = mSeveranceDivisorValueLeave;
            if (div.Length > 0)
            {
                divider = Decimal.Parse(div);
                TxtDtlDivider.EditValue = Sm.FormatNum(divider, 0);
            }

            //if(residuLeave>0)
            //{
            //    residuAmt = (residuLeave / divider) * Decimal.Parse(TxtDtlSalary3.Text);
            //    TxtDtlSevPay3.EditValue = Sm.FormatNum(residuAmt, 0);
            //}
            TxtDtlSevPay3.EditValue = Sm.FormatNum(0m, 0);
           
            ComputeSeverance4();
            ComputeSeverance5();
            ComputeAccruedExpenses();
        }

        private void ComputeSeverance4()
        {
            decimal prosen = 0;
            decimal sevLong = 0;
            decimal compen = 0;

            string ProsenStr = mSeveranceProsentaseCompensationRights;
            if (ProsenStr.Length > 0)
            {
                prosen = Decimal.Parse(ProsenStr) / 100;
            }

            if (prosen > 0)
            {
                sevLong = Decimal.Parse(TxtDtlSevLongService.Text);
                compen = (prosen * Decimal.Parse(TxtDtlSevLongService.Text));
                TxtDtlSevPay4.EditValue = Sm.FormatNum(compen, 0);
            }

            TxtDtlProsentase.EditValue = Sm.FormatNum(ProsenStr, 0);

            ComputeSeverance5();
        }

        private void ComputeSeverance5()
        {
            TxtDtlSevPay5.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlSevPay1.Text) + 
                Decimal.Parse(TxtDtlSevPay2.Text) -
                Decimal.Parse(TxtDtlSevPay3.Text)+
                Decimal.Parse(TxtDtlSevPay4.Text), 0);
            TxtDtlSevPay6.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlSevPay5.Text),0);

            TxtHdrSeverance.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlSevPay5.Text), 0);
            ComputeAmtBeforeTax();
            ComputeAmtAfterTax();
        }
        #endregion

        #region tab 4
        private void ComputeAccruedExpenses()
        {
            decimal CorporateFinancialAssistanceAmt = 0;
            decimal DtlSevPay6 = 0;
            decimal DtlCorporateFinancial = 0;
            decimal DtlEmprFee2=0;
            string SSCode = string.Empty;
            decimal DtlRetirementCapital2 = 0;

            string CorporateFinancialAssistanceAmtStr = mCorporateFinancialAssistanceAmt;
            if (CorporateFinancialAssistanceAmtStr.Length > 0)
            {
                CorporateFinancialAssistanceAmt = Decimal.Parse(CorporateFinancialAssistanceAmtStr);
               
            }
            if (TxtDtlSevPay6.Text.Length > 0)
                DtlSevPay6 = Decimal.Parse(TxtDtlSevPay6.Text);

            if (TxtDtlCorporateFinancial.Text.Length > 0)
            {
                DtlCorporateFinancial = Decimal.Parse(TxtDtlCorporateFinancial.Text);
            }

            if (TxtDtlEmprFee2.Text.Length > 0)
            {
                DtlEmprFee2 = Decimal.Parse(TxtDtlEmprFee2.Text);
            }

            if (TxtDtlRetirementCapital2.Text.Length > 0)
            {
                DtlRetirementCapital2 = Decimal.Parse(TxtDtlRetirementCapital2.Text);
            }
           
            if (TxtDtlPensionType.Text.Length > 0)
                SSCode = Sm.GetValue("Select SSCode From TblSS Where SSname = '" + TxtDtlPensionType.Text + "'");

            if (SSCode == mHIIPensionCode && TxtDtlRetirement.Text.Length > 0)
            {
                decimal accr = DtlSevPay6 - (DtlRetirementCapital2 + DtlEmprFee2);
                if (accr > 0)
                {
                    TxtDtlAccruedExp.EditValue = Sm.FormatNum(accr, 0);
                }
                else
                {
                    TxtDtlAccruedExp.EditValue = 0;
                }
                
                if(Decimal.Parse(Sm.GetValue("Select LeaveStartDt From tblEmployee Where EmpCode = '"+TxtEmpCode.Text+"'")) <= Decimal.Parse(mHIIMInDateCorporateFinancial))
                TxtDtlCorporateFinancial3.EditValue = Sm.FormatNum(DtlEmprFee2, 0);
            }
            else if (SSCode == mNATPensionCode && TxtDtlRetirement.Text.Length > 0)
            {
                TxtDtlCorporateFinancial.EditValue = Sm.FormatNum(CorporateFinancialAssistanceAmt, 0);
                TxtDtlCorporateFinancial2.EditValue = Sm.FormatNum(CorporateFinancialAssistanceAmt, 0);
                TxtDtlCorporateFinancial3.EditValue = Sm.FormatNum(CorporateFinancialAssistanceAmt, 0);
                TxtDtlAccruedExp.EditValue = Sm.FormatNum(DtlSevPay6 - (DtlCorporateFinancial + DtlEmprFee2), 0);
            }

            mInitAccruedExpAmt = Decimal.Parse(TxtDtlAccruedExp.Text);

            if (mIsEmpSeveranceUseDeduction)
            {
                decimal DeductionAmt = 0m;
                if (TxtDeductionAmt.Text.Length > 0) DeductionAmt = Decimal.Parse(TxtDeductionAmt.Text);
                if (DeductionAmt != 0m)
                {
                    TxtDtlAccruedExp.EditValue = Sm.FormatNum((mInitAccruedExpAmt - DeductionAmt), 0);
                }
            }

            TxtDtlAccruedExp2.EditValue = Sm.FormatNum(TxtDtlAccruedExp.Text, 0);
            TxtDtlAccruedExp3.EditValue = Sm.FormatNum(TxtDtlAccruedExp.Text, 0);
            TxtHdrAccruedExp.EditValue = Sm.FormatNum(TxtDtlAccruedExp.Text, 0);
            ComputeReceivedPay();
            ComputeAmtBeforeTax();
            ComputeAmtAfterTax();
        }
        #endregion

        #region tab 5
        private void ComputeReceivedPay()
        {
            string SSCode = string.Empty;
            decimal CorporateDonation = 0;
            if (TxtDtlPensionType.Text.Length > 0)
                SSCode = Sm.GetValue("Select SSCode From TblSS Where SSname = '" + TxtDtlPensionType.Text + "'");

            if (SSCode == mNATPensionCode && TxtDtlSalary.Text.Length > 0)
            {
                CorporateDonation = Decimal.Parse(TxtDtlSalary.Text)*2;
                TxtDtlCorporateDonation.EditValue = Sm.FormatNum(CorporateDonation, 0);
                TxtDtlCorporateDonation2.EditValue = Sm.FormatNum(CorporateDonation, 0);
                TxtDtlReceivedPay.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlEmpFee2.Text) + Decimal.Parse(TxtDtlEmprFee3.Text) + Decimal.Parse(TxtDtlCorporateFinancial2.Text) + CorporateDonation + decimal.Parse(TxtDtlAccruedExp2.Text), 0);
            }
            else if (SSCode == mHIIPensionCode && TxtDtlSalary.Text.Length > 0)
            {
                CorporateDonation = Decimal.Parse(TxtDtlSalary.Text) * 2;
                TxtDtlCorporateDonation.EditValue = Sm.FormatNum(CorporateDonation, 0);
                TxtDtlCorporateDonation2.EditValue = Sm.FormatNum(CorporateDonation, 0);
                TxtDtlReceivedPay.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlEmpFee2.Text) + Decimal.Parse(TxtDtlEmprFee3.Text) + Decimal.Parse(TxtDtlRetirementCapital3.Text) + CorporateDonation + decimal.Parse(TxtDtlAccruedExp2.Text), 0); 
            }

            TxtHdrReceivedPay.EditValue = Sm.FormatNum(TxtDtlReceivedPay.Text, 0);
        }
        #endregion

        #region tab 6
        private void ComputeAmtBeforeTax()
        {
            TxtDtlAmtBeforeTax.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlAccruedExp3.Text) + Decimal.Parse(TxtDtlCorporateFinancial3.Text) + Decimal.Parse(TxtDtlCorporateDonation2.Text), 0);

            ComputeAmtAfterTax();
        }

        private void ComputeAmtAfterTax()
        {
            decimal AccruedExp1 = Decimal.Parse(TxtDtlAccruedExp3.Text)/1000;
            decimal AccruedExp2 = (Decimal.Parse(TxtDtlCorporateFinancial3.Text) + Decimal.Parse(TxtDtlCorporateDonation2.Text)) / 1000;

            //decimal AccruedExpRound1 = Math.Truncate(AccruedExp1)*1000;
            //decimal AccruedExpRound2 = Math.Truncate(AccruedExp2) * 1000;

            decimal AccruedExpRound1 = AccruedExp1 * 1000;
            decimal AccruedExpRound2 = AccruedExp2 * 1000;


            decimal  TaxTemp=0, Amt2Temp =0, TaxTemp2=0, Amt2Temp2 =0;
            

            var lTI = new List<TI>();
            ProcessTI(ref lTI);


            #region tax terhadap pesangon

            if (AccruedExpRound1 > 0m && lTI.Count > 0)
            {
                TaxTemp = AccruedExpRound1;
                Amt2Temp = 0m;
                AccruedExpRound1 = 0m;

                foreach (TI i in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (i.Amt2 - Amt2Temp))
                        {
                            AccruedExpRound1 += (TaxTemp * i.TaxRate * 0.01m);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            AccruedExpRound1 += ((i.Amt2 - Amt2Temp) * i.TaxRate * 0.01m);
                            TaxTemp -= (i.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = i.Amt2;
                }
            }


            #endregion

            #region tax terhadap uang tali asih

            if (AccruedExpRound2 > 0m && lTI.Count > 0)
            {
                TaxTemp2 = AccruedExpRound2;
                Amt2Temp2 = 0m;
                AccruedExpRound2 = 0m;

                foreach (TI i in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp2 > 0m)
                    {
                        if (TaxTemp2 <= (i.Amt2 - Amt2Temp2))
                        {
                            AccruedExpRound2 += (TaxTemp2 * i.TaxRate * 0.01m);
                            TaxTemp2 = 0m;
                        }
                        else
                        {
                            AccruedExpRound2 += ((i.Amt2 - Amt2Temp2) * i.TaxRate * 0.01m);
                            TaxTemp2 -= (i.Amt2 - Amt2Temp2);
                        }
                    }
                    Amt2Temp2 = i.Amt2;
                }
            }
           
            #endregion

            TxtDtlTax.EditValue = Sm.FormatNum(AccruedExpRound1<0?0:AccruedExpRound1, 0); ;
            TxtDtlTax2.EditValue = Sm.FormatNum(AccruedExpRound2<0?0:AccruedExpRound2, 0);

            TxtDtlAmtAfterTax.EditValue = Sm.FormatNum(Decimal.Parse(TxtDtlAmtBeforeTax.Text) - Decimal.Parse(TxtDtlTax.Text) - Decimal.Parse(TxtDtlTax2.Text), 0);

            TxtHdrRecvDirect.EditValue = Sm.FormatNum(TxtDtlAmtAfterTax.Text, 0);
        }
        #endregion 

        private void ProcessTI(ref List<TI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.ActInd='Y' And A.UsedFor='02'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }


        public static void SetLueKoefisien(ref LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { null, "0.00", "1.00", "2.00" });
        }

        internal static void SetLookUpEdit(LookUpEdit Lue, object ds)
        {
            //populate data for LookUpEdit control
            try
            {
                Lue.DataBindings.Clear();
                Lue.Properties.DataSource = ds;
                Lue.Properties.PopulateColumns();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Lue.EditValue = null;
            }
        }

        public static int RoundUp(int value)
        {
          return 100 * ((value + 99) / 100);
        }

        #endregion

        #region FTP

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                //FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mFrmParent.mHostAddrForFTPClient, mFrmParent.mPortForFTPClient, mFrmParent.mSharedFolderForFTPClient));
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/employee-severance/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/employee-severance/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }
        #endregion

        #endregion

        #region Event

        private void BtnEmpCode_Click_1(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmpSeveranceDlg(this));
        }

        private void BtnEmpCode2_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtEmpCode, "Employee", false))
            {
                var f = new FrmEmployee(this.mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = TxtEmpCode.Text;
                f.ShowDialog();
            }
        }

        private void TxtSalary_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtHdrSalary, 0);
        }

        private void TxtPension_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDtlPension, 0);
        }

        private void TxtNonPension_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDtlNonPension, 0);
        }

        private void TxtDtlRetirement_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDtlRetirement, 0); 
        }

        private void TxtDtlEmpFee_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDtlEmpFee, 0);
        }

        private void TxtDtlEmprFee_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDtlEmprFee, 0);
        }

        private void TxtDtlRetirement_EditValueChanged(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
                ComputeBenefit();
        }

        private void LueDtlKoefisien1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDtlKoefisien1, new Sm.RefreshLue1(SetLueKoefisien));
            if (TxtDocNo.Text.Length == 0 && Sm.GetLue(LueDtlKoefisien1).Length>0)
            {
                ComputeSeverance1();
                ComputeSeverance3();
            }
        }

        private void LueDtlKoefisien2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDtlKoefisien2, new Sm.RefreshLue1(SetLueKoefisien));

            if (TxtDocNo.Text.Length == 0 && Sm.GetLue(LueDtlKoefisien2).Length > 0)
            {
                ComputeSeverance2();
                ComputeSeverance3();
            }
        }

        private void BtnSalary_Click(object sender, EventArgs e)
        {
            Tpg.SelectTab("TpgSalary");
        }

        private void BtnBenefit_Click(object sender, EventArgs e)
        {
            Tpg.SelectTab("TpgBenefit");
        }

        private void BtnSevPay_Click(object sender, EventArgs e)
        {
            Tpg.SelectTab("TpgSeverance");
        }

        private void btnAccExp_Click(object sender, EventArgs e)
        {
            Tpg.SelectTab("TpgAccrued");
        }

        private void BtnRecvPay_Click(object sender, EventArgs e)
        {
            Tpg.SelectTab("TpgRecvPay");
        }

        private void BtnRecvDirect_Click(object sender, EventArgs e)
        {
            Tpg.SelectTab("TpgPayment");
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);

        }

        private void TxtDtlResidual_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDtlResidual, 0); 
        }

        private void TxtDeductionAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDeductionAmt, 0);
                ComputeAccruedExpenses();
            }
        }

        #endregion

        #region Class TI
        private class TI
        {
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }
        #endregion
        
    }
}
