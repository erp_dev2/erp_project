﻿namespace RunSystem
{
    partial class FrmPrintRptFicoSetting5Consolidation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.LblYr = new System.Windows.Forms.Label();
            this.LblType = new System.Windows.Forms.Label();
            this.LueOptionCode = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnPrint2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkProfitCenterCode = new DevExpress.XtraEditors.CheckEdit();
            this.CcbProfitCenterCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.LblMultiProfitCenterCode = new System.Windows.Forms.Label();
            this.LblPeriode1 = new System.Windows.Forms.Label();
            this.LblPeriode2 = new System.Windows.Forms.Label();
            this.LblEndDt = new System.Windows.Forms.Label();
            this.LblStartDt = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.LblEndDt2 = new System.Windows.Forms.Label();
            this.LblStartDt2 = new System.Windows.Forms.Label();
            this.DteDocDt4 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt3 = new DevExpress.XtraEditors.DateEdit();
            this.LueYr2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LblYr2 = new System.Windows.Forms.Label();
            this.LblBalance2 = new System.Windows.Forms.Label();
            this.LblBalance1 = new System.Windows.Forms.Label();
            this.TypeRule = new System.Windows.Forms.Label();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TxtBalance1 = new DevExpress.XtraEditors.TextEdit();
            this.TxtBalance2 = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOptionCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt4.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnPrint2);
            this.panel1.Location = new System.Drawing.Point(1033, 0);
            this.panel1.Size = new System.Drawing.Size(70, 495);
            this.panel1.Controls.SetChildIndex(this.BtnRefresh, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.BtnExcel, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.BtnChart, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint2, 0);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtBalance2);
            this.panel2.Controls.Add(this.TxtBalance1);
            this.panel2.Controls.Add(this.TypeRule);
            this.panel2.Controls.Add(this.LblBalance2);
            this.panel2.Controls.Add(this.LblBalance1);
            this.panel2.Controls.Add(this.LblEndDt2);
            this.panel2.Controls.Add(this.LblStartDt2);
            this.panel2.Controls.Add(this.DteDocDt4);
            this.panel2.Controls.Add(this.DteDocDt3);
            this.panel2.Controls.Add(this.LueYr2);
            this.panel2.Controls.Add(this.LblYr2);
            this.panel2.Controls.Add(this.LblEndDt);
            this.panel2.Controls.Add(this.LblStartDt);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.LblPeriode2);
            this.panel2.Controls.Add(this.LblPeriode1);
            this.panel2.Controls.Add(this.ChkProfitCenterCode);
            this.panel2.Controls.Add(this.CcbProfitCenterCode);
            this.panel2.Controls.Add(this.LblMultiProfitCenterCode);
            this.panel2.Controls.Add(this.LblType);
            this.panel2.Controls.Add(this.LueOptionCode);
            this.panel2.Controls.Add(this.LueYr);
            this.panel2.Controls.Add(this.LblYr);
            this.panel2.Size = new System.Drawing.Size(1033, 183);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.BackColorEvenRows = System.Drawing.Color.White;
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(1033, 312);
            this.Grd1.TabIndex = 31;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 431);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Grd2);
            this.panel3.Location = new System.Drawing.Point(0, 183);
            this.panel3.Size = new System.Drawing.Size(1033, 312);
            this.panel3.Controls.SetChildIndex(this.Grd2, 0);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(78, 26);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 100;
            this.LueYr.Size = new System.Drawing.Size(75, 20);
            this.LueYr.TabIndex = 10;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueYr.EditValueChanged += new System.EventHandler(this.LueYr_EditValueChanged);
            // 
            // LblYr
            // 
            this.LblYr.AutoSize = true;
            this.LblYr.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblYr.ForeColor = System.Drawing.Color.Red;
            this.LblYr.Location = new System.Drawing.Point(36, 29);
            this.LblYr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblYr.Name = "LblYr";
            this.LblYr.Size = new System.Drawing.Size(32, 14);
            this.LblYr.TabIndex = 9;
            this.LblYr.Text = "Year";
            this.LblYr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblType
            // 
            this.LblType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblType.AutoSize = true;
            this.LblType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblType.ForeColor = System.Drawing.Color.Red;
            this.LblType.Location = new System.Drawing.Point(786, 29);
            this.LblType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblType.Name = "LblType";
            this.LblType.Size = new System.Drawing.Size(35, 14);
            this.LblType.TabIndex = 22;
            this.LblType.Text = "Type";
            this.LblType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueOptionCode
            // 
            this.LueOptionCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LueOptionCode.EnterMoveNextControl = true;
            this.LueOptionCode.Location = new System.Drawing.Point(831, 28);
            this.LueOptionCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueOptionCode.Name = "LueOptionCode";
            this.LueOptionCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptionCode.Properties.Appearance.Options.UseFont = true;
            this.LueOptionCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptionCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueOptionCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptionCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueOptionCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptionCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueOptionCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptionCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueOptionCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueOptionCode.Properties.DropDownRows = 25;
            this.LueOptionCode.Properties.NullText = "[Empty]";
            this.LueOptionCode.Properties.PopupWidth = 500;
            this.LueOptionCode.Size = new System.Drawing.Size(174, 20);
            this.LueOptionCode.TabIndex = 23;
            this.LueOptionCode.ToolTip = "F4 : Show/hide list";
            this.LueOptionCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueOptionCode.EditValueChanged += new System.EventHandler(this.LueOptionCode_EditValueChanged);
            // 
            // BtnPrint2
            // 
            this.BtnPrint2.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint2.Appearance.Options.UseBackColor = true;
            this.BtnPrint2.Appearance.Options.UseFont = true;
            this.BtnPrint2.Appearance.Options.UseForeColor = true;
            this.BtnPrint2.Appearance.Options.UseTextOptions = true;
            this.BtnPrint2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPrint2.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnPrint2.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnPrint2.Location = new System.Drawing.Point(0, 93);
            this.BtnPrint2.Name = "BtnPrint2";
            this.BtnPrint2.Size = new System.Drawing.Size(70, 31);
            this.BtnPrint2.TabIndex = 11;
            this.BtnPrint2.Text = "  &Print";
            this.BtnPrint2.ToolTip = "Refresh Data (Alt-R)";
            this.BtnPrint2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPrint2.ToolTipTitle = "Run System";
            this.BtnPrint2.Click += new System.EventHandler(this.BtnPrint2_Click);
            // 
            // ChkProfitCenterCode
            // 
            this.ChkProfitCenterCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkProfitCenterCode.Location = new System.Drawing.Point(1008, 47);
            this.ChkProfitCenterCode.Name = "ChkProfitCenterCode";
            this.ChkProfitCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProfitCenterCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProfitCenterCode.Properties.Caption = " ";
            this.ChkProfitCenterCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProfitCenterCode.Size = new System.Drawing.Size(18, 22);
            this.ChkProfitCenterCode.TabIndex = 26;
            this.ChkProfitCenterCode.ToolTip = "Remove filter";
            this.ChkProfitCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProfitCenterCode.ToolTipTitle = "Run System";
            this.ChkProfitCenterCode.CheckedChanged += new System.EventHandler(this.ChkProfitCenterCode_CheckedChanged);
            // 
            // CcbProfitCenterCode
            // 
            this.CcbProfitCenterCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CcbProfitCenterCode.Location = new System.Drawing.Point(831, 48);
            this.CcbProfitCenterCode.Name = "CcbProfitCenterCode";
            this.CcbProfitCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbProfitCenterCode.Properties.DropDownRows = 30;
            this.CcbProfitCenterCode.Properties.IncrementalSearch = true;
            this.CcbProfitCenterCode.Size = new System.Drawing.Size(174, 20);
            this.CcbProfitCenterCode.TabIndex = 25;
            this.CcbProfitCenterCode.EditValueChanged += new System.EventHandler(this.CcbProfitCenterCode_EditValueChanged);
            // 
            // LblMultiProfitCenterCode
            // 
            this.LblMultiProfitCenterCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblMultiProfitCenterCode.AutoSize = true;
            this.LblMultiProfitCenterCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMultiProfitCenterCode.ForeColor = System.Drawing.Color.Black;
            this.LblMultiProfitCenterCode.Location = new System.Drawing.Point(715, 50);
            this.LblMultiProfitCenterCode.Name = "LblMultiProfitCenterCode";
            this.LblMultiProfitCenterCode.Size = new System.Drawing.Size(106, 14);
            this.LblMultiProfitCenterCode.TabIndex = 24;
            this.LblMultiProfitCenterCode.Text = "Multi Profit Center";
            // 
            // LblPeriode1
            // 
            this.LblPeriode1.AutoSize = true;
            this.LblPeriode1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPeriode1.ForeColor = System.Drawing.Color.Black;
            this.LblPeriode1.Location = new System.Drawing.Point(9, 7);
            this.LblPeriode1.Name = "LblPeriode1";
            this.LblPeriode1.Size = new System.Drawing.Size(59, 14);
            this.LblPeriode1.TabIndex = 8;
            this.LblPeriode1.Text = "Periode 1";
            // 
            // LblPeriode2
            // 
            this.LblPeriode2.AutoSize = true;
            this.LblPeriode2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPeriode2.ForeColor = System.Drawing.Color.Black;
            this.LblPeriode2.Location = new System.Drawing.Point(9, 91);
            this.LblPeriode2.Name = "LblPeriode2";
            this.LblPeriode2.Size = new System.Drawing.Size(59, 14);
            this.LblPeriode2.TabIndex = 15;
            this.LblPeriode2.Text = "Periode 2";
            // 
            // LblEndDt
            // 
            this.LblEndDt.AutoSize = true;
            this.LblEndDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEndDt.ForeColor = System.Drawing.Color.Red;
            this.LblEndDt.Location = new System.Drawing.Point(10, 72);
            this.LblEndDt.Name = "LblEndDt";
            this.LblEndDt.Size = new System.Drawing.Size(58, 14);
            this.LblEndDt.TabIndex = 13;
            this.LblEndDt.Text = "End Date";
            // 
            // LblStartDt
            // 
            this.LblStartDt.AutoSize = true;
            this.LblStartDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblStartDt.ForeColor = System.Drawing.Color.Red;
            this.LblStartDt.Location = new System.Drawing.Point(4, 51);
            this.LblStartDt.Name = "LblStartDt";
            this.LblStartDt.Size = new System.Drawing.Size(64, 14);
            this.LblStartDt.TabIndex = 11;
            this.LblStartDt.Text = "Start Date";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(77, 69);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt2.TabIndex = 14;
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(77, 48);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt1.TabIndex = 12;
            // 
            // LblEndDt2
            // 
            this.LblEndDt2.AutoSize = true;
            this.LblEndDt2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEndDt2.ForeColor = System.Drawing.Color.Black;
            this.LblEndDt2.Location = new System.Drawing.Point(10, 156);
            this.LblEndDt2.Name = "LblEndDt2";
            this.LblEndDt2.Size = new System.Drawing.Size(58, 14);
            this.LblEndDt2.TabIndex = 20;
            this.LblEndDt2.Text = "End Date";
            // 
            // LblStartDt2
            // 
            this.LblStartDt2.AutoSize = true;
            this.LblStartDt2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblStartDt2.ForeColor = System.Drawing.Color.Black;
            this.LblStartDt2.Location = new System.Drawing.Point(4, 135);
            this.LblStartDt2.Name = "LblStartDt2";
            this.LblStartDt2.Size = new System.Drawing.Size(64, 14);
            this.LblStartDt2.TabIndex = 18;
            this.LblStartDt2.Text = "Start Date";
            // 
            // DteDocDt4
            // 
            this.DteDocDt4.EditValue = null;
            this.DteDocDt4.EnterMoveNextControl = true;
            this.DteDocDt4.Location = new System.Drawing.Point(78, 153);
            this.DteDocDt4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt4.Name = "DteDocDt4";
            this.DteDocDt4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt4.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt4.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt4.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt4.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt4.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt4.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt4.TabIndex = 21;
            // 
            // DteDocDt3
            // 
            this.DteDocDt3.EditValue = null;
            this.DteDocDt3.EnterMoveNextControl = true;
            this.DteDocDt3.Location = new System.Drawing.Point(78, 132);
            this.DteDocDt3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt3.Name = "DteDocDt3";
            this.DteDocDt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt3.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt3.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt3.TabIndex = 19;
            // 
            // LueYr2
            // 
            this.LueYr2.EnterMoveNextControl = true;
            this.LueYr2.Location = new System.Drawing.Point(79, 110);
            this.LueYr2.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr2.Name = "LueYr2";
            this.LueYr2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr2.Properties.Appearance.Options.UseFont = true;
            this.LueYr2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr2.Properties.DropDownRows = 12;
            this.LueYr2.Properties.NullText = "[Empty]";
            this.LueYr2.Properties.PopupWidth = 100;
            this.LueYr2.Size = new System.Drawing.Size(75, 20);
            this.LueYr2.TabIndex = 17;
            this.LueYr2.ToolTip = "F4 : Show/hide list";
            this.LueYr2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueYr2.EditValueChanged += new System.EventHandler(this.LueYr2_EditValueChanged);
            // 
            // LblYr2
            // 
            this.LblYr2.AutoSize = true;
            this.LblYr2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblYr2.ForeColor = System.Drawing.Color.Black;
            this.LblYr2.Location = new System.Drawing.Point(36, 113);
            this.LblYr2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblYr2.Name = "LblYr2";
            this.LblYr2.Size = new System.Drawing.Size(32, 14);
            this.LblYr2.TabIndex = 16;
            this.LblYr2.Text = "Year";
            this.LblYr2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblBalance2
            // 
            this.LblBalance2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblBalance2.AutoSize = true;
            this.LblBalance2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBalance2.ForeColor = System.Drawing.Color.Black;
            this.LblBalance2.Location = new System.Drawing.Point(762, 137);
            this.LblBalance2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBalance2.Name = "LblBalance2";
            this.LblBalance2.Size = new System.Drawing.Size(59, 14);
            this.LblBalance2.TabIndex = 29;
            this.LblBalance2.Text = "Balance 2";
            this.LblBalance2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblBalance1
            // 
            this.LblBalance1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblBalance1.AutoSize = true;
            this.LblBalance1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBalance1.ForeColor = System.Drawing.Color.Black;
            this.LblBalance1.Location = new System.Drawing.Point(762, 113);
            this.LblBalance1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBalance1.Name = "LblBalance1";
            this.LblBalance1.Size = new System.Drawing.Size(59, 14);
            this.LblBalance1.TabIndex = 27;
            this.LblBalance1.Text = "Balance 1";
            this.LblBalance1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TypeRule
            // 
            this.TypeRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TypeRule.AutoSize = true;
            this.TypeRule.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TypeRule.ForeColor = System.Drawing.Color.Red;
            this.TypeRule.Location = new System.Drawing.Point(1007, 29);
            this.TypeRule.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TypeRule.Name = "TypeRule";
            this.TypeRule.Size = new System.Drawing.Size(21, 14);
            this.TypeRule.TabIndex = 31;
            this.TypeRule.Text = "(!)";
            this.TypeRule.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TypeRule.Click += new System.EventHandler(this.TypeRule_Click);
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.GroupBox.Visible = true;
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 19;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowMode = true;
            this.Grd2.RowModeHasCurCell = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(1033, 312);
            this.Grd2.TabIndex = 19;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.Visible = false;
            this.Grd2.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // TxtBalance1
            // 
            this.TxtBalance1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtBalance1.EnterMoveNextControl = true;
            this.TxtBalance1.Location = new System.Drawing.Point(831, 110);
            this.TxtBalance1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBalance1.Name = "TxtBalance1";
            this.TxtBalance1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBalance1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBalance1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBalance1.Properties.Appearance.Options.UseFont = true;
            this.TxtBalance1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBalance1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBalance1.Properties.ReadOnly = true;
            this.TxtBalance1.Size = new System.Drawing.Size(174, 20);
            this.TxtBalance1.TabIndex = 61;
            this.TxtBalance1.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // TxtBalance2
            // 
            this.TxtBalance2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtBalance2.EnterMoveNextControl = true;
            this.TxtBalance2.Location = new System.Drawing.Point(831, 134);
            this.TxtBalance2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBalance2.Name = "TxtBalance2";
            this.TxtBalance2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBalance2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBalance2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBalance2.Properties.Appearance.Options.UseFont = true;
            this.TxtBalance2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBalance2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBalance2.Properties.ReadOnly = true;
            this.TxtBalance2.Size = new System.Drawing.Size(174, 20);
            this.TxtBalance2.TabIndex = 62;
            this.TxtBalance2.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // FrmPrintRptFicoSetting5Consolidation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 495);
            this.Name = "FrmPrintRptFicoSetting5Consolidation";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOptionCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt4.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private System.Windows.Forms.Label LblYr;
        private System.Windows.Forms.Label LblType;
        internal DevExpress.XtraEditors.LookUpEdit LueOptionCode;
        protected DevExpress.XtraEditors.SimpleButton BtnPrint2;
        private DevExpress.XtraEditors.CheckEdit ChkProfitCenterCode;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbProfitCenterCode;
        private System.Windows.Forms.Label LblMultiProfitCenterCode;
        private System.Windows.Forms.Label LblPeriode2;
        private System.Windows.Forms.Label LblPeriode1;
        private System.Windows.Forms.Label LblEndDt2;
        private System.Windows.Forms.Label LblStartDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt4;
        internal DevExpress.XtraEditors.DateEdit DteDocDt3;
        private DevExpress.XtraEditors.LookUpEdit LueYr2;
        private System.Windows.Forms.Label LblYr2;
        private System.Windows.Forms.Label LblEndDt;
        private System.Windows.Forms.Label LblStartDt;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private System.Windows.Forms.Label LblBalance2;
        private System.Windows.Forms.Label LblBalance1;
        private System.Windows.Forms.Label TypeRule;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.TextEdit TxtBalance2;
        internal DevExpress.XtraEditors.TextEdit TxtBalance1;
    }
}