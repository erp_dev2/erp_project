﻿namespace RunSystem
{
    partial class FrmMaterialRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMaterialRequest));
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LblReqType = new System.Windows.Forms.Label();
            this.LueReqType = new DevExpress.XtraEditors.LookUpEdit();
            this.LblDept = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtRemainingBudget = new DevExpress.XtraEditors.TextEdit();
            this.LblAvailableBudget = new System.Windows.Forms.Label();
            this.DteUsageDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtPOQtyCancelDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnPOQtyCancelDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPOQtyCancelDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label76 = new System.Windows.Forms.Label();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblBudgetCt = new System.Windows.Forms.Label();
            this.LueBCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtDORequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblDORequestDocNo = new System.Windows.Forms.Label();
            this.BtnDORequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDORequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.LblFile = new System.Windows.Forms.Label();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.LblPICCode = new System.Windows.Forms.Label();
            this.LuePICCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.LueMRType = new DevExpress.XtraEditors.LookUpEdit();
            this.LblMRType = new System.Windows.Forms.Label();
            this.LblBudgetMR = new System.Windows.Forms.Label();
            this.TxtBudgetForMR = new DevExpress.XtraEditors.TextEdit();
            this.LblRemark = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkRMInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel8 = new System.Windows.Forms.Panel();
            this.LblEPROC = new System.Windows.Forms.Label();
            this.TxtEproc = new DevExpress.XtraEditors.TextEdit();
            this.BtnApprovalSheetDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnApprovalSheetDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtApprovalSheetDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblApprovalSheet = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnReference = new DevExpress.XtraEditors.SimpleButton();
            this.TxtGrandTotal = new DevExpress.XtraEditors.TextEdit();
            this.LblReference = new System.Windows.Forms.Label();
            this.TxtReference = new DevExpress.XtraEditors.TextEdit();
            this.LueProcurementType = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.ChkTenderInd = new DevExpress.XtraEditors.CheckEdit();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.BtnDroppingRequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDroppingRequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtDR_Balance = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtDR_MRAmt = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.MeeDR_Remark = new DevExpress.XtraEditors.MemoExEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtDR_DroppingRequestAmt = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtDR_BCCode = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtDR_PRJIDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtDR_Mth = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtDR_Yr = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtDR_DeptCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtDroppingRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.LblFile6 = new System.Windows.Forms.Label();
            this.LblFile2 = new System.Windows.Forms.Label();
            this.LblFile5 = new System.Windows.Forms.Label();
            this.LblFile4 = new System.Windows.Forms.Label();
            this.LblFile3 = new System.Windows.Forms.Label();
            this.TxtFile6 = new DevExpress.XtraEditors.TextEdit();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile6 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload6 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile6 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.PbUpload6 = new System.Windows.Forms.ProgressBar();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.TxtFile5 = new DevExpress.XtraEditors.TextEdit();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.ChkFile5 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnFile3 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload5 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.BtnFile5 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload5 = new System.Windows.Forms.ProgressBar();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile4 = new DevExpress.XtraEditors.TextEdit();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.ChkFile4 = new DevExpress.XtraEditors.CheckEdit();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload4 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload4 = new System.Windows.Forms.ProgressBar();
            this.BtnFile4 = new DevExpress.XtraEditors.SimpleButton();
            this.Tp5 = new DevExpress.XtraTab.XtraTabPage();
            this.GrdReview = new TenTec.Windows.iGridLib.iGrid();
            this.Tp6 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.LueDurationUom = new DevExpress.XtraEditors.LookUpEdit();
            this.LuePtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOQtyCancelDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDORequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueMRType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBudgetForMR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRMInd.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEproc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtApprovalSheetDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrandTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcurementType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTenderInd.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Balance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_MRAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDR_Remark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DroppingRequestAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_BCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_PRJIDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Mth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Yr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDroppingRequestDocNo.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            this.Tp5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdReview)).BeginInit();
            this.Tp6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDurationUom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(891, 0);
            this.panel1.Size = new System.Drawing.Size(70, 460);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(891, 336);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LuePtCode);
            this.panel3.Controls.Add(this.LueDurationUom);
            this.panel3.Controls.Add(this.LueCurCode);
            this.panel3.Controls.Add(this.DteUsageDt);
            this.panel3.Location = new System.Drawing.Point(0, 336);
            this.panel3.Size = new System.Drawing.Size(891, 124);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.DteUsageDt, 0);
            this.panel3.Controls.SetChildIndex(this.LueCurCode, 0);
            this.panel3.Controls.SetChildIndex(this.LueDurationUom, 0);
            this.panel3.Controls.SetChildIndex(this.LuePtCode, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 438);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(891, 124);
            this.Grd1.TabIndex = 58;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.GrdAfterCommitEdit);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(105, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(30, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(105, 47);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 20;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(70, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 19;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblReqType
            // 
            this.LblReqType.AutoSize = true;
            this.LblReqType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblReqType.ForeColor = System.Drawing.Color.Red;
            this.LblReqType.Location = new System.Drawing.Point(19, 113);
            this.LblReqType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblReqType.Name = "LblReqType";
            this.LblReqType.Size = new System.Drawing.Size(84, 14);
            this.LblReqType.TabIndex = 25;
            this.LblReqType.Text = "Request Type";
            this.LblReqType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReqType
            // 
            this.LueReqType.EnterMoveNextControl = true;
            this.LueReqType.Location = new System.Drawing.Point(105, 110);
            this.LueReqType.Margin = new System.Windows.Forms.Padding(5);
            this.LueReqType.Name = "LueReqType";
            this.LueReqType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.Appearance.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReqType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReqType.Properties.DropDownRows = 30;
            this.LueReqType.Properties.NullText = "[Empty]";
            this.LueReqType.Properties.PopupWidth = 300;
            this.LueReqType.Size = new System.Drawing.Size(349, 20);
            this.LueReqType.TabIndex = 26;
            this.LueReqType.ToolTip = "F4 : Show/hide list";
            this.LueReqType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReqType.EditValueChanged += new System.EventHandler(this.LueReqType_EditValueChanged);
            // 
            // LblDept
            // 
            this.LblDept.AutoSize = true;
            this.LblDept.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDept.ForeColor = System.Drawing.Color.Red;
            this.LblDept.Location = new System.Drawing.Point(30, 155);
            this.LblDept.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDept.Name = "LblDept";
            this.LblDept.Size = new System.Drawing.Size(73, 14);
            this.LblDept.TabIndex = 29;
            this.LblDept.Text = "Department";
            this.LblDept.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(105, 152);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(349, 20);
            this.LueDeptCode.TabIndex = 30;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // TxtRemainingBudget
            // 
            this.TxtRemainingBudget.EnterMoveNextControl = true;
            this.TxtRemainingBudget.Location = new System.Drawing.Point(105, 236);
            this.TxtRemainingBudget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemainingBudget.Name = "TxtRemainingBudget";
            this.TxtRemainingBudget.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemainingBudget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemainingBudget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseFont = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemainingBudget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemainingBudget.Properties.ReadOnly = true;
            this.TxtRemainingBudget.Size = new System.Drawing.Size(166, 20);
            this.TxtRemainingBudget.TabIndex = 40;
            // 
            // LblAvailableBudget
            // 
            this.LblAvailableBudget.AutoSize = true;
            this.LblAvailableBudget.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAvailableBudget.ForeColor = System.Drawing.Color.Black;
            this.LblAvailableBudget.Location = new System.Drawing.Point(6, 240);
            this.LblAvailableBudget.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAvailableBudget.Name = "LblAvailableBudget";
            this.LblAvailableBudget.Size = new System.Drawing.Size(97, 14);
            this.LblAvailableBudget.TabIndex = 39;
            this.LblAvailableBudget.Text = "Available Budget";
            this.LblAvailableBudget.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteUsageDt
            // 
            this.DteUsageDt.EditValue = null;
            this.DteUsageDt.EnterMoveNextControl = true;
            this.DteUsageDt.Location = new System.Drawing.Point(74, 21);
            this.DteUsageDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUsageDt.Name = "DteUsageDt";
            this.DteUsageDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.Appearance.Options.UseFont = true;
            this.DteUsageDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUsageDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUsageDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUsageDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUsageDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUsageDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUsageDt.Size = new System.Drawing.Size(125, 20);
            this.DteUsageDt.TabIndex = 59;
            this.DteUsageDt.Visible = false;
            this.DteUsageDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteUsageDt_KeyDown);
            this.DteUsageDt.Leave += new System.EventHandler(this.DteUsageDt_Leave);
            // 
            // TxtPOQtyCancelDocNo
            // 
            this.TxtPOQtyCancelDocNo.EnterMoveNextControl = true;
            this.TxtPOQtyCancelDocNo.Location = new System.Drawing.Point(105, 26);
            this.TxtPOQtyCancelDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPOQtyCancelDocNo.Name = "TxtPOQtyCancelDocNo";
            this.TxtPOQtyCancelDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPOQtyCancelDocNo.Properties.MaxLength = 16;
            this.TxtPOQtyCancelDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtPOQtyCancelDocNo.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(3, 29);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 14);
            this.label8.TabIndex = 15;
            this.label8.Text = "Cancellation PO#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPOQtyCancelDocNo2
            // 
            this.BtnPOQtyCancelDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPOQtyCancelDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPOQtyCancelDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPOQtyCancelDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseFont = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnPOQtyCancelDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPOQtyCancelDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPOQtyCancelDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnPOQtyCancelDocNo2.Image")));
            this.BtnPOQtyCancelDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPOQtyCancelDocNo2.Location = new System.Drawing.Point(302, 26);
            this.BtnPOQtyCancelDocNo2.Name = "BtnPOQtyCancelDocNo2";
            this.BtnPOQtyCancelDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnPOQtyCancelDocNo2.TabIndex = 18;
            this.BtnPOQtyCancelDocNo2.ToolTip = "Show Cancellation PO Document";
            this.BtnPOQtyCancelDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPOQtyCancelDocNo2.ToolTipTitle = "Run System";
            this.BtnPOQtyCancelDocNo2.Click += new System.EventHandler(this.BtnPOQtyCancelDocNo2_Click);
            // 
            // BtnPOQtyCancelDocNo
            // 
            this.BtnPOQtyCancelDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPOQtyCancelDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPOQtyCancelDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPOQtyCancelDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseFont = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPOQtyCancelDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPOQtyCancelDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPOQtyCancelDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPOQtyCancelDocNo.Image")));
            this.BtnPOQtyCancelDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPOQtyCancelDocNo.Location = new System.Drawing.Point(274, 26);
            this.BtnPOQtyCancelDocNo.Name = "BtnPOQtyCancelDocNo";
            this.BtnPOQtyCancelDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPOQtyCancelDocNo.TabIndex = 17;
            this.BtnPOQtyCancelDocNo.ToolTip = "Find Cancellation PO Document";
            this.BtnPOQtyCancelDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPOQtyCancelDocNo.ToolTipTitle = "Run System";
            this.BtnPOQtyCancelDocNo.Click += new System.EventHandler(this.BtnPOQtyCancelDocNo_Click);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(105, 68);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 250;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(350, 20);
            this.TxtLocalDocNo.TabIndex = 22;
            this.TxtLocalDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(8, 71);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(95, 14);
            this.label76.TabIndex = 21;
            this.label76.Text = "Local Document";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(75, 134);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 27;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(105, 131);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(349, 20);
            this.LueSiteCode.TabIndex = 28;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.Validated += new System.EventHandler(this.LueSiteCode_Validated);
            // 
            // LblBudgetCt
            // 
            this.LblBudgetCt.AutoSize = true;
            this.LblBudgetCt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBudgetCt.ForeColor = System.Drawing.Color.Black;
            this.LblBudgetCt.Location = new System.Drawing.Point(3, 218);
            this.LblBudgetCt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBudgetCt.Name = "LblBudgetCt";
            this.LblBudgetCt.Size = new System.Drawing.Size(100, 14);
            this.LblBudgetCt.TabIndex = 37;
            this.LblBudgetCt.Text = "Budget Category";
            this.LblBudgetCt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBCCode
            // 
            this.LueBCCode.EnterMoveNextControl = true;
            this.LueBCCode.Location = new System.Drawing.Point(105, 215);
            this.LueBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBCCode.Name = "LueBCCode";
            this.LueBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.Appearance.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBCCode.Properties.DropDownRows = 30;
            this.LueBCCode.Properties.NullText = "[Empty]";
            this.LueBCCode.Properties.PopupWidth = 300;
            this.LueBCCode.Size = new System.Drawing.Size(349, 20);
            this.LueBCCode.TabIndex = 38;
            this.LueBCCode.ToolTip = "F4 : Show/hide list";
            this.LueBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBCCode.EditValueChanged += new System.EventHandler(this.LueBCCode_EditValueChanged);
            // 
            // TxtDORequestDocNo
            // 
            this.TxtDORequestDocNo.EnterMoveNextControl = true;
            this.TxtDORequestDocNo.Location = new System.Drawing.Point(105, 194);
            this.TxtDORequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDORequestDocNo.Name = "TxtDORequestDocNo";
            this.TxtDORequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDORequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDORequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDORequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDORequestDocNo.Properties.MaxLength = 250;
            this.TxtDORequestDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDORequestDocNo.TabIndex = 34;
            // 
            // LblDORequestDocNo
            // 
            this.LblDORequestDocNo.AutoSize = true;
            this.LblDORequestDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDORequestDocNo.Location = new System.Drawing.Point(21, 197);
            this.LblDORequestDocNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDORequestDocNo.Name = "LblDORequestDocNo";
            this.LblDORequestDocNo.Size = new System.Drawing.Size(82, 14);
            this.LblDORequestDocNo.TabIndex = 33;
            this.LblDORequestDocNo.Text = "DO Request#";
            this.LblDORequestDocNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDORequestDocNo2
            // 
            this.BtnDORequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDORequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDORequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDORequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDORequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDORequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDORequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDORequestDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDORequestDocNo2.Image")));
            this.BtnDORequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDORequestDocNo2.Location = new System.Drawing.Point(302, 193);
            this.BtnDORequestDocNo2.Name = "BtnDORequestDocNo2";
            this.BtnDORequestDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnDORequestDocNo2.TabIndex = 36;
            this.BtnDORequestDocNo2.ToolTip = "Show DO Request Document";
            this.BtnDORequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDORequestDocNo2.ToolTipTitle = "Run System";
            this.BtnDORequestDocNo2.Click += new System.EventHandler(this.BtnDORequestDocNo2_Click);
            // 
            // BtnDORequestDocNo
            // 
            this.BtnDORequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDORequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDORequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDORequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDORequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDORequestDocNo.Appearance.Options.UseFont = true;
            this.BtnDORequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDORequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDORequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDORequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDORequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnDORequestDocNo.Image")));
            this.BtnDORequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDORequestDocNo.Location = new System.Drawing.Point(274, 193);
            this.BtnDORequestDocNo.Name = "BtnDORequestDocNo";
            this.BtnDORequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnDORequestDocNo.TabIndex = 35;
            this.BtnDORequestDocNo.ToolTip = "Find DO Request Document";
            this.BtnDORequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDORequestDocNo.ToolTipTitle = "Run System";
            this.BtnDORequestDocNo.Click += new System.EventHandler(this.BtnDORequestDocNo_Click);
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(205, 21);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 14;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 200;
            this.LueCurCode.Size = new System.Drawing.Size(125, 20);
            this.LueCurCode.TabIndex = 60;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            this.LueCurCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode_KeyDown);
            this.LueCurCode.Leave += new System.EventHandler(this.LueCurCode_Leave);
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(3, 2);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(390, 20);
            this.TxtFile.TabIndex = 13;
            // 
            // LblFile
            // 
            this.LblFile.AutoSize = true;
            this.LblFile.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile.ForeColor = System.Drawing.Color.Black;
            this.LblFile.Location = new System.Drawing.Point(25, 7);
            this.LblFile.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile.Name = "LblFile";
            this.LblFile.Size = new System.Drawing.Size(24, 14);
            this.LblFile.TabIndex = 12;
            this.LblFile.Text = "File";
            this.LblFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(426, 3);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 15;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(3, 25);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(390, 17);
            this.PbUpload.TabIndex = 17;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(457, 3);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 16;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(398, 3);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 14;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // LblPICCode
            // 
            this.LblPICCode.AutoSize = true;
            this.LblPICCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPICCode.ForeColor = System.Drawing.Color.Black;
            this.LblPICCode.Location = new System.Drawing.Point(78, 177);
            this.LblPICCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPICCode.Name = "LblPICCode";
            this.LblPICCode.Size = new System.Drawing.Size(25, 14);
            this.LblPICCode.TabIndex = 31;
            this.LblPICCode.Text = "PIC";
            this.LblPICCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePICCode
            // 
            this.LuePICCode.EnterMoveNextControl = true;
            this.LuePICCode.Location = new System.Drawing.Point(105, 173);
            this.LuePICCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePICCode.Name = "LuePICCode";
            this.LuePICCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.Appearance.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePICCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePICCode.Properties.DropDownRows = 30;
            this.LuePICCode.Properties.NullText = "[Empty]";
            this.LuePICCode.Properties.PopupWidth = 300;
            this.LuePICCode.Size = new System.Drawing.Size(350, 20);
            this.LuePICCode.TabIndex = 32;
            this.LuePICCode.ToolTip = "F4 : Show/hide list";
            this.LuePICCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePICCode.EditValueChanged += new System.EventHandler(this.LuePICCode_EditValueChanged);
            // 
            // Tc1
            // 
            this.Tc1.Appearance.Options.UseTextOptions = true;
            this.Tc1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(891, 336);
            this.Tc1.TabIndex = 11;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3,
            this.Tp5,
            this.Tp6});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(885, 308);
            this.Tp1.Text = "General";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.LueMRType);
            this.panel5.Controls.Add(this.LblMRType);
            this.panel5.Controls.Add(this.LblBudgetMR);
            this.panel5.Controls.Add(this.TxtBudgetForMR);
            this.panel5.Controls.Add(this.LblRemark);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.ChkRMInd);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.ChkTenderInd);
            this.panel5.Controls.Add(this.TxtDocNo);
            this.panel5.Controls.Add(this.LblPICCode);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.LuePICCode);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.DteDocDt);
            this.panel5.Controls.Add(this.LueReqType);
            this.panel5.Controls.Add(this.LblReqType);
            this.panel5.Controls.Add(this.LueDeptCode);
            this.panel5.Controls.Add(this.LblDept);
            this.panel5.Controls.Add(this.BtnDORequestDocNo2);
            this.panel5.Controls.Add(this.BtnDORequestDocNo);
            this.panel5.Controls.Add(this.LblAvailableBudget);
            this.panel5.Controls.Add(this.TxtDORequestDocNo);
            this.panel5.Controls.Add(this.TxtRemainingBudget);
            this.panel5.Controls.Add(this.LblDORequestDocNo);
            this.panel5.Controls.Add(this.BtnPOQtyCancelDocNo);
            this.panel5.Controls.Add(this.LblBudgetCt);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.LueBCCode);
            this.panel5.Controls.Add(this.TxtPOQtyCancelDocNo);
            this.panel5.Controls.Add(this.LblSiteCode);
            this.panel5.Controls.Add(this.BtnPOQtyCancelDocNo2);
            this.panel5.Controls.Add(this.LueSiteCode);
            this.panel5.Controls.Add(this.label76);
            this.panel5.Controls.Add(this.TxtLocalDocNo);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(885, 308);
            this.panel5.TabIndex = 22;
            // 
            // LueMRType
            // 
            this.LueMRType.EnterMoveNextControl = true;
            this.LueMRType.Location = new System.Drawing.Point(106, 89);
            this.LueMRType.Margin = new System.Windows.Forms.Padding(5);
            this.LueMRType.Name = "LueMRType";
            this.LueMRType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMRType.Properties.Appearance.Options.UseFont = true;
            this.LueMRType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMRType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMRType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMRType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMRType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMRType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMRType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMRType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMRType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMRType.Properties.DropDownRows = 30;
            this.LueMRType.Properties.NullText = "[Empty]";
            this.LueMRType.Properties.PopupWidth = 300;
            this.LueMRType.Size = new System.Drawing.Size(349, 20);
            this.LueMRType.TabIndex = 24;
            this.LueMRType.ToolTip = "F4 : Show/hide list";
            this.LueMRType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMRType.EditValueChanged += new System.EventHandler(this.LueMRType_EditValueChanged);
            // 
            // LblMRType
            // 
            this.LblMRType.AutoSize = true;
            this.LblMRType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMRType.ForeColor = System.Drawing.Color.Red;
            this.LblMRType.Location = new System.Drawing.Point(49, 92);
            this.LblMRType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblMRType.Name = "LblMRType";
            this.LblMRType.Size = new System.Drawing.Size(55, 14);
            this.LblMRType.TabIndex = 23;
            this.LblMRType.Text = "MR Type";
            this.LblMRType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblBudgetMR
            // 
            this.LblBudgetMR.AutoSize = true;
            this.LblBudgetMR.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBudgetMR.ForeColor = System.Drawing.Color.Black;
            this.LblBudgetMR.Location = new System.Drawing.Point(17, 260);
            this.LblBudgetMR.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBudgetMR.Name = "LblBudgetMR";
            this.LblBudgetMR.Size = new System.Drawing.Size(86, 14);
            this.LblBudgetMR.TabIndex = 41;
            this.LblBudgetMR.Text = "Budget for MR";
            this.LblBudgetMR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBudgetForMR
            // 
            this.TxtBudgetForMR.EnterMoveNextControl = true;
            this.TxtBudgetForMR.Location = new System.Drawing.Point(105, 257);
            this.TxtBudgetForMR.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBudgetForMR.Name = "TxtBudgetForMR";
            this.TxtBudgetForMR.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBudgetForMR.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBudgetForMR.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBudgetForMR.Properties.Appearance.Options.UseFont = true;
            this.TxtBudgetForMR.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBudgetForMR.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBudgetForMR.Properties.ReadOnly = true;
            this.TxtBudgetForMR.Size = new System.Drawing.Size(166, 20);
            this.TxtBudgetForMR.TabIndex = 42;
            // 
            // LblRemark
            // 
            this.LblRemark.AutoSize = true;
            this.LblRemark.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRemark.Location = new System.Drawing.Point(56, 280);
            this.LblRemark.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRemark.Name = "LblRemark";
            this.LblRemark.Size = new System.Drawing.Size(47, 14);
            this.LblRemark.TabIndex = 43;
            this.LblRemark.Text = "Remark";
            this.LblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(105, 278);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 2000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(349, 20);
            this.MeeRemark.TabIndex = 44;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // ChkRMInd
            // 
            this.ChkRMInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkRMInd.Location = new System.Drawing.Point(364, 5);
            this.ChkRMInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkRMInd.Name = "ChkRMInd";
            this.ChkRMInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkRMInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkRMInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkRMInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkRMInd.Properties.Appearance.Options.UseFont = true;
            this.ChkRMInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkRMInd.Properties.Caption = "RUNMarket";
            this.ChkRMInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkRMInd.Size = new System.Drawing.Size(91, 22);
            this.ChkRMInd.TabIndex = 43;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.LblEPROC);
            this.panel8.Controls.Add(this.TxtEproc);
            this.panel8.Controls.Add(this.BtnApprovalSheetDocNo2);
            this.panel8.Controls.Add(this.BtnApprovalSheetDocNo);
            this.panel8.Controls.Add(this.TxtApprovalSheetDocNo);
            this.panel8.Controls.Add(this.LblApprovalSheet);
            this.panel8.Controls.Add(this.label17);
            this.panel8.Controls.Add(this.BtnReference);
            this.panel8.Controls.Add(this.TxtGrandTotal);
            this.panel8.Controls.Add(this.LblReference);
            this.panel8.Controls.Add(this.TxtReference);
            this.panel8.Controls.Add(this.LueProcurementType);
            this.panel8.Controls.Add(this.label22);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(468, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(417, 308);
            this.panel8.TabIndex = 45;
            // 
            // LblEPROC
            // 
            this.LblEPROC.AutoSize = true;
            this.LblEPROC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEPROC.Location = new System.Drawing.Point(4, 92);
            this.LblEPROC.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEPROC.Name = "LblEPROC";
            this.LblEPROC.Size = new System.Drawing.Size(172, 14);
            this.LblEPROC.TabIndex = 57;
            this.LblEPROC.Text = "Asset Status Change (EPROC)";
            this.LblEPROC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEproc
            // 
            this.TxtEproc.EnterMoveNextControl = true;
            this.TxtEproc.Location = new System.Drawing.Point(178, 89);
            this.TxtEproc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEproc.Name = "TxtEproc";
            this.TxtEproc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEproc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEproc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEproc.Properties.Appearance.Options.UseFont = true;
            this.TxtEproc.Properties.MaxLength = 250;
            this.TxtEproc.Size = new System.Drawing.Size(166, 20);
            this.TxtEproc.TabIndex = 58;
            // 
            // BtnApprovalSheetDocNo2
            // 
            this.BtnApprovalSheetDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnApprovalSheetDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnApprovalSheetDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnApprovalSheetDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnApprovalSheetDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnApprovalSheetDocNo2.Appearance.Options.UseFont = true;
            this.BtnApprovalSheetDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnApprovalSheetDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnApprovalSheetDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnApprovalSheetDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnApprovalSheetDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnApprovalSheetDocNo2.Image")));
            this.BtnApprovalSheetDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnApprovalSheetDocNo2.Location = new System.Drawing.Point(375, 67);
            this.BtnApprovalSheetDocNo2.Name = "BtnApprovalSheetDocNo2";
            this.BtnApprovalSheetDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnApprovalSheetDocNo2.TabIndex = 56;
            this.BtnApprovalSheetDocNo2.ToolTip = "Show DO Request Document";
            this.BtnApprovalSheetDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnApprovalSheetDocNo2.ToolTipTitle = "Run System";
            this.BtnApprovalSheetDocNo2.Click += new System.EventHandler(this.BtnApprovalSheetDocNo2_Click);
            // 
            // BtnApprovalSheetDocNo
            // 
            this.BtnApprovalSheetDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnApprovalSheetDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnApprovalSheetDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnApprovalSheetDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnApprovalSheetDocNo.Appearance.Options.UseBackColor = true;
            this.BtnApprovalSheetDocNo.Appearance.Options.UseFont = true;
            this.BtnApprovalSheetDocNo.Appearance.Options.UseForeColor = true;
            this.BtnApprovalSheetDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnApprovalSheetDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnApprovalSheetDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnApprovalSheetDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnApprovalSheetDocNo.Image")));
            this.BtnApprovalSheetDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnApprovalSheetDocNo.Location = new System.Drawing.Point(347, 67);
            this.BtnApprovalSheetDocNo.Name = "BtnApprovalSheetDocNo";
            this.BtnApprovalSheetDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnApprovalSheetDocNo.TabIndex = 55;
            this.BtnApprovalSheetDocNo.ToolTip = "Find DO Request Document";
            this.BtnApprovalSheetDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnApprovalSheetDocNo.ToolTipTitle = "Run System";
            this.BtnApprovalSheetDocNo.Click += new System.EventHandler(this.BtnApprovalSheetDocNo_Click);
            // 
            // TxtApprovalSheetDocNo
            // 
            this.TxtApprovalSheetDocNo.EnterMoveNextControl = true;
            this.TxtApprovalSheetDocNo.Location = new System.Drawing.Point(178, 68);
            this.TxtApprovalSheetDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtApprovalSheetDocNo.Name = "TxtApprovalSheetDocNo";
            this.TxtApprovalSheetDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtApprovalSheetDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtApprovalSheetDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtApprovalSheetDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtApprovalSheetDocNo.Properties.MaxLength = 250;
            this.TxtApprovalSheetDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtApprovalSheetDocNo.TabIndex = 54;
            // 
            // LblApprovalSheet
            // 
            this.LblApprovalSheet.AutoSize = true;
            this.LblApprovalSheet.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblApprovalSheet.Location = new System.Drawing.Point(85, 71);
            this.LblApprovalSheet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblApprovalSheet.Name = "LblApprovalSheet";
            this.LblApprovalSheet.Size = new System.Drawing.Size(91, 14);
            this.LblApprovalSheet.TabIndex = 53;
            this.LblApprovalSheet.Text = "Approval Sheet";
            this.LblApprovalSheet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(105, 29);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 14);
            this.label17.TabIndex = 48;
            this.label17.Text = "Grand Total";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnReference
            // 
            this.BtnReference.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnReference.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnReference.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnReference.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnReference.Appearance.Options.UseBackColor = true;
            this.BtnReference.Appearance.Options.UseFont = true;
            this.BtnReference.Appearance.Options.UseForeColor = true;
            this.BtnReference.Appearance.Options.UseTextOptions = true;
            this.BtnReference.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnReference.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnReference.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnReference.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnReference.Location = new System.Drawing.Point(348, 46);
            this.BtnReference.Name = "BtnReference";
            this.BtnReference.Size = new System.Drawing.Size(24, 21);
            this.BtnReference.TabIndex = 52;
            this.BtnReference.ToolTip = "Find Cancellation MR Document";
            this.BtnReference.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnReference.ToolTipTitle = "Run System";
            this.BtnReference.Click += new System.EventHandler(this.BtnReference_Click);
            // 
            // TxtGrandTotal
            // 
            this.TxtGrandTotal.EnterMoveNextControl = true;
            this.TxtGrandTotal.Location = new System.Drawing.Point(178, 26);
            this.TxtGrandTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrandTotal.Name = "TxtGrandTotal";
            this.TxtGrandTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrandTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrandTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrandTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtGrandTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrandTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtGrandTotal.Properties.ReadOnly = true;
            this.TxtGrandTotal.Size = new System.Drawing.Size(217, 20);
            this.TxtGrandTotal.TabIndex = 49;
            // 
            // LblReference
            // 
            this.LblReference.AutoSize = true;
            this.LblReference.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblReference.ForeColor = System.Drawing.Color.Black;
            this.LblReference.Location = new System.Drawing.Point(84, 50);
            this.LblReference.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblReference.Name = "LblReference";
            this.LblReference.Size = new System.Drawing.Size(92, 14);
            this.LblReference.TabIndex = 50;
            this.LblReference.Text = "Reference MR#";
            this.LblReference.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtReference
            // 
            this.TxtReference.EnterMoveNextControl = true;
            this.TxtReference.Location = new System.Drawing.Point(178, 47);
            this.TxtReference.Margin = new System.Windows.Forms.Padding(5);
            this.TxtReference.Name = "TxtReference";
            this.TxtReference.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtReference.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtReference.Properties.Appearance.Options.UseBackColor = true;
            this.TxtReference.Properties.Appearance.Options.UseFont = true;
            this.TxtReference.Properties.MaxLength = 16;
            this.TxtReference.Size = new System.Drawing.Size(166, 20);
            this.TxtReference.TabIndex = 51;
            // 
            // LueProcurementType
            // 
            this.LueProcurementType.EnterMoveNextControl = true;
            this.LueProcurementType.Location = new System.Drawing.Point(178, 5);
            this.LueProcurementType.Margin = new System.Windows.Forms.Padding(5);
            this.LueProcurementType.Name = "LueProcurementType";
            this.LueProcurementType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.Appearance.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProcurementType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProcurementType.Properties.DropDownRows = 30;
            this.LueProcurementType.Properties.NullText = "[Empty]";
            this.LueProcurementType.Properties.PopupWidth = 300;
            this.LueProcurementType.Size = new System.Drawing.Size(217, 20);
            this.LueProcurementType.TabIndex = 47;
            this.LueProcurementType.ToolTip = "F4 : Show/hide list";
            this.LueProcurementType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(66, 8);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(110, 14);
            this.label22.TabIndex = 46;
            this.label22.Text = "Procurement Type";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkTenderInd
            // 
            this.ChkTenderInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTenderInd.Location = new System.Drawing.Point(274, 5);
            this.ChkTenderInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTenderInd.Name = "ChkTenderInd";
            this.ChkTenderInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTenderInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTenderInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTenderInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTenderInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTenderInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTenderInd.Properties.Caption = "For Tender";
            this.ChkTenderInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTenderInd.Size = new System.Drawing.Size(91, 22);
            this.ChkTenderInd.TabIndex = 14;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel6);
            this.Tp2.Name = "Tp2";
            this.Tp2.PageVisible = false;
            this.Tp2.Size = new System.Drawing.Size(766, 308);
            this.Tp2.Text = "For Dropping Request";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.BtnDroppingRequestDocNo2);
            this.panel6.Controls.Add(this.BtnDroppingRequestDocNo);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.TxtDR_Balance);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.TxtDR_MRAmt);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.MeeDR_Remark);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.TxtDR_DroppingRequestAmt);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.TxtDR_BCCode);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.TxtDR_PRJIDocNo);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.TxtDR_Mth);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.TxtDR_Yr);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.TxtDR_DeptCode);
            this.panel6.Controls.Add(this.TxtDroppingRequestDocNo);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(766, 308);
            this.panel6.TabIndex = 22;
            // 
            // BtnDroppingRequestDocNo2
            // 
            this.BtnDroppingRequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDroppingRequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDroppingRequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDroppingRequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDroppingRequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDroppingRequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDroppingRequestDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDroppingRequestDocNo2.Image")));
            this.BtnDroppingRequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDroppingRequestDocNo2.Location = new System.Drawing.Point(369, 10);
            this.BtnDroppingRequestDocNo2.Name = "BtnDroppingRequestDocNo2";
            this.BtnDroppingRequestDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnDroppingRequestDocNo2.TabIndex = 15;
            this.BtnDroppingRequestDocNo2.ToolTip = "Show Dropping Request Document";
            this.BtnDroppingRequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDroppingRequestDocNo2.ToolTipTitle = "Run System";
            this.BtnDroppingRequestDocNo2.Click += new System.EventHandler(this.BtnDroppingRequestDocNo2_Click);
            // 
            // BtnDroppingRequestDocNo
            // 
            this.BtnDroppingRequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDroppingRequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDroppingRequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDroppingRequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseFont = true;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDroppingRequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDroppingRequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDroppingRequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnDroppingRequestDocNo.Image")));
            this.BtnDroppingRequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDroppingRequestDocNo.Location = new System.Drawing.Point(339, 10);
            this.BtnDroppingRequestDocNo.Name = "BtnDroppingRequestDocNo";
            this.BtnDroppingRequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnDroppingRequestDocNo.TabIndex = 14;
            this.BtnDroppingRequestDocNo.ToolTip = "Find Dropping Request Document";
            this.BtnDroppingRequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDroppingRequestDocNo.ToolTipTitle = "Run System";
            this.BtnDroppingRequestDocNo.Click += new System.EventHandler(this.BtnDroppingRequestDocNo_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(116, 190);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 14);
            this.label20.TabIndex = 32;
            this.label20.Text = "Balance";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_Balance
            // 
            this.TxtDR_Balance.EnterMoveNextControl = true;
            this.TxtDR_Balance.Location = new System.Drawing.Point(170, 187);
            this.TxtDR_Balance.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_Balance.Name = "TxtDR_Balance";
            this.TxtDR_Balance.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_Balance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_Balance.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_Balance.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_Balance.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDR_Balance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDR_Balance.Properties.ReadOnly = true;
            this.TxtDR_Balance.Size = new System.Drawing.Size(166, 20);
            this.TxtDR_Balance.TabIndex = 33;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(53, 168);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(111, 14);
            this.label19.TabIndex = 30;
            this.label19.Text = "MR\'s Total Amount";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_MRAmt
            // 
            this.TxtDR_MRAmt.EnterMoveNextControl = true;
            this.TxtDR_MRAmt.Location = new System.Drawing.Point(170, 165);
            this.TxtDR_MRAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_MRAmt.Name = "TxtDR_MRAmt";
            this.TxtDR_MRAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_MRAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_MRAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_MRAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_MRAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDR_MRAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDR_MRAmt.Properties.ReadOnly = true;
            this.TxtDR_MRAmt.Size = new System.Drawing.Size(166, 20);
            this.TxtDR_MRAmt.TabIndex = 31;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(117, 212);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 14);
            this.label18.TabIndex = 34;
            this.label18.Text = "Remark";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeDR_Remark
            // 
            this.MeeDR_Remark.EnterMoveNextControl = true;
            this.MeeDR_Remark.Location = new System.Drawing.Point(170, 209);
            this.MeeDR_Remark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDR_Remark.Name = "MeeDR_Remark";
            this.MeeDR_Remark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeDR_Remark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.Appearance.Options.UseBackColor = true;
            this.MeeDR_Remark.Properties.Appearance.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDR_Remark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDR_Remark.Properties.MaxLength = 400;
            this.MeeDR_Remark.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeDR_Remark.Properties.ShowIcon = false;
            this.MeeDR_Remark.Size = new System.Drawing.Size(500, 20);
            this.MeeDR_Remark.TabIndex = 35;
            this.MeeDR_Remark.ToolTip = "F4 : Show/hide text";
            this.MeeDR_Remark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDR_Remark.ToolTipTitle = "Run System";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(11, 147);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(153, 14);
            this.label16.TabIndex = 26;
            this.label16.Text = "Dropping Request Amount";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_DroppingRequestAmt
            // 
            this.TxtDR_DroppingRequestAmt.EnterMoveNextControl = true;
            this.TxtDR_DroppingRequestAmt.Location = new System.Drawing.Point(170, 143);
            this.TxtDR_DroppingRequestAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_DroppingRequestAmt.Name = "TxtDR_DroppingRequestAmt";
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDR_DroppingRequestAmt.Properties.ReadOnly = true;
            this.TxtDR_DroppingRequestAmt.Size = new System.Drawing.Size(166, 20);
            this.TxtDR_DroppingRequestAmt.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(64, 124);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 14);
            this.label15.TabIndex = 24;
            this.label15.Text = "Budget Category";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_BCCode
            // 
            this.TxtDR_BCCode.EnterMoveNextControl = true;
            this.TxtDR_BCCode.Location = new System.Drawing.Point(170, 121);
            this.TxtDR_BCCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_BCCode.Name = "TxtDR_BCCode";
            this.TxtDR_BCCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_BCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_BCCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_BCCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_BCCode.Properties.MaxLength = 250;
            this.TxtDR_BCCode.Size = new System.Drawing.Size(300, 20);
            this.TxtDR_BCCode.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(19, 102);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(145, 14);
            this.label14.TabIndex = 22;
            this.label14.Text = "Project Implementation#";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_PRJIDocNo
            // 
            this.TxtDR_PRJIDocNo.EnterMoveNextControl = true;
            this.TxtDR_PRJIDocNo.Location = new System.Drawing.Point(170, 99);
            this.TxtDR_PRJIDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_PRJIDocNo.Name = "TxtDR_PRJIDocNo";
            this.TxtDR_PRJIDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_PRJIDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_PRJIDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_PRJIDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_PRJIDocNo.Properties.MaxLength = 250;
            this.TxtDR_PRJIDocNo.Size = new System.Drawing.Size(300, 20);
            this.TxtDR_PRJIDocNo.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(122, 80);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 14);
            this.label13.TabIndex = 20;
            this.label13.Text = "Month";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_Mth
            // 
            this.TxtDR_Mth.EnterMoveNextControl = true;
            this.TxtDR_Mth.Location = new System.Drawing.Point(170, 77);
            this.TxtDR_Mth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_Mth.Name = "TxtDR_Mth";
            this.TxtDR_Mth.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_Mth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_Mth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_Mth.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_Mth.Properties.MaxLength = 250;
            this.TxtDR_Mth.Size = new System.Drawing.Size(106, 20);
            this.TxtDR_Mth.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(132, 58);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 14);
            this.label12.TabIndex = 18;
            this.label12.Text = "Year";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_Yr
            // 
            this.TxtDR_Yr.EnterMoveNextControl = true;
            this.TxtDR_Yr.Location = new System.Drawing.Point(170, 55);
            this.TxtDR_Yr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_Yr.Name = "TxtDR_Yr";
            this.TxtDR_Yr.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_Yr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_Yr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_Yr.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_Yr.Properties.MaxLength = 250;
            this.TxtDR_Yr.Size = new System.Drawing.Size(106, 20);
            this.TxtDR_Yr.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(91, 36);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 14);
            this.label10.TabIndex = 16;
            this.label10.Text = "Department";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_DeptCode
            // 
            this.TxtDR_DeptCode.EnterMoveNextControl = true;
            this.TxtDR_DeptCode.Location = new System.Drawing.Point(170, 33);
            this.TxtDR_DeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_DeptCode.Name = "TxtDR_DeptCode";
            this.TxtDR_DeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_DeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_DeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_DeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_DeptCode.Properties.MaxLength = 250;
            this.TxtDR_DeptCode.Size = new System.Drawing.Size(300, 20);
            this.TxtDR_DeptCode.TabIndex = 17;
            // 
            // TxtDroppingRequestDocNo
            // 
            this.TxtDroppingRequestDocNo.EnterMoveNextControl = true;
            this.TxtDroppingRequestDocNo.Location = new System.Drawing.Point(170, 11);
            this.TxtDroppingRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDroppingRequestDocNo.Name = "TxtDroppingRequestDocNo";
            this.TxtDroppingRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDroppingRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDroppingRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDroppingRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDroppingRequestDocNo.Properties.MaxLength = 30;
            this.TxtDroppingRequestDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDroppingRequestDocNo.TabIndex = 13;
            this.TxtDroppingRequestDocNo.EditValueChanged += new System.EventHandler(this.TxtDroppingRequestDocNo_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(50, 14);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 14);
            this.label9.TabIndex = 12;
            this.label9.Text = "Dropping Request#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Appearance.PageClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.Tp3.Appearance.PageClient.Options.UseBackColor = true;
            this.Tp3.Controls.Add(this.panel4);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 308);
            this.Tp3.Text = "Upload File";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.splitContainer1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(766, 308);
            this.panel4.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(3, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.LblFile6);
            this.splitContainer1.Panel1.Controls.Add(this.LblFile2);
            this.splitContainer1.Panel1.Controls.Add(this.LblFile5);
            this.splitContainer1.Panel1.Controls.Add(this.LblFile);
            this.splitContainer1.Panel1.Controls.Add(this.LblFile4);
            this.splitContainer1.Panel1.Controls.Add(this.LblFile3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile6);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile2);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile6);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload2);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload6);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile6);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile3);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload6);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile2);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile5);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile2);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile5);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile3);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload5);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload3);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile5);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload5);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload3);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile4);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload2);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile4);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile3);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload4);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload4);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile4);
            this.splitContainer1.Size = new System.Drawing.Size(754, 259);
            this.splitContainer1.SplitterDistance = 51;
            this.splitContainer1.TabIndex = 48;
            // 
            // LblFile6
            // 
            this.LblFile6.AutoSize = true;
            this.LblFile6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile6.ForeColor = System.Drawing.Color.Black;
            this.LblFile6.Location = new System.Drawing.Point(25, 221);
            this.LblFile6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile6.Name = "LblFile6";
            this.LblFile6.Size = new System.Drawing.Size(24, 14);
            this.LblFile6.TabIndex = 42;
            this.LblFile6.Text = "File";
            this.LblFile6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblFile2
            // 
            this.LblFile2.AutoSize = true;
            this.LblFile2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile2.ForeColor = System.Drawing.Color.Black;
            this.LblFile2.Location = new System.Drawing.Point(25, 50);
            this.LblFile2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile2.Name = "LblFile2";
            this.LblFile2.Size = new System.Drawing.Size(24, 14);
            this.LblFile2.TabIndex = 18;
            this.LblFile2.Text = "File";
            this.LblFile2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblFile5
            // 
            this.LblFile5.AutoSize = true;
            this.LblFile5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile5.ForeColor = System.Drawing.Color.Black;
            this.LblFile5.Location = new System.Drawing.Point(25, 179);
            this.LblFile5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile5.Name = "LblFile5";
            this.LblFile5.Size = new System.Drawing.Size(24, 14);
            this.LblFile5.TabIndex = 36;
            this.LblFile5.Text = "File";
            this.LblFile5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblFile4
            // 
            this.LblFile4.AutoSize = true;
            this.LblFile4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile4.ForeColor = System.Drawing.Color.Black;
            this.LblFile4.Location = new System.Drawing.Point(25, 136);
            this.LblFile4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile4.Name = "LblFile4";
            this.LblFile4.Size = new System.Drawing.Size(24, 14);
            this.LblFile4.TabIndex = 30;
            this.LblFile4.Text = "File";
            this.LblFile4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblFile3
            // 
            this.LblFile3.AutoSize = true;
            this.LblFile3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile3.ForeColor = System.Drawing.Color.Black;
            this.LblFile3.Location = new System.Drawing.Point(25, 93);
            this.LblFile3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile3.Name = "LblFile3";
            this.LblFile3.Size = new System.Drawing.Size(24, 14);
            this.LblFile3.TabIndex = 24;
            this.LblFile3.Text = "File";
            this.LblFile3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFile6
            // 
            this.TxtFile6.EnterMoveNextControl = true;
            this.TxtFile6.Location = new System.Drawing.Point(3, 216);
            this.TxtFile6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile6.Name = "TxtFile6";
            this.TxtFile6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile6.Properties.Appearance.Options.UseFont = true;
            this.TxtFile6.Properties.MaxLength = 16;
            this.TxtFile6.Size = new System.Drawing.Size(390, 20);
            this.TxtFile6.TabIndex = 43;
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(426, 44);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(24, 21);
            this.BtnFile2.TabIndex = 21;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // ChkFile6
            // 
            this.ChkFile6.Location = new System.Drawing.Point(398, 217);
            this.ChkFile6.Name = "ChkFile6";
            this.ChkFile6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile6.Properties.Appearance.Options.UseFont = true;
            this.ChkFile6.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile6.Properties.Caption = " ";
            this.ChkFile6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile6.Size = new System.Drawing.Size(20, 22);
            this.ChkFile6.TabIndex = 44;
            this.ChkFile6.ToolTip = "Remove filter";
            this.ChkFile6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile6.ToolTipTitle = "Run System";
            this.ChkFile6.CheckedChanged += new System.EventHandler(this.ChkFile6_CheckedChanged);
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(457, 44);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 22;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // BtnDownload6
            // 
            this.BtnDownload6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload6.Appearance.Options.UseBackColor = true;
            this.BtnDownload6.Appearance.Options.UseFont = true;
            this.BtnDownload6.Appearance.Options.UseForeColor = true;
            this.BtnDownload6.Appearance.Options.UseTextOptions = true;
            this.BtnDownload6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload6.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDownload6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload6.Location = new System.Drawing.Point(457, 217);
            this.BtnDownload6.Name = "BtnDownload6";
            this.BtnDownload6.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload6.TabIndex = 46;
            this.BtnDownload6.ToolTip = "DownloadFile";
            this.BtnDownload6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload6.ToolTipTitle = "Run System";
            this.BtnDownload6.Click += new System.EventHandler(this.BtnDownload6_Click);
            // 
            // BtnFile6
            // 
            this.BtnFile6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile6.Appearance.Options.UseBackColor = true;
            this.BtnFile6.Appearance.Options.UseFont = true;
            this.BtnFile6.Appearance.Options.UseForeColor = true;
            this.BtnFile6.Appearance.Options.UseTextOptions = true;
            this.BtnFile6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile6.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnFile6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile6.Location = new System.Drawing.Point(426, 217);
            this.BtnFile6.Name = "BtnFile6";
            this.BtnFile6.Size = new System.Drawing.Size(24, 21);
            this.BtnFile6.TabIndex = 45;
            this.BtnFile6.ToolTip = "BrowseFile";
            this.BtnFile6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile6.ToolTipTitle = "Run System";
            this.BtnFile6.Click += new System.EventHandler(this.BtnFile6_Click);
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(3, 88);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 16;
            this.TxtFile3.Size = new System.Drawing.Size(390, 20);
            this.TxtFile3.TabIndex = 25;
            // 
            // PbUpload6
            // 
            this.PbUpload6.Location = new System.Drawing.Point(3, 239);
            this.PbUpload6.Name = "PbUpload6";
            this.PbUpload6.Size = new System.Drawing.Size(390, 17);
            this.PbUpload6.TabIndex = 47;
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(398, 44);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 20;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // TxtFile5
            // 
            this.TxtFile5.EnterMoveNextControl = true;
            this.TxtFile5.Location = new System.Drawing.Point(3, 174);
            this.TxtFile5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile5.Name = "TxtFile5";
            this.TxtFile5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile5.Properties.Appearance.Options.UseFont = true;
            this.TxtFile5.Properties.MaxLength = 16;
            this.TxtFile5.Size = new System.Drawing.Size(390, 20);
            this.TxtFile5.TabIndex = 37;
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(3, 45);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(390, 20);
            this.TxtFile2.TabIndex = 19;
            // 
            // ChkFile5
            // 
            this.ChkFile5.Location = new System.Drawing.Point(398, 175);
            this.ChkFile5.Name = "ChkFile5";
            this.ChkFile5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile5.Properties.Appearance.Options.UseFont = true;
            this.ChkFile5.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile5.Properties.Caption = " ";
            this.ChkFile5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile5.Size = new System.Drawing.Size(20, 22);
            this.ChkFile5.TabIndex = 38;
            this.ChkFile5.ToolTip = "Remove filter";
            this.ChkFile5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile5.ToolTipTitle = "Run System";
            this.ChkFile5.CheckedChanged += new System.EventHandler(this.ChkFile5_CheckedChanged);
            // 
            // BtnFile3
            // 
            this.BtnFile3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile3.Appearance.Options.UseBackColor = true;
            this.BtnFile3.Appearance.Options.UseFont = true;
            this.BtnFile3.Appearance.Options.UseForeColor = true;
            this.BtnFile3.Appearance.Options.UseTextOptions = true;
            this.BtnFile3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile3.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnFile3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile3.Location = new System.Drawing.Point(426, 87);
            this.BtnFile3.Name = "BtnFile3";
            this.BtnFile3.Size = new System.Drawing.Size(24, 21);
            this.BtnFile3.TabIndex = 27;
            this.BtnFile3.ToolTip = "BrowseFile";
            this.BtnFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile3.ToolTipTitle = "Run System";
            this.BtnFile3.Click += new System.EventHandler(this.BtnFile3_Click);
            // 
            // BtnDownload5
            // 
            this.BtnDownload5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload5.Appearance.Options.UseBackColor = true;
            this.BtnDownload5.Appearance.Options.UseFont = true;
            this.BtnDownload5.Appearance.Options.UseForeColor = true;
            this.BtnDownload5.Appearance.Options.UseTextOptions = true;
            this.BtnDownload5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload5.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDownload5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload5.Location = new System.Drawing.Point(457, 175);
            this.BtnDownload5.Name = "BtnDownload5";
            this.BtnDownload5.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload5.TabIndex = 40;
            this.BtnDownload5.ToolTip = "DownloadFile";
            this.BtnDownload5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload5.ToolTipTitle = "Run System";
            this.BtnDownload5.Click += new System.EventHandler(this.BtnDownload5_Click);
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(3, 111);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(390, 17);
            this.PbUpload3.TabIndex = 29;
            // 
            // BtnFile5
            // 
            this.BtnFile5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile5.Appearance.Options.UseBackColor = true;
            this.BtnFile5.Appearance.Options.UseFont = true;
            this.BtnFile5.Appearance.Options.UseForeColor = true;
            this.BtnFile5.Appearance.Options.UseTextOptions = true;
            this.BtnFile5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile5.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnFile5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile5.Location = new System.Drawing.Point(426, 175);
            this.BtnFile5.Name = "BtnFile5";
            this.BtnFile5.Size = new System.Drawing.Size(24, 21);
            this.BtnFile5.TabIndex = 39;
            this.BtnFile5.ToolTip = "BrowseFile";
            this.BtnFile5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile5.ToolTipTitle = "Run System";
            this.BtnFile5.Click += new System.EventHandler(this.BtnFile5_Click);
            // 
            // PbUpload5
            // 
            this.PbUpload5.Location = new System.Drawing.Point(3, 197);
            this.PbUpload5.Name = "PbUpload5";
            this.PbUpload5.Size = new System.Drawing.Size(390, 17);
            this.PbUpload5.TabIndex = 41;
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(457, 87);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload3.TabIndex = 28;
            this.BtnDownload3.ToolTip = "DownloadFile";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // TxtFile4
            // 
            this.TxtFile4.EnterMoveNextControl = true;
            this.TxtFile4.Location = new System.Drawing.Point(3, 131);
            this.TxtFile4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile4.Name = "TxtFile4";
            this.TxtFile4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile4.Properties.Appearance.Options.UseFont = true;
            this.TxtFile4.Properties.MaxLength = 16;
            this.TxtFile4.Size = new System.Drawing.Size(390, 20);
            this.TxtFile4.TabIndex = 31;
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(3, 68);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(390, 17);
            this.PbUpload2.TabIndex = 23;
            // 
            // ChkFile4
            // 
            this.ChkFile4.Location = new System.Drawing.Point(398, 132);
            this.ChkFile4.Name = "ChkFile4";
            this.ChkFile4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile4.Properties.Appearance.Options.UseFont = true;
            this.ChkFile4.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile4.Properties.Caption = " ";
            this.ChkFile4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile4.Size = new System.Drawing.Size(20, 22);
            this.ChkFile4.TabIndex = 32;
            this.ChkFile4.ToolTip = "Remove filter";
            this.ChkFile4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile4.ToolTipTitle = "Run System";
            this.ChkFile4.CheckedChanged += new System.EventHandler(this.ChkFile4_CheckedChanged);
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(398, 87);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 26;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // BtnDownload4
            // 
            this.BtnDownload4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload4.Appearance.Options.UseBackColor = true;
            this.BtnDownload4.Appearance.Options.UseFont = true;
            this.BtnDownload4.Appearance.Options.UseForeColor = true;
            this.BtnDownload4.Appearance.Options.UseTextOptions = true;
            this.BtnDownload4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload4.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDownload4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload4.Location = new System.Drawing.Point(457, 132);
            this.BtnDownload4.Name = "BtnDownload4";
            this.BtnDownload4.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload4.TabIndex = 34;
            this.BtnDownload4.ToolTip = "DownloadFile";
            this.BtnDownload4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload4.ToolTipTitle = "Run System";
            this.BtnDownload4.Click += new System.EventHandler(this.BtnDownload4_Click);
            // 
            // PbUpload4
            // 
            this.PbUpload4.Location = new System.Drawing.Point(3, 154);
            this.PbUpload4.Name = "PbUpload4";
            this.PbUpload4.Size = new System.Drawing.Size(390, 17);
            this.PbUpload4.TabIndex = 35;
            // 
            // BtnFile4
            // 
            this.BtnFile4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile4.Appearance.Options.UseBackColor = true;
            this.BtnFile4.Appearance.Options.UseFont = true;
            this.BtnFile4.Appearance.Options.UseForeColor = true;
            this.BtnFile4.Appearance.Options.UseTextOptions = true;
            this.BtnFile4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile4.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnFile4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile4.Location = new System.Drawing.Point(426, 132);
            this.BtnFile4.Name = "BtnFile4";
            this.BtnFile4.Size = new System.Drawing.Size(24, 21);
            this.BtnFile4.TabIndex = 33;
            this.BtnFile4.ToolTip = "BrowseFile";
            this.BtnFile4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile4.ToolTipTitle = "Run System";
            this.BtnFile4.Click += new System.EventHandler(this.BtnFile4_Click);
            // 
            // Tp5
            // 
            this.Tp5.Appearance.Header.Options.UseTextOptions = true;
            this.Tp5.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp5.Controls.Add(this.GrdReview);
            this.Tp5.Name = "Tp5";
            this.Tp5.Size = new System.Drawing.Size(766, 308);
            this.Tp5.Text = "Review";
            // 
            // GrdReview
            // 
            this.GrdReview.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.GrdReview.DefaultRow.Height = 20;
            this.GrdReview.DefaultRow.Sortable = false;
            this.GrdReview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrdReview.ForeColor = System.Drawing.SystemColors.WindowText;
            this.GrdReview.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.GrdReview.Header.Height = 21;
            this.GrdReview.Location = new System.Drawing.Point(0, 0);
            this.GrdReview.Name = "GrdReview";
            this.GrdReview.RowHeader.Visible = true;
            this.GrdReview.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.GrdReview.SingleClickEdit = true;
            this.GrdReview.Size = new System.Drawing.Size(766, 308);
            this.GrdReview.TabIndex = 50;
            this.GrdReview.TreeCol = null;
            this.GrdReview.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // Tp6
            // 
            this.Tp6.Appearance.Header.Options.UseTextOptions = true;
            this.Tp6.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp6.Controls.Add(this.Grd2);
            this.Tp6.Name = "Tp6";
            this.Tp6.Size = new System.Drawing.Size(766, 308);
            this.Tp6.Text = "RFQ List";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 308);
            this.Grd2.TabIndex = 51;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // LueDurationUom
            // 
            this.LueDurationUom.EnterMoveNextControl = true;
            this.LueDurationUom.Location = new System.Drawing.Point(340, 21);
            this.LueDurationUom.Margin = new System.Windows.Forms.Padding(5);
            this.LueDurationUom.Name = "LueDurationUom";
            this.LueDurationUom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.Appearance.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDurationUom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDurationUom.Properties.DropDownRows = 14;
            this.LueDurationUom.Properties.NullText = "[Empty]";
            this.LueDurationUom.Properties.PopupWidth = 200;
            this.LueDurationUom.Size = new System.Drawing.Size(125, 20);
            this.LueDurationUom.TabIndex = 61;
            this.LueDurationUom.ToolTip = "F4 : Show/hide list";
            this.LueDurationUom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDurationUom.EditValueChanged += new System.EventHandler(this.LueDurationUom_EditValueChanged);
            this.LueDurationUom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueDurationUom_KeyDown);
            this.LueDurationUom.Leave += new System.EventHandler(this.LueDurationUom_Leave);
            // 
            // LuePtCode
            // 
            this.LuePtCode.EnterMoveNextControl = true;
            this.LuePtCode.Location = new System.Drawing.Point(475, 21);
            this.LuePtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePtCode.Name = "LuePtCode";
            this.LuePtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.Appearance.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePtCode.Properties.DropDownRows = 14;
            this.LuePtCode.Properties.NullText = "[Empty]";
            this.LuePtCode.Properties.PopupWidth = 200;
            this.LuePtCode.Size = new System.Drawing.Size(125, 20);
            this.LuePtCode.TabIndex = 62;
            this.LuePtCode.ToolTip = "F4 : Show/hide list";
            this.LuePtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePtCode.EditValueChanged += new System.EventHandler(this.LuePtCode_EditValueChanged);
            this.LuePtCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePtCode_KeyDown);
            this.LuePtCode.Leave += new System.EventHandler(this.LuePtCode_Leave);
            // 
            // FrmMaterialRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 460);
            this.Name = "FrmMaterialRequest";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOQtyCancelDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDORequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueMRType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBudgetForMR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRMInd.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEproc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtApprovalSheetDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrandTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcurementType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTenderInd.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Balance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_MRAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDR_Remark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DroppingRequestAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_BCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_PRJIDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Mth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Yr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDroppingRequestDocNo.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            this.Tp5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GrdReview)).EndInit();
            this.Tp6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDurationUom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblDept;
        private System.Windows.Forms.Label LblReqType;
        internal DevExpress.XtraEditors.TextEdit TxtRemainingBudget;
        private System.Windows.Forms.Label LblAvailableBudget;
        internal DevExpress.XtraEditors.DateEdit DteUsageDt;
        public DevExpress.XtraEditors.SimpleButton BtnPOQtyCancelDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtPOQtyCancelDocNo;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnPOQtyCancelDocNo;
        private System.Windows.Forms.Label label76;
        public DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label LblSiteCode;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label LblBudgetCt;
        public DevExpress.XtraEditors.LookUpEdit LueBCCode;
        private System.Windows.Forms.Label LblDORequestDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnDORequestDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnDORequestDocNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtDORequestDocNo;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label LblFile;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.ProgressBar PbUpload;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.SaveFileDialog SFD;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        private System.Windows.Forms.Label LblPICCode;
        public DevExpress.XtraEditors.LookUpEdit LuePICCode;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtDR_DroppingRequestAmt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtDR_Balance;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtDR_MRAmt;
        internal DevExpress.XtraEditors.TextEdit TxtDroppingRequestDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtDR_PRJIDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtDR_Mth;
        internal DevExpress.XtraEditors.TextEdit TxtDR_Yr;
        internal DevExpress.XtraEditors.TextEdit TxtDR_DeptCode;
        internal DevExpress.XtraEditors.MemoExEdit MeeDR_Remark;
        internal DevExpress.XtraEditors.TextEdit TxtDR_BCCode;
        internal DevExpress.XtraEditors.LookUpEdit LueReqType;
        private DevExpress.XtraEditors.LookUpEdit LueDurationUom;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        public DevExpress.XtraEditors.SimpleButton BtnFile3;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label LblFile2;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        private System.Windows.Forms.ProgressBar PbUpload3;
        private System.Windows.Forms.ProgressBar PbUpload2;
        private System.Windows.Forms.Label LblFile3;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.LookUpEdit LuePtCode;
        protected System.Windows.Forms.Panel panel8;
        internal DevExpress.XtraEditors.LookUpEdit LueProcurementType;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtFile4;
        private System.Windows.Forms.Label LblFile4;
        private DevExpress.XtraEditors.CheckEdit ChkFile4;
        public DevExpress.XtraEditors.SimpleButton BtnDownload4;
        public DevExpress.XtraEditors.SimpleButton BtnFile4;
        private System.Windows.Forms.ProgressBar PbUpload4;
        internal DevExpress.XtraEditors.TextEdit TxtFile5;
        private System.Windows.Forms.Label LblFile5;
        private DevExpress.XtraEditors.CheckEdit ChkFile5;
        public DevExpress.XtraEditors.SimpleButton BtnDownload5;
        public DevExpress.XtraEditors.SimpleButton BtnFile5;
        private System.Windows.Forms.ProgressBar PbUpload5;
        internal DevExpress.XtraEditors.TextEdit TxtFile6;
        private System.Windows.Forms.Label LblFile6;
        private DevExpress.XtraEditors.CheckEdit ChkFile6;
        public DevExpress.XtraEditors.SimpleButton BtnDownload6;
        public DevExpress.XtraEditors.SimpleButton BtnFile6;
        private System.Windows.Forms.ProgressBar PbUpload6;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraTab.XtraTabPage Tp5;
        protected internal TenTec.Windows.iGridLib.iGrid GrdReview;
        public DevExpress.XtraEditors.SimpleButton BtnReference;
        private System.Windows.Forms.Label LblReference;
        internal DevExpress.XtraEditors.TextEdit TxtReference;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        internal DevExpress.XtraEditors.CheckEdit ChkTenderInd;
        internal DevExpress.XtraEditors.CheckEdit ChkRMInd;
        public DevExpress.XtraEditors.SimpleButton BtnDroppingRequestDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnDroppingRequestDocNo;
        private DevExpress.XtraTab.XtraTabPage Tp6;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Label LblBudgetMR;
        internal DevExpress.XtraEditors.TextEdit TxtBudgetForMR;
        private System.Windows.Forms.Label LblRemark;
        internal DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtGrandTotal;
        internal DevExpress.XtraEditors.LookUpEdit LueMRType;
        private System.Windows.Forms.Label LblMRType;
        public DevExpress.XtraEditors.SimpleButton BtnApprovalSheetDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnApprovalSheetDocNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtApprovalSheetDocNo;
        private System.Windows.Forms.Label LblApprovalSheet;
        private System.Windows.Forms.Label LblEPROC;
        internal DevExpress.XtraEditors.TextEdit TxtEproc;
    }
}