﻿#region Update
/*  
    03/05/2017 [TKG] menampilkan data dengan qty > 0 untuk filter back date
    10/07/2018 [WED] tambah filter multi warehouse
    06/08/2019 [TKG] bug proses multi warehouse
    23/03/2020 [TKG/IMS] berdasarkan parameter IsInvTrnShowItSpec, menampilkan specifikasi item
    30/04/2020 [IBL/IMS] Penambahan field Nama Project dan Nomer PO Customer berdasarkan parameter IsRptStockSumary2ShowProjectInfo
    30/09/2020 [TKG/IMS] tambah item's local code
    13/10/2020 [IBL/MGI] tambah kolom item's category name
    29/12/2020 [DITA/SRN] ubah kolom QTY menjadi QTY 1, UOM menjadi UOM 1, QTY menjadi QTY2, UOM menjadi UOM 2
    11/03/2021 [TKG/IMS] menambah heat number
    13/03/2021 [TKG/IMS] merubah proses GetParameter
    13/03/2021 [TKG/IMS] berdasarkan parameter IsRptStockSummary2InclSourceAsDefault menentukan apakan filter exclude source ditick atau tidak
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;
using System.Text.RegularExpressions;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockSummary2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mDbBackup = string.Empty
            ;
        private int mNumberOfInventoryUomCode = 1;
        private bool
            IsReCompute = false,
            mIsUseDbBackup = false,
            mIsShowForeignName = false,
            mIsItGrpCode = false,
            mIsInventoryRptFilterByGrpWhs = false,
            mIsFilterByItCt = false,
            mIsInvTrnShowItSpec = false,
            mIsRptStockSummary2ShowProjectInfo = false,
            mIsStockSummary2HeatNumberEnabled = false,
            mIsRptStockSummary2InclSourceAsDefault = false;
        private string[] mCols = null;
        
        #endregion

        #region Constructor

        public FrmRptStockSummary2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetCols();
                SetGrd();
                SetSQL();
                Sm.SetDteCurrentDate(DteDocDt);
                ChkExcludeSource.Checked = true;
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCatCode, mIsFilterByItCt ? "Y" : "N");
                if (mIsInventoryRptFilterByGrpWhs)
                    Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                else
                    Sl.SetLueWhsCode(ref LueWhsCode);
                SetCcbWhsName(ref CcbWhsName, string.Empty);
                ChkExcludeSource.Checked = !mIsRptStockSummary2InclSourceAsDefault;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            //SetNumberOfInventoryUomCode();
            //mDbBackup = Sm.GetParameter("DbBackup");
            //mIsItGrpCode = Sm.GetParameter("IsItGrpCodeShow") == "N";
            //mIsUseDbBackup = Sm.GetParameterBoo("IsUseDbBackup");
            //mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            //mIsInventoryRptFilterByGrpWhs = Sm.GetParameterBoo("IsInventoryRptFilterByGrpWhs");
            //mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            //mIsInvTrnShowItSpec = Sm.GetParameterBoo("IsInvTrnShowItSpec");
            //mIsRptStockSummary2ShowProjectInfo = Sm.GetParameterBoo("IsRptStockSummary2ShowProjectInfo");
            //mIsStockSummary2HeatNumberEnabled = Sm.GetParameterBoo("IsStockSummary2HeatNumberEnabled");
            //mIsRptStockSummary2InclSourceAsDefault = Sm.GetParameterBoo("IsRptStockSummary2InclSourceAsDefault");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsUseDbBackup', 'IsShowForeignName', 'IsInventoryRptFilterByGrpWhs', 'IsFilterByItCt', 'IsInvTrnShowItSpec', ");
            SQL.AppendLine("'IsRptStockSummary2ShowProjectInfo', 'IsStockSummary2HeatNumberEnabled', 'IsRptStockSummary2InclSourceAsDefault', 'IsItGrpCodeShow', 'DbBackup', ");
            SQL.AppendLine("'NumberOfInventoryUomCode' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch(ParCode)
                        {
                            //boolean
                            case "IsUseDbBackup": mIsUseDbBackup = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsInventoryRptFilterByGrpWhs": mIsInventoryRptFilterByGrpWhs = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsInvTrnShowItSpec": mIsInvTrnShowItSpec = ParValue == "Y"; break;
                            case "IsRptStockSummary2ShowProjectInfo": mIsRptStockSummary2ShowProjectInfo = ParValue == "Y"; break;
                            case "IsStockSummary2HeatNumberEnabled": mIsStockSummary2HeatNumberEnabled = ParValue == "Y"; break;
                            case "IsRptStockSummary2InclSourceAsDefault": mIsRptStockSummary2InclSourceAsDefault = ParValue == "Y"; break;
                            case "IsItGrpCodeShow": mIsItGrpCode = ParValue == "N"; break;

                            //string
                            case "DbBackup": mDbBackup = ParValue; break;

                            //integer
                            case "NumberOfInventoryUomCode": if (ParValue.Length > 0) mNumberOfInventoryUomCode = int.Parse(ParValue); break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private string SetSQL(bool IsExcludeSource, string Filter)
        {
            var SQL = new StringBuilder();

            if (!IsExcludeSource)
            {
                SQL.AppendLine("/* Stock Summary 2-1a */ ");

                SQL.AppendLine("    Select A.WhsCode, C.WhsName, A.Lot, A.Bin, A.ItCode, B.ItName, B.ItCodeInternal, B.ForeignName, D.PropName, A.BatchNo, A.Source, ");
                SQL.AppendLine("    A.Qty, B.InventoryUOMCode ");
                if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , A.Qty2, B.InventoryUOMCode2 ");
                if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , A.Qty3, B.InventoryUOMCode3 ");
                SQL.AppendLine("    ,B.ItGrpCode, E.ItGrpName, F.ItScName, B.Specification ");
                if (mIsRptStockSummary2ShowProjectInfo)
                    SQL.AppendLine("    , G.ProjectName, H.Value1 As PONo, ");
                else
                    SQL.AppendLine("    ,Null As ProjectName, Null As PONo, ");
                if (mIsStockSummary2HeatNumberEnabled)
                    SQL.AppendLine("    H.Value2 as HeatNumber, ");
                else
                    SQL.AppendLine("    Null as HeatNumber, ");
                SQL.AppendLine("I.ItCtName ");
                SQL.AppendLine("    From TblStockSummary A");
                SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("    Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
                SQL.AppendLine("    Left Join TblProperty D On A.PropCode=D.PropCode ");
                SQL.AppendLine("    Left Join TblItemGroup E On B.ItGrpCode = E.ItGrpCode ");
                SQL.AppendLine("    Left Join TblItemSubCategory F On B.ItScCode = F.ItScCode ");
                if (mIsRptStockSummary2ShowProjectInfo)
                    SQL.AppendLine("    Left Join TblProjectGroup G On A.BatchNo = G.ProjectCode ");
                if (mIsRptStockSummary2ShowProjectInfo || mIsStockSummary2HeatNumberEnabled)
                    SQL.AppendLine("    Left Join TblSourceInfo H On A.Source = H.Source");
                SQL.AppendLine("Inner Join TblItemCategory I On B.ItCtCode = I.ItCtCode ");
                SQL.AppendLine("    Where (");
                SQL.AppendLine("        A.Qty<>0 ");
                if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("        Or A.Qty2<>0 ");
                if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("        Or A.Qty3<>0 ");
                SQL.AppendLine(") " + Filter);
                if (mIsInventoryRptFilterByGrpWhs)
                {
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                    SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                    SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            else
            {
                SQL.AppendLine("/* Stock Summary 2-1b */ ");

                SQL.AppendLine("    Select A.WhsCode, C.WhsName, A.Lot, A.Bin, A.ItCode, B.ItName, B.ItCodeInternal, B.ForeignName, D.PropName, A.BatchNo, ");
                SQL.AppendLine("    '-' As Source, ");
                SQL.AppendLine("    A.Qty, B.InventoryUOMCode ");
                if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , A.Qty2, B.InventoryUOMCode2 ");
                if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , A.Qty3, B.InventoryUOMCode3 ");
                SQL.AppendLine("    ,B.ItGrpCode, E.ItGrpName, F.ItScName, B.Specification ");
                if (mIsRptStockSummary2ShowProjectInfo)
                    SQL.AppendLine("    , G.ProjectName, Null As PONo, ");
                else
                    SQL.AppendLine("    , Null As ProjectName, Null As PONo, ");
                SQL.AppendLine("    Null As HeatNumber, ");
                SQL.AppendLine("    H.ItCtName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, ");
                SQL.AppendLine("        Sum(A.Qty) As Qty ");
                if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("        , Sum(Qty2) As Qty2 ");
                if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("        , Sum(Qty3) As Qty3 ");
                SQL.AppendLine("        From TblStockSummary A, TblItem B ");
                SQL.AppendLine("        Where A.ItCode=B.ItCode " + Filter + " And (");
                SQL.AppendLine("        A.Qty<>0 ");
                if (mIsInventoryRptFilterByGrpWhs)
                {
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
                    SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                    SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("        ) Group By Concat(A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo) ");
                SQL.AppendLine("    ) A ");
                SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("    Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
                SQL.AppendLine("    Left Join TblProperty D On A.PropCode=D.PropCode ");
                SQL.AppendLine("    Left Join TblItemGroup E On B.ItGrpCode = E.ItGrpCode ");
                SQL.AppendLine("    Left Join TblItemSubCategory F On B.ItScCode = F.ItScCode ");
                if (mIsRptStockSummary2ShowProjectInfo)
                    SQL.AppendLine("    Left Join TblProjectGroup G On A.BatchNo = G.ProjectCode ");
                SQL.AppendLine("Inner Join TblItemCategory H On B.ItCtCode = H.ItCtCode ");
                SQL.AppendLine("Where 0=0 ");
            }

            return SQL.ToString();
        }

        private string SetSQL2(string DocDt, bool IsExcludeSource, string Filter)
        {
            var SQL = new StringBuilder();

            if (!IsExcludeSource)
            {
                if (mIsUseDbBackup)
                {
                    SQL.AppendLine("/* Stock Summary 2-2a */ ");
                    SQL.AppendLine("    Select A.WhsCode, C.WhsName, A.Lot, A.Bin, A.ItCode, B.ItName, B.ItCodeInternal, B.ForeignName, D.PropName, A.BatchNo, A.Source, ");
                    SQL.AppendLine("    A.Qty, B.InventoryUOMCode ");
                    if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , A.Qty2 As Qty2, B.InventoryUOMCode2 ");
                    if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , A.Qty3, B.InventoryUOMCode3 ");
                    SQL.AppendLine("    ,B.ItGrpCode, E.ItGrpName, F.ItScName, B.Specification ");
                    if (mIsRptStockSummary2ShowProjectInfo)
                        SQL.AppendLine("    , G.ProjectName, H.Value1 As PONo, ");
                    else
                        SQL.AppendLine("    , Null As ProjectName, Null As PONo, ");
                    if (mIsStockSummary2HeatNumberEnabled)
                        SQL.AppendLine("    H.Value2 As HeatNumber, ");
                    else
                        SQL.AppendLine("    Null As HeatNumber, ");
                    SQL.AppendLine("    I.ItCtName ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, ");
                    SQL.AppendLine("        Sum(Qty) As Qty ");
                    if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("        , Sum(Qty2) As Qty2 ");
                    if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("        , Sum(Qty3) As Qty3 ");

                    SQL.AppendLine("        From ( ");
                    SQL.AppendLine("            Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source, A.Qty, A.Qty2, A.Qty3 ");
                    SQL.AppendLine("            From TblStockMovement A, TblItem B ");
                    SQL.AppendLine("            Where A.ItCode=B.ItCode And A.DocDt<=" + DocDt + Filter);
                    if (mIsInventoryRptFilterByGrpWhs)
                    {
                        SQL.AppendLine("    And Exists( ");
                        SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
                        SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                        SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("            Union All ");
                    SQL.AppendLine("            Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source, A.Qty, A.Qty2, A.Qty3 ");
                    SQL.AppendLine("            From " + mDbBackup + ".TblStockMovement A, TblItem B ");
                    SQL.AppendLine("            Where A.ItCode=B.ItCode And A.DocDt<=" + DocDt + Filter);
                    if (mIsInventoryRptFilterByGrpWhs)
                    {
                        SQL.AppendLine("    And Exists( ");
                        SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
                        SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                        SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("        ) Tbl ");


                    SQL.AppendLine("        Group By Concat(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source) ");
                    SQL.AppendLine("        Having Sum(Qty)<>0 ");
                    SQL.AppendLine("    ) A ");
                    SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
                    if (mIsFilterByItCt)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                        SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("    Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
                    SQL.AppendLine("    Left Join TblProperty D On A.PropCode=D.PropCode ");
                    SQL.AppendLine("    Left Join TblItemGroup E On B.ItGrpCode = E.ItGrpCode ");
                    SQL.AppendLine("    Left Join TblItemSubCategory F On B.ItScCode = F.ItScCode ");
                    if (mIsRptStockSummary2ShowProjectInfo)
                        SQL.AppendLine("    Left Join TblProjectGroup G On A.BatchNo = G.ProjectCode ");
                    if (mIsRptStockSummary2ShowProjectInfo || mIsStockSummary2HeatNumberEnabled)
                        SQL.AppendLine("    Left Join TblSourceInfo H On A.Source=H.Source ");
                    SQL.AppendLine("    Inner Join TblItemCategory I On B.ItCtCode = I.ItCtCode ");
                    SQL.AppendLine("Where A.Qty>0 ");
                }
                else
                {
                    SQL.AppendLine("/* Stock Summary 2-2a */ ");
                    SQL.AppendLine("    Select A.WhsCode, C.WhsName, A.Lot, A.Bin, A.ItCode, B.ItName, B.ItCodeInternal, B.ForeignName, D.PropName, A.BatchNo, A.Source, ");
                    SQL.AppendLine("    A.Qty, B.InventoryUOMCode ");
                    if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , A.Qty2 As Qty2, B.InventoryUOMCode2 ");
                    if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , A.Qty3, B.InventoryUOMCode3 ");
                    SQL.AppendLine("    ,B.ItGrpCode, E.ItGrpName, F.ItScName, B.Specification ");
                    if (mIsRptStockSummary2ShowProjectInfo)
                        SQL.AppendLine("    ,G.ProjectName, H.Value1 as PONo, ");
                    else
                        SQL.AppendLine("    , Null As ProjectName, Null As PONo, ");
                    if (mIsStockSummary2HeatNumberEnabled)
                        SQL.AppendLine("    H.Value2 As HeatNumber, ");
                    else
                        SQL.AppendLine("    Null As HeatNumber, ");
                    SQL.AppendLine("    I.ItCtName ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source, ");
                    SQL.AppendLine("        Sum(A.Qty) As Qty ");
                    if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("        , Sum(A.Qty2) As Qty2 ");
                    if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("        , Sum(A.Qty3) As Qty3 ");

                    SQL.AppendLine("        From TblStockMovement A, TblItem B ");
                    SQL.AppendLine("        Where A.ItCode=B.ItCode And A.DocDt<=" + DocDt + Filter);
                    if (mIsInventoryRptFilterByGrpWhs)
                    {
                        SQL.AppendLine("    And Exists( ");
                        SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                        SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                        SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }

                    SQL.AppendLine("        Group By Concat(A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source) ");
                    SQL.AppendLine("        Having Sum(A.Qty)<>0 ");
                    //if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("        Or Sum(A.Qty2)<>0 ");
                    //if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("        Or Sum(A.Qty3)<>0 ");
                    SQL.AppendLine("    ) A ");
                    SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
                    if (mIsFilterByItCt)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                        SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("    Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
                    SQL.AppendLine("    Left Join TblProperty D On A.PropCode=D.PropCode ");
                    SQL.AppendLine("    Left Join TblItemGroup E On B.ItGrpCode = E.ItGrpCode ");
                    SQL.AppendLine("    Left Join TblItemSubCategory F On B.ItScCode = F.ItScCode ");
                    if (mIsRptStockSummary2ShowProjectInfo)
                        SQL.AppendLine("    Left Join TblProjectGroup G On A.BatchNo = G.ProjectCode ");
                    if (mIsRptStockSummary2ShowProjectInfo || mIsStockSummary2HeatNumberEnabled)
                        SQL.AppendLine("    Left Join TblSourceInfo H On A.Source = H.Source ");
                    SQL.AppendLine("Inner Join TblItemCategory I On B.ItCtCode = I.ItCtCode ");
                    SQL.AppendLine("Where A.Qty>0 ");
                }
            }
            else
            {
                if (mIsUseDbBackup)
                {
                    SQL.AppendLine("/* Stock Summary 2-2c */ ");
                    SQL.AppendLine("    Select A.WhsCode, C.WhsName, A.Lot, A.Bin, A.ItCode, B.ItName, B.ItCodeInternal, B.ForeignName, D.PropName, A.BatchNo, ");
                    SQL.AppendLine("    '-' As Source, ");
                    SQL.AppendLine("    A.Qty, B.InventoryUOMCode ");
                    if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , A.Qty2 As Qty2, B.InventoryUOMCode2 ");
                    if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , A.Qty3, B.InventoryUOMCode3 ");
                    SQL.AppendLine("    ,B.ItGrpCode, E.ItGrpName, F.ItScName, B.Specification ");
                    if (mIsRptStockSummary2ShowProjectInfo)
                        SQL.AppendLine("    ,G.ProjectName, Null As PONo, ");
                    else
                        SQL.AppendLine("    , Null As ProjectName, Null As PONo, ");
                    SQL.AppendLine("    Null As HeatNumber, ");
                    SQL.AppendLine("    H.ItCtName ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
                    SQL.AppendLine("        Sum(Qty) As Qty ");
                    if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("        , Sum(Qty2) As Qty2 ");
                    if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("        , Sum(Qty3) As Qty3 ");
                    SQL.AppendLine("        From ( ");
                    SQL.AppendLine("        Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Qty, A.Qty2, A.Qty3 ");
                    SQL.AppendLine("        From TblStockMovement A, TblItem B ");
                    SQL.AppendLine("        Where A.ItCode=B.ItCode And A.DocDt<=" + DocDt + Filter);
                    if (mIsInventoryRptFilterByGrpWhs)
                    {
                        SQL.AppendLine("    And Exists( ");
                        SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
                        SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                        SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Qty, A.Qty2, A.Qty3 ");
                    SQL.AppendLine("        From "+mDbBackup+".TblStockMovement A, TblItem B ");
                    SQL.AppendLine("        Where A.ItCode=B.ItCode And A.DocDt<=" + DocDt + Filter);
                    if (mIsInventoryRptFilterByGrpWhs)
                    {
                        SQL.AppendLine("    And Exists( ");
                        SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
                        SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                        SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("        ) Tbl ");

                    SQL.AppendLine("        Group By Concat(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo) ");
                    SQL.AppendLine("        Having Sum(Qty)<>0 ");
                    SQL.AppendLine("    ) A ");
                    SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
                    if (mIsFilterByItCt)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                        SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("    Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
                    SQL.AppendLine("    Left Join TblProperty D On A.PropCode=D.PropCode ");
                    SQL.AppendLine("    Left Join TblItemGroup E On B.ItGrpCode = E.ItGrpCode ");
                    SQL.AppendLine("    Left Join TblItemSubCategory F On B.ItScCode = F.ItScCode ");
                    SQL.AppendLine("    Left Join TblProjectGroup G On A.BatchNo = G.ProjectCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory H On B.ItCtCode = H.ItCtCode ");
                    SQL.AppendLine("Where A.Qty>0 ");                
                }
                else
                {
                    SQL.AppendLine("/* Stock Summary 2-2b */ ");
                    SQL.AppendLine("    Select A.WhsCode, C.WhsName, A.Lot, A.Bin, A.ItCode, B.ItName, B.ItCodeInternal, B.ForeignName, D.PropName, A.BatchNo, ");
                    SQL.AppendLine("    '-' As Source, ");
                    SQL.AppendLine("    A.Qty, B.InventoryUOMCode ");
                    if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , A.Qty2 As Qty2, B.InventoryUOMCode2 ");
                    if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , A.Qty3, B.InventoryUOMCode3 ");
                    SQL.AppendLine("    , B.ItGrpCode, E.ItGrpName, F.ItScName, B.Specification ");
                    if (mIsRptStockSummary2ShowProjectInfo)
                        SQL.AppendLine("    , G.ProjectName, '-' as PONo, ");
                    else
                        SQL.AppendLine("    , Null as ProjectName, '-' as PONo, ");
                    SQL.AppendLine("    Null As HeatNumber, ");
                    SQL.AppendLine("    H.ItCtName ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, ");
                    SQL.AppendLine("        Sum(A.Qty) As Qty ");
                    if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("        , Sum(A.Qty2) As Qty2 ");
                    if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("        , Sum(A.Qty3) As Qty3 ");
                    SQL.AppendLine("        From TblStockMovement A, TblItem B ");
                    SQL.AppendLine("        Where A.ItCode=B.ItCode And A.DocDt<=" + DocDt + Filter);
                    if (mIsInventoryRptFilterByGrpWhs)
                    {
                        SQL.AppendLine("    And Exists( ");
                        SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                        SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                        SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("        Group By Concat(A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo) ");
                    SQL.AppendLine("        Having Sum(A.Qty)<>0 ");
                    SQL.AppendLine("    ) A ");
                    SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
                    if (mIsFilterByItCt)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                        SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("    Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
                    SQL.AppendLine("    Left Join TblProperty D On A.PropCode=D.PropCode ");
                    SQL.AppendLine("    Left Join TblItemGroup E On B.ItGrpCode = E.ItGrpCode ");
                    SQL.AppendLine("    Left Join TblItemSubCategory F On B.ItScCode = F.ItScCode ");
                    if (mIsRptStockSummary2ShowProjectInfo)
                        SQL.AppendLine("    Left Join TblProjectGroup G On A.BatchNo = G.ProjectCode ");
                    SQL.AppendLine("Inner Join TblItemCategory H On B.ItCtCode = H.ItCtCode ");
                    SQL.AppendLine("Where A.Qty>0 ");
                }
            }

            return SQL.ToString();
        }

        //private void SetNumberOfInventoryUomCode()
        //{
        //    string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode"); 
        //    if (NumberOfInventoryUomCode.Length == 0)
        //        mNumberOfInventoryUomCode = 1;
        //    else
        //        mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        //}

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Warehouse", 
                        "Lot",
                        "Bin",   
                        "Item's Code", 
                        "Item's Name", 

                        //6-10
                        "Property",
                        "Batch#",
                        "Source",
                        "Quantity 1", 
                        "UoM 1",
                        
                        //11-15
                        "Quantity 2", 
                        "UoM 2",
                        "Quantity",
                        "UoM",
                        "Item Group"+Environment.NewLine+"Code",

                        //16-20
                        "Item Group"+Environment.NewLine+"Name",
                        "Foreign Name",
                        "Sub-Category",
                        "Specification",
                        "Project's Name",

                        //21-24
                        "Customer's PO#",
                        "Local Code",
                        "Item's Category"+Environment.NewLine+"Name",
                        "Heat Number"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 60, 80, 80, 250, 
                        
                        //6-10
                        80, 200, 200, 100, 80,  

                        //11-15
                        100, 80, 100, 80, 100,

                        //16-20
                        150, 150, 150, 200, 200,

                        //21-24
                        130, 100, 250, 200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 13 }, 0);
            Grd1.Cols[17].Move(6);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 8, 11, 12, 13, 14, 18, 19 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            if (mIsItGrpCode) Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, false);
            if(!mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 17 }, false);
            Grd1.Cols[15].Move(7);
            Grd1.Cols[16].Move(8);
            Grd1.Cols[18].Move(10);
            Grd1.Cols[22].Move(6);
            Grd1.Cols[23].Move(6);
            if (mIsInvTrnShowItSpec)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, true);
                Grd1.Cols[19].Move(7);
            }
            if (!mIsRptStockSummary2ShowProjectInfo) Sm.GrdColInvisible(Grd1, new int[] { 20, 21 });
            if (mIsStockSummary2HeatNumberEnabled)
                Grd1.Cols[24].Move(16);
            else
                Grd1.Cols[24].Visible = false;
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 8, 18 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14 }, true);
        }

        private void SetCols()
        {
            if (mNumberOfInventoryUomCode == 1)
                mCols = new string[] { 
                    "WhsName", 
                    "Lot", "Bin", "ItCode", "ItName", "ForeignName", 
                    "PropName", "BatchNo", "Source", "Qty", "InventoryUomCode", 
                    "ItGrpCode", "ItGrpName", "ItScName", "Specification", "ProjectName", 
                    "PONo", "ItCodeInternal", "ItCtName", "HeatNumber"
                };

            if (mNumberOfInventoryUomCode == 2)
                mCols = new string[] { 
                    "WhsName", 
                    "Lot", "Bin", "ItCode", "ItName", "ForeignName", 
                    "PropName", "BatchNo", "Source", "Qty", "InventoryUomCode", 
                    "Qty2", "InventoryUOMCode2", "ItGrpCode", "ItGrpName", "ItScName", 
                    "Specification", "ProjectName", "PONo", "ItCodeInternal", "ItCtName", 
                    "HeatNumber"
                };

            if (mNumberOfInventoryUomCode == 3)
                mCols = new string[] { 
                    "WhsName", 
                    "Lot", "Bin", "ItCode", "ItName", "ForeignName", 
                    "PropName", "BatchNo", "Source", "Qty", "InventoryUomCode", 
                    "Qty2", "InventoryUOMCode2", "Qty3", "InventoryUOMCode3", "ItGrpCode", 
                    "ItGrpName", "ItScName", "Specification", "ProjectName", "PONo", 
                    "ItCodeInternal", "ItCtName", "HeatNumber"
                };

        }

        override protected void ShowData()
        {
            try
            {
                if (ChkWhsName.Checked && ChkWhsCode.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "Please just use Multi Warehouse or Warehouse filter, instead of using both of them.");
                    CcbWhsName.Focus();
                    return;
                }

                if (Sm.IsDteEmpty(DteDocDt, "Date ")) return;
                
                Cursor.Current = Cursors.WaitCursor;

                string 
                    Filter = " And 1=1 ", 
                    DocDt = (ChkDocDt.Checked)?Sm.GetDte(DteDocDt).Substring(0, 8):"";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                if (ChkWhsName.Checked)
                    FilterStr(ref Filter, ref cm, ProcessCcb(Sm.GetCcb(CcbWhsName)), "A.WhsCode", "_2");
                else
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCatCode), "B.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, "A.ItCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItName.Text, new string[] { "B.ItName", "B.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "A.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);

                IsReCompute = false;
                
                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                ((ChkDocDt.Checked && decimal.Parse(DocDt) < decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8))) ?
                SetSQL2(DocDt, ChkExcludeSource.Checked, Filter) : SetSQL(ChkExcludeSource.Checked, Filter)) +
                Filter + " Order By C.WhsName, A.ItCode, A.BatchNo;",
                mCols,
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdVal("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdVal("S", Grd, dr, c, Row, 10, 10);
                    if (mNumberOfInventoryUomCode==1)
                    {
                        Grd.Cells[Row, 11].Value = 0m;
                        Grd.Cells[Row, 13].Value = 0m;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 19);
                    }
                    if (mNumberOfInventoryUomCode==2)
                    {
                        Grd.Cells[Row, 13].Value = 0m;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    }
                    if (mNumberOfInventoryUomCode==3)
                    {
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                    }
                }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 11, 13 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                IsReCompute = true;
                if (Grd1.Rows.Count > 1)
                    Sm.FocusGrd(Grd1, 1, ChkHideInfoInGrd.Checked ? 5 : 2);
                else
                    Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private void SetCcbWhsName(ref CheckedComboBoxEdit Ccb, string WhsName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.WhsName As Col From TblWarehouse T ");
            SQL.AppendLine("Where 1=1 ");
            if (WhsName.Length > 0) 
                SQL.AppendLine("And T.WhsName = @WhsName ");
            else
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=T.WhsCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T.WhsName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsName", WhsName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetCcb(ref Ccb, cm);
        }

        private string ProcessCcb(string Value)
        {
            if (Value.Length != 0)
            {
                Value = GetWhsCode(Value);
                Value = "#" + Value.Replace(", ", "# #") + "#";
                Value = Value.Replace("#", @"""");
            }
            return Value;
        }

        private string GetWhsCode(string Value)
        {
            if (Value.Length != 0)
            {
                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(T.WhsCode Separator ', ') WhsCode ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select WhsCode From TblWarehouse ");
                SQL.AppendLine("    Where Find_In_Set(WhsName, @Param) ");
                SQL.AppendLine(")T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
            }

            return Value;
        }

        private void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, string Param)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 + Param + Index.ToString();
                        //Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), "%" + s + "%");
                        Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), s);
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

       
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void ChkItCatCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item's Code");
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item's Name");
        }

        private void LueItCatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCatCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsInventoryRptFilterByGrpWhs)
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            else
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit2(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit2(this, sender, "Date");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void CcbWhsName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkWhsName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Warehouse");
        }

        #endregion

        #endregion
    }
}
