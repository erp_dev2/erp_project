﻿#region Update
/*
   
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDepreciationAsset2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty
            ;
        private bool mIsAutoJournalActived = false;

        #endregion

        #region Constructor

        public FrmRptDepreciationAsset2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, "01");
                Sl.SetLueMth(LueMth2);
                Sm.SetLue(LueMth2, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "1950");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                SetSQL();

               

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.AssetCode, C.AssetName, C.DisplayName, B.Yr As ThnPerolehan, B.Mth, D.CCName As Location, ");
            SQL.AppendLine("    A.Assetvalue, E.OptDesc As DepCode, C.EcoLife, F.Residuvalue, C.PercentageAnnualDepreciation, ");
            SQL.AppendLine("    B.DepreciationValue As DepValue, ((@Yr+1) - IfNull(Left(A.DepreciationDt, 4), 0000)) As Age, ");
            SQL.AppendLine("    (B.DepreciationValue*((@Yr+1)-IfNull(Left(A.DepreciationDt, 4), 0))) As DepYear, ");
            SQL.AppendLine("    if(A.depreciationCode='1',(A.AssetValue -  (B.DepreciationValue*((@Yr+1)-IfNull(Left(A.DepreciationDt, 4), 0)))),  ");
            SQL.AppendLine("    (A.AssetValue -  H.Depvalue))  As Balanced, G.AcDesc   ");
            SQL.AppendLine("    from TblDepreciationAssetHdr A    ");
            SQL.AppendLine("    Inner Join TblDepreciationAssetDtl B On A.DocNo = B.DocNo   ");
            SQL.AppendLine("    Inner Join TblAsset C On A.AssetCode = C.AssetCode  ");
            SQL.AppendLine("    Left Join TblCostCenter D On B.CcCode = D.CCCode   ");
            SQL.AppendLine("    Left Join TblOption E On C.DepreciationCode = E.OptCode And E.OptCat = 'DepreciationMethod'  ");
            SQL.AppendLine("    Left Join    ");
            SQL.AppendLine("    (   ");
            SQL.AppendLine("      Select A.AssetCode, B.DocNo, B.Dno, B.DepreciationValue As ResiduValue   ");
            SQL.AppendLine("      from TblDepreciationAssetHdr A   ");
            SQL.AppendLine("      Inner Join TbldepreciationAssetDtl B On A.DocNo = B.DocNo   ");
            SQL.AppendLine("      Inner Join (   ");
            SQL.AppendLine("             SELECT MAX(A.Dno) AS dno, A.DocNo    ");
            SQL.AppendLine("             FROM tbldepreciationAssetDtl  A   ");
            SQL.AppendLine("             Inner JOin TblDepreciationAssetHdr B On A.DocNo = B.DocNo   ");
            SQL.AppendLine("             Where B.cancelInd= 'N'    ");
            SQL.AppendLine("             GROUP BY A.Docno    ");
            SQL.AppendLine("             )C On B.Dno = C.Dno and B.DocNo = C.DocNo   ");
            SQL.AppendLine("      Where A.CancelInd = 'N'   ");
            SQL.AppendLine("    )F On A.AssetCode=F.AssetCode  And A.DocNo = B.DocNo   ");
            SQL.AppendLine("    Left Join TblCOA G On C.AcNo2=G.AcNo  ");
            SQL.AppendLine("    Left Join (   ");
            SQL.AppendLine("                Select A.Docno, A.AssetCode, SUm(DepreciationValue) As DepValue  from tblDepreciationAssethdr A   ");
            SQL.AppendLine("                Inner Join TblDepreciationAssetDtl B On A.DocNo = B.DocNo   ");
            SQL.AppendLine("                Where concat(B.yr, B.mth) between  Concat(@yr,@mth1) And  Concat(@yr,@mth2) And A.CancelInd = 'N'   ");
            SQL.AppendLine("                Group By A.DocNo, A.AssetCode    ");
            SQL.AppendLine("    )H On A.AssetCode = H.AssetCode And A.DocNo = H.DocNo   ");
            SQL.AppendLine("    Where A.cancelInd = 'N'  ");
            SQL.AppendLine(") Tbl Where Tbl.ThnPerolehan = @Yr And Tbl.Mth Between @Mth1 And @Mth2 ");
           

            mSQL = SQL.ToString();
        }
        
        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Asset"+Environment.NewLine+"Code", 
                    "Asset Name",
                    "Display"+Environment.NewLine+"Name",
                    "Year",
                    "Month",

                    //6-10
                    "Cost Center",
                    "Asset"+Environment.NewLine+"Value",
                    "Depreciation"+Environment.NewLine+"Method",
                    "Economic Life"+Environment.NewLine+"(Month)",
                    "Residual"+Environment.NewLine+"value",

                    //11-15
                    "Percentage"+Environment.NewLine+"Depreciation (%)",
                    "Depreciation"+Environment.NewLine+"Value",
                    "Age"+Environment.NewLine+"(Year)",
                    "Total Depreciation "+Environment.NewLine+"Value",
                    "Balance",
                    //16
                    "Description"
                    
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 200, 150, 40, 60, 

                    //6-10
                    180,  130, 120, 120, 100, 

                    //11-15
                    120, 150, 80, 150, 150, 
                    //16
                    250
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 10, 11, 12, 14, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 14 }, !mIsAutoJournalActived);
            Grd1.Cols[16].Move(mIsAutoJournalActived?1:4);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Mth1 = Sm.GetLue(LueMth);
            string Mth2 = Sm.GetLue(LueMth2);

            if (Year.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is empty.");
                return;
            }

            if (Mth1.Length == 0 || Mth2.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Start Month / End Month is empty.");
                return;
            }

            if (Mth2.Length > 0 && Mth1.Length > 0)
            {
                if (Decimal.Parse(Mth1) > Decimal.Parse(Mth2))
                {
                    Sm.StdMsg(mMsgType.Warning, "Filter Month Not valid.");
                    return;
                }
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "Tbl.AssetCode", "Tbl.AssetName", "Tbl.DisplayName" });
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth1", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Mth2", Sm.GetLue(LueMth2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By Tbl.AssetName, Tbl.Mth;",
                        new string[]
                        { 
                            //0
                            "AssetCode",

                            //1-5
                            "AssetName", "DisplayName", "ThnPerolehan", "Mth", "Location",  

                            //6-10
                            "AssetValue", "DepCode", "EcoLife", "ResiduValue", "PercentageAnnualDepreciation",   

                            //11-15
                            "DepValue", "Age", "DepYear", "Balanced", "AcDesc"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        }, true, false, false, false
                    );
                if (mIsAutoJournalActived)
                {
                    Grd1.GroupObject.Add(16);
                    Grd1.Group();
                    Sm.SetGrdAlwaysShowSubTotal(Grd1);
                }
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                {  
                    12, 14, 15
                });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion       

        #region Event

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        #endregion
    }
}
