﻿#region Update
/*
    20/11/2020 [WED/IMS] new apps
    20/01/2021 [DITA/IMS] tambah kolom specification berdasarkan param : IsBOMShowSpecifications
    04/02/2021 [DITA/IMS] menampilkan item dismantle berdasrakan no di socontractdtl4 dan socontractdtl, karena no di boq pasti berbeda dengan di socontract
    24/02/2021 [DITA/IMS] tambah sequence no boq dan data diurutkan berdasarkan seq no saat chkprogessind = Y
    25/02/2021 [WED/IMS] tambah informasi dan filter Transfer Request# kalau service di tick dan SO Contract diisi
    03/03/2021 [WED/IMS] Transfer Request nya di passing ke layar depan
    05/03/2021 [DITA/IMS] bug data item dengan sequence no terduplicate
    18/03/2021 [DITA/IMS] tambah informasi dan filter Transfer Request# ketika service tidak di tick dan socontract kosong
    09/06/2021 [TRI/IMS] Tambah Filter di function additionalitem
    16/06/2021 [WED/IMS] ketika item utama tidak ada datanya, maka item2 additional belum muncul
    02/07/2021 [VIN/IMS] Order By NO SOC
    28/09/2021 [DITA/IMS] Bug saat refresh dengan filter diisi
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt8Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt8 mFrmParent;
        string mSQL = string.Empty, mWhsCode= string.Empty, mCtCode=string.Empty, mCtCtCode = string.Empty;
        private bool mFlag = false;
        internal bool
            mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmDOCt8Dlg(FrmDOCt8 FrmParent, string WhsCode, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mCtCode = CtCode;

            if (mFrmParent.mCustomerCategoryCodeForWideDOCt.Length > 0)
            {
                mCtCtCode = Sm.GetValue("Select CtCtCode From TblCustomer Where CtCode = @Param Limit 1;", CtCode);

                string[] p = mFrmParent.mCustomerCategoryCodeForWideDOCt.Split(',');
                foreach (string s in p)
                {
                    if (mCtCtCode == s)
                    {
                        mFlag = true;
                        break;
                    }
                }
            }
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode);
                LblTransferRequestProjectDocNo.Visible = 
                    TxtTransferRequestProjectDocNo.Visible = 
                    ChkTransferRequestProjectDocNo.Visible =
                    ((!mFrmParent.ChkProgressInd.Checked && mFrmParent.TxtSOContractDocNo.Text.Length == 0) || (mFrmParent.mIsDOCtUseSOContract && mFrmParent.TxtSOContractDocNo.Text.Length > 0 && mFrmParent.ChkServiceInd.Checked));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mFrmParent.mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            if (mFrmParent.ChkProgressInd.Checked)
            {
                SQL.AppendLine("Select A.No, A.DNo SOContractDNo, A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName, Null as PropCode, Null As PropName, I.ProjectCode As BatchNo, Null As Source, C.ItCtName, Null As Lot, Null As Bin, ");
                SQL.AppendLine("(A.Qty - IfNull(J.DOQty, 0.00)) As Qty, B.InventoryUomCode, (A.Qty - IfNull(J.DOQty, 0.00)) As Qty2, B.InventoryUomCode2, (A.Qty - IfNull(J.DOQty, 0.00)) As Qty3, B.InventoryUomCode3, B.ItGrpCode, D.CtitCode, B.Specification, ");
                SQL.AppendLine("A.UPriceAfTax As StockPrice, ");
                if (mFrmParent.mIsCustomerShpAddressDisplayWide && mFrmParent.mCtSADNo.Length > 0)
                    SQL.AppendLine("IfNull(E.Wide, 0.00) As Wide ");
                else
                    SQL.AppendLine("0.00 As Wide ");
                SQL.AppendLine(", K.ItCodeDismantle, K.ItNameDismantle, K.LocalCodeDismantle, K.SpecificationDismantle, K.SeqNo BOQSeqNo, Null As TransferRequestProjectDocNo, Null As TransferRequestProjectDNo ");
                SQL.AppendLine("From TblSOContractDtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode And A.DocNo = @SOContractDocNo ");
                SQL.AppendLine("    And Not Find_In_Set(A.DNo, @SelectedDNo) ");
                SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode = C.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("        Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                }
                SQL.AppendLine("Left Join TblCustomerItem D On A.ItCode = D.ItCode And D.CtCode = @CtCode ");
                if (mFrmParent.mIsCustomerShpAddressDisplayWide && mFrmParent.mCtSADNo.Length > 0)
                {
                    SQL.AppendLine("Left Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select Wide ");
                    SQL.AppendLine("    From TblCustomerShipAddress ");
                    SQL.AppendLine("    Where CtCode = @CtCode And DNo = @CtSADNo ");
                    SQL.AppendLine(") E On 0 = 0 ");
                }
                SQL.AppendLine("Inner join TblSOContractHdr F On A.DocNo = F.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr G On F.BOQDocNo = G.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr H On G.LOPDocNo = H.DocNo ");
                SQL.AppendLine("Left Join TblProjectGroup I On H.PGCode = I.PGCode ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.SOContractDocNo, T2.SOContractDNo, Sum(T2.Qty) DOQty ");
                SQL.AppendLine("    From TblDOCtHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DocNo = T2.DocNo And T2.CancelInd = 'N' ");
                SQL.AppendLine("        And T1.SOContractDocNo Is Not Null And T1.SOContractDocNo = @SOContractDocNo ");
                SQL.AppendLine("        And Not Find_IN_set(T2.SOContractDNo, @SelectedDNo) ");
                SQL.AppendLine("    Group By T1.SOContractDocNo, T2.SOContractDNo ");
                SQL.AppendLine(") J On A.DocNo = J.SOContractDocNo And A.DNo = J.SOContractDNo ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct X1.DocNo, X1.DNo, X2.ItCodeDismantle, X4.ItName As ItNameDismantle, X4.ItCodeInternal As LocalCodeDismantle, X4.Specification As SpecificationDismantle, X2.SeqNo ");
                SQL.AppendLine("    From TblSOContractDtl X1 ");
                SQL.AppendLine("    Inner Join TblBOQDtl2 X2 On X1.ItCode = X2.ItCode ");
                SQL.AppendLine("    Inner Join TblSOContractDtl4 X3 On X1.ItCode = X3.ItCode And X1.No = X3.No And X2.SeqNo = X3.SeqNo And X1.DNo = X3.DNo ");
                SQL.AppendLine("        And X1.DocNo = @SOContractDocNo ");
                SQL.AppendLine("        And X2.DocNo In (Select BOQDocNo From TblSOContractHdr Where DocNo = @SOContractDocNo) ");
                SQL.AppendLine("    Inner Join TblItem X4 On X2.ItCodeDismantle = X4.ItCode ");
                SQL.AppendLine(") K On A.DocNo = K.DocNo And A.DNo = K.DNo ");
                SQL.AppendLine("Where (A.Qty - IfNull(J.DOQty, 0.00)) > 0.00 ");
            }
            else
            {
                SQL.AppendLine("Select Null As No, Null As SOContractDNo, A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName, A.PropCode, C.PropName, A.BatchNo, A.Source, D.ItCtName, A.Lot, A.Bin, ");
                SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, B.ItGrpCode, E.CtitCode, B.Specification, ");
                if (mFrmParent.mIsDOCtShowStockPrice)
                    SQL.AppendLine("G.UPrice*G.ExcRate As StockPrice, ");
                else
                    SQL.AppendLine("0.00 As StockPrice, ");
                if (mFrmParent.mIsCustomerShpAddressDisplayWide && mFrmParent.mCtSADNo.Length > 0)
                    SQL.AppendLine("IfNull(F.Wide, 0.00) As Wide ");
                else
                    SQL.AppendLine("0.00 As Wide ");
                SQL.AppendLine(", Null As ItCodeDismantle, Null As ItNameDismantle, Null As LocalCodeDismantle, Null As SpecificationDismantle, Null As BOQSeqNo, ");
                SQL.AppendLine("H.TransferRequestProjectDocNo, H.TransferRequestProjectDNo ");
                SQL.AppendLine("From TblStockSummary A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
                SQL.AppendLine("Inner Join TblItemCategory D On B.ItCtCode=D.ItCtCode ");
                SQL.AppendLine("left Join ( ");
                SQL.AppendLine("    Select CtCode, itCode, CtitCode From tblCustomeritem Where CtCode =@CtCode ");
                SQL.AppendLine("    ) E On B.ItCode = E.itCode ");
                if (mFrmParent.mIsCustomerShpAddressDisplayWide && mFrmParent.mCtSADNo.Length > 0)
                {
                    SQL.AppendLine("Left Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select Wide ");
                    SQL.AppendLine("    From TblCustomerShipAddress ");
                    SQL.AppendLine("    Where CtCode = @CtCode And DNo = @CtSADNo ");
                    SQL.AppendLine(") F On 0 = 0 ");
                }
                if (mFrmParent.mIsDOCtShowStockPrice)
                    SQL.AppendLine("Left Join TblStockPrice G On A.Source=G.Source ");

                // nampilin Transfer Request#
                // minta tampil jumlah baris nya berdasarkan dokumennya (bakalan double-double, sudah konfirm anel)
               
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct A.DocNo As TransferRequestProjectDocNo, B.DNo As TransferRequestProjectDNo, B.ItCode, A.WhsCode ");
                SQL.AppendLine("    From TblTransferRequestProjectHdr A ");
                SQL.AppendLine("    Inner Join TblTransferRequestProjectDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        And B.WhsCode Is Null ");
                if (mFrmParent.mIsDOCtUseSOContract && mFrmParent.TxtSOContractDocNo.Text.Length > 0 && mFrmParent.ChkServiceInd.Checked)
                    SQL.AppendLine("        And A.SOContractDocNo = @SOContractDocNo ");
                SQL.AppendLine("        And A.WhsCode = @WhsCode ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.`Status` = 'A' ");
                SQL.AppendLine(") H On A.WhsCode = H.WhsCode And A.ItCode = H.ItCode ");
             

                SQL.AppendLine("Where A.WhsCode=@WhsCode  ");
                //SQL.AppendLine("And (A.Qty>0 Or A.Qty2>0 Or A.Qty3>0) ");
                SQL.AppendLine("And Locate(Concat('##', A.ItCode, A.PropCode, A.BatchNo, A.Source, A.Lot, A.Bin, '##'), @SelectedItem)<1 ");

                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }

                if (mFrmParent.mIsDOCtUseSOContract && mFrmParent.TxtSOContractDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("And A.BatchNo = @ProjectCode ");
                }
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 32;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Item's Code", 
                    "", 
                    "Local Code", 
                    "Item's Name", 

                    //6-10
                    "Item's Category",
                    "Property Code",
                    "Property",
                    "Batch#",
                    "Source",
                    
                    //11-15
                    "Lot",
                    "Bin", 
                    "Stock",
                    "UoM",
                    "Stock",

                    //16-20
                    "UoM",
                    "Stock",
                    "UoM",
                    "Group",
                    "Customer's"+Environment.NewLine+"Reference",

                    //21-25
                    "Foreign Name",
                    "Specification",
                    "Wide",
                    "Stock's Price",
                    "SOCOntractDNo",

                    //26-30
                    "No",
                    "Item Dismantle's"+Environment.NewLine+"Code",
                    "Item Dismantle's"+Environment.NewLine+"Name",
                    "BOQ Sequence No",
                    "Transfer Request#",

                    //31
                    "Transfer Request D#"
                },
                 new int[] 
                {
                    //0
                    50,
                    //1-5
                    20, 80, 20, 60, 250, 
                    //6-10
                    150, 0, 100, 200, 180, 
                    //11-15
                    60, 60, 80, 80, 80, 
                    //16-20
                    80, 80, 80, 100, 100,
                    //21-25
                    230, 300, 0, 130, 0,
                    //26-30
                    100, 120, 200, 100, 150,
                    //31
                    0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17, 23, 24 }, 0);
            if (!mFrmParent.mIsCustomerShpAddressDisplayWide) Sm.GrdColInvisible(Grd1, new int[] { 23 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 });

            Grd1.Cols[22].Move(6);
            if ((!mFrmParent.ChkProgressInd.Checked && mFrmParent.TxtSOContractDocNo.Text.Length == 0) || (mFrmParent.mIsDOCtUseSOContract && mFrmParent.TxtSOContractDocNo.Text.Length > 0 && mFrmParent.ChkServiceInd.Checked))
                Grd1.Cols[30].Move(6);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 30 });

            if (mFrmParent.mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 7, 10, 15, 16, 17, 18, 19, 29 }, false);
                Grd1.Cols[21].Move(6);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 7, 10, 15, 16, 17, 18, 19, 21, 29 }, false);
            Grd1.Cols[20].Move(10);
            if (mFrmParent.mISDOCtShowCustomerItem == "N")
            {
               Sm.GrdColInvisible(Grd1, new int[] { 20 }, false);
            }
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[19].Visible = true;
                if (mFrmParent.mIsBOMShowSpecifications) Grd1.Cols[19].Move(7);
                else Grd1.Cols[19].Move(5);
            }
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 22 });
            
            Grd1.Cols[24].Visible = mFrmParent.mIsDOCtShowStockPrice;
            if (mFrmParent.mIsDOCtShowStockPrice) Grd1.Cols[24].Move(19);
            if (!mFrmParent.ChkProgressInd.Checked) Sm.GrdColInvisible(Grd1, new int[] { 26 });
            else
            {
                Grd1.Cols[26].Move(2);
                Grd1.Cols[29].Move(3);

            } 

            if (!mFrmParent.ChkProgressInd.Checked)
                Sm.GrdColInvisible(Grd1, new int[] { 27, 28 });

            Grd1.Cols[28].Move(9);
            Grd1.Cols[27].Move(9);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColInvisible(Grd1, new int[] { 9 }, !mFrmParent.mIsDOCtHideBatchNo);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 10 }, !ChkHideInfoInGrd.Checked);
            if (mFrmParent.ChkProgressInd.Checked) Sm.GrdColInvisible(Grd1, new int[] { 29}, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 1=1 ";
                var cm = new MySqlCommand();

                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());
                string aa = mFrmParent.GetSelectedItem();
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@CtSADNo", mFrmParent.mCtSADNo);
                if (mFrmParent.mIsDOCtUseSOContract)
                {
                    Sm.CmParam<String>(ref cm, "@SelectedDNo", mFrmParent.GetSelectedDNo());
                    Sm.CmParam<String>(ref cm, "@SOContractDocNo", mFrmParent.TxtSOContractDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@ProjectCode", mFrmParent.TxtProjectCode.Text);
                }

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItCodeInternal", "B.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtPropCode.Text, new string[] { "A.PropCode", "C.PropName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "A.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);
                Sm.FilterStr(ref Filter, ref cm, TxtTransferRequestProjectDocNo.Text, "TransferRequestProjectDocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter +
                    " Order By CAST(No AS DECIMAL(10,0)), Right(concat('0000000000', BOQSeqNo), 10), B.ItName; ",
                    new string[] 
                    { 
                        //0
                        "ItCode",

                        //1-5
                        "ItCodeInternal", "ItName", "ItCtName", "PropCode", "PropName", 
                        
                        //6-10
                        "BatchNo", "Source", "Lot", "Bin", "Qty", 
                        
                        //11-15
                        "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3",

                        //16-20
                        "ItGrpCode", "CtItCode", "ForeignName", "Specification", "Wide",

                        //21-25
                        "StockPrice", "SOContractDNo", "No", "ItCodeDismantle", "ItNameDismantle",

                        //26-28
                        "BOQSeqNo", "TransferRequestProjectDocNo", "TransferRequestProjectDNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 28);
                    }, false, false, false, false
                );

                AdditionalItem();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 18);
                        if(mFrmParent.ChkProgressInd.Checked)
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 25);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 35, Grd1, Row2, 26);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 27);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 37, Grd1, Row2, 28);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 41, Grd1, Row2, 30);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 42, Grd1, Row2, 31);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 15, 18, 21, 24 });
                        if (mFrmParent.mIsCustomerShpAddressDisplayWide)
                        {
                            if (mFlag)
                            {
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 23);
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 23);
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 23);
                            }
                        }
                        mFrmParent.ComputeTotalQty();
                        mFrmParent.GetCtQtUPrice(Row1);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27, 33 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            if (mFrmParent.ChkProgressInd.Checked)
            {
                string Key = Sm.GetGrdStr(Grd1, Row, 25);
                for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; ++Index)
                {
                    if (Sm.CompareStr(Key, Sm.GetGrdStr(mFrmParent.Grd1, Index, 34)))
                    {
                        return true;
                    }
                }
            }
            else
            {
                string Key =
                    Sm.GetGrdStr(Grd1, Row, 2) +
                    Sm.GetGrdStr(Grd1, Row, 7) +
                    Sm.GetGrdStr(Grd1, Row, 9) +
                    Sm.GetGrdStr(Grd1, Row, 10) +
                    Sm.GetGrdStr(Grd1, Row, 11) +
                    Sm.GetGrdStr(Grd1, Row, 12);
                for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                {
                    if (
                        Sm.CompareStr(Key,
                            Sm.GetGrdStr(mFrmParent.Grd1, Index, 4) +
                            Sm.GetGrdStr(mFrmParent.Grd1, Index, 8) +
                            Sm.GetGrdStr(mFrmParent.Grd1, Index, 10) +
                            Sm.GetGrdStr(mFrmParent.Grd1, Index, 11) +
                            Sm.GetGrdStr(mFrmParent.Grd1, Index, 12) +
                            Sm.GetGrdStr(mFrmParent.Grd1, Index, 13)
                        ))
                        return true;
                }
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                if (e.ColIndex == 1)
                {
                    bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                    for (int r= 0; r < Grd1.Rows.Count; r++)
                        Grd1.Cells[r, 1].Value = !IsSelected;
                }

                if (Sm.IsGrdColSelected(new int[] { 13, 15, 17 }, e.ColIndex))
                {
                    decimal Total = 0m;
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        if (Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, e.ColIndex).Length != 0) 
                            Total += Sm.GetGrdDec(Grd1, r, e.ColIndex);
                    Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
                }
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void AdditionalItem()
        {
            if (!mFrmParent.ChkProgressInd.Checked && 
                mFrmParent.TxtSOContractDocNo.Text.Length == 0
                //&& Grd1.Rows.Count > 0
                )
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                string Filter = " And 1=1 ";

                SQL.AppendLine("Select T.* From ( ");

                SQL.AppendLine("Select A.ItCode, A.ItCtCode, A.ItCodeInternal, A.ItName, B.ItCtName, Null As PropCode, Null As PropName, ");
                SQL.AppendLine("Null As BatchNo, Null As Source, Null As Lot, Null As Bin, 0.00 As Qty, Null As TransferRequestProjectDocNo, Null As TransferRequestProjectDNo, ");
                SQL.AppendLine("A.InventoryUomCode, 0.00 As Qty2, A.InventoryUomCode2, 0.00 As Qty3, A.InventoryUomCode3, ");
                SQL.AppendLine("A.ItGrpCode, C.CtItCode, A.ForeignName, A.Specification, 0.00 As Wide, ");
                SQL.AppendLine("0.00 As StockPrice, Null As SOContractDNo, Null As No ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
                SQL.AppendLine("left Join ( ");
                SQL.AppendLine("    Select CtCode, itCode, CtitCode From tblCustomeritem Where CtCode = @CtCode ");
                SQL.AppendLine("    ) C On A.ItCode = C.itCode ");
                if (mFrmParent.mIsCustomerShpAddressDisplayWide && mFrmParent.mCtSADNo.Length > 0)
                {
                    SQL.AppendLine("Left Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select Wide ");
                    SQL.AppendLine("    From TblCustomerShipAddress ");
                    SQL.AppendLine("    Where CtCode = @CtCode And DNo = @CtSADNo ");
                    SQL.AppendLine(") D On 0 = 0 ");
                }

                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct A.DocNo As TransferRequestProjectDocNo, B.DNo As TransferRequestProjectDNo, B.ItCode, A.WhsCode ");
                SQL.AppendLine("    From TblTransferRequestProjectHdr A ");
                SQL.AppendLine("    Inner Join TblTransferRequestProjectDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        And B.WhsCode Is Null ");
                if (mFrmParent.mIsDOCtUseSOContract && mFrmParent.TxtSOContractDocNo.Text.Length > 0 && mFrmParent.ChkServiceInd.Checked)
                    SQL.AppendLine("        And A.SOContractDocNo = @SOContractDocNo ");
                SQL.AppendLine("        And A.WhsCode = @WhsCode ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.`Status` = 'A' ");
                SQL.AppendLine(") H On A.ItCode = H.ItCode ");

                SQL.AppendLine("Where A.ActInd = 'Y' ");
                SQL.AppendLine("And A.ItCode Not In (Select Distinct ItCode From TblStockSummary Where WhsCode = @WhsCode) ");
                SQL.AppendLine("And Locate(Concat('##', A.ItCode, '##'), @SelectedItem)<1 ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }

                SQL.AppendLine(") T Where 0 = 0 ");

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItCodeInternal", "T.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "T.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtPropCode.Text, new string[] { "T.PropCode", "T.PropName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "T.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "T.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "T.Bin", false);
                Sm.FilterStr(ref Filter, ref cm, TxtTransferRequestProjectDocNo.Text, "T.TransferRequestProjectDocNo", false);

                //SQL.AppendLine(Filter);


                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
  
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString() + Filter + " Order By T.ItName;";
                    if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());
                    Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                    Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                    Sm.CmParam<String>(ref cm, "@CtSADNo", mFrmParent.mCtSADNo);

                    var dr = cm.ExecuteReader();
                    
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "ItCode",

                        //1-5
                        "ItCodeInternal", "ItName", "ItCtName", "PropCode", "PropName", 
                        
                        //6-10
                        "BatchNo", "Source", "Lot", "Bin", "Qty", 
                        
                        //11-15
                        "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3",

                        //16-20
                        "ItGrpCode", "CtItCode", "ForeignName", "Specification", "Wide",

                        //21-23
                        "StockPrice", "SOContractDNo", "No"
                    });
                    if (dr.HasRows)
                    {
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();

                            Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Grd1.Rows.Count;
                            Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = false;
                            Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = Sm.DrStr(dr, c[0]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.DrStr(dr, c[1]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = Sm.DrStr(dr, c[2]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = Sm.DrStr(dr, c[3]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 7].Value = Sm.DrStr(dr, c[4]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 8].Value = Sm.DrStr(dr, c[5]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 9].Value = Sm.DrStr(dr, c[6]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 10].Value = Sm.DrStr(dr, c[7]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 11].Value = Sm.DrStr(dr, c[8]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 12].Value = Sm.DrStr(dr, c[9]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 13].Value = Sm.DrDec(dr, c[10]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 14].Value = Sm.DrStr(dr, c[11]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 15].Value = Sm.DrDec(dr, c[12]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 16].Value = Sm.DrStr(dr, c[13]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 17].Value = Sm.DrDec(dr, c[14]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 18].Value = Sm.DrStr(dr, c[15]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 19].Value = Sm.DrStr(dr, c[16]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 20].Value = Sm.DrStr(dr, c[17]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 21].Value = Sm.DrStr(dr, c[18]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 22].Value = Sm.DrStr(dr, c[19]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 23].Value = Sm.DrDec(dr, c[20]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 24].Value = Sm.DrDec(dr, c[21]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 25].Value = Sm.DrStr(dr, c[22]);
                            Grd1.Cells[Grd1.Rows.Count - 1, 26].Value = Sm.DrStr(dr, c[23]);
                        }
                        Grd1.EndUpdate();
                    }
                    dr.Close();
                }                
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }
      

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtPropCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPropCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void TxtTransferRequestProjectDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTransferRequestProjectDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Transfer Request#");
        }

        #endregion

        #endregion        
    }
}
