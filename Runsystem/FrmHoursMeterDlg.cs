﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using MySql.Data.MySqlClient;
using System.ComponentModel;
using System.Text;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmHoursMeterDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmHoursMeter mFrmParent;
        private string mSQL =string.Empty,
             mTOCode = string.Empty,
             mDocDt = string.Empty;

        #endregion

        #region Constructor

        public FrmHoursMeterDlg(FrmHoursMeter FrmParent, string TOCode, string DocDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mTOCode = TOCode;
            mDocDt = DocDt;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetFormControl(mState.View);
            TxtToCode.EditValue = mTOCode;
            TxtToName.EditValue = Sm.GetValue("Select AssetName From TblAsset Where AssetCode='"+mTOCode+"'") ;
            TxtToDisplay.EditValue = Sm.GetValue("Select DisplayName From TblAsset Where AssetCode = '"+mTOCode+"'"); ;
            TxtMth.EditValue = Sm.MonthName(mDocDt.Substring(4, 2));
            SetGrd(); 
            ShowData();
        }

        private void SetFormControl(RunSystem.mState state)
        {

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtToCode, TxtToName, TxtMth, TxtToDisplay
                    }, true);
                    TxtToCode.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtToCode, TxtToName, TxtMth, TxtToDisplay
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

       private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 127;
            Grd1.FrozenArea.ColCount = 2;

            SetGrdHdr(ref Grd1, 0, 0, "TO" + Environment.NewLine + "Code", 2, 100);
            SetGrdHdr(ref Grd1, 0, 1, "Technical" + Environment.NewLine + "Object", 2, 150);
            SetGrdHdr(ref Grd1, 0, 2, "Display", 2, 200);
            SetGrdHdr2(ref Grd1, 1, 3, "01", 4, "01");
            SetGrdHdr2(ref Grd1, 1, 7, "02", 4, "02");
            SetGrdHdr2(ref Grd1, 1, 11, "03", 4, "03");
            SetGrdHdr2(ref Grd1, 1, 15, "04", 4, "04");
            SetGrdHdr2(ref Grd1, 1, 19, "05", 4, "05");
            SetGrdHdr2(ref Grd1, 1, 23, "06", 4, "06");
            SetGrdHdr2(ref Grd1, 1, 27, "07", 4, "07");
            SetGrdHdr2(ref Grd1, 1, 31, "08", 4, "08");
            SetGrdHdr2(ref Grd1, 1, 35, "09", 4, "09");
            SetGrdHdr2(ref Grd1, 1, 39, "10", 4, "10");
            SetGrdHdr2(ref Grd1, 1, 43, "11", 4, "11");
            SetGrdHdr2(ref Grd1, 1, 47, "12", 4, "12");
            SetGrdHdr2(ref Grd1, 1, 51, "13", 4, "13");
            SetGrdHdr2(ref Grd1, 1, 55, "14", 4, "14");
            SetGrdHdr2(ref Grd1, 1, 59, "15", 4, "15");
            SetGrdHdr2(ref Grd1, 1, 63, "16", 4, "16");
            SetGrdHdr2(ref Grd1, 1, 67, "17", 4, "17");
            SetGrdHdr2(ref Grd1, 1, 71, "18", 4, "18");
            SetGrdHdr2(ref Grd1, 1, 75, "19", 4, "19");
            SetGrdHdr2(ref Grd1, 1, 79, "20", 4, "20");
            SetGrdHdr2(ref Grd1, 1, 83, "21", 4, "21");
            SetGrdHdr2(ref Grd1, 1, 87, "22", 4, "22");
            SetGrdHdr2(ref Grd1, 1, 91, "23", 4, "23");
            SetGrdHdr2(ref Grd1, 1, 95, "24", 4, "24");
            SetGrdHdr2(ref Grd1, 1, 99, "25", 4, "25");
            SetGrdHdr2(ref Grd1, 1, 103, "26", 4, "26");
            SetGrdHdr2(ref Grd1, 1, 107, "27", 4, "27");
            SetGrdHdr2(ref Grd1, 1, 111, "28", 4, "28");
            SetGrdHdr2(ref Grd1, 1, 115, "29", 4, "29");
            SetGrdHdr2(ref Grd1, 1, 119, "30", 4, "30");
            SetGrdHdr2(ref Grd1, 1, 123, "31", 4, "31");
                      
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
            16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
            31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,43, 44, 45, 46, 47, 48, 49, 50,
            51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
            71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
            91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
            111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126
            });

        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols, string Mth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 120;

            SetGrdHdr(ref Grd1, 0, col, ""+Mth+" Pagi", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 1, "DocNo Pagi", 1, 100);
            SetGrdHdr(ref Grd1, 0, col + 2, ""+Mth+" Malam", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 3, "DocNo Malam", 1, 100);
            Sm.GrdFormatDec(Grd1, new int[] { col, (col + 2) }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { (col + 1), (col + 3) });
            
        }

        private void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

     

        #endregion

        #region Show data
        public void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.ToCode, B.AssetName, B.DisplayName ");
                SQL.AppendLine("From TblHoursMeter A ");
                SQL.AppendLine("Inner Join TblAsset B On A.TOCode=B.AssetCode ");
                SQL.AppendLine("Where A.ToCode = '"+mTOCode+"' ");

                mSQL = SQL.ToString();

                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString() + Filter + "limit 1",
                        new string[] 
                        { 
                            //0
                            "ToCode", 

                            //1-5
                            "AssetName", "DisplayName" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        }, false, false, true, false
                    );
                ProcessDataTO();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method
        private void ProcessDataTO()
        {
            var lEL = new List<HoursMeter>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;

            Sm.CmParam<String>(ref cm, "@DocDt", Sm.Left(mDocDt, 6));
            Sm.CmParam<String>(ref cm, "@ToCode", mTOCode);

            SQL.AppendLine("Select DocNo, DocDt, HoursMeter, if(ShiftCode='1', 'Pagi', 'Malam') As ShiftCode From TblHoursMeter ");
            SQL.AppendLine("Where Left(DocDt, 6)=@DocDt And ToCode=@ToCode ");
            SQL.AppendLine("Order By DocDt ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DocDt", "HoursMeter", "ShiftCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEL.Add(new HoursMeter()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            HM = Sm.DrDec(dr, c[2]),
                            Shift = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }

            if (lEL.Count > 0 && Grd1.Cols.Count > 3)
            {
                lEL.ForEach(h =>
                {
                     for (int i = 3; i < Grd1.Cols.Count; i++)
                      {
                          string data = string.Concat(Sm.Right(h.DocDt,2), ' ', h.Shift);
                          if (Sm.CompareStr(data, Grd1.Header.Cells[0, i].Value.ToString()))
                          {
                              Grd1.Cells[0, i].Value = h.HM;
                              Grd1.Cells[0, i + 1].Value = h.DocNo;
                          }
                      }
                });
            }
        }
        #endregion

        #endregion

        #region Class

        private class HoursMeter
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Shift { get; set; }
            public decimal HM { get; set; }
        }

        #endregion
    }

 
}
