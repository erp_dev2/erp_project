﻿#region Update
/*
    13/03/2019 [WED] new reporting
    01/04/2019 [WED] tambah checkbox LueType
    15/04/2019 [WED] dibedakan antara BC27 dan BC40
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptKB_3 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mStartDt = string.Empty,
            mEndDt = string.Empty;

        #endregion

        #region Constructor

        public FrmRptKB_3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueType(ref LueType);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Type)
        {
            var SQL = new StringBuilder();

            if (Type == "WIP")
            {
                SQL.AppendLine("Select 'BC27' As DocType, D.ItName, E.ItCtName, D.ItGrpCode, B.BatchNo, C.VdName, F.WhsName WhsRecvVd, ");
                SQL.AppendLine("B.Qty QtyRecvVd, D.InventoryUomCode UomRecvVd, G.DocNo2, G.WhsName Whs2, G.Qty Qty2, G.UoM UoM2 ");
                SQL.AppendLine("From TblRecvVdHdr A ");
                SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
	            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
	            SQL.AppendLine("    And A.POInd='Y' ");
                SQL.AppendLine("    And Substr(A.KBSubmissionNo, 5, 2) = '27' ");
	            SQL.AppendLine("    And B.Status In ('O', 'A') And B.CancelInd='N' ");
                SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
                SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode = E.ItCtCode ");
                SQL.AppendLine("Inner Join TblWarehouse F On A.WhsCode = F.WhsCode ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select Distinct(Concat(X1.DocNo, X2.DNo)), X1.DocNo, X2.DNo, X4.DocNo DocNo2, X6.WhsName, X3.Qty, X7.InventoryUomCode UoM ");
	            SQL.AppendLine("    From TblRecvVdHdr X1 ");
	            SQL.AppendLine("    Inner Join TblRecvVdDtl X2 On X1.DocNo = X2.DocNo ");
	            SQL.AppendLine("    And (X1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And X1.KBRegistrationNo Is Not Null ");
	            SQL.AppendLine("    And X1.POInd='Y' ");
                SQL.AppendLine("    And Substr(X1.KBSubmissionNo, 5, 2) = '27' ");
	            SQL.AppendLine("    And X2.Status In ('O', 'A') And X2.CancelInd='N' ");
	            SQL.AppendLine("    Inner Join TblShopFloorControl3Dtl X3 On X2.BatchNo = X3.BatchNo ");
	            SQL.AppendLine("    Inner Join TblShopFloorControlHdr X4 On X3.DocNo = X4.DocNo And X4.CancelInd = 'N' ");
	            SQL.AppendLine("    Inner Join TblWarehouse X6 On X4.WhsCode2 = X6.WhsCode ");
	            SQL.AppendLine("    Inner Join TblItem X7 On X3.ItCode = X7.ItCode ");
                SQL.AppendLine(") G On A.DocNo = G.DocNo And B.DNo = G.DNo ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select 'BC40' As DocType, D.ItName, E.ItCtName, D.ItGrpCode, B.BatchNo, C.VdName, F.WhsName WhsRecvVd, ");
                SQL.AppendLine("B.Qty QtyRecvVd, D.InventoryUomCode UomRecvVd, G.DocNo2, G.WhsName Whs2, G.Qty Qty2, G.UoM UoM2 ");
                SQL.AppendLine("From TblRecvVdHdr A ");
                SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
                SQL.AppendLine("    And A.POInd='Y' ");
                SQL.AppendLine("    And Substr(A.KBSubmissionNo, 5, 2) = '40' ");
                SQL.AppendLine("    And B.Status In ('O', 'A') And B.CancelInd='N' ");
                SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
                SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode = E.ItCtCode ");
                SQL.AppendLine("Inner Join TblWarehouse F On A.WhsCode = F.WhsCode ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct(Concat(X1.DocNo, X2.DNo)), X1.DocNo, X2.DNo, X4.DocNo DocNo2, X6.WhsName, X3.Qty, X7.InventoryUomCode UoM ");
                SQL.AppendLine("    From TblRecvVdHdr X1 ");
                SQL.AppendLine("    Inner Join TblRecvVdDtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("    And (X1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And X1.KBRegistrationNo Is Not Null ");
                SQL.AppendLine("    And X1.POInd='Y' ");
                SQL.AppendLine("    And Substr(X1.KBSubmissionNo, 5, 2) = '40' ");
                SQL.AppendLine("    And X2.Status In ('O', 'A') And X2.CancelInd='N' ");
                SQL.AppendLine("    Inner Join TblShopFloorControl3Dtl X3 On X2.BatchNo = X3.BatchNo ");
                SQL.AppendLine("    Inner Join TblShopFloorControlHdr X4 On X3.DocNo = X4.DocNo And X4.CancelInd = 'N' ");
                SQL.AppendLine("    Inner Join TblWarehouse X6 On X4.WhsCode2 = X6.WhsCode ");
                SQL.AppendLine("    Inner Join TblItem X7 On X3.ItCode = X7.ItCode ");
                SQL.AppendLine(") G On A.DocNo = G.DocNo And B.DNo = G.DNo ");
            }

            if(Type == "Moving")
            {
                SQL.AppendLine("Select 'BC27' As DocType, D.ItName, E.ItCtName, D.ItGrpCode, B.BatchNo, C.VdName, F.WhsName WhsRecvVd, ");
                SQL.AppendLine("B.Qty QtyRecvVd, D.InventoryUomCode UomRecvVd, G.DocNo2, G.WhsName Whs2, G.Qty Qty2, G.UoM UoM2 ");
                SQL.AppendLine("From TblRecvVdHdr A ");
                SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
                SQL.AppendLine("    And A.POInd='Y' ");
                SQL.AppendLine("    And Substr(A.KBSubmissionNo, 5, 2) = '27' ");
                SQL.AppendLine("    And B.Status In ('O', 'A') And B.CancelInd='N' ");
                SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
                SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode = E.ItCtCode ");
                SQL.AppendLine("Inner Join TblWarehouse F On A.WhsCode = F.WhsCode ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select X1.DocNo DocNo2, X2.BatchNo, X3.WhsName, X2.Qty, X4.InventoryUomCode As UoM ");
	            SQL.AppendLine("    From TblDOCtHdr X1 ");
	            SQL.AppendLine("    Inner Join TblDOCtDtl X2 On X1.DocNo = X2.DocNo And X1.KBRegistrationNo Is Not Null And X2.CancelInd = 'N' ");
	            SQL.AppendLine("    Inner Join TblWarehouse X3 On X1.WhsCode = X3.WhsCode ");
	            SQL.AppendLine("    Inner Join TblItem X4 On X2.ItCode = X4.ItCode ");
                	
	            SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select X1.DocNo DocNo2, X2.BatchNo, X3.WhsName, X2.Qty, X4.InventoryUomCode As UoM ");
	            SQL.AppendLine("    From TblDOCt2Hdr X1 ");
	            SQL.AppendLine("    Inner Join TblDOCt2Dtl X2 On X1.DocNo = X2.DocNo And X1.KBRegistrationNo Is Not Null And X2.CancelInd = 'N' ");
	            SQL.AppendLine("    Inner Join TblWarehouse X3 On X1.WhsCode = X3.WhsCode ");
	            SQL.AppendLine("    Inner Join TblItem X4 On X2.ItCode = X4.ItCode ");
                	
	            SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select X1.DocNo DocNo2, X2.BatchNo, X3.WhsName, X2.Qty, X4.InventoryUomCode As UoM ");
	            SQL.AppendLine("    From TblDOWhsHdr X1 ");
	            SQL.AppendLine("    Inner Join TblDOWhsDtl X2 On X1.DocNo = X2.DocNo And X1.KBRegistrationNo Is Not Null And X2.CancelInd = 'N' ");
	            SQL.AppendLine("    Inner Join TblWarehouse X3 On X1.WhsCode2 = X3.WhsCode ");
	            SQL.AppendLine("    Inner Join TblItem X4 On X2.ItCode = X4.ItCode ");
                SQL.AppendLine(") G On B.BatchNo = G.BatchNo ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select 'BC40' As DocType, D.ItName, E.ItCtName, D.ItGrpCode, B.BatchNo, C.VdName, F.WhsName WhsRecvVd, ");
                SQL.AppendLine("B.Qty QtyRecvVd, D.InventoryUomCode UomRecvVd, G.DocNo2, G.WhsName Whs2, G.Qty Qty2, G.UoM UoM2 ");
                SQL.AppendLine("From TblRecvVdHdr A ");
                SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
                SQL.AppendLine("    And A.POInd='Y' ");
                SQL.AppendLine("    And Substr(A.KBSubmissionNo, 5, 2) = '40' ");
                SQL.AppendLine("    And B.Status In ('O', 'A') And B.CancelInd='N' ");
                SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
                SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode = E.ItCtCode ");
                SQL.AppendLine("Inner Join TblWarehouse F On A.WhsCode = F.WhsCode ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select X1.DocNo DocNo2, X2.BatchNo, X3.WhsName, X2.Qty, X4.InventoryUomCode As UoM ");
                SQL.AppendLine("    From TblDOCtHdr X1 ");
                SQL.AppendLine("    Inner Join TblDOCtDtl X2 On X1.DocNo = X2.DocNo And X1.KBRegistrationNo Is Not Null And X2.CancelInd = 'N' ");
                SQL.AppendLine("    Inner Join TblWarehouse X3 On X1.WhsCode = X3.WhsCode ");
                SQL.AppendLine("    Inner Join TblItem X4 On X2.ItCode = X4.ItCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select X1.DocNo DocNo2, X2.BatchNo, X3.WhsName, X2.Qty, X4.InventoryUomCode As UoM ");
                SQL.AppendLine("    From TblDOCt2Hdr X1 ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl X2 On X1.DocNo = X2.DocNo And X1.KBRegistrationNo Is Not Null And X2.CancelInd = 'N' ");
                SQL.AppendLine("    Inner Join TblWarehouse X3 On X1.WhsCode = X3.WhsCode ");
                SQL.AppendLine("    Inner Join TblItem X4 On X2.ItCode = X4.ItCode ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select X1.DocNo DocNo2, X2.BatchNo, X3.WhsName, X2.Qty, X4.InventoryUomCode As UoM ");
                SQL.AppendLine("    From TblDOWhsHdr X1 ");
                SQL.AppendLine("    Inner Join TblDOWhsDtl X2 On X1.DocNo = X2.DocNo And X1.KBRegistrationNo Is Not Null And X2.CancelInd = 'N' ");
                SQL.AppendLine("    Inner Join TblWarehouse X3 On X1.WhsCode2 = X3.WhsCode ");
                SQL.AppendLine("    Inner Join TblItem X4 On X2.ItCode = X4.ItCode ");
                SQL.AppendLine(") G On B.BatchNo = G.BatchNo ");
            }

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 1;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;
            Grd1.Header.Cells[1, 1].Value = "(BC27 / BC40)";
            Grd1.Header.Cells[1, 1].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 1].SpanCols = 9;
            Grd1.Header.Cells[0, 1].Value = "Type";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 1;
            Grd1.Header.Cells[0, 2].Value = "Item Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 1;
            Grd1.Header.Cells[0, 3].Value = "Item Category";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 1;
            Grd1.Header.Cells[0, 4].Value = "Group";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 1;
            Grd1.Header.Cells[0, 5].Value = "Batch#";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 1;
            Grd1.Header.Cells[0, 6].Value = "Vendor";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 1;
            Grd1.Header.Cells[0, 7].Value = "Warehouse";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 1;
            Grd1.Header.Cells[0, 8].Value = "Quantity";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 1;
            Grd1.Header.Cells[0, 9].Value = "UoM";
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 9].SpanRows = 1;
            Grd1.Header.Cells[1, 10].Value = "";
            Grd1.Header.Cells[1, 10].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 10].SpanCols = 4;
            Grd1.Header.Cells[0, 10].Value = "Document#";
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 10].SpanRows = 1;
            Grd1.Header.Cells[0, 11].Value = "Warehouse";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 1;
            Grd1.Header.Cells[0, 12].Value = "Quantity";
            Grd1.Header.Cells[0, 12].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 12].SpanRows = 1;
            Grd1.Header.Cells[0, 13].Value = "UoM";
            Grd1.Header.Cells[0, 13].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 13].SpanRows = 1;

            Sm.GrdFormatDec(Grd1, new int[] { 8, 12 }, 0);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void ShowData()
        {
            mStartDt = string.Empty; mEndDt = string.Empty;
            Sm.ClearGrd(Grd1, true);
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
               Sm.IsLueEmpty(LueType, "Type")
               ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                mStartDt = Sm.GetDte(DteDocDt1).Substring(0, 8);
                mEndDt = Sm.GetDte(DteDocDt2).Substring(0, 8);

                Sm.CmParamDt(ref cm, "@DocDt1", mStartDt);
                Sm.CmParamDt(ref cm, "@DocDt2", mEndDt);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.KBContractNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtRegistrationNo.Text, "A.KBRegistrationNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(Sm.GetLue(LueType)) + Filter + " ; ",
                    new string[]
                    {
                        //0
                        "DocType", 

                        //1-5
                        "ItName", "ItCtName", "ItGrpCode", "BatchNo", "VdName", 

                        //6-10
                        "WhsRecvVd", "QtyRecvVd", "UomRecvVd", "DocNo2", "Whs2",

                        //11-12
                        "Qty2", "UoM2"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            try
            {
                if (Grd1.Rows.Count == 0 || mStartDt.Length == 0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }

                iGSubtotalManager.ForeColor = Color.Black;

                //string StartDt = string.Concat(Sm.Right(mStartDt, 2), "/", mStartDt.Substring(4, 2), "/", Sm.Left(mStartDt, 4));
                //string EndDt = string.Concat(Sm.Right(mEndDt, 2), "/", mEndDt.Substring(4, 2), "/", Sm.Left(mEndDt, 4));
                //string mTitle = string.Empty;

                //if (Sm.GetLue(LueType) == "BC23")
                //    mTitle = "LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //if (Sm.GetLue(LueType) == "BC25")
                //    mTitle = "LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //if (Sm.GetLue(LueType) == "BC40" || Sm.GetLue(LueType) == "BC27")
                //    mTitle = "LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //if (Sm.GetLue(LueType) == "BC41")
                //    mTitle = "LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //PM1.PageHeader.MiddleSection.Text = mTitle;

                //PM1.PageHeader.LeftSection.Text = Environment.NewLine + string.Concat("Periode : ", StartDt, " s/d ", EndDt); ;
                Sm.SetPrintManagerProperty(PM1, Grd1, ChkAutoWidth.Checked);
                iGSubtotalManager.ForeColor = Color.White;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private string MonthNameIDN(string Mth)
        {
            string[] mMonthNames = { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Novemeber", "Desember" };
            return mMonthNames[Int32.Parse(Mth) - 1];
        }

        private void SetLueType(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'WIP' As Col1, 'WIP' As Col2 ");
            SQL.AppendLine("Union All Select 'Moving' Col1, 'Moving Stock' Col2; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Contract#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
            Grd1.Header.Cells[1, 10].Value = "";
            if (Sm.GetLue(LueType).Length > 0)
            {
                if (Sm.GetLue(LueType) == "WIP") Grd1.Header.Cells[1, 10].Value = "Hasil Produksi";
                if (Sm.GetLue(LueType) == "Moving") Grd1.Header.Cells[1, 10].Value = "Moving Warehouse";
            }
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void TxtRegistrationNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRegistrationNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Registration#");
        }

        #endregion

        #endregion

    }
}
