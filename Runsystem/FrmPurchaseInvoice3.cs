﻿#region Update
/*
    16/12/2020 [WED/IMS] new apps (PI with approval)
    22/12/2020 [WED/IMS] print out
    30/12/2020 [WED/IMS] tambah informasi LocalCode, Specification
    04/01/2021 [ICA/IMS] tambah localcode dan specification di printout, PosEmp pada printout dibuat hardcode
    13/01/2021 [TKG/IMS] bug saat proses deposit vendor
    25/01/2020 [TKG/IMS] berdasarkan parameter IsPIJournalAPBasedOnCOATaxInd, merubah journal
    25/02/2021 [TKG/IMS] menggunakan parameter IsAPDownpaymentUseDefaultEntity dan DefaultEntity saat memproses vendor deposit, menyimpan informasi vendor deposit summary+informasi ap dp  
    03/03/2021 [VIN/IMS] Print By tanpa current date
    20/05/2021 [VIN/IMS] PO service dari docno PO service bukan PO biasa 
    08/06/2021 [TKG/PHT] saat cancel journal, informasi cost center tetap diisi dari journal saat diinsert sebelumnya.
    21/06/2021 [ICA/IMS] Detail Tax APDP belum sesuai
    06/07/2021 [TKG/IMS] tambah TaxAmt3 (invoice amount-coa amt)
    22/07/2021 [IBL/IMS] Saat savejournal, jika ada coa kepala 5 dan cost center kosong, maka CC otomatis terisi costcenter keuangan.
    18/10/2021 [WED/IMS] hanya tarik item yang tidak reject berdasarkan parameter IsRecvExpeditionEnabled
    13/01/2022 [IBL/IMS] BUG: Muncul warning deposit < dp, walaupun deposit > 0. Karena param IsAPDownpaymentUseEntity, tetapi EntCode nya kosong.
    07/06/2022 [DITA/IMS] UpdateVRVATFactur ditambah update ke VAT Settlemnt + ada kondisi ambilnya dari taxcode yg mana
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.Xml;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoice3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty, //if this application is called from other application;
            mSiteCode = string.Empty,
            mHeaderDate = string.Empty,
            mDetailDate = string.Empty,
            compare1 = string.Empty,
            compare2 = string.Empty,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty;
        internal bool 
            mProcFormatDocNo = false,
            mIsSiteMandatory = false, 
            mIsFilterBySite = false,
            mIsAPDownpaymentUseEntity = false,
            mIsDocInformationPIMandatory = false,
            mIsComparedToDetailDate = false,
            mIsPIDueDateAutoFill = false,
            mIsPIUseQRCodeInvoice = false,
            mIsGroupPaymentTermActived = false,
            mIsEProcUseDigitalInvoice = false,
            mIsVendorComboShowCategory = false,
            mIsPurchaseInvoiceUseApproval = false,
            mIsRecvExpeditionEnabled = false
            ;
        
        internal int mStateIndicator = 0;
        internal FrmPurchaseInvoice3Find FrmFind;
        internal bool mIsAutoGeneratePurchaseLocalDocNo = false, mIsFilterByDept = false;
        private List<LocalDocument> mlLocalDocument = null;
        private bool
            mIsPIWithZeroAmtProcessToOP = false,
            mIsAutoJournalActived = false,
            mIsPIShowWarningAPDP = false,
            mIsRemarkForJournalMandatory = false,
            mIsPurchaseInvoiceShowDataAfterInsert = false,
            mIsPurchaseInvoiceUseCOATaxInd = false,
            mIsPIPaymentTypeMandatory = false,
            mIsPIDeptEqualToPORDept = false,
            mIsTaxAliasMandatory = false,
            mIsPIAmtRoundOff = false,
            mIsPIQRCodeWarningEnabled = false,
            mIsPIDocInfoEditable = false,
            mIsPurchaseInvoiceShowAPDownpayment2 = false,
            mIsPurchaseInvoiceOnlyShowDataAfterInsert = false,
            mIsPurchaseInvoiceTaxInvoiceInfoValidationDisabled = false,
            mIsPITotalTaxOnlyShowTaxCalculation = false,
            mIsPIJournalAPBasedOnCOATaxInd = false,
            mIsAPDownpaymentUseDefaultEntity = false;
        iGCell fCell;
        bool fAccept;
        private iGCopyPasteManager fCopyPasteManager;
        private byte[] downloadedData;
        internal bool 
            mIsShowForeignName = false,
            mIsPITotalWithoutTaxInclDownpaymentEnabled = false;
        public string 
            mVoucherCodeFormatType = "1",
            mMInd = "N",
            mMainCurCode = string.Empty,
            mPIQRCodeTaxDocType = string.Empty,
            mPIQRCodeDPTaxDocType = string.Empty,
            mPurchaseInvoiceQRCodeTaxCodeDefault = string.Empty,
            mAcNoForRoundingCost = string.Empty,
            mOldServiceCode1 = string.Empty,
            mOldServiceCode2 = string.Empty,
            mOldServiceCode3 = string.Empty,
            mOldServiceNote1 = string.Empty,
            mOldServiceNote2 = string.Empty,
            mOldServiceNote3 = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mDigitalInvoiceHost = string.Empty,
            mDigitalInvoiceFolder = string.Empty,
            mCostCenterFormulaForAPAR = string.Empty
            ;
        internal bool
            mIsUseMInd = false,
            mIsUseActivePeriod = false,
            mIsPurchaseInvoiceDeptMandatory = false,
            mIsPIAutoShowPOTax = false,
            mIsPIAllowToUploadFile = false,
            mIsBOMShowSpecifications = false;
        private decimal 
            mAmt = 0m,
            mPIRoundedMaxValIfJournalNotBalance = 0m,
            mTotalTaxWithCOAAmt = 0m;

        #endregion

        #region Constructor

        public FrmPurchaseInvoice3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Purchase Invoice";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();

                if (!mIsPIDueDateAutoFill)
                    BtnDueDt.Visible = false;

                tabControl1.SelectTab("TpgPurchaseInvoiceDtl8");
                if (!mIsPurchaseInvoiceUseApproval)
                    tabControl1.TabPages.Remove(TpgPurchaseInvoiceDtl8);

                SetGrd();
                SetFormControl(mState.View);
                
                SetLueVdCode(ref LueVdCode, "");
                SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");

                if (mIsSiteMandatory)
                    LblSiteCode.ForeColor = Color.Red;

                if (mIsPurchaseInvoiceDeptMandatory)
                    LblDeptCode.ForeColor = Color.Red;

                if (mIsPIPaymentTypeMandatory)
                    LblPaymentType.ForeColor = Color.Red;

                //tabControl1.TabPages.Remove(TpgPurchaseInvoiceDtl3);

                tabControl1.SelectTab("TpgPurchaseInvoiceDtl1");
                Sl.SetLueCurCode(ref LueTaxCurCode);
                Sl.SetLueTaxCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueTaxCode1, LueTaxCode2, LueTaxCode3 });
                if (mPIQRCodeTaxDocType.Length == 0) PnlQRCode.Visible = false;
                Sl.SetLueServiceCode(ref LueServiceCode1);
                Sl.SetLueServiceCode(ref LueServiceCode2);
                Sl.SetLueServiceCode(ref LueServiceCode3);

                tabControl1.SelectTab("TpgPurchaseInvoiceDtl2");
                panel9.Visible = mIsPurchaseInvoiceUseCOATaxInd;
                
                tabControl1.SelectTab("TpgPurchaseInvoiceDtl4");
                //if (mPIQRCodeTaxDocType.Length == 0) panel10.Visible = false;
                Sl.SetLuePurchaseInvoiceDocType(ref LueDocType);
                Sl.SetLuePurchaseInvoiceDocInd(ref LueDocInd);
                LueDocType.Visible = false;
                LueDocInd.Visible = false;
                
                tabControl1.SelectTab("TpgPurchaseInvoiceDtl5");
                mlLocalDocument = new List<LocalDocument>();

                tabControl1.SelectTab("TpgPurchaseInvoiceDtl6");
                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType2);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueUserCode(ref LuePIC);
                Sl.SetLueCurCode(ref LueCurCode);
                if (mIsRemarkForJournalMandatory) LblRemark.ForeColor = Color.Red;
                if (mIsPurchaseInvoiceDeptMandatory) LblDeptCode.ForeColor = Color.Red;

                if (mIsUseMInd == true && mMInd == "N")
                    tabControl1.TabPages.Remove(TpgPurchaseInvoiceDtl6);

                if (!mIsPurchaseInvoiceShowAPDownpayment2) tabControl1.TabPages.Remove(TpgPurchaseInvoiceDtl7);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd 1

            Grd1.Cols.Count = 42;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "DNo",

                        //1-5
                        "",
                        "Received#",
                        "Received"+Environment.NewLine+"DNo",
                        "",
                        "PO#",
                        
                        //6-10
                        "",
                        "Item's Code",
                        "",
                        "Item's Name",    
                        "Quantity",
                        
                        //11-15
                        "UoM",
                        "Term of"+Environment.NewLine+"Payment",
                        "Currency",
                        "Unit"+Environment.NewLine+"Price",
                        "Discount"+Environment.NewLine+"%",
                        
                        //16-20
                        "Discount"+Environment.NewLine+"Amount",
                        "Rounding"+Environment.NewLine+"Value",
                        "Tax 1"+Environment.NewLine+"For Information Only",
                        "Tax 2"+Environment.NewLine+"For Information Only",
                        "Tax 3"+Environment.NewLine+"For Information Only",

                        //21-25
                        "Total",
                        "Delivery Type",
                        "Remark",
                        "ItScCode",
                        "Sub-Category",

                        //26-30
                        "Remark From PR",
                        "Received Local#",
                        "Site Code",
                        "Site",
                        "Received"+Environment.NewLine+"Date",

                        //31-34
                        "Foreign Name",
                        "DO#",
                        "PR Local#",
                        "Estimated"+Environment.NewLine+"Downpayment",
                        "",

                        //36-40
                        "File Name",
                        "",
                        "Local Code",
                        "Specification",
                        "Rejected",

                        //41
                        "PO#"
                    },
                    new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 170, 0, 20, 130,
                        
                        //6-10
                        20, 100, 20, 200, 100, 
                        
                        //11-15
                        80, 150, 70, 100, 100, 
                        
                        //16-20
                        100, 100, 140, 140, 140, 
                        
                        //21-23
                        100, 150, 400, 100, 150,

                        //26-30
                        280, 180, 0, 150, 80,

                        //31-34
                        180, 130, 150, 140, 20,
                        
                        //36-40
                        180, 20, 120, 250, 0,

                        //41
                        130
                    }
                );

            Sm.GrdColButton(Grd1, new int[] { 1, 4, 6, 8, 35, 37 });
            Sm.GrdFormatDate(Grd1, new int[] { 30 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 38, 39, 40, 41 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 14, 15, 16, 17, 18, 19, 20, 21, 34 }, 0);

            Grd1.Cols[41].Move(5);
            Sm.GrdColInvisible(Grd1, new int[] { 5 });

            Grd1.Cols[39].Move(10);
            Grd1.Cols[38].Move(9);

            Grd1.Cols[37].Move(36);
            Grd1.Cols[30].Move(3);
            if (mProcFormatDocNo)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3, 7, 8, 15, 16, 17, 18, 19, 20, 24, 26, 28, 29, 30, 31, 32, 35, 36 }, false);
                Grd1.Cols[25].Move(10);
            }
            else
            {
                  Sm.GrdColInvisible(Grd1, new int[] { 3, 7, 8, 15, 16, 17, 18, 19, 20, 24, 25, 26, 28, 29, 30, 31, 32, 35, 36 }, false);
            }

            if (!mIsShowForeignName)
            {
                Grd1.Cols[31].Visible = true;
                Grd1.Cols[31].Move(11);
            }
            if (mIsPIAllowToUploadFile)
                Sm.GrdColInvisible(Grd1, new int[] { 35, 36, 37 }, true);

            Grd1.Cols[27].Move(4);
            Grd1.Cols[33].Move(4);
            if (!mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 27 }, false);
            if (mIsSiteMandatory)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 29 }, true);
                Grd1.Cols[29].Move(7);
            }

            Grd1.Cols[32].Move(5);
            Grd1.Cols[34].Move(11);

            Sm.GrdColInvisible(Grd1, new int[] { 40 });

            fCopyPasteManager = new iGCopyPasteManager(Grd1);
            #endregion

            #region Grd 2

            Grd2.Cols.Count = 10;
            Grd2.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    { 
                         //0
                        "DNo",
                        
                        //1-5
                        "", 
                        "PO#",
                        "", 
                        "Amt Type",
                        "Type",
                        
                        //6-10
                        "Currency", 
                        "Outstanding"+Environment.NewLine+"Amount",
                        "Amount",
                        "Remark",
                       
                    },
                    new int[] { 0, 20, 130, 20, 0, 120, 80, 100, 100, 400, 180 }
                );
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 2, 4, 5, 6, 7 });
            Sm.GrdColButton(Grd2, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd2, new int[] { 7, 8 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 4, 9 }, !ChkHideInfoInGrd.Checked);


            #endregion

            #region Grd 3

            Grd3.Cols.Count = 20;
            Grd3.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Document Type Code", 
                        "Type", 
                        "QR Code", 
                        "", 
                        "Document#",  
                        
                        //6-10
                        "Tax Invoice"+Environment.NewLine+"Date", 
                        "Amount", 
                        "Tax", 
                        "Indicator Code", 
                        "Indicator", 
                        
                        //11-15
                        "Remark", 
                        "RemarkXML",
                        "Downpayment"+Environment.NewLine+"Document#",
                        "Downpayment DNo",
                        "",

                        //16-19
                        "File Name",
                        "",
                        "DigitalInvoiceDocNo",
                        "DigitalInvoiceDNo",
                    },
                    new int[] 
                    { 
                        //0
                        0, 

                        //1-5
                        0, 150, 250, 20, 180, 
                        
                        //6-10
                        120, 150, 150, 100, 100, 

                        //11-15
                        250, 0, 150, 0, 20,

                        //16-19
                        200, 20, 0, 0
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 4, 15, 17 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 1, 9, 13, 14, 16, 18, 19 });
            Sm.GrdFormatDate(Grd3, new int[] { 6 });
            Sm.GrdFormatDec(Grd3, new int[] { 7, 8 }, 0);
            if (!mIsPIUseQRCodeInvoice)
                Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 3, 4, 8, 9, 12, 13, 14, 15 }, false);
            else
                Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 9, 12, 14 }, false);
            Grd3.Cols[15].Move(0);
            
            Sm.GrdColInvisible(Grd3, new int[] { 18, 19 });
            if (!mIsEProcUseDigitalInvoice) Sm.GrdColInvisible(Grd3, new int[] { 16, 17 });

            #endregion

            #region Grd 4

            Grd4.Cols.Count = 6;
            Grd4.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400
                    }
                );
            Sm.GrdColButton(Grd4, new int[] { 0 });
            Sm.GrdColReadOnly(Grd4, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd4, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 1 }, false);

            #endregion

            #region Grd 5

            Grd5.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd5,
                new string[]
                { 
                    "Entity Code", 
                    "Entity", 
                    "", 
                    "Currency", 
                    "Deposit Amount" 
                },
                new int[] { 0, 150, 20, 100, 150 });
            Sm.GrdColButton(Grd5, new int[] { 2 });
            Sm.GrdFormatDec(Grd5, new int[] { 4 }, 0);
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 3, 4 });
            if (!mIsAPDownpaymentUseEntity)
                Sm.GrdColInvisible(Grd5, new int[] { 1, 2 }, false);

            #endregion

            #region Grd 6

            Grd6.Cols.Count = 13;
            Sm.GrdHdrWithColWidth(
                Grd6,
                new string[]
                { 
                    //0
                    "AP DP#",
 
                    //1-5
                    "",
                    "PO (Before Tax)", 
                    "Downpayment", 
                    "Tax Name", 
                    "Tax", 
                    
                    //6-10
                    "Tax Name", 
                    "Tax", 
                    "Tax Name", 
                    "Tax",
                    "Tax Code",

                    //11-12
                    "Tax Code",
                    "Tax Code"
                },
                new int[] { 
                    150, 
                    20, 150, 150, 200, 150, 
                    200, 150, 200, 150, 0,
                    0, 0
                    });
            Sm.GrdColInvisible(Grd6, new int[] { 1 }, false);
            Sm.GrdColButton(Grd6, new int[] { 1 });
            Sm.GrdFormatDec(Grd6, new int[] { 2, 3, 5, 7, 9 }, 0);
            Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            
            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 15, 16, 17, 18, 19, 20, 26, 30, 32 }, !ChkHideInfoInGrd.Checked);
            if (!mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 27 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 9 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd4, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd6, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLocalDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, LueVdCode, 
                        TxtVdInvNo, DteVdInvDt, DteDueDt, TxtCurCode, TxtDownPayment, 
                        MeeRemark, TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCurCode, TxtTaxRateAmt, 
                        TxtTotalTaxAmt2, LueTaxCode1, LueTaxCode2, LueTaxCode3, TxtTaxAmt1, 
                        TxtTaxAmt2, TxtTaxAmt3, TxtTotalTaxAmt1, LueDeptCode, LuePaymentType,
                        LueAcType, LueBankAcCode, LuePaymentType2, LueBankCode, TxtGiroNo, 
                        LuePIC, DteDueDate2, MeeDescription, LueCurCode, TxtRateAmt, 
                        TxtAmt2, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, TxtTaxInvoiceNo3, DteTaxInvoiceDt3,
                        ChkCOATaxInd, TxtAlias1, TxtAlias2, TxtAlias3, LueServiceCode1, LueServiceCode2, LueServiceCode3,
                        MeeServiceNote1, MeeServiceNote2, MeeServiceNote3, DtePaymentDt
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 23 });
                    BtnDueDt.Enabled = false;
                    Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueVdCode, TxtVdInvNo, DteVdInvDt, LuePaymentType, 
                        DteDueDt, MeeRemark, TxtTaxInvoiceNo, DteTaxInvoiceDt, TxtTaxRateAmt, 
                        LueTaxCode1, LueTaxCode2, LueTaxCode3, TxtDownPayment,
                        LuePIC, LueBankAcCode, LuePaymentType2, MeeDescription, TxtRateAmt, 
                        LueCurCode, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, TxtTaxInvoiceNo3, DteTaxInvoiceDt3,
                        TxtAlias1, TxtAlias2, TxtAlias3, LueServiceCode1, LueServiceCode2, LueServiceCode3,
                        MeeServiceNote1, MeeServiceNote2, MeeServiceNote3, DtePaymentDt
                    }, false);
                    if (!mIsPIDeptEqualToPORDept) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode }, false);
                    if (mIsPurchaseInvoiceUseCOATaxInd) ChkCOATaxInd.Properties.ReadOnly = false;
                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                    Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = false;
                    BtnDueDt.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 23 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason, ChkCancelInd, TxtTaxInvoiceNo, DteTaxInvoiceDt, TxtTaxInvoiceNo2, 
                        DteTaxInvoiceDt2, TxtTaxInvoiceNo3, DteTaxInvoiceDt3, TxtAlias1, TxtAlias2, TxtAlias3  
                    }, false);
                    if (!mIsPIUseQRCodeInvoice || mIsPIDocInfoEditable) Grd3.ReadOnly = false;
                    if (!ChkCancelInd.Checked)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueServiceCode1, LueServiceCode2, LueServiceCode3, MeeServiceNote1, MeeServiceNote2, MeeServiceNote3
                        }, false);
                    }
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mAmt = 0m;
            mTotalTaxWithCOAAmt = 0m;
            mSiteCode = string.Empty;
            mDetailDate = string.Empty;
            mHeaderDate = string.Empty;
            compare1 = string.Empty;
            compare2 = string.Empty;

            mOldServiceCode1 = mOldServiceCode2 = mOldServiceCode3 = mOldServiceNote1 = mOldServiceNote2 = mOldServiceNote3 = string.Empty;

            ChkCancelInd.Checked = false;
            ChkCOATaxInd.Checked = false;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, LueVdCode, TxtVdInvNo, 
                DteVdInvDt, LuePaymentType, DteDueDt, TxtCurCode, LueDeptCode, 
                TxtSiteCode, MeeRemark, TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCurCode, 
                LueTaxCode1, LueTaxCode2, LueTaxCode3, MeeCancelReason, LueAcType, 
                LueBankAcCode, LuePaymentType2, LueBankCode, TxtGiroNo, LuePIC, 
                DteDueDate2, MeeDescription, LueCurCode, TxtOPDocNo, TxtVRDocNo, 
                TxtVCDocNo, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, TxtTaxInvoiceNo3, DteTaxInvoiceDt3,
                TxtQRCodeTaxInvoiceNo, DteQRCodeTaxInvoiceDt, TxtAlias1, TxtAlias2, TxtAlias3, 
                LueServiceCode1, LueServiceCode2, LueServiceCode3, MeeServiceNote1, MeeServiceNote2, 
                MeeServiceNote3, DtePaymentDt, TxtStatus
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { 
                TxtTotalWithoutTax, TxtTotalWithTax, TxtDownPayment, TxtAmt, TxtTaxRateAmt, 
                TxtTotalTaxAmt2, TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, TxtTotalTaxAmt1,
                TxtRateAmt, TxtAmt2, TxtTaxAmtDifference, TxtQRCodeTaxAmt, TxtAmt3
            }, 0);

            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            ClearGrd4();
            ClearGrd5();
            ClearGrd6();

            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.FocusGrd(Grd3, 0, 1);
            Sm.FocusGrd(Grd4, 0, 1);
            Sm.FocusGrd(Grd5, 0, 0);
        }

        #region Clear Grid

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 14, 15, 16, 17, 18, 19, 20, 21, 34 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 7, 8 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 7, 8 });
        }

        private void ClearGrd4()
        {
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 3, 4 });
        }

        private void ClearGrd5()
        {
            Grd5.Rows.Clear();
            Grd5.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 4 });
        }

        private void ClearGrd6()
        {
            Grd6.Rows.Clear();
            Grd6.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 2, 3, 5, 7, 9 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPurchaseInvoice3Find(this);
            mStateIndicator = 1;
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                mStateIndicator = 1;
                SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
                Sm.SetLue(LueAcType, "C");
                SetLueVdCode(ref LueVdCode, string.Empty);
                TxtRateAmt.EditValue = Sm.FormatNum(1, 0);
                Sm.SetLue(LueTaxCurCode, mMainCurCode);
                TxtTaxRateAmt.EditValue = Sm.FormatNum(1, 0);
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            var SQL = new StringBuilder();
            string mDocNo = string.Empty;

            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                    {
                        if (mDocNo.Length > 0) mDocNo += ",";
                        mDocNo += Sm.GetGrdStr(Grd1, i, 2);
                    }
                }
            }

            SQL.AppendLine("Select Distinct D.EntCode ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("    And Find_In_Set(A.DocNo, @Param) ");
            SQL.AppendLine("Inner Join TblCostCenter C On B.CCCode = C.CCCode ");
            SQL.AppendLine("Inner Join TblProfitCenter D On C.ProfitCenterCode = D.ProfitCenterCode ");
            SQL.AppendLine("Limit 1; ");

            mEntCode = Sm.GetValue(SQL.ToString(), mDocNo);

            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
            SetLueDeptCode(ref LueDeptCode, string.Empty);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            
            try
            {
                PrintData(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDueDt_Click(object sender, EventArgs e)
        {
            if (mIsPIDueDateAutoFill)
            {
                if (DteVdInvDt.Text.Length > 0 && Grd1.Rows.Count > 1)
                {
                    ComputeDueDt();
                }
                else
                    Sm.StdMsg(mMsgType.Warning, "Received list and Vendor Invoice Date should not be empty.");
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueVdCode, "Vendor") && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPurchaseInvoice3Dlg(this, Sm.GetLue(LueVdCode)));
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 35 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                try
                {
                    OD.InitialDirectory = "c:";
                    OD.Filter = "PDF files (*.pdf)|*.pdf";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd1.Cells[e.RowIndex, 36].Value = OD.FileName;
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }

            if (TxtDocNo.Text.Length == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0 && e.ColIndex == 22)
                e.DoDefault = false;
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                DteDueDt.Text = null;
                ComputeAmt();
                ShowPOTax();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueVdCode, "Vendor") && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmPurchaseInvoice3Dlg(this, Sm.GetLue(LueVdCode)));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            //upload file
            if(BtnSave.Enabled)
            {
                if (e.ColIndex == 35 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                {
                    try
                    {
                        OD.InitialDirectory = "c:";
                        OD.Filter = "PDF files (*.pdf)|*.pdf";
                        OD.FilterIndex = 2;
                        OD.ShowDialog();
                        Grd1.Cells[e.RowIndex, 36].Value = OD.FileName;
                    }
                    catch (Exception Exc)
                    {
                        Sm.ShowErrorMsg(Exc);
                    }
                }
            }

            //download file
            if (e.ColIndex == 37)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 36).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 36), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 36);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 36, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            mSiteCode = Sm.GetGrdStr(Grd1, 0, 28);
            var EntCode = GetEntity();
            CheckAPDP();
            if (mIsRecvExpeditionEnabled) CheckRejectedItem();

            if (mIsPIDeptEqualToPORDept)
            {
                if (IsDeptInvalid()) return;
            }

            if (mIsPIAmtRoundOff && mAcNoForRoundingCost.Length>0)
            {
                if (mIsUseMInd && mMInd == "Y")
                {
                    if (Sm.StdMsgYN("Question",
                            "Do you want to save data ?" + Environment.NewLine +
                            "It will automatic generate voucher." + Environment.NewLine +
                            "The Invoice's amount will be rounded off."
                            ) == DialogResult.No || 
                            IsInsertedDataNotValid2(EntCode)) return;
                }
                else
                {
                    if (Sm.StdMsgYN("Question",
                            "Do you want to save data ?" + Environment.NewLine +
                            "The Invoice's amount will be rounded off.") == DialogResult.No ||
                        IsInsertedDataNotValid(EntCode)) return;
                }
                ProcessRoundOff();
            }
            else
            {
                if (mIsUseMInd && mMInd == "Y")
                {
                    if (Sm.StdMsgYN("Question",
                            "Do you want to save data ?" + Environment.NewLine +
                            "It will automatic generate voucher."
                            ) == DialogResult.No || IsInsertedDataNotValid2(EntCode)) return;
                }
                else
                {
                    if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                        IsInsertedDataNotValid(EntCode)) return;
                }
            }

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;
            var lDepositSummary = new List<DepositSummary>();
            string SubCategory = Sm.GetGrdStr(Grd1, 0, 24);
            //string DocNo = GenerateDocNo(Sm.GetDte(DteDocDt), "PurchaseInvoice", "TblPurchaseInvoiceHdr", SubCategory);
            string LocalDocNo = mIsAutoGeneratePurchaseLocalDocNo ? string.Empty : TxtLocalDocNo.Text;
            string OPDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OutgoingPayment", "TblOutgoingPaymentHdr");
            string VoucherRequestDocNo = string.Empty;

            if(mDocNoFormat == "1")
                DocNo = GenerateDocNo(Sm.GetDte(DteDocDt), "PurchaseInvoice", "TblPurchaseInvoiceHdr", SubCategory);
            else
                DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "PurchaseInvoice", "TblPurchaseInvoiceHdr", mEntCode, "1");

            if (mVoucherCodeFormatType == "2")
                if(mDocNoFormat == "1")
                    VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
                else 
                    VoucherRequestDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");
            else
                if (mDocNoFormat == "1")
                    VoucherRequestDocNo = GenerateVoucherRequestDocNo("1");
                else
                    VoucherRequestDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");

            //string VoucherDocNo = string.Empty;

            //if (mVoucherCodeFormatType == "2")
            //    VoucherDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", "1");
            //else
            //    VoucherDocNo = GenerateVoucherDocNo("1");

            int JournalSeqNo = 0;
            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;

            ProcessDepositSummary(ref lDepositSummary, EntCode);

            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                SetLocalDocument(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr
                );
                if (mlLocalDocument.Count == 0) return;
                if (IsLocalDocumentNotValid(
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr
                    )) return;

                SetRevision(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr,
                    ref Revision
                    );

                if (SeqNo.Length > 0)
                {
                    SetLocalDocNo(
                        "PurchaseInvoice",
                        ref LocalDocNo,
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );
                }
                mlLocalDocument.Clear();
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SavePurchaseInvoiceHdr(DocNo,
                LocalDocNo,
                SeqNo,
                DeptCode,
                ItSCCode,
                Mth,
                Yr,
                Revision,
                (mIsUseMInd == true && mMInd == "Y")
                ));

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SavePurchaseInvoiceDtl(DocNo, Row));
            }

            cml.Add(SaveDocApproval(DocNo));

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SavePurchaseInvoiceDtl2(DocNo, Row));
            }

            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SavePurchaseInvoiceDtl3(DocNo, Row));
            }

            if (Grd4.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0) cml.Add(SavePurchaseInvoiceDtl4(DocNo, Row));
            }

            if (Grd5.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0) cml.Add(SavePurchaseInvoiceDtl6(DocNo, Row));
            }

            if (Grd6.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd6.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd6, Row, 0).Length > 0) cml.Add(SavePurchaseInvoiceDtl7(DocNo, Row));
            }

            if (decimal.Parse(TxtDownPayment.Text) != 0m)
            { 
                cml.Add(SaveVendorDeposit(DocNo, EntCode));
                if (lDepositSummary.Count > 0)
                {
                    for (int i = 0; i < lDepositSummary.Count; i++)
                        cml.Add(SavePurchaseInvoiceDtl5(DocNo, lDepositSummary[i], EntCode));
                }
            }

            if (mIsAutoJournalActived)
            {
                JournalSeqNo += 1;
                cml.Add(SaveJournal(DocNo));
            }

            if (mIsUseMInd == true && mMInd == "Y")
            {
                cml.Add(SaveOutgoingPaymentHdr(OPDocNo, VoucherRequestDocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision));
                cml.Add(SaveOutgoingPaymentDtl(OPDocNo, DocNo));

                cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo));
                cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo));

                var VoucherDocNo = string.Empty;

                if (mVoucherCodeFormatType == "2")
                    if(mDocNoFormat == "1")
                        VoucherDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", "1");
                    else
                        VoucherDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", mEntCode, "1");
                else
                    if (mDocNoFormat == "1")
                         VoucherDocNo = GenerateVoucherDocNo("1");
                    else
                        VoucherDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", mEntCode, "1");
                cml.Add(SaveVoucherHdr(VoucherRequestDocNo, VoucherDocNo));
                cml.Add(SaveVoucherDtl(VoucherRequestDocNo, VoucherDocNo));

                if (mIsAutoJournalActived)
                {
                    JournalSeqNo += 1;
                    cml.Add(SaveJournal(VoucherDocNo, "03", JournalSeqNo, VoucherRequestDocNo));
                }
            }

            Sm.ExecCommands(cml);

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (mIsPIAllowToUploadFile && Sm.GetGrdStr(Grd1, Row, 36).Length > 0 && Sm.GetGrdStr(Grd1, Row, 36) != "openFileDialog1")
                    {
                        decimal Dno = Row+1;
                        UploadFile(DocNo, Sm.Right(string.Concat("000", Dno.ToString()), 3), Sm.GetGrdStr(Grd1, Row, 36));
                    }
                }
            }

            if (mIsPurchaseInvoiceShowDataAfterInsert)
            {
                if (mIsPurchaseInvoiceOnlyShowDataAfterInsert)
                    ShowData(DocNo);
                else
                {
                    if (Sm.StdMsgYN("Print", "") == DialogResult.No)
                    {
                        BtnInsertClick(sender, e);
                        if (mProcFormatDocNo) ShowPreviousInvoiceHdr(DocNo);
                    }
                    else
                    {
                        ShowData(DocNo);
                        PrintData(DocNo);
                    }
                }
            }
            else
                BtnInsertClick(sender, e);
        }

        private MySqlCommand SaveJournal(string DocNo, string DocType, int JournalSeqNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            if (mDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), JournalSeqNo));
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, JournalSeqNo.ToString()));
            }
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@DocType", "Outgoing Payment");
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, Concat('Voucher (', IfNull(@DocType, 'None'), ') : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblVoucherHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, ");
            SQL.AppendLine("B.DAmt, ");
            SQL.AppendLine("B.CAmt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            SQL.AppendLine("Select T.AcNo, Sum(T.Amt) As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select Concat(F.ParValue, B.VdCode) As AcNo, ");
            SQL.AppendLine("    Case When IfNull(D.CurCode, E.CurCode)=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=IfNull(D.DocDt, E.DocDt) And CurCode1=IfNull(D.CurCode, E.CurCode) And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End * ");
            SQL.AppendLine("    C.Amt As Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr B On A.DocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='1' ");
            SQL.AppendLine("    Left Join TblPurchaseInvoiceHdr D On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("    Left Join TblPurchaseReturnInvoiceHdr E On C.InvoiceDocNo=E.DocNo ");
            SQL.AppendLine("    Inner Join TblParameter F On F.ParCode='VendorAcNoAP' And F.ParValue Is Not Null ");
            SQL.AppendLine("    Where A.DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine(") T Group By T.AcNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("0.00 As DAmt, ");
            SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    Case When C.CurCode<>@MainCurCode Then ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("    Else B.RateAmt End ");
            SQL.AppendLine("End ");
            SQL.AppendLine("*IfNull(A.Amt, 0.00) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On C.BankAcCode=@BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            //Laba rugi selisih kurs

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select ParValue As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");
            
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            SQL.AppendLine("Update TblJournalDtl A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select DAmt, CAmt From (");
            SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
            SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
            SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
            SQL.AppendLine("And A.AcNo In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
            SQL.AppendLine("    And ParValue Is Not Null ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Delete From TblJournalDtl ");
            SQL.AppendLine("Where DocNo=@JournalDocNo ");
            SQL.AppendLine("And (DAmt=0 And CAmt=0) ");
            SQL.AppendLine("And AcNo In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
            SQL.AppendLine("    And ParValue Is Not Null ");
            SQL.AppendLine("    );");

            cm.CommandText = SQL.ToString();

            return cm;
        }

        #region Generate Local Document

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }

        private bool IsLocalDocumentNotValid(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr
            )
        {
            if (SeqNo.Length == 0) return false;

            foreach (var x in mlLocalDocument.Where(x => x.SeqNo.Length > 0))
            {
                if (!(
                  Sm.CompareStr(SeqNo, x.SeqNo) &&
                  Sm.CompareStr(DeptCode, x.DeptCode) &&
                  Sm.CompareStr(ItSCCode, x.ItSCCode) &&
                  Sm.CompareStr(Mth, x.Mth) &&
                  Sm.CompareStr(Yr, x.Yr)
                ))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Document# : " + x.DocNo + Environment.NewLine +
                        "Local# : " + x.LocalDocNo + Environment.NewLine + Environment.NewLine +
                        "Invalid data.");
                    return true;
                }
            }
            return false;
        }

        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            bool IsFirst = true;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(DocNo=@DocNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 2));
                        No += 1;
                    }
                }
            }
            Filter = " Where (" + Filter + ")";

            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblRecvVdHdr " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);

                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (IsFirst && SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                            IsFirst = false;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private bool IsInsertedDataNotValid(string EntCode)
        {

            IsSubCategoryNull();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                (mIsPIPaymentTypeMandatory && Sm.IsLueEmpty(LuePaymentType, "Requested payment type")) ||
                Sm.IsDteEmpty(DteDueDt, "Due date") ||                 
                (mIsPurchaseInvoiceDeptMandatory && Sm.IsLueEmpty(LueDeptCode, "Department")) ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsCurrencyNotValid() ||
                IsRecvVdInvalid() ||
                IsAmtTypeDataNotValid() ||
                IsJournalAmtNotBalanced() ||
                IsDownpaymentBiggerThanDeposit(EntCode) ||
                IsSubcategoryDifferent() ||
                IsSiteNotValid() ||
                IsDateNotValid()||
                IsTaxInfoInvalid() ||
                IsTaxAlias() ||
                IsDocInformationEmpty() ||
                IsQRCodeTaxAmtNotValid() ||
                IsItemRejected()
                ;
        }

        private bool IsInsertedDataNotValid2(string EntCode)
        {
            IsSubCategoryNull();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                (mIsPIPaymentTypeMandatory && Sm.IsLueEmpty(LuePaymentType, "Requested payment type")) ||
                Sm.IsDteEmpty(DteDueDt, "Due date") ||
                (mIsPurchaseInvoiceDeptMandatory && Sm.IsLueEmpty(LueDeptCode, "Department")) ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                (TxtTaxInvoiceNo.Text.Length>0 && Sm.IsTxtEmpty(TxtTaxRateAmt, "Tax's rate.", true)) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsCurrencyNotValid() ||
                IsRecvVdInvalid() ||
                IsAmtTypeDataNotValid() ||
                IsJournalAmtNotBalanced() ||
                IsDownpaymentBiggerThanDeposit(EntCode) ||
                IsSubcategoryDifferent() ||
                IsSiteNotValid() ||
                IsDateNotValid() ||
                IsDocInformationEmpty() ||
                Sm.IsLueEmpty(LueAcType, "Account Type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Bank Account") ||
                Sm.IsLueEmpty(LuePaymentType2, "Payment Type") ||
                IsPaymentTypeNotValid()||
                Sm.IsLueEmpty(LuePIC, "PIC") ||
                Sm.IsMeeEmpty(MeeDescription, "Description") ||
                IsTaxInfoInvalid() ||
                IsItemRejected()
                ;
        }

        private bool IsItemRejected()
        {
            if (!mIsRecvExpeditionEnabled) return false;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 40) == "Y")
                {
                    Sm.StdMsg(mMsgType.Warning, "This item is rejected.");
                    Sm.FocusGrd(Grd1, i, 2);
                    return true;
                }
            }

            return false;
        }

        private bool IsQRCodeTaxAmtNotValid()
        {
            if (!mIsPIUseQRCodeInvoice) return false;
            if (!mIsPIQRCodeWarningEnabled) return false;
            decimal Amt = 0m;

            if (TxtTaxAmtDifference.Text.Length > 0) Amt = decimal.Parse(TxtTaxAmtDifference.Text);

            if (Amt != 0m)
            {
                decimal TotalWithTax = 0m, TotalWithoutTax = 0m;

                if (TxtTotalWithTax.Text.Length > 0) TotalWithTax = decimal.Parse(TxtTotalWithTax.Text);
                if (TxtTotalWithoutTax.Text.Length > 0) TotalWithoutTax = decimal.Parse(TxtTotalWithoutTax.Text);

                Sm.StdMsg(mMsgType.Warning,
                    "Auto-calculation : " + Sm.FormatNum(TotalWithTax-TotalWithoutTax, 0) + Environment.NewLine +
                    "QR Code : " + TxtQRCodeTaxAmt.Text + Environment.NewLine +
                    "Tax Balance : " + TxtTaxAmtDifference.Text + Environment.NewLine + Environment.NewLine +
                    "You can't save this document." + Environment.NewLine +
                    "There is a difference between auto-calculation and QR code tax amount."+ Environment.NewLine +
                    "You need to do balancing process.");
                return true;
            }
            return false;
        }

        private bool IsTaxInfoInvalid()
        {
            string 
                TaxCode1 = Sm.GetLue(LueTaxCode1),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3);

            if (!mIsPurchaseInvoiceTaxInvoiceInfoValidationDisabled)
            {
                if (TxtTaxInvoiceNo.Text.Length > 0 || Sm.GetDte(DteTaxInvoiceDt).Length > 0 || Sm.GetLue(LueTaxCode1).Length > 0)
                {
                    if (Sm.IsTxtEmpty(TxtTaxInvoiceNo, "Tax invoice#", false)) return true;
                    if (Sm.IsDteEmpty(DteTaxInvoiceDt, "Tax invoice date")) return true;
                    if (Sm.IsLueEmpty(LueTaxCode1, "Tax")) return true;
                }

                if (TxtTaxInvoiceNo2.Text.Length > 0 || Sm.GetDte(DteTaxInvoiceDt2).Length > 0 || Sm.GetLue(LueTaxCode2).Length > 0)
                {
                    if (Sm.IsTxtEmpty(TxtTaxInvoiceNo2, "Tax invoice#", false)) return true;
                    if (Sm.IsDteEmpty(DteTaxInvoiceDt2, "Tax invoice date")) return true;
                    if (Sm.IsLueEmpty(LueTaxCode2, "Tax")) return true;
                }

                if (TxtTaxInvoiceNo3.Text.Length > 0 || Sm.GetDte(DteTaxInvoiceDt3).Length > 0 || Sm.GetLue(LueTaxCode3).Length > 0)
                {
                    if (Sm.IsTxtEmpty(TxtTaxInvoiceNo3, "Tax invoice#", false)) return true;
                    if (Sm.IsDteEmpty(DteTaxInvoiceDt3, "Tax invoice date")) return true;
                    if (Sm.IsLueEmpty(LueTaxCode3, "Tax")) return true;
                }
            }

            if (TaxCode1.Length == 0 && (TaxCode2.Length > 0 || TaxCode3.Length > 0))
            {
                Sm.StdMsg(mMsgType.Warning, "Tax is empty.");
                return true;
            }

            if (TaxCode1.Length > 0)
            {
                var TaxRateAmt = decimal.Parse(TxtTaxRateAmt.Text);
                if (Sm.CompareStr(Sm.GetLue(LueTaxCurCode), TxtCurCode.Text))
                {
                    if (TaxRateAmt != 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid tax's rate.");
                        return true;
                    }
                }
                else
                {
                    if (TaxRateAmt == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid tax's rate.");
                        return true;
                    }
                }

                if (Sm.IsDataExist("Select 1 From TblTax Where TaxCode=@Param And TaxInvoiceInd='Y';", TaxCode1))
                {
                    if (Sm.IsTxtEmpty(TxtTaxInvoiceNo, "Tax invoice#", false)) return true;
                    if (Sm.IsDteEmpty(DteTaxInvoiceDt, "Tax invoice date")) return true;
                }

                if (Sm.IsDataExist("Select 1 From TblTax Where TaxCode=@Param And ServiceInd='Y';", TaxCode1))
                {
                    if (Sm.IsLueEmpty(LueServiceCode1, "Service")) return true;
                }
            }

            if (TaxCode2.Length > 0)
            {
                if (Sm.IsDataExist("Select 1 From TblTax Where TaxCode=@Param And ServiceInd='Y';", TaxCode2))
                {
                    if (Sm.IsLueEmpty(LueServiceCode2, "Service")) return true;
                }
            }

            if (TaxCode3.Length > 0)
            {
                if (Sm.IsDataExist("Select 1 From TblTax Where TaxCode=@Param And ServiceInd='Y';", TaxCode3))
                {
                    if (Sm.IsLueEmpty(LueServiceCode3, "Service")) return true;
                }
            }

            if (TaxCode1.Length > 0 && TaxCode2.Length > 0 && Sm.CompareStr(TaxCode1, TaxCode2))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Tax (1) : " + LueTaxCode1.GetColumnValue("Col2") + Environment.NewLine +
                    "Tax (2) : " + LueTaxCode2.GetColumnValue("Col2") + Environment.NewLine +
                    "Purchase invoice should not have the same taxes."
                    );
                return true;
            }

            if (TaxCode1.Length > 0 && TaxCode3.Length > 0 && Sm.CompareStr(TaxCode1, TaxCode3))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Tax (1) : " + LueTaxCode1.GetColumnValue("Col2") + Environment.NewLine +
                    "Tax (3) : " + LueTaxCode3.GetColumnValue("Col2") + Environment.NewLine +
                    "Purchase invoice should not have the same taxes."
                    );
                return true;
            }

            if (TaxCode2.Length > 0 && TaxCode3.Length > 0 && Sm.CompareStr(TaxCode2, TaxCode3))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Tax (2) : " + LueTaxCode2.GetColumnValue("Col2") + Environment.NewLine +
                    "Tax (3) : " + LueTaxCode3.GetColumnValue("Col2") + Environment.NewLine +
                    "Purchase invoice should not have the same taxes."
                    );
                return true;
            }
            return false;
        }

        private bool IsTaxAlias()
        {

            if (mIsTaxAliasMandatory && Sm.GetLue(LueTaxCode1).Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtAlias1, "Tax alias", false)) return true;
            }

            if (mIsTaxAliasMandatory && Sm.GetLue(LueTaxCode2).Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtAlias2, "Tax alias", false)) return true;
            }

            if (mIsTaxAliasMandatory && Sm.GetLue(LueTaxCode3).Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtAlias3, "Tax alias", false)) return true;
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType2), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType2), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType2), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDate2, "Due Date ")) return true;
            }

            return false;
        }

        private void CheckAPDP()
        {
            if (!mIsPIShowWarningAPDP)
            {
                if (Grd5.Rows.Count > 0 && Sm.GetGrdDec(Grd5, 0, 4) > 0 && Decimal.Parse(TxtDownPayment.Text) == 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Vendor " + LueVdCode.Text + " have deposit amount ");
                }
            }
        }

        private bool IsDocInformationEmpty()
        {
            if (Grd3.Rows.Count == 1)
            {
                if (mIsDocInformationPIMandatory)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in document information list.");
                    Sm.FocusGrd(Grd3, 0, 1);// Grd3.Focus();
                    return true;
                }
            }
            return false;
        }

        private bool IsDateNotValid()
        {
            mHeaderDate = Sm.GetDte(DteDocDt);

            if (mIsComparedToDetailDate)
            {
                if (Grd1.Rows.Count != 1)
                {
                    //loop grid untuk mendapatkan tanggal terbaru
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareDtTm(compare1, Sm.GetGrdDate(Grd1, Row, 30)) < 0)
                        {
                            compare1 = Sm.GetGrdDate(Grd1, Row, 30);
                            compare2 = compare1;
                        }
                        else
                            compare2 = compare1;
                    }
                    mDetailDate = compare2;

                    if (Sm.CompareDtTm(mHeaderDate, mDetailDate) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Document's date should not be earlier than received date.");
                        DteDocDt.Focus();
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsSiteNotValid()
        {
            TxtSiteCode.EditValue = Sm.GetGrdStr(Grd1, 0, 29);

            if (!mIsSiteMandatory) return false;

            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (!Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 28), mSiteCode))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Received# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "PO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                            "Site Name : " + Sm.GetGrdStr(Grd1, Row, 29) + Environment.NewLine + Environment.NewLine +
                            "Invalid site."
                            );
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsAmtTypeDataNotValid()
        { 
            ComputeOutstandingAmt();

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0 && IsQtyBiggerThanOutstandingQty(Row)) return true;
            
            return false;
        }

        private bool IsQtyBiggerThanOutstandingQty(int Row)
        {
            decimal
                Outstanding = Sm.GetGrdDec(Grd2, Row, 7), 
                Qty = Sm.GetGrdDec(Grd2, Row, 8);

            if (Qty > Outstanding)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "PO# : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                    "Type : " + Sm.GetGrdStr(Grd2, Row, 5) + Environment.NewLine + Environment.NewLine +
                    "Quantity (" + Sm.FormatNum(Qty, 0) + ") is bigger than outstanding (" + Sm.FormatNum(Outstanding, 0) + ")."
                    );
                return true;
            }

            return false;
        }

        private string GetEntity()
        {
            if (mSiteCode.Length == 0 || !mIsAPDownpaymentUseEntity) return string.Empty;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.EntCode ");
            SQL.AppendLine("From TblSite A ");
            SQL.AppendLine("Inner Join TblProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode ");
            SQL.AppendLine("Where A.SiteCode=@SiteCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);

            return Sm.GetValue(cm);
        }

        private bool IsDownpaymentBiggerThanDeposit(string EntCode)
        {
            decimal Downpayment = decimal.Parse(TxtDownPayment.Text);

            if (Downpayment == 0m) return false;

            decimal Deposit = 0m;

            //Recompute Deposit
            ShowVendorDepositSummary(Sm.GetLue(LueVdCode));
            ShowAPDownpayment();

            var EntName = string.Empty;

            //Get Currency
            string CurCode = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 13).Length != 0)
                    {
                        CurCode = Sm.GetGrdStr(Grd1, Row, 13);
                        break;
                    }
                }
            }

            //Get Deposit Amount Based on currency
            if (mIsAPDownpaymentUseEntity)
            {
                if (EntCode.Length > 0 && Grd5.Rows.Count > 0)
                {
                    for (int row = 0; row < Grd5.Rows.Count - 1; row++)
                    {
                        if (Sm.CompareStr(EntCode, Sm.GetGrdStr(Grd5, row, 0)) &&
                            Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd5, row, 3)))
                        {
                            EntName = Sm.GetGrdStr(Grd5, row, 1);
                            Deposit = Sm.GetGrdDec(Grd5, row, 4);
                            break;
                        }
                    }
                }
                else
                {
                    if (Grd5.Rows.Count > 0)
                    {
                        for (int row = 0; row < Grd5.Rows.Count - 1; row++)
                        {
                            if (Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd5, row, 3)))
                            {
                                Deposit = Sm.GetGrdDec(Grd5, row, 4);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                if (Grd5.Rows.Count > 0)
                {
                    for (int row = 0; row < Grd5.Rows.Count - 1; row++)
                    {
                        if (Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd5, row, 3)))
                        {
                            Deposit = Sm.GetGrdDec(Grd5, row, 4);
                            break;
                        }
                    }
                }
            }
            
            if (Downpayment > Deposit)
            {
                var Msg = new StringBuilder();

                if (mIsAPDownpaymentUseEntity && EntCode.Length>0)
                {
                    if (EntName.Length>0)    
                        Msg.AppendLine("Entity : " + EntName);
                    Msg.AppendLine("Currency : " + CurCode);
                    Msg.AppendLine("Deposit Amount: " + Sm.FormatNum(Deposit, 0));
                    Msg.AppendLine("Downpayment Amount: " + Sm.FormatNum(Downpayment, 0)+ Environment.NewLine);
                    Msg.AppendLine("Downpayment is bigger than existing deposit.");
                }
                else
                {
                    Msg.AppendLine("Currency : " + CurCode);
                    Msg.AppendLine("Deposit Amount: " + Sm.FormatNum(Deposit, 0));
                    Msg.AppendLine("Downpayment Amount: " + Sm.FormatNum(Downpayment, 0) + Environment.NewLine);
                    Msg.AppendLine("Downpayment is bigger than existing deposit.");
                }
                Sm.StdMsg(mMsgType.Warning, Msg.ToString());
                return true;
            }

            return false;
        }

        private bool IsRecvVdInvalid()
        {
            if (Grd1.Rows.Count <= 1) return false;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsRecvVdInvalid1(r)) return true;
                    if (IsRecvVdInvalid2(r)) return true;
                }
            }
            return false;
        }

        private bool IsRecvVdInvalid1(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A, TblPurchaseInvoiceDtl B ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.RecvVdDocNo=@DocNo ");
            SQL.AppendLine("And B.RecvVdDNo=@DNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 3));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Received# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                    "PO# : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Item's code : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "Item's name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + Environment.NewLine +
                    "This received# already invoiced.");
                return true;
            }
            return false;
        }

        private bool IsRecvVdInvalid2(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblRecvVdDtl ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And (CancelInd='Y' Or Status='C'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 3));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Received# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                    "PO# : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Item's code : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "Item's name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + Environment.NewLine +
                    "This received# already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1 && Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in received document list or discount/customs tax/down payment list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Received# is empty.")) return true;
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "PO# is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 8, true, 
                        "PO# : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                        "Type : " + Sm.GetGrdStr(Grd2, Row, 5) + Environment.NewLine + Environment.NewLine + 
                        "Amount is 0.")) return true;
                }
            }

            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 2, false, "Document type is empty.")) return true;
            }

            if (Grd4.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd4, Row, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd4, Row, 3) == 0m && Sm.GetGrdDec(Grd4, Row, 4) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Account# : " + Sm.GetGrdStr(Grd4, Row, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd4, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount can't be 0.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsCurrencyNotValid()
        {
            bool NotValid = false;
            string CurCode= string.Empty;

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 13).Length != 0)
                    {
                        if (CurCode.Length!=0 && !Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, Row, 13)))
                        {
                            NotValid = true;
                            break;
                        }
                        CurCode = Sm.GetGrdStr(Grd1, Row, 13);
                    }
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 6).Length != 0)
                    {
                        if (CurCode.Length != 0 && !Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, Row, 6)))
                        {
                            NotValid = true;
                            break;
                        }
                        CurCode = Sm.GetGrdStr(Grd2, Row, 6);
                    }
                }
            }
            if (NotValid) Sm.StdMsg(mMsgType.Warning, "One purchase invoice# only allowed 1 currency type.");
            return NotValid;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 3).Length > 0) Debit += Sm.GetGrdDec(Grd4, Row, 3);
                if (Sm.GetGrdStr(Grd4, Row, 4).Length > 0) Credit += Sm.GetGrdDec(Grd4, Row, 4);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 24).Length == 0)
                {
                    Grd1.Cells[Row, 24].Value = Grd1.Cells[Row, 25].Value = "XXX";
                }
            }
        }

        private bool IsSubcategoryDifferent()
        {
            if (mProcFormatDocNo)
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 24);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 24))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different subcategory ");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SavePurchaseInvoiceHdr(string DocNo,
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision,
            bool MInd
            )

        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPurchaseInvoiceHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, ProcessInd, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("VdCode, VdInvNo, VdInvDt, PaymentType, DueDt, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceDt, TaxInvoiceNo2, TaxInvoiceDt2, TaxInvoiceNo3, TaxInvoiceDt3, TaxCurCode, ");
            SQL.AppendLine("TaxRateAmt, TaxCode1, TaxCode2, TaxCode3, TaxAmt, DownPayment, CurCode, Amt, SiteCode, JournalDocNo, COATaxInd, Remark, TaxAlias1, TaxAlias2, TaxAlias3, ");
            SQL.AppendLine("ServiceCode1, ServiceCode2, ServiceCode3, ServiceNote1, ServiceNote2, ServiceNote3, PaymentDt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', 'O', @ProcessInd, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("@VdCode, @VdInvNo, @VdInvDt, @PaymentType, @DueDt, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceDt, @TaxInvoiceNo2, @TaxInvoiceDt2, @TaxInvoiceNo3, @TaxInvoiceDt3, @TaxCurCode, ");
            SQL.AppendLine("@TaxRateAmt, @TaxCode1, @TaxCode2, @TaxCode3, @TaxAmt, @DownPayment, @CurCode, @Amt, @SiteCode, Null, @COATaxInd, @Remark, @TaxAlias1, @TaxAlias2, @TaxAlias3, ");
            SQL.AppendLine("@ServiceCode1, @ServiceCode2, @ServiceCode3, @ServiceNote1, @ServiceNote2, @ServiceNote3, @PaymentDt, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime());");

            if (!mIsPIWithZeroAmtProcessToOP)
            {
                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ProcessInd='F' ");
                if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
                    SQL.AppendLine("Where DocNo=@DocNo And (Amt+TaxAmt)=0.00;");
                else
                    SQL.AppendLine("Where DocNo=@DocNo And (Amt+TaxAmt-DownPayment)=0.00;");
            }

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProcessInd", MInd?"F":"O");
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                if (DeptCode.Length==0)
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                else
                    Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            }
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@VdInvNo", TxtVdInvNo.Text);
            Sm.CmParamDt(ref cm, "@VdInvDt", Sm.GetDte(DteVdInvDt));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<String>(ref cm, "@TaxCurCode", Sm.GetLue(LueTaxCurCode));
            Sm.CmParam<Decimal>(ref cm, "@TaxRateAmt", Decimal.Parse(TxtTaxRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            //if (mIsPITotalTaxOnlyShowTaxCalculation) Sm.CmParam<Decimal>(ref cm, "@TaxAmt", mTotalTaxWithCOAAmt);
            //else 
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Decimal.Parse(TxtTotalTaxAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@DownPayment", Decimal.Parse(TxtDownPayment.Text));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, 0, 13));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtTotalWithoutTax.Text));
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            Sm.CmParam<String>(ref cm, "@COATaxInd", ChkCOATaxInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@TaxAlias1", TxtAlias1.Text);
            Sm.CmParam<String>(ref cm, "@TaxAlias2", TxtAlias2.Text);
            Sm.CmParam<String>(ref cm, "@TaxAlias3", TxtAlias3.Text);
            Sm.CmParam<String>(ref cm, "@ServiceCode1", Sm.GetLue(LueServiceCode1));
            Sm.CmParam<String>(ref cm, "@ServiceCode2", Sm.GetLue(LueServiceCode2));
            Sm.CmParam<String>(ref cm, "@ServiceCode3", Sm.GetLue(LueServiceCode3));
            Sm.CmParam<String>(ref cm, "@ServiceNote1", MeeServiceNote1.Text);
            Sm.CmParam<String>(ref cm, "@ServiceNote2", MeeServiceNote2.Text);
            Sm.CmParam<String>(ref cm, "@ServiceNote3", MeeServiceNote3.Text);
            Sm.CmParamDt(ref cm, "@PaymentDt", Sm.GetDte(DtePaymentDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePurchaseInvoiceDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPurchaseInvoiceDtl(DocNo, DNo, RecvVdDocNo, RecvVdDNo, FileName, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @RecvVdDocNo, @RecvVdDNo, @FileName, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@RecvVdDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@RecvVdDNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd1, Row, 36));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePurchaseInvoiceDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPurchaseInvoiceDtl2(DocNo, DNo, PODocNo, CurCode, AmtType, Amt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @PODocNo, @CurCode, @AmtType, @Amt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PODocNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@AmtType", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePurchaseInvoiceDtl3(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPurchaseInvoiceDtl3(DocNo, DNo, DocType, QRCode, DocNumber, TaxInvDt,  Amt, TaxAmt, DocInd, Remark, RemarkXml, APDownpaymentDocNo, APDownpaymentDNo, DigitalInvoiceDocNo, DigitalInvoiceDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @DocType, @QRCode, @DocNumber, @TaxInvDt, @Amt, @TaxAmt, @DocInd, @Remark, @RemarkXml, @APDownpaymentDocNo, @APDownpaymentDNo, @DigitalInvoiceDocNo, @DigitalInvoiceDNo, @UserCode, CurrentDateTime()); ");

            if (mIsPIUseQRCodeInvoice && !ChkCancelInd.Checked)
            {
                SQL.AppendLine("Update TblAPDownpaymentDtl2 Set ");
                SQL.AppendLine("    PurchaseInvoiceDocNo=@DocNo ");
                SQL.AppendLine("Where DocNo=@APDownpaymentDocNo ");
                SQL.AppendLine("And DNo=@APDownpaymentDNo ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
                SQL.AppendLine("    Where DocNo = @DocNo ");
                SQL.AppendLine("    And Status = 'A' ");
                SQL.AppendLine("); ");
            }

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@QRCode", Sm.GetGrdStr(Grd3, Row, 3));
            Sm.CmParam<String>(ref cm, "@DocNumber", Sm.GetGrdStr(Grd3, Row, 5));
            Sm.CmParamDt(ref cm, "@TaxInvDt", Sm.GetGrdDate(Grd3, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<String>(ref cm, "@DocInd", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 11));
            Sm.CmParam<String>(ref cm, "@RemarkXml", Sm.GetGrdStr(Grd3, Row, 12));
            Sm.CmParam<String>(ref cm, "@APDownpaymentDocNo", Sm.GetGrdStr(Grd3, Row, 13));
            Sm.CmParam<String>(ref cm, "@APDownpaymentDNo", Sm.GetGrdStr(Grd3, Row, 14));
            Sm.CmParam<String>(ref cm, "@DigitalInvoiceDocNo", Sm.GetGrdStr(Grd3, Row, 18));
            Sm.CmParam<String>(ref cm, "@DigitalInvoiceDNo", Sm.GetGrdStr(Grd3, Row, 19));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePurchaseInvoiceDtl4(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPurchaseInvoiceDtl4(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd4, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd4, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePurchaseInvoiceDtl5(string DocNo, DepositSummary i, string EntCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPurchaseInvoiceDtl5 ");
            SQL.AppendLine("(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @CurCode, @ExcRate, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr Where DocNo=@DocNo And Status = 'A'; ");

            SQL.AppendLine("Update TblVendorDepositSummary2 Set ");
            SQL.AppendLine("    Amt=Amt-@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where VdCode=@VdCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And IfNull(EntCode, '')=IfNull(@EntCode, '') ");
            SQL.AppendLine("And ExcRate=@ExcRate ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", i.ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", i.UsedAmt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePurchaseInvoiceDtl6(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPurchaseInvoiceDtl6 ");
            SQL.AppendLine("(DocNo, EntCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @EntCode, @CurCode, @Amt, @UserCode, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetGrdStr(Grd5, Row, 0));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd5, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd5, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand SavePurchaseInvoiceDtl7(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPurchaseInvoiceDtl7(DocNo, APDownpaymentDocNo, POBefTax, Downpayment, TaxCode, TaxCode2, TaxCode3, TaxAmt, TaxAmt2, TaxAmt3, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @APDownpaymentDocNo, @POBefTax, @Downpayment, @TaxCode, @TaxCode2, @TaxCode3, @TaxAmt, @TaxAmt2, @TaxAmt3, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@APDownpaymentDocNo", Sm.GetGrdStr(Grd6, Row, 0));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetGrdStr(Grd6, Row, 10));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetGrdStr(Grd6, Row, 11));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetGrdStr(Grd6, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@POBefTax", Sm.GetGrdDec(Grd6, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Downpayment", Sm.GetGrdDec(Grd6, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd6, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", Sm.GetGrdDec(Grd6, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", Sm.GetGrdDec(Grd6, Row, 9));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='PurchaseInvoice' ");

            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select Sum(B.QtyPurchase * E.UPrice) ");
            SQL.AppendLine("    From TblPurchaseInvoiceDtl A ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl B On A.RecvVdDocNo = B.DocNo And A.RecvVdDNo = B.DNo ");
            SQL.AppendLine("    Inner Join TblPODtl C On B.PODocNo = C.DocNo And B.PODNo = C.DNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl D On C.PORequestDocNo = D.DocNo And C.PORequestDNo = D.DNo ");
            SQL.AppendLine("    Inner Join TblQtDtl E On D.QtDocNo = E.DocNo And D.QtDNo = E.DNo ");
            SQL.AppendLine("    Where A.DocNo = @DocNo ");
            SQL.AppendLine("), 0)) ");

            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='PurchaseInvoice' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorDeposit(string DocNo, string EntCode)
        {
            var SQL = new StringBuilder();

            if (mIsAPDownpaymentUseDefaultEntity)
                SQL.AppendLine("Set @EntCode:=(Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='DefaultEntity'); ");

            SQL.AppendLine("Insert Into TblVendorDepositMovement(DocNo, DocType, DocDt, VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, ");
            SQL.AppendLine("(Case @CancelInd When 'Y' Then '04' Else '03' End) As DocType, ");
            SQL.AppendLine("DocDt, VdCode, @EntCode, CurCode, (Case @CancelInd When 'Y' Then 1 Else -1 End)*Downpayment, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And Status='A'; ");

            SQL.AppendLine("Insert Into TblVendorDepositSummary(VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VdCode, IfNull(@EntCode, ''), @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblPurchaseInvoicehdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status='A' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+((Case @CancelInd When 'Y' Then 1.00 Else -1.00 End)*@Amt), LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            if (!mIsAPDownpaymentUseDefaultEntity) Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, 0, 13));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtDownPayment.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveVendorDepositSummary2(DepositSummary i, string EntCode)
        {
            var SQL = new StringBuilder();

            if (mIsAPDownpaymentUseDefaultEntity)
                SQL.AppendLine("Set @EntCode:=(Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='DefaultEntity'); ");

            SQL.AppendLine("Update TblVendorDepositSummary2 Set ");
            SQL.AppendLine("    Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where VdCode=@VdCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And IfNull(EntCode, '')=IfNull(@EntCode, '') ");
            SQL.AppendLine("And ExcRate=@ExcRate; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            if (!mIsAPDownpaymentUseDefaultEntity) Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", i.ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", i.UsedAmt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var EntCode = Sm.GetValue(
                     "Select B.EntCode " +
                     "From TblSite A, TblProfitCenter B " +
                     "Where A.SiteCode=@Param " +
                     "And A.ProfitCenterCode=B.ProfitCenterCode;",
                     mSiteCode
                     );

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            if(mDocNoFormat == "1")
                SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            else
                SQL.AppendLine(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), mEntCode, "1" ));
            SQL.AppendLine("Where DocNo=@DocNo And Status = 'A';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Purchase Invoice : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr Where DocNo=@DocNo And Status = 'A'; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            //Hutang Uninvoice

            SQL.AppendLine("        Select Concat(E.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        (C.Qty*D.UPrice*D.ExcRate) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl C On B.RecvVdDocNo=C.DocNo And B.RecvVdDNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice D On C.Source=D.Source ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode='VendorAcNoUnInvoiceAP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            //PPN Masukan
            //if (TxtTotalTaxAmt1.Text.Length > 0 && decimal.Parse(TxtTotalTaxAmt1.Text)!=0)
            //{
                if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then (A.Amt*B.TaxRate*0.01) Else 0.00 End As DAmt, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01) End As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo1 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then (A.Amt*B.TaxRate*0.01) Else 0.00 End As DAmt, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01) End As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo1 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then (A.Amt*B.TaxRate*0.01) Else 0.00 End As DAmt, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01) End As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo1 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then (A.Amt*B.TaxRate*0.01*A.TaxRateAmt) Else 0.00 End As DAmt, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01*A.TaxRateAmt) End As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo1 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then (A.Amt*B.TaxRate*0.01*A.TaxRateAmt) Else 0.00 End As DAmt, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01*A.TaxRateAmt) End As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo1 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then (A.Amt*B.TaxRate*0.01*A.TaxRateAmt) Else 0.00 End As DAmt, ");
                    SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01*A.TaxRateAmt) End As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo1 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null ");
                }
            //}

            //Uang muka pembelian
            if (TxtDownPayment.Text.Length > 0 && decimal.Parse(TxtDownPayment.Text) != 0)
            {
                if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
                {
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        A.Downpayment As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoDownPayment' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Concat(C.ParValue, A.VdCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, B.ExcRate*B.Amt As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl5 B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='VendorAcNoDownPayment' And C.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
            }

            //Hutang Usaha
            if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                if (mIsPIJournalAPBasedOnCOATaxInd) // Custom IMS
                {
                    //COATaxInd='Y'
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        A.Amt+A.TaxAmt+IfNull(C.Amt, 0.00) As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Left Join (");
                    SQL.AppendLine("            Select A.DocNo, Sum(-1.00*DAmt)+Sum(CAmt) As Amt ");
                    SQL.AppendLine("            From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("            Inner Join TblPurchaseInvoiceDtl4 B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            Inner Join TblParameter C On C.ParCode='VendorAcNoAP' And C.ParValue Is Not Null ");
                    SQL.AppendLine("            Where A.DocNo=@DocNo ");
                    SQL.AppendLine("            And Concat(C.ParValue, A.VdCode)=B.AcNo ");
                    SQL.AppendLine("            Group By A.DocNo ");
                    SQL.AppendLine("        ) C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        And A.COATaxInd='Y' ");

                    //COATaxInd='N'
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        A.Amt+A.TaxAmt As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        And A.COATaxInd='N' ");
                }
                else
                {
                    if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
                    {
                        SQL.AppendLine("        Union All ");
                        SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        SQL.AppendLine("        A.Amt+A.TaxAmt As CAmt ");
                        SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Union All ");
                        SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        SQL.AppendLine("        A.Amt+A.TaxAmt-A.Downpayment As CAmt ");
                        SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                }
            }
            else
            {
                if (mIsPIJournalAPBasedOnCOATaxInd) // Custom IMS
                {
                    //COATaxInd='Y'
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                    SQL.AppendLine("        (A.Amt+A.TaxAmt+IfNull(C.Amt, 0.00)) As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Left Join (");
                    SQL.AppendLine("            Select A.DocNo, Sum(-1.00*DAmt)+Sum(CAmt) As Amt ");
                    SQL.AppendLine("            From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("            Inner Join TblPurchaseInvoiceDtl4 B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            Inner Join TblParameter C On C.ParCode='VendorAcNoAP' And C.ParValue Is Not Null ");
                    SQL.AppendLine("            Where A.DocNo=@DocNo ");
                    SQL.AppendLine("            And Concat(C.ParValue, A.VdCode)=B.AcNo ");
                    SQL.AppendLine("            Group By A.DocNo ");
                    SQL.AppendLine("        ) C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        And A.COATaxInd='Y' ");

                    //COATaxInd='N'
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                    SQL.AppendLine("        (A.Amt+A.TaxAmt) As CAmt ");
                    SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        And A.COATaxInd='N' ");
                }
                else
                {
                    if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
                    {
                        SQL.AppendLine("        Union All ");
                        SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00) * ");
                        SQL.AppendLine("        (A.Amt+A.TaxAmt) As CAmt ");
                        SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Union All ");
                        SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00) * ");
                        SQL.AppendLine("        (A.Amt+A.TaxAmt-A.Downpayment) As CAmt ");
                        SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                }
            }
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo, ");
            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
            }
            SQL.AppendLine("        B.DAmt, ");
            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
            }
            SQL.AppendLine("        B.CAmt ");
            SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl4 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='VendorAcNoAP' And C.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And Concat(C.ParValue, A.VdCode)<>B.AcNo ");

            //Laba rugi selisih kurs
            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");
            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            //SQL.AppendLine("Where A.DocNo=@JournalDocNo; ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl ");
                SQL.AppendLine("        Where DocNo In ( ");
                SQL.AppendLine("            Select JournalDocNo ");
                SQL.AppendLine("            From TblPurchaseInvoiceHdr ");
                SQL.AppendLine("            Where DocNo=@DocNo ");
                SQL.AppendLine("            And Status = 'A' ");
                SQL.AppendLine("            And JournalDocNo Is Not Null ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0.00 End, ");
                SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0.00 End ");
                SQL.AppendLine("Where A.DocNo In ( ");
                SQL.AppendLine("    Select JournalDocNo ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And Status = 'A' ");
                SQL.AppendLine("    And JournalDocNo Is Not Null ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");
            }
            else
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl ");
                SQL.AppendLine("        Where DocNo=@JournalDocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt=B.CAmt Then ");
                SQL.AppendLine("        A.DAmt Else ");
                SQL.AppendLine("        Case When Abs(B.DAmt-B.CAmt)<@PIRoundedMaxValIfJournalNotBalance Then ");
                SQL.AppendLine("            A.DAmt-(B.DAmt-B.CAmt) Else A.DAmt End ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoUnInvoiceAP' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        And A.Status = 'A' ");
                SQL.AppendLine("    );");
            }

            if (mCostCenterFormulaForAPAR == "2")
            {
                SQL.AppendLine("Update TblJournalHdr A ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr B On A.DocNo = B.JournalDocNo ");
                SQL.AppendLine("Inner Join TblParameter C On C.ParCode = 'CCCodeForJournalAPAR' And C.ParValue Is Not Null ");
                SQL.AppendLine("Set A.CCCode = C.ParValue ");
                SQL.AppendLine("Where B.DocNo = @DocNo ");
                SQL.AppendLine("    And A.CCCode Is Null ");
                SQL.AppendLine("    And Exists ( ");
                SQL.AppendLine("        Select 1 From TblJournalDtl ");
                SQL.AppendLine("        Where DocNo = A.DocNo And AcNo like '5.%' ");
                SQL.AppendLine("    ); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.CmParam<Decimal>(ref cm, "@PIRoundedMaxValIfJournalNotBalance", mPIRoundedMaxValIfJournalNotBalance == 0m ? 1m : mPIRoundedMaxValIfJournalNotBalance);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveOutgoingPaymentHdr(string OPDocNo, string VoucherRequestDocNo, 
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision
            )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblOutgoingPaymentHdr ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("Status, CancelInd, VoucherRequestDocNo, VoucherRequestSummaryInd, VoucherRequestSummaryDesc, ");
            SQL.AppendLine("VdCode, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, ");
            SQL.AppendLine("PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcName, PaidToBankAcNo,");
            SQL.AppendLine("CurCode, RateAmt, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@OPDocNo, @DocDt, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("'A', 'N', @VoucherRequestDocNo, 'N', null, ");
            SQL.AppendLine("@VdCode, @AcType, @BankAcCode, @PaymentType, @GiroNo, @BankCode, @DueDt, ");
            SQL.AppendLine("null, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcName, @PaidToBankAcNo,");
            SQL.AppendLine("@CurCode, @RateAmt, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@OPDocNo", OPDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            if (Sm.GetLue(LueDeptCode).Length == 0)
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetParameter("PPRDeptCode"));
            else
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDate2));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeDescription.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOutgoingPaymentDtl(string OPDocNo, string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblOutgoingPaymentDtl(DocNo, DNo, InvoiceDocno, InvoiceType, Amt, Remark, CreateBy, CreateDt) " +
                    "Values(@OPDocNo, '001', @InvoiceDocno, '1', @Amt, null, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@OPDocNo", OPDocNo);
            Sm.CmParam<String>(ref cm, "@InvoiceDocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, CurCode, CurCode2, ExcRate, Amt, PaymentUser, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VRDocNo, @DocDt, 'N', 'A', @MInd, @DeptCode, '03', Null, ");
            SQL.AppendLine("@AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("@PIC, @CurCode, @CurCode2, @ExcRate, @Amt, @PaymentUser, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@VRDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            if (Sm.GetLue(LueDeptCode).Length == 0)
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetParameter("PPRDeptCode"));
            else
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDate2));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@CurCode2", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string VoucherRequestDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@VRDocNo, '001', @Description, @Amt, Null, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@VRDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@Description", MeeDescription.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherHdr(string VoucherRequestDocNo, string VoucherDocNo)
        {
            var SQL = new StringBuilder();

            SQL.Append("Insert Into TblVoucherHdr (DocNo, DocDt, CancelInd, MInd, VoucherRequestDocNo, DocType, ");
            SQL.Append("CurCode, CurCode2, ExcRate, ");
            SQL.Append("AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, Amt, Remark, CreateBy, CreateDt) ");
            SQL.Append("Select @VoucherDocNo, DocDt, 'N', @MInd, @VoucherRequestDocNo, DocType, ");
            SQL.Append("CurCode, CurCode2, ExcRate, ");
            SQL.Append("AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, Amt, Remark, CreateBy, CreateDt ");
            SQL.Append("From TblVoucherRequestHdr Where DocNo=@VoucherRequestDocNo; ");

            SQL.Append("Update TblVoucherRequestHdr Set ");
            SQL.Append("    VoucherDocNo=@VoucherDocNo ");
            SQL.Append("Where DocNo=@VoucherRequestDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", VoucherDocNo);
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);

            return cm;
        }

        private MySqlCommand SaveVoucherDtl(string VoucherRequestDocNo, string VoucherDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Select @VoucherDocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt " +
                    "From TblVoucherRequestDtl Where DocNo=@VoucherRequestDocNo;"
            };
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", VoucherDocNo);

            return cm;
        }

        private MySqlCommand UpdatePIFile(string DocNo, string DNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPurchaseInvoiceDtl Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo = @Dno ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        #endregion

        #region Edit data

        private void EditData()
        {
            var EntCode = GetEntity();
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            if (IsVoucherRequestPPNExisted2() && !ChkCancelInd.Checked)
            {
                if ((mOldServiceCode1 != Sm.GetLue(LueServiceCode1)) ||
                    (mOldServiceCode2 != Sm.GetLue(LueServiceCode2)) ||
                    (mOldServiceCode3 != Sm.GetLue(LueServiceCode3))
                    )
                {
                    if (Sm.StdMsgYN("Question", "Due to this data has been processed to Voucher Request VAT, you can't change the Tax Service. Do you still want to proceed ?") == DialogResult.No) return;
                }

                if ((mOldServiceNote1 != MeeServiceNote1.Text) ||
                    (mOldServiceNote2 != MeeServiceNote2.Text) ||
                    (mOldServiceNote3 != MeeServiceNote3.Text)
                    )
                {
                    if (Sm.StdMsgYN("Question", "Due to this data has been processed to Voucher Request VAT, you can't change the Service's Note. Do you still want to proceed ?") == DialogResult.No) return;
                }
            }

            Cursor.Current = Cursors.WaitCursor;

            var lDepositSummary = new List<DepositSummary>();
            if (ChkCancelInd.Checked && 
                decimal.Parse(TxtDownPayment.Text) != 0 && 
                !Sm.CompareStr(TxtCurCode.Text, mMainCurCode)
                )
                GetDepositSummary(ref lDepositSummary);
            
            var cml = new List<MySqlCommand>();

            cml.Add(EditPurchaseInvoiceHdr());

            cml.Add(UpdateVRVATFactur());

            if (ChkCancelInd.Checked && 
                decimal.Parse(TxtDownPayment.Text) != 0)
            {
                cml.Add(SaveVendorDeposit(TxtDocNo.Text, EntCode));
                if (lDepositSummary.Count > 0)
                {
                    for (int i = 0; i < lDepositSummary.Count; i++)
                        cml.Add(SaveVendorDepositSummary2(lDepositSummary[i], EntCode));
                }
            }

            cml.Add(DeletePurchaseInvoiceDtl3());
            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SavePurchaseInvoiceDtl3(TxtDocNo.Text, Row));
            }

            if (mIsAutoJournalActived && ChkCancelInd.Checked) cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsGrd3ValueNotValid() ||
                IsPurchaseReturnInvoiceExisted() ||
                (ChkCancelInd.Checked && IsDataAlreadyCancelled()) ||
                (ChkCancelInd.Checked && IsOutgoingPaymentExisted()) ||
                (ChkCancelInd.Checked && IsAPSExisted()) ||
                (ChkCancelInd.Checked && IsVoucherRequestPPNExisted()) ||
                (ChkCancelInd.Checked && IsVATSettlementExisted()) ||
                IsTaxInfoInvalid() ||
                IsPIProcessedToPIPPH() ||
                IsPIProcessedToAPSettlement();
        }

        private bool IsPIProcessedToAPSettlement()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblAPShdr A ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.PurchaseInvoiceDocNo = @Param ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to AP Settlement #" + Sm.GetValue(SQL.ToString(), TxtDocNo.Text) + ".");
                return true;
            }

            return false;
        }

        private bool IsPIProcessedToPIPPH()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblPIPPHDtl A ");
            SQL.AppendLine("Inner Join TblPIPPHHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("    And A.PIDocNo = @Param ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to Purchase Invoice PPH #" + Sm.GetValue(SQL.ToString(), TxtDocNo.Text) + ".");
                return true;
            }

            return false;
        }

        private bool IsGrd3ValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 2, false, "Document type is empty.")) return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblPurchaseInvoiceHdr " +
                "Where (CancelInd='Y' Or Status = 'C') And DocNo=@Param; ",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private bool IsOutgoingPaymentExisted()
        {
            return Sm.IsDataExist(
                "Select 1 " +
                "From TblOutgoingPaymentHdr A " +
                "Inner Join TblOutgoingPaymentDtl B " +
                "   On A.DocNo=B.DocNo " +
                "   And B.InvoiceType='1' " +
                "   And B.InvoiceDocNo=@Param " +
                "Where A.CancelInd='N' And A.Status<>'C' Limit 1;",
                TxtDocNo.Text, 
                "This document already processed to outgoing payment."
                );
        }

        private bool IsAPSExisted()
        {
            return Sm.IsDataExist(
                    "Select 1 From TblAPSHdr " +
                    "Where PurchaseInvoiceDocNo=@Param And CancelInd='N' " +
                    "Limit 1;",
                    TxtDocNo.Text, 
                    "This document already processed to AP settlement.");       
        }

        private bool IsVoucherRequestPPNExisted()
        {
            if (Sm.IsDataExist(
                "SELECT 1 FROM TblPurchaseInvoiceHdr " +
                "WHERE DocNo=@Param " +
                "And (VoucherRequestPPNDocNo Is Not Null Or VoucherRequestPPNDocNo2 Is Not Null Or VoucherRequestPPNDocNo3 Is Not Null);", 
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Voucher Request VAT.");
                return true;
            }

            return false;
        }

        private bool IsVoucherRequestPPNExisted2()
        {
            return (Sm.IsDataExist(
                "SELECT 1 FROM TblPurchaseInvoiceHdr " +
                "WHERE DocNo=@Param " +
                "And (VoucherRequestPPNDocNo Is Not Null Or VoucherRequestPPNDocNo2 Is Not Null Or VoucherRequestPPNDocNo3 Is Not Null);",
                TxtDocNo.Text));
        }

        private bool IsVATSettlementExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblPurchaseInvoiceHdr " +
                "Where DocNo=@Param " +
                "And (VATSettlementDocNo Is Not Null Or VATSettlementDocNo2 Is Not Null Or VATSettlementDocNo3 Is Not Null);",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to VAT settlement.");
                return true;
            }
            return false;
        }

        private bool IsPurchaseReturnInvoiceExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOVdDtl C ");
            SQL.AppendLine("    On B.RecvVdDocNo=C.RecvVdDocNo ");
            SQL.AppendLine("    And B.RecvVdDNo=C.RecvVdDNo ");
            SQL.AppendLine("Inner Join TblPurchaseReturnInvoiceDtl D ");
            SQL.AppendLine("    On C.DocNo=D.DOVdDocNo ");
            SQL.AppendLine("    And C.DNo=D.DOVdDNo ");
            SQL.AppendLine("Inner Join TblPurchaseReturnInvoiceHdr E On D.DocNo=E.DocNo And E.CancelInd='N' ");
            SQL.AppendLine("Where A.DocNo=@Param Limit 1;");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to purchase return invoice.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditPurchaseInvoiceHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason=@CancelReason, ");
            SQL.AppendLine("    TaxInvoiceNo = @TaxInvoiceNo, TaxInvoiceDt = @TaxInvoiceDt, ");
            SQL.AppendLine("    TaxInvoiceNo2 = @TaxInvoiceNo2, TaxInvoiceDt2 = @TaxInvoiceDt2, ");
            SQL.AppendLine("    TaxInvoiceNo3 = @TaxInvoiceNo3, TaxInvoiceDt3 = @TaxInvoiceDt3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            if (mIsPIUseQRCodeInvoice && ChkCancelInd.Checked)
            {
                SQL.AppendLine("Update TblAPDownpaymentDtl2 Set ");
                SQL.AppendLine("    PurchaseInvoiceDocNo=Null ");
                SQL.AppendLine("Where PurchaseInvoiceDocNo=@DocNo ");
                SQL.AppendLine("And PurchaseInvoiceDocNo Is Not Null; ");
            }

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    ServiceCode1 = @ServiceCode1, ServiceCode2 = @ServiceCode2, ServiceCode3 = @ServiceCode3, ");
            SQL.AppendLine("    ServiceNote1 = @ServiceNote1, ServiceNote2 = @ServiceNote2, ServiceNote3 = @ServiceNote3 ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And (VoucherRequestPPNDocNo Is Null And VoucherRequestPPNDocNo2 Is Null And VoucherRequestPPNDocNo3 Is Null); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<String>(ref cm, "@ServiceCode1", Sm.GetLue(LueServiceCode1));
            Sm.CmParam<String>(ref cm, "@ServiceCode2", Sm.GetLue(LueServiceCode2));
            Sm.CmParam<String>(ref cm, "@ServiceCode3", Sm.GetLue(LueServiceCode3));
            Sm.CmParam<String>(ref cm, "@ServiceNote1", MeeServiceNote1.Text);
            Sm.CmParam<String>(ref cm, "@ServiceNote2", MeeServiceNote2.Text);
            Sm.CmParam<String>(ref cm, "@ServiceNote3", MeeServiceNote3.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateVRVATFactur()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestPPNDtl Set ");
            SQL.AppendLine("    TaxInvoiceNo =   ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode1 Then @TaxInvoiceNo ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode2 Then @TaxInvoiceNo2 ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode3 Then @TaxInvoiceNo3 ");
            SQL.AppendLine("    End, ");
            SQL.AppendLine("    TaxInvoiceDt =  ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode1 Then @TaxInvoiceDt ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode2 Then @TaxInvoiceDt2 ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode3 Then @TaxInvoiceDt3 ");
            SQL.AppendLine("    Else Null End, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNoIn=@DocNo And DocType = '1'; ");

            SQL.AppendLine("Update TblVATSettlementDtl Set ");
            SQL.AppendLine("    TaxInvoiceNo =   ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode1 Then @TaxInvoiceNo ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode2 Then @TaxInvoiceNo2 ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode3 Then @TaxInvoiceNo3 ");
            SQL.AppendLine("    End, ");
            SQL.AppendLine("    TaxInvoiceDt =  ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode1 Then @TaxInvoiceDt ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode2 Then @TaxInvoiceDt2 ");
            SQL.AppendLine("    When IfNull(TaxCode, '') = @TaxCode3 Then @TaxInvoiceDt3 ");
            SQL.AppendLine("    Else Null End, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNoInOut=@DocNo And DocType = '1'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeletePurchaseInvoiceDtl3()
        {
            var cm = new MySqlCommand() 
            { CommandText = "Delete From TblPurchaseInvoiceDtl3 Where DocNo=@DocNo;" };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine(");");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            else
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowPurchaseInvoiceHdr(DocNo);
                ShowPurchaseInvoiceDtl(DocNo);
                ShowPurchaseInvoiceDtl2(DocNo);
                ShowPurchaseInvoiceDtl3(DocNo);
                ShowPurchaseInvoiceDtl4(DocNo);
                ShowPurchaseInvoiceDtl5(DocNo);
                ShowPurchaseInvoiceDtl6(DocNo);
                ComputeAmt();
                if (mPIQRCodeTaxDocType.Length > 0) SetQRCodeInfo();
                if (mIsUseMInd == true && mMInd == "Y") ShowDataVoucher(DocNo);
                if (mIsPIAmtRoundOff) TxtAmt.EditValue = Sm.FormatNum(mAmt, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPurchaseInvoiceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("A.VdCode, A.VdInvNo, A.VdInvDt, A.PaymentType, A.DueDt, A.CurCode, ");
            SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceDt, A.TaxInvoiceNo2, A.TaxInvoiceDt2, A.TaxInvoiceNo3, A.TaxInvoiceDt3, A.TaxCurCode, ");
            SQL.AppendLine("A.TaxRateAmt, A.TaxCode1, A.TaxCode2, A.TaxCode3, A.TaxAmt, A.DownPayment, A.DeptCode, A.SiteCode, B.SiteName, A.COATaxInd, A.Remark, A.TaxAlias1, A.TaxAlias2, A.TaxAlias3, A.Amt, ");
            SQL.AppendLine("A.ServiceCode1, A.ServiceCode2, A.ServiceCode3, A.ServiceNote1, A.ServiceNote2, A.ServiceNote3, A.PaymentDt, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "LocalDocNo", "DocDt",  "CancelReason", "CancelInd", "VdCode", 
                        
                        //6-10
                        "VdInvNo", "VdInvDt", "PaymentType", "DueDt", "CurCode", 
                        
                        //11-15
                        "TaxInvoiceNo", "TaxInvoiceDt", "TaxInvoiceNo2", "TaxInvoiceDt2", "TaxInvoiceNo3", 
                        
                        //16-20
                        "TaxInvoiceDt3", "TaxCurCode", "TaxRateAmt", "TaxCode1", "TaxCode2", 
                        
                        //21-25
                        "TaxCode3", "DownPayment", "DeptCode", "SiteCode", "SiteName", 
                        
                        //26-30
                        "COATaxInd", "Remark", "Taxalias1", "Taxalias2", "Taxalias3",

                        //31-35
                        "Amt", "ServiceCode1", "ServiceCode2", "ServiceCode3", "ServiceNote1", 

                        //36-39
                        "ServiceNote2", "ServiceNote3", "PaymentDt", "StatusDesc"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        SetLueVdCode(ref LueVdCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[5]));
                        TxtVdInvNo.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetDte(DteVdInvDt, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[8]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[9]));
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[10]);
                        TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[12]));
                        TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[13]);
                        Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[14]));
                        TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[15]);
                        Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueTaxCurCode, Sm.DrStr(dr, c[17]));
                        TxtTaxRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[19]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[20]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[21]));
                        TxtDownPayment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 0);
                        SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[23]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[23]));
                        mSiteCode = Sm.DrStr(dr, c[24]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[25]);
                        ChkCOATaxInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[26]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[27]);
                        TxtAlias1.EditValue = Sm.DrStr(dr, c[28]);
                        TxtAlias2.EditValue = Sm.DrStr(dr, c[29]);
                        TxtAlias3.EditValue = Sm.DrStr(dr, c[30]);
                        mAmt = Sm.DrDec(dr, c[31]);
                        Sm.SetLue(LueServiceCode1, Sm.DrStr(dr, c[32]));
                        Sm.SetLue(LueServiceCode2, Sm.DrStr(dr, c[33]));
                        Sm.SetLue(LueServiceCode3, Sm.DrStr(dr, c[34]));
                        mOldServiceCode1 = Sm.DrStr(dr, c[32]);
                        mOldServiceCode2 = Sm.DrStr(dr, c[33]);
                        mOldServiceCode3 = Sm.DrStr(dr, c[34]);
                        MeeServiceNote1.EditValue = Sm.DrStr(dr, c[35]);
                        MeeServiceNote2.EditValue = Sm.DrStr(dr, c[36]);
                        MeeServiceNote3.EditValue = Sm.DrStr(dr, c[37]);
                        Sm.SetDte(DtePaymentDt, Sm.DrStr(dr, c[38]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[39]);
                    }, true
                );
        }

        private void ShowPurchaseInvoiceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.RecvVdDocNo, A.RecvVdDNo, B2.VdDONo, ifnull(P.MRDocNo, B.PODocNo) PODocNo, B.ItCode, I.ItName, B.QtyPurchase, I.PurchaseUomCode, B2.DocDt AS RecvVdDocDt, ");
            SQL.AppendLine("J.PtName, G.CurCode, H.UPrice, D.Discount, ((B.QtyPurchase/D.Qty)*D.DiscountAmt) As DiscountAmt, D.RoundingValue, ");
            SQL.AppendLine("(((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue )*Case When IfNull(K.TaxRate, 0)=0 Then 0 Else K.TaxRate/100 End) As TaxAmt1, ");
            SQL.AppendLine("(((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue )*Case When IfNull(L.TaxRate, 0)=0 Then 0 Else L.TaxRate/100 End) As TaxAmt2, ");
            SQL.AppendLine("(((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue )*Case When IfNull(M.TaxRate, 0)=0 Then 0 Else M.TaxRate/100 End) As TaxAmt3,  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    ((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue) ");
            SQL.AppendLine(") As Total, N.DTName, A.Remark, IfNull(I.ItScCode, 'XXX') As ItScCode, ifnull(O.ItScName, 'XXX') As ItScName, F.Remark As RemarkMR, B2.LocalDocNo, ");
            SQL.AppendLine("I.ForeignName, P.MaterialRequestLocalDocNo, IfNull(Q.APDownpaymentAmt, 0.00) As APDownpaymentAmt, A.FileName, I.ItCodeInternal, I.Specification, ");
            SQL.AppendLine("IfNull(F2.DocNo, B.PODocNo) PODocNoService ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
            SQL.AppendLine("Inner Join TblRecvVdhdr B2 On A.RecvVdDocNo=B2.DocNo ");
            SQL.AppendLine("Inner Join TblPOHdr C On B.PODocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
            SQL.AppendLine("Left Join TblMaterialRequestDtl F2 On E.MaterialRequestDocNo=F2.DocNo And E.MaterialRequestDNo=F2.DNo And F2.MaterialRequestServiceDocNo is Not Null ");
            SQL.AppendLine("Inner Join TblQtHdr G On E.QtDocNo=G.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
            SQL.AppendLine("Inner Join TblItem I On B.ItCode=I.ItCode ");
            SQL.AppendLine("Left Join TblPaymentTerm J On G.PtCode=J.PtCode ");
            SQL.AppendLine("Left Join TblTax K On C.TaxCode1=K.TaxCode ");
            SQL.AppendLine("Left Join TblTax L On C.TaxCode2=L.TaxCode ");
            SQL.AppendLine("Left Join TblTax M On C.TaxCode3=M.TaxCode ");
            SQL.AppendLine("Left Join TblDeliveryType N On G.DTCode=N.DTCode ");
            SQL.AppendLine("Left Join TblItemSubcategory O On I.ItScCOde = O.ItScCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, Group_Concat(Distinct T5.LocalDocNo Separator ', ') As MaterialRequestLocalDocNo, T5.DocNo MRDocNo ");
            SQL.AppendLine("    From TblRecvVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("        And Concat(T2.DocNo, T2.DNo ) In (");
            SQL.AppendLine("        Select Concat(RecvVdDocNo, RecvVdDNo) From TblPurchaseInvoiceDtl Where DocNo=@DocNo");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Inner Join TblPODtl T3 On T2.PODocNo=T3.DocNo And T2.PODNo=T3.DNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T4 On T3.PORequestDocNo=T4.DocNo And T3.PORequestDNo=T4.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr T5 On T4.MaterialRequestDocNo=T5.DocNo And T5.LocalDocNo Is Not Null ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") P On A.RecvVdDocNo=P.DocNo And A.RecvVdDNo=P.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PODocNo, Sum(T1.Amt) As APDownpaymentAmt ");
            SQL.AppendLine("    From TblAPDownpayment T1 ");
            SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    And T1.PODocNo In ( ");
            SQL.AppendLine("        Select Distinct X2.PODocNo ");
            SQL.AppendLine("        From TblPurchaseInvoiceDtl X1 ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl X2 On X1.RecvVdDocNo=X2.DocNo And X1.RecvVdDNo=X2.DNo ");
            SQL.AppendLine("        Where X1.DocNo=@DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T1.PODocNo ");
            SQL.AppendLine(") Q On B.PODocNo=Q.PODocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
                    
                    //1-5
                    "RecvVdDocNo", "RecvVdDNo", "PODocNo", "ItCode", "ItName", 
        
                    //6-10
                    "QtyPurchase", "PurchaseUomCode", "PtName", "CurCode", "UPrice", 
        
                    //11-15
                    "Discount", "DiscountAmt", "RoundingValue", "TaxAmt1", "TaxAmt2", 

                    //16-20
                    "TaxAmt3", "Total", "DTName", "Remark", "ItScCode",

                    //21-25
                    "ItScName", "RemarkMR", "LocalDocNo", "RecvVdDocDt", "ForeignName",

                    //26-30
                    "VDDONo", "MaterialRequestLocalDocNo", "APDownpaymentAmt", "FileName", "ItCodeInternal",

                    //31-32
                    "Specification", "PODocNoService"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                    Grd.Cells[Row, 28].Value = mSiteCode;
                    Grd.Cells[Row, 29].Value = TxtSiteCode.EditValue;
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 30, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 27);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 31);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 32);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 14, 15, 16, 17, 18, 19, 20, 21, 34 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPurchaseInvoiceDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();


            SQL.AppendLine("Select A.DNo, A.PODocNo, A.AmtType, B.OptDesc, A.CurCode, A.Amt, A.Remark ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl2 A ");
            SQL.AppendLine("Inner Join TblOption B On A.AmtType=B.OptCode And B.OptCat='InvoiceAmtType' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
                    
                    //1-5
                    "PODocNo", "AmtType", "OptDesc", "CurCode", "Amt", 
                    
                    //6
                    "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Grd.Cells[Row, 7].Value = Sm.FormatNum(0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 7, 8 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowPurchaseInvoiceDtl3(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DocType, B.OptDesc As DocTypeDesc, A.DocNumber, A.Amt, A.TaxAmt, A.DocInd, C.OptDesc As DocIndDesc, A.TaxInvDt, ");
            SQL.AppendLine("A.QRCode, A.Remark, A.RemarkXml, A.APDownpaymentDocNo, A.APDownpaymentDNo, A.DigitalInvoiceDocNo, A.DigitalInvoiceDNo, ");
            SQL.AppendLine("D.File ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl3 A ");
            SQL.AppendLine("Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='PurchaseInvoiceDocType' ");
            SQL.AppendLine("Left Join TblOption C On A.DocInd=C.OptCode And C.OptCat='PurchaseInvoiceDocInd' ");
            SQL.AppendLine("Left Join TblDigitalInvoiceDtl D On A.DigitalInvoiceDocNo = D.DocNo And A.DigitalInvoiceDNo = D.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
                    //1-5
                    "DocType", "DocTypeDesc", "QRCode", "DocNumber", "TaxInvDt",  
                    //6-10
                    "Amt", "TaxAmt", "DocInd",  "DocIndDesc", "Remark", 
                    //11-15
                    "RemarkXml", "APDownpaymentDocNo", "APDownpaymentDNo", "DigitalInvoiceDocNo", "DigitalInvoiceDNo",
                    //16
                    "File"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 7, 8 });
            Sm.FocusGrd(Grd3, 0, 2);
        }

        private void ShowPurchaseInvoiceDtl4(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl4 A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", "AcDesc", "DAmt", "CAmt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowPurchaseInvoiceDtl5(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.EntCode, B.EntName, A.CurCode, A.Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl6 A, TblEntity B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.EntCode=B.EntCode Order By B.EntName;");

            Sm.ShowDataInGrid(
                ref Grd5, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "EntCode", "EntName", "CurCode", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ShowPurchaseInvoiceDtl6(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.APDownpaymentDocNo, A.POBefTax, A.Downpayment, ");
            SQL.AppendLine("B.TaxName, A.TaxAmt, C.TaxName As TaxName2, A.TaxAmt2, D.TaxName As TaxName3, A.TaxAmt3, ");
            SQL.AppendLine("A.TaxCode, A.TaxCode2, A.TaxCode3 ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl7 A ");
            SQL.AppendLine("Left join TblTax B On A.TaxCode=B.TaxCode ");
            SQL.AppendLine("Left join TblTax C On A.TaxCode2=C.TaxCode ");
            SQL.AppendLine("Left join TblTax D On A.TaxCode3=D.TaxCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.APDownpaymentDocNo;");

            Sm.ShowDataInGrid(
                ref Grd6, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "APDownpaymentDocNo", 

                    //1-5
                    "POBefTax", 
                    "Downpayment", 
                    "TaxName", 
                    "TaxAmt", 
                    "TaxName2", 

                    //6-10
                    "TaxAmt2", 
                    "TaxName3", 
                    "TaxAmt3", 
                    "TaxCode", 
                    "TaxCode2", 

                    //11
                    "TaxCode3"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 2, 3, 5, 7, 9 });
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ShowVendorDepositSummary(string VdCode)
        {
            ClearGrd5();

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.EntCode, B.EntName, A.CurCode, A.Amt-IfNull(C.Amt, 0) As Amt ");
            SQL.AppendLine("From TblVendorDepositSummary A ");
            SQL.AppendLine("Left Join TblEntity B On A.EntCode=B.EntCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select IfNull(T1.EntCode, '') As EntCode, T1.CurCode, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblReturnAPDownpayment T1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr T2 ");
            SQL.AppendLine("        On T1.VoucherRequestDocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status<>'C' ");
            SQL.AppendLine("        And T2.VoucherDocNo Is Null ");
            SQL.AppendLine("    Where T1.VdCode=@VdCode And T1.Status<>'C' And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By IfNull(T1.EntCode, ''), T1.CurCode ");
            SQL.AppendLine(") C On IfNull(A.EntCode, '')=IfNull(C.EntCode, '') And A.CurCode=C.CurCode ");
            SQL.AppendLine("Where A.VdCode=@VdCode ");
            SQL.AppendLine("Order By A.CurCode;");

            Sm.ShowDataInGrid(
                ref Grd5, ref cm, SQL.ToString(),
                new string[]{ "EntCode", "EntName", "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ShowAPDownpayment()
        {
            if (!mIsPurchaseInvoiceShowAPDownpayment2) return;

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.AmtBefTax, A.Amt, ");
            SQL.AppendLine("C.TaxName, A.TaxAmt, ");
            SQL.AppendLine("D.TaxName As TaxName2, A.TaxAmt2, ");
            SQL.AppendLine("E.TaxName As TaxName3, A.TaxAmt3, ");
            SQL.AppendLine("A.TaxCode, A.TaxCode2, A.TaxCode3 ");
            SQL.AppendLine("From TblAPDownpayment A ");
            SQL.AppendLine("Inner Join TblPOHdr B On A.PODocNo=B.DocNo And B.VdCode=@VdCode ");
            SQL.AppendLine("Left Join TblTax C On A.TaxCode=C.TaxCode ");
            SQL.AppendLine("Left Join TblTax D On A.TaxCode2=D.TaxCode ");
            SQL.AppendLine("Left Join TblTax E On A.TaxCode3=E.TaxCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status In ('O', 'A') ");
            if (TxtDocNo.Text.Length > 0)
            {
                SQL.AppendLine("And A.PODocNo In (");
                SQL.AppendLine("    Select Distinct T2.PODocNo ");
                SQL.AppendLine("    From TblPurchaseInvoiceDtl T1, TblRecvVdDtl T2 ");
                SQL.AppendLine("    Where T1.RecvVdDocNo=T2.DocNo ");
                SQL.AppendLine("    And T1.RecvVdDNo=T2.DNo ");
                SQL.AppendLine("    And T1.DocNo=@DocNo ");
                SQL.AppendLine("    ) ");

                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            }
            else
            {
                SQL.AppendLine("And A.PODocNo Not In (");
                SQL.AppendLine("    Select Distinct T3.PODocNo ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl T2, TblRecvVdDtl T3 ");
                SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    And T2.RecvVdDocNo=T3.DocNo ");
                SQL.AppendLine("    And T2.RecvVdDNo=T3.DNo ");
                SQL.AppendLine("    And T1.CancelInd='N' ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd6, ref cm, SQL.ToString(),
                new string[] { 
                    "DocNo", 
                    "AmtBefTax", "Amt", "TaxName", "TaxAmt", "TaxName2", 
                    "TaxAmt2", "TaxName3", "TaxAmt3", "TaxCode", "TaxCode2", 
                    "TaxCode3" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 2, 3, 5, 7, 9 });
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ShowDataVoucher(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcType, A.BankAcCode, A.PaymentType,  A.GiroNo, A.BankCOde, ");
            SQL.AppendLine("A.DueDt, A.CurCode, A.RateAmt, A.Amt, A.Remark, A.DocNo, C.DocNo As VRDocNo, C.VoucherDocNo, C.PIC  ");
            SQL.AppendLine("from TblOutgoingPaymentHDr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr C ON A.VoucherRequestDocno = C.DocNo ");
            SQL.AppendLine("Where B.InvoiceDocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "AcType",

                        //1-5
                        "BankAcCode", "PaymentType", "GiroNo", "BankCOde", "DueDt", 
                        
                        //6-10
                        "CurCode", "RateAmt", "Amt", "Remark", "DocNo", 
                        
                        //11-15
                        "VRDocNo", "VoucherDocNo", "PIC", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Sm.SetLue(LueAcType, Sm.DrStr(dr, c[0]));
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LuePaymentType2, Sm.DrStr(dr, c[2]));
                        TxtGiroNo.EditValue = Sm.DrStr(dr, c[3]);
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[4]));
                        Sm.SetDte(DteDueDate2, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[6]));
                        TxtRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        decimal AmtTot = (Sm.DrDec(dr, c[7]) * Sm.DrDec(dr, c[8]));
                        TxtAmt2.EditValue = Sm.FormatNum(AmtTot, 0);
                        MeeDescription.EditValue = Sm.DrStr(dr, c[9]);
                        TxtOPDocNo.EditValue = Sm.DrStr(dr, c[10]);
                        TxtVRDocNo.EditValue = Sm.DrStr(dr, c[11]);
                        TxtVCDocNo.EditValue = Sm.DrStr(dr, c[12]);
                        Sm.SetLue(LuePIC, Sm.DrStr(dr, c[13]));
                        MeeDescription.EditValue = Sm.DrStr(dr, c[14]);
                    }, false
                );
        }


        #endregion

        #region Additional Method

        private void CheckRejectedItem()
        {
            string RecvNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if(Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (RecvNo.Length > 0) RecvNo += ",";
                    RecvNo += Sm.GetGrdStr(Grd1, i, 2) + Sm.GetGrdStr(Grd1, i, 3);
                }
            }

            if (RecvNo.Length > 0)
            {
                var l = new List<RejectedRecv>();

                PrepData(ref l, RecvNo);
                if (l.Count > 0) ProcessRejectedData(ref l);

                l.Clear();
            }
        }

        private void PrepData(ref List<RejectedRecv> l, string RecvNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, DNo, RejectedInd ");
            SQL.AppendLine("From TblRecvVdDtl ");
            SQL.AppendLine("Where Find_In_Set(Concat(DocNo, DNo), @RecvNo) ");
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@RecvNo", RecvNo);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    "DocNo",
                    "DNo",
                    "RejectedInd"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RejectedRecv()
                        {
                            RecvVdDocNo = Sm.DrStr(dr, c[0]),
                            RecvVdDNo = Sm.DrStr(dr, c[1]),
                            RejectedInd = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessRejectedData(ref List<RejectedRecv> l)
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    foreach (var x in l.Where(w => w.RecvVdDocNo == Sm.GetGrdStr(Grd1, i, 2) && w.RecvVdDNo == Sm.GetGrdStr(Grd1, i, 3)))
                    {
                        Grd1.Cells[i, 40].Value = x.RejectedInd;
                        break;
                    }
                }
            }
        }

        private bool IsPurchaseInvoiceUseApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocapprovalSetting Where DocType = 'PurchaseInvoice' Limit 1; ");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void Grd3RemoveRow(iGrid Grd, KeyEventArgs e, DXE.SimpleButton Btn)
        {
            if (Btn.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd.SelectedRows.Count > 0)
                {
                    if (Grd.Rows[Grd.Rows[Grd.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                if (Sm.GetGrdStr(Grd, Grd.SelectedRows[Index].Index, 18).Length <= 0)
                                {
                                    Grd.Rows.RemoveAt(Grd.SelectedRows[Index].Index);
                                    
                                }
                                else Sm.StdMsg(mMsgType.Warning, "You cannot delete this data. (Source : digital invoice)");
                            }
                            if (Grd.Rows.Count <= 0) Grd.Rows.Add();
                        }
                    }
                }
            }
        }

        internal void GetDigitalInvoice()
        {
            string mRecvVdDocNo = string.Empty;

            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if(Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                    {
                        if (mRecvVdDocNo.Length > 0) mRecvVdDocNo += ",";
                        mRecvVdDocNo += Sm.GetGrdStr(Grd1, i, 2);
                    }
                }
            }

            ClearGrd3();
            Grd3.ReadOnly = false;

            if(mRecvVdDocNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.DocType, C.OptDesc As DocTypeDesc, A.DocNumber, A.DocInd, D.OptDesc As DocIndDesc, A.TaxInvDt, A.Amt, A.TaxAmt, A.Remark, A.DocNo, A.DNo, A.File ");
                SQL.AppendLine("From TblDigitalInvoiceDtl A ");
                SQL.AppendLine("Inner Join TblDigitalInvoiceHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And B.CancelInd = 'N' ");
	            SQL.AppendLine("    And Find_In_Set(B.RecvVdDocNo, @RecvVdDocNo) ");
                SQL.AppendLine("Left Join TblOption C On A.DocType = C.OptCode And C.OptCat = 'PurchaseInvoiceDocType' ");
                SQL.AppendLine("Left Join TblOption D On A.DocInd = D.OptCode And D.OptCat = 'PurchaseInvoiceDocInd' ");
                SQL.AppendLine("Order By A.DocNo, A.DNo; ");

                Sm.CmParam<String>(ref cm, "@RecvVdDocNo", mRecvVdDocNo);

                Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocType",
                        
                        //1-5
                        "DocTypeDesc", "DocNumber", "TaxInvDt", "Amt", "TaxAmt", 
                        
                        //6-10
                        "DocInd", "DocIndDesc", "Remark", "DocNo", "DNo", 
                        
                        //11
                        "File"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 10);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 7, 8 });
                Sm.FocusGrd(Grd3, 0, 2);

                for (int i = 0; i < Grd3.Rows.Count - 1; i++)
                {
                    Grd3.Cells[i, 1].ReadOnly = Grd3.Cells[i, 2].ReadOnly = Grd3.Cells[i, 5].ReadOnly =
                    Grd3.Cells[i, 6].ReadOnly = Grd3.Cells[i, 7].ReadOnly = Grd3.Cells[i, 8].ReadOnly = 
                    Grd3.Cells[i, 9].ReadOnly = Grd3.Cells[i, 10].ReadOnly = Grd3.Cells[i, 11].ReadOnly = 
                    Grd3.Cells[i, 16].ReadOnly = Grd3.Cells[i, 18].ReadOnly = Grd3.Cells[i, 19].ReadOnly = iGBool.True;
                }
            }
        }

        private void ProcessRoundOff()
        {
            decimal Amt = decimal.Parse(TxtAmt.Text);
            decimal Value = Amt - decimal.Truncate(Amt);

            if (Value == 0m) return;

            int r = Grd4.Rows.Count - 1;
            
            Grd4.Cells[r, 1].Value = mAcNoForRoundingCost;
            Grd4.Cells[r, 2].Value = Sm.GetValue("Select AcDesc From TblCOA Where AcNo=@Param;", mAcNoForRoundingCost);
            Grd4.Cells[r, 3].Value = Value;
            Grd4.Cells[r, 4].Value = 0m;
            Grd4.Cells[r, 5].Value = "Inserted automatically by system";

            Grd4.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd4, 0, 1);

            TxtTotalWithoutTax.EditValue = Sm.FormatNum(decimal.Truncate(Amt), 0);
        }

        private bool IsDeptInvalid()
        {
            LueDeptCode.EditValue = null;
            
            if (Grd1.Rows.Count <= 1) return false;

            var cm = new MySqlCommand();
            string DocNo = string.Empty, DNo = string.Empty, Filter = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                DocNo = Sm.GetGrdStr(Grd1, r, 2);
                DNo = Sm.GetGrdStr(Grd1, r, 3);

                if (DocNo.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.DocNo=@DocNo" + r.ToString() + " And A.DNo=@DNo" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DocNo" + r.ToString(), DocNo);
                    Sm.CmParam<String>(ref cm, "@DNo" + r.ToString(), DNo);
                }
            }

            if (Filter.Length == 0) return false;

            Filter = " Where ( " + Filter + ") ";

            var SQL = new StringBuilder();
            bool IsFirst = true;
            string 
                PODocNo = string.Empty, 
                ItName = string.Empty, 
                DeptCode = string.Empty, 
                DeptName = string.Empty;

            DocNo = string.Empty;

            SQL.AppendLine("Select D.DeptCode, A.DocNo, A.PODocNo, G.ItName, E.DeptName ");
            SQL.AppendLine("From TblRecvVdDtl A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblDepartment E On D.DeptCode=E.DeptCode ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl F On C.MaterialRequestDocNo=F.DocNo And C.MaterialRequestDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblItem G On F.ItCode=G.ItCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By D.DeptCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DeptCode", 
                    "DocNo", 
                    "PODocNo", 
                    "ItName", 
                    "DeptName" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                        {
                            IsFirst = false;
                            DeptCode = Sm.DrStr(dr, c[0]);
                            DocNo = Sm.DrStr(dr, c[1]);
                            PODocNo = Sm.DrStr(dr, c[2]);
                            ItName = Sm.DrStr(dr, c[3]);
                            DeptName = Sm.DrStr(dr, c[4]);
                        }
                        else
                        {
                            if (!Sm.CompareStr(DeptCode, Sm.DrStr(dr, c[0])))
                            {
                                Sm.StdMsg(
                                    mMsgType.Warning,
                                    "1." + Environment.NewLine +
                                    "Received# : " + DocNo + Environment.NewLine +
                                    "PO# : " + PODocNo + Environment.NewLine +
                                    "Item : " + ItName + Environment.NewLine +
                                    "Department : " + DeptName + Environment.NewLine + Environment.NewLine +

                                    "2." + Environment.NewLine +
                                    "Received# : " + Sm.DrStr(dr, c[1]) + Environment.NewLine +
                                    "PO# : " + Sm.DrStr(dr, c[2]) + Environment.NewLine +
                                    "Item : " + Sm.DrStr(dr, c[3]) + Environment.NewLine +
                                    "Department : " + Sm.DrStr(dr, c[4]) + Environment.NewLine + Environment.NewLine +

                                    "One invoice should be processed from one department."
                                    );
                                dr.Close();
                                return true;
                            }
                        }
                    }
                }
                dr.Close();
                if (DeptCode.Length > 0)
                {
                    Sm.SetLue(LueDeptCode, DeptCode);
                    return false;
                }
                Sm.StdMsg(mMsgType.Warning, "Department is empty.");
                return true;    
            }



            //if (Grd1.Rows.Count > 1)
            //{
            //    string mPODocNo = string.Empty;

            //    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    {
            //        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
            //        {
            //            if (mPODocNo.Length > 0) mPODocNo += ",";
            //            mPODocNo += Sm.GetGrdStr(Grd1, Row, 5);
            //        }
            //    }

            //    if (mPODocNo.Length > 0)
            //    {
            //        var SQL = new StringBuilder();
            //        var lP = new List<PORDept>();
            //        var cm = new MySqlCommand();

            //        SQL.AppendLine("Select A.DocNo As PODocNo, B.DocNo As PORDocNo, C.DeptCode ");
            //        SQL.AppendLine("From ");
            //        SQL.AppendLine("( ");
            //        SQL.AppendLine("    Select T.DocNo, T.PORequestDocNo, T.PORequestDNo ");
            //        SQL.AppendLine("    From TblPODtl T ");
            //        SQL.AppendLine("    Where Find_In_Set(T.DocNo, @PODocNo) ");
            //        SQL.AppendLine(") A ");
            //        SQL.AppendLine("Inner Join TblPORequestDtl B On A.PORequestDocNo = B.DocNo And A.PORequestDNo = B.DNo ");
            //        SQL.AppendLine("Inner Join TblMaterialRequestHdr C On B.MaterialRequestDocNo = C.DocNo ");

            //        using (var cn = new MySqlConnection(Gv.ConnectionString))
            //        {
            //            cn.Open();
            //            cm.Connection = cn;
            //            cm.CommandText = SQL.ToString();
            //            Sm.CmParam<String>(ref cm, "@PODocNo", mPODocNo);
            //            var dr = cm.ExecuteReader();
            //            var c = Sm.GetOrdinal(dr, new string[] { "PODocNo", "PORDocNo", "DeptCode" });
            //            if (dr.HasRows)
            //            {
            //                while (dr.Read())
            //                {
            //                    lP.Add(new PORDept()
            //                    {
            //                        PODocNo = Sm.DrStr(dr, c[0]),
            //                        PORDocNo = Sm.DrStr(dr, c[1]),
            //                        DeptCode = Sm.DrStr(dr, c[2])
            //                    });
            //                }
            //            }
            //        }

            //        if(lP.Count > 0)
            //        {
            //            string mPORDeptCode = string.Empty;

            //            for (int i = 0; i < lP.Count - 1; i++)
            //            {
            //                if (lP[i].DeptCode != lP[i + 1].DeptCode)
            //                {
            //                    var mMsg = new StringBuilder();

            //                    mMsg.AppendLine("PO# 1 : " + lP[i].PODocNo);
            //                    mMsg.AppendLine("PO# 2 : " + lP[i + 1].PODocNo);
            //                    mMsg.AppendLine("POR# 1 : " + lP[i].PORDocNo);
            //                    mMsg.AppendLine("POR# 2 : " + lP[i + 1].PORDocNo);
            //                    mMsg.AppendLine(Environment.NewLine);
            //                    mMsg.AppendLine("PO Request has different Department.");

            //                    Sm.StdMsg(mMsgType.Warning, mMsg.ToString());
            //                    mPORDeptCode = string.Empty;
            //                    return;
            //                }
            //                else
            //                    mPORDeptCode = lP[i].DeptCode;
            //            }

            //            if (mPORDeptCode.Length > 0) Sm.SetLue(LueDeptCode, mPORDeptCode);
            //        }
            //    }
            //}
        }

        private void ProcessDepositSummary(ref List<DepositSummary> l, string EntCode)
        {
            if (Sm.CompareStr(TxtCurCode.Text, mMainCurCode)) return;

            decimal DownPayment = 0m;

            if (TxtDownPayment.Text.Length > 0) 
                DownPayment = decimal.Parse(TxtDownPayment.Text);

            if (DownPayment <= 0m) return;

            GetDepositSummary(ref l, EntCode);

            if (l.Count > 0m)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    if (DownPayment > 0m)
                    {
                        if (DownPayment >= l[i].Amt)
                        {
                            l[i].UsedAmt = l[i].Amt;
                            DownPayment -= l[i].Amt;
                        }
                        else
                        {
                            l[i].UsedAmt = DownPayment;
                            DownPayment = 0m;
                            break;
                        }
                    }
                    else
                        break;
                }

                for (int i = l.Count-1; i >= 0; i--)
                    if (l[i].UsedAmt == 0m) l.RemoveAt(i);
            }
        }

        private void GetDepositSummary(ref List<DepositSummary> l, string EntCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ExcRate, Amt ");
            SQL.AppendLine("From TblVendorDepositSummary2 ");
            SQL.AppendLine("Where VdCode=@VdCode ");
            SQL.AppendLine("And IfNull(EntCode, '')=IfNull(@EntCode, '') ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And Amt>0.00 ");
            SQL.AppendLine("Order By CreateDt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
                Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
                Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ExcRate", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DepositSummary()
                        {
                            ExcRate = Sm.DrDec(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1]),
                            UsedAmt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetDepositSummary(ref List<DepositSummary> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ExcRate, Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl5 ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By CreateDt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ExcRate", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DepositSummary()
                        {
                            ExcRate = Sm.DrDec(dr, c[0]),
                            UsedAmt = Sm.DrDec(dr, c[1]),
                            Amt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        internal void ComputeAmt2()
        {
            decimal Amt = 0m, RateAmt = 0m;
            if (TxtAmt.Text.Length != 0) Amt = decimal.Parse(TxtAmt.Text);
            if (TxtRateAmt.Text.Length != 0) RateAmt = decimal.Parse(TxtRateAmt.Text);

            TxtAmt2.EditValue = Sm.FormatNum(Amt * RateAmt, 0);
        }

        private void ComputeDueDt()
        {
            int temp = 0; int temp2 = 0; int TOP = 0;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select Round(E.PtDay, 0) As PtDay ");
                SQL.AppendLine("From TblRecvVdDtl A ");
                SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo = B.DocNo And A.PODNo = B.DNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo = C.DocNo And B.PORequestDNo = C.DNo ");
                SQL.AppendLine("Inner Join TblQtHdr D On C.QtDocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblPaymentTerm E On D.PtCode = E.PtCode ");
                SQL.AppendLine("Where A.DocNo = @RecvVdDocNo And A.DNo = @RecvVdDNo; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@RecvVdDocNo", Sm.GetGrdStr(Grd1, Row, 2));
                    Sm.CmParam<String>(ref cm, "@RecvVdDNo", Sm.GetGrdStr(Grd1, Row, 3));
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "PtDay" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            temp = Int32.Parse(Sm.DrStr(dr, 0));
                            if (temp2 == 0) temp2 = temp;
                            else
                            {
                                if (temp2 > temp) temp2 = temp;
                            }
                        }
                    }
                    dr.Close();
                }
                TOP = temp2;
            }

            DteDueDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteVdInvDt)).AddDays(TOP);
        }

        #region Generate DocNo

        private string GenerateVoucherRequestDocNo(string Value)
        {
            string
                Type = Sm.GetValue(
                    "Select " + (Sm.GetLue(LueAcType) == "C" ? "AutoNoCredit" : "AutoNoDebit") +
                    " From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' "),
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'");

            var SQL = new StringBuilder();
            
            SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
            SQL.Append("(Select Right(Concat('0000', Convert(DocNo+" + Value + ", Char)), 4) As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
            SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            if (Type.Length != 0) SQL.Append("And Right(DocNo, '" + Type.Length + "') = '" + Type + "' ");
            SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
            SQL.Append("), '000" + Value + "') As Number), '/', '" + DocAbbr + "'");
            if (Type.Length != 0) SQL.Append(", '/' , '" + Type + "' ");
            SQL.Append(") As DocNo ");
            
            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateVoucherDocNo(string Value)
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Voucher'"),
                Type = Sm.GetValue(
                    "Select " +
                    (Sm.GetLue(LueAcType) == "C" ? "AutoNoCredit" : "AutoNoDebit") +
                    " From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "';");

            var SQL = new StringBuilder();

            SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
            SQL.Append("(Select Right(Concat('0000', Convert(DocNo+" + Value + ", Char)), 4) As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherHdr ");
            SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            if (Type.Length > 0) SQL.Append("And Right(DocNo, '" + Type.Length + "') = '" + Type + "' ");
            SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
            SQL.Append("), '000" + Value + "') As Number), '/', '" + DocAbbr + "', '/' ");
            if (Type.Length > 0) SQL.Append(", '" + Type + "' ");
            SQL.Append(") As DocNo ");

            return Sm.GetValue(SQL.ToString());
        }
        #endregion

        #region Grid Event

        private void fButtonCopy_Click(object sender, EventArgs e)
        {
            if (!fCopyPasteManager.CanCopyToClipboard() )
            {
                MessageBox.Show(this, "No cells are selected.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            fCopyPasteManager.CopyToClipboard();
            
        }

        #endregion

        private void GetParameter()
        {
            string MenuCodeForDocWithMInd = Sm.GetParameter("MenuCodeForDocWithMInd");
            if (MenuCodeForDocWithMInd.Length > 0)
                mMInd =
                    MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1 ?
                        "Y" : "N";
            mIsUseMInd = Sm.GetParameterBoo("IsUseMInd");
            mIsAutoGeneratePurchaseLocalDocNo = Sm.GetParameterBoo("IsAutoGeneratePurchaseLocalDocNo");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsAPDownpaymentUseEntity = Sm.GetParameterBoo("IsAPDownpaymentUseEntity");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mProcFormatDocNo = Sm.GetParameter("ProcFormatDocNo") == "1";
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived") ;
            mIsComparedToDetailDate = Sm.GetParameterBoo("IsComparedToDetailDate");
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "N";
            mIsPIShowWarningAPDP = Sm.GetParameter("IsPIShowWarningAPDP") == "N";
            mIsDocInformationPIMandatory = Sm.GetParameterBoo("IsDocInformationPIMandatory");
            mIsPIDueDateAutoFill = Sm.GetParameterBoo("IsPIDueDateAutoFill");
            mIsRemarkForJournalMandatory = Sm.GetParameterBoo("IsRemarkForJournalMandatory");
            mIsPIUseQRCodeInvoice = Sm.GetParameterBoo("IsPIUseQRCodeInvoice");
            mPIQRCodeTaxDocType = Sm.GetParameter("PIQRCodeTaxDocType");
            mPIQRCodeDPTaxDocType = Sm.GetParameter("PIQRCodeDPTaxDocType");
            mPurchaseInvoiceQRCodeTaxCodeDefault = Sm.GetParameter("PurchaseInvoiceQRCodeTaxCodeDefault");
            mIsPurchaseInvoiceDeptMandatory = Sm.GetParameterBoo("IsPurchaseInvoiceDeptMandatory");
            mIsPurchaseInvoiceShowDataAfterInsert = Sm.GetParameterBoo("IsPurchaseInvoiceShowDataAfterInsert");
            mIsPurchaseInvoiceUseCOATaxInd = Sm.GetParameterBoo("IsPurchaseInvoiceUseCOATaxInd");
            mIsPIAutoShowPOTax = Sm.GetParameterBoo("IsPIAutoShowPOTax");
            mIsPIPaymentTypeMandatory = Sm.GetParameterBoo("IsPIPaymentTypeMandatory");
            mIsGroupPaymentTermActived = Sm.GetParameterBoo("IsGroupPaymentTermActived");
            mIsPIDeptEqualToPORDept = Sm.GetParameterBoo("IsPIDeptEqualToPORDept");
            mIsTaxAliasMandatory = Sm.GetParameterBoo("IsTaxAliasMandatory");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
            mIsPIWithZeroAmtProcessToOP = Sm.GetParameterBoo("IsPIWithZeroAmtProcessToOP");
            mIsPIAmtRoundOff = Sm.GetParameterBoo("IsPIAmtRoundOff");
            mAcNoForRoundingCost = Sm.GetParameter("AcNoForRoundingCost");
            mIsPIAllowToUploadFile = Sm.GetParameterBoo("IsPIAllowToUploadFile");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsEProcUseDigitalInvoice = Sm.GetParameterBoo("IsEProcUseDigitalInvoice");
            mDigitalInvoiceHost = Sm.GetParameter("DigitalInvoiceHost");
            mDigitalInvoiceFolder = Sm.GetParameter("DigitalInvoiceFolder");
            mIsPIQRCodeWarningEnabled = Sm.GetParameterBoo("IsPIQRCodeWarningEnabled");
            mPIRoundedMaxValIfJournalNotBalance = Sm.GetParameterDec("PIRoundedMaxValIfJournalNotBalance");
            mIsPIDocInfoEditable = Sm.GetParameterBoo("IsPIDocInfoEditable");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsPurchaseInvoiceShowAPDownpayment2 = Sm.GetParameterBoo("IsPurchaseInvoiceShowAPDownpayment2");
            mIsPITotalWithoutTaxInclDownpaymentEnabled = Sm.GetParameterBoo("IsPITotalWithoutTaxInclDownpaymentEnabled");
            mDocNoFormat = Sm.GetParameter("DocNoFormat");
            mIsPurchaseInvoiceOnlyShowDataAfterInsert = Sm.GetParameterBoo("IsPurchaseInvoiceOnlyShowDataAfterInsert");
            mIsPurchaseInvoiceTaxInvoiceInfoValidationDisabled = Sm.GetParameterBoo("IsPurchaseInvoiceTaxInvoiceInfoValidationDisabled");
            mIsPITotalTaxOnlyShowTaxCalculation = Sm.GetParameterBoo("IsPITotalTaxOnlyShowTaxCalculation");
            mIsVendorComboShowCategory = Sm.GetParameterBoo("IsVendorComboShowCategory");
            mIsPurchaseInvoiceUseApproval = IsPurchaseInvoiceUseApproval();
            mIsPIJournalAPBasedOnCOATaxInd = Sm.GetParameterBoo("IsPIJournalAPBasedOnCOATaxInd");
            mIsAPDownpaymentUseDefaultEntity = Sm.GetParameterBoo("IsAPDownpaymentUseDefaultEntity");
            mCostCenterFormulaForAPAR = Sm.GetParameter("CostCenterFormulaForAPAR");
            mIsRecvExpeditionEnabled = Sm.GetParameterBoo("IsRecvExpeditionEnabled");

            if (mCostCenterFormulaForAPAR.Length == 0) mCostCenterFormulaForAPAR = "1";
        }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue, string DeptCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
                SQL.AppendLine("From TblDepartment T ");
                SQL.AppendLine("Where (T.ActInd='Y' ");
                if (DeptCode.Length != 0)
                    SQL.AppendLine("Or T.DeptCode=@DeptCode ");
                if ((mIsFilterByDept) && (mStateIndicator == 1))
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By DeptName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                if (DeptCode.Length != 0)
                    Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
                if (mIsFilterByDept)
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedRecvVd()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0 &&
                        Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedPO()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0 &&
                        Sm.GetGrdStr(Grd2, Row, 4).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            Sm.GetGrdStr(Grd2, Row, 4) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void ComputeTotalWithoutTax()
        {
            decimal TotalWithoutTax = 0m, DownPayment = 0m;

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && Sm.GetGrdStr(Grd1, Row, 21).Length > 0)
                        TotalWithoutTax += Sm.GetGrdDec(Grd1, Row, 21);           
                }
            }

            if (Grd2.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 4).Length > 0 && Sm.GetGrdStr(Grd2, Row, 8).Length > 0)
                    {
                        switch(Sm.GetGrdStr(Grd2, Row, 4))
                        {
                            case "2": //Customs Tax
                                TotalWithoutTax += Sm.GetGrdDec(Grd2, Row, 8);
                                break;
                            case "1": //Discount Amount
                                TotalWithoutTax -= Sm.GetGrdDec(Grd2, Row, 8);
                                break;
                        }
                    }
                }
            }

            if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
            {
                if (TxtDownPayment.Text.Length != 0) DownPayment = decimal.Parse(TxtDownPayment.Text);
                TotalWithoutTax -= DownPayment;
            }
            TxtTotalWithoutTax.EditValue = Sm.FormatNum(TotalWithoutTax, 0);
        }

        private void ComputeTotalWithTax()
        {
            decimal TotalWithoutTax = 0m, TaxAmt1 = 0m, TaxAmt2 = 0m, TaxAmt3 = 0m, TotalTaxAmt1 = 0m, TaxRateAmt = 0m;

            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { 
                TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, TxtTotalTaxAmt1, TxtTotalTaxAmt2 
            }, 0);

            if (TxtTotalWithoutTax.Text.Length != 0) TotalWithoutTax = decimal.Parse(TxtTotalWithoutTax.Text);
            
            if (TxtTaxRateAmt.Text.Length != 0) TaxRateAmt = decimal.Parse(TxtTaxRateAmt.Text);

            if (Sm.GetLue(LueTaxCode1).Length != 0)
            {
                TaxAmt1 = GetTaxRate(Sm.GetLue(LueTaxCode1)) / 100 * TotalWithoutTax;
                TxtTaxAmt1.Text = Sm.FormatNum(TaxAmt1, 0);
                TotalTaxAmt1 = TaxAmt1;
            }

            if (Sm.GetLue(LueTaxCode2).Length != 0)
            {
                TaxAmt2 = GetTaxRate(Sm.GetLue(LueTaxCode2)) / 100 * TotalWithoutTax;
                TxtTaxAmt2.Text = Sm.FormatNum(TaxAmt2, 0);
                TotalTaxAmt1 += TaxAmt2;
            }

            if (Sm.GetLue(LueTaxCode3).Length != 0)
            {
                TaxAmt3 = GetTaxRate(Sm.GetLue(LueTaxCode3)) / 100 * TotalWithoutTax;
                TxtTaxAmt3.Text = Sm.FormatNum(TaxAmt3, 0);
                TotalTaxAmt1 += TaxAmt3;
            }

            TxtTotalTaxAmt1.EditValue = Sm.FormatNum(TotalTaxAmt1, 0);
            TxtTotalTaxAmt2.EditValue = Sm.FormatNum(TotalTaxAmt1*TaxRateAmt, 0);
            TxtTotalWithTax.EditValue = Sm.FormatNum(TotalWithoutTax + TotalTaxAmt1, 0);
        }

        private void ComputeDownPayment()
        {
            decimal DownPayment = 0m;

            if (Grd2.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 4).Length > 0 && 
                        Sm.GetGrdStr(Grd2, Row, 8).Length > 0 && 
                        Sm.GetGrdStr(Grd2, Row, 4)=="3")
                        DownPayment += Sm.GetGrdDec(Grd2, Row, 8);
                }
            }
            TxtDownPayment.EditValue = Sm.FormatNum(DownPayment, 0);
        }

        internal void ComputeAmtWithCOA()
        {
            if (ChkCOATaxInd.Checked) return;

            decimal AmtInv = Decimal.Parse(TxtTotalWithoutTax.Text);

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1) == Sm.GetValue(
                    "Select Concat(ParValue, '" + Sm.GetLue(LueVdCode) + "') " +
                    "From TblParameter Where ParCode = 'VendorAcNoAP';"))
                {
                    string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo = '" + Sm.GetGrdStr(Grd4, Row, 1) + "'");
                    if (Sm.GetGrdDec(Grd4, Row, 3) != 0)
                    {
                        String AcTypeGrid = "D";
                        if (AcType == AcTypeGrid)
                        {
                            AmtInv += Sm.GetGrdDec(Grd4, Row, 3);
                        }
                        else
                        {
                            AmtInv -= Sm.GetGrdDec(Grd4, Row, 3);
                        }

                    }
                    if (Sm.GetGrdDec(Grd4, Row, 4) != 0)
                    {
                        String AcTypeGrid = "C";
                        if (AcType == AcTypeGrid)
                        {
                            AmtInv += Sm.GetGrdDec(Grd4, Row, 4);
                        }
                        else
                        {
                            AmtInv -= Sm.GetGrdDec(Grd4, Row, 4);
                        }

                    }
                }
                //if (mIsPIAmtRoundOff && mAcNoForRoundingCost.Length > 0 && Sm.CompareStr(Sm.GetGrdStr(Grd4, Row, 1), mAcNoForRoundingCost))
                //    AmtInv -= Sm.GetGrdDec(Grd4, Row, 3);
                
            }
            TxtTotalWithoutTax.EditValue = Sm.FormatNum(AmtInv, 0);
        }

        internal void ComputeTotalWithTaxAndCOA()
        {
            if (!ChkCOATaxInd.Checked) return;

            var Amt = 0m;
            var AcNo = string.Concat(Sm.GetParameter("VendorAcNoAP"), Sm.GetLue(LueVdCode));
            var AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo=@Param;", AcNo);

            for (int r = 0; r < Grd4.Rows.Count - 1; r++)
            {
                if (Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd4, r, 1)))
                {
                    if (Sm.GetGrdDec(Grd4, r, 3) != 0)
                    {
                        if (AcType == "D")
                            Amt += Sm.GetGrdDec(Grd4, r, 3);
                        else
                            Amt -= Sm.GetGrdDec(Grd4, r, 3);                        
                    }
                    if (Sm.GetGrdDec(Grd4, r, 4) != 0)
                    {
                        if (AcType == "C")
                            Amt += Sm.GetGrdDec(Grd4, r, 4);
                        else
                            Amt -= Sm.GetGrdDec(Grd4, r, 4);
                    }
                }
            }
            decimal TaxRateAmt = 0m, TotalTaxAmt1 = 0m;

            if (TxtTaxRateAmt.Text.Length != 0) TaxRateAmt = decimal.Parse(TxtTaxRateAmt.Text);
            TxtTotalWithTax.EditValue = Sm.FormatNum(Decimal.Parse(TxtTotalWithTax.Text) + Amt, 0);

            if (mIsPITotalTaxOnlyShowTaxCalculation)
            {
                if (TxtTotalTaxAmt1.Text.Length != 0)mTotalTaxWithCOAAmt = decimal.Parse(TxtTotalTaxAmt1.Text) + Amt;
            }
            else
            {
                if (TxtTotalTaxAmt1.Text.Length != 0) TotalTaxAmt1 = decimal.Parse(TxtTotalTaxAmt1.Text) + Amt;
                TxtTotalTaxAmt1.EditValue = Sm.FormatNum(TotalTaxAmt1, 0);
                TxtTotalTaxAmt2.EditValue = Sm.FormatNum(TotalTaxAmt1 * TaxRateAmt, 0);
            }
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m, TotalWithTax=0m, DownPayment = 0m;

            ComputeTotalWithoutTax();
            ComputeAmtWithCOA();
            ComputeTotalWithTax();
            ComputeTotalWithTaxAndCOA();
            if (TxtTotalWithTax.Text.Length != 0) TotalWithTax = decimal.Parse(TxtTotalWithTax.Text);
            if (TxtDownPayment.Text.Length != 0) DownPayment = decimal.Parse(TxtDownPayment.Text);


            if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
                Amt = TotalWithTax;
            else
                Amt = TotalWithTax - DownPayment;

            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
            TxtAmt2.EditValue = Sm.FormatNum(Amt, 0);

            ComputeTaxAmtDifference();
            ComputeAmt3();
        }

        private void ComputeAmt3()
        {
            decimal Amt3 = 0m;
            string 
                AcNo = Sm.GetValue("Select Concat(ParValue, @Param) From TblParameter Where ParCode='VendorAcNoAP';", Sm.GetLue(LueVdCode)),
                AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", AcNo);

            if (TxtAmt.Text.Length != 0) Amt3 = decimal.Parse(TxtAmt.Text);

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd4, Row, 1), AcNo))
                {
                    if (Sm.GetGrdDec(Grd4, Row, 3) != 0)
                    {
                        if (AcType == "D")
                            Amt3 -= Sm.GetGrdDec(Grd4, Row, 3);
                        else
                            Amt3 += Sm.GetGrdDec(Grd4, Row, 3);
                    }
                    if (Sm.GetGrdDec(Grd4, Row, 4) != 0)
                    {
                        if (AcType == "C")
                            Amt3 -= Sm.GetGrdDec(Grd4, Row, 4);
                        else
                            Amt3 += Sm.GetGrdDec(Grd4, Row, 4);
                    }
                }
            }
            TxtAmt3.EditValue = Sm.FormatNum(Amt3, 0);
        }

        private decimal GetTaxRate(string TaxCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select TaxRate from TblTax Where TaxCode=@TaxCode;"
            };
            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);
            string TaxRate = Sm.GetValue(cm);

            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }

        private void ComputeOutstandingAmt()
        {
            var SQL = new StringBuilder();
            string Key = "'XXX'", Key2 = "'XXX'";

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                {
                    Key += (", '" + Sm.GetGrdStr(Grd2, Row, 2) + Sm.GetGrdStr(Grd2, Row, 4) + "'");
                    Key2 += (", '" + Sm.GetGrdStr(Grd2, Row, 2) + "'");
                }
            }
            SQL.AppendLine("Select T.DocNo, T.AmtType, T.Amt From (");
            SQL.AppendLine("    Select '1' As AmtType, A.DocNo, A.DocDt, A.VdCode, ");
            SQL.AppendLine("    (A.DiscountAmt ");
            SQL.AppendLine("        -IfNull((Select Sum(T2.Amt) From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='1' And T2.PODocNo=A.DocNo), 0) ");
            SQL.AppendLine("        +IfNull((Select Sum(T2.Amt) From TblPurchaseReturnInvoiceHdr T1, TblPurchaseReturnInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='1' And T2.PurchaseInvoiceDocNo=A.DocNo), 0) ");
            SQL.AppendLine("    )As Amt ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Where IfNull(A.DiscountAmt, 0)<>0 ");
            SQL.AppendLine("    And A.DocNo In ("+Key2+") ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As AmtType, A.DocNo, A.DocDt, A.VdCode, ");
            SQL.AppendLine("    (A.CustomsTaxAmt ");
            SQL.AppendLine("        -IfNull((Select Sum(T2.Amt) From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='2' And T2.PODocNo=A.DocNo), 0) ");
            SQL.AppendLine("        +IfNull((Select Sum(T2.Amt) From TblPurchaseReturnInvoiceHdr T1, TblPurchaseReturnInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='2' And T2.PurchaseInvoiceDocNo=A.DocNo), 0) ");
            SQL.AppendLine("    ) As Amt ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Where IfNull(A.CustomsTaxAmt, 0)<>0 ");
            SQL.AppendLine("    And A.DocNo In (" + Key2 + ") ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '3' As AmtType, A.DocNo, A.DocDt, A.VdCode, ");
            SQL.AppendLine("    (A.DownPayment ");
            SQL.AppendLine("        -IfNull((Select Sum(T2.Amt) From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='3' And T2.PODocNo=A.DocNo), 0) ");
            SQL.AppendLine("        +IfNull((Select Sum(T2.Amt) From TblPurchaseReturnInvoiceHdr T1, TblPurchaseReturnInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='3' And T2.PurchaseInvoiceDocNo=A.DocNo), 0) ");
            SQL.AppendLine("    ) As Amt ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Where IfNull(A.DownPayment, 0)<>0 ");
            SQL.AppendLine("    And A.DocNo In (" + Key2 + ") ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Where Concat(T.DocNo, T.AmtType) In (" + Key + ") ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "AmtType", "Amt" });

                if (dr.HasRows)
                {
                    Grd2.ProcessTab = true;
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row <= Grd2.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.GetGrdStr(Grd2, Row, 2).Length > 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 2), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 4), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 7, 2);
                                break;
                            }
                        }
                    }
                    Grd2.EndUpdate();
                }
                dr.Close();
            }
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex-1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd4.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd4, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void SetLueVdCode(ref DXE.LookUpEdit Lue, string VdCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (VdCode.Length != 0)
            {
                SQL.AppendLine("Select T.VdCode As Col1, ");
                if(mIsVendorComboShowCategory)
                    SQL.AppendLine("Concat(T.VdCode, ' [', T2.VdCtCode, ']') ");
                else
                    SQL.AppendLine("T.VdName As Col2 ");
                SQL.AppendLine("From TblVendor T ");
                SQL.AppendLine("Left Join TblVendorCategory T2 On T.VdCtCode=T2.VdCtCode ");
                SQL.AppendLine("Where T.VdCode=@VdCode; ");
            }
            else
            {
                SQL.AppendLine("Select Distinct A.VdCode As Col1, ");
                if (mIsVendorComboShowCategory)
                    SQL.AppendLine("Concat(C.VdName, ' [', G.VdCtName, ']') As Col2 ");
                else
                    SQL.AppendLine("C.VdName As Col2 ");
                SQL.AppendLine("From TblRecvVdHdr A ");
                SQL.AppendLine("Inner Join TblRecvVdDtl B ");
                SQL.AppendLine("    On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.CancelInd='N' ");
                SQL.AppendLine("    And IfNull(B.Status, 'O')='A' ");
                SQL.AppendLine("    And Not Exists( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl T2 ");
                SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T2.RecvVdDocNo=B.DocNo ");
                SQL.AppendLine("        And T2.RecvVdDNo=B.DNo ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
                if (mIsGroupPaymentTermActived)
                {
                    SQL.AppendLine("Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo And D.CancelInd='N' ");
                    SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
                    SQL.AppendLine("Inner Join TblQtHdr F On E.QtDocNo=F.DocNo ");
                    SQL.AppendLine("    And F.PtCode Is Not Null ");
                    SQL.AppendLine("    And F.PtCode In (");
                    SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join TblVendorCategory G On C.VdCtCode = G.VdCtCode ");
                SQL.AppendLine("Where 1=1 ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And (A.SiteCode Is Null Or (A.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ))) ");
                }
                SQL.AppendLine("Order By C.VdName;");
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            cm.CommandText = SQL.ToString();

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private string GenerateDocNo(string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

            var SQL = new StringBuilder();

            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

             if (IsDocNoFormatUseFullYear)
             {
                 Yr = Sm.Left(DocDt, 4);

                 SQL.Append("Select Concat( ");
                 SQL.Append("IfNull(( ");
                 SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                 SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                 SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                 SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                 SQL.Append("       ) As Temp ");
                 SQL.Append("   ), '0001') ");
                 SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                 SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                 SQL.Append(") As DocNo");
             }
             else
             {
                 if (mProcFormatDocNo)
                 {
                     SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                     SQL.Append("IfNull(( ");
                     SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                     SQL.Append("         Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                     SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                     SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                     SQL.Append("       ) As Temp ");
                     SQL.Append("   ), '0001') ");
                     SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                     SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                     SQL.Append(") As DocNo");
                 }
                 else
                 {
                     SQL.Append("Select Concat( ");
                     SQL.Append("IfNull(( ");
                     SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                     SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                     SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                     SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                     SQL.Append("       ) As Temp ");
                     SQL.Append("   ), '0001') ");
                     SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                     SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                     SQL.Append(") As DocNo");
                 }
             }

            return Sm.GetValue(SQL.ToString());
        }

        private void PrintData(string DocNo)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<InvHdr>();
            var ldtl = new List<InvDtl>();
            var ldtl2 = new List<InvDtl2>();
            var ldtl3 = new List<InvDtl3>();
            var ldtl4 = new List<InvDtl4>();
            var lDtlS = new List<PISignIMS>();
            var lDtlS2 = new List<PISignIMS2>();
            var lJournal = new List<PIJournal>();

            string mTaxName1 = Sm.GetLue(LueTaxCode1).Length > 0 ? LueTaxCode1.Text : string.Empty;
            string mTaxName2 = Sm.GetLue(LueTaxCode2).Length > 0 ? LueTaxCode2.Text : string.Empty;
            string mTaxName3 = Sm.GetLue(LueTaxCode3).Length > 0 ? LueTaxCode3.Text : string.Empty;
            string mTaxName = mTaxName1;
            if (mTaxName2.Length > 0) mTaxName += Environment.NewLine + mTaxName2;
            if (mTaxName3.Length > 0) mTaxName += Environment.NewLine + mTaxName3;

            string mPPHTaxCode = Sm.GetParameter("PPHTaxCode");
            string[] mPPHTaxCodes = { };
            decimal mPPHAmt = 0m;
            if (mPPHTaxCode.Length > 0)
            {
                mPPHTaxCodes = mPPHTaxCode.Split(',');

                if (Sm.GetLue(LueTaxCode1).Length > 0)
                {
                    foreach (var x in mPPHTaxCodes)
                    {
                        if (Sm.GetLue(LueTaxCode1) == x)
                        {
                            mPPHAmt += Decimal.Parse(TxtTaxAmt1.Text);
                        }
                    }
                }

                if (Sm.GetLue(LueTaxCode2).Length > 0)
                {
                    foreach (var x in mPPHTaxCodes)
                    {
                        if (Sm.GetLue(LueTaxCode2) == x)
                        {
                            mPPHAmt += Decimal.Parse(TxtTaxAmt2.Text);
                        }
                    }
                }

                if (Sm.GetLue(LueTaxCode3).Length > 0)
                {
                    foreach (var x in mPPHTaxCodes)
                    {
                        if (Sm.GetLue(LueTaxCode3) == x)
                        {
                            mPPHAmt += Decimal.Parse(TxtTaxAmt3.Text);
                        }
                    }
                }
            }

            string[] TableName = { "InvHdr", "InvDtl", "InvDtl2", "InvDtl3", "PISignIMS", "PISignIMS2", "InvDtl4", "PIJournal" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, E.CompanyName, E.CompanyPhone, E.CompanyFax, E.CompanyAddress, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='IsPOSplitBasedOnTax') As 'Kop', ");
            }
            SQL.AppendLine("A.DocNo, A.LocalDocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, C.VdName, DATE_FORMAT(A.DueDt,'%d %b %Y') As DueDt, A.Remark, I.CurCode, I.CurName, ");
            SQL.AppendLine("DATE_FORMAT(A.VdInvDt,'%d %M %Y') As VdInvDt, A.TaxAmt, A.DownPayment,ifnull(if(B.Damt!=0,B.DAmt,B.CAmt),0)As Discount, D.UserCode, ifnull(F.AcAmt, 0) AS AcAmt, ");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='doctitle') As TitleInd, A.VdInvNo, C.Address As AddressVd,  ");
            SQL.AppendLine("IfNull(A.Taxalias1,IfNull(A.TaxCode1, null))As TaxCode1,IfNull(A.Taxalias2,IfNull(A.TaxCode2, null))As TaxCode2, IfNull(A.Taxalias3,IfNull(A.TaxCode3, null))As TaxCode3,");
            SQL.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
            SQL.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
            SQL.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxName3, ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxRate1, ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxRate2, ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxRate3, G.DeptName, H.SiteName  ");
            SQL.AppendLine(" ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceDtl4 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr A");
                SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode");
                SQL.AppendLine("    Inner Join TblProfitCenter C On B.ProfitCenterCode  = C.ProfitCenterCode");
                SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode");
                SQL.AppendLine("    Where A.DocNo=@DocNo");
                SQL.AppendLine(")E On A.DocNo = E.DocNo");
            }
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X.DocNo,  X.AcNo, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("    When X.AcType = 'D' && X.DAmt>0 Then X.DAmt");
            SQL.AppendLine("    When X.AcType = 'D' && X.CAmt>0 Then X.CAmt *-1");
            SQL.AppendLine("    When X.AcType = 'C' && X.CAmt>0 Then X.Camt");
            SQL.AppendLine("    When X.AcType = 'C' && X.DAmt>0 Then X.DAmt *-1");
            SQL.AppendLine("    End As AcAmt");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    (");
            SQL.AppendLine("        Select A.DocNo, B.AcNo, C.Actype, B.DAmt, B.Camt ");
            SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl4 B On A.DocNo=B.DocNo ");
	        SQL.AppendLine("        Inner Join TblCOA C On B.AcNo=C.Acno");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoAP' ");
	        SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.COATaxInd='N' ");
            //SQL.AppendLine("        And Left(B.AcNo, Length(ParValue))=C.ParValue ");
            SQL.AppendLine("        And C.AcNo=Concat(D.ParValue, A.VdCode) ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine(")F ON A.DocNo = F.DocNo ");
            SQL.AppendLine("Left Join TblDepartment G On A.DeptCode=G.DeptCode ");
            SQL.AppendLine("Left Join TblSite H On A.SiteCode=H.SiteCode ");
            SQL.AppendLine("Inner Join TblCurrency I On A.CurCode=I.CurCode ");

            SQL.AppendLine("Where A.DocNo=@DocNo ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select D.EntLogoName " +
                       "From TblPurchaseInvoiceHdr A " +
                       "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                       "Inner Join TblProfitCenter C On B.ProfitCenterCode  = C.ProfitCenterCode " +
                       "Inner Join TblEntity D On C.EntCode = D.EntCode " +
                       "Where A.DocNo='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                }
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",
                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         
                         //6-10
                         "DocNo",
                         "DocDt",
                         "VdName",
                         "DueDt" ,
                         "Remark",
                         
                         //11-15
                         "VdInvDt",
                         "TaxAmt",
                         "DownPayment",
                         "Discount",
                         "UserCode",
                        
                         //16-20
                         "LocalDocNo",
                         "AcAmt",
                         "TitleInd",
                         "VdInvNo",
                         "AddressVd",

                         //21-25
                         "TaxCode1",
                         "TaxCode2",
                         "TaxCode3",
                         "TaxName1",
                         "TaxName2",

                         //26-30
                         "Taxname3",
                         "TaxRate1",
                         "TaxRate2",
                         "TaxRate3",
                         "DeptName", 

                         //31-33
                         "SiteName",
                         "CurCode",
                         "CurName"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new InvHdr()
                        {
                            
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            VdName = Sm.DrStr(dr, c[8]),
                            DueDate = Sm.DrStr(dr, c[9]),
                            Remark = Sm.DrStr(dr, c[10]),

                            VdInvDt = Sm.DrStr(dr, c[11]),
                            TaxAmt = Sm.DrDec(dr, c[12]),
                            DownPayment = Sm.DrDec(dr, c[13]),
                            Discount = Sm.DrDec(dr, c[14]),
                            UserCode = Sm.DrStr(dr, c[15]),

                            LocalDocNo = Sm.DrStr(dr, c[16]),
                            AcAmt = Sm.DrDec(dr, c[17]),
                            TitleInd = Sm.DrStr(dr, c[18]),
                            VdInvNo = Sm.DrStr(dr, c[19]),
                            AddressVd = Sm.DrStr(dr, c[20]),

                            TaxCode1 = Sm.DrStr(dr, c[21]),
                            TaxCode2 = Sm.DrStr(dr, c[22]),
                            TaxCode3 = Sm.DrStr(dr, c[23]),
                            TaxName1 = Sm.DrStr(dr, c[24]),
                            TaxName2 = Sm.DrStr(dr, c[25]),

                            TaxName3 = Sm.DrStr(dr, c[26]),
                            TaxRate1 = Sm.DrDec(dr, c[27]),
                            TaxRate2 = Sm.DrDec(dr, c[28]),
                            TaxRate3 = Sm.DrDec(dr, c[29]),
                            DeptName = Sm.DrStr(dr, c[30]),

                            SiteName = Sm.DrStr(dr, c[31]),
                            CurCode = Sm.DrStr(dr, c[32]),
                            CurName = Sm.DrStr(dr, c[33]),
                            DeliveryCost = mPPHAmt,
                            Terbilang = Sm.Terbilang(Decimal.Parse(TxtAmt.Text)),
                            InvoiceAmt = Decimal.Parse(TxtAmt.Text),
                            Subtotal = Decimal.Parse(TxtTotalWithoutTax.Text),
                            TotalTax2 = Decimal.Parse(TxtTotalTaxAmt2.Text),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode,
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                //SQLDtl.AppendLine("Select A.DocNo, A.RecvVdDocNo, B.ItCode, G.ItName, B.QtyPurchase, G.PurchaseUomCode, H.PtName, E.CurCode, F.UPrice, ");
                //SQLDtl.AppendLine("(F.UPrice - if(C.DiscountAmt>0, C.DiscountAmt, ((F.UPrice*C.Discount)/100*QtyPurchase))) As UPAD,");
                //SQLDtl.AppendLine("((B.QtyPurchase*F.UPrice)+C.RoundingValue-(if(C.DiscountAmt>0, C.DiscountAmt, ((F.UPrice*C.Discount)/100*QtyPurchase)))) As Total, ");
                //SQLDtl.AppendLine("A.Remark, if(C.DiscountAmt>0, C.DiscountAmt, ((F.UPrice*C.Discount)/100*QtyPurchase)) As Discount, H.PtDay, (F.UPrice*QtyPurchase) UPBD  ");
                //SQLDtl.AppendLine("From TblPurchaseInvoiceDtl A  ");
                //SQLDtl.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
                //SQLDtl.AppendLine("Inner Join TblPODtl C On B.PODocNo=C.DocNo And B.PODNo=C.DNo ");
                //SQLDtl.AppendLine("Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
                //SQLDtl.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
                //SQLDtl.AppendLine("Inner Join TblQtDtl F  On D.QtDocNo=F.DocNo And D.QtDNo=F.DNo ");
                //SQLDtl.AppendLine("Inner Join TblItem G On B.ItCode=G.ItCode ");
                //SQLDtl.AppendLine("Left Join TblPaymentTerm H On E.PtCode=H.PtCode ");
                //SQLDtl.AppendLine("Where A.DocNo=@DocNo ");

                SQLDtl.AppendLine("Select A.DocNo, A.RecvVdDocNo, B.ItCode, G.ItName, G.Specification, G.ItCodeInternal, B.QtyPurchase, G.PurchaseUomCode, E.CurCode, F.UPrice, ");
                SQLDtl.AppendLine("(F.UPrice - if(((B.QtyPurchase/C.Qty)*C.DiscountAmt)>0.00, ((B.QtyPurchase/C.Qty)*C.DiscountAmt), ((F.UPrice*C.Discount)/100*QtyPurchase))) As UPAD,");
                SQLDtl.AppendLine("((B.QtyPurchase*F.UPrice)+C.RoundingValue-(if(((B.QtyPurchase/C.Qty)*C.DiscountAmt)>0.00, ((B.QtyPurchase/C.Qty)*C.DiscountAmt), ((F.UPrice*C.Discount)/100*QtyPurchase)))) As Total, ");
                SQLDtl.AppendLine("A.Remark, if(((B.QtyPurchase/C.Qty)*C.DiscountAmt)>0, ((B.QtyPurchase/C.Qty)*C.DiscountAmt), ((F.UPrice*C.Discount)/100*QtyPurchase)) As Discount, ");
                if (Doctitle == "MAI")
                {
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("    ((QtyPurchase* F.UPrice * Case When IfNull(C.Discount, 0)=0 Then 1 Else (100-C.Discount)/100 End)-((QtyPurchase/C.Qty)*C.DiscountAmt)+C.RoundingValue) ");
                    SQLDtl.AppendLine(") As UPBD, ");
                }
                else
                    SQLDtl.AppendLine("(F.UPrice*QtyPurchase) UPBD, ");
                SQLDtl.AppendLine("DATE_FORMAT(B1.DocDt,'%d %M %Y') As RecvVdDocDt, H.PtDay, ");
                if (Doctitle == "IMS")
                    SQLDtl.AppendLine("Group_Concat(Distinct IfNull(B.PODocNo, '')Separator '\r\n') As PONo, Group_Concat(Distinct IfNull(H.PtName, '') Separator '\r\n') As PtName, Group_Concat(Distinct IfNull(Round(H.PtDay,0), 0)Separator '\r\n') As PtDay2 ");
                else
                    SQLDtl.AppendLine("B.PODocNo As PONo, H.PtName, null as PtDay2 ");
                SQLDtl.AppendLine("From TblPurchaseInvoiceDtl A  ");
                SQLDtl.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
                SQLDtl.AppendLine("Inner Join TblRecvVdHdr B1 On B1.DocNo=B.DocNo ");
                SQLDtl.AppendLine("Inner Join TblPODtl C On B.PODocNo=C.DocNo And B.PODNo=C.DNo ");
                SQLDtl.AppendLine("Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
                SQLDtl.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
                SQLDtl.AppendLine("Inner Join TblQtDtl F  On D.QtDocNo=F.DocNo And D.QtDNo=F.DNo ");
                SQLDtl.AppendLine("Inner Join TblItem G On B.ItCode=G.ItCode ");
                SQLDtl.AppendLine("Left Join TblPaymentTerm H On E.PtCode=H.PtCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl.AppendLine("GROUP BY A.DocNo, A.DNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "RecvVdDocNo" ,

                         //1-5
                         "ItCode",
                         "ItName",
                         "QtyPurchase" ,
                         "PurchaseUomCode",
                         "PtName" ,
                        
                         //6-10
                         "CurCode" ,
                         "UPrice",
                         "Total",
                         "Remark",
                         "Discount",

                         //11-15
                         "PtDay",
                         "UPAD",
                         "UPBD",
                         "RecvVdDocDt",
                         "PONo",

                         //16
                         "PtDay2",
                         "ItCodeInternal",
                         "Specification"
                         
                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new InvDtl()
                        {
                            nomor = nomor,
                            RecvVdDocNo = Sm.DrStr(drDtl, cDtl[0]),

                            ItCode = Sm.DrStr(drDtl, cDtl[1]),
                            ItName = Sm.DrStr(drDtl, cDtl[2]),
                            QtyPurchase = Sm.DrDec(drDtl, cDtl[3]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[4]),
                            PtName = Sm.DrStr(drDtl, cDtl[5]),

                            CurCode = Sm.DrStr(drDtl, cDtl[6]),
                            UPrice = Sm.DrDec(drDtl, cDtl[7]),
                            Total = Sm.DrDec(drDtl, cDtl[8]),
                            Remark = Sm.DrStr(drDtl, cDtl[9]),
                            Discount = Sm.DrDec(drDtl, cDtl[10]),
                            PtDay = Sm.DrDec(drDtl, cDtl[11]),
                            UPAD = Sm.DrDec(drDtl, cDtl[12]),
                            UPBD = Sm.DrDec(drDtl, cDtl[13]),
                            RecvVdDocDt = Sm.DrStr(drDtl, cDtl[14]),
                            PONo = Sm.DrStr(drDtl, cDtl[15]),
                            PtDay2 = Sm.DrStr(drDtl, cDtl[16]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[17]),
                            Specification = Sm.DrStr(drDtl, cDtl[18]),
                            TaxName = mTaxName,
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail data 2
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select AmtType, Amt From TblPurchaseInvoiceDtl2  ");
                SQLDtl2.AppendLine("Where DocNo=@DocNo ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "AmtType",

                         //1-5
                         "Amt",
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new InvDtl2()
                        {
                            AmtType = Sm.DrStr(drDtl2, cDtl2[0]),
                            Amt = Sm.DrDec(drDtl2, cDtl2[1]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail Signature
            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;
                SQLDtl3.AppendLine("Select A.CreateBy As UserCode, B.UserName, ");
                SQLDtl3.AppendLine("Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict, E.Posname, Left(A.CreateDt, 8) As LastUpDt ");
                SQLDtl3.AppendLine("From TblPurchaseInvoiceHdr A ");
                SQLDtl3.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
                SQLDtl3.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("Left Join tblemployee D On A.CreateBy=D.UserCode ");
                SQLDtl3.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
                SQLDtl3.AppendLine("Where DocNo=@DocNo ");
                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                        {
                         //0-4
                         "UserCode" ,
                         "UserName",
                         "EmpPict",
                         "Posname",
                         "LastUpDt",
                        });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new InvDtl3()
                        {
                            UserCode = Sm.DrStr(drDtl3, cDtl3[0]),
                            UserName = Sm.DrStr(drDtl3, cDtl3[1]),
                            EmpPict = Sm.DrStr(drDtl3, cDtl3[2]),
                            PosName = Sm.DrStr(drDtl3, cDtl3[3]),
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[4]),

                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);
            #endregion

            #region Detail Signature IMS

            //Dibuat Oleh
            var cmDtlS = new MySqlCommand();

            var SQLDtlS = new StringBuilder();
            using (var cnDtlS = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS.Open();
                cmDtlS.Connection = cnDtlS;

                SQLDtlS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtlS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtlS.AppendLine("From ( ");
                SQLDtlS.AppendLine("    Select Distinct ");
                SQLDtlS.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, '1' As DNo, 0 As Level, 'Prepared By,' As Title, Left(A.CreateDt, 8) As LastUpDt  ");
                SQLDtlS.AppendLine("    From TblPurchaseInvoiceHdr A ");
                SQLDtlS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtlS.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtlS.AppendLine(") T1 ");
                SQLDtlS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtlS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtlS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtlS.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                SQLDtlS.AppendLine("Order By T1.Level; ");

                cmDtlS.CommandText = SQLDtlS.ToString();
                Sm.CmParam<String>(ref cmDtlS, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS, "@DocNo", TxtDocNo.Text);
                var drDtlS = cmDtlS.ExecuteReader();
                var cDtlS = Sm.GetOrdinal(drDtlS, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",

                         //6
                         "LastupDt"
                        });
                if (drDtlS.HasRows)
                {
                    while (drDtlS.Read())
                    {
                        lDtlS.Add(new PISignIMS()
                        {
                            Signature = Sm.DrStr(drDtlS, cDtlS[0]),
                            UserName = Sm.DrStr(drDtlS, cDtlS[1]),
                            PosName = Sm.DrStr(drDtlS, cDtlS[2]),
                            Space = Sm.DrStr(drDtlS, cDtlS[3]),
                            DNo = Sm.DrStr(drDtlS, cDtlS[4]),
                            Title = Sm.DrStr(drDtlS, cDtlS[5]),
                            LastUpDt = Sm.DrStr(drDtlS, cDtlS[6])
                        });
                    }
                }
                drDtlS.Close();
            }
            myLists.Add(lDtlS);

            //Disetujui Oleh
            var cmDtlS2 = new MySqlCommand();

            var SQLDtlS2 = new StringBuilder();
            using (var cnDtlS2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS2.Open();
                cmDtlS2.Connection = cnDtlS2;


                SQLDtlS2.AppendLine("Select Distinct ");
                SQLDtlS2.AppendLine("B.UserCode, C.UserCode2, B.UserName, C.UserName2, B.PosName, C.PosName2 ");
                SQLDtlS2.AppendLine("From TblPurchaseInvoiceHdr A ");
                SQLDtlS2.AppendLine("Left Join ( ");
                SQLDtlS2.AppendLine("    Select Distinct ");
                SQLDtlS2.AppendLine("    A.DocNo , B.UserCode As UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                SQLDtlS2.AppendLine("    F.PosName ");
                SQLDtlS2.AppendLine("    From TblPurchaseInvoiceHdr A ");
                SQLDtlS2.AppendLine("    Inner Join TblDocApproval B On B.DocType='PurchaseInvoice' And A.DocNo=B.DocNo ");
                SQLDtlS2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtlS2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'PurchaseInvoice' ");
                SQLDtlS2.AppendLine("    Left Join TblEmployee E On B.userCode = E.Usercode ");
                SQLDtlS2.AppendLine("    Left Join TblPosition F On E.PosCode = F.PosCode ");
                SQLDtlS2.AppendLine("    Where D.Level = '1' And A.DocNo=@DocNo ");
                SQLDtlS2.AppendLine(" ) B On A.DocNo = B.DocNo ");
                SQLDtlS2.AppendLine("Left Join ( ");
                SQLDtlS2.AppendLine("    Select Distinct ");
                SQLDtlS2.AppendLine("    A.DocNo, B.UserCode As UserCode2, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName2, ");
                SQLDtlS2.AppendLine("    F.PosName PosName2 ");
                SQLDtlS2.AppendLine("    From TblPurchaseInvoiceHdr A ");
                SQLDtlS2.AppendLine("    Inner Join TblDocApproval B On B.DocType='PurchaseInvoice' And A.DocNo=B.DocNo ");
                SQLDtlS2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtlS2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'PurchaseInvoice' ");
                SQLDtlS2.AppendLine("    Left Join TblEmployee E On B.userCode = E.Usercode ");
                SQLDtlS2.AppendLine("    Left Join TblPosition F On E.PosCode = F.PosCode ");
                SQLDtlS2.AppendLine("    Where D.Level = '2' And A.DocNo=@DocNo ");
                SQLDtlS2.AppendLine(" ) C On A.DocNo = C.DocNo ");
                SQLDtlS2.AppendLine(" WHERE A.DocNo =@DocNo ");

                cmDtlS2.CommandText = SQLDtlS2.ToString();
                Sm.CmParam<String>(ref cmDtlS2, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS2, "@DocNo", TxtDocNo.Text);
                var drDtlS2 = cmDtlS2.ExecuteReader();
                var cDtlS2 = Sm.GetOrdinal(drDtlS2, new string[] 
                        {
                         //0
                         "Username" ,

                         //1-2
                         "Username2" ,
                         "PosName",
                         "PosName2"
                        });
                if (drDtlS2.HasRows)
                {
                    while (drDtlS2.Read())
                    {

                        lDtlS2.Add(new PISignIMS2()
                        {
                            UserName = Sm.DrStr(drDtlS2, cDtlS2[0]),
                            UserName2 = Sm.DrStr(drDtlS2, cDtlS2[1]),
                            PosName = Sm.DrStr(drDtlS2, cDtlS2[2]),
                            PosName2 = Sm.DrStr(drDtlS2, cDtlS2[3]),
                        });
                    }
                }
                drDtlS2.Close();
            }
            myLists.Add(lDtlS2);


            #endregion

            #region Detail data 4
            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine(" Select IfNull(A.AcDesc, '')As AcDesc,IfNull(B.AcDesc2, '') As AcDesc2, ");
                SQLDtl4.AppendLine("IfNull(A.DAmt, 0) As DAmt,IfNull(B.CAmt, 0) As CAmt ");
                SQLDtl4.AppendLine("From( ");
                SQLDtl4.AppendLine("Select X.DocNo, X2.AcDesc, ");
                SQLDtl4.AppendLine("SUM(X.DAmt) DAmt ");
                SQLDtl4.AppendLine("From TblPurchaseInvoiceDtl4 X ");
                SQLDtl4.AppendLine("Left Join TblCoa X2 On X.AcNo = X2.AcNo Where DocNo = @DocNo ");
                SQLDtl4.AppendLine("And X.DAmt != 0 ");
                SQLDtl4.AppendLine(") A ");
                SQLDtl4.AppendLine("Left Join ( ");
                SQLDtl4.AppendLine("Select X.DocNo, X2.AcDesc AcDesc2,  ");
                SQLDtl4.AppendLine("SUM(X.CAmt) CAmt ");
                SQLDtl4.AppendLine("From TblPurchaseInvoiceDtl4 X ");
                SQLDtl4.AppendLine("Left Join TblCoa X2 On X.AcNo = X2.AcNo Where DocNo = @DocNo ");
                SQLDtl4.AppendLine("And X.CAmt != 0 ");
                SQLDtl4.AppendLine(")B On A.DocNo = B.DocNo ");
                SQLDtl4.AppendLine("Where A.DocNo=@DocNo ");

                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "AcDesc",

                         //1-3
                         "AcDesc2",
                         "DAmt",
                         "CAmt"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {
                        ldtl4.Add(new InvDtl4()
                        {

                            AcDesc = Sm.DrStr(drDtl4, cDtl4[0]),
                            AcDesc2 = Sm.DrStr(drDtl4, cDtl4[1]),
                            DAmt = Sm.DrDec(drDtl4, cDtl4[2]),
                            CAmt = Sm.DrDec(drDtl4, cDtl4[3]),
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            #region Journal
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                SQL2.AppendLine("SELECT A.JournalDocNo, C.AcNo, C.AcDesc, B.DAmt, B.CAmt ");
                SQL2.AppendLine("From TblPurchaseInvoiceHdr A ");
                SQL2.AppendLine("Inner Join TblJournalDtl B On A.JournalDocNo = B.DocNo ");
                SQL2.AppendLine("INNER JOIN TBLCOA C ON B.ACNO = C.ACNO ");
                SQL2.AppendLine("WHERE A.DocNo = @DocNo ");
                SQL2.AppendLine("ORDER BY Camt ");

                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "AcNo" ,

                         //1-5
                         "AcDesc" ,
                         "DAmt",
                         "CAmt",
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {

                        lJournal.Add(new PIJournal()
                        {
                            AcNo = Sm.DrStr(dr2, c2[0]),
                            AcDesc = Sm.DrStr(dr2, c2[1]),
                            DAmt = Sm.DrDec(dr2, c2[2]),
                            CAmt = Sm.DrDec(dr2, c2[3]),

                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(lJournal);

            #endregion


            if (Doctitle == "KIM")
                Sm.PrintReport("PurchaseInvoice2", myLists, TableName, false);
            else if (Doctitle == "TWC")
                Sm.PrintReport("PurchaseInvoice3", myLists, TableName, false);
            else if (Doctitle == "KMI")
                Sm.PrintReport("PurchaseInvoice4", myLists, TableName, false);
            else if (Doctitle == "IMS")
                Sm.PrintReport("PurchaseInvoice5", myLists, TableName, false);
            else
                Sm.PrintReport("PurchaseInvoice", myLists, TableName, false);
        }

        private void ShowPreviousInvoiceHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocDt, VdCode, VdInvNo, VdInvDt, DueDt, DeptCode " +
                    "From TblPurchaseInvoiceHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocDt",  
                        
                        //1-5
                        "VdCode", "VdInvNo", "VdInvDt", "DueDt", "DeptCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[0]));
                        SetLueVdCode(ref LueVdCode, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[1]));
                        TxtVdInvNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetDte(DteVdInvDt, Sm.DrStr(dr, c[3]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[4]));
                        SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[5]));
                    }, true
                );
        }

        private void ShowEntitySite(string EntCode)
        {
            if (EntCode.Length == 0) return;

            StringBuilder
                SQL = new StringBuilder(),
                Msg = new StringBuilder();

            SQL.AppendLine("Select A.SiteName ");
            SQL.AppendLine("From TblSite A ");
            SQL.AppendLine("Inner Join TblProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode And B.EntCode=@EntCode;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);

            Msg.Append("Sites : "+Environment.NewLine);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(), new string[]{ "SiteName" },
                    (MySqlDataReader dr, int[] c) =>
                        { Msg.AppendLine("- " + Sm.DrStr(dr, c[0])); }, false
                );

            Sm.StdMsg(mMsgType.Info, Msg.ToString());
        }

        private void ReadData(int Row)
        {
            try
            {
                bool IsCopy = 
                    (mPIQRCodeTaxDocType.Length > 0 && 
                    Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, Row, 1)) 
                    //&& !IsOnlyDPTaxDocTypeExisted()
                    );
                string Detail = "";
                string QRCode = Sm.GetGrdStr(Grd3, Row, 3);
                XmlReader xmlReader = XmlReader.Create(QRCode);

                getXML(Row);

                while (xmlReader.Read())
                {
                   
                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "nomorFaktur"))
                    {
                        Grd3.Cells[Row, 5].Value = xmlReader.ReadString();
                    }
                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "tanggalFaktur"))
                    {
                        string x = xmlReader.ReadString();

                        string dateFaktur = string.Concat(Sm.Right(x, 4), x.Substring(3, 2), Sm.Left(x, 2));
                        Grd3.Cells[Row, 6].Value = Sm.ConvertDate(dateFaktur);
                    }
                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "jumlahDpp"))
                    {
                        Grd3.Cells[Row, 7].Value = decimal.Parse(xmlReader.ReadString());
                    }
                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "jumlahPpn"))
                    {
                        Grd3.Cells[Row, 8].Value = decimal.Parse(xmlReader.ReadString());
                    }

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "nama"))
                    {
                        Detail += string.Concat(xmlReader.ReadString(), " , ");
                    }                   
                }
                Grd3.Cells[Row, 11].Value = Detail.Remove(Detail.Length - 2);

                if (IsCopy)
                {
                    var TaxInvoiceNo = Sm.GetGrdStr(Grd3, Row, 5);
                    var TaxInvDt = Sm.GetGrdDate(Grd3, Row, 6);
                    //var TaxAmt = 0m;

                    //if (Sm.GetGrdStr(Grd3, Row, 8).Length > 0)
                    //    TaxAmt = Sm.GetGrdDec(Grd3, Row, 8);

                    if (TaxInvoiceNo.Length > 0)
                    {
                        TxtQRCodeTaxInvoiceNo.EditValue = TaxInvoiceNo;
                        TxtTaxInvoiceNo.EditValue = TaxInvoiceNo;
                    }
                    else
                        TxtQRCodeTaxInvoiceNo.EditValue = null;

                    if (TaxInvDt.Length > 0)
                    {
                        TaxInvDt = TaxInvDt.Substring(0, 8);
                        Sm.SetDte(DteQRCodeTaxInvoiceDt, TaxInvDt);
                        Sm.SetDte(DteTaxInvoiceDt, TaxInvDt);
                    }
                    else
                        DteQRCodeTaxInvoiceDt.EditValue = null;
                }
                var TaxAmt = 0m;
                
                for (int r = 0; r < Grd3.Rows.Count; r++)
                {
                    if (Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                    {
                        if (Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                            TaxAmt += Sm.GetGrdDec(Grd3, r, 8);
                    }
                    if (Sm.CompareStr(mPIQRCodeDPTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                    {
                        if (Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                            TaxAmt += Sm.GetGrdDec(Grd3, r, 8);
                    }
                }

                TxtQRCodeTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
                ComputeAmt();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        //internal bool IsOnlyDPTaxDocTypeExisted()
        //{
        //    bool IsDPTaxDocTypeExisted = false, IsTaxDocTypeExisted = false;

        //    for (int r = 0; r < Grd3.Rows.Count; r++)
        //    {
        //        if (Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, r, 1))) IsTaxDocTypeExisted = true;
        //        if (Sm.CompareStr(mPIQRCodeDPTaxDocType, Sm.GetGrdStr(Grd3, r, 1))) IsDPTaxDocTypeExisted = true;
        //    }

        //    if (!IsTaxDocTypeExisted && IsDPTaxDocTypeExisted) return true;
        //    return false;
        //}

        private void getXML(int Row)
        {
            try
            {
                string alfa = string.Empty;

                bool IsCopy = (mPIQRCodeTaxDocType.Length > 0 && Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, Row, 1)));
                string QRCode = Sm.GetGrdStr(Grd3, Row, 3);
                XmlReader xmlReader = XmlReader.Create(QRCode);

                while (xmlReader.Read())
                {
                    switch (xmlReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            alfa += ("<" + xmlReader.Name + ">");
                            break;
                        case XmlNodeType.Text:
                            alfa += (xmlReader.Value);
                            break;
                        case XmlNodeType.EndElement:
                            alfa += ("</" + xmlReader.Name + ">") + "\n"; ;
                            break;
                    }
                }

                Grd3.Cells[Row, 12].Value = alfa;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetQRCodeInfo()
        {
            if (mPIQRCodeTaxDocType.Length == 0) return;

            TxtQRCodeTaxInvoiceNo.EditValue = null;
            DteQRCodeTaxInvoiceDt.EditValue = null;
            TxtQRCodeTaxAmt.EditValue = Sm.FormatNum(0m, 0);

            //bool IsOnlyDPTaxDocType = IsOnlyDPTaxDocTypeExisted();

            int Row = -1;
            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                //if (IsOnlyDPTaxDocType)
                //{
                //    if (Sm.CompareStr(mPIQRCodeDPTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                //    {
                //        Row = r;
                //        break;
                //    }
                //}
                //else
                //{
                    if (Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                    {
                        Row = r;
                        break;
                    }
                //}
            }

            if (Row >= 0)
            {
                var TaxInvoiceNo = Sm.GetGrdStr(Grd3, Row, 5);
                var TaxInvDt = Sm.GetGrdDate(Grd3, Row, 6);
                //var TaxAmt = 0m;

                //if (Sm.GetGrdStr(Grd3, Row, 8).Length > 0)
                //    TaxAmt = Sm.GetGrdDec(Grd3, Row, 8);

                if (TaxInvoiceNo.Length > 0)
                {
                    TxtQRCodeTaxInvoiceNo.EditValue = TaxInvoiceNo;
                    TxtTaxInvoiceNo.EditValue = TaxInvoiceNo;
                }
                else
                    TxtQRCodeTaxInvoiceNo.EditValue = null;

                if (TaxInvDt.Length > 0)
                {
                    TaxInvDt = TaxInvDt.Substring(0, 8);
                    Sm.SetDte(DteQRCodeTaxInvoiceDt, TaxInvDt);
                    Sm.SetDte(DteTaxInvoiceDt, TaxInvDt);
                }
                else
                    DteQRCodeTaxInvoiceDt.EditValue = null;
            }
            var TaxAmt = 0m;
            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                //if (IsOnlyDPTaxDocType)
                //{
                //    if (Sm.CompareStr(mPIQRCodeDPTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                //    {
                //        if (Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                //            TaxAmt += Sm.GetGrdDec(Grd3, r, 8);
                //    }
                //}
                //else
                //{
                //    if (Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                //    {
                //        if (Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                //            TaxAmt += Sm.GetGrdDec(Grd3, r, 8);
                //    }
                //}
                if (Sm.CompareStr(mPIQRCodeDPTaxDocType, Sm.GetGrdStr(Grd3, r, 1)) || Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                {
                    if (Sm.GetGrdStr(Grd3, r, 8).Length > 0) TaxAmt += Sm.GetGrdDec(Grd3, r, 8);
                }
            }
            TxtQRCodeTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
            //}
            ComputeAmt();
        }

        internal void ComputeTaxAmtDifference()
        {
            if (mPIQRCodeTaxDocType.Length == 0)
            {
                TxtTaxAmtDifference.EditValue = Sm.FormatNum(0m, 0);
                return;
            }

            decimal TotalWithTax = 0m, TotalWithoutTax = 0m, Amt2 = 0m;

            if (TxtTotalWithTax.Text.Length > 0) TotalWithTax = decimal.Parse(TxtTotalWithTax.Text);
            if (TxtTotalWithoutTax.Text.Length > 0) TotalWithoutTax = decimal.Parse(TxtTotalWithoutTax.Text);

            if (TxtQRCodeTaxAmt.Text.Length > 0) Amt2 = decimal.Parse(TxtQRCodeTaxAmt.Text);
            TxtTaxAmtDifference.EditValue = Sm.FormatNum(Amt2 - (TotalWithTax-TotalWithoutTax), 0);
        }

        internal void ShowPOTax()
        {
            if (!mIsPIAutoShowPOTax) return;
            if (!(Grd1.Rows.Count > 0 && Sm.GetGrdStr(Grd1, 0, 5).Length > 0))
            {
                LueTaxCode1.EditValue = null;
                LueTaxCode2.EditValue = null;
                LueTaxCode3.EditValue = null;
                return;
            }

            string 
                PODocNo = Sm.GetGrdStr(Grd1, 0, 5),
                TaxCode1 = string.Empty,
                TaxCode2 = string.Empty,
                TaxCode3 = string.Empty;

            GetPOTax(PODocNo, ref TaxCode1, ref TaxCode2, ref TaxCode3);

            if (TaxCode1.Length > 0)
                Sm.SetLue(LueTaxCode1, TaxCode1);
            else
                LueTaxCode1.EditValue = null;

            if (TaxCode2.Length > 0)
                Sm.SetLue(LueTaxCode2, TaxCode2);
            else
                LueTaxCode2.EditValue = null;

            if (TaxCode3.Length > 0)
                Sm.SetLue(LueTaxCode3, TaxCode3);
            else
                LueTaxCode3.EditValue = null;
        }

        private void GetPOTax(string DocNo, ref string TaxCode1, ref string TaxCode2, ref string TaxCode3)
        {   
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    { 
                        Connection = cn,
                        CommandText = 
                            "Select TaxCode1, TaxCode2, TaxCode3 " +
                            "From TblPOHdr Where DocNo=@DocNo;"
                    };
                    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { "TaxCode1", "TaxCode2", "TaxCode3" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            TaxCode1 = Sm.DrStr(dr, 0);
                            TaxCode2 = Sm.DrStr(dr, 1);
                            TaxCode3 = Sm.DrStr(dr, 2);
                        }
                    }
                    dr.Close();
                }     
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }   
        }

        internal void ShowAPDownpaymentInfo(string DocNo)
        {
            for (int r= Grd3.Rows.Count - 1; r >= 0; r--)
            {
                if (Sm.CompareStr(mPIQRCodeDPTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                    Grd3.Rows.RemoveAt(r);
                if (Grd3.Rows.Count <= 0) Grd3.Rows.Add();
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mPIQRCodeDPTaxDocType);
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, @DocType As DocType, B.OptDesc As DocTypeDesc, A.DocNumber, A.Amt, A.TaxAmt, A.DocInd, C.OptDesc As DocIndDesc, A.TaxInvDt, A.QRCode, A.Remark, A.RemarkXml ");
            SQL.AppendLine("From TblAPDownPaymentDtl2 A ");
            SQL.AppendLine("Left Join TblOption B On @DocType=B.OptCode And B.OptCat='PurchaseInvoiceDocType' ");
            SQL.AppendLine("Left Join TblOption C On A.DocInd=C.OptCode And C.OptCat='PurchaseInvoiceDocInd' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DocType In (Select ParValue from TblParameter Where ParCode='PIQRCodeTaxDocType') ");
            SQL.AppendLine("Order By A.DNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                        new string[] 
                        { 
                            //0
                            "DocType", 
                            
                            //1-5
                            "DocTypeDesc", "QRCode", "DocNumber", "TaxInvDt", "Amt", 
                            
                            //6-10
                            "TaxAmt", "DocInd",  "DocIndDesc", "Remark", "RemarkXml" 
                        });
                   
                    int Row = Grd3.Rows.Count-1;
                    Grd3.BeginUpdate();
                    while (dr.Read())
                    {
                        Grd3.Rows.Add();
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("D", Grd3, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd3, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd3, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 12, 10);
                        Row++;
                    }
                    Grd3.EndUpdate();
                    dr.Close();
                    dr.Dispose();
                    cm.Dispose();
                }
            }
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 7, 8 });
            decimal TaxAmt = 0m;
            //bool IsOnlyDPTaxDocType = IsOnlyDPTaxDocTypeExisted();

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                //if (IsOnlyDPTaxDocType)
                //{
                //    if (Sm.CompareStr(mPIQRCodeDPTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                //    {
                //        if (Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                //            TaxAmt += Sm.GetGrdDec(Grd3, r, 8);
                //    }
                //}
                //else
                //{
                //    if (Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                //    {
                //        if (Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                //            TaxAmt += Sm.GetGrdDec(Grd3, r, 8);
                //    }
                //}
                if (Sm.CompareStr(mPIQRCodeDPTaxDocType, Sm.GetGrdStr(Grd3, r, 1)) ||
                    Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                {
                    if (Sm.GetGrdStr(Grd3, r, 8).Length > 0) TaxAmt += Sm.GetGrdDec(Grd3, r, 8);
                }
            }
            TxtQRCodeTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
            ComputeAmt();
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #region additional method FTP
        private void UploadFile(string DocNo, string DNo, string FileName)
        {
            if (IsUploadFileNotValid(FileName, DNo)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                
                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                }
            }
            while (bytesRead != 0);          
            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePIFile(DocNo, DNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid(string FileName, string Dno)
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid(FileName) 
                //IsFileNameAlreadyExisted(FileName)
             ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsPIAllowToUploadFile && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsPIAllowToUploadFile && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsPIAllowToUploadFile && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsPIAllowToUploadFile && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(string FileName)
        {
            if (mIsPIAllowToUploadFile &&  FileName.Length> 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(string FileName)
        {
            if (mIsPIAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where B.FileName=@FileName ");
                SQL.AppendLine("And A.CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }
        #endregion


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode), "");
        }

        private void LueVdCode_Validated(object sender, EventArgs e)
        {
            try
            {
                if (BtnSave.Enabled && TxtDocNo.Text.Length==0)
                {
                    ClearGrd1();
                    ClearGrd2();
                    ClearGrd3();
                    ClearGrd5();
                    ClearGrd6();
                    if (Sm.GetLue(LueVdCode).Length > 0)
                    {
                        ShowVendorDepositSummary(Sm.GetLue(LueVdCode));
                        ShowAPDownpayment();
                    }
                    ComputeAmt();
                    ShowPOTax();
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }   
        }

        private void TxtVdInvNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtVdInvNo);
        }

        private void TxtTaxInvoiceNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo);
        }

        private void TxtTaxInvoiceNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo2);
        }

        private void TxtTaxInvoiceNo3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo3);
        }

        private void LueDocType_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueDocType.Visible = false;
        }

        private void LueDocType_Leave(object sender, EventArgs e)
        {
            if (LueDocType.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueDocType).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 1].Value =
                    Grd3.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueDocType);
                    Grd3.Cells[fCell.RowIndex, 2].Value = LueDocType.GetColumnValue("Col2");
                }
                if (mPIQRCodeTaxDocType.Length > 0) ComputeAmt();
            }
        }

        private void LueDocType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLuePurchaseInvoiceDocType));
        }

        private void LueDocInd_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueDocInd.Visible = false;
        }

        private void LueDocInd_Leave(object sender, EventArgs e)
        {
            if (LueDocInd.Visible && fAccept && fCell.ColIndex == 10)
            {
                if (Sm.GetLue(LueDocInd).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 9].Value =
                    Grd3.Cells[fCell.RowIndex, 10].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 9].Value = Sm.GetLue(LueDocInd);
                    Grd3.Cells[fCell.RowIndex, 10].Value = LueDocInd.GetColumnValue("Col2");
                }
            }
        }

        private void LueDocInd_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueDocInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocInd, new Sm.RefreshLue1(Sl.SetLuePurchaseInvoiceDocInd));
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void TxtTaxRateAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtTaxRateAmt, 0);
                ComputeAmt();
            }
        }

        private void LueTaxCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
            }
        }

        private void TxtDownPayment_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDownPayment, 0);
                ComputeAmt();
            }
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(SetLueDeptCode), string.Empty);
            }
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue2(Sl.SetLueOption), "VoucherPaymentType");
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);    
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue2(Sl.SetLueUserCode), string.Empty);
            }
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
            }
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
            }
        }

        private void LuePaymentType2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType2, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDate2 });

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType2), "B"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDate2, true);
                return;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType2), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType2), "K"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDate2, false);
                return;
            }

            Sm.SetControlReadOnly(LueBankCode, true);
            Sm.SetControlReadOnly(TxtGiroNo, true);
            Sm.SetControlReadOnly(DteDueDate2, true);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtRateAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtRateAmt, 0);
                ComputeAmt2();
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void DteTaxInvDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteTaxInvDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteTaxInvDt, ref fCell, ref fAccept);
        }

        private void DteVdInvDt_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsPIDueDateAutoFill) DteDueDt.Text = null;
        }

        private void ChkCOATaxInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ComputeAmt();
        }

        private void LueServiceCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueServiceCode1, new Sm.RefreshLue1(Sl.SetLueServiceCode));
        }

        private void LueServiceCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueServiceCode2, new Sm.RefreshLue1(Sl.SetLueServiceCode));
        }

        private void LueServiceCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueServiceCode3, new Sm.RefreshLue1(Sl.SetLueServiceCode));
        }

        #endregion

        #region Grid Event

        #region Grid 2 Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPurchaseInvoice3Dlg2(this, Sm.GetLue(LueVdCode)));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (TxtDocNo.Text.Length == 0 && 
                Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length == 0 && 
                (e.ColIndex == 8 || e.ColIndex == 9))
                e.DoDefault = false;
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            ComputeAmt();
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueVdCode, "Vendor")) Sm.FormShowDialog(new FrmPurchaseInvoice3Dlg2(this, Sm.GetLue(LueVdCode)));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 8)
            {
                if (Sm.GetGrdStr(Grd2, e.RowIndex, e.ColIndex).Length == 0)
                    Grd2.Cells[e.RowIndex, e.ColIndex].Value = 0m;
                ComputeAmt();
            }
        }

        #endregion

        #region Grid 3 Event

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2, 5, 6, 7, 10, 11 }, e.ColIndex))
            {
                if (e.ColIndex == 6) Sm.DteRequestEdit(Grd3, DteTaxInvDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 2) LueRequestEdit(Grd3, LueDocType, ref fCell, ref fAccept, e);
                if (e.ColIndex == 10) LueRequestEdit(Grd3, LueDocInd, ref fCell, ref fAccept, e);

                if (e.ColIndex == 15 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmPurchaseInvoice3Dlg4(
                            this, 
                            Sm.GetLue(LueVdCode), 
                            mPIQRCodeDPTaxDocType,
                            mPIQRCodeTaxDocType));
                }

                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 7, 8 });
            }
            if (BtnSave.Enabled &&  e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 3).Length > 0)
            {
                ReadData(e.RowIndex);
            }
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd3, new int[] { 5, 11 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd3, new int[] { 7, 8 }, e);
            if (Sm.IsGrdColSelected(new int[] { 2, 3, 5, 6, 7, 8 }, e.ColIndex))
            {
                if (mPIQRCodeTaxDocType.Length > 0)
                {
                    SetQRCodeInfo();
                    ComputeAmt();
                }
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 3).Length > 0)
                    ReadData(e.RowIndex);
                if (e.ColIndex == 15 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
                    Sm.FormShowDialog(new FrmPurchaseInvoice3Dlg4(
                        this, 
                        Sm.GetLue(LueVdCode), 
                        mPIQRCodeDPTaxDocType,
                        mPIQRCodeTaxDocType));
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd3, e.RowIndex, 16).Length > 0 && mIsEProcUseDigitalInvoice)
            {
                try
                {
                    string remoteUri = string.Concat("http://", mDigitalInvoiceHost, "/", mDigitalInvoiceFolder);
                    string fileName = Sm.GetGrdStr(Grd3, e.RowIndex, 16).Replace("dist/pdf/", ""), myStringWebResource = null;
                    // Create a new WebClient instance.
                    WebClient myWebClient = new WebClient();
                    // Concatenate the domain with the Web resource filename.
                    myStringWebResource = remoteUri + fileName;

                    // Download the Web resource and save it into the current filesystem folder.
                    myWebClient.DownloadFile(myStringWebResource, fileName);
                    Sm.StdMsg(mMsgType.Info, "File successfully downloaded to " + Application.StartupPath);
                }
                catch (Exception ex)
                {
                    Sm.ShowErrorMsg(ex);
                }
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Grd3RemoveRow(Grd3, e, BtnSave);
            SetQRCodeInfo();
            if (mPIQRCodeTaxDocType.Length > 0) ComputeAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 4 Event

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length != 0)
            {
                Grd4.Cells[e.RowIndex, 4].Value = 0;
                //if (Sm.GetGrdStr(Grd4, e.RowIndex, e.ColIndex).Length == 0)
                //    Grd4.Cells[e.RowIndex, e.ColIndex].Value = 0;

                //if (Sm.GetGrdDec(Grd4, e.RowIndex, e.ColIndex) != 0m)
                //    Grd4.Cells[e.RowIndex, (e.ColIndex == 3) ? 4 : 3].Value = 0m;

                ComputeAmt();
                //ComputeAmtWithCOA();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length != 0)
            {
                Grd4.Cells[e.RowIndex, 3].Value = 0;
                //if (Sm.GetGrdStr(Grd4, e.RowIndex, e.ColIndex).Length == 0)
                //    Grd4.Cells[e.RowIndex, e.ColIndex].Value = 0;

                //if (Sm.GetGrdDec(Grd4, e.RowIndex, e.ColIndex) != 0m)
                //    Grd4.Cells[e.RowIndex, (e.ColIndex == 3) ? 4 : 3].Value = 0m;

                ComputeAmt();
                //ComputeAmtWithCOA();             
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPurchaseInvoice3Dlg3(this));
            }
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmPurchaseInvoice3Dlg3(this));
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            ComputeAmt();
            Sm.GrdEnter(Grd4, e);
            Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 5 Event

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) ShowEntitySite(Sm.GetGrdStr(Grd5, e.RowIndex, 0));
            }
        }

        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2) ShowEntitySite(Sm.GetGrdStr(Grd5, e.RowIndex, 0));
        }

        #endregion

        #region Grid 6 Event

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd6, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmAPDownpayment2(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd6, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd6, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAPDownpayment2(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd6, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #endregion

        #region Class

        private class RejectedRecv
        {
            public string RecvVdDocNo { get; set; }
            public string RecvVdDNo { get; set; }
            public string RejectedInd { get; set; }
        }
        private class InvHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string DueDate { get; set; }
            public string Remark { get; set; }
            public string VdInvDt { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal DownPayment { get; set; }
            public decimal Discount { get; set; }
            public string UserCode { get; set; }
            public string LocalDocNo { get; set; }
            public decimal AcAmt { get; set; }
            public string TitleInd { get; set; }
            public string PrintBy { get; set; }
            public string VdInvNo { get; set; }
            public string AddressVd { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public string CurCode { get; set; }
            public string CurName { get; set; }
            public string Terbilang { get; set; }
            public decimal DeliveryCost { get; set; }
            public decimal InvoiceAmt { get; set; }
            public decimal Subtotal { get; set; }
            public decimal TotalTax2 { get; set; }
        }

        private class InvDtl
        {
            public string RecvVdDocNo { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal QtyPurchase { get; set; }
            public string PurchaseUomCode { get; set; }
            public string PtName { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Total { get; set; }
            public string Remark { get; set; }
            public decimal Discount { get; set; }
            public int nomor { get; set; }
            public decimal PtDay { get; set; }
            public decimal UPAD { get; set; }
            public decimal UPBD { get; set; }
            public string PONo { get; set; }
            public string RecvVdDocDt { get; set; }
            public string TaxName { get; set; }
            public string PtDay2 { get; set; }
            public string ItCodeInternal { get; set; }
            public string Specification { get; set; }
        }

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        private class InvDtl2
        {
            public string AmtType { get; set; }
            public decimal Amt { get; set; }
        }

        private class DepositSummary
        {
            public decimal ExcRate { get; set; }
            public decimal Amt { get; set; }
            public decimal UsedAmt { get; set; }
        }

        private class InvDtl3
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string LastUpDt { get; set; }
            public string PosName { get; set; }

        }

        private class InvDtl4
        {

            public string AcDesc { get; set; }
            public string AcDesc2 { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
 
        }

        private class PORDept
        {
            public string PODocNo { get; set; }
            public string PORDocNo { get; set; }
            public string DeptCode { get; set; }
        }

        private class PISignIMS
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class PISignIMS2
        {
            public string UserName { get; set; }
            public string UserName2 { get; set; }
            public string PosName { get; set; }
            public string PosName2 { get; set; }
        }

        private class PIJournal
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
        }

        #endregion
    }
}
