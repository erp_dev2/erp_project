﻿namespace RunSystem
{
    partial class FrmRptPeriodicCostDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.TxtCCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtCCCodeChild = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCCCodeParent = new DevExpress.XtraEditors.TextEdit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCodeChild.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCodeParent.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 77);
            this.panel3.Size = new System.Drawing.Size(710, 316);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.ReadOnly = true;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(710, 316);
            this.Grd1.TabIndex = 17;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtCCtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtCCCodeChild);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtCCCodeParent);
            this.panel2.Size = new System.Drawing.Size(710, 77);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(41, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Cost Category";
            // 
            // TxtCCtCode
            // 
            this.TxtCCtCode.EnterMoveNextControl = true;
            this.TxtCCtCode.Location = new System.Drawing.Point(129, 50);
            this.TxtCCtCode.Name = "TxtCCtCode";
            this.TxtCCtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCCtCode.Properties.MaxLength = 30;
            this.TxtCCtCode.Properties.ReadOnly = true;
            this.TxtCCtCode.Size = new System.Drawing.Size(323, 20);
            this.TxtCCtCode.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Cost Center Child";
            // 
            // TxtCCCodeChild
            // 
            this.TxtCCCodeChild.EnterMoveNextControl = true;
            this.TxtCCCodeChild.Location = new System.Drawing.Point(129, 28);
            this.TxtCCCodeChild.Name = "TxtCCCodeChild";
            this.TxtCCCodeChild.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCCCodeChild.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCCodeChild.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCCodeChild.Properties.Appearance.Options.UseFont = true;
            this.TxtCCCodeChild.Properties.MaxLength = 30;
            this.TxtCCCodeChild.Properties.ReadOnly = true;
            this.TxtCCCodeChild.Size = new System.Drawing.Size(323, 20);
            this.TxtCCCodeChild.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 14);
            this.label6.TabIndex = 11;
            this.label6.Text = "Cost Center Parent";
            // 
            // TxtCCCodeParent
            // 
            this.TxtCCCodeParent.EnterMoveNextControl = true;
            this.TxtCCCodeParent.Location = new System.Drawing.Point(129, 6);
            this.TxtCCCodeParent.Name = "TxtCCCodeParent";
            this.TxtCCCodeParent.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCCCodeParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCCodeParent.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCCodeParent.Properties.Appearance.Options.UseFont = true;
            this.TxtCCCodeParent.Properties.MaxLength = 30;
            this.TxtCCCodeParent.Properties.ReadOnly = true;
            this.TxtCCCodeParent.Size = new System.Drawing.Size(323, 20);
            this.TxtCCCodeParent.TabIndex = 12;
            // 
            // FrmRptPeriodicCostDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 393);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmRptPeriodicCostDlg";
            this.Text = "DO To Customer Information";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCodeChild.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCodeParent.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.TextEdit TxtCCtCode;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.TextEdit TxtCCCodeChild;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.TextEdit TxtCCCodeParent;
    }
}