﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPayrollAuthorizationFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPayrollAuthorization mFrmParent;

        #endregion

        #region Constructor

        public FrmPayrollAuthorizationFind(FrmPayrollAuthorization FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "User Name",
                        "User Code",
                        "Grade Level"+Environment.NewLine+"Code",
                        "Grade Level"+Environment.NewLine+"Name",
                        "Grade Level"+Environment.NewLine+"Group Name",
                        
                        //6-10
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 7, 10 });
            Sm.GrdFormatTime(Grd1, new int[] { 8, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 7, 8, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 7, 8, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtUserCode.Text, new string[] { "C.UserCode", "D.UserName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select C.UserCode, D.UserName, A.GrdLvlCode, A.GrdLvlName, B.GrdLvlGrpName, C.CreateBy, C.CreateDt, C.LastUpBy, C.LastUpDt  " +
                        "From TblGradeLevelHdr A " +
                        "Inner Join TblGradeLevelGroup B On A.GrdLvlGrpCode = B.GrdLvlGrpCode " +
                        "Inner Join TblPayrollAuthorization C On A.GrdLvlCode = C.GrdLvlCode "+
                        "Inner Join TblUser D On C.UserCode = D.UserCode " +
                        Filter + " Order By D.UserName ",
                        new string[]
                        {
                            //0
                            "UserCode",
                                
                            //1-5
                            "UserName", "GrdLvlCode", "GrdLvlName", "GrdLvlGrpName", "CreateBy", 
                            
                            //6-8
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 11, 8);
                        }, true, true, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 2))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2));
                this.Hide();
            }
        }

        #endregion


        #region Event
        private void TxtGrdLvlCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkGrdLvlCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "User");

        }
        #endregion
    }
}
