﻿#region Update
/*
    02/03/2022 [DITA/PHT] New Apps
    10/03/2022 [IBL/PHT] tambah printout
    11/04/2022 [DITA/PHT] ubah multi filter profit center jadi profit center 1 dan 2
    11/04/2022 [DITA/PHT] penyesuaian tampilan data journal interoffice based on filter nya, debit credit disesuaikan biar balance
    25/04/2022 [DITA/PHT] ambil setlue profit center dari SetLue
    26/04/2022 [DITA/PHT] tampilan profit center di printout nya disesuaikan dengan combobox yg baru
    26/04/2022 [DITA/PHT] kolom remark minta ditampilkan dari remark di journal hdr saja
    23/05/2022 [TYO/PHT] saat refresh langsung terfilter berdasarkan account description dan profit center.
    10/06/2022 [DITA/PHT] tambah validasi cluster no untuk menambahkan jurnal payroll di reporting rak
    15/03/2023 [WED/PHT] rubah ordering PrepData
    28/03/2023 [WED/PHT] tambah DNo dari table Dtl3, lalu di order by TransactionDocNo dan DNo nya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using System.Collections;

#endregion

namespace RunSystem
{
    public partial class FrmRptJournalInterOffice : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;
        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mMainCurCode = string.Empty;
        private bool mIsAllProfitCenterSelected = false;


        #endregion

        #region Constructor

        public FrmRptJournalInterOffice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueProfitCenterCode(ref LueProfitCenterCode, string.Empty, "Y");
                Sl.SetLueProfitCenterCode(ref LueProfitCenterCode2,string.Empty, "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Date",
                        "Document#",
                        "Profit Center",
                        "From",
                        "To",
                        
                        //6-10
                        "Account#",
                        "Account Description",
                        "Debit",
                        "Credit",
                        "Balance",
                        
                        //11-12
                        "Remark",
                        "Transaction",
                       
                    },
                    new int[]
                    {
                        //0
                        50,
                        //1-5
                        100, 150, 200, 200, 200, 
                        //6-10
                        150, 200, 150, 150, 150, 
                        //11-12
                        200, 200, 
                        
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] {  8, 9, 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 12 }, false);

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
           Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                Sm.IsLueEmpty(LueProfitCenterCode, "Profit Center 1") ||
                Sm.IsLueEmpty(LueProfitCenterCode2, "Profit Center 2") ||
                IsProfitCenterFilterInvalid()
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Sm.ClearGrd(Grd1, false);
                var l = new List<JournalInterOffice>();

                PrepData(ref l);
                if(l.Count > 0)
                {
                    ProcessFromTo(ref l);
                    ProcessProfitCenter(ref l);
                    ProcessDebitCredit(ref l);
                    ProcessAllData(ref l);
                }
                if(Grd1.Rows.Count <= 1 || l.Count == 0) Sm.StdMsg(mMsgType.NoData, string.Empty);
                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void PrepData(ref List<JournalInterOffice> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select* ");
            SQL.AppendLine("From( ");
            SQL.AppendLine("    Select A.DocDt, A.DocNo, F.ProfitCenterCode, F.ProfitCenterName, ");
            SQL.AppendLine("    B.AcNo, G.AcDesc, B.DAmt, B.CAmt, A.Remark, H.OptDesc DocType, D.DocNo TransactionDocNo, C.ClusterNo, C.DNo ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And B.AcNo In( ");
            SQL.AppendLine("            Select AcNo ");
            SQL.AppendLine("            From TblCOAInterOfficeHdr T1 ");
            SQL.AppendLine("            Inner Join TblCOAInterOfficeDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.CancelInd = 'N' And T1.CancelReason Is Null ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Inner Join TblVoucherDtl3 C On A.DocNo = C.JournalDocNo ");
            SQL.AppendLine("    Inner Join TblVoucherHdr D On C.DocNo = D.DocNo ");
            SQL.AppendLine("    And D.CancelInd = 'N' And D.CancelReason Is Null ");
            SQL.AppendLine("    Inner Join TblCostCenter E On A.CCCode = E.CCCode ");
            SQL.AppendLine("    Inner Join TblProfitCenter F On E.ProfitCenterCode = F.ProfitCenterCode ");
            SQL.AppendLine("    Inner Join TblCOA G On B.AcNo = G.AcNo ");
            SQL.AppendLine("    Inner Join TblOption H On D.DocType = H.OptCode And OptCat = 'VoucherDoctype' ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select A.DocDt, A.DocNo, F.ProfitCenterCode, F.ProfitCenterName, ");
            SQL.AppendLine("    B.AcNo, G.AcDesc, B.DAmt, B.CAmt, A.Remark, 'Journal Inter-Office' As DocType, D.DocNo TransactionDocNo, Null As ClusterNo, C.DNo ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And B.AcNo In ( ");
            SQL.AppendLine("            Select AcNo ");
            SQL.AppendLine("            From TblCOAInterOfficeHdr T1 ");
            SQL.AppendLine("            Inner Join TblCOAInterOfficeDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.CancelInd = 'N' And T1.CancelReason Is Null ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Inner Join TblJournalInterOfficeDtl3 C On A.DocNo = C.JournalDocNo ");
            SQL.AppendLine("    Inner Join TblJournalInterOfficeHdr D On C.DocNo = D.DocNo ");
            SQL.AppendLine("    And D.CancelInd = 'N' And D.CancelReason Is Null ");
            SQL.AppendLine("    Inner Join TblCostCenter E On A.CCCode = E.CCCode ");
            SQL.AppendLine("    Inner Join TblProfitCenter F On E.ProfitCenterCode = F.ProfitCenterCode ");
            SQL.AppendLine("    Inner Join TblCOA G On B.AcNo = G.AcNo ");

            SQL.AppendLine(") T ");
            //SQL.AppendLine("Where DocDt Between @DocDt1 And @DocDt2  ");
            //SQL.AppendLine("Group By DocDt, ProfitCenterCode, ProfitCenterName, AcNo, AcDesc, Remark, DocType, TransactionDocNo, ClusterNo ");
            SQL.AppendLine("Order By T.TransactionDocNo, T.DNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL.ToString() };
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] {
                    //0
                    "DocDt",

                    //1-5
                    "DocNo",
                    "ProfitCenterCode",
                    "ProfitCenterName",
                    "AcNo",
                    "AcDesc",

                    //6-10
                    "DAmt",
                    "CAmt",
                    "Remark",
                    "DocType",
                    "TransactionDocNo",

                    //11
                    "ClusterNo"

                });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        l.Add(new JournalInterOffice()
                        {
                            DocDt = Sm.DrStr(dr, c[0]),
                            DocNo = Sm.DrStr(dr, c[1]),
                            ProfitCenterCode = Sm.DrStr(dr, c[2]),
                            ProfitCenterName = Sm.DrStr(dr, c[3]),
                            AcNo = Sm.DrStr(dr, c[4]),
                            AcDesc = Sm.DrStr(dr, c[5]),
                            DAmt = Sm.DrDec(dr, c[6]),
                            CAmt = Sm.DrDec(dr, c[7]),
                            Balance = Sm.DrDec(dr, c[6]) - Sm.DrDec(dr, c[7]),
                            Remark = Sm.DrStr(dr, c[8]),
                            DocType = Sm.DrStr(dr, c[9]),
                            TransactionDocNo = Sm.DrStr(dr, c[10]),
                            From = string.Empty,
                            To = string.Empty,
                            Flag = "N",
                            ProfitCenterFlag = "N",
                            ClusterNo = Sm.DrStr(dr, c[11]),

                        });
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ProcessFromTo(ref List<JournalInterOffice> l)
        {
            string CurrDocNo = string.Empty, From = string.Empty, To = string.Empty, CurrClusterNo = string.Empty;

            var lastData = l.Last();
            foreach (var y in l)
            {
                if (CurrDocNo.Length == 0)
                {
                    CurrDocNo = y.TransactionDocNo; CurrClusterNo = y.ClusterNo;
                }

                if (CurrDocNo != y.TransactionDocNo || y == lastData || (CurrClusterNo != y.ClusterNo))
                {
                    if (y == lastData) To = y.ProfitCenterName;
                    foreach (var x in l.Where(w=>(w.TransactionDocNo == CurrDocNo && w.ClusterNo == CurrClusterNo)))
                    {
                        x.To = To;
                    }
                    CurrDocNo = y.TransactionDocNo;
                    CurrClusterNo = y.ClusterNo;
                    if (y != lastData) From = string.Empty;
                }
                
                if (From.Length == 0) From = y.ProfitCenterName;
                
                y.From = From;
                To = y.ProfitCenterName;
                CurrClusterNo = y.ClusterNo;
            }
        }

        private void ProcessProfitCenter(ref List<JournalInterOffice> l)
        {
            string 
                mFlag = string.Empty, mFrom = string.Empty,
                mTo = string.Empty, mTransactionDocNo = string.Empty, mClusterNo = string.Empty;
            bool mRun = false;
            bool IsFirst = true;
            
            foreach (var y in l)
            {
                if (IsFirst)
                {
                    mFrom = y.From;
                    mTo = y.To;
                    mTransactionDocNo = y.TransactionDocNo;
                    mClusterNo = y.ClusterNo;

                    IsFirst = false;
                }

                if (y.TransactionDocNo != mTransactionDocNo || 
                    (y.TransactionDocNo == mTransactionDocNo && y.ClusterNo != mClusterNo && y.ClusterNo.Trim().Length > 0)
                    )
                {
                    mFrom = y.From;
                    mTo = y.To;
                    mTransactionDocNo = y.TransactionDocNo;
                    mClusterNo = y.ClusterNo;
                    mFlag = string.Empty;
                    mRun = false;
                }

                if (!mRun)
                {
                    foreach (var x in l.Where(w => 
                    w.From == mFrom && w.To == mTo && 
                    w.TransactionDocNo == mTransactionDocNo && 
                    w.ProfitCenterCode == Sm.GetLue(LueProfitCenterCode) &&
                    w.ClusterNo == mClusterNo
                    ).GroupBy(x => x.ProfitCenterCode))
                    {
                        mFlag = "1";
                    }

                    foreach (var z in l.Where(w => 
                    w.From == mFrom && w.To == mTo && 
                    w.TransactionDocNo == mTransactionDocNo && 
                    w.ProfitCenterCode == Sm.GetLue(LueProfitCenterCode2) &&
                    w.ClusterNo == mClusterNo
                    ).GroupBy(x => x.ProfitCenterCode))
                    {
                        if (mFlag == "1") mFlag = "2";
                        else mFlag = "1";
                    }
                    mRun = true;
                }

                if (y.From == mFrom && y.To == mTo && y.TransactionDocNo == mTransactionDocNo &&
                    ((y.ProfitCenterCode == Sm.GetLue(LueProfitCenterCode)) || y.ProfitCenterCode == Sm.GetLue(LueProfitCenterCode2)) &&
                    mClusterNo == y.ClusterNo
                    && mFlag == "2") y.ProfitCenterFlag = "Y";
                
            }
        }

        private void ProcessDebitCredit(ref List<JournalInterOffice> l)
        {
            string mDocNo = string.Empty, mDocNo2 = string.Empty, mDocNo3 = string.Empty, mDocNo4 = string.Empty,
                mFrom = string.Empty, mTo = string.Empty, mTransactionDocNo = string.Empty, mClusterNo = string.Empty;
            bool mRun = false, IsFirst = true;
            decimal[] DAmt = new decimal[20], CAmt = new decimal[20], DAmt2 = new decimal[20], CAmt2 = new decimal[20] ;
            int mFlag = 0, mFlag2 = 0;

            foreach (var y in l.Where(w => w.ProfitCenterFlag == "Y"))
            {
                if (IsFirst)
                {
                    mFrom = y.From;
                    mTo = y.To;
                    mTransactionDocNo = y.TransactionDocNo;
                    mDocNo3 = y.DocNo;
                    mClusterNo = y.ClusterNo;

                    IsFirst = false;
                }

                if (y.TransactionDocNo != mTransactionDocNo ||
                    (y.TransactionDocNo == mTransactionDocNo && mClusterNo != y.ClusterNo && mClusterNo.Trim().Length > 0))
                {
                    mFrom = y.From;
                    mTo = y.To;
                    mTransactionDocNo = y.TransactionDocNo;
                    mFlag = 0;
                    mFlag2 = 0;
                    mRun = false;
                    DAmt = new decimal[20];
                    CAmt = new decimal[20];
                    DAmt2 = new decimal[20];
                    CAmt2 = new decimal[20];
                    mDocNo3 = y.DocNo;
                    mClusterNo = y.ClusterNo;
                }

                if (!mRun)
                {
                    int i = 0;
                    foreach (var x in l.Where(w => w.From == mFrom && w.To == mTo && 
                    w.TransactionDocNo == mTransactionDocNo && 
                    w.ProfitCenterCode == Sm.GetLue(LueProfitCenterCode) &&
                    mClusterNo == w.ClusterNo
                    ))
                    {
                        mFlag = i + 1;
                        DAmt[i] = x.DAmt;
                        CAmt[i] = x.CAmt;
                        if(mFlag == 1)
                        {
                            mDocNo4 = x.DocNo;
                            mDocNo = x.DocNo;
                        }
                        else if(mDocNo3 == mDocNo4) mDocNo = x.DocNo;
                        i++;
                    }

                    i = 0;
                    foreach (var z in l.Where(w => w.From == mFrom && w.To == mTo && 
                    w.TransactionDocNo == mTransactionDocNo && 
                    w.ProfitCenterCode == Sm.GetLue(LueProfitCenterCode2) &&
                    mClusterNo == w.ClusterNo
                    ))
                    {
                        mFlag2 = i + 1;
                        DAmt2[i] = z.DAmt;
                        CAmt2[i] = z.CAmt;
                        if (mFlag2 == 1) mDocNo2 = z.DocNo;
                        else if (mDocNo3 != mDocNo4) mDocNo2 = z.DocNo;
                        i++;
                    }
                    mRun = true;
                }


                if (y.From == mFrom && y.To == mTo && y.TransactionDocNo == mTransactionDocNo && 
                    ((mFlag == 1 && y.DocNo == mDocNo) || (mFlag2 == 1 && y.DocNo == mDocNo2)) &&
                    mClusterNo == y.ClusterNo
                    ) y.Flag = "Y";

                if (y.From == mFrom && y.To == mTo && y.TransactionDocNo == mTransactionDocNo && mClusterNo == y.ClusterNo && (mFlag > 1 || mFlag2 > 1))
                {
                    if (mFlag > 1 && mFlag2 == 1)
                    {
                        if (DAmt2[0] == 0)
                        {
                            if (y.DAmt != 0m && y.DocNo == mDocNo) y.Flag = "Y";
                        }
                        if (CAmt2[0] == 0)
                        {
                            if (y.CAmt != 0m && y.DocNo == mDocNo) y.Flag = "Y";
                        }
                    }
                    if (mFlag == 1 && mFlag2 > 1)
                    {
                        if (DAmt[0] == 0)
                        {
                            if (y.DAmt != 0m && y.DocNo == mDocNo2) y.Flag = "Y";
                        }
                        if (CAmt[0] == 0)
                        {
                            if (y.CAmt != 0m && y.DocNo == mDocNo2) y.Flag = "Y";
                        }
                    }
                    if (mFlag > 1 && mFlag2 > 1)
                    {
                        if (y.DocNo == mDocNo || y.DocNo == mDocNo2) y.Flag = "Y";
                    }
                }

            }
        }

        private void ProcessAllData(ref List<JournalInterOffice> l)
        {
            Sm.ClearGrd(Grd1, false);
            Grd1.BeginUpdate();
            int Row = 0;
            foreach (var x in l.Where(w=>w.Flag == "Y" && (ChkDocNo.Checked ? w.DocNo.Contains(TxtDocNo.Text) : w.DocNo.Contains("/"))))
            {
                Grd1.Rows.Add();
                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Row + 1;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = Sm.ConvertDate(x.DocDt);
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.DocNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = x.ProfitCenterName;
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = x.From;
                Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = x.To;
                Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = x.AcNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 7].Value = x.AcDesc;
                Grd1.Cells[Grd1.Rows.Count - 1, 8].Value = Sm.FormatNum(x.DAmt, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 9].Value = Sm.FormatNum(x.CAmt, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 10].Value = Sm.FormatNum(x.Balance, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 11].Value = x.Remark;
                Grd1.Cells[Grd1.Rows.Count - 1, 12].Value = x.DocType;

                Row += 1;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] {8, 9, 10});
            Grd1.EndUpdate();

            Grd1.GroupObject.Add(7);
            Grd1.GroupObject.Add(3);
            Grd1.Group();
        }

        protected override void PrintData()
        {
            var l = new List<ProfitCenterFromTo>();
            var l1 = new List<JournalInterOfficeHdr>();
            var l2 = new List<JournalInterOfficeDtl>();

            string[] TableName = { "ProfitCenterFromTo", "JournalInterOfficeHdr", "JournalInterOfficeDtl" };
            List<IList> myLists = new List<IList>();

            string PrintBy =
                Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}",
                Sm.ConvertDateTime(Sm.ServerCurrentDateTime()));

            #region ProfitCenterFromTo

            //string[] mProfitCenters = CcbProfitCenterCode.Text.Split(',');

            l.Add(new ProfitCenterFromTo()
            {
                ProfitCenterFrom = LueProfitCenterCode.Text,
                ProfitCenterTo = LueProfitCenterCode2.Text,
            });

            #endregion

            #region Header
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Min(A.ProfitCenterCode) As ProfitCenterCode, A.ProfitCenterName, ");
            SQL.AppendLine("Concat(Right(@DocDt,2),' ', ");
            SQL.AppendLine("Case Substring(@DocDt, 5, 2) ");
            SQL.AppendLine("	When '01' Then 'Januari' When '02' Then 'Februari' When '03' Then 'Maret' When '04' Then 'April' ");
            SQL.AppendLine("	When '05' Then 'Mei' When '06' Then 'Juni' When '07' Then 'Juli' When '08' Then 'Agustus' ");
            SQL.AppendLine("	When '09' Then 'September' When '10' Then 'Oktober' When '11' Then 'November' When '12' Then 'Desember' ");
            SQL.AppendLine("End, ' ', Left(@DocDt, 4)) As DocDt ");
            SQL.AppendLine("From TblProfitCenter A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	Select Min(Level) As Level From TblProfitCenter  ");
            SQL.AppendLine("	Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
            SQL.AppendLine(")B On A.Level = B.Level ");
            SQL.AppendLine("Where Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", string.Concat(Sm.GetLue(LueProfitCenterCode), ',', Sm.GetLue(LueProfitCenterCode2)));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocDt", "ProfitCenterName" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l1.Add(new JournalInterOfficeHdr()
                        {
                            DocDt = Sm.DrStr(dr, c[0]),
                            ProfitCenterName = Sm.DrStr(dr, c[1]),
                            PrintBy = PrintBy,
                        });
                    }
                }
                dr.Close();
            }

            #endregion

            #region Detail

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    l2.Add(new JournalInterOfficeDtl()
                    {
                        DocDt =  Sm.GetGrdText(Grd1, i, 1),
                        DocNo = Sm.GetGrdStr(Grd1, i, 2),
                        ProfitCenterNo = (Sm.GetGrdStr(Grd1, i, 3) == LueProfitCenterCode.Text ? "A" : "B"),
                        ProfitCenterName = Sm.GetGrdStr(Grd1, i, 3),
                        AcNo = Sm.GetGrdStr(Grd1, i, 6),
                        AcDesc = Sm.GetGrdStr(Grd1, i, 7),
                        DAmt = Sm.GetGrdDec(Grd1, i, 8),
                        CAmt = Sm.GetGrdDec(Grd1, i, 9),
                        Balance = Sm.GetGrdDec(Grd1, i, 10),
                        Remark = Sm.GetGrdStr(Grd1, i, 11),
                    });
                }
            }

            #endregion

            #region Add List
            myLists.Add(l);
            myLists.Add(l1);
            myLists.Add(l2);
            #endregion

            Sm.PrintReport("RptJournalInterOffice", myLists, TableName, false);
            l.Clear(); l1.Clear(); l2.Clear();
        }

        #region Additional Method

        private bool IsProfitCenterFilterInvalid()
        {
            
            string mProfitCenterCode = string.Empty, mProfitCenterCode2 = string.Empty;

            mProfitCenterCode = Sm.GetLue(LueProfitCenterCode);
            mProfitCenterCode2 = Sm.GetLue(LueProfitCenterCode2);

            if (mProfitCenterCode.Length > 0 || mProfitCenterCode2.Length > 0)
            {
                if (mProfitCenterCode == mProfitCenterCode2)
                {
                    Sm.StdMsg(mMsgType.Warning, "Both Profit Center could not be the same.");
                    LueProfitCenterCode.Focus();
                    return true;
                }
            }
            return false;
        }

       
        #endregion 

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfitCenterCode, new Sm.RefreshLue3(Sl.SetLueProfitCenterCode), string.Empty, "Y");
        }

        private void LueProfitCenterCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfitCenterCode2, new Sm.RefreshLue3(Sl.SetLueProfitCenterCode), string.Empty, "Y");
        }

        #endregion

        #region Class

        private class JournalInterOffice
        {
            public string DocDt { get; set; }
            public string DocNo { get; set; }
            public string ProfitCenterCode { get; set; }
            public string ProfitCenterName { get; set; }
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal Balance { get; set; }
            public string Remark { get; set; }
            public string DocType { get; set; }
            public string TransactionDocNo { get; set; }
            public string From { get; set; }
            public string To { get; set; }
            public string Flag { get; set; }
            public string ClusterNo { get; set; }
            public string ProfitCenterFlag { get; set; }
        }

      

        #endregion

        #region Report Class

        private class ProfitCenterFromTo
        {
            public string ProfitCenterFrom { get; set; }
            public string ProfitCenterTo { get; set; }
        }

        private class JournalInterOfficeHdr
        {
            public string DocDt { get; set; }
            public string ProfitCenterName { get; set; }
            public string PrintBy { get; set; }
        }

        private class JournalInterOfficeDtl
        {
            public string DocDt { get; set; }
            public string DocNo { get; set; }
            public string ProfitCenterNo { get; set; }
            public string ProfitCenterName { get; set; }
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal Balance { get; set; }
            public string Remark { get; set; }
        }

        #endregion
    }
}
