﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptRecvForestProducts : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string DocDt = string.Empty;

        #endregion

        #region Constructor

        public FrmRptRecvForestProducts(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();

            string CurrentDateTime = Sm.ServerCurrentDateTime();

            Sl.SetLueMth(LueMth);
            Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
            Sl.SetLueYr(LueYr, "");
            Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

            base.FrmLoad(sender, e);
            BtnChart.Visible = (Sm.GetValue("Select IfNull(ParValue, '') from tblparameter where ParCode='ChartAvailable'") == "Yes");
        }

        override protected void SetSQL()
        {
            string ItCtRMPActual = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ItCtRMPActual'");
            string ItCtRMPActual2 = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ItCtRMPActual2'");
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocNo, 'XXXX' As Noseri, A.DocDt, D.ItCode, D.ItName, "); 
			SQL.AppendLine("	  Concat(D.ItName, ' ( ', ");
            SQL.AppendLine("                Case D.ItCtCode  ");
            SQL.AppendLine("                    When '"+ItCtRMPActual+"' Then "); 
            SQL.AppendLine("                        Concat( ");
            SQL.AppendLine("                        'L: ', Convert(Format(D.Length, 2) Using utf8), ");
            SQL.AppendLine("                        ' D: ', Convert(Format(D.Diameter, 2) Using utf8) ");
            SQL.AppendLine("                       ) ");
            SQL.AppendLine("                    When '"+ItCtRMPActual2+"' Then ");    
            SQL.AppendLine("                        Concat( ");
            SQL.AppendLine("                        'L: ', Convert(Format(D.Length, 2) Using utf8) ,  ");
            SQL.AppendLine("                        ' W: ', Convert(Format(D.Width, 2) Using utf8), ");
            SQL.AppendLine("                        ', H: ', Convert(Format(D.Height, 2) Using utf8) ");
            SQL.AppendLine("                        ) ");
            SQL.AppendLine("                End, ");
            SQL.AppendLine("            ' )') As Item, ");     
            SQL.AppendLine("            SUM(C.Qty) As Qty, ");    
			SQL.AppendLine("	case (Left(D.ItCode, 3))  ");
            SQL.AppendLine("When (Select Parvalue From TblParameter Where ParCode='ItCtRMP') then truncate(((((D.Diameter/100)*(D.Diameter/100)) * D.Length/100*22/7/4)*SUM(C.Qty)), 8) ");
            SQL.AppendLine("When (Select Parvalue From TblParameter Where ParCode='ItCtRMPActual') then truncate(((((D.Diameter/100)*(D.Diameter/100)) * D.Length/100*22/7/4)*SUM(C.Qty)), 8) ");    
            SQL.AppendLine("When (Select Parvalue From TblParameter Where ParCode='ItCtRMP2') then ((D.Length*D.Width*D.height)*SUM(C.Qty)) ");
            SQL.AppendLine("When (Select Parvalue From TblParameter Where ParCode='ItCtRMPActual2') then ((D.Length*D.Width*D.height)*SUM(C.Qty)) ");    
            SQL.AppendLine("End As Volume, ");
			SQL.AppendLine("	T1.VdCode2, Concat(F.VdName, ', ', F.Address) As Address, Concat(G.TTName, ' ', T1.VehicleRegNo) As Plat  ");
            SQL.AppendLine("From TblRecvRawMaterialHdr A  ");
            SQL.AppendLine("Inner Join TblRecvrawMaterialDtl2 C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
			SQL.AppendLine("	Left Join ");
            SQL.AppendLine("(Select T.DocNo, T.VehicleRegNo, VdCode2, ");
			SQL.AppendLine("	(Select DocNo From TblLoadingQueue ");
			SQL.AppendLine("	Where DocNo = T.QueueNo And Status = 'O') As Queue, ");
			SQL.AppendLine("	(Select TTCode From TblLoadingQueue ");
			SQL.AppendLine("	Where DocNo = T.QueueNo And Status = 'O') As TTCode ");
			SQL.AppendLine("	From TblLegalDocVerifyHdr T ");
			SQL.AppendLine("	Where CancelInd = 'N' And ProcessInd1 = 'F' And ProcessInd2 = 'F' ");
			SQL.AppendLine("	)T1 On T1.DocNo = A.LegalDocVerifyDocNo ");
			SQL.AppendLine("	Left Join TblVendor F On T1.VdCode2 = F.VdCode "); 
			SQL.AppendLine("	Left Join TblTransportType G On T1.TTCode = G.TTCode ");
            SQL.AppendLine("Where A.ProcessInd = 'F' And A.CancelInd = 'N' ");
            SQL.AppendLine("Group By C.ItCode ");
            SQL.AppendLine(")T ");
            SQL.AppendLine("Where T.MonthFilter = @MonthFilter ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item",
                        "Dokumen", 
                        "Nomor Seri",
                        "Tanggal",
                        "Jenis Kayu",   
                        

                        //6-10
                        "Jumlah",
                        "Volume",
                        "Pengirim "+Environment.NewLine+" Tempat Muat", 
                        "Jenis "+Environment.NewLine+" Alat Angkut", 
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 50, 100, 150, 100,
                        
                        //6-10
                        100, 100, 300, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] {  }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
            DocDt = string.Concat(Year, Month);

            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
            }
            else
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " And 0=0 ";

                    var cm = new MySqlCommand();
                    Sm.CmParam<String>(ref cm, "@MonthFilter", DocDt);

                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + "",
                            new string[]
                        { 
                            //0
                            "ItName",

                            //1-5
                             "DocNo", "NoSeri", "DocDt", "Item", "Qty",  
                            
                            //6-10
                            "Volume", "Address", "Plat", 
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd1.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);

                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);

                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                                
                            }, true, false, false, false
                        );
                    Grd1.GroupObject.Add(1);
                    Grd1.Group();
                    AdjustSubtotals();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }
        }
      
        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 6 });
        }

        #endregion

        #endregion
    }
}
