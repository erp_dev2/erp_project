﻿#region Update
/*
    28/11/2022 [SET/BBT] Manu Baru
    16/11/2022 [SET/BBT] Bug data tidak muncul
    27/12/2022 [SET/BBT] Data yang muncul 1 baris 1 DocNo
    28/12/2022 [MAU/BBT] Memunculkan kolom status 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryCostFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPropertyInventoryCost mFrmParent;
        private string mSQL = string.Empty;


        #endregion

        #region Constructor

        public FrmPropertyInventoryCostFind(FrmPropertyInventoryCost FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.CancelInd,  ");
            SQL.AppendLine("    Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As Status, ");
            SQL.AppendLine("    A.PropertyCode, C.PropertyName, E.SiteName, A.CostComponentAmt, F.OptDesc Type, A.CostComponentReason, D.PropertyCategoryName,");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCostComponentDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryHdr C On A.PropertyCode = C.PropertyCode ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCategory D On C.PropertyCategoryCode = D.PropertyCategoryCode ");
            SQL.AppendLine("    Inner Join TblSite E On C.SiteCode = E.SiteCode ");
            SQL.AppendLine("    Inner Join TblOption F On F.OptCat = 'PropertyInventoryDocType' And B.DocType = F.OptCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("    Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As Status, ");

            SQL.AppendLine("    A.PropertyCode, C.PropertyName, E.SiteName, A.CostComponentAmt, F.OptDesc Type, A.CostComponentReason, D.PropertyCategoryName,");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCostComponentDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryHdr C On A.PropertyCode = C.PropertyCode ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCategory D On C.PropertyCategoryCode = D.PropertyCategoryCode ");
            SQL.AppendLine("    Inner Join TblSite E On C.SiteCode = E.SiteCode ");
            SQL.AppendLine("    Inner Join TblOption F On F.OptCat = 'PropertyInventoryDocType' And B.DocType = F.OptCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("    Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As Status, ");
            SQL.AppendLine("    A.PropertyCode, C.PropertyName, E.SiteName, A.CostComponentAmt, F.OptDesc Type, A.CostComponentReason, D.PropertyCategoryName,");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCostComponentDtl3 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryHdr C On A.PropertyCode = C.PropertyCode ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCategory D On C.PropertyCategoryCode = D.PropertyCategoryCode ");
            SQL.AppendLine("    Inner Join TblSite E On C.SiteCode = E.SiteCode ");
            SQL.AppendLine("    Inner Join TblOption F On F.OptCat = 'PropertyInventoryDocType' And B.DocType = F.OptCode ");
            SQL.AppendLine(") A  ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
            Grd1, new string[]
            {
                //0
                "No.",

                //1-5
                "Date",
                "Cancel",
                "Document#",
                "Property Code",
                "Property Name",

                //6-10
                "Site",
                "Cost Component Value",
                "DocType",
                "Property Category",
                "Add Cost Component Reason",

                //11-15
                "Created By",
                "Created"+Environment.NewLine+"Date",
                "Created"+Environment.NewLine+"Time",
                "Last"+Environment.NewLine+"Updated By",
                "Last"+Environment.NewLine+"Updated Date",

                //16-17
                "Last"+Environment.NewLine+"Updated Time",
                "Status"
            },
            new int[]
            {
                //0
                50,

                //1-5
                150, 50, 150, 50, 200, 

                //6-10
                150, 150, 200, 100, 100, 

                //11-15
                100, 100, 100, 100, 100, 
                
                //16-17
                100, 50
            }
            );
            Sm.GrdColCheck(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 1, 12, 15 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 16 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Grd1.Cols[17].Move(3);
            Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14, 15, 16 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL+ Filter + " Group By A.DocNo Order By A.DocNo; ",
                        new string[]
                        {
                            //0
                            "DocDt", 
                                
                            //1-5
                            "CancelInd", "DocNo", "PropertyCode", "PropertyName", "SiteName", 
                            
                            //6-10
                            "CostComponentAmt", "Type", "PropertyCategoryName", "CostComponentReason", "CreateBy", 
                            
                            //11-14
                            "CreateDt", "LastUpBy", "LastUpDt", "Status"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 14);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3));
                this.Hide();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Event

        #region Misc Control Event

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }
        #endregion

        #endregion
    }
}
