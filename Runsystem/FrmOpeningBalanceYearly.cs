﻿#region Update
/*
    19/07/2018 [TKG] logic perhitungan nomor rekening parent diubah untuk mengakomodasi apabila anak nomor rekening anak yg panjangnya tidak sama.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOpeningBalanceYearly : RunSystem.FrmBase3
    {
        #region Field, Property

        internal FrmOpeningBalanceYearlyFind FrmFind;
        private bool mIsEntityMandatory = false, mIsReportingFilterByEntity = false;

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mMth = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mCOAAssetStartYr = string.Empty,
            mCOAAssetAcNo = string.Empty,
            mAccountingRptStartFrom = string.Empty;

        private decimal Acc1 = 0, Acc2 = 0, Acc3 = 0, AccLaba = 0,  Selisih = 0; 
        int RowAcclaba = 0;

        #endregion

        #region Constructor

        public FrmOpeningBalanceYearly(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method


        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                SetFormControl(mState.View);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                GetParameter();
                if (mIsEntityMandatory) LblEntity.ForeColor = Color.Red;
                else LblEntity.ForeColor = Color.Black;
                SetLueEntCode(ref LueEntCode);
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                mMth = "12";
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Account Name";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Header.Cells[1, 2].Value = "Opening Balance";
            Grd1.Header.Cells[1, 2].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 2].SpanCols = 2;
            Grd1.Header.Cells[0, 2].Value = "Debit";
            Grd1.Header.Cells[0, 3].Value = "Credit";
            Grd1.Cols[2].Width = 130;
            Grd1.Cols[3].Width = 130;

            Grd1.Header.Cells[1, 4].Value = "Month To Date";
            Grd1.Header.Cells[1, 4].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 4].SpanCols = 3;
            Grd1.Header.Cells[0, 4].Value = "Debit";
            Grd1.Header.Cells[0, 5].Value = "Credit";
            Grd1.Header.Cells[0, 6].Value = "Balance";
            Grd1.Cols[4].Width = 130;
            Grd1.Cols[5].Width = 130;
            Grd1.Cols[6].Width = 130;

            Grd1.Header.Cells[1, 7].Value = "Current Month";
            Grd1.Header.Cells[1, 7].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 7].SpanCols = 3;
            Grd1.Header.Cells[0, 7].Value = "Debit";
            Grd1.Header.Cells[0, 8].Value = "Credit";
            Grd1.Header.Cells[0, 9].Value = "Balance";
            Grd1.Cols[7].Width = 130;
            Grd1.Cols[8].Width = 130;
            Grd1.Cols[9].Width = 130;

            Grd1.Header.Cells[1, 10].Value = "";
            Grd1.Header.Cells[1, 10].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 10].SpanCols = 3;
            Grd1.Header.Cells[0, 10].Value = "Debit";
            Grd1.Header.Cells[0, 11].Value = "Credit";
            Grd1.Header.Cells[0, 12].Value = "Balance";
            Grd1.Cols[10].Width = 130;
            Grd1.Cols[11].Width = 130;
            Grd1.Cols[12].Width = 130;

            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }, 2);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
            Sm.GrdColReadOnly(Grd1, new int[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8,9, 10, 11, 12} );
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, LueYr, LueEntCode, MeeRemark
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    LueYr.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueYr, LueEntCode, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    LueYr.Focus();
                    break;
                case mState.Edit:
                    Grd1.ReadOnly = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, LueYr, LueEntCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 4, 5 });
        }

        #endregion
      
        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            
        }



        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOpeningBalanceYearlyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            if (ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "You can't edit this data." + Environment.NewLine +
                    "This data already cancelled.");
                return;
            }
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    UpdateData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "COAOpeningBalance", "TblCOAOpeningBalanceHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveOBHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    cml.Add(SaveOBDtl(DocNo, Row));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsDocAlreadyCreated()
                ;
        }


        private bool IsDocAlreadyCreated()
        {
            var EntCode = Sm.GetLue(LueEntCode);
            var SQL = new StringBuilder();
            SQL.AppendLine("Select DocNo From TblCOAOpeningBalanceHdr ");
            SQL.AppendLine("Where Yr=@Yr ");
            SQL.AppendLine("And CancelInd='N' ");
            if (EntCode.Length > 0)
                SQL.AppendLine(" And EntCode Is Not Null And EntCode=@EntCode ");
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            if (EntCode.Length > 0) Sm.CmParam<String>(ref cm, "@EntCode", EntCode);

            if (Sm.IsDataExist(cm))
            {
                var Msg = string.Empty;

                Msg = ("Year : " + Sm.GetLue(LueYr) + Environment.NewLine);
                if (EntCode.Length > 0) Msg += ("Entity : " + LueEntCode.GetColumnValue("Col2") + Environment.NewLine);
                Msg += (Environment.NewLine + "Opening balance has been created.");

                Sm.StdMsg(mMsgType.Warning, Msg);
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveOBHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            string CurrentDateTime = Sm.ServerCurrentDateTime();

            SQL.AppendLine("Insert Into TblCOAOpeningBalanceHdr(DocNo, DocDt, Yr, CancelInd, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, 'N', @EntCode, @Remark, @CreateBy, @CreateDt); ");

            SQL.AppendLine("Insert Into TblCOAOpeningBalanceDtl(DocNo, AcNo, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, AcNo, 0, @CreateBy, @CreateDt ");
            SQL.AppendLine("From TblCOA Where (Left(AcNo, 1) = '1' Or  Left(AcNo, 1) = '2'  Or Left(AcNo, 1) = '3') ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("And AcNo In ( ");
                SQL.AppendLine("    Select AcNo From TblCOADtl ");
                SQL.AppendLine("    Where EntCode Is Not Null ");
                SQL.AppendLine("    And EntCode=@EntCode ");
                SQL.AppendLine(") ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateDt", CurrentDateTime);

            return cm;
        }

        private MySqlCommand SaveOBDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand() { CommandText = "Update TblCOAOpeningBalanceDtl Set Amt=@Amt Where DocNo=@DocNo And AcNo=@AcNo;" };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void UpdateData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditCOAOpeningBalanceHdr());
            
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocAlreadyCancel();
        }

        private bool IsDocAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblCOAOpeningBalanceHdr Where DocNo=@DocNo And CancelInd='Y';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditCOAOpeningBalanceHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblCOAOpeningBalanceHdr Set " +
                    "   CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }
        
        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowOBHdr(DocNo);
                ShowAccount();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowOBHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.Yr, A.CancelInd, A.EntCode, A.Remark " +
                    "From TblCOAOpeningBalanceHdr A " +
                    "Where A.DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "Yr", "CancelInd", "EntCode", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    }, true
                );
        }


        private void ShowAccount()
        {
            int Level = 0;
            string PrevMenuCode = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var EntCode = Sm.GetLue(LueEntCode);

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    
                    SQL.AppendLine("Select Concat(A.AcNo, ' ', B.AcDesc) As Account, ");
                    SQL.AppendLine("A.AcNo, A.Amt ");
                    SQL.AppendLine("From tblCOAOpeningBalanceDtl A ");
                    SQL.AppendLine("Inner Join TblCOA B On A.AcNo=B.AcNo ");
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("   Select AcNo From TblCoa ");
                    SQL.AppendLine("   Where Acno Not In ");
                    SQL.AppendLine("   (Select Parent From TblCoa Where Parent Is Not Null) ");
                    SQL.AppendLine(") C On B.Acno = C.AcNo ");
                    SQL.AppendLine("Where DocNo=@DocNo Order By AcNo;");

                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "Account", "AcNo", "Amt" });
                    if (dr.HasRows)
                    {
                        int Row = 0;
                        Grd1.Rows.Count = 0;
                        Grd1.ProcessTab = true;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Level = Sm.DrStr(dr, 1).Length - ((Sm.DrStr(dr, 1).Length + 1) / 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 2);
                            Grd1.Rows[Row].Level = Level;
                            if (Row > 0)
                                Grd1.Rows[Row - 1].TreeButton =
                                    (PrevMenuCode.Length >= Sm.DrStr(dr, 1).Length) ?
                                        iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                            PrevMenuCode = Sm.DrStr(dr, 1);
                            Row++;
                        }
                        //if (Row > 0)
                        //    Grd1.Rows[Row - 1].TreeButton =
                        //        (PrevMenuCode.Length >= Sm.DrStr(dr, 1).Length) ?
                        //            iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                    }
                    Grd1.TreeLines.Visible = true;
                    Grd1.EndUpdate();
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mAcNoForCurrentEarning = Sm.GetParameter("AcNoForCurrentEarning");
            mAcNoForIncome = Sm.GetParameter("AcNoForIncome");
            mAcNoForCost = Sm.GetParameter("AcNoForCost");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mCOAAssetStartYr = Sm.GetParameter("COAAssetStartYr");
            mCOAAssetAcNo = Sm.GetParameter("COAAssetAcNo");
            mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'CONSOLIDATE' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 ");
            SQL.AppendLine("From TblEntity T Where T.ActInd='Y'");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ProcessAfterShowData()
        {
            Selisih = Acc1 - (Acc2 + (Acc3 - AccLaba));
            Grd1.Cells[RowAcclaba, 12].Value = Selisih;
            Reproseskepala3(RowAcclaba, AccLaba);
        }

        private void Reproseskepala3(int Row, decimal AcLabaStart)
        {
            if (Sm.GetGrdDec(Grd1, Row, 12) > 0)
            {
                string Ac = Sm.GetGrdStr(Grd1, Row, 0);
                string ccc = string.Empty;
                for (int i = 0; i < Ac.Length; i++)
                {
                    if (Ac[i] == '.')
                    {
                        ccc = Ac.Substring(0, i);
                        for (int y = 0; y < Row; y++)
                        {
                            if (Sm.GetGrdStr(Grd1, y, 0) == ccc)
                            {
                                decimal QtyRow = (Sm.GetGrdDec(Grd1, Row, 12));
                                decimal QtyRow2 = (Sm.GetGrdDec(Grd1, y, 12));
                                decimal Qty = QtyRow + (QtyRow2 - AcLabaStart);
                                Grd1.Cells[y, 12].Value = Qty;
                            }
                        }
                    }
                }
            }
        }


        #region Process
        private void Process1(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Left Join TblCOADtl B On A.AcNo=B.AcNo ");
                if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                    SQL.AppendLine("And B.EntCode=@EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Where A.ActInd='Y' And  (left(A.Acno,1) = '1' Or left(A.Acno,1) = '2' Or left(A.Acno,1) = '3')  ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode).Length > 0 ? Sm.GetLue(LueEntCode) == "Consolidate" ? "" : Sm.GetLue(LueEntCode) : Sm.GetLue(LueEntCode));

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
           
            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A, TblCOAOpeningBalanceDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            SQL.AppendLine("And B.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Yr=@Yr And (left(B.Acno,1) = '1' Or left(B.Acno,1) = '2' Or left(B.Acno,1) = '3') ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And IfNull(A.EntCode, '') = IfNull(@EntCode, '') ");
            SQL.AppendLine("Order By B.AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });

                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A, TblJournalDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("And Left(A.DocDt, 6)<@YrMth ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("And Left(A.DocDt, 6)<@YrMth ");
            }
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And IfNull(B.EntCode, '') = IfNull(@EntCode, '') ");
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By AcNo;");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0)
                Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<COA> lCOA, string Yr, string Mth)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A, TblJournalDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
            SQL.AppendLine("And Substring(A.DocDt, 5, 2)=@Mth ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And IfNull(B.EntCode, '') = IfNull(@EntCode, '') ");
            SQL.AppendLine("Group By B.AcNo;");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<COA> lCOA)
        {
            return;
            if (
                mAcNoForCurrentEarning.Length == 0 ||
                mAcNoForIncome.Length == 0 ||
                mAcNoForCost.Length == 0
                ) return;

            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }

            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;
 
            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                        lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                        lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }           
        }

        private void Process6(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                
                    lCOA[i].YearToDateDAmt =
                        lCOA[i].OpeningBalanceDAmt +
                        lCOA[i].MonthToDateDAmt +
                        lCOA[i].CurrentMonthDAmt;

                    lCOA[i].YearToDateCAmt =
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateCAmt +
                        lCOA[i].CurrentMonthCAmt;
            }
        }

        private void Process7(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process8(ref List<EntityCOA> lEntityCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And U.EntCode = @EntCode ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process9(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                
                    if (lCOA[i].AcType == "D")
                    {
                        lCOA[i].MonthToDateBalance =
                            lCOA[i].OpeningBalanceDAmt +
                            lCOA[i].MonthToDateDAmt -
                            lCOA[i].MonthToDateCAmt;

                        lCOA[i].CurrentMonthBalance =
                            lCOA[i].MonthToDateBalance +
                            lCOA[i].CurrentMonthDAmt -
                            lCOA[i].CurrentMonthCAmt;

                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                    }
                    if (lCOA[i].AcType == "C")
                    {
                        lCOA[i].MonthToDateBalance =
                            lCOA[i].OpeningBalanceCAmt +
                            lCOA[i].MonthToDateCAmt -
                            lCOA[i].MonthToDateDAmt;

                        lCOA[i].CurrentMonthBalance =
                            lCOA[i].MonthToDateBalance +
                            lCOA[i].CurrentMonthCAmt -
                            lCOA[i].CurrentMonthDAmt;

                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                }
            }
        }

        #endregion


        #endregion

        #endregion

        #region Event

        private void BtnShowAc_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;

            Cursor.Current = Cursors.WaitCursor;

            var Yr = Sm.GetLue(LueYr);
            var Mth = mMth;
            var StartFrom = Sm.GetLue(LueYr);

            try
            {
                var lCOA = new List<COA>();
                var lEntityCOA = new List<EntityCOA>();

                Process1(ref lCOA);

                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom);
                        Process3(ref lCOA, Yr, Mth, StartFrom);
                    }
                    else
                    {
                        Process2(ref lCOA, Yr);
                        Process3(ref lCOA, Yr, Mth, string.Empty);
                    }
                    Process4(ref lCOA, Yr, Mth);
                    Process5(ref lCOA);
                    Process6(ref lCOA);
                    Process7(ref lCOA);
                    Process9(ref lCOA);


                    Grd1.BeginUpdate();
                    Grd1.Rows.Count = 0;

                    iGRow r;

                    r = Grd1.Rows.Add();
                    r.Level = 0;
                    r.TreeButton = iGTreeButtonState.Visible;
                    r.Cells[0].Value = "COA";
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        r = Grd1.Rows.Add();
                        r.Level = lCOA[i].Level;
                        r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                        r.Cells[0].Value = lCOA[i].AcNo;
                        r.Cells[1].Value = lCOA[i].AcDesc;
                        for (var c = 2; c < 13; c++)
                            r.Cells[c].Value = 0m;
                        r.Cells[2].Value = lCOA[i].OpeningBalanceDAmt;
                        r.Cells[3].Value = lCOA[i].OpeningBalanceCAmt;
                        r.Cells[4].Value = lCOA[i].MonthToDateDAmt;
                        r.Cells[5].Value = lCOA[i].MonthToDateCAmt;
                        r.Cells[6].Value = lCOA[i].MonthToDateBalance;
                        r.Cells[7].Value = lCOA[i].CurrentMonthDAmt;
                        r.Cells[8].Value = lCOA[i].CurrentMonthCAmt;
                        r.Cells[9].Value = lCOA[i].CurrentMonthBalance;
                        r.Cells[10].Value = lCOA[i].YearToDateDAmt;
                        r.Cells[11].Value = lCOA[i].YearToDateCAmt;
                        r.Cells[12].Value = lCOA[i].YearToDateBalance;

                        if (lCOA[i].AcNo == "1")
                        {
                            Acc1 = Sm.GetGrdDec(Grd1, i+1, 12);
                        }
                        if (lCOA[i].AcNo == "2")
                        {
                            Acc2 = Sm.GetGrdDec(Grd1, i+1, 12);
                        }
                        if (lCOA[i].AcNo == "3")
                        {
                            Acc3 = Sm.GetGrdDec(Grd1, i+1, 12);
                        }
                        if (lCOA[i].AcNo == mAcNoForCurrentEarning)
                        {
                            AccLaba = Sm.GetGrdDec(Grd1, i+1, 12);
                            RowAcclaba = i+1;
                        }

                    }
                    Grd1.TreeLines.Visible = true;
                    Grd1.Rows.CollapseAll();

                    decimal
                        OpeningBalanceDAmt = 0m,
                        OpeningBalanceCAmt = 0m,
                        MonthToDateDAmt = 0m,
                        MonthToDateCAmt = 0m,
                        MonthToDateBalance = 0m,
                        CurrentMonthDAmt = 0m,
                        CurrentMonthCAmt = 0m,
                        CurrentMonthBalance = 0m,
                        YearToDateDAmt = 0m,
                        YearToDateCAmt = 0m,
                        YearToDateBalance = 0m;

                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (lCOA[i].Parent.Length == 0)
                        {
                            OpeningBalanceDAmt += lCOA[i].OpeningBalanceDAmt;
                            OpeningBalanceCAmt += lCOA[i].OpeningBalanceCAmt;
                            MonthToDateDAmt += lCOA[i].MonthToDateDAmt;
                            MonthToDateCAmt += lCOA[i].MonthToDateCAmt;
                            MonthToDateBalance += lCOA[i].MonthToDateBalance;
                            CurrentMonthDAmt += lCOA[i].CurrentMonthDAmt;
                            CurrentMonthCAmt += lCOA[i].CurrentMonthCAmt;
                            CurrentMonthBalance += lCOA[i].CurrentMonthBalance;
                            YearToDateDAmt += lCOA[i].YearToDateDAmt;
                            YearToDateCAmt += lCOA[i].YearToDateCAmt;
                            YearToDateBalance += lCOA[i].YearToDateBalance;
                        }
                    }


                    Grd1.EndUpdate();
                }


                lCOA.Clear();
                lEntityCOA.Clear();

                ProcessAfterShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

       
        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        #endregion

      
    }

  
}
