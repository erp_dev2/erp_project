﻿#region Update
#region Old
/*
    23/04/2017 [TKG] tambah document type Switching Bank Account
    10/05/2017 [TKG] difilter berdasarkan department
    13/07/2017 [TKG] tambah entity
    26/07/2017 [TKG] Department difilter berdasarkan otorisasi group
    27/07/2017 [TKG] Bank account dpt diinput/dibuka oleh user berdasarkan otorisasi group
    15/08/2017 [ARI] Paid To dibuat Mandatory (utk AWG)
    31/08/2017 [TKG] tambah status cancel
    16/10/2017 [ARI] tambah printout surat perintah perjalanan dinas[HIN]
    30/10/2017 [ARI] print out HIN muncul disemua customer [bug] 
    12/01/2018 [HAR] validasi jika amount minus, maka tidak bisa save [bug] 
    16/01/2018 [TKG] Set bank secara otomatis berdasarkan bank account, ditambah informasi account type bank account.
    18/01/2018 [HAR] tambah inputan project
    31/01/2018 [HAR] tambah parameter buat ngeset urutan bank account
    07/02/2018 [TKG] Saat insert data, hanya bisa memilih type manual dan switching bank account
    19/02/2018 [TKG] panjang local document# tambah menjadi 300 karakter. 
    21/02/2018 [ARI] tambah printout KIM.
    06/03/2018 [WED] Paid To tambah menjadi 100 karakter
    19/03/2018 [ARI] printout VR[KIM] approval level 1 menggunakan 'Akcnowledge By'
    14/04/2018 [TKG] menggunakan format khusus voucher atau format standard dokumen run system berdasarkan parameter VoucherCodeFormatType
    01/08/2018 [ARI] Cashier pd kolom approval di printout jika type manual diganti prepared by
    07/09/2018 [HAR] tambah printout SPPD untuk KIM
    07/09/2018 [HAR] printout twc untuk tulisan cash in cash out diganti switching bank untuk type switcing bank
    22/09/2018 [HAR] printout SPPD HIN dihapus
    03/09/2019 [DITA] Transaksi voucher request ditambah type "Cash Advance"
    20/01/2020 [WED/SIER] tambah tab COA, berdasarkan parameter IsVoucherRequestUseCOA
    24/01/2020 [WED/SIER] COA terpilih otomatis sesuai bank account nya
    08/02/2020 [TKG/IMS] Berdasarkan parameter IsVoucherRequestBtnDocTypeEnabled, Button document type diaktifkan atau tidak.
    09/02/2020 [TKG/IMS] tambah refund date
    05/03/2020 [VIN/KBN] Generate Docno 6 digit
    16/03/2020 [VIN/KBN] Berdasarkan parameter rate di Voucher Request default nya ngambil dari currency rate
    19/03/2020 [VIN/KBN] menambah parameter IsVoucherBankAccountFilteredByCurrency untuk menampilkan bank account sesuai currency yg dipilih
    19/03/2020 [VIN/KBN] auto approve untuk VR manual Debit
    23/03/2020 [IBL/KBN] Field DUE DATE bisa terbuka untuk Type Voucher Request CASH ADVANCE
    14/04/2020 [TKG/KBN] Berdasarkan parameter IsVoucherRequestPrintOutUseDocDt, print out bagian signature menggunakan document date
    04/05/2020 [WED/IMS] tab baru untuk VR Budget, berdasarkan param MenuCodeForVRBudget
    12/05/2020 [DITA/SIER] Pengaturan Budget berdasarkan tahun dengan parametr baru mIsBudget2YearlyFormat
    06/05/2020 [DITA/IMS] computeavailablebudget juga ambil dari travel request total
    16/06/2020 [DITA/IMS] Penyesuaian Print out
    29/06/2020 [TKG/IMS] Proses save untuk vr budget
    01/07/2020 [DITA/IMS] budget bisa menyimpan tipe cash advance di voucher request param --> VoucherDocTypeBudget
    01/07/2020 [DITA/IMS] menghubungkan budget dengan cash advance settlement
    03/07/2020 [DITA/IMS] rumus pada cash advance settlement, sum nya diluar subquery
    10/07/2020 [WED/SRN] fitur baru untuk memasukan nilai non-IDR untuk VR Manual, berdasarkan parameter IsVRManualUseOtherCurrency
    13/07/2020 [WED/IMS] VR Budget doctype nya divalidasi
    22/07/2020 [WED/SRN] IsVRManualUseOtherCurrency ga jadi pakai manual currency
    06/08/2020 [ICA/IMS] IsVRForBudgetUseSOContract menambahkan field input SO Contract#, COA#, Finish Good. 
    12/08/2020 [DITA/IMS] Tambah lue BIOP/Project
    01/11/2020 [TKG/IMS] button type bisa membuat ss, vr payroll, vr special
    18/11/2020 [VIN/PHT] DocType baru CASBA (VoucherDocType: 70)
    18/11/2020 [WED/PHT] pilihan ALL ada di Budget Category, bukan Request Type
    19/11/2020 [WED/PHT] ketika pilih ALL, available budget nya muncul semua untuk dept tersebut
    26/11/2020 [DITA/PHT] Fasilitas send csv ke BRI
    01/12/2020 [VIN/SIER] Tambah Where 0=0 di SetLueBankACCode & SetLueBankACCode2
    07/12/2020 [DITA/IMS] lup type = incoming payment masih di frmincomingpayment belum incomingpayment2
    16/12/2020 [ICA/SIER] Menambah format baru untuk menampilkan SetLueBankAcCode based on parameter BankAccountFormat
    23/12/2020 [WED/PHT] tambah setluebccode saat showdata hdr
    23/12/2020 [VIN/PHT] VR for budget --> amount tidak boleh lebih besar dr available budget (IsVRForBudgetUseAvailableBudget)
    29/12/2020 [ICA/SIER] Mengubah format lue bankaccount
    05/01/2021 [VIN/SIER] Lue PIC dibatasi --> yang muncul user yang login   
    06/01/2021 [HAR/PHT] send ke bank utk debit account ambil bank account number,document yg sdh di csv kan tdk bs dicancel, date saat kirim dikosongkan
    13/01/2021 [VIN/ALL] bug SetLueBankACCode --> IsVoucherBankAccountFilteredByCurrency
    17/01/2021 [TKG/PHT] ubah GenerateDocNo*
    20/01/2021 [WED/SIER] tambah ambil dari Cash Advance Settlement berdasarkan parameter IsCASUsedForBudget
    26/02/2021 [VIN/KIM] bug SetLueBankACCode --> BankAccountFormat kurang else if 
    01/03/2021 [WED/IMS] tambah inputan SO Contract untuk VR biasa, berdasarkan parameter IsVRUseSOContract.
                         kalau lagi buat pakai buka menu VR For Budget, maka inputan ini gak muncul
    17/03/2021 [IBL/SIER] Approval voucher request dibedakan berdasarkan voucher request type
    22/03/2021 [RDH/SIER] Approval voucher request dibedakan berdasarkan voucher request type modify type = VoucherRequestSwitchingBankAcc
    22/03/2021 [ICA/SIER] Mengubah isi Method IsDocApprovalSettingNotExist menjadi SQL2.ToString() dan mengubah VoucherRequestCashAdvanceManual menjadi VoucherRequestManual
    06/04/2021 [IBL/PHT] menambah field billing id berdasarkan parameter IsVRUseBillingID
    09/04/2021 [ICA/SIER] menambah kondisi di setluebankaccode yg muncul hanya bank active
    16/04/2021 [WED/ALL] parameter baru IsVRDeptCodeAutoFillBasedOnPIC dan IsPICShowEmpCode
    30/04/2021 [IBL/PHT] tambah CSV yang ada billing id nya berdasarkan parameter IsVRUseBillingID
    18/06/2021 [HAR/PHT] pemisah saat create csv menggunakan pipe |
    22/06/2021 [HAR/PHT] uncomment di SendDataBRI dan UpdateCSVInd
    28/06/2021 [HAR/PHT] BUG sat kirim data BRI untuk yg Payment priority masih masuk ke folder MPN
    14/07/2021 [DITA/PHT] tambah tipe Switching Cost Center di VR Budget
    16/07/2021 [DITA/PHT] untuk switching cost center tidak harus membuat 2 bankl account dalam 1 dokumen
    22/07/2021 [WED/PHT] ComputeAvailableBudget ambil dari StdMtd
    20/08/2021 [RDA/AMKA] menambah tab upload file untuk menu voucher request berdasarkan parameter IsVoucherRequestAllowToUploadFile
    03/09/2021 [MYA/AMKA] Membuat upload file di voucher request bisa upload hingga 18 file
    06/09/2021 [SET/AMKA] Penyesuaian Printout Voucher Request manual
    13/09/2021 [RDA/AMKA] Penyesuaian printout untuk semua type voucher request
    28/09/2021 [RDA/AMKA] bug printout untuk menu voucher request
    05/10/2021 [MYA/GKS] Menambah TTD digital di prinout VR
    25/10/2021 [TKG/TWC] menggunakan parameter IsVoucherDocSeqNoEnabled saat membuat Voucher Request#
    04/11/2021 [RDA/AMKA] feedback printout untuk doctype cash advance
    26/11/2021 [ICA/PHT] Mengubah billing id menjadi multi billing id berdasarkan parameter IsVRUseMultiBillingID
    29/11/2021 [VIN/ALL] Amt2 hanya di show untuk type manual saja 
*/
#endregion
/*
    06/01/2022 [TKG/PHT] ubah GetParameter dan proses save
    04/01/2022 [BRI/SIER] Penyesuaian printout voucher request
    04/01/2022 [SET/SIER] Penyesuaian Print Out Voucher Request For Budget
    07/03/2022 [RDA/SIER] Penambahan Start dan End Amount Pada Approval Setting Transaksi VR & VR for Budget dan menyambungkan Document Approval Group
    17/05/2022 [TKG/PHT] tambah approval berdasarkan department untuk tipe switching cost center dan casba
    30/06/2022 [BRI/PHT] merubah nama file csv
    22/07/2022 [DITA/SIER] untuk vr for budget, based on apram mIsBudgetTransactionCanUseOtherDepartmentBudget menambahkan department for budget untuk validasi pengambilan budget
    27/07/2022 [MYA/SIER] Memunculkan seluruh approver pada tab approval information (seperti pada monitoring document approval setting) dan menambahkan kolom jabatan di menu Voucher Request
    27/07/2022 [MYA/SIER] Memunculkan seluruh approver pada tab approval information (seperti pada monitoring document approval setting) dan menambahkan kolom jabatan di menu Voucher Request for Budget
    27/07/2022 [TYO/SIER] Group filter doc Type berdasarkan parameter IsGroupFilterByType
    01/08/2022 [ICA/SIER] memunculkan button print saat di akses di menu approval
    01/08/2022 [MYA/PHT] FEEDBACK : PIC saat disave jadi hilang
    03/08/2022 [TYO/SIER] tambah fitur copy data berdasarkan parameter IsVRForAllowToCopyData
    05/08/2022 [TYO/SIER] Penyesuaian filter by group Type
    05/08/2022 [DITA/SIER] vr budget : ketika clear grid seharusnya atx amount dicompute ulang supaya nilainya sesuai
    05/08/2022 [DITA/SIER]  jika department for budget yg dipilih merupakan department itu sendiri maka budgetcategory si department itu akan otomatis muncul tanpa di set diparameter
    09/08/2022 [TYO/SIER] penyesuaian fitur copy data
    10/08/2022 [VIN/SIER] BUG Approval blm nyambung ke DAG
    23/08/2022 [BRI/PRODUCT] mempercepat process
    31/08/2022 [VIN/ALL] Department tidak bisa dipilih
    02/09/2022 [BRI/SIER] type belum terfilter
    12/09/2022 [DITA/SIER] dept for budget tidak di filter by site, dan detail tidak akan hilang ketika header di edit
    13/09/2022 [TYO/SIER] set LueBCCode read only
    14/09/2022 [DITA/SIER] penyesuaian ux utk budget, compute budget ketika ada perubahan amount di detail,  dan clear txtstatus
    16/09/2022 [DITA/SIER] saat copy data melakukan compute amt dan budget 
    05/10/2022 [VIN/SIER] Bug validasi available budget
    10/10/2022 [VIN/SIER] Bug saat insert tab general masih ikut dok sebelumnya
    13/10/2022 [HPH/SIER] Penyesuaian print out sier
    25/10/2022 [MAU/BBT] Penambahan doctype VR Petty Cash
    08/11/2022 [IBL/PRODUCT] Memunculkan approval information untuk VR yg tergenerate dari Petty Cash Disbursement
    10/11/2022 [SET/BBT] Penyesuaian printout VR
    17/11/2022 [ICA/BBT] menambah validasi kedua bank account mandatory berdasarkan parameter CashAdvanceJournalDebitFormat
    28/11/2022 [MAU/BBT] Feedback : debit/credit2 mandatory
    28/11/2022 [ICA/BBT] Feedback : filter bank account hanya untuk type cash advance
    05/12/2022 [IBL/BBT] Feedback : Penyesuaian printout
    09/12/2022 [DITA/BBT] penambahan validasi jenis bank yang bisa muncul pada VR tipe Petty Cash Disbursement
    09/12/2022 [SET/BBT] Penyesuaian printout
    12/12/2022 [DITA/PRODUCT] memandatory kan (label merah) additional information berdasarkan payment type yg dipilih
    22/12/2022 [DITA/PRODUCT] bug fix SetLueBankAcCode2
    19/01/2023 [SET/SIER] menyesuaikan lue Department For Budget Budget akan null ketika lue Department General ada diubah
    26/01/2023 [MAU/PHT] BUG : muncul warning saat input department 
    05/02/2023 [MAU/PHT] validasi monthly closing journal Voucher Request
    08/02/2023 [MAU/MNET] menambah format upload file 
    22/02/2023 [WED/KBN] penambahan inputan baru Cash Advance Type berdasarkan parameter IsVoucherRequestCASUseDueDate 
    28/02/2023 [MAU/BBT] penyesuaian format upload file
    08/03/2023 [SET/MNET] Penyesuaian printout
    16/03/2023 [MAU/PHT] Menambah validasi claim employee
    17/03/2023 [MAU/BBT] feedback : merubah label field form pada tab upload file
    21/03/2023 [MAU/PHT] expand dropdown opsi Bank Account
    23/03/2023 [MAU/PHT] penyesuaian filter Type berdasarkan group user
    27/03/2023 [RDA/MNET] bug fix upload file di grid 5
    30/03/2023 [WED/PHT] tambah inputan Cash Advance PIC, berdasarkan parameter IsVRCashAdvanceUseEmployeePIC
    05/04/2023 [VIN/MNET] menyamakan validasi type valid dengan frmVoucher
    06/04/2023 [MAU/MNET] bug fix download file grid 5
    11/04/2023 [MAU/MNET] rombak download & upload file format 3 (Grid 5)
    18/04/2023 [SET/SIER] Value Avalaible budget reset 0 ketika ada perubahan dept for budget & nilai amount pada detail (Grd1)
    27/04/2023 [BRI/SIER] Bug Budget Category
    02/05/2023 [MAU/PHT] Expand field department
    03/05/2023 [BRI/SIER] Bug Budget Category lagi
    04/05/2023 [VIN/BBT] Bug Invalid Type 
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using System.IO;
using System.Drawing.Imaging;
using System.Net;
using System.Threading;
using Renci.SshNet;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequest : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mBankAccountFormat = "0",
            mIsPrintOutVR,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mMenuCodeForVRBudget = string.Empty,
            mVoucherDocTypeCashAdvance = string.Empty,
            mBIOPProjectCodeForVRBudget = string.Empty,
            mVoucherDocTypePettyCash = string.Empty,
            mBankAccountTypeForVRPCD1 = string.Empty,
            mEmpCode = string.Empty,

            mHostAddrForFTPClient = string.Empty,
            mPortForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mBudgetCategoryCodeForCrossDepartment = string.Empty,
            mVoucherDocTypeCodeSwitchingBankAccount = string.Empty
            ;

        private byte[] downloadedData;

        internal FrmVoucherRequestFind FrmFind;
        private string
            mMInd = "N", mVoucherCodeFormatType = "1",
            mMainCurCode = string.Empty,
            mVoucherDocTypeManual = string.Empty,
            mVoucherDocTypeCASBA = string.Empty,
            mVoucherDocTypePayrollProcessing = string.Empty,
            mInstructionCodeForTransferInstructionType = string.Empty,
            mBankChargeTypeForVR = string.Empty,
            mPathToSaveExportedBRIVR = string.Empty,
            mHostAddrForBRIVR = string.Empty,
            mSharedFolderForBRIVR = string.Empty,
            mSharedFolderForBRIVRMPN = string.Empty,
            mUserNameForBRIVR = string.Empty,
            mPasswordForBRIVR = string.Empty,
            mPortForBRIVR = string.Empty,
            mProtocolForBRIVR = string.Empty,
            mCashAdvanceJournalDebitFormat = string.Empty,
            mBankAccountTypeForVRCA1 = string.Empty,
            mBankAccountTypeForVRCA2 = string.Empty,
            //VR for budget
            mVoucherDocTypeBudget = string.Empty,
            mReqTypeForNonBudget = string.Empty,
            mMRAvailableBudgetSubtraction = string.Empty,
            mBudgetBasedOn = string.Empty,
            mFormatFTPClient = string.Empty,
            mVoucherRequestUploadFormat = string.Empty
            //end VR for budget
            ;
        private bool
            mIsRemarkForApprovalMandatory = false,
            mIsVRPaidToMandatory = false,
            mIsVoucherRequestBtnDocTypeEnabled = false,
            mIsVoucherRequestPrintOutUseDocDt = false,
            mIsVRManualUseOtherCurrency = false,
            mIsVRApprovalByType = false,
            //VR for Budget
            mIsSiteMandatory = false,
            mIsDeptFilterBySite = false,
            mIsMRBudgetBasedOnBudgetCategory = false,
            mIsBudgetCalculateFromEstimatedPrice = false,
            mIsMRShowEstimatedPrice = false,
            mIsBudgetActive = false,
            mIsFilterBySite = false,
            mIsVRForBudgetUseAvailableBudget = false,
            //end VR for Budget
            mIsVRForBudgetUseSOContract = false,
            mIsVRBudgetUseCASBA = false,
            mIsVRSendtoBank = false,
            mIsCSVUseRealAmt = false,
            mIsPICGrouped = false,
            mIsCASUsedForBudget = false,
            mIsVRUseBillingID = false,
            mIsVRDeptCodeAutoFillBasedOnPIC = false,
            mIsPICShowEmpCode = false,
            mIsVRBudgetUseSwitchingCostCenter = false,
            mIsVRAllowToUploadFile = false,
            mIsVRUseMultiBillingID = false,
            mIsVRClaimUsePIC = false,
            mIsTransactionUseDetailApprovalInformation = false,
            mIsPettyCashTypeEnabled = false,
            mIsAutoJournalActived = false,
            mIsVoucherRequestTransactionValidatedbyClosingJournal = false,
            mIsVoucherRequestCASUseDueDate = false,
            mIsVoucherRequestUploadFileMandatory = false,
            mIsVoucherCASAllowMultipleBankAccount = false
            ;
        internal bool
            mIsUseMInd = false,
            mIsFilterByDept = false,
            mIsVoucherRequestFilteredByDept = false,
            mIsAutoGeneratePurchaseLocalDocNo = false,
            mIsVoucherRequestBankAccountFilteredByGrp = false,
            mIsProjectSystemActive = false,
            mIsVoucherRequestUseCOA = false,
            mAllowToSaveDifferentAmtJournalVoucher = true,
            mIsUseDailyCurrencyRate = false,
            mIsVoucherBankAccountFilteredByCurrency = false,
            mIsDebitVRAutoApproved = false,
            mIsVRUseSOContract = false,
            mIsVoucherRequestAllowToUploadFile = false,
            mIsBudgetTransactionCanUseOtherDepartmentBudget = false,
            mIsBudget2YearlyFormat = false,
            mIsGroupFilterByType = false,
            mIsVRForAllowToCopyData = false,
            mIsVRCashAdvanceUseEmployeePIC = false,
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false
            ;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmVoucherRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Voucher Request";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                ExecQuery();
                GetParameter();
                SetGrd();
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                LblManualCurCode.Visible = LblManualExcRate.Visible = TxtManualExcRate.Visible = LueManualCurCode.Visible = false;
                if (mIsVRPaidToMandatory) LblPaymentUser.ForeColor = Color.Red;
                if (!mIsVRManualUseOtherCurrency) TxtAmt2.Visible = false;
                if (mVoucherDocTypeCodeSwitchingBankAccount.Length <= 0) mVoucherDocTypeCodeSwitchingBankAccount = "16";
                if (mMenuCodeForVRBudget != mMenuCode)
                {
                    SetLueDeptCode(ref LueDeptCode);
                    TcVoucherRequest.TabPages.Remove(TpBudget);
                    if (!mIsVRUseSOContract)
                        LblSOContractDocNo.Visible = TxtSOContractDocNo.Visible = BtnSOContractDocNo.Visible = BtnSOContractDocNo2.Visible = false;
                    //Sl.SetLueDeptCode(ref LueDeptCode2, string.Empty, mIsFilterByDept ? "Y" : "N");
                }
                else
                {
                    TcVoucherRequest.SelectedTabPage = TpBudget;
                    SetLueBIOPProject(ref LueBIOPProject, string.Empty);
                    SetLueReqType(ref LueReqType, string.Empty);
                    Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                    SetLueDeptCode(ref LueDeptCode);
                    Sl.SetLueDeptCode(ref LueDeptCode2, string.Empty, mIsFilterByDept ? "Y" : "N");
                    SetLueBCCode(ref LueBCCode, string.Empty, string.Empty);
                    if (!mIsBudgetTransactionCanUseOtherDepartmentBudget)
                    {
                        LblDeptCode2.Visible = LueDeptCode2.Visible = BtnBCCode.Visible = false;
                    }
                    LblSOContractDocNo.Visible = TxtSOContractDocNo.Visible = BtnSOContractDocNo.Visible = BtnSOContractDocNo2.Visible = false;
                }

                TcVoucherRequest.SelectedTabPage = TpCOA;

                TcVoucherRequest.SelectedTabPage = TpAdditional;
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueEntCode(ref LueEntCode);

                //if (mIsGroupFilterByType)
                //    SetLueVoucherDocType(ref LueDocType, mIsGroupFilterByType ? "Y" : "N");
                //else
                SetLueVoucherDocType(ref LueDocType, string.Empty);

                TcVoucherRequest.SelectedTabPage = TpUpload2;
                if (mIsVoucherRequestAllowToUploadFile)
                {
                    if (mVoucherRequestUploadFormat == "1")
                    {
                        TcVoucherRequest.TabPages.Remove(TpUpload2);
                        TcVoucherRequest.TabPages.Remove(TpUpload3);
                    }
                    if (mVoucherRequestUploadFormat == "2")
                    {
                        TcVoucherRequest.TabPages.Remove(TpUpload);
                        TcVoucherRequest.TabPages.Remove(TpUpload3);
                    }
                    if (mVoucherRequestUploadFormat == "3") {
                        TcVoucherRequest.TabPages.Remove(TpUpload);
                        TcVoucherRequest.TabPages.Remove(TpUpload2);
                    }
                }

                if (!mIsVoucherRequestAllowToUploadFile)
                {
                    TcVoucherRequest.TabPages.Remove(TpUpload);
                    TcVoucherRequest.TabPages.Remove(TpUpload2);
                }

                if (mIsVoucherRequestCASUseDueDate)
                {
                    Sl.SetLueOption(ref LueCashAdvanceTypeCode, "VoucherRequestCASDueDate");
                }
                else
                {
                    LblCashAdvanceTypeCode.Visible = LueCashAdvanceTypeCode.Visible = false;
                }

                TcVoucherRequest.SelectedTabPage = TpGeneral;
                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueAcType(ref LueAcType2);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueCurCode(ref LueCurCode);
                SetLuePICCode(ref LuePIC);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                SetLueBankAcCode2(ref LueBankAcCode2, string.Empty);
                BtnDocType.Visible = mIsVoucherRequestBtnDocTypeEnabled;

                TpgProject.PageVisible = mIsProjectSystemActive;
                TcVoucherRequest.SelectedTabPage = TpgProject;
                if (TcVoucherRequest.SelectedTabPage == TpgProject)
                    SetLueProjectSystem1(ref LueProjectDocNo1);

                if (!mIsVoucherRequestUseCOA) TcVoucherRequest.TabPages.Remove(TpCOA);

                Sl.SetLueOption(ref LueVRCtCode, "FileCategoryForVR");
                LueVRCtCode.Visible = false;

                if (!mIsVRForAllowToCopyData)
                    LblCopyGeneral.Visible = BtnCopy.Visible = false;

                Sl.SetLueBankCode(ref LueBankCode);

                if (!mIsVRForBudgetUseSOContract)
                {
                    this.label36.Visible = false;
                    this.TxtSOCDocNo.Visible = false;
                    this.BtnSOCDocNo.Visible = false;
                    this.BtnSOCDocNo2.Visible = false;

                    this.label37.Visible = false;
                    this.TxtItCode.Visible = false;
                    this.TxtItName.Visible = false;
                    this.BtnItCode.Visible = false;
                    this.BtnItCode2.Visible = false;

                    this.label38.Visible = false;
                    this.TxtAcNo.Visible = false;
                    this.TxtAcDesc.Visible = false;
                    this.BtnCOA.Visible = false;
                    this.BtnCOA2.Visible = false;

                    this.LueBIOPProject.Visible = false;
                    this.label39.Visible = false;
                }

                if (!mIsVRUseBillingID || mIsVRUseMultiBillingID)
                    LblBillingID.Visible = TxtBillingID.Visible = false;

                if (!mIsVoucherRequestAllowToUploadFile)
                    TcVoucherRequest.TabPages.Remove(TpUpload);

                if (mIsPettyCashTypeEnabled)
                {
                    label24.ForeColor = Color.Red;
                    LblBankAcCode2.ForeColor = Color.Red;

                }
                if (mIsVoucherRequestUploadFileMandatory)
                {
                    label45.ForeColor = Color.Red;
                }

                LblEmpName.Visible = TxtEmpName.Visible = BtnEmpCode.Visible = BtnEmpCode2.Visible = mIsVRCashAdvanceUseEmployeePIC;
                if (mIsVRCashAdvanceUseEmployeePIC)
                {
                    if (!LblCashAdvanceTypeCode.Visible)
                    {
                        LblEmpName.Top = LblCashAdvanceTypeCode.Top;
                        TxtEmpName.Top = BtnEmpCode.Top = BtnEmpCode2.Top = LueCashAdvanceTypeCode.Top;
                    }

                    if (!LblSOContractDocNo.Visible)
                    {
                        LblEmpName.Top = LblSOContractDocNo.Top;
                        TxtEmpName.Top = BtnEmpCode.Top = BtnEmpCode2.Top = TxtSOContractDocNo.Top;
                    }
                }

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = false;
                    BtnPrint.Visible = true;
                }

                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "DNo",

                    //1-5
                    "Description",
                    "Amount",
                    "Tax Billing ID",
                    "Remark",
                    "Amount"+Environment.NewLine+"(non -" +mMainCurCode+" )"
                },
                new int[] { 0, 300, 130, 150, 250, 130 }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            //if (!mIsVRManualUseOtherCurrency) 
            Sm.GrdColInvisible(Grd1, new int[] { 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, (mIsVRUseBillingID && mIsVRUseMultiBillingID));

            Grd2.Cols.Count = 6;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Position",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 4 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3, 4, 5 });

            Grd3.Cols.Count = 6;
            Grd3.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark"
                    },
                     new int[]
                    {
                        20,
                        100, 250, 100, 100, 400
                    }
                );

            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4 }, 0);
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColInvisible(Grd3, new int[] { 5 }, false);

            Grd4.Cols.Count = 7;
            Grd4.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[]
                    {
                        //0
                        "D No",

                        //1-5
                        "Category Code",
                        "Category",
                        "File Name",
                        "U",
                        "D",

                        //6
                        "File Name2"
                    },
                     new int[]
                    {
                        // 0 
                        0, 

                        // 1-5
                        0, 180, 250, 20, 20,

                        // 6
                        250
                    }
                );

            Sm.GrdColInvisible(Grd4, new int[] { 0, 1, 6 }, false);

            Sm.GrdColButton(Grd4, new int[] { 4 }, "1");
            Sm.GrdColButton(Grd4, new int[] { 5 }, "2");
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            #region Grid 5
            Grd5.Cols.Count = 5;
            Grd5.FrozenArea.ColCount = 3;
            // Grd5.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[]
                    {
                        //0
                        "D No",

                        "File Name",
                        "U",
                        "D",
                        "File Name2"
                    },
                     new int[]
                    {
                        0,
                        250, 20, 20, 250
                    }
                );

            Sm.GrdColInvisible(Grd5, new int[] { 0, 4 }, false);
            Sm.GrdColButton(Grd5, new int[] { 2 }, "1");
            Sm.GrdColButton(Grd5, new int[] { 3 }, "2");
            Sm.GrdColReadOnly(Grd5, new int[] { 0, 1, 4 });
            //if (!mIsPropertyInventoryAllowToUploadFile) Sm.GrdColReadOnly(Grd5, new int[] { 2 });
            #endregion
        }

        private void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] { 1, 2 }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdAutoSize(Grd1);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, TxtLocalDocNo, TxtVoucherDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, LueDeptCode,
                        LueDocType, LueAcType, LueBankAcCode, LueAcType2, LueBankAcCode2, LueCurCode, LuePaymentType,
                        LueBankCode, TxtGiroNo, DteOpeningDt, DteDueDt, TxtAmt,
                        TxtExcRate, LuePIC, MeeRemark, TxtPaymentUser, TxtBankAcNo,
                        TxtBankCode, TxtBankBranch, TxtBankAcName, TxtDocEnclosure, LueEntCode,
                        LueProjectDocNo1, LueProjectDocNo2, LueProjectDocNo3, TxtDbt, TxtCdt,
                        TxtBalanced, DteRefundDt, LueReqType, LueSiteCode, LueBCCode, LueManualCurCode, TxtManualExcRate,
                        LueBIOPProject, TxtBillingID, TxtFile, TxtFile2, TxtFile3, LueVRCtCode, LueDeptCode2,
                        LueCashAdvanceTypeCode
                    }, true);
                    Grd1.ReadOnly =
                    Grd2.ReadOnly = true;
                    Sm.GrdColReadOnly(false, false, Grd4, new int[] { 5 });
                    BtnSOCDocNo.Enabled =
                    BtnItCode.Enabled =
                    BtnCOA.Enabled =
                    BtnSOContractDocNo.Enabled =
                    BtnBCCode.Enabled =
                    BtnCopy.Enabled =
                    BtnEmpCode.Enabled = false;
                    //Send Csv to Bank
                    if (mIsVRUseBillingID)
                    {
                        bool IsBillingIdComplated = (Grd1.Rows.Count > 1);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (Sm.GetGrdStr(Grd1, row, 3).Length == 0)
                                IsBillingIdComplated = false;
                        }

                        if ((mIsVRUseMultiBillingID && IsBillingIdComplated) || (!mIsVRUseMultiBillingID && TxtBillingID.Text.Length > 0))
                        {
                            if (!BtnSave.Enabled &&
                                    TxtDocNo.Text.Length > 0 &&
                                    mIsVRSendtoBank &&
                                    !ChkCancelInd.Checked &&
                                    Sm.GetValue("Select Status From TblVoucherRequestHdr Where DocNo = '" + TxtDocNo.Text + "'") == "A" &&
                                    !Sm.IsDataExist("Select 1 From TblVoucherHdr Where VoucherRequestDocNo = @Param", TxtDocNo.Text) &&
                                    Sm.GetValue("Select CSVInd From TblVoucherRequestHdr Where DocNo = '" + TxtDocNo.Text + "'") == "N"
                                )
                            {
                                BtnCSV.Visible = true;
                                BtnCSV.Enabled = true;
                            }
                        }
                        else
                        {
                            BtnCSV.Visible = false;
                            BtnCSV.Enabled = false;
                        }
                    }
                    else
                    {
                        if (TxtDocNo.Text.Length > 0 &&
                            mIsVRSendtoBank &&
                            !ChkCancelInd.Checked &&
                            Sm.GetValue("Select Status From TblVoucherRequestHdr Where DocNo = '" + TxtDocNo.Text + "'") == "A"
                            && Sm.GetValue("Select CSVInd From TblVoucherRequestHdr Where DocNo = '" + TxtDocNo.Text + "'") == "N"
                            && Sm.GetLue(LueDocType) != mVoucherDocTypePayrollProcessing
                            && Sm.GetLue(LueAcType) == "C"
                            )
                        {
                            BtnCSV.Visible = true;
                            BtnCSV.Enabled = true;
                        }
                        else
                        {
                            BtnCSV.Visible = false;
                            BtnCSV.Enabled = false;
                        }
                    }

                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;

                    if (TxtDocNo.Text.Length > 0 && mIsVoucherRequestAllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsVoucherRequestAllowToUploadFile)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsVoucherRequestAllowToUploadFile)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;
                    TxtDocNo.Focus();
                    for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd5, Row, 1).Length == 0)
                        {
                            Grd5.Cells[Row, 2].ReadOnly = iGBool.False;
                            Grd5.Cells[Row, 3].ReadOnly = iGBool.True;
                            Grd5.Cells[Row, 1].ReadOnly = iGBool.True;
                        }
                        else
                        {
                            Grd5.Cells[Row, 2].ReadOnly = iGBool.True;
                            Grd5.Cells[Row, 3].ReadOnly = iGBool.False;
                            Grd5.Cells[Row, 1].ReadOnly = iGBool.True;
                        }
                    }
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueDocType, LueDeptCode, LueAcType, LueBankAcCode,
                        LueAcType2, LueBankAcCode2, LueCurCode, LuePaymentType, LuePIC,
                        MeeRemark, TxtPaymentUser, TxtBankAcNo, TxtBankCode, TxtBankBranch,
                        TxtBankAcName, TxtDocEnclosure, LueEntCode, LueProjectDocNo1, LueProjectDocNo2,
                        LueProjectDocNo3, DteRefundDt, LueReqType, LueSiteCode, LueBIOPProject,
                        TxtBillingID, TxtFile, TxtFile2, TxtFile3, LueVRCtCode, LueDeptCode2
                    }, false);
                    BtnSOCDocNo.Enabled =
                    BtnItCode.Enabled =
                    BtnCOA.Enabled =
                    BtnBCCode.Enabled =
                    BtnEmpCode.Enabled =
                    true;
                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);

                    //if (mIsVRManualUseOtherCurrency)
                    //{
                    //    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    //    { 
                    //        LueManualCurCode, TxtManualExcRate
                    //    }, false);
                    //}
                    ChkCancelInd.Checked = false;
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 1, 2, 4 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus();
                    if (mIsVRUseSOContract) BtnSOContractDocNo.Enabled = true;
                    if (mIsVoucherRequestAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = true;
                        BtnFile3.Enabled = true;
                        BtnDownload3.Enabled = true;
                    }
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    BtnCopy.Enabled = true;

                    Grd5.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    Grd1.ReadOnly = true;
                    BtnCopy.Enabled = false;
                    //Grd4.ReadOnly = false;
                    Sm.GrdColReadOnly(false, false, Grd4, new int[] { 5 });
                    ChkCancelInd.Focus();
                    Grd5.ReadOnly = false;
                    if (ChkCancelInd.Checked)
                        Grd5.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mEmpCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, TxtLocalDocNo, TxtVoucherDocNo, DteDocDt, LueDeptCode,
                LueDocType, LueAcType, LueBankAcCode, LueAcType2, LueBankAcCode2,
                LueCurCode, ChkCancelInd, LuePaymentType, LueBankCode, TxtGiroNo,
                DteOpeningDt, DteDueDt, TxtAmt, LuePIC, MeeRemark, TxtPaymentUser,
                TxtBankAcNo, TxtBankCode, TxtBankBranch, TxtBankAcName, MeeCancelReason,
                TxtCurCode2, LueEntCode, TxtBankAcTp, TxtBankAcTp2, LueProjectDocNo1,
                LueProjectDocNo2, LueProjectDocNo3, DteRefundDt, LueReqType, LueSiteCode, LueBCCode,
                LueManualCurCode, TxtSOCDocNo, TxtItCode, TxtItName, TxtAcNo, TxtAcDesc, LueBIOPProject,
                TxtSOContractDocNo, TxtBillingID, TxtFile, TxtFile2, TxtFile3, LueVRCtCode, LueDeptCode2, TxtStatus,
                LueCashAdvanceTypeCode, TxtEmpName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtAmt, TxtExcRate, TxtDocEnclosure,
                TxtDbt, TxtCdt, TxtBalanced, TxtRemainingBudget, TxtManualExcRate, TxtAmt2
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherRequestFind(this, mMInd);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                //if (mIsGroupFilterByType)
                //    SetLueVoucherDocType(ref LueDocType, mIsGroupFilterByType ? "Y" : "N");
                //else
                SetLueVoucherDocType(ref LueDocType, string.Empty);
                Sm.SetLue(LueDocType, "01");
                SetLuePICCode(ref LuePIC);
                SetLueDeptCode(ref LueDeptCode);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                SetLueBankAcCode2(ref LueBankAcCode2, string.Empty);

                if (mMenuCodeForVRBudget == mMenuCode)
                    Sm.SetLue(LueReqType, "1");
                else if (mReqTypeForNonBudget.Length > 0)
                    Sm.SetLue(LueReqType, mReqTypeForNonBudget);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetLueVoucherDocType(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='VoucherDocType' ");
            if (Code.Length > 0)
                SQL.AppendLine("And OptCode=@Code;");
            else
            {
                SQL.AppendLine("And OptCode In ('01', '16', '56' ");
                if (mMenuCodeForVRBudget == mMenuCode && mIsVRBudgetUseCASBA)
                    SQL.AppendLine(", '70' ");
                if (mMenuCodeForVRBudget == mMenuCode && mIsVRBudgetUseSwitchingCostCenter)
                    SQL.AppendLine(", '73' ");
                if (mIsPettyCashTypeEnabled)
                    SQL.AppendLine(", '74' ");

                SQL.AppendLine(") Order By OptDesc;");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetLueCurCodeNonMain(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CurCode As Col1, CurName As Col2 From TblCurrency ");
            SQL.AppendLine("Where CurCode Not In (Select ParValue From TblParameter Where ParCode = 'MainCurCode') Order By CurName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            mEntCode = Sm.GetLue(LueEntCode);
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (
                        Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                        IsDataNotValid()
                        ) return;

                    Cursor.Current = Cursors.WaitCursor;

                    string DocNo = string.Empty;

                    if (mVoucherCodeFormatType == "2")
                    {
                        if (mDocNoFormat == "1")
                            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
                        else
                            DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");
                    }
                    else
                    {
                        if (mDocNoFormat == "1")
                            DocNo = GenerateDocNo();
                        else
                            DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");
                    }

                    bool NoNeedApproval = IsDocApprovalSettingNotExisted();

                    var cml = new List<MySqlCommand>();

                    cml.Add(SaveVoucherRequest(DocNo, NoNeedApproval));








                    //cml.Add(SaveVoucherRequestHdr(DocNo, NoNeedApproval));
                    //if (Grd1.Rows.Count > 1)
                    //{
                    //    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    //        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveVoucherRequestDtl(DocNo, Row));
                    //}

                    //if (Grd3.Rows.Count > 1)
                    //{
                    //    for (int r = 0; r < Grd3.Rows.Count; r++)
                    //        if (Sm.GetGrdStr(Grd3, r, 1).Length > 0) cml.Add(SaveVoucherRequestDtl2(DocNo, r));
                    //}

                    if (Grd4.Rows.Count > 1)
                    {
                        for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd4, Row, 3).Length > 0)
                            {
                                if (!IsUploadFileNotValid4(Row, (Sm.GetGrdStr(Grd4, Row, 3))))
                                {
                                    cml.Add(SaveVoucherRequestDtl3(DocNo, Row));
                                }
                                else
                                {
                                    return;
                                }
                            }
                    }

                    if (Grd5.Rows.Count > 1)
                    {
                        for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0) // File Name length > 0
                            {
                                if (!IsUploadFileNotValid5(Row, (Sm.GetGrdStr(Grd5, Row, 1))))
                                {
                                    cml.Add(SaveVoucherRequestDtl4(DocNo, Row));
                                }
                                else
                                {
                                    return;
                                }
                            }
                    }




                    //if (mVoucherRequestUploadFormat == "3")
                    //    cml.Add(SaveVoucherRequestDtl4(DocNo));





                    // ====== Save Data
                    if (mIsVoucherRequestAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                        UploadFile(DocNo);
                    if (mIsVoucherRequestAllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                        UploadFile2(DocNo);
                    if (mIsVoucherRequestAllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                        UploadFile3(DocNo);

                    for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd4, Row, 3).Length > 0)
                        {
                            if (mIsVoucherRequestAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 3).Length > 0 && Sm.GetGrdStr(Grd4, Row, 3) != "openFileDialog1")
                            {
                                if (Sm.GetGrdStr(Grd4, Row, 3) != Sm.GetGrdStr(Grd4, Row, 6))
                                {
                                    UploadFile4(DocNo, Row, Sm.GetGrdStr(Grd4, Row, 3), Sm.GetGrdStr(Grd4, Row, 1));
                                }
                            }
                        }
                    }
                    Sm.ExecCommands(cml);
                    if (mVoucherRequestUploadFormat == "3")
                    {
                        for (int Row = 0; Row < Grd5.Rows.Count ; Row++)
                        {
                            if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
                            {
                                UploadFile5(DocNo, Row, Sm.GetGrdStr(Grd5, Row, 1));
                            }
                        }
                    }

                    // ====== Save Data

                    ShowData(DocNo);
                }
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            string DocType = Sm.GetLue(LueDocType);
            string DocTypeStr = string.Empty;
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || (Sm.GetParameter("DocTitle") == "KIM" && ShowPrintApproval()) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            string[] TableName = { "VoucherReq", "VoucherReq2", "VoucherReqDtl", "VoucherReqDtl2", "VoucherReqDtl3", "VoucherReqDtl4", "VoucherReqDtl5", "Files", "VoucherReqDtl6", "VoucherReqDtl7", "VoucherReqDtl8", "VoucherReqDtl9", "VoucherReqDtl10" };

            var l = new List<VoucherReqHdr>();
            var l2 = new List<VoucherReqHdr2>();
            var ldtl = new List<VoucherReqDtl>();
            var ldtl2 = new List<VoucherReqDtl2>();
            var ldtl3 = new List<VoucherReqDtl3>();
            var ldtl4 = new List<VoucherReqDtl4>();
            var ldtl5 = new List<VoucherReqDtl5>();
            var ldtl6 = new List<VoucherReqDtl6>();
            var ldtl7 = new List<VoucherReqDtl7>();
            var ldtl8 = new List<VoucherReqDtl8>();
            var ldtl9 = new List<VoucherReqDtl9>();
            var ldtl10 = new List<VoucherReqDtl10>();
            var fd = new List<FileDetails>();
            var f = new List<Files>();

            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine(" Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL.AppendLine(" A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.CancelInd, A.DocType, ");
            SQL.AppendLine(" A.AcType, A.VoucherDocNo, K.DivisionName, L.BCName, M.SiteName, ");
            SQL.AppendLine(" (Select DATE_FORMAT(DocDt,'%d %M %Y') As DocDt from tblvoucherhdr Where DocNo=A.VoucherDocNo Limit 1) As VoucherDocDt, ");
            SQL.AppendLine(" Concat(IfNull(C.BankAcNo, ''), ' [', IfNull(C.BankAcNm, ''), ']') As BankAcc, C.BankAcNo as BankAcNo, C.BankAcNm as BankAcNm, ");
            SQL.AppendLine(" (Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=A.PaymentType Limit 1) As PaymentType, ");
            SQL.AppendLine(" A.GiroNo,E.BankName As GiroBankName, DATE_FORMAT(A.DueDt,'%d %M %Y') As GiroDueDt, F.UserName As PICName, A.Amt As AmtHdr, A.Remark As RemarkHdr, A.CurCode, A.DocEnclosure, A.PaymentUser, G.DeptName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='IsPrintOutVoucherRequestShowDocument') As PrintFormat, H.OptDesc As DocTypeDesc, A.LocalDocNo, A.PIC PICCode, I.UserName, J.CurName, ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc ");
            SQL.AppendLine(" From TblVoucherRequestHdr A ");
            SQL.AppendLine(" Left Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
            SQL.AppendLine(" Left Join TblBank D On C.BankCode=D.BankCode ");
            SQL.AppendLine(" Left Join TblBank E On A.BankCode=E.BankCode ");
            SQL.AppendLine(" Left Join TblUser F On A.PIC=F.UserCode ");
            SQL.AppendLine(" Left Join TblDepartment G On A.DeptCode=G.DeptCode ");
            SQL.AppendLine(" Left Join TblOption H On A.DocType=H.OptCode and H.OptCat='VoucherDocType' ");
            SQL.AppendLine(" Inner Join TblUser I On A.CreateBy=I.UserCode ");
            SQL.AppendLine(" Left Join TblCurrency J On A.CurCode=J.CurCode ");
            SQL.AppendLine(" Left Join TblDivision K On G.DivisionCode=K.DivisionCode ");
            SQL.AppendLine(" Left Join TblBudgetCategory L On A.BCCode=L.BCCode ");
            SQL.AppendLine(" Left Join TblSite M On A.SiteCode=M.SiteCode ");
            SQL.AppendLine(" Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo",
                         "DocDt", 
                        
 
                         //6-10
                         "CancelInd",
                         "DocType",
                         "AcType",
                         "VoucherDocNo",
                         "VoucherDocDt", 
                          
                         
                         //11-15
                         "BankAcc",
                         "PaymentType",
                         "GiroNo",
                         "GiroBankName",
                         "GiroDueDt", 
                         
                         //16-20
                         "PICName",
                         "AmtHdr",
                         "RemarkHdr",
                         "CompanyLogo",
                         "CurCode",
                         
                         //21-25
                         "DocEnclosure",
                         "PaymentUser",
                         "DeptName",
                         "PrintFormat",
                         "DocTypeDesc",

                         //26-30
                         "LocalDocNo",
                         "PICCode",
                         "UserName",
                         "BankAcNo",
                         "BankAcNm",

                         //31-35
                         "CurName",
                         "StatusDesc",
                         "DivisionName",
                         "BCName",
                         "SiteName"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new VoucherReqHdr()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),
                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyPhone = Sm.DrStr(dr, c[2]),
                            CompanyFax = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),
                            CancelInd = Sm.DrStr(dr, c[6]),
                            DocType = Sm.DrStr(dr, c[7]),
                            AcType = Sm.DrStr(dr, c[8]),
                            VoucherDocNo = Sm.DrStr(dr, c[9]),
                            VoucherDocDt = Sm.DrStr(dr, c[10]),
                            BankAcc = Sm.DrStr(dr, c[11]),
                            PaymentType = Sm.DrStr(dr, c[12]),
                            GiroNo = Sm.DrStr(dr, c[13]),
                            GiroBankName = Sm.DrStr(dr, c[14]),
                            GiroDueDt = Sm.DrStr(dr, c[15]),
                            PICName = Sm.DrStr(dr, c[16]),
                            AmtHdr = Sm.DrDec(dr, c[17]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[17])),
                            Terbilang2 = Sm.Terbilang2(Sm.DrDec(dr, c[17])),
                            RemarkHdr = Sm.DrStr(dr, c[18]),

                            CompanyLogo = Sm.DrStr(dr, c[19]),
                            CurCode = Sm.DrStr(dr, c[20]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            DocEnclosure = Sm.DrDec(dr, c[21]),
                            PaymentUser = Sm.DrStr(dr, c[22]),
                            Department = Sm.DrStr(dr, c[23]),
                            PrintFormat = Sm.DrStr(dr, c[24]),
                            DocTypeDesc = Sm.DrStr(dr, c[25]),
                            LocalDocNo = Sm.DrStr(dr, c[26]),
                            PICCode = Sm.DrStr(dr, c[27]),
                            UserName = Sm.DrStr(dr, c[28]),
                            BankAcNo = Sm.DrStr(dr, c[29]),
                            BankAcNm = Sm.DrStr(dr, c[30]),
                            CurName = Sm.DrStr(dr, c[31]),
                            StatusDesc = Sm.DrStr(dr, c[32]),
                            DivisionName = Sm.DrStr(dr, c[33]),
                            BCName = Sm.DrStr(dr, c[34]),
                            SiteName = Sm.DrStr(dr, c[35]),
                            FooterImage = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\FooterImage.png",
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region Header2
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine(" Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL2.AppendLine(" Date_Format(A.DocDt, '%d %M %Y')As DocDt, ");
            SQL2.AppendLine(" A.DocNo As DocNoVR, B.DocNo, B.DNo, D.EmpName As PICName, E.PosName As PICPosName, F.GrdLvlName As PICGrdLvlName,  C.TravelService, G.OptDesc As Transportation, ");
            SQL2.AppendLine(" Date_Format(C.StartDt, '%d %M %Y')As StartDt, Date_Format(C.EndDt,'%d %M %Y')As EndDt,");
            SQL2.AppendLine(" if(length(C.StartTm)>0, Concat(Left(C.StartTm,2),':',Right(C.StartTm,2)), '')As StartTm, if(Length(C.EndTm)>0, Concat(Left(C.EndTm,2),':',Right(C.EndTm,2)),'') As EndTm, C.result, H.CityName, ");
            SQL2.AppendLine(" @CompanyLogo As CompanyLogo, I.Empname, J.PosName, K.GrdLvlName, C.Result, C.Remark As Remarkhdr, M.ProfitcenterName ");
            SQL2.AppendLine(" From Tblvoucherrequesthdr A ");
            SQL2.AppendLine(" Inner Join tbltravelrequestdtl7 B On A.DocNo=B.VoucherRequestDocNo ");
            SQL2.AppendLine(" Left Join TbltravelRequestHdr C On B.DocNo=C.DocNo ");
            SQL2.AppendLine(" Left Join TblEmployee D On A.PIC=D.UserCode ");
            SQL2.AppendLine(" Left Join TblPosition E On D.PosCode=E.PosCode ");
            SQL2.AppendLine(" Left Join tblgradelevelhdr F On D.GrdLvlCode=F.GrdLvlCode ");
            SQL2.AppendLine(" Left Join TblOption G On G.OptCat='TransportTravel' And C.Transportation = G.OptCode ");
            SQL2.AppendLine(" Left Join tblcity H On C.CityCode = H.CityCode ");
            SQL2.AppendLine(" Inner Join TblEmployee I On B.EmpCode = I.EmpCode ");
            SQL2.AppendLine(" Left Join TblPosition J On I.PosCode=J.PosCode  ");
            SQL2.AppendLine(" Left Join tblgradelevelhdr K On I.GrdLvlCode=K.GrdLvlCode  ");
            SQL2.AppendLine(" left Join TblSite L On D.SiteCode = L.SiteCode ");
            SQL2.AppendLine(" left Join TblProfitCenter M On L.ProfitCenterCode = M.ProfitCenterCode ");
            SQL2.AppendLine(" Where A.DocNo=@DocNo And A.DocType='23' ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[]
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "DocNoVR",
                         "DocNo",
                         "DocDt",
                         "DNo",
                         "PICName",
                        
                         //6-10
                         "PICPosName",
                         "PICGrdLvlName",
                         "TravelService",
                         "Transportation",
                         "StartDt",
                         
                         //11-15
                         "EndDt",
                         "StartTm",
                         "EndTm",
                         "Result",
                         "CityNAme",

                         //16-20
                         "CompanyLogo",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "Empname", 
                         
                         //21-25
                         "PosName",
                         "GrdLvlName",
                         "Result",
                         "Remarkhdr",
                         "ProfitcenterName"
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new VoucherReqHdr2()
                        {
                            CompanyName = Sm.DrStr(dr2, c2[0]),

                            DocNoVR = Sm.DrStr(dr2, c2[1]),
                            DocNo = Sm.DrStr(dr2, c2[2]),
                            DocDt = Sm.DrStr(dr2, c2[3]),
                            DNo = Sm.DrStr(dr2, c2[4]),
                            PICEmpName = Sm.DrStr(dr2, c2[5]),
                            PICPosName = Sm.DrStr(dr2, c2[6]),
                            PICGrdLvlName = Sm.DrStr(dr2, c2[7]),
                            TravelService = Sm.DrStr(dr2, c2[8]),
                            Transportation = Sm.DrStr(dr2, c2[9]),
                            StartDt = Sm.DrStr(dr2, c2[10]),
                            EndDt = Sm.DrStr(dr2, c2[11]),
                            StartTm = Sm.DrStr(dr2, c2[12]),
                            EndTm = Sm.DrStr(dr2, c2[13]),
                            result = Sm.DrStr(dr2, c2[14]),
                            CityName = Sm.DrStr(dr2, c2[15]),
                            CompanyLogo = Sm.DrStr(dr2, c2[16]),
                            CompanyAddress = Sm.DrStr(dr2, c2[17]),
                            CompanyPhone = Sm.DrStr(dr2, c2[18]),
                            CompanyFax = Sm.DrStr(dr2, c2[19]),
                            EmpName = Sm.DrStr(dr2, c2[20]),
                            PosName = Sm.DrStr(dr2, c2[21]),
                            GrdLvlName = Sm.DrStr(dr2, c2[22]),
                            Result = Sm.DrStr(dr2, c2[23]),
                            RemarkHdr = Sm.DrStr(dr2, c2[24]),
                            ProfitCenterName = Sm.DrStr(dr2, c2[25]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                        });
                    }
                }
                dr2.Close();
            }

            myLists.Add(l2);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = "Select DNo, Description, Amt, Remark from TblVoucherRequestDtl Where DocNo=@DocNo";
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                        {
                         //0
                         "DNo",

                         //1-5
                         "Description",
                         "Amt",
                         "Remark"
                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor += 1;
                        ldtl.Add(new VoucherReqDtl()
                        {
                            No = nomor,
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            Description = Sm.DrStr(drDtl, cDtl[1]),
                            Amt = Sm.DrDec(drDtl, cDtl[2]),
                            Remark = Sm.DrStr(drDtl, cDtl[3])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail2

            var cmDtl2 = new MySqlCommand();

            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                cmDtl2.CommandText = "Select A.voucherRequestDocNo, A.DocNo, group_concat(B.EmpName)As EmpName From TblTravelRequestDtl7 A " +
                                     "Inner Join TblEmployee B On A.EmpCode=B.EmpCode Where A.DocNo=@DocNo Group by A.DocNo ";

                Sm.CmParam<String>(ref cmDtl2, "@DocNo", Sm.GetValue("Select DocNo From TblTravelRequestDtl7 where VoucherRequestDocNo='" + TxtDocNo.Text + "'"));
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                        {
                         //0
                         "voucherRequestDocNo",

                         //1-2
                         "DocNo",
                         "EmpName",
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new VoucherReqDtl2()
                        {
                            VoucherRequestDocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                            DocNo = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpNAme = Sm.DrStr(drDtl2, cDtl2[2])
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail Signature

            var cmDtl3 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtl2.AppendLine("From ( ");
                SQLDtl2.AppendLine("    Select Distinct ");
                SQLDtl2.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                SQLDtl2.AppendLine("    B.ApprovalDNo As DNo, D.Level, if(D.Level=1,'Acknowledge By','Approved By') As Title, ");
                if (mIsVoucherRequestPrintOutUseDocDt)
                    SQLDtl2.AppendLine("    E.DocDt As LastUpDt ");
                else
                    SQLDtl2.AppendLine("    Left(B.LastUpDt, 8) As LastUpDt ");
                SQLDtl2.AppendLine("    From TblVoucherRequestDtl A ");
                SQLDtl2.AppendLine("    Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQLDtl2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' ");
                SQLDtl2.AppendLine("    Inner Join TblVoucherRequestHdr E On A.DocNo=E.DocNo ");
                SQLDtl2.AppendLine("    Where A.DocNo=@DocNo ");
                SQLDtl2.AppendLine("    Union All ");
                SQLDtl2.AppendLine("    Select Distinct ");
                SQLDtl2.AppendLine("    A.CreateBy As UserCode, ");
                SQLDtl2.AppendLine("    Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                SQLDtl2.AppendLine("    '00' As DNo, 0 As Level, ");
                if (Doctitle == "KIM")
                    SQLDtl2.AppendLine("if(A.DocType='01','Prepared By','Cashier') As Title, ");
                else
                    SQLDtl2.AppendLine("    'Cashier' As Title, ");
                if (mIsVoucherRequestPrintOutUseDocDt)
                    SQLDtl2.AppendLine("    A.DocDt As LastUpDt ");
                else
                    SQLDtl2.AppendLine("    Left(A.CreateDt, 8) As LastUpDt ");
                SQLDtl2.AppendLine("    From TblVoucherRequestHdr A ");
                SQLDtl2.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl2.AppendLine("    Where A.DocNo=@DocNo ");
                SQLDtl2.AppendLine(") T1 ");
                SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                SQLDtl2.AppendLine("Order By T1.DNo desc; ");

                cmDtl3.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[]
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {

                        ldtl3.Add(new VoucherReqDtl3()
                        {
                            Signature = Sm.DrStr(drDtl3, cDtl3[0]),
                            UserName = Sm.DrStr(drDtl3, cDtl3[1]),
                            PosName = Sm.DrStr(drDtl3, cDtl3[2]),
                            Space = Sm.DrStr(drDtl3, cDtl3[3]),
                            DNo = Sm.DrStr(drDtl3, cDtl3[4]),
                            Title = Sm.DrStr(drDtl3, cDtl3[5]),
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[6])
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);
            #endregion

            #region Detail3

            var cmDtl4 = new MySqlCommand();

            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;
                cmDtl4.CommandText =
                    "Select A.DocNo, A.EmpCode, F.ADName,  (C.Amt+D.Amt+E.Amt) As Amt, (A.Qty+A.Qty2+A.Qty3) As Qty, " +
                    "((C.Amt*A.Qty) +(D.Amt*A.Qty2) + (E.Amt*A.Qty3)) As Total " +
                    "From tbltravelrequestdtl A " +
                    "Inner Join TblEMployee B On A.EmpCode = B.EmpCode " +
                    "Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno " +
                    "Left Join TblLevelDtl D On A.LvlCode2 = D.LevelCode And A.LvlCodeDno2 = D.Dno " +
                    "Left Join TblLevelDtl E On A.LvlCode3 = E.LevelCode And A.LvlCodeDno3 = E.Dno " +
                    "Inner Join TblAllowanceDeduction F On C.ADCode = F.AdCode " +
                    "Where Concat(A.DocNO, A.Dno) = @DocNo " +
                    "Union All " +
                    "Select A.DocNo, A.EmpCode, F.AdName, C.Amt, A.Qty, (C.Amt*A.Qty) Total " +
                    "From tbltravelrequestdtl2 A " +
                    "Inner Join TblEMployee B On A.EmpCode = B.EmpCode " +
                    "Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno " +
                    "Inner Join TblAllowanceDeduction F On C.ADCode = F.AdCode " +
                    "Where C.Amt <>0 And Concat(A.DocNO, A.Dno) = @DocNo " +
                    "Union All " +
                    "Select A.DocNO, A.EmpCode, 'Trasnport' As ADName,   " +
                    "A.Amt, A.Qty, (A.Amt*A.Qty) Total " +
                    "From tbltravelrequestdtl3 A  " +
                    "Inner Join TblEMployee B On A.EmpCode = B.EmpCode  " +
                    "Where A.Amt <>0 And Concat(A.DocNO, A.Dno) = @DocNo " +
                    "union all " +
                    "Select A.DocNo, A.EmpCode, 'Transport' As ADName,  " +
                    "A.Amt, A.Qty, if(OfficeInd='N', (A.Amt*A.Qty), 0) Total " +
                    "From tbltravelrequestdtl4 A  " +
                    "Inner Join TblEmployee B On A.EmpCode = B.EmpCode  " +
                    "Where A.Amt <> 0 And Concat(A.DocNO, A.Dno) = @DocNo " +
                    "Union all " +
                    "Select A.DocNo, A.EmpCode, F.AdName,  " +
                    "C.Amt, A.Qty, if(OfficeInd='N', (C.Amt*A.Qty), 0) Total " +
                    "From tbltravelrequestdtl5 A  " +
                    "Inner Join TblEmployee B On A.EmpCode = B.EmpCode  " +
                    "Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno  " +
                    "Inner Join TblAllowanceDeduction F On C.ADCode = F.AdCode " +
                    "Where C.Amt <> 0  And Concat(A.DocNO, A.Dno) = @DocNo " +
                    "union all " +
                    "Select A.DocNo, A.EmpCode, D.ADName, C.Amt, A.Qty, (C.Amt*A.Qty) Total " +
                    "From tbltravelrequestdtl6 A  " +
                    "Inner Join TblEmployee B On A.EmpCode = B.EmpCode  " +
                    "Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno  " +
                    "Left Join TblAllowanceDeduction D On C.AdCode=D.AdCode  " +
                    "Where C.Amt <>0 And Concat(A.DocNO, A.Dno) = @DocNo ";

                Sm.CmParam<String>(ref cmDtl4, "@DocNo", Sm.GetValue("Select Concat(DocNo, Dno) As DocnO From TblTravelRequestDtl7 where VoucherRequestDocNo='" + TxtDocNo.Text + "'"));
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[]
                        {
                         //0
                         "DocNo",

                         //1-5
                         "EmpCode",
                         "ADName",
                         "Amt",
                         "Qty",
                         "Total"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {
                        ldtl4.Add(new VoucherReqDtl4()
                        {
                            DocNo = Sm.DrStr(drDtl4, cDtl4[0]),
                            EmpCode = Sm.DrStr(drDtl4, cDtl4[1]),
                            AdName = Sm.DrStr(drDtl4, cDtl4[2]),
                            Amt = Sm.DrDec(drDtl4, cDtl4[3]),
                            Qty = Sm.DrDec(drDtl4, cDtl4[4]),
                            Total = Sm.DrDec(drDtl4, cDtl4[5]),
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            #region Detail 4 Sign IMS

            var cmDtl5 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl5.Open();
                cmDtl5.Connection = cnDtl5;

                SQLDtl3.AppendLine("Select Distinct ");
                SQLDtl3.AppendLine("B.UserCode, C.UserCode2, D.UserCode3, B.UserName, C.UserName2, D.UserName3, E.UserName4 ");
                SQLDtl3.AppendLine("From TblVoucherRequestHdr A ");
                SQLDtl3.AppendLine("Left Join ( ");
                SQLDtl3.AppendLine("    Select Distinct ");
                SQLDtl3.AppendLine("    A.DocNo , B.UserCode As UserCode,  Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName ");
                SQLDtl3.AppendLine("    From TblVoucherRequestHdr A ");
                SQLDtl3.AppendLine("    Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
                SQLDtl3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' ");
                SQLDtl3.AppendLine("    Where D.Level = '1' And A.DocNo=@DocNo ");
                SQLDtl3.AppendLine(" ) B On A.DocNo = B.DocNo ");
                SQLDtl3.AppendLine("Left Join ( ");
                SQLDtl3.AppendLine("    Select Distinct ");
                SQLDtl3.AppendLine("    A.DocNo, B.UserCode As UserCode2, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName2 ");
                SQLDtl3.AppendLine("    From TblVoucherRequestHdr A ");
                SQLDtl3.AppendLine("    Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
                SQLDtl3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' ");
                SQLDtl3.AppendLine("    Where D.Level = '2' And A.DocNo=@DocNo ");
                SQLDtl3.AppendLine(" ) C On A.DocNo = C.DocNo ");
                SQLDtl3.AppendLine("Left Join ( ");
                SQLDtl3.AppendLine("    Select Distinct ");
                SQLDtl3.AppendLine("   A.DocNo, B.UserCode As UserCode3, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName)))  As UserName3 ");
                SQLDtl3.AppendLine("    From TblVoucherRequestHdr A ");
                SQLDtl3.AppendLine("    Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
                SQLDtl3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' ");
                SQLDtl3.AppendLine("    Where D.Level = '3' And A.DocNo=@DocNo ");
                SQLDtl3.AppendLine(") D On A.DocNo = D.DocNo ");
                SQLDtl3.AppendLine("Left Join ( ");
                SQLDtl3.AppendLine("    Select Distinct ");
                SQLDtl3.AppendLine("   A.DocNo, B.UserCode As UserCode4, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName)))  As UserName4 ");
                SQLDtl3.AppendLine("    From TblVoucherRequestHdr A ");
                SQLDtl3.AppendLine("    Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
                SQLDtl3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' ");
                SQLDtl3.AppendLine("    Where D.Level = '4' And A.DocNo=@DocNo ");
                SQLDtl3.AppendLine(") E On A.DocNo = E.DocNo ");
                SQLDtl3.AppendLine("Where A.DocNo = @DocNo; ");

                cmDtl5.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl5, "@DocNo", TxtDocNo.Text);
                var drDtl5 = cmDtl5.ExecuteReader();
                var cDtl5 = Sm.GetOrdinal(drDtl5, new string[]
                                {
                                 //0
                                 "UserName" ,

                                 //1-2
                                 "Username2" ,
                                 "Username3" ,
                                 "Username4"

                                });
                if (drDtl5.HasRows)
                {
                    while (drDtl5.Read())
                    {

                        ldtl5.Add(new VoucherReqDtl5()
                        {
                            UserName = Sm.DrStr(drDtl5, cDtl5[0]),
                            UserName2 = Sm.DrStr(drDtl5, cDtl5[1]),
                            UserName3 = Sm.DrStr(drDtl5, cDtl5[2]),
                            UserName4 = Sm.DrStr(drDtl5, cDtl5[3]),
                        });
                    }
                }
                drDtl5.Close();
            }
            myLists.Add(ldtl5);
            #endregion

            #region Files
            var cmFiles = new MySqlCommand();
            var SQLF = new StringBuilder();

            SQLF.AppendLine("SELECT A.DocNo, B.CategoryCode, IFNULL(B.FileName, 'N') as FileName ");
            SQLF.AppendLine("FROM tblvoucherrequesthdr A ");
            SQLF.AppendLine("INNER JOIN tblvoucherrequestdtl3 B ON A.DocNo = B.DocNo ");
            SQLF.AppendLine("WHERE A.DocNo = @Docno ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cmFiles.Connection = cn;
                cmFiles.CommandText = SQLF.ToString();
                Sm.CmParam<String>(ref cmFiles, "@DocNo", TxtDocNo.Text);
                var drF = cmFiles.ExecuteReader();
                var cF = Sm.GetOrdinal(drF, new string[]
                        {
                             "DocNo",
                             "CategoryCode",
                             "FileName"
                        });
                string
                  mFile1 = "N",
                  mFile2 = "N",
                  mFile3 = "N",
                  mFile4 = "N",
                  mFile5 = "N",
                  mFile6 = "N",
                  mFile7 = "N",
                  mFile8 = "N",
                  mFile9 = "N",
                  mFile10 = "N",
                  mFile11 = "N",
                  mFile12 = "N",
                  mFile13 = "N",
                  mFile14 = "N",
                  mFile15 = "N",
                  mFile16 = "N",
                  mFile17 = "N",
                  mFile18 = "N";

                if (drF.HasRows)
                {
                    while (drF.Read())
                    {
                        fd.Add(new FileDetails()
                        {
                            DocNo = Sm.DrStr(drF, cF[0]),
                            CategoryCode = Sm.DrStr(drF, cF[1]),
                            FileName = Sm.DrStr(drF, cF[2]),
                        });
                    }
                    foreach (var x in fd.Distinct())
                    {
                        if (x.CategoryCode == "1") mFile1 = x.FileName;
                        if (x.CategoryCode == "2") mFile2 = x.FileName;
                        if (x.CategoryCode == "3") mFile3 = x.FileName;
                        if (x.CategoryCode == "4") mFile4 = x.FileName;
                        if (x.CategoryCode == "5") mFile5 = x.FileName;
                        if (x.CategoryCode == "6") mFile6 = x.FileName;
                        if (x.CategoryCode == "7") mFile7 = x.FileName;
                        if (x.CategoryCode == "8") mFile8 = x.FileName;
                        if (x.CategoryCode == "9") mFile9 = x.FileName;
                        if (x.CategoryCode == "10") mFile10 = x.FileName;
                        if (x.CategoryCode == "11") mFile11 = x.FileName;
                        if (x.CategoryCode == "12") mFile12 = x.FileName;
                        if (x.CategoryCode == "13") mFile13 = x.FileName;
                        if (x.CategoryCode == "14") mFile14 = x.FileName;
                        if (x.CategoryCode == "15") mFile15 = x.FileName;
                        if (x.CategoryCode == "16") mFile16 = x.FileName;
                        if (x.CategoryCode == "17") mFile17 = x.FileName;
                        if (x.CategoryCode == "18") mFile18 = x.FileName;
                    }
                }
                drF.Close();

                f.Add(new Files()
                {
                    File1 = mFile1,
                    File2 = mFile2,
                    File3 = mFile3,
                    File4 = mFile4,
                    File5 = mFile5,
                    File6 = mFile6,
                    File7 = mFile7,
                    File8 = mFile8,
                    File9 = mFile9,
                    File10 = mFile10,
                    File11 = mFile11,
                    File12 = mFile12,
                    File13 = mFile13,
                    File14 = mFile14,
                    File15 = mFile15,
                    File16 = mFile16,
                    File17 = mFile17,
                    File18 = mFile18,
                });
            }

            myLists.Add(f);
            #endregion

            #region Signature Cash Advance Settlement AMKA

            var cmDtl6 = new MySqlCommand();
            var SQLDtl6 = new StringBuilder();

            SQLDtl6.AppendLine("SELECT T1.EmpPict1, T1.UserName1, Description1, ApproveDate1, T2.EmpPict2, T2.UserName2, T2.Description2, T2.ApproveDate2, ");
            SQLDtl6.AppendLine("T3.EmpPict3, T3.UserName3, T3.Description3, T3.ApproveDate3, T4.EmpPict4, T4.UserName4, T4.Description4, T4.ApproveDate4, ");
            SQLDtl6.AppendLine("T5.EmpPict5, T5.UserName5, T5.Description5, T5.ApproveDate5, T6.EmpPict6, T6.UserName6, T6.Description6, T6.ApproveDate6, ");
            SQLDtl6.AppendLine("T7.EmpPict7, T7.UserName7, T7.Description7, T7.ApproveDate7, T8.EmpPict8, T8.UserName8, T8.Description8, T8.ApproveDate8, ");
            SQLDtl6.AppendLine("T9.EmpPict9, T9.UserName9, T9.Description9, T9.ApproveDate9, T10.EmpPict10, T10.UserName10, T10.Description10, T10.ApproveDate10 ");
            SQLDtl6.AppendLine("FROM ( ");
            SQLDtl6.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict1,  C.UserName AS UserName1,'Diajukan' As Description1, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate1 ");
            SQLDtl6.AppendLine("From tblvoucherrequesthdr A ");
            SQLDtl6.AppendLine("Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo AND B.UserCode = A.CreateBy ");
            SQLDtl6.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQLDtl6.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' ");
            SQLDtl6.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQLDtl6.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQLDtl6.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");
            SQLDtl6.AppendLine(") T1 ");
            SQLDtl6.AppendLine("INNER JOIN( ");
            SQLDtl6.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict2,  C.UserName AS UserName2,'Diperiksa' As Description2, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate2 ");
            SQLDtl6.AppendLine("From tblvoucherrequesthdr A ");
            SQLDtl6.AppendLine("Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
            SQLDtl6.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQLDtl6.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' AND D.`Level` = '1' ");
            SQLDtl6.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQLDtl6.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQLDtl6.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");
            SQLDtl6.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
            SQLDtl6.AppendLine("INNER JOIN ( ");
            SQLDtl6.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict3,  C.UserName AS UserName3,'Disetujui' As Description3, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate3 ");
            SQLDtl6.AppendLine("From tblvoucherrequesthdr A ");
            SQLDtl6.AppendLine("Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
            SQLDtl6.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQLDtl6.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' AND D.`Level` = '2' ");
            SQLDtl6.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQLDtl6.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQLDtl6.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");
            SQLDtl6.AppendLine(") T3 ON T1.DocNo = T3.DocNo ");
            SQLDtl6.AppendLine("INNER JOIN ( ");
            SQLDtl6.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict4,  C.UserName AS UserName4,'Staf Akuntansi' As Description4, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate4 ");
            SQLDtl6.AppendLine("From tblvoucherrequesthdr A ");
            SQLDtl6.AppendLine("Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
            SQLDtl6.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQLDtl6.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' AND D.`Level` = '3' ");
            SQLDtl6.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQLDtl6.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQLDtl6.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");
            SQLDtl6.AppendLine(") T4 ON T1.DocNo = T4.DocNo ");
            SQLDtl6.AppendLine("INNER JOIN ( ");
            SQLDtl6.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict5,  C.UserName AS UserName5,'VP Akuntansi' As Description5, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate5 ");
            SQLDtl6.AppendLine("From tblvoucherrequesthdr A ");
            SQLDtl6.AppendLine("Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
            SQLDtl6.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQLDtl6.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' AND D.`Level` = '4' ");
            SQLDtl6.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQLDtl6.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQLDtl6.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");
            SQLDtl6.AppendLine(") T5 ON T1.DocNo = T5.DocNo ");
            SQLDtl6.AppendLine("INNER JOIN ( ");
            SQLDtl6.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict6,  C.UserName AS UserName6,'Staf Pajak' As Description6, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate6 ");
            SQLDtl6.AppendLine("From tblvoucherrequesthdr A ");
            SQLDtl6.AppendLine("Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
            SQLDtl6.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQLDtl6.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' AND D.`Level` = '5' ");
            SQLDtl6.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQLDtl6.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQLDtl6.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");
            SQLDtl6.AppendLine(") T6 ON T1.DocNo = T6.DocNo ");
            SQLDtl6.AppendLine("INNER JOIN ( ");
            SQLDtl6.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict7,  C.UserName AS UserName7,'VP Pajak' As Description7, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate7 ");
            SQLDtl6.AppendLine("From tblvoucherrequesthdr A ");
            SQLDtl6.AppendLine("Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
            SQLDtl6.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQLDtl6.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' AND D.`Level` = '6' ");
            SQLDtl6.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQLDtl6.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQLDtl6.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");
            SQLDtl6.AppendLine(") T7 ON T1.DocNo = T7.DocNo ");
            SQLDtl6.AppendLine("INNER JOIN ( ");
            SQLDtl6.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict8,  C.UserName AS UserName8,'Anggaran' As Description8, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate8 ");
            SQLDtl6.AppendLine("From tblvoucherrequesthdr A ");
            SQLDtl6.AppendLine("Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
            SQLDtl6.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQLDtl6.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' AND D.`Level` = '7' ");
            SQLDtl6.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQLDtl6.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQLDtl6.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");
            SQLDtl6.AppendLine(") T8 ON T1.DocNo = T8.DocNo ");
            SQLDtl6.AppendLine("INNER JOIN ( ");
            SQLDtl6.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict9,  C.UserName AS UserName9,'VP Keu' As Description9, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate9 ");
            SQLDtl6.AppendLine("From tblvoucherrequesthdr A ");
            SQLDtl6.AppendLine("Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
            SQLDtl6.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQLDtl6.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' AND D.`Level` = '8' ");
            SQLDtl6.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQLDtl6.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo  ");
            SQLDtl6.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");
            SQLDtl6.AppendLine(") T9 ON T1.DocNo = T9.DocNo ");
            SQLDtl6.AppendLine("INNER JOIN ( ");
            SQLDtl6.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict10,  C.UserName AS UserName10,'SVP DKA' As Description10, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate10 ");
            SQLDtl6.AppendLine("From tblvoucherrequesthdr A ");
            SQLDtl6.AppendLine("Inner Join TblDocApproval B On B.DocType='VoucherRequest' And A.DocNo=B.DocNo ");
            SQLDtl6.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQLDtl6.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequest' AND D.`Level` = '9' ");
            SQLDtl6.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQLDtl6.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQLDtl6.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQLDtl6.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");
            SQLDtl6.AppendLine(") T10 ON T1.DocNo = T10.DocNo ");

            using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl6.Open();
                cmDtl6.Connection = cnDtl6;
                cmDtl6.CommandText = SQLDtl6.ToString();
                Sm.CmParam<String>(ref cmDtl6, "@DocNo", TxtDocNo.Text);
                var drDtl6 = cmDtl6.ExecuteReader();
                var cDtl6 = Sm.GetOrdinal(drDtl6, new string[]
                {
                    "EmpPict1",
                    "Description1",
                    "UserName1",
                    "ApproveDate1",

                    "EmpPict2",
                    "Description2",
                    "UserName2",
                    "ApproveDate2",

                    "EmpPict3",
                    "Description3",
                    "UserName3",
                    "ApproveDate3",

                    "EmpPict4",
                    "Description4",
                    "UserName4",
                    "ApproveDate4",

                    "EmpPict5",
                    "Description5",
                    "UserName5",
                    "ApproveDate5",

                    "EmpPict6",
                    "Description6",
                    "UserName6",
                    "ApproveDate6",

                    "EmpPict7",
                    "Description7",
                    "UserName7",
                    "ApproveDate7",

                    "EmpPict8",
                    "Description8",
                    "UserName8",
                    "ApproveDate8",

                    "EmpPict9",
                    "Description9",
                    "UserName9",
                    "ApproveDate9",

                    "EmpPict10",
                    "Description10",
                    "UserName10",
                    "ApproveDate10",
                });
                if (drDtl6.HasRows)
                {
                    while (drDtl6.Read())
                    {
                        ldtl6.Add(new VoucherReqDtl6()
                        {
                            EmpPict1 = Sm.DrStr(drDtl6, cDtl6[0]),
                            Description1 = Sm.DrStr(drDtl6, cDtl6[1]),
                            UserName1 = Sm.DrStr(drDtl6, cDtl6[2]),
                            ApproveDt1 = Sm.DrStr(drDtl6, cDtl6[3]),

                            EmpPict2 = Sm.DrStr(drDtl6, cDtl6[4]),
                            Description2 = Sm.DrStr(drDtl6, cDtl6[5]),
                            UserName2 = Sm.DrStr(drDtl6, cDtl6[6]),
                            ApproveDt2 = Sm.DrStr(drDtl6, cDtl6[7]),

                            EmpPict3 = Sm.DrStr(drDtl6, cDtl6[8]),
                            Description3 = Sm.DrStr(drDtl6, cDtl6[9]),
                            UserName3 = Sm.DrStr(drDtl6, cDtl6[10]),
                            ApproveDt3 = Sm.DrStr(drDtl6, cDtl6[11]),

                            EmpPict4 = Sm.DrStr(drDtl6, cDtl6[12]),
                            Description4 = Sm.DrStr(drDtl6, cDtl6[13]),
                            UserName4 = Sm.DrStr(drDtl6, cDtl6[14]),
                            ApproveDt4 = Sm.DrStr(drDtl6, cDtl6[15]),

                            EmpPict5 = Sm.DrStr(drDtl6, cDtl6[16]),
                            Description5 = Sm.DrStr(drDtl6, cDtl6[17]),
                            UserName5 = Sm.DrStr(drDtl6, cDtl6[18]),
                            ApproveDt5 = Sm.DrStr(drDtl6, cDtl6[19]),

                            EmpPict6 = Sm.DrStr(drDtl6, cDtl6[20]),
                            Description6 = Sm.DrStr(drDtl6, cDtl6[21]),
                            UserName6 = Sm.DrStr(drDtl6, cDtl6[22]),
                            ApproveDt6 = Sm.DrStr(drDtl6, cDtl6[23]),

                            EmpPict7 = Sm.DrStr(drDtl6, cDtl6[24]),
                            Description7 = Sm.DrStr(drDtl6, cDtl6[25]),
                            UserName7 = Sm.DrStr(drDtl6, cDtl6[26]),
                            ApproveDt7 = Sm.DrStr(drDtl6, cDtl6[27]),

                            EmpPict8 = Sm.DrStr(drDtl6, cDtl6[28]),
                            Description8 = Sm.DrStr(drDtl6, cDtl6[29]),
                            UserName8 = Sm.DrStr(drDtl6, cDtl6[30]),
                            ApproveDt8 = Sm.DrStr(drDtl6, cDtl6[31]),

                            EmpPict9 = Sm.DrStr(drDtl6, cDtl6[32]),
                            Description9 = Sm.DrStr(drDtl6, cDtl6[33]),
                            UserName9 = Sm.DrStr(drDtl6, cDtl6[34]),
                            ApproveDt9 = Sm.DrStr(drDtl6, cDtl6[35]),

                            EmpPict10 = Sm.DrStr(drDtl6, cDtl6[36]),
                            Description10 = Sm.DrStr(drDtl6, cDtl6[37]),
                            UserName10 = Sm.DrStr(drDtl6, cDtl6[38]),
                            ApproveDt10 = Sm.DrStr(drDtl6, cDtl6[39])
                        });
                    }
                }
                drDtl6.Close();
            }
            myLists.Add(ldtl6);

            #endregion

            #region Signature AMKA

            var cmDtl7 = new MySqlCommand();
            var SQLDtl7 = new StringBuilder();

            SQLDtl7.AppendLine("SELECT ");
            SQLDtl7.AppendLine("    T1.EmpPict AS EmpPict1, T1.UserName AS UserName1, T1.Description AS Description1, T1.ApproveDate AS ApproveDate1, ");
            SQLDtl7.AppendLine("    T2.EmpPict AS EmpPict2, T2.UserName AS UserName2, T2.Description AS Description2, T2.ApproveDate AS ApproveDate2, ");
            SQLDtl7.AppendLine("    T3.EmpPict AS EmpPict3, T3.UserName AS UserName3, T3.Description AS Description3, T3.ApproveDate AS ApproveDate3, ");
            SQLDtl7.AppendLine("    T4.EmpPict AS EmpPict4, T4.UserName AS UserName4, T4.Description AS Description4, T4.ApproveDate AS ApproveDate4 ");
            SQLDtl7.AppendLine("FROM( ");
            if (DocType == "01")
            {
                //VR Manual
                //Level 1
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequestManual", "1", "VP Keuangan"));
                SQLDtl7.AppendLine(") T1 ");
                SQLDtl7.AppendLine("INNER JOIN( ");
                //Level 2
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequestManual", "2", "Diperiksa"));
                SQLDtl7.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 3
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequestManual", "3", "VP Akuntansi"));
                SQLDtl7.AppendLine(") T3 ON T1.DocNo = T3.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 4
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequestManual", "4", "Disetujui SVP DKA"));
                SQLDtl7.AppendLine(") T4 ON T1.DocNo = T4.DocNo ");
            }
            else if (DocType == "16")
            {
                //VR Switching Bank Account
                //Level 1
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequestSwitchingBankAcc", "1", "VP Keuangan"));
                SQLDtl7.AppendLine(") T1 ");
                SQLDtl7.AppendLine("INNER JOIN( ");
                //Level 2
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequestSwitchingBankAcc", "2", "Diperiksa"));
                SQLDtl7.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 3
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequestSwitchingBankAcc", "3", "VP Akuntansi"));
                SQLDtl7.AppendLine(") T3 ON T1.DocNo = T3.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 4
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequestSwitchingBankAcc", "4", "Disetujui SVP DKA"));
                SQLDtl7.AppendLine(") T4 ON T1.DocNo = T4.DocNo ");
            }
            else if (DocType == "02" || DocType == "18")
            {
                //VR Incoming Payment
                //Level 1
                SQLDtl7.AppendLine(GetApprovalSQL("tblincomingpaymenthdr", "IncomingPayment", "1", "VP Keuangan"));
                SQLDtl7.AppendLine(") T1 ");
                SQLDtl7.AppendLine("INNER JOIN( ");
                //Level 2
                SQLDtl7.AppendLine(GetApprovalSQL("tblincomingpaymenthdr", "IncomingPayment", "2", "Diperiksa"));
                SQLDtl7.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 3
                SQLDtl7.AppendLine(GetApprovalSQL("tblincomingpaymenthdr", "IncomingPayment", "3", "VP Akuntansi"));
                SQLDtl7.AppendLine(") T3 ON T1.DocNo = T3.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 4
                SQLDtl7.AppendLine(GetApprovalSQL("tblincomingpaymenthdr", "IncomingPayment", "4", "Disetujui SVP DKA"));
                SQLDtl7.AppendLine(") T4 ON T1.DocNo = T4.DocNo ");
            }
            else if (DocType == "03" || DocType == "17")
            {
                //VR Outgoing Payment
                //Level 1
                SQLDtl7.AppendLine(GetApprovalSQL("tbloutgoingpaymenthdr", "OutgoingPayment", "1", "VP Keuangan"));
                SQLDtl7.AppendLine(") T1 ");
                SQLDtl7.AppendLine("INNER JOIN( ");
                //Level 2
                SQLDtl7.AppendLine(GetApprovalSQL("tbloutgoingpaymenthdr", "OutgoingPayment", "2", "Diperiksa"));
                SQLDtl7.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 3
                SQLDtl7.AppendLine(GetApprovalSQL("tbloutgoingpaymenthdr", "OutgoingPayment", "3", "VP Akuntansi"));
                SQLDtl7.AppendLine(") T3 ON T1.DocNo = T3.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 4
                SQLDtl7.AppendLine(GetApprovalSQL("tbloutgoingpaymenthdr", "OutgoingPayment", "4", "Disetujui SVP DKA"));
                SQLDtl7.AppendLine(") T4 ON T1.DocNo = T4.DocNo ");
            }
            else if (DocType == "04" || DocType == "19")
            {
                //VR AP Downpayment
                //Level 1
                SQLDtl7.AppendLine(GetApprovalSQL("tblapdownpayment", "APDownpayment", "1", "VP Keuangan"));
                SQLDtl7.AppendLine(") T1 ");
                SQLDtl7.AppendLine("INNER JOIN( ");
                //Level 2
                SQLDtl7.AppendLine(GetApprovalSQL("tblapdownpayment", "APDownpayment", "2", "Diperiksa"));
                SQLDtl7.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 3
                SQLDtl7.AppendLine(GetApprovalSQL("tblapdownpayment", "APDownpayment", "3", "VP Akuntansi"));
                SQLDtl7.AppendLine(") T3 ON T1.DocNo = T3.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 4
                SQLDtl7.AppendLine(GetApprovalSQL("tblapdownpayment", "APDownpayment", "4", "Disetujui SVP DKA"));
                SQLDtl7.AppendLine(") T4 ON T1.DocNo = T4.DocNo ");
            }
            else if (DocType == "05" || DocType == "20")
            {
                //VR AR Downpayment
                //Level 1
                SQLDtl7.AppendLine(GetApprovalSQL("tblardownpayment", "ARDownpayment", "1", "VP Keuangan"));
                SQLDtl7.AppendLine(") T1 ");
                SQLDtl7.AppendLine("INNER JOIN( ");
                //Level 2
                SQLDtl7.AppendLine(GetApprovalSQL("tblardownpayment", "ARDownpayment", "2", "Diperiksa"));
                SQLDtl7.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 3
                SQLDtl7.AppendLine(GetApprovalSQL("tblardownpayment", "ARDownpayment", "3", "VP Akuntansi"));
                SQLDtl7.AppendLine(") T3 ON T1.DocNo = T3.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 4
                SQLDtl7.AppendLine(GetApprovalSQL("tblardownpayment", "ARDownpayment", "4", "Disetujui SVP DKA"));
                SQLDtl7.AppendLine(") T4 ON T1.DocNo = T4.DocNo ");
            }
            else
            {
                //VoucherRequest
                //Level 1
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequest", "1", "VP Keuangan"));
                SQLDtl7.AppendLine(") T1 ");
                SQLDtl7.AppendLine("INNER JOIN( ");
                //Level 2
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequest", "2", "Diperiksa"));
                SQLDtl7.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 3
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequest", "3", "VP Akuntansi"));
                SQLDtl7.AppendLine(") T3 ON T1.DocNo = T3.DocNo ");
                SQLDtl7.AppendLine("INNER JOIN ( ");
                //Level 4
                SQLDtl7.AppendLine(GetApprovalVRSQL("VoucherRequest", "4", "Disetujui SVP DKA"));
                SQLDtl7.AppendLine(") T4 ON T1.DocNo = T4.DocNo ");
            }

            using (var cnDtl7 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl7.Open();
                cmDtl7.Connection = cnDtl7;
                cmDtl7.CommandText = SQLDtl7.ToString();
                Sm.CmParam<String>(ref cmDtl7, "@DocNo", TxtDocNo.Text);
                var drDtl7 = cmDtl7.ExecuteReader();
                var cDtl7 = Sm.GetOrdinal(drDtl7, new string[]
            {
                    "EmpPict1",
                    "Description1",
                    "UserName1",
                    "ApproveDate1",

                    "EmpPict2",
                    "Description2",
                    "UserName2",
                    "ApproveDate2",

                    "EmpPict3",
                    "Description3",
                    "UserName3",
                    "ApproveDate3",

                    "EmpPict4",
                    "Description4",
                    "UserName4",
                    "ApproveDate4",
            });
                if (drDtl7.HasRows)
                {
                    while (drDtl7.Read())
                    {
                        ldtl7.Add(new VoucherReqDtl7()
                        {
                            EmpPict1 = Sm.DrStr(drDtl7, cDtl7[0]),
                            Description1 = Sm.DrStr(drDtl7, cDtl7[1]),
                            UserName1 = Sm.DrStr(drDtl7, cDtl7[2]),
                            ApproveDt1 = Sm.DrStr(drDtl7, cDtl7[3]),

                            EmpPict2 = Sm.DrStr(drDtl7, cDtl7[4]),
                            Description2 = Sm.DrStr(drDtl7, cDtl7[5]),
                            UserName2 = Sm.DrStr(drDtl7, cDtl7[6]),
                            ApproveDt2 = Sm.DrStr(drDtl7, cDtl7[7]),

                            EmpPict3 = Sm.DrStr(drDtl7, cDtl7[8]),
                            Description3 = Sm.DrStr(drDtl7, cDtl7[9]),
                            UserName3 = Sm.DrStr(drDtl7, cDtl7[10]),
                            ApproveDt3 = Sm.DrStr(drDtl7, cDtl7[11]),

                            EmpPict4 = Sm.DrStr(drDtl7, cDtl7[12]),
                            Description4 = Sm.DrStr(drDtl7, cDtl7[13]),
                            UserName4 = Sm.DrStr(drDtl7, cDtl7[14]),
                            ApproveDt4 = Sm.DrStr(drDtl7, cDtl7[15])
                        });
                    }
                }
                drDtl7.Close();
            }
            myLists.Add(ldtl7);

            #endregion

            #region Signature GKS

            var cmDtl8 = new MySqlCommand();
            var SQLDtl8 = new StringBuilder();

            SQLDtl8.AppendLine("SELECT 	T1.EmpPict1, T1.Description1, T1.UserName1, T1.PosName1, T1.ApproveDt1, ");
            SQLDtl8.AppendLine("       	T2.EmpPict2, T2.Description2, T2.UserName2, T2.PosName2, T2.ApproveDt2, ");
            SQLDtl8.AppendLine("       	T3.EmpPict3, T3.Description3, T3.UserName3, T3.PosName3, T3.ApproveDt3  ");
            SQLDtl8.AppendLine("FROM  ");
            SQLDtl8.AppendLine("( ");
            SQLDtl8.AppendLine("	 SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict1,  ");
            SQLDtl8.AppendLine("	    'Accountant' AS Description1, C.UserName AS UserName1, D.GrpName PosName1, IFNULL(DATE_FORMAT(Left(A.LastUpDt, 8), '%d %M %Y'), '') ApproveDt1 ");
            SQLDtl8.AppendLine("	 FROM tblvoucherrequesthdr A ");
            SQLDtl8.AppendLine("	 INNER JOIN TblUser C ON A.CreateBy = C.UserCode ");
            SQLDtl8.AppendLine("	 LEFT JOIN TblGroup D On C.GrpCode = D.GrpCode ");
            SQLDtl8.AppendLine("	 LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQLDtl8.AppendLine("	 WHERE A.DocNo = @DocNo ");
            SQLDtl8.AppendLine(")T1 ");
            SQLDtl8.AppendLine("LEFT JOIN  ");
            SQLDtl8.AppendLine("(  ");
            SQLDtl8.AppendLine("    SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict2, ");
            SQLDtl8.AppendLine("    'Approve By' AS Description2, C.UserName AS UserName2, D.GrpName PosName2, IFNULL(DATE_FORMAT(Left(A.LastUpDt, 8), '%d %M %Y'), '') ApproveDt2 ");
            SQLDtl8.AppendLine("    FROM TblDocApproval A  ");
            SQLDtl8.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType  ");
            SQLDtl8.AppendLine("        AND A.docno = @DocNo  ");
            SQLDtl8.AppendLine("        AND B.DNo = A.ApprovalDNo  ");
            SQLDtl8.AppendLine("        And B.Level = 1  ");
            SQLDtl8.AppendLine("        AND A.Status = 'A'  ");
            SQLDtl8.AppendLine("    INNER JOIN TblUser C ON A.UserCode = C.UserCode  ");
            SQLDtl8.AppendLine("    INNER JOIN TblGroup D ON C.GrpCode = D.GrpCode  ");
            SQLDtl8.AppendLine("    LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQLDtl8.AppendLine(") T2 ON T1.DocNo = T2.DocNo  ");
            SQLDtl8.AppendLine("LEFT JOIN  ");
            SQLDtl8.AppendLine("(  ");
            SQLDtl8.AppendLine("    SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict3,  ");
            SQLDtl8.AppendLine("	 'Cashier' AS Description3, C.UserName AS UserName3, D.GrpName PosName3, IFNULL(DATE_FORMAT(Left(A.LastUpDt, 8), '%d %M %Y'), '') ApproveDt3  ");
            SQLDtl8.AppendLine("    FROM TblDocApproval A  ");
            SQLDtl8.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType  ");
            SQLDtl8.AppendLine("        AND A.docno = @DocNo  ");
            SQLDtl8.AppendLine("        AND B.DNo = A.ApprovalDNo  ");
            SQLDtl8.AppendLine("        And B.Level = 2  ");
            SQLDtl8.AppendLine("        AND A.Status = 'A'  ");
            SQLDtl8.AppendLine("    INNER JOIN TblUser C ON A.UserCode = C.UserCode  ");
            SQLDtl8.AppendLine("    INNER JOIN TblGroup D ON C.GrpCode = D.GrpCode  ");
            SQLDtl8.AppendLine("    LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature'  ");
            SQLDtl8.AppendLine(") T3 ON T1.DocNo = T3.DocNo  ");

            using (var cnDtl8 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl8.Open();
                cmDtl8.Connection = cnDtl8;
                cmDtl8.CommandText = SQLDtl8.ToString();
                Sm.CmParam<String>(ref cmDtl8, "@DocNo", TxtDocNo.Text);
                var drDtl8 = cmDtl8.ExecuteReader();
                var cDtl8 = Sm.GetOrdinal(drDtl8, new string[]
                {
                    //0
                    "EmpPict1",
                    "Description1",
                    "Username1",
                    "ApproveDt1",

                    "EmpPict2",
                    "Description2",
                    "Username2",
                    "ApproveDt2",

                    "EmpPict3",
                    "Description3",
                    "Username3",
                    "ApproveDt3",

                    "PosName1",
                    "PosName2",
                    "PosName3",
                });
                if (drDtl8.HasRows)
                {
                    while (drDtl8.Read())
                    {
                        ldtl8.Add(new VoucherReqDtl8()
                        {
                            EmpPict1 = Sm.DrStr(drDtl8, cDtl8[0]),
                            Description1 = Sm.DrStr(drDtl8, cDtl8[1]),
                            UserName1 = Sm.DrStr(drDtl8, cDtl8[2]),
                            ApproveDt1 = Sm.DrStr(drDtl8, cDtl8[3]),
                            EmpPict2 = Sm.DrStr(drDtl8, cDtl8[4]),
                            Description2 = Sm.DrStr(drDtl8, cDtl8[5]),
                            UserName2 = Sm.DrStr(drDtl8, cDtl8[6]),
                            ApproveDt2 = Sm.DrStr(drDtl8, cDtl8[7]),
                            EmpPict3 = Sm.DrStr(drDtl8, cDtl8[8]),
                            Description3 = Sm.DrStr(drDtl8, cDtl8[9]),
                            UserName3 = Sm.DrStr(drDtl8, cDtl8[10]),
                            ApproveDt3 = Sm.DrStr(drDtl8, cDtl8[11]),
                            PosName1 = Sm.DrStr(drDtl8, cDtl8[12]),
                            PosName2 = Sm.DrStr(drDtl8, cDtl8[13]),
                            PosName3 = Sm.DrStr(drDtl8, cDtl8[14]),
                        });
                    }
                }
                drDtl8.Close();
            }
            myLists.Add(ldtl8);

            # endregion

            #region Signature SIER

            var cmDtl9 = new MySqlCommand();
            var SQLDtl9 = new StringBuilder();

            SQLDtl9.AppendLine("Select A.Signature, B.EmpName, C.PosName, A.Seq, A.Title, DATE_FORMAT(A.LastUpDt, '%d/%m/%Y') as LastUpDt  ");
            SQLDtl9.AppendLine("FROM ( ");
            SQLDtl9.AppendLine("SELECT Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As Signature, A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt ");
            SQLDtl9.AppendLine("FROM TblDocApproval A ");
            SQLDtl9.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 2 ");
            SQLDtl9.AppendLine("Inner Join tbluser C ON A.UserCode = C.UserCode ");
            SQLDtl9.AppendLine("Inner Join tblparameter D ON D.ParCode = 'ImgFileSignature' ");
            SQLDtl9.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl9.AppendLine("UNION ALL ");
            SQLDtl9.AppendLine("SELECT Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As Signature, A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt ");
            SQLDtl9.AppendLine("FROM TblDocApproval A ");
            SQLDtl9.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 1 ");
            SQLDtl9.AppendLine("Inner Join tbluser C ON A.UserCode = C.UserCode ");
            SQLDtl9.AppendLine("Inner Join tblparameter D ON D.ParCode = 'ImgFileSignature' ");
            SQLDtl9.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl9.AppendLine("UNION ALL ");
            SQLDtl9.AppendLine("SELECT Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As Signature, A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt ");
            SQLDtl9.AppendLine("FROM TblDocApproval A ");
            SQLDtl9.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 3 ");
            SQLDtl9.AppendLine("Inner Join tbluser C ON A.UserCode = C.UserCode ");
            SQLDtl9.AppendLine("Inner Join tblparameter D ON D.ParCode = 'ImgFileSignature' ");
            SQLDtl9.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl9.AppendLine("UNION ALL ");
            SQLDtl9.AppendLine("SELECT Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As Signature, A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt ");
            SQLDtl9.AppendLine("FROM TblDocApproval A ");
            SQLDtl9.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 4 ");
            SQLDtl9.AppendLine("Inner Join tbluser C ON A.UserCode = C.UserCode ");
            SQLDtl9.AppendLine("Inner Join tblparameter D ON D.ParCode = 'ImgFileSignature' ");
            SQLDtl9.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl9.AppendLine("UNION ALL ");
            SQLDtl9.AppendLine("SELECT Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As Signature, A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt ");
            SQLDtl9.AppendLine("FROM TblDocApproval A ");
            SQLDtl9.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 5 ");
            SQLDtl9.AppendLine("Inner Join tbluser C ON A.UserCode = C.UserCode ");
            SQLDtl9.AppendLine("Inner Join tblparameter D ON D.ParCode = 'ImgFileSignature' ");
            SQLDtl9.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl9.AppendLine("UNION ALL ");
            SQLDtl9.AppendLine("SELECT Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As Signature, A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt ");
            SQLDtl9.AppendLine("FROM TblDocApproval A ");
            SQLDtl9.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 6 ");
            SQLDtl9.AppendLine("Inner Join tbluser C ON A.UserCode = C.UserCode ");
            SQLDtl9.AppendLine("Inner Join tblparameter D ON D.ParCode = 'ImgFileSignature' ");
            SQLDtl9.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl9.AppendLine("UNION ALL ");
            SQLDtl9.AppendLine("SELECT Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As Signature, A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt ");
            SQLDtl9.AppendLine("FROM TblDocApproval A ");
            SQLDtl9.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 7 ");
            SQLDtl9.AppendLine("Inner Join tbluser C ON A.UserCode = C.UserCode ");
            SQLDtl9.AppendLine("Inner Join tblparameter D ON D.ParCode = 'ImgFileSignature' ");
            SQLDtl9.AppendLine("Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
            SQLDtl9.AppendLine("UNION ALL ");
            SQLDtl9.AppendLine("Select Concat(IfNull(C.ParValue, ''), B.UserCode, '.JPG') As Signature, A.CreateBy As UserCode, 200 As Seq, 'Proposed By,' As Title, Left(A.CreateDt, 8) As LastUpDt ");
            SQLDtl9.AppendLine("From TblVoucherRequestHdr A ");
            SQLDtl9.AppendLine("Inner Join tbluser B ON A.CreateBy = B.UserCode ");
            SQLDtl9.AppendLine("Inner Join tblparameter C ON C.ParCode = 'ImgFileSignature' ");
            SQLDtl9.AppendLine("Where A.DocNo = @DocNo ");
            SQLDtl9.AppendLine(") A ");
            SQLDtl9.AppendLine("Left Join TblEmployee B On A.UserCode = B.UserCode And (ResignDt Is Null Or ResignDt > Left(currentdatetime(), 8)) ");
            SQLDtl9.AppendLine("Left Join TblPosition C On B.PosCode = C.PosCode ");
            SQLDtl9.AppendLine("Group By A.UserCode, B.EmpCode, C.PosCode, A.Seq, A.Title, A.LastUpDt ");
            SQLDtl9.AppendLine("Order By A.Seq ");

            using (var cnDtl9 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl9.Open();
                cmDtl9.Connection = cnDtl9;
                cmDtl9.CommandText = SQLDtl9.ToString();
                Sm.CmParam<String>(ref cmDtl9, "@DocNo", TxtDocNo.Text);
                var drDtl9 = cmDtl9.ExecuteReader();
                var cDtl9 = Sm.GetOrdinal(drDtl9, new string[]
                {
                    //0
                    "Signature",

                    //1-5
                    "EmpName" ,
                    "PosName" ,
                    "LastUpDt",
                    "Seq",
                    "Title",
                });
                if (drDtl9.HasRows)
                {
                    while (drDtl9.Read())
                    {
                        ldtl9.Add(new VoucherReqDtl9()
                        {
                            Signature = Sm.DrStr(drDtl9, cDtl9[0]),
                            EmpName = Sm.DrStr(drDtl9, cDtl9[1]),
                            Position = Sm.DrStr(drDtl9, cDtl9[2]),
                            Date = Sm.DrStr(drDtl9, cDtl9[3]),
                            Sequence = Sm.DrStr(drDtl9, cDtl9[4]),
                            Title = Sm.DrStr(drDtl9, cDtl9[5]),
                            Space = "                       ",
                            LabelName = "", //Name       : 
                            LabelPos = "", //Position    : 
                            LabelDt = "Date : ",
                        });
                    }
                }
                drDtl9.Close();
            }
            myLists.Add(ldtl9);

            #endregion

            #region Signature BBT

            var cmDtl10 = new MySqlCommand();
            var SQLDtl10 = new StringBuilder();

            using (var cnDtl10 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl10.Open();
                cmDtl10.Connection = cnDtl10;

                SQLDtl10.AppendLine("Select B.UserName, C.GrpName PosName, A.Seq, A.Title, DATE_FORMAT(A.LastUpDt, '%d/%m/%Y') as LastUpDt, A.EmpPict, @Space As Space, Null Level ");
                SQLDtl10.AppendLine("From ( ");
                SQLDtl10.AppendLine("    Select A.CreateBy As UserCode, 0 As Seq, 'Created By,' As Title, Left(A.CreateDt, 8) As LastUpDt, Concat(IfNull(C.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict ");
                SQLDtl10.AppendLine("    From TblVoucherRequestHdr A ");
                SQLDtl10.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl10.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl10.AppendLine("    Where A.DocNo = @DocNo ");
                SQLDtl10.AppendLine("    Union All ");
                SQLDtl10.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl10.AppendLine("    FROM TblDocApproval A ");
                SQLDtl10.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 1 ");
                SQLDtl10.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl10.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl10.AppendLine("    Union All ");
                SQLDtl10.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl10.AppendLine("    FROM TblDocApproval A ");
                SQLDtl10.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 2 ");
                SQLDtl10.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl10.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl10.AppendLine("    Union All ");
                SQLDtl10.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl10.AppendLine("    FROM TblDocApproval A ");
                SQLDtl10.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 3 ");
                SQLDtl10.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl10.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl10.AppendLine("    Union All ");
                SQLDtl10.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl10.AppendLine("    FROM TblDocApproval A ");
                SQLDtl10.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 4 ");
                SQLDtl10.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl10.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl10.AppendLine("    Union All ");
                SQLDtl10.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl10.AppendLine("    FROM TblDocApproval A ");
                SQLDtl10.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 5 ");
                SQLDtl10.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl10.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl10.AppendLine("    ) A ");
                SQLDtl10.AppendLine("Left Join Tbluser B On A.UserCode = B.UserCode ");
                SQLDtl10.AppendLine("Left Join TblGroup C On B.GrpCode = C.GrpCode ");
                SQLDtl10.AppendLine("Group By A.UserCode, A.Seq, A.Title, A.LastUpDt ");
                SQLDtl10.AppendLine("Order By A.Seq Desc; ");

                cmDtl10.CommandText = SQLDtl10.ToString();
                Sm.CmParam<String>(ref cmDtl10, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl10, "@DocNo", TxtDocNo.Text);
                var drDtl10 = cmDtl10.ExecuteReader();
                var cDtl10 = Sm.GetOrdinal(drDtl10, new string[]
                {
                        //0
                        "EmpPict" ,

                        //1-5
                        "UserName" ,
                        "PosName",
                        "Space",
                        "Level",
                        "Title",

                        "LastupDt",
                        "Seq"
                });
                if (drDtl10.HasRows)
                {
                    while (drDtl10.Read())
                    {

                        ldtl10.Add(new VoucherReqDtl10()
                        {
                            Signature = Sm.DrStr(drDtl10, cDtl10[0]),
                            UserName = Sm.DrStr(drDtl10, cDtl10[1]),
                            PosName = Sm.DrStr(drDtl10, cDtl10[2]),
                            Space = "",
                            DNo = Sm.DrStr(drDtl10, cDtl10[4]),
                            Title = Sm.DrStr(drDtl10, cDtl10[5]),
                            LastUpDt = Sm.DrStr(drDtl10, cDtl10[6]),
                            Sequence = Sm.DrStr(drDtl10, cDtl10[7]),
                        });
                    }
                }
                drDtl10.Close();
            }
            myLists.Add(ldtl10);

            #endregion

            if (Doctitle == "AMKA" && mIsVRApprovalByType == true)
            {
                if (DocType == "58" || DocType == "56")
                {
                    Sm.PrintReport("VRCashAdvanceSettlementAMKA", myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport("VRAMKA", myLists, TableName, false);
                }
            }
            else
            {
                if (Doctitle == "iProbe")
                {
                    Sm.PrintReport("VoucherRequestGKS", myLists, TableName, false);
                }
                else
                {
                    if (mMenuCode == "0102025007" && Doctitle == "SIER")
                        Sm.PrintReport("VoucherRequestForBudgetSIER", myLists, TableName, false);
                    else
                        Sm.PrintReport(mIsPrintOutVR, myLists, TableName, false);
                }
            }

            if (Doctitle == "KIM" && DocType == "23") Sm.PrintReport("KIMSppd", myLists, TableName, false);
        }

        private string GetApprovalSQL(string Tbl, string ApprovalDocType, string Lvl, string Description)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(H.ParValue, ''), C.UserCode, '.JPG') As EmpPict, D.UserName AS UserName,'" + Description + "' As Description, DATE_FORMAT(Left(C.LastUpDt, 8),'%d %M %Y') As ApproveDate ");
            SQL.AppendLine("FROM tblvoucherrequesthdr A ");
            SQL.AppendLine("INNER JOIN " + Tbl + " B ON A.DocNo = B.VoucherRequestDocNo");
            SQL.AppendLine("INNER JOIN tbldocapproval C ON B.DocNo = C.DocNo ");
            SQL.AppendLine("INNER JOIN tbluser D ON C.UserCode = D.UserCode ");
            SQL.AppendLine("INNER JOIN Tbldocapprovalsetting E On C.ApprovalDNo=E.DNo And E.DocType = '" + ApprovalDocType + "' AND E.`Level` = '" + Lvl + "' ");
            SQL.AppendLine("LEFT JOIN tblemployee F ON C.UserCode = F.UserCode ");
            SQL.AppendLine("LEFT JOIN tblposition G ON F.PosCode = G.PosCode ");
            SQL.AppendLine("LEFT JOIN tblparameter H ON H.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("WHERE A.CancelInd='N' AND A.DocNo=@DocNo ");
            SQL.AppendLine("GROUP BY H.ParValue, C.UserCode, D.UserName, G.PosName ");

            return SQL.ToString();
        }

        private string GetApprovalVRSQL(string ApprovalDocType, string Lvl, string Description)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict,  C.UserName AS UserName,'" + Description + "' As Description, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDate ");
            SQL.AppendLine("From tblvoucherrequesthdr A ");
            SQL.AppendLine("Inner Join TblDocApproval B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblUser C ON B.UserCode=C.UserCode ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = '" + ApprovalDocType + "' AND D.`Level` = '" + Lvl + "' ");
            SQL.AppendLine("LEFT JOIN tblemployee E ON B.UserCode = E.UserCode ");
            SQL.AppendLine("LEFT JOIN tblposition F ON E.PosCode = F.PosCode ");
            SQL.AppendLine("LEFT JOIN tblparameter G ON G.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
            SQL.AppendLine("GROUP BY G.ParValue, B.UserCode, C.UserName, F.PosName ");

            return SQL.ToString();
        }

        private void BtnCSV_Click(object sender, EventArgs e)
        {
            if (mIsVRUseBillingID)
            {
                bool IsBillingIdComplated = (Grd1.Rows.Count > 1);
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, 3).Length == 0)
                        IsBillingIdComplated = false;
                }

                if ((!mIsVRUseMultiBillingID && TxtBillingID.Text.Length > 0) || (mIsVRUseMultiBillingID && IsBillingIdComplated))
                {
                    if (!BtnSave.Enabled &&
                    TxtDocNo.Text.Length > 0 &&
                    mIsVRSendtoBank &&
                    !ChkCancelInd.Checked &&
                    Sm.GetValue("Select Status From TblVoucherRequestHdr Where DocNo = '" + TxtDocNo.Text + "'") == "A" &&
                    !Sm.IsDataExist("Select 1 From TblVoucherHdr Where VoucherRequestDocNo = @Param", TxtDocNo.Text) &&
                    Sm.GetValue("Select CSVInd From TblVoucherRequestHdr Where DocNo = '" + TxtDocNo.Text + "'") == "N"
                    )
                    {
                        var lBank2 = new List<Bank2>();

                        string mProcCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ProcCodeForBRIBillingID';");
                        string mDebitAccount = Sm.GetValue("Select BankAcNo From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "'");
                        string mReference = string.Concat(TxtDocNo.Text, "_1");
                        string mDate = string.Concat(Sm.Left(Sm.GetDte(DteDocDt), 8), Sm.Right(Sm.ServerCurrentDateTime(), 4));

                        if (mIsVRUseMultiBillingID)
                        {
                            for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                            {
                                lBank2.Add(new Bank2()
                                {
                                    ProcCode = mProcCode,
                                    BillingID = Sm.GetGrdStr(Grd1, row, 3),
                                    DebitAccount = mDebitAccount,
                                    Reference = TxtDocNo.Text + "_" + (row + 1).ToString(),
                                    Date = mDate,
                                    Email = "",
                                });
                            }
                        }
                        else
                        {
                            lBank2.Add(new Bank2()
                            {
                                ProcCode = mProcCode,
                                BillingID = TxtBillingID.Text,
                                DebitAccount = mDebitAccount,
                                Reference = mReference,
                                Date = mDate,
                                Email = "",
                                Count = 1,
                            });
                        }

                        if (lBank2.Count > 0)
                        {
                            foreach (var x in lBank2)
                            {
                                if (x.ProcCode.Length <= 0)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Proc Code can't be empty.");
                                    return;
                                }
                                if (x.BillingID.Length <= 0)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Billing Id can't be empty.");
                                    return;
                                }
                                if (x.DebitAccount.Length <= 0)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Account# cant't be empty.");
                                    return;
                                }
                                if (x.Reference.Length <= 0)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Unique Reference cant't be empty.");
                                    return;
                                }
                            }
                            ExportCSVBRI2(ref lBank2);

                        }
                    }
                }
            }
            else
            {
                if (!BtnSave.Enabled &&
                TxtDocNo.Text.Length > 0 &&
                mIsVRSendtoBank &&
                !ChkCancelInd.Checked &&
                Sm.GetValue("Select Status From TblVoucherRequestHdr Where DocNo = '" + TxtDocNo.Text + "'") == "A"
                && Sm.GetValue("Select CSVInd From TblVoucherRequestHdr Where DocNo = '" + TxtDocNo.Text + "'") == "N"
                && Sm.GetLue(LueDocType) != mVoucherDocTypePayrollProcessing
                && Sm.GetLue(LueAcType) == "C"
                )
                {
                    var lBank = new List<Bank>();
                    var SQL = new StringBuilder();
                    var cm = new MySqlCommand();


                    string mTrxCode = string.Empty;
                    string mBankAddress = Sm.GetValue("SELECT CONCAT (B.ProvName, ';' , C.CityName, ';', Address ) FROM TblBank A " +
                                           " LEFT JOIN TblProvince B ON A.ProvCode = B.ProvCode " +
                                           " LEFT JOIN TblCity C ON A.CityCode = C.Citycode AND A.BankCode=@Param; ", Sm.GetLue(LueBankCode));
                    string mNotification = Sm.GetValue("SELECT IfNull(Replace(Trim(B.Email),',',';'), '') FROM TblUser A " +
                                           " INNER JOIN TblEmployee B ON A.UserCode = B.UserCode AND A.UserCode=@Param; ", Gv.CurrentUserCode);
                    string mRTGSCode = Sm.GetValue("Select RTGSCode From TblBank Where BankCode=@Param; ", Sm.GetLue(LueBankCode));
                    for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                    {
                        if (mTrxCode.Length > 0) mTrxCode += " ";
                        mTrxCode += Sm.GetGrdStr(Grd1, i, 1);
                    }

                    lBank.Add(new Bank()
                    {
                        No = "1",
                        DocNo = TxtDocNo.Text,
                        InstructionCode = mInstructionCodeForTransferInstructionType,
                        FxCode = "ID",
                        DocDt = "",//Sm.Left((Sm.GetDte(DteDocDt)), 12),
                        CurCode = Sm.GetLue(LueCurCode),
                        Amt = mIsCSVUseRealAmt ? Decimal.Parse(TxtAmt.Text) : 1m,
                        TrxRemark = mTrxCode,
                        RateVoucherCode = string.Empty,
                        ChargeType = mBankChargeTypeForVR,

                        DebitAccount = Sm.GetValue("Select BankAcno From TblbankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "'"),
                        SenderName = Sm.GetParameter("ReportTitle1"),
                        SenderAddress = string.Concat(Sm.GetParameter("ReportTitle2"), ";", Sm.GetParameter("ReportTitle3")).Replace(',', ' '),
                        SenderCountryCode = "ID",
                        CreditAccount = TxtBankAcNo.Text,
                        BeneficiaryName = TxtBankAcName.Text,
                        BeneficiaryAddress = TxtBankBranch.Text,
                        BeneficiaryCountryCode = "ID",
                        BenBankIdentifier = mRTGSCode,
                        BenBankName = LueBankCode.Text,

                        BenBankAddress = mBankAddress.Replace(',', ' '),
                        BenBankCountryCode = string.Empty,
                        InterBankIdentifier = string.Empty,
                        InterBankName = string.Empty,
                        InterBankAddress = string.Empty,
                        InterBankCountryCode = string.Empty,
                        Notification = mNotification,
                        BeneficiaryCategory = "E0",
                        BeneficiaryRelation = "N",
                        BITransactionCode = "011",

                        TemplateCode = "1"
                    });



                    if (lBank.Count > 0)
                    {
                        for (int i = 0; i < lBank.Count; i++)
                        {
                            if (lBank[i].TrxRemark.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Voucher Description can't be empty.");
                                return;
                            }

                            if (lBank[i].CreditAccount.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Account# can't be empty.");
                                return;
                            }

                            if (lBank[i].BeneficiaryName.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Account Name can't be empty.");
                                return;
                            }

                            if (lBank[i].BeneficiaryAddress.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Branch Name can't be empty.");
                                return;
                            }

                            if (lBank[i].BenBankIdentifier.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "RTGS code in master bank can't be empty.");
                                return;
                            }

                            if (lBank[i].BenBankName.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Bank Name can't be empty.");
                                return;
                            }

                            if (lBank[i].BenBankAddress.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Bank Address in master Bank can't be empty.");
                                return;
                            }
                        }
                        ExportCSVBRI(ref lBank);

                    }

                }
            }
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (MeeCancelReason.Text.Length > 0) ChkCancelInd.Checked = true;
            //if (IsVoucherRequestApprovedAlready()) return;
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;


            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            var ExistRowsCount = Int32.Parse(Sm.GetValue("SELECT count(DNo) FROM tblvoucherrequestdtl4 WHERE docno = @Param", TxtDocNo.Text));
            var CurrentRowsCount = Grd5.Rows.Count - 1;
            if (ChkCancelInd.Checked)
                cml.Add(CancelVoucherRequestHdr());

            else
            {
                // UpdateVoucherRequestDtl4
                
                if ( CurrentRowsCount > ExistRowsCount )
                {
                    for (int Row = ExistRowsCount; Row < CurrentRowsCount; Row++)
                    {
                        // UpdateVoucherRequestDtl4(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd5, Row, 1));
                        cml.Add(SaveVoucherRequestDtl4(TxtDocNo.Text, Row)); 
                    }
                }

            }


            Sm.ExecCommands(cml);
            if (!ChkCancelInd.Checked) {
                if (mVoucherRequestUploadFormat == "3")
                {
                    for (int Row = ExistRowsCount ; Row < CurrentRowsCount; Row++)
                    {
                        if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
                        {
                            UploadFile5(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd5, Row, 1));
                        }
                    }
                }
            }




            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                (mIsVoucherRequestTransactionValidatedbyClosingJournal && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode())) ||
                //IsVoucherRequestNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready() ||
                (mIsVRSendtoBank && IsDataProcessedtoBankAlready()) ||
                IsClaimTransactionActive();
            ;
        }

        private bool IsVoucherRequestApprovedAlready()
        {
            var mVRStatus = Sm.GetValue("SELECT Status FROM tblvoucherrequesthdr WHERE DocNo = @Param", TxtDocNo.Text);

            if (mVRStatus == "A")
            {
                Sm.StdMsg(mMsgType.Warning, "This Data already Approved .");
                // Sm.StdMsg(mMsgType.Warning, "You need to cancel this voucher request.");
                return true;
            }
            return false;
        
        }

        private bool IsVoucherRequestNotCancelled()
        {
            if (ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this voucher request.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblVoucherRequestHdr " +
                "Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private bool IsDataProcessedAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblVoucherHdr Where CancelInd='N' And VoucherRequestDocNo=@Param;",
                TxtDocNo.Text,
                "Data already processed into voucher.");
        }

        private bool IsClaimTransactionActive()
        {
            if (Sm.GetLue(LueDocType) != "08") return false;
            var mEmpClaimSQL = "SELECT DocNo FROM tblempclaimhdr WHERE voucherrequestdocno = @Param AND CancelInd = 'N' And Status In ('O', 'A'); ";
            var mEmpClaimDocNo = Sm.GetValue(mEmpClaimSQL, TxtDocNo.Text);

            if (mEmpClaimDocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel the claim transaction.\rClaim Document#: " + mEmpClaimDocNo + " .");
                return true;
            }
            return false;


        }

        //You need to cancel the claim transaction.

        private MySqlCommand CancelVoucherRequestHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsDataProcessedtoBankAlready()
        {
            return Sm.IsDataExist(
                    "Select 1 From TblVoucherRequestHdr " +
                    "Where CsvInd = 'Y' And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already processed to bank.");
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            var SQL = new StringBuilder();
            string DocType = string.Empty;
            string PettyCashDocNo = Sm.GetValue("Select DocNo From TblPettyCashDisbursementHdr Where VoucherRequestDocNo = @Param Limit 1;", DocNo);
            string ProfitCenterTest = GetProfitCenterCode();

            SQL.AppendLine("Select ");
            if (mIsVRApprovalByType)
            {
                if (mMenuCodeForVRBudget != mMenuCode)
                {
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Case ");
                    SQL.AppendLine("        When DocType = '01' Then 'VoucherRequestManual' ");
                    SQL.AppendLine("        When DocType = '16' Then 'VoucherRequestSwitchingBankAcc' ");
                    SQL.AppendLine("        When DocType = '56' Then 'VoucherRequestCashAdvance' ");
                    SQL.AppendLine("        When DocType = '74' Then 'VoucherRequestPettyCash' ");
                    SQL.AppendLine("    End");
                    SQL.AppendLine(") ");
                }
                else
                    SQL.Append("'VoucherRequestForBudget' ");
            }
            else
                SQL.Append("'VoucherRequest' ");
            SQL.AppendLine("From TblVoucherRequestHdr Where DocNo = @Param;");

            if (PettyCashDocNo.Length > 0)
                DocType = "PettyCashDisbursement";
            else
                DocType = Sm.GetValue(SQL.ToString(), DocNo);

            try
            {
                ClearData();
                ShowVoucherRequestHdr(DocNo);
                ShowVoucherRequestDtl(DocNo);
                ShowVoucherRequestDtl2(DocNo);
                ShowVoucherRequestDtl3(DocNo);
                ShowVoucherRequestDtl4(DocNo);
                if (mMenuCodeForVRBudget == mMenuCode)
                { ComputeAmt(); ComputeRemainingBudget(); }
                ComputeDebetCredit(); ComputeBalanced();
                Sm.ShowDocApproval((PettyCashDocNo.Length > 0 ? PettyCashDocNo : DocNo), DocType, ref Grd2, mIsTransactionUseDetailApprovalInformation);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        internal void CopyData(string DocNo)
        {
            try
            {
                ClearData();
                CopyDataHdr(DocNo);
                CopyDataDtl(DocNo);
                if (mMenuCodeForVRBudget == mMenuCode)
                { ComputeAmt(); ComputeRemainingBudget(); }
            }

            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }
        private void CopyDataHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.`Status`, A.DeptCode, A.DocType, ");
            SQL.AppendLine("A.VoucherDocNo, A.AcType, A.BankAcCode, A.AcType2, A.BankAcCode2, ");
            SQL.AppendLine("A.PaymentType, A.BankCode, A.GiroNo, A.OpeningDt, A.DueDt, A.PIC, ");
            SQL.AppendLine("A.CurCode, A.Amt, A.Amt2, A.CurCode2, A.ExcRate, A.PaymentUser, A.DocEnclosure, ");
            SQL.AppendLine("A.PaidToBankCode, A.PaidToBankBranch, A.PaidToBankAcNo, ");
            SQL.AppendLine("A.PaidToBankAcName, A.LocalDocNo, A.EntCode, A.ProjectDocNo1, A.ProjectDocNo2, ");
            SQL.AppendLine("A.ProjectDocNo3, A.RefundDt, A.SiteCode, A.ReqType, A.BCCode, A.ManualCurCode, ");
            SQL.AppendLine("A.ManualExcRate, A.Remark, A.SOContractDocNo, A.FGItCode, A.AcNo, A.BIOPProjectInd, ");
            SQL.AppendLine("A.DeptCode2, B.ItName, A.BRIBillingID, C.AcDesc ");
            SQL.AppendLine("FROM tblvoucherrequesthdr A ");
            SQL.AppendLine("LEFT JOIN tblitem B ON A.FGItCode = B.ItCode ");
            SQL.AppendLine("LEFT JOIN tblcoa C ON A.AcNo = C.AcNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "Status", "DeptCode", "DocType", "VoucherDocNo",

                    //6-10
                    "AcType", "BankAcCode", "AcType2", "BankAcCode2", "PaymentType", 

                    //11-15
                    "BankCode", "GiroNo", "OpeningDt", "DueDt", "PIC", 

                    //16-20
                    "CurCode", "Amt", "Amt2", "CurCode2", "ExcRate", 

                    //21-25
                    "PaymentUser", "DocEnclosure", "PaidToBankCode", "PaidToBankBranch", "PaidToBankAcNo", 

                    //26-30
                    "PaidToBankAcName", "LocalDocNo", "EntCode", "ProjectDocNo1", "ProjectDocNo2", 

                    //31-35
                    "ProjectDocNo3", "RefundDt", "SiteCode", "ReqType", "BCCode", 

                    //36-40
                    "ManualCurCode", "ManualExcRate", "Remark", "SOContractDocNo", "FGItCode", 

                    //41-45
                    "AcNo", "BIOPPRojectInd", "DeptCode2", "ItName", "BRIBillingID",

                    //46
                    "AcDesc"



                },
                (MySqlDataReader dr, int[] c) =>
                {
                    Sm.SetDteCurrentDate(DteDocDt);
                    //Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    //TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[3]));
                    SetLueVoucherDocType(ref LueDocType, Sm.DrStr(dr, c[4]));
                    //TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[5]);
                    Sm.SetLue(LueAcType, Sm.DrStr(dr, c[6]));
                    SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[7]));
                    Sm.SetLue(LueAcType2, Sm.DrStr(dr, c[8]));
                    SetLueBankAcCode(ref LueBankAcCode2, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[11]));
                    TxtGiroNo.EditValue = Sm.DrStr(dr, c[12]);
                    Sm.SetDte(DteOpeningDt, Sm.DrStr(dr, c[13]));
                    Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[14]));
                    Sl.SetLueUserCode(ref LuePIC, Sm.DrStr(dr, c[15]));
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[16]));
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                    TxtAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                    TxtCurCode2.EditValue = Sm.DrStr(dr, c[19]);
                    TxtExcRate.EditValue = FormatNum(Sm.DrDec(dr, c[20]));
                    TxtPaymentUser.EditValue = Sm.DrStr(dr, c[21]);
                    TxtDocEnclosure.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 0);
                    TxtBankCode.EditValue = Sm.DrStr(dr, c[23]);
                    TxtBankBranch.EditValue = Sm.DrStr(dr, c[24]);
                    TxtBankAcNo.EditValue = Sm.DrStr(dr, c[25]);
                    TxtBankAcName.EditValue = Sm.DrStr(dr, c[26]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[27]);
                    Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[28]));
                    if (mIsProjectSystemActive)
                    {
                        Sm.SetLue(LueProjectDocNo1, Sm.DrStr(dr, c[29]));
                        SetLueProjectSystem2(ref LueProjectDocNo2, Sm.DrStr(dr, c[29]));
                        Sm.SetLue(LueProjectDocNo2, Sm.DrStr(dr, c[30]));
                        SetLueProjectSystem3(ref LueProjectDocNo3, Sm.DrStr(dr, c[30]));
                        Sm.SetLue(LueProjectDocNo3, Sm.DrStr(dr, c[31]));
                    }
                    Sm.SetDte(DteRefundDt, Sm.DrStr(dr, c[32]));

                    if (mMenuCodeForVRBudget == mMenuCode)
                    {
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[33]));
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[34]));
                        SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[35]), mIsBudgetTransactionCanUseOtherDepartmentBudget ? Sm.DrStr(dr, c[43]) : Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[35]));
                        Sm.SetLue(LueDeptCode2, Sm.DrStr(dr, c[43]));
                    }
                    Sm.SetLue(LueManualCurCode, Sm.DrStr(dr, c[36]));
                    TxtManualExcRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[37]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[38]);

                    if (mIsVRForBudgetUseSOContract)
                    {
                        TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[39]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[40]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[44]);
                        TxtAcNo.EditValue = Sm.DrStr(dr, c[41]);
                        TxtAcDesc.EditValue = Sm.DrStr(dr, c[46]);
                    }

                    Sm.SetLue(LueBIOPProject, Sm.DrStr(dr, c[42]));
                    TxtBillingID.EditValue = Sm.DrStr(dr, c[45]);
                }, true
                );
        }

        private void CopyDataDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select DNo, Description, Amt, AmtNonMain, BRIBillingID, Remark ");
            SQL.AppendLine("From TblVoucherRequestDtl ");
            SQL.AppendLine("Where DocNo=@DocNo Order By Dno ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),

                new string[]
                {
                    "DNo","Description","Amt","BRIBillingID", "Remark", "AmtNonMain"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowVoucherRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.DeptCode, A.CancelReason, A.CancelInd, A.DocType, A.VoucherDocNo, ");
            SQL.AppendLine("A.AcType, A.BankAcCode, A.AcType2, A.BankAcCode2, ");
            SQL.AppendLine("A.PaymentType, A.GiroNo, A.BankCode, A.OpeningDt, A.DueDt, A.PIC, A.CurCode, A.Amt, A.DocEnclosure, A.PaymentUser, A.Remark, ");
            SQL.AppendLine("A.PaidToBankCode, A.PaidToBankBranch, A.PaidToBankAcNo, A.PaidToBankAcName, A.CurCode2, A.ExcRate, A.DocEnclosure, A.EntCode, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("B.BankAcTp, C.BankAcTp As BankAcTp2, A.RefundDt, A.ProjectDocNo1, A.ProjectDocNo2, A.ProjectDocNo3, A.SiteCode, A.ReqType, A.BCCode, ");
            SQL.AppendLine("A.ManualCurCode, A.ManualExcRate, A.Amt2, A.BRIBillingID, A.DeptCode2,  ");
            if (mIsVRUseSOContract || mIsVRForBudgetUseSOContract)
                SQL.AppendLine("A.SOContractDocNo, A.FGItCode, D.ItName, A.AcNo, E.AcDesc, A.BIOPProjectInd, ");
            else
                SQL.AppendLine("Null As SOContractDocNo, Null As FGItCode, Null As ItName, Null As AcNo, Null As AcDesc, 'N' As BIOPProjectInd, ");
            if (mIsVRAllowToUploadFile || mVoucherRequestUploadFormat == "1")
            {
                SQL.AppendLine("A.FileName, A.FileName2, A.FileName3 ");
            }
            else
            {
                SQL.AppendLine("Null as FileName, Null as FileName2, Null as FileName3 ");
            }

            if (mIsVoucherRequestCASUseDueDate) SQL.AppendLine(", A.CashAdvanceTypeCode ");
            else SQL.AppendLine(", Null As CashAdvanceTypeCode ");

            if (mIsVRCashAdvanceUseEmployeePIC) SQL.AppendLine(", A.EmpCodeCashAdv, F.EmpName As EmpNameCashAdv ");
            else SQL.AppendLine(", Null As EmpCodeCashAdv, Null As EmpNameCashAdv ");

            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Left Join TblBankAccount B On A.BankAcCode=B.BankAcCode ");
            SQL.AppendLine("Left Join TblBankAccount C On A.BankAcCode2=C.BankAcCode ");
            if (mIsVRUseSOContract || mIsVRForBudgetUseSOContract)
            {
                SQL.AppendLine("Left Join TblItem D On A.FGItCode = D.ItCode ");
                SQL.AppendLine("Left Join TblCOA E On A.AcNo = E.AcNo ");
            }
            if (mIsVRCashAdvanceUseEmployeePIC) SQL.AppendLine("Left Join TblEmployee F On A.EmpCodeCashAdv = F.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "LocalDocNo", "DocDt", "CancelReason", "DocType", "VoucherDocNo", 

                    //6-10
                    "AcType", "BankAcCode", "AcType2", "BankAcCode2", "PaymentType", 
                    
                    //11-15
                    "GiroNo", "BankCode", "DueDt", "PIC", "CurCode",
                    
                    //16-20
                    "Amt", "CancelInd", "PaymentUser", "Remark", "DeptCode", 
                    
                    //21-25
                    "PaidToBankCode", "PaidToBankBranch", "PaidToBankAcNo", "PaidToBankAcName", "CurCode2",

                    //26-30
                    "ExcRate", "DocEnclosure", "OpeningDt", "EntCode", "StatusDesc",

                    //31-35
                    "BankAcTp", "BankAcTp2", "RefundDt", "ProjectDocNo1", "ProjectDocNo2", 
                    
                    //36-40
                    "ProjectDocNo3", "SiteCode", "ReqType", "BCCode", "ManualCurCode",

                    //41-45
                    "ManualExcRate", "Amt2", "SOContractDocNo", "FGItCode", "ItName",

                    //46-50
                    "AcNo", "AcDesc", "BIOPProjectInd", "BRIBillingID", "FileName",

                    //51-55
                    "FileName2", "FileName3", "DeptCode2", "CashAdvanceTypeCode", "EmpCodeCashAdv",

                    //56
                    "EmpNameCashAdv"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                     SetLueVoucherDocType(ref LueDocType, Sm.DrStr(dr, c[4]));
                     TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[5]);
                     Sm.SetLue(LueAcType, Sm.DrStr(dr, c[6]));
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[15]));
                     SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[7]));
                     Sm.SetLue(LueAcType2, Sm.DrStr(dr, c[8]));
                     SetLueBankAcCode(ref LueBankAcCode2, Sm.DrStr(dr, c[9]));
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[10]));
                     TxtGiroNo.EditValue = Sm.DrStr(dr, c[11]);
                     Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[12]));
                     Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[13]));
                     //if (mIsVRClaimUsePIC)
                     //{
                     //    SetLuePICCode2(ref LuePIC, Sm.DrStr(dr, c[14]));
                     //}
                     //else
                     //{
                     Sl.SetLueUserCode(ref LuePIC, Sm.DrStr(dr, c[14]));
                     //}
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[17]), "Y");
                     TxtPaymentUser.EditValue = Sm.DrStr(dr, c[18]);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[19]);
                     SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[20]));
                     TxtBankCode.EditValue = Sm.DrStr(dr, c[21]);
                     TxtBankBranch.EditValue = Sm.DrStr(dr, c[22]);
                     TxtBankAcNo.EditValue = Sm.DrStr(dr, c[23]);
                     TxtBankAcName.EditValue = Sm.DrStr(dr, c[24]);
                     TxtCurCode2.EditValue = Sm.DrStr(dr, c[25]);
                     TxtExcRate.EditValue = FormatNum(Sm.DrDec(dr, c[26]));
                     TxtDocEnclosure.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[27]), 0);
                     Sm.SetDte(DteOpeningDt, Sm.DrStr(dr, c[28]));
                     Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[29]));
                     TxtStatus.EditValue = Sm.DrStr(dr, c[30]);
                     TxtBankAcTp.EditValue = Sm.DrStr(dr, c[31]);
                     TxtBankAcTp2.EditValue = Sm.DrStr(dr, c[32]);
                     Sm.SetDte(DteRefundDt, Sm.DrStr(dr, c[33]));
                     if (mIsProjectSystemActive)
                     {
                         Sm.SetLue(LueProjectDocNo1, Sm.DrStr(dr, c[34]));
                         SetLueProjectSystem2(ref LueProjectDocNo2, Sm.DrStr(dr, c[34]));
                         Sm.SetLue(LueProjectDocNo2, Sm.DrStr(dr, c[35]));
                         SetLueProjectSystem3(ref LueProjectDocNo3, Sm.DrStr(dr, c[35]));
                         Sm.SetLue(LueProjectDocNo3, Sm.DrStr(dr, c[36]));
                     }
                     if (mMenuCodeForVRBudget == mMenuCode)
                     {
                         Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[37]));
                         Sm.SetLue(LueReqType, Sm.DrStr(dr, c[38]));
                         SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[39]), mIsBudgetTransactionCanUseOtherDepartmentBudget ? Sm.DrStr(dr, c[53]) : Sm.DrStr(dr, c[20]));
                         Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[39]));
                         Sm.SetLue(LueDeptCode2, Sm.DrStr(dr, c[53]));
                     }
                     Sm.SetLue(LueManualCurCode, Sm.DrStr(dr, c[40]));
                     TxtManualExcRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[41]), 0);
                     TxtAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[42]), 0);
                     if (mIsVRForBudgetUseSOContract)
                     {
                         TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[43]);
                         TxtItCode.EditValue = Sm.DrStr(dr, c[44]);
                         TxtItName.EditValue = Sm.DrStr(dr, c[45]);
                         TxtAcNo.EditValue = Sm.DrStr(dr, c[46]);
                         TxtAcDesc.EditValue = Sm.DrStr(dr, c[47]);
                     }

                     if (mIsVRUseSOContract)
                         TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[43]);
                     Sm.SetLue(LueBIOPProject, Sm.DrStr(dr, c[48]));
                     TxtBillingID.EditValue = Sm.DrStr(dr, c[49]);
                     TxtFile.EditValue = Sm.DrStr(dr, c[50]);
                     TxtFile2.EditValue = Sm.DrStr(dr, c[51]);
                     TxtFile3.EditValue = Sm.DrStr(dr, c[52]);
                     if (mIsVRManualUseOtherCurrency)
                     {
                         if (Sm.GetLue(LueDocType) == mVoucherDocTypeManual)
                             TxtAmt2.Visible = true;
                         else
                             TxtAmt2.Visible = false;
                     }

                     if (mIsVoucherRequestCASUseDueDate) Sm.SetLue(LueCashAdvanceTypeCode, Sm.DrStr(dr, c[54]));
                     if (mIsVRCashAdvanceUseEmployeePIC)
                     {
                         mEmpCode = Sm.DrStr(dr, c[55]);
                         TxtEmpName.Text = Sm.DrStr(dr, c[56]);
                     }
                 }, true
             );
        }

        private void ShowVoucherRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                "Select DNo, Description, Amt, AmtNonMain, BRIBillingID, Remark From TblVoucherRequestDtl " +
                "Where DocNo=@DocNo Order By Dno",

                new string[]
                {
                    "DNo","Description","Amt","BRIBillingID", "Remark", "AmtNonMain"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowVoucherRequestDtl2(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark ");
                SQL.AppendLine("From TblVoucherRequestDtl2 A ");
                SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.AcNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

                Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[]
                    {
                        "AcNo", "AcDesc", "DAmt", "CAmt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 3, 4 });
                Sm.FocusGrd(Grd3, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVoucherRequestDtl3(String DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm,
                   "select A.DNo, A.FileName, B.OptDesc " +
                   "from  TblVoucherRequestDtl3 A " +
                   "INNER JOIN tbloption B ON A.CategoryCode = B.OptCode AND B.OptCat = 'FileCategoryForVR' " +
                   "Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "OptDesc",
                        "FileName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 6, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        private void ShowVoucherRequestDtl4(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd5, ref cm,
                   "select DNo, FileName from  TblVoucherRequestDtl4 Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd5, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd5, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd5, dr, c, Row, 4, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            //if (mIsVRManualUseOtherCurrency)
            //{
            //    if (Sm.GetLue(LueManualCurCode).Length == 0)
            //        TxtManualExcRate.EditValue = Sm.FormatNum(1m, 0);
            //}

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsVoucherRequestTransactionValidatedbyClosingJournal && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, Sm.GetDte(DteDocDt), GetProfitCenterCode())) ||
                Sm.IsLueEmpty(LuePIC, "Person In Charge") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LueDocType, "Document Type") ||
                Sm.IsLueEmpty(LueAcType, "Debit/Credit") ||
                Sm.IsLueEmpty(LueBankAcCode, LblBankAcCode.Text) ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", false) ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                (mIsVRPaidToMandatory && Sm.IsTxtEmpty(TxtPaymentUser, "Paid To", false)) ||
                (mIsRemarkForApprovalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                (mIsVoucherRequestCASUseDueDate && Sm.GetLue(LueDocType) == "56" && Sm.IsLueEmpty(LueCashAdvanceTypeCode, "Cash Advance Type")) ||
                IsPaymentTypeNotValid() ||
                (IsBankAccountCurrencyNotValid(Sm.GetLue(LueBankAcCode), Sm.GetLue(LueCurCode))) ||
                IsAcType2NotValid() ||
                IsBankAcNo2NotValid() ||
                IsExcRateNotValid() ||
                IsSwitchingBankAccountDocTypeNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsEntCodeNotValid() ||
                IsAmtMinus() ||
                (mIsVoucherRequestUseCOA && IsBalancedNotValid()) ||
                (mIsVoucherRequestUseCOA && IsAmtAndJournalAmtDifferent()) ||
                (mIsVoucherRequestUseCOA && IsGrdValueShouldNotLess()) ||
                (mIsVoucherRequestUseCOA && IsVoucherBankAccountAcNoNotExisted()) ||
                (mMenuCodeForVRBudget == mMenuCode && IsVoucherDocTypeInvalid()) ||
                (mMenuCodeForVRBudget == mMenuCode && mIsVRForBudgetUseAvailableBudget && IsAmountNotValid()) ||
                (mMenuCodeForVRBudget == mMenuCode && mIsVRForBudgetUseSOContract && Sm.IsLueEmpty(LueBIOPProject, "BIOP/Project")) ||
                (mMenuCodeForVRBudget == mMenuCode && Sm.IsLueEmpty(LueReqType, "Request Type")) ||
                (mMenuCodeForVRBudget == mMenuCode && mIsBudgetTransactionCanUseOtherDepartmentBudget && Sm.IsLueEmpty(LueDeptCode2, "Department for Budget")) ||
                (mMenuCodeForVRBudget == mMenuCode && Sm.GetLue(LueReqType) != mReqTypeForNonBudget && Sm.IsLueEmpty(LueBCCode, "Budget Category")) ||
                (mMenuCodeForVRBudget == mMenuCode && mIsVRForBudgetUseSOContract && Sm.GetLue(LueBIOPProject) == mBIOPProjectCodeForVRBudget && Sm.IsTxtEmpty(TxtSOCDocNo, "SO Contract#", false)) ||
                (mMenuCodeForVRBudget == mMenuCode && mIsVRForBudgetUseSOContract && Sm.GetLue(LueBIOPProject) == mBIOPProjectCodeForVRBudget && Sm.IsTxtEmpty(TxtAcNo, "COA#", false)) ||
                IsManualCurCodeInvalid() ||
                IsManualExcRateInvalid() ||
                IsDocTypeInvalid() ||
                IsEmployeePICInvalid()
                ;
        }

        private bool IsEmployeePICInvalid()
        {
            if (!mIsVRCashAdvanceUseEmployeePIC) return false;

            if (Sm.GetLue(LueDocType) == "56" && mEmpCode.Length == 0)
            {
                TcVoucherRequest.SelectedTabPage = TpAdditional;
                TxtEmpName.Focus();
                Sm.StdMsg(mMsgType.Warning, "Employee Cash Adv. PIC is empty.");

                return true;
            }

            return false;
        }

        private bool IsAmountNotValid()
        {
            ComputeRemainingBudget();
            if ((Decimal.Parse(TxtRemainingBudget.Text) < 0 && Sm.GetLue(LueDocType) != "16") || (Sm.GetLue(LueCurCode) != "IDR" && Sm.GetLue(LueDocType) != "16"))
            {
                Sm.StdMsg(mMsgType.Warning, "Amount should not be greater than available budget.");
                TxtAmt.Focus();
                return true;
            }

            return false;
        }

        private bool IsDocTypeInvalid()
        {
            if (!mIsVRManualUseOtherCurrency) return false;

            if (Sm.GetLue(LueDocType) != mVoucherDocTypeManual && Sm.GetLue(LueCurCode) != mMainCurCode && Sm.GetLue(LueBankAcCode2).Length == 0)
            {
                TcVoucherRequest.SelectedTabPage = TpGeneral;
                Sm.StdMsg(mMsgType.Warning, "Type should be Manual for this document.");
                LueDocType.Focus();
                return true;
            }

            return false;
        }

        private bool IsManualExcRateInvalid()
        {
            if (!mIsVRManualUseOtherCurrency) return false;

            //if (Sm.GetLue(LueManualCurCode).Length == 0 || Sm.GetLue(LueManualCurCode) == mMainCurCode)
            //{
            //    if (TxtManualExcRate.Text.Length > 0)
            //    {
            //        if (Decimal.Parse(TxtManualExcRate.Text) != 1m)
            //        {
            //            TcVoucherRequest.SelectedTabPage = TpGeneral;
            //            Sm.StdMsg(mMsgType.Warning, "Manual Exchange Rate should be 1.");
            //            TxtManualExcRate.Focus();
            //            return true;
            //        }
            //    }
            //    else
            //    {
            //        TcVoucherRequest.SelectedTabPage = TpGeneral;
            //        Sm.StdMsg(mMsgType.Warning, "Manual Exchange Rate should be 1.");
            //        TxtManualExcRate.Focus();
            //        return true;
            //    }
            //}
            //else
            //{
            //    if (TxtManualExcRate.Text.Length > 0)
            //    {
            //        if (Decimal.Parse(TxtManualExcRate.Text) == 0m)
            //        {
            //            TcVoucherRequest.SelectedTabPage = TpGeneral;
            //            Sm.StdMsg(mMsgType.Warning, "Manual Exchange Rate should not be 0 and 1.");
            //            TxtManualExcRate.Focus();
            //            return true;
            //        }
            //    }
            //    else
            //    {
            //        TcVoucherRequest.SelectedTabPage = TpGeneral;
            //        Sm.StdMsg(mMsgType.Warning, "Manual Exchange Rate should not be 0 and 1.");
            //        TxtManualExcRate.Focus();
            //        return true;
            //    }
            //}

            return false;
        }

        private bool IsManualCurCodeInvalid()
        {
            if (!mIsVRManualUseOtherCurrency) return false;

            //if (Sm.GetLue(LueManualCurCode) == mMainCurCode)
            //{
            //    TcVoucherRequest.SelectedTabPage = TpGeneral;
            //    Sm.StdMsg(mMsgType.Warning, "Manual currency should not be the main currency code.");
            //    LueManualCurCode.Focus();
            //    return true;
            //}

            //if (Sm.GetLue(LueManualCurCode).Length > 0 && Sm.GetLue(LueCurCode) != mMainCurCode)
            //{
            //    TcVoucherRequest.SelectedTabPage = TpGeneral;
            //    Sm.StdMsg(mMsgType.Warning, "Currency should be the main currency code.");
            //    LueCurCode.Focus();
            //    return true;
            //}

            return false;
        }

        private bool IsVoucherDocTypeInvalid()
        {
            if (mVoucherDocTypeBudget.Length == 0) return false;

            bool mValid = false;
            string[] Doctypes = mVoucherDocTypeBudget.Split(',');

            foreach (var x in Doctypes)
            {
                if (Sm.GetLue(LueDocType) == x)
                {
                    mValid = true;
                    break;
                }
            }

            if (!mValid)
            {
                Sm.StdMsg(mMsgType.Warning, "Voucher type invalid.");
                return true;
            }

            return false;
        }

        private bool IsAmtAndJournalAmtDifferent()
        {
            decimal TotalDebit = 0m, Amt = 0m;

            if (TxtDbt.Text.Length != 0) TotalDebit = decimal.Parse(TxtDbt.Text);
            if (TxtAmt.Text.Length != 0) Amt = decimal.Parse(TxtAmt.Text);

            if (TotalDebit != Amt)
            {
                if (!mAllowToSaveDifferentAmtJournalVoucher)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Total Debit : " + Sm.FormatNum(TotalDebit, 0) + Environment.NewLine +
                        "Voucher's amount : " + Sm.FormatNum(Amt, 0) + Environment.NewLine + Environment.NewLine +
                        "Total debit is different than voucher's amount.");
                    TcVoucherRequest.SelectedTabPage = TpCOA;
                    return true;
                }

                else
                {
                    TcVoucherRequest.SelectedTabPage = TpCOA;
                    if (Sm.StdMsgYN(
                       "Question",
                       "Total Debit : " + Sm.FormatNum(TotalDebit, 0) + Environment.NewLine +
                       "Voucher's amount : " + Sm.FormatNum(Amt, 0) + Environment.NewLine + Environment.NewLine +
                       "Total debit is different than voucher's amount." + Environment.NewLine +
                       "Do you still want to save this data ?"
                       ) == DialogResult.No)
                        return true;
                }
            }
            return false;
        }

        private bool IsBalancedNotValid()
        {
            decimal Balanced = 0m;
            if (TxtBalanced.Text.Length != 0) Balanced = decimal.Parse(TxtBalanced.Text);
            if (Balanced != 0m)
            {
                Sm.StdMsg(mMsgType.Warning, "Debit and Credit are not balanced");
                return true;
            }
            return false;
        }

        private bool IsVoucherBankAccountAcNoNotExisted()
        {
            var SQL = new StringBuilder();
            var AcType = string.Empty;

            SQL.AppendLine("Select COAAcNo ");
            SQL.AppendLine("From TblBankAccount ");
            SQL.AppendLine("Where BankAcCode = @Param; ");

            var AcNo = Sm.GetValue(SQL.ToString(), Sm.GetLue(LueBankAcCode));

            if (AcNo.Length == 0) return false;

            var AcDesc = Sm.GetValue("Select AcDesc From TblCOA where AcNo=@Param;", AcNo);

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd3, r, 1)))
                {
                    AcType = Sm.GetLue(LueAcType);

                    if (AcType == "D" && Sm.GetGrdDec(Grd3, r, 4) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA Account# : " + AcNo + Environment.NewLine +
                            "COA Account Description : " + AcDesc + Environment.NewLine +
                            "Voucher's Account Type : Debit" + Environment.NewLine + Environment.NewLine +
                            "For This account, You should input debit amount instead of credit amount."
                            );
                        return true;
                    }
                    if (AcType == "C" && Sm.GetGrdDec(Grd3, r, 3) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA Account# : " + AcNo + Environment.NewLine +
                            "COA Account Description : " + AcDesc + Environment.NewLine +
                            "Voucher's Account Type : Credit" + Environment.NewLine + Environment.NewLine +
                            "For This account, You should input credit amount instead of debit amount."
                            );
                        return true;
                    }
                    return false;
                }
            }

            Sm.StdMsg(mMsgType.Warning,
                "Mandatory Bank Account's COA Account" + Environment.NewLine + Environment.NewLine +
                "COA Account# : " + AcNo + Environment.NewLine +
                "COA Account Description : " + AcDesc + Environment.NewLine + Environment.NewLine +
                "You should input this COA account in this journal voucher."
                );
            TcVoucherRequest.SelectedTabPage = TpCOA;
            return true;
        }

        private bool IsGrdValueShouldNotLess()
        {
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                int amt = (Sm.GetGrdInt(Grd3, Row, 3));
                int amt2 = (Sm.GetGrdInt(Grd3, Row, 4));
                if ((amt < 0) || (amt2 < 0))
                {
                    Sm.StdMsg(mMsgType.Warning,
                       "Account# : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                       "Amount should not be less than 0.");
                    TcVoucherRequest.SelectedTabPage = TpCOA;
                    return true;
                }
            }
            return false;
        }

        private bool IsEntCodeNotValid()
        {
            var EntCode = Sm.GetLue(LueEntCode);
            if (EntCode.Length > 0)
            {
                if (!Sm.IsDataExist(
                    "Select 1 From TblBankAccount " +
                    "Where BankAcCode=@Param1 " +
                    "And EntCode Is Not Null " +
                    "And EntCode=@Param2;",
                    Sm.GetLue(LueBankAcCode), EntCode, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Invalid entity." + Environment.NewLine +
                        "Voucher request's entity should be the same as bank account's entity.");
                    return true;
                }
            }
            return false;
        }

        private bool IsAmtMinus()
        {
            if (TxtAmt.Text.Length > 0 && Decimal.Parse(TxtAmt.Text) < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Amount couldn't be less than 0.");
                return true;
            }
            return false;
        }

        #region remark by VIN - disamakan dengan VC
        //private bool IsSwitchingBankAccountDocTypeNotValid()
        //{
        //    int testLueBankAcCode2 = Sm.GetLue(LueBankAcCode2).Length;
        //    string testLueDocType = Sm.GetLue(LueDocType);
        //    string testmVoucherDocTypeCASBA = mVoucherDocTypeCASBA;

        //    bool testkiri = Sm.GetLue(LueBankAcCode2).Length > 0;
        //    bool testkanan = !(Sm.CompareStr(Sm.GetLue(LueDocType), "16") || Sm.CompareStr(Sm.GetLue(LueDocType), mVoucherDocTypeCASBA));


        //        if (Sm.GetLue(LueBankAcCode2).Length > 0 && (!(Sm.CompareStr(Sm.GetLue(LueDocType), "16")|| Sm.CompareStr(Sm.GetLue(LueDocType), "74")) || Sm.CompareStr(Sm.GetLue(LueDocType), mVoucherDocTypeCASBA)))
        //        //if (Sm.GetLue(LueBankAcCode2).Length > 0 && !(Sm.CompareStr(Sm.GetLue(LueDocType), "16") || Sm.CompareStr(Sm.GetLue(LueDocType), mVoucherDocTypeCASBA)))
        //        {
        //        //Sm.StdMsg(mMsgType.Warning, "Switching bank account's o document type is not valid.");
        //        //return true;

        //        switch (Sm.GetLue(LueDocType))
        //        {
        //            case "16":
        //                Sm.StdMsg(mMsgType.Warning, "Switching bank account's document type is not valid.");
        //                return true;
        //            case "74":
        //                Sm.StdMsg(mMsgType.Warning, "Petty Cash's document type is not valid.");
        //                return true;

        //        }
        //    }

        //    if (Sm.CompareStr(Sm.GetLue(LueDocType), "16") || Sm.CompareStr(Sm.GetLue(LueDocType), mVoucherDocTypeCASBA))
        //    {
        //        if (Sm.IsLueEmpty(LueAcType2, "Debit/Credit (Switching To)") ||
        //            Sm.IsLueEmpty(LueBankAcCode2, "Debit/Credit To (Switching To)")) return true;
        //    }
        //    if (Sm.CompareStr(Sm.GetLue(LueDocType), "74"))
        //    {
        //        if (Sm.IsLueEmpty(LueAcType2, "Debit/Credit (Switching To)") ||
        //            Sm.IsLueEmpty(LueBankAcCode2, "Debit/Credit To (Switching To)")) return true;
        //    }

        //    return false;
        //}
        #endregion

        private bool IsSwitchingBankAccountDocTypeNotValid()
        {
            string DocType = LueDocType.Text;
            if (!(Sm.IsDataExist("Select 1 From TblOption Where OptCat = 'VoucherDoctype' And OptCode=@Param1 And Find_In_Set(OptCode, @Param2);", DocType, mVoucherDocTypeCodeSwitchingBankAccount, string.Empty) ||
                Sm.IsDataExist("Select 1 From TblOption Where OptCat = 'VoucherDoctype' And OptCode=@Param1 And Find_In_Set(OptCode, @Param2);", DocType, mVoucherDocTypeCASBA, string.Empty) ||
                (mCashAdvanceJournalDebitFormat == "3" && Sm.IsDataExist("Select 1 From TblOption Where OptCat = 'VoucherDoctype' And OptCode=@Param1 And Find_In_Set(OptCode, @Param2);", DocType, mVoucherDocTypeCashAdvance, string.Empty))))
            {
                if (Sm.GetLue(LueBankAcCode2).Length == 0) return false;
                if (DocType == "Petty Cash" || (DocType == "Cash Advance Settlement" && mIsVoucherCASAllowMultipleBankAccount))
                    return false;
                Sm.StdMsg(mMsgType.Warning, "Switching bank account's document type is not valid.");
                return true;
            }
            else
            {
                if (Sm.IsLueEmpty(LueAcType2, "Debit/Credit (Switching To)") ||
                    Sm.IsLueEmpty(LueBankAcCode2, "Debit/Credit To (Switching To)")) return true;
            }
            return false;
        }

        private bool IsAcType2NotValid()
        {
            var AcType2 = Sm.GetLue(LueAcType2);

            if (AcType2.Length != 0)
            {
                if (AcType2 == Sm.GetLue(LueAcType))
                {
                    Sm.StdMsg(mMsgType.Warning, "Both Debit/Credit should not be the same.");
                    return true;
                }

                if (Sm.IsLueEmpty(LueBankAcCode2, LblBankAcCode2.Text)) return true;
            }
            return false;
        }

        private bool IsBankAcNo2NotValid()
        {
            var BankAcCode2 = Sm.GetLue(LueBankAcCode2);
            if (BankAcCode2.Length != 0)
            {
                if (BankAcCode2 == Sm.GetLue(LueBankAcCode))
                {
                    Sm.StdMsg(mMsgType.Warning, "Both debit/credit to should not be the same.");
                    return true;
                }
                if (Sm.IsLueEmpty(LueAcType2, "Debit/Credit")) return true;
            }
            return false;
        }

        private bool IsBankAccountCurrencyNotValid(string BankAcCode, string CurCode)
        {
            var cm = new MySqlCommand() { CommandText = "Select CurCode From TblBankAccount Where BankAcCode=@BankAcCode;" };
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);
            var BankAccountCurCode = Sm.GetValue(cm);
            if (!Sm.CompareStr(CurCode, BankAccountCurCode))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Bank account's currency : " + CurCode + Environment.NewLine +
                    "Document's currency : " + BankAccountCurCode + Environment.NewLine + Environment.NewLine +
                    "Both currency should not be different.");
                return true;
            }
            return false;
        }

        private bool IsExcRateNotValid()
        {
            string CurCode1 = Sm.GetLue(LueCurCode), CurCode2 = TxtCurCode2.Text;
            decimal ExcRate = decimal.Parse(TxtExcRate.Text);
            if (CurCode2.Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtExcRate, "Rate", true)) return true;
                if (Sm.CompareStr(CurCode1, CurCode2) && ExcRate != 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid exchange rate.");
                    TxtExcRate.Focus();
                    return true;
                }
                if (!Sm.CompareStr(CurCode1, CurCode2) && ExcRate == 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid exchange rate.");
                    TxtExcRate.Focus();
                    return true;
                }
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            string testbank = Sm.GetLue(LuePaymentType);
            if (Sm.GetLue(LuePaymentType) == "B")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
            }
            else if (Sm.GetLue(LuePaymentType) == "G")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Bilyet#", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }
            else if (Sm.GetLue(LuePaymentType) == "K")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in description list or amount list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Description is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "Description : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine + "Amount is 0.")) return true;
            }

            if (mIsVoucherRequestUseCOA)
            {
                for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, i, 1, false, "COA's account is empty.")) { TcVoucherRequest.SelectedTabPage = TpCOA; return true; }
                }
            }

            return false;
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select 1 From TblDocApprovalSetting ");
            SQL2.AppendLine("Where UserCode Is not Null And DeptCode = @DeptCode ");
            if (mIsVRApprovalByType)
            {
                if (mMenuCodeForVRBudget != mMenuCode)
                {
                    SQL2.AppendLine("And DocType= ");
                    SQL2.AppendLine("( ");
                    SQL2.AppendLine("    Case ");
                    SQL2.AppendLine("        When @VoucherDoctype = '01' Then 'VoucherRequestManual' ");
                    SQL2.AppendLine("        When @VoucherDocType = '16' Then 'VoucherRequestSwitchingBankAcc' ");
                    SQL2.AppendLine("        When @VoucherDocType = '56' Then 'VoucherRequestCashAdvance' ");
                    SQL2.AppendLine("        When @VoucherDocType = '74' Then 'VoucherRequestPettyCash' ");
                    SQL2.AppendLine("    End");
                    SQL2.AppendLine(") ");
                }
                else
                    SQL2.AppendLine("And DocType='VoucherRequestForBudget' ");
            }
            else
                SQL2.AppendLine("And DocType='VoucherRequest' ");
            SQL2.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL2.ToString() };
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@VoucherDocType", Sm.GetLue(LueDocType));
            if (!Sm.IsDataExist(cm))
                return true;
            else
                return false;
        }

        private MySqlCommand SaveVoucherRequest(string DocNo, bool NoNeedApproval)
        {
            bool IsFirst = true, IsExists1 = false, IsExists2 = false;
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("/* Voucher Request */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, LocalDocNo,  DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, GiroNo, ");
            SQL.AppendLine("BankCode, OpeningDt, DueDt, PIC, CurCode, Amt, CurCode2, ExcRate, PaymentUser, ");
            SQL.AppendLine("PaidToBankCode, PaidToBankBranch, PaidToBankAcNo, PaidToBankAcName, DocEnclosure, EntCode, RefundDt, Remark, ");
            if (mIsVRForBudgetUseSOContract || mIsVRUseSOContract)
                SQL.AppendLine("SOContractDocNo, FGItCode, AcNo, ");

            if (mIsVoucherRequestCASUseDueDate) SQL.AppendLine("CashAdvanceTypeCode, ");

            if (mIsVRCashAdvanceUseEmployeePIC) SQL.AppendLine("EmpCodeCashAdv, ");

            SQL.AppendLine("ProjectDocNo1, ProjectDocNo2, ProjectDocNo3, SiteCode, ReqType, BCCode, Amt2, BIOPProjectInd, BRIBillingID, DeptCode2 ,CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @LocalDocNo, @DocDt, 'N', 'O', @MInd, @DeptCode, @DocType, @VoucherDocNo, ");
            SQL.AppendLine("@AcType, @BankAcCode, @AcType2, @BankAcCode2, @PaymentType, @GiroNo, ");
            SQL.AppendLine("@BankCode, @OpeningDt, @DueDt, @PIC, @CurCode, @Amt, @CurCode2, @ExcRate, @PaymentUser,");
            SQL.AppendLine("@PaidToBankCode, @PaidToBankBranch, @PaidToBankAcNo, @PaidToBankAcName, @DocEnclosure, @EntCode, @RefundDt, @Remark, ");
            if (mIsVRForBudgetUseSOContract || mIsVRUseSOContract)
                SQL.AppendLine("@SOContractDocNo, @ItCode, @AcNo, ");

            if (mIsVoucherRequestCASUseDueDate) SQL.AppendLine("@CashAdvanceTypeCode, ");

            if (mIsVRCashAdvanceUseEmployeePIC) SQL.AppendLine("@EmpCodeCashAdv, ");

            SQL.AppendLine("@ProjectDocNo1, @ProjectDocNo2, @ProjectDocNo3, @SiteCode, @ReqType, @BCCode, @Amt2, @BIOPProjectInd, @BillingID, @DeptCode2, @CreateBy, @Dt); ");

            if (!NoNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, @Dt ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType= ");
                if (mIsVRApprovalByType)
                {
                    if (mMenuCodeForVRBudget != mMenuCode)
                    {
                        SQL.AppendLine("( ");
                        SQL.AppendLine("    Case ");
                        SQL.AppendLine("        When @VoucherDoctype = '01' Then 'VoucherRequestManual' ");
                        SQL.AppendLine("        When @VoucherDocType = '16' Then 'VoucherRequestSwitchingBankAcc' ");
                        SQL.AppendLine("        When @VoucherDocType = '56' Then 'VoucherRequestCashAdvance' ");
                        SQL.AppendLine("        When @VoucherDocType = '70' Then 'VoucherRequestCASBA' ");
                        SQL.AppendLine("        When @VoucherDocType = '73' Then 'VoucherRequestSwitchingCostCenter' ");
                        SQL.AppendLine("        When @VoucherDocType = '74' Then 'VoucherRequestPettyCash' ");
                        SQL.AppendLine("    End");
                        SQL.AppendLine(") ");
                    }
                    else
                        SQL.Append("'VoucherRequestForBudget' ");

                    SQL.AppendLine("And (T.DAGCode Is Null Or ");
                    SQL.AppendLine("(T.DAGCode Is Not Null ");
                    SQL.AppendLine("And T.DAGCode In ( ");
                    SQL.AppendLine("    Select A.DAGCode ");
                    SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                    SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                    SQL.AppendLine("    And A.ActInd='Y' ");
                    SQL.AppendLine("    And B.EmpCode=(SELECT empcode FROM tblemployee WHERE usercode = @PIC) ");
                    SQL.AppendLine("))) ");
                    SQL.AppendLine("And (T.EndAmt=0 ");
                    SQL.AppendLine("Or T.EndAmt>=IfNull(( ");
                    SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
                    SQL.AppendLine("    From TblVoucherRequestHdr A ");
                    SQL.AppendLine("    Left Join ( ");
                    SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
                    SQL.AppendLine("        From TblCurrencyRate B1 ");
                    SQL.AppendLine("        Inner Join ( ");
                    SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                    SQL.AppendLine("            From TblCurrencyRate ");
                    SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
                    SQL.AppendLine("            Group By CurCode1 ");
                    SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
                    SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    SQL.AppendLine("), 0)) ");
                }
                else
                    SQL.Append("'VoucherRequest' ");
                SQL.AppendLine("And T.DeptCode=@DeptCode ");
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
                SQL.AppendLine("    From TblVoucherRequestHdr A ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
                SQL.AppendLine("        From TblCurrencyRate B1 ");
                SQL.AppendLine("        Inner Join ( ");
                SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                SQL.AppendLine("            From TblCurrencyRate ");
                SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
                SQL.AppendLine("            Group By CurCode1 ");
                SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
                SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)) ");
                if (mIsDebitVRAutoApproved)
                {
                    SQL.AppendLine("And Not Exists ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select 1 ");
                    SQL.AppendLine("    From TblVoucherRequestHdr ");
                    SQL.AppendLine("    Where DocNo = @DocNo ");
                    SQL.AppendLine("    And DocType = '01' ");
                    SQL.AppendLine("    And AcType = 'D' ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("; ");
            }

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType= ");
            if (mIsVRApprovalByType)
            {
                if (mMenuCodeForVRBudget != mMenuCode)
                {
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Case ");
                    SQL.AppendLine("        When @VoucherDoctype = '01' Then 'VoucherRequestManual' ");
                    SQL.AppendLine("        When @VoucherDocType = '16' Then 'VoucherRequestSwitchingBankAcc' ");
                    SQL.AppendLine("        When @VoucherDocType = '56' Then 'VoucherRequestCashAdvance' ");
                    SQL.AppendLine("        When @VoucherDocType = '74' Then 'VoucherRequestPettyCash' ");
                    SQL.AppendLine("    End");
                    SQL.AppendLine(") ");
                }
                else
                    SQL.Append("'VoucherRequestForBudget' ");
            }
            else
                SQL.Append("'VoucherRequest' ");

            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            if (Grd1.Rows.Count > 1)
            {
                SQL2.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, BRIBillingID, Remark, CreateBy, CreateDt) Values ");
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    {
                        IsExists1 = true;
                        if (IsFirst)
                            IsFirst = false;
                        else
                            SQL2.AppendLine(", ");
                        SQL2.AppendLine("(@DocNo, @DNo_1_" + r.ToString() + ", @Description_1_" + r.ToString() + ", @Amt_1_" + r.ToString() + ", @BillingID_1_" + r.ToString() + ", @Remark_1_" + r.ToString() + ", @CreateBy, @Dt) ");

                        Sm.CmParam<String>(ref cm, "@DNo_1_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                        Sm.CmParam<String>(ref cm, "@Description_1_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                        Sm.CmParam<Decimal>(ref cm, "@Amt_1_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 2));
                        Sm.CmParam<String>(ref cm, "@BillingID_1_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                        Sm.CmParam<String>(ref cm, "@Remark_1_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    }
                }
                SQL2.AppendLine("; ");
            }

            if (Grd3.Rows.Count > 1)
            {
                IsFirst = true;
                SQL3.AppendLine("Insert Into TblVoucherRequestDtl2(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) Values ");
                for (int r = 0; r < Grd3.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                    {
                        IsExists2 = true;
                        if (IsFirst)
                            IsFirst = false;
                        else
                            SQL3.AppendLine(", ");
                        SQL3.AppendLine("(@DocNo, @DNo_2_" + r.ToString() + ", @AcNo_2_" + r.ToString() + ", @DAmt_2_" + r.ToString() + ", @CAmt_2_" + r.ToString() + ", @Remark_2_" + r.ToString() + ", @CreateBy, @Dt) ");
                        Sm.CmParam<String>(ref cm, "@DNo_2_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                        Sm.CmParam<String>(ref cm, "@AcNo_2_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                        Sm.CmParam<Decimal>(ref cm, "@DAmt_2_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 3));
                        Sm.CmParam<Decimal>(ref cm, "@CAmt_2_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 4));
                        Sm.CmParam<String>(ref cm, "@Remark_2_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 5));
                    }
                }
                SQL3.AppendLine("; ");
            }

            cm.CommandText = SQL.ToString() + (IsExists1 ? SQL2.ToString() : string.Empty) + (IsExists2 ? SQL3.ToString() : string.Empty);

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", TxtVoucherDocNo.Text);
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@AcType2", Sm.GetLue(LueAcType2));
            Sm.CmParam<String>(ref cm, "@BankAcCode2", Sm.GetLue(LueBankAcCode2));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParamDt(ref cm, "@OpeningDt", Sm.GetDte(DteOpeningDt));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CurCode2", TxtCurCode2.Text);
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtExcRate.Text));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankCode", TxtBankCode.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtBankAcName.Text);
            Sm.CmParam<Decimal>(ref cm, "@DocEnclosure", decimal.Parse(TxtDocEnclosure.Text));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParamDt(ref cm, "@RefundDt", Sm.GetDte(DteRefundDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ProjectDocNo1", mIsProjectSystemActive && Sm.GetLue(LueDocType) == "01" ? Sm.GetLue(LueProjectDocNo1) : "");
            Sm.CmParam<String>(ref cm, "@ProjectDocNo2", mIsProjectSystemActive && Sm.GetLue(LueDocType) == "01" ? Sm.GetLue(LueProjectDocNo2) : "");
            Sm.CmParam<String>(ref cm, "@ProjectDocNo3", mIsProjectSystemActive && Sm.GetLue(LueDocType) == "01" ? Sm.GetLue(LueProjectDocNo3) : "");
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
            Sm.CmParam<String>(ref cm, "@DeptCode2", Sm.GetLue(LueDeptCode2));
            if (mMenuCodeForVRBudget == mMenuCode)
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);
            else
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@BIOPProjectInd", Sm.GetLue(LueBIOPProject).Length < 0 ? "1" : Sm.GetLue(LueBIOPProject));
            Sm.CmParam<String>(ref cm, "@VoucherDocType", Sm.GetLue(LueDocType));
            Sm.CmParam<String>(ref cm, "@BillingID", TxtBillingID.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            if (mIsVoucherRequestCASUseDueDate) Sm.CmParam<String>(ref cm, "@CashAdvanceTypeCode", Sm.GetLue(LueCashAdvanceTypeCode));
            if (mIsVRCashAdvanceUseEmployeePIC) Sm.CmParam<String>(ref cm, "@EmpCodeCashAdv", mEmpCode);

            return cm;
        }

        //private MySqlCommand SaveVoucherRequestHdr(string DocNo, bool NoNeedApproval)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/* Voucher Request */ ");

        //    SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
        //    SQL.AppendLine("(DocNo, LocalDocNo,  DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
        //    SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, GiroNo, ");
        //    SQL.AppendLine("BankCode, OpeningDt, DueDt, PIC, CurCode, Amt, CurCode2, ExcRate, PaymentUser, ");
        //    SQL.AppendLine("PaidToBankCode, PaidToBankBranch, PaidToBankAcNo, PaidToBankAcName, DocEnclosure, EntCode, RefundDt, Remark, ");
        //    if (mIsVRForBudgetUseSOContract || mIsVRUseSOContract)
        //        SQL.AppendLine("SOContractDocNo, FGItCode, AcNo, ");
        //    SQL.AppendLine("ProjectDocNo1, ProjectDocNo2, ProjectDocNo3, SiteCode, ReqType, BCCode, Amt2, BIOPProjectInd, BRIBillingID, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values ");
        //    SQL.AppendLine("(@DocNo, @LocalDocNo, @DocDt, 'N', 'O', @MInd, @DeptCode, @DocType, @VoucherDocNo, ");
        //    SQL.AppendLine("@AcType, @BankAcCode, @AcType2, @BankAcCode2, @PaymentType, @GiroNo, ");
        //    SQL.AppendLine("@BankCode, @OpeningDt, @DueDt, @PIC, @CurCode, @Amt, @CurCode2, @ExcRate, @PaymentUser,");
        //    SQL.AppendLine("@PaidToBankCode, @PaidToBankBranch, @PaidToBankAcNo, @PaidToBankAcName, @DocEnclosure, @EntCode, @RefundDt, @Remark, ");
        //    if (mIsVRForBudgetUseSOContract || mIsVRUseSOContract)
        //        SQL.AppendLine("@SOContractDocNo, @ItCode, @AcNo, ");
        //    SQL.AppendLine("@ProjectDocNo1, @ProjectDocNo2, @ProjectDocNo3, @SiteCode, @ReqType, @BCCode, @Amt2, @BIOPProjectInd, @BillingID, @CreateBy, CurrentDateTime()); ");

        //    if (!NoNeedApproval)
        //    {
        //        SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
        //        SQL.AppendLine("From TblDocApprovalSetting T ");
        //        SQL.AppendLine("Where T.DocType= ");
        //        if (mIsVRApprovalByType)
        //        {
        //            if (mMenuCodeForVRBudget != mMenuCode)
        //            {
        //                SQL.AppendLine("( ");
        //                SQL.AppendLine("    Case ");
        //                SQL.AppendLine("        When @VoucherDoctype = '01' Then 'VoucherRequestManual' ");
        //                SQL.AppendLine("        When @VoucherDocType = '16' Then 'VoucherRequestSwitchingBankAcc' ");
        //                SQL.AppendLine("        When @VoucherDocType = '56' Then 'VoucherRequestCashAdvance' ");
        //                SQL.AppendLine("    End");
        //                SQL.AppendLine(") ");
        //            }
        //            else
        //                SQL.Append("'VoucherRequestForBudget' ");
        //        }
        //        else
        //            SQL.Append("'VoucherRequest' ");

        //        SQL.AppendLine("And T.DeptCode=@DeptCode ");
        //        SQL.AppendLine("And (T.StartAmt=0 ");
        //        SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
        //        SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
        //        SQL.AppendLine("    From TblVoucherRequestHdr A ");
        //        SQL.AppendLine("    Left Join ( ");
        //        SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
        //        SQL.AppendLine("        From TblCurrencyRate B1 ");
        //        SQL.AppendLine("        Inner Join ( ");
        //        SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
        //        SQL.AppendLine("            From TblCurrencyRate ");
        //        SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
        //        SQL.AppendLine("            Group By CurCode1 ");
        //        SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
        //        SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
        //        SQL.AppendLine("    Where A.DocNo=@DocNo ");
        //        SQL.AppendLine("), 0)) ");
        //        if (mIsDebitVRAutoApproved)
        //        {
        //            SQL.AppendLine("And Not Exists ");
        //            SQL.AppendLine("( ");
        //            SQL.AppendLine("    Select * ");
        //            SQL.AppendLine("    From TblVoucherRequestHdr ");
        //            SQL.AppendLine("    Where DocNo = @DocNo ");
        //            SQL.AppendLine("    And DocType = '01' ");
        //            SQL.AppendLine("    And AcType = 'D' ");
        //            SQL.AppendLine(") ");
        //        }
        //        SQL.AppendLine("; ");
        //    }

        //    SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
        //    SQL.AppendLine("Where DocNo=@DocNo ");
        //    SQL.AppendLine("And Not Exists( ");
        //    SQL.AppendLine("    Select DocNo From TblDocApproval ");
        //    SQL.AppendLine("    Where DocType= ");
        //    if (mIsVRApprovalByType)
        //    {
        //        if (mMenuCodeForVRBudget != mMenuCode)
        //        {
        //            SQL.AppendLine("( ");
        //            SQL.AppendLine("    Case ");
        //            SQL.AppendLine("        When @VoucherDoctype = '01' Then 'VoucherRequestManual' ");
        //            SQL.AppendLine("        When @VoucherDocType = '16' Then 'VoucherRequestSwitchingBankAcc' ");
        //            SQL.AppendLine("        When @VoucherDocType = '56' Then 'VoucherRequestCashAdvance' ");
        //            SQL.AppendLine("    End");
        //            SQL.AppendLine(") ");
        //        }
        //        else
        //            SQL.Append("'VoucherRequestForBudget' ");
        //    }
        //    else
        //        SQL.Append("'VoucherRequest' ");

        //    SQL.AppendLine("    And DocNo=@DocNo ");
        //    SQL.AppendLine("    ); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
        //    Sm.CmParam<String>(ref cm, "@MInd", mMInd);
        //    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
        //    Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
        //    Sm.CmParam<String>(ref cm, "@VoucherDocNo", TxtVoucherDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
        //    Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
        //    Sm.CmParam<String>(ref cm, "@AcType2", Sm.GetLue(LueAcType2));
        //    Sm.CmParam<String>(ref cm, "@BankAcCode2", Sm.GetLue(LueBankAcCode2));
        //    Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
        //    Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
        //    Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
        //    Sm.CmParamDt(ref cm, "@OpeningDt", Sm.GetDte(DteOpeningDt));
        //    Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
        //    Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
        //    Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
        //    Sm.CmParam<String>(ref cm, "@CurCode2", TxtCurCode2.Text);
        //    Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtExcRate.Text));
        //    Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
        //    Sm.CmParam<String>(ref cm, "@PaidToBankCode", TxtBankCode.Text);
        //    Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtBankBranch.Text);
        //    Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtBankAcNo.Text);
        //    Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtBankAcName.Text);
        //    Sm.CmParam<Decimal>(ref cm, "@DocEnclosure", decimal.Parse(TxtDocEnclosure.Text));
        //    Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
        //    Sm.CmParamDt(ref cm, "@RefundDt", Sm.GetDte(DteRefundDt));
        //    Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
        //    Sm.CmParam<String>(ref cm, "@ProjectDocNo1", mIsProjectSystemActive && Sm.GetLue(LueDocType) == "01" ? Sm.GetLue(LueProjectDocNo1) : "");
        //    Sm.CmParam<String>(ref cm, "@ProjectDocNo2", mIsProjectSystemActive && Sm.GetLue(LueDocType) == "01" ? Sm.GetLue(LueProjectDocNo2) : "");
        //    Sm.CmParam<String>(ref cm, "@ProjectDocNo3", mIsProjectSystemActive && Sm.GetLue(LueDocType) == "01" ? Sm.GetLue(LueProjectDocNo3) : "");
        //    Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
        //    Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt2", decimal.Parse(TxtAmt2.Text));
        //    Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
        //    if (mMenuCodeForVRBudget == mMenuCode)
        //        Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);
        //    else
        //        Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
        //    Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
        //    Sm.CmParam<String>(ref cm, "@BIOPProjectInd", Sm.GetLue(LueBIOPProject).Length  < 0 ? "1" : Sm.GetLue(LueBIOPProject));
        //    Sm.CmParam<String>(ref cm, "@VoucherDocType", Sm.GetLue(LueDocType));
        //    Sm.CmParam<String>(ref cm, "@BillingID", TxtBillingID.Text);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveVoucherRequestDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, BRIBillingID, Remark, CreateBy, CreateDt) " +
        //            "Values (@DocNo, @DNo, @Description, @Amt, @BillingID, @Remark, @CreateBy, CurrentDateTime()) "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@BillingID", Sm.GetGrdStr(Grd1, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveVoucherRequestDtl2(string DocNo, int r)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblVoucherRequestDtl2(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @UserCode, CurrentDateTime());"
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (r + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, r, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, r, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, r, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, r, 5));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveVoucherRequestDtl3(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestDtl3(DocNo, DNo, CategoryCode, FileName, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @CategoryCode, @FileName, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("    CategoryCode=@CategoryCode, ");
            SQL.AppendLine("    FileName=@FileName, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CategoryCode", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl4(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestDtl4(DocNo, DNo, FileName, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @FileName, @UserCode, CurrentDateTime())");
            
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("    FileName=@FileName, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");



            var cm = new MySqlCommand() {

                CommandText = SQL.ToString()
            };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd5, Row, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm ;
        }


        private MySqlCommand SaveVoucherRequestDtl4(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            bool IsFirstOrExisted = true;

            //SQL.AppendLine("/* PropertyDtl  ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd5.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd5, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblVoucherRequestDtl4(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd5, r, 1));
                }

            }
            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }



            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }
        private MySqlCommand UpdateVoucherRequestDtl4(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Update tblvoucherrequestdtl4 Set ");
            //SQL.AppendLine("    DNo=@FileName, ");
            //SQL.AppendLine("    FileName=@FileName, ");
            //SQL.AppendLine("Where DocNo=@DocNo  ; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private MySqlCommand DeleteVoucherRequestFile()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblVoucherRequestDtl3 Where DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }


        #endregion

        #region Additional Method

        internal void ChooseEmployeePIC(string EmpCode, string EmpName)
        {
            mEmpCode = EmpCode;
            TxtEmpName.EditValue = EmpName;
        }

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('IsVoucherRequestCASUseDueDate', 'Apakah Voucher Request menggunakan due date untuk tipe Cash Advance ? [Y = Ya, N = Tidak]', 'N', 'KBN', NULL, 'Y', 'WEDHA', '202302221600', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('IsVRCashAdvanceUseEmployeePIC', 'Apakah Voucher Request menggunakan employee untuk tipe Cash Advance ? [Y = Ya, N = Tidak]', 'N', 'PHT', NULL, 'Y', 'WEDHA', '202303301600', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `Property1`, `Property2`, `Property3`, `Property4`, `Property5`, `Property6`, `Property7`, `Property8`, `Property9`, `Property10`, `Remark`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('VoucherRequestCASDueDate', '1', 'Operasional', 'KBN', '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WEDHA', '202302221600', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `Property1`, `Property2`, `Property3`, `Property4`, `Property5`, `Property6`, `Property7`, `Property8`, `Property9`, `Property10`, `Remark`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('VoucherRequestCASDueDate', '2', 'Non-Operasional', 'KBN', '7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'WEDHA', '202302221600', NULL, NULL); ");

            SQL.AppendLine("ALTER TABLE `tblvoucherrequesthdr` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `CashAdvanceTypeCode` VARCHAR(3) NULL DEFAULT NULL AFTER `Remark`, ");
            SQL.AppendLine("    ADD INDEX IF NOT EXISTS `CashAdvanceTypeCode` (`CashAdvanceTypeCode`); ");

            SQL.AppendLine("ALTER TABLE `tblvoucherrequesthdr` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `EmpCodeCashAdv` VARCHAR(50) NULL DEFAULT NULL AFTER `PIC`, ");
            SQL.AppendLine("    ADD INDEX IF NOT EXISTS `EmpCodeCashAdv` (`EmpCodeCashAdv`); ");

            Sm.ExecQuery(SQL.ToString());
        }

        private void ExportCSVBRI(ref List<Bank> l)
        {
            try
            {
                // format : DocNo_CurrentDtTime() -> '/' replace '_'
                var FileName = TxtDocNo.Text.Replace('/', '_')+ "_" + Sm.ServerCurrentDateTime() + ".csv";
                if (l.Count > 0)
                {
                    CreateCSVFileBRI(ref l, FileName);

                    if (IsBankBRIDataNotValid() || Sm.StdMsgYN("Question", "Bank BRI" + Environment.NewLine + "Do you want to send the exported data ?") == DialogResult.No) return;

                    SendDataBRI(mPathToSaveExportedBRIVR, FileName);

                    UpdateCSVInd(TxtDocNo.Text);
                   
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ExportCSVBRI2(ref List<Bank2> l)
        {
            try
            {
                // format : DocNo_CurrentDtTime() -> '/' replace '_'
                //var FileName = TxtDocNo.Text.Replace('/', '_') + "_" + Sm.ServerCurrentDateTime() + ".csv";
                var FileName = MeeRemark.Text + "_" + TxtDocNo.Text.Replace('/', '_') + ".csv";
                if (l.Count > 0)
                {
                    CreateCSVFileBRI2(ref l, FileName);

                    if (IsBankBRIDataNotValid() || Sm.StdMsgYN("Question", "Bank BRI" + Environment.NewLine + "Do you want to send the exported data ?") == DialogResult.No) return;

                    SendDataBRI(mPathToSaveExportedBRIVR, FileName);

                    UpdateCSVInd(TxtDocNo.Text);

                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void CreateCSVFileBRI(ref List<Bank> l, string FileName)
        {
            StreamWriter sw = new StreamWriter(mPathToSaveExportedBRIVR + FileName, false, Encoding.GetEncoding(1252));

            foreach (var x in l)
            {
                sw.WriteLine(
                     x.No + "|" + x.DocNo + "|" + x.InstructionCode + "|" + x.FxCode + "|" + x.DocDt + "|" + x.CurCode + "|" +
                     x.Amt + "|" + x.TrxRemark + "|" + x.RateVoucherCode + "|" + x.ChargeType + "|" + x.DebitAccount + "|" +
                     x.SenderName + "|" + x.SenderAddress + "|" + x.SenderCountryCode + "|" + x.CreditAccount + "|" +
                     x.BeneficiaryName + "|" + x.BeneficiaryAddress + "|" + x.BeneficiaryCountryCode + "|" + x.BenBankIdentifier + "|" +
                     x.BenBankName + "|" + x.BenBankAddress + "|" + x.BenBankCountryCode + "|" + x.InterBankIdentifier + "|" +
                     x.InterBankName + "|" + x.InterBankAddress + "|" + x.InterBankCountryCode + "|" + x.Notification + "|" +
                     x.BeneficiaryCategory + "|" + x.BeneficiaryRelation + "|" + x.BITransactionCode + "|" + x.TemplateCode 
                );
            }
            sw.Close();
            Sm.StdMsg(mMsgType.Info, "Successfully export file to" + Environment.NewLine + mPathToSaveExportedBRIVR + FileName);
            l.Clear();
        }

        private void CreateCSVFileBRI2(ref List<Bank2> l, string FileName)
        {
            StreamWriter sw = new StreamWriter(mPathToSaveExportedBRIVR + FileName, false, Encoding.GetEncoding(1252));
            sw.WriteLine("ProcCode,ID Billing,Nomor Rekening Debet,Referensi Unik,Value Date,Email");
            foreach (var x in l)
            {
                sw.WriteLine(
                     x.ProcCode + "|" + x.BillingID + "|" + x.DebitAccount + "|" + x.Reference + "|" + x.Date + "|" + x.Email
                );
            }
            sw.WriteLine("COUNT,"+l.Count);
            sw.Close();
            Sm.StdMsg(mMsgType.Info, "Successfully export file to" + Environment.NewLine + mPathToSaveExportedBRIVR + FileName);
            l.Clear();
        }

        private void SendDataBRI(string sourceDrive, string FileName)
        {
            if (mProtocolForBRIVR.ToUpper() == "FTP")
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", sourceDrive + FileName));
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForBRIVR, mPortForBRIVR, ((mIsVRUseBillingID) ? mSharedFolderForBRIVRMPN : mSharedFolderForBRIVR), toUpload.Name));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(mUserNameForBRIVR, mPasswordForBRIVR);
                request.KeepAlive = false;
                request.EnableSsl = true;
              

                Stream ftpStream = request.GetRequestStream();

                FileStream file = File.OpenRead(string.Format(@"{0}", sourceDrive + FileName));

                int length = 1024;
                byte[] buffer = new byte[length];
                int bytesRead = 0;

                do
                {
                    bytesRead = file.Read(buffer, 0, length);
                    ftpStream.Write(buffer, 0, bytesRead);
                }
                while (bytesRead != 0);

                file.Close();
                ftpStream.Close();
            }
            else if (mProtocolForBRIVR.ToUpper() == "SFTP")
            {

                using (SftpClient client = new SftpClient(mHostAddrForBRIVR, Int32.Parse(mPortForBRIVR), mUserNameForBRIVR, mPasswordForBRIVR))
                {
                    string destinationPath = string.Format(@"{0}{1}{0}", "/", ((mIsVRUseBillingID) ? mSharedFolderForBRIVRMPN : mSharedFolderForBRIVR));
                    client.Connect();
                    client.ChangeDirectory(destinationPath);
                    using (FileStream fs = new FileStream(string.Format(@"{0}", sourceDrive + FileName), FileMode.Open))
                    {
                        client.BufferSize = 4 * 1024;
                        client.UploadFile(fs, Path.GetFileName(string.Format(@"{0}", sourceDrive + FileName)), null);
                    }
                    client.Dispose();
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "Unknown protocol.");
                return;
            }

            Sm.StdMsg(mMsgType.Info, "File uploaded to BRI Server.");
        }

        private bool IsBankBRIDataNotValid()
        {
            if (mProtocolForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Protocol (BRI) is empty.");
                return true;
            }

            if (mHostAddrForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address (BRI) is empty.");
                return true;
            }

            if (mIsVRUseBillingID && mSharedFolderForBRIVRMPN.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder (BRI) is empty.");
                return true;
            }

            if (!mIsVRUseBillingID && mSharedFolderForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder (BRI) is empty.");
                return true;
            }

            if (mUserNameForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username (BRI) is empty.");
                return true;
            }

            if (mPortForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number (BRI) is empty.");
                return true;
            }
          

            return false;
        }

        private void UpdateCSVInd(string DocNo)
        {
            var cml = new List<MySqlCommand>();

            cml.Add(UpdateCSVInd2(DocNo));

            Sm.ExecCommands(cml);

            BtnCSV.Enabled = false;
            BtnCSV.Visible = false;
        }

        private MySqlCommand UpdateCSVInd2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestHdr ");
            SQL.AppendLine("    Set CSVInd = 'Y', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private void SetLueBCCode(ref LookUpEdit Lue, string BCCode, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode As Col1, BCName As Col2 From TblBudgetCategory ");
            if (BCCode.Length > 0)
                SQL.AppendLine("Where BCCode=@BCCode ");
            else
                SQL.AppendLine("Where DeptCode=@DeptCode And ActInd='Y' ");

            if (mIsVRBudgetUseCASBA && Sm.CompareStr(Sm.GetLue(LueDocType), mVoucherDocTypeCASBA))
            {
                SQL.AppendLine(" UNION ALL ");
                SQL.AppendLine(" SELECT 'ALL' AS coll, 'ALL'  AS Col2 ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (BCCode.Length > 0) Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
            if (DeptCode.Length > 0) Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BCCode.Length > 0) Sm.SetLue(Lue, BCCode);
        }

        private void ComputeAmt2()
        {
            if (mIsVRManualUseOtherCurrency)
            {
                if (Sm.GetLue(LueDocType) == mVoucherDocTypeManual &&
                    Sm.GetLue(LueCurCode) != mMainCurCode &&
                    Sm.GetLue(LueBankAcCode2).Length == 0)
                {
                    decimal mExcRate = 0m, mAmt = 0m;
                    if (TxtExcRate.Text.Length > 0) mExcRate = Decimal.Parse(TxtExcRate.Text);

                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        mAmt += Sm.GetGrdDec(Grd1, i, 2) * mExcRate;
                    }

                    TxtAmt2.EditValue = Sm.FormatNum(mAmt, 0);
                }
            }
        }

        private decimal ComputeAvailableBudget()
        {
            string AvailableBudget = "0";

            if (Sm.GetLue(LueDeptCode).Length != 0 && Sm.GetDte(DteDocDt).Length != 0 && (Sm.CompareStr(Sm.GetLue(LueReqType), "1")))
            {
                var SQL = new StringBuilder();

                if (mIsMRBudgetBasedOnBudgetCategory)
                {
                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select  ");
                    if (!mIsBudget2YearlyFormat && !Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL"))
                         SQL.AppendLine("        Amt2 ");
                    else
                         SQL.AppendLine("        SUM(Amt2) ");
                    SQL.AppendLine("        From TblBudgetSummary A ");
                    SQL.AppendLine("        Where A.DeptCode=@DeptCode ");
                    if (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL"))
                        SQL.AppendLine("        AND A.BCCode = @BCCode ");
                    SQL.AppendLine("        And A.Yr=Left(@DocDt, 4) ");
                    if(!mIsBudget2YearlyFormat)
                         SQL.AppendLine("        And A.Mth=Substring(@DocDt, 5, 2) ");
                    SQL.AppendLine("    ), 0.00)- ");
                }
                else
                {

                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Amt From TblBudget ");
                    SQL.AppendLine("        Where DeptCode=@DeptCode ");
                    SQL.AppendLine("        And Yr=Left(@DocDt, 4) ");
                    SQL.AppendLine("        And Mth=Substring(@DocDt, 5, 2) ");
                    //SQL.AppendLine("        And UserCode Is Not Null ");
                    SQL.AppendLine("    ), 0.00)- ");

                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(Amt) ");
                SQL.AppendLine("        From TblVoucherRequestHdr ");
                SQL.AppendLine("        Where DocNo <> @DocNo ");
                SQL.AppendLine("        And Find_In_Set(DocType, ");
                SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
                SQL.AppendLine("        And CancelInd = 'N' ");
                SQL.AppendLine("        And Status In ('O', 'A') ");
                SQL.AppendLine("        And ReqType Is Not Null ");
                SQL.AppendLine("        And ReqType <> @ReqTypeForNonBudget ");
                SQL.AppendLine("        And DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                     SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                if (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL"))
                    SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ), 0.00) - ");

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) ");
                SQL.AppendLine("        From TblTravelRequestHdr A ");
                SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("        Where A.DocNo <> @DocNo ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.Status In ('O', 'A') ");
                SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                if (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL"))
                    SQL.AppendLine("        And IfNull(B.BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ), 0.00) + ");

                SQL.AppendLine("      IfNull(( ");
                SQL.AppendLine("      Select SUM(A.Amt) from( ");
                SQL.AppendLine("      Select Case when A.AcType = 'D' Then IFNULL(A.Amt, 0.00) ELSE IFNULL((A.Amt)*-1, 0.00) END As Amt ");
                SQL.AppendLine("        From tblvoucherhdr A ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("	    (");
                SQL.AppendLine("			SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
                SQL.AppendLine("		  	From tblvoucherhdr X1 ");
                SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
                SQL.AppendLine("			AND X1.DocType = '58' ");
                SQL.AppendLine("            AND X1.CancelInd = 'N' ");
                SQL.AppendLine("            AND X2.Status In ('O', 'A') ");
                SQL.AppendLine("		  ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("       Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("       Inner Join ");
                SQL.AppendLine("		  ( ");
                SQL.AppendLine("		  	SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("		  	From tblvoucherhdr X1  ");
                SQL.AppendLine("		  	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("		  	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("			AND X1.DocType = '56' ");
                SQL.AppendLine("		    AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("            AND X3.Status In ('O', 'A')  ");
                SQL.AppendLine("		  ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("       Where A.DocNo <> @DocNo  ");
                SQL.AppendLine("       And D.DeptCode = @DeptCode  ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                if (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL"))
                    SQL.AppendLine("       And IfNull(D.BCCode, '') = IfNull(@BCCode, '')  ");
                SQL.AppendLine(" )A  ");
                SQL.AppendLine("  ), 0.00) - ");

                if (mMRAvailableBudgetSubtraction == "1")
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(B.Qty*B.UPrice) ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                    SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory && (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL")))
                    {
                        SQL.AppendLine("        And A.BCCode=@BCCode ");
                    }
                    SQL.AppendLine("        And A.ReqType='1' ");
                    SQL.AppendLine("        And B.CancelInd='N' ");
                    SQL.AppendLine("        And B.Status<>'C' ");
                    if (!mIsBudget2YearlyFormat)
                         SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                         SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("       And A.DocNo<>@DocNo ");
                    SQL.AppendLine("    ),0.00)+");
                }

                if (mMRAvailableBudgetSubtraction == "2")
                {
                    SQL.AppendLine("    IfNull(( ");

                    SQL.AppendLine("    Select Sum( ");
                    SQL.AppendLine("    X2.Amt -  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("      IfNull( X1.DiscountAmt *  ");
                    SQL.AppendLine("          Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("          IfNull(( ");
                    SQL.AppendLine("              Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("              Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End ");
                    SQL.AppendLine("      , 0.00) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    From TblPOHdr X1 ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select A.DeptCode, E.DocNo, Sum(  ");
                    SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
                    SQL.AppendLine("        * Case When F.CurCode=@MainCurCode Then 1.00 Else  ");
                    SQL.AppendLine("        IfNull((  ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End  ");
                    SQL.AppendLine("        ) As Amt  ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
                    SQL.AppendLine("            On A.DocNo=B.DocNo ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("            And A.DocNo <> @DocNo ");
                    SQL.AppendLine("            And B.CancelInd='N'  ");
                    SQL.AppendLine("            And B.Status In ('A', 'O')  ");
                    //SQL.AppendLine("            And A.Status='A'  ");
                    SQL.AppendLine("            And A.Reqtype='1'   ");

                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory && (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL")))
                        SQL.AppendLine("        And A.BCCode=@BCCode ");

                    SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
                    SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
                    SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
                    SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
                    SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("    Group By X2.DeptCode ");

                    SQL.AppendLine("    ),0.00)+");
                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(A.Qty*D.UPrice) ");
                SQL.AppendLine("        From TblPOQtyCancel A ");
                SQL.AppendLine("        Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestHdr E ");
                SQL.AppendLine("            On D.DocNo=E.DocNo  ");
                if (mBudgetBasedOn == "1")
                    SQL.AppendLine("        And E.DeptCode=@DeptCode ");
                if (mBudgetBasedOn == "2")
                    SQL.AppendLine("        And E.SiteCode=@DeptCode ");
                if (mIsMRBudgetBasedOnBudgetCategory && (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL")))
                    SQL.AppendLine("        And E.BCCode=@BCCode ");

                SQL.AppendLine("            And E.ReqType='1' ");
                if(!mIsBudget2YearlyFormat)
                    SQL.AppendLine("            And Left(E.DocDt, 6)=Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("            And Left(E.DocDt, 4)=Left(@DocDt, 4) ");
                SQL.AppendLine("            And E.DocNo<>@DocNo ");
                SQL.AppendLine("        Where A.CancelInd='N' ");
                SQL.AppendLine("    ),0.00) ");

                if (mIsCASUsedForBudget)
                {
                    SQL.AppendLine(" - IfNull(( ");
                    SQL.AppendLine("    Select Sum(T.Amt) Amt From ( ");
                    SQL.AppendLine("        Select Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) As Mth, C.BCCode, C.DeptCode, B.Amt ");
                    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            And A.`Status` = 'A' And A.CancelInd = 'N' ");
                    SQL.AppendLine("            And A.DocStatus = 'F' ");
                    SQL.AppendLine("            And B.CCtCode Is Not Null ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                    SQL.AppendLine("            And C.CCtCode Is Not Null ");
                    SQL.AppendLine("            And C.BCCode = @BCCode ");
                    SQL.AppendLine("            And C.DeptCOde = @DeptCode ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("), 0.00) ");
                }

                SQL.AppendLine(" As RemainingBudget");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (mBudgetBasedOn == "1")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                if (mBudgetBasedOn == "2")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueSiteCode));

                Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length != 0) ? TxtDocNo.Text : "XXX");
                Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@MainCurCode", Sm.GetParameter("MainCurCode"));

                AvailableBudget = Sm.GetValue(cm);
            }

            return decimal.Parse(AvailableBudget);
        }

        public void ComputeRemainingBudget()
        {
            decimal AvailableBudget = 0m, RequestedBudget = 0m;
            if (Sm.CompareStr(Sm.GetLue(LueReqType), "1") && mIsBudgetTransactionCanUseOtherDepartmentBudget ? Sm.GetLue(LueDeptCode2).Length > 0 : Sm.GetLue(LueDeptCode).Length > 0)
            {
                AvailableBudget = Sm.ComputeAvailableBudget(
                    (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                    Sm.GetLue(LueSiteCode),  mIsBudgetTransactionCanUseOtherDepartmentBudget ? Sm.GetLue(LueDeptCode2) : Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                    );
                RequestedBudget = Decimal.Parse(TxtAmt.Text);
            }
            TxtRemainingBudget.Text = Sm.FormatNum((AvailableBudget-RequestedBudget), 0);
        }

        private void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine("And ActInd = 'Y' ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsDeptFilterBySite && Sm.GetLue(LueSiteCode).Length > 0)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblDepartmentBudgetSite ");
                SQL.AppendLine("    Where SiteCode=@SiteCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueReqType(ref DXE.LookUpEdit Lue, string ReqType)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='ReqType' ");
                if (ReqType.Length > 0)
                    SQL.AppendLine("And OptCode=@ReqType ");
                else
                {
                    if (mIsBudgetActive && mReqTypeForNonBudget.Length > 0)
                        SQL.AppendLine("And OptCode<>@ReqTypeForNonBudget ");
                }
                SQL.AppendLine("Order By Col2;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@ReqType", ReqType);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueBIOPProject(ref DXE.LookUpEdit Lue, string BIOPProject)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='BIOPProject' ");
                if (BIOPProject.Length > 0)
                    SQL.AppendLine("And OptCode=@BIOPProject ");
                SQL.AppendLine("Order By OptDesc;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@BIOPProject", BIOPProject);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetBudgetCategory()
        {
            LueBCCode.EditValue = null;
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, true);

            if (!mIsBudgetTransactionCanUseOtherDepartmentBudget)
            {
                var ReqType = Sm.GetLue(LueReqType);
                var DeptCode = Sm.GetLue(LueDeptCode);
            

                if (
                mReqTypeForNonBudget.Length == 0 ||
                ReqType.Length == 0 ||
                Sm.CompareStr(ReqType, mReqTypeForNonBudget) ||
                DeptCode.Length == 0
                ) return;

                SetLueBCCode(ref LueBCCode, string.Empty, DeptCode);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, false);
            }
            else return;
        }

        private void Compute(int Row, int ac)
        {
            if (mIsVoucherRequestUseCOA)
            {
                switch (ac)
                {
                    case 1:
                        if (Sm.GetGrdStr(Grd3, Row, 3).Length != 0)
                        {
                            Grd3.Cells[Row, 4].Value = 0; //Sm.GrdColReadOnly(Grd3, new int[] { 5 });
                        }
                        break;
                    case 2:
                        if (Sm.GetGrdStr(Grd3, Row, 4).Length != 0)
                        {
                            Grd3.Cells[Row, 3].Value = 0; //Sm.GrdColReadOnly(Grd3, new int[] { 4});
                        }
                        break;
                }
            }
        }

        private void ComputeBalanced()
        {
            if (mIsVoucherRequestUseCOA)
            {
                decimal balanced = 0; decimal debit, credit;
                debit = Convert.ToDecimal(TxtDbt.Text);
                credit = Convert.ToDecimal(TxtCdt.Text);
                balanced = debit - credit;
                TxtBalanced.EditValue = Sm.FormatNum(balanced, 0);
            }
        }

        private void ComputeDebetCredit()
        {
            if (mIsVoucherRequestUseCOA)
            {
                decimal DebetAmount = 0m; decimal CreditAmount = 0m;
                for (int Row = 0; Row <= Grd3.Rows.Count - 1; Row++)
                    DebetAmount += Sm.GetGrdDec(Grd3, Row, 3);
                for (int Row = 0; Row <= Grd3.Rows.Count - 1; Row++)
                    CreditAmount += Sm.GetGrdDec(Grd3, Row, 4);

                TxtDbt.EditValue = Sm.FormatNum(DebetAmount, 0);
                TxtCdt.EditValue = Sm.FormatNum(CreditAmount, 0);
            }
        }

        private void SetBankCode()
        {
            if (!LueBankCode.Properties.ReadOnly &&
                Sm.GetLue(LueBankAcCode).Length > 0)
            {
                try
                {
                    var BankCode = Sm.GetValue(
                        "Select BankCode From TblBankAccount Where BankAcCode=@Param;",
                        Sm.GetLue(LueBankAcCode));
                    if (BankCode.Length > 0) Sm.SetLue(LueBankCode, BankCode);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void SetBankAcTp(ref LookUpEdit Lue, ref TextEdit Txt)
        {
            Txt.EditValue = null;
            if (Sm.GetLue(Lue).Length > 0)
            {
                try
                {
                    var BankAcTp = Sm.GetValue(
                        "Select BankAcTp From TblBankAccount Where BankAcCode=@Param;",
                        Sm.GetLue(Lue));
                    if (BankAcTp.Length > 0) Txt.EditValue = BankAcTp;
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private string FormatNum(Decimal NumValue)
        {
            try
            {
                return String.Format("{0:#,###,##0.00##########}", NumValue);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        private string FormatNum(string Value)
        {
            decimal NumValue = 0m;
            try
            {
                Value = (Value.Length == 0) ? "0" : Value.Trim();
                if (!decimal.TryParse(Value, out NumValue))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid numeric value.");
                    NumValue = 0m;
                }
                if (NumValue < 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Value should not be less than 0.");
                    NumValue = 0m;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return String.Format("{0:#,###,##0.00##########}", NumValue);
        }

        private void FormatNumTxt(TextEdit Txt)
        {
            Txt.EditValue = FormatNum(Txt.Text);
        }

        private void GetParameter()
        {
            var MenuCodeForDocWithMInd = string.Empty;
            mMenuCodeForVRBudget = Sm.GetParameter("MenuCodeForVRBudget"); // tetap menggunakan function yg biasa.
            
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            if (Sm.CompareStr(mMenuCodeForVRBudget, mMenuCode))
            {
                SQL.AppendLine("'IsFilterBySite', 'IsSiteMandatory', 'MRAvailableBudgetSubtraction', 'IsMRBudgetBasedOnBudgetCategory', 'IsBudgetCalculateFromEstimatedPrice', ");
                SQL.AppendLine("'IsBudget2YearlyFormat', 'IsBudgetActive', 'IsMRShowEstimatedPrice', 'IsVRBudgetUseSwitchingCostCenter', 'IsVRBudgetUseCASBA', ");
                SQL.AppendLine("'BudgetBasedOn', 'ReqTypeForNonBudget', 'VoucherDocTypeCashAdvance', 'BIOPProjectCodeForVRBudget', ");
            }
            SQL.AppendLine("'IsVoucherRequestAllowToUploadFile', 'VoucherRequestUploadFormat', 'VoucherCodeFormatType', 'HostAddrForFTPClient', 'PortForFTPClient', ");
            SQL.AppendLine("'FormatFTPClient', 'FileSizeMaxUploadFTPClient', 'SharedFolderForFTPClient', 'PasswordForFTPClient', 'UsernameForFTPClient', ");
            SQL.AppendLine("'VoucherDocTypeManual', 'MainCurCode', 'DocNoFormat', 'FormPrintOutVR', 'VoucherDocTypeCASBA', ");
            SQL.AppendLine("'BankAccountFormat', 'MenuCodeForDocWithMInd', 'IsVRDeptCodeAutoFillBasedOnPIC', 'IsPICShowEmpCode', 'IsVRApprovalByType', ");
            SQL.AppendLine("'IsVoucherBankAccountFilteredByCurrency', 'IsDebitVRAutoApproved', 'IsVoucherRequestPrintOutUseDocDt', 'IsVRManualUseOtherCurrency', 'IsVRUseSOContract', ");
            SQL.AppendLine("'IsUseDailyCurrencyRate', 'IsVoucherRequestBtnDocTypeEnabled', 'AllowToSaveDifferentAmtJournalVoucher', 'IsVoucherRequestUseCOA', 'IsProjectSystemActive', ");
            SQL.AppendLine("'IsVRPaidToMandatory', 'IsVoucherRequestBankAccountFilteredByGrp', 'IsFilterByDept', 'IsUseMInd', 'IsPICGrouped', ");
            SQL.AppendLine("'IsVoucherRequestRemarkForApprovalMandatory', 'IsAutoGeneratePurchaseLocalDocNo', 'IsVoucherRequestFilteredByDept', 'IsVRUseMultiBillingID', 'IsVRUseBillingID', ");
            SQL.AppendLine("'IsVRForBudgetUseSOContract', 'IsCASUsedForBudget', 'IsVRSendtoBank', 'IsCSVUseRealAmt', 'VoucherDocTypePayrollProcessing', ");
            SQL.AppendLine("'SharedFolderForBRIVRMPN', 'UserNameForBRIVR', 'PasswordForBRIVR', 'PortForBRIVR', 'ProtocolForBRIVR', ");
            SQL.AppendLine("'InstructionCodeForTransferInstructionType', 'BankChargeTypeForVR', 'PathToSaveExportedBRIVR', 'HostAddrForBRIVR', 'SharedFolderForBRIVR', ");
            SQL.AppendLine("'VoucherDocTypeBudget', 'IsVRForBudgetUseAvailableBudget', 'IsVRClaimUsePIC', 'IsBudgetTransactionCanUseOtherDepartmentBudget', 'BudgetCategoryCodeForCrossDepartment', ");
            SQL.AppendLine("'IsTransactionUseDetailApprovalInformation', 'IsGroupFilterByType', 'IsVRForAllowToCopyData', 'IsPettyCashTypeEnabled', 'CashAdvanceJournalDebitFormat', 'VoucherDocTypeCashAdvance', ");
            SQL.AppendLine("'BankAccountTypeForVRCA1', 'BankAccountTypeForVRCA2', 'VoucherDocTypePettyCash', 'BankAccountTypeForVRPCD1', 'IsAutoJournalActived', 'IsVoucherRequestTransactionValidatedbyClosingJournal', 'IsVoucherRequestCASUseDueDate', ");
            SQL.AppendLine("'IsVoucherRequestUploadFileMandatory', 'IsVRCashAdvanceUseEmployeePIC', 'IsFilterBySiteHR', 'IsFilterByDeptHR', 'VoucherDocTypeCodeSwitchingBankAccount', 'IsVoucherCASAllowMultipleBankAccount' );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();

                        if (Sm.CompareStr(mMenuCodeForVRBudget, mMenuCode))
                        {
                            switch (ParCode)
                            {
                                //boolean
                                case "IsBudget2YearlyFormat": mIsBudget2YearlyFormat = ParValue == "Y"; break;
                                case "IsBudgetActive": mIsBudgetActive = ParValue == "Y"; break;
                                case "IsMRShowEstimatedPrice": mIsMRShowEstimatedPrice = ParValue == "Y"; break;
                                case "IsVRBudgetUseSwitchingCostCenter": mIsVRBudgetUseSwitchingCostCenter = ParValue == "Y"; break;
                                case "IsVRBudgetUseCASBA": mIsVRBudgetUseCASBA = ParValue == "Y"; break;
                                case "IsBudgetCalculateFromEstimatedPrice": mIsBudgetCalculateFromEstimatedPrice = ParValue == "Y"; break;
                                case "IsMRBudgetBasedOnBudgetCategory": mIsMRBudgetBasedOnBudgetCategory = ParValue == "Y"; break;
                                case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                                case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                                case "IsBudgetTransactionCanUseOtherDepartmentBudget": mIsBudgetTransactionCanUseOtherDepartmentBudget = ParValue == "Y"; break;
                                case "IsGroupFilterByType": mIsGroupFilterByType = ParValue == "Y"; break;
                                case "IsVRForAllowToCopyData": mIsVRForAllowToCopyData = ParValue == "Y"; break;
                                case "IsPettyCashTypeEnabled": mIsPettyCashTypeEnabled = ParValue == "Y"; break;
                                case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                                case "IsVoucherRequestTransactionValidatedbyClosingJournal": mIsVoucherRequestTransactionValidatedbyClosingJournal = ParValue == "Y"; break;
                                

                                //string
                                case "MRAvailableBudgetSubtraction": mMRAvailableBudgetSubtraction = ParValue; break;
                                case "BudgetBasedOn": mBudgetBasedOn = ParValue; break;
                                case "ReqTypeForNonBudget": mReqTypeForNonBudget = ParValue; break;
                                case "VoucherDocTypeCashAdvance": mVoucherDocTypeCashAdvance = ParValue; break;
                                case "BIOPProjectCodeForVRBudget": mBIOPProjectCodeForVRBudget = ParValue; break;
                                case "BudgetCategoryCodeForCrossDepartment": mBudgetCategoryCodeForCrossDepartment = ParValue; break;
                                case "VoucherDocTypeCodeSwitchingBankAccount": mVoucherDocTypeCodeSwitchingBankAccount = ParValue; break;


                            }
                        }

                        switch (ParCode)
                        {
                            //boolean
                            case "IsVRForBudgetUseAvailableBudget": mIsVRForBudgetUseAvailableBudget = ParValue == "Y"; break;
                            case "IsVRForBudgetUseSOContract": mIsVRForBudgetUseSOContract = ParValue == "Y"; break;
                            case "IsCASUsedForBudget": mIsCASUsedForBudget = ParValue == "Y"; break;
                            case "IsVRSendtoBank": mIsVRSendtoBank = ParValue == "Y"; break;
                            case "IsCSVUseRealAmt": mIsCSVUseRealAmt = ParValue == "Y"; break;
                            case "IsVRUseBillingID": mIsVRUseBillingID = ParValue == "Y"; break;
                            case "IsVRUseMultiBillingID": mIsVRUseMultiBillingID = ParValue == "Y"; break;
                            case "IsVoucherRequestFilteredByDept": mIsVoucherRequestFilteredByDept = ParValue == "Y"; break;
                            case "IsAutoGeneratePurchaseLocalDocNo": mIsAutoGeneratePurchaseLocalDocNo = ParValue == "Y"; break;
                            case "IsVoucherRequestRemarkForApprovalMandatory": mIsRemarkForApprovalMandatory = ParValue == "Y"; break;
                            case "IsProjectSystemActive": mIsProjectSystemActive = ParValue == "Y"; break;
                            case "IsVoucherRequestUseCOA": mIsVoucherRequestUseCOA = ParValue == "Y"; break;
                            case "AllowToSaveDifferentAmtJournalVoucher": mAllowToSaveDifferentAmtJournalVoucher = ParValue == "Y"; break;
                            case "IsVoucherRequestBtnDocTypeEnabled": mIsVoucherRequestBtnDocTypeEnabled = ParValue == "Y"; break;
                            case "IsUseDailyCurrencyRate": mIsUseDailyCurrencyRate = ParValue == "Y"; break;
                            case "IsPICGrouped": mIsPICGrouped = ParValue == "Y"; break;
                            case "IsUseMInd": mIsUseMInd = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsVoucherRequestBankAccountFilteredByGrp": mIsVoucherRequestBankAccountFilteredByGrp = ParValue == "Y"; break;
                            case "IsVRPaidToMandatory": mIsVRPaidToMandatory = ParValue == "Y"; break;
                            case "IsVoucherBankAccountFilteredByCurrency": mIsVoucherBankAccountFilteredByCurrency = ParValue == "Y"; break;
                            case "IsDebitVRAutoApproved": mIsDebitVRAutoApproved = ParValue == "Y"; break;
                            case "IsVoucherRequestPrintOutUseDocDt": mIsVoucherRequestPrintOutUseDocDt = ParValue == "Y"; break;
                            case "IsVRManualUseOtherCurrency": mIsVRManualUseOtherCurrency = ParValue == "Y"; break;
                            case "IsVRUseSOContract": mIsVRUseSOContract = ParValue == "Y"; break;
                            case "IsVRApprovalByType": mIsVRApprovalByType = ParValue == "Y"; break;
                            case "IsVoucherRequestAllowToUploadFile": mIsVoucherRequestAllowToUploadFile = ParValue == "Y"; break;
                            case "IsVRDeptCodeAutoFillBasedOnPIC": mIsVRDeptCodeAutoFillBasedOnPIC = ParValue == "Y"; break;
                            case "IsPICShowEmpCode": mIsPICShowEmpCode = ParValue == "Y"; break;
                            case "IsVRClaimUsePIC": mIsVRClaimUsePIC = ParValue == "Y"; break;
                            case "IsTransactionUseDetailApprovalInformation": mIsTransactionUseDetailApprovalInformation = ParValue == "Y"; break;
                            case "IsGroupFilterByType": mIsGroupFilterByType = ParValue == "Y"; break;
                            case "IsVRForAllowToCopyData": mIsVRForAllowToCopyData = ParValue == "Y"; break;
                            case "IsPettyCashTypeEnabled": mIsPettyCashTypeEnabled = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsVoucherRequestTransactionValidatedbyClosingJournal": mIsVoucherRequestTransactionValidatedbyClosingJournal = ParValue == "Y"; break;
                            case "IsVoucherRequestCASUseDueDate": mIsVoucherRequestCASUseDueDate = ParValue == "Y"; break;
                            case "IsVoucherRequestUploadFileMandatory": mIsVoucherRequestUploadFileMandatory = ParValue == "Y"; break;
                            case "IsVRCashAdvanceUseEmployeePIC": mIsVRCashAdvanceUseEmployeePIC = ParValue == "Y"; break;
                            case "IsFilterByDeptHR": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsVoucherCASAllowMultipleBankAccount": mIsVoucherCASAllowMultipleBankAccount = ParValue == "Y"; break;


                            //string
                            case "VoucherDocTypePayrollProcessing": mVoucherDocTypePayrollProcessing = ParValue; break;
                            case "VoucherRequestUploadFormat": mVoucherRequestUploadFormat = ParValue; break;
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "VoucherDocTypeCASBA": mVoucherDocTypeCASBA = ParValue; break;
                            case "FormPrintOutVR": mIsPrintOutVR = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "VoucherDocTypeManual": mVoucherDocTypeManual = ParValue; break;
                            case "BankAccountFormat": mBankAccountFormat = ParValue; break;
                            case "MenuCodeForDocWithMInd": MenuCodeForDocWithMInd = ParValue;break;
                            case "VoucherDocTypeBudget": mVoucherDocTypeBudget = ParValue; break;
                            case "SharedFolderForBRIVR": mSharedFolderForBRIVR = ParValue; break;
                            case "HostAddrForBRIVR": mHostAddrForBRIVR = ParValue; break;
                            case "PathToSaveExportedBRIVR": mPathToSaveExportedBRIVR = ParValue; break;
                            case "BankChargeTypeForVR": mBankChargeTypeForVR = ParValue; break;
                            case "InstructionCodeForTransferInstructionType": mInstructionCodeForTransferInstructionType = ParValue; break;
                            case "ProtocolForBRIVR": mProtocolForBRIVR = ParValue; break;
                            case "PortForBRIVR": mPortForBRIVR = ParValue; break;
                            case "PasswordForBRIVR": mPasswordForBRIVR = ParValue; break;
                            case "UserNameForBRIVR": mUserNameForBRIVR = ParValue; break;
                            case "SharedFolderForBRIVRMPN": mSharedFolderForBRIVRMPN = ParValue; break;
                            case "CashAdvanceJournalDebitFormat": mCashAdvanceJournalDebitFormat = ParValue; break;
                            case "VoucherDocTypeCashAdvance": mVoucherDocTypeCashAdvance = ParValue; break;
                            case "BankAccountTypeForVRCA1":mBankAccountTypeForVRCA1 = ParValue; break;
                            case "BankAccountTypeForVRCA2": mBankAccountTypeForVRCA2 = ParValue; break;
                            case "VoucherDocTypePettyCash": mVoucherDocTypePettyCash = ParValue; break;
                            case "BankAccountTypeForVRPCD1": mBankAccountTypeForVRPCD1 = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mVoucherDocTypeCASBA.Length == 0) mVoucherDocTypeCASBA = "70";
            if (mIsPrintOutVR.Length == 0) mIsPrintOutVR = "VoucherRequest";
            if (MenuCodeForDocWithMInd.Length > 0) mMInd = MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1 ? "Y" : "N";
            if (mIsSiteMandatory && mIsBudgetActive) mIsDeptFilterBySite = Sm.IsDataExist("Select 1 From TblDepartment A, TblDepartmentBudgetSite B Where A.DeptCode=B.DeptCode And A.ActInd='Y' Limit 1;");
            if (mCashAdvanceJournalDebitFormat.Length == 0) mCashAdvanceJournalDebitFormat = "1";
         }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue, string DeptCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");

            if (DeptCode.Length != 0)
                SQL.AppendLine("Where DeptCode=@DeptCode ");
            else
            {
                SQL.AppendLine("Where ActInd='Y' ");
                if (mIsFilterByDept || mIsVoucherRequestFilteredByDept)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (DeptCode.Length != 0) Sm.SetLue(Lue, DeptCode);
        }

        private void SetLuePICCode2(ref DXE.LookUpEdit Lue, string PICCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.EmpCode As Col1, ");
                SQL.AppendLine("A.EmpName As Col2 ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Where A.EmpCode=@EmpCode ");
                SQL.AppendLine("Order By A.EmpName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@EmpCode", PICCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (PICCode.Length > 0) Sm.SetLue(Lue, PICCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLuePICCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.UserCode As Col1, ");
                SQL.AppendLine("Concat(A.UserName ");
                if (mIsPICShowEmpCode)
                {
                    SQL.AppendLine(", ' [', B.EmpCode, ']' ");
                }
                SQL.AppendLine(") As Col2 ");
                SQL.AppendLine("From TblUser A ");
                if (mIsPICShowEmpCode)
                {
                    SQL.AppendLine("Left Join TblEmployee B On A.UserCode = B.UserCode And B.UserCode Is Not Null ");
                }
                if (TxtDocNo.Text.Length == 0 && mIsPICGrouped && BtnSave.Enabled)
                    SQL.AppendLine("where A.UserCode=@UserCode ");
                SQL.AppendLine("Order By A.UserName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueBankAcCode(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BankAcCode As Col1, ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            else if (mBankAccountFormat == "2")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(IfNull(A.BankAcNm, ''), ' : ', A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(' [', B.BankName, ']') Else '' End ");
                SQL.AppendLine("    )) As Col2 ");
            }
            else
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            if (mIsVoucherBankAccountFilteredByCurrency)
            {
                string mcurrency = string.Empty;
                mcurrency = Sm.GetLue(LueCurCode);
                SQL.AppendLine("Left Join TblCurrency C On A.CurCode=C.CurCode where C.CurCode= '" + mcurrency + "'");
            }
            if (!mIsVoucherBankAccountFilteredByCurrency)
                SQL.AppendLine("Where 0=0 ");
            
            if (BankAcCode.Length != 0)
                SQL.AppendLine("And A.BankAcCode=@BankAcCode ");
            else
            {
                if (mIsVoucherRequestBankAccountFilteredByGrp)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("And A.ActInd = 'Y' ");
                if (mMenuCode != mMenuCodeForVRBudget && ((mCashAdvanceJournalDebitFormat == "3" && Sm.GetLue(LueDocType) == mVoucherDocTypeCashAdvance) || Sm.GetLue(LueDocType) == mVoucherDocTypePettyCash))
                    SQL.AppendLine("And Find_In_Set(A.BankAcTp, @BankAcTp)");
            }

            SQL.AppendLine("Order By A.Sequence;");
            

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);
            Sm.CmParam<String>(ref cm, "@BankAcTp", Sm.GetLue(LueDocType) == mVoucherDocTypeCashAdvance ? mBankAccountTypeForVRCA1 : mBankAccountTypeForVRPCD1);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        private void SetLueBankAcCode2(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BankAcCode As Col1, ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            else if (mBankAccountFormat == "2")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(IfNull(A.BankAcNm, ''), ' : ', A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(' [', B.BankName, ']') Else '' End ");
                SQL.AppendLine("    )) As Col2 ");
            }
            else
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            SQL.AppendLine("Where 0=0 ");

            if (BankAcCode.Length != 0)
                SQL.AppendLine("And A.BankAcCode=@BankAcCode ");
            else
            {
                if (mIsVoucherRequestBankAccountFilteredByGrp)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("And A.ActInd = 'Y' ");

                if (mMenuCode != mMenuCodeForVRBudget && ((mCashAdvanceJournalDebitFormat == "3" && Sm.GetLue(LueDocType) == mVoucherDocTypeCashAdvance) || Sm.GetLue(LueDocType) == mVoucherDocTypePettyCash))
                    SQL.AppendLine("And Find_In_Set(A.BankAcTp, @BankAcTp2)");
            }

            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);
            Sm.CmParam<String>(ref cm, "@BankAcTp2", mBankAccountTypeForVRCA2);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }
        
        internal void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 5 });
            Grd2.Rows.Clear();
            Sm.ClearGrd(Grd3, true);
            Sm.FocusGrd(Grd3, 0, 1);
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.ClearGrd(Grd5, true);
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m;

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        Amt += Sm.GetGrdDec(Grd1, Row, 2);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void ConvertManual()
        {
            if (!mIsVRManualUseOtherCurrency) return;

            //decimal mManualRate = 0m;
            //if (Sm.GetLue(LueManualCurCode).Length == 0 || Sm.GetLue(LueManualCurCode) == mMainCurCode) mManualRate = 1m;
            //else
            //{
            //    if (TxtManualExcRate.Text.Length > 0) mManualRate = Decimal.Parse(TxtManualExcRate.Text);
            //}

            //if (Grd1.Rows.Count >= 1)
            //{
            //    for (int i = 0; i < Grd1.Rows.Count; ++i)
            //    {
            //        if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
            //        {
            //            Grd1.Cells[i, 2].Value = mManualRate * Sm.GetGrdDec(Grd1, i, 4);
            //        }
            //    }
            //}
        }

        private string GenerateDocNo()
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty,
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (Sm.GetLue(LueAcType) == "C")
                type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");
            else
                type = Sm.GetValue("Select AutoNoDebit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");

                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("       Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                //SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("       Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                //SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        private void SetLueProjectSystem1(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("    Select A.DocNo As Col1, Concat(A.AcCode, ' ', B.profitCenterName) As Col2  ");
                SQL.AppendLine("    From TblProject A  ");
                SQL.AppendLine("    Inner Join tblProfitCenter B On A.ProfitCenterCode = B.ProfitCenterCode  ");
                SQL.AppendLine("    Where ParentDocNo is null And Level = 1  ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Description", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueProjectSystem2(ref DXE.LookUpEdit Lue, string ProjectDocNo)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("    Select DocNo As Col1, Concat(AcCode, ' ', AcDesc) As Col2  ");
                SQL.AppendLine("    From TblProject  ");
                SQL.AppendLine("    Where ParentDocNo is not null And Level = 2 And ParentDocNo = '" + ProjectDocNo + "' ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Description", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueProjectSystem3(ref DXE.LookUpEdit Lue, string ProjectDocNo)
        {
            try
            {
                string AcCodeParent = Sm.GetValue("Select AcCode From TblProject Where Docno = '" + ProjectDocNo + "' ");
                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo As Col1, concat(AcCode, ' ', AcDesc) As Col2 From TblProject ");
                SQL.AppendLine("Where DocNo Not In (  ");
                SQL.AppendLine("    Select ParentDocNo From TblProject  ");
                SQL.AppendLine("    Where ParentDocNo Is Not Null  ");
                SQL.AppendLine("    )  ");
                SQL.AppendLine("And AcCode like '" + AcCodeParent + "%'  ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Description", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblVoucherRequestHdr " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        internal string GetSelectedJournal()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd3, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }
        private string GetProfitCenterCode()
        {
            //var Value = string.Empty ;
            var Value = Sm.GetLue(LueBankAcCode);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    " SELECT B.ProfitCenterCode FROM tblbankaccount A " +
                    " INNER JOIN tblsite B ON A.SiteCode = B.SiteCode " +
                    " INNER JOIN tblprofitcenter C ON B.ProfitCenterCode = C.ProfitCenterCode " +
                    " WHERE A.Bankaccode = @Param ",

            Value);
        }


        private void ComputeCurrencyRate()
        {
            string mRate = string.Empty;

            #region Old Code
            //string mCurCode2 = "IDR";

            //if (Sm.GetDte(DteDocDt).Length > 0 && TxtCurCode2.Text.Length > 0)
            //{
            //    if (TxtCurCode2.Text == mCurCode2) mRate = "1";
            //    else
            //    {
            //        mRate = Sm.GetValue("Select Amt From TblDailyCurrencyRate Where CurCode2 = '" + mCurCode2 + "' And Curcode1 = '" + TxtCurCode2.Text + "' And RateDt = '" + Sm.Left(Sm.GetDte(DteDocDt), 8) + "'");
            //    }
            //}
            //if (mRate.Length == 0) mRate = "0";
            #endregion

            string mCurCode2 = Sm.GetLue(LueCurCode);

            if (Sm.GetDte(DteDocDt).Length > 0 && TxtCurCode2.Text.Length > 0 && mCurCode2.Length > 0)
            {
                if (TxtCurCode2.Text == mCurCode2) mRate = "1";
                else
                {
                    mRate = Sm.GetValue("Select Amt From TblDailyCurrencyRate Where CurCode2 = '" + mCurCode2 + "' And Curcode1 = '" + TxtCurCode2.Text + "' And RateDt = '" + Sm.Left(Sm.GetDte(DteDocDt), 8) + "'");
                }
            }
            if (mRate.Length == 0) mRate = "0";

            TxtExcRate.Text = Sm.FormatNum(Decimal.Parse(mRate), 0);
        }
        
        #endregion

        #region FTP

        private void DownloadFile4(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private bool IsFTPClientDataNotValid4(int Row, string FileName)
        {

            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid4(int Row, string FileName)
        {
            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted4(int Row, string FileName)
        {
            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd4, Row, 3) != Sm.GetGrdStr(Grd4, Row, 6))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblVoucherRequestDtl3 ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }



        private bool IsFTPClientDataNotValid5(int Row, string FileName)
        {

            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid5(int Row, string FileName)
        {
            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted5(int Row, string FileName)
        {
            if (mIsVoucherRequestAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != Sm.GetGrdStr(Grd4, Row, 6))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblVoucherRequestDtl4 ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(SetLuePICCode));
                //Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue2(Sl.SetLueUserCode), string.Empty);

                if (mIsVRDeptCodeAutoFillBasedOnPIC)
                {
                    string UserCode = Sm.GetLue(LuePIC);
                    if (UserCode.Length > 0)
                    {
                        LueDeptCode.EditValue = null;
                        string DeptCode = Sm.GetValue("Select DeptCode From TblEmployee Where UserCode Is Not Null And UserCode = @Param Limit 1; ", UserCode);
                        Sm.SetLue(LueDeptCode, DeptCode);
                    }
                }
            }
        }

        private void LueDeptCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mMenuCode == mMenuCodeForVRBudget)
                {
                    
                    Sm.RefreshLookUpEdit(LueDeptCode2, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                    LueBCCode.EditValue = null;
                    ComputeAmt();
                    ComputeRemainingBudget();
                }
            }
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsGroupFilterByType)
                    Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue2(SetLueVoucherDocType), mIsGroupFilterByType ? "Y" : "N");
                else
                    Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue2(SetLueVoucherDocType), string.Empty);
                if (mMenuCodeForVRBudget == mMenuCode && mIsVRBudgetUseCASBA)
                    SetLueReqType(ref LueReqType, Sm.GetLue(LueReqType));
                if (mIsVRManualUseOtherCurrency)
                {
                    ComputeAmt();

                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 5 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });

                    //if (Sm.GetLue(LueDocType) != mVoucherDocTypeManual)
                    //{
                    //    LueManualCurCode.EditValue = null;
                    //    TxtManualExcRate.EditValue = Sm.FormatNum(1m, 0);
                    //}

                    //if (Sm.GetLue(LueManualCurCode).Length > 0)
                    //{
                    //    if (Sm.GetLue(LueDocType) == mVoucherDocTypeManual)
                    //    {
                    //        Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2 });
                    //        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4 });
                    //    }
                    //}
                    if (Sm.GetLue(LueDocType) != mVoucherDocTypeManual)
                        TxtAmt2.Visible = false;
                    else
                        TxtAmt2.Visible = true;
                }
                if (mMenuCodeForVRBudget != mMenuCode)
                {
                    SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                    SetLueBankAcCode2(ref LueBankAcCode2, string.Empty);
                    if ((Sm.GetLue(LueDocType) == mVoucherDocTypeCashAdvance && mCashAdvanceJournalDebitFormat == "3") || Sm.GetLue(LueDocType) == mVoucherDocTypePettyCash)
                    {
                        Sm.SetLue(LueAcType, "D");
                        Sm.SetLue(LueAcType2, "C");
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueAcType, LueAcType2 }, true);
                        label24.ForeColor = LblBankAcCode2.ForeColor = Color.Red;
                    }
                    else
                    {
                        Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueAcType, LueAcType2 });
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueAcType, LueAcType2 }, false);
                        label24.ForeColor = LblBankAcCode2.ForeColor = Color.Black;
                    }
                }

                if (mIsVoucherRequestCASUseDueDate && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueCashAdvanceTypeCode });
                    LblCashAdvanceTypeCode.ForeColor = Color.Black;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCashAdvanceTypeCode }, true);

                    if (Sm.GetLue(LueDocType) == "56") //CashAdvance
                    {
                        LblCashAdvanceTypeCode.ForeColor = Color.Red;
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCashAdvanceTypeCode }, false);
                    }
                }

                if (mIsVRCashAdvanceUseEmployeePIC)
                {
                    LblEmpName.ForeColor = Color.Black;
                    if (Sm.GetLue(LueDocType) == "56") LblEmpName.ForeColor = Color.Red;
                }
            }
        }

        private void LueManualCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && mIsVRManualUseOtherCurrency)
            {
                //Sm.RefreshLookUpEdit(LueManualCurCode, new StdMtd.RefreshLue1(SetLueCurCodeNonMain));

                //Sm.GrdColReadOnly(true, true, Grd1, new int[] { 4 });
                //Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });

                //if (Sm.GetLue(LueManualCurCode).Length > 0)
                //{
                //    if (Sm.GetLue(LueDocType) == mVoucherDocTypeManual)
                //    {
                //        Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2 });
                //        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4 });
                //    }
                //}
                //else
                //{
                //    TxtManualExcRate.EditValue = Sm.FormatNum(1m, 0);
                //    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                //    {
                //        Grd1.Cells[i, 4].Value = 0m;
                //    }
                //}

                //ConvertManual();
                //ComputeAmt();
            }
        }

        private void TxtManualExcRate_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && mIsVRManualUseOtherCurrency)
            {
                //Sm.FormatNumTxt(TxtManualExcRate, 0);

                //Sm.GrdColReadOnly(true, true, Grd1, new int[] { 4 });
                //Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });

                //if (Sm.GetLue(LueManualCurCode).Length > 0)
                //{
                //    if (Sm.GetLue(LueDocType) == mVoucherDocTypeManual)
                //    {
                //        Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2 });
                //        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4 });
                //    }
                //}

                //ConvertManual();
                //ComputeAmt();
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new StdMtd.RefreshLue1(Sl.SetLueCurCode));
                var CurCode = Sm.GetLue(LueCurCode);
                TxtExcRate.EditValue = Sm.FormatNum(0m, 0);
                if (CurCode.Length > 0 && Sm.CompareStr(CurCode, TxtCurCode2.Text))
                    TxtExcRate.EditValue = Sm.FormatNum(1m, 0);

                if (mIsVoucherBankAccountFilteredByCurrency)
                {
                    SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                    SetLueBankAcCode2(ref LueBankAcCode2, string.Empty);
                }

                if (mIsVRManualUseOtherCurrency)
                {
                    if (Sm.GetLue(LueDocType) == mVoucherDocTypeManual && Sm.GetLue(LueCurCode) != mMainCurCode)
                    {
                        Sm.SetControlReadOnly(TxtExcRate, false);
                        TxtExcRate.EditValue = Sm.FormatNum(0m, 0);
                    }
                }
            }
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
            if (Sm.GetLue(LueAcType) == "C")
            {
                LblBankAcCode.Text = "           Credit To";
                LblPaymentUser.Text = "                   Paid To";
            }
            else if (Sm.GetLue(LueAcType) == "D")
            {
                LblBankAcCode.Text = "           Debit To";
                LblPaymentUser.Text = "             Received By";
            }
            else
            {
                LblBankAcCode.Text = "Debit / Credit To";
                LblPaymentUser.Text = "Paid To / Received By";
            }
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
                if (mIsVoucherRequestUseCOA)
                {
                    Sm.ClearGrd(Grd3, true);
                    if (Sm.GetLue(LueBankAcCode).Length > 0)
                    {
                        string mAcNo = Sm.GetValue("Select COAAcNo From TblBankAccount Where BankAcCode = @Param; ", Sm.GetLue(LueBankAcCode));
                        string mAcDesc = Sm.GetValue("Select AcDesc From TblCOA Where AcNo = @Param; ", mAcNo);

                        if (mAcNo.Length > 0)
                        {
                            Grd3.Rows.Add();
                            Grd3.Cells[0, 1].Value = mAcNo;
                            Grd3.Cells[0, 2].Value = mAcDesc;
                            Grd3.Cells[0, 3].Value = Sm.FormatNum(0m, 0);
                            Grd3.Cells[0, 4].Value = Sm.FormatNum(0m, 0);
                            Grd3.Cells[0, 5].Value = null;
                            Sm.SetGrdNumValueZero(ref Grd3, 1, new int[] { 3, 4 });
                        }
                    }
                }
                SetBankAcTp(ref LueBankAcCode, ref TxtBankAcTp);
                SetBankCode();
            }
        }

        private void LueAcType2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcType2, new Sm.RefreshLue1(Sl.SetLueAcType));
            if (Sm.GetLue(LueAcType2) == "C")
            {
                LblBankAcCode2.Text = "           Credit To";
            }
            else if (Sm.GetLue(LueAcType2) == "D")
            {
                LblBankAcCode2.Text = "           Debit To";
            }
            else
            {
                LblBankAcCode2.Text = "Debit / Credit To";
            }


        }

        private void LueBankAcCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (!BtnSave.Enabled) return;
            try
            {
                Sm.RefreshLookUpEdit(LueBankAcCode2, new Sm.RefreshLue2(SetLueBankAcCode2), string.Empty);
                                
                SetBankAcTp(ref LueBankAcCode2, ref TxtBankAcTp2);
                Sm.SetControlReadOnly(TxtExcRate, true);
                TxtCurCode2.EditValue = null;
                TxtExcRate.EditValue = Sm.FormatNum(0m, 0);
                var BankAcCode = Sm.GetLue(LueBankAcCode2);
                if (BankAcCode.Length > 0)
                {
                    TxtCurCode2.EditValue = Sm.GetValue(
                        "Select CurCode From TblBankAccount Where BankAcCode=@Param;",
                        BankAcCode);
                    Sm.SetControlReadOnly(TxtExcRate, false);
                    if (Sm.CompareStr(Sm.GetLue(LueCurCode), TxtCurCode2.Text))
                            TxtExcRate.EditValue = Sm.FormatNum(1m, 0);
                    if (mIsUseDailyCurrencyRate)
                        ComputeCurrencyRate();
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsUseDailyCurrencyRate) ComputeCurrencyRate();
        }

        private void TxtExcRate_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                FormatNumTxt(TxtExcRate);
                ComputeAmt2();                
            }
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueBankCode, TxtGiroNo, DteOpeningDt, DteDueDt
            });

            if (Sm.GetLue(LuePaymentType) == "C")
            {
                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteOpeningDt, true);
                Sm.SetControlReadOnly(DteDueDt, false);
                LblBankCode.ForeColor = Color.Black;
                LblGiroNo.ForeColor = Color.Black;
                LblDueDt.ForeColor = Color.Black;
            }
            else if (Sm.GetLue(LuePaymentType) == "B")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteOpeningDt, true);
                Sm.SetControlReadOnly(DteDueDt, false);
                LblBankCode.ForeColor = Color.Red;
                LblGiroNo.ForeColor = Color.Black;
                LblDueDt.ForeColor = Color.Black;
                SetBankCode();
            }
            else if (Sm.GetLue(LuePaymentType) == "G")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteOpeningDt, false);
                Sm.SetControlReadOnly(DteDueDt, false);
                LblBankCode.ForeColor = Color.Red;
                LblGiroNo.ForeColor = Color.Red;
                LblDueDt.ForeColor = Color.Red;
                SetBankCode();
            }
            else if (Sm.GetLue(LuePaymentType) == "K")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteOpeningDt, false);
                Sm.SetControlReadOnly(DteDueDt, false);
                LblBankCode.ForeColor = Color.Red;
                LblGiroNo.ForeColor = Color.Red;
                LblDueDt.ForeColor = Color.Red;
                SetBankCode();
            }
            else
            {
                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteOpeningDt, true);
                Sm.SetControlReadOnly(DteDueDt, false);
                LblBankCode.ForeColor = Color.Black;
                LblGiroNo.ForeColor = Color.Black;
                LblDueDt.ForeColor = Color.Black;
            }
        }

        private void TxtPaymentUser_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaymentUser);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtGiroNo);
        }

        private void TxtBankCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankCode);
        }

        private void TxtBankBranch_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankBranch);
        }

        private void TxtBankAcNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankAcNo);
        }

        private void TxtBankAcName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankAcName);
        }

        private void LueDeptCode_Validated(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
            {
                if (mMenuCode == mMenuCodeForVRBudget)
                {
                    LueDeptCode2.EditValue = null;
                    LueBCCode.EditValue = null;
                }
            }   
        }

        private void BtnCopy_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestDlg6(this, mMInd));
        }

        private void LueCashAdvanceTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsVoucherRequestCASUseDueDate && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                Sm.RefreshLookUpEdit(LueCashAdvanceTypeCode, new Sm.RefreshLue2(Sl.SetLueOption), "VoucherRequestCASDueDate");
            }
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mMenuCode == mMenuCodeForVRBudget)
                {
                    if (mIsDeptFilterBySite)
                        Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
                    else
                        Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                    SetBudgetCategory();
                    ComputeAmt();
                    if (!mIsBudgetTransactionCanUseOtherDepartmentBudget)
                        ComputeRemainingBudget();
                }
                else
                    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(SetLueDeptCode), string.Empty);
            }

        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
          
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );

                Grd2.Font = new Font(
                    Grd2.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void ChkAutoWidth_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void LueProjectDocNo1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProjectDocNo1, new Sm.RefreshLue1(SetLueProjectSystem1));
                if (Sm.GetLue(LueProjectDocNo1).Length > 0)
                    SetLueProjectSystem2(ref LueProjectDocNo2, Sm.GetLue(LueProjectDocNo1));
            }
        }

        private void LueProjectDocNo2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueProjectDocNo2), "<Refresh>")) LueProjectDocNo2.EditValue = null;
            if (BtnSave.Enabled && Sm.GetLue(LueProjectDocNo1).Length > 0)
            {
                Sm.RefreshLookUpEdit(LueProjectDocNo1, new Sm.RefreshLue2(SetLueProjectSystem2), Sm.GetLue(LueProjectDocNo1));
                SetLueProjectSystem3(ref LueProjectDocNo3, Sm.GetLue(LueProjectDocNo2));
            }
        }

        private void BtnBCCode_Click(object sender, EventArgs e)
        {
            string mYr = Sm.Left(Sm.GetDte(DteDocDt), 4), mMth = Sm.GetDte(DteDocDt).Substring(4, 2);

            if (!Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode2, "Department for Budget") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmVoucherRequestDlg5(this, mYr, mMth, Sm.GetLue(LueDeptCode), Sm.GetLue(LueDeptCode2)));
        }

        private void LueProjectDocNo3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && Sm.GetLue(LueProjectDocNo2).Length > 0)
            {
                Sm.RefreshLookUpEdit(LueProjectDocNo3, new Sm.RefreshLue2(SetLueProjectSystem3), Sm.GetLue(LueProjectDocNo2));
            }
        }

        private void BtnDocType_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsTxtEmpty(TxtDocNo, "VR#", false))
                {
                    var VoucherRequestDocNo = TxtDocNo.Text;
                    var DocType = Sm.GetValue("Select DocType From TblVoucherRequestHdr Where DocNo=@Param;", TxtDocNo.Text);
                    var DocNo = string.Empty;
                    switch (DocType)
                    {
                        case "01":
                            DocNo = VoucherRequestDocNo;
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmVoucherRequest(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "02":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblIncomingPaymentHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                if (Sm.GetParameter("DocTitle") == "IMS")
                                {
                                    var f02 = new FrmIncomingPayment2(mMenuCode);
                                    f02.Tag = "***";
                                    f02.WindowState = FormWindowState.Normal;
                                    f02.StartPosition = FormStartPosition.CenterScreen;
                                    f02.mDocNo = DocNo;
                                    f02.ShowDialog();
                                }
                                else
                                {
                                    var f02 = new FrmIncomingPayment(mMenuCode);
                                    f02.Tag = "***";
                                    f02.WindowState = FormWindowState.Normal;
                                    f02.StartPosition = FormStartPosition.CenterScreen;
                                    f02.mDocNo = DocNo;
                                    f02.ShowDialog();
                                }
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "03":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblOutgoingPaymentHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f03 = new FrmOutgoingPayment(mMenuCode);
                                f03.Tag = "***";
                                f03.WindowState = FormWindowState.Normal;
                                f03.StartPosition = FormStartPosition.CenterScreen;
                                f03.mDocNo = DocNo;
                                f03.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "04":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblAPDownpayment Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmAPDownpayment(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "05":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblARDownpayment Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmARDownpayment(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "06":
                            if (Sm.GetParameter("DocTitle") == "IMS")
                            {
                                DocNo = Sm.GetValue(
                                    "Select DocNo From TblVoucherRequestPayrollHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                    VoucherRequestDocNo);
                                if (DocNo.Length > 0)
                                {
                                    var f01 = new FrmVoucherRequestPayroll16(mMenuCode);
                                    f01.Tag = "***";
                                    f01.WindowState = FormWindowState.Normal;
                                    f01.StartPosition = FormStartPosition.CenterScreen;
                                    f01.mDocNo = DocNo;
                                    f01.ShowDialog();
                                }
                                else
                                    Sm.StdMsg(mMsgType.Info, "Not available information.");
                                break;
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "07":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblVoucherRequestSSHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmVoucherRequestSS(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "08":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblEmpClaimHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmEmpClaim(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "09":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblAdvancePaymentHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmAdvancePayment(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "10":
                            Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "11":
                            Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "13":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblRHAHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmRHA(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "14":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblReturnAPDownpayment Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmReturnAPDownpayment(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "15":
                            Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "16":
                            DocNo = VoucherRequestDocNo;
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmVoucherRequest(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "17":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblOutgoingPaymentHdr Where VoucherRequestDocNo2=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f17 = new FrmOutgoingPayment(mMenuCode);
                                f17.Tag = "***";
                                f17.WindowState = FormWindowState.Normal;
                                f17.StartPosition = FormStartPosition.CenterScreen;
                                f17.mDocNo = DocNo;
                                f17.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "18":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblIncomingPaymentHdr Where VoucherRequestDocNo2=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f18 = new FrmIncomingPayment(mMenuCode);
                                f18.Tag = "***";
                                f18.WindowState = FormWindowState.Normal;
                                f18.StartPosition = FormStartPosition.CenterScreen;
                                f18.mDocNo = DocNo;
                                f18.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "19":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblAPDownpayment Where VoucherRequestDocNo2=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f19 = new FrmAPDownpayment(mMenuCode);
                                f19.Tag = "***";
                                f19.WindowState = FormWindowState.Normal;
                                f19.StartPosition = FormStartPosition.CenterScreen;
                                f19.mDocNo = DocNo;
                                f19.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "20":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblARDownpayment Where VoucherRequestDocNo2=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f20 = new FrmARDownpayment(mMenuCode);
                                f20.Tag = "***";
                                f20.WindowState = FormWindowState.Normal;
                                f20.StartPosition = FormStartPosition.CenterScreen;
                                f20.mDocNo = DocNo;
                                f20.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "21":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblVoucherRequestTax Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f21 = new FrmVoucherRequestTax(mMenuCode);
                                f21.Tag = "***";
                                f21.WindowState = FormWindowState.Normal;
                                f21.StartPosition = FormStartPosition.CenterScreen;
                                f21.mDocNo = DocNo;
                                f21.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "22":
                            Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "23":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblTravelRequestHdr Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f23 = new FrmTravelRequest(mMenuCode);
                                f23.Tag = "***";
                                f23.WindowState = FormWindowState.Normal;
                                f23.StartPosition = FormStartPosition.CenterScreen;
                                f23.mDocNo = DocNo;
                                f23.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "51":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblReturnARDownpayment Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f51 = new FrmReturnARDownpayment(mMenuCode);
                                f51.Tag = "***";
                                f51.WindowState = FormWindowState.Normal;
                                f51.StartPosition = FormStartPosition.CenterScreen;
                                f51.mDocNo = DocNo;
                                f51.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "52":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblSurvey Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f52 = new FrmSurvey(mMenuCode);
                                f52.Tag = "***";
                                f52.WindowState = FormWindowState.Normal;
                                f52.StartPosition = FormStartPosition.CenterScreen;
                                f52.mDocNo = DocNo;
                                f52.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "53":
                            // Partner Loan Payment
                            Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "54":
                            DocNo =
                              Sm.GetValue(
                              "Select DocNo From TblBonusHdr Where VoucherRequestDocNo=@Param Limit 1;",
                              VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f54 = new FrmBonus2(mMenuCode);
                                f54.Tag = "***";
                                f54.WindowState = FormWindowState.Normal;
                                f54.StartPosition = FormStartPosition.CenterScreen;
                                f54.mDocNo = DocNo;
                                f54.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "55":
                            DocNo =
                              Sm.GetValue(
                              "Select DocNo From TblDroppingPaymentHdr Where VoucherRequestDocNo=@Param Limit 1;",
                              VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f55 = new FrmDroppingPayment(mMenuCode);
                                f55.Tag = "***";
                                f55.WindowState = FormWindowState.Normal;
                                f55.StartPosition = FormStartPosition.CenterScreen;
                                f55.mDocNo = DocNo;
                                f55.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "56":
                            DocNo = VoucherRequestDocNo;
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmVoucherRequest(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "58":
                            DocNo =
                              Sm.GetValue(
                              "Select DocNo From TblCashAdvanceSettlementHdr Where VoucherRequestDocNo=@Param Limit 1;",
                              VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f58 = new FrmCashAdvanceSettlement(mMenuCode);
                                f58.Tag = "***";
                                f58.WindowState = FormWindowState.Normal;
                                f58.StartPosition = FormStartPosition.CenterScreen;
                                f58.mDocNo = DocNo;
                                f58.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "61":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblTravelRequestDtl8 Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f61 = new FrmTravelRequest4(mMenuCode);
                                f61.Tag = "***";
                                f61.WindowState = FormWindowState.Normal;
                                f61.StartPosition = FormStartPosition.CenterScreen;
                                f61.mDocNo = DocNo;
                                f61.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "65":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblVoucherRequestSpecialHdr Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f65 = new FrmVoucherRequestSpecial(Sm.GetParameter("MenuCodeForVRSOT"));
                                f65.Tag = "***";
                                f65.WindowState = FormWindowState.Normal;
                                f65.StartPosition = FormStartPosition.CenterScreen;
                                f65.mDocNo = DocNo;
                                f65.mIsVRSOT = true;
                                f65.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "66":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblVoucherRequestSpecialHdr Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f66 = new FrmVoucherRequestSpecial(Sm.GetParameter("MenuCodeForVRSRHA"));
                                f66.Tag = "***";
                                f66.WindowState = FormWindowState.Normal;
                                f66.StartPosition = FormStartPosition.CenterScreen;
                                f66.mIsVRSRHA = true;
                                f66.mDocNo = DocNo;
                                f66.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "67":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblVoucherRequestSpecialHdr Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f67 = new FrmVoucherRequestSpecial(Sm.GetParameter("MenuCodeForVRSIncentive"));
                                f67.Tag = "***";
                                f67.WindowState = FormWindowState.Normal;
                                f67.StartPosition = FormStartPosition.CenterScreen;
                                f67.mIsVRSIncentive = true;
                                f67.mDocNo = DocNo;
                                f67.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Sm.StdMsg(mMsgType.Info, "Not available information.");
            }
        }

        private void LueReqType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueReqType, new Sm.RefreshLue2(SetLueReqType), string.Empty);
                SetBudgetCategory();
                ComputeAmt();
                ComputeRemainingBudget();
            }
        }

        private void LueBIOPProject_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBIOPProject, new Sm.RefreshLue2(SetLueBIOPProject), string.Empty);
                if (Sm.GetLue(LueBIOPProject) == mBIOPProjectCodeForVRBudget)
                {
                    label36.ForeColor = Color.Red;
                    label38.ForeColor = Color.Red;
                }
                else
                {
                    label36.ForeColor = Color.Black;
                    label38.ForeColor = Color.Black;
                }
            }
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if(mIsBudgetTransactionCanUseOtherDepartmentBudget)
                    Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode2));
                else
                    Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode));
            }
            ComputeRemainingBudget();
        }

        private void TxtSOCDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSOCDocNo);
        }

        private MySqlCommand UpdateVRFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateVRFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateVRFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateVRFile4(string DocNo, int Row, string FileName, string Category)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestDtl3 Set ");
            SQL.AppendLine("    FileName=@FileName, ");
            SQL.AppendLine("    CategoryCode=@CategoryCode ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@CategoryCode", Category);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private MySqlCommand UpdateVRFile5(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestDtl4 Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo=@DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            var testdocno = DocNo;
            var testdno = Sm.Right("00" + (Row + 1).ToString(), 3);
            var testfilename = FileName;
            return cm;
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateVRFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateVRFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateVRFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile4(string DocNo, int Row, string FileName, string Category)
        {
            if (IsUploadFileNotValid4(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateVRFile4(DocNo, Row, toUpload.Name, Category));
            Sm.ExecCommands(cml);

        }

        private void UploadFile5(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid5(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));
            // FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));
            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            var NewFileName = toUpload.Name;
            cml.Add(UpdateVRFile5(DocNo, Row, NewFileName));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsUploadFileNotValid4(int Row, string FileName)
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid4(Row, FileName) ||
                IsFileSizeNotvalid4(Row, FileName) ||
                IsFileNameAlreadyExisted4(Row, FileName);
        }

        private bool IsUploadFileNotValid5(int Row, string FileName)
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid5(Row, FileName) ||
                IsFileSizeNotvalid5(Row, FileName) // ||
                // IsFileNameAlreadyExisted5(Row, FileName)
                ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsVoucherRequestAllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsVoucherRequestAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsVoucherRequestAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsVoucherRequestAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsVoucherRequestAllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsVoucherRequestAllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsVoucherRequestAllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsVoucherRequestAllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (mIsVoucherRequestAllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsVoucherRequestAllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsVoucherRequestAllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsVoucherRequestAllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsVoucherRequestAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsVoucherRequestAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsVoucherRequestAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsVoucherRequestAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblVoucherRequestHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (mIsVoucherRequestAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblVoucherRequestHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (mIsVoucherRequestAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblVoucherRequestHdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }
       
        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }
        
        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4, 5 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = 0;
            Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = 0;
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                //if (mIsVRManualUseOtherCurrency) ConvertManual();
                ComputeAmt();
                if(mMenuCodeForVRBudget == mMenuCode)
                    ComputeRemainingBudget();
                ComputeAmt2();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 2 || e.ColIndex == 5)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                    Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0m;

                //if (e.ColIndex == 4 && mIsVRManualUseOtherCurrency)
                //    ConvertManual();
                ComputeAmt();
                ComputeAmt2();
                if (mMenuCodeForVRBudget == mMenuCode) ComputeRemainingBudget();
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && BtnSave.Enabled)
                Sm.FormShowDialog(new FrmVoucherRequestDlg(this));
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmVoucherRequestDlg(this));
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0 &&
                ((e.ColIndex == 1 && Sm.GetGrdBool(Grd3, e.RowIndex, 2)) || e.ColIndex != 1))
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;
            }
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3) Compute(e.RowIndex, 1);
            if (e.ColIndex == 4) Compute(e.RowIndex, 2);
            ComputeDebetCredit(); ComputeBalanced();
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                ComputeDebetCredit();
                ComputeBalanced();
            }
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #region Grid 4

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 3).Length > 0)
                {
                    DownloadFile4(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 3), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 3);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 3, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 4)
                {
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 4;
                    OD.ShowDialog();
                    Grd4.Cells[e.RowIndex, 3].Value = OD.FileName;
                }
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4, 5 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);

                if (e.ColIndex == 2)
                {
                    Sm.LueRequestEdit(ref Grd4, ref LueVRCtCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                }
            }

            if (e.ColIndex == 5)
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 3).Length > 0)
                {
                    DownloadFile4(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 3), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 3);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 3, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd4_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 4)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 5)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdEnter(Grd4, e);
            Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
        }

        private void LueVRCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVRCtCode, new Sm.RefreshLue2(Sl.SetLueOption), "FileCategoryForVR");
        }

        private void LueVRCtCode_Validated(object sender, EventArgs e)
        {
            LueVRCtCode.Visible = false;
        }

        private void LueVRCtCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueVRCtCode_Leave(object sender, EventArgs e)
        {
            if (LueVRCtCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (LueVRCtCode.Text.Trim().Length == 0)
                    Grd4.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueVRCtCode).Trim();
                    Grd4.Cells[fCell.RowIndex, 2].Value = LueVRCtCode.GetColumnValue("Col2");
                }
            }
        }

        #endregion
        #region Grd5
        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd5, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile4(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd5, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd5, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd5, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");

                    //new
                    //Sm.DownloadFile(ref downloadedData, mFormatFTPClient, mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd5, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    //Sm.DownloadAction(ref SFD, ref downloadedData, Sm.GetGrdStr(Grd5, e.RowIndex, 1));
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd5, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt" +
                                "|Archive files (*.rar;*.zip)|*.rar;*.zip";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd5.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }
        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
                if (Sm.GetGrdStr(Grd5, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd5, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd5, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd5, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }
        private void Grd5_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd5, e, BtnSave);
                Sm.GrdEnter(Grd5, e);
                Sm.GrdTabInLastCell(Grd5, e, BtnFind, BtnSave);
            }
        }
        #endregion
        #endregion

        #region Button Clicks

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestDlg7(this));
        }

        private void BtnEmpCode2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtEmpName, "Employee Cash Adv. PIC", false))
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = mEmpCode;
                f.ShowDialog();
            }
        }

        private void BtnSOCDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestDlg2(this));
        }

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestDlg3(this));
        }

        private void BtnCOA_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestDlg4(this));
        }

        private void BtnSOCDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOCDocNo, "SO Contract#", false))
            {
                try
                {
                    var f = new FrmSOContract2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtSOCDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnItCode2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtItCode, "Finish Good", false))
            {
                try
                {
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = TxtItCode.Text;
                    f.ShowDialog();

                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnCOA2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtAcNo, "COA#", false))
            {
                try
                {
                    var f = new FrmCOA(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mAcNo = TxtAcNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestDlg2(this));
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false))
            {
                var f = new FrmSOContract2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtSOContractDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF Files (*.pdf)|*.pdf|MS Word (*.doc;*.docx)|*.doc;*.docx|MS Excel (*.xls;*.xlsx)|*.xls;*.xlsx|MS PowerPoint (*.ppt;*.pptx)|*.ppt;*.pptx|JPG (*.jpg)|*.jpg|PNG Files (*.png)|*.png";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF Files (*.pdf)|*.pdf|MS Word (*.doc;*.docx)|*.doc;*.docx|MS Excel (*.xls;*.xlsx)|*.xls;*.xlsx|MS PowerPoint (*.ppt;*.pptx)|*.ppt;*.pptx|JPG (*.jpg)|*.jpg|PNG Files (*.png)|*.png";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF Files (*.pdf)|*.pdf|MS Word (*.doc;*.docx)|*.doc;*.docx|MS Excel (*.xls;*.xlsx)|*.xls;*.xlsx|MS PowerPoint (*.ppt;*.pptx)|*.ppt;*.pptx|JPG (*.jpg)|*.jpg|PNG Files (*.png)|*.png";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile3, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #endregion

        #region Report Class

        private class VoucherReqHdr
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CancelInd { get; set; }
            public string DocType { get; set; }
            public string AcType { get; set; }
            public string VoucherDocNo { get; set; }
            public string VoucherDocDt { get; set; }
            public string BankAcc { get; set; }
            public string PaymentType { get; set; }
            public string GiroNo { get; set; }
            public string GiroBankName { get; set; }
            public string GiroDueDt { get; set; }
            public string PICName { get; set; }
            public decimal AmtHdr { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string RemarkHdr { get; set; }
            public string CompanyLogo { get; set; }
            public string PrintBy { get; set; }
            public string CurCode { get; set; }
            public decimal DocEnclosure { get; set; }
            public string PaymentUser { get; set; }
            public string Department { get; set; }
            public string PrintFormat { get; set; }
            public string DocTypeDesc { get; set; }
            public string LocalDocNo { get; set; }
            public string PICCode { get; set; }
            public string UserName { get; set; }
            public string BankAcNo { get; set; }
            public string BankAcNm { get; set; }
            public string CurName { get; set; }
            public string StatusDesc { get; set; }
            public string DivisionName { get; set; }
            public string BCName { get; set; }
            public string SiteName { get; set; }
            public string FooterImage { get; set; }
        }

        private class VoucherReqHdr2
        {
            public string CompanyName { get; set; }
            public string DocNo { get; set; }
            public string DocNoVR { get; set; }
            public string DocDt { get; set; }
            public string DNo { get; set; }
            public string PICEmpName { get; set; }
            public string PICPosName { get; set; }
            public string PICGrdLvlName { get; set; }
            public string TravelService { get; set; }
            public string Transportation { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public string result { get; set; }
            public string CityName { get; set; }
            public string PrintBy { get; set; }
            public string CompanyLogo { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string EmpName { get; set; }
            public string PosName { get; set; }
            public string GrdLvlName { get; set; }
            public string Result { get; set; }
            public string RemarkHdr { get; set; }
            public string ProfitCenterName { get; set; }
        }

        private class VoucherReqDtl
        {
            public int No { get; set; }
            public string DNo { get; set; }
            public string Description { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
        }

        private class VoucherReqDtl2
        {
            public string VoucherRequestDocNo { get; set; }
            public string DocNo { get; set; }
            public string EmpNAme { get; set; }
        }

        private class VoucherReqDtl3
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class VoucherReqDtl4
        {
            public string DocNo { get; set; }
            public string EmpCode { get; set; }
            public string AdName { get; set; }
            public decimal Amt { get; set; }
            public decimal Qty { get; set; }
            public decimal Total { get; set; }
        }

        class VoucherReqDtl5
        {
            public string UserName { get; set; }
            public string UserName2 { get; set; }
            public string UserName3 { get; set; }
            public string UserName4 { get; set; }
        }

        private class Bank
        {
            public string No { get; set; }
            public string DocNo { get; set; }
            public string InstructionCode { get; set; }
            public string FxCode { get; set; }
            public string DocDt { get; set; }
            public string CurCode { get; set; }
            public decimal Amt { get; set; }
            public string TrxRemark { get; set; }
            public string RateVoucherCode { get; set; }
            public string ChargeType { get; set; }

            public string DebitAccount { get; set; }
            public string SenderName { get; set; }
            public string SenderAddress { get; set; }
            public string SenderCountryCode { get; set; }
            public string CreditAccount { get; set; }
            public string BeneficiaryName { get; set; }
            public string BeneficiaryAddress { get; set; }
            public string BeneficiaryCountryCode { get; set; }
            public string BenBankIdentifier { get; set; }
            public string BenBankName { get; set; }

            public string BenBankAddress { get; set; }
            public string BenBankCountryCode { get; set; }
            public string InterBankIdentifier { get; set; }
            public string InterBankName { get; set; }
            public string InterBankAddress { get; set; }
            public string InterBankCountryCode { get; set; }
            public string Notification { get; set; }
            public string BeneficiaryCategory { get; set; }
            public string BeneficiaryRelation { get; set; }
            public string BITransactionCode { get; set; }

            public string TemplateCode { get; set; }
        }

        private class Bank2
        {
            public string ProcCode { get; set; }
            public string BillingID { get; set; }
            public string DebitAccount { get; set; }
            public string Reference { get; set; }
            public string Date { get; set; }

            public string Email { get; set; }
            public int Count { get; set; }
        }

        private class FileDetails
        {
            public string DocNo { get; set; }
            public string CategoryCode { get; set; }
            public string FileName { get; set; }
        }

        private class Files
        {
            public string File1 { get; set; }
            public string File2 { get; set; }
            public string File3 { get; set; }
            public string File4 { get; set; }
            public string File5 { get; set; }
            public string File6 { get; set; }
            public string File7 { get; set; }
            public string File8 { get; set; }
            public string File9 { get; set; }
            public string File10 { get; set; }
            public string File11 { get; set; }
            public string File12 { get; set; }
            public string File13 { get; set; }
            public string File14 { get; set; }
            public string File15 { get; set; }
            public string File16 { get; set; }
            public string File17 { get; set; }
            public string File18 { get; set; }
        }

        private class VoucherReqDtl6
        {
            public string EmpPict1 { get; set; }
            public string Description1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }

            public string EmpPict2 { get; set; }
            public string Description2 { get; set; }
            public string UserName2 { get; set; }
            public string ApproveDt2 { get; set; }

            public string EmpPict3 { get; set; }
            public string Description3 { get; set; }
            public string UserName3 { get; set; }
            public string ApproveDt3 { get; set; }

            public string EmpPict4 { get; set; }
            public string Description4 { get; set; }
            public string UserName4 { get; set; }
            public string ApproveDt4 { get; set; }

            public string EmpPict5 { get; set; }
            public string Description5 { get; set; }
            public string UserName5 { get; set; }
            public string ApproveDt5 { get; set; }

            public string EmpPict6 { get; set; }
            public string Description6 { get; set; }
            public string UserName6 { get; set; }
            public string ApproveDt6 { get; set; }

            public string EmpPict7 { get; set; }
            public string Description7 { get; set; }
            public string UserName7 { get; set; }
            public string ApproveDt7 { get; set; }

            public string EmpPict8 { get; set; }
            public string Description8 { get; set; }
            public string UserName8 { get; set; }
            public string ApproveDt8 { get; set; }

            public string EmpPict9 { get; set; }
            public string Description9 { get; set; }
            public string UserName9 { get; set; }
            public string ApproveDt9 { get; set; }

            public string EmpPict10 { get; set; }
            public string Description10 { get; set; }
            public string UserName10 { get; set; }
            public string ApproveDt10 { get; set; }
        }

        private class VoucherReqDtl7 
        {
            public string EmpPict1 { get; set; }
            public string Description1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }

            public string EmpPict2 { get; set; }
            public string Description2 { get; set; }
            public string UserName2 { get; set; }
            public string ApproveDt2 { get; set; }

            public string EmpPict3 { get; set; }
            public string Description3 { get; set; }
            public string UserName3 { get; set; }
            public string ApproveDt3 { get; set; }

            public string EmpPict4 { get; set; }
            public string Description4 { get; set; }
            public string UserName4 { get; set; }
            public string ApproveDt4 { get; set; }
        }

        private class VoucherReqDtl8
        {
            public string EmpPict1 { get; set; }
            public string Description1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }

            public string EmpPict2 { get; set; }
            public string Description2 { get; set; }
            public string UserName2 { get; set; }
            public string ApproveDt2 { get; set; }

            public string EmpPict3 { get; set; }
            public string Description3 { get; set; }
            public string UserName3 { get; set; }
            public string ApproveDt3 { get; set; }


            public string PosName1 { get; set; }
            public string PosName2 { get; set; }
            public string PosName3 { get; set; }

        }

        private class VoucherReqDtl9
        {
            public string Signature { get; set; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Date { get; set; }
            public string Sequence { get; set; }
            public string Title { get; set; }
            public string Space { get; set; }
            public string LabelName { get; set; }
            public string LabelPos { get; set; }
            public string LabelDt { get; set; }

        }

        private class VoucherReqDtl10
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string Sequence { get; set; }
        }

        #endregion
    }
}
