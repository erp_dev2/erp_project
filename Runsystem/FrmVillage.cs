﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVillage : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmVillageFind FrmFind;

        #endregion

        #region Constructor

        public FrmVillage(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);

            Sl.SetLueSDCode(ref LueSDCode);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtVilCode, TxtVilName, LueSDCode
                    }, true);
                    TxtVilCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtVilCode, TxtVilName, LueSDCode
                    }, false);
                    TxtVilCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtVilName, LueSDCode
                    }, false);
                    TxtVilName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtVilCode, TxtVilName, LueSDCode
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVillageFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtVilCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtVilCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblVillage Where VilCode=@VilCode" };
                Sm.CmParam<String>(ref cm, "@VilCode", TxtVilCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblVillage(VilCode, VilName, SDCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@VilCode, @VilName, @SDCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update VilName=@VilName, SDCode=@SDCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@VilCode", TxtVilCode.Text);
                Sm.CmParam<String>(ref cm, "@VilName", TxtVilName.Text);
                Sm.CmParam<String>(ref cm, "@SDCode", Sm.GetLue(LueSDCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtVilCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string VilCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@VilCode", VilCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select VilCode, VilName, SDCode From TblVillage Where VilCode=@VilCode",
                        new string[] 
                        {
                            "VilCode", "VilName", "SDCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtVilCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtVilName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueSDCode, Sm.DrStr(dr, c[2]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtVilCode, "Village code", false) ||
                Sm.IsTxtEmpty(TxtVilName, "Village name", false) ||
                Sm.IsLueEmpty(LueSDCode, "Country") ||
                IsVilCodeExisted();
        }

        private bool IsVilCodeExisted()
        {
            if (!TxtVilCode.Properties.ReadOnly && Sm.IsDataExist("Select VilCode From TblVillage Where VilCode='" + TxtVilCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Village code ( " + TxtVilCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtVilCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVilCode);
        }

        private void TxtVilName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVilName);
        }

        private void LueSDCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSDCode, new Sm.RefreshLue1(Sl.SetLueSDCode));
        }

        #endregion

        #endregion
    }
}
