﻿#region Update
    // 31/08/2017 [WED] bug fixing saat ambil nilai Additional Cost / Discount Sales Invoice
    // 11/09/2017 [WED] hanya tampilkan kolom VAT In dan VAT Out
    // 11/09/2017 [WED] tambah filter document type
    // 15/09/2020 [IBL/YK] Reporting dapat menarik SI for project
    // 15/09/2020 [IBL/YK] Menambah kolom Vendor Name / Customer Name
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEstimatedVRVAT : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptEstimatedVRVAT(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueTaxGrpCode(ref LueTaxGrpCode);
                SetLueDocType(ref LueDocType);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            #region Old Code

            //SQL.AppendLine("Select T.* From ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select '1' As Type, X2.DocNo, X2.DocDt, X2.Amt, null as AddCostDisc, ");
            //SQL.AppendLine("    null as Total, null as TaxBase, IfNull(X1.Amt, 0) As VATIn, null as VATOut, ");
            //SQL.AppendLine("    Concat('Tax# : ', X1.TaxInvoiceNo, ' | ', 'Tax Date : ', DATE_FORMAT(X1.TaxInvoiceDt, '%d/%b/%Y')) As Remark ");
            //SQL.AppendLine("    From TblVoucherRequestPPNDtl X1 ");
            //SQL.AppendLine("    Inner Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select T1.DocNo, T1.DocDt, IfNull(Sum(T2.Amt), 0) As Amt ");
            //SQL.AppendLine("        From TblPurchaseInvoiceHdr T1 ");
            //SQL.AppendLine("        Inner Join ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select A.DocNo, ");
            //SQL.AppendLine("            (((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue )*Case When IfNull(K.TaxRate, 0)=0 Then 0 Else K.TaxRate/100 End) As TaxAmt1, ");
            //SQL.AppendLine("           (((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue )*Case When IfNull(L.TaxRate, 0)=0 Then 0 Else L.TaxRate/100 End) As TaxAmt2, ");
            //SQL.AppendLine("           (((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue )*Case When IfNull(M.TaxRate, 0)=0 Then 0 Else M.TaxRate/100 End) As TaxAmt3, ");
            //SQL.AppendLine("           ( ");
            //SQL.AppendLine("               ((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue) ");
            //SQL.AppendLine("           ) As Amt ");
            //SQL.AppendLine("            From TblPurchaseInvoiceDtl A ");
            //SQL.AppendLine("           Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
            //SQL.AppendLine("           Inner Join TblRecvVdhdr B2 On A.RecvVdDocNo=B2.DocNo ");
            //SQL.AppendLine("           Inner Join TblPOHdr C On B.PODocNo=C.DocNo ");
            //SQL.AppendLine("           Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo ");
            //SQL.AppendLine("           Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
            //SQL.AppendLine("           Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
            //SQL.AppendLine("           Inner Join TblQtHdr G On E.QtDocNo=G.DocNo ");
            //SQL.AppendLine("           Inner Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
            //SQL.AppendLine("           Inner Join TblItem I On B.ItCode=I.ItCode ");
            //SQL.AppendLine("           Left Join TblPaymentTerm J On G.PtCode=J.PtCode ");
            //SQL.AppendLine("           Left Join TblTax K On C.TaxCode1=K.TaxCode ");
            //SQL.AppendLine("           Left Join TblTax L On C.TaxCode2=L.TaxCode ");
            //SQL.AppendLine("           Left Join TblTax M On C.TaxCode3=M.TaxCode ");
            //SQL.AppendLine("        )T2 On T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("        Where T1.CancelInd = 'N' ");
            //SQL.AppendLine("        Group By T1.DocNo ");
            //SQL.AppendLine("    )X2 On X1.DocNoIn = X2.DocNo ");
            	
            //SQL.AppendLine("    Union All ");
            	
            //SQL.AppendLine("    Select '2' As Type, X2.DocNo, X2.DocDt, X2.Amt, null as AddCostDisc,  ");
            //SQL.AppendLine("    null as Total, null as TaxBase, null As VATIn, IfNull(X1.Amt, 0) as VATOut, ");
            //SQL.AppendLine("    Concat('Tax# : ', X1.TaxInvoiceNo, ' | ', 'Tax Date : ', DATE_FORMAT(X1.TaxInvoiceDt, '%d/%b/%Y')) As Remark ");
            //SQL.AppendLine("    From TblVoucherRequestPPNDtl2 X1 ");
            //SQL.AppendLine("    Inner Join  ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select T1.DocNo, T1.DocDt, IfNull(T1.TotalAmt, 0) As Amt ");
            //SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
            //SQL.AppendLine("        Where T1.CancelInd = 'N' ");
            //SQL.AppendLine("    )X2 On X1.DocNoOut = X2.DocNo ");
            //SQL.AppendLine(")T ");

            #endregion

            SQL.AppendLine("Select T.Type, T.DocTypeDesc, T.DocNo, T.DocDt, T.VATIn, T.VATOut, T.Remark, T.VdCtName From ( ");

            #region VAT In

            // Purchase Invoice
            SQL.AppendLine("Select '1' As Type, 'Purchase Invoice' As DocTypeDesc, A.DocNo, A.DocDt, E.VdName As VdCtName, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then B.TaxRate*A.Amt*0.01 ");
            SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then C.TaxRate*A.Amt*0.01 ");
            SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then D.TaxRate*A.Amt*0.01 ");
            SQL.AppendLine("Else 0.00 End As VATIn, ");
            SQL.AppendLine("0.00 As VATOut, ");
            SQL.AppendLine("Concat('Tax# : ', ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo ");
            SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo2 ");
            SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo3 ");
            SQL.AppendLine("Else Null End, ");
            SQL.AppendLine("' | Tax Date : ', ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then DATE_FORMAT(A.TaxInvoiceDt, '%d/%b/%Y') ");
            SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then DATE_FORMAT(A.TaxInvoiceDt2, '%d/%b/%Y') ");
            SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then DATE_FORMAT(A.TaxInvoiceDt3, '%d/%b/%Y') ");
            SQL.AppendLine("Else Null End  ");
            SQL.AppendLine(") As Remark ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Left Join TblTax B On A.TaxCode1=B.TaxCode ");
            SQL.AppendLine("Left Join TblTax C On A.TaxCode2=C.TaxCode ");
            SQL.AppendLine("Left Join TblTax D On A.TaxCode3=D.TaxCode ");
            SQL.AppendLine("Inner Join TblVendor E On A.VdCode=E.VdCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (T1.TaxCode1 Is Not Null And T1.VoucherRequestPPNDocNo Is Null) ");
            SQL.AppendLine("    Or (T1.TaxCode2 Is Not Null And T1.VoucherRequestPPNDocNo2 Is Null) ");
            SQL.AppendLine("    Or (T1.TaxCode3 Is Not Null And T1.VoucherRequestPPNDocNo3 Is Null) ");            
            SQL.AppendLine(") ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("(T1.TaxCode1 Is Not Null And T1.VATSettlementDocNo Is Null) ");
            SQL.AppendLine("Or (T1.TaxCode2 Is Not Null And T1.VATSettlementDocNo2 Is Null) ");
            SQL.AppendLine("Or (T1.TaxCode3 Is Not Null And T1.VATSettlementDocNo3 Is Null) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine("Or IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine("Or IfNull(T1.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") F On A.DocNo=F.DocNo ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (A.TaxCode1 Is Not Null And A.VoucherRequestPPNDocNo Is Null) ");
            SQL.AppendLine("    Or (A.TaxCode2 Is Not Null And A.VoucherRequestPPNDocNo2 Is Null) ");
            SQL.AppendLine("    Or (A.TaxCode3 Is Not Null And A.VoucherRequestPPNDocNo3 Is Null) ");            
            SQL.AppendLine(") ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("(A.TaxCode1 Is Not Null And A.VATSettlementDocNo Is Null) ");
            SQL.AppendLine("Or (A.TaxCode2 Is Not Null And A.VATSettlementDocNo2 Is Null) ");
            SQL.AppendLine("Or (A.TaxCode3 Is Not Null And A.VATSettlementDocNo3 Is Null) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine("Or IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine("Or IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine(") ");

            SQL.AppendLine("Union All ");

            // Purchase Return Invoice
            SQL.AppendLine("Select '2' As Type, 'Purchase Return Invoice' As DocTypeDesc, A.DocNo, A.DocDt, B.VdName As VdCtName, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxAmt1 ");
            SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxAmt2 ");
            SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxAmt3 ");
            SQL.AppendLine("Else 0.00 End As VATIn, ");
            SQL.AppendLine("0.00 As  VATOut, ");
            SQL.AppendLine("Concat('Tax# : ', ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo ");
            SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo2 ");
            SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then A.TaxInvoiceNo3 ");
            SQL.AppendLine("Else Null End, ");
            SQL.AppendLine("' | Tax Date : ', ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then DATE_FORMAT(A.TaxInvoiceDt, '%d/%b/%Y') ");
            SQL.AppendLine("When IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then DATE_FORMAT(A.TaxInvoiceDt2, '%d/%b/%Y') ");
            SQL.AppendLine("When IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then DATE_FORMAT(A.TaxInvoiceDt3, '%d/%b/%Y') ");
            SQL.AppendLine("Else Null End ");
            SQL.AppendLine(") As Remark ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo ");
            SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblPurchaseReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T3 On T2.DOVdDocNo=T3.DocNo And T2.DOVdDNo=T3.DNo ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T4 On T3.RecvVdDocNo=T4.DocNo And T3.RecvVdDNo=T4.DNo ");
            SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (T1.TaxCode1 Is Not Null And T1.VoucherRequestPPNDocNo Is Null) ");
            SQL.AppendLine("    Or (T1.TaxCode2 Is Not Null And T1.VoucherRequestPPNDocNo2 Is Null) ");
            SQL.AppendLine("    Or (T1.TaxCode3 Is Not Null And T1.VoucherRequestPPNDocNo3 Is Null) ");            
            SQL.AppendLine(") ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("(T1.TaxCode1 Is Not Null And T1.VATSettlementDocNo Is Null) ");
            SQL.AppendLine("Or (T1.TaxCode2 Is Not Null And T1.VATSettlementDocNo2 Is Null) ");
            SQL.AppendLine("Or (T1.TaxCode3 Is Not Null And T1.VATSettlementDocNo3 Is Null) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine("Or IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine("Or IfNull(T1.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblPurchaseReturnInvoiceDtl D On A.DocNo=D.DocNo And D.DNo='001' ");
            SQL.AppendLine("Inner Join TblDOVdDtl E On D.DOVdDocNo=E.DocNo And D.DOVdDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl F On E.RecvVdDocNo=F.RecvVdDocNo And E.RecvVdDNo=F.RecvVdDNo ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr G On F.DocNo=G.DocNo And G.CancelInd='N' ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (A.TaxCode1 Is Not Null And A.VoucherRequestPPNDocNo Is Null) ");
            SQL.AppendLine("    Or (A.TaxCode2 Is Not Null And A.VoucherRequestPPNDocNo2 Is Null) ");
            SQL.AppendLine("    Or (A.TaxCode3 Is Not Null And A.VoucherRequestPPNDocNo3 Is Null) ");            
            SQL.AppendLine(") ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("(A.TaxCode1 Is Not Null And A.VATSettlementDocNo Is Null) ");
            SQL.AppendLine("Or (A.TaxCode2 Is Not Null And A.VATSettlementDocNo2 Is Null) ");
            SQL.AppendLine("Or (A.TaxCode3 Is Not Null And A.VATSettlementDocNo3 Is Null) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine("Or IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine("Or IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            SQL.AppendLine(") ");
            
            SQL.AppendLine("Union All ");

            // Voucher Request Tax
            SQL.AppendLine("Select '3' As Type, 'Voucher Request Tax' As DocTypeDesc, DocNo, DocDt, Null As VdCtName, ");
            SQL.AppendLine("Case When IfNull(TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) Then Amt Else 0.00 End As VATIn, ");
            SQL.AppendLine("0.00 As VATOut, ");
            SQL.AppendLine("Concat('Tax# : ', IfNull(TaxInvNo, ''), ' | Tax Date : ', IfNull(DATE_FORMAT(TaxInvDt, '%d/%b/%Y'), '')) As Remark ");
            SQL.AppendLine("From TblVoucherRequestTax A ");
            SQL.AppendLine("Where (DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");            
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
            SQL.AppendLine("And IfNull(A.TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
            if (ChkDocNo.Checked) SQL.AppendLine(" And Upper(A.DocNo) Like @DocNo1 ");

            #endregion

            SQL.AppendLine("Union All ");

            #region VAT Out

            // Sales Invoice
            SQL.AppendLine("Select '4' As DocType, 'Sales Invoice' As DocTypeDesc, A.DocNo, A.DocDt, B.CtName As VdCtName, ");
            SQL.AppendLine("0.00 As VATIn, A.TotalTax As VATOut, ");
            SQL.AppendLine("Concat('Tax# : ', IfNull(A.TaxInvDocument, ''), ' | Tax Date : ', IfNull(DATE_FORMAT(A.DocDt, '%d/%b/%Y'), '')) As Remark ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.DocNo ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct T1.DocNo ");
            SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl2 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
            SQL.AppendLine("        Inner Join TblDRDtl T5 On T3.DRDocNo=T5.DocNo And T4.DRDNo=T5.DNo ");
            SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
            SQL.AppendLine("        And T1.TotalTax<>0 ");
            SQL.AppendLine("        And T1.SODocNo Is Null ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Distinct T1.DocNo ");
            SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl3 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
            SQL.AppendLine("        Inner Join TblPLDtl T5 On T3.PLDocNo=T5.DocNo And T4.PLDNo=T5.DNo ");
            SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
            SQL.AppendLine("        And T1.TotalTax<>0 ");
            SQL.AppendLine("        And T1.SODocNo Is Null ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Distinct DocNo ");
            SQL.AppendLine("        From TblSalesInvoiceHdr ");
            SQL.AppendLine("        Where (DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    VoucherRequestPPNDocNo Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("        And VATSettlementDocNo Is Null ");
            SQL.AppendLine("        And TotalTax<>0 ");
            SQL.AppendLine("        And SODocNo Is Not Null ");
            SQL.AppendLine("    ) T Group By T.DocNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
            SQL.AppendLine("And A.TotalTax<>0 ");

            SQL.AppendLine("Union All ");

            // Sales Return Invoice
            SQL.AppendLine("Select '5' As DocType, 'Sales Return Invoice' As DocTypeDesc, A.DocNo, A.DocDt, B.CtName As VdCtName, ");
            SQL.AppendLine("0.00 As VATIn, A.TaxAmt As VATOut, ");
            SQL.AppendLine("Concat('Tax# : ', IfNull(A.TaxInvDocument, ''), ' | Tax Date : ', IfNull(DATE_FORMAT(A.TaxInvoiceDt, '%d/%b/%Y'), '')) As Remark ");
            SQL.AppendLine("From TblSalesReturnInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.DocNo ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct T1.DocNo ");
            SQL.AppendLine("        From TblSalesReturnInvoiceHdr T1 ");
            SQL.AppendLine("        Inner Join TblSalesReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblRecvCtDtl T3 On T1.RecvCtDocNo=T3.DocNo And T2.RecvCtDNo=T3.DNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr T4 On T3.DOCtDocNo=T4.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl2 T5 On T3.DOCtDocNo=T5.DocNo And T3.DOCtDNo=T5.DNo ");
            SQL.AppendLine("        Inner Join TblDRDtl T6 On T4.DRDocNo=T6.DocNo And T5.DRDNo=T6.DNo ");
            SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
            SQL.AppendLine("        And T1.TaxAmt<>0 ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Distinct T1.DocNo ");
            SQL.AppendLine("        From TblSalesReturnInvoiceHdr T1 ");
            SQL.AppendLine("        Inner Join TblSalesReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblRecvCtDtl T3 On T1.RecvCtDocNo=T3.DocNo And T2.RecvCtDNo=T3.DNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr T4 On T3.DOCtDocNo=T4.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl3 T5 On T3.DOCtDocNo=T5.DocNo And T3.DOCtDNo=T5.DNo ");
            SQL.AppendLine("        Inner Join TblPLDtl T6 On T4.PLDocNo=T6.DocNo And T5.PLDNo=T6.DNo ");
            SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
            SQL.AppendLine("        And T1.TaxAmt<>0 ");
            SQL.AppendLine("    ) T Group By T.DocNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblSalesReturnInvoiceDtl D On A.DocNo=D.DocNo And D.DNo='001' ");
            SQL.AppendLine("Inner Join TblRecvCtDtl E On D.RecvCtDocNo=E.DocNo And D.RecvCtDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl F On E.DOCtDocNo=F.DOCtDocNo And E.DOCtDNo=F.DOCtDNo ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr G On F.DocNo=G.DocNo And G.CancelInd='N' ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
            SQL.AppendLine("And A.TaxAmt<>0 ");

            SQL.AppendLine("Union All ");

            // POS
            SQL.AppendLine("Select '6' As DocType, 'Point Of Sale' As DocTypeDesc, A.DocNo, A.DocDt, B.CtName As VdCtName, ");
            SQL.AppendLine("0.00 As VATIn, A.Amt As VATOut, ");
            SQL.AppendLine("Concat('Tax# : ', IfNull(A.TaxInvoiceNo, ''), ' | Tax Date : ', IfNull(DATE_FORMAT(A.TaxInvoiceDt, '%d/%b/%Y'), '')) As Remark ");
            SQL.AppendLine("From TblPosVat A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left Join TblOption C On A.PaymentType=C.OptCode And C.OptCat='VoucherPaymentType' ");
            SQL.AppendLine("Left Join TblBankAccount D On A.BankAcCode=D.BankAcCode ");
            SQL.AppendLine("Left Join TblBank E On D.BankCode=E.BankCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.CancelInd='N' ");

            SQL.AppendLine("And ( ");
            SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
            SQL.AppendLine("And A.Amt<>0 ");

            SQL.AppendLine("Union All");

            //Sales Invoice For Project
            SQL.AppendLine("Select '7' As DocType, 'Sales Invoice For Project' As DocTypeDesc, A.DocNo, A.DocDt, B.CtName As VdCtName, ");
            SQL.AppendLine("0.00 As VATIn, A.TotalTax As VATOut, ");
            SQL.AppendLine("Concat('Tax# : ', IfNull(A.TaxInvoiceNo, ''), ' | Tax Date : ', IfNull(DATE_FORMAT(A.DocDt, '%d/%b/%Y'), '')) As Remark ");
            SQL.AppendLine("From TblSalesInvoice5Hdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
            SQL.AppendLine("And A.TotalTax<>0 ");


            SQL.AppendLine("Order By DocNo ");

            #endregion

            SQL.AppendLine(")T ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type", 
                        "Document Type",
                        "Document#",
                        "",
                        "Date", 
                        
                        //6-9
                        "VAT (in)",
                        "VAT (out)",
                        "Remark",
                        "Vendor / Customer"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        0, 150, 180, 20, 80, 
                        
                        //6-9
                        150, 150, 420, 300
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9 });
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[9].Move(6);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Sm.ClearGrd(Grd1, false);
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                    Sm.IsLueEmpty(LueTaxGrpCode, "Tax Group")
                    ) return;

                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@MainCurCode", Sm.GetParameter("MainCurCode"));
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@TaxGrpCode", Sm.GetLue(LueTaxGrpCode));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "T.Type", false);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Order By T.Type, T.DocNo;",
                    new string[]
                    {
                        //0
                        "Type",
                        
                        //1-5
                        "DocTypeDesc", "DocNo", "DocDt", "VATIn", "VATOut", 
                        
                        //6-7
                        "Remark", "VdCtName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    }, true, false, false, false
                );
                //ComputeOtherAmt();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 7 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 7 });
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "1")
                {
                    var f = new FrmPurchaseInvoice(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = "Purchase Invoice";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "2")
                {
                    var f = new FrmPurchaseReturnInvoice(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = "Purchase Return Invoice";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "3")
                {
                    var f = new FrmVoucherRequestTax(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = "Voucher Request for Tax";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "4")
                {
                    var CBDInd = Sm.GetValue("Select CBDInd From TblSalesInvoiceHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 3) + "';");
                    if (CBDInd == "Y")
                    {
                        var f = new FrmSalesInvoice2(mMenuCode);
                        f.Tag = mMenuCode;
                        f.Text = "Sales Invoice";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                        f.ShowDialog();
                    }
                    else
                    {
                        var f = new FrmSalesInvoice(mMenuCode);
                        f.Tag = mMenuCode;
                        f.Text = "Sales Invoice";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                        f.ShowDialog();
                    }
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "5")
                {
                    var f = new FrmSalesReturnInvoice(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = "Sales Return Invoice";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "6")
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select B.DocNo As VoucherDocNo ");
                    SQL.AppendLine("From TblPosVat A ");
                    SQL.AppendLine("Inner Join TblVoucherHdr B On A.DocDt = B.DocDt And A.BankAcCode = B.BankAcCode And B.DocType = '10' ");
                    SQL.AppendLine("Where A.DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 3) + "'; ");

                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = "Voucher";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetValue(SQL.ToString());
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "1")
                {
                    var f = new FrmPurchaseInvoice(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = "Purchase Invoice";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "2")
                {
                    var f = new FrmPurchaseReturnInvoice(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = "Purchase Return Invoice";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "3")
                {
                    var f = new FrmVoucherRequestTax(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = "Voucher Request for Tax";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "4")
                {
                    var CBDInd = Sm.GetValue("Select CBDInd From TblSalesInvoiceHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 3) + "';");
                    if (CBDInd == "Y")
                    {
                        var f = new FrmSalesInvoice2(mMenuCode);
                        f.Tag = mMenuCode;
                        f.Text = "Sales Invoice";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                        f.ShowDialog();
                    }
                    else
                    {
                        var f = new FrmSalesInvoice(mMenuCode);
                        f.Tag = mMenuCode;
                        f.Text = "Sales Invoice";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                        f.ShowDialog();
                    }
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "5")
                {
                    var f = new FrmSalesReturnInvoice(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = "Sales Return Invoice";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "6")
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select B.DocNo As VoucherDocNo ");
                    SQL.AppendLine("From TblPosVat A ");
                    SQL.AppendLine("Inner Join TblVoucherHdr B On A.DocDt = B.DocDt And A.BankAcCode = B.BankAcCode And B.DocType = '10' ");
                    SQL.AppendLine("Where A.DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 3) + "'; ");

                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = "Voucher";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetValue(SQL.ToString());
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void SetLueDocType(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Purchase Invoice' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Purchase Return Invoice' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '3' As Col1, 'Voucher Request for Tax' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '4' As Col1, 'Sales Invoice' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '5' As Col1, 'Sales Return Invoice' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '6' As Col1, 'Point Of Sales' As Col2 ");
            
            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueTaxGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxGrpCode, new Sm.RefreshLue1(Sl.SetLueTaxGrpCode));
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        #endregion

        #endregion

    }
}
