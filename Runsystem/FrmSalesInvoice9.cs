﻿#region Update
/*
    19/04/2021 [ICA/KSM] new apps
    29/27/2021 [IBL/KSM] Loop pada Tab Additional, Disc, Cost, Etc. masih blm bisa buka dialog.
    30/07/2021 [RDA/KSM] printout spinning/weaving menu Sales Invoice CBD (Based on Sales Contract)
    08/05/2021 [RDA/KSM] penambahan field department pada menu Sales Invoice CBD (Based on Sales Contract)
    20/09/2021 [TKG/ALL] tambah validasi setting journal
    12/11/2021 [YOG/KSM] Penyesuaian Printout : Menambahkan Nomor Dokumen di Printout
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice9 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mSalesBasedDOQtyIndex = string.Empty,
            mSLI3TaxCalculationFormat = string.Empty,
            mLocalDocNo = string.Empty;
        private string
            mMainCurCode = string.Empty,
            mDueDtSIValue = string.Empty,
            mTaxCodeSIValue = string.Empty,
            mEmpCodeSI = string.Empty,
            mEmpCodeTaxCollector = string.Empty,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mTaxInvDocument = string.Empty
            ;
        internal bool
            mIsAutoJournalActived = false,
            mIsRemarkForJournalMandatory = false,
            mSITaxRoundingDown = false,
            mIsCustomerItemNameMandatory = false,
            mIsDOCtAmtRounded = false,
            mIsSLI3UseAutoFacturNumber = false,
            mIsFilterByCtCt = false,
            mIsCustomerComboShowCategory = false,
            mIsFilterBySite = false
            ;

        private decimal mAdditionalCostDiscAmt = 0m;
        private bool 
            mIsSalesInvoiceTaxInvDocEditable = false,
            mIsSalesInvoice8TaxEnabled = false,
            mIsSalesInvoice8COANonTaxable = false,
            mIsSalesInvoice8TaxInvDocumentAbleToEdit = false,
            mIsCheckCOAJournalNotExists = false,
            mIsSalesMemoGenerateLocalDocNo = false
            ;

        internal FrmSalesInvoice9Find FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmSalesInvoice9(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Sales Invoice Based on Sales Contract ";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                SetLueSiteCode(ref LueSiteCode, string.Empty);
                //SetLueSPCode(ref LueSPCode);
                TcSalesInvoice8.SelectedTabPage = Tp3;
                SetLueOptionCode(ref LueOption);
                LueOption.Visible = false;
                TcSalesInvoice8.SelectedTabPage = Tp4;
                Sl.SetLueTaxCode(ref LueTaxCode1);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);
                
                SetGrd();
                SetFormControl(mState.View);
                if (mIsRemarkForJournalMandatory) LblRemark.ForeColor = Color.Red;
                if (mIsSalesMemoGenerateLocalDocNo) BtnLocalDocNo.Visible = false;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "SC#",
                        "SC DNo",
                        "SC" + Environment.NewLine + "Date",
                        "",
                        "Item's Code",

                        //6-10
                        "",
                        "Item's Name",
                        "Product's Name", 
                        "Local Code", 
                        "Quantity",

                        //11-15
                        "UoM",
                        "Price", 
                        "Total", 
                        "Unit Price" + Environment.NewLine + "(After Tax)",
                        "Total"+Environment.NewLine + "(After Tax)",

                        //16-18
                        "Delivery"+Environment.NewLine+"Date",
                        "Currency",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        150, 100, 100, 20, 100, 
 
                        //6-10
                        20, 200, 200, 150, 80, 
                        
                        //11-15
                        80, 100, 120, 100, 120, 
                        
                        //16-18
                        100, 80, 100, 
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 4, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 13, 14, 15 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 16 });

            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(Grd2, new string[] { "Currency", "Deposit Amount" }, new int[] { 100, 200 });
            Sm.GrdFormatDec(Grd2, new int[] { 1 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 11;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "",
                        "Debit"+Environment.NewLine+"Amount",
                        "",
                        //6-10
                        "Credit"+Environment.NewLine+"Amount",
                        "OptCode",
                        "Option",
                        "Remark",
                        ""
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 20, 100, 20, 
                        //6-10
                        100, 50, 150, 400, 20
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 3, 5, 10 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 7, 10 }, false);

            #endregion
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 6 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason, ChkCancelInd, DteDocDt, LueCtCode, DteDueDt, 
                        TxtLocalDocNo, LueSPCode, LueTaxCode1, LueTaxCode2, LueTaxCode3, 
                        DteTaxInvoiceDt, TxtTaxInvDocument, LueCurCode, TxtDownpayment, LueBankAcCode, 
                        TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, MeeRemark, LueSiteCode, LueDeptCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 18 });
                    BtnSCDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, DteDueDt,LueTaxCode1, LueTaxCode2, LueTaxCode3,
                        LueCurCode, TxtDownpayment, MeeRemark,  LueBankAcCode, LueSPCode,DteTaxInvoiceDt,
                        LueSiteCode, TxtTaxInvDocument, TxtLocalDocNo, TxtDownpayment, LueDeptCode
                    }, false);
                    BtnSCDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 18 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 8, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    BtnSCDocNo.Enabled = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mAdditionalCostDiscAmt = 0m;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, MeeCancelReason, DteDocDt, LueCtCode, TxtCtCtCode, 
                 DteDueDt, TxtLocalDocNo, TxtTaxInvDocument, LueCurCode, MeeRemark, 
                 LueSPCode, LueBankAcCode, LueTaxCode1, LueTaxCode2, LueTaxCode3, 
                 DteTaxInvoiceDt, TxtJournalDocNo, TxtJournalDocNo2, LueSiteCode, 
                 TxtSCDocNo, LueDeptCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { 
                TxtTotalAmt, TxtTotalTax, TxtDownpayment, TxtAmt,
                TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        #region Clear Grid

        private void ClearGrd()
        {
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
        }

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 12, 13, 14, 15 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 1 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesInvoice9Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                SetLueCtCode(ref LueCtCode, string.Empty);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Order by CreateDt limit 1"));
                if (mDueDtSIValue.Length > 0 && Sm.GetDte(DteDocDt).Length > 0)
                    DteDueDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteDocDt).Substring(0, 8)).AddDays(double.Parse(mDueDtSIValue));

                if (mTaxCodeSIValue.Length > 0)
                {
                    Sm.SetLue(LueTaxCode1, mTaxCodeSIValue);
                    ComputeAmtFromTax();
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            if (IsDataCancelledAlready()) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty, SCDocNo = string.Empty;
            if (Sm.GetGrdStr(Grd1, 0, 1).Length > 0) 
                SCDocNo = Sm.GetGrdStr(Grd1, 0, 1);
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesInvoice9", "TblSalesInvoiceHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalesInvoiceHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveSalesInvoiceDtl(DocNo, Row));

            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveSalesInvoiceDtl2(DocNo, Row));
            }

            if (decimal.Parse(TxtDownpayment.Text) != 0)
            {
                var lR = new List<Rate>();
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                decimal DP = 0m;

                SQL.AppendLine("Select CtCode, CurCode, ExcRate, Amt ");
                SQL.AppendLine("From TblCustomerDepositSummary2 ");
                SQL.AppendLine("Where CtCode = @CtCode ");
                SQL.AppendLine("And CurCode = @CurCode ");
                SQL.AppendLine("And Amt > 0 Order By CreateDt Asc; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                    Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CtCode",

                         //1-3
                         "CurCode",
                         "ExcRate",
                         "Amt"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lR.Add(new Rate()
                            {
                                CtCode = Sm.DrStr(dr, c[0]),
                                CurCode = Sm.DrStr(dr, c[1]),
                                ExcRate = Sm.DrDec(dr, c[2]),
                                Amt = Sm.DrDec(dr, c[3])
                            });
                        }
                    }
                    dr.Close();
                }

                if (lR.Count > 0)
                {
                    DP = decimal.Parse(TxtDownpayment.Text);
                    for (int i = 0; i < lR.Count; i++)
                    {
                        if (DP > 0)
                        {
                            cml.Add(SaveExcRate(ref lR, i, DocNo, DP));
                            DP = DP - lR[i].Amt;
                        }
                    }
                }

                cml.Add(SaveCustomerDeposit(DocNo));
            }

            //cml.Add(UpdateDOCtProcessInd(DocNo, "N"));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsDteEmpty(DteDueDt, "Due date") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsCurrencyNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsJournalSettingInvalid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 SC.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "SC list entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd3.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Additional cost, discount information entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                var AcType = string.Empty;

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd3, Row, 4) == 0m && Sm.GetGrdDec(Grd3, Row, 6) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount can't be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd3, Row, 4) != 0m && Sm.GetGrdDec(Grd3, Row, 6) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount# : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount both can't be bigger than 0.");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsCurrencyNotValid()
        {
            string CurCode = Sm.GetLue(LueCurCode);
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, Row, 17)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "One sales invoice# only allowed 1 currency type.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDownpaymentNotValid()
        {
            decimal Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (Downpayment == 0m) return false;

            //Recompute Deposit
            ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));

            //Get Currency
            decimal Deposit = 0m;
            string CurCode = Sm.GetLue(LueCurCode);

            //Get Deposit Amount Based on currency
            if (Grd2.Rows.Count > 0)
            {
                for (int row = 0; row < Grd2.Rows.Count - 1; row++)
                {
                    if (Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, row, 0)))
                    {
                        Deposit = Sm.GetGrdDec(Grd2, row, 1);
                        break;
                    }
                }
            }

            if (Downpayment > Deposit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Currency : " + CurCode + Environment.NewLine +
                    "Deposit Amount: " + Sm.FormatNum(Deposit, 0) + Environment.NewLine +
                    "Downpayment Amount: " + Sm.FormatNum(Downpayment, 0) + Environment.NewLine + Environment.NewLine +
                    "Downpayment is bigger than existing deposit."
                    );
                return true;
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) Debit += Sm.GetGrdDec(Grd3, Row, 4);
                if (Sm.GetGrdStr(Grd3, Row, 6).Length > 0) Credit += Sm.GetGrdDec(Grd3, Row, 6);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter

            if (Sm.GetParameter("CustomerAcNoAR").Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoAR is empty.");
                return true;
            }

            if (Sm.GetParameter("CustomerAcNoNonInvoice").Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                return true;
            }

            if (TxtDownpayment.Text.Length > 0 && decimal.Parse(TxtDownpayment.Text) != 0)
            {
                if (Sm.GetParameter("CustomerAcNoDownPayment").Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoDownPayment is empty.");
                    return true;
                }
            }

            if (Sm.GetParameter("AcNoForForeignCurrencyExchangeGains").Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForForeignCurrencyExchangeGains is empty.");
                return true;
            }

            //Table
            if (IsJournalSettingInvalid_Tax(Msg)) return true;
            
            return false;
        }

        private bool IsJournalSettingInvalid_Tax(string Msg)
        {
            var subSQL = new StringBuilder();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsExist = false;
            string
                Tax = string.Empty,
                TaxCode1 = Sm.GetLue(LueTaxCode1),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3);

            if (TaxCode1.Length > 0)
            {
                subSQL.AppendLine("Select Concat(TaxName, ' [Output]') As Tax From TblTax ");
                subSQL.AppendLine("Where TaxCode=@TaxCode1 ");
                subSQL.AppendLine("And TaxRate>=0.00 ");
                subSQL.AppendLine("And AcNo2 Is Null ");
                subSQL.AppendLine("Union All ");
                subSQL.AppendLine("Select Concat(TaxName, ' [Input]') As Tax  From TblTax ");
                subSQL.AppendLine("Where TaxCode=@TaxCode1 ");
                subSQL.AppendLine("And TaxRate<0.00 ");
                subSQL.AppendLine("And AcNo1 Is Null ");

                Sm.CmParam<String>(ref cm, "@TaxCode1", TaxCode1);

                IsExist = true;
            }

            if (TaxCode2.Length > 0)
            {
                if (IsExist) SQL.AppendLine("Union All ");

                subSQL.AppendLine("Select Concat(TaxName, ' [Output]') As Tax From TblTax ");
                subSQL.AppendLine("Where TaxCode=@TaxCode2 ");
                subSQL.AppendLine("And TaxRate>=0.00 ");
                subSQL.AppendLine("And AcNo2 Is Null ");
                subSQL.AppendLine("Union All ");
                subSQL.AppendLine("Select Concat(TaxName, ' [Input]') As Tax  From TblTax ");
                subSQL.AppendLine("Where TaxCode=@TaxCode2 ");
                subSQL.AppendLine("And TaxRate<0.00 ");
                subSQL.AppendLine("And AcNo1 Is Null ");

                Sm.CmParam<String>(ref cm, "@TaxCode2", TaxCode2);

                IsExist = true;
            }

            if (TaxCode3.Length > 0)
            {
                if (IsExist) SQL.AppendLine("Union All ");

                subSQL.AppendLine("Select Concat(TaxName, ' [Output]') As Tax From TblTax ");
                subSQL.AppendLine("Where TaxCode=@TaxCode3 ");
                subSQL.AppendLine("And TaxRate>=0.00 ");
                subSQL.AppendLine("And AcNo2 Is Null ");
                subSQL.AppendLine("Union All ");
                subSQL.AppendLine("Select Concat(TaxName, ' [Input]') As Tax From TblTax ");
                subSQL.AppendLine("Where TaxCode=@TaxCode3 ");
                subSQL.AppendLine("And TaxRate<0.00 ");
                subSQL.AppendLine("And AcNo1 Is Null ");

                Sm.CmParam<String>(ref cm, "@TaxCode3", TaxCode3);

                IsExist = true;
            }

            if (IsExist)
            {
                SQL.AppendLine("Select Tax From (");
                SQL.AppendLine(subSQL.ToString());
                SQL.AppendLine(") T Limit 1;");
            }
            else
                SQL.AppendLine("Select TaxName As Tax From TblTax Where 1=0;");

            cm.CommandText = SQL.ToString();
            Tax = Sm.GetValue(cm);
            if (Tax.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Tax's COA account# (" + Tax + ") is empty.");
                return true;
            }
            return false;
        }

        //private bool IsJournalSettingInvalid()
        //{
        //    if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

        //    var cm = new MySqlCommand();
        //    Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
        //    Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
        //    Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));

        //    if (Grd3.Rows.Count > 1)
        //    {
        //        for (int row = 0; row < Grd3.Rows.Count - 1; row++)
        //        {
        //            Sm.CmParam<String>(ref cm, "@AcNo_" + row, Sm.GetGrdStr(Grd3, row, 1));
        //        }
        //        if (mSLI3TaxCalculationFormat != "1")
        //            Sm.CmParam<String>(ref cm, "@CtCode", string.Concat("%.", Sm.GetLue(LueCtCode)));
        //    }



        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select AcNo From ");
        //    SQL.AppendLine("( ");

        //    //Piutang Usaha
        //    SQL.AppendLine("Select ParValue As AcNo ");
        //    SQL.AppendLine("From TblParameter Where ParCode = 'CustomerAcNoAR' ");

        //    //Piutang Uninvoice
        //    SQL.AppendLine("Union All ");
        //    SQL.AppendLine("Select ParValue As AcNo ");
        //    SQL.AppendLine("From TblParameter Where ParCode = 'CustomerAcNoNonInvoice' ");

        //    //PPN Keluaran 1
        //    SQL.AppendLine("Union All ");
        //    SQL.AppendLine("Select Case When TaxRate>=0.00 Then AcNo2 Else AcNo1 End As AcNo ");
        //    SQL.AppendLine("From TblTax Where TaxCode = @TaxCode1 ");

        //    SQL.AppendLine("Union All ");
        //    SQL.AppendLine("Select Case When TaxRate>=0.00 Then AcNo2 Else AcNo1 End As AcNo ");
        //    SQL.AppendLine("From TblTax Where TaxCode = @TaxCode2 ");

        //    SQL.AppendLine("Union All ");
        //    SQL.AppendLine("Select Case When TaxRate>=0.00 Then AcNo2 Else AcNo1 End As AcNo ");
        //    SQL.AppendLine("From TblTax Where TaxCode = @TaxCode3 ");

        //    //Downpayment
        //    if (TxtDownpayment.Text.Length > 0 && decimal.Parse(TxtDownpayment.Text) != 0)
        //    {
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select ParValue As AcNo ");
        //        SQL.AppendLine("From TblParameter Where ParCode = 'CustomerAcNoDownPayment' ");
        //    }

        //    //List Of COA
        //    if (Grd3.Rows.Count > 1)
        //    {
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select AcNo From ( ");
        //        for (int row = 0; row < Grd3.Rows.Count - 1; row++)
        //        {
        //            SQL.AppendLine("Select @AcNo_" + row + " As AcNo ");
        //            if (row < Grd3.Rows.Count - 2)
        //                SQL.AppendLine("Union All ");
        //        }
        //        SQL.AppendLine(") T1 ");
        //        if (mSLI3TaxCalculationFormat != "1")
        //            SQL.AppendLine("Where T1.AcNo Not Like @CtCode ");
        //    }

        //    //Laba Rugi Selisih Kurs
        //    if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
        //    {
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select ParValue As AcNo ");
        //        SQL.AppendLine("From TblParameter Where ParCode = 'AcNoForForeignCurrencyExchangeGains' ");
        //    }
        //    SQL.AppendLine(") Tbl ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandText = SQL.ToString();

        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //        {
        //            "AcNo",
        //        });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                if (Sm.DrStr(dr, c[0]).Length == 0)
        //                {
        //                    Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
        //                    return true;
        //                }
        //            }
        //        }
        //        dr.Close();
        //    }

        //    return false;
        //}

        private MySqlCommand SaveCustomerDeposit(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, ");
            SQL.AppendLine("(Case @CancelInd When 'Y' Then '04' Else '03' End) As DocType, ");
            SQL.AppendLine("DocDt, CtCode, CurCode, (Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesInvoiceHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @CtCode, @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+((Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt), LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoiceHdr(string DocNo)
        {
            string Doctitle = Sm.GetParameter("Doctitle");
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoiceHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, ProcessInd, CBDInd, DeptCode, CtCode, DueDt, LocalDocNo, SODocNo, TaxInvDocument, TaxInvDt, CurCode, TotalAmt, TotalTax, Downpayment, Amt, AdditionalCostDiscAmt, BankAcCode, SalesName, JournalDocNo, JournalDocNo2, ");
            SQL.AppendLine("TaxCode1, TaxCode2, TaxCode3, TaxAmt1, TaxAmt2, TaxAmt3, SiteCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', 'O', 'Y', @DeptCode, @CtCode, @DueDt, @LocalDocNo, @SCDocNo, @TaxInvDocument, @TaxInvDt, @CurCode, @TotalAmt, @TotalTax, @Downpayment, @Amt, @AdditionalCostDiscAmt, @BankAcCode, @SalesName, Null, Null, ");
            SQL.AppendLine("@TaxCode1, @TaxCode2, @TaxCode3, @TaxAmt1, @TaxAmt2, @TaxAmt3, @Sitecode, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SCDocNo", TxtSCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvDocument", TxtTaxInvDocument.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", decimal.Parse(TxtTotalTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@Downpayment", decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@AdditionalCostDiscAmt", mAdditionalCostDiscAmt);
            Sm.CmParam<String>(ref cm, "@SalesName", Sm.GetValue("Select SpName From tblSalesPerson Where SpCode = '" + Sm.GetLue(LueSPCode) + "' "));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParamDt(ref cm, "@TaxInvDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", decimal.Parse(TxtTaxAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoiceDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoiceDtl(DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ItCode, Qty, UPriceBeforeTax, UPriceAfterTax, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @DocType, @DOCtDocNo, @DOCtDNo, 'O', @ItCode, @Qty, @UPriceBeforeTax, @UPriceAfterTax, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocType", "3");
            Sm.CmParam<String>(ref cm, "@DOCtDocNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@DOCtDNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@UPriceBeforeTax", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@UPriceAfterTax", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSalesInvoiceDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSalesInvoiceDtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc,  AcInd,  Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @OptAcDesc,  @AcInd, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@OptAcDesc", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@AcInd", Sm.GetGrdBool(Grd3, Row, 10) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var CurCode = Sm.GetLue(LueCurCode);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblSalesInvoiceHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Sales Invoice : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, @EntCode As EntCode, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");


            // Piutang Usaha
            if (mSLI3TaxCalculationFormat == "1")
            {
                #region Default
                if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        A.TotalAmt+A.TotalTax-A.Downpayment As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    if (mIsDOCtAmtRounded)
                    {
                        SQL.AppendLine("        Floor(IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        )) * ");
                        SQL.AppendLine("        (A.TotalAmt+A.TotalTax-A.Downpayment), 0) As DAmt, ");
                    }
                    else
                    {
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0) * ");
                        SQL.AppendLine("        (A.TotalAmt+A.TotalTax-A.Downpayment) As DAmt, ");
                    }

                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                #endregion
            }
            else
            {
                #region SRN
                if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        A.Amt As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null  ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    if (mIsDOCtAmtRounded)
                    {
                        SQL.AppendLine("        Floor(IfNull((( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        )) * ");
                        SQL.AppendLine("        (A.Amt), 0)) As DAmt, ");
                    }
                    else
                    {
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0) * ");
                        SQL.AppendLine("        (A.Amt) As DAmt, ");
                    }
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                #endregion
            }

            //Piutang Uninvoice

            if (mSLI3TaxCalculationFormat == "1")
            {
                #region Default
                if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        A.TotalAmt As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");

                    #region Old Code, ngga perlu parameter kata ian via WA Chat ke wedha tanggal 22 des 2020 jam 14:57
                    //SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    //SQL.AppendLine("        0.00 As DAmt, ");
                    //if (mIsDOCtAmtRounded)
                    //{
                    //    SQL.AppendLine("        Floor(IfNull((");
                    //    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    //    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    //    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    //    SQL.AppendLine("        ), 0) * ");
                    //    SQL.AppendLine("        A.TotalAmt) As CAmt ");
                    //}
                    //else
                    //{
                    //    SQL.AppendLine("        IfNull(( ");
                    //    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    //    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    //    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    //    SQL.AppendLine("        ), 0) * ");
                    //    SQL.AppendLine("        A.TotalAmt As CAmt ");
                    //}
                    //SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    //SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                    //SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    #endregion

                    SQL.AppendLine("        Select Concat(D.Parvalue, E.CtCode) As AcNo, 0.00 As DAmt, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Floor( ");

                    SQL.AppendLine("            Sum( ");
                    SQL.AppendLine("            IfNull( ");
                    SQL.AppendLine("                 ( ");
                    SQL.AppendLine("                      Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                      Where RateDt <= C.DocDt And CurCode1 = E.CurCode And CurCode2 = @MainCurCode ");
                    SQL.AppendLine("                      Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("                ) ");
                    SQL.AppendLine("            , 0.00)  ");
                    SQL.AppendLine("            * (A.QtyPackagingUnit * A.UPriceBeforeTax)) ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        ) ");
                    SQL.AppendLine("            As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceDtl A ");
                    SQL.AppendLine("        Inner Join TblDoct2Dtl B On A.DOCtDocNo = B.DocNo And A.DOCtDNo = B.DNo ");
                    SQL.AppendLine("            And A.DocNo = @DocNo ");
                    SQL.AppendLine("        Inner Join TblDoct2Hdr C On B.DocNo = C.DocNo ");
                    SQL.AppendLine("        Inner Join TblParameter D On D.ParCode = 'CustomerAcNoNonInvoice' And D.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join TblSalesInvoiceHdr E On A.DocNo = E.DocNo ");
                    SQL.AppendLine("        Group By Concat(D.Parvalue, E.CtCode) ");
                }
                #endregion
            }
            else
            {
                #region SRN
                if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        C.Amt As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select DocNo, Sum(Qty * UPriceBeforeTax) Amt ");
                    SQL.AppendLine("            From TblSalesInvoiceDtl ");
                    SQL.AppendLine("            Where DocNo = @DocNo ");
                    SQL.AppendLine("            Group By DocNo ");
                    SQL.AppendLine("        ) C On A.DocNo = C.DocNo ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    if (mIsDOCtAmtRounded)
                    {
                        SQL.AppendLine("        Floor(IfNull((");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0) * ");
                        SQL.AppendLine("        C.Amt) As CAmt ");
                    }
                    else
                    {
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0) * ");
                        SQL.AppendLine("        C.Amt As CAmt ");
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select DocNo, Sum(Qty * UPriceBeforeTax) Amt ");
                    SQL.AppendLine("            From TblSalesInvoiceDtl ");
                    SQL.AppendLine("            Where DocNo = @DocNo ");
                    SQL.AppendLine("            Group By DocNo ");
                    SQL.AppendLine("        ) C On A.DocNo = C.DocNo ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                #endregion
            }

            //PPN Keluaran

            if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                if (mIsDOCtAmtRounded)
                {
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TotalAmt*B.TaxRate*0.01)) End As DAmt, ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TotalAmt*B.TaxRate*0.01) Else 0.00 End As CAmt ");
                }
                else
                {
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                }
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                if (mIsDOCtAmtRounded)
                {
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TotalAmt*B.TaxRate*0.01)) End As DAmt, ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TotalAmt*B.TaxRate*0.01) Else 0.00 End As CAmt ");
                }
                else
                {
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                }
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                if (mIsDOCtAmtRounded)
                {
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TotalAmt*B.TaxRate*0.01)) End As DAmt, ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TotalAmt*B.TaxRate*0.01) Else 0.00 End As CAmt ");
                }
                else
                {
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                }
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null ");
            }
            else
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");

                if (mIsDOCtAmtRounded)
                {
                    SQL.AppendLine("        Floor(IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End) As DAmt, ");
                    SQL.AppendLine("        Floor(IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End) As CAmt ");
                }
                else
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                }
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                if (mIsDOCtAmtRounded)
                {
                    SQL.AppendLine("        Floor(IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End) As DAmt, ");
                    SQL.AppendLine("        Floor(IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End) As CAmt ");
                }
                else
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                }
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                if (mIsDOCtAmtRounded)
                {
                    SQL.AppendLine("        Floor(IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End) As DAmt, ");
                    SQL.AppendLine("        Floor(IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End) As CAmt ");
                }
                else
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00)* ");
                    SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                }
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null ");
            }

            //Downpayment
            if (TxtDownpayment.Text.Length > 0 && decimal.Parse(TxtDownpayment.Text) != 0)
            {
                if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        A.Downpayment As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    if (mIsDOCtAmtRounded)
                    {
                        SQL.AppendLine("        Floor(IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00)*A.Downpayment) As DAmt, ");
                    }
                    else
                    {
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00) * ");
                        SQL.AppendLine("        A.Downpayment As DAmt, ");
                    }
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
            }


            //List of COA

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo, ");
            if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
            }
            SQL.AppendLine("        B.DAmt, ");
            if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
            }
            SQL.AppendLine("        B.CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            if (mSLI3TaxCalculationFormat != "1")
            {
                SQL.AppendLine("        And B.AcNo Not Like @CtCode ");
            }


            //Laba rugi selisih kurs
            if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");
            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo  ");
            SQL.AppendLine(") T;  ");

            if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
                SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");
            }
            else
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl ");
                SQL.AppendLine("        Where DocNo=@JournalDocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ParValue From TblParameter Where ParCode='SIRoundedMaxValIfJournalNotBalance' And ParValue Is Not Null ");
                SQL.AppendLine(") C On 1=1 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt=B.CAmt Then ");
                SQL.AppendLine("        A.DAmt Else ");
                SQL.AppendLine("        Case When Abs(B.DAmt-B.CAmt)<IfNull(C.ParValue, 0.00) Then ");
                SQL.AppendLine("            A.DAmt-(B.DAmt-B.CAmt) Else A.DAmt End ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    );");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (mSLI3TaxCalculationFormat != "1")
                Sm.CmParam<String>(ref cm, "@CtCode", string.Concat("%.", Sm.GetLue(LueCtCode)));
            if (mDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));

            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);

            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesInvoiceHdr());

            if (ChkCancelInd.Checked)
            {
                if (decimal.Parse(TxtDownpayment.Text) != 0)
                {
                    var lR = new List<Rate>();
                    var cm = new MySqlCommand();
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select B.CtCode, A.CurCode, A.ExcRate, A.Amt ");
                    SQL.AppendLine("From TblSalesInvoiceDtl3 A ");
                    SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("Where A.DocNo = @DocNo And B.CtCode = @CtCode ");
                    SQL.AppendLine("And A.CurCode = @CurCode ");
                    SQL.AppendLine("And A.Amt > 0 Order By A.CreateDt Desc; ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                        Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CtCode",

                         //1-3
                         "CurCode",
                         "ExcRate",
                         "Amt"
                        });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                lR.Add(new Rate()
                                {
                                    CtCode = Sm.DrStr(dr, c[0]),

                                    CurCode = Sm.DrStr(dr, c[1]),
                                    ExcRate = Sm.DrDec(dr, c[2]),
                                    Amt = Sm.DrDec(dr, c[3])
                                });
                            }
                        }
                        dr.Close();
                    }

                    if (lR.Count > 0)
                    {
                        for (int i = 0; i < lR.Count; i++)
                        {
                            cml.Add(UpdateSummary2(ref lR, i));

                        }
                    }

                    cml.Add(SaveCustomerDeposit(TxtDocNo.Text));
                }
                //cml.Add(UpdateDOCtProcessInd(TxtDocNo.Text, "Y"));
                if (mIsAutoJournalActived) cml.Add(SaveJournal2());
            }
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                (!mIsSalesInvoiceTaxInvDocEditable && !mIsSalesInvoice8TaxInvDocumentAbleToEdit && IsDocumentNotCancelled()) ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToIncomingPayment() ||
                (ChkCancelInd.Checked && IsVoucherRequestPPNExisted()) ||
                (ChkCancelInd.Checked && IsVATSettlementExisted()) ||
                (!ChkCancelInd.Checked && IsSalesInvoice8TaxInvDocumentNotAbleToEdit(true))
                ;
        }

        private bool IsSalesInvoice8TaxInvDocumentNotAbleToEdit(bool IsShowWarningMsg)
        {
            if (!mIsSalesInvoice8TaxInvDocumentAbleToEdit) return false;

            if (Sm.IsDataExist(
                "Select 1 From TblSalesInvoiceHdr " +
                "Where TaxInvDocument Is Not Null " +
                "And PrevTaxInvDocument Is Not Null " +
                "And TaxInvDocument<>PrevTaxInvDocument " +
                "And DocNo=@Param Limit 1;",
                TxtDocNo.Text))
            {
                if (IsShowWarningMsg)
                    Sm.StdMsg(mMsgType.Warning, "You can't edit tax invoice#.");
                return true;
            }

            return false;
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist("Select 1 From TblSalesInvoiceHdr Where CancelInd='Y' And DocNo=@Param Limit 1;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyProcessedToIncomingPayment()
        {
            if (Sm.IsDataExist("Select 1 From TblSalesInvoiceHdr Where ProcessInd<>'O' And DocNo=@Param Limit 1;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to incoming payment.");
                return true;
            }
            return false;
        }

        private bool IsVoucherRequestPPNExisted()
        {
            if (Sm.IsDataExist(
                "SELECT 1 FROM TblSalesInvoiceHdr " +
                "WHERE DocNo=@Param AND VoucherRequestPPNDocNo IS NOT NULL;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Voucher Request VAT.");
                return true;
            }
            return false;
        }

        private bool IsVATSettlementExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblSalesInvoiceHdr " +
                "Where DocNo=@Param And VATSettlementDocNo Is Not Null;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to VAT settlement.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditSalesInvoiceHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
            if (ChkCancelInd.Checked)
                SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, ");
            if (mIsSalesInvoice8TaxInvDocumentAbleToEdit)
            {
                if (!ChkCancelInd.Checked)
                    SQL.AppendLine("    TaxInvDocument=@TaxInvDocument, PrevTaxInvDocument=Case When PrevTaxInvDocument Is Null Then @TaxInvDocument Else PrevTaxInvDocument End, ");
            }
            else
            {
                if (mIsSalesInvoiceTaxInvDocEditable)
                {
                    if (!ChkCancelInd.Checked)
                        SQL.AppendLine("    TaxInvDocument=@TaxInvDocument, PrevTaxInvDocument=@TaxInvDocument, ");
                }
            }
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvDocument", TxtTaxInvDocument.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo From TblSalesInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo is Not Null ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, ");
            SQL.AppendLine("EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo From TblSalesInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo is Not Null ");
            SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            else
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSalesInvoiceHdr(DocNo);
                ShowSCDtl(DocNo);
                ShowSalesInvoiceDtl2(DocNo);
                ComputeAmt();
                ComputeAmtFromTax();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowSalesInvoice(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                Sm.SetDteCurrentDate(DteDocDt);
                ShowSalesInvoiceHdr2(DocNo);
                ShowSalesInvoiceDtl12(DocNo);
                ShowSalesInvoiceDtl22(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesInvoiceHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("A.CtCode, C.CtCtName, A.DueDt, A.LocalDocNo, A.TaxInvDocument, ");
            SQL.AppendLine("A.CurCode, A.TotalAmt, A.TotalTax, A.DownPayment, A.Amt, ");
            SQL.AppendLine("A.BankAcCode, A.SalesName, A.TaxCode1, A.TaxCode2, A.TaxCode3, A.TaxInvDt, ");
            SQL.AppendLine("A.Remark, A.JournalDocNo, A.JournalDocNo2, A.AdditionalCostDiscAmt, A.SiteCode, A.SODocNo, A.DeptCode ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Left Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left Join TblCustomerCategory C On B.CtCtCode=C.CtCtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelReason", "CtCode", "DueDt", "LocalDocNo", 
                        
                        //6-10
                        "TaxInvDocument", "CurCode", "TotalAmt", "TotalTax", "DownPayment",  
                        
                        //11-14
                        "Amt", "BankAcCode", "SalesName", "TaxCode1", "TaxCode2", 
                        
                        //16-20
                        "TaxCode3", "TaxInvDt", "CancelInd", "Remark", "JournalDocNo", 

                        //21-25
                        "JournalDocNo2", "AdditionalCostDiscAmt", "SiteCode", "SODocNo", "DeptCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[3]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[4]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        TxtTaxInvDocument.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[7]));
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtTotalTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtDownpayment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                        Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Where SPname  = '" + Sm.DrStr(dr, c[13]) + "'"));
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[12]));
                        Sl.SetLueTaxCode(ref LueTaxCode1);
                        Sl.SetLueTaxCode(ref LueTaxCode2);
                        Sl.SetLueTaxCode(ref LueTaxCode3);
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[14]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[16]));
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[17]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[18]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[19]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[20]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[21]);
                        mAdditionalCostDiscAmt = Sm.DrDec(dr, c[22]);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[23]));
                        TxtSCDocNo.EditValue = Sm.DrStr(dr, c[24]);
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[25]));
                    }, true
                );

        }

        internal void ShowSalesInvoiceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.Dno, B.Doctype, B.DoctDocno, B.DOCtDno, D.DocDt, B.ItCode, ");
            SQL.AppendLine("C.ItName, B.Qty, C.SalesUomCode As PriceUomCode, B.UpriceBeforeTax, ");
            SQL.AppendLine("B.DocType, B.UPriceAfterTax, C.ItCodeInternal, E.CurCode, F.DeliveryDt, F.ProductName, F.Remark ");
            SQL.AppendLine("From TblSalesInvoicehdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblSalesContract D On B.DOCtDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblSalesMemoHdr E On A.SalesMemoDocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblSalesMemoDtl F On F.DocNo = E.DocNo ");
            SQL.AppendLine("Where B.DocType = '3' And A.DocNo=@DocNo; ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "DOCtDocNo", "DOCtDNo", "DocDt", "ItCode", "ItName", 
                    
                    //6-10
                    "ProductName", "LocalCode", "Qty", "PriceUomCode", "UPriceBeforeTax", 
                    
                    //11-15
                    "DocType", "UPriceAfterTax", "CurCode", "DeliveryDt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Grd.Cells[Row, 13].Value = dr.GetDecimal(c[8]) * dr.GetDecimal(c[10]);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Grd.Cells[Row, 15].Value = dr.GetDecimal(c[8]) * dr.GetDecimal(c[12]);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 12, 13, 14, 15 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSalesInvoiceDtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, A.OptAcDesc, C.OptDesc, A.Remark ");
            SQL.AppendLine("From TblSalesInvoiceDtl2 A ");
            SQL.AppendLine("Inner Join TblCOA B ");
            SQL.AppendLine("Left Join TblOption C On A.OptAcDesc = C.OptCode And OptCat='AccountDescriptionOnSalesInvoice'  ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "AcInd", "DAmt", "CAmt", "OptAcDesc",
                    "OptDesc", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);

                    if (Sm.GetGrdBool(Grd3, Row, 10).ToString() == "True")
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        }
                        else
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 2);
                        }
                    }
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowSalesInvoiceHdr2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, ReceiptNo, DocDt, CancelReason, CancelInd, ");
            SQL.AppendLine("CtCode, DueDt, LocalDocNo, TaxInvDocument, ");
            SQL.AppendLine("CurCode, TotalAmt, TotalTax, DownPayment, Amt, ");
            SQL.AppendLine("BankAcCode, SalesName, TaxCode1, TaxCode2, TaxCode3, TaxInvDt, TaxInd, ");
            SQL.AppendLine("Remark, JournalDocNo, JournalDocNo2  ");
            SQL.AppendLine("From TblSalesInvoiceHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "ReceiptNo", "DocDt", "CancelReason", "CtCode", "DueDt", 
                        
                        //6-10
                        "LocalDocNo", "TaxInvDocument", "CurCode", "TotalAmt", "TotalTax", 
                        
                        //11-14
                        "DownPayment",  "Amt", "BankAcCode", "SalesName", "TaxCode1", 
                        
                        //16-20
                        "TaxCode2", "TaxCode3", "TaxInvDt", "CancelInd", "Remark",

                        //21-22
                        "JournalDocNo", "JournalDocNo2", "TaxInd"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {

                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[4]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[5]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        TxtTaxInvDocument.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[8]));
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtTotalTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        TxtDownpayment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Where SPname  = '" + Sm.DrStr(dr, c[14]) + "'"));
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[13]));
                        Sl.SetLueTaxCode(ref LueTaxCode1);
                        Sl.SetLueTaxCode(ref LueTaxCode2);
                        Sl.SetLueTaxCode(ref LueTaxCode3);
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[17]));
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[18]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[19]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                    }, true
                );

        }

        private void ShowSalesInvoiceDtl12(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.Dno, B.Doctype, B.DoctDocno, B.DOCtDno, D.DocDt, B.ItCode, ");
            SQL.AppendLine("C.ItName, B.Qty QtyPackagingUnit, C.SalesuomCode As PackagingUnitUomCode, ");
            SQL.AppendLine("B.Qty, C.SalesUomCode As PriceUomCode, B.UpriceBeforeTax, ");
            SQL.AppendLine("B.TaxRate, B.TaxAmt, B.UPriceAfterTax, D.LocalDocNo ");
            SQL.AppendLine("From TblSalesInvoicehdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblDoct2Hdr D On B.DOCtDocNo = D.DocNo ");
            SQL.AppendLine("Where B.DocType = '3' And A.DocNo=@DocNo; ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "DOCtDocNo", "DOCtDNo", "DocDt", "ItCode", "ItName", 
                    
                    //6-10
                    "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", "PriceUomCode", "UPriceBeforeTax", 
                    
                    //11-15
                    "TaxRate", "TaxAmt", "UPriceAfterTax", "DocType", "LocalDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 9);
                    Grd.Cells[Row, 22].Value = Sm.GetLue(LueCurCode);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 13);
                    if (mIsDOCtAmtRounded)
                        Grd.Cells[Row, 27].Value = decimal.Truncate(dr.GetDecimal(c[8]) * dr.GetDecimal(c[13]));
                    else
                        Grd.Cells[Row, 27].Value = dr.GetDecimal(c[8]) * dr.GetDecimal(c[13]);
                    Grd.Cells[Row, 28].Value = 0m;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 15);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 18, 20, 23, 24, 25, 26, 27, 28 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSalesInvoiceDtl22(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, A.OptAcDesc, C.OptDesc, A.Remark ");
            SQL.AppendLine("From TblSalesInvoiceDtl2 A ");
            SQL.AppendLine("Inner Join TblCOA B ");
            SQL.AppendLine("Left Join TblOption C On A.OptAcDesc = C.OptCode And OptCat='AccountDescriptionOnSalesInvoice'  ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "AcInd", "DAmt", "CAmt", "OptAcDesc",
                    "OptDesc", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);

                    if (Sm.GetGrdBool(Grd3, Row, 10).ToString() == "True")
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        }
                        else
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 2);
                        }
                    }
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowCustomerDepositSummary(string CtCode)
        {
            ClearGrd2();

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select CurCode, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary ");
            SQL.AppendLine("Where CtCode=@CtCode Order By CurCode;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        internal void ShowSCDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.DNo, C.ItCode, C.ProductName, C.ItCodeInternal, C.ItName, ");
            SQL.AppendLine("C.Qty, C.PurchaseUomCode, C.UPrice UPriceBefTax, C.UPriceAfterTax, ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("J.Remark, J.DocType, ");
            else
                SQL.AppendLine("Null as Remark, Null as DocType, ");
            SQL.AppendLine("B.CurCode, C.DeliveryDt ");
            SQL.AppendLine("From TblSalesContract A ");
            SQL.AppendLine("Inner Join TblSalesMemoHdr B On A.SalesMemoDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    SELECT A.DocNo, A.DNo, A.ItCode, A.ProductName, B.ItName, B.ItCodeInternal, ");
            SQL.AppendLine("    A.Qty, B.PurchaseUomCode, A.UPrice, A.UPriceAfterTax, A.DeliveryDt ");
            SQL.AppendLine("    FROM tblsalesmemodtl A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine(")C ON B.DocNo = C.DocNo ");
            if (TxtDocNo.Text.Length > 0)
            {
                SQL.AppendLine("   Inner Join TblSalesInvoiceDtl J On A.DocNo = J.DoCtDocNo And C.Dno = J.DOCtDno ");
                SQL.AppendLine("   Where J.DocNo = @DocNo  ");
            }
            else
            {
                SQL.AppendLine("   Where A.DocNo = @DocNo  ");
                SQL.AppendLine("   And Concat(A.DocNo, C.Dno) Not In (Select Concat(DOctDocNo, DOCtDno) As keyDocNo From TblSalesInvoicehdr A Inner Join TblSalesInvoiceDtl B On A.DocNo=B.DocNo Where A.SODocNo Is Not Null And CancelInd = 'N') ");
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "DocNo", "DNo", "DocDt", "ItCode", "ItName", 
                    
                    //6-10
                    "ProductName", "ItCodeInternal", "Qty", "PurchaseUomCode", "UPriceBefTax", 
                    
                    //11-15
                    "DocType", "UPriceAfterTax", "CurCode", "DeliveryDt", "Remark"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Grd.Cells[Row, 13].Value = dr.GetDecimal(c[8]) * dr.GetDecimal(c[10]);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Grd.Cells[Row, 15].Value = dr.GetDecimal(c[8]) * dr.GetDecimal(c[12]);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0, 12, 13, 14, 15 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                if (SiteCode.Length == 0)
                    SQL.AppendLine("Where T.ActInd='Y' ");
                else
                    SQL.AppendLine("Where T.SiteCode=@SiteCode ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private MySqlCommand SaveExcRate(ref List<Rate> lR, int i, string DocNo, decimal DP)
        {
            var SQL1 = new StringBuilder();

            decimal x = ((DP - lR[i].Amt) >= 0) ? lR[i].Amt : DP;

            SQL1.AppendLine("Select @DP:=(Case @CancelInd When 'Y' Then -1 Else 1 End * " + x + "); ");

            SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt-@DP ");
            SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            SQL1.AppendLine("Insert Into TblSalesInvoiceDtl3(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @DocNo, @CurCode, @ExcRate, @DP, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt-@DP, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
            Sm.CmParam<String>(ref cm1, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand UpdateSummary2(ref List<Rate> lR, int i)
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt + @Amt, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
            Sm.CmParam<Decimal>(ref cm1, "@Amt", lR[i].Amt);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }


        internal void ComputeAmtFromTax()
        {
            ComputeAmt();
            //ComputeTotalWithTax(); // remark by wed. kalkulasi tax masuk ke ComputeAmt() dengan method baru ComputeTax(Amt), dimiripkan dengan SalesInvoice ComputeAmt()
        }

        //private void ComputeTotalWithTax() // remark by wed. kalkulasi tax masuk ke ComputeAmt() dengan method baru ComputeTax(Amt), dimiripkan dengan SalesInvoice ComputeAmt()
        //{
        //    decimal TotalWithoutTax = 0m, DownPayment = 0m, TaxAmt1 = 0m, TaxAmt2 = 0m, TaxAmt3 = 0m, TotalTaxAmt1 = 0m;

        //    Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, TxtTotalTax }, 0);

        //    if (TxtTotalAmt.Text.Length != 0) TotalWithoutTax = decimal.Parse(TxtTotalAmt.Text);

        //    if (Sm.GetLue(LueTaxCode1).Length != 0)
        //    {
        //        TaxAmt1 = GetTaxRate(Sm.GetLue(LueTaxCode1)) * 0.01m * TotalWithoutTax;
        //        if (mIsDOCtAmtRounded) TaxAmt1 = decimal.Truncate(TaxAmt1);
        //        if (TaxAmt1 > 0)
        //        {
        //            //TxtTaxAmt1.EditValue = Sm.FormatNum(mSITaxRoundingDown == true ? Math.Floor(TaxAmt1) : TaxAmt1, 0);
        //            //TotalTaxAmt1 = mSITaxRoundingDown == true ? Math.Floor(TaxAmt1) : TaxAmt1;

        //            TxtTaxAmt1.EditValue = Sm.FormatNum(TaxAmt1, 0);
        //            TotalTaxAmt1 = TaxAmt1;

        //        }
        //        else
        //        {
        //            TaxAmt1 = -1 * TaxAmt1;
        //            TxtTaxAmt1.EditValue = Sm.FormatNum(-1 * TaxAmt1, 0);
        //            TotalTaxAmt1 = -1 * TaxAmt1;
        //        }
        //    }

        //    if (Sm.GetLue(LueTaxCode2).Length != 0)
        //    {
        //        TaxAmt2 = GetTaxRate(Sm.GetLue(LueTaxCode2))*0.01m*TotalWithoutTax;
        //        if (mIsDOCtAmtRounded) TaxAmt2 = decimal.Truncate(TaxAmt2);
        //        if (TaxAmt2 > 0)
        //        {
        //            TxtTaxAmt2.EditValue = Sm.FormatNum(TaxAmt2, 0);
        //            TotalTaxAmt1 += TaxAmt2;
        //        }
        //        else
        //        {
        //            TaxAmt2 = -1 * TaxAmt2;
        //            TxtTaxAmt2.EditValue = Sm.FormatNum(-1*TaxAmt2, 0);
        //            TotalTaxAmt1 += (-1 * TaxAmt2);
        //        }
        //    }

        //    if (Sm.GetLue(LueTaxCode3).Length != 0)
        //    {
        //        TaxAmt3 = GetTaxRate(Sm.GetLue(LueTaxCode3))*0.01m*TotalWithoutTax;
        //        if (mIsDOCtAmtRounded) TaxAmt3 = decimal.Truncate(TaxAmt3);
        //        if (TaxAmt3 > 0)
        //        {
        //            TxtTaxAmt3.EditValue = Sm.FormatNum(TaxAmt3, 0);
        //            TotalTaxAmt1 += TaxAmt3;
        //        }
        //        else
        //        {
        //            TaxAmt3 = -1 * TaxAmt3;
        //            TxtTaxAmt3.EditValue = Sm.FormatNum(-1 * TaxAmt3, 0);
        //            TotalTaxAmt1 += (-1 * TaxAmt3);
        //        }
        //    }
        //    if (mIsDOCtAmtRounded) TotalTaxAmt1 = decimal.Truncate(TotalTaxAmt1);
        //    TxtTotalTax.EditValue = Sm.FormatNum(TotalTaxAmt1, 0);

        //    if (TxtDownpayment.Text.Length != 0) DownPayment = decimal.Parse(TxtDownpayment.Text);
        //    if (mIsDOCtAmtRounded) DownPayment = decimal.Truncate(DownPayment);

        //    if (mIsDOCtAmtRounded)
        //        TxtAmt.EditValue = Sm.FormatNum(decimal.Truncate(TotalWithoutTax + TotalTaxAmt1 - DownPayment), 0);
        //    else
        //        TxtAmt.EditValue = Sm.FormatNum(TotalWithoutTax + TotalTaxAmt1 - DownPayment, 0);

        //    if (mSLI3TaxCalculationFormat != "1") // SRN
        //    {
        //        if (mIsDOCtAmtRounded)
        //            TxtAmt.EditValue = Sm.FormatNum(Decimal.Truncate((Decimal.Parse(TxtAmt.Text) + mAdditionalCostDiscAmt)), 0);
        //        else
        //            TxtAmt.EditValue = Sm.FormatNum((Decimal.Parse(TxtAmt.Text) + mAdditionalCostDiscAmt), 0);
        //    }
        //}

        private void ComputeTax(decimal Amt)
        {
            string
                TaxCode1 = Sm.GetLue(LueTaxCode1),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3);
            decimal
                TaxAmt1 = 0m,
                TaxAmt2 = 0m,
                TaxAmt3 = 0m;

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3 }, 0);

            if (TaxCode1.Length != 0) TaxAmt1 = GetTaxRate(TaxCode1) * 0.01m * Amt;
            if (TaxCode2.Length != 0) TaxAmt2 = GetTaxRate(TaxCode2) * 0.01m * Amt;
            if (TaxCode3.Length != 0) TaxAmt3 = GetTaxRate(TaxCode3) * 0.01m * Amt;

            if (mIsDOCtAmtRounded)
            {
                TaxAmt1 = Decimal.Truncate(TaxAmt1);
                TaxAmt2 = Decimal.Truncate(TaxAmt2);
                TaxAmt3 = Decimal.Truncate(TaxAmt3);
            }

            TxtTaxAmt1.Text = Sm.FormatNum(TaxAmt1, 0);
            TxtTaxAmt2.Text = Sm.FormatNum(TaxAmt2, 0);
            TxtTaxAmt3.Text = Sm.FormatNum(TaxAmt3, 0);
        }

        internal void ComputeAmt()
        {
            decimal COAAmt = 0m, Amt = 0m, TotalTax = 0m, Downpayment = 0m;

            if (TxtDownpayment.Text.Length > 0) Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (mSLI3TaxCalculationFormat == "1")
            {
                #region Default
                var CustomerAcNoAR =
                Sm.GetValue(
                        "Select (Select Concat(ParValue, A.CtCode) From TblParameter Where ParCode='CustomerAcNoAR' Limit 1) As AcNo " +
                        "From TblCustomer A, TblCustomerCategory B " +
                        "Where A.CtCtCode is Not Null " +
                        "And A.CtCtCode=B.CtCtCode " +
                        "And A.CtCode='" + Sm.GetLue(LueCtCode) + "' Limit 1; "
                        );
                //if (CustomerAcNoAR.Length > 0)
                //{
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    //if (Sm.CompareStr(CustomerAcNoAR, Sm.GetGrdStr(Grd3, Row, 1)))
                    //{
                    string AcType = Sm.GetValue(
                        "Select AcType From TblCoa Where AcNo = '" + Sm.GetGrdStr(Grd3, Row, 1) + "';");
                    if (Sm.GetGrdBool(Grd3, Row, 3))
                    {
                        if (AcType == "D")
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                    }
                    if (Sm.GetGrdBool(Grd3, Row, 5))
                    {
                        if (AcType == "C")
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 6);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 6);
                    }
                }
                #endregion
            }
            else //SRN, pilihan additional cost & discount nya ada yg masuk hitungan atau enggak
            {
                #region SRN

                mAdditionalCostDiscAmt = 0m;
                string CtCode = Sm.GetLue(LueCtCode);

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    string AcNo = Sm.GetGrdStr(Grd3, Row, 1);
                    if (!AcNo.Contains(string.Concat(".", CtCode))) // kalau bukan COA customer
                    {
                        //kalau dicentang di debit, (-)
                        if (Sm.GetGrdBool(Grd3, Row, 3))
                        {
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                        }
                        //kalau dicentang di kredit, (+)
                        if (Sm.GetGrdBool(Grd3, Row, 5))
                        {
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 6);
                        }

                        //kalau ga di centang baik debit dan kredit, masuk ke AdditionalCostDiscAmt
                        if (!Sm.GetGrdBool(Grd3, Row, 3) && !Sm.GetGrdBool(Grd3, Row, 5))
                        {
                            mAdditionalCostDiscAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                            mAdditionalCostDiscAmt += Sm.GetGrdDec(Grd3, Row, 6);
                        }
                    }
                }

                #endregion
            }

            Amt = COAAmt;

            if (mIsSalesInvoice8TaxEnabled)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    decimal Qty = Sm.GetGrdDec(Grd1, Row, 10);
                    decimal UPriceBefTax = Sm.GetGrdDec(Grd1, Row, 12);

                    Amt += (Qty * UPriceBefTax);
                }

                if (mSLI3TaxCalculationFormat == "1")
                {
                    if (mIsSalesInvoice8COANonTaxable)
                        ComputeTax(Amt - COAAmt);
                    else
                        ComputeTax(Amt);
                }
                else
                    ComputeTax(Amt);

                TotalTax = Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text);

                TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
                TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
                TxtAmt.EditValue = Sm.FormatNum(Amt + TotalTax - Downpayment, 0);
            }
            else
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 13).Length > 0)
                    {
                        decimal Amount = Sm.GetGrdDec(Grd1, Row, 13);
                        Amt += Amount;
                    }
                }

                //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                //{
                //    if (Sm.GetGrdStr(Grd1, Row, 25).Length > 0)
                //    {
                //        decimal Qty = Sm.GetGrdDec(Grd1, Row, 20);
                //        decimal TaxAmt = Sm.GetGrdDec(Grd1, Row, 25);
                //        TotalTax += (Qty * TaxAmt);
                //    }
                //}
                ComputeTax(Amt);
                TotalTax = Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text);

                if (mIsDOCtAmtRounded)
                {
                    Amt = decimal.Truncate(Amt);
                    TotalTax = decimal.Truncate(TotalTax);
                    Downpayment = decimal.Truncate(Downpayment);
                }
                TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
                TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
                TxtAmt.EditValue = Sm.FormatNum(Amt + TotalTax - Downpayment, 0);
            }

            if (mSLI3TaxCalculationFormat != "1")
            {
                // ditambahkan nilai additional cost discount yg ga dicentang
                TxtAmt.EditValue = Sm.FormatNum((Decimal.Parse(TxtAmt.Text) + mAdditionalCostDiscAmt), 0);
            }
        }

        internal decimal ComputeTotalAmt()
        {
            decimal TotalAmt = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                TotalAmt += Sm.GetGrdDec(Grd1, Row, 13);
            }
            return TotalAmt;

        }

        private decimal GetTaxRate(string TaxCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select TaxRate from TblTax Where TaxCode=@TaxCode;"
            };
            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);
            string TaxRate = Sm.GetValue(cm);

            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }

        private void GetParameter()
        {
            mIsCustomerItemNameMandatory = Sm.GetParameterBoo("IsCustomerItemNameMandatory");
            mIsRemarkForJournalMandatory = Sm.GetParameterBoo("IsRemarkForJournalMandatory");
            mDueDtSIValue = Sm.GetParameter("DueDtSIValue");
            mTaxCodeSIValue = Sm.GetParameter("TaxCodeSIValue");
            mEmpCodeSI = Sm.GetParameter("EmpCodeSI");
            mEmpCodeTaxCollector = Sm.GetParameter("EmpCodeTaxCollector");
            mSITaxRoundingDown = Sm.GetParameterBoo("SITaxRoundingDown");
            mIsSalesInvoiceTaxInvDocEditable = Sm.GetParameterBoo("IsSalesInvoiceTaxInvDocEditable");
            mSalesBasedDOQtyIndex = Sm.GetParameter("SalesBasedDOQtyIndex");
            mDocNoFormat = Sm.GetParameter("DocNoFormat");
            mIsSLI3UseAutoFacturNumber = Sm.GetParameterBoo("IsSLI3UseAutoFacturNumber");
            mSLI3TaxCalculationFormat = Sm.GetParameter("SLI3TaxCalculationFormat");
            mIsSalesInvoice8TaxEnabled = Sm.GetParameterBoo("IsSalesInvoice8TaxEnabled");
            mIsSalesInvoice8COANonTaxable = Sm.GetParameterBoo("IsSalesInvoice8COANonTaxable");
            mIsFilterByCtCt = Sm.GetParameterBoo("IsFilterByCtCt");
            if (mSLI3TaxCalculationFormat.Length == 0) mSLI3TaxCalculationFormat = "1";
            if (mSalesBasedDOQtyIndex.Length <= 0) mSalesBasedDOQtyIndex = "2";
            
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'IsCheckCOAJournalNotExists', 'MainCurCode', 'IsDOCtAmtRounded', 'IsFilterBySite', ");
            SQL.AppendLine("'IsSalesMemoGenerateLocalDocNo', 'IsCustomerComboShowCategory', 'IsSalesInvoice8TaxInvDocumentAbleToEdit' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsCustomerComboShowCategory": mIsCustomerComboShowCategory = ParValue == "Y"; break;
                            case "IsSalesMemoGenerateLocalDocNo": mIsSalesMemoGenerateLocalDocNo = ParValue == "Y"; break;
                            case "IsSalesInvoice8TaxInvDocumentAbleToEdit": mIsSalesInvoice8TaxInvDocumentAbleToEdit = ParValue == "Y"; break;

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union ALL Select 'All' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void LueRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.LookUpEdit Lue,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();

                if (CtCode.Length == 0)
                {
                    SQL.AppendLine("Select Distinct B.CtCode As Col1, ");
                    if (mIsCustomerComboShowCategory)
                        SQL.AppendLine("Concat(C.Ctname, ' [',D.CtCtName,']') As Col2 ");
                    else
                        SQL.AppendLine("C.Ctname As Col2 ");
                    SQL.AppendLine("From TblSalesContract A  ");
                    SQL.AppendLine("Inner Join TblSalesMemoHdr B On A.SalesMemoDocNo=B.DocNo And B.CurCode Is Not Null ");
                    SQL.AppendLine("Inner Join TblSalesMemoDtl B2 On B.DocNo = B2.DocNo ");
                    SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode And C.ActInd='Y' ");
                    if (mIsFilterByCtCt)
                    {
                        SQL.AppendLine("And (C.CtCtCode Is Null Or (C.CtCtCode Is Not Null And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                        SQL.AppendLine("    Where CtCtCode=C.CtCtCode ");
                        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ))) ");
                    }
                    SQL.AppendLine("Inner Join TblCustomerCategory D On C.CtCtCode = D.CtCtCode ");
                    SQL.AppendLine("Where Not Exists( ");
                    SQL.AppendLine("    Select 1 ");
                    SQL.AppendLine("    From TblSalesInvoiceHdr T1, TblSalesInvoiceDtl T2 ");
                    SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("    And T1.CancelInd='N' ");
                    SQL.AppendLine("    And T2.DOCtDocNo=A.DocNo ");
                    SQL.AppendLine("    And T2.DOCtDNo=B2.DNo ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("And A.PtCode = 'CBD' ");
                    SQL.AppendLine("And A.Status = 'O' And A.CancelInd = 'N' ");
                    SQL.AppendLine("Order By C.CtName; ");
                }
                else
                {
                    SQL.AppendLine("Select A.CtCode As Col1, ");
                    if (mIsCustomerComboShowCategory)
                        SQL.AppendLine("Concat(A.CtName, ' [',B.CtCtName,']') As Col2");
                    else
                        SQL.AppendLine("A.CtName As Col2 ");
                    SQL.AppendLine("From TblCustomer A ");
                    SQL.AppendLine("Inner Join TblCustomerCategory B On A.CtCtCode = B.CtCtCode ");
                    SQL.AppendLine("Where CtCode=@CtCode; ");
                }

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (CtCode.Length > 0)
                    Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
                else
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");

                if (CtCode.Length > 0) Sm.SetLue(LueCtCode, CtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueOptionCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='AccountDescriptionOnSalesInvoice' ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };



        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        internal string GenerateReceipt(string DocDt)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblSalesInvoiceHdr ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoiceHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As ReceiptNo");

            return Sm.GetValue(SQL.ToString());
        }

        //internal string GenerateReceipt(string DocDt)
        //{
        //    string
        //        Yr = DocDt.Substring(2, 2),
        //        Mth = DocDt.Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle");
        //    //  DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

        //    var SQL = new StringBuilder();

        //    SQL.Append("Select Concat( ");
        //    SQL.Append("IfNull(( ");
        //    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
        //    SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoiceHdr ");
        //    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
        //    SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
        //    SQL.Append("       ) As Temp ");
        //    SQL.Append("   ), '0001') ");
        //    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
        //    SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
        //    SQL.Append(") As ReceiptNo");

        //    return Sm.GetValue(SQL.ToString());
        //}

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("Doctitle");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            {
                var l = new List<InvoiceHdr2>();
                var ldtl = new List<InvoiceDtl>();
                var lC = new List<AddCost>();
                var lCD = new List<AddCostDisc>();
                string[] TableName = { "InvoiceHdr2", "InvoiceDtl", "AddCostDisc" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header KSM
                var cm1 = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyPhone2', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation1') As 'CompLocation1', ");

                SQL.AppendLine("A.DocNo, ");

                SQL.AppendLine("Concat(Right(A.DocDt,2),' ',Case substring(A.DocDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
                SQL.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
                SQL.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DocDt,4))As DocDt, ");

                SQL.AppendLine("A.LocalDocNo, ");

                SQL.AppendLine("Concat(Right(A.DueDt,2),' ',Case substring(A.DueDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
                SQL.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
                SQL.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DueDt,4))As DueDt, ");

                SQL.AppendLine("A.CurCode, A.TotalTax, A.TotalAmt, B.CtName, D.Address, ");

                SQL.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1, ");
                SQL.AppendLine("IfNull(A.TaxCode2, null) As TaxCode2, ");
                SQL.AppendLine("IfNull(A.TaxCode3, null) As TaxCode3, ");
                SQL.AppendLine("F.TaxName TaxName1, G.TaxName TaxName2, H.TaxName TaxName3, ");
                SQL.AppendLine("F.TaxRate TaxRate1, G.TaxRate TaxRate2, H.TaxRate TaxRate3, ");

                SQL.AppendLine("A.Amt, ");

                SQL.AppendLine("(Select Parvalue From TblParameter Where Parcode = 'SLI9SpinningSign') As SignSpinning, ");
                SQL.AppendLine("(Select Parvalue From TblParameter Where Parcode = 'SLI9WeavingSign') As SignWeaving, ");

                SQL.AppendLine("A.TaxInvDocument ");

                SQL.AppendLine("From TblSalesInvoiceHdr A ");
                SQL.AppendLine("Left Join TblCustomer B On A.CtCode=B.CtCode ");
                SQL.AppendLine("Left Join TblCustomerCategory C On B.CtCtCode=C.CtCtCode ");
                SQL.AppendLine("Left Join TblCustomer D On D.CtCode = A.CtCode ");
                SQL.AppendLine("Left Join TblTax F On A.TaxCode1 = F.TaxCode ");
                SQL.AppendLine("Left Join TblTax G On A.TaxCode2 = G.TaxCode ");
                SQL.AppendLine("Left Join TblTax H On A.TaxCode3 = H.TaxCode ");

                SQL.AppendLine("Where A.DocNo=@DocNo; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm1.Connection = cn;
                    cm1.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm1, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm1.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    {
                    //0
                     "CompanyLogo",

                     //1-5
                     "CompanyName",
                     "CompanyAddress",
                     "CompanyAddressFull",
                     "CompanyPhone",
                     "CompanyPhone2",

                     //6-10
                     "CompLocation1",
                     "DocNo",
                     "DocDt",
                     "LocalDocNo", 
                     "DueDt",

                     //11-15
                     "CurCode",
                     "TotalTax",
                     "TotalAmt",
                     "CtName",
                     "Address",

                     //16-20
                     "TaxCode1",
                     "TaxCode2",
                     "TaxCode3",
                     "TaxName1",
                     "TaxName2",

                     //21-25
                     "Taxname3",
                     "TaxRate1",
                     "TaxRate2",
                     "TaxRate3",
                     "Amt",

                     //27
                     "SignSpinning",
                     "SignWeaving",
                     "TaxInvDocument"
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new InvoiceHdr2()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressFull = Sm.DrStr(dr, c[3]),
                                CompanyPhone = (Sm.GetLue(LueSiteCode) == "001") ? Sm.DrStr(dr, c[5]) : Sm.DrStr(dr, c[4]),
                                CompLocation1 = Sm.DrStr(dr, c[6]),

                                DocNo = Sm.DrStr(dr, c[7]),
                                DocDt = Sm.DrStr(dr, c[8]),
                                LocalDocNo = Sm.DrStr(dr, c[9]),
                                DueDt = Sm.DrStr(dr, c[10]),
                                CurCode = Sm.DrStr(dr, c[11]),
                                TotalTax = Sm.DrDec(dr, c[12]),

                                TotalAmt = Sm.DrDec(dr, c[13]),
                                CtName = Sm.DrStr(dr, c[14]),
                                Address = Sm.DrStr(dr, c[15]),
                                TaxCode1 = Sm.GetLue(LueTaxCode1), //Sm.DrStr(dr, c[15]),
                                TaxCode2 = Sm.GetLue(LueTaxCode2), //Sm.DrStr(dr, c[16]),
                                TaxCode3 = Sm.GetLue(LueTaxCode3), //Sm.DrStr(dr, c[17]),

                                TaxName1 = Sm.GetValue("Select TaxName From TblTax Where TaxCode = @Param", Sm.GetLue(LueTaxCode1)), //Sm.DrStr(dr, c[18]),
                                TaxName2 = Sm.GetValue("Select TaxName From TblTax Where TaxCode = @Param", Sm.GetLue(LueTaxCode2)), //Sm.DrStr(dr, c[19]),
                                TaxName3 = Sm.GetValue("Select TaxName From TblTax Where TaxCode = @Param", Sm.GetLue(LueTaxCode3)), //Sm.DrStr(dr, c[20]),
                                TaxRate1 = Sm.DrDec(dr, c[22]),
                                TaxRate2 = Sm.DrDec(dr, c[23]),

                                TaxRate3 = Sm.DrDec(dr, c[24]),
                                Amt = Decimal.Parse(TxtAmt.Text), //Sm.DrDec(dr2, c2[27]),
                                TotalAmt2 = ComputeTotalAmt(),
                                TaxAmt1 = Decimal.Parse(TxtTaxAmt1.Text),
                                TaxAmt2 = Decimal.Parse(TxtTaxAmt2.Text),

                                TaxAmt3 = Decimal.Parse(TxtTaxAmt3.Text),
                                DownPayment = Decimal.Parse(TxtDownpayment.Text),
                                Total = Decimal.Parse(TxtAmt.Text),  //ComputeTotalAmt() + Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text),
                                //Terbilang = Sm.Terbilang(ComputeTotalAmt() + Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text)), //Sm.Terbilang(Sm.DrDec(dr2, c2[27])),
                                Terbilang = Sm.Terbilang(Decimal.Parse(TxtAmt.Text)),
                                Sign = (Sm.GetLue(LueSiteCode) == "001") ? Sm.DrStr(dr, c[26]) : Sm.DrStr(dr, c[27]),
                                TaxInvDocument = Sm.DrStr(dr, c[28])
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region Detail KSM

                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select A.DocNo, B.ItCode, C.ItName, Sum(B.Qty) As Qty, B.UPriceBeforeTax As UPrice, ");
                    SQLDtl.AppendLine("Sum((B.Qty * B.UPriceBeforeTax)) As Amt, A.LocalDocNo, C.SalesUomCode ");
                    SQLDtl.AppendLine("From TblSalesInvoiceHdr A ");
                    SQLDtl.AppendLine("Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                    SQLDtl.AppendLine("Group By C.ItCode, B.UPriceBeforeTax, C.SalesUomCode ");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[]
                    {
                        //0
                        "ItCode" ,

                        //1-5
                        "ItName" ,
                        "Qty",
                        "UPrice",
                        "Amt",
                        "LocalDocNo",

                        //
                        "SalesUomCode"

                    });

                    if (drDtl.HasRows)
                    {
                        int No = 0;
                        while (drDtl.Read())
                        {
                            No = No + 1;
                            ldtl.Add(new InvoiceDtl()
                            {
                                nomor = No,
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                Qty = Sm.DrDec(drDtl, cDtl[2]),
                                UPrice = Sm.DrDec(drDtl, cDtl[3]),
                                Amt = Sm.DrDec(drDtl, cDtl[4]),
                                LocalDocNo = Sm.DrStr(drDtl, cDtl[5]),
                                PriceUomCode = Sm.DrStr(drDtl, cDtl[6]),
                            });
                        }
                    }

                    drDtl.Close();
                }

                myLists.Add(ldtl);

                #endregion

                #region Additional Cost

                var cmC = new MySqlCommand();
                var SQLC = new StringBuilder();
                using (var cnC = new MySqlConnection(Gv.ConnectionString))
                {
                    cnC.Open();
                    cmC.Connection = cnC;

                    SQLC.AppendLine("    select DocNo, AcNo, DAmt, CAmt from tblsalesinvoicedtl2 ");
                    SQLC.AppendLine("    where acind = 'Y' ");
                    SQLC.AppendLine("    And DocNo = @DocNo ");

                    cmC.CommandText = SQLC.ToString();

                    Sm.CmParam<String>(ref cmC, "@DocNo", TxtDocNo.Text);

                    var drC = cmC.ExecuteReader();
                    var cDtC = Sm.GetOrdinal(drC, new string[] 
                    {
                     //0
                     "DocNo" ,

                     //1-3
                     "AcNo", "DAmt", "CAmt"
                    });

                    decimal mAddCost = 0m;
                    decimal mAddDisc = 0m;

                    if (drC.HasRows)
                    {
                        while (drC.Read())
                        {
                            lC.Add(new AddCost()
                            {
                                DocNo = Sm.DrStr(drC, cDtC[0]),
                                AcNo = Sm.DrStr(drC, cDtC[1]),
                                DAmt = Sm.DrDec(drC, cDtC[2]),
                                CAmt = Sm.DrDec(drC, cDtC[3]),
                            });
                        }
                    }
                    drC.Close();

                    if (lC.Count > 0)
                    {
                        for (int Row = 0; Row < lC.Count; Row++)
                        {
                            string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", lC[Row].AcNo);
                            if (lC[Row].DAmt > 0)
                            {
                                if (AcType == "D")
                                    mAddCost += lC[Row].DAmt;
                                else
                                    mAddDisc -= lC[Row].DAmt;
                            }
                            if (lC[Row].CAmt > 0)
                            {
                                if (AcType == "C")
                                    mAddCost += lC[Row].CAmt;
                                else
                                    mAddDisc -= lC[Row].CAmt;
                            }
                        }

                        lCD.Add(new AddCostDisc()
                        {
                            AddCost = mAddCost,
                            AddDisc = mAddDisc
                        });
                    }

                }
                myLists.Add(lCD);

                #endregion

                if (Doctitle == "KSM")
                {
                    if (Sm.GetLue(LueSiteCode) == "001")
                        Sm.PrintReport("Invoice9KSM_Spinning", myLists, TableName, false);
                    else if (Sm.GetLue(LueSiteCode) == "002")
                        Sm.PrintReport("Invoice9KSM_Weaving", myLists, TableName, false);
                }

            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mDueDtSIValue.Length > 0 && Sm.GetDte(DteDocDt).Length > 0)
                {
                    DteDueDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteDocDt).Substring(0, 8)).AddDays(double.Parse(mDueDtSIValue));
                }
                else
                {
                    DteDueDt.EditValue = null;
                }
            };
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
            ClearGrd();
            ComputeAmtFromTax();
            var CtCode = Sm.GetLue(LueCtCode);
            if (CtCode.Length > 0)
            {
                TxtCtCtCode.EditValue = Sm.GetValue(
                    "Select B.CtCtName From TblCustomer A, TblCustomerCategory B " +
                    "Where A.CtCtCode=B.CtCtCode And A.CtCode=@Param Limit 1;",
                    CtCode);
                ShowCustomerDepositSummary(CtCode);
            }
            //if (Sm.Find_In_Set(Sm.GetLue(LueSiteCode), mPPNSiteCodeForSalesMemo))
            //{
            //    ChkTaxInd.Enabled = true;
            //    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
            //        { 
            //            ChkTaxInd  
            //        }, false);
            //}
            //else
            //{
            //    ChkTaxInd.Checked = false;
            //    ChkTaxInd.Enabled = false;
            //}
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mDueDtSIValue.Length > 0 && Sm.GetDte(DteDocDt).Length > 0)
                    DteDueDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteDocDt).Substring(0, 8)).AddDays(double.Parse(mDueDtSIValue));
                else
                    DteDueDt.EditValue = null;
                ClearGrd();
                ComputeAmtFromTax();
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);
                TxtCtCtCode.EditValue = null;
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length > 0)
                {
                    TxtCtCtCode.EditValue = Sm.GetValue(
                        "Select B.CtCtName From TblCustomer A, TblCustomerCategory B " +
                        "Where A.CtCtCode=B.CtCtCode And A.CtCode=@Param Limit 1;",
                        CtCode);
                    ShowCustomerDepositSummary(CtCode);
                }
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtDownpayment_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDownpayment, 0);
                if (mIsDOCtAmtRounded)
                {
                    decimal Downpayment = decimal.Truncate(decimal.Parse(TxtDownpayment.Text));
                    TxtDownpayment.EditValue = Sm.FormatNum(Downpayment, 0);
                }
                ComputeAmtFromTax();
            }
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void TxtTaxInvDocument_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTaxInvDocument);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue1(SetLueOptionCode));
        }

        private void LueOption_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value =
                    Grd3.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueOption);
                    Grd3.Cells[fCell.RowIndex, 8].Value = LueOption.GetColumnValue("Col2");
                }
                LueOption.Visible = false;
            }
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtFromTax();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtFromTax();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtFromTax();
            }
        }

        #endregion

        #region Button Event

        private void BtnCopySalesInvoice_Click(object sender, EventArgs e)
        {
            //if (BtnSave.Enabled && !Sm.IsLueEmpty(LueCtCode, "Customer"))
            //    Sm.FormShowDialog(new FrmSalesInvoice8Dlg3(this, Sm.GetLue(LueCtCode)));
        }

        private void BtnDueDt_Click(object sender, EventArgs e)
        {
            if (mDueDtSIValue.Length > 0)
            {
                DteDueDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteDocDt).Substring(0, 8)).AddDays(double.Parse(mDueDtSIValue));
            }
            else
            {
                DteDueDt.EditValue = null;
                var DocDt = Sm.GetDte(DteDocDt);
                if (DocDt.Length > 0 && Grd1.Rows.Count > 1)
                {
                    decimal TOP = Sm.GetGrdDec(Grd1, 0, 28);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        {
                            if (TOP > Sm.GetGrdDec(Grd1, Row, 28))
                                TOP = Sm.GetGrdDec(Grd1, Row, 28);
                        }
                    }

                    DteDueDt.EditValue = new DateTime(
                       Int32.Parse(DocDt.Substring(0, 4)),
                       Int32.Parse(DocDt.Substring(4, 2)),
                       Int32.Parse(DocDt.Substring(6, 2)),
                       0, 0, 0).AddDays((double)TOP);

                }
            }
        }

        private void BtnLocalDocNo_Click(object sender, EventArgs e)
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 29).Length > 0)
                {
                    TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 29);
                    break;
                }
            }
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        private void BtnSCDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSalesInvoice9Dlg(this, Sm.GetLue(LueCtCode)));
        }
        #endregion

        #region Grid

        #region Grd1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSalesContract(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSalesContract(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdDec(Grd3, e.RowIndex, 4) > 0)
            {
                Grd3.Cells[e.RowIndex, 6].Value = 0;
            }

            if (e.ColIndex == 6 && Sm.GetGrdDec(Grd3, e.RowIndex, 6) > 0)
            {
                Grd3.Cells[e.RowIndex, 4].Value = 0;
            }

            if (e.ColIndex == 3 || e.ColIndex == 5)
                Grd3.Cells[e.RowIndex, 10].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6 }, e.ColIndex))
            {
                ComputeAmt();
                ComputeAmtFromTax();
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesInvoice9Dlg2(this));
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LueOption, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                SetLueOptionCode(ref LueOption);
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmSalesInvoice9Dlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeAmt();
            ComputeAmtFromTax();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion


        #endregion

        #endregion

        #region Class

        #region Report Class

        private class InvoiceHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public decimal TotalTax { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal DownPayment { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string Remark { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string SACityName { get; set; }
            public string NPWP { get; set; }
            public string RemarkSI { get; set; }
            public string ReceiptNo { get; set; }
            public string IsDOCtAmtRounded { get; set; }
            public string Terbilang2 { get; set; }
            public string CompanyNPWP { get; set; }
            public string CityNameCT { get; set; }
            public string BankName { get; set; }
            public string ReceiptDt { get; set; }
            public decimal AddCost { get; set; }
            public string SalesName { get; set; }
            public string PrintBy { get; set; }
            public string BankAcNm { get; set; }
            public string BankAcNo { get; set; }
            public string LocalDocNo { get; set; }
            public string AddressTTD { get; set; }
            public string SLINotes { get; set; }
            public string SLINotes2 { get; set; }
            public string DocDtMthYr { get; set; }
            public string TTDPrintOut { get; set; }
            public string CompanyLogo2 { set; get; }
            public string CtCode { set; get; }
            public string TaxCodeSier1 { get; set; }
            public string TaxCodeSier2 { get; set; }
            public string TaxCodeSier3 { get; set; }
            public decimal TaxAmtSier1 { get; set; }
            public decimal TaxAmtSier2 { get; set; }
            public decimal TaxAmtSier3 { get; set; }
        }

        class InvoiceDtl
        {
            public int nomor { get; set; }
            public decimal UPriceBeforeTax { get; set; }
            public decimal UPriceAfterTax { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public string PackagingUnitUomCode { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public string CurCode { get; set; }
            public string CTPoNo { get; set; }
            public string PriceUomCode { get; set; }
            public decimal Discount { get; set; }
            public decimal DiscAmt { get; set; }
            public decimal TaxRate { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal AmtDisc { get; set; }
            public decimal AmtTax { get; set; }
            public decimal AmtCost { get; set; }
            public string PtName { get; set; }
            public decimal Qty2 { get; set; }
            public string SODocNo { get; set; }
            public decimal AddCost { get; set; }
            public decimal AddDisc { get; set; }
            public string SACityName { get; set; }
            public string HSCode { get; set; }
            public string Color { get; set; }
            public string Material { get; set; }
            public string ItCodeInternal { get; set; }
            public string DTNAme { get; set; }
            public string CtItCode { get; set; }
            public string Top { get; set; }
            public decimal QtyPcs { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string DOCtRemark { get; set; }
            public decimal DownpaymentAmt { get; set; }
            public string LocalDocNo { get; set; }
            public string Sign { get; set; }
            public string SalesUomCode { get; set; }
           
        }

        class InvoiceDtl2
        {
            public string DocNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal TAmt { get; set; }
        }

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string EmpPict { get; set; }
        }

        private class EmployeeTaxCollector
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Mobile { get; set; }
        }

        //KSM
        private class InvoiceHdr2
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompLocation1 { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public decimal TotalTax { get; set; }
            public decimal TotalAmt { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public decimal TaxAmt1 { get; set; }
            public decimal TaxAmt2 { get; set; }
            public decimal TaxAmt3 { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string SACityName { get; set; }
            public decimal TotalAmt2 { get; set; }
            public decimal Total { get; set; }
            public decimal DownPayment { get; set; }
            public string LocalDocNo { get; set; }
            public string SalesUomCode { get; set; }
            public string Sign { get; set; }
            public string TaxInvDocument { get; set; }

        }

        class InvoiceDtl3
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amt { get; set; }
            public string PriceUomCode { get; set; }
            public string Remark { get; set; }
        }

        private class AddCost
        {
            public string DocNo { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class AddCostDisc
        {
            public decimal AddCost { get; set; }
            public decimal AddDisc { get; set; }
        }

        private class RemarkDetail
        {
            public string Remark { get; set; }
        }

        #endregion

        #region Rate Class

        class Rate
        {
            public decimal Downpayment { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string CtCode { get; set; }
        }

        #endregion

        #endregion

    }
}
