﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseReturnInvoiceDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPurchaseReturnInvoice mFrmParent;
        private string mVdCode = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPurchaseReturnInvoiceDlg2(FrmPurchaseReturnInvoice FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#",
                        "DNo",
                        "", 
                        "Date",
                        
                        //6-9
                        "Amount Type",
                        "Type",
                        "Currency",
                        "Amount"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            // 1=Discount (Total), 2=Bea Cukai, 3=Uang Muka

            SQL.AppendLine("Select T.* From (");
            SQL.AppendLine("    Select A.DocNo, B.DNo, A.DocDt, B.AmtType, C.OptDesc As AmtTypeDesc, B.CurCode, ");
            SQL.AppendLine("    B.Amt-IfNull(( ");
            SQL.AppendLine("        Select Sum(T2.Amt) ");
            SQL.AppendLine("        From TblPurchaseReturnInvoiceHdr T1, TblPurchaseReturnInvoiceDtl2 T2 ");
            SQL.AppendLine("        Where T1.DocNo=T2.DocNo And T1.CancelInd='N' ");
            SQL.AppendLine("        And T2.PurchaseInvoiceDocNo=A.DocNo And T2.PurchaseInvoiceDNo=B.DNo And T2.AmtType=B.AmtType ");
            SQL.AppendLine("    ), 0) As OutstandingAmt ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr A, TblPurchaseInvoiceDtl2 B, TblOption C ");
            SQL.AppendLine("    Where A.DocNo=B.DocNo And B.AmtType In ('1', '3') ");
            SQL.AppendLine("    And C.OptCat='InvoiceAmtType' And B.AmtType=C.OptCode ");
            SQL.AppendLine("    And A.CancelInd='N' And A.VdCode=@VdCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = ") T Where IfNull(T.OutstandingAmt, 0) <>0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T.DocDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        " And Concat(A.DocNo, B.DNo, B.AmtType) Not In (" + mFrmParent.GetSelectedPO() + ") " +
                        Filter + " Order By T.DocDt, T.DocNo, T.DNo, T.AmtTypeDesc;",
                        new string[] 
                        { 
                            //0
                            "DocNo",
                            
                            //1-5
                            "DNo", "DocDt", "AmtType", "AmtTypeDesc", "CurCode", 
                            
                            //6
                            "OutstandingAmt"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 8, Grd1, Row2, 9);
                        mFrmParent.Grd2.Cells[Row1, 9].Value = 0m;
                        mFrmParent.Grd2.Cells[Row1, 10].Value = null;

                        mFrmParent.ComputeAmt();

                        mFrmParent.Grd2.Rows.Add();
                        mFrmParent.Grd2.Cells[mFrmParent.Grd2.Rows.Count - 1, 8].Value = 0m;
                        mFrmParent.Grd2.Cells[mFrmParent.Grd2.Rows.Count - 1, 9].Value = 0m;
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 data.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            for (int Index = 0; Index <= mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd2, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd2, Index, 3) + Sm.GetGrdStr(mFrmParent.Grd2, Index, 5),
                    Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 3) + Sm.GetGrdStr(Grd1, Row, 6)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        #endregion

        #endregion
    }
}
