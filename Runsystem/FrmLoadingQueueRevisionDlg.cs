﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLoadingQueueRevisionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmLoadingQueueRevision mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmLoadingQueueRevisionDlg(FrmLoadingQueueRevision FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e); this.Text = "Daftar Nomor Antrian";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Nomor"+Environment.NewLine+"Antrian", 
                        "Status", 
                        "Status"+Environment.NewLine+"Antrian",   
                        "Tipe"+Environment.NewLine+"Transportasi",   
                        "Nomor"+Environment.NewLine+"Polisi",   
                        
                        //6-10
                        "Pengemudi",   
                        "Jenis"+Environment.NewLine+"Antrian",   
                        "Area"+Environment.NewLine+"Bongkar",   
                        "Berat Sebelum"+Environment.NewLine+"Bongkar",   
                        "Tanggal/Waktu Sebelum"+Environment.NewLine+"Bongkar",  
                        
                        //11-15
                        "Berat Setelah"+Environment.NewLine+"Bongkar",   
                        "Tanggal/Waktu Setelah"+Environment.NewLine+"Bongkar",   
                        "Berat"+Environment.NewLine+"Bersih",   
                        "Created"+Environment.NewLine+"By", 
                        "Created"+Environment.NewLine+"Date", 

                        //16-19
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19});
            Sm.GrdColInvisible(Grd1, new int[] { 2, 10, 12, 14, 15, 16, 17, 18, 19 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 10, 12, 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, Case When A.Status='I' Then 'In' When A.Status='O' then 'Out' ");
            SQL.AppendLine("When A.Status='C' then 'Cancelled' end As StatusDesc, A.DocDateBf, ");
            SQL.AppendLine("Case When A.Status='I' Then 'Timbangan ke-1' When A.Status='O' then 'Timbangan ke-2' When A.Status='C' then 'Cancelled' end As QueueType,  ");
            SQL.AppendLine("D.TTName, A.LicenceNo, A.DrvName, C.ItName, B.LoadAreaName, A.WeightBf, ");
            SQL.AppendLine("Concat(Right(Left(A.DocDateBf, 8), 2), '/', Right(Left(A.DocDateBf, 6), 2), ");
            SQL.AppendLine("'/', Left(A.DocDateBf, 4), ' ', Left(A.DocTimeBf, 2), ':', Right(A.DocTimeBf, 2)) As WeightBfDt, ");
            SQL.AppendLine("A.WeightAf, ");
            SQL.AppendLine("Concat(Right(Left(A.DocDateAf, 8), 2), '/', Right(Left(A.DocDateAf, 6), 2), ");
            SQL.AppendLine("'/', Left(A.DocDateAf, 4), ' ', Left(A.DocTimeAf, 2), ':', Right(A.DocTimeAf, 2)) As WeightAfDt, ");
            SQL.AppendLine("A.Status, A.Netto, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblLoadingQueue A ");
            SQL.AppendLine("Left Join (Select OptCode As LoadArea, OptDesc As LoadAreaName from tblOption where OptCat='LoadingArea') B On A.LoadArea=B.LoadArea ");
            SQL.AppendLine("Left Join (Select OptCode As ItCat, OptDesc As ItName from tblOption where OptCat='LoadingItemCat') C On A.ItCat=C.ItCat ");
            SQL.AppendLine("Left Join TblTransportType D On A.TTCode=D.TTCode ");
            SQL.AppendLine("Where A.DocDateAf is null ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDateBf");
                Sm.FilterStr(ref Filter, ref cm, TxtLicenceNo.Text, "A.LicenceNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "StatusDesc", "QueueType", "TTName", "LicenceNo", "DrvName", 
                            
                            //6-10
                            "ItName", "LoadAreaName", "WeightBf", "WeightBfDt", "WeightAf", 
                            
                            //11-15
                            "WeightAfDt", "Netto", "CreateBy", "CreateDt", "LastUpBy",  
                            
                            //16-17
                            "LastUpDt", "Status"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            if (Sm.GetGrdStr(Grd1, Row, 2) == "Cancelled")
                                Grd1.Rows[Row].ForeColor = Color.Red;
                            else
                                if (Sm.GetGrdStr(Grd1, Row, 2) == "In")
                                    Grd1.Rows[Row].ForeColor = Color.SlateBlue;
                                else
                                    Grd1.Rows[Row].ForeColor = Color.Black;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 19, 16);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtDocNoLoadingQueue.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtTTCodeOld.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                mFrmParent.TxtItem.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.TxtLicenceNoOld.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtDriverNameOld.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtAreaBongkarOld.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                mFrmParent.LoadAreaOld = Sm.GetValue("Select OptCode From TblOption Where OptCat = 'LoadingArea' And optDesc = '" + Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8) + "' ");
                this.Close();
            }

        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor Antrian");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Tanggal Antrian");
        }

        private void ChkLicenceNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor Polisi");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtLicenceNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

      
    }
}
