﻿#region Update
//09/02/2018 [HAR] info pisition ganti level name 
//18/07/2022 [RIS/PRODUCT] Menambah checkbox ActInd
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAssesment2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmAssesment2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmAssesment2Find(FrmAssesment2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            SetLueCompetenceLevelType(ref LueCompetenceType);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AsmCode, A.AsmName, B.levelname, C.OptDesc As CompType,  A.Remark, ActInd, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblAssesmentHdr A ");
            SQL.AppendLine("Inner Join TbllevelHdr B On A.Competencelevel = B.LevelCode ");
            SQL.AppendLine("Inner Join TblOption C On A.CompetenceLevelType=C.OptCode And C.OptCat='CompetenceLevelType'  ");
           
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = true;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Assesment Code", 
                        "Assesment Name",
                        "Active",
                        "Competence"+Environment.NewLine+"Level",
                        "Potency / Competence",
                        
                        //6-10
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        
                        //11-12
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 8, 11 });
            Sm.GrdFormatTime(Grd1, new int[] { 9, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAsmName.Text, new string[] { "A.AsmCode", "A.AsmName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCompetenceType), "A.CompetenceLevelType", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.AsmCode",
                        new string[]
                        {
                            //0
                            "AsmCode", 

                            //1-5
                            "AsmName", "levelname", "CompType", "Remark", "CreateBy",  
                            
                            //6
                            "CreateDt",  "LastUpBy", "LastUpDt", "ActInd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 12, 8);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void SetLueCompetenceLevelType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption " +
                "Where OptCat = 'CompetenceLevelType' Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        #endregion

        #region Event

        private void TxtAsmName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAsmName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Assesment");
        }

    

        #endregion

        private void ChkCompetenceType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void LueCompetenceType_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCompetenceType, new Sm.RefreshLue1(SetLueCompetenceLevelType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

      

       
       
    }
}
