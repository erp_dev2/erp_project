﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOtFormula : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmOtFormulaFind FrmFind;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmOtFormula(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Over Time Formula";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo }, true);
            SetGrd();
            SetFormControl(mState.View);
            SetLueSeqNo(ref LueSeqNo);
            LueSeqNo.Visible = false;

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }

            base.FrmLoad(sender, e);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standart Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                       //0
                        "DNo",
                        
                        //1-3
                        "Sequence#",
                        "Hour",
                        "Index",
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-3
                        80, 100, 100
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, ChkActInd, TxtWh, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3});
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, TxtWh, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 3 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, ChkActInd, TxtWh, MeeRemark, LueSeqNo
            });
            ChkActInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtWh 
            }, 0);
            Sm.ClearGrd(Grd1, true);//
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 3 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOtFormulaFind(this);
           Sm.SetFormFindSetting(this, FrmFind);
        } 

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (Sm.IsGrdColSelected(new int[] { 1, 2, 3 }, e.ColIndex))
                    {
                        if (e.ColIndex == 1) LueRequestEdit(Grd1, LueSeqNo, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3});
                    }
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 2, 3 }, e);
        }

        #endregion

        #region Save Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Question", "Do you want to save this data ?") == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OtFormula", "TblOtFormulaHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveOtFormulaHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    cml.Add(SaveOtFormulaDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtWh, "Working Hour", true) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Sequence# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "Hour is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 3, true, "Ind is empty.")
                    ) return true;

                for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                {
                    if (
                        Row != Row2 &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row2, 1))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Sequence# : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                            "Duplicate entry."
                            );
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveOtFormulaHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblOtFormulaHdr Set ");
            SQL.AppendLine("    ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where ActInd='Y'; ");

            SQL.AppendLine("Insert Into TblOtFormulaHdr(DocNo, DocDt, ActInd, TotalWHrIn1Mth, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'Y', @TotalWHrIn1Mth, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@TotalWHrIn1Mth", Decimal.Parse(TxtWh.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOtFormulaDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into tblotformuladtl(DocNo, DNo, SeqNo, Hr, Ind, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, @SeqNo, @Hr, @Ind, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SeqNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Hr", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Ind", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditOtFormula());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataInactiveAlready();
        }

        private bool IsDataInactiveAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblOtFormulaHdr " +
                    "Where ActInd='N' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already not actived.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditOtFormula()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblOtFormulaHdr Set ");
            SQL.AppendLine("    ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowOtFormulaHdr(DocNo);
                ShowOtFormulaDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowOtFormulaHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, ActInd, TotalWHrIn1Mth, Remark From TblOtFormulaHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "ActInd", "TotalWHrIn1Mth", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        TxtWh.EditValue = Sm.DrDec(dr, c[3]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowOtFormulaDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, SeqNo, Hr, Ind ");
            SQL.AppendLine("From TblOtFormulaDtl ");
            SQL.AppendLine("Where DocNo=@DocNo Order By SeqNo, DNo");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "SeqNo", "Hr", "Ind"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetLueSeqNo(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                for (int i = 1; i <= 8; i++)
                    SQL.AppendLine("Select '" + i + "' As Col1, '" + i + "' As Col2 Union All ");
                SQL.AppendLine("Select '9' As Col1, '9' As Col2 ;");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 1).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        private void LueSeqNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSeqNo, new Sm.RefreshLue1(SetLueSeqNo));
        }

        private void LueSeqNo_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueSeqNo_Leave(object sender, EventArgs e)
        {
            if (LueSeqNo.Visible && fAccept && fCell.ColIndex == 1)
            {
                Grd1.Cells[fCell.RowIndex, 1].Value =
                    (Sm.GetLue(LueSeqNo).Length == 0)
                    ? null : Sm.GetLue(LueSeqNo);
                LueSeqNo.Visible = false;
            }
        }

        private void TxtWh_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtWh, 0);
        }

        #endregion

    }
}
