﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAccountCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmAccountCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmAccountCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtAcCtCode, TxtAcCtName }, true);
                    TxtAcCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtAcCtCode, TxtAcCtName }, false);
                    TxtAcCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtAcCtName }, false);
                    TxtAcCtName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtAcCtCode, TxtAcCtName });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAccountCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAcCtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAcCtCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblAccountCategory Where AcCtCode=@AcCtCode" };
                Sm.CmParam<String>(ref cm, "@AcCtCode", TxtAcCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblAccountCategory(AcCtCode, AcCtName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@AcCtCode, @AcCtName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update AcCtName=@AcCtName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@AcCtCode", TxtAcCtCode.Text);
                Sm.CmParam<String>(ref cm, "@AcCtName", TxtAcCtName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtAcCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string AcCtCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@AcCtCode", AcCtCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblAccountCategory Where AcCtCode=@AcCtCode",
                        new string[] 
                        {
                            "AcCtCode", "AcCtName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtAcCtCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtAcCtName.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAcCtCode, "Account's category code", false) ||
                Sm.IsTxtEmpty(TxtAcCtName, "Account's category name", false) ||
                IsAcCtCodeExisted();
        }

        private bool IsAcCtCodeExisted()
        {
            if (!TxtAcCtCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select AcCtCode From TblAccountCategory Where AcCtCode=@AcCtCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@AcCtCode", TxtAcCtCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Account's category code ( " + TxtAcCtCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAcCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAcCtCode);
        }

        private void TxtAcCtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAcCtName);
        }

        #endregion

        #endregion
    }
}
