﻿#region Update
/*
    28/02/2023 [WED/MNET] new dialog
    02/03/2023 [WED/MNET] penyesuaian nama menu untuk RecvVd dan RecvVd2 ketika dia auto DO atau bukan
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectSystemRevenueExpenseDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptProjectSystemRevenueExpense mFrmParent;
        private string mPRJIDocNo = string.Empty;
        List<FrmRptProjectBudgetRealization.DroppingRealization> l2 = null;

        #endregion

        #region Constructor

        public FrmRptProjectSystemRevenueExpenseDlg(
            FrmRptProjectSystemRevenueExpense FrmParent,
            string PRJIDocNo,
            ref List<FrmRptProjectBudgetRealization.DroppingRealization> l
            )
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPRJIDocNo = PRJIDocNo;

            l2 = new List<FrmRptProjectBudgetRealization.DroppingRealization>();
            l2 = l;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnChoose.Visible = BtnRefresh.Visible = false;
            SetGrd();
            ShowData();
        }

        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-3
                    "Document#",
                    "",
                    "Date"
                }, new int[]
                {
                    //0
                    50,

                    //1-3
                    180, 20, 80
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }
        #endregion

        #region Grid Method

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                bool IsChosed = false;
                foreach (var x in l2.Where(w => w.ProjectImplementationDocNo == mPRJIDocNo))
                {
                    string[] docs = x.RealizationDocNo.Split(',');

                    foreach (var y in docs.Where(w => w.Length > 0))
                    {
                        if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == y)
                        {
                            switch (x.DocType)
                            {
                                case "1":

                                    string MenuCode = string.Empty;

                                    if (Sm.IsDataExist("Select 1 From TblDODeptHdr Where RecvVdDocNo = @Param Limit 1; ", Sm.GetGrdStr(Grd1, e.RowIndex, 1))) MenuCode = Sm.GetParameter("MenuCodeForRecvVdAutoCreateDO");
                                    else MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmRecvVd' And MenuCode Not In (Select ParValue From TblParameter WHere parCode = 'MenuCodeForRecvVdAutoCreateDO' And ParValue Is Not Null) LImit 1; ");

                                    var f = new FrmRecvVd(mFrmParent.mMenuCode);
                                    f.Tag = MenuCode;
                                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                                    f.WindowState = FormWindowState.Normal;
                                    f.StartPosition = FormStartPosition.CenterScreen;
                                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                                    f.ShowDialog();
                                    break;
                                case "2":

                                    if (Sm.IsDataExist("Select 1 From TblDODeptHdr Where RecvVdDocNo = @Param Limit 1; ", Sm.GetGrdStr(Grd1, e.RowIndex, 1))) MenuCode = Sm.GetParameter("MenuCodeForRecvVd2AutoCreateDO");
                                    else MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmRecvVd2' And MenuCode Not In (Select ParValue From TblParameter WHere parCode = 'MenuCodeForRecvVd2AutoCreateDO' And ParValue Is Not Null) LImit 1; ");

                                    var g = new FrmRecvVd2(MenuCode);
                                    g.Tag = MenuCode;
                                    g.Text = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                                    g.WindowState = FormWindowState.Normal;
                                    g.StartPosition = FormStartPosition.CenterScreen;
                                    g.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                                    g.ShowDialog();
                                    break;
                                case "3":
                                    var h = new FrmCashAdvanceSettlement(mFrmParent.mMenuCode);
                                    h.Tag = mFrmParent.mMenuCode;
                                    h.WindowState = FormWindowState.Normal;
                                    h.StartPosition = FormStartPosition.CenterScreen;
                                    h.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                                    h.ShowDialog();
                                    break;
                                case "4":
                                    var i = new FrmPettyCashDisbursement(mFrmParent.mMenuCode);
                                    i.Tag = mFrmParent.mMenuCode;
                                    i.WindowState = FormWindowState.Normal;
                                    i.StartPosition = FormStartPosition.CenterScreen;
                                    i.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                                    i.ShowDialog();
                                    break;
                                case "5":
                                    var j = new FrmVoucher(mFrmParent.mMenuCode);
                                    j.Tag = mFrmParent.mMenuCode;
                                    j.WindowState = FormWindowState.Normal;
                                    j.StartPosition = FormStartPosition.CenterScreen;
                                    j.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                                    j.ShowDialog();
                                    break;
                                default:
                                    break;
                            }

                            IsChosed = true;
                            break;
                        }
                    }

                    if (IsChosed) break;
                }
            }
        }

        #endregion

        #region Show Data
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Sm.ClearGrd(Grd1, false);

                Grd1.BeginUpdate();
                int Row = 0;

                foreach (var x in l2.Where(w => w.ProjectImplementationDocNo == mPRJIDocNo))
                {
                    string[] docs = x.RealizationDocNo.Split(',');

                    string Tbl = string.Empty;

                    switch (x.DocType)
                    {
                        case "1": Tbl = "TblRecvVdHdr"; break;
                        case "2": Tbl = "TblRecvVdHdr"; break;
                        case "3": Tbl = "TblCashAdvanceSettlementHdr"; break;
                        case "4": Tbl = "TblPettyCashDisbursementHdr"; break;
                        case "5": Tbl = "TblVoucherHdr"; break;
                    }

                    foreach(var y in docs.Where(w => w.Length > 0))
                    {
                        string DocDt = Sm.GetValue("Select DocDt From " + Tbl + "  Where DocNo = @Param", y);

                        Grd1.Rows.Add();

                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = y;
                        if (DocDt.Length == 8)
                            Grd1.Cells[Row, 3].Value = Sm.ConvertDate(DocDt);

                        Row++;
                    }
                }
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
        }
        #endregion

        #endregion

    }
}
