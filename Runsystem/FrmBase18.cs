﻿#region Update
/*
    17/11/2022 [IBL/PRODUCT] New base app
 */
#endregion

#region Namespace

using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmBase18 : Form
    {
        public FrmBase18()
        {
            InitializeComponent();
        }

        #region Method

        #region From Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            
        }

        #endregion

        #endregion

        #region Event

        #region Form Event

        private void FrmBase18_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        #endregion

        #endregion
    }
}
