﻿#region Update
/*
    26/01/2021 [WED/PHT] new apps
    18/05/2021 [VIN/PHT] seqno 8 digit 
    08/06/2021 [TKG/PHT] pada saat cancel data, mengecek apakah document# ini sudah diproses di budget maintenance yearly.
    18/06/2021 [TKG/PHT] 
        profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
        cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetRequestYearly : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty
            ;
        internal FrmBudgetRequestYearlyFind FrmFind;
        internal bool 
            mIsMRShowEstimatedPrice = true,
            mIsBudgetCalculateFromEstimatedPrice = true, 
            mIsBudget2YearlyFormat = false,
            mIsBudgetRequestUseItemAndQty = false,
            IsInsert = false,
            mIsFilterByCC = false
            ;
        private string mReqTypeForNonBudget = string.Empty;
        private bool 
            mIsCASUsedForBudget = false
            ;
        internal int[] mInputAmt = { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
        internal List<AcItem> ml;

        #endregion

        #region Constructor

        public FrmBudgetRequestYearly(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Budget Request Yearly";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                ml = new List<AcItem>();
                GetParameter();
                SetGrd();

                if (mIsBudget2YearlyFormat)
                {
                    BtnCopyPrevMth.Text = "Copy Prev. Year";
                    BtnCopyPrevMth.ToolTip = "Copy from previous year";
                }
                SetFormControl(mState.View);
                Sl.SetLueYr(LueYr, string.Empty);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "Budget"+Environment.NewLine+"Category",
                    "Budget"+Environment.NewLine+"Category",
                    "",
                    "Requested Amount"+Environment.NewLine+"01",
                    "Requested Amount"+Environment.NewLine+"02",

                    //6-10
                    "Requested Amount"+Environment.NewLine+"03",
                    "Requested Amount"+Environment.NewLine+"04",
                    "Requested Amount"+Environment.NewLine+"05",
                    "Requested Amount"+Environment.NewLine+"06",
                    "Requested Amount"+Environment.NewLine+"07",

                    //11-15
                    "Requested Amount"+Environment.NewLine+"08",
                    "Requested Amount"+Environment.NewLine+"09",
                    "Requested Amount"+Environment.NewLine+"10",
                    "Requested Amount"+Environment.NewLine+"11",
                    "Requested Amount"+Environment.NewLine+"12",

                    //16
                    "Remark"
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    0, 200, 20, 150, 150,

                    //6-10
                    150, 150, 150, 150, 150,

                    //11-15
                    150, 150, 150, 150, 150,

                    //16
                    200
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueYr, LueCCCode, MeeRemark
                    }, true);
                    BtnRefreshData.Enabled = false;
                    BtnCopyPrevMth.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    TxtDocNo.Focus();
                    IsInsert = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueYr, LueCCCode, MeeRemark }, false);
                    BtnRefreshData.Enabled = true;
                    BtnCopyPrevMth.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 16 });
                    DteDocDt.Focus();
                    IsInsert = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason, ChkCancelInd 
                    }, false);
                    MeeCancelReason.Focus();
                    IsInsert = false;
                    break;
            }
        }

        private void ClearData()
        {
            ml.Clear();
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, 
                DteDocDt, MeeCancelReason, LueYr, LueCCCode, TxtStatus,
                TxtBudgetDocNo, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBudgetRequestYearlyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueCCCodeFilterByProfitCenter(ref LueCCCode, string.Empty, "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "")) return;
            SetFormControl(mState.Edit);
        }


        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                string BCCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                string BCName = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                Sm.FormShowDialog(new FrmBudgetRequestYearlyDlg(this, e.RowIndex, BCCode, BCName, IsInsert));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BudgetRequest", "TblBudgetRequestYearlyHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBudgetRequestHdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveBudgetRequestDtl(DocNo, r));

            Sm.ExecCommands(cml);

            DeactivateOtherBudgetRequest(DocNo);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueCCCode, "Cost Center") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsDocExists() ||
                IsApprovalOngoing();
        }

        private bool IsApprovalOngoing()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblBudgetRequestYearlyHdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And Status = 'O' ");
            SQL.AppendLine("And CCCode = @Param1 ");
            SQL.AppendLine("And Yr = @Param2 ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueCCCode), Sm.GetLue(LueYr), string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "You can't create this request. Another request is still on approval process. Make sure this document#" + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueCCCode), Sm.GetLue(LueYr), string.Empty) + " is approved by all level, or cancelled via document approval.");
                return true;
            }

            return false;
        }

        private bool IsDocExists()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblBudgetRequestYearlyHdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And Status = 'A' ");
            SQL.AppendLine("And CCCode = @Param1 ");
            SQL.AppendLine("And Yr = @Param2 ");
            SQL.AppendLine("And BudgetDocNo Is Not NULL ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueCCCode), Sm.GetLue(LueYr), string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already requested on " + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueCCCode), Sm.GetLue(LueYr), string.Empty) + " and has been processed to Budget Maintenance.");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 budget category.");
                BtnRefreshData.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            //ReComputeStock();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Budget category is empty.")) return true;
                Msg = "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine + Environment.NewLine;
            }

            return false;
        }

        private MySqlCommand SaveBudgetRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblBudgetRequestYearlyHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Yr, CCCode, Amt, BudgetDocNo, Remark, Status, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', @Yr, @CCCode, @Amt, Null, @Remark, 'O', @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='BudgetRequestYearly' ");
            SQL.AppendLine("And T.DeptCode Is Not Null And T.DeptCode In (Select DeptCode From TblCostCenter Where CCCode = @CCCode) ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyHdr A ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblBudgetRequestYearlyHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'BudgetRequestYearly' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            int i = 0;
            foreach (var x in ml)
            {
                SQL.AppendLine("Insert Into TblBudgetRequestYearlyDtl2(DocNo, BCCode, ItCode, Rate01, Rate02, Rate03, Rate04, Rate05, Rate06, Rate07, Rate08, Rate09, Rate10, Rate11, Rate12, Qty01, Qty02, Qty03, Qty04, Qty05, Qty06, Qty07, Qty08, Qty09, Qty10, Qty11, Qty12, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @BCCode_" + i.ToString() +
                    ", @ItCode_" + i.ToString() +
                    ", @Rate01_" + i.ToString() +
                    ", @Rate02_" + i.ToString() +
                    ", @Rate03_" + i.ToString() +
                    ", @Rate04_" + i.ToString() +
                    ", @Rate05_" + i.ToString() +
                    ", @Rate06_" + i.ToString() +
                    ", @Rate07_" + i.ToString() +
                    ", @Rate08_" + i.ToString() +
                    ", @Rate09_" + i.ToString() +
                    ", @Rate10_" + i.ToString() +
                    ", @Rate11_" + i.ToString() +
                    ", @Rate12_" + i.ToString() +
                    ", @Qty01_" + i.ToString() +
                    ", @Qty02_" + i.ToString() +
                    ", @Qty03_" + i.ToString() +
                    ", @Qty04_" + i.ToString() +
                    ", @Qty05_" + i.ToString() +
                    ", @Qty06_" + i.ToString() +
                    ", @Qty07_" + i.ToString() +
                    ", @Qty08_" + i.ToString() +
                    ", @Qty09_" + i.ToString() +
                    ", @Qty10_" + i.ToString() +
                    ", @Qty11_" + i.ToString() +
                    ", @Qty12_" + i.ToString() +
                    ", @CreateBy, CurrentDateTime()); ");

                Sm.CmParam<String>(ref cm, "@BCCode_" + i, x.BCCode);
                Sm.CmParam<String>(ref cm, "@ItCode_" + i, x.ItCode);
                Sm.CmParam<Decimal>(ref cm, "@Rate01_" + i, x.Rate01);
                Sm.CmParam<Decimal>(ref cm, "@Rate02_" + i, x.Rate02);
                Sm.CmParam<Decimal>(ref cm, "@Rate03_" + i, x.Rate03);
                Sm.CmParam<Decimal>(ref cm, "@Rate04_" + i, x.Rate04);
                Sm.CmParam<Decimal>(ref cm, "@Rate05_" + i, x.Rate05);
                Sm.CmParam<Decimal>(ref cm, "@Rate06_" + i, x.Rate06);
                Sm.CmParam<Decimal>(ref cm, "@Rate07_" + i, x.Rate07);
                Sm.CmParam<Decimal>(ref cm, "@Rate08_" + i, x.Rate08);
                Sm.CmParam<Decimal>(ref cm, "@Rate09_" + i, x.Rate09);
                Sm.CmParam<Decimal>(ref cm, "@Rate10_" + i, x.Rate10);
                Sm.CmParam<Decimal>(ref cm, "@Rate11_" + i, x.Rate11);
                Sm.CmParam<Decimal>(ref cm, "@Rate12_" + i, x.Rate12);
                Sm.CmParam<Decimal>(ref cm, "@Qty01_" + i, x.Qty01);
                Sm.CmParam<Decimal>(ref cm, "@Qty02_" + i, x.Qty02);
                Sm.CmParam<Decimal>(ref cm, "@Qty03_" + i, x.Qty03);
                Sm.CmParam<Decimal>(ref cm, "@Qty04_" + i, x.Qty04);
                Sm.CmParam<Decimal>(ref cm, "@Qty05_" + i, x.Qty05);
                Sm.CmParam<Decimal>(ref cm, "@Qty06_" + i, x.Qty06);
                Sm.CmParam<Decimal>(ref cm, "@Qty07_" + i, x.Qty07);
                Sm.CmParam<Decimal>(ref cm, "@Qty08_" + i, x.Qty08);
                Sm.CmParam<Decimal>(ref cm, "@Qty09_" + i, x.Qty09);
                Sm.CmParam<Decimal>(ref cm, "@Qty10_" + i, x.Qty10);
                Sm.CmParam<Decimal>(ref cm, "@Qty11_" + i, x.Qty11);
                Sm.CmParam<Decimal>(ref cm, "@Qty12_" + i, x.Qty12);

                ++i;
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBudgetRequestDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBudgetRequestYearlyDtl(DocNo, BCCode, SeqNo, Amt01, Amt02, Amt03, Amt04, Amt05, Amt06, Amt07, Amt08, Amt09, Amt10, Amt11, Amt12, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @BCCode, @SeqNo, @Amt01, @Amt02, @Amt03, @Amt04, @Amt05, @Amt06, @Amt07, @Amt08, @Amt09, @Amt10, @Amt11, @Amt12, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", Sm.Right("0000000" + (Row + 1).ToString(), 8));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt01", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt02", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt03", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Amt04", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Amt05", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Amt06", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Amt07", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt08", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Amt09", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Amt10", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Amt11", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Amt12", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 16));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private void DeactivateOtherBudgetRequest(string DocNo)
        {
            if (Sm.IsDataExist("Select 1 From TblBudgetRequestYearlyHdr Where DocNo = @Param And Status ='A' Limit 1;", DocNo))
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Update TblBudgetRequestYearlyHdr Set CancelInd='Y', CancelReason='Auto cancelled by system.' ");
                SQL.AppendLine("Where CancelInd='N' ");
                SQL.AppendLine("And Yr=@Yr ");
                SQL.AppendLine("And CCCode=@CCCode ");
                SQL.AppendLine("And DocNo <> @DocNo ");
                SQL.AppendLine("And BudgetDocNo Is Null; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

                Sm.ExecCommand(cm);
            }
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditBudgetRequestHdr());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready() ||
                IsDataAlreadyProcessToBudgetMaintenanceYearly();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyProcessToBudgetMaintenanceYearly()
        {
            return Sm.IsDataExist(
                "Select 1 From TblBudgetMaintenanceYearlyHdr Where Status In ('O', 'A') And BudgetRequestDocNo=@Param Limit 1;",
                TxtDocNo.Text,
                "This document already processed to budget maintenance yearly.");
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblBudgetRequestYearlyHdr Where DocNo=@Param And (CancelInd='Y' Or Status = 'C');",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private bool IsDataProcessedAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblBudgetRequestYearlyHdr Where DocNo=@Param And BudgetDocNo Is Not Null;",
                TxtDocNo.Text,
                "This document already processed to budget.");
        }


        private MySqlCommand EditBudgetRequestHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBudgetRequestYearlyHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBudgetRequestYearlyHdr(DocNo);
                ShowBudgetRequestYearlyDtl(DocNo);
                ShowBudgetRequestYearlyDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBudgetRequestYearlyHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' Else '' End As StatusDesc, ");
            SQL.AppendLine("A.CancelReason, A.CancelInd, A.Yr, A.CCCode, A.Amt, A.BudgetDocNo, A.Remark ");
            SQL.AppendLine("From TblBudgetRequestYearlyHdr A ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    //1-5
                    "DocDt", "StatusDesc", "CancelReason", "CancelInd", "Yr", 
                    //6-10
                    "CCCode", "Amt", "BudgetDocNo", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[5]));
                    Sl.SetLueCCCodeFilterByProfitCenter(ref LueCCCode, Sm.DrStr(dr, c[6]), "N");
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    TxtBudgetDocNo.EditValue = Sm.DrStr(dr, c[8]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                }, true
            );
        }

        private void ShowBudgetRequestYearlyDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);


            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SeqNo, A.BCCode, B.BCName, A.Amt01, A.Amt02, ");
            SQL.AppendLine("A.Amt03, A.Amt04, A.Amt05, A.Amt06, A.Amt07, A.Amt08, A.Amt09, ");
            SQL.AppendLine("A.Amt10, A.Amt11, A.Amt12, A.Remark ");
            SQL.AppendLine("From TblBudgetRequestYearlyDtl A ");
            SQL.AppendLine("Inner Join TblBudgetCategory B On A.BCCode = B.BCCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.SeqNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "SeqNo", 
                    "BCCode", "BCName", "Amt01", "Amt02", "Amt03", 
                    "Amt04", "Amt05", "Amt06", "Amt07", "Amt08",
                    "Amt09", "Amt10", "Amt11", "Amt12", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        private void ShowBudgetRequestYearlyDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.BCCode, B.BCName, A.ItCode, C.ItName, C.PurchaseUomCode, A.Rate01, ");
            SQL.AppendLine("A.Rate02, A.Rate03, A.Rate04, A.Rate05, A.Rate06, A.Rate07, A.Rate08, A.Rate09, ");
            SQL.AppendLine("A.Rate10, A.Rate11, A.Rate12, A.Qty01, A.Qty02, A.Qty03, A.Qty04, A.Qty05, A.Qty06, ");
            SQL.AppendLine("A.Qty07, A.Qty08, A.Qty09, A.Qty10, A.Qty11, A.Qty12 ");
            SQL.AppendLine("From TblBudgetRequestYearlyDtl2 A ");
            SQL.AppendLine("Inner Join TblBudgetCategory B On A.BCCode = B.BCCode ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "BCCode", 
                    //1-5
                    "BCName", "ItCode", "ItName", "PurchaseUomCode", "Rate01", 
                    //6-10
                    "Rate02", "Rate03", "Rate04", "Rate05", "Rate06", 
                    //11-15
                    "Rate07", "Rate08", "Rate09", "Rate10", "Rate11", 
                    //16-20
                    "Rate12", "Qty01", "Qty02", "Qty03", "Qty04", 
                    //21-25
                    "Qty05", "Qty06", "Qty07", "Qty08", "Qty09", 
                    //26-28
                    "Qty10", "Qty11", "Qty12"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ml.Add(new AcItem()
                        {
                            BCCode = Sm.DrStr(dr, c[0]),
                            BCName = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            ItName = Sm.DrStr(dr, c[3]),
                            PurchaseUomCode = Sm.DrStr(dr, c[4]),
                            Rate01 = Sm.DrDec(dr, c[5]),
                            Rate02 = Sm.DrDec(dr, c[6]),
                            Rate03 = Sm.DrDec(dr, c[7]),
                            Rate04 = Sm.DrDec(dr, c[8]),
                            Rate05 = Sm.DrDec(dr, c[9]),
                            Rate06 = Sm.DrDec(dr, c[10]),
                            Rate07 = Sm.DrDec(dr, c[11]),
                            Rate08 = Sm.DrDec(dr, c[12]),
                            Rate09 = Sm.DrDec(dr, c[13]),
                            Rate10 = Sm.DrDec(dr, c[14]),
                            Rate11 = Sm.DrDec(dr, c[15]),
                            Rate12 = Sm.DrDec(dr, c[16]),
                            Qty01 = Sm.DrDec(dr, c[17]),
                            Qty02 = Sm.DrDec(dr, c[18]),
                            Qty03 = Sm.DrDec(dr, c[19]),
                            Qty04 = Sm.DrDec(dr, c[20]),
                            Qty05 = Sm.DrDec(dr, c[21]),
                            Qty06 = Sm.DrDec(dr, c[22]),
                            Qty07 = Sm.DrDec(dr, c[23]),
                            Qty08 = Sm.DrDec(dr, c[24]),
                            Qty09 = Sm.DrDec(dr, c[25]),
                            Qty10 = Sm.DrDec(dr, c[26]),
                            Qty11 = Sm.DrDec(dr, c[27]),
                            Qty12 = Sm.DrDec(dr, c[28])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            mIsBudgetRequestUseItemAndQty = Sm.GetParameterBoo("IsBudgetRequestUseItemAndQty");
            mIsCASUsedForBudget = Sm.GetParameterBoo("IsCASUsedForBudget");
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
        }

        internal void ComputeTotalAmount()
        {
            decimal Amt = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length != 0)
                {
                    for (int j = 4; j < 16; ++j)
                        Amt += Sm.GetGrdDec(Grd1, r, j);
                }
            }
            TxtAmt.Text = Sm.FormatNum(Amt, 0);
        }

        private void RefreshData()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode, BCName ");
            SQL.AppendLine("From TblBudgetCategory ");
            SQL.AppendLine("Where CCCode Is Not Null ");
            SQL.AppendLine("And CCCode = @CCCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "BCCode", "BCName" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    for (int i = 4; i < 16; ++i)
                    {
                        Grd1.Cells[Row, i].Value = 0m;
                    }
                    Grd1.Cells[Row, 16].Value = null;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.FocusGrd(Grd1, 0, 2);
            ComputeTotalAmount();
        }

        private void CopyPrevYr()
        {
            string PrevDocNo = string.Empty;

            PrevDocNo = Sm.GetValue("Select DocNo From TblBudgetRequestYearlyHdr Where Yr < @Param1 And Status = 'A' And CancelInd = 'N' And CCCode = @Param2; ", Sm.GetLue(LueYr), Sm.GetLue(LueCCCode), string.Empty);

            if (PrevDocNo.Length > 0)
            {
                TxtAmt.EditValue = Sm.FormatNum(0m, 0);
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length != 0)
                    {
                        for (int j = 4; j < 16; ++j)
                            Grd1.Cells[r, j].Value = 0m;
                    }
                }

                ml.Clear();

                CopyPrevDtl(PrevDocNo);
                ComputeTotalAmount();
                CopyPrevDtl2(PrevDocNo);
            }
            else
            {
                Sm.StdMsg(mMsgType.NoData, "No previous data available.");
                return;
            }
        }

        private void CopyPrevDtl(string PrevDocNo)
        {
            string BCCode = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode, Amt01, Amt02, Amt03, Amt04, ");
            SQL.AppendLine("Amt05, Amt06, Amt07, Amt08, Amt09, Amt10, ");
            SQL.AppendLine("Amt11, Amt12 ");
            SQL.AppendLine("From TblBudgetRequestYearlyDtl ");
            SQL.AppendLine("Where DocNo = @PrevDocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@PrevDocNo", PrevDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr,
                    new string[] 
                        { 
                            "BCCode", 
                            "Amt01", "Amt02", "Amt03", "Amt04", "Amt05", 
                            "Amt06", "Amt07", "Amt08", "Amt09", "Amt10", 
                            "Amt11", "Amt12"
                        });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        BCCode = Sm.DrStr(dr, 0);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), BCCode))
                            {
                                Grd1.Cells[r, 4].Value = Sm.DrDec(dr, 1);
                                Grd1.Cells[r, 5].Value = Sm.DrDec(dr, 2);
                                Grd1.Cells[r, 6].Value = Sm.DrDec(dr, 3);
                                Grd1.Cells[r, 7].Value = Sm.DrDec(dr, 4);
                                Grd1.Cells[r, 8].Value = Sm.DrDec(dr, 5);
                                Grd1.Cells[r, 9].Value = Sm.DrDec(dr, 6);
                                Grd1.Cells[r, 10].Value = Sm.DrDec(dr, 7);
                                Grd1.Cells[r, 11].Value = Sm.DrDec(dr, 8);
                                Grd1.Cells[r, 12].Value = Sm.DrDec(dr, 9);
                                Grd1.Cells[r, 13].Value = Sm.DrDec(dr, 10);
                                Grd1.Cells[r, 14].Value = Sm.DrDec(dr, 11);
                                Grd1.Cells[r, 15].Value = Sm.DrDec(dr, 12);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void CopyPrevDtl2(string PrevDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BCCode, B.BCName, A.ItCode, C.ItName, C.PurchaseUomCode, A.Rate01, A.Rate02, ");
            SQL.AppendLine("A.Rate03, A.Rate04, A.Rate05, A.Rate06, A.Rate07, A.Rate08, A.Rate09, A.Rate10, ");
            SQL.AppendLine("A.Rate11, A.Rate12, A.Qty01, A.Qty02, A.Qty03, A.Qty04, A.Qty05, A.Qty06, A.Qty07, ");
            SQL.AppendLine("A.Qty08, A.Qty09, A.Qty10, A.Qty11, A.Qty12 ");
            SQL.AppendLine("From TblBudgetRequestYearlyDtl2 A ");
            SQL.AppendLine("Inner Join TblBudgetCategory B On A.BCCode = B.BCCode ");
            SQL.AppendLine("    And A.DocNo = @PrevDocNo ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@PrevDocNo", PrevDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr,
                new string[] 
                { 
                    //0
                    "BCCode", 
                    //1-5
                    "BCName", "ItCode", "ItName", "PurchaseUomCode", "Rate01", 
                    //6-10
                    "Rate02", "Rate03", "Rate04", "Rate05", "Rate06", 
                    //11-15
                    "Rate07", "Rate08", "Rate09", "Rate10", "Rate11", 
                    //16-20
                    "Rate12", "Qty01", "Qty02", "Qty03", "Qty04", 
                    //21-25
                    "Qty05", "Qty06", "Qty07", "Qty08", "Qty09", 
                    //26-28
                    "Qty10", "Qty11", "Qty12"
                });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ml.Add(new AcItem()
                        {
                            BCCode = Sm.DrStr(dr, c[0]),
                            BCName = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            ItName = Sm.DrStr(dr, c[3]),
                            PurchaseUomCode = Sm.DrStr(dr, c[4]),
                            Rate01 = Sm.DrDec(dr, c[5]),
                            Rate02 = Sm.DrDec(dr, c[6]),
                            Rate03 = Sm.DrDec(dr, c[7]),
                            Rate04 = Sm.DrDec(dr, c[8]),
                            Rate05 = Sm.DrDec(dr, c[9]),
                            Rate06 = Sm.DrDec(dr, c[10]),
                            Rate07 = Sm.DrDec(dr, c[11]),
                            Rate08 = Sm.DrDec(dr, c[12]),
                            Rate09 = Sm.DrDec(dr, c[13]),
                            Rate10 = Sm.DrDec(dr, c[14]),
                            Rate11 = Sm.DrDec(dr, c[15]),
                            Rate12 = Sm.DrDec(dr, c[16]),
                            Qty01 = Sm.DrDec(dr, c[17]),
                            Qty02 = Sm.DrDec(dr, c[18]),
                            Qty03 = Sm.DrDec(dr, c[19]),
                            Qty04 = Sm.DrDec(dr, c[20]),
                            Qty05 = Sm.DrDec(dr, c[21]),
                            Qty06 = Sm.DrDec(dr, c[22]),
                            Qty07 = Sm.DrDec(dr, c[23]),
                            Qty08 = Sm.DrDec(dr, c[24]),
                            Qty09 = Sm.DrDec(dr, c[25]),
                            Qty10 = Sm.DrDec(dr, c[26]),
                            Qty11 = Sm.DrDec(dr, c[27]),
                            Qty12 = Sm.DrDec(dr, c[28])
                        });
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }

        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, "N");
                ClearGrd();
            }
        }

        #endregion

        #region Button

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to refresh data ?") == DialogResult.No) return;
            if (!(Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueCCCode, "Cost Center")))
                RefreshData();
        }

        private void BtnCopyPrevMth_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to copy amount from previous year's data ?") == DialogResult.No) return;

            if (!(Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueCCCode, "Cost Center")))
                CopyPrevYr();
        }

        #endregion

        #endregion

        #region Class

        internal class AcItem
        {
            public string BCCode { set; get; }
            public string BCName { set; get; }
            public string ItCode { set; get; }
            public string ItName { get; set; }
            public string PurchaseUomCode { get; set; }
            public decimal Rate01 { get; set; }
            public decimal Rate02 { get; set; }
            public decimal Rate03 { get; set; }
            public decimal Rate04 { get; set; }
            public decimal Rate05 { get; set; }
            public decimal Rate06 { get; set; }
            public decimal Rate07 { get; set; }
            public decimal Rate08 { get; set; }
            public decimal Rate09 { get; set; }
            public decimal Rate10 { get; set; }
            public decimal Rate11 { get; set; }
            public decimal Rate12 { get; set; }
            public decimal Qty01 { get; set; }
            public decimal Qty02 { get; set; }
            public decimal Qty03 { get; set; }
            public decimal Qty04 { get; set; }
            public decimal Qty05 { get; set; }
            public decimal Qty06 { get; set; }
            public decimal Qty07 { get; set; }
            public decimal Qty08 { get; set; }
            public decimal Qty09 { get; set; }
            public decimal Qty10 { get; set; }
            public decimal Qty11 { get; set; }
            public decimal Qty12 { get; set; }
        }

        internal class AcRow
        {
            public string BCCode { set; get; }
            public int Row { set; get; }
        }

        #endregion
    }
}
