﻿#region Update
/*
    18/06/2020 [IBL/KSM] new apps
    03/07/2020 [ICA/KSM] Membuat remark bisa diisi
    03/12/2020 [VIN/KSM] LocalDocNo tergenerate otomatis berdasarkan parameter=IsSalesMemoGenerateLocalDocNo
    06/01/2021 [ICA/KSM] menambah kolom Qty1, Qty2, Uom1 dan Uom2 bersifat mandatory
    28/01/2021 [VIN/KSM] Generate Local DocNo --> nomor urut tidak berdasarkan local docno sales contract yang dipilih
    31/01/2021 [ICA/KSM] Penyesuaian Printout Weaving dan Spinning
    03/02/2021 [WED/KSM] parameter IsDR3HideSeveralColumn untuk meng hide beberapa kolom
 *  09/02/2021 [ICA/KSM] Saat save, mengupdate status sales contract menjadi Fulfilled
 *  11/02/2021 [ICA/KSM] Param sign weaving dan spinning dibedakan
    16/02/2021 [IBL/KSM] TTD printout weaving masih kurang 1 (yg menyetujui)
 *  19/02/2021 [ICA/KSM] Menambahkan Customer Category di LueCtCode berdasarkan param IsCustomerComboShowCategory
    22/02/2021 [IBL/KSM] Local document generate otomatis. tapi masih bisa diedit.
 *  08/03/2021 [RDH/KSM] Tambah kolom Local Document Sales Contract (0109990102) di panel detail menu DR 
 *  18/03/2021 [ICA/KSM] Update Status SC berdasarkan data yg ditarik (semua/partial)
 *  12/04/2021 [ICA/KSM] mengubah code status dari 1,2 dan 8 menjadi O, F dan M
 *  22/04/2021 [ICA/KSM] Mengaktifkan untuk DR CBD
 *  21/06/2021 [HAR/KSM] BUG saat validasi IsQtyNotValid tdk bisa save, padahal masih ada qty outstanding
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmDR3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mCity = string.Empty, mCnt = string.Empty, mExpCity = string.Empty, mDocNo = string.Empty;
        internal FrmDR3Find FrmFind;
        iGCell fCell;
        bool fAccept;
        internal int bal, left, right = 0;
        internal int start, end = 0;
        internal decimal pallet = 0m;
        internal string 
            QtyPallet = string.Empty,
            mIsPrintOutDRSC=string.Empty,
            mLogisticItemCategoryCode = string.Empty,
            mLogisticDepartmentCode = string.Empty,
            mLocalDocument = "0",
            mAllowanceSiteCodeForDRSC = string.Empty,
            mAllowancePercentageQtyForDRSC = String.Empty, 
            mDocTitle = string.Empty;
        internal bool 
            mIsDRWithCBD= false,
            mIsSystemUseCostCenter = false,
            mIsCreditLimitValidate = false,
            mIsShowForeignName = false,
            mIsApprovalBySiteMandatory = false,
            mIsCustomerItemNameMandatory = false,
            mIsSalesMemoGenerateLocalDocNo= false,
            mIsCustomerComboShowCategory = false;

        private string
           mPortForFTPClient = string.Empty,
           mHostAddrForFTPClient = string.Empty,
           mSharedFolderForFTPClient = string.Empty,
           mUsernameForFTPClient = string.Empty,
           mPasswordForFTPClient = string.Empty,
           mFileSizeMaxUploadFTPClient = string.Empty,
           mPPNSiteCodeForSalesMemo = string.Empty;
        private byte[] downloadedData;
        private bool mIsDR3HideSeveralColumn = false,
            mIsClearData = false, 
            mIsPartial = false;

        #endregion

        #region Constructor

        public FrmDR3(string MenuCode)
        {
            this.Text = "Delivery Request Based On Sales Contract";
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Delivery Request";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();

                Sl.SetLueTTCode(ref LueTtCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty);
                Sl.SetLueUomCode(ref LueUomCode1);
                Sl.SetLueUomCode(ref LueUomCode2);
                LueUomCode1.Visible = LueUomCode2.Visible = false;
                SetLueCtCode2(ref LueCtCode, string.Empty);
                SetLueItCode(ref LueItCode);
                SetLueVdCode(ref LueVdCode);
                SetFormControl(mState.View);
                SetLueStatus(ref LueStatus);

                var cm = new MySqlCommand()
                {
                    CommandText = "Select ParValue from TblParameter Where ParCode='FormPrintOutDRSC' "
                };
                mIsPrintOutDRSC = Sm.GetValue(cm);
                if (mIsPrintOutDRSC.Length == 0)
                    mIsPrintOutDRSC = "DRSC";
                SetGrd();
                base.FrmLoad(sender, e);
                //if this application is called from other application :)
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            bool mFlag = true;
            string mMenuDRStd = Sm.GetParameter("MenuCodeForDRBasedOnSCStandard");
            if (mMenuDRStd.Length > 0)
            {
                string[] s = mMenuDRStd.Split(',');
                foreach (string d in s)
                {
                    if (mMenuCode == d)
                    {
                        mFlag = false;
                        break;
                    }
                }
            }
            //mIsDRWithCBD = !Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForDeliveryRequestStandard"));
            mIsDRWithCBD = mFlag;
            mDocTitle = Sm.GetParameter("Doctitle");
            mIsCreditLimitValidate = Sm.GetParameter("IsCreditLimitValidate") == "Y";
            mLogisticDepartmentCode = Sm.GetParameter("LogisticDepartmentCode");
            mLogisticItemCategoryCode = Sm.GetParameter("LogisticItemCategoryCode");
            mIsApprovalBySiteMandatory = Sm.GetParameter("IsApprovalBySiteMandatory") == "Y";
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mLocalDocument = Sm.GetParameter("LocalDocument");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsCustomerItemNameMandatory = Sm.GetParameterBoo("IsCustomerItemNameMandatory");
            mAllowanceSiteCodeForDRSC = Sm.GetParameter("AllowanceSiteCodeForDRSC");
            mAllowancePercentageQtyForDRSC = Sm.GetParameter("AllowancePercentageQtyForDRSC");
            mIsSalesMemoGenerateLocalDocNo = Sm.GetParameterBoo("IsSalesMemoGenerateLocalDocNo");
            mPPNSiteCodeForSalesMemo = Sm.GetParameter("PPNSiteCodeForSalesMemo");
            mIsDR3HideSeveralColumn = Sm.GetParameterBoo("IsDR3HideSeveralColumn");
            mIsCustomerComboShowCategory = Sm.GetParameterBoo("IsCustomerComboShowCategory");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 39;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Item's"+Environment.NewLine+"Code",
                    "Item's"+Environment.NewLine+"Name",
                    "",
                    "Item's"+Environment.NewLine+"Local Code",
                    "SC#",

                    //6-10
                    "",
                    "Sales Memo DNo", 
                    "Packaging"+Environment.NewLine+"Unit",
                    "Outstanding"+Environment.NewLine+"Packaging"+Environment.NewLine+"Quantity",
                    "Packaging"+Environment.NewLine+"Quantity",

                    //11-15
                    "Balance",
                    "Outstanding"+Environment.NewLine+"Sales"+Environment.NewLine+"Memo",
                    "Requested"+Environment.NewLine+"Quantity (Sales)",
                    "Balance",
                    "UoM"+Environment.NewLine+"(Sales)",

                    //16-20
                    "Stock",
                    "Requested"+Environment.NewLine+"Quantity (Inventory)",
                    "UoM"+Environment.NewLine+"(Inventory)",
                    "Delivery"+Environment.NewLine+"Date",
                    "Remark",

                    //21-25
                    "Price After Tax",
                    "Foreign Name",
                    "Customer's"+Environment.NewLine+"Item Code",
                    "Customer's"+Environment.NewLine+"Item Name",
                    "Shipping Name",

                    //26-30
                    "Order",
                    "AllowanceQty",
                    "UsedItem",
                    "Sales Memo#", 
                    "UsedItem",

                    //31-35
                    "TaxInd", 
                    "Quantity1",
                    "UoMCode1",
                    "UoM1",
                    "Quantity2",

                    //36-38
                    "UoMCode2",
                    "UoM2",
                    "Local#"
                },
                new int[] 
                {
                    20, 

                    80, 200, 20, 150, 150, 

                    20, 50, 100, 100, 100,

                    80, 100, 100, 80, 80, 

                    40, 130, 80, 100, 150,

                    100, 120, 120, 250, 250,

                    250, 10, 10, 100, 100,

                    30, 100, 0, 80, 100, 

                    0, 80, 120
                }
            );
            Grd1.Cols[22].Move(3);
            Grd1.Cols[24].Move(4);
            Grd1.Cols[23].Move(4);
            Grd1.Cols[29].Move(7);
            Grd1.Cols[32].Move(16);
            Grd1.Cols[34].Move(17);
            Grd1.Cols[35].Move(18);
            Grd1.Cols[37].Move(19);
            Grd1.Cols[38].Move(10);

            Sm.GrdFormatDate(Grd1, new int[] { 19 });
            if (mIsDRWithCBD == false)
            {
                Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 27, 28, 29, 30, 31, 38 });
            }
            else
            {
                Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 27, 28, 29, 30, 31, 38 });
            }
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 10, 12, 13, 14, 17, 21, 30, 32, 35 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0, 3, 6, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 7, 16, 21, 27, 28, 30, 31, 33, 36 }, false);

            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 22 });

            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 23, 24 });

            if (mIsDR3HideSeveralColumn)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 17, 18 });
            }

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtLocalDocNo, LueCtCode, LueShipAdd, 
                        LueStatus, LueShipAdd, TxtAddress, TxtCity, TxtCountry, 
                        TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                        LueVdCode, LueDriver, TxtCityCode, LueTtCode, TxtPlatNo, 
                        TxtMobile2, LueAgtCode, MeeNote, MeeRemark, LueItCode,
                        DteUsageDt, TxtMRDocNo, LueSiteCode
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnCtShippingAddress.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 6 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueCtCode, LueShipAdd, LueVdCode, 
                        LueDriver, TxtCityCode, LueTtCode, TxtPlatNo, TxtMobile2, 
                        LueAgtCode, MeeNote, MeeRemark, LueItCode, LueSiteCode 
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = false;
                    BtnCtShippingAddress.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 10, 20, 25, 26, 32, 34, 35, 37 });
                    DteDocDt.Focus();
                  break;
                case mState.Edit:
                    Sm.SetControlReadOnly(LueStatus, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    BtnCtShippingAddress.Enabled = false;
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 32, 34, 35, 37 });
                    LueStatus.Focus();
                    break;
            }
        }

        private void ClearData(int Option)
        {
            switch (Option)
            {
                case 1:
                    mIsClearData = true;
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtDocNo, DteDocDt, TxtLocalDocNo, LueCtCode, LueShipAdd, 
                        LueStatus, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                        TxtPhone, TxtFax, TxtEmail, TxtMobile, LueVdCode, 
                        LueDriver, TxtCityCode, LueTtCode, TxtPlatNo, TxtMobile2, 
                        LueAgtCode, MeeNote, MeeRemark, LueItCode, DteUsageDt,
                        LueSiteCode
                    });
                    ChkCancelInd.Checked = false;
                    ClearGrd();
                    Sm.FocusGrd(Grd1, 0, 1);
                    break;
                case 2:
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtAddress, TxtCity, TxtCountry, TxtPostalCd, TxtPhone, 
                        TxtFax, TxtEmail, TxtMobile, LueAgtCode
                    });
                    break;
                case 3:
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        LueDriver, TxtMobile2, TxtCityCode, LueTtCode, TxtPlatNo
                    });
                    break;
            }
           
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9, 10, 13, 32, 35});
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDR3Find(this, mIsDRWithCBD);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData(1);
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueStatus, "O");
                mIsClearData = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mIsPartial = false;
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData(1);
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueSiteCode, "Site") && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDR3Dlg(this, Sm.GetLue(LueCtCode), Sm.GetLue(LueSiteCode), mIsDRWithCBD));
                    }

                }
                if (e.ColIndex == 34) LueRequestEdit(Grd1, LueUomCode1, ref fCell, ref fAccept, e);
                if (e.ColIndex == 37) LueRequestEdit(Grd1, LueUomCode2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSalesContract(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                if (e.ColIndex == 16) ShowStockInfo(e.RowIndex);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueSiteCode, "Site") && !Sm.IsLueEmpty(LueCtCode, "Customer") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmDR3Dlg(this, Sm.GetLue(LueCtCode), Sm.GetLue(LueSiteCode), mIsDRWithCBD));

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmSalesContract(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 16) 
                ShowStockInfo(e.RowIndex);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtDocNo.Text.Length == 0)
            {
                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);
                                if (Grd1.Rows.Count <= 0)
                                {
                                    Grd1.Rows.Add();
                                }
                            }
                        }
                    }
                }
                if (Grd1.Rows.Count == 1) GenerateLocalDocNo();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 10)
            {
                ComputeUom(e.RowIndex);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;

            string DocNo = GenerateDocNo2(Sm.GetDte(DteDocDt), "DR", "TblDRHdr"), SalesContractDocNo=string.Empty,
                SalesMemoDocNo=string.Empty, LocalDocNo= string.Empty, TaxInd = string.Empty;

            var cml = new List<MySqlCommand>();

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 5).Length > 0)
                {
                    if (SalesContractDocNo.Length > 0) SalesContractDocNo += ",";
                    SalesContractDocNo += Sm.GetGrdStr(Grd1, i, 5);
                }
            }
            var SQL = new StringBuilder();

            SQL.Append("SELECT A.SalesMemoDocNo FROM tblsalescontract A ");
            SQL.Append("where FIND_IN_SET(A.DocNo,'" + SalesContractDocNo + "')  ");
            SQL.Append("ORDER BY LEFT(A.docno, 4), LEFT(A.docdt, 6) ");
            SQL.Append("LIMIT 1 ");

            SalesMemoDocNo = Sm.GetValue(SQL.ToString());

            //if (mIsSalesMemoGenerateLocalDocNo)
            //    LocalDocNo = Sm.GenerateLocalDocNoSalesMemo(SalesMemoDocNo, "DR");
            //else
            //    LocalDocNo = TxtLocalDocNo.Text;

            if (Sm.GetLue(LueItCode).Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Material Request Document is automatically generated based on this data. Do you want to proceed ?") == DialogResult.Yes)
                {
                    if (Sm.IsDteEmpty(DteUsageDt, "Usage Date") || Sm.IsMeeEmpty(MeeRemark, "Remark")) return;
                    else
                    {
                        Cursor.Current = Cursors.WaitCursor;

                        string MRDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr");
                        string ItCode = Sm.GetLue(LueItCode);

                        cml.Add(SaveDRHdr(DocNo, MRDocNo, TxtLocalDocNo.Text));
                        for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                                cml.Add(SaveDRDtl(DocNo, Row));

                        cml.Add(SaveMRHdr(MRDocNo));
                        cml.Add(SaveMRDtl(MRDocNo, ItCode, IsDocApprovalSettingNotExisted()));
                    }
                }
                else
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        DteUsageDt
                    });

                    cml.Add(SaveDRHdr(DocNo, string.Empty, TxtLocalDocNo.Text));
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                            cml.Add(SaveDRDtl(DocNo, Row));
                }
            }
            else
            {
                cml.Add(SaveDRHdr(DocNo, string.Empty, TxtLocalDocNo.Text));
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveDRDtl(DocNo, Row));
            }

            cml.Add(UpdateSalesMemo(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                IsSalesContractAlreadyProcessed() ||
                IsSOCancelledAlready() ||
                IsSOStatusFulfilledAlready()||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsQtyNotValid() ||
                IsSalesMemoNotValid();
        }

        private bool IsSalesContractAlreadyProcessed()
        {
            var DocNo = GetValue(
                "Select DocNo From TblSalesContract " +
                "Where Status='F' And Locate(Concat('##', DocNo, '##'), @Param)>0 " +
                "Limit 1; ",
                GetSelectedSODocNo()
                );
            if (DocNo.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "SC# :" + DocNo + Environment.NewLine +
                    "This document already fulfilled.");
                return true;
            }

            return false;
        }

        private bool IsDRCityNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd1, Row, 5).Length>0 &&
                    IsDRCityNotValid(Sm.GetGrdStr(Grd1, Row, 5)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "SO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                        "City : " + TxtCity.Text + Environment.NewLine + Environment.NewLine +
                        "Invalid City." + Environment.NewLine +
                        "Please update quotation."
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsDRCityNotValid(string SODocNo)
        {
            var SQL = new StringBuilder();

            string ParamDtCode = Sm.GetParameter("DTCodeOnDR");
            string ShpMCode = Sm.GetValue("Select A.ShpMCode From tblCtQtHdr A Inner Join TblSoHdr B On A.DocNo = B.CTQtDocNo Where B.Docno ='"+SODocNo+"' ");

            if (ParamDtCode != ShpMCode)
            {
                SQL.AppendLine("Select CityName From TblCity ");
                SQL.AppendLine("Where CityCode=@CityCode ");
                SQL.AppendLine("And CityCode In ( ");
                SQL.AppendLine("    Select Distinct B.CityCode ");
                SQL.AppendLine("    From TblSOHdr A ");
                SQL.AppendLine("    Inner Join TblCtQtDtl2 B On A.CtQtDocNo=B.DocNo ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("); ");
            }
            else
            {
                SQL.AppendLine("Select CityName From TblCity ");
                SQL.AppendLine("Where CityCode=@CityCode ");
                SQL.AppendLine("And CityCode In ( ");
                SQL.AppendLine("    Select CityCode From TblCity ");
                SQL.AppendLine("); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CityCode", mCity);
            Sm.CmParam<String>(ref cm, "@DocNo", SODocNo);
            return !Sm.IsDataExist(cm);
        }

        private bool IsSOProcessIndAlreadyFulfilled(string Param)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSalesMemoDtl " +
                    "Where ProcessInd='F' " +
                    "And Locate(Concat('##', DocNo, DNo, '##'), @Param)>0; "
            };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Request delivery already processed to DO to customer.");
                return true;
            }
            return false;
        }

        private bool IsSOStatusFulfilledAlready()
        {
            var DocNo = GetValue(
                "Select DocNo From TblSOHdr " +
                "Where Status In ('M', 'F') And Locate(Concat('##', DocNo, '##'), @Param)>0 " +
                "Limit 1; ",
                GetSelectedSODocNo()
                );
            if (DocNo.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "SM# :" + DocNo + Environment.NewLine +
                    "This document already fulfilled.");
                return true;
            }
            return false;
        }

        private bool IsSOCancelledAlready()
        {
           
            var DocNo = GetValue(
                "Select DocNo From TblSalesContract " +
                "Where CancelInd='Y' And Locate(Concat('##', DocNo, '##'), @Param)>0 " +
                "Limit 1; ",
                GetSelectedSODocNo()
                );
            if (DocNo.Length!=0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "SC# :" + DocNo + Environment.NewLine +
                    "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            RecomputeBalance();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                   "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                   "Packaging Unit : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                   "Sales Uom : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                   "SC# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine;

                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 8, false, "Uom (Packaging Unit) is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 10, true, Msg + "Requested Quantity (Packaging Unit) should not be 0.")|| 
                    Sm.IsGrdValueEmpty(Grd1, Row, 13, true, Msg + "Requested Quantity (Sales) should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 17, true, Msg + "Requested Quantity (Inventory) should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 32, true, Msg + "Requested Quantity1 should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 34, false, Msg + "UoM1 is empty") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 35, true, Msg + "Requested Quantity2 should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 37, false, Msg + "UoM2 is empty")
                    )
                    return true;



                if (Sm.GetGrdDec(Grd1, Row, 10) > Sm.GetGrdDec(Grd1, Row, 9))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Requested quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0) +
                        " ) should not be bigger than outstanding quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 9), 0) +
                        " ).");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 13) > Sm.GetGrdDec(Grd1, Row, 12))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Requested quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 13), 0) +
                        " ) should not be bigger than outstanding quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0) +
                        " ).");
                    return true;
                }

                if (IsDataExists(
                        "Select DocNo From TblSODtl " +
                        "Where ProcessInd='F' And Locate(Concat('##', DocNo, DNo, '##'), @Param)>0 " +
                        "Limit 1; ",
                        "##" + Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 7) + "##",
                        Msg + "This data already fulfilled."
                    )) return true;
                
            }
            return false;
        }

        private bool IsSalesMemoNotValid()
        {
            string TaxInd = Sm.GetValue("Select TaxInd From TblSalesMemoHdr Where DocNo = @Param Limit 1; ", Sm.GetGrdStr(Grd1, 0, 29));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 31) != TaxInd)
                {
                    Sm.StdMsg(mMsgType.Warning,
                    "Sales Memo# :" + Sm.GetGrdStr(Grd1, Row, 29) + Environment.NewLine +
                    "Item's Code :" + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                    "Item's Name :" + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "This document has different tax value.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 SC#.");
                return true;
            }
            return false;
        }

        private bool IsQtyNotValid()
        {
            for(int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                //decimal Qty = Sm.GetGrdDec(Grd1, Row, 10) + Sm.GetGrdDec(Grd1, Row, 30);
                decimal Qty = Sm.GetGrdDec(Grd1, Row, 10);
                if ( Qty > Sm.GetGrdDec(Grd1, Row, 27))
                {
                    Sm.StdMsg(mMsgType.Warning, "Quantity should not bigger than allowed qty ");
                    return true;
                }
            }
            return false;
        }

        private bool IsQtyRequestNotValid()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 11) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Packaging Uom : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                            "Packaging Quantity is bigger than Outstanding Packaging Quantity.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 14) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Packaging Uom : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                            "Request Quantity is bigger than Outstanding Request Quantity.");
                        return true;
                    }
                }
            }

            return false;
        }
     
        private MySqlCommand SaveDRHdr(string DocNo, string MRDocNo, string LocalDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDRHdr(DocNo, DocDt, CancelInd, DocType, ProcessInd, LocalDocNo, MRDocNo, ItCode, UsageDt, WhsCode, CtCode, SiteCode, CBDInd, SAName, SAAddress, SACityCode, SACntCode, SAPostalCD, SAPhone, SAFax, SAEmail, SAMobile, ExpVdCode, ExpDriver, ExpPlatNo, ExpMobile, ExpCityCode, ExpTTCode, AgtCode, Note, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @CancelInd, '3', @ProcessInd, @LocalDocNo, @MRDocNo, @ItCode, @UsageDt, @WhsCode, @CtCode, @SiteCode, @CBDInd, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCD, @SAPhone, @SAFax, @SAEmail, @SAMobile, @ExpVdCode, @ExpDriver, @ExpPlatNo, @ExpMobile, @ExpCityCode, @ExpTTCode, @AgtCode, @Note, @Remark, @CreateBy, CurrentDateTime()); " 
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MRDocNo", MRDocNo);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetLue(LueItCode));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetDte(DteUsageDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@CBDInd", mIsDRWithCBD ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@SAName", Sm.GetLue(LueShipAdd));
            Sm.CmParam<String>(ref cm, "@SAAddress", TxtAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCity);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCnt);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@ExpVdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@ExpDriver", Sm.GetLue(LueDriver));
            Sm.CmParam<String>(ref cm, "@ExpPlatNo", TxtPlatNo.Text);
            Sm.CmParam<String>(ref cm, "@ExpMobile", TxtMobile2.Text);
            Sm.CmParam<String>(ref cm, "@ExpCityCode", mExpCity);
            Sm.CmParam<String>(ref cm, "@ExpTTCode", Sm.GetLue(LueTtCode));
            Sm.CmParam<String>(ref cm, "@AgtCode", Sm.GetLue(LueAgtCode));
            Sm.CmParam<String>(ref cm, "@Note", MeeNote.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDRDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDRDtl(DocNo, DNo, SODocNo, SODNo, SCDocNo, QtyPackagingUnit, Qty, QtyInventory, Qty1, UoMCode1, Qty2, UoMCode2, Shipping, `Order`, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @SMDocNo, @SMDNo, @SCDocNo, @QtyPackagingUnit, @Qty, @QtyInventory, @Qty1, @UoMCode1, @Qty2, @UoMCode2, @Shipping, @Order, @Remark, @CreateBy, CurrentDateTime()); ");

            if (Sm.GetGrdDec(Grd1, Row, 14) == 0 && mIsPartial == false)
            {
                SQL.AppendLine("Update TblSalesContract Set ");
                SQL.AppendLine("    Status =  ");
                SQL.AppendLine("        Case @CancelInd When 'Y' Then 'O' ");
                SQL.AppendLine("        Else 'F' ");
                SQL.AppendLine("        End, ");
                SQL.AppendLine("    LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@SCDocNo; ");
            }
            else
            {
                SQL.AppendLine("Update TblSalesContract Set ");
                SQL.AppendLine("    Status = 'O', ");
                SQL.AppendLine("    LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@SCDocNo; ");

                mIsPartial = true;
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y": "N");
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SMDocNo", Sm.GetGrdStr(Grd1, Row, 29));
            Sm.CmParam<String>(ref cm, "@SMDNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@SCDocNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@QtyInventory", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@Qty1", Sm.GetGrdDec(Grd1, Row, 32));
            Sm.CmParam<String>(ref cm, "@UomCode1", Sm.GetGrdStr(Grd1, Row, 33));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 35));
            Sm.CmParam<String>(ref cm, "@UomCode2", Sm.GetGrdStr(Grd1, Row, 36));
            Sm.CmParam<String>(ref cm, "@Shipping", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@Order", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateSalesMemo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesMemoDtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Distinct SODocNo, SODNo ");
            SQL.AppendLine("    From TblDrDtl  ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select A.SODocNo, A.SODNo, Sum(IfNull(B.QtyPackagingUnit, 0)) As QtyPackagingUnit ");
	        SQL.AppendLine("    From ( ");
		    SQL.AppendLine("        Select Distinct SODocNo, SODNo "); 
		    SQL.AppendLine("        From TblDrDtl  ");
		    SQL.AppendLine("        Where DocNo=@DocNo ");
	        SQL.AppendLine("    ) A ");
	        SQL.AppendLine("    Inner Join TblDrDtl B On A.SODocNo=B.SODocNo And A.SODNo=B.SODNo ");
	        SQL.AppendLine("    Inner Join TblDrHdr C On B.DocNo=C.DocNo And C.CancelInd='N' ");
	        SQL.AppendLine("    Group By A.SODocNo, A.SODNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
            SQL.AppendLine("Set T1.ProcessInd =  ");
	        SQL.AppendLine("    Case When IfNull(T3.QtyPackagingUnit, 0)=0 Then 'O' ");
	        SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When IfNull(T3.QtyPackagingUnit, 0)>=T1.Qty Then 'F' Else 'P' End ");
	        SQL.AppendLine("End ");

            //SQL.AppendLine("Update TblSOHdr T Set ");
            //SQL.AppendLine("    T.Status= ");
            //SQL.AppendLine("        Case When Not Exists( ");
            //SQL.AppendLine("            Select DocNo From TblSODtl ");
            //SQL.AppendLine("            Where ProcessInd<>'O' And DocNo=T.DocNo Limit 1 ");
            //SQL.AppendLine("        ) Then 'O' Else ");
            //SQL.AppendLine("            Case When Not Exists( ");
            //SQL.AppendLine("                Select DocNo From TblSODtl ");
            //SQL.AppendLine("                Where ProcessInd<>'F' And DocNo=T.DocNo Limit 1 ");
            //SQL.AppendLine("            ) Then 'F' Else 'P' End ");
            //SQL.AppendLine("        End ");
            //SQL.AppendLine("Where T.Status<>'M' ");
            //SQL.AppendLine("And CancelInd='N' ");
            //SQL.AppendLine("And DocNo In ( ");
            //SQL.AppendLine("    Select SODocNo From TblDRDtl ");
            //SQL.AppendLine("    Where DocNo=@DocNo ");
            //SQL.AppendLine("    ); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand SaveMRHdr(string MRDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, LocalDocNo, POQtyCancelDocNo, DocDt, SiteCode, DeptCode, ReqType, BCCode, SeqNo, ItScCode, Mth, Yr, Revision, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @LocalDocNo, NULL, @DocDt, NULL, @DeptCode, @ReqType, NULL, NULL, NULL, NULL, NULL, NULL, @Remark, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", MRDocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", mLogisticDepartmentCode);
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetParameter("ReqTypeForNonBudget"));
            //Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetGrdStr(Grd1, 0, 22));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveMRDtl(string MRDocNo, string ItCode, bool NoNeedApproval)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, CancelInd, CancelReason, Status, ItCode, Qty, UsageDt, QtDocNo, QtDNo, UPrice, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, 'N', NULL, @Status, @ItCode, '1', @UsageDt, NULL, NULL, 0, @Remark,  @CreateBy, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQLDtl.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQLDtl.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
                SQLDtl.AppendLine("From TblDocApprovalSetting T ");
                SQLDtl.AppendLine("Where T.DeptCode=@DeptCode ");
                //if (mIsApprovalBySiteMandatory)
                //    SQLDtl.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
                SQLDtl.AppendLine("And T.DocType='MaterialRequest'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", MRDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetDte(DteUsageDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", mLogisticDepartmentCode);
            //Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }


        //private void UpdateSOFullFill()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select T.SODocNo, SUM(T.Outs) As outs From ( ");
        //    SQL.AppendLine("    select B.SODocNo,if(C.Qty-SUM(B.QtyPackagingUnit)=0, 0, 1) As Outs ");
        //    SQL.AppendLine("    From TblDRhdr A ");
        //    SQL.AppendLine("    Inner Join TblDrDtl B On A.DOcNo = B.DocNo ");
        //    SQL.AppendLine("    Inner Join (  ");
        //    SQL.AppendLine("        Select A.DocNo, SUM(B.QtyPackagingUnit) As Qty    ");
        //    SQL.AppendLine("        From TblSOHdr A ");
        //    SQL.AppendLine("        Inner Join TblSODtl B On A.DocNo = B.DocNo ");
        //    SQL.AppendLine("        Where A.Status = 'O' And B.DOcNo in (" + GetSelectedItemSO2() + ") ");
        //    SQL.AppendLine("        Group By A.DocNo ");
        //    SQL.AppendLine("    )C On B.SoDocNo = C.DocNo  ");
        //    SQL.AppendLine("    Where A.CancelInd = 'N' And B.SODOcNo in (" + GetSelectedItemSO2() + ") And A.ProcessInd = 'F' ");
        //    SQL.AppendLine("    Group By B.SODocNo");
        //    SQL.AppendLine(")T Group By T.SODocNo ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        var cm = new MySqlCommand()
        //        {
        //            Connection = cn,
        //            CommandText = SQL.ToString()
        //        };

        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //        {
        //            //0
        //            "SODocNo",
        //            "Outs",
        //        });

        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                if (Sm.DrDec(dr, 1) == 0)
        //                {
        //                    var cm2 = new MySqlCommand()
        //                    {
        //                        CommandText =
        //                        "Update TblSOHdr Set " +
        //                        "    Status='F', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
        //                        "Where DocNo=@DocNo "
        //                    };
        //                    Sm.CmParam<String>(ref cm2, "@DocNo", Sm.DrStr(dr, 0));
        //                    Sm.CmParam<String>(ref cm2, "@UserCode", Gv.CurrentUserCode);
        //                    Sm.ExecCommand(cm2);
        //                }
        //                else
        //                {
        //                    var cm3 = new MySqlCommand()
        //                    {
        //                        CommandText =
        //                        "Update TblSOHdr Set " +
        //                        "    Status='O', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
        //                        "Where DocNo=@DocNo "
        //                    };
        //                    Sm.CmParam<String>(ref cm3, "@DocNo", Sm.DrStr(dr, 0));
        //                    Sm.CmParam<String>(ref cm3, "@UserCode", Gv.CurrentUserCode);
        //                    Sm.ExecCommand(cm3);
        //                }
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelDRHdr());
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    cml.Add(SaveDRDtl(TxtDocNo.Text, Row));
            cml.Add(UpdateSalesMemo(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyFullfiled() ||
                IsDRAlreadyProcessedToDKO() ||
                (!ChkCancelInd.Checked && IsStatusNotValid()) ||
                (ChkCancelInd.Checked && IsDRAlreadyProcessedToDO()) ||
                IsDataProcessedToMR() ||
                IsGrdValueNotValid() ||
                IsGrdEmpty() ||
                IsMREximExisted(); 
        }

        private bool IsMREximExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.Status In ('O', 'A') ");
            SQL.AppendLine("Where A.DRDocNo Is Not Null And A.DRDocNo=@Param Limit 1;");

            string DocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);
            if (DocNo.Length>0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "You can't cancel this document." + Environment.NewLine + 
                    "You need to cancel it's material request ("+DocNo+") first."
                    );
                return true;
            }
            return false;
        }

        private bool IsDataProcessedToMR()
        {
            if (TxtMRDocNo.Text.Length > 0)
            {
                var SQLMR = new StringBuilder();

                SQLMR.AppendLine("Select T.DocNo From TblMaterialRequestDtl T ");
                SQLMR.AppendLine("Where T.DocNo = ( ");
                SQLMR.AppendLine("  Select A.MRDocNo From TblDRHdr A ");
                SQLMR.AppendLine("  Where A.DocNo = @DocNo ");
                SQLMR.AppendLine(") ");
                SQLMR.AppendLine("And T.CancelInd = 'N' ");

                var cm = new MySqlCommand() { CommandText = SQLMR.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This data is already processed to Material Request.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDRAlreadyProcessedToDKO()
        {
            var DocNo = Sm.GetValue(
                "Select DRDocNo From TblDKO " +
                "Where DRDocno = '" + TxtDocNo.Text + "' And cancelInd = 'N' And DRDocno is not null " +
                "Limit 1; ");
            if (DocNo.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "This document has been processed into DKO. ");
                return true;
            }
            return false;
        }


        private bool IsStatusNotValid()
        {
            if (Sm.GetLue(LueStatus) != "M")
            {
                Sm.StdMsg(mMsgType.Warning, "Delivery Request's status is not valid.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyFullfiled()
        {
            return IsDataExists(
                "Select ProcessInd From TblDrHdr " +
                "Where ProcessInd='F' And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already fulfilled."
                ); 
        }

        private bool IsDataAlreadyCancelled()
        {
            return IsDataExists(
                "Select CancelInd From TblDrHdr " +
                "Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled."
                ); 
        }

        private bool IsDRAlreadyProcessedToDO()
        {
            return IsDataExists(
                "Select A.DRDocNo " +
                "From TblDOCt2Hdr A " +
                "Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' " +
                "Where A.DRDocNo=@Param Limit 1;",
                TxtDocNo.Text,
                "This requested data already processed to DO."
                );
        }

        private MySqlCommand CancelDRHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDRHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, ProcessInd=@ProcessInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblDRDtl Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N"); 
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData(1);
                ShowDRHdr(DocNo);
                ShowDRDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDRHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ProcessInd, A.LocalDocNo, A.MRDocNo, E.ItCode, A.UsageDt, ");
            SQL.AppendLine("A.CtCode, A.SiteCode, A.SAName, A.SAAddress, A.SACityCode, B.CityName, A.SACntCode, D.CntName, A.SAPostalCD, ");
            SQL.AppendLine("A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.ExpVdCode, A.ExpDriver, A.ExpPlatNo, A.ExpMobile, ");
            SQL.AppendLine("A.ExpCityCode, C.CityName As ExpCityName, A.ExpTtCode, A.AgtCode, A.Note, A.Remark, A.ItCode As LogisticItCode ");
            SQL.AppendLine("From TblDRHdr A ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblCity C On A.ExpCityCode = C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode = D.CntCode ");
            SQL.AppendLine("Left Join TblMaterialRequestDtl E On A.MRDocNo = E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "ProcessInd", "LocalDocNo", "CtCode", 

                        //6-10
                        "SAName", "SAAddress", "SACityCode", "CityName", "SACntCode", 

                        //11-15
                        "CntName", "SAPostalCD",  "SAPhone", "SAFax", "SAEmail", 

                        //16-20
                        "SAMobile", "ExpVdCode", "ExpDriver", "ExpPlatNo", "ExpMobile", 

                        //21-25
                        "ExpCityCode", "ExpCityName", "ExpTtCode", "AgtCode", "Note", 

                        //26-30
                        "Remark", "ItCode", "UsageDt", "MRDocNo", "LogisticItCode",

                        //31
                        "SiteCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr("Y", Sm.DrStr(dr, c[2])); 
                        Sm.SetLue(LueStatus, Sm.DrStr(dr, c[3]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueShipAdd, Sm.DrStr(dr, c[6]));
                        TxtAddress.EditValue = Sm.DrStr(dr, c[7]);
                        mCity = Sm.DrStr(dr, c[8]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[9]);
                        mCnt = Sm.DrStr(dr, c[10]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[11]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[12]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[13]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[14]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[15]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[16]);
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueDriver, Sm.DrStr(dr, c[18]));
                        TxtPlatNo.EditValue = Sm.DrStr(dr, c[19]);
                        TxtMobile2.EditValue = Sm.DrStr(dr, c[20]);
                        mExpCity = Sm.DrStr(dr, c[21]);
                        TxtCityCode.EditValue = Sm.DrStr(dr, c[22]);
                        Sm.SetLue(LueTtCode, Sm.DrStr(dr, c[23]));
                        Sm.SetLue(LueAgtCode, Sm.DrStr(dr, c[24]));
                        MeeNote.EditValue = Sm.DrStr(dr, c[25]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[26]);
                        Sm.SetLue(LueItCode, Sm.DrStr(dr, c[27]));
                        Sm.SetDte(DteUsageDt, Sm.DrStr(dr, c[28]));
                        TxtMRDocNo.EditValue = Sm.DrStr(dr, c[29]);
                        Sm.SetLue(LueItCode, Sm.DrStr(dr, c[30]));
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[31]));
                    }, true
                );
        }

        private void ShowDRDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.QtyInventory, B.Qty, ");
            SQL.AppendLine("F.ItCode, F.ItName, F.ForeignName, F.ItCodeInternal, F.InventoryUomCode, ");
            SQL.AppendLine("G.CtItCode, G.CtItName, ");
            SQL.AppendLine("D.DocNo SMDocNo, C.DocNo SCDocNo, F.SalesUomCode, ");
            SQL.AppendLine("B.QtyPackagingUnit, E.DeliveryDt, B.Remark, B.Shipping, B.`Order`, B.SODocNo, B.SODNo, C.LocalDocNo,");
            SQL.AppendLine("B.Qty1, B.Qty2, B.UomCode1, B.UomCode2, L.UomName UomName1, M.UomName UomName2, ");
            SQL.AppendLine("(E.Qty + ");
            SQL.AppendLine("IFNULL( ");
	        SQL.AppendLine("    Case When J.ParValue = D.SiteCode Then ");
	        SQL.AppendLine("        Case When Length(I.ParValue) > 0 Then ");
	        SQL.AppendLine("            (E.Qty * I.ParValue * 0.01) ");
	        SQL.AppendLine("        ELSE 0.00 End ");
	        SQL.AppendLine("    ELSE 0.00 End ");
            SQL.AppendLine(", 0.00) - IFNULL(K.QtyPackagingUnit, 0.00)) OutstandingQty ");
            SQL.AppendLine("FROM TblDrHdr A ");
            SQL.AppendLine("INNER JOIN TblDrDtl B ON A.DocNo = B.DocNo ");
	        SQL.AppendLine("     AND A.DocType = '3' ");
            SQL.AppendLine("    AND B.DocNo = @DocNo ");
            SQL.AppendLine("INNER JOIN TblSalesContract C ON B.SCDocNo = C.DocNo ");
            SQL.AppendLine("INNER JOIN TblSalesMemoHdr D ON C.SalesMemoDocNo = D.DocNo ");
            SQL.AppendLine("INNER JOIN TblSalesMemoDtl E ON D.DocNo = E.DocNo ");
	        SQL.AppendLine("     AND E.DNo = B.SODNo ");
            SQL.AppendLine("INNER JOIN TblItem F ON E.ItCode = F.ItCode ");
            SQL.AppendLine("LEFT JOIN TblCustomerItem G ON F.ItCode = G.ItCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("   SELECT T2.SCDocNo, T2.SODocNo, T2.SODNo, ");
		    SQL.AppendLine("        Sum(T2.QtyPackagingUnit) As QtyPackagingUnit, Sum(T2.Qty) As Qty ");
		    SQL.AppendLine("        From TblDrHdr T1 ");
		    SQL.AppendLine("        Inner Join TblDrDtl T2 On T1.DocNo = T2.DocNo ");
		    SQL.AppendLine("        Inner Join ( ");
			SQL.AppendLine("            Select Distinct SODocNo, SODNo From TblDrDtl Where DocNo=@DocNo ");
		    SQL.AppendLine("        ) T3 On T2.SODocNo = T3.SODocNo And T2.SODNo = T3.SODNo ");
		    SQL.AppendLine("        Where T1.CancelInd='N' ");
		    SQL.AppendLine("        And T1.DocNo<>@DocNo ");
		    SQL.AppendLine("        Group By T2.SODocNo, T2.SODNo ");
            SQL.AppendLine(") H ON C.DocNo = H.SCDocNo AND D.DocNo = H.SODocNo AND E.DNo = H.SODNo "); 
            SQL.AppendLine("LEFT JOIN TblParameter I ON I.ParCode = 'AllowancePercentageQtyForDRSC' ");
            SQL.AppendLine("LEFT JOIN TblParameter J ON J.ParCode = 'AllowanceSiteCodeForDRSC' ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T1.SODocNo, T1.SODNo, T1.SCDocNo, SUM(T1.QtyPackagingUnit) QtyPackagingUnit ");
            SQL.AppendLine("    FROM TblDRDtl T1 ");
            SQL.AppendLine("    INNER JOIN TblDRHdr T2 ON T1.DocNO = T2.DocNo ");
            SQL.AppendLine("        AND T2.DocType = '3' ");
            SQL.AppendLine("        AND T2.CancelInd = 'N' ");
            SQL.AppendLine("        AND T1.DocNo <> @DocNo ");
            SQL.AppendLine("        AND T1.CreateDt <= @CreateDt ");
            SQL.AppendLine("        AND CONCAT(T2.DocDt, LEFT(T2.DocNo, 4)) < CONCAT(@DocDt, LEFT(@DocNo, 4)) ");
            SQL.AppendLine("    GROUP BY T1.SODocNo, T1.SODNo, T1.SCDocNo ");
            SQL.AppendLine(") K ON B.SODocNo = K.SODocNo AND B.SODNo = K.SODNo AND B.SCDocNo = K.SCDocNo ");
            SQL.AppendLine("Left Join TblUom L On B.UomCode1 = L.UomCode ");
            SQL.AppendLine("Left Join TblUom M On B.UomCode2 = M.UomCode ");;
            SQL.AppendLine("ORDER BY B.DocNo, B.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.Left(Sm.GetDte(DteDocDt), 8));
            Sm.CmParam<String>(ref cm, "@CreateDt", Sm.GetValue("Select CreateDt From TblDrHdr Where DocNo='" + TxtDocNo.Text + "'"));
            
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",
 
                    //1-5
                    "ItName", "ItCodeInternal", "SCDocNo", "SalesUomCode", "QtyPackagingUnit",

                    //6-10
                     "OutstandingQty", "Qty", "SODNo", "QtyInventory", "InventoryUomCode",
                    
                    //11-15
                    "DeliveryDt", "Remark", "Shipping", "Order", "ForeignName",
 
                    //16-20
                    "CtItCode", "CtItName", "Qty1", "UomCode1", "UomName1",  
                    
                    //21-25
                    "Qty2", "UomCode1", "UomName2", "SODocNo", "LocalDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);//itcode
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);//itname
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);//localcode
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);//sc#
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 8);//smdno
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 10);//packunit
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);//outstandingpackunit
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 5);//packqty
                    Grd.Cells[Row, 11].Value = Sm.GetGrdDec(Grd, Row, 9) - Sm.GetGrdDec(Grd, Row, 10);//balance
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 6);//oustandingsm
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 5);//reqQtySales
                    Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 12) - Sm.GetGrdDec(Grd, Row, 13);//balance
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 4);//uomsales
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 5);//reqQtyInv
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 10);//uomInv
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 11);//DeliveryDt
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 12);//Remark
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 15);//Foreignname
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 16);//CustItCode
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);//CustItName
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 13);//Shippingname
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 14);//order
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 24);//AllowaceQty
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 18);//Qty1
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 19);//UomCode1
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 20);//Uom1
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 21);//Qty2
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 22);//UomCode2
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 23);//Uom2
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 25);//Local#

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13, 14, 17, 21, 32, 35 });
            Sm.FocusGrd(Grd1, 0, 1);
            //ComputeUom();
        }

        private void ShowShippingAddressData(string CtCode, string ShipName)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ShipName", ShipName);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.Address, A.CityCode, A.CntCode, B.CityName, C.CntName, A.PostalCd, A.Phone, A.Fax, A.Email, A.Mobile " +
                    "From TblCustomerShipAddress A " +
                    "Left Join TblCity B On A.CityCode = B.CityCode " +
                    "Left Join TblCountry C On A.CntCode = C.CntCode Where A.CtCode=@CtCode And A.Name=@ShipName ",
                    new string[] 
                    { 
                        "Address", 
                        "CityCode", "CityName", "CntCode", "CntName", "PostalCd",  
                        "Phone", "Fax", "Email", "Mobile", 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAddress.EditValue = Sm.DrStr(dr, c[0]);
                        mCity = Sm.DrStr(dr, c[1]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[2]);
                        mCnt = Sm.DrStr(dr, c[3]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[6]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[7]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[8]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[9]);
                    }, false
                );
        }

        private void ShowExpeditionData(string VdCode, string ContactName)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.CmParam<String>(ref cm, "@ContactPersonName", ContactName);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.ContactPersonName, A.Position, A.ContactNumber, B.CityCode, C.CityName " +
                    "From TblVendorContactPerson A " +
                    "Left Join TblVendor B On A.VdCode = B.VdCode " +
                    "Left Join TblCity C On B.CityCode = C.CityCode Where A.VdCode=@VdCode And A.ContactPersonName=@ContactPersonName ",
                    new string[] 
                    { 
                        "ContactNumber", 
                        "CityCode", "CityName", 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtMobile2.EditValue = Sm.DrStr(dr, c[0]);
                        mExpCity = Sm.DrStr(dr, c[1]);
                        TxtCityCode.EditValue = Sm.DrStr(dr, c[2]);
                    }, false
                );
        }

        private void ShowShippingAddressData2(string SODocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SODocNo", SODocNo);

            Sm.ShowDataInCtrl( ref cm,
                    "Select A.SAName, SAAddress, A.SACityCode, B.CityName, A.SACntCode, C.CntName, SAPostalCd "+ 
                    "From TblSohdr  A "+
                    "Left Join TblCity B On A.SACityCode = B.CityCode " +
                    "Left Join TblCountry C On A.SACntCode = C.CntCode "+
                    "Where A.DocNo=@SODocNo ",
                    new string[] 
                    { 
                        "SAName", 
                        "SAAddress", "SACityCode", "CityName", "SACntCode", "CntName", 
                        "SAPostalCd"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Sm.SetLue(LueShipAdd, Sm.DrStr(dr, c[0]));
                        TxtAddress.EditValue = Sm.DrStr(dr, c[1]);
                        mCity = Sm.DrStr(dr, c[2]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[3]);
                        mCnt = Sm.DrStr(dr, c[4]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[6]);
                    }, false
                );
        }


        #endregion

        #region Additional Method

        private string GenerateDocNo2(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mDocNo = string.Empty,
            Yr = Sm.Left(DocDt, 4),
            Mth = DocDt.Substring(4, 2),


            DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType),
            CtShortCode = Sm.GetValue("Select CtShortCode From TblCustomer WHERE CtCode = @Param; ", Sm.GetLue(LueCtCode)),
            SiteShortCode = Sm.GetValue("Select ShortCode From TblSite Where SiteCode = @Param; ", Sm.GetLue(LueSiteCode)),
            DocTitle = Sm.GetParameter("DocTitle");

            SQL.Append("Select Concat(  ");
            SQL.Append("IfNull((  ");
            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From (  ");
            SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, LENGTH(CONCAT(IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr)))=Concat(IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr)  ");
            if (Sm.GetGrdStr(Grd1, 0, 29).Contains("KSM"))
                SQL.Append("       And Substr(DocNo, 6, 3) = 'KSM' ");
            else
                SQL.Append("       And Substr(DocNo, 6, 3) != 'KSM' ");
            SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1  ");
            SQL.Append("       ) As Temp  ");
            SQL.Append("   ), '0001')  ");
            if (Sm.GetGrdStr(Grd1, 0, 29).Contains("KSM"))
            {
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("' ");
            }
            SQL.Append(", '/', IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.Append(") As DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@CtShortCode", CtShortCode);
                Sm.CmParam<String>(ref cm, "@SiteShortCode", SiteShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }

        internal void GenerateLocalDocNo()
        {
            string LocalDocNo = string.Empty;

            // VIN - 27/1/2021 - Nomer Urut Local Docno tidak berdasarkan Local DocNo sales contract 
            if (mIsSalesMemoGenerateLocalDocNo)
                LocalDocNo = GenerateLocalDocNo(Sm.GetDte(DteDocDt), "DR", "TblDRHdr");
            else
                LocalDocNo = TxtLocalDocNo.Text;

            TxtLocalDocNo.Text = LocalDocNo;
        }

        private string GenerateLocalDocNo(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mLocalDocNo = string.Empty,
            Yr = Sm.Left(DocDt, 4),
            Mth = DocDt.Substring(4, 2),


            DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType),
            CtShortCode = Sm.GetValue("Select CtShortCode From TblCustomer WHERE CtCode = @Param; ", Sm.GetLue(LueCtCode)),
            SiteShortCode = Sm.GetValue("Select ShortCode From TblSite Where SiteCode = @Param; ", Sm.GetLue(LueSiteCode)),
            DocTitle = Sm.GetParameter("DocTitle");

            SQL.Append("Select Concat(  ");
            SQL.Append("IfNull((  ");
            SQL.Append("   Select Right(Concat('0000', Convert(LocalDocNo+1, Char)), 4) From (  ");
            SQL.Append("       Select Convert(Left(LocalDocNo, 4), Decimal) As LocalDocNo From " + Tbl);
            SQL.Append("       Where Right(LocalDocNo, LENGTH(CONCAT(@Mth,'/', @Yr)))=Concat(@Mth,'/', @Yr)  ");
            SQL.Append("       And Substring(LocalDocNo, 10, LENGTH(IfNull(@SiteShortCode, '')))=IfNull(@SiteShortCode, '')   ");
            if (Sm.GetGrdStr(Grd1, 0, 29).Contains("KSM"))
                SQL.Append("       And Substr(LocalDocNo, 6, 3) = 'KSM' ");
            else
                SQL.Append("       And Substr(LocalDocNo, 6, 3) != 'KSM' ");
            SQL.Append("       Order By Left(LocalDocNo, 4) Desc Limit 1  ");
            SQL.Append("       ) As Temp  ");
            SQL.Append("   ), '0001')  ");
            if (Sm.GetGrdStr(Grd1, 0, 29).Contains("KSM"))
            {
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("' ");
            }
            SQL.Append(", '/', IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.Append(") As LocalDocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@CtShortCode", CtShortCode);
                Sm.CmParam<String>(ref cm, "@SiteShortCode", SiteShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LocalDocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mLocalDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mLocalDocNo;
        }
        
        

        #region credit limit

        private bool CheckCreditLimit()
        {
            if (mIsCreditLimitValidate && !mIsDRWithCBD)
            {
                decimal DOSI = 0; 
                decimal SIIP = 0m;
                decimal IPVC = 0m;
                decimal RIDO =0m;
                decimal CL = 0m;
                decimal Outs = 0m;
                decimal DRNow = 0;
                decimal DRDO = 0;
                string CTQT = string.Empty;

                //nentuin customer code
                string CtCode = string.Empty;
                CtCode = Sm.GetLue(LueCtCode);

                //nentuin nilai credit limit
                CL = Decimal.Parse(Sm.GetValue("Select CreditLimit From TblCtQthdr Where CtCode='" + CtCode + "' And Status = 'A' And ActInd = 'Y' "));

                //nentuin customer quotation yang aktif
                CTQT = Sm.GetValue("Select DocNo From TblCtQthdr Where CtCode='" + CtCode + "' And Status = 'A' And ActInd = 'Y' ");

                if (GetDOSI(CtCode) == 0)
                {
                    DOSI = 0;
                }
                else
                {
                    DOSI = GetDOSI(CtCode);
                }


                //nentuin amount DR yang sdg dibuat
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    DRNow += (Sm.GetGrdDec(Grd1, Row, 17) * Sm.GetGrdDec(Grd1, Row, 21));
                }

                SIIP = GetSIIP(CtCode);
                IPVC = GetIPVC(CtCode);
                RIDO = GetRIDO(CtCode, CTQT);
                DRDO = GetDRDO(CtCode);

                Outs = DOSI + SIIP + IPVC + DRNow + DRDO - RIDO;

                if (CL < Outs)
                {
                    Sm.StdMsg(
                    mMsgType.Warning,
                    "Credit Limit : " + Sm.FormatNum(CL, 0) + Environment.NewLine +
                    "Outstanding : " + Sm.FormatNum(Outs, 0) + Environment.NewLine +
                    "Balance : " + Sm.FormatNum(CL - Outs, 0) + Environment.NewLine +
                    "Total amount should not be greater than Credit Limit."
                    );
                    return true;
                }
                return false;
            }
            return false;
        }

        //Sales Retur  terhadap Quotation aktif
        private decimal GetRIDO(string CtCode, string CtQtDocNo)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select ifnull(SUM(T.Amt), 0) As RIDOAMT From ( " +
                    "   Select B.ItCode, (A.Qty* C.UPrice) As AMt   " +
                    "   from TblRecvCtDtl A  " +
                    "   Inner Join TblDoCt2Dtl B On A.DOCtDocNo = B.DocNo And A.DOCtDno = B.Dno   " +
                    "   Inner Join (  " +
                    "       Select A.DocNo, B.DNo, C.UPrice  " +
                    "       From TblDOCt2Hdr A  " +
                    "       Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N'  " +
                    "       Left Join  " +
                    "       (  " +
                    "           Select Distinct A.DocNo, G.ItCode, C.CurCode,  " +
                    "           ((E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0)))+ ((E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0)))*0.01*D.TaxRate)) As UPrice  " +
                    "           From TblDrhdr A  " +
                    "           Inner Join TblDrDtl B On A.DocNo = B.DocNo  " +
                    "           Inner Join TblSOHdr C On B.SODocNo = C.DocNo And C.cancelInd = 'N'  " +
                    "           Inner Join TblSODtl D On C.DocNo = D.DocNo  " +
                    "           Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo  " +
                    "           Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo  " +
                    "           Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo  " +
                    "           Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  " +
                    "           Inner Join TblItem I On G.ItCode=I.ItCode  " +
                    "           Where A.cancelInd = 'N' And A.CtCode = @CtCode And E.DocNo = @CtQtDocNo  " +
                    "        ) C On A.DrDocno = C.DocNo And B.ItCode=C.ItCode  " +
                    "   )C On B.DocNo = C.DocNo And B.Dno = C.Dno  " +
                    ")T "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@CtQtDocNo", CtQtDocNo);
            return Sm.GetValueDec(cm);
        }

        //Incoming payment yang belum divoucherkan
        private decimal GetIPVC(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select ifnull(SUM(A.Amt), 0) As AmtIPVC From TblIncomingpaymentHdr A " +
                    "Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo " +
                    "Where A.cancelInd = 'N' And A.CtCode = @CtCode And " +
                    "B.DocNo Not In ( " +
                    "    Select A.VoucherRequestDocNo " +
                    "    From TblVoucherHdr A " +
                    "    Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocno = B.Docno " +
                    "    Where A.CancelInd = 'N' ); "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        //SalesInvoice yang belum di incoming paymentkan
        private decimal GetSIIP(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select ifnull(Sum(Amt), 0) As AmtSIIP from tblsalesinvoicehdr t where ctcode = @CtCode and cancelind='N' and " +
                    "not exists( " +
                    "    select a.docno " +
                    "    from tblincomingpaymenthdr a, tblincomingpaymentdtl b " +
                    "    where a.docno=b.docno " +
                    "    and status<>'C'  " +
                    "    and cancelind='N' " +
                    "    and b.InvoiceDocNo=t.docno and b.InvoiceType='1' " +
                    "); "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        //delivery order yang belum di sales invoicekan
        private decimal GetDOSI(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select IFNULL(SUM(T.Qty * T.PriceAfterTax), 0) AmtDOSI " +
                    "From ( " +
                    "   Select '1' As DocType, " +
                    "   If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, " +
                    "   (G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0))) +  " +
                    "   ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*E.TaxRate) As PriceAfterTax " +
                    "   From TblDOCt2Hdr A " +
                    "   Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' " +
                    "   Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo " +
                    "   Inner Join TblSOHdr D On C.SODocNo=D.DocNo And D.CancelInd = 'N'  " +
                    "   Inner Join TblSODtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo " +
                    "   Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo   " +
                    "   Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo " +
                    "   Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo  " +
                    "   Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo " +
                    "   Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode  " +
                    "   Inner Join TblDRHdr M On A.DRDocNo=M.DocNo " +
                    "   Where A.CtCode=@CtCode " +
                    "   Union All " +
                    "   Select '2' As DocType, " +
                    "   If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, " +
                    "   (G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0))) + " +
                    "   ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*E.TaxRate) As PriceAfterTax " +
                    "   From TblDOCt2Hdr A  " +
                    "   Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' " +
                    "   Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo  " +
                    "   Inner Join TblSOHdr D On C.SODocNo=D.DocNo And D.CancelInd = 'N'  " +
                    "   inner Join TblSODtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo " +
                    "   Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo " +
                    "   Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo " +
                    "   Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo  " +
                    "   Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo " +
                    "   Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode " +
                    "   Where A.CtCode=@CtCode " +
                    " )T; "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        //Delivery request yang belum di DO kan
        private decimal GetDRDO(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =

                    "Select  ifnull(SUM((B.Qty* " +
                    "((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))+ ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*E.TaxRate)))), 0)  PriceAfterTax " +
                    "From TblDrhdr A " +
                    "Inner Join TblDRDtl B On A.DocNo = B.DocNO " +
                    "Inner Join TblSOHdr D On B.SODocNo=D.DocNo And D.CancelInd = 'N'  " +
                    "Inner Join TblSODtl E On B.SODocNo=E.DocNo And B.SODNo=E.DNo " +
                    "Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo " +
                    "Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo " +
                    "Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo " +
                    "Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo " +
                    "Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode  " +
                    "Where A.ProcessInd = 'O' And A.Cancelind = 'N' And A.CtCode = @CtCode "
               
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        #endregion

        private bool IsDocApprovalSettingNotExisted()
        {
            if (!Sm.IsDataExist(
                    "Select DocType From TblDocApprovalSetting " +
                    "Where UserCode Is not Null " +
                    "And DocType='MaterialRequest' " +
                    "And DeptCode='" + mLogisticDepartmentCode + "' Limit 1"
                ))
                //Sm.StdMsg(mMsgType.Warning, "Nobody will approve this request.");
                return true;
            else
                return false;
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'O' As Col1, 'Outstanding' As Col2 Union All ");
            SQL.AppendLine("Select 'F', 'Fulfilled' Union All ");
            SQL.AppendLine("Select 'M', 'Manual Fulfilled';");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void SetLueItCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.ItCode As Col1, T.ItName As Col2 ");
            SQL.AppendLine("From TblItem T ");
            SQL.AppendLine("Where T.ActInd = 'Y' And T.ItCtCode = @ItCtCode ");
            SQL.AppendLine("Order By T.ItName Asc ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@ItCtCode", mLogisticItemCategoryCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void ShowStockInfo(int Row)
        {
            var SQL = new StringBuilder();
            string Msg = string.Empty;
                
            SQL.AppendLine("Select IfNull(( ");
            SQL.AppendLine("    Select Sum(Qty) ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where ItCode=@ItCode And Qty>0 ");
            SQL.AppendLine("    ), 0) -");
            SQL.AppendLine("IfNull(( ");
	        SQL.AppendLine("    Select Sum(B.QtyInventory) ");
		    SQL.AppendLine("    From TblDrhdr A ");
		    SQL.AppendLine("    Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
		    SQL.AppendLine("    Inner Join TblSOHdr C On B.SODocNo = C.DocNo ");
            SQL.AppendLine("    Inner Join TblSODtl D On B.SODocNo = D.DocNo And B.SODNo = D.DNo ");
            SQL.AppendLine("    Inner Join TblCtQtDtl E On C.CtQtDocNo=E.DocNo And D.CtQtDNo=E.DNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl F On E.ItemPriceDocNo=F.DocNo And E.ItemPriceDNo=E.DNo And F.ItCode=@ItCode "); 
	        SQL.AppendLine("    Where A.CancelInd = 'N' And A.ProcessInd = 'O' ");
		    SQL.AppendLine("), 0) As Qty; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[]{ "Qty" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                            Msg =
                                "Item Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                                "Item Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                                "Uom (Inventory) : " + Sm.GetGrdStr(Grd1, Row, 18) + Environment.NewLine +
                                "Available Stock : " + ((Sm.DrDec(dr, 0) < 0) ? "0" : Sm.FormatNum(Sm.DrDec(dr, 0), 0));
                    }
                    dr.Close();
                    dr.Dispose();
                }
                cm.Dispose();
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No available stock.");
        }

        //internal string GetSelectedSONumber()
        //{
        //    var SQL = string.Empty;
        //    if (Grd1.Rows.Count != 1)
        //    {
        //        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //        {
        //            if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
        //            {
        //                if (SQL.Length != 0) SQL += ", ";
        //                SQL +=
        //                    "##" +
        //                    Sm.GetGrdStr(Grd1, Row, 5) +
        //                    Sm.GetGrdStr(Grd1, Row, 7) +
        //                    "##";
        //            }
        //        }
        //    }
        //    return (SQL.Length == 0 ? "##XXX##" : SQL);
        //}

        private void SetLueCtCode2(ref DXE.LookUpEdit Lue, string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode As Col1, ");
            if (mIsCustomerComboShowCategory)
                SQL.AppendLine("Concat(A.CtName, ' [', IfNull(B.CtCtName, ''), '] ') As Col2 ");
            else 
                SQL.AppendLine("A.CtName As Col2 ");
            SQL.AppendLine("From TblCustomer A ");
            if (mIsCustomerComboShowCategory)
            {
                SQL.AppendLine("Left Join TblCustomerCategory B On A.CtCtCode = B.CtCtCode ");
            }
            if (CtCode.Length <= 0)
                SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("Order By A.CtName; ");

            Sm.SetLue2(
               ref Lue,
               SQL.ToString(),
               0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode As Col1, ");
            if (mIsCustomerComboShowCategory)
                SQL.AppendLine("Concat(A.CtName, ' [', IfNull(B.CtCtname, ''), '] ') as Col2 ");
            else
                SQL.AppendLine("A.CtName As Col2 ");
            SQL.AppendLine("From TblCustomer A ");
            if (mIsCustomerComboShowCategory)
                SQL.AppendLine("Left Join TblCustomerCategory B On A.CtCtCode = B.CtCtCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' And A.CtCode In ( ");
            SQL.AppendLine("   Select Distinct CtCode From tblSOHdr Where Status In ('O', 'P') And OverSeaInd = 'N' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By A.CtName; ");

            Sm.SetLue2(
               ref Lue,
               SQL.ToString(),
               0, 35, false, true, "Code", "Name", "Col2", "Col1");

            //Sm.SetLue2(
            //    ref Lue,
            //    "Select CtCode As Col1, CtName As Col2 " +
            //    "From TblCustomer " +
            //    "Where ActInd = 'Y' And CtCode In " +
            //    "(Select Distinct CtCode From tblSOHdr Where Status In ('O', 'P') And OverSeaInd = 'N' ) Order By CtName;",
            //    0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueVdCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select VdCode As Col1, VdName As Col2 From TblVendor " +
                "Where ActInd='Y' " +
                "And VdCtCode In (Select ParValue From TblParameter Where ParCode='VdCtCodeExpedition') " +
                "Order By VdName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueAgtCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Distinct B.AgtCode As Col1, C.Agtname As Col2  "+  
                "From TblSOHdr A "+
                "Inner Join TblSODtl B On A.DocNo = B.DocNo "+
                "Inner Join TblAgent C On B.AgtCode = C.AgtCode " +
                "Where C.ActInd = 'Y' And A.CtCode = '"+CtCode+"' Order By AgtName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueDriver(ref LookUpEdit Lue, string VdCode)
        {
            Sm.SetLue3(
                ref Lue,
                "Select ContactPersonName As Col1, Position As Col2, ContactNumber As Col3 From TblVendorContactPerson Where VdCode = '"+VdCode+"'" +
                "Order By ContactPersonName",
                30, 35, 35, true, true, true, "Name", "Position", "Phone", "Col1", "Col1");
        }

        private void SetLueShippingAddress(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Col1, Col2, Col3, Col4 From ( ");
                SQL.AppendLine("Select A.Dno As Col1, A.Name As Col2, Concat( A.Address, ' ',B.CityName) As Col3, A.Phone As Col4 From TblCustomershipaddress A ");
                SQL.AppendLine("Left Join tblcity B on A.CityCode = B.CityCode ");
                SQL.AppendLine("Where A.CtCode='" + CtCode + "' ");
                if (TxtDocNo.Text.Length != 0)
                {
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select Distinct '000' As Col1, A.SAName As Col2, Concat(A.SAAddress, ' ', 'B.CityName') As Col3, A.SAPhone As Col4 ");
                    SQL.AppendLine("From TblDRHdr A ");
                    SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
                    SQL.AppendLine("Where A.CtCode='" + CtCode + "' ");
                }
                if (TxtDocNo.Text.Length == 0 && Sm.GetLue(LueCtCode) == Sm.GetParameter("OnlineCtCode"))
                {
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select '' As Col1, A.SAName As Col2, A.SAAddress As Col3, '' As Col4 ");
                    SQL.AppendLine("From TblSohdr  A ");
                    SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
                    SQL.AppendLine("Left Join TblCountry C On A.SACntCode = C.CntCode ");
                    SQL.AppendLine("Where A.CtCode='" + CtCode + "' ");
                }
                SQL.AppendLine(") Tbl Order By Col2");

                Sm.SetLue4(
                    ref Lue,
                    SQL.ToString(),
                    0, 100, 250, 50, false, true, true, true, "Code", "Name", "Address", "Phone", "Col2", "Col2");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ComputeUom(int index)
        {
            decimal QtyUomConvert12 = 0m, QtyUomConvert21 = 0m, QtyPackaging = 0m;
            string UomSales, UomSales1, UomInv;


            if (Sm.GetGrdDec(Grd1, index, 10) != 0 && Sm.GetGrdStr(Grd1, index, 1).Length != 0)
            {
                try
                {
                    QtyPackaging = Sm.GetGrdDec(Grd1, index, 10);
                    UomSales1 = Sm.GetValue("Select SalesUomCode From TblItem Where ItCode = '" + Sm.GetGrdStr(Grd1, index, 1) + "' ");

                    string SQtyUomConvert12 = Sm.GetValue("Select Qty From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, index, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, index, 8) + "' ");
                    string SQtyUomConvert21 = Sm.GetValue("Select Qty2 From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, index, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, index, 8) + "' ");

                    UomSales = Sm.GetGrdStr(Grd1, index, 15); 
                    UomInv = Sm.GetGrdStr(Grd1, index, 18); 

                    if (SQtyUomConvert12.Length > 0)
                    {
                        QtyUomConvert12 = Decimal.Parse(SQtyUomConvert12);
                    }
                    else
                    {
                        QtyUomConvert12 = 0;
                    }

                    if (SQtyUomConvert21.Length > 0)
                    {
                        QtyUomConvert21 = Decimal.Parse(SQtyUomConvert21);
                    }
                    else
                    {
                        QtyUomConvert21 = 0;
                    }

                    if (UomSales1 == UomSales)
                    {
                        Grd1.Cells[index, 13].Value = QtyPackaging * QtyUomConvert12;
                        Grd1.Cells[index, 11].Value = Sm.GetGrdDec(Grd1, index, 9) - Sm.GetGrdDec(Grd1, index, 10);
                        Grd1.Cells[index, 14].Value = Sm.GetGrdDec(Grd1, index, 12) - Sm.GetGrdDec(Grd1, index, 13);

                        if (UomSales == UomInv)
                        {
                            Grd1.Cells[index, 17].Value = Sm.GetGrdDec(Grd1, index, 13);
                        }
                        else
                        {
                            if (UomInv == UomSales1)
                            {
                                Grd1.Cells[index, 17].Value = QtyPackaging * QtyUomConvert12;
                            }
                            else
                            {
                                Grd1.Cells[index, 17].Value = QtyPackaging * QtyUomConvert21;
                            }
                        }
                    }
                    else
                    {
                        Grd1.Cells[index, 13].Value = QtyPackaging * QtyUomConvert21;
                        Grd1.Cells[index, 11].Value = Sm.GetGrdDec(Grd1, index, 9) - Sm.GetGrdDec(Grd1, index, 10);
                        Grd1.Cells[index, 14].Value = Sm.GetGrdDec(Grd1, index, 12) - Sm.GetGrdDec(Grd1, index, 13);

                        if (UomSales == UomInv)
                        {
                            Grd1.Cells[index, 17].Value = Sm.GetGrdDec(Grd1, index, 13);
                        }
                        else
                        {
                            if (UomInv == UomSales1)
                            {
                                Grd1.Cells[index, 17].Value = QtyPackaging * QtyUomConvert12;
                            }
                            else
                            {
                                Grd1.Cells[index, 17].Value = QtyPackaging * QtyUomConvert21;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        public void ComputeUom()
        {
            decimal QtyPackaging = 0m, QtyUomConvert12 = 0m, QtyUomConvert21 = 0m;
            string UomSales, UomSales1, UomInv;


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 10) != 0 && Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                {
                    try
                    {
                        QtyPackaging = Sm.GetGrdDec(Grd1, Row, 10);
                        UomSales = Sm.GetGrdStr(Grd1, Row, 15);
                        UomInv = Sm.GetGrdStr(Grd1, Row, 18);
                        UomSales1 = Sm.GetValue("Select SalesUomCode From TblItem Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' ");

                        string SQtyUomConvert12 = Sm.GetValue("Select ifnull(Qty, 0) As Qty From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ");
                        string SQtyUomConvert21 = Sm.GetValue("Select ifnull(Qty2, 0) As Qty2 From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ");

                        if (SQtyUomConvert12.Length > 0)
                        {
                            QtyUomConvert12 = Decimal.Parse(SQtyUomConvert12);
                        }
                        else
                        {
                            QtyUomConvert12 = 0;
                        }

                        if (SQtyUomConvert21.Length > 0)
                        {
                            QtyUomConvert21 = Decimal.Parse(SQtyUomConvert21);
                        }
                        else
                        {
                            QtyUomConvert21 = 0;
                        }

                        if (UomSales1 == UomSales)
                        {
                            Grd1.Cells[Row, 13].Value = QtyPackaging * QtyUomConvert12;
                            Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                            Grd1.Cells[Row, 14].Value = Sm.GetGrdDec(Grd1, Row, 12) - Sm.GetGrdDec(Grd1, Row, 13);

                            if (UomSales == UomInv)
                            {
                                Grd1.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 13);
                            }
                            else
                            {
                                if (UomInv == UomSales1)
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert12;
                                }
                                else
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert21;
                                }
                            }
                        }
                        else
                        {
                            Grd1.Cells[Row, 13].Value = QtyPackaging * QtyUomConvert21;
                            Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                            Grd1.Cells[Row, 14].Value = Sm.GetGrdDec(Grd1, Row, 12) - Sm.GetGrdDec(Grd1, Row, 13);

                            if (UomSales == UomInv)
                            {
                                Grd1.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 13);
                            }
                            else
                            {
                                if (UomInv == UomSales1)
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert12;
                                }
                                else
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert21;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("" + ex + "");
                    }
                }
            }
        }

        internal void ComputeAllowanceQty()
        {
            if (Sm.GetLue(LueSiteCode) == mAllowanceSiteCodeForDRSC)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    decimal AllowanceQty = (Convert.ToDecimal(mAllowancePercentageQtyForDRSC) * 0.01m) * Sm.GetGrdDec(Grd1, i, 27);
                    Grd1.Cells[i, 27].Value = AllowanceQty + Sm.GetGrdDec(Grd1, i, 27);
                }
            }
        }
        
        internal string GetSelectedItemSO()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ",";
                        SQL +=
                            (
                            Sm.GetGrdStr(Grd1, Row, 1) + // ItCode
                            Sm.GetGrdStr(Grd1, Row, 5) + // SC#
                            Sm.GetGrdStr(Grd1, Row, 29) + // SM#
                            Sm.GetGrdStr(Grd1, Row, 7) // SMD#
                            );
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedItemSO2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            ("'" +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            "'");
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void RecomputeBalance()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, ");
            SQL.AppendLine("(B.QtyPackagingUnit-IfNull(C.QtyPackagingUnit, 0)) As OutstandingQtyPackagingUnit,  ");
            SQL.AppendLine("(B.Qty-IfNull(C.Qty, 0)) As OutstandingQty ");
            SQL.AppendLine("From TblSOHdr A ");
            SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo and B.ProcessInd<>'F' ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.SODocNo As DocNo, T2.SODNo As DNo,  ");
	        SQL.AppendLine("    Sum(IfNull(T2.QtyPackagingUnit, 0)) As QtyPackagingUnit, Sum(IfNull(T2.Qty, 0)) As Qty  ");
	        SQL.AppendLine("    From TblDRHdr T1  ");
	        SQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
	        SQL.AppendLine("    Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo And T3.CancelInd='N' And T3.Status Not In ('M', 'F') And T3.CtCode=@CtCode ");
	        SQL.AppendLine("    Inner Join TblSODtl T4 On T2.SODocNo=T4.DocNo And T2.SODNo=T4.DNo and T4.ProcessInd<>'F' ");
	        SQL.AppendLine("    Where T1.CancelInd='N' ");
	        SQL.AppendLine("    Group By T2.SODocNo, T2.SODNo ");
            SQL.AppendLine(") C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            SQL.AppendLine("And A.Status Not In ('M', 'F') ");
            SQL.AppendLine("And A.CtCode=@CtCode ");
            SQL.AppendLine("And Locate(Concat('##', A.DocNo, B.DNo, '##'), @SelectedSO)>0 ");
            SQL.AppendLine("Order By A.DocNo, B.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SelectedSO", GetSelectedSO());
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                        new string[] 
                        { 
                            //0
                           "DocNo",  

                            //1-3
                           "DNo", "OutstandingQtyPackagingUnit", "OutstandingQty"
                        }
                        );
                    if (dr.HasRows)
                    {
                        Grd1.ProcessTab = true;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), Sm.DrStr(dr, 0)) &&
                                    Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 7), Sm.DrStr(dr, 1)))
                                {
                                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 2);
                                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 3);
                                    break;
                                }
                            }
                        }
                        Grd1.EndUpdate();
                    }
                    dr.Close();
                    ComputeUom();
                }
            }
        }

        private bool IsDataExists(string SQL, string Param, string Warning)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        private string GetValue(string SQL, string Param)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            return Sm.GetValue(cm);
        }

        private string GetSelectedSODocNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 5) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedSO()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 7) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        public static string GetNumber(string palet)
        {
            string number = string.Empty;
            for (int ind = 0; ind < palet.Length; ind++)
            {
                if (Char.IsNumber(palet[ind]) == true)
                {
                    number = number + palet[ind];
                }

            }
            return number;
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<DRHdr>();
            var ldtl = new List<DRDtl>();

            string[] TableName = { "DRHdr", "DRDtl", "DRDtl2" };

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();
            
            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LocalDocNo As DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.Ctname As Customer,");
            SQL.AppendLine("A.ExpDriver, A.ExpPlatNo, C.TTName, D.VdName, A.Remark,  ");
            SQL.AppendLine("A.DocNo As DocNoDR, H.ItName, DATE_FORMAT(A.UsageDt,'%d %M %Y')As UsageDt, A.SAName, A.SAAddress,"); // tambahan
            SQL.AppendLine("E.CityName, F.CntName, A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, G.UserName, ");
            SQL.AppendLine("I.InventoryUomCode, I.UomCode1, I.UomCode2, ");
            if (Sm.GetLue(LueSiteCode) == "001")
            {
                SQL.AppendLine("(Select ParValue From TblParameter where ParCode = 'DRSCSpinningPrintOutSignName1') Sign1, ");
                SQL.AppendLine("(Select ParValue From TblParameter where ParCode = 'DRSCSpinningPrintOutSignName2') Sign2, ");
                SQL.AppendLine("(Select ParValue From TblParameter where ParCode = 'DRSCSpinningPrintOutSignName3') Sign3, ");
                SQL.AppendLine("Null As Sign4 ");
            }
            else if (Sm.GetLue(LueSiteCode) == "002")
            {
                SQL.AppendLine("(Select ParValue From TblParameter where ParCode = 'DRSCWeavingPrintOutSignName1') Sign1, ");
                SQL.AppendLine("(Select ParValue From TblParameter where ParCode = 'DRSCWeavingPrintOutSignName2') Sign2, ");
                SQL.AppendLine("(Select ParValue From TblParameter where ParCode = 'DRSCWeavingPrintOutSignName3') Sign3, ");
                SQL.AppendLine("(Select ParValue From TblParameter where ParCode = 'DRSCWeavingPrintOutSignName4') Sign4 ");
            }
            else
                SQL.AppendLine("Null as Sign1, Null as Sign2, Null as Sign3, Null as Sign4 ");
            SQL.AppendLine("From TblDRHdr A  ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("left Join Tbltransporttype C On A.ExpTTCode = C.TTCode ");
            SQL.AppendLine("left Join TblVendor D On A.ExpVdCode = D.VdCode ");
            SQL.AppendLine("Left Join TblCity E On A.SACityCode=E.CityCode ");
            SQL.AppendLine("Left Join TblCountry F On A.SACntCode = F.CntCode ");
            SQL.AppendLine("Left Join TblUser G On A.Createby = G.UserCode ");
            SQL.AppendLine("Left Join TblItem H On A.ItCode = H.ItCode ");
            SQL.AppendLine("Inner Join( ");
            SQL.AppendLine("    Select A.DocNo, E.InventoryUomCode, A.UomCode1, A.UomCode2 ");
            SQL.AppendLine("    From TblDRDtl A ");
            SQL.AppendLine("    INNER JOIN TblSalesContract B ON A.SCDocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblSalesMemoHdr C ON B.SalesMemoDocNo = C.DocNo ");
            SQL.AppendLine("    INNER JOIN TblSalesMemoDtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        AND D.DNo = A.SODNo ");
            SQL.AppendLine("    INNER JOIN TblItem E ON D.ItCode = E.ItCode ");
            SQL.AppendLine("    Where A.DocNo = @DocNo ");
            SQL.AppendLine("    Order By A.DNo ");
            SQL.AppendLine("    Limit 1 ");
            SQL.AppendLine(")I On I.DocNo = A.DocNo");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.cancelInd ='N'  ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "DocNo",
                         //1
                         "DocDt",
                         "Customer",
                         "ExpDriver", 
                         "ExpPlatNo",
                         "TTName",
                         //6-10
                         "VdName",
                         "Remark",
                         "DocNoDR",
                         "ItName",
                         "UsageDt",
                         //11-15
                         "SAName",
                         "SAAddress",
                         "CityName",
                         "CntName",
                         "SAPostalCd",
                         //16-20
                         "SAPhone",
                         "SAFax",
                         "SAEmail",
                         "SAMobile",
                         "UserName",

                         //21-25
                         "InventoryUomCode",
                         "UomCode1", 
                         "UomCode2", 
                         "Sign1", 
                         "Sign2",

                         //26-27
                         "Sign3",
                         "Sign4"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DRHdr()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            Customer = Sm.DrStr(dr, c[2]),
                            ExpDriver = Sm.DrStr(dr, c[3]),
                            ExpPlatNo = Sm.DrStr(dr, c[4]),
                            ExpTT = Sm.DrStr(dr, c[5]),

                            ExpVd = Sm.DrStr(dr, c[6]),
                            Remark = Sm.DrStr(dr, c[7]),
                            DocNoDR = Sm.DrStr(dr, c[8]),
                            ItCode = Sm.DrStr(dr, c[9]),
                            UsageDt = Sm.DrStr(dr, c[10]),

                            SAName = Sm.DrStr(dr, c[11]),
                            SAAddress = Sm.DrStr(dr, c[12]),
                            CityName = Sm.DrStr(dr, c[13]),
                            CntName = Sm.DrStr(dr, c[14]),
                            SAPostalCd = Sm.DrStr(dr, c[15]),

                            SAPhone = Sm.DrStr(dr, c[16]),
                            SAFax = Sm.DrStr(dr, c[17]),
                            SAEmail = Sm.DrStr(dr, c[18]),
                            SAMobile = Sm.DrStr(dr, c[19]),
                            UserName = Sm.DrStr(dr, c[20]),

                            InventoryUomCode = Sm.DrStr(dr, c[21]),
                            UomCode1 = Sm.DrStr(dr, c[22]),
                            UomCode2 = Sm.DrStr(dr, c[23]),
                            Sign1 = Sm.DrStr(dr, c[24]),
                            Sign2 = Sm.DrStr(dr, c[25]),

                            Sign3 = Sm.DrStr(dr, c[26]),
                            Sign4 = Sm.DrStr(dr, c[27]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("SELECT C.LocalDocNo, F.ItCode, F.ItName, F.ItCodeInternal, G.CtName, F.InventoryUomCode, ");
                SQLDtl.AppendLine("B.QtyPackagingUnit, B.Remark, B.Shipping,  ");
                SQLDtl.AppendLine("B.Qty1, B.Qty2, B.UomCode1, B.UomCode2");
                SQLDtl.AppendLine("FROM TblDrHdr A   ");
                SQLDtl.AppendLine("INNER JOIN TblDrDtl B ON A.DocNo = B.DocNo ");
                SQLDtl.AppendLine("    AND A.DocType = '3' ");
                SQLDtl.AppendLine("    AND B.DocNo = @DocNo ");
                SQLDtl.AppendLine("INNER JOIN TblSalesContract C ON B.SCDocNo = C.DocNo ");
                SQLDtl.AppendLine("INNER JOIN TblSalesMemoHdr D ON C.SalesMemoDocNo = D.DocNo ");
                SQLDtl.AppendLine("INNER JOIN TblSalesMemoDtl E ON D.DocNo = E.DocNo ");
                SQLDtl.AppendLine("    AND E.DNo = B.SODNo ");
                SQLDtl.AppendLine("INNER JOIN TblItem F ON E.ItCode = F.ItCode ");
                SQLDtl.AppendLine("LEFT JOIN TblCustomer G ON A.CtCode = G.CtCode ");
                SQLDtl.AppendLine("ORDER BY B.DocNo, B.DNo ");


                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ItCode" ,
                     //1-5
                     "ItName",
                     "ItCodeInternal",
                     "CtName", 
                     "Shipping",
                     "InventoryUomCode", 

                     //6-10
                     "QtyPackagingUnit",
                     "Qty1", 
                     "UomCode1", 
                     "Qty2", 
                     "UomCode2",

                     //11
                     "LocalDocno",
                     "Remark"
                    });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new DRDtl()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[2]),
                            Customer = Sm.DrStr(drDtl, cDtl[3]),
                            Shipping = Sm.DrStr(drDtl, cDtl[4]),
                            PackagingUnitUomCode = Sm.DrStr(drDtl, cDtl[5]),
                            QtyPackagingUnit = Sm.DrDec(drDtl, cDtl[6]),
                            Qty1 = Sm.DrDec(drDtl, cDtl[7]),
                            UomCode1 = Sm.DrStr(drDtl, cDtl[8]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[9]),
                            UomCode2 = Sm.DrStr(drDtl, cDtl[10]),
                            LocalDocNo = Sm.DrStr(drDtl, cDtl[11]),
                            Remark = Sm.DrStr(drDtl, cDtl[12]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            if (mDocTitle == "KSM")
            {
                if(Sm.GetLue(LueSiteCode) == "001")
                    Sm.PrintReport(mIsPrintOutDRSC + "Spinning", myLists, TableName, false);
                else if (Sm.GetLue(LueSiteCode) == "002")
                    Sm.PrintReport(mIsPrintOutDRSC + "Weaving", myLists, TableName, false);

            }
            else
                Sm.PrintReport(mIsPrintOutDRSC, myLists, TableName, false);
        }


        private void DownloadFileKu(string TxtFile)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (SFD1.ShowDialog() == DialogResult.OK)
            {
                Application.DoEvents();

                //Write the bytes to a file
                FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                newFile.Write(downloadedData, 0, downloadedData.Length);
                newFile.Close();
                MessageBox.Show("Saved Successfully");
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;


                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnCtShippingAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(SetLueVdCode));
            ClearData(3);
            if (Sm.GetLue(LueVdCode) != null)
                SetLueDriver(ref LueDriver, Sm.GetLue(LueVdCode));
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
            ClearData(2);
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode2), "");
            if (Sm.GetLue(LueCtCode) != null)
            {
                if (Sm.GetLue(LueCtCode) != Sm.GetParameter("OnlineCtCode"))
                {
                    SetLueShippingAddress(ref LueShipAdd, Sm.GetLue(LueCtCode));
                    SetLueAgtCode(ref LueAgtCode, Sm.GetLue(LueCtCode));
                }
                else
                {
                    SetLueShippingAddress(ref LueShipAdd, Sm.GetLue(LueCtCode));
                    SetLueAgtCode(ref LueAgtCode, Sm.GetLue(LueCtCode));
                    //Lock online customer
                    //LueShipAdd.Properties.ReadOnly = true;
                }
            }
            if (BtnSave.Enabled && !mIsClearData) GenerateLocalDocNo();
        }

        private void LueAgtCode_EditValueChanged(object sender, EventArgs e)
        {

            Sm.ClearGrd(Grd1, true);
            Sm.RefreshLookUpEdit(LueAgtCode, new Sm.RefreshLue2(SetLueAgtCode), Sm.GetLue(LueCtCode));
        }

        private void LueShipAdd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShipAdd, new Sm.RefreshLue2(SetLueShippingAddress), Sm.GetLue(LueCtCode));
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData(2);
                ShowShippingAddressData(Sm.GetLue(LueCtCode), Sm.GetLue(LueShipAdd));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueDriver_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDriver, new Sm.RefreshLue2(SetLueDriver), Sm.GetLue(LueVdCode));
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ShowExpeditionData(Sm.GetLue(LueVdCode), Sm.GetLue(LueDriver));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }     

        private void LueTtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTtCode, new Sm.RefreshLue1(Sl.SetLueTTCode));
        }

        private void LueShipAdd_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            (LueShipAdd.Properties.DataSource as List<Lue1>).Add
              (
                  new Lue1
                  {
                      Col1 = e.DisplayValue.ToString()
                  }
              );

            e.Handled = true;
        }

        private void LueDriver_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            (LueDriver.Properties.DataSource as List<Lue1>).Add
             (
                 new Lue1
                 {
                     Col1 = e.DisplayValue.ToString()
                 }
             );

            e.Handled = true;
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueItCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode, new Sm.RefreshLue1(SetLueItCode));

            if (Sm.GetLue(LueItCode).Length > 0)
            {
                Sm.SetControlReadOnly(DteUsageDt, false);
                return;
            }
        }

        private void LueUomCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUomCode1, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUomCode1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueUomCode1_Leave(object sender, EventArgs e)
        {
            if (LueUomCode1.Visible && fAccept && fCell.ColIndex == 34)
            {
                if (Sm.GetLue(LueUomCode1).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 33].Value = Grd1.Cells[fCell.RowIndex, 34].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 33].Value = Sm.GetLue(LueUomCode1);
                    Grd1.Cells[fCell.RowIndex, 34].Value = LueUomCode1.GetColumnValue("Col2");
                }
            }
        }

        private void LueUomCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUomCode2, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUomCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueUomCode2_Leave(object sender, EventArgs e)
        {
            if (LueUomCode2.Visible && fAccept && fCell.ColIndex == 37)
            {
                if (Sm.GetLue(LueUomCode2).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 36].Value =
                    Grd1.Cells[fCell.RowIndex, 37].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 36].Value = Sm.GetLue(LueUomCode2);
                    Grd1.Cells[fCell.RowIndex, 37].Value = LueUomCode2.GetColumnValue("Col2");
                }
            }
        }

        private void LueUomCode1_Validated(object sender, EventArgs e)
        {
            LueUomCode1.Visible = false;
        }

        private void LueUomCode2_Validated(object sender, EventArgs e)
        {
            LueUomCode2.Visible = false;
        }

        #endregion

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            if (BtnSave.Enabled && !mIsClearData) GenerateLocalDocNo();
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            List<string> FileName = new List<string>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LocalDocNo, A.FileName1, A.FileName2, A.FileName3, A.FileName4, A.FileName5, ");
            SQL.AppendLine("A.FileName6, A.FileName7, A.FileName8, A.FileName9, A.FileName10, ");
            SQL.AppendLine("A.FileName11, A.FileName12, A.FileName13, A.FileName14, A.FileName15, ");
            SQL.AppendLine("A.FileName16, A.FileName17, A.FileName18, A.FileName19, A.FileName20 ");
            SQL.AppendLine("From TblAttachmentFile A ");
            SQL.AppendLine("Where A.LocalDocNo=@LocalDocNo;");

            Sm.CmParam<String>(ref cm, "@localDocNo", TxtLocalDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "FileName1", 
                        //1-5
                        "FileName2", "FileName3", "FileName4", "FileName5", "FileName6",  
                        //6-10
                        "FileName7", "FileName8", "FileName9", "FileName10", "FileName11", 
                        //11-15
                        "FileName12", "FileName13", "FileName14", "FileName15", "FileName16", 
                        //16-19
                        "FileName17", "FileName18", "FileName19", "FileName20"                         
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (Sm.DrStr(dr, c[0]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[0]));
                        if (Sm.DrStr(dr, c[1]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[1]));
                        if (Sm.DrStr(dr, c[2]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[2]));
                        if (Sm.DrStr(dr, c[3]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[3]));
                        if (Sm.DrStr(dr, c[4]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[4]));
                        if (Sm.DrStr(dr, c[5]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[5]));
                        if (Sm.DrStr(dr, c[6]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[6]));
                        if (Sm.DrStr(dr, c[7]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[7]));
                        if (Sm.DrStr(dr, c[8]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[8]));
                        if (Sm.DrStr(dr, c[9]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[9]));
                        if (Sm.DrStr(dr, c[10]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[10]));
                        if (Sm.DrStr(dr, c[11]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[11]));
                        if (Sm.DrStr(dr, c[12]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[12]));
                        if (Sm.DrStr(dr, c[13]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[13]));
                        if (Sm.DrStr(dr, c[14]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[14]));
                        if (Sm.DrStr(dr, c[15]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[15]));
                        if (Sm.DrStr(dr, c[16]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[16]));
                        if (Sm.DrStr(dr, c[17]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[17]));
                        if (Sm.DrStr(dr, c[18]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[18]));
                        if (Sm.DrStr(dr, c[19]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[19]));
                    }, false
                );
            if (FileName.Count > 0)
            {
                for (int i = 0; i < FileName.Count; i++)
                {
                    DownloadFileKu(FileName[i]);
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "No Attachment File");
            }
        }

        #endregion

        #region Report Class

        class DRHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
            public string Customer { get; set; }
            public string ExpVd { get; set; }
            public string ExpDriver { get; set; }
            public string ExpPlatNo { get; set; }
            public string ExpTT { get; set; }
            public string DocNoDR { get; set; }
            public string ItCode { get; set; }
            public string UsageDt { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string CityName { get; set; }
            public string CntName { get; set; }
            public string SAPostalCd { get; set; }
            public string SAPhone { get; set; }
            public string SAFax { get; set; }
            public string SAEmail { get; set; }
            public string SAMobile { get; set; }
            public string UserName { get; set; }
            public string InventoryUomCode { get; set; }
            public string UomCode1 { get; set; }
            public string UomCode2 { get; set; }
            public string Sign1 { get; set; }
            public string Sign2 { get; set; }
            public string Sign3 { get; set; }
            public string Sign4 { get; set; }
        }

        class DRDtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ItCodeInternal { get; set; }
            public string PackagingUnitUomCode { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public decimal Qty1 { get; set; }
            public string UomCode1 { get; set; }
            public decimal Qty2 { get; set; }
            public string UomCode2 { get; set; }
            public string Remark { get; set; }
            public string Shipping { get; set; }
            public string Customer { get; set; }
            public string LocalDocNo { get; set; }
        }

        #endregion
    }
}
