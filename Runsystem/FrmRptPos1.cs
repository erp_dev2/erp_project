﻿#region Update
/*
    24/07/2017 [TKG] menambah Pos#
    27/09/2017 [HAR] total summary
    28/09/2017 [TKG] Amount dibuat minus kalau retur.
    31/05/2018 [TKG] Tambah discount per nota.
    04/07/2018 [TKG] Tambah site
    07/07/2018 [TKG] tambah item's group
    13/08/2018 [TKG] tambah informasi entity
    30/01/2019 [TKG] apabila retur nilai quantity dan amount menjadi negatif
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Drawing.Printing;


#endregion

namespace RunSystem
{
    public partial class FrmRptPos1 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool mIsSiteMandatory = false;

        // On Printing Purpose

        private string
            PosNo= string.Empty,
            TrnNo = string.Empty,
            BsDate = string.Empty;
        private decimal PaymentChange = 0;
        private string CurrentPrintNumber = string.Empty;
        private PrintDocument pdoc = null;
        private string PrinterName = string.Empty;
        private enum GrdPosition { Row, Col };

        private class GrdPos
        {
            public int Row;
            public int Col;
            public int ColWidth = 80;
            public string Title;
            public iGContentAlignment TextAlign = iGContentAlignment.TopLeft;
            public Type ValueType = typeof(string);
            public decimal BaseDecimal = 0;
            public int[] RoundValue = new int[10];
            public bool Visible = true;
        }

        private GrdPos GrdPosSalesType = new GrdPos
        {
            Row = 0,
            Col = 0,
            ColWidth = 20,
            Title = "Type",
            TextAlign = iGContentAlignment.TopCenter,
            Visible = false,
            ValueType = typeof(string)
        };

        private GrdPos GrdPosProductName = new GrdPos
        {
            Row = 0,
            Col = 1,
            ColWidth = 400,
            Title = "Product Name",
            TextAlign = iGContentAlignment.TopLeft,
            Visible = true,
            ValueType = typeof(string)
        };

        private GrdPos GrdPosProductCode = new GrdPos
        {
            Row = 1,
            Col = 1,
            ColWidth = 80,
            Title = "Product Code",
            TextAlign = iGContentAlignment.TopLeft,
            Visible = true,
            ValueType = typeof(string)
        };


        private GrdPos GrdPosCurrencyCode = new GrdPos
        {
            Row = 0,
            Col = 2,
            ColWidth = 45,
            Title = "",
            Visible = false,
            TextAlign = iGContentAlignment.TopLeft
        };

        private GrdPos GrdPosCurrencyRate = new GrdPos
        {
            Row = 1,
            Col = 2,
            ColWidth = 45,
            Title = "",
            TextAlign = iGContentAlignment.TopRight,
            Visible = false,
            ValueType = typeof(decimal)
        };

        private GrdPos GrdPosActualUnitPrice = new GrdPos
        {
            Row = 0,
            Col = 3,
            ColWidth = 100,
            Title = "Actual Price",
            ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false
        };

        private GrdPos GrdPosUnitPrice = new GrdPos
        {
            Row = 0,
            Col = 4,
            ColWidth = 100,
            Title = "Unit Price",
            TextAlign = iGContentAlignment.TopRight,
            Visible = true,
            ValueType = typeof(decimal)
        };

        private GrdPos GrdPosProductDiscount = new GrdPos
        {
            Row = 1,
            Col = 4,
            ColWidth = 100,
            Title = "Discount",
            TextAlign = iGContentAlignment.TopRight,
            Visible = true,
            ValueType = typeof(decimal)
        };

        private GrdPos GrdPosQuantity = new GrdPos
        {
            Row = 0,
            Col = 5,
            ColWidth = 100,
            Title = "Quantity",
            TextAlign = iGContentAlignment.TopRight,
            Visible = true,
            ValueType = typeof(decimal),
            RoundValue = new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }
        };

        private GrdPos GrdPosWeight = new GrdPos
        {
            Row = 0,
            Col = 6,
            ColWidth = 100,
            Title = "Weight",
            ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false
        };

        private GrdPos GrdPosSubTotal = new GrdPos
        {
            Row = 0,
            Col = 7,
            ColWidth = 120,
            Title = "Sub Total",
            TextAlign = iGContentAlignment.TopRight,
            Visible = true,
            ValueType = typeof(decimal)
        };

        private GrdPos GrdPosTotalDisc = new GrdPos
        {
            Row = 0,
            Col = 8,
            ColWidth = 100,
            Title = "Total Disc",
            ValueType = typeof(decimal),
            Visible = false
        };

        private GrdPos GrdPosProductDiscountRate = new GrdPos
        {
            Row = 0,
            Col = 9,
            ColWidth = 100,
            Title = "Discount Rate",
            Visible = false
        };


        private GrdPos GrdPosTax1 = new GrdPos
        {
            Row = 0,
            Col = 10,
            ColWidth = 100,
            Title = "Tax",
            ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false
        };

        private GrdPos GrdPosTaxRate1 = new GrdPos
        {
            Row = 1,
            Col = 10,
            ColWidth = 100,
            Title = "Tax",
            ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false
        };

        private GrdPos GrdPosTax2 = new GrdPos
        {
            Row = 0,
            Col = 11,
            ColWidth = 100,
            Title = "Tax",
            ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false
        };

        private GrdPos GrdPosTaxRate2 = new GrdPos
        {
            Row = 1,
            Col = 11,
            ColWidth = 100,
            Title = "Tax",
            ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false
        };

        private GrdPos GrdPosTax3 = new GrdPos
        {
            Row = 0,
            Col = 12,
            ColWidth = 100,
            Title = "Tax",
            ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false
        };

        private GrdPos GrdPosTaxRate3 = new GrdPos
        {
            Row = 1,
            Col = 12,
            ColWidth = 100,
            Title = "Tax",
            ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false
        };

        private GrdPos GrdPosTaxCode1 = new GrdPos
        {
            Row = 0,
            Col = 13,
            ColWidth = 100,
            Title = "Tax Code",
            Visible = false
        };

        private GrdPos GrdPosTaxCode2 = new GrdPos
        {
            Row = 0,
            Col = 14,
            ColWidth = 100,
            Title = "Tax Code",
            Visible = false
        };

        private GrdPos GrdPosTaxCode3 = new GrdPos
        {
            Row = 0,
            Col = 15,
            ColWidth = 100,
            Title = "Tax Code",
            Visible = false
        };

        #endregion

        #region Constructor

        public FrmRptPos1(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                TxtPosNo.EditValue = Gv.PosNo;
                ChkPosNo.Checked = Gv.PosNo.Length > 0;
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PosNo, A.TrnNo, A.BsDate, A.TrnDtTm, A.ShiftNo, A.SlsType, A.UserCode, B.ItCode, C.ItName, I.ItGrpName, ");
            SQL.AppendLine("Case When A.SlsType='S' Then 1.00 Else -1.00 End * B.Qty As Qty, ");
            SQL.AppendLine("C.SalesUomCode, ");
            SQL.AppendLine("D.SetValue As Cur, B.UPrice, ");
            SQL.AppendLine("Case When A.SlsType='S' Then 1.00 Else -1.00 End * B.DiscAmt As DiscAmt, ");
            SQL.AppendLine("Case When A.SlsType='S' Then 1.00 Else -1.00 End * B.Tax1Amt As Tax1Amt, ");
            SQL.AppendLine("Case When A.SlsType='S' Then 1.00 Else -1.00 End * B.Tax2Amt As Tax2Amt, ");
            SQL.AppendLine("Case When A.SlsType='S' Then 1.00 Else -1.00 End * B.Tax3Amt As Tax3Amt, ");
            SQL.AppendLine("Case When A.SlsType='S' Then 1.00 Else -1.00 End * A.TDiscAmt As TDiscAmt, ");
            SQL.AppendLine("Case When A.SlsType='S' Then 1.00 Else -1.00 End * ((B.Qty*(B.UPrice+ifnull(B.DiscAmt,0)))+ifnull(B.Tax1Amt,0)+ifnull(B.Tax2Amt,0)+ifnull(B.Tax3Amt,0)) As Amt, ");
            SQL.AppendLine("F.Stock, C.InventoryUomCode, H.SiteName, K.EntName ");
            SQL.AppendLine("From TblPosTrnHdr A ");
            SQL.AppendLine("Inner Join TblPosTrnDtl B On A.TrnNo=B.TrnNo And A.BsDate=B.BsDate And A.PosNo=B.PosNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblPosSetting D On A.PosNo=D.PosNo And D.SetCode='LocalCur' ");
            SQL.AppendLine("Left Join TblPosSetting E On A.PosNo=E.PosNo And E.SetCode='StoreWhsCode' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.WhsCode, T.ItCode, Sum(T.Qty) Stock ");
            SQL.AppendLine("    From TblStockSummary T ");
            SQL.AppendLine("    Where Exists(");
            SQL.AppendLine("        Select 1 ");
            SQL.AppendLine("        From TblPosTrnHdr T1 ");
            SQL.AppendLine("        Inner Join TblPosTrnDtl T2 On T1.TrnNo=T2.TrnNo And T1.BsDate=T2.BsDate And T1.PosNo=T2.PosNo ");
            SQL.AppendLine("        Inner Join TblItem T3 On T2.ItCode=T3.ItCode ");
            SQL.AppendLine("        Where (T1.BSDate Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And T2.ItCode=T.ItCode ");
            SQL.AppendLine((Filter.Replace("A.", "T1.")).Replace("C.", "T3."));
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T.WhsCode, T.ItCode ");
            SQL.AppendLine("    Having Sum(T.Qty)<>0 ");
            SQL.AppendLine(") F On B.ItCode=F.ItCode And E.SetValue=F.WhsCode ");
            SQL.AppendLine("Left Join TblPosNo G On A.PosNo=G.PosNo ");
            SQL.AppendLine("Left Join TblSite H On G.SiteCode=H.SiteCode ");
            SQL.AppendLine("Left Join TblItemGroup I On C.ItGrpCode=I.ItGrpCode ");
            SQL.AppendLine("Left Join TblProfitCenter J On H.ProfitCenterCode=J.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity K On J.EntCode=K.EntCode ");
            SQL.AppendLine("Where (A.BSDate Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.PosNo, A.BsDate, A.TrnNo;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Pos#",
                        "Receipt#", 
                        "Business"+Environment.NewLine+"Date",
                        "Transaction"+Environment.NewLine+"Date",
                        "Shift",
                        
                        //6-10
                        "Sales/"+Environment.NewLine+"Retur",               
                        "Cashier",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Item's Group",
                        
                        //11-15
                        "Quantity",
                        "UoM",
                        "Currency",
                        "Price",
                        "Discount"+Environment.NewLine+"(Item)",
                        
                        //16-20
                        "Tax",
                        "Tax",
                        "Tax",
                        "Amount"+Environment.NewLine+"Before Discount",
                        "Discount"+Environment.NewLine+"(Receipt)",
                        
                        //21-25
                        "Amount"+Environment.NewLine+"After Discount",
                        "",
                        "Stock"+Environment.NewLine+"(Current)",
                        "UoM",
                        "Site",

                        //26
                        "Entity"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 100, 110, 120, 70, 

                        //6-10
                        100, 100, 100, 200, 180, 

                        //11-15
                        80, 80, 80, 150, 130, 
                        
                        //16-20
                        120, 120, 120, 150, 150, 

                        //21-25
                        150, 20, 100, 80, 180,

                        //26
                        150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 22 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 14, 15, 16, 17, 18, 19, 20, 21, 23 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 8, 10, 22 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24, 25, 26 }, false);
            Sm.SetGrdProperty(Grd1, false);
            if (mIsSiteMandatory)
            {
                Grd1.Cols[25].Move(2);
                Grd1.Cols[26].Move(3);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 25, 26 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 8, 10, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string
                    Filter = " ",
                    PosNo = string.Empty,
                    BsDate = string.Empty,
                    TrnNo = string.Empty;
                bool IsFirst = false;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtPosNo.Text, "A.PosNo", true);
                Sm.FilterStr(ref Filter, ref cm, TxtShiftNo.Text, "A.ShiftNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "C.ItCode", "C.ItName" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, GetSQL(Filter),
                new string[]
                    {
                        //0
                        "PosNo",  

                        //1-5
                        "TrnNo", "BsDate", "TrnDtTm", "ShiftNo", "SlsType",  

                        //6-10
                        "UserCode", "ItCode", "ItName", "ItGrpName", "Qty", 
 
                        //11-15
                        "SalesUomCode", "Cur", "UPrice", "DiscAmt", "Tax1Amt", 
                        
                        //16-20
                        "Tax2Amt", "Tax3Amt", "TDiscAmt", "Amt", "Stock", 

                        //21-23
                        "InventoryUomCode", "SiteName", "EntName"
                    },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;

                    IsFirst = false;

                    if (!(Sm.CompareStr(PosNo, dr.GetString(c[0])) &&
                        Sm.CompareStr(TrnNo, dr.GetString(c[1])) &&
                        Sm.CompareStr(BsDate, dr.GetString(c[2]))))
                        IsFirst = true;

                    PosNo = dr.GetString(c[0]);
                    TrnNo = dr.GetString(c[1]);
                    BsDate = dr.GetString(c[2]);

                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    if (dr[c[5]] == DBNull.Value)
                        Grd.Cells[Row, 6].Value = string.Empty;
                    else
                        Grd.Cells[Row, 6].Value = (dr.GetString(c[5]) == "S" ? "Sales" : "Retur");
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                    if (IsFirst)
                        Grd.Cells[Row, 20].Value = dr.GetDecimal(c[18]);
                    else
                        Grd.Cells[Row, 20].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                    Grd.Cells[Row, 21].Value = Sm.GetGrdDec(Grd1, Row, 19) - Sm.GetGrdDec(Grd1, Row, 20);
                }, true, false, false, false
                );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 15, 16, 17, 18, 19, 20, 21, 23 });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            //Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                PosNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                TrnNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                BsDate = Sm.Left(Sm.GetGrdDate(Grd1, e.RowIndex, 3), 8);
                PrintReceipt();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        public void PrintReceipt()
        {
            try
            {
                int Line = 100;
                GetLine(ref Line);

                PrintDialog pd = new PrintDialog();
                pdoc = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();
                Font font = new Font("Courier New", 15);

                PaperSize psize = new PaperSize("",520,Line);
                ps.DefaultPageSettings.PaperSize = psize;

                pd.Document = pdoc;
                pd.Document.DefaultPageSettings.PaperSize = psize;
                pdoc.DefaultPageSettings.PaperSize.Height = Line;
                pdoc.DefaultPageSettings.PaperSize.Width = 520;
                pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintReceiptPage);
                
                pdoc.PrinterSettings.PrinterName = Sm.GetValue("Select SetValue From TblPosSetting Where SetCode='PrinterName' And PosNo=@Param", PosNo);
                pdoc.Print();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetLine(ref int Line)
        {
            //AddToAuditRoll("Printing Receipt...");

            Font font = new Font("Courier New", 10);
            float fontHeight = font.GetHeight();
            int startX = 3;
            int startY = 0;
            int Offset = 50;
            bool PrintHeader = true;
            decimal SubTotal = 0;

            decimal TAmount = 0;
            decimal TChange = 0;
            decimal TDisc = 0;

            decimal TTax1Amt = 0;
            decimal TTax2Amt = 0;
            decimal TTax3Amt = 0;

            string Tax1Name = "";
            string Tax2Name = "";
            string Tax3Name = "";

            // read from database
            try
            {
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.TrnNo, A.BsDate, A.TrnDtTm, A.PosNo, A.ShiftNo, A.UserCode, ");
                SQL.AppendLine("A.TDiscRt, A.TDiscAmt, A.Total, A.TChange, A.Tax1Incl, A.Tax2Incl, A.Tax3Incl, ");
                SQL.AppendLine("A.Tax1Amt, A.Tax2Amt, A.Tax3Amt, D.TaxName As Tax1Name, E.TaxName As Tax2Name, F.TaxName As Tax3Name, ");
                SQL.AppendLine("B.DtlType, C.ItName, B.Qty, B.UPrice, B.DiscAmt, B.DiscRate ");
                SQL.AppendLine("From TblPosTrnHdr A ");
                SQL.AppendLine("Inner Join tblPosTrnDtl B On A.PosNo=B.PosNo And A.TrnNo=B.TrnNo And A.BsDate=B.BsDate ");
                SQL.AppendLine("Left Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("Left join TblTax D On A.Tax1Code=D.TaxCode ");
                SQL.AppendLine("Left join TblTax E On A.Tax2Code=E.TaxCode ");
                SQL.AppendLine("Left join TblTax F On A.Tax3Code=F.TaxCode ");
                SQL.AppendLine("Where A.PosNo=@PosNo And A.TrnNo=@TrnNo And A.BsDate=@BsDate;");

                Sm.CmParam<String>(ref cm, "@PosNo", PosNo);
                Sm.CmParam<String>(ref cm, "@TrnNo", TrnNo);
                Sm.CmParam<String>(ref cm, "@BsDate", BsDate);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        //0
                        dr.GetOrdinal("TrnNo"),

                        //1-5
                        dr.GetOrdinal("TrnDtTm"),
                        dr.GetOrdinal("PosNo"),
                        dr.GetOrdinal("ShiftNo"),
                        dr.GetOrdinal("UserCode"),                    
                        dr.GetOrdinal("TDiscRt"),
                        //6-10
                        dr.GetOrdinal("TDiscAmt"),                    
                        dr.GetOrdinal("Total"),                    
                        dr.GetOrdinal("TChange"),                    
                        dr.GetOrdinal("Tax1Amt"),                    
                        dr.GetOrdinal("Tax2Amt"), 
                        //11-15
                        dr.GetOrdinal("Tax3Amt"),                    
                        dr.GetOrdinal("DtlType"),                    
                        dr.GetOrdinal("ItName"),                    
                        dr.GetOrdinal("Qty"),                    
                        dr.GetOrdinal("UPrice"), 

                        //16-20                                       
                        dr.GetOrdinal("DiscAmt"),                                                                
                        dr.GetOrdinal("DiscRate"),                                                                
                        dr.GetOrdinal("Tax1Incl"),                                                                
                        dr.GetOrdinal("Tax2Incl"),                                                                
                        dr.GetOrdinal("Tax3Incl"),  
                        
                        //21                                      
                        dr.GetOrdinal("Tax1Name"),                                                                
                        dr.GetOrdinal("Tax2Name"),                                                                
                        dr.GetOrdinal("Tax3Name"),                                                                
                        dr.GetOrdinal("BsDate"),
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (PrintHeader)
                            {
                                string HeaderStr = Sm.GetPosSetting("Header1");
                                if (HeaderStr != "")
                                {
                                    Line += 15;
                                }
                                HeaderStr = Sm.GetPosSetting("Header2");
                                if (HeaderStr != "")
                                {
                                    Line += 10;
                                }

                                HeaderStr = Sm.GetPosSetting("Header3");
                                if (HeaderStr != "")
                                {
                                    Line += 10;
                                }

                                HeaderStr = Sm.GetPosSetting("Header4");
                                if (HeaderStr != "")
                                {
                                    Line += 10;
                                }

                                HeaderStr = Sm.GetPosSetting("Header5");
                                if (HeaderStr != "")
                                {
                                    Line += 10;
                                }
                                
                                Line += 10;
                                

                                string TrnDateTime = Sm.DrStr(dr, c[1]);
                                if (TrnDateTime.Length == 12)
                                    TrnDateTime =
                                        TrnDateTime.Substring(6, 2) + "." +
                                        TrnDateTime.Substring(4, 2) + "." +
                                        TrnDateTime.Substring(0, 4) + " " +
                                        TrnDateTime.Substring(8, 2) + ":" +
                                        TrnDateTime.Substring(10, 2);

                                string HeaderLine = TrnDateTime + " P" + Sm.DrStr(dr, c[2]) + "-" + Sm.DrStr(dr, c[3]) + " " + Sm.DrStr(dr, c[4]);

                                Line += 40;
                                //if (Reprint)
                                //    PrintLine(graphics, "REPRINT - Receipt Copy", startX, startY, 15, 8, ref Offset);
                                TAmount = Sm.DrDec(dr, c[7]);
                                TChange = Sm.DrDec(dr, c[8]);
                                TDisc = Sm.DrDec(dr, c[6]);

                                TTax1Amt = Sm.DrDec(dr, c[9]);
                                TTax2Amt = Sm.DrDec(dr, c[10]);
                                TTax3Amt = Sm.DrDec(dr, c[11]);

                                Tax1Name = Sm.DrStr(dr, c[21]);
                                Tax2Name = Sm.DrStr(dr, c[22]);
                                Tax3Name = Sm.DrStr(dr, c[23]);

                                if (Sm.DrStr(dr, c[18]) == "Y") Tax1Name += " (Incl)";
                                if (Sm.DrStr(dr, c[19]) == "Y") Tax2Name += " (Incl)";
                                if (Sm.DrStr(dr, c[20]) == "Y") Tax3Name += " (Incl)";

                                PrintHeader = false;
                            }

                            //TCL
                            string ReturnTag = "";
                            if (Sm.DrStr(dr, c[12]) == "R") ReturnTag = "(Return) ";
                            string Line1 = ReturnTag + String.Format(GetNumFormat(GrdPosQuantity.BaseDecimal), Sm.DrDec(dr, c[14])) + " x " + Sm.DrStr(dr, c[13]);
                            
                            //string Line1 = Sm.PadRight(String.Format(GetNumFormat(GrdPosQuantity.BaseDecimal), Sm.DrDec(dr, c[14])), 3) +
                            //    " x " + Sm.PadLeft(Sm.DrStr(dr, c[13]), Gv.MaxCharLinePrint - 6);


                            var rows = Sm.WordWrap(Line1, Gv.MaxCharLinePrint);
                            for (int intX = 0; intX < rows.Count; intX++)
                            {
                                Line += 15;
                            }

                            string Line2 = string.Empty;

                            if (Sm.GetParameter("IsPOSReceiptShowDiscount") == "Y")
                            {
                                //MSI
                                Line2 =
                                Sm.PadRight("@ " + String.Format(GetNumFormat(GrdPosUnitPrice.BaseDecimal), Sm.DrDec(dr, c[15])), 12) +
                                ((Sm.DrDec(dr, c[16]) == 0) ? Sm.CopyStr(" ", 10) :
                                Sm.PadRight(String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), Sm.DrDec(dr, c[16])), 10)) +
                                Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), (Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])) * Sm.DrDec(dr, c[14])), 12);
                            }
                            else
                            {
                                //IOK
                                Line2 =
                                Sm.PadRight("@ " + String.Format(GetNumFormat(GrdPosUnitPrice.BaseDecimal), Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])), 20) +
                                    //((Sm.DrDec(dr, c[16]) == 0) ? Sm.CopyStr(" ", 10) :
                                    //Sm.PadRight(String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), Sm.DrDec(dr, c[16])), 10)) +
                                Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), (Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])) * Sm.DrDec(dr, c[14])), 12);
                            }

                            SubTotal += (Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])) * Sm.DrDec(dr, c[14]);
                            Line += 20;
                        }

                        Line += 15;

                        string SubTotalLine = Sm.PadRight("Sub Total ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), SubTotal), 12);
                        Line += 15;

                        if (TDisc != 0)
                        {
                            string DiscountLine = Sm.PadRight("Total Discount ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TDisc), 12);
                            Line += 15;
                        }

                        if (Tax1Name != "")
                        {
                            string Tax1Line = Sm.PadRight(Tax1Name, Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TTax1Amt), 12);
                            Line += 15;
                        }
                        if (Tax2Name != "")
                        {
                            string Tax2Line = Sm.PadRight(Tax2Name, Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TTax2Amt), 12);
                            Line += 15;
                        }
                        if (Tax3Name != "")
                        {
                            string Tax3Line = Sm.PadRight(Tax3Name, Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TTax3Amt), 12);
                            Line += 15;
                        }

                        string TotalLine = Sm.PadRight("Total ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TAmount), 12);
                        Line += 30;
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.Warning, "Transaction Not Found !!");
                    }

                    dr.Close();

                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    //TCL
                    SQL.AppendLine("Select Case When C.SlsType='R' then concat(B.PayTpNm, ' Return') else B.PayTpNm end As PayTpNm, B.Charge, A.PayAmt, A.Charge As ChargeAmt, ");
                    SQL.AppendLine("A.CardNo from tblpostrnpay A ");
                    SQL.AppendLine("Inner join tblpospaybytype B on A.PayTpNo=B.PayTpNo ");
                    SQL.AppendLine("Inner join tblpostrnhdr C on A.TrnNo=C.TrnNo And A.BsDate=C.BsDate And A.PosNo=C.PosNo ");
                    SQL.AppendLine("Where A.TrnNo=@TrnNo And A.BsDate=@BSDate And A.PosNo=@PosNo ");
                    SQL.AppendLine("Order By A.DtlNo;");

                    var cm2 = new MySqlCommand();

                    Sm.CmParam<String>(ref cm2, "@PosNo", PosNo);
                    Sm.CmParam<String>(ref cm2, "@TrnNo", TrnNo);
                    Sm.CmParam<String>(ref cm2, "@BsDate", BsDate);

                    cm2.Connection = cn;
                    cm2.CommandText = SQL.ToString();
                    var dr2 = cm2.ExecuteReader();
                    var c2 = new int[]
                    {
                        //0
                        dr2.GetOrdinal("PayTpNm"),

                        //1-4
                        dr2.GetOrdinal("Charge"),
                        dr2.GetOrdinal("PayAmt"),
                        dr2.GetOrdinal("ChargeAmt"),
                        dr2.GetOrdinal("CardNo")
                    };

                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            if (Sm.DrDec(dr2, c2[1]) == 0)
                            {
                                Line += 15;
                            }
                            else
                            {
                                string CardNo = Sm.DrStr(dr2, c2[4]);
                                Line += 15;

                                if (CardNo != "")
                                {
                                    Line += 15;
                                }
                                Line += 15;
                            }
                        }
                    }
                    if (TChange != 0)
                    {
                        Line += 15;
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, "Unable to print." + Exc.Message);
            }
        }

        void pdoc_PrintReceiptPage(object sender, PrintPageEventArgs e)
        {
            //AddToAuditRoll("Printing Receipt...");

            int Line = 100;

            Graphics graphics = e.Graphics;

            graphics.DrawImage(
                    Image.FromFile(@Sm.CompanyLogo()),
                     new Rectangle() { Height = 35, Width = 120, X = 70, Y = 10 }
                    );

            Font font = new Font("Courier New", 10);
            float fontHeight = font.GetHeight();
            int startX = 3;
            int startY = 0;
            int Offset = 50;
            bool PrintHeader = true;
            decimal SubTotal = 0;

            decimal TAmount = 0;
            decimal TChange = 0;
            decimal TDisc = 0;

            decimal TTax1Amt = 0;
            decimal TTax2Amt = 0;
            decimal TTax3Amt = 0;

            string Tax1Name = "";
            string Tax2Name = "";
            string Tax3Name = "";

            // read from database
            try
            {
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.TrnNo, A.BsDate, A.TrnDtTm, A.PosNo, A.ShiftNo, A.UserCode, ");
                SQL.AppendLine("A.TDiscRt, A.TDiscAmt, A.Total, A.TChange, A.Tax1Incl, A.Tax2Incl, A.Tax3Incl, ");
                SQL.AppendLine("A.Tax1Amt, A.Tax2Amt, A.Tax3Amt, D.TaxName As Tax1Name, E.TaxName As Tax2Name, F.TaxName As Tax3Name, ");
                SQL.AppendLine("B.DtlType, C.ItName, B.Qty, B.UPrice, B.DiscAmt, B.DiscRate ");
                SQL.AppendLine("From TblPosTrnHdr A ");
                SQL.AppendLine("Inner Join tblPosTrnDtl B On A.PosNo=B.PosNo And A.TrnNo=B.TrnNo And A.BsDate=B.BsDate ");
                SQL.AppendLine("Left Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("Left join TblTax D On A.Tax1Code=D.TaxCode ");
                SQL.AppendLine("Left join TblTax E On A.Tax2Code=E.TaxCode ");
                SQL.AppendLine("Left join TblTax F On A.Tax3Code=F.TaxCode ");
                SQL.AppendLine("Where A.PosNo=@PosNo And A.TrnNo=@TrnNo And A.BsDate=@BsDate;");

                Sm.CmParam<String>(ref cm, "@PosNo", PosNo);
                Sm.CmParam<String>(ref cm, "@TrnNo", TrnNo);
                Sm.CmParam<String>(ref cm, "@BsDate", BsDate);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        //0
                        dr.GetOrdinal("TrnNo"),

                        //1-5
                        dr.GetOrdinal("TrnDtTm"),
                        dr.GetOrdinal("PosNo"),
                        dr.GetOrdinal("ShiftNo"),
                        dr.GetOrdinal("UserCode"),                    
                        dr.GetOrdinal("TDiscRt"),
                        //6-10
                        dr.GetOrdinal("TDiscAmt"),                    
                        dr.GetOrdinal("Total"),                    
                        dr.GetOrdinal("TChange"),                    
                        dr.GetOrdinal("Tax1Amt"),                    
                        dr.GetOrdinal("Tax2Amt"), 
                        //11-15
                        dr.GetOrdinal("Tax3Amt"),                    
                        dr.GetOrdinal("DtlType"),                    
                        dr.GetOrdinal("ItName"),                    
                        dr.GetOrdinal("Qty"),                    
                        dr.GetOrdinal("UPrice"), 

                        //16-20                                       
                        dr.GetOrdinal("DiscAmt"),                                                                
                        dr.GetOrdinal("DiscRate"),                                                                
                        dr.GetOrdinal("Tax1Incl"),                                                                
                        dr.GetOrdinal("Tax2Incl"),                                                                
                        dr.GetOrdinal("Tax3Incl"),  
                        
                        //21                                      
                        dr.GetOrdinal("Tax1Name"),                                                                
                        dr.GetOrdinal("Tax2Name"),                                                                
                        dr.GetOrdinal("Tax3Name"),                                                                
                        dr.GetOrdinal("BsDate"),
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (PrintHeader)
                            {
                                string HeaderStr = Sm.GetPosSetting("Header1");
                                if (HeaderStr != "")
                                {
                                    PrintLine(graphics, Sm.PadCenter(HeaderStr, Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);
                                    Line += 15;
                                }
                                HeaderStr = Sm.GetPosSetting("Header2");
                                if (HeaderStr != "")
                                {
                                    PrintLine(graphics, Sm.PadCenter(HeaderStr, Gv.MaxCharLinePrint + 5), startX, startY, 10, 7, ref Offset);
                                    Line += 10;
                                }

                                HeaderStr = Sm.GetPosSetting("Header3");
                                if (HeaderStr != "")
                                {
                                    PrintLine(graphics, Sm.PadCenter(HeaderStr, Gv.MaxCharLinePrint + 5), startX, startY, 10, 7, ref Offset);
                                    Line += 10;
                                }

                                HeaderStr = Sm.GetPosSetting("Header4");
                                if (HeaderStr != "")
                                {
                                    PrintLine(graphics, Sm.PadCenter(HeaderStr, Gv.MaxCharLinePrint + 5), startX, startY, 10, 7, ref Offset);
                                    Line += 10;
                                }

                                HeaderStr = Sm.GetPosSetting("Header5");
                                if (HeaderStr != "")
                                {
                                    PrintLine(graphics, Sm.PadCenter(HeaderStr, Gv.MaxCharLinePrint + 5), startX, startY, 10, 7, ref Offset);
                                    Line += 10;
                                }
                                
                                PrintLine(graphics, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 10, 8, ref Offset);
                                Line += 10;
                                

                                string TrnDateTime = Sm.DrStr(dr, c[1]);
                                if (TrnDateTime.Length == 12)
                                    TrnDateTime =
                                        TrnDateTime.Substring(6, 2) + "." +
                                        TrnDateTime.Substring(4, 2) + "." +
                                        TrnDateTime.Substring(0, 4) + " " +
                                        TrnDateTime.Substring(8, 2) + ":" +
                                        TrnDateTime.Substring(10, 2);

                                string HeaderLine = TrnDateTime + " P" + Sm.DrStr(dr, c[2]) + "-" + Sm.DrStr(dr, c[3]) + " " + Sm.DrStr(dr, c[4]);

                                Line += 40;
                                PrintLine(graphics, Sm.PadLeft(HeaderLine, Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);
                                PrintLine(graphics, Sm.PadLeft("T." + CurrentPrintNumber + "-" + Sm.DrStr(dr, c[24]), Gv.MaxCharLinePrint), startX, startY, 10, 8, ref Offset);
                                PrintLine(graphics, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);
                                //if (Reprint)
                                //    PrintLine(graphics, "REPRINT - Receipt Copy", startX, startY, 15, 8, ref Offset);
                                TAmount = Sm.DrDec(dr, c[7]);
                                TChange = Sm.DrDec(dr, c[8]);
                                TDisc = Sm.DrDec(dr, c[6]);

                                TTax1Amt = Sm.DrDec(dr, c[9]);
                                TTax2Amt = Sm.DrDec(dr, c[10]);
                                TTax3Amt = Sm.DrDec(dr, c[11]);

                                Tax1Name = Sm.DrStr(dr, c[21]);
                                Tax2Name = Sm.DrStr(dr, c[22]);
                                Tax3Name = Sm.DrStr(dr, c[23]);

                                if (Sm.DrStr(dr, c[18]) == "Y") Tax1Name += " (Incl)";
                                if (Sm.DrStr(dr, c[19]) == "Y") Tax2Name += " (Incl)";
                                if (Sm.DrStr(dr, c[20]) == "Y") Tax3Name += " (Incl)";

                                PrintHeader = false;
                            }

                            //TCL
                            string ReturnTag = "";
                            if (Sm.DrStr(dr, c[12]) == "R") ReturnTag = "(Return) ";
                            string Line1 = ReturnTag + String.Format(GetNumFormat(GrdPosQuantity.BaseDecimal), Sm.DrDec(dr, c[14])) + " x " + Sm.DrStr(dr, c[13]);
                            
                            //string Line1 = Sm.PadRight(String.Format(GetNumFormat(GrdPosQuantity.BaseDecimal), Sm.DrDec(dr, c[14])), 3) +
                            //    " x " + Sm.PadLeft(Sm.DrStr(dr, c[13]), Gv.MaxCharLinePrint - 6);


                            var rows = Sm.WordWrap(Line1, Gv.MaxCharLinePrint);
                            for (int intX = 0; intX < rows.Count; intX++)
                            {
                                PrintLine(graphics, Sm.PadLeft(rows[intX], Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);
                                Line += 15;
                            }

                            string Line2 = string.Empty;

                            if (Sm.GetParameter("IsPOSReceiptShowDiscount") == "Y")
                            {
                                //MSI
                                Line2 =
                                Sm.PadRight("@ " + String.Format(GetNumFormat(GrdPosUnitPrice.BaseDecimal), Sm.DrDec(dr, c[15])), 12) +
                                ((Sm.DrDec(dr, c[16]) == 0) ? Sm.CopyStr(" ", 10) :
                                Sm.PadRight(String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), Sm.DrDec(dr, c[16])), 10)) +
                                Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), (Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])) * Sm.DrDec(dr, c[14])), 12);
                            }
                            else
                            {
                                //IOK
                                Line2 =
                                Sm.PadRight("@ " + String.Format(GetNumFormat(GrdPosUnitPrice.BaseDecimal), Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])), 20) +
                                    //((Sm.DrDec(dr, c[16]) == 0) ? Sm.CopyStr(" ", 10) :
                                    //Sm.PadRight(String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), Sm.DrDec(dr, c[16])), 10)) +
                                Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), (Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])) * Sm.DrDec(dr, c[14])), 12);
                            }

                            SubTotal += (Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])) * Sm.DrDec(dr, c[14]);
                            PrintLine(graphics, Line2, startX, startY, 20, 8, ref Offset);
                            Line += 20;
                        }

                        PrintLine(graphics, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);
                        Line += 15;

                        string SubTotalLine = Sm.PadRight("Sub Total ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), SubTotal), 12);
                        PrintLine(graphics, SubTotalLine, startX, startY, 15, 8, ref Offset);
                        Line += 15;

                        if (TDisc != 0)
                        {
                            string DiscountLine = Sm.PadRight("Total Discount ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TDisc), 12);
                            PrintLine(graphics, DiscountLine, startX, startY, 15, 8, ref Offset);
                            Line += 15;
                        }

                        if (Tax1Name != "")
                        {
                            string Tax1Line = Sm.PadRight(Tax1Name, Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TTax1Amt), 12);
                            PrintLine(graphics, Tax1Line, startX, startY, 15, 8, ref Offset);
                            Line += 15;
                        }
                        if (Tax2Name != "")
                        {
                            string Tax2Line = Sm.PadRight(Tax2Name, Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TTax2Amt), 12);
                            PrintLine(graphics, Tax2Line, startX, startY, 15, 8, ref Offset);
                            Line += 15;
                        }
                        if (Tax3Name != "")
                        {
                            string Tax3Line = Sm.PadRight(Tax3Name, Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TTax3Amt), 12);
                            PrintLine(graphics, Tax3Line, startX, startY, 15, 8, ref Offset);
                            Line += 15;
                        }

                        PrintLine(graphics, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);
                        string TotalLine = Sm.PadRight("Total ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TAmount), 12);
                        PrintLine(graphics, TotalLine, startX, startY, 15, 8, ref Offset);
                        Line += 30;
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.Warning, "Transaction Not Found !!");
                    }

                    dr.Close();

                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    //TCL
                    SQL.AppendLine("Select Case When C.SlsType='R' then concat(B.PayTpNm, ' Return') else B.PayTpNm end As PayTpNm, B.Charge, A.PayAmt, A.Charge As ChargeAmt, ");
                    SQL.AppendLine("A.CardNo from tblpostrnpay A ");
                    SQL.AppendLine("Inner join tblpospaybytype B on A.PayTpNo=B.PayTpNo ");
                    SQL.AppendLine("Inner join tblpostrnhdr C on A.TrnNo=C.TrnNo And A.BsDate=C.BsDate And A.PosNo=C.PosNo ");
                    SQL.AppendLine("Where A.TrnNo=@TrnNo And A.BsDate=@BSDate And A.PosNo=@PosNo ");
                    SQL.AppendLine("Order By A.DtlNo;");

                    //SQL.AppendLine("Select B.PayTpNm, B.Charge, A.PayAmt, A.Charge As ChargeAmt, A.CardNo ");
                    //SQL.AppendLine("From TblPosTrnPay A ");
                    //SQL.AppendLine("Inner Join TblPosPayByType B on A.PayTpNo=B.PayTpNo ");
                    //SQL.AppendLine("Where A.PosNo=@PosNo And A.TrnNo=@TrnNo And A.BsDate=@BsDate ");
                    //SQL.AppendLine("Order by DtlNo;");

                    var cm2 = new MySqlCommand();

                    Sm.CmParam<String>(ref cm2, "@PosNo", PosNo);
                    Sm.CmParam<String>(ref cm2, "@TrnNo", TrnNo);
                    Sm.CmParam<String>(ref cm2, "@BsDate", BsDate);

                    cm2.Connection = cn;
                    cm2.CommandText = SQL.ToString();
                    var dr2 = cm2.ExecuteReader();
                    var c2 = new int[]
                    {
                        //0
                        dr2.GetOrdinal("PayTpNm"),

                        //1-4
                        dr2.GetOrdinal("Charge"),
                        dr2.GetOrdinal("PayAmt"),
                        dr2.GetOrdinal("ChargeAmt"),
                        dr2.GetOrdinal("CardNo")
                    };

                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            string PaymentLine = "";
                            if (Sm.DrDec(dr2, c2[1]) == 0)
                            {
                                PaymentLine = Sm.PadRight(Sm.DrStr(dr2, c2[0]) + " ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), Sm.DrDec(dr2, c2[2])), 12);
                                PrintLine(graphics, PaymentLine, startX, startY, 15, 8, ref Offset);
                                Line += 15;
                            }
                            else
                            {
                                string CardNo = Sm.DrStr(dr2, c2[4]);
                                if (CardNo.Length > 4) CardNo = CardNo.Substring(0, CardNo.Length - 4) + Sm.CopyStr("X", 4);

                                PaymentLine = Sm.PadRight(Sm.DrStr(dr2, c2[0]) + " " + "+" + Sm.DrDec(dr2, c2[1]) + "%", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), Sm.DrDec(dr2, c2[2])), 12);
                                PrintLine(graphics, PaymentLine, startX, startY, 15, 8, ref Offset);
                                Line += 15;

                                if (CardNo != "")
                                {
                                    PaymentLine = Sm.PadLeft("    " + CardNo, Gv.MaxCharLinePrint - 12);
                                    PrintLine(graphics, PaymentLine, startX, startY, 15, 8, ref Offset);
                                    Line += 15;
                                }
                                PaymentLine = Sm.PadLeft("    " + "Swipe:" + String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), Sm.DrDec(dr2, c2[2]) + Sm.DrDec(dr2, c2[3])), Gv.MaxCharLinePrint - 12);
                                PrintLine(graphics, PaymentLine, startX, startY, 15, 8, ref Offset);
                                Line += 15;
                            }
                        }
                    }
                    if (TChange != 0)
                    {
                        string ChangeLine = Sm.PadRight("Change ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TChange), 12);
                        PrintLine(graphics, ChangeLine, startX, startY, 15, 8, ref Offset);
                        Line += 15;
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, "Unable to print." + Exc.Message);
            }
        }

        private void PrintLine(Graphics graphics, string LineStr, int startX, int startY, int spaceY, int FontSize, ref int Offset)
        {
            //AddToAuditRoll(LineStr);
            graphics.DrawString(LineStr, new Font("Courier New", FontSize), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + spaceY;
        }

        private void AddToAuditRoll(string Message)
        {
            //Sm.AddToAuditRollFile(BusinessDate, Message);
            //LbAuditRoll.Items.Add(Message);
        }

        private string GetNumFormat(decimal Value)
        {
            string FormatDec = "";

            if (Value >= 0)
                FormatDec = "{0:###,###,###,##0}";
            else
                if (Double.Parse(Value.ToString()) == -1)
                    FormatDec = "{0:###,###,###,##0.0}";
                else
                    if (Double.Parse(Value.ToString()) >= -2)
                        FormatDec = "{0:###,###,###,##0.00}";
                    else
                        if (Double.Parse(Value.ToString()) >= -3)
                            FormatDec = "{0:###,###,###,##0.000}";
                        else
                            if (Double.Parse(Value.ToString()) >= -4)
                                FormatDec = "{0:###,###,###,##0.0000}";
                            else
                                if (Double.Parse(Value.ToString()) >= -5)
                                    FormatDec = "{0:###,###,###,##0.00000}";
                                else
                                    FormatDec = "{0:###,###,###,##0.000000}";
            return FormatDec;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }
       
        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
       
        private void TxtShiftNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkShiftNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Shift");
        }

        private void TxtPosNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPosNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Pos#");
        }

        #endregion

        #endregion
    }
}
