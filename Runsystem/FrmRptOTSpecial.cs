﻿#region Update
// 11/11/2019 [HAR/SRN]
// 14/11/2019 [HAR/SRN] tambahan validasi OTspecial indicator
// 28/01/2020 [HAR/SRN] hanya yang punyta OT yang dimunculin 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptOTSpecial : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptOTSpecial(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueDivisionCode(ref LueDivision);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * from (  ");
            SQL.AppendLine("    select A.PayrunCode, C.PayrunName, A.EmpCode, B.EMpName, B.EmpCodeOld, B.DeptCode, B.PosCode, D.DeptName, E.PosName, ");
            SQL.AppendLine("    ifnull(G.Otdays, 0) Otdays, ifnull(H.OTHolidayDays, 0) OTHolidayDays, SUM(A.OT1Hr)+SUM(A.Ot2Hr) As OTHr, SUM(A.Ot1Amt)+SUM(A.OT2Amt) As OtAmt,  ");
            SQL.AppendLine("    SUM(A.OtHolidayHr) OtHolidayHr, SUM(A.OtHolidayAmt) OtHolidayAmt, SUM(A.ADOT) ADOT, F.DocNo AS VRDocNo, B.DivisionCode, I.DivisionName, SUM(A.Meal) Meal ");
            SQL.AppendLine("    From TblPayrollProcess2 A ");
            SQL.AppendLine("    Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("    Inner Join Tblpayrun C On A.PayrunCode =  C.payrunCode ");
            SQL.AppendLine("    Inner Join TblDepartment D On B.DeptCode = D.DeptCode ");
            SQL.AppendLine("    Left Join TblPosition E On B.PosCode = E.Poscode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select A.DocNo, F.PayrunCode from TblVoucherRequestOTHdr A ");
            SQL.AppendLine("        Inner Join TblVoucherRequestOTdtl2 F On A.DocNo=F.DocNo ");
            SQL.AppendLine("        Where A.CancelInd = 'N' And A.Status ='A' ");
            SQL.AppendLine("    )F On A.PayrunCode = F.PayrunCode ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    (  ");
            SQL.AppendLine("        Select PayrunCode, EmpCOde, Count(*) As OTDays  ");
            SQL.AppendLine("        From tblPayrollProcess2  ");
            SQL.AppendLine("        Where (OT1Hr>0 Or Ot2Hr>0 Or Ot3Hr>0)  ");
            SQL.AppendLine("        Group By PayrunCode, EmpCOde  ");
            SQL.AppendLine("    )G On A.PayrunCode = G.PayrunCode And A.EmpCode = G.EmpCode  ");
            SQL.AppendLine("    Left Join   ");
            SQL.AppendLine("    (  ");
            SQL.AppendLine("        Select PayrunCode, EmpCOde, Count(*) As OTHolidayDays  ");
            SQL.AppendLine("        From tblPayrollProcess2  ");
            SQL.AppendLine("        Where (OtHolidayHr > 0)  ");
            SQL.AppendLine("        Group By PayrunCode, EmpCOde  ");
            SQL.AppendLine("    )H On A.payrunCode = H.PayrunCode And A.EmpCode = H.EmpCode  ");
            SQL.AppendLine("    Left Join TblDivision I On  B.DivisionCOde = I.DivisionCode ");
            SQL.AppendLine("    Where OTSpecialInd = 'Y' And (G.OtDays>0 Or H.OtHolidayDays>0) ");
            SQL.AppendLine("    Group By  ");
            SQL.AppendLine("    A.PayrunCode, C.PayrunName, A.EmpCode, B.EMpName, B.EmpCodeOld, B.DeptCode, B.PosCode, D.DeptName, E.PosName,  ");
            SQL.AppendLine("    B.DivisionCode, I.DivisionName  ");
            SQL.AppendLine(") T   ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Payrun Code",
                        "Payrun",
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's Name",
                        "Old Code", 
                        
                        //6-10
                        "Position",
                        "Department",
                        "Division",
                        "OT Days",
                        "OT Hours",

                        //11-15
                        "OT Holiday"+Environment.NewLine+"Days",
                        "OT Holiday"+Environment.NewLine+"Hours",
                        "Amount OT",
                        "Amount"+Environment.NewLine+"OT Holiday",
                        "Allowance OT",
                        //16
                        "Meal",
                        "Total",
                        "Voucher Request"
                    },
                    new int[] 
                    {
                        //0
                        40, 

                        //1-5
                        100, 150, 80, 200, 80,  
                        
                        //6-10
                        150, 150, 150, 100, 100, 100, 

                        //11-15
                        100, 100, 100, 100, 100,
                        //16
                        100, 100, 120
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12, 13, 14, 15, 16, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
           Sm.GrdColInvisible(Grd1, new int[] { 3, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
           
            try
            {
                Cursor.Current = Cursors.WaitCursor;
            
                string Filter = "Where 0=0";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T.EmpCode", "T.EmpCodeOld", "T.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, TxtPayrun.Text, new string[] { "T.PayrunCode", "T.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDivision), "T.DivisionCode", true);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "PayrunCode",  
                            //1-5
                            "PayrunName", "EmpCode", "EmpName", "EmpCodeOld",  "PosName", 
                            //6-10
                            "DeptName", "DivisionName", "OTDays", "OTHr", "OTHolidayDays",  
                            //11-15
                            "OTHolidayHr", "OTAmt", "OTHolidayAmt", "ADOT", "Meal", 
                            //16
                            "VRDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Grd.Cells[Row, 17].Value = Sm.GetGrdDec(Grd, Row, 13) + Sm.GetGrdDec(Grd, Row, 14) + Sm.GetGrdDec(Grd, Row, 15) + Sm.GetGrdDec(Grd, Row, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkPayrun_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtPayrun_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDivision_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDivision, new Sm.RefreshLue1(Sl.SetLueDivisionCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDivision_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Division");
        }

        #endregion

       
    }
}
