﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPenaltyCt : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmPenaltyCtFind FrmFind;

        #endregion

        #region Constructor

        public FrmPenaltyCt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }
        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //  if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPenaltyCtCode, TxtPenaltyCtName, MeeRemark
                    }, true);
                    TxtPenaltyCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtPenaltyCtCode, TxtPenaltyCtName, MeeRemark
                    }, false);
                    TxtPenaltyCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtPenaltyCtName, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPenaltyCtName
                    }, false);
                    TxtPenaltyCtName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtPenaltyCtCode, TxtPenaltyCtName, MeeRemark
            });
        }

        #endregion

        #region Button Method

       override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPenaltyCtFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        } 

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPenaltyCtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into tblpenaltycategory(PenaltyCtCode, PenaltyCtName, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PenaltyCtCode, @PenaltyCtName, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("Update PenaltyCtName=@PenaltyCtName, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PenaltyCtCode", TxtPenaltyCtCode.Text);
                Sm.CmParam<String>(ref cm, "@PenaltyCtName", TxtPenaltyCtName.Text);
                Sm.CmParam<String>(ref cm, "@Remark",MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPenaltyCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ProvCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PenaltyCtCode", ProvCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select PenaltyCtCode, PenaltyCtName, Remark From tblpenaltycategory Where PenaltyCtCode=@PenaltyCtCode",
                        new string[] 
                        {
                            "PenaltyCtCode", "PenaltyCtName", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPenaltyCtCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPenaltyCtName.EditValue = Sm.DrStr(dr, c[1]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[2]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPenaltyCtCode, "Penalty category code", false) ||
                Sm.IsTxtEmpty(TxtPenaltyCtName, "Penalty category name", false) ||
                IsCityCodeExisted();
        }

        private bool IsCityCodeExisted()
        {
            if (!TxtPenaltyCtCode.Properties.ReadOnly && Sm.IsDataExist("Select PenaltyCtCode From TblPenaltyCategory Where PenaltyCtCode='" + TxtPenaltyCtCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Penalty category code ( " + TxtPenaltyCtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event
        private void TxtPenaltyCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPenaltyCtCode);
        }
        #endregion
    }
}
