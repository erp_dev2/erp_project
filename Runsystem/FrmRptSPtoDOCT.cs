﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSPtoDOCT : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        #endregion

        #region Constructor

        public FrmRptSPtoDOCT(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetSQL();
            var CurrentDate = Sm.ServerCurrentDateTime();
            DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-30);
            DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
            SetGrd();
            Sl.SetLueCtCode(ref LueCtCode); 
            base.FrmLoad(sender, e);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As SPDocNo, A.LocalDocNo As SPLocalDocNo, A.DocDt As SpDt, "); 
            SQL.AppendLine("Case When A.Status = 'F' Then 'Fulfilled' When A.Status = 'R' Then 'Released'  ");
            SQL.AppendLine("When A.Status = 'P' Then 'Planning' else 'Cancelled' End As SPStatus, ");
            SQL.AppendLine("A.CtCode, G.Ctname, A.CtNotifyparty, A.StfDt, A.PEB, A.BlNo, A.PlaceReceipt, A.PlaceDelivery, ");
            SQL.AppendLine("B.DocNo As SIDocNo, B.LocalDocNo As SIlocalDocNo, C.DocNo As PLDocNo, E.SectionNo, E.CntName, ");
            SQL.AppendLine("D.DocNo As SinvDocNo, E2.DocNo As DOctDocNo, F.DocNo As MrDocNo  ");
            SQL.AppendLine("from TblSp A ");
            SQL.AppendLine("Left Join TblSIHdr B On A.DocNo = B.SPDocNo ");
            SQL.AppendLine("Left Join TblPLHdr C On B.DocNo = C.SIDocNo ");
            SQL.AppendLine("Left Join TblSinv D On C.DocNo = D.PLDocNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select Distinct A.DocNo, B.SectionNo, ");
	        SQL.AppendLine("    Case  ");
	        SQL.AppendLine("    When B.SectionNo = '1' then A.Cnt1 ");
	        SQL.AppendLine("    When B.SectionNo = '2' then A.Cnt2 ");
	        SQL.AppendLine("    When B.SectionNo = '3' then A.Cnt3 ");
	        SQL.AppendLine("    When B.SectionNo = '4' then A.Cnt4 ");
	        SQL.AppendLine("    When B.SectionNo = '5' then A.Cnt5 ");
	        SQL.AppendLine("    When B.SectionNo = '6' then A.Cnt6 ");
	        SQL.AppendLine("    When B.SectionNo = '7' then A.Cnt7 ");
	        SQL.AppendLine("    When B.SectionNo = '8' then A.Cnt8 ");
	        SQL.AppendLine("    When B.SectionNo = '9' then A.Cnt9 ");
	        SQL.AppendLine("    When B.SectionNo = '10' then A.Cnt10 ");
	        SQL.AppendLine("    When B.SectionNo = '11' then A.Cnt11 ");
	        SQL.AppendLine("    When B.SectionNo = '12' then A.Cnt12 ");
	        SQL.AppendLine("    When B.SectionNo = '13' then A.Cnt13 ");
	        SQL.AppendLine("    When B.SectionNo = '14' then A.Cnt14 ");
	        SQL.AppendLine("    When B.SectionNo = '15' then A.Cnt15 ");
	        SQL.AppendLine("    When B.SectionNo = '16' then A.Cnt16 ");
	        SQL.AppendLine("    When B.SectionNo = '17' then A.Cnt17 ");
	        SQL.AppendLine("    When B.SectionNo = '18' then A.Cnt18 ");
	        SQL.AppendLine("    When B.SectionNo = '19' then A.Cnt19 ");
	        SQL.AppendLine("    When B.SectionNo = '20' then A.Cnt20 ");
	        SQL.AppendLine("    When B.SectionNo = '21' then A.Cnt21 ");
	        SQL.AppendLine("    When B.SectionNo = '22' then A.Cnt22 ");
	        SQL.AppendLine("    When B.SectionNo = '23' then A.Cnt23 ");
	        SQL.AppendLine("    When B.SectionNo = '24' then A.Cnt24 ");
	        SQL.AppendLine("    When B.SectionNo = '25' then A.Cnt25 ");
	        SQL.AppendLine("    End As CntName ");
	        SQL.AppendLine("    From TblPlHdr A ");
	        SQL.AppendLine("    Inner Join tblPlDtl B On A.DocNo = B.DocNo ");
	        SQL.AppendLine("    )E On C.DocNo = E.DocNo ");
            SQL.AppendLine("Left Join TblDOct2Hdr E2 On E.DocNo = E2.PLDocNo And E.SectionNo = E2.SectionNo ");
            SQL.AppendLine("Left Join TblMaterialRequestHdr F On B.DocNo = F.SIDocNo And F.SIDocNo Is not null ");
            SQL.AppendLine("Inner Join TblCustomer G On A.CtCode = G.CtCode ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Shipment Planning"+Environment.NewLine+"Document",
                        "Shipment Planning"+Environment.NewLine+"Local Document",
                        "",
                        "Date",
                        "Status",
                        //6-10 
                        "Customer",
                        "Notify Party",
                        "Stuffing Date",
                        "PEB",
                        "BL Number",
                        //11-15
                        "Place Receipt",
                        "Place Delivery",
                        "Shipment Instruction"+Environment.NewLine+"Document",
                        "Shipment Instruction"+Environment.NewLine+"Local Document",
                        "",
                        //16-20
                        "Packing List"+Environment.NewLine+"Document",
                        "",
                        "Section"+Environment.NewLine+"Number",
                        "Container",
                        "Shipment Invoice"+Environment.NewLine+"Document",
                        //21-25
                        "",
                        "DO to Customer"+Environment.NewLine+"Document",
                        "",
                        "Material Request"+Environment.NewLine+"Document",
                        ""
                    },
                    new int[] 
                    {
                        30,
                        130, 130, 20, 100, 100, 
                        200, 150, 100, 120, 120,  
                        150, 150, 150, 150, 20,
                        150, 20, 80, 100, 150,
                        20, 150, 20, 150, 20
  
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 8 });
            Sm.GrdColButton(Grd1, new int[] { 3, 15, 17, 21, 23, 25});
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,  16,  18, 19, 20, 22,  24}, false);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 15, 17, 18, 21, 23, 25 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 15, 17, 18, 21, 23, 25 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtSPDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtSIDocNo.Text, new string[] { "B.DocNo", "B.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtPLDocNo.Text, new string[] { "C.DocNo", "C.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtSinvDocNo.Text, new string[] { "D.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtDOCtDocNo.Text, new string[] { "E2.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);

                SetSQL();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "Order By A.DocDt",
                        new string[]
                        { 
                           //0
                           "SPDocNo", 
                           //1-5
                           "SPLocalDocNo", "SpDt", "SPStatus", "Ctname", "CtNotifyparty", 
                           //6-10
                           "StfDt", "PEB", "BlNo", "PlaceReceipt", "PlaceDelivery",  
                           //11-15
                           "SIDocNo", "SIlocalDocNo", "PLDocNo", "SectionNo", "CntName",
                           //16-18
                           "SinvDocNo", "DOctDocNo", "MrDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);

                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 10);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 16);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 17);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 18);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSI(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                f.ShowDialog();
            }
            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPL(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                f.ShowDialog();
            }
            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 20).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSInv(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 20);
                f.ShowDialog();
            }
            if (e.ColIndex == 23 && Sm.GetGrdStr(Grd1, e.RowIndex, 22).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOCt2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 22);
                f.ShowDialog();
            }
            if (e.ColIndex == 25 && Sm.GetGrdStr(Grd1, e.RowIndex, 24).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmMaterialRequest2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 24);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
            {
                var f = new FrmSI(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                f.ShowDialog();
            }
            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length != 0)
            {
                var f = new FrmPL(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                f.ShowDialog();
            }
            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 20).Length != 0)
            {
                var f = new FrmSInv(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 20);
                f.ShowDialog();
            }
            if (e.ColIndex == 23 && Sm.GetGrdStr(Grd1, e.RowIndex, 22).Length != 0)
            {
                var f = new FrmDOCt2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 22);
                f.ShowDialog();
            }
            if (e.ColIndex == 25 && Sm.GetGrdStr(Grd1, e.RowIndex, 24).Length != 0)
            {
                var f = new FrmMaterialRequest2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 24);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion
        #endregion

        #region Event
        private void ChkSPDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Shipment Planning#");
        }

        private void ChkSIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Shipment Instructuion#");
        }

        private void ChkSinvDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Shipment Invoice#");
        }

        private void ChkDOCtDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Do to Customer#");
        }

        private void TxtSPDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtSIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtSinvDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtDOCtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void ChkPLDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Packing List#");
        }

        private void TxtPLDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

    }
}
