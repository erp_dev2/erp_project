﻿#region Update
/*
    11/11/2022 [IBL/BBT] New application. Based on CAS
    17/11/2022 [IBL/BBT] Penyesuaian print out
    05/12/2022 [IBL/BBT] Bug: Muncul warning saat print
    08/12/2022 [IBL/BBT] Feedback : Approver printout belum mengambil username
    09/12/2022 [IBL/BBT] Bank account yg muncul adl yg typenya tercantum di param BankAccountTypeForVRCA1, tambah kolom UoM (ambil dr inventory uom).
                         VC yg bisa ditarik adl yg bank "Debit To" nya sama dg yg dipilih di header
    12/12/2022 [IBL/BBT] Amt1 > Amt2. VR yg terbentuk -> actype = D, BankAcCode1 = BankAcCode debit di VC row 1 di detail. actype2 = C, BankAcCode2 = BankAcCode Credit di VC row 1 di detail
                         Amt1 < Amt2. VR yg terbentuk -> actype = D, BankAcCode1 = BankAcCode credit di VC row 1 di detail. actype2 = C, BankAcCode2 = BankAcCode Debit di VC row 1 di detail
    13/12/2022 [IBL/BBT] Bug: Belum nyimpan CurCode2 di VR.
    15/12/2022 [IBL/BBT] Menyimpan excrate di VR.
    27/12/2022 [MAU/BBT] validasi tidak bisa cancel dokumen apabila sedang digunakan pada transaksi Property Inventory Cost Component
    18/01/2023 [MAU/BBT] BUG : tidak bisa cancel CAS , ketika PICC sudah di cancel (approval/transaksi)
*/

#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmCashAdvanceSettlement2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mPIC = string.Empty,
            mDocTitle = string.Empty,
            mDeptCode = string.Empty,
            mVoucherDocTypeCASBA = string.Empty,
            mBankAccountTypeForCAS = string.Empty,
            mCASDlgFilterPICDataSource = string.Empty,
            mVoucherCASJournalAcNoSource = string.Empty,
            mCASApprovalGroupValidation = string.Empty
            ;
        private bool 
            mIsAutoJournalActived = false,
            IsInsert = false,
            mIsVoucherBankAccountFilteredByGrp = false,
            mIsFilterByCC = false,
            mIsBankAccountUseCostCenterAndInterOffice = false,
            mIsCostCategoryCASMandatory = false,
            mIsCASUsePartialAmount = false,
            mIsCASAllowMinusAmount = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsTransactionUseDetailApprovalInformation = false,
            mIsCASAllowToCopyData = false
            ;
        internal bool
            mIsSystemUseCostCenter = false,
            mIsCASUseDraftDocument = false,
            mIsCASAllowToProcessZeroAmount = false,
            mIsCASFromTRProcessAllAmount = false,
            mIsCostCategoryFilteredInCashAdvanceSettlement = false,
            mIsVRBudgetUseCASBA = false,
            mIsVoucherCASBA = false,
            mIsCASShowCOACostCategory = false,
            mIsCheckCOAJournalNotExists = false,
            mIsCASUseItemRate = false,
            mIsCASUseCostCenter = false,
            mIsCASCCFilterMandatory = false,
            mIsVRForBudgetUseSOContract = false,
            mIsVRUseSOContract = false,
            mIsCASUseItemRateNotBasedOnCBP = false,
            mIsCASUseCompletedInd = false,
            mIsFilterByDept = false,
            mIsCASUseBudgetCategory = false
            ;
        private string
            mBankAccountFormat = string.Empty,
            mMainCurCode = string.Empty,
            mVoucherCodeFormatType = string.Empty,
            mCashAdvanceJournalDebitFormat = string.Empty,
            mCASJournalFormula = string.Empty,
            mCASNotEqualToCostFormula = string.Empty,
            mBankAccountTypeForVRCA1 = string.Empty,
            mCASCompletedIndFormula = string.Empty
            ;
        iGCell fCell;
        bool fAccept;

        private string 
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty
            ;

        private byte[] downloadedData;

        internal FrmCashAdvanceSettlement2Find FrmFind;

        #endregion

        #region Constructor

        public FrmCashAdvanceSettlement2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Cash Advance Settlement";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Tc1.SelectedTabPage = Tp2;
                Sl.SetLueOption(ref LueTypeCode, "CASDetailsType");
                LueTypeCode.Visible = false;
                Tc1.SelectedTabPage = Tp1;
                if (!mIsCASUseCostCenter)
                {
                    LblCCCode.Visible = LueCCCode.Visible = false;
                }
                if (mIsCASUseCompletedInd)
                {
                    if (mCASCompletedIndFormula == "1")
                    {
                        ChkCompletedInd.Visible = true;
                    }                        
                    else
                    {
                        ChkCompletedInd.Visible = false;
                        ChkCompletedInd.Checked = true;
                    }
                }
                else
                {
                    ChkCompletedInd.Visible = false;
                }
                
                SetGrd();               

                SetFormControl(mState.View);
                if (!mIsCASUseDraftDocument)
                {
                    LblDocStatus.Visible = false;//hide
                    LueDocStatus.Visible = false;
                }
                else
                    LblDocStatus.ForeColor = Color.Red;

                if(!mIsCASAllowToCopyData)
                {
                    TxtCopyData.Visible = false;
                    LblCopyData.Visible = false;
                    BtnCopyData.Visible = false;
                }

                    //if this application is called from other application
                    if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = false;
                    BtnPrint.Visible = true;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 35;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Voucher#",
                    "",
                    "Cost Category Code",
                    "Cost Category",
                    "Currency",

                    //6-10
                    "Amount"+Environment.NewLine+"(Cost)",
                    "Amount"+Environment.NewLine+"(Cash Advance)",
                    "User Code",
                    "PIC",
                    "Department Code",

                    //11-15
                    "Department",
                    "Remark",
                    "Voucher's Type",
                    "File Name", 
                    "", 
                    
                    //16-20
                    "",
                    "File Name 2",
                    "",
                    "Dno",
                    "VCDocType",

                    //21-25
                    "COA#",
                    "COA",
                    "",
                    "Item Code",
                    "",

                    //26-30
                    "Item Name",
                    "Quantity",
                    "Unit Price",
                    "SO Contract#",
                    "",

                    //31-34
                    "Budget Category Code",
                    "Budget Category",
                    "Description",
                    "UoM"
                },
                new int[] { 
                    //0
                    20, 

                    //1-5
                    130, 20, 0, 200, 80,
                    
                    //6-10
                    130, 130, 0, 0, 0,

                    //11-15
                    0, 250, 200, 200, 20,
                    
                    //16-20
                    20, 200, 20, 20, 0,

                    //21-25
                    100, 200, 20, 100, 20,

                    //26-30
                    200, 130, 140, 130, 20,

                    //31-34
                    0, 200, 200, 130
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0, 2, 15, 16, 18, 23, 25, 30 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 27, 28 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 17, 19, 20, 24, 25 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 5, 7, 8, 9, 10, 11, 13, 19, 21, 22, 24, 26, 29, 30, 31, 32, 33, 34 });
            if (!mIsCASUseItemRateNotBasedOnCBP) Sm.GrdColReadOnly(true, true, Grd1, new int[] { 28 });
            if (mIsCASUseItemRateNotBasedOnCBP) Sm.GrdColReadOnly(true, true, Grd1, new int[] { 18 });
            if (mIsCASUseItemRate)
            {
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6 });
                Grd1.Cols[28].Move(5);
                Grd1.Cols[27].Move(5);
                Grd1.Cols[34].Move(5);
                Grd1.Cols[26].Move(3);
                Grd1.Cols[25].Move(3);
                Grd1.Cols[24].Move(3);
                Grd1.Cols[23].Move(3);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 23, 26, 27, 28, 34 });
            }

            Grd1.Cols[13].Move(3);
            Grd1.Cols[18].Move(4);

            if (!mIsVRForBudgetUseSOContract && !mIsVRUseSOContract)
                Sm.GrdColInvisible(Grd1, new int[] { 29 });
            else
            {
                Grd1.Cols[29].Move(3);
                Grd1.Cols[13].Move(3);
            }

            Grd1.Cols[22].Move(7);
            Grd1.Cols[21].Move(7);
            if(!mIsCASShowCOACostCategory)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22 }, false);

            if(!mIsCASUseBudgetCategory)
                Sm.GrdColInvisible(Grd1, new int[] { 30, 31, 32, 33 });
            else
            {
                Grd1.Cols[30].Move(5);
                Grd1.Cols[32].Move(6);
                Sm.GrdColInvisible(Grd1, new int[] { 18, 31 });
                Grd1.Cols[33].Move(11);
            }

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "Cost Category Code",

                    //1-4
                    "Cost Category",
                    "Amount (Cost)",
                    "COA#",
                    "COA"
                },
                new int[] { 
                    0, 
                    200, 130, 100, 200
                }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 2 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4 });
            Grd2.Cols[4].Move(2);
            Grd2.Cols[3].Move(2);
            if (!mIsCASShowCOACostCategory)
                Sm.GrdColInvisible(Grd2, new int[] { 3, 4 });

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[] 
                {
                    //0
                    "Voucher#",

                    //1-4
                    "",
                    "Currency",
                    "Amount"+Environment.NewLine+"(Cost)",
                    "Amount"+Environment.NewLine+"(Cash Advance)"
                },
                new int[] { 
                    130, 
                    20, 80, 130, 130
                }
            );
            Sm.GrdColButton(Grd3, new int[] { 1 });
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4 });

            #endregion

            #region Grid 4

            Grd4.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd4,
                new string[] { "DNo", "Description", "Amount", "Type Code", "Type" },
                new int[] { 0, 300, 130, 100, 100 }
            );
            Sm.GrdFormatDec(Grd4, new int[] { 2 }, 0);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 3 });
            Sm.GrdColInvisible(Grd4, new int[] { 3, 4 }, false);

            if (mIsCASUseBudgetCategory)
            {
                Grd4.Cols[4].Move(2);
                Sm.GrdColInvisible(Grd4, new int[] { 4 }, true);
            }

            #endregion

            #region Grid 5

            Grd5.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "User", 
                        "Position",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 150, 100, 80, 200 }
                );
            Sm.GrdFormatDate(Grd5, new int[] { 4 });

            #endregion
        }

        private void HideInfoInGrd()
        {
            if (mIsCASUseItemRate) Sm.GrdColInvisible(Grd1, new int[] { 24, 25 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueBankAcCode, TxtVoucherRequestDocNo, MeeRemark, LueDocStatus, LueCCCode, TxtLocalDocument
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 4, 6, 12, 14, 15, 18, 23, 27, 33 });
                    if (mIsCASUseItemRateNotBasedOnCBP)
                        Sm.GrdColReadOnly(true, true, Grd1, new int[] { 28 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 4 });
                    ChkCompletedInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    BtnVR.Enabled = true;
                    LblCopyData.Visible = false;
                    BtnCopyData.Visible = false;
                    TxtCopyData.Visible = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueBankAcCode, MeeRemark, LueDocStatus, TxtLocalDocument
                    }, false);
                    if (mIsCASUseCostCenter)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueCCCode
                        }, false);
                    }
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 2, 12, 15, 18, 30, 33 });
                    if (!mIsCASUseItemRate) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6 });
                    else
                    {
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 23, 27 });
                        if (mIsCASUseItemRateNotBasedOnCBP)
                        {
                            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 18 });
                            Sm.GrdColReadOnly(false, true, Grd1, new int[] { 28 });
                        }
                    }
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0, 1, 2, 4 });
                    if (mIsCASUseCompletedInd)
                    {
                        if (mCASCompletedIndFormula == "1")
                            ChkCompletedInd.Properties.ReadOnly = false;
                        else
                            ChkCompletedInd.Checked = true;
                    }
                    DteDocDt.Focus();
                    BtnVR.Enabled = false;
                    LblCopyData.Visible = true;
                    BtnCopyData.Visible = true;
                    TxtCopyData.Visible = true;
                    break;
                case mState.Edit:
                    if (mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "D")
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDocStatus, MeeCancelReason, ChkCancelInd }, false);
                        MeeCancelReason.Focus();
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 2, 12, 15, 18 });
                        if (!mIsCASUseItemRate) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6 });
                        else
                        {
                            Sm.GrdColReadOnly(false, true, Grd1, new int[] { 23, 27 });
                            if (mIsCASUseItemRateNotBasedOnCBP)
                                Sm.GrdColReadOnly(false, true, Grd1, new int[] { 28 });
                        }
                        Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0, 1, 2 });
                        if (mIsCASUseCompletedInd && mCASCompletedIndFormula == "1") ChkCompletedInd.Properties.ReadOnly = false;
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                        MeeCancelReason.Focus();
                        Sm.GrdColReadOnly(true, true, Grd1, new int[] { 15 });
                        LblCopyData.Visible = true;
                        BtnCopyData.Visible = true;
                        TxtCopyData.Visible = true;
                    }
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            IsInsert = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, TxtPIC, TxtDeptCode, 
                TxtCurCode, LueBankAcCode, MeeRemark, TxtVoucherRequestDocNo, LueDocStatus, LueCCCode, TxtLocalDocument,
                TxtCopyData
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt1, TxtAmt2, TxtAmt3 }, 0);
            ChkCancelInd.Checked = false;
            ChkCompletedInd.Checked = false;
            ClearGrd();
            if (mIsCASUseCostCenter) SetLueCCCode(ref LueCCCode);
            Sm.FocusGrd(Grd1, 0, 0);

        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCashAdvanceSettlement2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sm.SetDteCurrentDate(DteDocDt);
                if (mIsCASUseDraftDocument)
                {
                    Sl.SetLueOption(ref LueDocStatus, "CASDocumentStatus");
                    Sm.SetLue(LueDocStatus, "D");
                }
                else
                {
                    Sl.SetLueOption(ref LueDocStatus, "CASDocumentStatus");
                    Sm.SetLue(LueDocStatus, "F");
                }
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            if (IsDataCancelledAlready()) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No ||
                    Sm.IsTxtEmpty(TxtDocNo, "Document#", false)
                    ) return;

                string Doctitle = Sm.GetParameter("DocTitle");
                string[] TableName = { "CASHdr", "CASDtl", "CASJournal", "CASSignIMS", "CASSignAMKA", "CASSignSIER", "CASSignBBT" };
                var l = new List<CASHdr>();
                var lDtl = new List<CASDtl>();
                var lJournal = new List<CASJournal>();
                var lSign = new List<CASSignIMS>();
                var lSignAMKA = new List<CASSignAMKA>();
                var lSignSIER = new List<CASSignSIER>();
                var lSignBBT = new List<CASSignBBT>();

                var myLists = new List<IList>();

                if (Doctitle == "AMKA")
                {
                    #region Header AMKA

                    var cm2 = new MySqlCommand();
                    var SQL2 = new StringBuilder();


                    SQL2.AppendLine("SELECT Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL2.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, E.VoucherDocNo, E.VoucherDocDt, C.Amt VoucherAmt, D.UserName, F.DeptName, A.Remark, Concat(IfNull(G.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict5, H.UserName AS UserNameDibuat, H.UserCode AS UserCodeDibuat ");
                    SQL2.AppendLine("FROM tblcashadvancesettlementhdr A  ");
                    SQL2.AppendLine("LEFT JOIN tblcashadvancesettlementdtl3 B ON A.DocNo = B.DocNo  ");
                    SQL2.AppendLine("INNER JOIN tblvoucherhdr C ON B.VoucherDocNo = C.DocNo  ");
                    SQL2.AppendLine("INNER JOIN tbluser D ON A.PIC = D.UserCode  ");
                    SQL2.AppendLine("LEFT JOIN (  ");
                    SQL2.AppendLine("SELECT A.DocNo, GROUP_CONCAT(C.DocNo SEPARATOR ' \n') AS VoucherDocNo, GROUP_CONCAT(Date_Format(C.DocDt,'%d %M %Y') SEPARATOR '\n') AS VoucherDocDt  ");
                    SQL2.AppendLine("FROM tblcashadvancesettlementhdr A  ");
                    SQL2.AppendLine("LEFT JOIN tblcashadvancesettlementdtl3 B ON A.DocNo = B.DocNo  ");
                    SQL2.AppendLine("INNER JOIN tblvoucherhdr C ON B.VoucherDocNo = C.DocNo  ");
                    SQL2.AppendLine("GROUP BY A.DocNo  ");
                    SQL2.AppendLine(")E ON E.DocNo = A.DocNo  ");
                    SQL2.AppendLine("INNER JOIN tbldepartment F ON A.DeptCode = F.DeptCode  ");
                    SQL2.AppendLine("LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                    SQL2.AppendLine("INNER JOIN tbluser H ON A.CreateBy = H.UserCode  ");
                    SQL2.AppendLine("Where A.DocNo=@DocNo ");
                    SQL2.AppendLine("GROUP BY A.DocNo;  ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {

                        cn.Open();
                        cm2.Connection = cn;
                        cm2.CommandText = SQL2.ToString();
                        Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());


                        var dr = cm2.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[]
                            {
                             //0
                             "CompanyLogo",

                             //1-5
                             "DocNo",
                             "DocDt",
                             "VoucherDocNo",
                             "VoucherDocDt",
                             "UserName",

                             //6-10
                             "DeptName",
                             "Remark",
                             "VoucherAmt",
                             "UserNameDibuat",
                             "UserCodeDibuat",
                             
                             //11
                             "EmpPict5",
                            });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                l.Add(new CASHdr()
                                {
                                    CompanyLogo = Sm.DrStr(dr, c[0]),
                                    DocNo = Sm.DrStr(dr, c[1]),
                                    DocDt = Sm.DrStr(dr, c[2]),
                                    VoucherDocNo = Sm.DrStr(dr, c[3]),
                                    VoucherDocDt = Sm.DrStr(dr, c[4]),

                                    PJ = Sm.DrStr(dr, c[5]),
                                    Department = Sm.DrStr(dr, c[6]),
                                    Remark = Sm.DrStr(dr, c[7]),
                                    VoucherAmt = Sm.DrDec(dr, c[8]),
                                    UserNameDibuat = Sm.DrStr(dr, c[9]),
                                    UserCodeDibuat = Sm.DrStr(dr, c[10]),

                                    EmpPict5 = Sm.DrStr(dr, c[11]),

                                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                                });
                            }
                        }

                        dr.Close();
                    }
                    myLists.Add(l);

                    #endregion
                }
                else if (Doctitle == "SIER")
                {
                    #region Header Sier

                    var cm2 = new MySqlCommand();
                    var SQL2 = new StringBuilder();


                    SQL2.AppendLine("SELECT Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                    SQL2.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d/%b/%Y') As DocDt, ");
                    SQL2.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As Status, ");
                    SQL2.AppendLine("F.UserName, A.Remark, C.DeptName,  ");
                    SQL2.AppendLine("G.DivisionName AS Division, A.LocalDocNo, B.CurCode,   ");
                    SQL2.AppendLine("CONCAT(D.BankAcNm, ' : ', D.BankAcNo, ' [', E.BankName, '] ') AS Bank, SUM(B.Amt1) AS Amt1, SUM(B.Amt2) AS Amt2,   ");
                    SQL2.AppendLine("(SUM(B.Amt2) - SUM(B.Amt1)) AS DifferenceAmt  ");
                    SQL2.AppendLine("FROM tblcashadvancesettlementhdr A  ");
                    SQL2.AppendLine("LEFT JOIN tblcashadvancesettlementdtl3 B ON A.DocNo = B.DocNo  ");
                    SQL2.AppendLine("LEFT JOIN tbldepartment C ON A.DeptCode = C.DeptCode  ");
                    SQL2.AppendLine("INNER JOIN tblbankaccount D ON A.BankAcCode = D.BankAcCode  ");
                    SQL2.AppendLine("LEFT JOIN tblbank E ON D.BankCode = E.BankCode  ");
                    SQL2.AppendLine("INNER JOIN tbluser F ON A.PIC = F.UserCode  ");
                    SQL2.AppendLine("LEFT JOIN tbldivision G ON C.DivisionCode = G.DivisionCode  ");
                    SQL2.AppendLine("Where A.DocNo=@DocNo ");
                    SQL2.AppendLine("GROUP BY A.DocNo;  ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {

                        cn.Open();
                        cm2.Connection = cn;
                        cm2.CommandText = SQL2.ToString();
                        Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());


                        var dr = cm2.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[]
                            {
                             //0
                             "CompanyLogo",

                             //1-5
                             "DocNo",
                             "DocDt",
                             "Status",
                             "UserName",
                             "Remark",

                             //6-10
                             "DeptName",
                             "Division",
                             "LocalDocNo",
                             "Bank",
                             "Amt1",
                             
                             //11-13
                             "Amt2",
                             "DifferenceAmt",
                             "CurCode",
                            });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                l.Add(new CASHdr()
                                {
                                    CompanyLogo = Sm.DrStr(dr, c[0]),
                                    DocNo = Sm.DrStr(dr, c[1]),
                                    DocDt = Sm.DrStr(dr, c[2]),
                                    Status = Sm.DrStr(dr, c[3]),
                                    PJ = Sm.DrStr(dr, c[4]),

                                    Remark = Sm.DrStr(dr, c[5]),
                                    Department = Sm.DrStr(dr, c[6]),
                                    Division = Sm.DrStr(dr, c[7]),
                                    LocalDocNo = Sm.DrStr(dr, c[8]),
                                    Bank = Sm.DrStr(dr, c[9]),
                                    Amt = Sm.DrDec(dr, c[10]),

                                    Amt2 = Sm.DrDec(dr, c[11]),
                                    DifferenceAmt = Sm.DrDec(dr, c[12]),
                                    CurName = Sm.DrStr(dr, c[13]),
                                    FooterImage = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\FooterImage.png",

                                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                                });
                            }
                        }

                        dr.Close();
                    }
                    myLists.Add(l);

                    #endregion
                }
                else
                {
                    #region Header

                    var cm = new MySqlCommand();
                    var SQL = new StringBuilder();


                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL.AppendLine("(Select A.EmpName FROM TBLEmployee A INNER JOIN  tblparameter D ON A.EmpCode=D.ParValue and D.ParCode='CASPrintOutSignName') AS Verifikator1, ");
                    SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, A.Remark,");
                    SQL.AppendLine("D.Description, E.VoucherDocNo VCDocNo, E.Amt2 As VCAmt, Date_Format(C.DocDt,'%d %M %Y') As VCDocDt,  ");
                    SQL.AppendLine("T2.UserName UserName1, G.UserName UserName2, H.DeptName, I.Description as Description2 ");
                    SQL.AppendLine(",C.VoucherRequestDocNo VRDocNo, T2.UserName, DATE_FORMAT(T1.DocDt,'%d %M %Y') VRDocDt, T1.Amt VRAmt ");
                    SQL.AppendLine("FROM tblcashadvancesettlementhdr A   ");
                    SQL.AppendLine("left JOIN tblvoucherrequesthdr B ON A.VoucherRequestDocNo=B.DocNo  ");
                    SQL.AppendLine("left Join tblcashadvancesettlementDtl4 D on A.DocNo=D.Docno  ");
                    SQL.AppendLine("left Join tblcashadvancesettlementDtl3 E on A.DocNo=E.Docno  ");
                    SQL.AppendLine("inner JOIN tblvoucherhdr C on E.VoucherDocNo=C.DocNo  ");
                    SQL.AppendLine("INNER JOIN tblvoucherrequesthdr T1 ON C.VoucherRequestDocNo= T1.DocNo ");
                    SQL.AppendLine("left JOIN tbluser F ON B.CreateBy= F.UserCode ");
                    SQL.AppendLine("INNER JOIN tbluser G ON A.PIC= G.UserCode ");
                    SQL.AppendLine("INNER JOIN tbluser T2 ON T1.PIC= T2.UserCode ");
                    SQL.AppendLine("INNER JOIN tbldepartment H ON A.Deptcode = H.DeptCode ");
                    SQL.AppendLine("INNER JOIN tblvoucherdtl I ON C.DocNo = I.DocNo");

                    SQL.AppendLine("Where A.DocNo=@DocNo; ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {

                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());


                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[]
                            {
                             //0
                             "CompanyLogo",

                             //1-5
                             "DocNo",
                             "DocDt",
                             "VRDocNo",
                             "VRDocDt",
                             "VRAmt",

                             //6-10
                             "VCDocNo",
                             "VCDocDt",
                             "Description",
                             "VCAmt",
                             "Verifikator1",

                             //11-15
                             "UserName1",
                             "UserName2",
                             "DeptName",
                             "Remark",
                             "Description2"

                             //16-20
                            
                            });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                l.Add(new CASHdr()
                                {
                                    CompanyLogo = Sm.DrStr(dr, c[0]),
                                    DocNo = Sm.DrStr(dr, c[1]),
                                    DocDt = Sm.DrStr(dr, c[2]),
                                    VoucherRequestDocNo = Sm.DrStr(dr, c[3]),
                                    VoucherRequestDocDt = Sm.DrStr(dr, c[4]),

                                    VoucherRequestAmt = Sm.DrDec(dr, c[5]),
                                    VoucherDocNo2 = Sm.GetGrdStr(Grd3, 1, 1),
                                    VoucherDocNo = Sm.DrStr(dr, c[6]),

                                    VoucherDocDt = Sm.DrStr(dr, c[7]),
                                    TotalCost = decimal.Parse(TxtAmt1.Text),
                                    TotalCAS = decimal.Parse(TxtAmt2.Text),

                                    TotalDetail = decimal.Parse(TxtAmt3.Text),
                                    Description = Sm.DrStr(dr, c[8]),
                                    VoucherAmt = Sm.DrDec(dr, c[9]),
                                    Terbilang = Sm.Terbilang(decimal.Parse(TxtAmt1.Text)),
                                    Verifikator1 = Sm.DrStr(dr, c[10]),

                                    PJ = Sm.DrStr(dr, c[11]),
                                    SettledBy = Sm.DrStr(dr, c[12]),
                                    Department = Sm.DrStr(dr, c[13]),
                                    Remark = Sm.DrStr(dr, c[14]),
                                    Description2 = Sm.DrStr(dr, c[15]),

                                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                                });
                            }
                        }

                        dr.Close();
                    }
                    myLists.Add(l);

                    #endregion                 
                }

                #region Detail

                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();

                if (Doctitle == "SIER")
                {
                    SQLDtl.AppendLine("SELECT A.DocNo, E.DocNo AS VoucherDocNo, F.DocNo AS VRDocNo, B.Description,   ");
                    SQLDtl.AppendLine("B.Remark, B.Amt, G.BCName AS BudgetCategory  ");
                    SQLDtl.AppendLine("FROM tblcashadvancesettlementhdr A  ");
                    SQLDtl.AppendLine("LEFT JOIN tblcashadvancesettlementdtl B ON A.DocNo = B.DocNo  ");
                    SQLDtl.AppendLine("LEFT JOIN tblcashadvancesettlementdtl3 C ON B.DocNo = C.DocNo AND B.DNo = C.DNo ");
                    //SQLDtl.AppendLine("LEFT JOIN tblcashadvancesettlementdtl4 D ON A.DocNo = D.DocNo  ");
                    SQLDtl.AppendLine("INNER JOIN tblvoucherhdr E ON B.VoucherDocNo = E.DocNo  ");
                    SQLDtl.AppendLine("INNER JOIN tblvoucherrequesthdr F ON E.VoucherRequestDocNo = F.DocNo  ");
                    SQLDtl.AppendLine("LEFT JOIN tblbudgetcategory G ON B.BCCode = G.BCCode  ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo");
                    SQLDtl.AppendLine("GROUP BY A.DocNo, B.Dno Order By B.DNo;  ");


                    using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl.Open();
                        cmDtl.Connection = cnDtl;
                        cmDtl.CommandText = SQLDtl.ToString();
                        Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                        var drDtl = cmDtl.ExecuteReader();
                        var cDtl = Sm.GetOrdinal(drDtl, new string[]
                                {
                             //0
                             "VoucherDocNo",

                             //1-5
                             "VRDocNo",
                             "Description",
                             "Remark",
                             "Amt",
                             "BudgetCategory",


                                });
                        if (drDtl.HasRows)
                        {
                            int nomor = 0;
                            while (drDtl.Read())
                            {
                                nomor = nomor + 1;
                                lDtl.Add(new CASDtl()
                                {
                                    nomor = nomor,
                                    VoucherDocNo = Sm.DrStr(drDtl, cDtl[0]),
                                    VoucherRequestDocNo = Sm.DrStr(drDtl, cDtl[1]),
                                    Description = Sm.DrStr(drDtl, cDtl[2]),
                                    Remark = Sm.DrStr(drDtl, cDtl[3]),
                                    Amt = Sm.DrDec(drDtl, cDtl[4]),
                                    BudgetCategory = Sm.DrStr(drDtl, cDtl[5]),


                                });
                            }
                        }

                        drDtl.Close();
                    }
                    myLists.Add(lDtl);
                }
                else
                {
                    SQLDtl.AppendLine("Select A.Dno, A.VoucherDocNo, A.CCtCode, B.AcNo, F.CCName, B.CCtName,  ");
                    SQLDtl.AppendLine("A.CurCode, A.Amt, A.PIC, D.UserName, A.DeptCode, E.DeptName, A.Remark, G.OptDesc, A.FileName, ");
                    SQLDtl.AppendLine("If(C.DocType ='61', (C.Amt- H.DailyAmt) , C.Amt) As VoucherAmt, concat(C.Remark, ' : ', B.CCTName, ' (', IfNull(F.CCName, '-'), ')' ) As Description  ");
                    SQLDtl.AppendLine("From TblCashAdvanceSettlementDtl A  ");
                    SQLDtl.AppendLine("Left Join TblCostCategory B On A.CCtCode=B.CCtCode  ");
                    if (Doctitle == "AMKA")
                    {
                        SQLDtl.AppendLine("LEFT JOIN (  ");
                        SQLDtl.AppendLine("         SELECT A.DocNo, A.DocType, A.Amt, A.VoucherRequestDocNo, ");
                        SQLDtl.AppendLine("         GROUP_CONCAT(B.Description SEPARATOR '\n')AS Remark  ");
                        SQLDtl.AppendLine("         FROM tblvoucherhdr A  ");
                        SQLDtl.AppendLine("         INNER JOIN tblvoucherdtl B ON A.DocNo = B.DocNo  ");
                        SQLDtl.AppendLine("         GROUP BY A.DocNo, A.DocType, A.VoucherRequestDocNo, A.Amt ");
                        SQLDtl.AppendLine("     )C ON C.DocNo = A.VoucherDocNo  ");
                    }
                    else
                    {
                        SQLDtl.AppendLine("Left Join TblVoucherHdr C On A.VoucherDocNo=C.DocNo  ");
                    }
                    SQLDtl.AppendLine("Left Join TblUser D On A.PIC=D.UserCode  ");
                    SQLDtl.AppendLine("Left Join TblDepartment E On A.DeptCode=E.DeptCode  ");
                    SQLDtl.AppendLine("Left Join TblCostCenter F On B.CCCode=F.CCCode  ");
                    SQLDtl.AppendLine("Left Join TblOption G On C.DocType=G.OptCode And G.OptCat='VoucherDocType'  ");
                    SQLDtl.AppendLine("Left Join (  ");
                    SQLDtl.AppendLine("        Select A.VoucherRequestDocNo As DocNo, SUM(B.Amt2) As DailyAmt   ");
                    SQLDtl.AppendLine("        From TblTravelRequestDtl8 A   ");
                    SQLDtl.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo  And A.EmpCode = B.PICCode  ");
                    SQLDtl.AppendLine("        Inner Join TblTravelRequestHdr C On A.DocNo = C.DocNo And CancelInd = 'N'  ");
                    SQLDtl.AppendLine("        Group By A.VoucherRequestDocNo  ");
                    SQLDtl.AppendLine("     )H On H.DocNo = C.VoucherRequestDocNo  ");

                    SQLDtl.AppendLine(" Where A.DocNo=@DocNo Order By A.DNo; ");



                    using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl.Open();
                        cmDtl.Connection = cnDtl;
                        cmDtl.CommandText = SQLDtl.ToString();
                        Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                        var drDtl = cmDtl.ExecuteReader();
                        var cDtl = Sm.GetOrdinal(drDtl, new string[]
                                {
                             //0
                             "CCName",

                             //1-5
                             "CCtName",
                             "AcNo",
                             "Remark",
                             "Amt",
                             "VoucherAmt",

                             //16-20
                             "Description"

                                });
                        if (drDtl.HasRows)
                        {
                            int nomor = 0;
                            while (drDtl.Read())
                            {
                                nomor = nomor + 1;
                                lDtl.Add(new CASDtl()
                                {
                                    nomor = nomor,
                                    CostCenter = Sm.DrStr(drDtl, cDtl[0]),
                                    CostCategory = Sm.DrStr(drDtl, cDtl[1]),
                                    AcNo = Sm.DrStr(drDtl, cDtl[2]),
                                    Desc = Sm.DrStr(drDtl, cDtl[3]),
                                    Amt = Sm.DrDec(drDtl, cDtl[4]),
                                    VoucherAmt = Sm.DrDec(drDtl, cDtl[5]),
                                    Description = Sm.DrStr(drDtl, cDtl[6]),


                                });
                            }
                        }

                        drDtl.Close();
                    }
                    myLists.Add(lDtl);
                }
                #endregion

                #region CASJournal

                var cmJournal = new MySqlCommand();
                var SQLJournal = new StringBuilder();

                SQLJournal.AppendLine("SELECT C.DocNo As JournalDocNo, A.DocNo As CASDocNo, D.AcNo, E.AcDesc, D.DAmt, D.CAmt ");
                SQLJournal.AppendLine("FROM tblcashadvancesettlementhdr A ");
                SQLJournal.AppendLine("INNER JOIN tblcashadvancesettlementdtl B ON A.DocNo = B.DocNo ");
                SQLJournal.AppendLine("INNER JOIN tbljournalhdr C ON A.JournalDocNo = C.DocNo ");
                SQLJournal.AppendLine("LEFT JOIN tbljournaldtl D ON C.DocNo = D.DocNo ");
                SQLJournal.AppendLine("LEFT JOIN tblcoa E ON D.AcNo = E.AcNo ");
                SQLJournal.AppendLine("Where A.DocNo=@DocNo Order By D.CAmt Asc, D.AcNo");


                using (var cnJournal = new MySqlConnection(Gv.ConnectionString))
                {
                    cnJournal.Open();
                    cmJournal.Connection = cnJournal;
                    cmJournal.CommandText = SQLJournal.ToString();
                    Sm.CmParam<String>(ref cmJournal, "@DocNo", TxtDocNo.Text);

                    var drJournal = cmJournal.ExecuteReader();
                    var cJournal = Sm.GetOrdinal(drJournal, new string[]
                            {
                             //0
                             "JournalDocNo",

                             //1-5
                             "CASDocNo",
                             "AcNo",
                             "AcDesc",
                             "DAmt",
                             "CAmt",

                            });
                    if (drJournal.HasRows)
                    {
                        int nomor = 0;
                        while (drJournal.Read())
                        {
                            lJournal.Add(new CASJournal()
                            {
                                AcNo = Sm.DrStr(drJournal, cJournal[2]),
                                AcDesc = Sm.DrStr(drJournal, cJournal[3]),
                                DAmt = Sm.DrDec(drJournal, cJournal[4]),
                                CAmt = Sm.DrDec(drJournal, cJournal[5])

                            });
                        }
                    }

                    drJournal.Close();
                }
                myLists.Add(lJournal);

                #endregion

                #region Sign IMS

                var cm3 = new MySqlCommand();

                var SQL3 = new StringBuilder();
                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;

                    SQL3.AppendLine(" Select Distinct  ");
                    SQL3.AppendLine("B.UserCode, C.UserCode2, D.UserCode3, B.UserName, C.UserName2, D.UserName3  ");
                    SQL3.AppendLine("From tblcashadvancesettlementhdr A  ");
                    SQL3.AppendLine("Inner Join (  ");
                    SQL3.AppendLine("    Select Distinct  ");
                    SQL3.AppendLine("    A.DocNo , B.UserCode As UserCode,  Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName  ");
                    SQL3.AppendLine("    From tblcashadvancesettlementhdr A  ");
                    SQL3.AppendLine("    Inner Join TblDocApproval B On B.DocType='CashAdvanceSettlement' And A.DocNo=B.DocNo  ");
                    SQL3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode  ");
                    SQL3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'CashAdvanceSettlement'  ");
                    SQL3.AppendLine("    Where D.Level = '1' And A.DocNo=@DocNo  ");
                    SQL3.AppendLine(" ) B On A.DocNo = B.DocNo  ");
                    SQL3.AppendLine("Left Join (  ");
                    SQL3.AppendLine("    Select Distinct  ");
                    SQL3.AppendLine("    A.DocNo, B.UserCode As UserCode2, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName2 ");
                    SQL3.AppendLine("    From tblcashadvancesettlementhdr A  ");
                    SQL3.AppendLine("    Inner Join TblDocApproval B On B.DocType='CashAdvanceSettlement' And A.DocNo=B.DocNo  ");
                    SQL3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode  ");
                    SQL3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'CashAdvanceSettlement'  ");
                    SQL3.AppendLine("    Where D.Level = '2' And A.DocNo=@DocNo  ");
                    SQL3.AppendLine(" ) C On A.DocNo = C.DocNo  ");
                    SQL3.AppendLine("Left Join (   ");
                    SQL3.AppendLine("    Select Distinct  ");
                    SQL3.AppendLine("   A.DocNo, B.UserCode As UserCode3, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName)))  As UserName3  ");
                    SQL3.AppendLine("    From tblcashadvancesettlementhdr A  ");
                    SQL3.AppendLine("    Inner Join TblDocApproval B On B.DocType='CashAdvanceSettlement' And A.DocNo=B.DocNo  ");
                    SQL3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode  ");
                    SQL3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'CashAdvanceSettlement'  ");
                    SQL3.AppendLine("    Where D.Level = '3' And A.DocNo=@DocNo  ");
                    SQL3.AppendLine(") D On A.DocNo = D.DocNo  ");

                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[]
                                {
                                 //0
                                 "UserName" ,

                                 //1-2
                                 "Username2" ,
                                 "Username3" ,

                                });
                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {

                            lSign.Add(new CASSignIMS()
                            {
                                UserName = Sm.DrStr(dr3, c3[0]),
                                UserName2 = Sm.DrStr(dr3, c3[1]),
                                UserName3 = Sm.DrStr(dr3, c3[2]),
                            });
                        }
                    }
                    dr3.Close();
                }
                myLists.Add(lSign);

                #endregion               

                #region Signature AMKA

                var cm4 = new MySqlCommand();
                var SQL4 = new StringBuilder();

                SQL4.AppendLine("SELECT T1.EmpPict1, T1.Description1, T1.UserName1, T1.ApproveDt1, T1.UserCode1, EmpPict2, T2.Description2, T2.UserName2, T2.ApproveDt2, T2.UserCode2, EmpPict3, T3.Description3, T3.Username3, T3.ApproveDt3, T3.UserCode3, EmpPict4, T4.Description4, T4.Username4, T4.ApproveDt4, T4.UserCode4 ");
                SQL4.AppendLine("FROM ");
                SQL4.AppendLine("( ");
                SQL4.AppendLine("     SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict1, 'Atasan YBS' AS Description1, C.UserName AS UserName1, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt1, C.UserCode AS UserCode1 ");
                SQL4.AppendLine("     FROM TblDocApproval A ");
                SQL4.AppendLine("     INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
                SQL4.AppendLine("         AND A.docno = @DocNo ");
                SQL4.AppendLine("         AND B.DNo = A.ApprovalDNo ");
                SQL4.AppendLine("         And B.Level = 1 ");
                SQL4.AppendLine("         AND A.Status = 'A' ");
                SQL4.AppendLine("     INNER JOIN TblUser C ON A.UserCode = C.UserCode ");
                SQL4.AppendLine("     LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine(") T1 ");
                SQL4.AppendLine("LEFT JOIN ");
                SQL4.AppendLine("( ");
                SQL4.AppendLine("     SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict2, 'Verifikasi Oleh' AS Description2, C.UserName AS UserName2, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt2, C.UserCode AS UserCode2 ");
                SQL4.AppendLine("     FROM TblDocApproval A ");
                SQL4.AppendLine("     INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
                SQL4.AppendLine("         AND A.docno = @DocNo ");
                SQL4.AppendLine("         AND B.DNo = A.ApprovalDNo ");
                SQL4.AppendLine("         And B.Level = 2 ");
                SQL4.AppendLine("         AND A.Status = 'A' ");
                SQL4.AppendLine("     INNER JOIN TblUser C ON A.UserCode = C.UserCode ");
                SQL4.AppendLine("     LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
                SQL4.AppendLine("LEFT JOIN ");
                SQL4.AppendLine("( ");
                SQL4.AppendLine("     SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict3, ");
                SQL4.AppendLine("     'Diperiksa' AS Description3, C.UserName AS UserName3, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt3, C.UserCode AS UserCode3 ");
                SQL4.AppendLine("    FROM TblDocApproval A ");
                SQL4.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
                SQL4.AppendLine("        AND A.DocNo = @DocNo ");
                SQL4.AppendLine("        AND B.DNo = A.ApprovalDNo ");
                SQL4.AppendLine("        And B.Level = 3 ");
                SQL4.AppendLine("        AND A.Status = 'A' ");
                SQL4.AppendLine("    INNER JOIN TblUser C ON A.UserCode = C.UserCode ");
                SQL4.AppendLine("    LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine(")T3 ON T1.DocNo = T3.DocNo ");
                SQL4.AppendLine("LEFT JOIN ");
                SQL4.AppendLine("( ");
                SQL4.AppendLine("     SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict4, ");
                SQL4.AppendLine("     'Disetujui' AS Description4, C.UserName AS UserName4, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt4, C.UserCode AS UserCode4 ");
                SQL4.AppendLine("    FROM TblDocApproval A ");
                SQL4.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
                SQL4.AppendLine("        AND A.DocNo = @DocNo ");
                SQL4.AppendLine("        AND B.DNo = A.ApprovalDNo ");
                SQL4.AppendLine("        And B.Level = 4 ");
                SQL4.AppendLine("        AND A.Status = 'A' ");
                SQL4.AppendLine("    INNER JOIN TblUser C ON A.UserCode = C.UserCode ");
                SQL4.AppendLine("    LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine(")T4 ON T1.DocNo = T4.DocNo ");
                //SQL4.AppendLine("LEFT JOIN ");
                //SQL4.AppendLine("( ");
                //SQL4.AppendLine("    SELECT distinct A.DocNo, Concat(IfNull(G.ParValue, ''), D.UserCode, '.JPG') AS EmpPict5, ");
                //SQL4.AppendLine("    'DiBuat' AS Description5, D.UserName AS UserName5, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt5, D.UserCode AS UserCode5 ");
                //SQL4.AppendLine("    FROM TblDocApproval A ");
                //SQL4.AppendLine("    INNER JOIN tblcashadvancesettlementhdr B ON A.DocNo = B.DocNo ");
                //SQL4.AppendLine("    INNER JOIN tbluser D ON B.PIC = D.UserCode ");
                //SQL4.AppendLine("    LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                //SQL4.AppendLine("    WHERE B.DocNo = @DocNo ");
                //SQL4.AppendLine(")T5 ON T1.DocNo = T5.DocNo; ");



                using (var cn4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn4.Open();
                    cm4.Connection = cn4;
                    cm4.CommandText = SQL4.ToString();
                    Sm.CmParam<String>(ref cm4, "@DocNo", TxtDocNo.Text);
                    var dr4 = cm4.ExecuteReader();
                    var c4 = Sm.GetOrdinal(dr4, new string[]
                {
                    //0

                    "EmpPict1",
                    "Description1",
                    "Username1",
                    "ApproveDt1",
                    "UserCode1",

                    "EmpPict2",
                    "Description2",
                    "Username2",
                    "ApproveDt2",
                    "UserCode2",

                    "EmpPict3",
                    "Description3",
                    "Username3",
                    "ApproveDt3",
                    "UserCode3",

                    "EmpPict4",
                    "Description4",
                    "Username4",
                    "ApproveDt4",
                    "UserCode4",
                });
                    if (dr4.HasRows)
                    {
                        while (dr4.Read())
                        {
                            lSignAMKA.Add(new CASSignAMKA()
                            {
                                EmpPict1 = Sm.DrStr(dr4, c4[0]),
                                Description1 = Sm.DrStr(dr4, c4[1]),
                                UserName1 = Sm.DrStr(dr4, c4[2]),
                                ApproveDt1 = Sm.DrStr(dr4, c4[3]),
                                UserCode1 = Sm.DrStr(dr4, c4[4]),
                                EmpPict2 = Sm.DrStr(dr4, c4[5]),
                                Description2 = Sm.DrStr(dr4, c4[6]),
                                UserName2 = Sm.DrStr(dr4, c4[7]),
                                ApproveDt2 = Sm.DrStr(dr4, c4[8]),
                                UserCode2 = Sm.DrStr(dr4, c4[9]),
                                EmpPict3 = Sm.DrStr(dr4, c4[10]),
                                Description3 = Sm.DrStr(dr4, c4[11]),
                                UserName3 = Sm.DrStr(dr4, c4[12]),
                                ApproveDt3 = Sm.DrStr(dr4, c4[13]),
                                UserCode3 = Sm.DrStr(dr4, c4[14]),
                                EmpPict4 = Sm.DrStr(dr4, c4[15]),
                                Description4 = Sm.DrStr(dr4, c4[16]),
                                UserName4 = Sm.DrStr(dr4, c4[17]),
                                ApproveDt4 = Sm.DrStr(dr4, c4[18]),
                                UserCode4 = Sm.DrStr(dr4, c4[19]),
                            });
                        }
                    }
                    dr4.Close();
                }
                myLists.Add(lSignAMKA);

                #endregion

                #region Signature SIER

                var cm5 = new MySqlCommand();
                var SQL5 = new StringBuilder();
                using (var cn5 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn5.Open();
                    cm5.Connection = cn5;

                    SQL5.AppendLine("SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') AS EmpPict1, 'Proposed By,' AS Description1,  ");
                    SQL5.AppendLine("B.UserName AS UserName1, CONCAT('Date : ',DATE_FORMAT(LEFT(A.CreateDt,8), '%d/%m/%Y')) ApproveDt1, C.UserCode AS UserCode1, ");
                    SQL5.AppendLine("D.PosName AS PosName1 ");
                    SQL5.AppendLine("FROM tblcashadvancesettlementhdr A ");
                    SQL5.AppendLine("INNER JOIN tbluser B ON A.CreateBy = B.UserCode ");
                    SQL5.AppendLine("LEFT JOIN tblemployee C ON B.UserCode = C.UserCode ");
                    SQL5.AppendLine("LEFT JOIN tblposition D ON C.PosCode = D.PosCode ");
                    SQL5.AppendLine("LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                    SQL5.AppendLine("WHERE A.DocNo = @DocNo; ");



                    cm5.CommandText = SQL5.ToString();
                    Sm.CmParam<String>(ref cm5, "@DocNo", TxtDocNo.Text);
                    var dr5 = cm5.ExecuteReader();
                    var c5 = Sm.GetOrdinal(dr5, new string[]
                {
                    //0

                    "EmpPict1",

                    "Description1",
                    "Username1",
                    "ApproveDt1",
                    "UserCode1",
                    "PosName1",
                });
                    if (dr5.HasRows)
                    {
                        while (dr5.Read())
                        {
                            lSignSIER.Add(new CASSignSIER()
                            {
                                EmpPict1 = Sm.DrStr(dr5, c5[0]),
                                Description1 = Sm.DrStr(dr5, c5[1]),
                                UserName1 = Sm.DrStr(dr5, c5[2]),
                                ApproveDt1 = Sm.DrStr(dr5, c5[3]),
                                UserCode1 = Sm.DrStr(dr5, c5[4]),

                                PosName1 = Sm.DrStr(dr5, c5[5]),
                            });
                        }
                    }
                    dr5.Close();
                }
                myLists.Add(lSignSIER);

                #endregion

                #region Signature BBT

                var cm6 = new MySqlCommand();
                var SQL6 = new StringBuilder();

                using (var cn6 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn6.Open();
                    cm6.Connection = cn6;

                    SQL6.AppendLine("Select Distinct Concat(IfNull(T3.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQL6.AppendLine("T1.UserName, T2.GrpName As PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQL6.AppendLine("From ( ");
                    SQL6.AppendLine("	Select Distinct ");
                    SQL6.AppendLine("	B.UserName, B.UserCode, B.GrpCode, '00' As DNo, 0 As Level, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                    SQL6.AppendLine("	From TblCashAdvanceSettlementHdr A ");
                    SQL6.AppendLine("	Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    SQL6.AppendLine("	Where A.DocNo=@DocNo ");
                    SQL6.AppendLine("   Union All ");
                    SQL6.AppendLine("	Select Distinct ");
                    SQL6.AppendLine("	C.UserName, B.UserCode, C.GrpCode, B.ApprovalDNo As DNo, D.Level, 'Approved By, ' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                    SQL6.AppendLine("	From TblCashAdvanceSettlementHdr A ");
                    SQL6.AppendLine("	Inner Join TblDocApproval B On B.DocType='CashAdvanceSettlement' And A.DocNo=B.DocNo ");
                    SQL6.AppendLine("	Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQL6.AppendLine("	Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'CashAdvanceSettlement' ");
                    SQL6.AppendLine("	Where A.DocNo=@DocNo And B.LastUpDt Is Not Null ");
                    SQL6.AppendLine(") T1 ");
                    SQL6.AppendLine("Inner Join TblGroup T2 On T1.GrpCode = T2.GrpCode ");
                    SQL6.AppendLine("Left Join TblParameter T3 On T3.ParCode = 'ImgFileSignature' ");
                    SQL6.AppendLine("Group  By T3.ParValue, T1.UserName, T2.GrpName ");
                    SQL6.AppendLine("Order By T1.Level; ");

                    cm6.CommandText = SQL6.ToString();
                    Sm.CmParam<String>(ref cm6, "@Space", "-------------------------");
                    Sm.CmParam<String>(ref cm6, "@DocNo", TxtDocNo.Text);
                    var dr6 = cm6.ExecuteReader();
                    var c6 = Sm.GetOrdinal(dr6, new string[]
                            {
                             //0
                             "Signature" ,

                             //1-5
                             "UserName" ,
                             "PosName",
                             "Space",
                             "Level",
                             "Title",

                             "LastupDt"
                            });
                    if (dr6.HasRows)
                    {
                        while (dr6.Read())
                        {

                            lSignBBT.Add(new CASSignBBT()
                            {
                                Signature = Sm.DrStr(dr6, c6[0]),
                                UserName = Sm.DrStr(dr6, c6[1]),
                                PosName = Sm.DrStr(dr6, c6[2]),
                                Space = "",
                                DNo = Sm.DrStr(dr6, c6[4]),
                                Title = Sm.DrStr(dr6, c6[5]),
                                LastUpDt = Sm.DrStr(dr6, c6[6]),
                            });
                        }
                    }
                    dr6.Close();
                }
                myLists.Add(lSignBBT);

                #endregion


                if (Doctitle == "IMS")
                    Sm.PrintReport("CAS", myLists, TableName, false);

                else if (Doctitle == "SIER")
                    Sm.PrintReport("CASSIER", myLists, TableName, false);

                else if (Doctitle == "AMKA")
                    Sm.PrintReport("CASAMKA", myLists, TableName, false);

                else if (Doctitle == "BBT")
                    Sm.PrintReport("CASBBT", myLists, TableName, false);

                else
                    Sm.PrintReport("CAS", myLists, TableName, false);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CashAdvanceSettlement", "TblCashAdvanceSettlementHdr");
            string VoucherRequestDocNo = string.Empty;
            var IsNeedApproval = IsDocNeedApproval();
            var cml = new List<MySqlCommand>();

            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
            else
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            cml.Add(SaveCashAdvanceSettlementHdr(DocNo, IsNeedApproval));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    cml.Add(SaveCashAdvanceSettlementDtl(DocNo, r));

            for (int r = 0; r < Grd2.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd2, r, 0).Length > 0)
                    cml.Add(SaveCashAdvanceSettlementDtl2(DocNo, r));

            for (int r = 0; r < Grd3.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd3, r, 0).Length > 0)
                    cml.Add(SaveCashAdvanceSettlementDtl3(DocNo, r));

            for (int r = 0; r < Grd4.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
                    cml.Add(SaveCashAdvanceSettlementDtl4(DocNo, r));

            if (mCASJournalFormula == "1")
            {
                if (Grd4.Rows.Count > 1)
                {
                    if (!mIsCASUseDraftDocument || (mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F"))
                    {
                        cml.Add(SaveVoucherRequest(VoucherRequestDocNo, DocNo));
                    }
                }
                else // Grd4.Rows.Count <= 1
                {
                    if (IsNeedApproval)
                    {
                        if (!mIsCASUseDraftDocument || (mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F"))
                        {
                            cml.Add(SaveDocApproval(DocNo));
                        }
                    }
                    else
                    {
                        if (!mIsCASUseDraftDocument || (mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F"))
                        {
                            if (mIsAutoJournalActived)
                                cml.Add(SaveJournal(DocNo));
                        }
                    }
                }
            }
            else
            {
                decimal Amt1 = Decimal.Parse(TxtAmt1.Text);
                decimal Amt2 = Decimal.Parse(TxtAmt2.Text);

                if (mCASNotEqualToCostFormula == "1")
                {
                    if (Amt1 != Amt2 && ChkCompletedInd.Checked)
                    {
                        cml.Add(SaveVoucherRequest(VoucherRequestDocNo, DocNo));
                    }
                    else
                    {
                        if (mIsAutoJournalActived)
                            cml.Add(SaveJournal(DocNo));
                    }
                }
                else
                {
                    if (mIsCASUseCompletedInd)
                    {
                        if (Amt1 != Amt2 && ChkCompletedInd.Checked)
                            cml.Add(SaveVoucherRequest(VoucherRequestDocNo, DocNo));
                        if (Amt1 == Amt2 && ChkCompletedInd.Checked)
                            cml.Add(SaveDocApproval(DocNo));
                            //cml.Add(UpdateStatusApproval(DocNo));

                        if (!IsNeedApproval && mIsAutoJournalActived && ChkCompletedInd.Checked)
                            cml.Add(SaveJournal(DocNo));
                    }
                        
                }
            }

            Sm.ExecCommands(cml);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, r, 14).Length > 0 && Sm.GetGrdStr(Grd1, r, 14) != "openFileDialog1")
                    {
                        UploadFile(DocNo, r, Sm.GetGrdStr(Grd1, r, 14));
                        // IsFile = true;
                    }
                }
            }


            ShowData(DocNo);
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            decimal
                Amt1 = decimal.Parse(TxtAmt1.Text),
                Amt2 = decimal.Parse(TxtAmt2.Text);

            if (mCASJournalFormula == "1")
            {
                if (Amt1 == Amt2)
                {
                    SQL.AppendLine("Update TblCashAdvanceSettlementHdr Set ");
                    SQL.AppendLine("    JournalDocNo=");
                    SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
                    SQL.AppendLine("Where DocNo=@DocNo;");

                    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select A.JournalDocNo, ");
                    SQL.AppendLine("A.DocDt, ");
                    SQL.AppendLine("Concat('Cash Advance Settlement : ', @DocNo) As JnDesc, ");
                    SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
                    if (mIsCASUseCostCenter)
                        SQL.AppendLine("A.CCCode, ");
                    else
                        SQL.AppendLine("@CCCode, ");
                    SQL.AppendLine("A.Remark, ");
                    SQL.AppendLine("A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("Where A.DocNo=@DocNo; ");

                    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                    SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblJournalHdr A ");
                    SQL.AppendLine("Inner Join ( ");
                    SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                    //Amt1=Amt2
                    if (mIsCASAllowMinusAmount)
                    {
                        SQL.AppendLine("        Select B.AcNo As AcNo, ");
                        SQL.AppendLine("        IfNull(A.Amt,0.00) As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                        SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode=B.CCtCode And B.AcNo Is Not Null ");
                        SQL.AppendLine("        Where A.Amt >= 0 And A.DocNo=@DocNo ");
                        SQL.AppendLine("        Union All ");
                        SQL.AppendLine("        Select B.AcNo As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        SQL.AppendLine("        IfNull(A.Amt,0.00)*-1 As CAmt ");
                        SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                        SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode=B.CCtCode And B.AcNo Is Not Null ");
                        SQL.AppendLine("        Where A.Amt < 0 And A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select B.AcNo As AcNo, ");
                        SQL.AppendLine("        IfNull(A.Amt,0.00) As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                        SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode=B.CCtCode And B.AcNo Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }

                    if (mIsBankAccountUseCostCenterAndInterOffice)
                    {
                        SQL.AppendLine("        Union All ");
                        if (mVoucherCASJournalAcNoSource == "1")
                            SQL.AppendLine("Select C.COAAcNoInteroffice As AcNo, ");
                        else if (mVoucherCASJournalAcNoSource == "2")
                            SQL.AppendLine("Select D.ParValue As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        SQL.AppendLine("        A.Amt As CAmt ");
                        SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                        SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("        Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode And C.COAAcNoInterOffice Is Not Null ");
                        if (mVoucherCASJournalAcNoSource == "2")
                            SQL.AppendLine("Inner Join TblParameter D On D.ParCode='CashAdvanceJournalDebitAcNo' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        if (mCashAdvanceJournalDebitFormat == "1")
                        {
                            SQL.AppendLine("        Union All ");
                            SQL.AppendLine("        Select B.ParValue As AcNo, ");
                            SQL.AppendLine("        0.00 As DAmt, ");
                            SQL.AppendLine("        A.Amt2 As CAmt ");
                            SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CashAdvanceJournalDebitAcNo' And B.ParValue Is Not Null ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                        else
                        {
                            SQL.AppendLine("        Union All ");
                            SQL.AppendLine("        Select B.AcNo9 As AcNo, ");
                            SQL.AppendLine("        0.00 As DAmt, ");
                            SQL.AppendLine("        A.Amt2 As CAmt ");
                            SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                            SQL.AppendLine("        Inner Join TblDepartment B On A.DeptCode = B.DeptCode And B.AcNo9 Is Not Null ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                    }

                    #region Old Code
                    //if (mIsVRBudgetUseCASBA && mIsVoucherCASBA)
                    //{                
                    //    SQL.AppendLine("        Union All ");
                    //    SQL.AppendLine("        Select D.COAAcNo As AcNo, ");
                    //    SQL.AppendLine("        0.00 As DAmt, ");
                    //    SQL.AppendLine("        B.Amt As CAmt ");
                    //    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    //    SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo And A.DocNo = @DocNo ");
                    //    SQL.AppendLine("        Inner Join TblVoucherHdr C On B.VoucherDocNo = C.DocNo ");
                    //    SQL.AppendLine("            And C.AcType = 'D' ");
                    //    SQL.AppendLine("        Inner Join TblBankAccount D On C.BankAcCode = D.BankAcCode ");

                    //    SQL.AppendLine("        Union All ");
                    //    SQL.AppendLine("        Select D.COAAcNo As AcNo, ");
                    //    SQL.AppendLine("        0.00 As DAmt, ");
                    //    SQL.AppendLine("        B.Amt As CAmt ");
                    //    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    //    SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo And A.DocNo = @DocNo ");
                    //    SQL.AppendLine("        Inner Join TblVoucherHdr C On B.VoucherDocNo = C.DocNo ");
                    //    SQL.AppendLine("            And C.AcType2 = 'D' ");
                    //    SQL.AppendLine("        Inner Join TblBankAccount D On C.BankAcCode2 = D.BankAcCode ");
                    //}

                    ////Amt1<Amt2
                    //if (Amt1 < Amt2)
                    //{
                    //    SQL.AppendLine("        Select B.AcNo As AcNo, ");
                    //    SQL.AppendLine("        A.Amt As DAmt, ");
                    //    SQL.AppendLine("        0.00 As CAmt ");
                    //    SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                    //    SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode=B.CCtCode And B.AcNo Is Not Null ");
                    //    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    //    SQL.AppendLine("        Union All ");
                    //    SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
                    //    SQL.AppendLine("        A.Amt2-A.Amt1 As DAmt, ");
                    //    SQL.AppendLine("        0.00 As CAmt ");
                    //    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    //    SQL.AppendLine("        Inner Join TblBankAccount B On B.BankAcCode=@BankAcCode And B.COAAcNo Is Not Null ");
                    //    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    //    SQL.AppendLine("        Union All ");
                    //    SQL.AppendLine("        Select B.ParValue As AcNo, ");
                    //    SQL.AppendLine("        0.00 As DAmt, ");
                    //    SQL.AppendLine("        A.Amt2 As CAmt ");
                    //    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    //    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CashAdvanceJournalDebitAcNo' And B.ParValue Is Not Null ");
                    //    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    //}

                    ////Amt1>Amt2
                    //if (Amt1 > Amt2)
                    //{
                    //    SQL.AppendLine("        Select B.AcNo As AcNo, ");
                    //    SQL.AppendLine("        A.Amt As DAmt, ");
                    //    SQL.AppendLine("        0.00 As CAmt ");
                    //    SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                    //    SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode=B.CCtCode And B.AcNo Is Not Null ");
                    //    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    //    SQL.AppendLine("        Union All ");
                    //    SQL.AppendLine("        Select B.ParValue As AcNo, ");
                    //    SQL.AppendLine("        0.00 As DAmt, ");
                    //    SQL.AppendLine("        A.Amt2 As CAmt ");
                    //    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    //    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CashAdvanceJournalDebitAcNo' And B.ParValue Is Not Null ");
                    //    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    //    SQL.AppendLine("        Union All ");
                    //    SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
                    //    SQL.AppendLine("        0.00 As DAmt, ");
                    //    SQL.AppendLine("        A.Amt1-A.Amt2 As CAmt ");
                    //    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    //    SQL.AppendLine("        Inner Join TblBankAccount B On B.BankAcCode=@BankAcCode And B.COAAcNo Is Not Null ");
                    //    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    //}
                    #endregion

                    SQL.AppendLine("    ) Tbl ");
                    SQL.AppendLine("    Where AcNo Is Not Null ");
                    SQL.AppendLine("    Group By AcNo ");
                    SQL.AppendLine(") B On 1=1 ");
                    SQL.AppendLine("Where A.DocNo In ( ");
                    SQL.AppendLine("    Select JournalDocNo ");
                    SQL.AppendLine("    From TblCashAdvanceSettlementHdr ");
                    SQL.AppendLine("    Where DocNo=@DocNo ");
                    SQL.AppendLine("    And JournalDocNo Is Not Null ");
                    SQL.AppendLine("    ); ");

                    if (mIsVRForBudgetUseSOContract || mIsVRUseSOContract)
                    {
                        SQL.AppendLine("Update TblJournalDtl A ");
                        SQL.AppendLine("Inner Join TblCashAdvanceSettlementHdr B On A.DocNo = B.JournalDocNo ");
                        SQL.AppendLine("    And B.DocNo = @DocNo ");
                        SQL.AppendLine("Inner Join TblCashAdvanceSettlementDtl C On B.DocNo = C.DocNo ");
                        SQL.AppendLine("Inner Join TblVoucherRequestHdr D On C.VoucherDocNo = D.VoucherDocNo ");
                        SQL.AppendLine("Set A.SOContractDocNo = D.SOContractDocNo ");
                        SQL.AppendLine("Where D.SOContractDocNo Is Not Null; ");
                    }

                    SQL.AppendLine("Update TblJournalDtl A ");
                    SQL.AppendLine("Inner Join TblCashAdvanceSettlementHdr B On A.DocNo = B.JournalDocNo And B.DocNo = @DocNo ");
                    SQL.AppendLine("Inner JOin TblCashAdvanceSettlementDtl B1 On B.DocNo = B1.DocNo ");
                    SQL.AppendLine("Inner Join TblVoucherHdr C On B1.VoucherDocNo = C.DocNo And C.Doctype = '61' ");
                    SQL.AppendLine("Inner Join TblTravelRequestDtl8 D On C.VoucherRequestDocNo = D.VoucherRequestDocNo ");
                    SQL.AppendLine("Inner Join TblTravelRequestHdr E On D.DocNo = E.DocNo ");
                    SQL.AppendLine("Set A.SOContractDocNo = E.SOContractDocNo ");
                    SQL.AppendLine("Where B.DocNo = @DocNo ");
                    SQL.AppendLine("And E.SOContractDocNo Is Not Null; ");
                }
            }
            else
            {
                SQL.AppendLine("Update TblCashAdvanceSettlementHdr Set ");
                SQL.AppendLine("    JournalDocNo=");
                SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
                SQL.AppendLine("Where DocNo=@DocNo;");

                SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.JournalDocNo, ");
                SQL.AppendLine("A.DocDt, ");
                SQL.AppendLine("Concat('Cash Advance Settlement : ', @DocNo) As JnDesc, ");
                SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
                if (mIsCASUseCostCenter)
                    SQL.AppendLine("A.CCCode, ");
                else
                    SQL.AppendLine("@CCCode, ");
                SQL.AppendLine("A.Remark, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("Where A.DocNo=@DocNo; ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                if (Amt1 == Amt2)
                {
                    SQL.AppendLine("        Select B.AcNo As AcNo, ");
                    SQL.AppendLine("        A.Amt As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                    SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode=B.CCtCode And B.AcNo Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");

                    SQL.AppendLine("        Union All ");

                    SQL.AppendLine("        Select C.COAAcNo As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        A.Amt As CAmt ");
                    SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                    SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("        Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode And C.COAAcNo Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    if (mCASNotEqualToCostFormula == "1")
                    {
                        //Cost < CAS
                        if (Amt1 < Amt2)
                        {
                            SQL.AppendLine("        Select B.AcNo, A.Amt As DAmt, 0.00 As CAmt ");
                            SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                            SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode = B.CCtCode And B.AcNo Is Not Null ");
                            SQL.AppendLine("        Where A.DocNo = @DocNo ");

                            SQL.AppendLine("        Union All ");

                            SQL.AppendLine("        Select IfNull(C.COAAcNo, D.COAAcNo) AcNo, (B.Amt - (A.Amt + IfNull(C.Amt, 0.00))) DAmt, 0.00 CAmt ");
                            SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                            SQL.AppendLine("        Inner Join TblVoucherHdr B On A.VoucherDocNo = B.DocNo ");
                            SQL.AppendLine("        Left Join ");
                            SQL.AppendLine("        ( ");
                            SQL.AppendLine("            Select T2.VoucherDocNo, Sum(T2.Amt) Amt ");
                            SQL.AppendLine("            From TblCashAdvanceSettlementHdr T1 ");
                            SQL.AppendLine("            Inner Join TblCashAdvanceSettlementDtl T2 On T1.DocNo = T2.DocNo ");
                            SQL.AppendLine("                And T1.Status In ('O', 'A') And T1.CancelInd = 'N' ");
                            SQL.AppendLine("                And T1.DocNo != @DocNo ");
                            SQL.AppendLine("                And Concat(Right(T1.DocNo, 2), Substring(T1.DocNo, -5, 2), Substring_index(T1.DocNo, '/', 1)) < Concat(Right(@DocNo, 2), Substring(@DocNo, -5, 2), Substring_index(@DocNo, '/', 1)) ");
                            SQL.AppendLine("            Group By T2.VoucherDocNo ");
                            SQL.AppendLine("        ) C On B.DocNo = C.VoucherDocNo ");
                            SQL.AppendLine("        Left Join TblBankAccount C On B.BankAcCode = C.BankAcCode And B.AcType = 'C' And C.COAAcNo Is Not Null ");
                            SQL.AppendLine("        Left Join TblBankAccount D On B.BankAcCode2 = D.BankAcCode And B.AcType2 = 'C' And D.COAAcNo Is Not Null ");
                            SQL.AppendLine("        Where A.DocNo = @DocNo ");

                            SQL.AppendLine("        Union All ");

                            SQL.AppendLine("        Select D.COAAcNo As AcNo, 0.00 As DAmt, (A.Amt + (B.Amt - (A.Amt + IfNull(E.Amt, 0.00)))) As CAmt ");
                            SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                            SQL.AppendLine("        Inner Join TblVoucherHdr B On A.VoucherDocNo = B.DocNo ");
                            SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On A.DocNo = C.DocNo ");
                            SQL.AppendLine("        Inner Join TblBankAccount D On C.BankAcCode = D.BankAcCode And D.COAAcNo Is Not Null ");
                            SQL.AppendLine("        Left Join ");
                            SQL.AppendLine("        ( ");
                            SQL.AppendLine("            Select T2.VoucherDocNo, Sum(T2.Amt) Amt ");
                            SQL.AppendLine("            From TblCashAdvanceSettlementHdr T1 ");
                            SQL.AppendLine("            Inner Join TblCashAdvanceSettlementDtl T2 On T1.DocNo = T2.DocNo ");
                            SQL.AppendLine("                And T1.Status In ('O', 'A') And T1.CancelInd = 'N' ");
                            SQL.AppendLine("                And T1.DocNo != @DocNo ");
                            SQL.AppendLine("                And Concat(Right(T1.DocNo, 2), Substring(T1.DocNo, -5, 2), Substring_index(T1.DocNo, '/', 1)) < Concat(Right(@DocNo, 2), Substring(@DocNo, -5, 2), Substring_index(@DocNo, '/', 1)) ");
                            SQL.AppendLine("            Group By T2.VoucherDocNo ");
                            SQL.AppendLine("        ) E On B.DocNo = E.VoucherDocNo ");
                            SQL.AppendLine("        Where A.DocNo = @DocNo ");
                        }
                    }
                    else 
                    {
                        //Cost < CAS
                        if (Amt1 < Amt2)
                        {
                            SQL.AppendLine("        Select B.AcNo, A.Amt As DAmt, 0.00 As CAmt ");
                            SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                            SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode = B.CCtCode And B.AcNo Is Not Null ");
                            SQL.AppendLine("        Where A.DocNo = @DocNo ");

                            SQL.AppendLine("        Union All ");

                            SQL.AppendLine("        Select B.COAAcNo As AcNo, 0.00 As DAmt, A.Amt1 As CAmt ");
                            SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode = B.BankAcCode And B.COAAcNo Is Not Null ");
                            SQL.AppendLine("        Where A.DocNo = @DocNo ");
                        }
                        //Cost > CAS
                        if (Amt1 > Amt2)
                        {
                            SQL.AppendLine("        Select B.AcNo, A.Amt As DAmt, 0.00 As CAmt ");
                            SQL.AppendLine("        From TblCashAdvanceSettlementDtl A ");
                            SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode = B.CCtCode And B.AcNo Is Not Null ");
                            SQL.AppendLine("        Where A.DocNo = @DocNo ");

                            SQL.AppendLine("        Union All ");

                            SQL.AppendLine("        Select B.COAAcNo As AcNo, 0.00 As DAmt, A.Amt1 As CAmt ");
                            SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode = B.BankAcCode And B.COAAcNo Is Not Null ");
                            SQL.AppendLine("        Where A.DocNo = @DocNo ");
                        }
                    }
                }

                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine("    Where AcNo Is Not Null ");
                SQL.AppendLine("    Group By AcNo ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Where A.DocNo In ( ");
                SQL.AppendLine("    Select JournalDocNo ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And JournalDocNo Is Not Null ");
                SQL.AppendLine("    ); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CCCode", GetCCCodeFromDept());
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
            SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
            SQL.Append("From TblVoucherRequestHdr ");
            //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
            //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
            SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
            SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("), '0001' ");
            SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            
            return Sm.GetValue(SQL.ToString());
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    string
        //        Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
        //        Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle"),
        //        DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'");

        //    var SQL = new StringBuilder();

        //    SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //    SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
        //    SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
        //    SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //    SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
        //    SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");

        //    return Sm.GetValue(SQL.ToString());
        //}

        private MySqlCommand SaveVoucherRequest(string VoucherRequestDocNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCashAdvanceSettlementHdr Set VoucherRequestDocNo=@DocNo Where DocNo=@CashAdvanceSettlementDocNo; ");

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("CancelInd, Status, DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, CurCode, AcType2, BankAcCode2, CurCode2, ExcRate, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, Amt, PaymentUser, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, A.DocDt, Null, Null, Null, Null, Null, Null, ");
            SQL.AppendLine("'N', 'O', A.DeptCode, '58', Null, ");
            if (mCASCompletedIndFormula == "1")
                SQL.AppendLine("Case When A.Amt1<A.Amt2 Then 'D' Else 'C' End, A.BankAcCode, A.CurCode, Null, Null, Null, Null, ");
            else
            {
                SQL.AppendLine("'D', Case ");
                SQL.AppendLine("	When A.Amt1>A.Amt2 Then ");
                SQL.AppendLine("		Case When C.AcType = 'D' Then C.BankAcCode Else C.BankAcCode2 End ");
                SQL.AppendLine("	Else ");
                SQL.AppendLine("		Case When C.AcType = 'D' Then C.BankAcCode2 Else C.BankAcCode End ");
                SQL.AppendLine("End As BankAcCode, C.CurCode, ");
                SQL.AppendLine("'C', Case ");
                SQL.AppendLine("	When A.Amt1>A.Amt2 Then ");
                SQL.AppendLine("		Case When C.AcType2 = 'C' Then C.BankAcCode2 Else C.BankAcCode End ");
                SQL.AppendLine("	Else ");
                SQL.AppendLine("		Case When C.AcType2 = 'C' Then C.BankAcCode Else C.BankAcCode2 End ");
                SQL.AppendLine("End As BankAcCode2, C.CurCode2, 1.00, ");
            }
            SQL.AppendLine("Null, Null, Null, Null, A.PIC, 0.00, ");
            if (mCASJournalFormula == "1")
                SQL.AppendLine("Amt3, ");
            else
                SQL.AppendLine("If((A.Amt1-A.Amt2) > 0, (A.Amt1-A.Amt2), (A.Amt2-A.Amt1)), ");
            SQL.AppendLine("Null, Null, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblCashAdvanceSettlementHdr A ");
            SQL.AppendLine("Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo And B.DNo = '00001' ");
            SQL.AppendLine("Inner Join TblVoucherHdr C On B.VoucherDocNo = C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@CashAdvanceSettlementDocNo; ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, '001', 'Amount discrepancy', ");
            if (mCASJournalFormula == "1")
                SQL.AppendLine("Amt3, ");
            else
                SQL.AppendLine("If((Amt1-Amt2) > 0, (Amt1-Amt2), (Amt2-Amt1)), ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblCashAdvanceSettlementHdr Where DocNo=@CashAdvanceSettlementDocNo; ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @CashAdvanceSettlementDocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='CashAdvanceSettlement' And T.DeptCode=@DeptCode ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select IfNull(Amt1, 0) ");
            SQL.AppendLine("    From TblCashAdvanceSettlementHdr ");
            SQL.AppendLine("    Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("), 0)) ");
            SQL.AppendLine("And (T.EndAmt=0 ");
            SQL.AppendLine("Or T.EndAmt>=IfNull(( ");
            SQL.AppendLine("    Select IfNull(Amt1, 0) ");
            SQL.AppendLine("    From TblCashAdvanceSettlementHdr ");
            SQL.AppendLine("    Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("), 0)) ");
            SQL.AppendLine("And (T.DAGCode Is Null Or ");
            SQL.AppendLine("(T.DAGCode Is Not Null ");
            SQL.AppendLine("And T.DAGCode In ( ");
            SQL.AppendLine("    Select A.DAGCode ");
            SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
            SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
            SQL.AppendLine("    And A.ActInd='Y' ");
            SQL.AppendLine("    And B.EmpCode=@PIC ");
            SQL.AppendLine("))); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='CashAdvanceSettlement' ");
            SQL.AppendLine("    And DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblCashAdvanceSettlementHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='CashAdvanceSettlement' ");
            SQL.AppendLine("    And DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblCashAdvanceSettlementHdr B On A.DocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("    And B.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblCashAdvanceSettlementDtl C On B.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr D On C.VoucherDocNo = D.VoucherDocNo ");
            SQL.AppendLine("Set A.SOContractDocNo = D.SOContractDocNo ");
            SQL.AppendLine("Where D.SOContractDocNo Is Not Null; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CashAdvanceSettlementDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@PIC", mPIC);

            return cm;
        }

        private MySqlCommand UpdateStatusApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Update TblCashAdvanceSettlementHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='CashAdvanceSettlement' ");
            SQL.AppendLine("    And DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CashAdvanceSettlementDocNo", DocNo);


            return cm;
        }
        private MySqlCommand SaveDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @CashAdvanceSettlementDocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='CashAdvanceSettlement' And T.DeptCode=@DeptCode ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select IfNull(Amt1, 0) ");
            SQL.AppendLine("    From TblCashAdvanceSettlementHdr ");
            SQL.AppendLine("    Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("), 0)) ");
            SQL.AppendLine("And (T.EndAmt=0 ");
            SQL.AppendLine("Or T.EndAmt>=IfNull(( ");
            SQL.AppendLine("    Select IfNull(Amt1, 0) ");
            SQL.AppendLine("    From TblCashAdvanceSettlementHdr ");
            SQL.AppendLine("    Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("), 0)) ");
            if (mCASApprovalGroupValidation == "1")
            {
                SQL.AppendLine("And (T.DAGCode Is Null Or ");
                SQL.AppendLine("(T.DAGCode Is Not Null ");
                SQL.AppendLine("And T.DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@PIC ");
                SQL.AppendLine("))) ");
            }
            if (mCASApprovalGroupValidation == "2")
            {
                SQL.AppendLine("And (T.DAGCode Is Null Or ");
                SQL.AppendLine("(T.DAGCode Is Not Null ");
                SQL.AppendLine("And T.DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@PIC ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblCashAdvanceSettlementHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='CashAdvanceSettlement' ");
            SQL.AppendLine("    And DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CashAdvanceSettlementDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@PIC", mPIC);


            return cm;
        }
        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueBankAcCode, "Bank account") ||
                Sm.IsTxtEmpty(TxtCurCode, "Currency", false) ||
                (mIsCASUseCostCenter && Sm.IsLueEmpty(LueCCCode, "Cost Center")) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ? 
                    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) : 
                    Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt))) ||
                IsGrdEmpty() ||
                IsDocTypeNotValid() ||
                IsTotalDetailNotValid() ||
                (!mIsCASUsePartialAmount && IsTotalDetailNotValid2()) ||
                IsGrdValueNotValid() ||
                IsUploadFileNotValid()||
                IsCostCenterInvalid() ||
                (Sm.IsLueEmpty(LueDocStatus, "Document Status") && mIsCASUseDraftDocument)||
                IsCompletedInvalid() ||
                //(mIsCheckCOAJournalNotExists && IsCOAJournalNotValid()) ||
                IsJournalSettingInvalid()
                ;
        }

        private bool IsCompletedInvalid()
        {
            if (!mIsCASUseCompletedInd || (mIsCASUseCompletedInd && mCASCompletedIndFormula == "2")) return false;

            decimal Amt1 = Decimal.Parse(TxtAmt1.Text);
            decimal Amt2 = Decimal.Parse(TxtAmt2.Text);

            if (Amt1 > Amt2)
            {
                if (!ChkCompletedInd.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to marked this document as completed.");
                    ChkCompletedInd.Focus();
                    return true;
                }
            }

            return false;
        }

        private bool IsCostCenterInvalid()
        {
            if (!mIsCASUseCostCenter) return false;

            var SQL = new StringBuilder();
            string CCCode = Sm.GetLue(LueCCCode);
            string CCtCode = string.Empty;
            string CCCode2 = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (CCtCode.Length > 0) CCtCode += ",";
                CCtCode += Sm.GetGrdStr(Grd1, i, 3);
            }

            if (CCtCode.Length > 0)
            {
                SQL.AppendLine("Select Group_Concat(Distinct CCCode) CCCode ");
                SQL.AppendLine("From TblCostCategory ");
                SQL.AppendLine("Where Find_In_Set(CCtCode, @Param); ");

                CCCode2 = Sm.GetValue(SQL.ToString(), CCtCode);

                if (CCCode2 != CCCode)
                {
                    Sm.StdMsg(mMsgType.Warning, "Cost center is not match to listed cost category.");
                    LueCCCode.Focus();
                    return true;
                }
            }

            return false;
        }

        //private bool IsCOAJournalNotValid()
        //{
        //    var l = new List<COA>();
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();
        //    string mCCtCode = string.Empty, mVoucherDocNo = string.Empty;
        //    decimal
        //       Amt1 = decimal.Parse(TxtAmt1.Text),
        //       Amt2 = decimal.Parse(TxtAmt2.Text);

        //    for (int r = 0; r < Grd1.Rows.Count; r++)
        //    {
        //        if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
        //        {
        //            if (mCCtCode.Length > 0) mCCtCode += " Or ";
        //            mCCtCode += " (CCtCode=@CCtCode00" + r.ToString() + ") ";
        //            Sm.CmParam<String>(ref cm, "@CCtCode00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
        //        }
        //    }

        //    if (mCCtCode.Length > 0) mCCtCode = " And (" + mCCtCode + ") ";

        //    for (int r = 0; r < Grd1.Rows.Count; r++)
        //    {
        //        if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
        //        {
        //            if (mVoucherDocNo.Length > 0) mVoucherDocNo += " Or ";
        //            mVoucherDocNo += " (A.DocNo=@VoucherDocNo00" + r.ToString() + ") ";
        //            Sm.CmParam<String>(ref cm, "@VoucherDocNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
        //        }
        //    }

        //    if (mVoucherDocNo.Length > 0) mVoucherDocNo = " And (" + mVoucherDocNo + ") ";

        //    bool IsNeedApproval = IsDocNeedApproval();

        //    if (Grd4.Rows.Count <= 1)
        //    {
        //        //if ((IsNeedApproval && !ChkCancelInd.Checked &&
        //        //    ((mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F") || !mIsCASUseDraftDocument)) ||
        //        //   ((!IsNeedApproval && !ChkCancelInd.Checked &&
        //        //     (mIsAutoJournalActived && ((mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F") || !mIsCASUseDraftDocument)))))
        //        if (!ChkCancelInd.Checked &&
        //            (!mIsCASUseDraftDocument || (mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F")) &&
        //            (IsNeedApproval || (!IsNeedApproval && mIsAutoJournalActived))
        //            )
        //        {

        //            SQL.AppendLine("Select AcNo From ");
        //            SQL.AppendLine("( ");
        //            //Amt1=Amt2
        //            if (mIsVRBudgetUseCASBA && mIsVoucherCASBA)
        //            {
        //                SQL.AppendLine("        Select AcNo");
        //                SQL.AppendLine("        From TblCostCategory Where 1 = 1 " + mCCtCode);

        //                SQL.AppendLine("        Union All ");
        //                SQL.AppendLine("        Select B.COAAcNo As AcNo ");
        //                SQL.AppendLine("        From TblVoucherHdr A ");
        //                SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode = B.BankAcCode " + mVoucherDocNo);

        //                SQL.AppendLine("        Union All ");
        //                SQL.AppendLine("        Select B.COAAcNo As AcNo ");
        //                SQL.AppendLine("        From TblVoucherHdr A ");
        //                SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2 = B.BankAcCode " + mVoucherDocNo);
        //            }
        //            else
        //            {
        //                if (Amt1 == Amt2)
        //                {
        //                    SQL.AppendLine("        Select AcNo ");
        //                    SQL.AppendLine("        From TblCostCategory Where 1=1 " + mCCtCode);
        //                    if (mIsVRBudgetUseCASBA && mIsVoucherCASBA)
        //                    {
        //                        SQL.AppendLine("        Union All ");
        //                        SQL.AppendLine("        Select B.COAAcNo As AcNo ");
        //                        SQL.AppendLine("        From TblVoucherHdr A ");
        //                        SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode = B.BankAcCode " + mVoucherDocNo);

        //                        SQL.AppendLine("        Union All ");
        //                        SQL.AppendLine("        Select B.COAAcNo As AcNo ");
        //                        SQL.AppendLine("        From TblVoucherHdr A ");
        //                        SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2 = B.BankAcCode " + mVoucherDocNo);
        //                    }
        //                    else
        //                    {
        //                        if (mCashAdvanceJournalDebitFormat == "1")
        //                        {
        //                            SQL.AppendLine("        Union All ");
        //                            SQL.AppendLine("        Select ParValue As AcNo ");
        //                            SQL.AppendLine("        From TblParameter Where ParCode='CashAdvanceJournalDebitAcNo' ");
        //                        }
        //                        else
        //                        {
        //                            SQL.AppendLine("        Union All ");
        //                            SQL.AppendLine("        Select AcNo9 As AcNo ");
        //                            SQL.AppendLine("        From TblDepartment Where DeptCode = @DeptCode");
        //                        }
        //                    }
        //                }
        //            }

        //            SQL.AppendLine(")T ; ");

        //            using (var cn = new MySqlConnection(Gv.ConnectionString))
        //            {
        //                cn.Open();
        //                cm.Connection = cn;
        //                cm.CommandTimeout = 600;
        //                cm.CommandText = SQL.ToString();

        //                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
        //                var dr = cm.ExecuteReader();
        //                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
        //                if (dr.HasRows)
        //                {
        //                    while (dr.Read())
        //                    {
        //                        l.Add(new COA()
        //                        {
        //                            AcNo = Sm.DrStr(dr, c[0])

        //                        });
        //                    }
        //                }
        //                dr.Close();
        //            }


        //            foreach (var x in l.Where(w => w.AcNo.Length <= 0))
        //            {
        //                Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
        //                return true;
        //            }

        //        }
        //    } 
            
        

        //    return false;
        //}

        private bool IsJournalSettingInvalid()
        {
            if (mIsAutoJournalActived && mIsCheckCOAJournalNotExists)
            {
                var SQL = new StringBuilder();
                decimal
                Amt1 = decimal.Parse(TxtAmt1.Text),
                Amt2 = decimal.Parse(TxtAmt2.Text);
                var IsNeedApproval = IsDocNeedApproval();

                string mCashAdvanceJournalDebitAcNo = Sm.GetValue("Select ParValue From TblParameter Where Parcode='CashAdvanceJournalDebitAcNo'");
                var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

                if ((!(Grd4.Rows.Count > 1) && !IsNeedApproval && (!mIsCASUseDraftDocument || (mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F"))) || !(Amt1 != Amt2 && ChkCompletedInd.Checked))
                    if (mCASJournalFormula == "1")
                    {
                        if (Amt1 == Amt2)
                        {
                            if (IsJournalSettingInvalid_CostCategory(Msg)) return true;
                            if (mIsBankAccountUseCostCenterAndInterOffice)
                            {
                                if (IsJournalSettingInvalid_BankAccount(Msg, "", "COAAcNoInterOffice")) return true;
                            }
                            else
                            {
                                if (mIsBankAccountUseCostCenterAndInterOffice)
                                {
                                    if (IsJournalSettingInvalid_BankAccount(Msg, "", "COAAcNoInterOffice")) return true;
                                    if (mCashAdvanceJournalDebitAcNo.Length == 0 && mVoucherCASJournalAcNoSource == "2")
                                    {
                                        Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CashAdvanceJournalDebitAcNo is empty.");
                                        return true;
                                    }
                                }
                                else
                                {
                                    if (IsJournalSettingInvalid_Department(Msg, "AcNo9")) return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Amt1 == Amt2)
                        {
                            if (IsJournalSettingInvalid_CostCategory(Msg)) return true;
                            if (IsJournalSettingInvalid_BankAccount(Msg, "", "COAAcNo")) return true;
                        }
                        else
                        {
                            if (Amt1 < Amt2)
                            {
                                if (IsJournalSettingInvalid_BankAccount(Msg, "", "COAAcNo") || IsJournalSettingInvalid_BankAccount(Msg, "2", "COAAcNo")) return true;
                                if (IsJournalSettingInvalid_BankAccount(Msg, "", "COAAcNo")) return true;
                            }
                        }
                    }
            }
            return false;
        }

        private bool IsJournalSettingInvalid_CostCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string CCtName = string.Empty, CCtCode = string.Empty;
            SQL.AppendLine("Select B.CCtName ");
            SQL.AppendLine("From TblCashAdvanceSettlementDtl A, TblCostCategory B ");
            SQL.AppendLine("Where A.CCtCode=B.CCtCode And B.AcNo Is Null ");
            SQL.AppendLine("And B.CCtCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                CCtCode = Sm.GetGrdStr(Grd1, r, 3);
                if (CCtCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@CCtCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@CCtCode_" + r.ToString(), CCtCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            CCtName = Sm.GetValue(cm);
            if (CCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Cost category's COA account# (" + CCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_BankAccount(string Msg, string formula, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            decimal
                Amt1 = decimal.Parse(TxtAmt1.Text),
                Amt2 = decimal.Parse(TxtAmt2.Text);
            string BankAcCode = string.Empty, BankAcName = string.Empty;

            SQL.AppendLine("Select B.BankAcNm ");
            SQL.AppendLine("From TblCashAdvanceSettlementHdr A, TblBankAccount B ");
            SQL.AppendLine("Where A.BankAcCode" + formula + " = B.BankAcCode ");
            if (Amt1 < Amt2)
            {
                SQL.AppendLine("A.AcType"+formula+" = 'C'");
            }
            SQL.AppendLine("And B." + COA + " Is Null ");
            SQL.AppendLine("And B.BankAcCode=@BankAcCode ");
            SQL.AppendLine("Limit 1;");
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            cm.CommandText = SQL.ToString();
            BankAcName = Sm.GetValue(cm);
            if (BankAcName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Bank Account's COA account# (" + BankAcName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_Department(string Msg, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string DeptName = string.Empty, DeptCode = string.Empty;
            SQL.AppendLine("Select B.DeptName ");
            SQL.AppendLine("From TblCashAdvanceSettlementHdr A, TblDepartment B ");
            SQL.AppendLine("Where A.DeptCode = B.DeptCode And B."+COA+" Is Null ");
            SQL.AppendLine("And B.DeptCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                DeptCode = Sm.GetGrdStr(Grd1, r, 10);
                if (DeptCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@DeptCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@DeptCode_" + r.ToString(), DeptCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            DeptName = Sm.GetValue(cm);
            if (DeptName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Department's COA account# (" + DeptName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsDocTypeNotValid()
        {
            if (!mIsVRBudgetUseCASBA) return false;

            GetVCDocType();
            mIsVoucherCASBA = IsVoucherCASBA();

            string VCDocType = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (VCDocType.Length == 0) VCDocType = Sm.GetGrdStr(Grd1, i, 20);

                    if (VCDocType != Sm.GetGrdStr(Grd1, i, 20))
                    {
                        Tc1.SelectedTabPage = Tp1;
                        Sm.StdMsg(mMsgType.Warning, "You could not add different type of voucher.");
                        Sm.FocusGrd(Grd1, i, 1);
                        return true;
                    }

                    VCDocType = Sm.GetGrdStr(Grd1, i, 20);
                }
            }

            return false;
        }

        private bool IsUploadFileNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 14).Length > 0 && Sm.GetGrdStr(Grd1, Row, 14) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd1, Row, 14))) return true;
                    }
                }
            }

            return false;
        }

        private bool IsDocNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='CashAdvanceSettlement' And DeptCode = @Param Limit 1;", mDeptCode);
            
        }
       



        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsTotalDetailNotValid() //membandingkan antara total details dengan total cash advanced jika total cost 0
        {
            var AmtCost = decimal.Parse(TxtAmt1.Text);
            var AmtCA = decimal.Parse(TxtAmt2.Text);
            var AmtDetail = decimal.Parse(TxtAmt3.Text);

            if ((mIsCASAllowMinusAmount ? AmtCost == 0 : AmtCost <= 0) && AmtDetail != AmtCA && mIsCASAllowToProcessZeroAmount && Sm.GetLue(LueDocStatus) == "F")
            {
                Sm.StdMsg(mMsgType.Warning, "Total amount cash advance different with total amount detail.");
                return true;
            }

            return false;
        }

        private bool IsTotalDetailNotValid2() //membandingkan antara total details dengan selisih total cash advanced dan total cost 
        {
            var AmtCost = decimal.Parse(TxtAmt1.Text);
            var AmtCA = decimal.Parse(TxtAmt2.Text);
            var AmtDetail = decimal.Parse(TxtAmt3.Text);

            decimal Selisih =0m;
            if(AmtCA > AmtCost)
                Selisih = AmtCA - AmtCost;
            else
                Selisih = AmtCost - AmtCA;

            if (Selisih != AmtDetail)
            {
                Sm.StdMsg(mMsgType.Warning, "The different between Total(Cash Advance) and Total(Cost) should be the same with Total(Detail).");
                TxtAmt3.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            mDeptCode = Sm.GetGrdStr(Grd1, 0, 10);
            TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, 0, 11);
            mPIC = Sm.GetGrdStr(Grd1, 0, 8);
            TxtPIC.EditValue = Sm.GetGrdStr(Grd1, 0, 9);
            var CurCode = TxtCurCode.Text;

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Voucher is empty.")) return true;
                if ((!mIsCASAllowToProcessZeroAmount || mIsCostCategoryCASMandatory) && Sm.IsGrdValueEmpty(Grd1, r, 4, false, "Cost category is empty.")) return true;
                if (!mIsCASAllowToProcessZeroAmount && Sm.IsGrdValueEmpty(Grd1, r, 6, true, "Settlement amount should not be 0.00.")) return true;
                if (mIsCASUseBudgetCategory && Sm.IsGrdValueEmpty(Grd1, r, 32, false, "Budget category is empty.")) return true;
                if (mIsCASUseItemRate && Sm.IsGrdValueEmpty(Grd1, r, 26, false, "Item is empty.")) return true;
                if (mIsCASUseItemRate && Sm.IsGrdValueEmpty(Grd1, r, 6, true, "Amount (Cost) is zero.")) return true;
                if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, r, 5)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Voucher# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                        "Currency : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine + Environment.NewLine +
                        "Currency is not valid."
                        );
                    return true;
                }
                if (!Sm.CompareStr(mPIC, Sm.GetGrdStr(Grd1, r, 8)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Voucher# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                        "Person in charge : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + Environment.NewLine +
                        "Person in charge is not valid."
                        );
                    return true;
                }
                if (!Sm.CompareStr(mDeptCode, Sm.GetGrdStr(Grd1, r, 10)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Voucher# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                        "Department : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine + Environment.NewLine +
                        "Department is not valid."
                        );
                    return true;
                }
            }

            if (Sm.GetLue(LueDocStatus) == "F")
            {
                var Amt1 = decimal.Parse(TxtAmt1.Text);
                var Amt2 = decimal.Parse(TxtAmt2.Text);

                if (!mIsCASUsePartialAmount && !IsVoucherCASBA())
                {
                    if (Amt1 != Amt2)
                    { 
                        if (Grd4.Rows.Count <= 1)
                        {
                            Sm.StdMsg(mMsgType.Warning, "In details information, You need to input at least 1 record.");
                            return true;
                        }
                    }
                    else
                    {
                        if (Grd4.Rows.Count > 1)
                        {
                            Sm.StdMsg(mMsgType.Warning, "You don't need to input any record in details information.");
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveCashAdvanceSettlementHdr(string DocNo, bool IsNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCashAdvanceSettlementHdr ");
            SQL.AppendLine("(DocNo, LocalDocNo, DocDt, Status, CancelInd, CancelReason, DocStatus, PIC, DeptCode, ");
            if (mIsCASUseCostCenter)
                SQL.AppendLine("CCCode, ");
            if (mIsCASUseCompletedInd)
                SQL.AppendLine("CompletedInd, ");
            SQL.AppendLine("CurCode, BankAcCode, Amt1, Amt2, Amt3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @LocalDocNo, @DocDt, @Status, 'N', Null, @DocStatus, @PIC, @DeptCode, ");
            if (mIsCASUseCostCenter)
                SQL.AppendLine("@CCCode, ");
            if (mIsCASUseCompletedInd)
                SQL.AppendLine("@CompletedInd, ");
            SQL.AppendLine("@CurCode, @BankAcCode, @Amt1, @Amt2, @Amt3, @Remark, @UserCode, CurrentDateTime()); ");
            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocument.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", IsNeedApproval ? "O" : "A");
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@PIC", mPIC);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            if (mIsCASUseCostCenter)
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            if (mIsCASUseCompletedInd)
                Sm.CmParam<String>(ref cm, "@CompletedInd", ChkCompletedInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DocStatus", Sm.GetLue(LueDocStatus));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Decimal.Parse(TxtAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt3", Decimal.Parse(TxtAmt3.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCashAdvanceSettlementDtl(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCashAdvanceSettlementDtl(DocNo, DNo, VoucherDocNo, CCtCode, ");
            if (mIsCASUseItemRate)
                SQL.AppendLine("ItCode, Qty, Rate, ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine("BCCode, Description, ");
            SQL.AppendLine("CurCode, Amt, PIC, DeptCode, Remark, FileName, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @VoucherDocNo, @CCtCode, ");
            if (mIsCASUseItemRate)
                SQL.AppendLine("@ItCode, @Qty, @Rate, ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine("@BCCode, @Description, ");
            SQL.AppendLine("@CurCode, @Amt, @PIC, @DeptCode, @Remark, @FileName, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (r + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@CCtCode", Sm.GetGrdStr(Grd1, r, 3));
            if (mIsCASUseItemRate)
            {
                Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, r, 24));
                Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 27));
                Sm.CmParam<Decimal>(ref cm, "@Rate", Sm.GetGrdDec(Grd1, r, 28));
            }
            if (mIsCASUseBudgetCategory)
            {
                Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetGrdStr(Grd1, r, 31));
                Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, r, 33));
            }
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, r, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, r, 6));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetGrdStr(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetGrdStr(Grd1, r, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, r, 12));
            Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd1, r, 14));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }
        
        private MySqlCommand UpdateFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCashAdvanceSettlementDtl Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));

            return cm;
        }

        private MySqlCommand SaveCashAdvanceSettlementDtl2(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCashAdvanceSettlementDtl2(DocNo, DNo, CCtCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @CCtCode, @Amt, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (r + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@CCtCode", Sm.GetGrdStr(Grd2, r, 0));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, r, 2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCashAdvanceSettlementDtl3(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCashAdvanceSettlementDtl3(DocNo, DNo, VoucherDocNo, CurCode, Amt1, Amt2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @VoucherDocNo, @CurCode, @Amt1, @Amt2, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (r + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", Sm.GetGrdStr(Grd3, r, 0));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd3, r, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd3, r, 3));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Sm.GetGrdDec(Grd3, r, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCashAdvanceSettlementDtl4(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCashAdvanceSettlementDtl4(DocNo, DNo, Description, ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine(" TypeCode, ");
            SQL.AppendLine(" Amt, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Description, ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine("@TypeCode, ");
            SQL.AppendLine("@Amt, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (r + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd4, r, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd4, r, 2));
            if (mIsCASUseBudgetCategory)
                Sm.CmParam<String>(ref cm, "@TypeCode", Sm.GetGrdStr(Grd4, r, 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string VoucherRequestDocNo = string.Empty;
            var IsNeedApproval = IsDocNeedApproval();
            string mDocNo = TxtDocNo.Text;
            string mInitProcessInd = Sm.GetValue("Select DocStatus From TblCashAdvanceSettlementHdr Where DocNo = @Param; ", TxtDocNo.Text);
            bool IsCreatedJournal2 = Sm.IsDataExist("Select 1 From TblCashAdvanceSettlementHdr Where DocNo = @Param And JournalDocNo2 Is Not Null; ", TxtDocNo.Text);

            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
            else
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(EditCashAdvanceSettlementHdr());

            if (mIsCASUseDraftDocument)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                        cml.Add(SaveCashAdvanceSettlementDtl(mDocNo, r));

                for (int r = 0; r < Grd2.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd2, r, 0).Length > 0)
                        cml.Add(SaveCashAdvanceSettlementDtl2(mDocNo, r));

                for (int r = 0; r < Grd3.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd3, r, 0).Length > 0)
                        cml.Add(SaveCashAdvanceSettlementDtl3(mDocNo, r));

                for (int r = 0; r < Grd4.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
                        cml.Add(SaveCashAdvanceSettlementDtl4(mDocNo, r));
            }

            if (!ChkCancelInd.Checked)
            {
                //hanya untuk yg pakai draft-final, yang statusnya dari draft jadi final.
                //kalau dia sudah final dari awal, tidak perlu process ini
                if (mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F" && mInitProcessInd != "F")
                {
                    if (!mIsCASUsePartialAmount && Grd4.Rows.Count > 1)
                        cml.Add(SaveVoucherRequest(VoucherRequestDocNo, mDocNo));
                    else
                    {
                        if (IsNeedApproval)
                            cml.Add(SaveDocApproval(mDocNo));
                        else
                        {
                            if (mIsAutoJournalActived)
                                cml.Add(SaveJournal(mDocNo));
                        }
                    }
                }
            }
            else // if cancelled
            {
                if (mCASJournalFormula == "1")
                {
                    //jika param auto journal nya aktif, dan belum pernah dibuat journal pembalik sebelumnya
                    if (mIsAutoJournalActived && !IsCreatedJournal2)
                    {
                        if (!mIsCASUseDraftDocument || (mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F"))
                        {
                            cml.Add(SaveJournal2());
                        }
                    }
                }
                else
                {
                    cml.Add(SaveJournal2());
                    cml.Add(UpdateVoucherRequest());
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                //Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ? 
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) : 
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt))) ||
                (!mIsCASUseDraftDocument && IsDocNotCancelled()) ||
                IsGrdEmpty() ||
                (!ChkCancelInd.Checked && IsGrdValueNotValid()) ||
                (!ChkCancelInd.Checked && IsTotalDetailNotValid()) ||
                (!ChkCancelInd.Checked && IsTotalDetailNotValid2()) ||
                IsDataCancelledAlready() ||
                //IsDocAlreadyProcessToVoucher() ||
                IsDocAlreadyProcessToPropertyCostComponent() ||
                (mIsCASUseDraftDocument && Sm.IsLueEmpty(LueDocStatus, "Document Status"))
                //(mIsCheckCOAJournalNotExists && IsCOAJournalNotValid());
                ;
        }

        private bool IsDocNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblCashAdvanceSettlementHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private bool IsDocumentFinalAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblCashAdvanceSettlementHdr Where DocStatus='F' And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already final."
                );
        }

        private bool IsDocAlreadyProcessToVoucher()
        {
            return Sm.IsDataExist(
                "Select 1 From TblVoucherHdr " +
                "Where CancelInd='N' " +
                "And VoucherRequestDocNo In ( " +
                "   Select VoucherRequestDocNo From TblCashAdvanceSettlementHdr Where DocNo=@Param " +
                ");",
                TxtDocNo.Text,
                "This document already processed to voucher."
                );
        }

        private string GetPropertyInventoryCostComponentCode()
        {
            string PICCCode = string.Empty;
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select CCCode ");
            //SQL.AppendLine("From TblCostCenter ");
            //SQL.AppendLine("Where DeptCode Is Not Null ");
            //SQL.AppendLine("And DeptCode = @Param ");
            //SQL.AppendLine("Limit 1; ");

            SQL.AppendLine(" SELECT DocNo ");
            SQL.AppendLine(" FROM tblpropertyinventorycostcomponentdtl2 ");
            SQL.AppendLine(" WHERE CASDocNo = @Param ");
            SQL.AppendLine("  ");
            SQL.AppendLine(" LIMIT 1; ");
            PICCCode = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);

            return PICCCode;
        }

        private bool IsDocAlreadyProcessToPropertyCostComponent()
        {

            string mPICCCode = GetPropertyInventoryCostComponentCode();

            return Sm.IsDataExist(
                " SELECT 1 FROM tblcashadvancesettlementhdr " +
                " WHERE CancelInd = 'N' " +
                " AND DocNo IN( " +
                " SELECT A.CASDocNo FROM tblpropertyinventorycostcomponentdtl2 A " +
                " JOIN tblpropertyinventorycostcomponenthdr B ON A.DocNo = B.DocNo  " +
                " WHERE A.CASDocNo = @Param AND B.CancelInd='N' AND B.`Status`='A' " +
                " ); ",
                TxtDocNo.Text,
                "The document is already processed to Add. Property Cost Component document " + mPICCCode

                );
        }

        private MySqlCommand EditCashAdvanceSettlementHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCashAdvanceSettlementHdr Set ");
            if (mIsCASUseCompletedInd)
                SQL.AppendLine("    CompletedInd = @CompletedInd, ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, DocStatus=@DocStatus, ");
            SQL.AppendLine("    Amt1=@Amt1, Amt2=@Amt2, Amt3=@Amt3,  LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            if (mIsCASUseDraftDocument)
            {
                SQL.AppendLine("Delete From  TblCashAdvanceSettlementDtl Where Docno = @DocNo; ");
                SQL.AppendLine("Delete From  TblCashAdvanceSettlementDtl2 Where Docno = @DocNo; ");
                SQL.AppendLine("Delete From  TblCashAdvanceSettlementDtl3 Where Docno = @DocNo; ");
                SQL.AppendLine("Delete From  TblCashAdvanceSettlementDtl4 Where Docno = @DocNo; ");
            }

            if (ChkCancelInd.Checked)
            {
                SQL.AppendLine("Update TblVoucherRequestHdr Set ");
                SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@VRDocno And CancelInd='N'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (mIsCASUseCompletedInd)
                Sm.CmParam<String>(ref cm, "@CompletedInd", ChkCompletedInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@VRDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Decimal.Parse(TxtAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt3", Decimal.Parse(TxtAmt3.Text));
            Sm.CmParam<String>(ref cm, "@DocStatus", Sm.GetLue(LueDocStatus));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblCashAdvanceSettlementHdr Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblCashAdvanceSettlementHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine(");");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, SOContractDocNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblCashAdvanceSettlementHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            if (mIsVRForBudgetUseSOContract || mIsVRUseSOContract)
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join TblCashAdvanceSettlementHdr B On A.DocNo = B.JournalDocNo2 ");
                SQL.AppendLine("    And B.DocNo = @DocNo ");
                SQL.AppendLine("Inner Join TblCashAdvanceSettlementDtl C On B.DocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblVoucherRequestHdr D On C.VoucherDocNo = D.VoucherDocNo ");
                SQL.AppendLine("Set A.SOContractDocNo = D.SOContractDocNo ");
                SQL.AppendLine("Where D.SOContractDocNo Is Not Null; ");
            }

            SQL.AppendLine("Update TblJournalDtl A ");
            SQL.AppendLine("Inner Join TblCashAdvanceSettlementHdr B On A.DocNo = B.JournalDocNo2 And B.DocNo = @DocNo ");
            SQL.AppendLine("Inner JOin TblCashAdvanceSettlementDtl B1 On B.DocNo = B1.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherHdr C On B1.VoucherDocNo = C.DocNo And C.Doctype = '61' ");
            SQL.AppendLine("Inner Join TblTravelRequestDtl8 D On C.VoucherRequestDocNo = D.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblTravelRequestHdr E On D.DocNo = E.DocNo ");
            SQL.AppendLine("Set A.SOContractDocNo = E.SOContractDocNo ");
            SQL.AppendLine("Where B.DocNo = @DocNo ");
            SQL.AppendLine("And E.SOContractDocNo Is Not Null; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateVoucherRequest()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblCashAdvanceSettlementHdr B On A.Docno = B.VoucherRequestDocNo ");
            SQL.AppendLine("Set A.CancelInd = 'Y', A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where B.DocNo = @DocNo ");
            SQL.AppendLine("And A.VoucherDocNo Is Null; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowCashAdvanceSettlementHdr(DocNo);
                ShowCashAdvanceSettlementDtl(DocNo);
                ShowCashAdvanceSettlementDtl2(DocNo);
                ShowCashAdvanceSettlementDtl3(DocNo);
                ShowCashAdvanceSettlementDtl4(DocNo);
                Sm.ShowDocApproval(DocNo, "CashAdvanceSettlement", ref Grd5, mIsTransactionUseDetailApprovalInformation);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void CopyData(string DocNo)
        {
            try
            {
                ClearData();
                ShowCashAdvanceSettlementHdr(DocNo);
                TxtDocNo.EditValue = TxtStatus.EditValue = MeeCancelReason.EditValue = TxtPIC.EditValue = TxtDeptCode.EditValue = TxtVoucherRequestDocNo.EditValue = null;
                Sm.SetDteCurrentDate(DteDocDt);
                ChkCancelInd.Checked = false;
                TxtCopyData.EditValue = DocNo;
                Sm.SetLue(LueDocStatus, "F");
                ShowCashAdvanceSettlementDtl(DocNo);
                ShowCashAdvanceSettlementDtl2(DocNo);
                ShowCashAdvanceSettlementDtl3(DocNo);
                ShowCashAdvanceSettlementDtl4(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ShowCashAdvanceSettlementHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("A.CancelReason, A.CancelInd, A.PIC, ifnull(B.UserName, D.Empname) As Username, A.DeptCode, C.DeptName, A.CurCode, A.BankAcCode, A.Remark, A.Amt1, A.Amt2, A.Amt3, A.VoucherRequestDocNo, A.DocStatus, ");

            if (mIsCASUseCompletedInd)
                SQL.AppendLine("A.CompletedInd, ");
            else
                SQL.AppendLine("'N' As CompletedInd, ");

            if (mIsCASUseCostCenter)
                SQL.AppendLine("A.CCCode ");
            else
                SQL.AppendLine("Null As CCCode ");
            SQL.AppendLine("From TblCashAdvanceSettlementHdr A ");
            SQL.AppendLine("Left Join TblUser B On A.PIC=B.UserCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblEmployee D On A.PIC = D.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "StatusDesc", "CancelReason", "CancelInd", "PIC", 

                    //6-10
                    "UserName", "DeptCode", "DeptName", "CurCode", "BankAcCode", 

                    //11-15
                    "Remark", "Amt1", "Amt2", "Amt3", "VoucherRequestDocNo",

                    //16-19
                    "DocStatus", "CCCode", "CompletedInd", "LocalDocNo"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                    mPIC = Sm.DrStr(dr, c[5]);
                    TxtPIC.EditValue = Sm.DrStr(dr, c[6]);
                    mDeptCode = Sm.DrStr(dr, c[7]);
                    TxtDeptCode.EditValue = Sm.DrStr(dr, c[8]);
                    TxtCurCode.EditValue = Sm.DrStr(dr, c[9]);
                    SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[10]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                    TxtAmt1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    TxtAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                    TxtAmt3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                    TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[15]);
                    Sl.SetLueOption(ref LueDocStatus, "CASDocumentStatus");
                    Sm.SetLue(LueDocStatus, Sm.DrStr(dr, c[16]));
                    if (mIsCASUseCostCenter)
                    {
                        Sl.SetLueCCCode(ref LueCCCode, Sm.DrStr(dr, c[17]), mIsFilterByCC ? "Y" : "N");
                        Sm.SetLue(LueCCCode, Sm.DrStr(dr, c[17]));
                    }
                    if (mIsCASUseCompletedInd) ChkCompletedInd.Checked = Sm.DrStr(dr, c[18]) == "Y";
                    TxtLocalDocument.EditValue = Sm.DrStr(dr, c[19]);
                }, true
            );
        }

        private void ShowCashAdvanceSettlementDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.VoucherDocNo, A.CCtCode, Concat(B.CCtName, ' (', IfNull(F.CCName, '-'), ')') As CCtName, ");
            SQL.AppendLine("A.CurCode, A.Amt, A.PIC, D.UserName, A.DeptCode, E.DeptName, A.Remark, G.OptDesc, A.FileName, B.AcNo, I.AcDesc, ");
            SQL.AppendLine("If(C.DocType ='61', ");
            if (mIsCASFromTRProcessAllAmount)
                SQL.AppendLine("C.Amt");
            else
                SQL.AppendLine("(C.Amt- H.DailyAmt) ");
            SQL.AppendLine(", C.Amt) As VoucherAmt, C.DocType VCDocType, ");
            if (mIsVRForBudgetUseSOContract || mIsVRUseSOContract)
                SQL.AppendLine("K.SOContractDocNo, ");
            else
                SQL.AppendLine("Null As SOContractDocNo, ");
            if (mIsCASUseItemRate)
            {
                SQL.AppendLine("A.ItCode, J.ItName, A.Qty, A.Rate, N.UoMName As InventoryUoM, ");
            }
            else
            {
                SQL.AppendLine("Null As ItCode, Null As ItName, 0.00 As Qty, 0.00 As Rate, ");
            }
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine("A.BCCode, M.BCName, A.Description ");
            else
                SQL.AppendLine("Null As BCCode, Null As BCName, null As Description ");
            SQL.AppendLine("From TblCashAdvanceSettlementDtl A ");
            SQL.AppendLine("Left Join TblCostCategory B On A.CCtCode=B.CCtCode ");
            SQL.AppendLine("Inner Join TblVoucherHdr C On A.VoucherDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblUser D On A.PIC=D.UserCode ");
            SQL.AppendLine("Left Join TblDepartment E On A.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblCostCenter F On B.CCCode=F.CCCode ");
            SQL.AppendLine("Left Join TblOption G On C.DocType=G.OptCode And G.OptCat='VoucherDocType' ");
            if (!mIsCASFromTRProcessAllAmount)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.VoucherRequestDocNo As DocNo, SUM(B.Amt2) As DailyAmt  ");
                SQL.AppendLine("    From TblTravelRequestDtl8 A  ");
                SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo  And A.EmpCode = B.PICCode ");
                SQL.AppendLine("    Inner Join TblTravelRequestHdr C On A.DocNo = C.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("    Group By A.VoucherRequestDocNo ");
                SQL.AppendLine(")H On H.DocNo = C.VoucherRequestDocNo ");
            }
            SQL.AppendLine("Left Join TblCOA I On B.AcNo = I.AcNo ");
            if (mIsCASUseItemRate)
            {
                SQL.AppendLine("Left Join TblItem J On A.ItCode = J.ItCode ");
                SQL.AppendLine("Left Join TblUoM N On J.InventoryUoMCode = N.UoMCode ");
            }

            if (mIsVRUseSOContract || mIsVRForBudgetUseSOContract)
                SQL.AppendLine("Left Join TblVoucherRequestHdr K On C.VOucherRequestDocNo = K.DocNo ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine("Left Join TblBudgetCategory M On A.BCCode = M.BCCode ");

            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "VoucherDocNo", 
                        "CCtCode", "CCtName", "CurCode", "Amt", "VoucherAmt", 
                        "PIC", "UserName", "DeptCode", "DeptName", "Remark",
                        "OptDesc", "FileName", "Dno", "VCDocType", "AcNo",
                        "AcDesc", "ItCode", "ItName", "Qty", "Rate",
                        "SOContractDocNo", "BCCode", "BCName", "Description", "InventoryUoM"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                        if (mIsCASUseItemRate)
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 25);
                        }
                        if (mIsVRUseSOContract || mIsVRForBudgetUseSOContract)
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 21);
                        if (mIsCASUseBudgetCategory)
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 24);
                        }
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowCashAdvanceSettlementDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCtCode, Concat(B.CCtName, ' (', IfNull(C.CCName, '-'), ')') As CCtName, A.Amt, B.AcNo, D.AcDesc ");
            SQL.AppendLine("From TblCashAdvanceSettlementDtl2 A ");
            SQL.AppendLine("Left Join TblCostCategory B On A.CCtCode=B.CCtCode ");
            SQL.AppendLine("Left Join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Left Join TblCOA D On B.AcNo = D.AcNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "CCtCode", 
                        "CCtName", "Amt", "AcNo", "AcDesc"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowCashAdvanceSettlementDtl3(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select VoucherDocNo, CurCode, Amt1, Amt2 ");
            SQL.AppendLine("From TblCashAdvanceSettlementDtl3 ");
            SQL.AppendLine("Where DocNo=@DocNo Order By DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] { "VoucherDocNo", "CurCode", "Amt1", "Amt2" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd3, 0, 0);
        }

        private void ShowCashAdvanceSettlementDtl4(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DNo, A.Description, A.Amt ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine(", B.OptDesc Type, A.TypeCode ");
            else
                SQL.AppendLine(", NULL As Type , NULL as TypeCode");
            SQL.AppendLine("From tblcashadvancesettlementdtl4 A ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine("INNER JOIN tbloption B ON A.TypeCode = B.OptCode AND B.OptCat='CASDetailsType' ");
            SQL.AppendLine("Where DocNo=@DocNo Order By DNo ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd4, ref cm, SQL.ToString(),
                    new string[] { "DNo", "Description", "Amt", "Type", "TypeCode" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 4);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #region Additional Method       

        private string GetCCCodeFromDept()
        {
            string CCCode = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode ");
            SQL.AppendLine("From TblCostCenter ");
            SQL.AppendLine("Where DeptCode Is Not Null ");
            SQL.AppendLine("And DeptCode = @Param ");
            SQL.AppendLine("Limit 1; ");

            CCCode = Sm.GetValue(SQL.ToString(), mDeptCode);

            return CCCode;
        }

        internal void GetCCCode()
        {
            if (mIsCASUseCostCenter)
            {
                string CCCode = Sm.GetLue(LueCCCode);
                string DeptCode = string.Empty;

                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (DeptCode.Length > 0) DeptCode += ",";
                    DeptCode += Sm.GetGrdStr(Grd1, i, 10);
                }

                if (DeptCode.Length > 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select Group_Concat(Distinct IfNull(CCCode, '')) CCCode ");
                    SQL.AppendLine("From TblCostCenter ");
                    SQL.AppendLine("Where ActInd = 'Y' ");
                    SQL.AppendLine("And Find_IN_Set(DeptCode, @Param) ");

                    if (Sm.GetValue(SQL.ToString(), DeptCode) != CCCode)
                    {
                        SetLueCCCode(ref LueCCCode);
                    }
                }
            }
        }

        private void SetLueCCCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCCode Col1, A.CCName Col2 ");
            SQL.AppendLine("From TblCostCenter A ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            if (mIsFilterByCC)
            {
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblGroupCostCenter T ");
                SQL.AppendLine("    Where T.CCCode = A.CCCode ");
                SQL.AppendLine("    And GrpCode In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select GrpCode ");
                SQL.AppendLine("        From TblUser ");
                SQL.AppendLine("        Where UserCode = @UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By A.CCName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private string GetProfitCenterCode()
        {
            var Value = Sm.GetLue(LueCCCode);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select ProfitCenterCode From TblCostCenter " +
                    "Where ProfitCenterCode Is Not Null And CCCode=@Param;",
                    Value);
        }

        internal void ClearItem()
        {
            if (!mIsCASUseItemRate) return;

            if (!mIsCASUseItemRateNotBasedOnCBP)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    Grd1.Cells[i, 24].Value = Grd1.Cells[i, 26].Value = null;
                    Grd1.Cells[i, 27].Value = Grd1.Cells[i, 28].Value = 0m;
                    ComputeRateAmount(i);
                }
                ComputeAmt1();
                SetCostCategoryInfo();
                SetVoucherInfo();
                ComputeAmt2();
            }
        }

        internal void CalculateRate(int Row, string ItCode, string Yr, string CCCode)
        {
            decimal Rate = 0m, Qty = 0m, Amt = 0m;

            Qty = Sm.GetGrdDec(Grd1, Row, 27);

            if (!mIsCASUseItemRateNotBasedOnCBP)
                Rate = GetRate(Row, ItCode, Yr, CCCode);

            Amt = Rate * Qty;
            Grd1.Cells[Row, 28].Value = Sm.FormatNum(Rate, 0);
            Grd1.Cells[Row, 6].Value = Sm.FormatNum(Amt, 0);
        }

        private decimal GetRate(int Row, string ItCode, string Yr, string CCCode)
        {
            decimal rate = 0m;
            string data = string.Empty;
            string Mth = Sm.GetDte(DteDocDt).Substring(4, 2);

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Rate" + Mth + " As Rate ");
            SQL.AppendLine("From TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("Inner Join TblCompanyBudgetPlanDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CCCode = @CCCode ");
            SQL.AppendLine("    And A.DocType = '2' ");
            SQL.AppendLine("    And B.ItCode = @ItCode ");
            SQL.AppendLine("Inner Join TblCostCategory C On C.CCtCode = @CCtCode ");
            SQL.AppendLine("    And C.AcNo = B.AcNo ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
                Sm.CmParam<String>(ref cm, "@CCtCode", Sm.GetGrdStr(Grd1, Row, 3));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Rate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        data = Sm.DrStr(dr, c[0]);
                    }
                }

                dr.Close();
            }

            if (data.Length > 0) rate = Decimal.Parse(data);

            return rate;
        }

        private void ComputeRateAmount(int Row)
        {
            decimal Rate = 0m, Qty = 0m, Amt = 0m;

            Qty = Sm.GetGrdDec(Grd1, Row, 27);
            Rate = Sm.GetGrdDec(Grd1, Row, 28);
            Amt = Qty * Rate;

            Grd1.Cells[Row, 6].Value = Sm.FormatNum(Amt, 0);
        }

        private bool IsVoucherCASBA()
        {
            if (!mIsVRBudgetUseCASBA) return false;

            bool mFlag = true;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, i, 20) != mVoucherDocTypeCASBA)
                    {
                        mFlag = false;
                        break;
                    }
                }
            }

            return mFlag;
        }

        private void BtnCopyData_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg6(this));
        }

        private void LueTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            
          Sm.RefreshLookUpEdit(LueTypeCode, new Sm.RefreshLue2(Sl.SetLueOption), "CASDetailsType");
           
        }

        private void LueTypeCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueTypeCode_Leave(object sender, EventArgs e)
        {
            if (LueTypeCode.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LueTypeCode).Length == 0)
                    Grd4.Cells[fCell.RowIndex, 3].Value =
                    Grd4.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueTypeCode);
                    Grd4.Cells[fCell.RowIndex, 4].Value = LueTypeCode.GetColumnValue("Col2");
                }
                LueTypeCode.Visible = false;
            }

        }

        private void GetVCDocType()
        {
            string VCDocNo = string.Empty;
            var l = new List<Voucher>();

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (VCDocNo.Length > 0) VCDocNo += ",";
                    VCDocNo += Sm.GetGrdStr(Grd1, i, 1);
                }
            }

            if (VCDocNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select DocNo, DocType ");
                SQL.AppendLine("From TblVoucherHdr ");
                SQL.AppendLine("Where Find_In_Set(DocNo, @DocNo); ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", VCDocNo);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DocNo","DocType" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new Voucher()
                            {
                                DocNo = Sm.DrStr(dr, c[0]),
                                DocType = Sm.DrStr(dr, c[1])

                            });
                        }
                    }

                    dr.Close();
                }

                if (l.Count > 0)
                {
                    for (int i = 0; i < Grd1.Rows.Count; ++i)
                    {
                        if(Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                        {
                            foreach(var x in l.Where(w => w.DocNo == Sm.GetGrdStr(Grd1, i, 1)))
                            {
                                Grd1.Cells[i, 20].Value = x.DocType;
                            }
                        }
                    }
                }
            }

            l.Clear();
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsClosingJournalBasedOnMultiProfitCenter', 'CASApprovalGroupValidation' ");
            SQL.AppendLine("'CASApprovalGroupValidation', 'IsAutoJournalActived', 'IsVoucherBankAccountFilteredByGrp', 'BankAccountFormat', 'MainCurCode', 'VoucherCodeFormatType', ");
            SQL.AppendLine("'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient', 'FileSizeMaxUploadFTPClient', ");
            SQL.AppendLine("'IsSystemUseCostCenter', 'IsCASUseDraftDocument', 'IsCASAllowToProcessZeroAmount', 'IsCASFromTRProcessAllAmount', 'IsCostCategoryFilteredInCashAdvanceSettlement', ");
            SQL.AppendLine("'CashAdvanceJournalDebitFormat', 'IsVRBudgetUseCASBA', 'VoucherDocTypeCASBA', 'IsCASShowCOACostCategory', 'FormatFTPClient', 'IsCheckCOAJournalNotExists', ");
            SQL.AppendLine("'IsCASUseItemRate', 'IsCASUseCostCenter', 'IsFilterByCC', 'IsCASCCFilterMandatory', 'IsVRForBudgetUseSOContract', 'IsVRUseSOContract', ");
            SQL.AppendLine("'IsCASUseItemRateNotBasedOnCBP', 'IsCASUseCompletedInd', 'IsBankAccountUseCostCenterAndInterOffice', 'IsCostCategoryCASMandatory', 'IsCASUsePartialAmount', ");
            SQL.AppendLine("'BankAccountTypeForCAS', 'CASJournalFormula', 'IsCASAllowMinusAmount', 'CASDlgFilterPICDataSource', 'VoucherCASJournalAcNoSource', ");
            SQL.AppendLine("'IsFilterByDept', 'IsCASUseBudgetCategory', 'CASNotEqualToCostFormula', 'IsTransactionUseDetailApprovalInformation', 'IsCASAllowToCopyData', 'BankAccountTypeForVRCA1', ");
            SQL.AppendLine("'CASCompletedIndFormula');");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsVoucherBankAccountFilteredByGrp": mIsVoucherBankAccountFilteredByGrp = ParValue == "Y"; break;
                            case "IsSystemUseCostCenter": mIsSystemUseCostCenter = ParValue == "Y"; break;
                            case "IsCASUseDraftDocument": mIsCASUseDraftDocument = ParValue == "Y"; break;
                            case "IsCASAllowToProcessZeroAmount": mIsCASAllowToProcessZeroAmount = ParValue == "Y"; break;
                            case "IsCASFromTRProcessAllAmount": mIsCASFromTRProcessAllAmount = ParValue == "Y"; break;
                            case "IsCostCategoryFilteredInCashAdvanceSettlement": mIsCostCategoryFilteredInCashAdvanceSettlement = ParValue == "Y"; break;
                            case "IsVRBudgetUseCASBA": mIsVRBudgetUseCASBA = ParValue == "Y"; break;
                            case "IsCASShowCOACostCategory": mIsCASShowCOACostCategory = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsCASUseItemRate": mIsCASUseItemRate = ParValue == "Y"; break;
                            case "IsCASUseCostCenter": mIsCASUseCostCenter = ParValue == "Y"; break;
                            case "IsFilterByCC": mIsFilterByCC = ParValue == "Y"; break;
                            case "IsCASCCFilterMandatory": mIsCASCCFilterMandatory = ParValue == "Y"; break;
                            case "IsVRForBudgetUseSOContract": mIsVRForBudgetUseSOContract = ParValue == "Y"; break;
                            case "IsVRUseSOContract": mIsVRUseSOContract = ParValue == "Y"; break;
                            case "IsCASUseItemRateNotBasedOnCBP": mIsCASUseItemRateNotBasedOnCBP = ParValue == "Y"; break;
                            case "IsCASUseCompletedInd": mIsCASUseCompletedInd = ParValue == "Y"; break;
                            case "IsBankAccountUseCostCenterAndInterOffice": mIsBankAccountUseCostCenterAndInterOffice = ParValue == "Y"; break;
                            case "IsCostCategoryCASMandatory": mIsCostCategoryCASMandatory = ParValue == "Y"; break;
                            case "IsCASUsePartialAmount": mIsCASUsePartialAmount = ParValue == "Y"; break;
                            case "IsCASAllowMinusAmount": mIsCASAllowMinusAmount = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsCASUseBudgetCategory": mIsCASUseBudgetCategory = ParValue == "Y"; break;
                            case "IsTransactionUseDetailApprovalInformation": mIsTransactionUseDetailApprovalInformation = ParValue == "Y"; break;
                            case "IsCASAllowToCopyData": mIsCASAllowToCopyData = ParValue == "Y"; break;

                            //string
                            case "CASApprovalGroupValidation": mCASApprovalGroupValidation = ParValue; break;
                            case "BankAccountFormat": mBankAccountFormat = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "CashAdvanceJournalDebitFormat": mCashAdvanceJournalDebitFormat = ParValue; break;
                            case "VoucherDocTypeCASBA": mVoucherDocTypeCASBA = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                            case "BankAccountTypeForCAS": mBankAccountTypeForCAS = ParValue; break;
                            case "CASJournalFormula": mCASJournalFormula = ParValue; break;
                            case "CASDlgFilterPICDataSource": mCASDlgFilterPICDataSource = ParValue; break;
                            case "VoucherCASJournalAcNoSource": mVoucherCASJournalAcNoSource = ParValue; break;
                            case "CASNotEqualToCostFormula": mCASNotEqualToCostFormula = ParValue; break;
                            case "BankAccountTypeForVRCA1": mBankAccountTypeForVRCA1 = ParValue; break;
                            case "CASCompletedIndFormula": mCASCompletedIndFormula = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

            if (mCashAdvanceJournalDebitFormat.Length == 0) mCashAdvanceJournalDebitFormat = "1";
            if (mCASJournalFormula.Length == 0) mCASJournalFormula = "1";
            if (mCASDlgFilterPICDataSource.Length == 0) mCASDlgFilterPICDataSource = "1";
            if (mVoucherCASJournalAcNoSource.Length == 0) mVoucherCASJournalAcNoSource = "1";
            if (mCASNotEqualToCostFormula.Length == 0) mCASNotEqualToCostFormula = "1";
            if (mCASCompletedIndFormula.Length == 0) mCASCompletedIndFormula = "1";
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7, 27, 28 });

            ClearGrd2();
            ClearGrd3();

            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 2 });

            Sm.ClearGrd(Grd5, false);
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
        }

        internal void ComputeAmt1()
        {
            decimal Amt = 0m;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 6).Length > 0)
                        Amt += Sm.GetGrdDec(Grd1, r, 6);
            }
            TxtAmt1.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeAmt2()
        {
            decimal Amt = 0m;
            if (Grd3.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd3.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd3, r, 4).Length > 0)
                        Amt += Sm.GetGrdDec(Grd3, r, 4);
            }
            TxtAmt2.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void ComputeAmt3()
        {
            decimal Amt = 0m;
            if (Grd4.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd4.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd4, r, 2).Length > 0)
                        Amt += Sm.GetGrdDec(Grd4, r, 2);
            }
            TxtAmt3.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void SetCostCategoryInfo()
        {
            ClearGrd2();
            var CCtCode = string.Empty;
            var IsExisted = false;
            int row = 0;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                CCtCode = Sm.GetGrdStr(Grd1, r, 3);
                if (CCtCode.Length > 0)
                {
                    IsExisted = false;
                    if (Grd2.Rows.Count > 0)
                    {
                        for (int i = 0; i < Grd2.Rows.Count; i++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd2, i, 0), CCtCode))
                            {
                                Grd2.Cells[i, 2].Value = Sm.GetGrdDec(Grd2, i, 2) + Sm.GetGrdDec(Grd1, r, 6);
                                IsExisted = true;
                                break;
                            }
                        }
                    }
                    if (!IsExisted)
                    {
                        row = Grd2.Rows.Count - 1;
                        Grd2.Cells[row, 0].Value = CCtCode;
                        Grd2.Cells[row, 1].Value = Sm.GetGrdStr(Grd1, r, 4);
                        Grd2.Cells[row, 2].Value = Sm.GetGrdDec(Grd1, r, 6);
                        Grd2.Cells[row, 3].Value = Sm.GetGrdStr(Grd1, r, 21);
                        Grd2.Cells[row, 4].Value = Sm.GetGrdStr(Grd1, r, 22);
                        Grd2.Rows.Add();
                    }
                }
            }
        }

        internal void SetVoucherInfo()
        {
            ClearGrd3();
            var DocNo = string.Empty;
            var IsExisted = false;
            int row = 0;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                DocNo = Sm.GetGrdStr(Grd1, r, 1);
                if (DocNo.Length > 0)
                {
                    IsExisted = false;
                    if (Grd3.Rows.Count > 0)
                    {
                        for (int i = 0; i < Grd3.Rows.Count; i++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd3, i, 0), DocNo))
                            {
                                Grd3.Cells[i, 3].Value = Sm.GetGrdDec(Grd3, i, 3) + Sm.GetGrdDec(Grd1, r, 6);
                                IsExisted = true;
                                break;
                            }
                        }
                    }
                    if (!IsExisted)
                    {
                        row = Grd3.Rows.Count - 1;
                        Grd3.Cells[row, 0].Value = DocNo;
                        Grd3.Cells[row, 2].Value = Sm.GetGrdStr(Grd1, r, 5);
                        Grd3.Cells[row, 3].Value = Sm.GetGrdDec(Grd1, r, 6);
                        Grd3.Cells[row, 4].Value = Sm.GetGrdDec(Grd1, r, 7);
                        Grd3.Rows.Add();
                    }
                }
            }
            ComputeAmt2();
        }

        internal void SetDeptCode()
        {
            if (!mIsCostCategoryFilteredInCashAdvanceSettlement) return;

            mDeptCode = Sm.GetGrdStr(Grd1, 0, 10);
            TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, 0, 11);
        }

        private void SetLueBankAcCode(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BankAcCode As Col1, ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            else if (mBankAccountFormat == "2")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(IfNull(A.BankAcNm, ''), ' : ', A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(' [', B.BankName, ']') Else '' End ");
                SQL.AppendLine("    )) As Col2 ");
            }
            else
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            
            if (BankAcCode.Length != 0)
                SQL.AppendLine("Where A.BankAcCode=@BankAcCode ");
            else
            {
                SQL.AppendLine("Where A.ActInd = 'Y' ");
                if (mIsVoucherBankAccountFilteredByGrp)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            if (BankAcCode.Length == 0)
                SQL.AppendLine("And Find_In_set(A.BankAcTp, @BankAccountTypeForVRCA1) ");

            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);
            Sm.CmParam<String>(ref cm, "@BankAccountTypeForVRCA1", mBankAccountTypeForVRCA1);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        internal void SetLueCCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.CCtCode As Col1, Concat(A.CCtName, ' (', IfNull(B.CCName, '-'), ')') As Col2 ");
            SQL.AppendLine("From TblCostCategory A ");
            SQL.AppendLine("Left Join TblCostCenter B On A.CCCode=B.CCCode ");
            if (mIsSystemUseCostCenter)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=B.CCCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By A.CCtName, B.CCName; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }
        
        private void UploadFile(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;

            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }
            
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            // if (!IsInsert) Grd1.Cells[Row, 14].Value = toUpload.Name;
            if (IsInsert)
            {
                var cml = new List<MySqlCommand>();
                cml.Add(UpdateFile(DocNo, Row, toUpload.Name));
                Sm.ExecCommands(cml);
            }
        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblCashAdvanceSettlementDtl ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }



        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnVR_Click(object sender, EventArgs e)
        {

            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher Request#", false))
            {
                try
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherRequestDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueCCtCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDocStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocStatus, new Sm.RefreshLue2(Sl.SetLueOption), "CASDocumentStatus");
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(SetLueCCCode));
        }
        
        #region Grid

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueBankAcCode, "Bank Account"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg(this));
                }

                if (e.ColIndex == 18 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 1, false, "Voucher# is empty."))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                    {
                        if (mIsCASCCFilterMandatory)
                            Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg4(this, e.RowIndex));
                        else
                            Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg2(this, e.RowIndex));
                    }
                }
                if (mIsCASUseBudgetCategory)
                {
                    if (e.ColIndex == 30 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 1, false, "Voucher# is empty."))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                        {
                            Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg5(this, e.RowIndex));
                        }
                    }
                }
                if (Sm.IsGrdColSelected(new int[] { 0, 4, 6, 12, 18, 33 }, e.ColIndex))
                {

                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7 });
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucher("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 16)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 14), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 14, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 16)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 14), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 14, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueBankAcCode, "Bank Account")) Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg(this));
                if (e.ColIndex == 18 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 1, false, "Voucher# is empty."))
                {
                    if (mIsCASCCFilterMandatory)
                        Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg4(this, e.RowIndex));
                    else
                        Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg2(this, e.RowIndex));
                }

                if (mIsCASUseBudgetCategory)
                {
                    if (e.ColIndex == 30 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 1, false, "Voucher# is empty."))
                        Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg5(this, e.RowIndex));
                }

                if (e.ColIndex == 15)
                {
                    try
                    {
                        OD.InitialDirectory = "c:";
                        OD.Filter = "Pdf files (*.pdf)|*.pdf|Office Files|*.xls;*.xlsx";
                        OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv";
                        OD.FilterIndex = 2;
                        OD.ShowDialog();
                        Grd1.Cells[e.RowIndex, 14].Value = OD.FileName;
                    }
                    catch (Exception Exc)
                    {
                        Sm.ShowErrorMsg(Exc);
                    }
                }
                
                if (mIsCASUseItemRate)
                {
                    if (e.ColIndex == 23 && !Sm.IsDteEmpty(DteDocDt, "Date"))
                    {
                        if (!mIsCASUseItemRateNotBasedOnCBP)
                        {
                            if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 4, false, "Cost Category"))
                                Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg3(this, e.RowIndex, Sm.Left(Sm.GetDte(DteDocDt), 4), Sm.GetGrdStr(Grd1, e.RowIndex, 3)));
                        }
                        else
                        {
                            if (mIsCASUseCostCenter)
                            {
                                if (!Sm.IsLueEmpty(LueCCCode, "Cost Center"))
                                    Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg3(this, e.RowIndex, Sm.Left(Sm.GetDte(DteDocDt), 4), Sm.GetGrdStr(Grd1, e.RowIndex, 3)));
                            }
                            else
                                Sm.FormShowDialog(new FrmCashAdvanceSettlement2Dlg3(this, e.RowIndex, Sm.Left(Sm.GetDte(DteDocDt), 4), Sm.GetGrdStr(Grd1, e.RowIndex, 3)));
                        }
                    }

                    if (e.ColIndex == 25 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 24, false, "Item code is empty."))
                    {
                        var f = new FrmItem("***");
                        f.Tag = "***";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 24);
                        f.ShowDialog();
                    }
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmVoucher("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (mIsCASUseDraftDocument)
            {
                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                int Row = Grd1.SelectedRows[Index].Index;
                                if (Sm.GetGrdStr(Grd1, Row, 19).Length == 0)
                                {
                                    Grd1.Rows.RemoveAt(Row);
                                    if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
                                }
                            }
                        }
                    }
                }
                ComputeAmt1();
                SetCostCategoryInfo();
                SetVoucherInfo();
                ComputeAmt2();
            }
            else
            {
                if (TxtDocNo.Text.Length == 0 && e.KeyCode == Keys.Delete)
                {
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                    ComputeAmt1();
                    SetCostCategoryInfo();
                    SetVoucherInfo();
                    ComputeAmt2();
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 12) Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 12 }, e);
            if (!mIsCASUseItemRate)
            {
                if (e.ColIndex == 6)
                {
                    if(!mIsCASAllowMinusAmount)
                        Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 6 }, e);
                    ComputeAmt1();
                    SetCostCategoryInfo();
                    SetVoucherInfo();
                    ComputeAmt2();
                }
            }
            else
            {
                if (e.ColIndex == 27 || e.ColIndex == 28)
                {
                    ComputeRateAmount(e.RowIndex);
                    if (!mIsCASUseItemRate)
                        Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 6 }, e);
                    ComputeAmt1();
                    SetCostCategoryInfo();
                    SetVoucherInfo();
                    ComputeAmt2();
                }
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 2 });
            }

            if (e.ColIndex == 4)
            {
                LueRequestEdit(Grd4, LueTypeCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);

            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (mIsCASUseDraftDocument)
            {
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
                ComputeAmt3();
            }
            else
            {
                if (TxtDocNo.Text.Length == 0 && e.KeyCode == Keys.Delete)
                {
                    Sm.GrdRemoveRow(Grd4, e, BtnSave);
                    ComputeAmt3();
                }
            }
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 1) Sm.GrdAfterCommitEditTrimString(Grd4, new int[] { 1 }, e);
            if (e.ColIndex == 2)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd4, new int[] { 2 }, e);
                ComputeAmt3();
            }
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                try
                {
                    Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
                    var BankAcCode = Sm.GetLue(LueBankAcCode);
                    if (BankAcCode.Length > 0) TxtCurCode.EditValue = Sm.GetValue("Select CurCode From TblBankAccount Where BankAcCode=@Param;", BankAcCode);
                    TxtAmt1.EditValue = Sm.FormatNum(0m, 0);
                    TxtAmt2.EditValue = Sm.FormatNum(0m, 0);
                    TxtAmt3.EditValue = Sm.FormatNum(0m, 0);
                    ClearGrd();
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
            }
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsCASUseItemRate && BtnSave.Enabled)
            {
                ClearItem();
            }
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #endregion

        #region Report Class

        private class Voucher
        {
            public string DocNo { get; set; }
            public string DocType { get; set; }
        }

        class CASHdr
        {
            public string CompanyLogo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VoucherRequestDocNo { get; set; }
            public string VoucherRequestDocDt { get; set; }

            public decimal VoucherRequestAmt { get; set; }
            public string VoucherDocNo { get; set; }
            public string VoucherDocNo2 { get; set; }

            public string VoucherDocDt { get; set; }
            public decimal VoucherAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
            public decimal DifferenceAmt { get; set; }



            public string CreateDt { get; set; }
            public string Remark { get; set; }
            public string SiteInd { get; set; }
            public string PrintBy { get; set; }
            public string UserNameDibuat { get; set; }

            public string CurName { get; set; }
            public decimal TotalCost { get; set; }
            public decimal TotalCAS { get; set; }
            public decimal TotalDetail { get; set; }
            public string Description { get; set; }

            public string Terbilang { get; set; }
            public string Verifikator1 { get; set; }
            public string SettledBy { get; set; }
            public string PJ { get; set; }
            public string Department { get; set; }

            public string Description2 { get; set; }
            public string UserCodeDibuat { get; set; }
            public string EmpPict5 { get; set; }
            public string Status { get; set; }
            public string Division { get; set; }
            public string LocalDocNo { get; set; }
            public string Bank { get; set; }

            public string FooterImage { get; set; }

        }

        class CASDtl
        {
            public int nomor { get; set; }
            public string CostCenter { get; set; }
            public string CostCategory { get; set; }
            public string AcNo { get; set; }
            public string Desc { get; set; }
            public decimal Amt { get; set; }
            public decimal VoucherAmt { get; set; }
            public string Description { get; set; }
            public string VoucherDocNo { get; set; }
            public string VoucherRequestDocNo { get; set; }
            public string Remark { get; set; }
            public string BudgetCategory { get; set; }
        }

        class CASJournal
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        class CASSignIMS
        {
            public string UserName { get; set; }
            public string UserName2 { get; set; }
            public string UserName3 { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
        }

        private class CASSignAMKA
        {
            public string EmpPict1 { get; set; }
            public string Description1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }
            public string UserCode1 { get; set; }
            public string EmpPict2 { get; set; }
            public string Description2 { get; set; }
            public string UserName2 { get; set; }
            public string ApproveDt2 { get; set; }
            public string UserCode2 { get; set; }
            public string EmpPict3 { get; set; }
            public string Description3 { get; set; }
            public string UserName3 { get; set; }
            public string ApproveDt3 { get; set; }
            public string UserCode3 { get; set; }
            public string EmpPict4 { get; set; }
            public string Description4 { get; set; }
            public string UserName4 { get; set; }
            public string ApproveDt4 { get; set; }
            public string UserCode4 { get; set; }
        }

        private class CASSignSIER
        {
            public string EmpPict1 { get; set; }
            public string Description1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }
            public string UserCode1 { get; set; }
            public string PosName1 { get; set; }

        }
        class CASSignBBT
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }


        #endregion 
    }
}
