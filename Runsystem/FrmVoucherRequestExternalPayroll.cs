﻿#region Update
/*
    03/10/2019 [WED/YK] new apps
    07/10/2019 [WED/YK] nomor voucher melihat parameter VoucherCodeFormatType
    29/01/2020 [WED/YK] tambah otomatis journal
    12/15/2022 [VIN/YK] duplicate entry saat save journal
    25/01/2023 [TYO/MNET] menambah parameter IsDroppingRequestUseType
    13/02/2023 [SET/MNET] menyesuaikan CCCode untuk savejournal
    22/02/2023 [MYA/MNET] Penyesuaian menu Voucher for External Payroll
    27/02/2023 [MYA/MNET] BUG: Bisa Save VR for External payroll tanpa milih Account From
    07/03/2023 [MYA/MNET] BUG: Bisa save VR external payroll tanpa milih account from
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using System.IO;
using System.Drawing.Imaging;
using System.Net;
using System.Threading;
using Renci.SshNet;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestExternalPayroll : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmVoucherRequestExternalPayrollFind FrmFind;
        private string 
            mVoucherDocType = "57", 
            mVoucherCodeFormatType = string.Empty,
            mBankAccountFormat = "0";
        internal bool 
            mIsFilterBySite = false,
            mIsDroppingRequestUseType = false,
            mIsVoucherRequestExternalPayrollNoJournal = false,
            mIsVoucherRequestExternalPayrollHideVendor = false,
            mIsVoucherRequestExternalPayrollUseBankAcc = false;
        private bool 
            mIsAutoJournalActived = false,
            mIsJournalVoucherExternalPayrollUseCC = false;

        #endregion

        #region Constructor

        public FrmVoucherRequestExternalPayroll(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Voucher Request For External Payroll";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();

                Sl.SetLueDeptCode2(ref LueDeptCode, string.Empty);
                Sl.SetLueBankCode(ref LueBankCode);
                SetLuePICCode(ref LuePICCode);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                SetGrd();
                if (!mIsVoucherRequestExternalPayrollUseBankAcc)
                {
                    LblBankAc.Visible = false;
                    LueBankAcCode.Visible = false;
                    LblPIC.Visible = false;
                    LuePICCode.Visible = false;
                    LueDeptCode.Top = label18.Top = LueDeptCode.Top - 21;
                    label8.Top = LueBankCode.Top = label8.Top - 21;
                    label6.Top = TxtAmt.Top = label6.Top - 21;
                    label3.Top = TxtVoucherRequestDocNo.Top = BtnVoucherRequestDocNo.Top = label3.Top - 21;
                    label4.Top = TxtVoucherDocNo.Top = BtnVoucherDocNo.Top = label4.Top - 21;
                    label12.Top = MeeRemark.Top = label12.Top - 21;
                }

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                { 
                    "", 
                    "Item Code", "Item", "Vendor Code", "Vendor", "Amount",
                    "Remark", "D#", "Dropping#", "Journal#", "Cancel Journal#"
                },
                new int[] 
                { 
                    20, 
                    100, 180, 100, 180, 150, 
                    300, 100, 180, 180, 180
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 });
            if(mIsVoucherRequestExternalPayrollNoJournal)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10 });
            if (mIsVoucherRequestExternalPayrollHideVendor)
                Sm.GrdColInvisible(Grd1, new int[] { 4 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, LueDeptCode,
                        LueBankCode, TxtStatus, TxtAmt, LuePICCode, LueBankAcCode,
                        TxtVoucherRequestDocNo, TxtVoucherDocNo, MeeRemark 
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    ChkCancelInd.Checked = false;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LueBankCode, LuePICCode, LueBankAcCode, MeeRemark
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, LueDeptCode,
                LueBankCode, TxtStatus, LuePICCode, LueBankAcCode,
                TxtVoucherRequestDocNo, TxtVoucherDocNo, MeeRemark 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd1();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #region Clear Grid

        private void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherRequestExternalPayrollFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                Sl.SetLueDeptCode2(ref LueDeptCode, string.Empty);
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsLueEmpty(LueBankCode, "Bank Name") &&
                    TxtDocNo.Text.Length == 0 &&
                    e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmVoucherRequestExternalPayrollDlg2(this, Sm.GetLue(LueBankCode)));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsVoucherRequestExternalPayrollHideVendor)
                {
                    Sm.FormShowDialog(new FrmVoucherRequestExternalPayrollDlg2(this, Sm.GetLue(LueBankCode)));
                }
                else
                {
                    if (!Sm.IsLueEmpty(LueBankCode, "Bank Name") &&
                    TxtDocNo.Text.Length == 0 &&
                    e.ColIndex == 0)
                    {
                        Sm.FormShowDialog(new FrmVoucherRequestExternalPayrollDlg2(this, Sm.GetLue(LueBankCode)));
                    }
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                    ComputeAmt();
                }
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
            //Sm.GrdEnter(Grd1, e);
            //Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 5)
            {
                ComputeAmt();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequestExternalPayroll", "TblVoucherRequestExternalPayrollHdr");
            string VoucherRequestDocNo = string.Empty;
            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
            else 
                VoucherRequestDocNo = Sm.GenerateDocNoVoucher(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "C", string.Empty);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveVoucherRequestExternalPayrollHdr(DocNo, VoucherRequestDocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveVoucherRequestExternalPayrollDtl(DocNo, VoucherRequestDocNo, Row));
                    if (mIsAutoJournalActived && !mIsVoucherRequestExternalPayrollNoJournal) cml.Add(SaveJournal(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LueBankCode, "Bank Name") ||
                (mIsVoucherRequestExternalPayrollUseBankAcc && Sm.IsLueEmpty(LueBankAcCode, "Bank Account From")) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsDroppingRequestDuplicated() ||
                IsDroppingRequestIsAlreadyProceed()
                ;
        }

        private bool IsDroppingRequestDuplicated()
        {
            string mDropping1 = string.Empty, mDropping2 = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                mDropping1 = string.Concat(Sm.GetGrdStr(Grd1, i, 8), Sm.GetGrdStr(Grd1, i, 7));
                for (int j = (i + 1); j < Grd1.Rows.Count - 1; j++)
                {
                    mDropping2 = string.Concat(Sm.GetGrdStr(Grd1, j, 8), Sm.GetGrdStr(Grd1, j, 7));

                    if (mDropping1 == mDropping2)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Data duplicated at row#" + (j + 1));
                        Sm.FocusGrd(Grd1, j, 2);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsDroppingRequestIsAlreadyProceed()
        {
            string mDroppingRequest = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (mDroppingRequest.Length > 0) mDroppingRequest += ",";
                    mDroppingRequest += string.Concat(Sm.GetGrdStr(Grd1, i, 8), Sm.GetGrdStr(Grd1, i, 7));
                }
            }

            if (mDroppingRequest.Length > 0)
            {
                var SQL = new StringBuilder();
                var SQL2 = new StringBuilder();

                SQL.AppendLine("Select Concat(A.DroppingRequestDocNo, A.DroppingRequestDNo) ");
                SQL.AppendLine("From TblVoucherRequestExternalPayrollDtl A ");
                SQL.AppendLine("Inner Join TblVoucherRequestExternalPayrollHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And B.CancelInd = 'N' And B.Status In ('O', 'A') ");
                SQL.AppendLine("    And Find_In_Set(Concat(A.DroppingRequestDocNo, A.DroppingRequestDNo), @Param) ");
                SQL.AppendLine("Limit 1; ");

                SQL.AppendLine("Select A.DocNo ");
                SQL.AppendLine("From TblVoucherRequestExternalPayrollDtl A ");
                SQL.AppendLine("Inner Join TblVoucherRequestExternalPayrollHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And B.CancelInd = 'N' And B.Status In ('O', 'A') ");
                SQL.AppendLine("    And Find_In_Set(Concat(A.DroppingRequestDocNo, A.DroppingRequestDNo), @Param) ");
                SQL.AppendLine("Limit 1; ");

                if (Sm.IsDataExist(SQL.ToString(), mDroppingRequest))
                {
                    int mRow = 0;
                    for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                    {
                        if (Sm.GetValue(SQL.ToString(), mDroppingRequest) == string.Concat(Sm.GetGrdStr(Grd1, i, 8), Sm.GetGrdStr(Grd1, i, 7)))
                        {
                            mRow = i;
                            break;
                        }
                    }

                    Sm.StdMsg(mMsgType.Warning, "This data is already proceed to Voucher Request External Payroll#" + Sm.GetValue(SQL2.ToString(), mDroppingRequest));
                    Sm.FocusGrd(Grd1, mRow, 2);
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Amount is zero.")) return true;
            }
            return false;
        }

        private MySqlCommand SaveVoucherRequestExternalPayrollHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestExternalPayrollHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, BankCode, ");
            SQL.AppendLine("Amt, VoucherRequestDocNo, BankAcCode, PICCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', 'O', @DeptCode, @BankCode, ");
            SQL.AppendLine("@Amt, @VoucherRequestDocNo, @BankAcCode, @PICCode, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestHdr( ");
            SQL.AppendLine("DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, BankAcCode, PaymentType, BankCode, PIC, CurCode, Amt, ");
            SQL.AppendLine("ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@VoucherRequestDocNo, @DocDt, 'N', 'O', @DeptCode, @DocType, Null, ");
            SQL.AppendLine("'C', Null, 'B', @BankCode, @CreateBy, 'IDR', @Amt, ");
            SQL.AppendLine("1, @Remark, @CreateBy, CurrentDateTime()); ");

            // approval nya tidak melihat site, karena site nya di dropping request itu bisa banyak banget
            // 1 dokumen VREP punya banyak Dropping Request
            // banyak Dropping Request = banyak PRJI = (bisa jadi) banyak site
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='VoucherRequestExternalPayroll' ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblVoucherRequestExternalPayrollHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And Status = 'O' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And (T.StartAmt = 0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.GrandTotal*1 ");
            SQL.AppendLine("    From TblBOQHdr A ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblVoucherRequestExternalPayrollHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'VoucherRequestExternalPayroll' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @VoucherRequestDocNo ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'VoucherRequestExternalPayroll' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mVoucherDocType);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@PICCode", Sm.GetLue(LuePICCode                                                ));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveVoucherRequestExternalPayrollDtl(string DocNo, string VoucherRequestDocNo, int Row)
        {
            var SQL = new StringBuilder();
            string mDescription = string.Concat("Dropping Request#", 
                    Sm.GetGrdStr(Grd1, Row, 8), // DroppingRequestDocNo
                    "-",
                    Sm.GetGrdStr(Grd1, Row, 7), // DroppingRequestDNo
                    " [",
                    Sm.GetGrdStr(Grd1, Row, 2), // ItemName
                    "]"
                );

            SQL.AppendLine("Insert Into TblVoucherRequestExternalPayrollDtl ");
            SQL.AppendLine("(DocNo, DNo, DroppingRequestDocNo, DroppingRequestDNo, Amt, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @DroppingRequestDocNo, @DroppingRequestDNo, @Amt, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl( ");
            SQL.AppendLine("DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@VoucherRequestDocNo, @DNo, @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@DroppingRequestDNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Description", mDescription);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblVoucherRequestExternalPayrollDtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo = @DNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestExternalPayroll' And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, ");
            if(mIsJournalVoucherExternalPayrollUseCC)
            {
                SQL.AppendLine("CCCode, ");
            }
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, @DocDt, ");
            SQL.AppendLine("Concat('Voucher Request External Payroll : ', A.DocNo, '-', A.DNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, A.Remark, ");
            if(mIsJournalVoucherExternalPayrollUseCC)
            {
                SQL.AppendLine("H.CCCode, ");
            }
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblVoucherRequestExternalPayrollDtl A ");
            if(mIsJournalVoucherExternalPayrollUseCC)
            {
                SQL.AppendLine("LEFT JOIN tbldroppingrequestdtl B ON A.DroppingRequestDocNo = B.DocNo And A.DroppingRequestDNo = B.DNo ");
                SQL.AppendLine("LEFT JOIN tbldroppingrequesthdr C ON B.DocNo = C.DocNo ");
                SQL.AppendLine("LEFT JOIN tblprojectimplementationhdr D ON C.PRJIDocNo = D.DocNo ");
                SQL.AppendLine("LEFT JOIN tblsocontractRevisionhdr E ON D.SOContractDocNo = E.DocNo ");
                SQL.AppendLine("LEFT JOIN tblsocontracthdr F ON E.SOCDocNo = F.DocNo ");
                SQL.AppendLine("LEFT JOIN tblboqhdr G ON F.BOQDocNo = G.DocNo ");
                SQL.AppendLine("LEFT JOIN tbllophdr H ON G.LOPDocNo = H.DocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DNo = @DNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocType From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestExternalPayroll' And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, Null, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.AcNo, Sum(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("        SELECT CONCAT(C.ParValue, F.ProjectCode2) AcNo, A.Amt DAmt, 0.00 CAmt ");
            SQL.AppendLine("        FROM TblVoucherRequestExternalPayrollDtl A ");
            SQL.AppendLine("        INNER JOIN TblDroppingRequestHdr B ON A.DroppingRequestDocNo = B.DocNo ");
            SQL.AppendLine("            AND A.DocNo = @DocNo AND A.DNo = @DNo ");
            SQL.AppendLine("        INNER JOIN TblParameter C ON C.ParCode = 'DebitAcNoForVRExternalPayroll' AND C.ParValue IS NOT NULL ");
            SQL.AppendLine("        INNER JOIN TblProjectImplementationHdr D ON B.PRJIDocNo = D.DocNo ");
            SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr E ON D.SOContractDocNo = E.DocNo ");
            SQL.AppendLine("        INNER JOIN TblSOContractHdr F ON E.SOCDocNo = F.DocNo ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        SELECT CONCAT(C.ParValue, F.ProjectCode2) AcNo, 0.00 DAmt, A.Amt CAmt ");
            SQL.AppendLine("        FROM TblVoucherRequestExternalPayrollDtl A ");
            SQL.AppendLine("        INNER JOIN TblDroppingRequestHdr B ON A.DroppingRequestDocNo = B.DocNo ");
            SQL.AppendLine("            AND A.DocNo = @DocNo AND A.DNo = @DNo ");
            SQL.AppendLine("        INNER JOIN TblParameter C ON C.ParCode = 'CreditAcNoForVRExternalPayroll' AND C.ParValue IS NOT NULL ");
            SQL.AppendLine("        INNER JOIN TblProjectImplementationHdr D ON B.PRJIDocNo = D.DocNo ");
            SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr E ON D.SOContractDocNo = E.DocNo ");
            SQL.AppendLine("        INNER JOIN TblSOContractHdr F ON E.SOCDocNo = F.DocNo ");
            SQL.AppendLine("    ) T Where T.AcNo Is Not Null Group By T.AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), Row+1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelVoucherRequestExternalPayrollHdr());
            if (mIsAutoJournalActived && mIsVoucherRequestExternalPayrollNoJournal)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    cml.Add(SaveJournal2(i));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProceedToVoucher();
        }

        private bool IsDataAlreadyProceedToVoucher()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestExternalPayrollHdr B On A.VoucherRequestDocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("    And B.DocNo = @Param ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already proceed to voucher#" + Sm.GetValue(SQL.ToString(), TxtDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this data.");
                ChkCancelInd.Focus();
                return true;
            }

            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblVoucherRequestExternalPayrollHdr ");
            SQL.AppendLine("Where DocNo= @Param ");
            SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C'); ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelVoucherRequestExternalPayrollHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestExternalPayrollHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestExternalPayrollHdr B On A.DocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("Set A.CancelReason=@CancelReason, A.CancelInd='Y', A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where B.DocNo = @DocNo And B.CancelInd = 'Y'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(int Row)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @JournalDocNo:= ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine(Sm.GetNewJournalDocNo(CurrentDt, 1));
            else
                SQL.AppendLine(Sm.GetNewJournalDocNo(DocDt, 1));
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblVoucherRequestExternalPayrollDtl A Set ");
            SQL.AppendLine("    A.JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DNo = @DNo And A.JournalDocNo2 Is Null ");
            SQL.AppendLine(";");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling Voucher Request External Payroll : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherRequestExternalPayrollHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd = 'Y'; ");            

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, B.DNo, ");
            SQL.AppendLine("B.AcNo, B.CAmt, B.DAMt, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherRequestExternalPayrollDtl A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.JournalDocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo And A.DNo = @DNo ");
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                SetFormControl(mState.View);
                ShowVoucherRequestExternalPayrollHdr(DocNo);
                ShowVoucherRequestExternalPayrollDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVoucherRequestExternalPayrollHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.STATUS When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' ELSE '' END AS StatusDesc, ");
            SQL.AppendLine("A.DeptCode, A.BankCode, A.VoucherRequestDocNo, B.VoucherDocNo, A.Amt, A.BankAcCode, A.PICCode, A.Remark ");
            SQL.AppendLine("FROM TblVoucherRequestExternalPayrollHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "StatusDesc", "DeptCode",  
                    
                    //6-10
                    "BankCode", "VoucherRequestDocNo", "Amt", "Remark", "VoucherDocNo",

                    //11
                    "BankAcCode", "PICCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                    Sl.SetLueDeptCode2(ref LueDeptCode, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[6]));
                    TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[10]);
                    Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[11]));
                    Sm.SetLue(LuePICCode, Sm.DrStr(dr, c[12]));
                }, true
            );
        }

        private void ShowVoucherRequestExternalPayrollDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT E.ItCode, E.ItName, B.VdCode, C.VdName, A.Amt, A.Remark, A.DroppingRequestDNo, A.DroppingRequestDocNo, ");
            SQL.AppendLine("A.JournalDocNo, A.JournalDocNo2 ");
            SQL.AppendLine("FROM TblVoucherRequestExternalPayrollDtl A ");
            SQL.AppendLine("INNER JOIN TblDroppingRequestDtl B ON A.DroppingRequestDocNo = B.DocNo AND A.DroppingRequestDNo = B.DNo ");
            SQL.AppendLine("    AND A.DocNo = @DocNo ");
            if (mIsVoucherRequestExternalPayrollHideVendor)
            {
                SQL.AppendLine("LEFT JOIN TblVendor C ON B.VdCode = C.VdCode ");
            }
            else
            {
                SQL.AppendLine("INNER JOIN TblVendor C ON B.VdCode = C.VdCode ");
            }
            SQL.AppendLine("INNER JOIN TblProjectImplementationRBPHdr D ON B.PRBPDocNo = D.DocNo ");
            SQL.AppendLine("INNER JOIN TblItem E ON D.ResourceItCode = E.ItCode ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "ItCode", 
                    "ItName", "VdCode", "VdName", "Amt", "Remark", 
                    "DroppingRequestDNo", "DroppingRequestDocNo", "JournalDocNo", "JournalDocNo2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        internal string GetSelectedDroppingRequest()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 8) + Sm.GetGrdStr(Grd1, Row, 7) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsDroppingRequestUseType = Sm.GetParameterBoo("IsDroppingRequestUseType");
            mIsJournalVoucherExternalPayrollUseCC = Sm.GetParameterBoo("IsJournalVoucherExternalPayrollUseCC");
            mIsVoucherRequestExternalPayrollHideVendor = Sm.GetParameterBoo("IsVoucherRequestExternalPayrollHideVendor");
            mIsVoucherRequestExternalPayrollNoJournal = Sm.GetParameterBoo("IsVoucherRequestExternalPayrollNoJournal");
            mIsVoucherRequestExternalPayrollUseBankAcc = Sm.GetParameterBoo("IsVoucherRequestExternalPayrollUseBankAcc");
            mBankAccountFormat = Sm.GetParameter("BankAccountFormat");
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                        Amt += Sm.GetGrdDec(Grd1, r, 5);
            }
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void SetLuePICCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select UserCode As Col1, UserName As Col2 ");
                SQL.AppendLine("From TblUser ");
                //if (mIsPICGrouped)
                SQL.AppendLine("where UserCode=@UserCode ");
                SQL.AppendLine("Order By UserName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueBankAcCode(ref LookUpEdit Lue, string BankCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            BankCode = Sm.GetLue(LueBankCode);

            SQL.AppendLine("Select A.BankAcCode As Col1, ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            else if (mBankAccountFormat == "2")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(IfNull(A.BankAcNm, ''), ' : ', A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(' [', B.BankName, ']') Else '' End ");
                SQL.AppendLine("    )) As Col2 ");
            }
            else
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            SQL.AppendLine("Where 0=0 ");
            if (BankCode.Length != 0)
                SQL.AppendLine("And A.BankCode=@BankCode ");
            else
            {
                //if (mIsVoucherRequestBankAccountFilteredByGrp)
                //{
                //    SQL.AppendLine("And Exists( ");
                //    SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                //    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                //    SQL.AppendLine("    And GrpCode In ( ");
                //    SQL.AppendLine("        Select GrpCode From TblUser ");
                //    SQL.AppendLine("        Where UserCode=@UserCode ");
                //    SQL.AppendLine("    ) ");
                //    SQL.AppendLine(") ");
                //}
                SQL.AppendLine("And A.ActInd = 'Y' ");
                //if (mMenuCode != mMenuCodeForVRBudget && ((mCashAdvanceJournalDebitFormat == "3" && Sm.GetLue(LueDocType) == mVoucherDocTypeCashAdvance) || Sm.GetLue(LueDocType) == mVoucherDocTypePettyCash))
                //    SQL.AppendLine("And Find_In_Set(A.BankAcTp, @BankAcTp)");
            }

            SQL.AppendLine("Order By A.Sequence;");


            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankCode", BankCode);
            //Sm.CmParam<String>(ref cm, "@BankAcTp", Sm.GetLue(LueDocType) == mVoucherDocTypeCashAdvance ? mBankAccountTypeForVRCA1 : mBankAccountTypeForVRPCD1);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankCode.Length != 0) Sm.SetLue(Lue, BankCode);
        }

        #endregion

        #endregion

        #region Events

        #region Button Events

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher Request#", false))
            {
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Events

        private void LuePICCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LuePICCode, new Sm.RefreshLue1(SetLuePICCode)); 
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode2), string.Empty);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                ClearGrd1();
            }
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        #endregion

        #endregion

    }
}
