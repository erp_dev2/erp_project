﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPosPayByType : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmPosPayByTypeFind FrmFind;

        #endregion

        #region Constructor

        public FrmPosPayByType(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Pos Pay By Type";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method
        
        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtPayTpNo, TxtPayTpNm, TxtCharge, ChkIsCash, LuePaymentType, LueBankAcCode }
                        , true);
                    TxtPayTpNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPayTpNo, TxtPayTpNm, TxtCharge, ChkIsCash, LuePaymentType, LueBankAcCode }
                        , false);
                    TxtPayTpNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtPayTpNo, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPayTpNm, TxtCharge, ChkIsCash, LuePaymentType, LueBankAcCode }, false);
                    TxtPayTpNm.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtPayTpNo, TxtPayTpNm, LuePaymentType, LueBankAcCode });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtCharge }, 0);
            ChkIsCash.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPosPayByTypeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPayTpNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || Sm.IsTxtEmpty(TxtPayTpNo, string.Empty, false)) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblPosPayByType Where PayTpNo=@PayTpNo;" };
                Sm.CmParam<String>(ref cm, "@PayTpNo", TxtPayTpNo.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblPosPayByType(PayTpNo, PayTpNm, Charge, IsCash, PaymentType, BankAcCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PayTpNo, @PayTpNm, @Charge, @IsCash, @PaymentType, @BankAcCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update PayTpNm=@PayTpNm, Charge=@Charge, IsCash=@IsCash, PaymentType=@PaymentType, BankAcCode=@BankAcCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PayTpNo", TxtPayTpNo.Text);
                Sm.CmParam<String>(ref cm, "@PayTpNm", TxtPayTpNm.Text);
                Sm.CmParam<Decimal>(ref cm, "@Charge", Decimal.Parse(TxtCharge.Text));
                Sm.CmParam<String>(ref cm, "@IsCash", ChkIsCash.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
                Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPayTpNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PayTpNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PayTpNo", PayTpNo);
                Sm.ShowDataInCtrl(
                        ref cm,

                        "SELECT * FROM tblpospaybytype " +
                        "WHERE PayTpNo=@PayTpNo;",

                        new string[]
                        {
                            //0
                            "PayTpNo",
                            
                            //1-5
                            "PayTpNm", "Charge", "IsCash", "PaymentType", "BankAcCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPayTpNo.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPayTpNm.EditValue = Sm.DrStr(dr, c[1]);
                            TxtCharge.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                            ChkIsCash.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                            Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[5]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPayTpNo, "Type number", false) ||
                Sm.IsTxtEmpty(TxtPayTpNm, "Type name", false) ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Bank account") ||
                IsPayTpNoExisted();
        }

        private bool IsPayTpNoExisted()
        {
            if (!TxtPayTpNo.Properties.ReadOnly)
            {
                var cm = new MySqlCommand();
                cm.CommandText = "Select PayTpNo From TblPosPayByType Where PayTpNo=@PayTpNo;";
                Sm.CmParam<String>(ref cm, "@PayTpNo", TxtPayTpNo.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Type number ( " + TxtPayTpNo.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPayTpNo_Validated(object sender, EventArgs e)
            {
                Sm.TxtTrim(TxtPayTpNo);
            }

            private void TxtPayTpNm_Validated(object sender, EventArgs e)
            {
                Sm.TxtTrim(TxtPayTpNm);
            }

            private void TxtCharge_Validated(object sender, EventArgs e)
            {
                Sm.FormatNumTxt(TxtCharge, 0);
            }

            private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
            {
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
            }

            private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
            {
                Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
            }

            #endregion

        #endregion

    }
}
