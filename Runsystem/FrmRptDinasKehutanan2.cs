﻿#region Update
/*
    23/12/2017 [TKG] tambah local/export
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDinasKehutanan2 : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptDinasKehutanan2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

            #region Standard Methods

            override protected void FrmLoad(object sender, EventArgs e)
            {
                try
                {
                    mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                    Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                    Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                    SetLueType(ref LueType);
                    SetGrd();
                    base.FrmLoad(sender, e);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }

            private string GetSQL(string Filter)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT X.Name, X.LocalDocno, X.DocDt, X.Sortimen, SUM(IFNULL(X.Qty, 0)) AS Qty, ");
                SQL.AppendLine("SUM(IFNULL(X.QtyInventory, 0)) QtyInventory, X.CtName, X.Destination, X.LocalExport ");
                SQL.AppendLine("FROM( ");
                SQL.AppendLine("     SELECT A.DocNo, 'NOTA ANGKUTAN' AS Name, A.LocalDocNo, A.DocDt, A.Sortimen As sortimen2, ");
                SQL.AppendLine("     IFNULL(D.Qty2, E.Qty2) AS Qty, ");
                SQL.AppendLine("     IFNULL(D.QtyInventory, E.QtyInventory) AS QtyInventory, ");
                SQL.AppendLine("     C.CtName, B.Destination, Ifnull(D.ItGrpName, E.ItGrpName) As Sortimen, ");
                SQL.AppendLine("     Case When B.PLDocNo Is Null Then 'Local' Else 'Export' End As LocalExport ");
                SQL.AppendLine("     FROM TblFako A ");
                SQL.AppendLine("     INNER JOIN TblDko B ON A.DKODocNo = B.Docno ");
                SQL.AppendLine("     INNER JOIN TblCustomer C ON B.CtCode = C.CtCode ");
                SQL.AppendLine("     LEFT JOIN ");
                SQL.AppendLine("     ( ");
                SQL.AppendLine("          SELECT A.DocNo,  A2.SectionNo, ");
                SQL.AppendLine("          ROUND(A2.QtyInventory, 4) As QtyInventory, J.ITGrpName, ");
                SQL.AppendLine("          ROUND(if(I.Qty =0, 0, if(G.PriceUomCode = B.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))), 0) As Qty2 ");
                SQL.AppendLine("          FROM TblPlhdr A ");
                SQL.AppendLine("          INNER JOIN TblPlDtl A2 On A.DocNo = A2.DocNo ");
                SQL.AppendLine("          INNER JOIN TblItem B On A2.ItCode = B.ItCode ");
                SQL.AppendLine("          INNER JOIN TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                SQL.AppendLine("          INNER JOIN TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo ");
                SQL.AppendLine("          INNER JOIN TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                SQL.AppendLine("          INNER JOIN TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo ");
                SQL.AppendLine("          INNER JOIN TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
                SQL.AppendLine("          LEFT JOIN TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And A2.ItCode = I.ItCode ");
                SQL.AppendLine("          LEFT JOIN TblItemGroup J On B.ItGrpCode = J.ItGrpCode ");
                SQL.AppendLine("     )D On B.PlDocno = D.DocNo And B.SectionNo = D.SectionNo ");
                SQL.AppendLine("     LEFT JOIN ");
                SQL.AppendLine("     ( ");
                SQL.AppendLine("          SELECT A.DocNo, ");
                SQL.AppendLine("          ROUND(A2.Qty, 0) As Qty, ROUND(A2.QtyInventory, 4) As QtyInventory, J.ITGrpName, ");
                SQL.AppendLine("          ROUND(if(I.Qty =0, 0, if(G.PriceUomCode = F.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))), 0) As Qty2 ");
                SQL.AppendLine("          FROM TblDRhdr A ");
                SQL.AppendLine("          INNER JOIN TblDRDtl A2 On A.DocNo = A2.DocNo ");
                SQL.AppendLine("          INNER JOIN TblSOHdr B On A2.SODocNo=B.DocNo ");
                SQL.AppendLine("          INNER JOIN TblSODtl C On A2.SODocNo=C.DocNo And A2.SODNo=C.DNo ");
                SQL.AppendLine("          INNER JOIN TblCtQtDtl D On B.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
                SQL.AppendLine("          INNER JOIN TblItemPriceDtl E On D.ItemPriceDocNo=E.DocNo And D.ItemPriceDNo=E.DNo ");
                SQL.AppendLine("          INNER JOIN TblItem F On E.ItCode=F.ItCode ");
                SQL.AppendLine("          INNER JOIN TblItemPriceHdr G On D.ItemPriceDocNo = G.DocNo ");
                SQL.AppendLine("          LEFT JOIN TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And E.ItCode = I.ItCode");
                SQL.AppendLine("          LEFT JOIN TblItemGroup J On F.ItGrpCode = J.ItGrpCode ");
                SQL.AppendLine("          Where A.CancelInd='N' ");
                SQL.AppendLine("     )E On B.DrDocNo = E.DocNo");
                SQL.AppendLine("     Where A.CancelInd='N' ");
                SQL.AppendLine("     And A.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine(")X ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("GROUP BY X.Name, X.LocalDocno, X.DocDt, X.Sortimen, X.CtName, X.Destination, X.LocalExport ");
                SQL.AppendLine("ORDER BY X.DocDt, X.LocalDocNo; ");
                
                return SQL.ToString();
            }

            private void SetGrd()
            {
                Grd1.Cols.Count = 10;
                Grd1.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                        {
                            //0
                            "No.",

                            //1-5
                            "Dokumen"+Environment.NewLine+"Eksport",
                            "Nomor Dokumen"+Environment.NewLine+"Ekspor", 
                            "Tanggal"+Environment.NewLine+"Stuffing",
                            "Jenis"+Environment.NewLine+"Hasil Hutan",
                            "Jumlah",

                            //6-9
                            "Vol/Berat",
                            "Local/Export",
                            "Tujuan"+Environment.NewLine+"Pengangkutan",
                            "Negara Tujuan"
                        },
                        new int[] 
                        {
                            //0
                            50,

                            //1-5
                            120, 150, 100, 150, 80, 
                            
                            //6-9
                            100, 150, 280, 200
                        }
                    );
                Sm.GrdFormatDate(Grd1, new int[] { 3 });
                Sm.GrdFormatDec(Grd1, new int[] { 5, 6 }, 0);
                Sm.SetGrdProperty(Grd1, false);
            }

            override protected void ShowData()
            {
                Sm.ClearGrd(Grd1, false);
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                    ) return;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;

                    var cm = new MySqlCommand();
                    var Filter = string.Empty;

                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                    
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "LocalExport", true);

                    Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                            { 
                                //0
                                "Name",  

                                //1-5
                                "LocalDocNo", "DocDt", "Sortimen", "Qty", "QtyInventory",  

                                //6-8
                                "LocalExport", "CtName", "Destination"
                            },

                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            }, true, false, false, false
                        );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6 });
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }

            }

            #endregion

            #region Grid Method

            override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
            {
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6 });
                Sm.SetGridNo(Grd1, 0, 1, true);
            }

            override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
            {
            }

            #endregion

            #region Misc Control Method

            override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
            {
                if (Sm.GetLue(LueFontSize).Length != 0)
                {
                    Grd1.Font = new Font(
                        Grd1.Font.FontFamily.Name.ToString(),
                        int.Parse(Sm.GetLue(LueFontSize))
                        );
                }
            }

            #endregion

            private void SetLueType(ref LookUpEdit Lue)
            {
                Sm.SetLue2(
                 ref Lue,
                 "Select 'Local' As Col1, 'Local' As Col2 Union All Select 'Export' As Col1, 'Export' As Col2;",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        
        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        #endregion

        #endregion
    }
}
