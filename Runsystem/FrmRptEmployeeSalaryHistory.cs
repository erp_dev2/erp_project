﻿#region Update
/* 
    03/09/2018 [TKG] New reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmployeeSalaryHistory : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsNotFilterByAuthorization = false;

        #endregion

        #region Constructor

        public FrmRptEmployeeSalaryHistory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                GetParameter();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Code",
                    "Employee's Name",
                    "Old Code",
                    "Position",
                    "Department",

                    //6-10
                    "Site",
                    "Join",
                    "Resign",
                    "Monthly"+Environment.NewLine+"Previous Salary",
                    "Daily"+Environment.NewLine+"Previous Salary",

                    //11-13
                    "Previous Salary's"+Environment.NewLine+"End Date",
                    "Monthly"+Environment.NewLine+"New Salary",
                    "Daily"+Environment.NewLine+"New Salary",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 200, 100, 180, 200, 

                    //6-10
                    200, 80, 80, 130, 130, 
                    
                    //11-13
                    100, 130, 130
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 12, 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7, 8 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            var l = new List<Result>();
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Process3(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<Result> l)
        {
            string Filter = string.Empty;
            var cm = new MySqlCommand();
            
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@CurrentDt", Sm.ServerCurrentDateTime());
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "B.EmpCodeOld", "B.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.SiteName, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, F.StartDt, A.Amt, A.Amt2 ");
            SQL.AppendLine("From TblEmployeeSalary A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("    And (B.ResignDt Is Null Or (B.ResignDt Is Not Null And B.ResignDt>=@CurrentDt)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And B.GrdLvlCode In ( ");
                SQL.AppendLine("    Select T2.GrdLvlCode ");
                SQL.AppendLine("    From TblPPAHdr T1 ");
                SQL.AppendLine("    Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode=E.SiteCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select EmpCode, Max(StartDt) StartDt ");
            SQL.AppendLine("    From TblEmployeeSalary ");
            SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode And A.StartDt=F.StartDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(" Order By E.SiteName, D.DeptName, B.EmpName, A.EmpCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName",

                    //6-10
                    "JoinDt", "ResignDt", "StartDt", "Amt", "Amt2"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            EmpCodeOld = Sm.DrStr(dr, c[2]),
                            PosName = Sm.DrStr(dr, c[3]),
                            DeptName = Sm.DrStr(dr, c[4]),
                            SiteName = Sm.DrStr(dr, c[5]),
                            JoinDt = Sm.DrStr(dr, c[6]),
                            ResignDt = Sm.DrStr(dr, c[7]),
                            StartDt = Sm.DrStr(dr, c[8]),
                            Amt = Sm.DrDec(dr, c[9]),
                            Amt2 = Sm.DrDec(dr, c[10]),
                            AmtOld = 0m,
                            Amt2Old = 0m,
                            
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            string Filter = string.Empty, Filter2 = string.Empty, EmpCode = string.Empty, StartDt = string.Empty;
            decimal Amt = 0m, Amt2 = 0m;

            for (int i = 0; i < l.Count; i++)
            {
                EmpCode = l[i].EmpCode;
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode); 
            }

            Filter = " Where ( " + Filter + ") ";

            for (int i = 0; i < l.Count; i++)
            {
                EmpCode = l[i].EmpCode;
                StartDt = l[i].StartDt;
                if (Filter2.Length > 0) Filter2 += " Or ";
                Filter2 += " (EmpCode=@EmpCode" + i.ToString() + " And StartDt=@StartDt" + i.ToString() + " ) ";
                Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), EmpCode);
                Sm.CmParamDt(ref cm, "@StartDt" + i.ToString(), StartDt);
            }

            Filter2 = " And Not ( " + Filter2 + ") ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.Amt, A.Amt2 ");
            SQL.AppendLine("From TblEmployeeSalary A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select EmpCode, Max(StartDt) StartDt ");
            SQL.AppendLine("    From TblEmployeeSalary ");
            SQL.AppendLine(Filter.Replace("A.", string.Empty));
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(") B On A.EmpCode=B.EmpCode And A.StartDt=B.StartDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(" Order By A.EmpCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "EmpCode", "Amt", "Amt2" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        Amt = Sm.DrDec(dr, c[1]);
                        Amt2 = Sm.DrDec(dr, c[2]);

                        foreach (var i in l.Where(w => Sm.CompareStr(w.EmpCode, EmpCode)))
                        {
                            i.AmtOld = Amt;
                            i.Amt2Old = Amt2;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Result> l)
        {
            iGRow r;

            Grd1.BeginUpdate();
            
            var Dt = new DateTime(
                   Int32.Parse(l[0].StartDt.Substring(0, 4)),
                   Int32.Parse(l[0].StartDt.Substring(4, 2)),
                   Int32.Parse(l[0].StartDt.Substring(6, 2)),
                   0, 0, 0
                   );

            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = l[i].EmpCode;
                r.Cells[2].Value = l[i].EmpName;
                r.Cells[3].Value = l[i].EmpCodeOld;
                r.Cells[4].Value = l[i].PosName;
                r.Cells[5].Value = l[i].DeptName;
                r.Cells[6].Value = l[i].SiteName;
                SetDt(r, 7, l[i].JoinDt);
                SetDt(r, 8, l[i].ResignDt);
                r.Cells[9].Value = l[i].AmtOld;
                r.Cells[10].Value = l[i].Amt2Old;

                if (l[i].AmtOld != 0m && l[i].Amt2Old != 0m)
                {
                    Dt = new DateTime(
                       Int32.Parse(l[i].StartDt.Substring(0, 4)),
                       Int32.Parse(l[i].StartDt.Substring(4, 2)),
                       Int32.Parse(l[i].StartDt.Substring(6, 2)),
                       0, 0, 0
                       );
                    Dt = Dt.AddDays(1);
                    SetDt(r, 11,
                        Dt.Year.ToString() +
                        ("00" + Dt.Month.ToString()).Substring(("00" + Dt.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt.Day.ToString()).Substring(("00" + Dt.Day.ToString()).Length - 2, 2));
                }
                r.Cells[12].Value = l[i].Amt;
                r.Cells[13].Value = l[i].Amt2;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10, 12, 13 });
            Grd1.EndUpdate();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetDt(iGRow r, int c, string Dt)
        {
            if (Dt.Length == 0)
                r.Cells[c].Value = string.Empty;
            else
                r.Cells[c].Value = Sm.ConvertDate(Dt);
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
            public decimal AmtOld { get; set; }
            public decimal Amt2Old { get; set; }
        }

        #endregion
    }
}
