﻿#region Update
/*
    17/11/2017 [TKG] nama pengirim diisi vendor-nya.
    03/12/2017 [TKG] kota, kecamatan dan desa diabil dari informasi vendor.
    22/01/2018 [HAR] Bug Fixing Reporting Penerimaaan Bahan Baku tidak sama nilainya dengan reporting cost raw material. Inner Join diganti Left Join
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptBRIK : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool IsReCompute = false;

        #endregion

        #region Constructor

        public FrmRptBRIK(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetLueDocType(ref LueDocType);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Y1.DLCOde, Y1.NoSeri, Y1.QueueNo, Y1.DateTerbit, Y1.DateVcr, Y1.Sumber, Y1.DocTypeDesc, Y1.Jenis,  ");
            SQL.AppendLine("Y1.Jumlah, Y1.Volume, Y1.Village, Y1.SubDistrict, ");
            SQL.AppendLine("Y1.City, Y1.Vendor, Y1.PemilikLegalitas, Y1.TTName, Y1.LicenceNo, Y1.Sortimen, Y1.DtBongkar, Y1.PaymentType, Y1.BankAcDesc ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select L.DlCode, L.TTName, L.LicenceNo, L.Noseri, L.TglTerbit As DateTerbit, M.DocDt As DateVcr, t1.QueueNo, 'HR' As Sumber, T1.DocType, ");
            SQL.AppendLine("    Case T1.DocType when '1' then 'Log' When '2' Then 'Balok' Else Null End As DocTypeDesc, ");
            SQL.AppendLine("    Case T1.DocType when '1' then I.CityName when '2' then Q.CityName else null End As City, ");
            SQL.AppendLine("    Case T1.DocType when '1' then J.SDName when '2' then R.SDName else null End As SubDistrict, ");
            SQL.AppendLine("    Case T1.DocType when '1' then K.VilName when '2' then S.VilName else null End As Village, ");
            //SQL.AppendLine("    E.CityName As City, F.SDName As SubDistrict, G.VilName As Village, ");
            SQL.AppendLine("    D.VdName As Vendor, "); //Case T1.DocType when '1' then D.VdName when '2' then H.VdName else null End As Vendor, ");
            SQL.AppendLine("    T1.ItemInformation As Jenis, ");
            SQL.AppendLine("    t1.qty1 as jumlah, t1.Qty2 As volume, ");
            SQL.AppendLine("    Case T1.DocType when '1' Then 'KB' When '2' then 'KG' else null End As Sortimen, ");
            SQL.AppendLine("    Case T1.DocType ");
            SQL.AppendLine("    When '1' Then H.VdName ");
            SQL.AppendLine("    When '2' Then P.VdName End ");
            SQL.AppendLine("    As PemilikLegalitas, ");
            SQL.AppendLine("    T1.DtBongkar, T1.PaymentType, ");
            SQL.AppendLine("    Trim(Concat( ");
            SQL.AppendLine("    Case When N.BankAcNo Is Not Null  ");
            SQL.AppendLine("        Then Concat(N.BankAcNo, ' [', IfNull(N.BankAcNm, ''), ']') ");
            SQL.AppendLine("        Else IfNull(N.BankAcNm, '') End, ");
            SQL.AppendLine("    Case When O.BankName Is Not Null Then Concat(' ', O.BankName) Else '' End ");
            SQL.AppendLine("    )) As BankAcDesc ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select D.QueueNo, A.DocType, F.Qty1, ");
            SQL.AppendLine("        G.OptDesc As PaymentType, E.DocNo, H.ItemInformation, C.DocDt As DtBongkar, ");
            SQL.AppendLine("        Sum(B.Qty) As Qty2 ");
            SQL.AppendLine("        From tblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("        Inner Join tblPurchaseInvoiceRawMaterialDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join tblRawMaterialVerify C On A.RawMaterialVerifyDocNo= C.DocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("        Inner Join TblLegalDocVerifyHdr D On C.LegaldocVerifyDocNo = D.DocNo And D.CancelInd = 'N' ");
            SQL.AppendLine("        Inner Join TblVoucherHdr E On A.VoucherRequestDocNo=E.VoucherRequestDocNo And E.CancelInd = 'N' And E.DocType='11' And E.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Inner Join ( ");
	        SQL.AppendLine("            Select G1.LegaldocVerifyDocNo, G1.DocType, Sum(G2.Qty) Qty1 ");
	        SQL.AppendLine("            From TblRecvRawMaterialHdr G1 ");
	        SQL.AppendLine("            Inner Join TblRecvrawMaterialDtl2 G2 On G1.DocNo = G2.DocNo ");
	        SQL.AppendLine("            Where G1.ProcessInd='F' ");
	        SQL.AppendLine("            Group By G1.LegaldocVerifyDocNo, G1.DocType ");
	        SQL.AppendLine("        ) F On D.DocNo=F.LegaldocVerifyDocNo And A.DocType=F.DocType ");
            SQL.AppendLine("        Left Join TblOption G On A.PaymentType=G.OptCode And G.OptCat='VoucherPaymentType' ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select T1.DocNo, ");
            SQL.AppendLine("            Group_Concat(Distinct T5.OptDesc Order By T5.OptDesc Asc Separator ',') As ItemInformation ");
            SQL.AppendLine("            From TblVoucherHdr T1 ");
            SQL.AppendLine("            Inner Join TblPurchaseInvoiceRawMaterialHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
            SQL.AppendLine("            Inner Join TblPurchaseInvoiceRawMaterialDtl T3 On T2.DocNo=T3.DocNo ");
            SQL.AppendLine("            Inner Join TblItem T4 On T3.ItCode=T4.ItCode ");
            SQL.AppendLine("            Inner Join TblOption T5 On T4.Information1=T5.OptCode And T5.OptCat='ItemInformation1' ");
            SQL.AppendLine("            Where T1.DocType='11' ");
            SQL.AppendLine("            And T1.CancelInd = 'N' ");
            SQL.AppendLine("            And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            Group By T1.DocNo ");
            SQL.AppendLine("        ) H On E.DocNo=H.DocNo ");
            SQL.AppendLine("        Group By D.QueueNo, A.DocType, F.Qty1, G.OptDesc, E.DocNo, H.ItemInformation, C.DocDt ");
            SQL.AppendLine("    ) t1 ");
            SQL.AppendLine("    Left Join TblLegalDocVerifyHdr C On t1.QueueNo = C.QueueNo And C.cancelInd = 'N' ");
            SQL.AppendLine("    Left Join TblVendor D On C.VdCode = D.VdCode "); //And t1.doctype='1' ");
            SQL.AppendLine("    Left Join TblCity E On D.CityCode = E.CityCode ");
            SQL.AppendLine("    Left Join TblSubDistrict F On D.SdCode = F.SdCOde ");
            SQL.AppendLine("    Left Join TblVillage G On D.VilCode = G.VilCOde ");
            SQL.AppendLine("    Left Join TblVendor H On C.VdCode1 = H.VdCode ");
            SQL.AppendLine("    Left Join TblVendor P On C.VdCode2 = P.VdCode ");
            
            SQL.AppendLine("    Left Join TblCity I On H.CityCode = I.CityCode ");
            SQL.AppendLine("    Left Join TblSubDistrict J On H.SdCode = J.SdCOde ");
            SQL.AppendLine("    Left Join TblVillage K On H.VilCode = K.VilCOde ");
            
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select A.DocNo, A.DlType, C.TTCode,  D.TTName, C.LicenceNo, group_concat(case A.DlCode ");
            SQL.AppendLine("        when 'NA' then 'Nota Angkutan' ");
		    SQL.AppendLine("        When 'FAKO' then 'FAKO' ");
            SQL.AppendLine("        When 'SKAU' then 'SKAU' ");
            SQL.AppendLine("        When 'NAB' then 'Nota Angkutan Balok' ");
            SQL.AppendLine("        Else null End Separator '/ ') As DlCode, ");
		    SQL.AppendLine("        group_concat(case A.DlCode ");
            SQL.AppendLine("        when 'NA' then B.QueueNo ");
		    SQL.AppendLine("        When 'FAKO' then A.DlDocNo ");
            SQL.AppendLine("        When 'SKAU' then A.DlDOcNo ");
            SQL.AppendLine("        When 'NAB' then A.DlDOcNo ");
            SQL.AppendLine("        Else null End Separator '/ ') As NoSeri, ");
            SQL.AppendLine("        group_concat(case A.DlCode ");
            SQL.AppendLine("        when 'NA' Or 'NAB' then Date_Format(Str_To_Date(Concat(Substring(C.CreateDt, 7, 2), Substring(C.CreateDt, 5, 2), Left(C.CreateDt, 4)),'%d%m%Y'),'%d/%b/%Y') ");
            SQL.AppendLine("        When 'FAKO' Or 'SKAU' then Date_Format(Str_To_Date(Concat(Substring(B.CreateDt, 7, 2), Substring(B.CreateDt, 5, 2), Left(B.CreateDt, 4)),'%d%m%Y'),'%d/%b/%Y') ");
            SQL.AppendLine("        Else null End Separator ', ') As TglTerbit ");
		    SQL.AppendLine("        From TblLegalDocVerifyDtl A ");
		    SQL.AppendLine("        Inner Join TblLegalDocverifyHdr B On A.DocNo = B.DocNo And B.CancelInd = 'N' ");
		    SQL.AppendLine("        Inner Join TblLoadingQueue C On B.QueueNo = C.DOcNo ");
            SQL.AppendLine("        Inner Join TblTransportType D On C.TTCode = D.TTCode ");
            SQL.AppendLine("        Where A.DLCode = 'NA' Or A.DlCode = 'FAKO' Or A.DlCode = 'SKAU' Or A.DlCode = 'NAB' ");
            SQL.AppendLine("        Group By A.DocNo, A.DlType, C.TTCode,  D.TTName, C.LicenceNo ");
            SQL.AppendLine("    ) L On C.DOcNo = L.DocNo And T1.DocType = L.DlType ");
            SQL.AppendLine("    Inner Join TblVoucherHdr M On T1.DocNo=M.DocNo ");
            SQL.AppendLine("    Left Join TblBankAccount N On M.BankAcCode=N.BankAcCode ");
            SQL.AppendLine("    Left Join TblBank O On N.BankCode=O.BankCode ");

            SQL.AppendLine("    Left Join TblCity Q On P.CityCode = Q.CityCode ");
            SQL.AppendLine("    Left Join TblSubDistrict R On P.SdCode = R.SdCOde ");
            SQL.AppendLine("    Left Join TblVillage S On P.VilCode = S.VilCOde ");
            

            SQL.AppendLine(")Y1 ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Jenis",
                        "No.Seri",
                        "No.Antrian",
                        "Terbit",
                        "Terima",
                        
                        //6-10
                        "Sumber",
                        "Log/Balok",
                        "Jenis"+Environment.NewLine+"Kayu",
                        "Jumlah"+Environment.NewLine+"(batang)",
                        "Volume"+Environment.NewLine+"(kubik)",

                        //11-15
                        "Desa",
                        "Kecamatan",
                        "Kota",
                        "Pengirim",
                        "Pemilik",
                        
                        //16-20
                        "Alat"+Environment.NewLine+"Angkut",
                        "Nomor Polisi",
                        "Sortimen",
                        "Bongkar",
                        "Pembayaran",
                        
                        //21
                        "Rekening Bank"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 100, 100, 100,   
                        
                        //6-10
                        80, 100, 100, 150, 150, 

                        //11-15
                        100, 100, 100, 150, 150, 

                        //16-20
                        100, 100, 120, 80, 150, 
                        
                        //21
                        400
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5, 19 });
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "Y1.DocType", true);
                Sm.FilterStr(ref Filter, ref cm, TxtQueueNo.Text, new string[] { "Y1.QueueNo"});
                Sm.FilterStr(ref Filter, ref cm, TxtVdCode.Text, new string[] { "Y1.PemilikLegalitas" });

                IsReCompute = false;
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By Y1.DateTerbit;",
                        new string[]
                        { 
                            //0
                            "DlCode", 

                            //1-5
                            "NoSeri", "QueueNo", "DateTerbit", "DateVcr", "Sumber",    
                            
                            //6-10
                            "DocTypeDesc", "Jenis", "Jumlah", "Volume", "Village", 
                            
                            //11-15
                            "SubDistrict", "City", "Vendor", "PemilikLegalitas", "TTName", 
                            
                            //16-20
                            "LicenceNo", "Sortimen", "DtBongkar", "PaymentType", "BankAcDesc"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10 });
                Grd1.Rows.CollapseAll();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                IsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsFilterByDateInvalid()
        {
            if (Sm.CompareDtTm(Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2)) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }
       
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (IsReCompute) Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                Grd1.Cells[Row, 4].Value = "'" + Sm.GetGrdStr(Grd1, Row, 4);
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                Grd1.Cells[Row, 4].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 4).Length - 1);
            Grd1.EndUpdate();
        }

        private void SetLueDocType(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Log' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Balok' As Col2;");

            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkQueueNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor Antrian");
        }

        private void TxtQueueNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor");
        }
        private void TxtVdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Tipe");
        }

        #endregion

        #endregion
    }
}
