﻿#region Update
/*
    01/02/2023 [MYA/MNET] New Apps -> based on FrmBOQ3
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBOQ3Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBOQ3 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBOQ3Dlg4(FrmBOQ3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Load
        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.ItGrpCode, B.ItGrpName ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemGroup B On A.ItGrpCode = B.ItGrpCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("And Find_In_Set(A.ItCtCode, @ListItCtCodeForResource) ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "",
                    "Resource Code",
                    "Resource Name",
                    "Group",

                    "ItGrpCode"
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 20, 120, 280, 200,

                    0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Show Data
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And Locate(Concat('##', A.ItCode, '##'), @SelectedItCode)<1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedItCode", mFrmParent.GetSelectedItCode());
                Sm.CmParam<String>(ref cm, "@ListItCtCodeForResource", mFrmParent.mListItCtCodeForResource);
                Sm.FilterStr(ref Filter, ref cm, TxtResourceItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ForeignName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.ItName; ",
                    new string[]
                    {
                        //0
                        "ItCode", 

                        //1-2
                        "ItName", "ItGrpName", "ItGrpCode"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd6.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 1, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 2, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 3, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 12, Grd1, Row2, 6);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd6, Row1, new int[] { 5, 6, 7, 8, 9, 10, 11 });

                        mFrmParent.Grd6.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd6, mFrmParent.Grd6.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 resource.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string ItCode = Sm.GetGrdStr(Grd1, Row, 3);
            for (int Index = 0; Index < mFrmParent.Grd6.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd6, Index, 1), ItCode)) return true;
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtResourceItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkResourceItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Resource");
        }

        #endregion

        #endregion

    }
}
