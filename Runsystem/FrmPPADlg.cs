﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPPADlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPPA mFrmParent;
        private string mSQL = String.Empty;

        #endregion

        #region Constructor

        public FrmPPADlg(FrmPPA FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.UserCode, A.Username, B.GrpName ");
            SQL.AppendLine("From Tbluser A ");
            SQL.AppendLine("Inner Join  Tblgroup B On A.GrpCode = B.GrpCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "User Code",
                        "User Name",
                        "Group",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 150, 150
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {}, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtUser.Text, new string[] {"A.UserCode", "A.userName"});
                Sm.FilterStr(ref Filter, ref cm, TxtGroup.Text, new string[] {"A.GrpCode", "B.GrpName"});

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL +
                        Filter + " Order By A.UserCode ;",
                        new string[] 
                        { 
                            //0
                            "UserCode", 

                            //1-5
                            "UserName", "GrpName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtUserCode.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtUserName.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtGrpName.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                this.Close();
            }
        }

        #endregion

        #region Grid Method


        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }


        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

       
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkUser_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "User");
        }

        private void ChkGroup_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Group");
        }

        private void TxtUser_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtGroup_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion

       
       
    }
}
