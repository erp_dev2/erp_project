﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMutation : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private bool 
            mIsShowForeignName = false,
            mIsFilterByItCt = false; 


        #endregion

        #region Constructor

        public FrmRptMutation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode2, mIsFilterByItCt ? "Y" : "N");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private string GetSQL()
        {
            StringBuilder SQL = new StringBuilder(), SQL2 = new StringBuilder(), SQL3 = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, T5.WhsName, ");
            //SQL.AppendLine("T1.RowSource, T2.RowNo, T3.DNo As DNo1, T4.DNo As DNo2, ");
            SQL.AppendLine("T3.ItCode As ItCode1, T3.BatchNo As BatchNo1, T3.Source As Source1, ");
            SQL.AppendLine("T3.Lot As Lot1, T3.Bin As Bin1, ");
            SQL.AppendLine("IfNull(T3.Qty, 0) As Qty_1, ");
            SQL.AppendLine("IfNull(T3.Qty2, 0) As Qty2_1, ");
            SQL.AppendLine("IfNull(T3.Qty3, 0) As Qty3_1, ");
            SQL.AppendLine("T3.ItName As ItName1, ");
            SQL.AppendLine("T3.InventoryUomCode As InventoryUomCode_1, ");
            SQL.AppendLine("T3.InventoryUomCode2 As InventoryUomCode2_1, ");
            SQL.AppendLine("T3.InventoryUomCode3 As InventoryUomCode3_1, ");
            SQL.AppendLine("T3.CurCode As CurCode1, IfNull(T3.UPrice, 0) As UPrice1, ");
            SQL.AppendLine("T4.ItCode As ItCode2, T4.BatchNo As BatchNo2, T4.Source As Source2, ");
            SQL.AppendLine("T4.Lot As Lot2, T4.Bin As Bin2, ");
            SQL.AppendLine("IfNull(T4.Qty, 0) As Qty_2, ");
            SQL.AppendLine("IfNull(T4.Qty2, 0) As Qty2_2, ");
            SQL.AppendLine("IfNull(T4.Qty3, 0) As Qty3_2, ");
            SQL.AppendLine("T4.ItName As ItName2, ");
            SQL.AppendLine("T4.InventoryUomCode As InventoryUomCode_2, ");
            SQL.AppendLine("T4.InventoryUomCode2 As InventoryUomCode2_2, ");
            SQL.AppendLine("T4.InventoryUomCode3 As InventoryUomCode3_2, ");
            SQL.AppendLine("T4.CurCode As CurCode2, IfNull(T4.UPrice, 0) As UPrice2, T3.ForeignName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.WhsCode, Case When B.Count1 >= C.Count2 Then '1' Else '2' End As RowSource ");
            SQL.AppendLine("    From TblMutationsHdr A ");
            SQL.AppendLine("    Left Join ( ");
	        SQL.AppendLine("        Select a.DocNo, Count(a.DocNo) As Count1, C.ForeignName ");
	        SQL.AppendLine("        From TblMutationsHdr a ");
	        SQL.AppendLine("        Inner Join TblMutationsDtl b On a.DocNo=b.DocNo ");
            if (ChkBatchNo.Checked) SQL.AppendLine("    And b.BatchNo Like @BatchNo1 ");
	        SQL.AppendLine("        Inner Join TblItem C On b.ItCode=c.ItCode And c.ItCtCode=@ItCtCode1 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
	        SQL.AppendLine("        Where a.CancelInd='N' ");
	        if (ChkDocDt.Checked) SQL.AppendLine("        And a.DocDt Between @DocDt1 And @DocDt2 ");
	        SQL.AppendLine("        Group By a.DocNo ");
	        SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");
           
            SQL.AppendLine("    Left Join ( ");
	        SQL.AppendLine("        Select a.DocNo, Count(a.DocNo) As Count2 ");
	        SQL.AppendLine("        From TblMutationsHdr a ");
	        SQL.AppendLine("        Inner Join TblMutationsDtl2 b On a.DocNo=b.DocNo ");
            if (ChkBatchNo2.Checked) SQL.AppendLine("    And b.BatchNo Like @BatchNo2 ");
	        SQL.AppendLine("        Inner Join TblItem c On b.ItCode=c.ItCode And c.ItCtCode=@ItCtCode2 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("        Where a.CancelInd='N' ");
            if (ChkDocDt.Checked) SQL.AppendLine("        And a.DocDt Between @DocDt1 And @DocDt2 ");
	        SQL.AppendLine("        Group By a.DocNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            if (ChkDocDt.Checked) SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.DocNo In ( ");
            SQL.AppendLine("        Select a.DocNo ");
            SQL.AppendLine("        From TblMutationsHdr a ");
            SQL.AppendLine("        Inner Join TblMutationsDtl b On a.DocNo=b.DocNo "); //
            if (ChkBatchNo.Checked) SQL.AppendLine("    And b.BatchNo Like @BatchNo1 ");
            SQL.AppendLine("        Inner Join TblItem c On b.ItCode=c.ItCode And c.ItCtCode=@ItCtCode1 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("        Where a.CancelInd='N' ");
            if (ChkDocDt.Checked) SQL.AppendLine("        And a.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.DocNo In ( ");
            SQL.AppendLine("        Select a.DocNo ");
            SQL.AppendLine("        From TblMutationsHdr a ");
            SQL.AppendLine("        Inner Join TblMutationsDtl2 b On a.DocNo=b.DocNo ");
            if (ChkBatchNo2.Checked) SQL.AppendLine("    And b.BatchNo Like @BatchNo2 ");
            SQL.AppendLine("        Inner Join TblItem c On b.ItCode=c.ItCode And c.ItCtCode=@ItCtCode2 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("        Where a.CancelInd='N' ");
            if (ChkDocDt.Checked) SQL.AppendLine("        And a.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") T1 ");

            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select If(@prev != A.DocNo, @rownum:=1, @rownum:=@rownum+1) as RowNo, @prev:=A.DocNo, ");
	        SQL.AppendLine("    A.DocNo, '1' As RowSource ");
	        SQL.AppendLine("    From TblMutationsHdr A ");
	        SQL.AppendLine("    Inner Join TblMutationsDtl B On A.DocNo=B.DocNo ");
            if (ChkBatchNo.Checked) SQL.AppendLine("    And B.BatchNo Like @BatchNo1 ");
	        SQL.AppendLine("    Inner Join TblItem C On B.ItCode=C.ItCode And C.ItCtCode=@ItCtCode1 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Inner Join (Select @rownum := 0, @prev:='') D On 0=0 ");
	        SQL.AppendLine("    Where A.CancelInd='N' ");
            if (ChkDocDt.Checked) SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
	        SQL.AppendLine("        Union All ");
	        SQL.AppendLine("    Select If(@prev != A.DocNo, @rownum:=1, @rownum:=@rownum+1) as RowNo, @prev:=A.DocNo, ");
	        SQL.AppendLine("    A.DocNo, '2' As RowSource ");
	        SQL.AppendLine("    From TblMutationsHdr A ");
	        SQL.AppendLine("    Inner Join TblMutationsDtl2 B On A.DocNo=B.DocNo ");
            if (ChkBatchNo2.Checked) SQL.AppendLine("    And B.BatchNo Like @BatchNo2 ");
	        SQL.AppendLine("    Inner Join TblItem C On B.ItCode=C.ItCode And C.ItCtCode=@ItCtCode2 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Inner Join (Select @rownum := 0, @prev:='') D On 0=0 ");
	        SQL.AppendLine("    Where A.CancelInd='N' ");
            if (ChkDocDt.Checked) SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
	        SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.RowSource=T2.RowSource ");

            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select If(@prev != A.DocNo, @rownum:=1, @rownum:=@rownum+1) as RowNo, @prev:=A.DocNo, ");
	        SQL.AppendLine("    A.DocNo, B.DNo, B.ItCode, B.BatchNo, B.Source, B.Lot, B.Bin, B.Qty, B.Qty2, B.Qty3, ");
	        SQL.AppendLine("    F.ItName, F.InventoryUomCode, F.InventoryUomCode2, F.InventoryUomCode3, ");
	        SQL.AppendLine("    E.CurCode, E.UPrice, C.ForeignName ");
	        SQL.AppendLine("    From TblMutationsHdr A ");
	        SQL.AppendLine("    Inner Join TblMutationsDtl B On A.DocNo=B.DocNo ");
            if (ChkBatchNo.Checked) SQL.AppendLine("    And B.BatchNo Like @BatchNo1 ");
	        SQL.AppendLine("    Inner Join TblItem C On B.ItCode=C.ItCode And C.ItCtCode=@ItCtCode1 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Inner Join (Select @rownum := 0, @prev:='') D On 0=0 ");
	        SQL.AppendLine("    Left Join TblStockPrice E On B.ItCode=E.ItCode And B.BatchNo=E.BatchNo And B.Source=E.Source ");
	        SQL.AppendLine("    Left Join TblItem F On B.ItCode=F.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
	        SQL.AppendLine("    Where A.CancelInd='N' ");
            if (ChkDocDt.Checked) SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo And T2.RowNo=T3.RowNo ");
           
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select If(@prev != A.DocNo, @rownum:=1, @rownum:=@rownum+1) as RowNo, @prev:=A.DocNo, ");
	        SQL.AppendLine("    A.DocNo, B.DNo, B.ItCode, B.BatchNo, B.Source, B.Lot, B.Bin, B.Qty, B.Qty2, B.Qty3, ");
	        SQL.AppendLine("    F.ItName, F.InventoryUomCode, F.InventoryUomCode2, F.InventoryUomCode3, ");
	        SQL.AppendLine("    E.CurCode, E.UPrice, F.ForeignName ");
	        SQL.AppendLine("    From TblMutationsHdr A ");
	        SQL.AppendLine("    Inner Join TblMutationsDtl2 B On A.DocNo=B.DocNo ");
            if (ChkBatchNo2.Checked) SQL.AppendLine("    And B.BatchNo Like @BatchNo2 ");
	        SQL.AppendLine("    Inner Join TblItem C On B.ItCode=C.ItCode And C.ItCtCode=@ItCtCode2 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Inner Join (Select @rownum := 0, @prev:='') D On 0=0 ");
	        SQL.AppendLine("    Left Join TblStockPrice E On B.ItCode=E.ItCode And B.BatchNo=E.BatchNo And B.Source=E.Source ");
	        SQL.AppendLine("    Left Join TblItem F On B.ItCode=F.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
	        SQL.AppendLine("    Where A.CancelInd='N' ");
            if (ChkDocDt.Checked) SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") T4 On T1.DocNo=T4.DocNo And T2.RowNo=T4.RowNo ");
            SQL.AppendLine("Left Join TblWarehouse T5 On T1.WhsCode=T5.WhsCode ");

            SQL2.AppendLine("Select A.DocNo, A.DocDt, G.WhsName, ");
            SQL2.AppendLine("B.ItCodeFrom As ItCode1, B.BatchNoFrom As BatchNo1, B.SourceFrom As Source1, ");
            SQL2.AppendLine("B.Lot As Lot1, B.Bin As Bin1, ");
            SQL2.AppendLine("IfNull(B.QtyFrom, 0) As Qty_1, ");
            SQL2.AppendLine("IfNull(B.Qty2From, 0) As Qty2_1, ");
            SQL2.AppendLine("IfNull(B.Qty3From, 0) As Qty3_1, ");
            SQL2.AppendLine("C.ItName As ItName1, ");
            SQL2.AppendLine("C.InventoryUomCode As InventoryUomCode_1, ");
            SQL2.AppendLine("C.InventoryUomCode2 As InventoryUomCode2_1, ");
            SQL2.AppendLine("C.InventoryUomCode3 As InventoryUomCode3_1, ");
            SQL2.AppendLine("E.CurCode As CurCode1, IfNull(E.UPrice, 0) As UPrice1, ");
            SQL2.AppendLine("B.ItCodeTo As ItCode2, B.BatchNoTo As BatchNo2, B.SourceTo As Source2, ");
            SQL2.AppendLine("B.Lot As Lot2, B.Bin As Bin2, ");
            SQL2.AppendLine("IfNull(B.QtyTo, 0) As Qty_2, ");
            SQL2.AppendLine("IfNull(B.Qty2To, 0) As Qty2_2, ");
            SQL2.AppendLine("IfNull(B.Qty3To, 0) As Qty3_2, ");
            SQL2.AppendLine("D.ItName As ItName2, ");
            SQL2.AppendLine("D.InventoryUomCode As InventoryUomCode_2, ");
            SQL2.AppendLine("D.InventoryUomCode2 As InventoryUomCode2_2, ");
            SQL2.AppendLine("D.InventoryUomCode3 As InventoryUomCode3_2, ");
            SQL2.AppendLine("F.CurCode As CurCode2, IfNull(F.UPrice, 0) As UPrice2, C.ForeignName ");
            SQL2.AppendLine("From TblMutationHdr A ");
            SQL2.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            if (ChkBatchNo.Checked) SQL2.AppendLine("    And B.BatchNoFrom Like @BatchNo1 ");
            if (ChkBatchNo2.Checked) SQL2.AppendLine("    And B.BatchNoTo Like @BatchNo2 ");
            SQL2.AppendLine("Inner Join TblItem C On B.ItCodeFrom=C.ItCode And C.ItCtCode=@ItCtCode1 ");
            if (mIsFilterByItCt)
            {
                SQL2.AppendLine("And Exists( ");
                SQL2.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL2.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL2.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL2.AppendLine("    ) ");
            }
            SQL2.AppendLine("Inner Join TblItem D On B.ItCodeTo=D.ItCode And D.ItCtCode=@ItCtCode2 ");
            if (mIsFilterByItCt)
            {
                SQL2.AppendLine("And Exists( ");
                SQL2.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL2.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL2.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL2.AppendLine("    ) ");
            }
            SQL2.AppendLine("Left Join TblStockPrice E On B.ItCodeFrom=E.ItCode And B.BatchNoFrom=E.BatchNo And B.SourceFrom=E.Source ");
            SQL2.AppendLine("Left Join TblStockPrice F On B.ItCodeTo=F.ItCode And B.BatchNoTo=F.BatchNo And B.SourceTo=F.Source ");
            SQL2.AppendLine("Inner Join TblWarehouse G On A.WhsCode=G.WhsCode ");
            if (ChkDocDt.Checked) SQL2.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");

            SQL3.AppendLine("Select * From (" + SQL.ToString() + " Union All " + SQL2.ToString() + " ) T ");

            return SQL3.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",   
                        "Warehouse", 
                        "Item's Code",
                        "Item's Name", 

                        //6-10
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",
                        "Quantity", 
                        
                        //11-15
                        "UoM",                     
                        "Quantity", 
                        "UoM",
                        "Quantity", 
                        "UoM",
                        
                        //16-20
                        "Currency",
                        "Unit Price",
                        "Item's Code",
                        "Item's Name", 
                        "Batch#",
                        
                        //21-25
                        "Source",
                        "Lot",
                        "Bin",
                        "Quantity", 
                        "UoM",                     
                        
                        //26-30
                        "Quantity", 
                        "UoM",
                        "Quantity", 
                        "UoM",
                        "Currency",
                        
                        //31-32
                        "Unit Price",
                        "Foreign Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 150, 80, 150,
                        
                        //6-10
                        200, 170, 60, 60, 80,  
                        
                        //11-15
                        80, 80, 80, 80, 80, 

                        //16-20
                        60, 100, 80, 150, 200,
                        
                        //21-25
                        170, 60, 60, 80, 80,  
                        
                        //26-30
                        80, 80, 80, 80, 60,
                        
                        //31-32
                        100, 170
                    }
                );
            for (int Col = 4; Col <= 17; Col++)
                Grd1.Cols[Col].ColHdrStyle.BackColor = Color.LightSalmon;

            for (int Col = 18; Col <= 31; Col++)
                Grd1.Cols[Col].ColHdrStyle.BackColor = Color.LightGreen;

            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 14, 17, 24, 26, 28, 31 }, 0);
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 8, 9, 12, 13, 14, 15, 18, 21, 22, 23, 26, 27, 28, 29 }, false);
                Grd1.Cols[32].Move(4);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 8, 9, 12, 13, 14, 15, 18, 21, 22, 23, 26, 27, 28, 29, 32 }, false);
            }
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 18, 21 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 26, 27 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 26, 27, 28, 29 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsLueEmpty(LueItCtCode, "Mutated from")) return;
                if (Sm.IsLueEmpty(LueItCtCode2, "Mutated to")) return;

                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@ItCtCode1", Sm.GetLue(LueItCtCode));
                Sm.CmParam<String>(ref cm, "@ItCtCode2", Sm.GetLue(LueItCtCode2));
                Sm.CmParam<String>(ref cm, "@BatchNo1", "%" + TxtBatchNo.Text + "%");
                Sm.CmParam<String>(ref cm, "@BatchNo2", "%" + TxtBatchNo2.Text + "%");
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                GetSQL() + " Order By DocDt, DocNo, ItCode1 Desc, BatchNo1, ItCode2 Desc, BatchNo2;",
                new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "WhsName", "ItCode1", "ItName1", "BatchNo1", 
                        
                        //6-10
                        "Source1", "Lot1", "Bin1", "Qty_1", "InventoryUomCode_1",

                        //11-15
                        "Qty2_1", "InventoryUomCode2_1", "Qty3_1", "InventoryUomCode3_1", "CurCode1",
                        
                        //16-20
                        "UPrice1", "ItCode2", "ItName2", "BatchNo2", "Source2", 
                        
                        //21-25
                        "Lot2", "Bin2", "Qty_2", "InventoryUomCode_2", "Qty2_2", 

                        //26-30
                        "InventoryUomCode2_2", "Qty3_2", "InventoryUomCode3_2", "CurCode2", "UPrice2", 
                        
                        //31
                        "ForeignName"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 31);
                }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 12, 14, 24, 26, 28 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtBatchNo2_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch# (To)");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
        }

        private void LueItCtCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode2, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
        }

        #endregion

        #endregion

    }
}
