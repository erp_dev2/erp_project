﻿#region Update
/*
    09/12/2021 [IBL/PHT] New apps based on RptProfitLossComparation
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLossComparation2 : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;
        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mAccountingRptStartFrom = string.Empty,
            mDocTitle = string.Empty,
            mAcNoForCurrentEarning2 = "9",
            mAcNoForActiva = "1",
            mAcNoForPassiva = "2",
            mAcNoForCapital = "3",
            mCurrentEarningFormulaType = "1",
            mMaxAccountCategory = "9",
            mDefaultBalanceInProfitLoss = "0",
             mCOAInterOffice = string.Empty,
            mCOAInterOffice2 = string.Empty,
            mFormulaForComputeProfitLoss = string.Empty;

        private bool 
            mIsEntityMandatory = false,
            mIsJournalCostCenterEnabled = false,
            mIsReportingFilterByEntity = false,
            mIsRptProfitLossComparationUseProfitCenter = false,
            mIsAllProfitCenterSelected = false,
            mIsFilterByCC = false,
            mIsProfitLossShowInterOfficeAccount = false;

        string[] mCOAInterOffice3 = null;
        #endregion

        #region Constructor

        public FrmRptProfitLossComparation2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                if (mAccountingRptStartFrom.Length > 0)
                {
                    Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                    Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                }
                else
                {
                    Sl.SetLueYr(LueStartFrom, string.Empty);
                    Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                }
                if (mIsReportingFilterByEntity) SetLueEntCode(ref LueEntCode);
                if (mIsRptProfitLossComparationUseProfitCenter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                SetGrd();
                Sl.SetLueCCCode(ref LueCCCode, string.Empty, mIsFilterByCC ? "Y" : "N");
                //Sl.SetLueCCCode(ref LueCCCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mAcNoForCurrentEarning = Sm.GetParameter("AcNoForCurrentEarning");
            mAcNoForIncome = Sm.GetParameter("AcNoForIncome");
            mAcNoForCost = Sm.GetParameter("AcNoForCost");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
            mDocTitle = Sm.GetParameter("DocTitle");
            mIsJournalCostCenterEnabled = Sm.GetParameterBoo("IsJournalCostCenterEnabled");
            mAcNoForCurrentEarning2 = Sm.GetParameter("AcNoForCurrentEarning2");
            mAcNoForActiva = Sm.GetParameter("AcNoForActiva");
            mAcNoForPassiva = Sm.GetParameter("AcNoForPassiva");
            mAcNoForCapital = Sm.GetParameter("AcNoForCapital");
            mCurrentEarningFormulaType = Sm.GetParameter("CurrentEarningFormulaType");
            mMaxAccountCategory = Sm.GetParameter("MaxAccountCategory");
            if (mMaxAccountCategory.Length == 0) mMaxAccountCategory = "5";
            mDefaultBalanceInProfitLoss = Sm.GetParameter("DefaultBalanceInProfitLoss");
            mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
            mIsRptProfitLossComparationUseProfitCenter = Sm.GetParameterBoo("IsRptProfitLossComparationUseProfitCenter");
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
            mIsProfitLossShowInterOfficeAccount = Sm.GetParameterBoo("IsProfitLossShowInterOfficeAccount");
            mFormulaForComputeProfitLoss = Sm.GetParameter("FormulaForComputeProfitLoss");
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 41;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Account Name";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 120;
            Grd1.Header.Cells[0, 2].Value = "Debit";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 120;
            Grd1.Header.Cells[0, 3].Value = "Credit";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 180;
            Grd1.Header.Cells[0, 4].Value = "Balance";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            int LastCol = 5;
            string[] arrMth = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            for (int z = 0; z < 12; z++)
            {
                Grd1.Cols[LastCol].Width = 120;
                Grd1.Header.Cells[0, LastCol].Value = "Debit" + Environment.NewLine + "" + arrMth[z] + "";
                Grd1.Header.Cells[0, LastCol].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Header.Cells[0, LastCol].SpanRows = 2;

                Grd1.Cols[LastCol+1].Width = 120;
                Grd1.Header.Cells[0, LastCol + 1].Value = "Credit" + Environment.NewLine + "" + arrMth[z] + "";
                Grd1.Header.Cells[0, LastCol + 1].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Header.Cells[0, LastCol + 1].SpanRows = 2;

                Grd1.Cols[LastCol+2].Width = 150;
                Grd1.Header.Cells[0, LastCol + 2].Value = mDocTitle == "KIM"?"Year to Date "+ Environment.NewLine + "" + arrMth[z] + "":"Balance" + Environment.NewLine + "" + arrMth[z] + "";
                Grd1.Header.Cells[0, LastCol + 2].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Header.Cells[0, LastCol + 2].SpanRows = 2;

                LastCol = LastCol + 3;
            }

            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 6, 8, 9, 11, 12, 14, 15, 17, 18, 20, 21, 23, 24, 26, 27, 29, 30, 32, 33, 35, 36, 38, 39 }, false);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 6, 8, 9, 11, 12, 14, 15, 17, 18, 20, 21, 23, 24, 26, 27, 29, 30, 32, 33, 35, 36, 38, 39 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;

            Cursor.Current = Cursors.WaitCursor;

            decimal Bal4 = 0, Bal5 = 0, Bal6 = 0, Bal7 = 0, Bal8 = 0;
            var Yr = Sm.GetLue(LueYr);
            var StartFrom = Sm.GetLue(LueStartFrom);
            var defaultColumn = 5;
            try
            {
                var lCOA = new List<COA>();
                var lEntityCOA = new List<EntityCOA>();
                var lCOAInterOffice = new List<COAInterOffice>();
                var lCOAProfitLossSetting = new List<COAProfitLossSetting>();
                var IsFilterByEntity = ChkEntCode.Checked;

                SetProfitCenter();
                if (mIsProfitLossShowInterOfficeAccount) ProcessCOAInterOffice(ref lCOAInterOffice);
                Process1(ref lCOA);
                GetCOAProfitLossSetting(ref lCOAProfitLossSetting);
                ProcessProfitLossSetting(ref lCOA, ref lCOAProfitLossSetting);
                if (IsFilterByEntity) Process7(ref lEntityCOA);
                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom);
                        Process3(ref lCOA, Yr, StartFrom, "00");
                    }
                    else
                    {
                        Process2(ref lCOA, Yr);
                        Process3(ref lCOA, Yr, StartFrom, "00");
                    }
                    Process4(ref lCOA);
                    Process6(ref lCOA);
                    ComputeProfitLoss(ref lCOA, "00");
                    #region Filter by Entity

                    if (IsFilterByEntity)
                    {
                        if (lEntityCOA.Count > 0)
                        {
                            if (lCOA.Count > 0)
                            {
                                Grd1.BeginUpdate();
                                Grd1.Rows.Count = 0;

                                iGRow r;

                                r = Grd1.Rows.Add();
                                r.Level = 0;
                                r.TreeButton = iGTreeButtonState.Visible;
                                r.Cells[0].Value = "COA";
                                for (var j = 0; j < lEntityCOA.Count; j++)
                                {
                                    for (var i = 0; i < lCOA.Count; i++)
                                    {
                                        if (lCOA[i].ProfitLossSetting.Length > 0)
                                        {
                                            if (lCOA[i].AcNo == lEntityCOA[j].AcNo)
                                            {
                                                r = Grd1.Rows.Add();
                                                r.Level = lCOA[i].Level;
                                                r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                r.Cells[0].Value = lCOA[i].AcNo;
                                                r.Cells[1].Value = lCOA[i].AcDesc;
                                                for (var c = 2; c < 5; c++)
                                                    r.Cells[c].Value = 0m;
                                                r.Cells[2].Value = lCOA[i].YearToDateDAmt;
                                                r.Cells[3].Value = lCOA[i].YearToDateCAmt;
                                                r.Cells[4].Value = lCOA[i].Balance;
                                                if (lCOA[i].AcNo == "4") Bal4 = lCOA[i].Balance;
                                                if (lCOA[i].AcNo == "5") Bal5 = lCOA[i].Balance;
                                                if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                                                {
                                                    if (lCOA[i].AcNo == "6") Bal6 = lCOA[i].Balance;
                                                    if (lCOA[i].AcNo == "7") Bal7 = lCOA[i].Balance;
                                                    if (lCOA[i].AcNo == "8") Bal8 = lCOA[i].Balance;
                                                }
                                            }
                                        }
                                    }
                                }

                                Grd1.TreeLines.Visible = true;
                                Grd1.Rows.CollapseAll();
                                r = Grd1.Rows.Add();
                                r.Cells[0].Value = "Total";
                                if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                                    r.Cells[4].Value = (Bal4 - (Bal5+Bal6+Bal7+Bal8));
                                else
                                    r.Cells[4].Value = (Bal4 - Bal5);
                                r.BackColor = Color.LightSalmon;
                                Grd1.EndUpdate();
                            }
                        }
                    }
                    #endregion

                    #region Not Filter By Entity
                    else
                    {
                        if (lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();
                            Grd1.Rows.Count = 0;

                            iGRow r;

                            r = Grd1.Rows.Add();
                            r.Level = 0;
                            r.TreeButton = iGTreeButtonState.Visible;
                            r.Cells[0].Value = "COA";
                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                if (lCOA[i].ProfitLossSetting.Length > 0)
                                {
                                    r = Grd1.Rows.Add();
                                    r.Level = lCOA[i].Level;
                                    r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                    r.Cells[0].Value = lCOA[i].AcNo;
                                    r.Cells[1].Value = lCOA[i].AcDesc;
                                    for (var c = 2; c < 5; c++)
                                        r.Cells[c].Value = 0m;
                                    r.Cells[2].Value = lCOA[i].YearToDateDAmt;
                                    r.Cells[3].Value = lCOA[i].YearToDateCAmt;
                                    r.Cells[4].Value = lCOA[i].Balance;
                                    if (lCOA[i].AcNo == "4") Bal4 = lCOA[i].Balance;
                                    if (lCOA[i].AcNo == "5") Bal5 = lCOA[i].Balance;
                                    if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                                    {
                                        if (lCOA[i].AcNo == "6") Bal6 = lCOA[i].Balance;
                                        if (lCOA[i].AcNo == "7") Bal7 = lCOA[i].Balance;
                                        if (lCOA[i].AcNo == "8") Bal8 = lCOA[i].Balance;
                                    }
                                }
                            }
                            Grd1.TreeLines.Visible = true;
                            Grd1.Rows.CollapseAll();
                            r = Grd1.Rows.Add();
                            r.Cells[0].Value = "Total";
                            if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                                r.Cells[4].Value = (Bal4 - (Bal5 + Bal6 + Bal7 + Bal8));
                            else
                                r.Cells[4].Value = (Bal4 - Bal5);
                            r.BackColor = Color.LightSalmon;
                            Grd1.EndUpdate();
                        }
                    }
                      
                    #endregion

                    lCOA.Clear();
                    lEntityCOA.Clear();
                }

                for (int monthly = 1; monthly < 13; monthly++)
                {
                    ShowData2(Yr, StartFrom,
                        monthly.ToString().Length > 1 ?
                            monthly.ToString() :
                            string.Concat("0", monthly), defaultColumn);
                    defaultColumn = defaultColumn + 3;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowData2(string Yr, string StartFrom, string Mth, int colGrid)
        {
            try
            {
                var lCOA = new List<COA>();
                var lEntityCOA = new List<EntityCOA>();
                var lCOAProfitLossSetting = new List<COAProfitLossSetting>();
                var IsFilterByEntity = ChkEntCode.Checked;
                decimal Bal4 = 0, Bal5 = 0, Bal6 = 0, Bal7 = 0, Bal8 = 0;

                Process1(ref lCOA);
                GetCOAProfitLossSetting(ref lCOAProfitLossSetting);
                ProcessProfitLossSetting(ref lCOA, ref lCOAProfitLossSetting);

                if (IsFilterByEntity) Process7(ref lEntityCOA);
                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom);
                        Process3(ref lCOA, Yr, StartFrom, Mth);
                    }
                    else
                    {
                        Process2(ref lCOA, Yr);
                        Process3(ref lCOA, Yr, StartFrom, Mth);
                    }

                    //Process2(ref lCOA, Yr);
                    //Process3(ref lCOA, Yr, Mth);
                    Process4(ref lCOA);
                    Process6(ref lCOA);
                    ComputeProfitLoss(ref lCOA, Mth);

                    #region Filter by Entity

                    if (IsFilterByEntity) 
                    {
                        if (lEntityCOA.Count > 0 && lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();
                            int r = 1;

                            for (var j = 0; j < lEntityCOA.Count; j++)
                            {
                                r = 1;
                                foreach (var i in lCOA.OrderBy(o=>o.AcNo))
                                {
                                    if (Sm.CompareStr(i.AcNo, lEntityCOA[j].AcNo))
                                    {
                                        if (i.ProfitLossSetting.Length > 0)
                                        {
                                            for (int rowGrd = r; rowGrd < Grd1.Rows.Count; rowGrd++)
                                            {
                                                if (Sm.CompareStr(i.AcNo, Sm.GetGrdStr(Grd1, rowGrd, 0)))
                                                {
                                                    Grd1.Cells[rowGrd, colGrid].Value = i.YearToDateDAmt;
                                                    Grd1.Cells[rowGrd, colGrid + 1].Value = i.YearToDateCAmt;
                                                    Grd1.Cells[rowGrd, colGrid + 2].Value = i.Balance;
                                                    if (i.AcNo == "4") Bal4 = i.Balance;
                                                    if (i.AcNo == "5") Bal5 = i.Balance;
                                                    if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                                                    {
                                                        if (i.AcNo == "6") Bal6 = i.Balance;
                                                        if (i.AcNo == "7") Bal7 = i.Balance;
                                                        if (i.AcNo == "8") Bal8 = i.Balance;
                                                    }
                                                    r = rowGrd;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }


                            Grd1.TreeLines.Visible = true;
                            Grd1.Rows.CollapseAll();
                            if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                                Grd1.Cells[(Grd1.Rows.Count - 1), (colGrid + 2)].Value = (Bal4 - (Bal5+Bal6+Bal7+Bal8));
                            else
                                Grd1.Cells[(Grd1.Rows.Count - 1), (colGrid + 2)].Value = (Bal4 -Bal5);
                            Grd1.EndUpdate();
                        }
                    }
                    #endregion

                    #region Not Filter By Entity
                    else
                    {
                        if (lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();
                            int r = 1;
                            foreach (var i in lCOA.OrderBy(o=>o.AcNo))
                            {
                                if (i.ProfitLossSetting.Length > 0)
                                {
                                    for (int rowGrd = r; rowGrd < Grd1.Rows.Count; rowGrd++)
                                    {
                                        if (Sm.CompareStr(i.AcNo, Sm.GetGrdStr(Grd1, rowGrd, 0)))
                                        {
                                            Grd1.Cells[rowGrd, colGrid].Value = i.YearToDateDAmt;
                                            Grd1.Cells[rowGrd, colGrid + 1].Value = i.YearToDateCAmt;
                                            Grd1.Cells[rowGrd, colGrid + 2].Value = i.Balance;
                                            if (i.AcNo == "4") Bal4 = i.Balance;
                                            if (i.AcNo == "5") Bal5 = i.Balance;
                                            if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                                            {
                                                if (i.AcNo == "6") Bal6 = i.Balance;
                                                if (i.AcNo == "7") Bal7 = i.Balance;
                                                if (i.AcNo == "8") Bal8 = i.Balance;
                                            }
                                            r = rowGrd;
                                            break;
                                        }
                                    }
                                }
                            }
                            Grd1.TreeLines.Visible = true;
                            Grd1.Rows.CollapseAll();
                            if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                                Grd1.Cells[(Grd1.Rows.Count - 1), (colGrid + 2)].Value = (Bal4 - (Bal5 + Bal6 + Bal7 + Bal8));
                            else
                                Grd1.Cells[(Grd1.Rows.Count - 1), (colGrid + 2)].Value = (Bal4 -Bal5);
                            Grd1.EndUpdate();
                        }
                    }

                    #endregion

                    lCOA.Clear();
                    lEntityCOA.Clear();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptProfitLossComparationUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked)
                mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
            //SQL.AppendLine("Union All ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("    Select A.EntCode As Col1, B.EntName As Col2 ");
                SQL.AppendLine("    From TblGroupEntity A ");
                SQL.AppendLine("    Inner Join TblEntity B On A.EntCode = B.EntCode And B.ActInd='Y' ");
                SQL.AppendLine("    Where A.GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T Where T.ActInd='Y'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        //show account number, account description, parent, type
        private void Process1(ref List<COA> lCOA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);

            SQL.AppendLine("Select A.AcNo, A.AcDesc, A.Parent, A.AcType, C.ProfitLossSetting ");
            SQL.AppendLine("From TblCOA A ");
            if (ChkEntCode.Checked) SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo=B.AcNo And B.EntCode=@EntCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.AcNo, 'plus' As ProfitLossSetting ");
            SQL.AppendLine("    From TblProfitlossSettingHdr A ");
            SQL.AppendLine("    Inner Join TblProfitLossSettingDtl B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    Where A.Yr = @Yr And A.ActInd = 'Y' And A.ProfitLossInd = 'Y' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select B.AcNo, 'minus' As ProfitLossSetting ");
            SQL.AppendLine("    From TblProfitlossSettingHdr A ");
            SQL.AppendLine("    Inner Join TblProfitLossSettingDtl2 B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    Where A.Yr = @Yr And A.ActInd = 'Y' And A.ProfitLossInd = 'Y' ");
            SQL.AppendLine(")C On A.AcNo = C.AcNo ");
            SQL.AppendLine("Where (Left(A.AcNo, 1) In ('4', '5' ");
            for (int i = 5; i < MaxAccountCategory; i++)
            {
                int Numb = i + 1;
                SQL.AppendLine(", '" + Numb.ToString() + "' ");
            }
            SQL.AppendLine(") ");
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or A.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or A.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(A.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.ActInd='Y' ");
            SQL.AppendLine("ORDER BY A.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                if (mIsProfitLossShowInterOfficeAccount)
                {
                    Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                    Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
                }
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType", "ProfitLossSetting" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            ProfitLossSetting = Sm.DrStr(dr, c[4]),
                            HasChild = false,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                        });
                    }
                }
                dr.Close();
            }
            foreach (var l in lCOA)
                Console.WriteLine("Test1.1 : " + l.AcNo + " - " + l.ProfitLossSetting);
        }

        //get opening balance
        private void Process2(ref List<COA> lCOA, string Yr) //, string Mth)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var lJournal = new List<Journal>();
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A, TblCOAOpeningBalanceDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("And (Left(B.AcNo, 1) In ('4', '5' ");
            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory > 6)
                    SQL.AppendLine(", '6' ");
                if (MaxAccountCategory > 7)
                    SQL.AppendLine(", '7' ");
                if (MaxAccountCategory > 8)
                    SQL.AppendLine(", '8' ");
            }
            if (mCurrentEarningFormulaType == "3")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(", '6' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(", '7' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(", '8' ");
            }
            SQL.AppendLine(") ");
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or B.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or B.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(B.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("And C.ActInd='Y' ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Yr=@Yr ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And A.EntCode Is Not Null And A.EntCode=@EntCode ");
            if (mIsRptProfitLossComparationUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.ProfitCenterCode=@ProfitCenter" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                        SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                }
            }
            SQL.AppendLine("Order By B.AcNo; ");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            //Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (mIsProfitLossShowInterOfficeAccount)
            {
                Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
            }
            if (ChkEntCode.Checked)
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        //sum debit and credit amount
        private void Process3(ref List<COA> lCOA, string Yr, string StartFrom, string Mth)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);

            SQL.AppendLine("SELECT B.AcNo, SUM(B.DAmt) AS DAmt, SUM(B.CAmt) AS CAmt ");
            SQL.AppendLine("FROM TblJournalHdr A  ");
            SQL.AppendLine("INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo And (Left(B.AcNo, 1) In ('4', '5' ");
            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory > 6)
                    SQL.AppendLine(", '6' ");
                if (MaxAccountCategory > 7)
                    SQL.AppendLine(", '7' ");
                if (MaxAccountCategory > 8)
                    SQL.AppendLine(", '8' ");
            }
            if (mCurrentEarningFormulaType == "3")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(", '6' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(", '7' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(", '8' ");
            }
            SQL.AppendLine(") ");
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or B.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or B.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(B.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("INNER JOIN TblCOA C ON B.AcNo=C.AcNo And C.ActInd='Y' ");
            if (Mth == "00")
                SQL.AppendLine("Where Left(A.DocDt, 6)<=Concat(@Yr, @Mth) ");
            else
                SQL.AppendLine("Where Left(A.DocDt, 6)=Concat(@Yr, @Mth) ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
            }
            if (ChkCCCode.Checked) 
                SQL.AppendLine("And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            if (mIsRptProfitLossComparationUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine("Group By B.AcNo; ");
            
            var lJournal = new List<Journal>();
            
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            if (Mth == "00")
                Sm.CmParam<String>(ref cm, "@Mth", Sm.ServerCurrentDateTime().Substring(4, 2));
            else
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            if (ChkCCCode.Checked) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            if (mIsProfitLossShowInterOfficeAccount)
            {
                Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
            }
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (lCOA[j].ProfitLossSetting.Length > 0 && lCOA[j].ProcessInd == "Y")
                        {
                            if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                            {
                                if (!IsFirst)
                                {
                                    IsFirst = true;
                                    Temp = j;
                                }
                                lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                                lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                                if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                    break;
                            }
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        //sum to column year to date
        private void Process4(ref List<COA> lCOA)
        {
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);
            string AcType9 = string.Empty;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].ProfitLossSetting.Length > 0)
                {
                    lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt;

                    lCOA[i].YearToDateCAmt =
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateCAmt;

                    if (lCOA[i].AcType == "D")
                        lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                    if (lCOA[i].AcType == "C")
                        lCOA[i].Balance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                }
            }
        }

        //checking whether this account has parent or nah
        private void Process6(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;
            //var ParentRow = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                        //lCOA[i].ParentRow = ParentRow;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                //ParentRow = j;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                //lCOA[i].ParentRow = ParentRow;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process7(ref List<EntityCOA> lEntityCOA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo And U.EntCode Is Not Null And U.EntCode=@EntCode ");
            else
                SQL.AppendLine("Left Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And (Left(T.AcNo, 1) In ('4','5' ");

            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory > 6)
                    SQL.AppendLine(", '6' ");
                if (MaxAccountCategory > 7)
                    SQL.AppendLine(", '7' ");
                if (MaxAccountCategory > 8)
                    SQL.AppendLine(", '8' ");
            }
            if (mCurrentEarningFormulaType == "3")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(", '6' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(", '7' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(", '8' ");
            }
            SQL.AppendLine(") ");
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or T.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or T.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(T.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                if (mIsProfitLossShowInterOfficeAccount)
                {
                    Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                    Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
                }
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ComputeProfitLoss(ref List<COA> lCOA, string Mth)
        {
            decimal penambah = 0;
            decimal pengurang = 0;
            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].ProcessInd == "Y")
                {
                    if (lCOA[i].ProfitLossSetting == "plus")
                    {
                        penambah = penambah + lCOA[i].Balance;
                    }

                    if (lCOA[i].ProfitLossSetting == "minus")
                    {
                        pengurang = pengurang + lCOA[i].Balance;
                    }

                }
            }
            if (Mth == "00") TxtProfitLoss.EditValue = Sm.FormatNum((penambah - pengurang), 0);
        }

        private void GetInterOfficeAccount(ref List<COAInterOffice> lCOAInterOffice)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.OptCode As ParentTarget, B.AcNo, B.Level, B.Parent ");
            SQL.AppendLine("From TblOption A ");
            SQL.AppendLine("Inner Join TblCOA B On A.Property1 = B.AcNo ");
            SQL.AppendLine("Where A.OptCat = 'ProfitLossInterOfficeAccount' ");
            SQL.AppendLine("And A.Property1 Is Not Null ");
            SQL.AppendLine("And A.Property1 Not In('All', 'Parent') ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Parent", "ParentTarget", "Level" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOAInterOffice.Add(new COAInterOffice()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Parent = Sm.DrStr(dr, c[1]),
                            ParentTarget = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrInt(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessCOAInterOffice(ref List<COAInterOffice> lCOAInterOffice)
        {
            GetInterOfficeAccount(ref lCOAInterOffice);
            mCOAInterOffice2 = Sm.GetValue("Select OptCode From TblOption Where  OptCat = 'ProfitLossInterOfficeAccount' And Property1 = 'Parent'; ");
            string COAInterOffice3 = Sm.GetValue("Select Group_Concat(OptCode) From TblOption Where  OptCat = 'ProfitLossInterOfficeAccount'  And Property1 = 'All'; ");
            char[] delimiters = { ',' };
            mCOAInterOffice3 = COAInterOffice3.Split(delimiters);
            if (lCOAInterOffice.Count > 0)
            {
                foreach (var a in lCOAInterOffice.OrderBy(o => o.AcNo))
                {
                    bool IsFirst = true;
                    string AcNo = string.Empty;

                    if (IsFirst)
                    {
                        AcNo = a.AcNo;
                        IsFirst = false;
                    }

                    int delimiterCount = AcNo.Count(c => c == '.');
                    int lastIndexDelimiter = AcNo.LastIndexOf('.');

                    for (int i = delimiterCount; i >= 0; --i)
                    {
                        if (AcNo == a.ParentTarget)
                        {
                            mCOAInterOffice += ',';
                            mCOAInterOffice += AcNo;
                            break;
                        }
                        if (mCOAInterOffice.Length != 0)
                            mCOAInterOffice += ',';
                        mCOAInterOffice += AcNo;

                        if (i != 0)
                        {
                            AcNo = AcNo.Substring(0, lastIndexDelimiter);
                            lastIndexDelimiter = AcNo.LastIndexOf('.');
                        }

                    }


                }
            }

        }

        private void GetCOAProfitLossSetting(ref List<COAProfitLossSetting> lCOAProfitLossSetting)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Tb1.AcNo, Tb1.ProfitLossSetting, tb2.Level, tb2.parent, tb1.ProcessInd From ( ");
            SQL.AppendLine("    Select B.AcNo, 'plus' AS ProfitLossSetting, B.ProcessInd ");
            SQL.AppendLine("    FROM TblProfitlossSettingHdr A ");
            SQL.AppendLine("    Inner Join TblProfitLossSettingDtl B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    Where Yr = @Yr AND ActInd = 'Y' AND ProfitLossInd = 'Y' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select B.AcNo, 'minus' AS ProfitLossSetting, B.ProcessInd ");
            SQL.AppendLine("    From TblProfitLossSettingHdr A ");
            SQL.AppendLine("    Inner Join TblProfitLossSettingDtl2 B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    Where Yr = @Yr AND ActInd = 'Y' AND ProfitLossInd = 'Y' ");
            SQL.AppendLine(")tb1 ");
            SQL.AppendLine("Inner Join TblCoa tb2 On Tb1.Acno = tb2.acNo ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "ProfitLossSetting", "Level", "Parent", "ProcessInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOAProfitLossSetting.Add(new COAProfitLossSetting()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            ProfitLossSetting = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrStr(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            ProcessInd = Sm.DrStr(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }


        private void ProcessProfitLossSetting(ref List<COA> lCOA, ref List<COAProfitLossSetting> lCOAProfitLossSetting)
        {
            for (int i = 0; i < lCOAProfitLossSetting.Count; i++)
            {
                if (lCOAProfitLossSetting[i].ProcessInd == "Y")
                {
                    for (int z = 0; z < lCOA.Count; z++)
                    {
                        if (lCOAProfitLossSetting[i].AcNo == lCOA[z].AcNo)
                        {
                            lCOA[z].ProcessInd = "Y";
                            break;
                        }
                    }
                }
            }

            string mParentPL = string.Empty;
            for (int i = 0; i < lCOAProfitLossSetting.Count; i++)
            {
                int mLevelPL = Convert.ToInt32(lCOAProfitLossSetting[i].Level);
                string ProfitLossSetting = lCOAProfitLossSetting[i].ProfitLossSetting;
                mParentPL = lCOAProfitLossSetting[i].Parent;

                for (int k = 0; k < mLevelPL; k++)
                {
                    if (mParentPL.Length > 0)
                    {
                        for (int j = 0; j < lCOA.Count; j++)
                        {
                            if (lCOA[j].AcNo == mParentPL)
                            {
                                lCOA[j].ProfitLossSetting = ProfitLossSetting;
                                mParentPL = lCOA[j].Parent;
                                break;
                            }
                        }
                    }
                }

            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);

        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost center");
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public string AcType { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public string ProfitLossSetting { get; set; }
            public string ProcessInd { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        private class COAInterOffice
        {
            public string AcNo { get; set; }
            public string Parent { get; set; }
            public string ParentTarget { get; set; }
            public int Level { get; set; }
        }


        private class COAProfitLossSetting
        {
            public string AcNo { get; set; }
            public string ProfitLossSetting { get; set; }
            public string Level { get; set; }
            public string Parent { get; set; }
            public string ProcessInd { get; set; }

        }

        #endregion


    }
}
