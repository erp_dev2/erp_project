﻿namespace RunSystem
{
    partial class FrmARDownpayment2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmARDownpayment2));
            this.label13 = new System.Windows.Forms.Label();
            this.TxtSOCurCode = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtSOAmt = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtNoOfDownpayment = new DevExpress.XtraEditors.TextEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LuePIC = new DevExpress.XtraEditors.LookUpEdit();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.BtnVoucherDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtVoucherDocNo = new DevExpress.XtraEditors.TextEdit();
            this.BtnVoucherRequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtVoucherRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.BtnSODocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSODocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtSODocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtGiroNo = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.LuePaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.TcARDownpayment = new DevExpress.XtraTab.XtraTabControl();
            this.TpGeneral = new DevExpress.XtraTab.XtraTabPage();
            this.TxtPtName = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtOtherAmt = new DevExpress.XtraEditors.TextEdit();
            this.TpTax = new DevExpress.XtraTab.XtraTabPage();
            this.label44 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt3 = new DevExpress.XtraEditors.DateEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.TxtTaxAmt3 = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.LueTaxCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTaxInvoiceNo3 = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt2 = new DevExpress.XtraEditors.DateEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.TxtTaxAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.LueTaxCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTaxInvoiceNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtDownpaymentPercentage = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtAmtAftTax = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt = new DevExpress.XtraEditors.DateEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtTaxAmt = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.LueTaxCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTaxInvoiceNo = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.TxtAmtBefTax = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TpCOA = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtVoucherRequestDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtVoucherDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.TxtCOAAmt = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TpApproal = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.label24 = new System.Windows.Forms.Label();
            this.MeeLocalDocNo = new DevExpress.XtraEditors.MemoExEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoOfDownpayment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSODocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcARDownpayment)).BeginInit();
            this.TcARDownpayment.SuspendLayout();
            this.TpGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOtherAmt.Properties)).BeginInit();
            this.TpTax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDownpaymentPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtAftTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBefTax.Properties)).BeginInit();
            this.TpCOA.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo2.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt.Properties)).BeginInit();
            this.TpApproal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeLocalDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(860, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.MeeLocalDocNo);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(860, 74);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TcARDownpayment);
            this.panel3.Location = new System.Drawing.Point(0, 74);
            this.panel3.Size = new System.Drawing.Size(860, 399);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.TcARDownpayment, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd1.Header.Height = 19;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(860, 399);
            this.Grd1.TabIndex = 54;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Green;
            this.label13.Location = new System.Drawing.Point(182, 345);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(304, 14);
            this.label13.TabIndex = 57;
            this.label13.Text = "Note : Use Remark for Voucher Request\'s Description.";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOCurCode
            // 
            this.TxtSOCurCode.EnterMoveNextControl = true;
            this.TxtSOCurCode.Location = new System.Drawing.Point(183, 92);
            this.TxtSOCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOCurCode.Name = "TxtSOCurCode";
            this.TxtSOCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSOCurCode.Properties.MaxLength = 16;
            this.TxtSOCurCode.Properties.ReadOnly = true;
            this.TxtSOCurCode.Size = new System.Drawing.Size(67, 20);
            this.TxtSOCurCode.TabIndex = 34;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(47, 95);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(130, 14);
            this.label11.TabIndex = 33;
            this.label11.Text = "SO Contract\'s Amount";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOAmt
            // 
            this.TxtSOAmt.EnterMoveNextControl = true;
            this.TxtSOAmt.Location = new System.Drawing.Point(253, 92);
            this.TxtSOAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOAmt.Name = "TxtSOAmt";
            this.TxtSOAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtSOAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSOAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSOAmt.Properties.ReadOnly = true;
            this.TxtSOAmt.Size = new System.Drawing.Size(191, 20);
            this.TxtSOAmt.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(118, 53);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 14);
            this.label10.TabIndex = 29;
            this.label10.Text = "Customer";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(52, 32);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 14);
            this.label6.TabIndex = 27;
            this.label6.Text = "No. of Downpayment";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNoOfDownpayment
            // 
            this.TxtNoOfDownpayment.EnterMoveNextControl = true;
            this.TxtNoOfDownpayment.Location = new System.Drawing.Point(183, 29);
            this.TxtNoOfDownpayment.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoOfDownpayment.Name = "TxtNoOfDownpayment";
            this.TxtNoOfDownpayment.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtNoOfDownpayment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoOfDownpayment.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoOfDownpayment.Properties.Appearance.Options.UseFont = true;
            this.TxtNoOfDownpayment.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNoOfDownpayment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtNoOfDownpayment.Properties.ReadOnly = true;
            this.TxtNoOfDownpayment.Size = new System.Drawing.Size(67, 20);
            this.TxtNoOfDownpayment.TabIndex = 28;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(183, 323);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(500, 20);
            this.MeeRemark.TabIndex = 59;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(130, 326);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 58;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(76, 263);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 14);
            this.label7.TabIndex = 50;
            this.label7.Text = "Person In Charge";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePIC
            // 
            this.LuePIC.EnterMoveNextControl = true;
            this.LuePIC.Location = new System.Drawing.Point(183, 260);
            this.LuePIC.Margin = new System.Windows.Forms.Padding(5);
            this.LuePIC.Name = "LuePIC";
            this.LuePIC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.Appearance.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePIC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePIC.Properties.DropDownRows = 21;
            this.LuePIC.Properties.NullText = "[Empty]";
            this.LuePIC.Properties.PopupWidth = 350;
            this.LuePIC.Size = new System.Drawing.Size(350, 20);
            this.LuePIC.TabIndex = 51;
            this.LuePIC.ToolTip = "F4 : Show/hide list";
            this.LuePIC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePIC.EditValueChanged += new System.EventHandler(this.LuePIC_EditValueChanged);
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(183, 113);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 12;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 150;
            this.LueCurCode.Size = new System.Drawing.Size(191, 20);
            this.LueCurCode.TabIndex = 37;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(122, 116);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 14);
            this.label8.TabIndex = 36;
            this.label8.Text = "Currency";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(42, 158);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(135, 14);
            this.label9.TabIndex = 40;
            this.label9.Text = "Downpayment Amount";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(183, 155);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(191, 20);
            this.TxtAmt.TabIndex = 41;
            // 
            // BtnVoucherDocNo
            // 
            this.BtnVoucherDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherDocNo.Appearance.Options.UseBackColor = true;
            this.BtnVoucherDocNo.Appearance.Options.UseFont = true;
            this.BtnVoucherDocNo.Appearance.Options.UseForeColor = true;
            this.BtnVoucherDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnVoucherDocNo.Image")));
            this.BtnVoucherDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVoucherDocNo.Location = new System.Drawing.Point(372, 302);
            this.BtnVoucherDocNo.Name = "BtnVoucherDocNo";
            this.BtnVoucherDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnVoucherDocNo.TabIndex = 57;
            this.BtnVoucherDocNo.ToolTip = "Find Item";
            this.BtnVoucherDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherDocNo.ToolTipTitle = "Run System";
            this.BtnVoucherDocNo.Click += new System.EventHandler(this.BtnVoucherDocNo_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(115, 305);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 14);
            this.label4.TabIndex = 55;
            this.label4.Text = "Voucher#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherDocNo
            // 
            this.TxtVoucherDocNo.EnterMoveNextControl = true;
            this.TxtVoucherDocNo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TxtVoucherDocNo.Location = new System.Drawing.Point(183, 302);
            this.TxtVoucherDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo.Name = "TxtVoucherDocNo";
            this.TxtVoucherDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo.Properties.MaxLength = 30;
            this.TxtVoucherDocNo.Properties.ReadOnly = true;
            this.TxtVoucherDocNo.Size = new System.Drawing.Size(187, 20);
            this.TxtVoucherDocNo.TabIndex = 56;
            // 
            // BtnVoucherRequestDocNo
            // 
            this.BtnVoucherRequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherRequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherRequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherRequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseFont = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherRequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherRequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherRequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnVoucherRequestDocNo.Image")));
            this.BtnVoucherRequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVoucherRequestDocNo.Location = new System.Drawing.Point(372, 281);
            this.BtnVoucherRequestDocNo.Name = "BtnVoucherRequestDocNo";
            this.BtnVoucherRequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnVoucherRequestDocNo.TabIndex = 54;
            this.BtnVoucherRequestDocNo.ToolTip = "Find Item";
            this.BtnVoucherRequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherRequestDocNo.ToolTipTitle = "Run System";
            this.BtnVoucherRequestDocNo.Click += new System.EventHandler(this.BtnVoucherRequestDocNo_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(66, 284);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 14);
            this.label3.TabIndex = 52;
            this.label3.Text = "Voucher Request#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherRequestDocNo
            // 
            this.TxtVoucherRequestDocNo.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo.Location = new System.Drawing.Point(183, 281);
            this.TxtVoucherRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo.Name = "TxtVoucherRequestDocNo";
            this.TxtVoucherRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo.Properties.MaxLength = 30;
            this.TxtVoucherRequestDocNo.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo.Size = new System.Drawing.Size(187, 20);
            this.TxtVoucherRequestDocNo.TabIndex = 53;
            // 
            // BtnSODocNo2
            // 
            this.BtnSODocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSODocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSODocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSODocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSODocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSODocNo2.Appearance.Options.UseFont = true;
            this.BtnSODocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSODocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSODocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSODocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSODocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSODocNo2.Image")));
            this.BtnSODocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSODocNo2.Location = new System.Drawing.Point(402, 8);
            this.BtnSODocNo2.Name = "BtnSODocNo2";
            this.BtnSODocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnSODocNo2.TabIndex = 26;
            this.BtnSODocNo2.ToolTip = "Show SO Contract information";
            this.BtnSODocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSODocNo2.ToolTipTitle = "Run System";
            this.BtnSODocNo2.Click += new System.EventHandler(this.BtnSODocNo2_Click);
            // 
            // BtnSODocNo
            // 
            this.BtnSODocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSODocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSODocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSODocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSODocNo.Appearance.Options.UseBackColor = true;
            this.BtnSODocNo.Appearance.Options.UseFont = true;
            this.BtnSODocNo.Appearance.Options.UseForeColor = true;
            this.BtnSODocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSODocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSODocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSODocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSODocNo.Image")));
            this.BtnSODocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSODocNo.Location = new System.Drawing.Point(374, 8);
            this.BtnSODocNo.Name = "BtnSODocNo";
            this.BtnSODocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnSODocNo.TabIndex = 25;
            this.BtnSODocNo.ToolTip = "Find SO Contract";
            this.BtnSODocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSODocNo.ToolTipTitle = "Run System";
            this.BtnSODocNo.Click += new System.EventHandler(this.BtnSODocNo_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(94, 12);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 14);
            this.label12.TabIndex = 23;
            this.label12.Text = "SO Contract#";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSODocNo
            // 
            this.TxtSODocNo.EnterMoveNextControl = true;
            this.TxtSODocNo.Location = new System.Drawing.Point(183, 8);
            this.TxtSODocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSODocNo.Name = "TxtSODocNo";
            this.TxtSODocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSODocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSODocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSODocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSODocNo.Properties.MaxLength = 30;
            this.TxtSODocNo.Properties.ReadOnly = true;
            this.TxtSODocNo.Size = new System.Drawing.Size(187, 20);
            this.TxtSODocNo.TabIndex = 24;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(146, 6);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(121, 20);
            this.TxtStatus.TabIndex = 18;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(97, 10);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 14);
            this.label17.TabIndex = 17;
            this.label17.Text = "Status";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(145, 49);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 21;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(111, 28);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(121, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(75, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(111, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(224, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(35, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(183, 50);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 25;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 350;
            this.LueCtCode.Size = new System.Drawing.Size(350, 20);
            this.LueCtCode.TabIndex = 30;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.MeeCancelReason);
            this.panel4.Controls.Add(this.TxtStatus);
            this.panel4.Controls.Add(this.ChkCancelInd);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(343, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(517, 74);
            this.panel4.TabIndex = 16;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(6, 30);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(133, 14);
            this.label14.TabIndex = 19;
            this.label14.Text = "Reason for Cancellation";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(146, 27);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(370, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(364, 20);
            this.MeeCancelReason.TabIndex = 20;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // TxtGiroNo
            // 
            this.TxtGiroNo.EnterMoveNextControl = true;
            this.TxtGiroNo.Location = new System.Drawing.Point(183, 218);
            this.TxtGiroNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGiroNo.Name = "TxtGiroNo";
            this.TxtGiroNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGiroNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGiroNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGiroNo.Properties.Appearance.Options.UseFont = true;
            this.TxtGiroNo.Properties.MaxLength = 80;
            this.TxtGiroNo.Size = new System.Drawing.Size(246, 20);
            this.TxtGiroNo.TabIndex = 47;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(61, 222);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(116, 14);
            this.label15.TabIndex = 46;
            this.label15.Text = "Giro Bilyet / Cheque";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(183, 239);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.MaxLength = 8;
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(121, 20);
            this.DteDueDt.TabIndex = 49;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(118, 243);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 14);
            this.label16.TabIndex = 48;
            this.label16.Text = "Due Date";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(109, 200);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 14);
            this.label18.TabIndex = 44;
            this.label18.Text = "Bank Name";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(183, 197);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 20;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 350;
            this.LueBankCode.Size = new System.Drawing.Size(350, 20);
            this.LueBankCode.TabIndex = 45;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(90, 179);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(87, 14);
            this.label21.TabIndex = 42;
            this.label21.Text = "Payment Type";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePaymentType
            // 
            this.LuePaymentType.EnterMoveNextControl = true;
            this.LuePaymentType.Location = new System.Drawing.Point(183, 176);
            this.LuePaymentType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaymentType.Name = "LuePaymentType";
            this.LuePaymentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.Appearance.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaymentType.Properties.DropDownRows = 20;
            this.LuePaymentType.Properties.NullText = "[Empty]";
            this.LuePaymentType.Properties.PopupWidth = 350;
            this.LuePaymentType.Size = new System.Drawing.Size(350, 20);
            this.LuePaymentType.TabIndex = 43;
            this.LuePaymentType.ToolTip = "F4 : Show/hide list";
            this.LuePaymentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaymentType.EditValueChanged += new System.EventHandler(this.LuePaymentType_EditValueChanged);
            // 
            // TcARDownpayment
            // 
            this.TcARDownpayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcARDownpayment.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcARDownpayment.Location = new System.Drawing.Point(0, 0);
            this.TcARDownpayment.Name = "TcARDownpayment";
            this.TcARDownpayment.SelectedTabPage = this.TpGeneral;
            this.TcARDownpayment.Size = new System.Drawing.Size(860, 399);
            this.TcARDownpayment.TabIndex = 22;
            this.TcARDownpayment.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpGeneral,
            this.TpTax,
            this.TpCOA,
            this.TpApproal});
            // 
            // TpGeneral
            // 
            this.TpGeneral.Appearance.Header.Options.UseTextOptions = true;
            this.TpGeneral.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpGeneral.Controls.Add(this.TxtPtName);
            this.TpGeneral.Controls.Add(this.label20);
            this.TpGeneral.Controls.Add(this.label22);
            this.TpGeneral.Controls.Add(this.TxtOtherAmt);
            this.TpGeneral.Controls.Add(this.TxtGiroNo);
            this.TpGeneral.Controls.Add(this.BtnVoucherRequestDocNo);
            this.TpGeneral.Controls.Add(this.TxtSODocNo);
            this.TpGeneral.Controls.Add(this.label15);
            this.TpGeneral.Controls.Add(this.BtnVoucherDocNo);
            this.TpGeneral.Controls.Add(this.label12);
            this.TpGeneral.Controls.Add(this.label13);
            this.TpGeneral.Controls.Add(this.BtnSODocNo);
            this.TpGeneral.Controls.Add(this.DteDueDt);
            this.TpGeneral.Controls.Add(this.LueCtCode);
            this.TpGeneral.Controls.Add(this.label16);
            this.TpGeneral.Controls.Add(this.BtnSODocNo2);
            this.TpGeneral.Controls.Add(this.label18);
            this.TpGeneral.Controls.Add(this.TxtSOCurCode);
            this.TpGeneral.Controls.Add(this.LueBankCode);
            this.TpGeneral.Controls.Add(this.TxtAmt);
            this.TpGeneral.Controls.Add(this.label21);
            this.TpGeneral.Controls.Add(this.label11);
            this.TpGeneral.Controls.Add(this.MeeRemark);
            this.TpGeneral.Controls.Add(this.label9);
            this.TpGeneral.Controls.Add(this.label7);
            this.TpGeneral.Controls.Add(this.TxtSOAmt);
            this.TpGeneral.Controls.Add(this.LuePaymentType);
            this.TpGeneral.Controls.Add(this.LuePIC);
            this.TpGeneral.Controls.Add(this.label8);
            this.TpGeneral.Controls.Add(this.label5);
            this.TpGeneral.Controls.Add(this.label10);
            this.TpGeneral.Controls.Add(this.TxtVoucherRequestDocNo);
            this.TpGeneral.Controls.Add(this.LueCurCode);
            this.TpGeneral.Controls.Add(this.label3);
            this.TpGeneral.Controls.Add(this.label6);
            this.TpGeneral.Controls.Add(this.TxtNoOfDownpayment);
            this.TpGeneral.Controls.Add(this.TxtVoucherDocNo);
            this.TpGeneral.Controls.Add(this.label4);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Size = new System.Drawing.Size(854, 371);
            this.TpGeneral.Text = "General";
            // 
            // TxtPtName
            // 
            this.TxtPtName.EnterMoveNextControl = true;
            this.TxtPtName.Location = new System.Drawing.Point(183, 71);
            this.TxtPtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPtName.Name = "TxtPtName";
            this.TxtPtName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPtName.Properties.Appearance.Options.UseFont = true;
            this.TxtPtName.Properties.MaxLength = 30;
            this.TxtPtName.Properties.ReadOnly = true;
            this.TxtPtName.Size = new System.Drawing.Size(350, 20);
            this.TxtPtName.TabIndex = 32;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(74, 75);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(103, 14);
            this.label20.TabIndex = 31;
            this.label20.Text = "Term of Payment";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(6, 137);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(171, 14);
            this.label22.TabIndex = 38;
            this.label22.Text = "Other Downpayment Amount";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOtherAmt
            // 
            this.TxtOtherAmt.EnterMoveNextControl = true;
            this.TxtOtherAmt.Location = new System.Drawing.Point(183, 134);
            this.TxtOtherAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOtherAmt.Name = "TxtOtherAmt";
            this.TxtOtherAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtOtherAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOtherAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOtherAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtOtherAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOtherAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOtherAmt.Properties.ReadOnly = true;
            this.TxtOtherAmt.Size = new System.Drawing.Size(191, 20);
            this.TxtOtherAmt.TabIndex = 39;
            // 
            // TpTax
            // 
            this.TpTax.Appearance.Header.Options.UseTextOptions = true;
            this.TpTax.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpTax.Controls.Add(this.label44);
            this.TpTax.Controls.Add(this.DteTaxInvoiceDt3);
            this.TpTax.Controls.Add(this.label45);
            this.TpTax.Controls.Add(this.TxtTaxAmt3);
            this.TpTax.Controls.Add(this.label46);
            this.TpTax.Controls.Add(this.LueTaxCode3);
            this.TpTax.Controls.Add(this.TxtTaxInvoiceNo3);
            this.TpTax.Controls.Add(this.label47);
            this.TpTax.Controls.Add(this.label40);
            this.TpTax.Controls.Add(this.DteTaxInvoiceDt2);
            this.TpTax.Controls.Add(this.label41);
            this.TpTax.Controls.Add(this.TxtTaxAmt2);
            this.TpTax.Controls.Add(this.label42);
            this.TpTax.Controls.Add(this.LueTaxCode2);
            this.TpTax.Controls.Add(this.TxtTaxInvoiceNo2);
            this.TpTax.Controls.Add(this.label43);
            this.TpTax.Controls.Add(this.label39);
            this.TpTax.Controls.Add(this.TxtDownpaymentPercentage);
            this.TpTax.Controls.Add(this.label38);
            this.TpTax.Controls.Add(this.label36);
            this.TpTax.Controls.Add(this.TxtAmtAftTax);
            this.TpTax.Controls.Add(this.label32);
            this.TpTax.Controls.Add(this.DteTaxInvoiceDt);
            this.TpTax.Controls.Add(this.label33);
            this.TpTax.Controls.Add(this.TxtTaxAmt);
            this.TpTax.Controls.Add(this.label34);
            this.TpTax.Controls.Add(this.LueTaxCode);
            this.TpTax.Controls.Add(this.TxtTaxInvoiceNo);
            this.TpTax.Controls.Add(this.label35);
            this.TpTax.Controls.Add(this.TxtAmtBefTax);
            this.TpTax.Controls.Add(this.label23);
            this.TpTax.Name = "TpTax";
            this.TpTax.Size = new System.Drawing.Size(766, 371);
            this.TpTax.Text = "Tax";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(115, 238);
            this.label44.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(75, 14);
            this.label44.TabIndex = 60;
            this.label44.Text = "Tax Amount";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt3
            // 
            this.DteTaxInvoiceDt3.EditValue = null;
            this.DteTaxInvoiceDt3.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt3.Location = new System.Drawing.Point(491, 212);
            this.DteTaxInvoiceDt3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt3.Name = "DteTaxInvoiceDt3";
            this.DteTaxInvoiceDt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt3.Size = new System.Drawing.Size(115, 20);
            this.DteTaxInvoiceDt3.TabIndex = 59;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(452, 215);
            this.label45.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(33, 14);
            this.label45.TabIndex = 58;
            this.label45.Text = "Date";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt3
            // 
            this.TxtTaxAmt3.EnterMoveNextControl = true;
            this.TxtTaxAmt3.Location = new System.Drawing.Point(196, 235);
            this.TxtTaxAmt3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt3.Name = "TxtTaxAmt3";
            this.TxtTaxAmt3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt3.Properties.ReadOnly = true;
            this.TxtTaxAmt3.Size = new System.Drawing.Size(188, 20);
            this.TxtTaxAmt3.TabIndex = 61;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(164, 195);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(27, 14);
            this.label46.TabIndex = 55;
            this.label46.Text = "Tax";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode3
            // 
            this.LueTaxCode3.EnterMoveNextControl = true;
            this.LueTaxCode3.Location = new System.Drawing.Point(196, 191);
            this.LueTaxCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode3.Name = "LueTaxCode3";
            this.LueTaxCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode3.Properties.DropDownRows = 25;
            this.LueTaxCode3.Properties.NullText = "[Empty]";
            this.LueTaxCode3.Properties.PopupWidth = 200;
            this.LueTaxCode3.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode3.TabIndex = 56;
            this.LueTaxCode3.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode3.EditValueChanged += new System.EventHandler(this.LueTaxCode3_EditValueChanged);
            // 
            // TxtTaxInvoiceNo3
            // 
            this.TxtTaxInvoiceNo3.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo3.Location = new System.Drawing.Point(196, 213);
            this.TxtTaxInvoiceNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo3.Name = "TxtTaxInvoiceNo3";
            this.TxtTaxInvoiceNo3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo3.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo3.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo3.TabIndex = 57;
            this.TxtTaxInvoiceNo3.Validated += new System.EventHandler(this.TxtTaxInvoiceNo3_Validated);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(112, 216);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(79, 14);
            this.label47.TabIndex = 57;
            this.label47.Text = "Tax Invoice#";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(116, 174);
            this.label40.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(75, 14);
            this.label40.TabIndex = 53;
            this.label40.Text = "Tax Amount";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt2
            // 
            this.DteTaxInvoiceDt2.EditValue = null;
            this.DteTaxInvoiceDt2.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt2.Location = new System.Drawing.Point(491, 146);
            this.DteTaxInvoiceDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt2.Name = "DteTaxInvoiceDt2";
            this.DteTaxInvoiceDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt2.Size = new System.Drawing.Size(115, 20);
            this.DteTaxInvoiceDt2.TabIndex = 52;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(452, 150);
            this.label41.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(33, 14);
            this.label41.TabIndex = 51;
            this.label41.Text = "Date";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt2
            // 
            this.TxtTaxAmt2.EnterMoveNextControl = true;
            this.TxtTaxAmt2.Location = new System.Drawing.Point(196, 170);
            this.TxtTaxAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt2.Name = "TxtTaxAmt2";
            this.TxtTaxAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt2.Properties.ReadOnly = true;
            this.TxtTaxAmt2.Size = new System.Drawing.Size(188, 20);
            this.TxtTaxAmt2.TabIndex = 54;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(164, 130);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(27, 14);
            this.label42.TabIndex = 37;
            this.label42.Text = "Tax";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode2
            // 
            this.LueTaxCode2.EnterMoveNextControl = true;
            this.LueTaxCode2.Location = new System.Drawing.Point(196, 126);
            this.LueTaxCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode2.Name = "LueTaxCode2";
            this.LueTaxCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode2.Properties.DropDownRows = 25;
            this.LueTaxCode2.Properties.NullText = "[Empty]";
            this.LueTaxCode2.Properties.PopupWidth = 200;
            this.LueTaxCode2.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode2.TabIndex = 38;
            this.LueTaxCode2.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode2.EditValueChanged += new System.EventHandler(this.LueTaxCode2_EditValueChanged);
            // 
            // TxtTaxInvoiceNo2
            // 
            this.TxtTaxInvoiceNo2.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo2.Location = new System.Drawing.Point(196, 148);
            this.TxtTaxInvoiceNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo2.Name = "TxtTaxInvoiceNo2";
            this.TxtTaxInvoiceNo2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo2.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo2.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo2.TabIndex = 50;
            this.TxtTaxInvoiceNo2.Validated += new System.EventHandler(this.TxtTaxInvoiceNo2_Validated);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(112, 152);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(79, 14);
            this.label43.TabIndex = 39;
            this.label43.Text = "Tax Invoice#";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(270, 19);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(19, 14);
            this.label39.TabIndex = 25;
            this.label39.Text = "%";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDownpaymentPercentage
            // 
            this.TxtDownpaymentPercentage.EnterMoveNextControl = true;
            this.TxtDownpaymentPercentage.Location = new System.Drawing.Point(196, 17);
            this.TxtDownpaymentPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDownpaymentPercentage.Name = "TxtDownpaymentPercentage";
            this.TxtDownpaymentPercentage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDownpaymentPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDownpaymentPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDownpaymentPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtDownpaymentPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDownpaymentPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDownpaymentPercentage.Size = new System.Drawing.Size(66, 20);
            this.TxtDownpaymentPercentage.TabIndex = 24;
            this.TxtDownpaymentPercentage.Validated += new System.EventHandler(this.TxtDownpaymentPercentage_Validated);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(79, 20);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(113, 14);
            this.label38.TabIndex = 23;
            this.label38.Text = "Downpayment (%)";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(46, 259);
            this.label36.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(143, 14);
            this.label36.TabIndex = 62;
            this.label36.Text = "Downpayment After Tax";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmtAftTax
            // 
            this.TxtAmtAftTax.EnterMoveNextControl = true;
            this.TxtAmtAftTax.Location = new System.Drawing.Point(196, 256);
            this.TxtAmtAftTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmtAftTax.Name = "TxtAmtAftTax";
            this.TxtAmtAftTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmtAftTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmtAftTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmtAftTax.Properties.Appearance.Options.UseFont = true;
            this.TxtAmtAftTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmtAftTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmtAftTax.Properties.ReadOnly = true;
            this.TxtAmtAftTax.Size = new System.Drawing.Size(188, 20);
            this.TxtAmtAftTax.TabIndex = 63;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(117, 109);
            this.label32.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(75, 14);
            this.label32.TabIndex = 35;
            this.label32.Text = "Tax Amount";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt
            // 
            this.DteTaxInvoiceDt.EditValue = null;
            this.DteTaxInvoiceDt.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt.Location = new System.Drawing.Point(491, 81);
            this.DteTaxInvoiceDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt.Name = "DteTaxInvoiceDt";
            this.DteTaxInvoiceDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt.Size = new System.Drawing.Size(115, 20);
            this.DteTaxInvoiceDt.TabIndex = 34;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(452, 85);
            this.label33.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(33, 14);
            this.label33.TabIndex = 32;
            this.label33.Text = "Date";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt
            // 
            this.TxtTaxAmt.EnterMoveNextControl = true;
            this.TxtTaxAmt.Location = new System.Drawing.Point(196, 105);
            this.TxtTaxAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt.Name = "TxtTaxAmt";
            this.TxtTaxAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt.Properties.ReadOnly = true;
            this.TxtTaxAmt.Size = new System.Drawing.Size(188, 20);
            this.TxtTaxAmt.TabIndex = 36;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(165, 65);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(27, 14);
            this.label34.TabIndex = 28;
            this.label34.Text = "Tax";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode
            // 
            this.LueTaxCode.EnterMoveNextControl = true;
            this.LueTaxCode.Location = new System.Drawing.Point(196, 61);
            this.LueTaxCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode.Name = "LueTaxCode";
            this.LueTaxCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode.Properties.DropDownRows = 25;
            this.LueTaxCode.Properties.NullText = "[Empty]";
            this.LueTaxCode.Properties.PopupWidth = 200;
            this.LueTaxCode.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode.TabIndex = 29;
            this.LueTaxCode.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode.EditValueChanged += new System.EventHandler(this.LueTaxCode_EditValueChanged);
            // 
            // TxtTaxInvoiceNo
            // 
            this.TxtTaxInvoiceNo.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo.Location = new System.Drawing.Point(196, 83);
            this.TxtTaxInvoiceNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo.Name = "TxtTaxInvoiceNo";
            this.TxtTaxInvoiceNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo.TabIndex = 31;
            this.TxtTaxInvoiceNo.Validated += new System.EventHandler(this.TxtTaxInvoiceNo_Validated);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(113, 87);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(79, 14);
            this.label35.TabIndex = 30;
            this.label35.Text = "Tax Invoice#";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmtBefTax
            // 
            this.TxtAmtBefTax.EnterMoveNextControl = true;
            this.TxtAmtBefTax.Location = new System.Drawing.Point(196, 39);
            this.TxtAmtBefTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmtBefTax.Name = "TxtAmtBefTax";
            this.TxtAmtBefTax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAmtBefTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmtBefTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmtBefTax.Properties.Appearance.Options.UseFont = true;
            this.TxtAmtBefTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmtBefTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmtBefTax.Size = new System.Drawing.Size(188, 20);
            this.TxtAmtBefTax.TabIndex = 27;
            this.TxtAmtBefTax.Validated += new System.EventHandler(this.TxtAmtBefTax_Validated);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(41, 42);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(151, 14);
            this.label23.TabIndex = 26;
            this.label23.Text = "Downpayment Before Tax";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpCOA
            // 
            this.TpCOA.Appearance.Header.Options.UseTextOptions = true;
            this.TpCOA.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCOA.Controls.Add(this.panel5);
            this.TpCOA.Name = "TpCOA";
            this.TpCOA.Size = new System.Drawing.Size(854, 371);
            this.TpCOA.Text = "COA";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.Grd3);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(854, 371);
            this.panel5.TabIndex = 22;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(854, 320);
            this.Grd3.TabIndex = 22;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.TxtVoucherRequestDocNo2);
            this.panel7.Controls.Add(this.label19);
            this.panel7.Controls.Add(this.TxtVoucherDocNo2);
            this.panel7.Controls.Add(this.label26);
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 320);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(854, 51);
            this.panel7.TabIndex = 24;
            // 
            // TxtVoucherRequestDocNo2
            // 
            this.TxtVoucherRequestDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo2.Location = new System.Drawing.Point(124, 5);
            this.TxtVoucherRequestDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo2.Name = "TxtVoucherRequestDocNo2";
            this.TxtVoucherRequestDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo2.Properties.MaxLength = 16;
            this.TxtVoucherRequestDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo2.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherRequestDocNo2.TabIndex = 26;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(57, 29);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 14);
            this.label19.TabIndex = 27;
            this.label19.Text = "Voucher#";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherDocNo2
            // 
            this.TxtVoucherDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherDocNo2.Location = new System.Drawing.Point(124, 26);
            this.TxtVoucherDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo2.Name = "TxtVoucherDocNo2";
            this.TxtVoucherDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo2.Properties.MaxLength = 16;
            this.TxtVoucherDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherDocNo2.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherDocNo2.TabIndex = 28;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(8, 8);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(111, 14);
            this.label26.TabIndex = 25;
            this.label26.Text = "Voucher Request#";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.TxtCOAAmt);
            this.panel8.Controls.Add(this.label28);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(624, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(230, 51);
            this.panel8.TabIndex = 29;
            // 
            // TxtCOAAmt
            // 
            this.TxtCOAAmt.EnterMoveNextControl = true;
            this.TxtCOAAmt.Location = new System.Drawing.Point(63, 5);
            this.TxtCOAAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCOAAmt.Name = "TxtCOAAmt";
            this.TxtCOAAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCOAAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCOAAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCOAAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtCOAAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCOAAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCOAAmt.Properties.ReadOnly = true;
            this.TxtCOAAmt.Size = new System.Drawing.Size(160, 20);
            this.TxtCOAAmt.TabIndex = 31;
            this.TxtCOAAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(6, 8);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 14);
            this.label28.TabIndex = 30;
            this.label28.Text = "Amount";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpApproal
            // 
            this.TpApproal.Appearance.Header.Options.UseTextOptions = true;
            this.TpApproal.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpApproal.Controls.Add(this.Grd2);
            this.TpApproal.Name = "TpApproal";
            this.TpApproal.Size = new System.Drawing.Size(766, 371);
            this.TpApproal.Text = "Approval Information";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 371);
            this.Grd2.TabIndex = 23;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(4, 52);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(104, 14);
            this.label24.TabIndex = 14;
            this.label24.Text = "Local Document#";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeLocalDocNo
            // 
            this.MeeLocalDocNo.EnterMoveNextControl = true;
            this.MeeLocalDocNo.Location = new System.Drawing.Point(111, 50);
            this.MeeLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.MeeLocalDocNo.Name = "MeeLocalDocNo";
            this.MeeLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.MeeLocalDocNo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLocalDocNo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeLocalDocNo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLocalDocNo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeLocalDocNo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLocalDocNo.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeLocalDocNo.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLocalDocNo.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeLocalDocNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeLocalDocNo.Properties.MaxLength = 400;
            this.MeeLocalDocNo.Properties.PopupFormSize = new System.Drawing.Size(370, 20);
            this.MeeLocalDocNo.Properties.ShowIcon = false;
            this.MeeLocalDocNo.Size = new System.Drawing.Size(224, 20);
            this.MeeLocalDocNo.TabIndex = 22;
            this.MeeLocalDocNo.ToolTip = "F4 : Show/hide text";
            this.MeeLocalDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeLocalDocNo.ToolTipTitle = "Run System";
            // 
            // FrmARDownpayment2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 473);
            this.Name = "FrmARDownpayment2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoOfDownpayment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSODocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcARDownpayment)).EndInit();
            this.TcARDownpayment.ResumeLayout(false);
            this.TpGeneral.ResumeLayout(false);
            this.TpGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOtherAmt.Properties)).EndInit();
            this.TpTax.ResumeLayout(false);
            this.TpTax.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDownpaymentPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtAftTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBefTax.Properties)).EndInit();
            this.TpCOA.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo2.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt.Properties)).EndInit();
            this.TpApproal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeLocalDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtSOCurCode;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtSOAmt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtNoOfDownpayment;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LuePIC;
        internal DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherDocNo;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherRequestDocNo;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnSODocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnSODocNo;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtSODocNo;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.LookUpEdit LueCtCode;
        protected System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtGiroNo;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LuePaymentType;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraTab.XtraTabControl TcARDownpayment;
        private DevExpress.XtraTab.XtraTabPage TpCOA;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabPage TpApproal;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.Panel panel7;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo2;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel8;
        internal DevExpress.XtraEditors.TextEdit TxtCOAAmt;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraTab.XtraTabPage TpGeneral;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtOtherAmt;
        internal DevExpress.XtraEditors.TextEdit TxtPtName;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraTab.XtraTabPage TpTax;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt3;
        private System.Windows.Forms.Label label45;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt3;
        private System.Windows.Forms.Label label46;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode3;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo3;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt2;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt2;
        private System.Windows.Forms.Label label42;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode2;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo2;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtDownpaymentPercentage;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label36;
        internal DevExpress.XtraEditors.TextEdit TxtAmtAftTax;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt;
        private System.Windows.Forms.Label label34;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.TextEdit TxtAmtBefTax;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraEditors.MemoExEdit MeeLocalDocNo;
    }
}