﻿#region Update
/*
05/09/2018 [HAR] waktu insert bisa milih all type trainer, waktu edit hanya ambil dari master training
*/
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTrainingDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTraining mFrmParent;
        private string mSQL = string.Empty; 
        private int mRowIndex = 0;
        string TrainerCode = string.Empty;
        string TrainerName = string.Empty;
        string mTrainingType = string.Empty;
        string mTrainingCode = string.Empty;
        string mTypeDoc = string.Empty;

        #endregion

        #region Constructor

        public FrmTrainingDlg(FrmTraining FrmParent, string TrainingType, int RowIndex, string TrainingCode, string TypeDoc)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mTrainingType = TrainingType;
            mRowIndex = RowIndex;
            mTrainingCode = TrainingCode;
            mTypeDoc = TypeDoc;
        }

        #endregion


        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Trainer";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "",
                        "Trainer Code",
                        "Trainer",
                        "Training",
                        "Type"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 200, 250, 120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Select B.trainerCode, B.trainername, C.TrainingName, if(B.trainerType = 'I', 'Internal', 'External') trainerType ");
            SQL.AppendLine("From TblTrainer B  ");
            SQL.AppendLine("Inner Join Tbltraining C On B.trainingCode = C.trainingCode ");
            SQL.AppendLine("Where 0=0 ");
            if (mTrainingType.Length > 0 && mTypeDoc == "0")
                SQL.AppendLine("And B.TrainerType = @TrainingType ");
            if (mTrainingCode.Length > 0 && mTypeDoc == "1")
                SQL.AppendLine("And C.trainingCode = @TrainingCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Trainingtype", mTrainingType);
                Sm.CmParam<String>(ref cm, "@TrainingCode", mTrainingCode);

                Sm.FilterStr(ref Filter, ref cm, TxtTrainerCode.Text, new string[] { "B.trainerCode", "B.TrainerName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.TrainerName Desc;",
                        new string[] 
                        { 
                             //0
                             "TrainerCode", 
                             
                             //1-2
                             "TrainerName", "TrainingName", "trainerType"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (TrainerCode.Length == 0)
                        {
                            TrainerCode = string.Concat(TrainerCode, Sm.GetGrdStr(Grd1, Row, 2), "#");
                            TrainerName = string.Concat(TrainerName, Sm.GetGrdStr(Grd1, Row, 3));
                        }
                        else
                        {
                            TrainerCode = string.Concat(TrainerCode, Sm.GetGrdStr(Grd1, Row, 2), "#");
                            TrainerName = string.Concat(TrainerName, ", ", Sm.GetGrdStr(Grd1, Row, 3));
                        }
                    }
                }
                mFrmParent.Grd1.Cells[mRowIndex, 4].Value = TrainerCode;
                mFrmParent.Grd1.Cells[mRowIndex, 5].Value = TrainerName;
                TrainerCode = "";
                TrainerName = "";
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkTrainerCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Trainer");
        }
        private void TxtTrainerCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

      
        #endregion

       
    }
}
