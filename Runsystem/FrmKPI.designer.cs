﻿namespace RunSystem
{
    partial class FrmKPI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmKPI));
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkActiveInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtKPIName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtPICCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtPICName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueParentKPI = new DevExpress.XtraEditors.LookUpEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LueOption = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnPIC = new DevExpress.XtraEditors.SimpleButton();
            this.LueType = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LuePosCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueJobDesc = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSourceKPI = new DevExpress.XtraEditors.TextEdit();
            this.BtnSourceKPI = new DevExpress.XtraEditors.SimpleButton();
            this.label8 = new System.Windows.Forms.Label();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtTotalBobot = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.LueMaxMin = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKPIName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParentKPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJobDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSourceKPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBobot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaxMin.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(851, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.LblSiteCode);
            this.panel2.Controls.Add(this.LueYr);
            this.panel2.Controls.Add(this.LueSiteCode);
            this.panel2.Controls.Add(this.TxtSourceKPI);
            this.panel2.Controls.Add(this.BtnSourceKPI);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtKPIName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.ChkActiveInd);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Size = new System.Drawing.Size(851, 162);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LueMaxMin);
            this.panel3.Controls.Add(this.LueJobDesc);
            this.panel3.Controls.Add(this.LueType);
            this.panel3.Controls.Add(this.LueOption);
            this.panel3.Location = new System.Drawing.Point(0, 162);
            this.panel3.Size = new System.Drawing.Size(851, 311);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.LueOption, 0);
            this.panel3.Controls.SetChildIndex(this.LueType, 0);
            this.panel3.Controls.SetChildIndex(this.LueJobDesc, 0);
            this.panel3.Controls.SetChildIndex(this.LueMaxMin, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(851, 311);
            this.Grd1.TabIndex = 32;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(120, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(120, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(21, 30);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "Document Date";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(120, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(270, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(4, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 14);
            this.label5.TabIndex = 10;
            this.label5.Text = "Document Number";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActiveInd
            // 
            this.ChkActiveInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActiveInd.Location = new System.Drawing.Point(392, 6);
            this.ChkActiveInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActiveInd.Name = "ChkActiveInd";
            this.ChkActiveInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActiveInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActiveInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActiveInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActiveInd.Properties.Caption = "Active";
            this.ChkActiveInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActiveInd.ShowToolTips = false;
            this.ChkActiveInd.Size = new System.Drawing.Size(58, 22);
            this.ChkActiveInd.TabIndex = 12;
            // 
            // TxtKPIName
            // 
            this.TxtKPIName.EnterMoveNextControl = true;
            this.TxtKPIName.Location = new System.Drawing.Point(120, 132);
            this.TxtKPIName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKPIName.Name = "TxtKPIName";
            this.TxtKPIName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKPIName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKPIName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKPIName.Properties.Appearance.Options.UseFont = true;
            this.TxtKPIName.Properties.MaxLength = 250;
            this.TxtKPIName.Size = new System.Drawing.Size(270, 20);
            this.TxtKPIName.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(55, 135);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 14);
            this.label2.TabIndex = 22;
            this.label2.Text = "KPI Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(40, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 14);
            this.label1.TabIndex = 24;
            this.label1.Text = "PIC Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPICCode
            // 
            this.TxtPICCode.EnterMoveNextControl = true;
            this.TxtPICCode.Location = new System.Drawing.Point(102, 28);
            this.TxtPICCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICCode.Name = "TxtPICCode";
            this.TxtPICCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPICCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPICCode.Properties.MaxLength = 250;
            this.TxtPICCode.Size = new System.Drawing.Size(266, 20);
            this.TxtPICCode.TabIndex = 25;
            // 
            // TxtPICName
            // 
            this.TxtPICName.EnterMoveNextControl = true;
            this.TxtPICName.Location = new System.Drawing.Point(102, 49);
            this.TxtPICName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICName.Name = "TxtPICName";
            this.TxtPICName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPICName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICName.Properties.Appearance.Options.UseFont = true;
            this.TxtPICName.Properties.MaxLength = 250;
            this.TxtPICName.Size = new System.Drawing.Size(266, 20);
            this.TxtPICName.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(37, 52);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 14);
            this.label3.TabIndex = 27;
            this.label3.Text = "PIC Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(32, 73);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 14);
            this.label6.TabIndex = 29;
            this.label6.Text = "Parent KPI";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueParentKPI
            // 
            this.LueParentKPI.EnterMoveNextControl = true;
            this.LueParentKPI.Location = new System.Drawing.Point(102, 70);
            this.LueParentKPI.Margin = new System.Windows.Forms.Padding(5);
            this.LueParentKPI.Name = "LueParentKPI";
            this.LueParentKPI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParentKPI.Properties.Appearance.Options.UseFont = true;
            this.LueParentKPI.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParentKPI.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParentKPI.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParentKPI.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParentKPI.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParentKPI.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParentKPI.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParentKPI.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParentKPI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParentKPI.Properties.DropDownRows = 20;
            this.LueParentKPI.Properties.NullText = "[Empty]";
            this.LueParentKPI.Properties.PopupWidth = 500;
            this.LueParentKPI.Size = new System.Drawing.Size(266, 20);
            this.LueParentKPI.TabIndex = 30;
            this.LueParentKPI.ToolTip = "F4 : Show/hide list";
            this.LueParentKPI.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParentKPI.EditValueChanged += new System.EventHandler(this.LueParentKPI_EditValueChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(50, 135);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 14);
            this.label28.TabIndex = 33;
            this.label28.Text = "Remark";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(102, 132);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 700;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(266, 20);
            this.MeeRemark.TabIndex = 35;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // LueOption
            // 
            this.LueOption.EnterMoveNextControl = true;
            this.LueOption.Location = new System.Drawing.Point(243, 146);
            this.LueOption.Margin = new System.Windows.Forms.Padding(5);
            this.LueOption.Name = "LueOption";
            this.LueOption.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.Appearance.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueOption.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueOption.Properties.DropDownRows = 20;
            this.LueOption.Properties.NullText = "[Empty]";
            this.LueOption.Properties.PopupWidth = 500;
            this.LueOption.Size = new System.Drawing.Size(286, 20);
            this.LueOption.TabIndex = 31;
            this.LueOption.ToolTip = "F4 : Show/hide list";
            this.LueOption.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueOption.EditValueChanged += new System.EventHandler(this.LueOption_EditValueChanged);
            this.LueOption.Leave += new System.EventHandler(this.LueOption_Leave);
            this.LueOption.Validated += new System.EventHandler(this.LueOption_Validated);
            // 
            // BtnPIC
            // 
            this.BtnPIC.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPIC.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPIC.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPIC.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPIC.Appearance.Options.UseBackColor = true;
            this.BtnPIC.Appearance.Options.UseFont = true;
            this.BtnPIC.Appearance.Options.UseForeColor = true;
            this.BtnPIC.Appearance.Options.UseTextOptions = true;
            this.BtnPIC.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPIC.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPIC.Image = ((System.Drawing.Image)(resources.GetObject("BtnPIC.Image")));
            this.BtnPIC.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPIC.Location = new System.Drawing.Point(369, 29);
            this.BtnPIC.Name = "BtnPIC";
            this.BtnPIC.Size = new System.Drawing.Size(24, 21);
            this.BtnPIC.TabIndex = 26;
            this.BtnPIC.ToolTip = "Add PIC";
            this.BtnPIC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPIC.ToolTipTitle = "Run System";
            this.BtnPIC.Click += new System.EventHandler(this.BtnPIC_Click);
            // 
            // LueType
            // 
            this.LueType.EnterMoveNextControl = true;
            this.LueType.Location = new System.Drawing.Point(355, 192);
            this.LueType.Margin = new System.Windows.Forms.Padding(5);
            this.LueType.Name = "LueType";
            this.LueType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.Appearance.Options.UseFont = true;
            this.LueType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueType.Properties.DropDownRows = 20;
            this.LueType.Properties.NullText = "[Empty]";
            this.LueType.Properties.PopupWidth = 500;
            this.LueType.Size = new System.Drawing.Size(286, 20);
            this.LueType.TabIndex = 32;
            this.LueType.ToolTip = "F4 : Show/hide list";
            this.LueType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueType.EditValueChanged += new System.EventHandler(this.LueType_EditValueChanged);
            this.LueType.Leave += new System.EventHandler(this.LueType_Leave);
            this.LueType.Validated += new System.EventHandler(this.LueType_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(2, 93);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 14);
            this.label7.TabIndex = 31;
            this.label7.Text = "Responsibility By";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePosCode
            // 
            this.LuePosCode.EnterMoveNextControl = true;
            this.LuePosCode.Location = new System.Drawing.Point(102, 91);
            this.LuePosCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosCode.Name = "LuePosCode";
            this.LuePosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.Appearance.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosCode.Properties.DropDownRows = 20;
            this.LuePosCode.Properties.NullText = "[Empty]";
            this.LuePosCode.Properties.PopupWidth = 500;
            this.LuePosCode.Size = new System.Drawing.Size(266, 20);
            this.LuePosCode.TabIndex = 32;
            this.LuePosCode.ToolTip = "F4 : Show/hide list";
            this.LuePosCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosCode.EditValueChanged += new System.EventHandler(this.LuePosCode_EditValueChanged);
            // 
            // LueJobDesc
            // 
            this.LueJobDesc.EnterMoveNextControl = true;
            this.LueJobDesc.Location = new System.Drawing.Point(399, 48);
            this.LueJobDesc.Margin = new System.Windows.Forms.Padding(5);
            this.LueJobDesc.Name = "LueJobDesc";
            this.LueJobDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobDesc.Properties.Appearance.Options.UseFont = true;
            this.LueJobDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueJobDesc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobDesc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueJobDesc.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobDesc.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueJobDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueJobDesc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueJobDesc.Properties.DropDownRows = 20;
            this.LueJobDesc.Properties.NullText = "[Empty]";
            this.LueJobDesc.Properties.PopupWidth = 500;
            this.LueJobDesc.Size = new System.Drawing.Size(286, 20);
            this.LueJobDesc.TabIndex = 33;
            this.LueJobDesc.ToolTip = "F4 : Show/hide list";
            this.LueJobDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueJobDesc.EditValueChanged += new System.EventHandler(this.LueJobDesc_EditValueChanged);
            this.LueJobDesc.Leave += new System.EventHandler(this.LueJobDesc_Leave);
            // 
            // TxtSourceKPI
            // 
            this.TxtSourceKPI.EnterMoveNextControl = true;
            this.TxtSourceKPI.Location = new System.Drawing.Point(120, 90);
            this.TxtSourceKPI.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSourceKPI.Name = "TxtSourceKPI";
            this.TxtSourceKPI.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSourceKPI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSourceKPI.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSourceKPI.Properties.Appearance.Options.UseFont = true;
            this.TxtSourceKPI.Properties.MaxLength = 250;
            this.TxtSourceKPI.Size = new System.Drawing.Size(270, 20);
            this.TxtSourceKPI.TabIndex = 18;
            // 
            // BtnSourceKPI
            // 
            this.BtnSourceKPI.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSourceKPI.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSourceKPI.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSourceKPI.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSourceKPI.Appearance.Options.UseBackColor = true;
            this.BtnSourceKPI.Appearance.Options.UseFont = true;
            this.BtnSourceKPI.Appearance.Options.UseForeColor = true;
            this.BtnSourceKPI.Appearance.Options.UseTextOptions = true;
            this.BtnSourceKPI.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSourceKPI.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSourceKPI.Image = ((System.Drawing.Image)(resources.GetObject("BtnSourceKPI.Image")));
            this.BtnSourceKPI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSourceKPI.Location = new System.Drawing.Point(392, 90);
            this.BtnSourceKPI.Name = "BtnSourceKPI";
            this.BtnSourceKPI.Size = new System.Drawing.Size(24, 21);
            this.BtnSourceKPI.TabIndex = 19;
            this.BtnSourceKPI.ToolTip = "List Of KPI";
            this.BtnSourceKPI.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSourceKPI.ToolTipTitle = "Run System";
            this.BtnSourceKPI.Click += new System.EventHandler(this.BtnSourceKPI_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(48, 93);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 14);
            this.label8.TabIndex = 17;
            this.label8.Text = "Source KPI";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(87, 114);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 20;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(120, 111);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(270, 20);
            this.LueSiteCode.TabIndex = 21;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.TxtTotalBobot);
            this.panel4.Controls.Add(this.LueParentKPI);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.LuePosCode);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtPICCode);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.TxtPICName);
            this.panel4.Controls.Add(this.BtnPIC);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(457, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(394, 162);
            this.panel4.TabIndex = 32;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(25, 114);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 14);
            this.label11.TabIndex = 36;
            this.label11.Text = "Total Bobot";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalBobot
            // 
            this.TxtTotalBobot.EnterMoveNextControl = true;
            this.TxtTotalBobot.Location = new System.Drawing.Point(102, 111);
            this.TxtTotalBobot.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalBobot.Name = "TxtTotalBobot";
            this.TxtTotalBobot.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalBobot.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalBobot.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalBobot.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalBobot.Properties.MaxLength = 250;
            this.TxtTotalBobot.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtTotalBobot.Size = new System.Drawing.Size(266, 20);
            this.TxtTotalBobot.TabIndex = 34;
            this.TxtTotalBobot.Validated += new System.EventHandler(this.TxtTotalBobot_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(83, 72);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 14);
            this.label9.TabIndex = 15;
            this.label9.Text = "Year";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(120, 69);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 500;
            this.LueYr.Size = new System.Drawing.Size(120, 20);
            this.LueYr.TabIndex = 16;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(73, 51);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 14);
            this.label10.TabIndex = 33;
            this.label10.Text = "Status";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(120, 48);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(112, 20);
            this.TxtStatus.TabIndex = 34;
            // 
            // LueMaxMin
            // 
            this.LueMaxMin.EnterMoveNextControl = true;
            this.LueMaxMin.Location = new System.Drawing.Point(299, 96);
            this.LueMaxMin.Margin = new System.Windows.Forms.Padding(5);
            this.LueMaxMin.Name = "LueMaxMin";
            this.LueMaxMin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaxMin.Properties.Appearance.Options.UseFont = true;
            this.LueMaxMin.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaxMin.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMaxMin.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaxMin.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMaxMin.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaxMin.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMaxMin.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaxMin.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMaxMin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMaxMin.Properties.DropDownRows = 20;
            this.LueMaxMin.Properties.NullText = "[Empty]";
            this.LueMaxMin.Properties.PopupWidth = 500;
            this.LueMaxMin.Size = new System.Drawing.Size(286, 20);
            this.LueMaxMin.TabIndex = 34;
            this.LueMaxMin.ToolTip = "F4 : Show/hide list";
            this.LueMaxMin.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMaxMin.EditValueChanged += new System.EventHandler(this.LueMaxMin_EditValueChanged);
            this.LueMaxMin.Click += new System.EventHandler(this.LueMaxMin_Click);
            this.LueMaxMin.Leave += new System.EventHandler(this.LueMaxMin_Leave);
            this.LueMaxMin.Validated += new System.EventHandler(this.LueMaxMin_Validated);
            // 
            // FrmKPI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 473);
            this.Name = "FrmKPI";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKPIName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParentKPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJobDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSourceKPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBobot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaxMin.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CheckEdit ChkActiveInd;
        private DevExpress.XtraEditors.TextEdit TxtKPIName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.SimpleButton BtnPIC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueParentKPI;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private DevExpress.XtraEditors.LookUpEdit LueOption;
        public DevExpress.XtraEditors.TextEdit TxtPICName;
        public DevExpress.XtraEditors.TextEdit TxtPICCode;
        private DevExpress.XtraEditors.LookUpEdit LueType;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LuePosCode;
        private DevExpress.XtraEditors.LookUpEdit LueJobDesc;
        public DevExpress.XtraEditors.TextEdit TxtSourceKPI;
        public DevExpress.XtraEditors.SimpleButton BtnSourceKPI;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LblSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private DevExpress.XtraEditors.LookUpEdit LueMaxMin;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtTotalBobot;
    }
}