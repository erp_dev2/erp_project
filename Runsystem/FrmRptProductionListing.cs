﻿#region Update
/*
    01/10/2017 [TKG] Tambah plan remark 
    06/11/2017 [TKG] Tambah plan#
    05/02/2020 [VIN/YK] Penambahan kolom untuk warna di bagian detail disamping kolom %
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProductionListing : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        internal bool mIsProductionListingShowColorRow = false ;

        #endregion

        #region Constructor

        public FrmRptProductionListing(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsProductionListingShowColorRow = Sm.GetParameterBoo("IsProductionListingShowColorRow");
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, C.DocDt, A.ItCode, D.ItName, ");
            SQL.AppendLine("A.PlannedQty, IfNull(B.SFCQty, 0) As SFCQty, D.PlanningUomCode, ");
            SQL.AppendLine("A.PlannedQty*D.PlanningUomCodeConvert12 As PlannedQty2, ");
            SQL.AppendLine("IfNull(B.SFCQty, 0)*D.PlanningUomCodeConvert12 As SFCQty2, ");
            SQL.AppendLine("D.PlanningUomCode2, C.Remark ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select T1.DocNo, T3.ItCode, Sum(T3.Qty) As PlannedQty ");
            SQL.AppendLine("    From TblPPHdr T1 ");
            SQL.AppendLine("    Inner Join TblPPDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblProductionOrderHdr T3 On T2.ProductionOrderDocNo=T3.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T1.DocNo, T3.ItCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T1.PPDocNo, T2.ItCode, Sum(T2.Qty) As SFCQty ");
            SQL.AppendLine("    From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPPHdr T3 On T1.PPDocNo=T3.DocNo And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.PPDocNo, T2.ItCode ");
            SQL.AppendLine(") B On A.DocNo=B.PPDocNo And A.ItCode=B.ItCode ");
            SQL.AppendLine("Inner Join TblPPHdr C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblItem D On A.ItCode=D.ItCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Planned#",
                        "",
                        "Date",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Item's Name",
                        "Planned",
                        "SFC",
                        "UoM",
                        "%",

                        //11-15
                        "Planned",
                        "SFC",
                        "UoM",
                        "%",
                        "Plan's Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 20, 80, 80, 20, 
                        
                        //6-10
                        250, 100, 100, 80, 80,

                        //11-15
                        100, 100, 80, 80, 250
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 11, 12 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 10, 14 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, false);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By C.DocDt, C.DocNo Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "ItCode", "ItName", "PlannedQty", "SFCQty",
                           
                            //6-10
                            "PlanningUomCode", "PlannedQty2", "SFCQty2", "PlanningUomCode2", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            if (dr.GetDecimal(c[4])==0)
                                Grd.Cells[Row, 10].Value = 0m;               
                            else
                                Grd.Cells[Row, 10].Value = dr.GetDecimal(c[5]) / dr.GetDecimal(c[4]) * 100;
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            if (dr.GetDecimal(c[7]) == 0)
                                Grd.Cells[Row, 14].Value = 0m;
                            else
                                Grd.Cells[Row, 14].Value = dr.GetDecimal(c[8]) / dr.GetDecimal(c[7]) * 100;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);


                            if (mIsProductionListingShowColorRow)
                            {
                                if (decimal.Parse(Sm.GetGrdStr(Grd1, Row, 14)) <= 0)
                                {
                                    Grd1.Rows[Row].BackColor = Color.Red;
                                }
                                else if (decimal.Parse(Sm.GetGrdStr(Grd1, Row, 14)) >= 100)
                                {
                                    Grd1.Rows[Row].BackColor = Color.Green;
                                }
                                else
                                {
                                    Grd1.Rows[Row].BackColor = Color.Yellow;
                                }
                            }
                           
                        }, true, false, false, false
                    );

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 4));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 4));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method
        //private void GrdColor(int Row)
        //{
        //    if (mIsProductionListingShowColorRow)
        //    {
        //        if (decimal.Parse(Sm.GetGrdStr(Grd1, Row, 14)) <= 0)
        //        {
        //            Grd1.Rows[Row].BackColor = Color.Red;
        //        }
        //        else if (decimal.Parse(Sm.GetGrdStr(Grd1, Row, 14)) >= 100)
        //        {
        //            Grd1.Rows[Row].BackColor = Color.Green;
        //        }
        //        else
        //        {
        //            Grd1.Rows[Row].BackColor = Color.Yellow;
        //        }
        //    }
        //}
        
        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Planned document");
        }

        #endregion
        
        #endregion
    }
}
