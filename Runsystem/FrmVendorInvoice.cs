﻿#region Update
/*
 * 16/09/2021 [NJP/RUNMARKET] Membuat Vendor Invoice Verification 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVendorInvoice : RunSystem.FrmBase5
    {
        #region Field

        private string
            mMenuCode = string.Empty, mSQL = string.Empty;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmVendorInvoice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
                Sl.SetLueVdCode(ref LueVdCode);
                SetLueStatus(ref LueStatus);
                LueStatus.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, B.VdName, (A.Amt-A.TaxAmt) AS TAmt, A.TaxAmt, A.Amt ");
            SQL.AppendLine("From TblVendorInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Document#", 
                        "Invoice#",
                        "Date",
                        "Vendor",

                        //6-10
                        "Total Without Tax",
                        "Total Tax",
                        "Total With Tax",
                        "",
                        "Status"

                    },
                    new int[] 
                    {
                        //0
                        30,

                        //1-5
                        20, 80, 80, 80, 100,

                        //6-10
                        80, 80, 80, 0, 100,

                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 9 }, false);
            Sm.GrdFormatDate(Grd1, new int[]{4});
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8 },0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "WHERE A.CancelInd = 'N' AND A.InvoiceStatusInd = 'O' ";
                //string Filter = "WHERE 0=0 ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "B.VdCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DocNo ASC, DocDt ASC;",
                        new string[] { 
                            "DocNo", 
                            
                            "LocalDocNo", "DocDt", "VdName", "TAmt", "TaxAmt", 
                            
                            "Amt"
                        
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                
                        }, true, false, true, false
                    );

                if (Grd1.Rows.Count > 1) Grd1.Rows.RemoveAt(Grd1.Rows.Count - 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No ||
                    IsEditedDataNotExisted()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if ((Sm.GetGrdBool(Grd1, r, 1))&&(Sm.GetGrdStr(Grd1, r, 2).Length != 0)
                        && (Sm.GetGrdStr(Grd1, r, 9).Length != 0) && (Sm.GetGrdStr(Grd1, r, 10).Length != 0))
                    {
                        cml.Add(UpdateStatus(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 9)));       
                    }
                }

                Sm.ExecCommands(cml);
                Sm.StdMsg(mMsgType.Info, "Data Saved.");
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
               
        private bool IsEditedDataNotExisted()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1)) return false;

            Sm.StdMsg(mMsgType.Warning, "You need to edit minimum 1 record.");
            return true;
        }

        private MySqlCommand UpdateStatus(string DocNo, string Status)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblVendorInvoiceHdr Set " +
                    "   InvoiceStatusInd=@Status, " +
                    "   LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Status", Status);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
                e.DoDefault = false;
            else
            {
                if (e.ColIndex == 1)
                {
                    if (Sm.GetGrdBool(Grd1, e.RowIndex, 1))
                    {
                        e.DoDefault = false;
                        Grd1.Cells[e.RowIndex, 1].Value = false;
                        Grd1.Cells[e.RowIndex, 9].Value = null;
                        Grd1.Cells[e.RowIndex, 10].Value = null;
                    }
                    else
                        LueRequestEdit(Grd1, LueStatus, ref fCell, ref fAccept, e);
                                               
                }
                else if (e.ColIndex == 10)
                    LueRequestEdit(Grd1, LueStatus, ref fCell, ref fAccept, e);
                
            }
        }

        private void Grd1_CellClick(object sender, iGCellClickEventArgs e)
        {
            if (e.ColIndex == 1 || e.ColIndex == 10)
                GoCols(e.RowIndex, e.ColIndex);
            else
                GoCols(e.RowIndex, 1);

            //if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            //    LueRequestEdit(Grd1, LueStatus, ref fCell, ref fAccept, e.RowIndex);
        }

        #endregion

        #region Additional Method
      
        private void SetLueStatus(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'P' As Col1, 'Verified' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'C' As Col1, 'Not Verified' As Col2;");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept,TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            //fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell = Grd.Cells[e.RowIndex, 10];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            var Status = Sm.GetGrdStr(Grd, fCell.RowIndex, 9);
            if (Status.Length == 0)
                Lue.EditValue = null;
            else
            {
                SetLueStatus(ref LueStatus);
                Sm.SetLue(Lue, Status);
            }
            Lue.Visible = true;
            Lue.Focus();
            fAccept = true;
        }

        private void GoCols(int r, int c)
        {
            Grd1.SetCurCell(r, c);
            Grd1.Focus();
                                 
        }
              

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Invoice#");
        }
        

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LueStatus_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStatus_Leave(object sender, EventArgs e)
        {
            if (LueStatus.Visible && fAccept && fCell.ColIndex == 10)
            {
                if (Sm.GetLue(LueStatus).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = false;
                    Grd1.Cells[fCell.RowIndex, 9].Value = null;
                    Grd1.Cells[fCell.RowIndex, 10].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = true;
                    Grd1.Cells[fCell.RowIndex, 9].Value = Sm.GetLue(LueStatus);
                    Grd1.Cells[fCell.RowIndex, 10].Value = LueStatus.GetColumnValue("Col2"); ;
                }
            }
                       
        }        

        #endregion
        
        #endregion
    }
}
