﻿#region Update
/*
    04/05/2020 [VIN/VIR] tambah field local code 
    27/11/2020 [ICA/IMS] tambah field cost category based on parameter IsBudgetCategoryUseCostCategory
    09/12/2020 [IBL/PHT] tambah field cost center dan coa based on parameter IsBudgetCategoryUseCostCenter
    16/12/2020 [ICA/IMS] Field cost category dihubungkan dengan cost category (diubah menjadi dialog)
    22/12/2020 [ICA/PHT] Memisahkan parameter IsbudgetCategoryUseCostCenter dengan IsBudgetCategoryUseCOA
    28/01/2020 [TKG/SIER] Budget category maksimal menjadi 30 karakter
    07/05/2021 [RDH/SIER] Budget Category dibuat tidak mandatory dalam memilih COST CATEGORY khususnya untuk jenis anggaran CAPEX (Capital Expenditure) karena bersifat investasi (tidak menjadi biaya)
    18/05/2021 [TKG/PHT] tambah parameter IsBudgetCategoryUseProfitCenter
    03/06/2021 [VIN/PHT] bug saat chose data
    10/06/2021 [TRI/IMS] Pilihan departemen dibuat hanya menampilkan departemen yang aktif
    25/06/2021 [MYA/IMS] tambah parameter IsCostCenterBasedOnCostCategory
    27/08/2021 [ICA/AMKA] penyesuaian location costcategory, costcenter dan coa
    02/09/2021 [ICA/AMKA] membuat bisa dibuka di aplikasi lain
    19/03/2022 [TKG/SIER] bug validasi di IsCostCategoryInvalid()
    01/09/2022 [VIN/SIER] ganti param internal
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal 
            string mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mBudgetTypeCodeForInvestment = string.Empty,
            mBCCode = string.Empty //if this application is called from other application
            ;
        internal FrmBudgetCategoryFind FrmFind;
        internal bool
            mIsBudgetCategoryUseCostCategory = false,
            mIsBudgetCategoryUseCostCenter = false,
            mIsBudgetCategoryUseCOA = false
            ;
        internal string mCCCode = string.Empty, mCCtCode = string.Empty;
        internal bool
            mIsCOAUseAlias = false,
            mIsFilterByCC = false,
            mIsBudgetCategoryUseProfitCenter = false,
            mIsCostCenterBasedOnCostCategory = false;

        private bool mResult = true;
            
        #endregion

        #region Constructor

        public FrmBudgetCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Budget Category";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                LblCCCode.Visible = TxtCCCode.Visible = BtnCCCode.Visible = LblCOA.Visible = TxtAcNo.Visible = TxtAcDesc.Visible = BtnAcNo.Visible = false;
                SetFormControl(mState.View);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                SetBudgetType(ref LueBudgetType);
                GetParameter();
                TxtCostCenterCode.Visible = TxtCostCategoryCode.Visible = false;
                TxtCCtName.Visible = label6.Visible = BtnCCtCode.Visible = mIsBudgetCategoryUseCostCategory;
                LblCCCode.Visible = TxtCCCode.Visible = BtnCCCode.Visible = mIsBudgetCategoryUseCostCenter;
                LblCOA.Visible = TxtAcNo.Visible = TxtAcDesc.Visible = BtnAcNo.Visible = mIsBudgetCategoryUseCOA;

                //if this application is called from other application
                if (mBCCode.Length != 0)
                {
                    ShowData(mBCCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtBCName, TxtLocalCode, LueDeptCode, ChkActInd, LueBudgetType, TxtCCtName, TxtCCCode }, true);
                    BtnAcNo.Enabled = BtnCCCode.Enabled = BtnCCtCode.Enabled = false;
                    TxtBCCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtLocalCode, TxtBCName, LueDeptCode, LueBudgetType }, false);
                    BtnAcNo.Enabled = BtnCCCode.Enabled = BtnCCtCode.Enabled = true;
                    if (mIsCostCenterBasedOnCostCategory) BtnCCCode.Enabled = false;
                    TxtLocalCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{TxtLocalCode, TxtBCName, LueDeptCode, ChkActInd, LueBudgetType }, false);
                    BtnAcNo.Enabled = BtnCCCode.Enabled = BtnCCtCode.Enabled = true;
                    if (mIsCostCenterBasedOnCostCategory) BtnCCCode.Enabled = false;
                    TxtLocalCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void GetParameter()
        {
            mIsBudgetCategoryUseCostCategory = Sm.GetParameterBoo("IsBudgetCategoryUseCostCategory");
            mIsBudgetCategoryUseCostCenter = Sm.GetParameterBoo("IsBudgetCategoryUseCostCenter");
            mIsBudgetCategoryUseCOA = Sm.GetParameterBoo("IsBudgetCategoryUseCOA");
            mIsCOAUseAlias = Sm.GetParameterBoo("IsCOAUseAlias");
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
            mBudgetTypeCodeForInvestment = Sm.GetParameter("BudgetTypeCodeForInvestment");
            mIsBudgetCategoryUseProfitCenter = Sm.GetParameterBoo("IsBudgetCategoryUseProfitCenter");
            mIsCostCenterBasedOnCostCategory = Sm.GetParameterBoo("IsCostCenterBasedOnCostCategory");
        }
        #endregion

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtBCCode, TxtLocalCode, TxtBCName, LueDeptCode, LueBudgetType, TxtCCtName, TxtCCCode, TxtAcNo, TxtAcDesc, TxtCostCategoryCode, TxtCostCenterCode });
            mCCCode = string.Empty;
            mCCtCode = string.Empty;
            ChkActInd.Checked = false;
        }

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBudgetCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtBCCode, string.Empty, false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblBudgetCategory Where BCCode=@BCCode;" };
                Sm.CmParam<String>(ref cm, "@BCCode", TxtBCCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;
                if (TxtBCCode.Text.Length == 0)
                    TxtBCCode.EditValue = GenerateBCCode();

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblBudgetCategory(BCCode, LocalCode, BCName, DeptCode, BudgetType, CCtCode, CCCode, AcNo, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@BCCode, @LocalCode, @BCName, @DeptCode, @BudgetType, @CCtCode, @CCCode, @AcNo, 'Y', @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update LocalCode=@LocalCode, BCName=@BCName, DeptCode=@DeptCode, BudgetType=@BudgetType, CCtCode=@CCtCode, CCCode=@CCCode, AcNo=@AcNo, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@BCCode", TxtBCCode.Text);
                Sm.CmParam<String>(ref cm, "@LocalCode", TxtLocalCode.Text);
                Sm.CmParam<String>(ref cm, "@BCName", TxtBCName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@BudgetType", Sm.GetLue(LueBudgetType));
                Sm.CmParam<String>(ref cm, "@CCtCode", TxtCostCategoryCode.Text);
                Sm.CmParam<String>(ref cm, "@CCCode", TxtCostCenterCode.Text);
                Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtBCCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string BCCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@BCCode", BCCode);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.BCCode, A.LocalCode, A.BCName, A.DeptCode, A.BudgetType, D.CCtName, A.ActInd, ");
                SQL.AppendLine("B.CCName, C.AcNo, C.AcDesc, C.Alias, A.CCCode, A.CCtCode ");
                SQL.AppendLine("From TblBudgetCategory A ");
                SQL.AppendLine("Left Join TblCostCenter B On A.CCCode = B.CCCode ");
                SQL.AppendLine("Left Join TblCOA C On A.AcNo = C.AcNo ");
                SQL.AppendLine("Left Join TblCostCategory D On A.CCtCode = D.CCtCode ");
                SQL.AppendLine("Where BCCode=@BCCode ");

                Sm.ShowDataInCtrl(
                        ref cm,SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "BCCode",

                            //1-5
                            "LocalCode", "BCName", "DeptCode", "BudgetType", "CCtName",

                            //6-10
                            "ActInd", "CCName", "AcNo", "AcDesc", "Alias", 

                            //11- 12
                            "CCCode", "CCtCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtBCCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtLocalCode.EditValue = Sm.DrStr(dr, c[1]);
                            TxtBCName.EditValue = Sm.DrStr(dr, c[2]);
                            Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                            Sm.SetLue(LueBudgetType, Sm.DrStr(dr, c[4]));
                            TxtCCtName.EditValue = Sm.DrStr(dr, c[5]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[6])=="Y";
                            TxtCCCode.EditValue = Sm.DrStr(dr, c[7]);
                            TxtAcNo.EditValue = Sm.DrStr(dr, c[8]);
                            if (mIsCOAUseAlias && Sm.DrStr(dr, c[10]).Length > 0)
                                TxtAcDesc.EditValue = Sm.DrStr(dr, c[9]) + " [" + Sm.DrStr(dr, c[10]) + "]";
                            else
                                TxtAcDesc.EditValue = Sm.DrStr(dr, c[9]);

                            TxtCostCenterCode.EditValue = Sm.DrStr(dr, c[11]);
                            TxtCostCategoryCode.EditValue = Sm.DrStr(dr, c[12]);


                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtBCName, "Budget category name", false) ||
                Sm.IsLueEmpty(LueDeptCode, "Department")||
                Sm.IsLueEmpty(LueBudgetType, "Budget Type")||
                IsCostCategoryInvalid() ||
                (mIsBudgetCategoryUseCostCenter && Sm.IsTxtEmpty(TxtCCCode, "Cost Center", false)) ||
                (mIsBudgetCategoryUseCOA && Sm.IsTxtEmpty(TxtAcNo, "COA's Account", false));
        }

        private bool IsCostCategoryInvalid()
        {
           
            if (!mIsBudgetCategoryUseCostCategory) return false;
            if (mBudgetTypeCodeForInvestment.Length > 0)
            {
                if (Sm.Find_In_Set(Sm.GetLue(LueBudgetType), mBudgetTypeCodeForInvestment))
                { 
                    if (Sm.IsTxtEmpty(TxtCCtName, "Cost category", false)) return true;
                }
            }
            return false;
        }


        private string GenerateBCCode()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Right(Concat('0000', IfNull(( ");
 	        SQL.AppendLine("    Select Convert(BCCode, Decimal)+1 As Value ");
	        SQL.AppendLine("    From TblBudgetCategory Order By BCCode Desc Limit 1 ");
            SQL.AppendLine("), 1)), 5) As BCCode");

            return Sm.GetValue(SQL.ToString());
        }

        #endregion


        public static void SetBudgetType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'BudgetType' Order By OptCode;",
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }

        #endregion

        #region Event

        private void TxtBCName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBCName);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void LueBudgetType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBudgetType, new Sm.RefreshLue1(SetBudgetType));
        }

        private void BtnCCCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBudgetCategoryDlg(this));
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBudgetCategoryDlg2(this));
        }

        private void BtnCCtCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBudgetCategoryDlg3(this));
        }

        #endregion 
     
    }
}
