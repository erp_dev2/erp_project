﻿#region Update
/*
    15/04/2019 [WED] new reporting
    23/05/2019 [TKG] bug double data
    28/05/2019 [MEY] kolom Invoice Amount yang ditampilkan nominal sebelum Tax
    18/06/2019 [WED] Invoice yang sama, dijadikan satu baris
    20/06/2019 [WED] Invoice yang cancel masih muncul
    01/07/2019 [WED] Amount ambil dari Qty * UPriceBeforeTax
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptCommercialInvoice2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mMainCurCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptCommercialInvoice2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.DocDt, D.DocDt SLIDocDt, D.LocalDocNo, E.CtCode, F.CtName, ");
            //SQL.AppendLine("H.SODocNo, ");
            //SQL.AppendLine("J.ItGrpName Sortimen, ");
            //SQL.AppendLine("C.ItCode, G.ItName, C.QtyPackagingUnit Pallet, ");
            //SQL.AppendLine("B.Qty As Volume, ");
            //SQL.AppendLine("G.PurchaseUomCode, C.Qty, D.TotalAmt ");
            SQL.AppendLine("Select Group_Concat(Distinct Date_Format(A.DocDt, '%d/%b/%Y')) DocDt, D.DocDt SLIDocDt, D.LocalDocNo, ");
            SQL.AppendLine("Group_Concat(Distinct E.CtCode) CtCode, Group_Concat(Distinct F.CtName) CtName, ");
            SQL.AppendLine("Group_Concat(Distinct H.SODocNo) SODocNo, ");
            SQL.AppendLine("Group_Concat(Distinct J.ItGrpName) Sortimen, ");
            SQL.AppendLine("Group_Concat(Distinct C.ItCode) ItCode, ");
            SQL.AppendLine("Group_Concat(Distinct G.ItName) ItName, ");
            SQL.AppendLine("IfNull(Sum(C.QtyPackagingUnit), 0.00) Pallet, ");
            SQL.AppendLine("IfNull(Sum(B.Qty), 0.00) As Volume, ");
            SQL.AppendLine("Group_Concat(Distinct G.PurchaseUomCode) PurchaseUomCode, ");
            SQL.AppendLine("IfNull(Sum(C.Qty), 0.00) Qty, ");
            //SQL.AppendLine("D.TotalAmt ");
            SQL.AppendLine("IfNull(Sum(C.Qty * C.UPriceBeforeTax), 0.0000) TotalAmt ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl C On B.DocNo = C.DOCtDocNo And B.DNo = C.DOCtDNo ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr D On C.DocNo = D.DocNo And D.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblDRHdr E On A.DRDocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer F On E.CtCode = F.CtCode ");
            SQL.AppendLine("Inner Join TblItem G On C.ItCode = G.ItCode ");
            SQL.AppendLine("Inner Join TblDRDtl H On E.DocNo = H.DocNo And B.DRDNo=H.DNo ");
            SQL.AppendLine("Inner Join TblSOHdr I On H.SODocNo = I.DocNo And I.OverseaInd = 'N' ");
            SQL.AppendLine("Left Join TblItemGroup J On G.ItGrpCode = J.ItGrpCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.DRDocNo Is Not Null ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Stuffing Date",
                    "SLI-AR Date",
                    "Invoice Local#",
                    "Customer#",
                    "Customer",
                    
                    //6-10
                    "SO#",
                    "Product"+Environment.NewLine+"Type",
                    "Item's Code",
                    "Item's Name",
                    "Pallet",

                    //11-14
                    "Volume",
                    "UoM",
                    "Quantity",
                    "Invoice Amount"
                },
                new int[] 
                {
                    //0
                    50,
                    
                    //1-5
                    120, 120, 120, 100, 200,
                    
                    //6-10
                    140, 150, 100, 200, 100, 
                    
                    //11-14
                    120, 120, 100, 170                
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 13, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 8 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = " ";

                //Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                //Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtInvoice.Text, new string[] { "D.LocalDocNo" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Group By D.DocDt, D.LocalDocNo; ",
                        new string[]
                        {
                            //0
                            "DocDt",

                            //1-5
                            "SLIDocDt", "LocalDocNo", "CtCode", "CtName", "SODocNo",  
                            
                            //6-10
                            "Sortimen", "ItCode", "ItName", "Pallet", "Volume", 
                        
                            //11-13
                            "PurchaseUomCode", "Qty","TotalAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 13, 14 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 13, 14 });
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtInvoice_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvoice_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Invoice#");
        }

        #endregion

        #endregion

    }
}
