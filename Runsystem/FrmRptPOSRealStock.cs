﻿#region Update
/*
    20/09/2017 [WED] revisi typo filter warehouse
    20/09/2017 [WED] tambah filter item category
    16/10/2017 [WED] Filter PosNo dihilangkan, warehouse sesuai yang terdaftar di POS Setting
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPOSRealStock : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty, 
            mCurrentDate = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPOSRealStock(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueItCtCode(ref LueItCatCode);
                Sm.SetControlReadOnly(new List<BaseEdit> 
                { 
                    DteDocDt
                }, true);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL()
        {

            var SQL = new StringBuilder();

            #region Old Code
            //SQL.AppendLine("Select X.ItCode, Y.ItName, Y1.ItCtName, Y2.ItGrpName, Y.InventoryUOMCode, X.QtyStock, X.QtySales, X.QtyReturn ");
            //SQL.AppendLine("From ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T1.ItCode, Sum(T1.QtyStock) QtyStock, Sum(T1.QtySales) QtySales, Sum(T1.QtyReturn) QtyReturn ");
            //SQL.AppendLine("    From ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select ItCode, Qty As QtyStock, 0 As QtySales, 0 As QtyReturn ");
            //SQL.AppendLine("        From TblStockSummary  ");
            //SQL.AppendLine("        Where Qty <> 0 ");
            //SQL.AppendLine("        And WhsCode = @WhsCode ");
            //SQL.AppendLine("        Union All ");
            //SQL.AppendLine("        Select A.ItCode, 0 As QtyStock, A.Qty As QtySales, 0 As QtyReturn ");
            //SQL.AppendLine("        From TblPosTrnDtl A ");
            //SQL.AppendLine("        Inner Join TblPosSetting B On A.PosNo = B.PosNo And B.SetCode = 'CurrentBnsDate' ");
            //SQL.AppendLine("        Inner Join TblPosSetting C On A.PosNo = C.PosNo And C.SetCode = 'StsDayEnd' ");
            //SQL.AppendLine("        Where (B.SetValue Is Not Null And Length(B.SetValue) > 0) ");
            //SQL.AppendLine("        And C.SetValue = 'O' ");
            //SQL.AppendLine("        And A.DtlType = 'S' ");
            //SQL.AppendLine("        And (B.PosNo = @PosNo And C.PosNo = @PosNo) ");
            //SQL.AppendLine("        And B.SetValue = @DocDt ");
            //SQL.AppendLine("        And A.BsDate = @DocDt ");
            //SQL.AppendLine("        Union All ");
            //SQL.AppendLine("        Select A.ItCode, 0 As QtyStock, 0 As QtySales, A.Qty As QtyReturn ");
            //SQL.AppendLine("        From TblPosTrnDtl A ");
            //SQL.AppendLine("        Inner Join TblPosSetting B On A.PosNo = B.PosNo And B.SetCode = 'CurrentBnsDate' ");
            //SQL.AppendLine("        Inner Join TblPosSetting C On A.PosNo = C.PosNo And C.SetCode = 'StsDayEnd' ");
            //SQL.AppendLine("        Where (B.SetValue Is Not Null And Length(B.SetValue) > 0) ");
            //SQL.AppendLine("        And C.SetValue = 'O' ");
            //SQL.AppendLine("        And A.DtlType = 'R' ");
            //SQL.AppendLine("        And (B.PosNo = @PosNo And C.PosNo = @PosNo) ");
            //SQL.AppendLine("        And B.SetValue = @DocDt ");
            //SQL.AppendLine("        And A.BsDate = @DocDt ");
            //SQL.AppendLine("    )T1 ");
            //SQL.AppendLine("    Group By T1.ItCode ");
            //SQL.AppendLine(")X ");
            //SQL.AppendLine("Inner Join TblItem Y On X.ItCode = Y.ItCode ");
            //SQL.AppendLine("Left Join TblItemCategory Y1 On Y.ItCtCode = Y1.ItCtCode ");
            //SQL.AppendLine("Left Join TblItemGroup Y2 On Y.ItGrpCode = Y2.ItGrpCode ");
            #endregion

            SQL.AppendLine("Select X.PosNo, X.ItCode, Y.ItName, Y1.ItCtName, Y2.ItGrpName, Y.InventoryUOMCode, (X.QtyStock - X.QtyIn - X.QtyOut) AS QtyStock, (X.QtySales * -1) As QtySales, X.QtyReturn, X.QtyIn, X.QtyOut, Y3.WhsName ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Group_Concat(Distinct T1.PosNo) As PosNo, T1.ItCode, Sum(T1.QtyStock) QtyStock, Sum(T1.QtySales) QtySales, Sum(T1.QtyReturn) QtyReturn, ");
	        SQL.AppendLine("    Sum(T1.QtyIn) QtyIn, Sum(T1.QtyOut) QtyOut, T1.WhsCode ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select null As PosNo, ItCode, Qty As QtyStock, 0 As QtySales, 0 As QtyReturn, 0 As QtyIn, 0 As QtyOut, WhsCode ");
            SQL.AppendLine("        From TblStockSummary  ");
            SQL.AppendLine("        Where Qty <> 0 ");
            SQL.AppendLine("        And Find_In_Set(WhsCode, @ListWhsCode) ");

            if(Sm.GetLue(LueWhsCode).Length > 0)
                SQL.AppendLine("        And WhsCode = @WhsCode ");

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select A.PosNo, A.ItCode, 0 As QtyStock, A.Qty As QtySales, 0 As QtyReturn, 0 As QtyIn, 0 As QtyOut, D.SetValue As WhsCode  ");
            SQL.AppendLine("        From TblPosTrnDtl A ");
            SQL.AppendLine("        Inner Join TblPosSetting B On A.PosNo = B.PosNo And B.SetCode = 'CurrentBnsDate' ");
            SQL.AppendLine("        Inner Join TblPosSetting C On A.PosNo = C.PosNo And C.SetCode = 'StsDayEnd' ");
            SQL.AppendLine("        Inner Join TblPosSetting D On A.PosNo = D.PosNo And D.SetCode = 'StoreWhsCode' ");
            SQL.AppendLine("        Where (B.SetValue Is Not Null And Length(B.SetValue) > 0) ");
            SQL.AppendLine("        And (D.SetValue Is Not Null And Length(D.SetValue) > 0) ");

            if(Sm.GetLue(LueWhsCode).Length > 0)
                SQL.AppendLine("        And D.SetValue = @WhsCode ");

            SQL.AppendLine("        And C.SetValue = 'O' ");
            SQL.AppendLine("        And A.DtlType = 'S' ");
            SQL.AppendLine("        And B.SetValue = @DocDt ");
            SQL.AppendLine("        And A.BsDate = @DocDt ");

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select A.PosNo, A.ItCode, 0 As QtyStock, 0 As QtySales, A.Qty As QtyReturn, 0 As QtyIn, 0 As QtyOut, D.SetValue As WhsCode  ");
            SQL.AppendLine("        From TblPosTrnDtl A ");
            SQL.AppendLine("        Inner Join TblPosSetting B On A.PosNo = B.PosNo And B.SetCode = 'CurrentBnsDate' ");
            SQL.AppendLine("        Inner Join TblPosSetting C On A.PosNo = C.PosNo And C.SetCode = 'StsDayEnd' ");
            SQL.AppendLine("        Inner Join TblPosSetting D On A.PosNo = D.PosNo And D.SetCode = 'StoreWhsCode' ");
            SQL.AppendLine("        Where (B.SetValue Is Not Null And Length(B.SetValue) > 0) ");
            SQL.AppendLine("        And (D.SetValue Is Not Null And Length(D.SetValue) > 0) ");

            if (Sm.GetLue(LueWhsCode).Length > 0)
                SQL.AppendLine("        And D.SetValue = @WhsCode ");

            SQL.AppendLine("        And C.SetValue = 'O' ");
            SQL.AppendLine("        And A.DtlType = 'R' ");
            SQL.AppendLine("        And B.SetValue = @DocDt ");
            SQL.AppendLine("        And A.BsDate = @DocDt ");

		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        Select null As PosNo, ItCode, 0 As QtyStock, 0 As QtySales, 0 As QtyReturn, Qty As QtyIn, 0 As QtyOut, WhsCode ");
		    SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocDt = @DocDt ");
            SQL.AppendLine("        And Find_In_Set(WhsCode, @ListWhsCode) ");

            if (Sm.GetLue(LueWhsCode).Length > 0)
                SQL.AppendLine("        And WhsCode = @WhsCode ");
            
            SQL.AppendLine("        And (Qty > 0 Or Qty2 > 0 Or Qty3 > 0) ");
            SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        Select null As PosNo, ItCode, 0 As QtyStock, 0 As QtySales, 0 As QtyReturn, 0 As QtyIn, Qty As QtyOut, WhsCode ");
		    SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocDt = @DocDt ");
            SQL.AppendLine("        And Find_In_Set(WhsCode, @ListWhsCode) ");

            if (Sm.GetLue(LueWhsCode).Length > 0)
                SQL.AppendLine("        And WhsCode = @WhsCode ");
            
            SQL.AppendLine("        And (Qty < 0 Or Qty2 < 0 Or Qty3 < 0) ");
            SQL.AppendLine("    )T1 ");
            SQL.AppendLine("    Group By T1.WhsCode, T1.ItCode ");
            SQL.AppendLine(")X ");
            SQL.AppendLine("Inner Join TblItem Y On X.ItCode = Y.ItCode ");
            SQL.AppendLine("Left Join TblItemCategory Y1 On Y.ItCtCode = Y1.ItCtCode ");
            SQL.AppendLine("Left Join TblItemGroup Y2 On Y.ItGrpCode = Y2.ItGrpCode ");
            SQL.AppendLine("Left Join TblWarehouse Y3 On X.WhsCode = Y3.WhsCode ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "POS#",
                        "Warehouse",
                        "Item's Code",
                        "Item's Name",
                        "Item's Category",

                        //6-10
                        "Item's Group",
                        "Uom",
                        "Quantity"+Environment.NewLine+"Stock",
                        "Quantity"+Environment.NewLine+"In",
                        "Quantity"+Environment.NewLine+"Out",

                        //11-13
                        "Quantity"+Environment.NewLine+"Sales",
                        "Quantity"+Environment.NewLine+"Return",
                        "Balance",
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        120, 180, 100, 300, 150, 
                        
                        //6-10
                        200, 60, 100, 100, 100, 

                        //11-13
                        100, 100, 100
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {                
                Cursor.Current = Cursors.WaitCursor;

                mCurrentDate = Sm.ServerCurrentDateTime();
 
                Sm.SetDteCurrentDate(DteDocDt);

                var cm = new MySqlCommand();

                string Filter = " Where 0=0 ";

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "Y.ItName" });
                //Sm.CmParam<String>(ref cm, "@PosNo", TxtPosNo.Text);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCatCode), "Y.ItCtCode", true);
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@ListWhsCode", Sm.GetValue("Select Group_Concat(Distinct SetValue) As WhsCode From TblPosSetting Where SetCode = 'StoreWhsCode'"));
                Sm.CmParam<String>(ref cm, "@DocDt", Sm.Left(mCurrentDate, 8));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By Y.ItName; ",
                        new string[]
                        {
                            //0
                            "ItCode", 
                            
                            //1-5
                            "ItName", "ItCtName", "ItGrpName", "InventoryUomCode", "QtyStock", 

                            //6-10
                            "QtyIn", "QtyOut", "QtySales", "QtyReturn", "PosNo",

                            //11
                            "WhsName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 9);
                            Grd1.Cells[Row, 13].Value = Sm.GetGrdDec(Grd1, Row, 8) + Sm.GetGrdDec(Grd1, Row, 9) + Sm.GetGrdDec(Grd1, Row, 10) + Sm.GetGrdDec(Grd1, Row, 11) + Sm.GetGrdDec(Grd1, Row, 12); 
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void SetLueWhsCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SetValue As Col1, B.WhsName As Col2 ");
            SQL.AppendLine("From TblPosSetting A ");
            SQL.AppendLine("Inner Join TblWarehouse B ON A.SetValue = B.WhsCode ");
            SQL.AppendLine("Where A.SetCode = 'StoreWhsCode' ");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCatCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCatCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        #endregion

        #region Button Events

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 3].Value = "'" + Sm.GetGrdStr(Grd1, Row, 3);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 3].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(Grd1, Row, 3).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #endregion       

    }
}
