﻿#region Update
/*
    21/06/2020 [TKG/IMS] New application
    06/07/2020 [TKG/IMS] tambah perhitungan break schedule 1+2
    23/10/2020 [TKG/IMS] tambah feature select all
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOTVerificationDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmOTVerification mFrmParent;
        private string mSQL = string.Empty, mPGCode = string.Empty;

        #endregion

        #region Constructor

        public FrmOTVerificationDlg(FrmOTVerification FrmParent, string PGCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPGCode = PGCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#", 
                        "DNo", 
                        "Date",
                        "Employee's Code",
                        
                        //6-10
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department", 
                        "OT Date",
                        
                        //11-15
                        "Requested"+Environment.NewLine+"Start",
                        "Requested"+Environment.NewLine+"End",
                        "Log"+Environment.NewLine+"Start",
                        "Log"+Environment.NewLine+"End",
                        "Verified"+Environment.NewLine+"Start",
                        
                        //16-20
                        "Verified"+Environment.NewLine+"End",
                        "Duration"+Environment.NewLine+"Recess",
                        "Duration"+Environment.NewLine+"Log",
                        "Duration"+Environment.NewLine+"Verification",
                        "Rate",

                        //21-23
                        "Amount",
                        "DeptCode",
                        "WSCode"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 0, 100, 130, 
                        
                        //6-10
                        200, 130, 200, 200, 150, 
                        
                        //11-15
                        100, 100, 100, 100, 100, 

                        //16-20
                        100, 130, 130, 130, 130,

                        //21-23
                        130, 0, 0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
            Sm.GrdFormatDec(Grd1, new int[] { 17, 18, 19, 20, 21 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 10 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 12, 13, 14, 15, 16 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
           
            SQL.AppendLine("Select B.DocNo, B.DNo, A.DocDt, "); 	
            SQL.AppendLine("B.EmpCode, C.EmpName, C.EmpCodeOld, D.PosName, E.DeptName, A.OTDt, B.OTStartTm, B.OTEndTm, C.DeptCode ");
            SQL.AppendLine("From TblOTRequestHdr A ");
            SQL.AppendLine("Inner Join TblOTRequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Not Exists(");
            SQL.AppendLine("        Select 1 ");
            SQL.AppendLine("        From TblOTVerificationHdr T1, TblOTVerificationDtl T2, TblOTRequestHdr T3 ");
            SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.Status In ('O', 'A') ");
            SQL.AppendLine("        And T2.OTRequestDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.OTRequestDocNo=B.DocNo ");
            SQL.AppendLine("        And T2.OTRequestDNo=B.DNo ");
            SQL.AppendLine("        And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode And C.PGCode Is Not Null And C.PGCode=@PGCode ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            
            
            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 8, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = " Concat(B.DocNo, B.DNo)='***' ";
                string key = string.Empty;    
                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        key = string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, r, 2), Sm.GetGrdStr(mFrmParent.Grd1, r, 3));
                        if (key.Length != 0)
                        {
                            Filter += " Or (Concat(B.DocNo, B.DNo)=@key" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@key" + r.ToString(), key);
                        }
                    }
                }
                if (Filter.Length > 0)
                    Filter = " And Not(" + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<string>(ref cm, "@PGCode", mPGCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName", "C.EmpCodeOld" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[] 
                        { 
                            //0
                           "DocNo", 
                           //1-5
                           "DNo", "DocDt", "EmpCode", "EmpName", "EmpCodeOld", 
                           //6-10
                           "PosName", "DeptName", "OTDt", "OTStartTm", "OTEndTm",
                           //11
                           "DeptCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 11);
                            Grd.Cells[Row, 13].Value = null;
                            Grd.Cells[Row, 14].Value = null;
                            Grd.Cells[Row, 15].Value = null;
                            Grd.Cells[Row, 16].Value = null;
                            Grd.Cells[Row, 17].Value = 0m;
                            Grd.Cells[Row, 18].Value = 0m;
                            Grd.Cells[Row, 19].Value = 0m;
                            Grd.Cells[Row, 20].Value = 0m;
                            Grd.Cells[Row, 21].Value = 0m;
                            Grd.Cells[Row, 23].Value = null;
                        }, true, false, false, false
                    );
                if (Grd1.Rows.Count != 0) ProcessData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 21);
                        mFrmParent.Grd1.Cells[Row1, 22].Value = null;
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 23);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 16, 17, 18, 19, 20, 21 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var key = string.Concat(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3));
            for (int r= 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(key, string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, r, 2), Sm.GetGrdStr(mFrmParent.Grd1, r, 3)))) 
                    return true;
            return false;
        }

        #endregion

        #region Additional Method

        private void ProcessData()
        {
            var l1 = new List<Data1>();
            var l2 = new List<Data2>();
            var l3 = new List<Data3>();
            var l4 = new List<Data4>();

            ProcessData1(ref l1);

            if (l1.Count == 0) return;

            ProcessData2(ref l1, ref l3);
            ProcessData3(ref l1, ref l2);

            l1.Clear();

            ProcessData4(ref l4, ref l2, ref l3);

            l2.Clear();
            l3.Clear();

            ProcessData5(ref l4);

            l4.Clear();
        }

        private void ProcessData1(ref List<Data1> l1)
        {
            var l = new List<Data1>();
            string EmpCodeTemp = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                EmpCodeTemp = Sm.GetGrdStr(Grd1, r, 5);
                if (EmpCodeTemp.Length > 0)
                    l.Add(new Data1() { EmpCode = EmpCodeTemp });
            }

            if (l.Count == 0) return;

            foreach (var x in l.Select(s => new { s.EmpCode }).Distinct())
                l1.Add(new Data1() { EmpCode = x.EmpCode });

            l.Clear();
        }

        private void ProcessData2(ref List<Data1> l1, ref List<Data3> l3)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            
            string Filter = " EmpCode='***' ";
            int i = 0;
            foreach (var x in l1)
            {
                Filter += " Or EmpCode=@EmpCode" + i.ToString();
                Sm.CmParam<string>(ref cm, "@EmpCode" + i.ToString(), x.EmpCode);
                i++;
            }
            Filter = " And ( " + Filter + ") ";
            
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDocDt2)).AddDays(1)));
            
            SQL.AppendLine("Select EmpCode, Dt, Left(Tm, 4) Tm ");
            SQL.AppendLine("From TblAttendanceLog ");
            SQL.AppendLine("Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By EmpCode, Dt, Tm;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new Data3()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            Tm = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessData3(ref List<Data1> l1, ref List<Data2> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = " A.EmpCode='***' ";
            int i = 0;
            foreach (var x in l1)
            {
                Filter += " Or A.EmpCode=@EmpCode" + i.ToString();
                Sm.CmParam<string>(ref cm, "@EmpCode" + i.ToString(), x.EmpCode);
                i++;
            }
            Filter = " And ( " + Filter + ") ";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.WSCode, ");
            SQL.AppendLine("B.HolidayInd, B.In1, B.Out1, B.In2, B.Out2, B.In4, B.Out4, ");
            SQL.AppendLine("C.AtdVerifyStartTm, C.AtdVerifyEndTm ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, A.Dt, SubString(A.InOutA1, 9, 4) AtdVerifyStartTm, SubString(A.InOutA1, 21, 4) AtdVerifyEndTm ");
            SQL.AppendLine("    From TblAtdDtl A ");
            SQL.AppendLine("    Inner Join TblAtdHdr B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And A.InOutA1 Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(") C On A.EmpCode=C.EmpCode And A.Dt=C.Dt ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.EmpCode, A.Dt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "EmpCode", 
                    //1-5
                    "Dt", "WSCode", "HolidayInd", "In1", "Out1", 
                    //6-10
                    "In2", "Out2", "In4", "Out4", "AtdVerifyStartTm", 
                    //11
                    "AtdVerifyEndTm"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Data2()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            WSCode = Sm.DrStr(dr, c[2]),
                            HolidayInd = Sm.DrStr(dr, c[3])=="Y",
                            In1 = Sm.DrStr(dr, c[4]),
                            Out1 = Sm.DrStr(dr, c[5]),
                            In2 = Sm.DrStr(dr, c[6]),
                            Out2 = Sm.DrStr(dr, c[7]),
                            In4 = Sm.DrStr(dr, c[8]),
                            Out4 = Sm.DrStr(dr, c[9]),
                            AtdVerifyStartTm = Sm.DrStr(dr, c[10]),
                            AtdVerifyEndTm = Sm.DrStr(dr, c[11])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessData4(ref List<Data4> l4, ref List<Data2> l2, ref List<Data3> l3)
        {
            string EmpCodeTemp = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                EmpCodeTemp = Sm.GetGrdStr(Grd1, r, 5);
                if (EmpCodeTemp.Length > 0)
                    l4.Add(new Data4() 
                    { 
                        OTRequestDocNo = Sm.GetGrdStr(Grd1, r, 2),
                        OTRequestDNo = Sm.GetGrdStr(Grd1, r, 3),
                        EmpCode = EmpCodeTemp,
                        OTDt = Sm.Left(Sm.GetGrdDate(Grd1, r, 10), 8),
                        OTDt2 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Sm.GetGrdDate(Grd1, r, 10)).AddDays(1)), 8),
                        OTRequestStartTm = Sm.GetGrdStr(Grd1, r, 11).Replace(":", ""),
                        OTRequestEndTm = Sm.GetGrdStr(Grd1, r, 12).Replace(":", ""),
                        LogStartTm = string.Empty,
                        LogEndTm = string.Empty,
                        OTVerificationStartTm = string.Empty,
                        OTVerificationEndTm = string.Empty,
                        DurationRecess = 0m,
                        DurationLog = 0m,
                        DurationVerification = 0m,
                        Rate = 0m,
                        Amt = 0m
                    });
            }

            foreach (var i in l4.OrderBy(o => o.EmpCode).ThenBy(o2 => o2.OTDt))
            {
                foreach (var j in l2.Where(w => Sm.CompareStr(w.EmpCode, i.EmpCode) && Sm.CompareStr(w.Dt, i.OTDt)))
                {
                    i.WSCode = j.WSCode;
                    i.HolidayInd = j.HolidayInd;
                    i.In1 = j.In1;
                    i.Out1 = j.Out1;
                    i.In2 = j.In2;
                    i.Out2 = j.Out2;
                    i.In4 = j.In4;
                    i.Out4 = j.Out4;
                    i.AtdVerifyStartTm = j.AtdVerifyStartTm;
                    i.AtdVerifyEndTm = j.AtdVerifyEndTm;
                    break;
                }
            }

            foreach (var i in l4.OrderBy(o => o.EmpCode).ThenBy(o2 => o2.OTDt))
            {
                string value1 = string.Empty, value2 = string.Empty, value3 = string.Empty, value4 = string.Empty; // temporary variable untuk menyimpan nilai
                decimal value5 = 0m; // temporary variable untuk menyimpan nilai
                bool
                    IsExists = false,
                    Is1Day = Sm.CompareDtTm(i.OTRequestStartTm, i.OTRequestEndTm) <= 0;

                if (i.HolidayInd) // hari libur
                {
                    #region Hari Libur

                    if (Is1Day)
                    {
                        #region Hari Libur (dalam 1 hari)

                        value1 = string.Concat(i.OTDt, i.OTRequestStartTm);
                        value2 = string.Concat(i.OTDt, i.OTRequestEndTm);
                        IsExists = false;

                        foreach (var x in l3.Where(w =>
                            Sm.CompareStr(w.EmpCode, i.EmpCode) &&
                            Sm.CompareStr(w.Dt, i.OTDt)
                        ).OrderBy(o => o.EmpCode).ThenBy(o2 => o2.Dt).ThenBy(o3 => o3.Tm))
                        {
                            IsExists = true;
                            value3 = string.Concat(x.Dt, x.Tm);
                            if (Sm.CompareDtTm(value3, value1) <= 0)
                            {
                                // Log <= OT Request Start
                                i.LogStartTm = x.Tm;
                                i.OTVerificationStartTm = i.OTRequestStartTm;
                            }
                            else
                            {
                                if (i.OTVerificationStartTm.Length == 0)
                                {
                                    if (Sm.CompareDtTm(value3, value2) < 0)
                                    {
                                        // Log < OT Request End
                                        i.LogStartTm = x.Tm;
                                        i.OTVerificationStartTm = x.Tm;
                                    }
                                }
                                break;
                            }
                        }

                        if (IsExists && i.OTVerificationStartTm.Length > 0)
                        {
                            foreach (var x in l3.Where(w =>
                                Sm.CompareStr(w.EmpCode, i.EmpCode) &&
                                Sm.CompareDtTm(w.Dt, i.OTDt) >= 0
                            ).OrderBy(o => o.EmpCode).ThenBy(o2 => o2.Dt).ThenBy(o3 => o3.Tm))
                            {
                                value3 = string.Concat(x.Dt, x.Tm);
                                if (Sm.CompareDtTm(value2, value3) <= 0)
                                {
                                    // Log >= OT Request End
                                    i.LogEndTm = x.Tm;
                                    i.OTVerificationEndTm = i.OTRequestEndTm;
                                    break;
                                }
                                else
                                {
                                    if (Sm.CompareDtTm(value3, value2) < 0 && Sm.CompareDtTm(value1, value3) < 0)
                                    {
                                        // Log < OT Request End dan Log > OT Request Start
                                        i.LogEndTm = x.Tm;
                                        i.OTVerificationEndTm = x.Tm;
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region Hari Libur (beda hari)

                        value1 = string.Concat(i.OTDt, i.OTRequestStartTm);
                        value2 = string.Concat(i.OTDt2, i.OTRequestEndTm);
                        IsExists = false;

                        foreach (var x in l3.Where(w =>
                            Sm.CompareStr(w.EmpCode, i.EmpCode) &&
                            Sm.CompareStr(w.Dt, i.OTDt)
                        ).OrderBy(o => o.EmpCode).ThenBy(o2 => o2.Dt).ThenBy(o3 => o3.Tm))
                        {
                            IsExists = true;
                            value3 = string.Concat(x.Dt, x.Tm);
                            if (Sm.CompareDtTm(value3, value1) <= 0)
                            {
                                // Log <= OT Request Start
                                i.LogStartTm = x.Tm;
                                i.OTVerificationStartTm = i.OTRequestStartTm;
                            }
                            else
                            {
                                if (i.OTVerificationStartTm.Length == 0)
                                {
                                    if (Sm.CompareDtTm(value3, value2) < 0)
                                    {
                                        // Log < OT Request End
                                        i.LogStartTm = x.Tm;
                                        i.OTVerificationStartTm = x.Tm;
                                    }
                                }
                                break;
                            }
                        }

                        if (IsExists && i.OTVerificationStartTm.Length > 0)
                        {
                            foreach (var x in l3.Where(w =>
                                Sm.CompareStr(w.EmpCode, i.EmpCode) &&
                                Sm.CompareDtTm(w.Dt, i.OTDt) >= 0
                            ).OrderBy(o => o.EmpCode).ThenBy(o2 => o2.Dt).ThenBy(o3 => o3.Tm))
                            {
                                value3 = string.Concat(x.Dt, x.Tm);
                                if (Sm.CompareDtTm(value2, value3) <= 0)
                                {
                                    // Log >= OT Request End
                                    i.LogEndTm = x.Tm;
                                    i.OTVerificationEndTm = i.OTRequestEndTm;
                                    break;
                                }
                                else
                                {
                                    if (Sm.CompareDtTm(value3, value2) < 0 && Sm.CompareDtTm(value1, value3) < 0)
                                    {
                                        // Log < OT Request End dan Log > OT Request Start
                                        i.LogEndTm = x.Tm;
                                        i.OTVerificationEndTm = x.Tm;
                                    }
                                }
                            }
                        }

                        #endregion
                    }

                    #endregion

                    #region menghitung jam istirahat

                    if (i.OTVerificationStartTm.Length > 0 && i.OTVerificationEndTm.Length > 0)
                    {
                        if (i.In2.Length > 0 && i.Out2.Length > 0)
                        {
                            value1 = string.Concat(i.OTDt, i.In2);
                            if (Sm.CompareDtTm(i.In2, i.Out2) <= 0)
                                value2 = string.Concat(i.OTDt, i.Out2);
                            else
                                value2 = string.Concat(i.OTDt2, i.Out2);
                            value3 = string.Concat(i.OTDt, i.OTVerificationStartTm);
                            if (Sm.CompareDtTm(i.OTVerificationStartTm, i.OTVerificationEndTm) <= 0)
                                value4 = string.Concat(i.OTDt, i.OTVerificationEndTm);
                            else
                                value4 = string.Concat(i.OTDt2, i.OTVerificationEndTm);
                            value5 = ComputeDuration(value2, value1);

                            if (Sm.CompareDtTm(value3, value1) <= 0 &&
                                Sm.CompareDtTm(value2, value4) <= 0)
                                i.DurationRecess += value5;
                        }

                        if (i.In4.Length > 0 && i.Out4.Length > 0)
                        {
                            value1 = string.Concat(i.OTDt, i.In4);
                            if (Sm.CompareDtTm(i.In4, i.Out4) <= 0)
                                value2 = string.Concat(i.OTDt, i.Out4);
                            else
                                value2 = string.Concat(i.OTDt2, i.Out4);
                            value3 = string.Concat(i.OTDt, i.OTVerificationStartTm);
                            if (Sm.CompareDtTm(i.OTVerificationStartTm, i.OTVerificationEndTm) <= 0)
                                value4 = string.Concat(i.OTDt, i.OTVerificationEndTm);
                            else
                                value4 = string.Concat(i.OTDt2, i.OTVerificationEndTm);
                            value5 = ComputeDuration(value2, value1);

                            if (Sm.CompareDtTm(value3, value1) <= 0 &&
                                Sm.CompareDtTm(value2, value4) <= 0)
                                i.DurationRecess += value5;
                        }
                        if (i.DurationRecess < 0) i.DurationRecess = 0m;
                    }

                    #endregion
                }
                else
                {
                    // masuk kerja
                    if (Sm.CompareDtTm(i.OTRequestStartTm, i.In1) <= 0)
                    {
                        #region sebelum masuk kerja

                        value1 = string.Concat(i.OTDt, i.OTRequestStartTm);
                        value2 = string.Concat(i.OTDt, i.OTRequestEndTm);
                        IsExists = false;

                        foreach (var x in l3.Where(w =>
                            Sm.CompareStr(w.EmpCode, i.EmpCode) &&
                            Sm.CompareStr(w.Dt, i.OTDt)
                        ).OrderBy(o => o.EmpCode).ThenBy(o2 => o2.Dt).ThenBy(o3 => o3.Tm))
                        {
                            IsExists = true;
                            value3 = string.Concat(x.Dt, x.Tm);
                            if (Sm.CompareDtTm(value3, value1) <= 0)
                            {
                                // Log <= OT Request Start
                                i.LogStartTm = x.Tm;
                                i.OTVerificationStartTm = i.OTRequestStartTm;
                            }
                            else
                            {
                                if (i.OTVerificationStartTm.Length == 0)
                                {
                                    if (Sm.CompareDtTm(value3, value2) < 0)
                                    {
                                        // Log < OT Request End
                                        i.LogStartTm = x.Tm;
                                        i.OTVerificationStartTm = x.Tm;
                                    }
                                }
                                break;
                            }
                        }

                        if (IsExists && i.OTVerificationStartTm.Length > 0)
                        {
                            foreach (var x in l3.Where(w =>
                                Sm.CompareStr(w.EmpCode, i.EmpCode) &&
                                Sm.CompareStr(w.Dt, i.OTDt)
                            ).OrderBy(o => o.EmpCode).ThenBy(o2 => o2.Dt).ThenBy(o3 => o3.Tm))
                            {
                                value3 = string.Concat(x.Dt, x.Tm);
                                if (Sm.CompareDtTm(value2, value3) <= 0)
                                {
                                    // Log >= OT Request End
                                    i.LogEndTm = x.Tm;
                                    i.OTVerificationEndTm = i.OTRequestEndTm;
                                    break;
                                }
                                else
                                {
                                    if (Sm.CompareDtTm(value3, value2) < 0 && Sm.CompareDtTm(value1, value3) < 0)
                                    {
                                        // Log < OT Request End dan Log > OT Request Start
                                        i.LogEndTm = x.Tm;
                                        i.OTVerificationEndTm = x.Tm;
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region setelah pulang kerja

                        if (Is1Day)
                        {
                            #region masuk kerja dalam 1 hari

                            value1 = string.Concat(i.OTDt, i.OTRequestStartTm);
                            value2 = string.Concat(i.OTDt, i.OTRequestEndTm);
                            IsExists = false;

                            foreach (var x in l3.Where(w =>
                                Sm.CompareStr(w.EmpCode, i.EmpCode) &&
                                Sm.CompareStr(w.Dt, i.OTDt)
                            ).OrderBy(o => o.EmpCode).ThenBy(o2 => o2.Dt).ThenBy(o3 => o3.Tm))
                            {
                                IsExists = true;
                                value3 = string.Concat(x.Dt, x.Tm);
                                if (Sm.CompareDtTm(value3, value1) <= 0)
                                {
                                    // Log <= OT Request Start
                                    i.LogStartTm = x.Tm;
                                    i.OTVerificationStartTm = i.OTRequestStartTm;
                                }
                                else
                                {
                                    if (i.OTVerificationStartTm.Length == 0)
                                    {
                                        if (Sm.CompareDtTm(value3, value2) < 0)
                                        {
                                            // Log < OT Request End
                                            i.LogStartTm = x.Tm;
                                            i.OTVerificationStartTm = x.Tm;
                                        }
                                    }
                                    break;
                                }
                            }

                            if (IsExists && i.OTVerificationStartTm.Length > 0)
                            {
                                foreach (var x in l3.Where(w =>
                                    Sm.CompareStr(w.EmpCode, i.EmpCode) &&
                                    Sm.CompareDtTm(w.Dt, i.OTDt) >= 0
                                ).OrderBy(o => o.EmpCode).ThenBy(o2 => o2.Dt).ThenBy(o3 => o3.Tm))
                                {
                                    value3 = string.Concat(x.Dt, x.Tm);
                                    if (Sm.CompareDtTm(value2, value3) <= 0)
                                    {
                                        // Log >= OT Request End
                                        i.LogEndTm = x.Tm;
                                        i.OTVerificationEndTm = i.OTRequestEndTm;
                                        break;
                                    }
                                    else
                                    {
                                        if (Sm.CompareDtTm(value3, value2) < 0 && Sm.CompareDtTm(value1, value3) < 0)
                                        {
                                            // Log < OT Request End dan Log > OT Request Start
                                            i.LogEndTm = x.Tm;
                                            i.OTVerificationEndTm = x.Tm;
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            #region masuk kerja beda hari

                            value1 = string.Concat(i.OTDt, i.OTRequestStartTm);
                            value2 = string.Concat(i.OTDt2, i.OTRequestEndTm);
                            IsExists = false;

                            foreach (var x in l3.Where(w =>
                                Sm.CompareStr(w.EmpCode, i.EmpCode) &&
                                Sm.CompareStr(w.Dt, i.OTDt)
                            ).OrderBy(o => o.EmpCode).ThenBy(o2 => o2.Dt).ThenBy(o3 => o3.Tm))
                            {
                                IsExists = true;
                                value3 = string.Concat(x.Dt, x.Tm);
                                if (Sm.CompareDtTm(value3, value1) <= 0)
                                {
                                    // Log <= OT Request Start
                                    i.LogStartTm = x.Tm;
                                    i.OTVerificationStartTm = i.OTRequestStartTm;
                                }
                                else
                                {
                                    if (i.OTVerificationStartTm.Length == 0)
                                    {
                                        if (Sm.CompareDtTm(value3, value2) < 0)
                                        {
                                            // Log < OT Request End
                                            i.LogStartTm = x.Tm;
                                            i.OTVerificationStartTm = x.Tm;
                                        }
                                    }
                                    break;
                                }
                            }

                            if (IsExists && i.OTVerificationStartTm.Length > 0)
                            {
                                foreach (var x in l3.Where(w =>
                                    Sm.CompareStr(w.EmpCode, i.EmpCode) &&
                                    Sm.CompareDtTm(w.Dt, i.OTDt) >= 0
                                ).OrderBy(o => o.EmpCode).ThenBy(o2 => o2.Dt).ThenBy(o3 => o3.Tm))
                                {
                                    value3 = string.Concat(x.Dt, x.Tm);
                                    if (Sm.CompareDtTm(value2, value3) <= 0)
                                    {
                                        // Log >= OT Request End
                                        i.LogEndTm = x.Tm;
                                        i.OTVerificationEndTm = i.OTRequestEndTm;
                                        break;
                                    }
                                    else
                                    {
                                        if (Sm.CompareDtTm(value3, value2) < 0 && Sm.CompareDtTm(value1, value3) < 0)
                                        {
                                            // Log < OT Request End dan Log > OT Request Start
                                            i.LogEndTm = x.Tm;
                                            i.OTVerificationEndTm = x.Tm;
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                        #endregion
                    }
                }

                if (i.OTVerificationStartTm.Length > 0 && i.OTVerificationEndTm.Length > 0)
                {
                    value1 = string.Concat(i.OTDt, i.OTVerificationStartTm);
                    if (Sm.CompareDtTm(i.OTVerificationStartTm, i.OTVerificationEndTm) <= 0)
                        value2 = string.Concat(i.OTDt, i.OTVerificationEndTm);
                    else
                        value2 = string.Concat(i.OTDt2, i.OTVerificationEndTm);
                    i.DurationVerification = ComputeDuration(value2, value1);
                    i.DurationVerification -= i.DurationRecess;
                    if (i.DurationVerification < 1)
                        i.DurationVerification = 0m;
                    else
                        i.DurationVerification = RoundTo30Minutes((double)i.DurationVerification);
                }

                if (i.LogStartTm.Length > 0 && i.LogEndTm.Length > 0)
                {
                    value1 = string.Concat(i.OTDt, i.LogStartTm);
                    if (Sm.CompareDtTm(i.LogStartTm, i.LogEndTm) <= 0)
                        value2 = string.Concat(i.OTDt, i.LogEndTm);
                    else
                        value2 = string.Concat(i.OTDt2, i.LogEndTm);
                    i.DurationLog = ComputeDuration(value2, value1);
                    if (i.DurationLog < 0) i.DurationLog = 0m;
                }

                if (i.HolidayInd) 
                    i.Rate = mFrmParent.mOTVerificationRateHoliday;
                else
                    i.Rate = mFrmParent.mOTVerificationRateWorkingday;

                i.Amt = i.Rate * i.DurationVerification;
            }
        }

        private void ProcessData5(ref List<Data4> l4)
        {
            string key = string.Empty;
            foreach (var i in l4)
            {
                key = string.Concat(i.OTRequestDocNo, i.OTRequestDNo);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.CompareStr(key, 
                        string.Concat(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 3))))
                    {
                        if (i.LogStartTm.Length > 0) Grd1.Cells[r, 13].Value = string.Concat(Sm.Left(i.LogStartTm, 2), ":", Sm.Right(i.LogStartTm, 2));
                        if (i.LogEndTm.Length > 0) Grd1.Cells[r, 14].Value = string.Concat(Sm.Left(i.LogEndTm, 2), ":", Sm.Right(i.LogEndTm, 2));
                        if (i.OTVerificationStartTm.Length > 0) Grd1.Cells[r, 15].Value = string.Concat(Sm.Left(i.OTVerificationStartTm, 2), ":", Sm.Right(i.OTVerificationStartTm, 2));
                        if (i.OTVerificationEndTm.Length > 0) Grd1.Cells[r, 16].Value = string.Concat(Sm.Left(i.OTVerificationEndTm, 2), ":", Sm.Right(i.OTVerificationEndTm, 2));
                        Grd1.Cells[r, 17].Value = i.DurationRecess;
                        Grd1.Cells[r, 18].Value = i.DurationLog;
                        Grd1.Cells[r, 19].Value = i.DurationVerification;
                        Grd1.Cells[r, 20].Value = i.Rate;
                        Grd1.Cells[r, 21].Value = i.Amt;
                        Grd1.Cells[r, 23].Value = i.WSCode;
                        break;
                    }
                }
            }
        }

        private decimal ComputeDuration(string Value1, string Value2)
        {
            return Convert.ToDecimal((Sm.ConvertDateTime(Value1) - Sm.ConvertDateTime(Value2)).TotalHours);
        }

        private decimal RoundTo30Minutes(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0.5)
                return (decimal)(Value2 + 0.5);
            else
            {
                if (Value - Value2 == 0)
                    return (decimal)(Value);
                else
                {
                    if (Value - Value2 > 0 && Value - Value2 < 0.5)
                        return (decimal)(Value2);
                    else
                        return (decimal)(Value);
                }
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 && Sm.GetGrdStr(Grd1, 0, 2).Length > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r= 0; r< Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion

        #endregion

        #region Class

        private class Data1
        {
            public string EmpCode { get; set; }
        }

        private class Data2
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string WSCode { get; set; }
            public bool HolidayInd { get; set; }
            public string In1 { get; set; }
            public string Out1 { get; set; }
            public string In2 { get; set; }
            public string Out2 { get; set; }
            public string In4 { get; set; }
            public string Out4 { get; set; }
            public string AtdVerifyStartTm { get; set; }
            public string AtdVerifyEndTm { get; set; }
        }

        private class Data3
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm { get; set; }
        }

        private class Data4
        {
            public string OTRequestDocNo { get; set; }
            public string OTRequestDNo { get; set; }
            public string EmpCode { get; set; }
            public string OTDt { get; set; }
            public string OTDt2 { get; set; } // H+1 untuk kebutuhan proses
            public string OTRequestStartTm { get; set; }
            public string OTRequestEndTm { get; set; }
            public string LogStartTm { get; set; }
            public string LogEndTm { get; set; }
            public string OTVerificationStartTm { get; set; }
            public string OTVerificationEndTm { get; set; }
            public string AtdVerifyStartTm { get; set; }
            public string AtdVerifyEndTm { get; set; }
            public string WSCode { get; set; }
            public bool HolidayInd { get; set; }
            public string In1 { get; set; }
            public string Out1 { get; set; }
            public string In2 { get; set; }
            public string Out2 { get; set; }
            public string In4 { get; set; }
            public string Out4 { get; set; }
            public decimal DurationRecess { get; set; }
            public decimal DurationLog { get; set; }
            public decimal DurationVerification { get; set; }
            public decimal Rate { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}
