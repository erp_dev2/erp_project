﻿#region Update
/*
    28/11/2022 [MAU/BBT] Menu baru
    16/12/2022 [MAU/BBT] Penambahan kolom Active dan Cancel 
    23/12/2022 [MAU/BBT] BUG : menambah kolom createby, createdate, updateby, updatedate
    29/12/2022 [DITA/BBT] tambah kolom status
    10/01/2023 [MAU/BBT] tambah kolom complete 
    12/01/2023 [SET/BBT] Penyesuain nama kolom Grd1
    13/01/2023 [IBL/BBT] nama kolom property category code blm sesuai
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPropertyInventory mFrmParent;

        #endregion

        #region Constructor

        public FrmPropertyInventoryFind(FrmPropertyInventory FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                //GetParameter();
                SetLuePropertyCategory(ref LuePropertyCategory);
                //SetLueStatus(ref LueStatus);
                //Sm.SetLue(LueStatus, "1");
                SetGrd();

                
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        private void SetGrd()
        {

            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Property Code",
                        "Date of Registration",
                        "Property Name",
                        "Initial Cost Center",
                        "Site",
                        
                        //6-10
                        "Property Inventory Value",
                        "Property Category",
                        "Inventory "+Environment.NewLine+" Qty",
                        "UoM",
                        "Remaining Stock "+Environment.NewLine+" Qty",
                        
                        //11-15
                        "Remaining Stock "+Environment.NewLine+" Value",
                        "COA",
                        "COA "+Environment.NewLine+" Description",
                        "Property "+Environment.NewLine+"Category Code",
                        "Active",

                        //16-20
                        "Cancel",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",

                        //21-24
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time", 
                        "Status",
                        "Complete"


                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        200, 120, 200, 350, 250, 
                        
                        //6-10
                        200, 250, 100, 80, 120, 

                        //11-15
                        120, 150, 250, 100, 60,

                        //16-20
                        60, 100, 100, 100, 100,

                        //21-24
                        100,100, 120, 60
                        
                        
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 10, 11 }, 0);
            //Sm.GrdColCheck(Grd1, new int[] { 5, 7, 22, 23, 24, 25 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 , 18, 21 });
            Sm.GrdColCheck(Grd1, new int[] { 15, 16 , 24});
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            //Sm.GrdColInvisible(Grd1, new int[] { 34, 35, 36, 37, 38, 39 }, false);
            //Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 28, 29, 30, 31, 32, 33 }, mFrmParent.mIsAssetShowAdditionalInformation);
            //Sm.GrdColInvisible(Grd1, new int[] { 41, 42 }, mIsAssetUseMultiProfitCenterFilter);
            //Sm.GrdColInvisible(Grd1, new int[] { 43 }, mFrmParent.mIsRecvForAssetShowPOLocalDocNo);
            //Grd1.Cols[40].Move(9);
            //Grd1.Cols[41].Move(16);
            //Grd1.Cols[42].Move(17);
            //Grd1.Cols[43].Move(2);
            Grd1.Cols[16].Move(2);
            Grd1.Cols[15].Move(2);
            Grd1.Cols[23].Move(4);
            Grd1.Cols[24].Move(2);

            Sm.GrdColInvisible(Grd1, new int[] { 14,17,18,19,20,21,22 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14,17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 1=1  ";
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                // filter
                Sm.FilterStr(ref Filter, ref cm, TxtPropertyInventoryCode.Text, new string[] { "A.PropertyCode", "A.PropertyName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePropertyCategory), "A.PropertyCategoryCode", true);



                SQL.AppendLine(" SELECT  A.PropertyCode, A.RegistrationDt, A.ActInd, A.CancelInd, A.PropertyName, B.CCName, C.SiteName, A.PropertyInventoryValue, A.PropertyCategoryCode , D.PropertyCategoryName,  ");
                SQL.AppendLine(" A.InventoryQty, A.UomCode, A.RemStockQty, A.RemStockValue, D.AcNo1 , E.AcDesc ,  A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
                SQL.AppendLine(" Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' End As StatusDesc, A.CompleteInd ");
                SQL.AppendLine(" FROM tblpropertyinventoryhdr A ");
                SQL.AppendLine(" LEFT JOIN tblcostcenter B ON A.CCCode = B.CCCode ");
                SQL.AppendLine(" LEFT JOIN tblsite C ON A.SiteCode = C.SiteCode ");
                SQL.AppendLine(" LEFT JOIN tblpropertyinventorycategory D ON A.PropertyCategoryCode = D.PropertyCategoryCode ");
                SQL.AppendLine(" LEFT JOIN tblcoa E ON D.AcNo1 = E.AcNo ");



                SQL.AppendLine(Filter);

                SQL.AppendLine(" Order By A.PropertyCode ; ");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "PropertyCode", 
                    
                            //1-5
                            "RegistrationDt", "PropertyName", "CCName", "SiteName", "PropertyInventoryValue",  
                    
                            //6-10
                            "PropertyCategoryName", "InventoryQty", "UomCode", "RemStockQty", "RemStockValue", 
                    
                            //11-15
                            "AcNo1", "AcDesc", "PropertyCategoryCode" , "ActInd", "CancelInd",

                            //16-20
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "StatusDesc",

                            //21
                            "CompleteInd"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);

                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 16, 15);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);


                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 24, 21);


                        }, true, false, false, false
                    );
                //iGSubtotalManager.BackColor = Color.LightSalmon;
                //iGSubtotalManager.ShowSubtotalsInCells = true;
                //iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 12, 17 });

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }
        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        public void SetLuePropertyCategory(ref DXE.LookUpEdit Lue)
        {
            //Sm.SetLue2(
            //    ref Lue,
            //    "Select PropertyCategoryCode As Col1, PropertyCategoryName As Col2 From TblPropertyInventoryCategory where actind = 'Y' " +
            //    "Order By PropertyCategoryName",
            //    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            try
            {
                //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                //ItCode = Sm.GetGrdStr(Grd1, Row, 1);

                var SQL = new StringBuilder();

                SQL.AppendLine(" Select PropertyCategoryCode As Col1, PropertyCategoryName As Col2 From TblPropertyInventoryCategory where Actind = 'Y' ");
                SQL.AppendLine(" Order By PropertyCategoryName ");


                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events
        private void LuePropertyCategory_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePropertyCategory, new Sm.RefreshLue1(SetLuePropertyCategory));
            Sm.FilterLueSetCheckEdit(this, sender);
            //ChkPropertyCategory.Checked = true;

        }

        private void ChkPropertyCategoryCode_CheckedChanged(object sender, EventArgs e)
        {
             Sm.FilterSetLookUpEdit(this, sender, "Property Category");
            // ChkPropertyCategory.Checked = false;
            //Sm.SetLue(LuePropertyCategory, string.Empty);
            //Sm.SetLue(SetLuePropertyCategory, "1");
        }
        private void TxtPropertyInventoryCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPropertyInventoryCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property");
        }




        #endregion

        #endregion
    }
}
