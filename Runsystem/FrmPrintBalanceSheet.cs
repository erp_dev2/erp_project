﻿#region update
/*
    27/10/2017 [ARI] Header sesuai draft MAI
    19/07/2018 [TKG] logic perhitungan nomor rekening parent diubah untuk mengakomodasi apabila anak nomor rekening anak yg panjangnya tidak sama.
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPrintBalanceSheet : RunSystem.FrmBase11
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            startYr = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mPOPrintOutCompanyLogo = "1",
            mBSPrintOutCompanyLogo = "1",
            mAccountingRptStartFrom = string.Empty,
            mBalanceSheetDraft = "0"
            ;

        private bool mIsEntityMandatory = false,
            mIsRptBalanceSheetCurrentEarningUseProfitLoss = false;

        #endregion

        #region Constructor

        public FrmPrintBalanceSheet(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                var CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt.DateTime = Sm.ConvertDate(CurrentDate);
                SetLueEntCode(ref LueEntCode);
                GetParameter();
                if (mIsEntityMandatory) LblEntCode.ForeColor = Color.Red;
                else
                {
                    LblEntCode.Visible = false;
                    LueEntCode.Visible = false;
                    LblEntCode.ForeColor = Color.Black;
                }
                if (mAccountingRptStartFrom.Length > 0)
                {
                    Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                    Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                }
                else
                {
                    Sl.SetLueYr(LueStartFrom, string.Empty);
                    Sm.SetLue(LueStartFrom, CurrentDate.Substring(0, 4));
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Print Data

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            mPOPrintOutCompanyLogo = Sm.GetParameter("POPrintOutCompanyLogo");
            mBSPrintOutCompanyLogo = Sm.GetParameter("BSPrintOutCompanyLogo");

            if (Sm.IsDteEmpty(DteDocDt, "Date") || (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity")) || IsStartFromBeSame()) return;

            var StartFrom = Sm.GetLue(LueStartFrom);

            try
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;
                Cursor.Current = Cursors.WaitCursor;

                string[] TableName = { "DocDt", "PrintBalanceSheet1", "PrintBalanceSheet12", "PrintBalanceSheet2", "PrintBalanceSheet3", "TotalBalanceSheet", "Logo" };

                List<IList> myLists = new List<IList>();

                var lCOA = new List<COA>();
                var lDocDt = new List<Header>();
                var lBalanceSheet1 = new List<PrintBalanceSheet1>();
                var lBalanceSheet12 = new List<PrintBalanceSheet12>();
                var lBalanceSheet2 = new List<PrintBalanceSheet2>();
                var lBalanceSheet3 = new List<PrintBalanceSheet3>();
                var lTotalBalanceSheet = new List<TotalBalanceSheet>();
                var lDisplayedCOA = new List<DisplayedCOA>();
                var lSpecialCaseCOA = new List<SpecialCaseCOA>();
                var lEntityCOA = new List<EntityCOA>();
                var lLogo = new List<Logo>();

                #region Proses
                Process1(ref lCOA);
                Process8(ref lDisplayedCOA);
                Process9(ref lSpecialCaseCOA);
                if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    Process10(ref lEntityCOA);
                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA);
                        Process3(ref lCOA, StartFrom);
                    }
                    else
                    {
                        Process2(ref lCOA);
                        Process3(ref lCOA, string.Empty);
                    }
                    Process4(ref lCOA);
                    Process5(ref lCOA);
                    Process6(ref lCOA);
                    Process7(ref lCOA);
                    Process11(ref lCOA);

                    #region Filter By Entity
                    if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    {
                        if (lEntityCOA.Count > 0)
                        {
                            if (lDisplayedCOA.Count > 0)
                            {
                                for (var l = 0; l < lEntityCOA.Count; l++)
                                {
                                    for (var i = 0; i < lDisplayedCOA.Count; i++)
                                    {
                                        if (lEntityCOA[l].AcNo == lDisplayedCOA[i].AcNo)
                                        {
                                            for (int j = 0; j < lCOA.Count; j++)
                                            {
                                                if (lDisplayedCOA[i].AcNo == lCOA[j].AcNo)
                                                {
                                                    if (Sm.Left(lCOA[j].AcNo, 1) == "1")
                                                    {
                                                        if (lCOA[j].AcNo.Length > 2)
                                                        {
                                                            // aktiva lancar
                                                            if (Sm.Left(lCOA[j].AcNo, 3) == "1.1")
                                                            {
                                                                lBalanceSheet1.Add(new PrintBalanceSheet1()
                                                                {
                                                                    AcNo = lCOA[j].AcNo,
                                                                    AcDesc = lCOA[j].AcDesc,
                                                                    AcType = lCOA[j].AcType,
                                                                    Parent = lCOA[j].Parent,
                                                                    Level = lCOA[j].Level,
                                                                    //HasChild = lCOA[j].HasChild,
                                                                    Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                                    OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                                    OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                                    MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                                    MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                                    CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                                    CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                                    YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                                    YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                                    Balance = Math.Abs(lCOA[j].Balance)
                                                                }
                                                                );
                                                            }
                                                            // aktiva tetap
                                                            else
                                                            {
                                                                if (lSpecialCaseCOA.Count > 0)
                                                                {
                                                                    bool specials = false;
                                                                    for (int k = 0; k < lSpecialCaseCOA.Count; k++)
                                                                    {
                                                                        if (lCOA[j].AcNo == lSpecialCaseCOA[k].AcNo)
                                                                        {
                                                                            specials = true;
                                                                            break;
                                                                        }
                                                                    }

                                                                    if (specials)
                                                                    {
                                                                        lBalanceSheet12.Add(new PrintBalanceSheet12()
                                                                        {
                                                                            AcNo = lCOA[j].AcNo,
                                                                            AcDesc = lCOA[j].AcDesc,
                                                                            AcType = lCOA[j].AcType,
                                                                            Parent = lCOA[j].Parent,
                                                                            Level = lCOA[j].Level,
                                                                            //HasChild = lCOA[j].HasChild,
                                                                            Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                                            OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                                            OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                                            MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                                            MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                                            CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                                            CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                                            YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                                            YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                                            Balance = lCOA[j].Balance
                                                                        }
                                                                    );
                                                                    }
                                                                    else
                                                                    {
                                                                        lBalanceSheet12.Add(new PrintBalanceSheet12()
                                                                        {
                                                                            AcNo = lCOA[j].AcNo,
                                                                            AcDesc = lCOA[j].AcDesc,
                                                                            AcType = lCOA[j].AcType,
                                                                            Parent = lCOA[j].Parent,
                                                                            Level = lCOA[j].Level,
                                                                            //HasChild = lCOA[j].HasChild,
                                                                            Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                                            OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                                            OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                                            MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                                            MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                                            CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                                            CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                                            YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                                            YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                                            Balance = Math.Abs(lCOA[j].Balance)
                                                                        }
                                                                    );
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    lBalanceSheet12.Add(new PrintBalanceSheet12()
                                                                    {
                                                                        AcNo = lCOA[j].AcNo,
                                                                        AcDesc = lCOA[j].AcDesc,
                                                                        AcType = lCOA[j].AcType,
                                                                        Parent = lCOA[j].Parent,
                                                                        Level = lCOA[j].Level,
                                                                        //HasChild = lCOA[j].HasChild,
                                                                        Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                                        OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                                        OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                                        MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                                        MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                                        CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                                        CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                                        YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                                        YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                                        Balance = Math.Abs(lCOA[j].Balance)
                                                                    }
                                                                    );
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            lBalanceSheet1.Add(new PrintBalanceSheet1()
                                                            {
                                                                AcNo = lCOA[j].AcNo,
                                                                AcDesc = lCOA[j].AcDesc,
                                                                AcType = lCOA[j].AcType,
                                                                Parent = lCOA[j].Parent,
                                                                Level = lCOA[j].Level,
                                                                //HasChild = lCOA[j].HasChild,
                                                                Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                                OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                                OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                                MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                                MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                                CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                                CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                                YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                                YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                                Balance = Math.Abs(lCOA[j].Balance)
                                                            }
                                                        );
                                                        }
                                                    }

                                                    // kewajiban
                                                    else if (Sm.Left(lCOA[j].AcNo, 1) == "2")
                                                    {
                                                        lBalanceSheet2.Add(new PrintBalanceSheet2()
                                                        {
                                                            AcNo = lCOA[j].AcNo,
                                                            AcDesc = lCOA[j].AcDesc,
                                                            AcType = lCOA[j].AcType,
                                                            Parent = lCOA[j].Parent,
                                                            Level = lCOA[j].Level,
                                                            //HasChild = lCOA[j].HasChild,
                                                            Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                            OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                            OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                            MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                            MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                            CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                            CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                            YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                            YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                            Balance = Math.Abs(lCOA[j].Balance)
                                                        }
                                                        );
                                                    }

                                                    // modal
                                                    else if (Sm.Left(lCOA[j].AcNo, 1) == "3")
                                                    {
                                                        if (lSpecialCaseCOA.Count > 0)
                                                        {
                                                            bool specials = false;
                                                            for (int k = 0; k < lSpecialCaseCOA.Count; k++)
                                                            {
                                                                if (lCOA[j].AcNo == lSpecialCaseCOA[k].AcNo)
                                                                {
                                                                    specials = true;
                                                                    break;
                                                                }
                                                            }

                                                            if (specials)
                                                            {
                                                                lBalanceSheet3.Add(new PrintBalanceSheet3()
                                                                {
                                                                    AcNo = lCOA[j].AcNo,
                                                                    AcDesc = lCOA[j].AcDesc,
                                                                    AcType = lCOA[j].AcType,
                                                                    Parent = lCOA[j].Parent,
                                                                    Level = lCOA[j].Level,
                                                                    //HasChild = lCOA[j].HasChild,
                                                                    Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                                    OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                                    OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                                    MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                                    MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                                    CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                                    CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                                    YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                                    YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                                    Balance = lCOA[j].Balance
                                                                }
                                                            );
                                                            }
                                                            else
                                                            {
                                                                lBalanceSheet3.Add(new PrintBalanceSheet3()
                                                                {
                                                                    AcNo = lCOA[j].AcNo,
                                                                    AcDesc = lCOA[j].AcDesc,
                                                                    AcType = lCOA[j].AcType,
                                                                    Parent = lCOA[j].Parent,
                                                                    Level = lCOA[j].Level,
                                                                    //HasChild = lCOA[j].HasChild,
                                                                    Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                                    OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                                    OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                                    MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                                    MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                                    CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                                    CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                                    YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                                    YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                                    Balance = Math.Abs(lCOA[j].Balance)
                                                                }
                                                            );
                                                            }
                                                        }
                                                        else
                                                        {
                                                            lBalanceSheet3.Add(new PrintBalanceSheet3()
                                                            {
                                                                AcNo = lCOA[j].AcNo,
                                                                AcDesc = lCOA[j].AcDesc,
                                                                AcType = lCOA[j].AcType,
                                                                Parent = lCOA[j].Parent,
                                                                Level = lCOA[j].Level,
                                                                //HasChild = lCOA[j].HasChild,
                                                                Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                                OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                                OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                                MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                                MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                                CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                                CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                                YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                                YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                                Balance = Math.Abs(lCOA[j].Balance)
                                                            }
                                                            );
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                decimal vTotal1 = 0m, vTotal2 = 0m, vTotal3 = 0m;

                                for (int i = 0; i < lCOA.Count; i++)
                                {
                                    if (Sm.Left(lCOA[i].AcNo, 1) != "4" && Sm.Left(lCOA[i].AcNo, 1) != "5" && lCOA[i].Parent.Length == 0)
                                    {
                                        if (Sm.Left(lCOA[i].AcNo, 1) == "1")
                                        {
                                            vTotal1 += lCOA[i].Balance;
                                        }
                                        else if (Sm.Left(lCOA[i].AcNo, 1) == "2")
                                        {
                                            vTotal2 += lCOA[i].Balance;
                                        }
                                        else if (Sm.Left(lCOA[i].AcNo, 1) == "3")
                                        {
                                            vTotal3 += lCOA[i].Balance;
                                        }
                                    }
                                }

                                lTotalBalanceSheet.Add(new TotalBalanceSheet()
                                {
                                    Total1 = Math.Abs(vTotal1),
                                    Total2 = Math.Abs(vTotal2),
                                    Total3 = Math.Abs(vTotal3)
                                });

                            }
                        }
                    }
                    #endregion

                    #region Not Filtered By Entity
                    else
                    {
                        if (lDisplayedCOA.Count > 0)
                        {
                            for (var i = 0; i < lDisplayedCOA.Count; i++)
                            {
                                for (int j = 0; j < lCOA.Count; j++)
                                {
                                    if (lDisplayedCOA[i].AcNo == lCOA[j].AcNo)
                                    {
                                        if (Sm.Left(lCOA[j].AcNo, 1) == "1")
                                        {
                                            if (lCOA[j].AcNo.Length > 2)
                                            {
                                                // aktiva lancar
                                                if (Sm.Left(lCOA[j].AcNo, 3) == "1.1")
                                                {
                                                    lBalanceSheet1.Add(new PrintBalanceSheet1()
                                                    {
                                                        AcNo = lCOA[j].AcNo,
                                                        AcDesc = lCOA[j].AcDesc,
                                                        AcType = lCOA[j].AcType,
                                                        Parent = lCOA[j].Parent,
                                                        Level = lCOA[j].Level,
                                                        //HasChild = lCOA[j].HasChild,
                                                        Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                        OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                        OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                        MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                        MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                        CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                        CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                        YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                        YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                        Balance = Math.Abs(lCOA[j].Balance)
                                                    }
                                                    );
                                                }
                                                // aktiva tetap
                                                else
                                                {
                                                    if (lSpecialCaseCOA.Count > 0)
                                                    {
                                                        bool specials = false;
                                                        for (int k = 0; k < lSpecialCaseCOA.Count; k++)
                                                        {
                                                            if (lCOA[j].AcNo == lSpecialCaseCOA[k].AcNo)
                                                            {
                                                                specials = true;
                                                                break;
                                                            }
                                                        }

                                                        if (specials)
                                                        {
                                                            lBalanceSheet12.Add(new PrintBalanceSheet12()
                                                            {
                                                                AcNo = lCOA[j].AcNo,
                                                                AcDesc = lCOA[j].AcDesc,
                                                                AcType = lCOA[j].AcType,
                                                                Parent = lCOA[j].Parent,
                                                                Level = lCOA[j].Level,
                                                                //HasChild = lCOA[j].HasChild,
                                                                Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                                OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                                OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                                MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                                MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                                CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                                CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                                YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                                YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                                Balance = lCOA[j].Balance
                                                            }
                                                        );
                                                        }
                                                        else
                                                        {
                                                            lBalanceSheet12.Add(new PrintBalanceSheet12()
                                                            {
                                                                AcNo = lCOA[j].AcNo,
                                                                AcDesc = lCOA[j].AcDesc,
                                                                AcType = lCOA[j].AcType,
                                                                Parent = lCOA[j].Parent,
                                                                Level = lCOA[j].Level,
                                                                //HasChild = lCOA[j].HasChild,
                                                                Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                                OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                                OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                                MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                                MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                                CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                                CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                                YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                                YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                                Balance = Math.Abs(lCOA[j].Balance)
                                                            }
                                                        );
                                                        }
                                                    }
                                                    else
                                                    {
                                                        lBalanceSheet12.Add(new PrintBalanceSheet12()
                                                        {
                                                            AcNo = lCOA[j].AcNo,
                                                            AcDesc = lCOA[j].AcDesc,
                                                            AcType = lCOA[j].AcType,
                                                            Parent = lCOA[j].Parent,
                                                            Level = lCOA[j].Level,
                                                            //HasChild = lCOA[j].HasChild,
                                                            Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                            OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                            OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                            MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                            MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                            CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                            CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                            YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                            YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                            Balance = Math.Abs(lCOA[j].Balance)
                                                        }
                                                        );
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                lBalanceSheet1.Add(new PrintBalanceSheet1()
                                                {
                                                    AcNo = lCOA[j].AcNo,
                                                    AcDesc = lCOA[j].AcDesc,
                                                    AcType = lCOA[j].AcType,
                                                    Parent = lCOA[j].Parent,
                                                    Level = lCOA[j].Level,
                                                    //HasChild = lCOA[j].HasChild,
                                                    Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                    OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                    OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                    MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                    MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                    CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                    CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                    YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                    YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                    Balance = Math.Abs(lCOA[j].Balance)
                                                }
                                            );
                                            }
                                        }

                                        // kewajiban
                                        else if (Sm.Left(lCOA[j].AcNo, 1) == "2")
                                        {
                                            lBalanceSheet2.Add(new PrintBalanceSheet2()
                                            {
                                                AcNo = lCOA[j].AcNo,
                                                AcDesc = lCOA[j].AcDesc,
                                                AcType = lCOA[j].AcType,
                                                Parent = lCOA[j].Parent,
                                                Level = lCOA[j].Level,
                                                //HasChild = lCOA[j].HasChild,
                                                Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                Balance = Math.Abs(lCOA[j].Balance)
                                            }
                                            );
                                        }

                                        // modal
                                        else if (Sm.Left(lCOA[j].AcNo, 1) == "3")
                                        {
                                            if (lSpecialCaseCOA.Count > 0)
                                            {
                                                bool specials = false;
                                                for (int k = 0; k < lSpecialCaseCOA.Count; k++)
                                                {
                                                    if (lCOA[j].AcNo == lSpecialCaseCOA[k].AcNo)
                                                    {
                                                        specials = true;
                                                        break;
                                                    }
                                                }

                                                if (specials)
                                                {
                                                    lBalanceSheet3.Add(new PrintBalanceSheet3()
                                                    {
                                                        AcNo = lCOA[j].AcNo,
                                                        AcDesc = lCOA[j].AcDesc,
                                                        AcType = lCOA[j].AcType,
                                                        Parent = lCOA[j].Parent,
                                                        Level = lCOA[j].Level,
                                                        //HasChild = lCOA[j].HasChild,
                                                        Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                        OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                        OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                        MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                        MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                        CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                        CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                        YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                        YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                        Balance = lCOA[j].Balance
                                                    }
                                                );
                                                }
                                                else
                                                {
                                                    lBalanceSheet3.Add(new PrintBalanceSheet3()
                                                    {
                                                        AcNo = lCOA[j].AcNo,
                                                        AcDesc = lCOA[j].AcDesc,
                                                        AcType = lCOA[j].AcType,
                                                        Parent = lCOA[j].Parent,
                                                        Level = lCOA[j].Level,
                                                        //HasChild = lCOA[j].HasChild,
                                                        Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                        OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                        OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                        MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                        MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                        CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                        CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                        YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                        YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                        Balance = Math.Abs(lCOA[j].Balance)
                                                    }
                                                );
                                                }
                                            }
                                            else
                                            {
                                                lBalanceSheet3.Add(new PrintBalanceSheet3()
                                                {
                                                    AcNo = lCOA[j].AcNo,
                                                    AcDesc = lCOA[j].AcDesc,
                                                    AcType = lCOA[j].AcType,
                                                    Parent = lCOA[j].Parent,
                                                    Level = lCOA[j].Level,
                                                    //HasChild = lCOA[j].HasChild,
                                                    Indent = ((lCOA[j].Level - 1) * 1) * 0.75F * 10F,

                                                    OpeningBalanceDAmt = lCOA[j].OpeningBalanceDAmt,
                                                    OpeningBalanceCAmt = lCOA[j].OpeningBalanceCAmt,
                                                    MonthToDateDAmt = lCOA[j].MonthToDateDAmt,
                                                    MonthToDateCAmt = lCOA[j].MonthToDateCAmt,
                                                    CurrentMonthDAmt = lCOA[j].CurrentMonthDAmt,
                                                    CurrentMonthCAmt = lCOA[j].CurrentMonthCAmt,
                                                    YearToDateDAmt = lCOA[j].YearToDateDAmt,
                                                    YearToDateCAmt = lCOA[j].YearToDateCAmt,
                                                    Balance = Math.Abs(lCOA[j].Balance)
                                                }
                                                );
                                            }
                                        }
                                    }
                                }
                            }

                            decimal vTotal1 = 0m, vTotal2 = 0m, vTotal3 = 0m;

                            for (int i = 0; i < lCOA.Count; i++)
                            {
                                if (Sm.Left(lCOA[i].AcNo, 1) != "4" && Sm.Left(lCOA[i].AcNo, 1) != "5" && lCOA[i].Parent.Length == 0)
                                {
                                    if (Sm.Left(lCOA[i].AcNo, 1) == "1")
                                    {
                                        vTotal1 += lCOA[i].Balance;
                                    }
                                    else if (Sm.Left(lCOA[i].AcNo, 1) == "2")
                                    {
                                        vTotal2 += lCOA[i].Balance;
                                    }
                                    else if (Sm.Left(lCOA[i].AcNo, 1) == "3")
                                    {
                                        vTotal3 += lCOA[i].Balance;
                                    }
                                }
                            }

                            lTotalBalanceSheet.Add(new TotalBalanceSheet()
                            {
                                Total1 = Math.Abs(vTotal1),
                                Total2 = Math.Abs(vTotal2),
                                Total3 = Math.Abs(vTotal3)
                            });

                        }
                    }
                    #endregion
                }

                if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                {
                    lDocDt.Add(new Header()
                    {
                        CompanyName = LueEntCode.Text.ToUpper(),
                        DocDt1 = string.Concat("01/Jan/", startYr),
                        DocDt2 = DteDocDt.Text
                    });
                }
                else
                {
                    lDocDt.Add(new Header()
                    {
                        CompanyName = Sm.GetParameter("ReportTitle1").ToUpper(),
                        DocDt1 = string.Concat("01/Jan/", startYr),
                        DocDt2 = DteDocDt.Text
                    });
                }

                myLists.Add(lDocDt);
                myLists.Add(lBalanceSheet1);
                myLists.Add(lBalanceSheet12);
                myLists.Add(lBalanceSheet2);
                myLists.Add(lBalanceSheet3);
                myLists.Add(lTotalBalanceSheet);

                lCOA.Clear();
                lDisplayedCOA.Clear();
                lSpecialCaseCOA.Clear();
                lEntityCOA.Clear();

                #endregion

                #region Logo
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName' ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    if(Sm.GetParameter("Doctitle")=="MSI")
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo(mBSPrintOutCompanyLogo));
                    else
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo(mPOPrintOutCompanyLogo));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                //0-1
                "CompanyLogo",
                "CompanyName",
               
                });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lLogo.Add(new Logo()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                CompanyName = Sm.DrStr(dr, c[1]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(lLogo);

                #endregion

                string RptName = Sm.GetParameter("FormPrintOutBS");
                if (RptName.Length > 0)
                    Sm.PrintReport(RptName, myLists, TableName, false);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mAcNoForCurrentEarning = Sm.GetParameter("AcNoForCurrentEarning");
            mAcNoForIncome = Sm.GetParameter("AcNoForIncome");
            mAcNoForCost = Sm.GetParameter("AcNoForCost");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
            mIsRptBalanceSheetCurrentEarningUseProfitLoss = Sm.GetParameterBoo("IsRptBalanceSheetCurrentEarningUseProfitLoss");
            mBalanceSheetDraft = Sm.GetParameter("BalanceSheetDraft");
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'CONSOLIDATE' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 ");
            SQL.AppendLine("From TblEntity T Where T.ActInd='Y' ");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void Process1(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AcNo, T.AcDesc, T.Parent, T.AcType ");
            SQL.AppendLine("From TblCOA T ");
            SQL.AppendLine("Where T.ActInd='Y'  ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            //ParentRow = 0,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COA> lCOA)
        {
            #region Old

            //var SQL = new StringBuilder();
            //var cm = new MySqlCommand();
            //int Temp = 0;

            //SQL.AppendLine("Select B.AcNo, B.Amt ");
            //SQL.AppendLine("From TblCOAOpeningBalanceHdr A, TblCOAOpeningBalanceDtl B ");
            //SQL.AppendLine("Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("And A.CancelInd='N' ");
            //SQL.AppendLine("And A.Yr=@Yr ");
            //if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
            //    SQL.AppendLine("And A.EntCode = @EntCode ");
            //SQL.AppendLine("Order By B.AcNo;");

            //Sm.CmParam<String>(ref cm, "@Yr", startYr);
            //Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    cm.Connection = cn;
            //    cm.CommandTimeout = 600;
            //    cm.CommandText = SQL.ToString();
            //    var dr = cm.ExecuteReader();
            //    var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Amt" });
            //    if (dr.HasRows)
            //    {
            //        while (dr.Read())
            //        {
            //            for (var i = Temp; i < lCOA.Count; i++)
            //            {
            //                if (string.Compare(lCOA[i].AcNo, dr.GetString(0)) == 0)
            //                {
            //                    if (lCOA[i].AcType == "D")
            //                        lCOA[i].OpeningBalanceDAmt = dr.GetDecimal(1);
            //                    if (lCOA[i].AcType == "C")
            //                        lCOA[i].OpeningBalanceCAmt = dr.GetDecimal(1);
            //                    Temp = i;
            //                    break;
            //                }
            //            }
            //        }
            //    }
            //    dr.Close();
            //}

            #endregion

            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A, TblCOAOpeningBalanceDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    B.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Yr=@Yr ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And A.EntCode = @EntCode ");
            SQL.AppendLine("Order By B.AcNo;");


            Sm.CmParam<String>(ref cm, "@Yr", startYr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();

                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<COA> lCOA, string StartFrom)
        {
            #region Old

            //var SQL = new StringBuilder();

            //SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            //SQL.AppendLine("From TblJournalHdr A, TblJournalDtl B ");
            //SQL.AppendLine("Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
            //SQL.AppendLine("And Substring(A.DocDt, 5, 2)<@Mth ");
            //if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
            //    SQL.AppendLine("And B.EntCode = @EntCode ");
            //SQL.AppendLine("Group By B.AcNo;");

            //var lJournal = new List<Journal>();
            //var cm = new MySqlCommand();
            //Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(0, 4));
            //Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            //Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    cm.Connection = cn;
            //    cm.CommandTimeout = 600;
            //    cm.CommandText = SQL.ToString();
            //    var dr = cm.ExecuteReader();
            //    var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
            //    if (dr.HasRows)
            //    {
            //        while (dr.Read())
            //        {
            //            lJournal.Add(new Journal()
            //            {
            //                AcNo = dr.GetString(0),
            //                DAmt = dr.GetDecimal(1),
            //                CAmt = dr.GetDecimal(2)
            //            });
            //        }
            //    }
            //    dr.Close();
            //}
            //if (lJournal.Count > 0)
            //{
            //    lJournal.OrderBy(x => x.AcNo);
            //    bool IsFirst = false;
            //    int Temp = 0;
            //    for (var i = 0; i < lJournal.Count; i++)
            //    {
            //        IsFirst = false;
            //        for (var j = Temp; j < lCOA.Count; j++)
            //        {
            //            if (
            //                lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
            //                lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
            //                )
            //            {
            //                if (!IsFirst)
            //                {
            //                    IsFirst = true;
            //                    Temp = j;
            //                }
            //                lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
            //                lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
            //                if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
            //                    break;
            //            }

            //        }
            //    }
            //}
            //lJournal.Clear();

            #endregion

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A, TblJournalDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            if (StartFrom.Length == 0)
            SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
            else
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
            SQL.AppendLine("And Left(A.DocDt, 6)<@YrMth ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And B.EntCode = @EntCode ");
            SQL.AppendLine("Group By B.AcNo;");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@YrMth", Sm.Left(Sm.GetDte(DteDocDt),6));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            if (StartFrom.Length > 0)
                Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<COA> lCOA)
        {
            #region Old

            //var SQL = new StringBuilder();

            //SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            //SQL.AppendLine("From TblJournalHdr A, TblJournalDtl B ");
            //SQL.AppendLine("Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("And Left(A.DocDt, 6) = @DocDt ");
            //if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
            //    SQL.AppendLine("And B.EntCode = @EntCode ");
            //SQL.AppendLine("Group By B.AcNo;");

            //var lJournal = new List<Journal>();
            //var cm = new MySqlCommand();
            //Sm.CmParam<String>(ref cm, "@DocDt", Sm.Left(Sm.GetDte(DteDocDt), 6));
            //Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    cm.Connection = cn;
            //    cm.CommandTimeout = 600;
            //    cm.CommandText = SQL.ToString();
            //    var dr = cm.ExecuteReader();
            //    var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
            //    if (dr.HasRows)
            //    {
            //        while (dr.Read())
            //        {
            //            lJournal.Add(new Journal()
            //            {
            //                AcNo = dr.GetString(0),
            //                DAmt = dr.GetDecimal(1),
            //                CAmt = dr.GetDecimal(2)
            //            });
            //        }
            //    }
            //    dr.Close();
            //}
            //if (lJournal.Count > 0)
            //{
            //    lJournal.OrderBy(x => x.AcNo);
            //    bool IsFirst = false;
            //    int Temp = 0;
            //    for (var i = 0; i < lJournal.Count; i++)
            //    {
            //        IsFirst = false;
            //        for (var j = Temp; j < lCOA.Count; j++)
            //        {
            //            if (
            //                lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
            //                lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
            //                )
            //            {
            //                if (!IsFirst)
            //                {
            //                    IsFirst = true;
            //                    Temp = j;
            //                }
            //                lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
            //                lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
            //                if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
            //                    break;
            //            }
            //        }
            //    }
            //}
            //lJournal.Clear();

            #endregion

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A, TblJournalDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            SQL.AppendLine("And Left(A.DocDt, 6)=@YrMth ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And B.EntCode = @EntCode ");
            SQL.AppendLine("Group By B.AcNo;");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@YrMth", Sm.Left(Sm.GetDte(DteDocDt), 6));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<COA> lCOA)
        {
            return;

            //if (
            //    mAcNoForCurrentEarning.Length == 0 ||
            //    mAcNoForIncome.Length == 0 ||
            //    mAcNoForCost.Length == 0
            //    ) return;

            //int
            //    CurrentProfiLossIndex = 0,
            //    IncomeIndex = 0,
            //    CostIndex = 0
            //    ;

            //byte Completed = 0;

            //for (var i = 0; i < lCOA.Count; i++)
            //{
            //    if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
            //    {
            //        CurrentProfiLossIndex = i;
            //        Completed += 1;
            //        if (Completed == 3) break;
            //    }

            //    if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
            //    {
            //        IncomeIndex = i;
            //        Completed += 1;
            //        if (Completed == 3) break;
            //    }

            //    if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
            //    {
            //        CostIndex = i;
            //        Completed += 1;
            //        if (Completed == 3) break;
            //    }
            //}

            //decimal Amt = 0m;

            ////Opening Balance
            //Amt =
            //    lCOA[IncomeIndex].OpeningBalanceDAmt
            //    + lCOA[CostIndex].OpeningBalanceDAmt
            //    - lCOA[IncomeIndex].OpeningBalanceCAmt
            //    - lCOA[CostIndex].OpeningBalanceCAmt
            //    ;
            //if (Amt > 0) lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt = Amt;
            //if (Amt < 0) lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt = Amt;


            //var CurrentProfiLossParentIndex = -1;
            //var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //var f = true;
            //bool test = true;
            //var iteration = 0;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            test = false;
            //            iteration = i;
            //            break;
            //        }
            //    }

            //    if (!test)
            //    {
            //        CurrentProfiLossParentIndex = iteration;
            //        if (Amt > 0) lCOA[CurrentProfiLossParentIndex].OpeningBalanceDAmt += Amt;
            //        if (Amt < 0) lCOA[CurrentProfiLossParentIndex].OpeningBalanceCAmt += Amt;

            //        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //        f = CurrentProfiLossParentAcNo.Length > 0;
            //        break;
            //    }
            //    else
            //    {
            //        CurrentProfiLossParentIndex = -1;
            //        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //        lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt = Amt;
            //        lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt = Amt;
            //        f = false;
            //        break;
            //    }
            //}

            ////Month To Date
            //Amt =
            //    lCOA[IncomeIndex].MonthToDateDAmt
            //    + lCOA[CostIndex].MonthToDateDAmt
            //    - lCOA[IncomeIndex].MonthToDateCAmt
            //    - lCOA[CostIndex].MonthToDateCAmt
            //    ;
            //if (Amt > 0) lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt;
            //if (Amt < 0) lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            //CurrentProfiLossParentIndex = -1;
            //CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //f = true;
            //test = true;
            //iteration = 0;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            test = false;
            //            iteration = i;
            //            break;
            //        }
            //    }

            //    if (!test)
            //    {
            //        CurrentProfiLossParentIndex = iteration;
            //        if (Amt > 0) lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += Amt;
            //        if (Amt < 0) lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

            //        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //        f = CurrentProfiLossParentAcNo.Length > 0;
            //        break;
            //    }
            //    else
            //    {
            //        CurrentProfiLossParentIndex = -1;
            //        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //        lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt;
            //        lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;
            //        f = false;
            //        break;
            //    }
            //}

            ////Current Month
            //Amt =
            //    lCOA[IncomeIndex].CurrentMonthDAmt
            //    + lCOA[CostIndex].CurrentMonthDAmt
            //    - lCOA[IncomeIndex].CurrentMonthCAmt
            //    - lCOA[CostIndex].CurrentMonthCAmt
            //    ;
            //if (Amt > 0) lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt;
            //if (Amt < 0) lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;
            //CurrentProfiLossParentIndex = -1;
            //CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //f = true;
            //test = true;
            //iteration = 0;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            test = false;
            //            iteration = i;
            //            break;
            //        }
            //    }

            //    if (!test)
            //    {
            //        CurrentProfiLossParentIndex = iteration;
            //        if (Amt > 0) lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += Amt;
            //        if (Amt < 0) lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

            //        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //        f = CurrentProfiLossParentAcNo.Length > 0;
            //        break;
            //    }
            //    else
            //    {
            //        CurrentProfiLossParentIndex = -1;
            //        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //        lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt;
            //        lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;
            //        f = false;
            //        break;
            //    }
            //}

            ////Year To Date
            //Amt =
            //    lCOA[IncomeIndex].YearToDateDAmt
            //    + lCOA[CostIndex].YearToDateDAmt
            //    - lCOA[IncomeIndex].YearToDateCAmt
            //    - lCOA[CostIndex].YearToDateCAmt
            //    ;
            //if (Amt > 0) lCOA[CurrentProfiLossIndex].YearToDateDAmt = Amt;
            //if (Amt < 0) lCOA[CurrentProfiLossIndex].YearToDateCAmt = Amt;
            //CurrentProfiLossParentIndex = -1;
            //CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //f = true;
            //test = true;
            //iteration = 0;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            test = false;
            //            iteration = i;
            //            break;
            //        }
            //    }

            //    if (!test)
            //    {
            //        CurrentProfiLossParentIndex = iteration;
            //        if (Amt > 0) lCOA[CurrentProfiLossParentIndex].YearToDateDAmt += Amt;
            //        if (Amt < 0) lCOA[CurrentProfiLossParentIndex].YearToDateCAmt += Amt;

            //        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //        f = CurrentProfiLossParentAcNo.Length > 0;
            //        break;
            //    }
            //    else
            //    {
            //        CurrentProfiLossParentIndex = -1;
            //        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //        lCOA[CurrentProfiLossIndex].YearToDateDAmt = Amt;
            //        lCOA[CurrentProfiLossIndex].YearToDateCAmt = Amt;
            //        f = false;
            //        break;
            //    }
            //}
        }

        private void Process6(ref List<COA> lCOA)
        {
            #region Old
            //for (var i = 0; i < lCOA.Count; i++)
            //{
            //    lCOA[i].YearToDateDAmt =
            //        lCOA[i].OpeningBalanceDAmt +
            //        lCOA[i].MonthToDateDAmt +
            //        lCOA[i].CurrentMonthDAmt;

            //    lCOA[i].YearToDateCAmt =
            //        lCOA[i].OpeningBalanceCAmt +
            //        lCOA[i].MonthToDateCAmt +
            //        lCOA[i].CurrentMonthCAmt;


            //    if (Sm.Left(lCOA[i].AcNo, 1) == "1")
            //        lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

            //    if (Sm.Left(lCOA[i].AcNo, 1) == "2")
            //        lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

            //    if (Sm.Left(lCOA[i].AcNo, 1) == "3")
            //        lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
            //}
            #endregion

            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt +
                    lCOA[i].CurrentMonthDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt +
                    lCOA[i].CurrentMonthCAmt;

                if (lCOA[i].AcType == "D")
                    lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                else
                    lCOA[i].Balance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
            }

        }

        private void Process7(ref List<COA> lCOA)
        {
            #region Old

            //var Parent = string.Empty;
            //var ParentLevel = 0;

            //for (var i = 0; i < lCOA.Count; i++)
            //{
            //    if (lCOA[i].Level == -1)
            //    {
            //        if (string.Compare(lCOA[i].Parent, Parent) == 0)
            //        {
            //            lCOA[i].Level = ParentLevel + 1;
            //        }
            //        else
            //        {
            //            for (var j = 0; j < lCOA.Count; j++)
            //            {
            //                if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
            //                {
            //                    Parent = lCOA[i].Parent;
            //                    ParentLevel = lCOA[j].Level;
            //                    lCOA[i].Level = lCOA[j].Level + 1;
            //                    break;
            //                }
            //            }
            //        }
            //    }
            //    for (var j = i + 1; j < lCOA.Count; j++)
            //    {
            //        if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
            //            break;
            //        else
            //        {
            //            if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
            //            {
            //                lCOA[i].HasChild = true;
            //                break;
            //            }
            //        }
            //    }
            //}

            #endregion

            var Parent = string.Empty;
            var ParentLevel = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process8(ref List<DisplayedCOA> lDisplayedCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct X.AcNo From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  Select AcNo ");
            SQL.AppendLine("  From TblCOA  ");
            SQL.AppendLine("  Where ActInd = 'Y' ");
            SQL.AppendLine("  And IfNull(Parent, '')='' ");
            SQL.AppendLine("  And Left(AcNo, 1) In ('1','2','3') ");
            SQL.AppendLine("  Union All ");
            SQL.AppendLine("  Select AcNo ");
            SQL.AppendLine("  From TblCOA ");
            SQL.AppendLine("  Where ActInd='Y' ");
            SQL.AppendLine("  And Parent Is Not Null ");
            SQL.AppendLine("  And Left(AcNo, 1) In ('1', '2', '3') ");
            SQL.AppendLine("  And AcNo In (Select Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("  Union All ");
            SQL.AppendLine("  Select AcNo ");
            SQL.AppendLine("  From TblCOA ");
            SQL.AppendLine("  Where ActInd = 'Y' ");
            SQL.AppendLine("  And Parent Is Not Null ");
            SQL.AppendLine("  And Find_In_Set(AcNo, (Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'DisplayedAcNoPrintOutBalanceSheet')) ");
            SQL.AppendLine(")X ");
            SQL.AppendLine("Order By X.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lDisplayedCOA.Add(new DisplayedCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process9(ref List<SpecialCaseCOA> lSpecialCaseCOA) 
        // akun COA yg tampilan balance nya sesuai dengan nilainya apakah dia plus atau minus
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo From TblCOA ");
            SQL.AppendLine("Where Find_In_Set(AcNo, (Select ParValue From TblParameter Where Parcode = 'SpecialCaseAcNoPrintOutBalanceSheet')) ");
            SQL.AppendLine("Order By AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lSpecialCaseCOA.Add(new SpecialCaseCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process10(ref List<EntityCOA> lEntityCOA)
        {
            #region Update

            //var cm = new MySqlCommand();
            //var SQL = new StringBuilder();

            //SQL.AppendLine("Select T.AcNo ");
            //SQL.AppendLine("From TblCOA T ");
            //if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
            //    SQL.AppendLine("Inner Join TblCOADtl V On T.AcNo = V.AcNo ");
            //SQL.AppendLine("Where T.ActInd='Y' ");
            //if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
            //    SQL.AppendLine("And V.EntCode = @EntCode ");
            //SQL.AppendLine("Order By T.AcNo; ");

            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    cm.Connection = cn;
            //    cm.CommandTimeout = 600;
            //    cm.CommandText = SQL.ToString();
            //    Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            //    var dr = cm.ExecuteReader();
            //    var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
            //    if (dr.HasRows)
            //    {
            //        while (dr.Read())
            //        {
            //            lEntityCOA.Add(new EntityCOA()
            //            {
            //                AcNo = Sm.DrStr(dr, c[0]),
            //            });
            //        }
            //    }
            //    dr.Close();
            //}

            #endregion

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And (Left(T.AcNo, 1) In ('1','2','3')) ");
            SQL.AppendLine("And U.EntCode = @EntCode ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process11(ref List<COA> lCOA)
        {
            if (mIsRptBalanceSheetCurrentEarningUseProfitLoss)
            {
                decimal IncomeD = 0m, IncomeC = 0m, CostD = 0m, CostC = 0m;

                foreach (var x in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForIncome)))
                {
                    IncomeD = x.YearToDateDAmt;
                    IncomeC = x.YearToDateCAmt;
                }

                foreach (var x in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForCost)))
                {
                    CostD = x.YearToDateDAmt;
                    CostC = x.YearToDateCAmt;
                }

                foreach (var i in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForCurrentEarning)))
                {

                    i.YearToDateDAmt = 0m;
                    i.YearToDateCAmt = 0m;
                    i.Balance = (IncomeC - IncomeD) - (CostD - CostC);
                }
            }
            else
            {
                //foreach (var i in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForCurrentEarning)))
                //{
                //    i.MonthToDateDAmt = 0m;
                //    i.CurrentMonthDAmt = 0m;
                //    i.YearToDateDAmt = i.OpeningBalanceDAmt;

                //    i.MonthToDateCAmt = 0m;
                //    i.CurrentMonthCAmt = 0m;
                //    i.YearToDateCAmt = i.OpeningBalanceCAmt;

                //    if (i.AcType == "D")
                //        i.Balance = i.YearToDateDAmt - i.YearToDateCAmt;
                //    else
                //        i.Balance = i.YearToDateCAmt - i.YearToDateDAmt;
                //}

                decimal IncomeD = 0m, IncomeC = 0m, CostD = 0m, CostC = 0m;

                foreach (var x in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForIncome)))
                {
                    IncomeD = x.YearToDateDAmt;
                    IncomeC = x.YearToDateCAmt;
                }

                foreach (var x in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForCost)))
                {
                    CostD = x.YearToDateDAmt;
                    CostC = x.YearToDateCAmt;
                }

                foreach (var i in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForCurrentEarning)))
                {

                    i.YearToDateDAmt = 0m;
                    i.YearToDateCAmt = 0m;
                    if (i.AcType == "D")
                    {
                        i.Balance = (i.OpeningBalanceDAmt - i.OpeningBalanceCAmt) + (IncomeC - IncomeD) - (CostD - CostC);
                    }
                    else
                    {
                        i.Balance = (i.OpeningBalanceCAmt - i.OpeningBalanceDAmt) + (IncomeC - IncomeD) - (CostD - CostC);
                    }
                }
            }
            if (mBalanceSheetDraft == "1")
            {
                foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(mAcNoForCurrentEarning, 1) && w.HasChild))
                {
                    i.YearToDateDAmt = 0m;
                    i.YearToDateCAmt = 0m;
                    i.Balance = 0m;
                }

                foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(mAcNoForCurrentEarning, 1) && !w.HasChild))
                {
                    foreach (var j in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(mAcNoForCurrentEarning, 1) && w.HasChild))
                    {
                        if (
                            i.AcNo.Count(x => x == '.') == j.AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, j.AcNo) ||
                            i.AcNo.Count(x => x == '.') != j.AcNo.Count(x => x == '.') && i.AcNo.StartsWith(j.AcNo)
                            )
                        {
                            j.YearToDateDAmt += i.YearToDateDAmt;
                            j.YearToDateCAmt += i.YearToDateCAmt;
                            j.Balance += i.Balance;
                            if (string.Compare(j.AcNo, i.AcNo) == 0)
                                break;
                        }
                    }
                }
            }
           
        }

        private bool IsStartFromBeSame()
        {
            int startfrom = int.Parse(Sm.GetLue(LueStartFrom));
            int Year =  int.Parse(Sm.Left(Sm.GetDte(DteDocDt), 4));

            if (startfrom != Year)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Start From should be as same as Date's Year");
                    return true;
                }
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            startYr = string.Empty;
            
            if(Sm.GetDte(DteDocDt).Length > 0)
                startYr = Sm.Left(Sm.GetDte(DteDocDt), 4);
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Header
        {
            public string CompanyName { get; set; }
            public string DocDt1 { get; set; }
            public string DocDt2 { get; set; }
        }

        private class PrintBalanceSheet1
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public float Indent { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
        }

        private class PrintBalanceSheet12
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public float Indent { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
        }

        private class PrintBalanceSheet2
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public float Indent { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
        }

        private class PrintBalanceSheet3
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public float Indent { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
        }

        private class TotalBalanceSheet
        {
            public decimal Total1 { get; set; }
            public decimal Total2 { get; set; }
            public decimal Total3 { get; set; }
        }

        private class DisplayedCOA
        {
            public string AcNo { get; set; }
        }

        private class SpecialCaseCOA
        {
            public string AcNo { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        class Logo
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string PrintBy { get; set; }
        }

        #endregion

    }
}
