﻿namespace RunSystem
{
    partial class FrmTraining
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtTrainingName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtTrainingCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueTrainingType = new DevExpress.XtraEditors.LookUpEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtMinScore = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTrainingName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTrainingCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingType.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinScore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 338);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Grd1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 338);
            // 
            // TxtTrainingName
            // 
            this.TxtTrainingName.EnterMoveNextControl = true;
            this.TxtTrainingName.Location = new System.Drawing.Point(120, 25);
            this.TxtTrainingName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTrainingName.Name = "TxtTrainingName";
            this.TxtTrainingName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTrainingName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTrainingName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTrainingName.Properties.Appearance.Options.UseFont = true;
            this.TxtTrainingName.Properties.MaxLength = 40;
            this.TxtTrainingName.Size = new System.Drawing.Size(319, 20);
            this.TxtTrainingName.TabIndex = 13;
            this.TxtTrainingName.Validated += new System.EventHandler(this.TxtTrainingName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(29, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Training Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTrainingCode
            // 
            this.TxtTrainingCode.EnterMoveNextControl = true;
            this.TxtTrainingCode.Location = new System.Drawing.Point(120, 4);
            this.TxtTrainingCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTrainingCode.Name = "TxtTrainingCode";
            this.TxtTrainingCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTrainingCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTrainingCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTrainingCode.Properties.Appearance.Options.UseFont = true;
            this.TxtTrainingCode.Properties.MaxLength = 16;
            this.TxtTrainingCode.Size = new System.Drawing.Size(76, 20);
            this.TxtTrainingCode.TabIndex = 10;
            this.TxtTrainingCode.Validated += new System.EventHandler(this.TxtTrainingCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(32, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Training Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(205, 4);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(65, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(8, 49);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 14);
            this.label5.TabIndex = 14;
            this.label5.Text = "Internal / External";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTrainingType
            // 
            this.LueTrainingType.EnterMoveNextControl = true;
            this.LueTrainingType.Location = new System.Drawing.Point(120, 46);
            this.LueTrainingType.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingType.Name = "LueTrainingType";
            this.LueTrainingType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingType.Properties.DropDownRows = 4;
            this.LueTrainingType.Properties.NullText = "[Empty]";
            this.LueTrainingType.Properties.PopupWidth = 500;
            this.LueTrainingType.Size = new System.Drawing.Size(319, 20);
            this.LueTrainingType.TabIndex = 15;
            this.LueTrainingType.ToolTip = "F4 : Show/hide list";
            this.LueTrainingType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingType.EditValueChanged += new System.EventHandler(this.LueTrainingType_EditValueChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.LueSiteCode);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.TxtTrainingName);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.LueTrainingType);
            this.panel3.Controls.Add(this.TxtTrainingCode);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 93);
            this.panel3.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(86, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "Site";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(120, 67);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 4;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 500;
            this.LueSiteCode.Size = new System.Drawing.Size(319, 20);
            this.LueSiteCode.TabIndex = 17;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtMinScore);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(528, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(244, 93);
            this.panel4.TabIndex = 16;
            // 
            // TxtMinScore
            // 
            this.TxtMinScore.EnterMoveNextControl = true;
            this.TxtMinScore.Location = new System.Drawing.Point(168, 3);
            this.TxtMinScore.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinScore.Name = "TxtMinScore";
            this.TxtMinScore.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMinScore.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinScore.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinScore.Properties.Appearance.Options.UseFont = true;
            this.TxtMinScore.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinScore.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinScore.Properties.MaxLength = 10;
            this.TxtMinScore.Size = new System.Drawing.Size(69, 20);
            this.TxtMinScore.TabIndex = 19;
            this.TxtMinScore.Validated += new System.EventHandler(this.TxtMinScore_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(97, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 14);
            this.label10.TabIndex = 18;
            this.label10.Text = "Min. Score";
            // 
            // Grd1
            // 
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 93);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(772, 245);
            this.Grd1.TabIndex = 20;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // FrmTraining
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 338);
            this.Name = "FrmTraining";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtTrainingName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTrainingCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingType.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinScore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtTrainingName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtTrainingCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingType;
        private System.Windows.Forms.Panel panel3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.TextEdit TxtMinScore;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
    }
}