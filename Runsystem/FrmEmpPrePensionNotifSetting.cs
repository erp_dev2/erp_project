﻿#region Update
/*
    05/04/2021 [ICA/PHT] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpPrePensionNotifSetting : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        private bool mIsDataEdit = false;

        internal FrmEmpPrePensionNotifSettingFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmpPrePensionNotifSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            Sl.SetLueSiteCode(ref LueSiteCode);
            SetGrd();
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-4
                        "",
                        "Employee"+Environment.NewLine+"Code",
                        "Employee"+Environment.NewLine+"Name",
                        "Department"
                    },
                    new int[] 
                    {
                        20,
                        20, 150, 200, 350
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0, 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueSiteCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3 });
                    LueSiteCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueSiteCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1 });
                    LueSiteCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueSiteCode
                    }, true);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1 });
                    LueSiteCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueSiteCode,
            });
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpPrePensionNotifSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            mIsDataEdit = false;
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            mIsDataEdit = true;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Sm.IsLueEmpty(LueSiteCode, "Site"))
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmEmpPrePensionNotifSettingDlg(this, Sm.GetLue(LueSiteCode)));
                }
            }

            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f1.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (!Sm.IsLueEmpty(LueSiteCode, "Site"))
            {
                if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmEmpPrePensionNotifSettingDlg(this, Sm.GetLue(LueSiteCode)));

                if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                {
                    var f1 = new FrmEmployee(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string SiteCode = Sm.GetLue(LueSiteCode);
            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveDataPIC(SiteCode, Row));

            Sm.ExecCommands(cml);

            ShowData(SiteCode);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                IsSiteAlreadyExist() ||
                IsGrdEmpty();
        }

        private bool IsSiteAlreadyExist()
        {
            if (!mIsDataEdit)
                return Sm.IsDataExist("Select 1 from TblEmpPrePensionNotifSetting where SiteCode = @Param", Sm.GetLue(LueSiteCode), "Site Already Exists.");
            else
                return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No Employee in the list.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveDataPIC(string SiteCode, int Row)
        {

            var SQL = new StringBuilder();
            if (mIsDataEdit)
            {
                SQL.AppendLine("Delete From TblEmpPrePensionNotifSetting where SiteCode = @SiteCode; ");
                mIsDataEdit = false;
            }
            SQL.AppendLine("Insert Into TblEmpPrePensionNotifSetting(SiteCode, EmpCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SiteCode, @EmpCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string SiteCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowDataPIC(SiteCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDataPIC(string SiteCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, C.DeptName ");
            SQL.AppendLine("From TblEmpPrePensionNotifSetting A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("Where A.SiteCode=@SiteCode ");
            SQL.AppendLine("Order By B.EmpName ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode", 
                    //1
                    "EmpName",
                    "DeptName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                }, false, false, true, false
                );
            Sm.SetLue(LueSiteCode, SiteCode);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #endregion
    }
}
