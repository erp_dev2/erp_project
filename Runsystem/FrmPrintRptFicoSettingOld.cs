﻿#region Update
/*
    24/08/2018 [HAR] dimunculin nilai di tahun sebelumnya, line, Font
    03/09/2018 [HAR] feedback spasi, garis dipanjangin,nilai diurutkan berdasarkan sequence
    13/09/2018 [HAR] BUG jika ada nilai minus
    16/09/2018 [HAR] tambah validasi hanya yang printind = 'Y' yang akan di print
    16/09/2018 [TKG] setting difilter berdasarkan otorisasi site dan entity difilter berdasarkan otorisasi group
    11/06/2019 [TKG] mempercepat proses
    13/11/2019 [HAR/TWC] BUG saat ada account yang sama digunakan lbh dari sekali dalam 1 reporting 
    27/01/2020 [DITA/KBN] Printout Neraca KBN
    20/04/2020 [DITA/KBN] Tambah parameter IsPrintRptFicoSettingYrAscOrder & IsPrintRptFicoSettingYr2DisplayMth
 *  04/05/2020 [HAR/KBN] tanbah parameter PrintFicoSettingLastYearBasedOnFilterMonth
    18/05/2020 [WED/KBN] bug saat loop data COA Fico Setting
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPrintRptFicoSettingOld : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mCOAAssetStartYr = string.Empty,
            mCOAAssetAcNo = string.Empty,
            mAccountingRptStartFrom = string.Empty;
        private bool 
            mIsCOAAssetUseStartYr = false,
            mIsEntityMandatory = false,
            mIsReportingFilterByEntity = false,
            mIsFilterByEnt = false,
            mIsFilterBySite = false,
            mIsPrintRptFicoSettingShowPercentage = false,
            mIsPrintRptFicoSettingYrAscOrder = false,
            mIsPrintRptFicoSettingYr2DisplayMth = false,
            mPrintFicoSettingLastYearBasedOnFilterMonth = false
            ;

        #endregion

        #region Constructor

        public FrmPrintRptFicoSettingOld(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnPrint.Visible = false;
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetLueFicoFormula(ref LueOptionCode);
                TxtStartYr.EditValue = Sm.GetLue(LueYr);

                SetLueEntCode(ref LueEntCode, string.Empty, mIsFilterByEnt ? "Y" : "N");
                if (mIsEntityMandatory) 
                    LblEntCode.ForeColor = Color.Red;
                else
                {
                    LblEntCode.Visible = false;
                    LueEntCode.Visible = false;
                    LblEntCode.ForeColor = Color.Black;
                }
                SetGrd();
                TxtStartYr.Enabled = false;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueEntCode(ref LookUpEdit Lue, string Code, string IsFilterByEnt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            if (IsFilterByEnt == "Y")
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=T.EntCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By Col2;");
            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void GetParameter()
        {
            mAcNoForCurrentEarning = Sm.GetParameter("AcNoForCurrentEarning");
            mAcNoForIncome = Sm.GetParameter("AcNoForIncome");
            mAcNoForCost = Sm.GetParameter("AcNoForCost");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsCOAAssetUseStartYr = Sm.GetParameterBoo("IsCOAAssetUseStartYr");
            mCOAAssetStartYr = Sm.GetParameter("COAAssetStartYr");
            mCOAAssetAcNo = Sm.GetParameter("COAAssetAcNo");
            mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
            mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
            mIsFilterByEnt = Sm.GetParameterBoo("IsFilterByEnt");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsPrintRptFicoSettingShowPercentage = Sm.GetParameterBoo("IsPrintRptFicoSettingShowPercentage");
            mIsPrintRptFicoSettingYr2DisplayMth = Sm.GetParameterBoo("IsPrintRptFicoSettingYr2DisplayMth");
            mIsPrintRptFicoSettingYrAscOrder = Sm.GetParameterBoo("IsPrintRptFicoSettingYrAscOrder");
            mPrintFicoSettingLastYearBasedOnFilterMonth = Sm.GetParameterBoo("PrintFicoSettingLastYearBasedOnFilterMonth");
        }

        private void SetGrd()
        {
            #region Grd1
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;

            
            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Description#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Balance#";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 2);
            Sm.GrdColInvisible(Grd1, new int[] { });
            #endregion

            #region Grd2
            Grd2.ReadOnly = true;
            Grd2.Header.Rows.Count = 2;
            Grd2.Cols.Count = 3;
            Grd2.FrozenArea.ColCount = 2;


            Grd2.Cols[0].Width = 250;
            Grd2.Header.Cells[0, 0].Value = "Account#";
            Grd2.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd2.Header.Cells[0, 0].SpanRows = 2;

            Grd2.Cols[1].Width = 250;
            Grd2.Header.Cells[0, 1].Value = "Description#";
            Grd2.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd2.Header.Cells[0, 1].SpanRows = 2;

            Grd2.Cols[2].Width = 250;
            Grd2.Header.Cells[0, 2].Value = "Balance#";
            Grd2.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd2.Header.Cells[0, 2].SpanRows = 2;

            Sm.GrdFormatDec(Grd2, new int[] { 2 }, 2);
            Sm.GrdColInvisible(Grd2, new int[] { });
            #endregion
            //for (int Index = 0; Index < Grd1.Cols.Count; Index++)
            //    Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || 
                Sm.IsLueEmpty(LueMth, "Month") ||
                (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity")) ||
                Sm.IsLueEmpty(LueOptionCode, "Type")) return;

            Cursor.Current = Cursors.WaitCursor;

            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            var StartFrom = Sm.GetLue(LueYr);
            var YrGrd2 = Convert.ToString(Decimal.Parse(Sm.GetLue(LueYr))-1);
            var StartFromGrd2 = Convert.ToString(Decimal.Parse(Sm.GetLue(LueYr)) - 1);
            var MthGrd2 = mPrintFicoSettingLastYearBasedOnFilterMonth ? Sm.GetLue(LueMth) : Convert.ToString(12);
            LblYrOld.Text = YrGrd2;

            try
            {
            #region grid 1
                Sm.ClearGrd(Grd1, true);
                Sm.ClearGrd(Grd2, true);
                var lCOA = new List<COA>();
                var lEntityCOA = new List<EntityCOA>();
                var lRptFicoSetting = new List<RptFicoSetting>();
                var lRptFicoSetting2 = new List<RptFicoSetting>();
                var lRptFicoSetting3 = new List<COAFicoSetting>();
                var lRptFicoSetting4 = new List<RptFicoSetting2>();

                Process1(ref lCOA);
                if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    Process8(ref lEntityCOA);
                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom);
                        Process3(ref lCOA, Yr, Mth, StartFrom);
                    }
                    else
                    {
                        Process2(ref lCOA, Yr);
                        Process3(ref lCOA, Yr, Mth, string.Empty);
                    }
                    Process4(ref lCOA, Yr, Mth);
                    Process5(ref lCOA);
                    Process6(ref lCOA);
                    Process7(ref lCOA);
                    Process9(ref lCOA);

                    ProcessFicoCOA(ref lRptFicoSetting);
                    if (lRptFicoSetting.Count > 0)
                    {
                        ProcessFicoCOA2(ref lRptFicoSetting, ref lRptFicoSetting2);
                    }

                    ProcessFicoCOA3(ref lRptFicoSetting3);

                    #region Filtered By Entity

                    if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    {
                        if (lEntityCOA.Count > 0 && lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();

                            //int r = 0;

                            foreach (var x in lEntityCOA.OrderBy(o => o.AcNo))
                            {
                                foreach (var y in lRptFicoSetting2.OrderBy(o => o.AcNo).Where(a => a.AcNo == x.AcNo))
                                {
                                    foreach (var z in lCOA.OrderBy(o => o.AcNo).Where(a => a.AcNo == y.AcNo))
                                    {
                                        lRptFicoSetting4.Add(new RptFicoSetting2()
                                        {
                                            FicoCode = y.FicoCode,
                                            SettingCode = y.SettingCode,
                                            SettingDesc = y.SettingDesc,
                                            Sequence = y.Sequence,
                                            Formula = y.Formula,
                                            AcNo = y.AcNo,
                                            Balance = z.YearToDateBalance
                                        });
                                        break;
                                    }
                                }
                            }

                            ProcessFicoCOA4(ref lRptFicoSetting3, ref lRptFicoSetting4, Grd1);
                            Grd1.EndUpdate();
                            
                        }
                    }

                    #endregion

                    #region Not Filtered By Entity

                    else
                    {
                        if (lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();
                            //for (var i = 0; i < lCOA.Count; i++)
                            //{
                            //    for (var j = 0; j < lRptFicoSetting2.Count; j++)
                            //    {
                            //        if (lCOA[i].AcNo == lRptFicoSetting2[j].AcNo)
                            //        {
                            //            lRptFicoSetting4.Add(new RptFicoSetting2()
                            //            {
                            //                FicoCode = lRptFicoSetting2[j].FicoCode,
                            //                SettingCode = lRptFicoSetting2[j].SettingCode,
                            //                SettingDesc = lRptFicoSetting2[j].SettingDesc,
                            //                Sequence = lRptFicoSetting2[j].Sequence,
                            //                Formula = lRptFicoSetting2[j].Formula,
                            //                AcNo = lRptFicoSetting2[j].AcNo,
                            //                Balance = lCOA[i].YearToDateBalance
                            //            });
                            //        }
                            //    }
                            //}

                            //int r = 0;

                            foreach (var x in lRptFicoSetting2.OrderBy(o => o.AcNo))
                            {
                                foreach (var y in lCOA.OrderBy(o => o.AcNo).Where(a => a.AcNo == x.AcNo))
                                {
                                    lRptFicoSetting4.Add(new RptFicoSetting2()
                                    {
                                        FicoCode = x.FicoCode,
                                        SettingCode = x.SettingCode,
                                        SettingDesc = x.SettingDesc,
                                        Sequence = x.Sequence,
                                        Formula = x.Formula,
                                        AcNo = x.AcNo,
                                        Balance = y.YearToDateBalance
                                    });
                                    break;
                                }
                            }

                            ProcessFicoCOA4(ref lRptFicoSetting3, ref lRptFicoSetting4, Grd1);
                            Grd1.EndUpdate();
                        }
                    }

                    #endregion

                    lCOA.Clear();
                    lEntityCOA.Clear();
                }
            #endregion

            #region grid 2
                var lCOAGrd2 = new List<COA>();
                var lEntityCOAGrd2 = new List<EntityCOA>();
                var lRptFicoSettingGrd2 = new List<RptFicoSetting>();
                var lRptFicoSetting2Grd2 = new List<RptFicoSetting>();
                var lRptFicoSetting3Grd2 = new List<COAFicoSetting>();
                var lRptFicoSetting4Grd2 = new List<RptFicoSetting2>();
                string mAcNo = string.Empty;

                Process1(ref lCOAGrd2);
                if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    Process8(ref lEntityCOAGrd2);
                if (lCOAGrd2.Count > 0)
                {
                    if (StartFromGrd2.Length > 0)
                    {
                        Process2(ref lCOAGrd2, StartFromGrd2);
                        Process3(ref lCOAGrd2, YrGrd2, MthGrd2.ToString(), StartFromGrd2);
                    }
                    else
                    {
                        Process2(ref lCOAGrd2, YrGrd2);
                        Process3(ref lCOAGrd2, YrGrd2, MthGrd2.ToString(), string.Empty);
                    }
                    Process4(ref lCOAGrd2, YrGrd2, MthGrd2.ToString());
                    Process5(ref lCOAGrd2);
                    Process6(ref lCOAGrd2);
                    Process7(ref lCOAGrd2);
                    Process9(ref lCOAGrd2);

                    ProcessFicoCOA(ref lRptFicoSettingGrd2);
                    if (lRptFicoSettingGrd2.Count > 0)
                    {
                        ProcessFicoCOA2(ref lRptFicoSettingGrd2, ref lRptFicoSetting2Grd2);
                    }

                    ProcessFicoCOA3(ref lRptFicoSetting3Grd2);

                    #region Filtered By Entity

                    if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    {
                        if (lEntityCOAGrd2.Count > 0 && lCOAGrd2.Count > 0)
                        {
                            Grd2.BeginUpdate();
                            //int r = 0;

                            foreach (var x in lEntityCOAGrd2.OrderBy(o => o.AcNo))
                            {
                                foreach (var y in lCOAGrd2.OrderBy(o => o.AcNo).Where(a => a.AcNo == x.AcNo))
                                {
                                    foreach (var z in lRptFicoSetting2Grd2.OrderBy(o => o.AcNo).Where(a => a.AcNo == y.AcNo))
                                    {
                                        lRptFicoSetting4Grd2.Add(new RptFicoSetting2()
                                        {
                                            FicoCode = z.FicoCode,
                                            SettingCode = z.SettingCode,
                                            SettingDesc = z.SettingDesc,
                                            Sequence = z.Sequence,
                                            Formula = z.Formula,
                                            AcNo = z.AcNo,
                                            Balance = y.YearToDateBalance
                                        });
                                        break;
                                    }
                                }
                            }

                            ProcessFicoCOA4(ref lRptFicoSetting3Grd2, ref lRptFicoSetting4Grd2, Grd2);
                            Grd2.EndUpdate();
                            
                        }
                    }

                    #endregion

                    #region Not Filtered By Entity

                    else
                    {
                        if (lCOAGrd2.Count > 0)
                        {
                            Grd2.BeginUpdate();
                            
                            //for (var i = 0; i < lCOAGrd2.Count; i++)
                            //{
                            //    for (var j = 0; j < lRptFicoSetting2Grd2.Count; j++)
                            //    {
                            //        if (lCOAGrd2[i].AcNo == lRptFicoSetting2Grd2[j].AcNo)
                            //        {
                            //            lRptFicoSetting4Grd2.Add(new RptFicoSetting2()
                            //            {
                            //                FicoCode = lRptFicoSetting2Grd2[j].FicoCode,
                            //                SettingCode = lRptFicoSetting2Grd2[j].SettingCode,
                            //                SettingDesc = lRptFicoSetting2Grd2[j].SettingDesc,
                            //                Sequence = lRptFicoSetting2Grd2[j].Sequence,
                            //                Formula = lRptFicoSetting2Grd2[j].Formula,
                            //                AcNo = lRptFicoSetting2Grd2[j].AcNo,
                            //                Balance = lCOAGrd2[i].YearToDateBalance
                            //            });
                            //        }
                            //    }
                            //}

                            //int r = 0;
                            foreach(var i in lRptFicoSetting2Grd2.OrderBy(o => o.AcNo))
                            {
                                foreach (var j in lCOAGrd2.OrderBy(o => o.AcNo).Where(x => x.AcNo == i.AcNo))
                                {
                                    lRptFicoSetting4Grd2.Add(new RptFicoSetting2()
                                    {
                                        FicoCode = i.FicoCode,
                                        SettingCode = i.SettingCode,
                                        SettingDesc = i.SettingDesc,
                                        Sequence = i.Sequence,
                                        Formula = i.Formula,
                                        AcNo = i.AcNo,
                                        Balance = j.YearToDateBalance
                                    });
                                    break;
                                }
                            }


                            ProcessFicoCOA4(ref lRptFicoSetting3Grd2, ref lRptFicoSetting4Grd2, Grd2);
                            Grd2.EndUpdate();
                        }
                    }

                    #endregion

                    lCOAGrd2.Clear();
                    lEntityCOAGrd2.Clear();
                }
                #endregion

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        //private void SetLueEntCode(ref LookUpEdit Lue)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
        //    SQL.AppendLine("Union All ");
        //    if (mIsReportingFilterByEntity)
        //    {
        //        SQL.AppendLine("    Select A.EntCode, B.EntName  From TblGroupEntity A ");
        //        SQL.AppendLine("    Inner Join TblEntity B On A.EntCode = B.EntCode ");
        //        SQL.AppendLine("    Where A.GrpCode In ( ");
        //        SQL.AppendLine("        Select GrpCode From TblUser ");
        //        SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
        //        SQL.AppendLine("    ) ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T; ");
        //    }

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}

        private void SetLueFicoFormula(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.RptFicoCode As Col1, T.RptFicoName As Col2 ");
            SQL.AppendLine("From TblRptFicoSettingHdr T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where (T.SiteCode Is Null Or ");
                SQL.AppendLine("(T.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void Process1(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;
          
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Left Join TblCOADtl B On A.AcNo=B.AcNo ");
                if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                    SQL.AppendLine("And B.EntCode=@EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Where A.ActInd='Y'  ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode).Length > 0 ? Sm.GetLue(LueEntCode) == "Consolidate" ? "" : Sm.GetLue(LueEntCode) : Sm.GetLue(LueEntCode));

                cm.CommandText = SQL.ToString();
                
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length==0?1:-1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            //int Temp = 0;
            //string AcNo = string.Empty;
            //decimal Amt = 0m;

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A, TblCOAOpeningBalanceDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            SQL.AppendLine("And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Yr=@Yr ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And IfNull(A.EntCode, '') = IfNull(@EntCode, '') ");
            SQL.AppendLine("Order By B.AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });

                        //AcNo = dr.GetString(0);
                        //Amt = dr.GetDecimal(1);
                        //for (var i = Temp; i < lCOA.Count; i++)
                        //{
                        //    if (string.Compare(lCOA[i].AcNo, AcNo) == 0)
                        //    {
                        //        if (lCOA[i].AcType == "D")
                        //            lCOA[i].OpeningBalanceDAmt = Amt;
                        //        if (lCOA[i].AcType == "C")
                        //            lCOA[i].OpeningBalanceCAmt = Amt;
                        //        Temp = i;
                        //        break;
                            
                        //    }
                        //}
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) &&
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.')
                            ||
                            lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.')) &&
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.')
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();
    
            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A, TblJournalDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("And Left(A.DocDt, 6)<@YrMth ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("And Left(A.DocDt, 6)<@YrMth ");
            }
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And IfNull(B.EntCode, '') = IfNull(@EntCode, '') ");
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By AcNo;");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length>0)
                Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                        
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<COA> lCOA, string Yr, string Mth)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A, TblJournalDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And Left(A.DocDt, 6)=@YrMth ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And IfNull(B.EntCode, '') = IfNull(@EntCode, '') ");
            SQL.AppendLine("Group By B.AcNo;");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<COA> lCOA)
        {
            return;
            if (
                mAcNoForCurrentEarning.Length == 0 ||
                mAcNoForIncome.Length == 0 ||
                mAcNoForCost.Length == 0
                ) return;

            int 
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed==3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }

            //var CurrentProfiLossParentIndex = -1;
            //var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;

            //for (var i = 0; i < lCOA.Count; i++)
            //{
            //    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //    {
            //        CurrentProfiLossParentIndex = i;
            //        break;
            //    }
            //}

            decimal Amt = 0m;
            
            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;
            //    (lCOA[IncomeIndex].OpeningBalanceCAmt - lCOA[IncomeIndex].OpeningBalanceDAmt)-
            //    (lCOA[CostIndex].OpeningBalanceDAmt - lCOA[CostIndex].OpeningBalanceCAmt);

            //lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt = 0m;
            //lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt = Amt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            CurrentProfiLossParentIndex = i;
            //            lCOA[CurrentProfiLossParentIndex].OpeningBalanceDAmt += 0m;
            //            lCOA[CurrentProfiLossParentIndex].OpeningBalanceCAmt += Amt;
                        
            //            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //            f = CurrentProfiLossParentAcNo.Length > 0;
            //            break;
            //        }
            //    }
            //}

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);
                
            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;
            
            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                        lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;
                        
                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);
                
            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                        lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;
                        
                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Year To Date
            //Amt =
            //    (lCOA[IncomeIndex].YearToDateCAmt - lCOA[IncomeIndex].YearToDateDAmt) -
            //    (lCOA[CostIndex].YearToDateDAmt - lCOA[CostIndex].YearToDateCAmt);
                
            //lCOA[CurrentProfiLossIndex].YearToDateDAmt = 0m;
            //lCOA[CurrentProfiLossIndex].YearToDateCAmt = Amt;
            //CurrentProfiLossParentIndex = -1;
            //CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //f = true;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            CurrentProfiLossParentIndex = i;
            //            lCOA[CurrentProfiLossParentIndex].YearToDateDAmt += 0m;
            //            lCOA[CurrentProfiLossParentIndex].YearToDateCAmt += Amt;
                        
            //            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //            f = CurrentProfiLossParentAcNo.Length > 0;
            //            break;
            //        }
            //    }
            //}
        }

        private void Process6(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt = 
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt +
                    lCOA[i].CurrentMonthDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt +
                    lCOA[i].CurrentMonthCAmt;
            }
        }

        private void Process7(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;
            //var ParentRow = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                        //lCOA[i].ParentRow = ParentRow;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                //ParentRow = j;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                //lCOA[i].ParentRow = ParentRow;
                                break;
                            }
                        }
                    }
                }
                for (var j = i+1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process8(ref List<EntityCOA> lEntityCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And U.EntCode = @EntCode ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process9(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].AcType == "D")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceDAmt +
                        lCOA[i].MonthToDateDAmt -
                        lCOA[i].MonthToDateCAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthDAmt -
                        lCOA[i].CurrentMonthCAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
                if (lCOA[i].AcType == "C")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateCAmt-
                        lCOA[i].MonthToDateDAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthCAmt -
                        lCOA[i].CurrentMonthDAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                }
            }
        }

        private void Process10(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Left Join TblCOADtl B On A.AcNo=B.AcNo ");
                if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                    SQL.AppendLine("And B.EntCode=@EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Where A.ActInd='Y'  ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode).Length > 0 ? Sm.GetLue(LueEntCode) == "Consolidate" ? "" : Sm.GetLue(LueEntCode) : Sm.GetLue(LueEntCode));

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessFicoCOA(ref List<RptFicoSetting> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            //var lRptFicoSetting = new List<RptFicoSetting>();

            SQL.AppendLine("Select B.RptFicoCode, B.SettingCode, B.SettingDesc, B.Sequence, B.Formula ");
            SQL.AppendLine("From TblRptFicoSettingHdr A ");
            SQL.AppendLine("Inner Join TblRptFicoSettingDtl B On A.RptFicoCode = B.RptFicoCode ");
            SQL.AppendLine("Where A.RptFicoCode = @FicoCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order by Cast(sequence As UNSIGNED); ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "SettingDesc", "Sequence", "Formula" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new RptFicoSetting()
                        { 
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            SettingDesc = Sm.DrStr(dr, c[2]),
                            Sequence = Sm.DrStr(dr, c[3]),
                            Formula = Sm.DrStr(dr, c[4]),
                            Balance = 0,
                            AcNo = string.Empty,
                        });
                }
                dr.Close();
            }
        }

        private void ProcessFicoCOA2(ref List<RptFicoSetting> l, ref List<RptFicoSetting>  lRptFicoSetting)
        {
             lRptFicoSetting.Clear();

            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].SettingDesc.Length > 0)
                {
                    var cm = new MySqlCommand();
                    var SQL = new StringBuilder();            
                
                    SQL.AppendLine("Select AcNo From TblCOA ");
                    SQL.AppendLine("Where Find_In_Set(AcNo,( ");
                    SQL.AppendLine("    Select Group_Concat(Acno) As AcNo ");
                    SQL.AppendLine("    From TblRptFicoSettingDtl ");
                    SQL.AppendLine("    Where SettingCode=@SettingCode ");
                    SQL.AppendLine("    And RptFicoCode=@FicoCode ");
                    SQL.AppendLine("    Order by Cast(sequence As UNSIGNED) ");
                    SQL.AppendLine(")) Order By AcNo;");
                    Sm.CmParam<String>(ref cm, "@SettingCode", l[i].SettingCode);
                    Sm.CmParam<String>(ref cm, "@FicoCode", l[i].FicoCode);
                   
                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandTimeout = 600;
                        cm.CommandText = SQL.ToString();
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                lRptFicoSetting.Add(new RptFicoSetting()
                                {
                                    FicoCode = l[i].FicoCode,
                                    SettingCode = l[i].SettingCode,
                                    SettingDesc = l[i].SettingDesc,
                                    Sequence = l[i].Sequence,
                                    Formula = l[i].Formula,
                                    AcNo = Sm.DrStr(dr, c[0]),
                                    Balance = 0,
                                });
                            }
                        }
                        dr.Close();
                    }
                }
            }
        }

        private void ProcessFicoCOA3(ref List<COAFicoSetting> lCOASet)
        {
            lCOASet.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            //var lRptFicoSetting = new List<RptFicoSetting>();

            SQL.AppendLine("Select B.RptFicoCode, B.SettingCode, B.SettingDesc, B.Sequence, B.Formula ");
            SQL.AppendLine("From TblRptFicoSettinghdr A ");
            SQL.AppendLine("Inner Join TblRptFicoSettingDtl B On A.RptFicoCode = B.RptFicoCode ");
            SQL.AppendLine("Where A.RptFicoCode = @FicoCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order By Cast(B.sequence As UNSIGNED);");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "SettingDesc", "Sequence", "Formula" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        lCOASet.Add(new COAFicoSetting()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            SettingDesc = Sm.DrStr(dr, c[2]),
                            Sequence = Sm.DrStr(dr, c[3]),
                            Formula = Sm.DrStr(dr, c[4]),
                            Balance = 0,
                        });
                }
                dr.Close();
            }
        }

        private void ProcessFicoCOA4(ref List<COAFicoSetting> lCOASet, ref List<RptFicoSetting2> l,iGrid Grdxx)
        {
            Sm.ClearGrd(Grdxx, true);
            iGRow r;


            for (var i = 0; i < lCOASet.Count; i++)
            {
                decimal bal = 0;
                r = Grdxx.Rows.Add();
                r.Cells[0].Value = lCOASet[i].SettingCode;
                r.Cells[1].Value = lCOASet[i].SettingDesc;

               
                if (lCOASet[i].Formula.Length == 0)
                {
                    l.OrderBy(o => o.Sequence).ToList();
                    for (var j = 0; j < l.Count; j++)
                    {
                        if (lCOASet[i].SettingCode == l[j].SettingCode)
                        {
                            r.Cells[2].Value = bal + l[j].Balance;
                            bal = bal + l[j].Balance;
                        }
                    }
                }
                else
                {
                    char[] delimiters = {'+', '-' };
                    string SQLFormula = lCOASet[i].Formula;
                    string[] ArraySQLFormula = lCOASet[i].Formula.Split(delimiters);
                    for (int ind = 0; ind < ArraySQLFormula.Count(); ind++)
                    {
                        for (var h = 0; h < Grdxx.Rows.Count; h++)
                        {
                            if (ArraySQLFormula[ind].ToString() == Sm.GetGrdStr(Grdxx, h, 0))
                            {
                                string oldS = ArraySQLFormula[ind].ToString();
                                string newS = Sm.GetGrdDec(Grdxx, h, 2).ToString();
                                
                                SQLFormula = SQLFormula.Replace(oldS, newS);
                            }
                        }
                    }
                    r.Cells[2].Value = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));
                }
            }
        }

        private void ParPrint()
        {
            int nomor = 0;
            var l = new List<TB>();
            var l2 = new List<TBDtl>();

            string[] TableName = { "TB", "TBDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();
           
            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddressCity', ");
            SQL.AppendLine("A.RptFicoCode, A.Remark, A.Createby ");
            SQL.AppendLine("From TblRptFicoSettingHdr A ");
            SQL.AppendLine("Where A.RptFIcoCode=@RptFicoCode  ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@RptFicoCode", Sm.GetLue(LueOptionCode));
                Sm.CmParam<String>(ref cm, "@CompanyLogo", Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-6
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyAddressCity",
                         "Remark",
                         "CreateBy",
                        });
                if (dr.HasRows)
                {
                    int month = Convert.ToInt32(Sm.GetLue(LueMth));
                    string mth = string.Empty;
                    switch (month)
                    {
                        case 1:
                            mth = "January";
                            break;
                        case 2:
                            mth = "February";
                            break;
                        case 3:
                            mth = "March";
                            break;
                        case 4:
                            mth = "April";
                            break;
                        case 5:
                            mth = "May";
                            break;
                        case 6:
                            mth = "June";
                            break;
                        case 7:
                            mth = "July";
                            break;
                        case 8:
                            mth = "August";
                            break;
                        case 9:
                            mth = "September";
                            break;
                        case 10:
                            mth = "October";
                            break;
                        case 11:
                            mth = "November";
                            break;
                        case 12:
                            mth = "December";
                            break;
                    }
                    while (dr.Read())
                    {
                        l.Add(new TB()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyAddressCity = Sm.DrStr(dr, c[4]),
                            Yr = (!mIsPrintRptFicoSettingYrAscOrder) ? Sm.GetLue(LueYr) : (!mIsPrintRptFicoSettingYr2DisplayMth) ? (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString() : "Dec " + (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString(),
                            Mth = mth,
                            Entity = Sm.GetValue("select EntName From tblentity Where EntCode = '"+Sm.GetLue(LueEntCode)+"' ") ,
                            HRemark = Sm.DrStr(dr, c[5]),
                            Type = LueOptionCode.Text,
                            Yr2 = (mIsPrintRptFicoSettingYrAscOrder) ? Sm.GetLue(LueYr) : (!mIsPrintRptFicoSettingYr2DisplayMth) ? (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString() : "Dec " + (Decimal.Parse(Sm.GetLue(LueYr)) - 1).ToString(),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            IsPrintRptFicoSettingShowPercentage = mIsPrintRptFicoSettingShowPercentage
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.SettingCode, A.SettingDesc, A.Sequence, A.AcNo, A.Formula, A.Font,  A.Line  ");
                SQLDtl.AppendLine("From TblRptFicoSettingDtl A   ");
                SQLDtl.AppendLine("Where A.RptFIcoCode=@RptFicoCode And PrintInd = 'Y'  order by Cast(sequence As UNSIGNED) ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@RptFicoCode", Sm.GetLue(LueOptionCode));

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0-1
                         "SettingCode" ,
                         "SettingDesc",
                         "Sequence",
                         "Formula",
                         "Font",
                         "Line"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        decimal bal = 0;
                        decimal bal2 = 0;
                        decimal percentage = 0;
                        for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0 && Sm.GetGrdStr(Grd1, Row, 0) == Sm.DrStr(drDtl, cDtl[0]))
                            {
                                bal =  Sm.GetGrdDec(Grd1, Row, 2);
                            }
                        }
                        for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0 && Sm.GetGrdStr(Grd2, Row, 0) == Sm.DrStr(drDtl, cDtl[0]))
                            {
                                bal2 = Sm.GetGrdDec(Grd2, Row, 2);
                            }
                        }

                        if(bal2 != 0) 
                        {
                            percentage = Sm.Round((bal / bal2 )*100 ,2);
                        }
                       
                        nomor = nomor + 1;
                        l2.Add(new TBDtl()
                        {
                            nomor = nomor,
                            SettingCode = Sm.DrStr(drDtl, cDtl[0]),
                            SettingDesc = Sm.DrStr(drDtl, cDtl[1]),
                            Sequence = Sm.DrStr(drDtl, cDtl[2]),
                            Formula = Sm.DrStr(drDtl, cDtl[3]),
                            Font = Sm.DrStr(drDtl, cDtl[4]),
                            Line = Sm.DrStr(drDtl, cDtl[5]) == "P" ? ""+Environment.NewLine+""+Environment.NewLine+"" : Sm.DrStr(drDtl, cDtl[5]),
                            Balance = (!mIsPrintRptFicoSettingYrAscOrder)? bal : bal2,
                            Balance2 = (!mIsPrintRptFicoSettingYrAscOrder) ? bal2 : bal,
                            Percentage = percentage,
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(l2);
            #endregion

            Sm.PrintReport("RptFicoSetting", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue3(SetLueEntCode), string.Empty, mIsFilterByEnt ? "Y" : "N");
        }

        private void LueOptionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOptionCode, new Sm.RefreshLue1(SetLueFicoFormula));
        }

        private void BtnPrint2_Click(object sender, EventArgs e)
        {
             ParPrint();
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueYr).Length > 0)
            {
                TxtStartYr.EditValue = Sm.GetLue(LueYr);
            }
        }
        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        private class COAFicoSetting
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
        }

        private class RptFicoSetting
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string AcNo { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
        }

        private class RptFicoSetting2
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string AcNo { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
        }

        private class TB
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyAddressCity { get; set; }
            public string Yr { get; set; }
            public string Yr2 { get; set; }
            public string Mth { get; set; }
            public string Entity { get; set; }
            public string HRemark { get; set; }
            public string Type { get; set; }
            public string PrintBy { get; set; }
            public bool IsPrintRptFicoSettingShowPercentage { get; set; }
        }

        private class TBDtl
        {
            public int nomor { set; get; }
            public string SettingCode { set; get; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
            public decimal Balance2 { get; set; }
            public string Font { get; set; }
            public string Line { get; set; }
            public decimal Percentage { get; set; }
          
        }
        #endregion
    }
}
