﻿#region Update
    // 13/07/2017 [ARI] tambah grid (user)
    // 25/07/2017 [TKG] tambah user code untuk link ke aplikasi POS
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesPerson : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool IsInsert = false;
        internal FrmSalesPersonFind FrmFind;

        #endregion

        #region Constructor

        public FrmSalesPerson(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo", 

                        //1-3
                        "", "User Code", "User Name",
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-3
                        20, 80, 180,
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3 });

        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtSPCode, TxtSPName, LueUserCode }, true);
                    TxtSPCode.Focus();
                    Grd1.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtSPCode, TxtSPName, LueUserCode }, false);
                    TxtSPCode.Focus();
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtSPName, LueUserCode }, false);
                    Grd1.ReadOnly = false;
                    TxtSPName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtSPCode, TxtSPName, LueUserCode });
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesPersonFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                SetLueUserCode(ref LueUserCode, string.Empty);
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSPCode, "", false)) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSPCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblSalesPerson Where SPCode=@SPCode" };
                Sm.CmParam<String>(ref cm, "@SPCode", TxtSPCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                SaveData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string SpCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowSP(SpCode);
                ShowSPDtl(SpCode);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSP(string SpCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SpCode", SpCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select SPCode, SPName, UserCode From TblSalesPerson Where SPCode=@SPCode",
                    new string[] { "SPCode", "SPName", "UserCode" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSPCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtSPName.EditValue = Sm.DrStr(dr, c[1]);
                        SetLueUserCode(ref LueUserCode, Sm.DrStr(dr, c[2]));
                    }, true
                );
        }

        private void ShowSPDtl(string SpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.UserCode, B.UserName ");
            SQL.AppendLine("From TblSalesPersonDtl A ");
            SQL.AppendLine("Inner Join TblUser B On A.UserCode=B.UserCode ");
            SQL.AppendLine("Where A.SpCode=@SpCode ");
            SQL.AppendLine("Order By A.DNo;");

            var cmDtl = new MySqlCommand();
            Sm.CmParam<String>(ref cmDtl, "@SpCode", SpCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cmDtl,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-2
                    "UserCode", "UserName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSPCode, "Sales person code", false) ||
                Sm.IsTxtEmpty(TxtSPName, "Sales person name", false) ||
                IsCurCodeExisted();
        }

        private void SaveData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            cml.Add(SaveSPHdr());
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveSPDtl(Row));

            Sm.ExecCommands(cml);

            ShowData(TxtSPCode.Text);
        }

        private MySqlCommand SaveSPHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesPerson(SPCode, SPName, UserCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SPCode, @SPName, @UserCode, @CurrentUserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update SpName=@SpName, UserCode=@UserCode, LastUpBy=@CurrentUserCode, LastUpDt=CurrentDateTime(); ");

            if (!IsInsert) SQL.AppendLine("Delete From TblSalesPersonDtl Where SPCode=@SpCode; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SPCode", TxtSPCode.Text);
            Sm.CmParam<String>(ref cm, "@SPName", TxtSPName.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Sm.GetLue(LueUserCode));
            Sm.CmParam<String>(ref cm, "@CurrentUserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSPDtl(int Row)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesPersonDtl(SpCode, DNo, UserCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SpCode, @DNo, @UserCode, @CreateBy, CurrentDateTime()) ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SpCode", TxtSPCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            
            return cm;
        }

        private bool IsCurCodeExisted()
        {
            if (!TxtSPCode.Properties.ReadOnly && Sm.IsDataExist("Select SPCode From TblSalesPerson Where SPCode='" + TxtSPCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Sales person code ( " + TxtSPCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Method

        internal string GetSelectedUserCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void SetLueUserCode(ref DXE.LookUpEdit Lue, string UserCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select UserCode As Col1, Concat(UserCode, ' <', UserName, '>') As Col2 ");
                SQL.AppendLine("From TblUser ");
                SQL.AppendLine("Where ExpDt Is Null ");
                if (UserCode.Length > 0) SQL.AppendLine("Or UserCode=@UserCode ");
                SQL.AppendLine("Order By UserCode;");    
                
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (UserCode.Length>0) Sm.CmParam<String>(ref cm, "@UserCode", UserCode);
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "User", "Col2", "Col1");
                if (UserCode.Length > 0) Sm.SetLue(LueUserCode, UserCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtSPCode);
        }

        private void TxtSPName_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtSPName);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesPersonDlg(this));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmSalesPersonDlg(this));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void LueUserCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueUserCode, new Sm.RefreshLue2(SetLueUserCode), string.Empty);
        }

        #endregion

        #endregion

    }
}
