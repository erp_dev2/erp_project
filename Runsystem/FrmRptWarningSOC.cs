﻿#region Update
/*
    05/04/2021 [IBL/IMS] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptWarningSOC : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptWarningSOC(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                ShowData();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DocNo, B.DocDt, Case B.Status When 'O' Then 'OutstAnding' When 'A' Then 'Approve' End As StatusDesc, B.LocalDocNo, C.CtName, B.BOQDocNo, B.SAAddress, ");
            SQL.AppendLine("D.ItCodeInternal, D.ItName, IfNull(I.RevisionQty, 0.00) As RevisionQty, IfNull(I.RevisionUPrice, 0.00) As RevisionUPrice, ");
            SQL.AppendLine("(IfNull(I.RevisionQty,0.00)*IfNull(I.RevisionUPrice,0.00)) As TotalPriceRev, I.Remark, A.DeliveryDt,IfNull(G.ProjectCode, B.ProjectCode2) As ProjectCode, ");
            SQL.AppendLine("IfNull(G.ProjectName, F.ProjectName) As ProjectName,H.DocNo As NTPDocNo, B.PONo ");
            SQL.AppendLine("From TblSOContractDtl A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode = C.CtCode ");
            SQL.AppendLine("Inner Join TblItem D On A.ItCode = D.ItCode ");
            SQL.AppendLine("Inner Join TblBOQHdr E On B.BOQDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr F On E.LOPDocNo = F.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup G On G.PGCode=F.PGCode ");
            SQL.AppendLine("Left Join TblNoticeToProceed H On F.DocNo = H.LOPDocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X1.DocNo, X1.DNo, X4.CreateDt, ");
            SQL.AppendLine("    IfNull(X4.UPrice, 0.00) RevisionUPrice, IfNull(X4.Qty, 0.00) As RevisionQty, IfNull(X4.Amt, 0.00) As RevisionAmt, X1.Remark ");
            SQL.AppendLine("    From TblSOContractDtl4 X1 ");
            SQL.AppendLine("    Inner Join TblBOQDtl2 X2 On X1.BOQDocNo = X2.DocNo ");
            SQL.AppendLine("        And X1.ItCode = X2.ItCode ");
            SQL.AppendLine("        And X1.SeqNo = X2.SeqNo ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Max(DocNo) DocNo, SOCDocNo ");
            SQL.AppendLine("        From TblSOContractRevisionHdr ");
            SQL.AppendLine("        Group By SOCDocNo ");
            SQL.AppendLine("    ) X3 On X1.DocNo = X3.SOCDocNo ");
            SQL.AppendLine("    Left Join TblSOContractRevisionDtl4 X4 On X3.DocNo = X4.DocNo And X1.DNo = X4.DNo And X1.ItCode = X4.ItCode ");
            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select X1.DocNo, X1.DNo, X4.CreateDt, ");
            SQL.AppendLine("    IfNull(X4.UPrice, 0.00) RevisionUPrice, IfNull(X4.Qty, 0.00) As RevisionQty, IfNull(X4.Amt, 0.00) As RevisionAmt, X1.Remark ");
            SQL.AppendLine("    From TblSOContractDtl5 X1 ");
            SQL.AppendLine("    Inner Join TblBOQDtl3 X2 On X1.BOQDocNo = X2.DocNo ");
            SQL.AppendLine("        And X1.ItCode = X2.ItCode ");
            SQL.AppendLine("        And X1.SeqNo = X2.SeqNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Max(DocNo) DocNo, SOCDocNo ");
            SQL.AppendLine("        From TblSOContractRevisionHdr ");
            SQL.AppendLine("        Group By SOCDocNo ");
            SQL.AppendLine("    ) X3 On X1.DocNo = X3.SOCDocNo ");
            SQL.AppendLine("    Left Join TblSOContractRevisionDtl5 X4 On X3.DocNo = X4.DocNo And X1.DNo = X4.DNo And X1.ItCode = X4.ItCode ");
            SQL.AppendLine(") I On A.DocNo = I.DocNo And A.DNo = I.DNo ");
            SQL.AppendLine("Where A.DeliveryDt Between CURRENT_DATE() And DATE_ADD(CURRENT_DATE(), INTERVAL 30 DAY) ");
            SQL.AppendLine("And B.CancelInd = 'N' And a.ProcessIndForDR REGEXP '^O|^P' ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Document#",
                    "Date",
                    "Status",
                    "Local#",
                    
                    //6-10
                    "Customer",
                    "BOQ#",
                    "Shipping Address",
                    "Local Code",
                    "Item's Name",
                    
                    //11-15
                    "Quantity" + Environment.NewLine + "Revision",
                    "Unit Price" + Environment.NewLine + "Revision",
                    "Total Price" + Environment.NewLine + "Revision",
                    "Remark",
                    "Delivery Date",

                    //16-19
                    "Project Code",
                    "Project Name",
                    "Notice To Proceed",
                    "Customer's PO"
                   
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 150, 80, 80, 150,
                    
                    //6-10
                    230, 180, 180, 150, 150,

                    //11-15
                    80, 130, 130, 150, 80,

                    //16-19
                    100, 250, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 15 });
            Sm.GrdColButton(Grd1, new int[] { 1 } );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "B.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtCtCode.Text, new string[] { "B.CtCode", "C.CtName" } );
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SetSQL() + Filter + " Order By B.DocDt, B.DocNo",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "StatusDesc", "LocalDocNo", "CtName", "BOQDocNo", 
                        
                        //6-10
                        "SAAddress", "ItCodeInternal", "ItName", "RevisionQty", "RevisionUPrice",

                        //11-15
                        "TotalPriceRev", "Remark", "DeliveryDt", "ProjectCode", "ProjectName",

                        //16-17
                        "NTPDocNo", "PONo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                    }, true, false, false, false
                );
                Grd1.GroupObject.Add(15);
                Grd1.Group();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSOContract2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmSOContract2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtCtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
