﻿namespace RunSystem
{
    partial class FrmSOContractDlg3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSOContractDlg3));
            this.TxtCtItName = new DevExpress.XtraEditors.TextEdit();
            this.TxtSpecification = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtCtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtQtyPackaging = new DevExpress.XtraEditors.TextEdit();
            this.LuePackaging = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDiscountAmt = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtUPrice1 = new DevExpress.XtraEditors.TextEdit();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.BtnMasterItem = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtQty = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.DteDeliveryDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtUomCode = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtUPrice = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnItem = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDiscount = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotal = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtPromoRate = new DevExpress.XtraEditors.TextEdit();
            this.TxtUPriceAfter = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtUPriceBefore = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxAmt = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtTaxRate = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpecification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtyPackaging.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePackaging.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscountAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPromoRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceAfter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceBefore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxRate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 434);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnAdd.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAdd.Appearance.Options.UseBackColor = true;
            this.BtnAdd.Appearance.Options.UseFont = true;
            this.BtnAdd.Appearance.Options.UseForeColor = true;
            this.BtnAdd.Appearance.Options.UseTextOptions = true;
            this.BtnAdd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtCtItName);
            this.panel2.Controls.Add(this.TxtSpecification);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.TxtCtItCode);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtQtyPackaging);
            this.panel2.Controls.Add(this.LuePackaging);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtDiscountAmt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtUPrice1);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.BtnMasterItem);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.TxtQty);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.DteDeliveryDt);
            this.panel2.Controls.Add(this.TxtUomCode);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.TxtUPrice);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.BtnItem);
            this.panel2.Controls.Add(this.TxtDiscount);
            this.panel2.Controls.Add(this.TxtTotal);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.TxtPromoRate);
            this.panel2.Controls.Add(this.TxtUPriceAfter);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.TxtUPriceBefore);
            this.panel2.Controls.Add(this.TxtTaxAmt);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtTaxRate);
            this.panel2.Size = new System.Drawing.Size(672, 434);
            // 
            // TxtCtItName
            // 
            this.TxtCtItName.EnterMoveNextControl = true;
            this.TxtCtItName.Location = new System.Drawing.Point(308, 71);
            this.TxtCtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtItName.Name = "TxtCtItName";
            this.TxtCtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtCtItName.Properties.MaxLength = 16;
            this.TxtCtItName.Properties.ReadOnly = true;
            this.TxtCtItName.Size = new System.Drawing.Size(353, 20);
            this.TxtCtItName.TabIndex = 21;
            // 
            // TxtSpecification
            // 
            this.TxtSpecification.EnterMoveNextControl = true;
            this.TxtSpecification.Location = new System.Drawing.Point(138, 50);
            this.TxtSpecification.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSpecification.Name = "TxtSpecification";
            this.TxtSpecification.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSpecification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSpecification.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSpecification.Properties.Appearance.Options.UseFont = true;
            this.TxtSpecification.Properties.MaxLength = 16;
            this.TxtSpecification.Properties.ReadOnly = true;
            this.TxtSpecification.Size = new System.Drawing.Size(167, 20);
            this.TxtSpecification.TabIndex = 18;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(57, 52);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(75, 14);
            this.label20.TabIndex = 17;
            this.label20.Text = "Specification";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCtItCode
            // 
            this.TxtCtItCode.EnterMoveNextControl = true;
            this.TxtCtItCode.Location = new System.Drawing.Point(138, 71);
            this.TxtCtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtItCode.Name = "TxtCtItCode";
            this.TxtCtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtItCode.Properties.MaxLength = 16;
            this.TxtCtItCode.Properties.ReadOnly = true;
            this.TxtCtItCode.Size = new System.Drawing.Size(167, 20);
            this.TxtCtItCode.TabIndex = 20;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(3, 73);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(129, 14);
            this.label19.TabIndex = 19;
            this.label19.Text = "Customer\'s Item Code";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(20, 117);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 14);
            this.label9.TabIndex = 24;
            this.label9.Text = "Packaging Quantity";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQtyPackaging
            // 
            this.TxtQtyPackaging.EnterMoveNextControl = true;
            this.TxtQtyPackaging.Location = new System.Drawing.Point(138, 114);
            this.TxtQtyPackaging.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtyPackaging.Name = "TxtQtyPackaging";
            this.TxtQtyPackaging.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQtyPackaging.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtyPackaging.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtyPackaging.Properties.Appearance.Options.UseFont = true;
            this.TxtQtyPackaging.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQtyPackaging.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQtyPackaging.Size = new System.Drawing.Size(102, 20);
            this.TxtQtyPackaging.TabIndex = 25;
            this.TxtQtyPackaging.Validated += new System.EventHandler(this.TxtQtyPackaging_Validated);
            // 
            // LuePackaging
            // 
            this.LuePackaging.EnterMoveNextControl = true;
            this.LuePackaging.Location = new System.Drawing.Point(138, 92);
            this.LuePackaging.Margin = new System.Windows.Forms.Padding(5);
            this.LuePackaging.Name = "LuePackaging";
            this.LuePackaging.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePackaging.Properties.Appearance.Options.UseFont = true;
            this.LuePackaging.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePackaging.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePackaging.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePackaging.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePackaging.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePackaging.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePackaging.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePackaging.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePackaging.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePackaging.Properties.DropDownRows = 30;
            this.LuePackaging.Properties.NullText = "[Empty]";
            this.LuePackaging.Properties.PopupWidth = 250;
            this.LuePackaging.Size = new System.Drawing.Size(225, 20);
            this.LuePackaging.TabIndex = 23;
            this.LuePackaging.ToolTip = "F4 : Show/hide list";
            this.LuePackaging.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePackaging.EditValueChanged += new System.EventHandler(this.LuePackaging_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(43, 95);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 14);
            this.label4.TabIndex = 22;
            this.label4.Text = "Packaging UoM";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(219, 243);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 14);
            this.label7.TabIndex = 40;
            this.label7.Text = "%";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(30, 202);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 14);
            this.label6.TabIndex = 34;
            this.label6.Text = "Discount Amount";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDiscountAmt
            // 
            this.TxtDiscountAmt.EnterMoveNextControl = true;
            this.TxtDiscountAmt.Location = new System.Drawing.Point(138, 199);
            this.TxtDiscountAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscountAmt.Name = "TxtDiscountAmt";
            this.TxtDiscountAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDiscountAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscountAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscountAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscountAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscountAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscountAmt.Properties.ReadOnly = true;
            this.TxtDiscountAmt.Size = new System.Drawing.Size(222, 20);
            this.TxtDiscountAmt.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(16, 223);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 14);
            this.label2.TabIndex = 36;
            this.label2.Text = "Price After Discount";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUPrice1
            // 
            this.TxtUPrice1.EnterMoveNextControl = true;
            this.TxtUPrice1.Location = new System.Drawing.Point(138, 220);
            this.TxtUPrice1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPrice1.Name = "TxtUPrice1";
            this.TxtUPrice1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUPrice1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPrice1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPrice1.Properties.Appearance.Options.UseFont = true;
            this.TxtUPrice1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPrice1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPrice1.Properties.ReadOnly = true;
            this.TxtUPrice1.Size = new System.Drawing.Size(222, 20);
            this.TxtUPrice1.TabIndex = 37;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(138, 8);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(167, 20);
            this.TxtItCode.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(59, 10);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 14);
            this.label12.TabIndex = 11;
            this.label12.Text = "Item\'s Code";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnMasterItem
            // 
            this.BtnMasterItem.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnMasterItem.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnMasterItem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMasterItem.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnMasterItem.Appearance.Options.UseBackColor = true;
            this.BtnMasterItem.Appearance.Options.UseFont = true;
            this.BtnMasterItem.Appearance.Options.UseForeColor = true;
            this.BtnMasterItem.Appearance.Options.UseTextOptions = true;
            this.BtnMasterItem.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnMasterItem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnMasterItem.Image = ((System.Drawing.Image)(resources.GetObject("BtnMasterItem.Image")));
            this.BtnMasterItem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnMasterItem.Location = new System.Drawing.Point(334, 8);
            this.BtnMasterItem.Name = "BtnMasterItem";
            this.BtnMasterItem.Size = new System.Drawing.Size(24, 21);
            this.BtnMasterItem.TabIndex = 14;
            this.BtnMasterItem.ToolTip = "Show Item Information";
            this.BtnMasterItem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnMasterItem.ToolTipTitle = "Run System";
            this.BtnMasterItem.Click += new System.EventHandler(this.BtnMasterItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(56, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 15;
            this.label1.Text = "Item\'s Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(138, 29);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 16;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(523, 20);
            this.TxtItName.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(78, 139);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 14);
            this.label11.TabIndex = 26;
            this.label11.Text = "Quantity";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(138, 388);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(523, 20);
            this.MeeRemark.TabIndex = 55;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // TxtQty
            // 
            this.TxtQty.EnterMoveNextControl = true;
            this.TxtQty.Location = new System.Drawing.Point(138, 136);
            this.TxtQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQty.Name = "TxtQty";
            this.TxtQty.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQty.Properties.Appearance.Options.UseFont = true;
            this.TxtQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQty.Size = new System.Drawing.Size(102, 20);
            this.TxtQty.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(85, 391);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 54;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDeliveryDt
            // 
            this.DteDeliveryDt.EditValue = null;
            this.DteDeliveryDt.EnterMoveNextControl = true;
            this.DteDeliveryDt.Location = new System.Drawing.Point(138, 367);
            this.DteDeliveryDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDeliveryDt.Name = "DteDeliveryDt";
            this.DteDeliveryDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeliveryDt.Properties.Appearance.Options.UseFont = true;
            this.DteDeliveryDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeliveryDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDeliveryDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDeliveryDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDeliveryDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeliveryDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDeliveryDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeliveryDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDeliveryDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDeliveryDt.Size = new System.Drawing.Size(126, 20);
            this.DteDeliveryDt.TabIndex = 53;
            // 
            // TxtUomCode
            // 
            this.TxtUomCode.EnterMoveNextControl = true;
            this.TxtUomCode.Location = new System.Drawing.Point(241, 136);
            this.TxtUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUomCode.Name = "TxtUomCode";
            this.TxtUomCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUomCode.Properties.MaxLength = 16;
            this.TxtUomCode.Properties.ReadOnly = true;
            this.TxtUomCode.Size = new System.Drawing.Size(119, 20);
            this.TxtUomCode.TabIndex = 28;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(53, 370);
            this.label24.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 14);
            this.label24.TabIndex = 52;
            this.label24.Text = "Delivery Date";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(37, 160);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 14);
            this.label3.TabIndex = 29;
            this.label3.Text = "Price (Price List)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(219, 286);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 14);
            this.label23.TabIndex = 45;
            this.label23.Text = "%";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUPrice
            // 
            this.TxtUPrice.EnterMoveNextControl = true;
            this.TxtUPrice.Location = new System.Drawing.Point(138, 157);
            this.TxtUPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPrice.Name = "TxtUPrice";
            this.TxtUPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtUPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPrice.Properties.ReadOnly = true;
            this.TxtUPrice.Size = new System.Drawing.Size(222, 20);
            this.TxtUPrice.TabIndex = 30;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(219, 181);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 14);
            this.label18.TabIndex = 33;
            this.label18.Text = "%";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(49, 180);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 14);
            this.label5.TabIndex = 31;
            this.label5.Text = "Discount Rate";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnItem
            // 
            this.BtnItem.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItem.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItem.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItem.Appearance.Options.UseBackColor = true;
            this.BtnItem.Appearance.Options.UseFont = true;
            this.BtnItem.Appearance.Options.UseForeColor = true;
            this.BtnItem.Appearance.Options.UseTextOptions = true;
            this.BtnItem.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItem.Image = ((System.Drawing.Image)(resources.GetObject("BtnItem.Image")));
            this.BtnItem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItem.Location = new System.Drawing.Point(309, 8);
            this.BtnItem.Name = "BtnItem";
            this.BtnItem.Size = new System.Drawing.Size(24, 21);
            this.BtnItem.TabIndex = 13;
            this.BtnItem.ToolTip = "Find Item";
            this.BtnItem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItem.ToolTipTitle = "Run System";
            this.BtnItem.Click += new System.EventHandler(this.BtnItem_Click);
            // 
            // TxtDiscount
            // 
            this.TxtDiscount.EnterMoveNextControl = true;
            this.TxtDiscount.Location = new System.Drawing.Point(138, 178);
            this.TxtDiscount.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscount.Name = "TxtDiscount";
            this.TxtDiscount.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDiscount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscount.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscount.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscount.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscount.Properties.ReadOnly = true;
            this.TxtDiscount.Size = new System.Drawing.Size(76, 20);
            this.TxtDiscount.TabIndex = 32;
            // 
            // TxtTotal
            // 
            this.TxtTotal.EnterMoveNextControl = true;
            this.TxtTotal.Location = new System.Drawing.Point(138, 346);
            this.TxtTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal.Properties.ReadOnly = true;
            this.TxtTotal.Size = new System.Drawing.Size(222, 20);
            this.TxtTotal.TabIndex = 51;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(61, 244);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 14);
            this.label10.TabIndex = 38;
            this.label10.Text = "Promo Rate";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(97, 348);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 14);
            this.label17.TabIndex = 50;
            this.label17.Text = "Total";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPromoRate
            // 
            this.TxtPromoRate.EnterMoveNextControl = true;
            this.TxtPromoRate.Location = new System.Drawing.Point(138, 241);
            this.TxtPromoRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPromoRate.Name = "TxtPromoRate";
            this.TxtPromoRate.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPromoRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPromoRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPromoRate.Properties.Appearance.Options.UseFont = true;
            this.TxtPromoRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPromoRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPromoRate.Properties.ReadOnly = true;
            this.TxtPromoRate.Size = new System.Drawing.Size(76, 20);
            this.TxtPromoRate.TabIndex = 39;
            // 
            // TxtUPriceAfter
            // 
            this.TxtUPriceAfter.EnterMoveNextControl = true;
            this.TxtUPriceAfter.Location = new System.Drawing.Point(138, 325);
            this.TxtUPriceAfter.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPriceAfter.Name = "TxtUPriceAfter";
            this.TxtUPriceAfter.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUPriceAfter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPriceAfter.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPriceAfter.Properties.Appearance.Options.UseFont = true;
            this.TxtUPriceAfter.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPriceAfter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPriceAfter.Properties.ReadOnly = true;
            this.TxtUPriceAfter.Size = new System.Drawing.Size(222, 20);
            this.TxtUPriceAfter.TabIndex = 49;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(35, 265);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 14);
            this.label13.TabIndex = 41;
            this.label13.Text = "Price Before Tax";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(43, 328);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 14);
            this.label16.TabIndex = 48;
            this.label16.Text = "Price After Tax";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUPriceBefore
            // 
            this.TxtUPriceBefore.EnterMoveNextControl = true;
            this.TxtUPriceBefore.Location = new System.Drawing.Point(138, 262);
            this.TxtUPriceBefore.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPriceBefore.Name = "TxtUPriceBefore";
            this.TxtUPriceBefore.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUPriceBefore.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPriceBefore.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPriceBefore.Properties.Appearance.Options.UseFont = true;
            this.TxtUPriceBefore.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPriceBefore.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPriceBefore.Properties.ReadOnly = true;
            this.TxtUPriceBefore.Size = new System.Drawing.Size(222, 20);
            this.TxtUPriceBefore.TabIndex = 42;
            // 
            // TxtTaxAmt
            // 
            this.TxtTaxAmt.EnterMoveNextControl = true;
            this.TxtTaxAmt.Location = new System.Drawing.Point(138, 304);
            this.TxtTaxAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt.Name = "TxtTaxAmt";
            this.TxtTaxAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt.Properties.ReadOnly = true;
            this.TxtTaxAmt.Size = new System.Drawing.Size(222, 20);
            this.TxtTaxAmt.TabIndex = 47;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(76, 286);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 14);
            this.label14.TabIndex = 43;
            this.label14.Text = "Tax Rate";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(57, 307);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 14);
            this.label15.TabIndex = 46;
            this.label15.Text = "Tax Amount";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxRate
            // 
            this.TxtTaxRate.EnterMoveNextControl = true;
            this.TxtTaxRate.Location = new System.Drawing.Point(138, 283);
            this.TxtTaxRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxRate.Name = "TxtTaxRate";
            this.TxtTaxRate.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxRate.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxRate.Size = new System.Drawing.Size(76, 20);
            this.TxtTaxRate.TabIndex = 44;
            this.TxtTaxRate.Validated += new System.EventHandler(this.TxtTaxRate_Validated);
            // 
            // FrmSO3Dlg3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 434);
            this.Name = "FrmSO3Dlg3";
            this.Text = "List Of Item";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpecification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtyPackaging.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePackaging.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscountAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPromoRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceAfter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceBefore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxRate.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtCtItName;
        internal DevExpress.XtraEditors.TextEdit TxtSpecification;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtCtItCode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtQtyPackaging;
        internal DevExpress.XtraEditors.LookUpEdit LuePackaging;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtDiscountAmt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtUPrice1;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label12;
        public DevExpress.XtraEditors.SimpleButton BtnMasterItem;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        internal DevExpress.XtraEditors.TextEdit TxtQty;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.DateEdit DteDeliveryDt;
        internal DevExpress.XtraEditors.TextEdit TxtUomCode;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtUPrice;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.SimpleButton BtnItem;
        internal DevExpress.XtraEditors.TextEdit TxtDiscount;
        internal DevExpress.XtraEditors.TextEdit TxtTotal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtPromoRate;
        internal DevExpress.XtraEditors.TextEdit TxtUPriceAfter;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtUPriceBefore;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtTaxRate;
    }
}