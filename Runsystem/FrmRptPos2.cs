﻿#region Update
/* 
    24/07/2017 [TKG] menambah Pos#
    27/09/2017 [HAR] total summary
    04/07/2018 [TKG] Tambah site
    13/08/2018 [TKG] tambah informasi entity
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPos2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsSiteMandatory = false;

        #endregion

        #region Constructor

        public FrmRptPos2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-1);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                TxtPosNo.EditValue = Gv.PosNo;
                ChkPosNo.Checked = Gv.PosNo.Length > 0;
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PosNo, A.TrnNo, A.BsDate, A.TrnDtTm, A.ShiftNo, A.SlsType, A.UserCode, C.PayTpNm, ");
            SQL.AppendLine("D.SetValue As Cur, Case When A.SlsType='R' Then -1.00 Else 1.00 End *B.PayAmtNett As PayAmtNett, ");
            SQL.AppendLine("B.Charge, B.CardNo, B.CardName, F.SiteName, H.EntName ");
            SQL.AppendLine("From TblPosTrnHdr A ");
            SQL.AppendLine("Inner Join TblPosTrnPay B On A.PosNo=B.PosNo And A.TrnNo=B.TrnNo And A.BsDate=B.BsDate ");
            SQL.AppendLine("Inner Join TblPosPayByType C On B.PayTpNo=C.PayTpNo ");
            SQL.AppendLine("Left Join TblPosSetting D On A.Posno=D.PosNo And D.SetCode='LocalCur' ");
            SQL.AppendLine("Left Join TblPosNo E On A.PosNo=E.PosNo ");
            SQL.AppendLine("Left Join TblSite F On E.SiteCode=F.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter G On F.ProfitCenterCode=G.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity H On G.EntCode=H.EntCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Pos#",
                        "Received#", 
                        "Business"+Environment.NewLine+"Date",
                        "Transaction"+Environment.NewLine+"Date",
                        "Shift",
                        
                        //6-10
                        "Sales/Retur",
                        "Cashier",
                        "Payment"+Environment.NewLine+"Type",
                        "Currency",
                        "Amount",
                        
                        //11-15
                        "Charge",
                        "Card#",
                        "Card"+Environment.NewLine+"Name",
                        "Site",
                        "Entity"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 100, 100, 120, 70, 

                        //6-10
                        100, 100, 150, 80, 120, 

                        //11-15
                        100, 120, 100, 180, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 12, 13 }, false);
            if (mIsSiteMandatory)
            {
                Grd1.Cols[14].Move(2);
                Grd1.Cols[15].Move(3);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.BsDate");
                Sm.FilterStr(ref Filter, ref cm, TxtPosNo.Text, "A.PosNo", true);
                Sm.FilterStr(ref Filter, ref cm, TxtShiftNo.Text, "A.ShiftNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtTrnNo.Text, "A.TrnNo",false);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.PosNo, A.BsDate, A.TrnNo;",
                new string[]
                    {
                        //0
                        "PosNo",  

                        //1-5
                        "TrnNo", "BsDate", "TrnDtTm", "ShiftNo", "SlsType",  

                        //6-10
                        "UserCode", "PayTpNm", "Cur", "PayAmtNett", "Charge", 
                        
                        //11-14
                        "CardNo", "CardName", "SiteName", "EntName"
                    },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    if (dr[c[5]] == DBNull.Value)
                        Grd.Cells[Row, 6].Value = string.Empty;
                    else
                        Grd.Cells[Row, 6].Value = (dr.GetString(c[5]) == "S" ? "Sales" : "Retur");
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                }, true, false, false, false
                );
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11 });
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtShiftNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkShiftNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Shift");
        }

        private void TxtTrnNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTrnNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this,sender,"Received#");
        }

        private void TxtPosNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPosNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Pos#");
        }

        #endregion

        #endregion
    }
}
