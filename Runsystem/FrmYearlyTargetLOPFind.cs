﻿#region update

/*
 *  29/09/2022 [SET/VIR] Menu Find Baru : Yearly Target LOP
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmYearlyTargetLOPFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmYearlyTargetLOP mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmYearlyTargetLOPFind(FrmYearlyTargetLOP FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            SetGrd();
            SetSQL();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Year",
                        "Site",
                        "Created By",
                        "Created Date",

                        //6-9
                        "Created Time",
                        "Last Updated By",
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 100, 130, 130, 

                        //26-27
                        130, 130, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] {  });
            Sm.GrdFormatDate(Grd1, new int[] { 5, 8 });
            Sm.GrdFormatTime(Grd1, new int[] { 6, 9 });
            Sm.GrdFormatDec(Grd1, new int[] {  }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7, 8, 9 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt,  A.Yr, C.SiteName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("FROM tblsitetargetLOPhdr A ");
            SQL.AppendLine("INNER JOIN tblsitetargetLOPdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("Left JOIN tblsite C On B.SiteCode = C.SiteCode ");

            mSQL = SQL.ToString();
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                //Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                //Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "Where A.DocDt Between @DocDt1 And @DocDt2 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "Yr", "SiteName", "CreateBy", "CreateDt", "LastUpBy", 

                            //6
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 9, 6);

                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion
    }
}
