﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPosSetting : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmPosSettingFind FrmFind;

        #endregion

        #region Constructor

        public FrmPosSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtPosNo, TxtSetCode, TxtSetValue }, true);
                    TxtPosNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtPosNo, TxtSetCode }, true);
                    Sm.SetControlReadOnly(TxtSetValue, false);
                    TxtSetValue.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtPosNo, TxtSetCode, TxtSetValue });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPosSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPosNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand() 
                {
                    CommandText = 
                        "Update TblPosSetting Set " +
                        "   SetValue=@SetValue, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                        "Where PosNo=@PosNo And SetCode=@SetCode;"
                };
                Sm.CmParam<String>(ref cm, "@PosNo", TxtPosNo.Text);
                Sm.CmParam<String>(ref cm, "@SetCode", TxtSetCode.Text);
                Sm.CmParam<String>(ref cm, "@SetValue", TxtSetValue.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPosNo.Text, TxtSetCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PosNo, string SetCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PosNo", PosNo);
                Sm.CmParam<String>(ref cm, "@SetCode", SetCode);
                Sm.ShowDataInCtrl(
                        ref cm, "Select PosNo, SetCode, SetValue From TblPosSetting Where PosNo=@PosNo And SetCode=@SetCode;",
                        new string[]{ "PosNo", "SetCode", "SetValue" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPosNo.EditValue = Sm.DrStr(dr, c[0]);
                            TxtSetCode.EditValue = Sm.DrStr(dr, c[1]);
                            TxtSetValue.EditValue = Sm.DrStr(dr, c[2]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPosNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPosNo);
        }

        private void TxtSetCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSetCode);
        }

        private void TxtSetValue_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSetValue);
        }

        #endregion

        #endregion
    }
}
