﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmVendorFileUploadFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmVendorFileUpload mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVendorFileUploadFind(FrmVendorFileUpload FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = "Find Vendor Upload File";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                
                SetGrd();
                SetSQL();
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueFileCategory(ref LueFileCategory);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.VdCode, C.VdName, A.CategoryCode, B.CategoryName, ");
            SQL.AppendLine("CONCAT((select parvalue from tblparameter where parcode ='SharedFolderForFTPClient'),'/', A.FileLocation) FileLocation, A.FileLocation FileLocation2, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy,  A.LastUpDt, ");
            SQL.AppendLine("case  ");
            SQL.AppendLine("when A.Status ='A' then 'Approved' ");
            SQL.AppendLine("ELSE 'Cancelled' ");
            SQL.AppendLine("END AS Status, A.Status Status2");

            SQL.AppendLine("From tblvendorfileupload A  "); 
            SQL.AppendLine("Inner Join TblVendorFileUploadCategory B On A.CategoryCode = B.CategoryCode ");
            SQL.AppendLine("INNER JOIN tblvendor C ON A.VdCode=C.VdCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Vendor Code", 
                        "Vendor Name",
                        "File"+Environment.NewLine+"Category Code",
                        "File"+Environment.NewLine+"Category Name",
                        "Status",

                        //6-10
                        "File Location",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",

                        //11-14
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",
                        "Status 2", 
                        "File Location 2"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        100, 200, 100, 200, 150, 
                        //6-10
                        350, 100, 100, 100, 100,
                        //11-14
                        100, 100, 100, 100
                    }
                );

            Sm.GrdFormatDate(Grd1, new int[] { 8, 11 });
            Sm.GrdFormatTime(Grd1, new int[] { 9, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);


            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueFileCategory), "A.CategoryCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By  CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "VdCode", 
                            //1-5
                            "VdName", "CategoryCode", "CategoryName", "Status", "FileLocation", 
                            //6-10
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "Status2",

                            //11
                            "FileLocation2"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 14), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 13));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion


        #region Event

        #region Misc Control Events

        
        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueFileCategory_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueFileCategory, new Sm.RefreshLue1(Sl.SetLueFileCategory));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkFileCategory_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit (this, sender, "Category");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion

        

        #endregion


    }
}
