﻿namespace RunSystem
{
    partial class FrmBinTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBinTransfer));
            this.BtnBinTransferRequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtBinTransferRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtWhsCode2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtWhsCode = new DevExpress.XtraEditors.TextEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnBinTransferRequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.PanelTotal = new System.Windows.Forms.Panel();
            this.TxtInventoryUomCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotal = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.PanelTotal2 = new System.Windows.Forms.Panel();
            this.TxtInventoryUomCode2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotal2 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.PanelTotal3 = new System.Windows.Forms.Panel();
            this.TxtInventoryUomCode3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotal3 = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBinTransferRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWhsCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.PanelTotal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).BeginInit();
            this.PanelTotal2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2.Properties)).BeginInit();
            this.PanelTotal3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.BtnBinTransferRequestDocNo2);
            this.panel2.Controls.Add(this.Grd2);
            this.panel2.Controls.Add(this.BtnBinTransferRequestDocNo);
            this.panel2.Controls.Add(this.TxtBinTransferRequestDocNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 214);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Location = new System.Drawing.Point(0, 214);
            this.panel3.Size = new System.Drawing.Size(772, 259);
            this.panel3.Controls.SetChildIndex(this.panel5, 0);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 225);
            this.Grd1.TabIndex = 26;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnBinTransferRequestDocNo
            // 
            this.BtnBinTransferRequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBinTransferRequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBinTransferRequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBinTransferRequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBinTransferRequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnBinTransferRequestDocNo.Appearance.Options.UseFont = true;
            this.BtnBinTransferRequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnBinTransferRequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnBinTransferRequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBinTransferRequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBinTransferRequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnBinTransferRequestDocNo.Image")));
            this.BtnBinTransferRequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBinTransferRequestDocNo.Location = new System.Drawing.Point(249, 45);
            this.BtnBinTransferRequestDocNo.Name = "BtnBinTransferRequestDocNo";
            this.BtnBinTransferRequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnBinTransferRequestDocNo.TabIndex = 16;
            this.BtnBinTransferRequestDocNo.ToolTip = "Find Bin Transfer Request Document";
            this.BtnBinTransferRequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBinTransferRequestDocNo.ToolTipTitle = "Run System";
            this.BtnBinTransferRequestDocNo.Click += new System.EventHandler(this.BtnBinTransferRequestDocNo_Click);
            // 
            // TxtBinTransferRequestDocNo
            // 
            this.TxtBinTransferRequestDocNo.EnterMoveNextControl = true;
            this.TxtBinTransferRequestDocNo.Location = new System.Drawing.Point(82, 46);
            this.TxtBinTransferRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBinTransferRequestDocNo.Name = "TxtBinTransferRequestDocNo";
            this.TxtBinTransferRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBinTransferRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBinTransferRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBinTransferRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBinTransferRequestDocNo.Properties.MaxLength = 30;
            this.TxtBinTransferRequestDocNo.Properties.ReadOnly = true;
            this.TxtBinTransferRequestDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtBinTransferRequestDocNo.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(14, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 14;
            this.label4.Text = "Request #";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(39, 28);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 14);
            this.label3.TabIndex = 21;
            this.label3.Text = "To";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(27, 7);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 14);
            this.label8.TabIndex = 19;
            this.label8.Text = "From";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(64, 46);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(400, 20);
            this.MeeRemark.TabIndex = 24;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 49);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 23;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(82, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(105, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(46, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(82, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.Appearance.Options.UseForeColor = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWhsCode2
            // 
            this.TxtWhsCode2.EnterMoveNextControl = true;
            this.TxtWhsCode2.Location = new System.Drawing.Point(64, 4);
            this.TxtWhsCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWhsCode2.Name = "TxtWhsCode2";
            this.TxtWhsCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtWhsCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWhsCode2.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TxtWhsCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWhsCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtWhsCode2.Properties.Appearance.Options.UseForeColor = true;
            this.TxtWhsCode2.Properties.MaxLength = 80;
            this.TxtWhsCode2.Properties.ReadOnly = true;
            this.TxtWhsCode2.Size = new System.Drawing.Size(339, 20);
            this.TxtWhsCode2.TabIndex = 20;
            // 
            // TxtWhsCode
            // 
            this.TxtWhsCode.EnterMoveNextControl = true;
            this.TxtWhsCode.Location = new System.Drawing.Point(64, 25);
            this.TxtWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWhsCode.Name = "TxtWhsCode";
            this.TxtWhsCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWhsCode.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TxtWhsCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWhsCode.Properties.Appearance.Options.UseFont = true;
            this.TxtWhsCode.Properties.Appearance.Options.UseForeColor = true;
            this.TxtWhsCode.Properties.MaxLength = 80;
            this.TxtWhsCode.Properties.ReadOnly = true;
            this.TxtWhsCode.Size = new System.Drawing.Size(339, 20);
            this.TxtWhsCode.TabIndex = 22;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 20;
            this.Grd2.Location = new System.Drawing.Point(0, 70);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(772, 144);
            this.Grd2.TabIndex = 25;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // BtnBinTransferRequestDocNo2
            // 
            this.BtnBinTransferRequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBinTransferRequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBinTransferRequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBinTransferRequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBinTransferRequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnBinTransferRequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnBinTransferRequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnBinTransferRequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnBinTransferRequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBinTransferRequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBinTransferRequestDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnBinTransferRequestDocNo2.Image")));
            this.BtnBinTransferRequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBinTransferRequestDocNo2.Location = new System.Drawing.Point(272, 45);
            this.BtnBinTransferRequestDocNo2.Name = "BtnBinTransferRequestDocNo2";
            this.BtnBinTransferRequestDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnBinTransferRequestDocNo2.TabIndex = 17;
            this.BtnBinTransferRequestDocNo2.ToolTip = "Show Bin Transfer Request Information";
            this.BtnBinTransferRequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBinTransferRequestDocNo2.ToolTipTitle = "Run System";
            this.BtnBinTransferRequestDocNo2.Click += new System.EventHandler(this.BtnBinTransferRequestDocNo2_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtWhsCode2);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.TxtWhsCode);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(301, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(471, 70);
            this.panel4.TabIndex = 18;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.PanelTotal);
            this.panel5.Controls.Add(this.PanelTotal2);
            this.panel5.Controls.Add(this.PanelTotal3);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 225);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(772, 34);
            this.panel5.TabIndex = 27;
            // 
            // PanelTotal
            // 
            this.PanelTotal.Controls.Add(this.TxtInventoryUomCode);
            this.PanelTotal.Controls.Add(this.TxtTotal);
            this.PanelTotal.Controls.Add(this.label7);
            this.PanelTotal.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotal.Location = new System.Drawing.Point(29, 0);
            this.PanelTotal.Name = "PanelTotal";
            this.PanelTotal.Size = new System.Drawing.Size(243, 34);
            this.PanelTotal.TabIndex = 28;
            // 
            // TxtInventoryUomCode
            // 
            this.TxtInventoryUomCode.EnterMoveNextControl = true;
            this.TxtInventoryUomCode.Location = new System.Drawing.Point(137, 5);
            this.TxtInventoryUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryUomCode.Name = "TxtInventoryUomCode";
            this.TxtInventoryUomCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInventoryUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryUomCode.Properties.MaxLength = 16;
            this.TxtInventoryUomCode.Properties.ReadOnly = true;
            this.TxtInventoryUomCode.Size = new System.Drawing.Size(98, 20);
            this.TxtInventoryUomCode.TabIndex = 31;
            // 
            // TxtTotal
            // 
            this.TxtTotal.EnterMoveNextControl = true;
            this.TxtTotal.Location = new System.Drawing.Point(45, 5);
            this.TxtTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal.Properties.ReadOnly = true;
            this.TxtTotal.Size = new System.Drawing.Size(89, 20);
            this.TxtTotal.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(5, 8);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 14);
            this.label7.TabIndex = 29;
            this.label7.Text = "Total";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PanelTotal2
            // 
            this.PanelTotal2.Controls.Add(this.TxtInventoryUomCode2);
            this.PanelTotal2.Controls.Add(this.TxtTotal2);
            this.PanelTotal2.Controls.Add(this.label6);
            this.PanelTotal2.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotal2.Location = new System.Drawing.Point(272, 0);
            this.PanelTotal2.Name = "PanelTotal2";
            this.PanelTotal2.Size = new System.Drawing.Size(250, 34);
            this.PanelTotal2.TabIndex = 32;
            this.PanelTotal2.Visible = false;
            // 
            // TxtInventoryUomCode2
            // 
            this.TxtInventoryUomCode2.EnterMoveNextControl = true;
            this.TxtInventoryUomCode2.Location = new System.Drawing.Point(144, 5);
            this.TxtInventoryUomCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryUomCode2.Name = "TxtInventoryUomCode2";
            this.TxtInventoryUomCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInventoryUomCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryUomCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryUomCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryUomCode2.Properties.MaxLength = 16;
            this.TxtInventoryUomCode2.Properties.ReadOnly = true;
            this.TxtInventoryUomCode2.Size = new System.Drawing.Size(98, 20);
            this.TxtInventoryUomCode2.TabIndex = 35;
            // 
            // TxtTotal2
            // 
            this.TxtTotal2.EnterMoveNextControl = true;
            this.TxtTotal2.Location = new System.Drawing.Point(52, 5);
            this.TxtTotal2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal2.Name = "TxtTotal2";
            this.TxtTotal2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotal2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal2.Properties.ReadOnly = true;
            this.TxtTotal2.Size = new System.Drawing.Size(89, 20);
            this.TxtTotal2.TabIndex = 34;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(4, 8);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 14);
            this.label6.TabIndex = 33;
            this.label6.Text = "Total 2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PanelTotal3
            // 
            this.PanelTotal3.Controls.Add(this.TxtInventoryUomCode3);
            this.PanelTotal3.Controls.Add(this.TxtTotal3);
            this.PanelTotal3.Controls.Add(this.label15);
            this.PanelTotal3.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotal3.Location = new System.Drawing.Point(522, 0);
            this.PanelTotal3.Name = "PanelTotal3";
            this.PanelTotal3.Size = new System.Drawing.Size(250, 34);
            this.PanelTotal3.TabIndex = 36;
            this.PanelTotal3.Visible = false;
            // 
            // TxtInventoryUomCode3
            // 
            this.TxtInventoryUomCode3.EnterMoveNextControl = true;
            this.TxtInventoryUomCode3.Location = new System.Drawing.Point(144, 5);
            this.TxtInventoryUomCode3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryUomCode3.Name = "TxtInventoryUomCode3";
            this.TxtInventoryUomCode3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInventoryUomCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryUomCode3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryUomCode3.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryUomCode3.Properties.MaxLength = 16;
            this.TxtInventoryUomCode3.Properties.ReadOnly = true;
            this.TxtInventoryUomCode3.Size = new System.Drawing.Size(98, 20);
            this.TxtInventoryUomCode3.TabIndex = 39;
            // 
            // TxtTotal3
            // 
            this.TxtTotal3.EnterMoveNextControl = true;
            this.TxtTotal3.Location = new System.Drawing.Point(52, 5);
            this.TxtTotal3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal3.Name = "TxtTotal3";
            this.TxtTotal3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotal3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal3.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal3.Properties.ReadOnly = true;
            this.TxtTotal3.Size = new System.Drawing.Size(89, 20);
            this.TxtTotal3.TabIndex = 38;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(4, 8);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 14);
            this.label15.TabIndex = 37;
            this.label15.Text = "Total 3";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmBinTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmBinTransfer";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBinTransferRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWhsCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.PanelTotal.ResumeLayout(false);
            this.PanelTotal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).EndInit();
            this.PanelTotal2.ResumeLayout(false);
            this.PanelTotal2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2.Properties)).EndInit();
            this.PanelTotal3.ResumeLayout(false);
            this.PanelTotal3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnBinTransferRequestDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtBinTransferRequestDocNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtWhsCode2;
        internal DevExpress.XtraEditors.TextEdit TxtWhsCode;
        public DevExpress.XtraEditors.SimpleButton BtnBinTransferRequestDocNo2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel PanelTotal3;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryUomCode3;
        internal DevExpress.XtraEditors.TextEdit TxtTotal3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel PanelTotal2;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryUomCode2;
        internal DevExpress.XtraEditors.TextEdit TxtTotal2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel PanelTotal;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtTotal;
        private System.Windows.Forms.Label label7;
    }
}