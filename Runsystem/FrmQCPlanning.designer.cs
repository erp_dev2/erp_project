﻿namespace RunSystem
{
    partial class FrmQCPlanning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkActiveInd = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueQCPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtRefValue = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtTotalQty1 = new DevExpress.XtraEditors.TextEdit();
            this.LblTotalQty1 = new System.Windows.Forms.Label();
            this.LblTotalQty2 = new System.Windows.Forms.Label();
            this.LblTotalQty3 = new System.Windows.Forms.Label();
            this.TxtTotalQty2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalQty3 = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueWorkCenter = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueQCParameter = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueQCPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRefValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalQty1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalQty2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalQty3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWorkCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueQCParameter.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.LueDeptCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueWorkCenter);
            this.panel2.Controls.Add(this.TxtTotalQty3);
            this.panel2.Controls.Add(this.TxtTotalQty2);
            this.panel2.Controls.Add(this.LblTotalQty3);
            this.panel2.Controls.Add(this.LblTotalQty2);
            this.panel2.Controls.Add(this.TxtTotalQty1);
            this.panel2.Controls.Add(this.LblTotalQty1);
            this.panel2.Controls.Add(this.TxtRefValue);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.LueQCPCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.ChkActiveInd);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueWhsCode);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 201);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LueQCParameter);
            this.panel3.Location = new System.Drawing.Point(0, 201);
            this.panel3.Size = new System.Drawing.Size(772, 272);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.LueQCParameter, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 272);
            this.Grd1.TabIndex = 33;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Firebrick;
            this.label8.Location = new System.Drawing.Point(39, 50);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 14);
            this.label8.TabIndex = 15;
            this.label8.Text = "Location";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(97, 48);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 20;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 500;
            this.LueWhsCode.Size = new System.Drawing.Size(319, 20);
            this.LueWhsCode.TabIndex = 16;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(97, 174);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(450, 20);
            this.MeeRemark.TabIndex = 32;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(45, 175);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 31;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(97, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(105, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(59, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(97, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(28, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActiveInd
            // 
            this.ChkActiveInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActiveInd.Location = new System.Drawing.Point(266, 5);
            this.ChkActiveInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActiveInd.Name = "ChkActiveInd";
            this.ChkActiveInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActiveInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActiveInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActiveInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActiveInd.Properties.Caption = "Active";
            this.ChkActiveInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActiveInd.ShowToolTips = false;
            this.ChkActiveInd.Size = new System.Drawing.Size(67, 22);
            this.ChkActiveInd.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(29, 113);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 21;
            this.label3.Text = "Parameter";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueQCPCode
            // 
            this.LueQCPCode.EnterMoveNextControl = true;
            this.LueQCPCode.Location = new System.Drawing.Point(97, 111);
            this.LueQCPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueQCPCode.Name = "LueQCPCode";
            this.LueQCPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQCPCode.Properties.Appearance.Options.UseFont = true;
            this.LueQCPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQCPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueQCPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQCPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueQCPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQCPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueQCPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQCPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueQCPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueQCPCode.Properties.DropDownRows = 20;
            this.LueQCPCode.Properties.NullText = "[Empty]";
            this.LueQCPCode.Properties.PopupWidth = 500;
            this.LueQCPCode.Size = new System.Drawing.Size(319, 20);
            this.LueQCPCode.TabIndex = 22;
            this.LueQCPCode.ToolTip = "F4 : Show/hide list";
            this.LueQCPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueQCPCode.EditValueChanged += new System.EventHandler(this.LueQCPCode_EditValueChanged);
            // 
            // TxtRefValue
            // 
            this.TxtRefValue.EnterMoveNextControl = true;
            this.TxtRefValue.Location = new System.Drawing.Point(97, 132);
            this.TxtRefValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRefValue.Name = "TxtRefValue";
            this.TxtRefValue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRefValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRefValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRefValue.Properties.Appearance.Options.UseFont = true;
            this.TxtRefValue.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRefValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRefValue.Size = new System.Drawing.Size(105, 20);
            this.TxtRefValue.TabIndex = 24;
            this.TxtRefValue.Validated += new System.EventHandler(this.TxtRefValue_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(25, 135);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 14);
            this.label16.TabIndex = 23;
            this.label16.Text = "Refrerence";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalQty1
            // 
            this.TxtTotalQty1.EnterMoveNextControl = true;
            this.TxtTotalQty1.Location = new System.Drawing.Point(97, 153);
            this.TxtTotalQty1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalQty1.Name = "TxtTotalQty1";
            this.TxtTotalQty1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalQty1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalQty1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalQty1.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalQty1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalQty1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalQty1.Size = new System.Drawing.Size(105, 20);
            this.TxtTotalQty1.TabIndex = 26;
            this.TxtTotalQty1.Validated += new System.EventHandler(this.TxtTotalQty1_Validated);
            // 
            // LblTotalQty1
            // 
            this.LblTotalQty1.AutoSize = true;
            this.LblTotalQty1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalQty1.ForeColor = System.Drawing.Color.Black;
            this.LblTotalQty1.Location = new System.Drawing.Point(46, 156);
            this.LblTotalQty1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTotalQty1.Name = "LblTotalQty1";
            this.LblTotalQty1.Size = new System.Drawing.Size(46, 14);
            this.LblTotalQty1.TabIndex = 25;
            this.LblTotalQty1.Text = "Total 1";
            this.LblTotalQty1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblTotalQty2
            // 
            this.LblTotalQty2.AutoSize = true;
            this.LblTotalQty2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalQty2.ForeColor = System.Drawing.Color.Black;
            this.LblTotalQty2.Location = new System.Drawing.Point(216, 155);
            this.LblTotalQty2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTotalQty2.Name = "LblTotalQty2";
            this.LblTotalQty2.Size = new System.Drawing.Size(46, 14);
            this.LblTotalQty2.TabIndex = 27;
            this.LblTotalQty2.Text = "Total 2";
            this.LblTotalQty2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblTotalQty3
            // 
            this.LblTotalQty3.AutoSize = true;
            this.LblTotalQty3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalQty3.ForeColor = System.Drawing.Color.Black;
            this.LblTotalQty3.Location = new System.Drawing.Point(389, 155);
            this.LblTotalQty3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTotalQty3.Name = "LblTotalQty3";
            this.LblTotalQty3.Size = new System.Drawing.Size(46, 14);
            this.LblTotalQty3.TabIndex = 29;
            this.LblTotalQty3.Text = "Total 3";
            this.LblTotalQty3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalQty2
            // 
            this.TxtTotalQty2.EnterMoveNextControl = true;
            this.TxtTotalQty2.Location = new System.Drawing.Point(269, 152);
            this.TxtTotalQty2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalQty2.Name = "TxtTotalQty2";
            this.TxtTotalQty2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalQty2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalQty2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalQty2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalQty2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalQty2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalQty2.Size = new System.Drawing.Size(105, 20);
            this.TxtTotalQty2.TabIndex = 28;
            this.TxtTotalQty2.Validated += new System.EventHandler(this.TxtTotalQty2_Validated);
            // 
            // TxtTotalQty3
            // 
            this.TxtTotalQty3.EnterMoveNextControl = true;
            this.TxtTotalQty3.Location = new System.Drawing.Point(442, 151);
            this.TxtTotalQty3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalQty3.Name = "TxtTotalQty3";
            this.TxtTotalQty3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalQty3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalQty3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalQty3.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalQty3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalQty3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalQty3.Size = new System.Drawing.Size(105, 20);
            this.TxtTotalQty3.TabIndex = 30;
            this.TxtTotalQty3.Validated += new System.EventHandler(this.TxtTotalQty3_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Firebrick;
            this.label4.Location = new System.Drawing.Point(15, 71);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 17;
            this.label4.Text = "Work Center";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWorkCenter
            // 
            this.LueWorkCenter.EnterMoveNextControl = true;
            this.LueWorkCenter.Location = new System.Drawing.Point(97, 69);
            this.LueWorkCenter.Margin = new System.Windows.Forms.Padding(5);
            this.LueWorkCenter.Name = "LueWorkCenter";
            this.LueWorkCenter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkCenter.Properties.Appearance.Options.UseFont = true;
            this.LueWorkCenter.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkCenter.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWorkCenter.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkCenter.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWorkCenter.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkCenter.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWorkCenter.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkCenter.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWorkCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWorkCenter.Properties.DropDownRows = 20;
            this.LueWorkCenter.Properties.NullText = "[Empty]";
            this.LueWorkCenter.Properties.PopupWidth = 500;
            this.LueWorkCenter.Size = new System.Drawing.Size(319, 20);
            this.LueWorkCenter.TabIndex = 18;
            this.LueWorkCenter.ToolTip = "F4 : Show/hide list";
            this.LueWorkCenter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWorkCenter.EditValueChanged += new System.EventHandler(this.LueWorkCenter_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(19, 92);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 14);
            this.label6.TabIndex = 19;
            this.label6.Text = "Department";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(97, 90);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 350;
            this.LueDeptCode.Size = new System.Drawing.Size(319, 20);
            this.LueDeptCode.TabIndex = 20;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // LueQCParameter
            // 
            this.LueQCParameter.EnterMoveNextControl = true;
            this.LueQCParameter.Location = new System.Drawing.Point(148, 42);
            this.LueQCParameter.Margin = new System.Windows.Forms.Padding(5);
            this.LueQCParameter.Name = "LueQCParameter";
            this.LueQCParameter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQCParameter.Properties.Appearance.Options.UseFont = true;
            this.LueQCParameter.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQCParameter.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueQCParameter.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQCParameter.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueQCParameter.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQCParameter.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueQCParameter.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQCParameter.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueQCParameter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueQCParameter.Properties.DropDownRows = 20;
            this.LueQCParameter.Properties.NullText = "[Empty]";
            this.LueQCParameter.Properties.PopupWidth = 500;
            this.LueQCParameter.Size = new System.Drawing.Size(171, 20);
            this.LueQCParameter.TabIndex = 35;
            this.LueQCParameter.ToolTip = "F4 : Show/hide list";
            this.LueQCParameter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueQCParameter.EditValueChanged += new System.EventHandler(this.LueQCParameter_EditValueChanged);
            this.LueQCParameter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueQCParameter_KeyDown);
            this.LueQCParameter.Leave += new System.EventHandler(this.LueQCParameter_Leave);
            // 
            // FrmQCPlanning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmQCPlanning";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueQCPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRefValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalQty1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalQty2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalQty3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWorkCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueQCParameter.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkActiveInd;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueQCPCode;
        internal DevExpress.XtraEditors.TextEdit TxtRefValue;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtTotalQty1;
        private System.Windows.Forms.Label LblTotalQty1;
        private System.Windows.Forms.Label LblTotalQty3;
        private System.Windows.Forms.Label LblTotalQty2;
        internal DevExpress.XtraEditors.TextEdit TxtTotalQty3;
        internal DevExpress.XtraEditors.TextEdit TxtTotalQty2;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.LookUpEdit LueWorkCenter;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        internal DevExpress.XtraEditors.LookUpEdit LueQCParameter;
    }
}