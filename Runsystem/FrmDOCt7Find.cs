﻿#region Update
/*
    25/06/2020 [WED/KSM] new apps
    19/02/2021 [IBL/KSM] Tambah kolom customer category berdasarkan parameter IsCustomerComboShowCategory
 *  04/05/2021 [ICA/KSM] Memecah menu CBD dan Non CBD
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt7Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmDOCt7 mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsInventoryShowTotalQty = false;

        #endregion

        #region Constructor

        public FrmDOCt7Find(FrmDOCt7 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, B.WhsName, C.CtName, A.DRDocNo, A.PLDocNo, A.Cnt, ");
            SQL.AppendLine("D.CancelInd, D.ItCode, E.ItName, D.BatchNo, D.Source, ");
            SQL.AppendLine("D.Qty, E.InventoryUomCode, D.Qty2, E.InventoryUomCode2, D.Qty3, E.InventoryUomCode3, F.CtCtName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode And A.DocType = '3' ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=B.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl D On A.DocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join TblCustomerCategory F On C.CtCtCode = F.CtCtCode ");
            SQL.AppendLine("Inner Join TblDRHdr G On A.DRDocNo = G.DocNo ");
            if (mFrmParent.mIsDOCtWithCBD)
                SQL.AppendLine("And G.CBDInd = 'Y' ");
            else
                SQL.AppendLine("And G.CBDInd = 'N' ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Local"+Environment.NewLine+"Document#",
                    "Warehouse",
                    "Customer",
                    
                    //6-10
                    "Customer Category",
                    "DR#",
                    "PL#",
                    "Container#",
                    "Cancel",

                    //11-15
                    "Item's"+Environment.NewLine+"Code",
                    "Item's Name",
                    "Batch#",
                    "Source",
                    "Quantity",

                    //16-20
                    "UoM",
                    "Quantity",
                    "UoM",
                    "Quantity",
                    "UoM",

                    //21-25
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 

                    //26
                    "Last"+Environment.NewLine+"Updated Time",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 130, 200, 250, 
                    
                    //6-10
                    150, 140, 140, 150, 60,
                    
                    //11-15
                    100, 200, 180, 180, 100, 

                    //16-20
                    80, 100, 80, 100, 80, 

                    //21-25
                    100, 100, 100, 100, 100,

                    //26
                    100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 15, 17, 19 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 22, 25, });
            Sm.GrdFormatTime(Grd1, new int[] { 23, 26 });
            Sm.GrdColInvisible(Grd1, new int[] { 11, 14, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 }, false);
            if(!mFrmParent.mIsCustomerComboShowCategory)
                Sm.GrdColInvisible(Grd1, new int[] { 6 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11, 14, 21, 22, 23, 24, 25, 26 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter = " And D.CancelInd='N' ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "LocalDocNo", "WhsName", "CtName", "CtCtName", 
                        
                        //6-10
                        "DRDocNo", "PLDocNo", "Cnt", "CancelInd", "ItCode", 
                        
                        //11-15
                        "ItName", "BatchNo", "Source", "Qty", "InventoryUomCode", 
                        
                        //16-20
                        "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CreateBy", 
                        
                        //21-23
                        "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 23);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20 }, true);
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local document#");
        }

        #endregion

        #endregion
    }
}
