﻿#region Update
/*
 * 14/09/2021 [NJP/RUNMARKET] Menambahkan Master Transaction Fee
 * 14/09/2021 [NJP/RUNMARKET] Menambahkan Master Transaction Fee Find
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFee : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmFeeFind FrmFind;

        #endregion

        #region Constructor

        public FrmFee(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                ClearData();
                BtnDelete.Visible = false;
                BtnPrint.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }
        #endregion

        #region Standart Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtFeeCode, TxtFeeName, TxtAmt, ChkActiveInd
                    }, true);
                    TxtFeeCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtFeeCode, TxtFeeName, TxtAmt
                    }, false);
                    ChkActiveInd.Checked = true;
                    TxtFeeCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtFeeName, TxtAmt, ChkActiveInd 
                    }, false);
                    if (!ChkActiveInd.Checked) ChkActiveInd.Properties.ReadOnly = true;
                    //ChkActiveInd.Checked = true;
                    TxtFeeName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtFeeCode, TxtFeeName, 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkActiveInd.Checked = false;
        }
        
        #endregion
        
        #region Button Method
        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFeeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsDataNotValid()) return;

                SaveData();

                ShowData(TxtFeeCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtFeeCode, "", false)) return;
            SetFormControl(mState.Edit);
        }


        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtFeeCode, "Transaction Fee Code", false) ||
                Sm.IsTxtEmpty(TxtFeeName, "Transaction Fee Name", false) ||
                Sm.IsTxtEmpty(TxtAmt, "Transaction Fee Value", true) ||
                IsDataNotActive()||
                IsCodeExisted();
        }

        private bool IsDataNotActive()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT 1 ");
            SQL.AppendLine("FROM TblFee ");
            SQL.AppendLine("WHERE  FeeCode = @Param ");
            SQL.AppendLine("AND ActInd = 'N' ");
            SQL.AppendLine("LIMIT 1 ");

            if (Sm.IsDataExist(SQL.ToString(),TxtFeeCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is Inactive.");
                return true;
            }

            return false;
        }

        private bool IsCodeExisted()
        {
            if (!TxtFeeCode.Properties.ReadOnly && Sm.IsDataExist("Select FeeCode From TblFee Where FeeCode='" + TxtFeeCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Transsaction Fee code ( " + TxtFeeCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private void SaveData()
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblFee(FeeCode, FeeName, AMT, ActInd, ");
                SQL.AppendLine("    CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@FeeCode, @FeeName, @Amt, @ActInd, ");
                SQL.AppendLine("    @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("Update ");
                SQL.AppendLine("   FeeName = @FeeName, AMT = @Amt, ActInd=@ActInd, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@FeeCode", TxtFeeCode.Text);
                Sm.CmParam<String>(ref cm, "@FeeName", TxtFeeName.Text);
                Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

        }

        #endregion

        #region Show Data

        internal void ShowData(String Code)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@Code", Code);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select FeeCode, FeeName, Amt, ActInd From TblFee Where FeeCode=@Code Limit 1",
                        new string[] 
                        {
                            "FeeCode",
 
                            "FeeName", "AMT", "ActInd" 
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtFeeCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtFeeName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]),2);
                            ChkActiveInd.Checked = Sm.DrStr(dr, c[3]) == "Y";                         

                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion        

        #endregion

        #region Misc Control Event

        private void TxtFeeCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFeeCode);
        }

        private void TxtFeeName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFeeName);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            TxtAmt.EditValue = Sm.FormatNum(TxtAmt.Text, 0);
        }
        #endregion

        
    }
}
