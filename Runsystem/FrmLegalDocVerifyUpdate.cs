﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLegalDocVerifyUpdate : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmLegalDocVerifyUpdateFind FrmFind;
        internal string 
            mGlobalVendorRMP = string.Empty,
            mVdCode = string.Empty, 
            mVdCode1 = string.Empty, 
            mVdCode2 = string.Empty,
            mLegalDocVerifyDocNo = string.Empty;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmLegalDocVerifyUpdate(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Verifikasi Legalitas Dokumen";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd(ref Grd1);
                SetGrd(ref Grd2);
                SetFormControl(mState.View);

                tabControl1.SelectTab("tabPage2");
                Sl.SetLueDLCode(ref LueDLCode1);
                LueDLCode1.Visible = false;

                tabControl1.SelectTab("tabPage3");
                Sl.SetLueDLCode(ref LueDLCode2);
                LueDLCode2.Visible = false;

                tabControl1.SelectTab("tabPage1");

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtQueueNo, TxtVehicleRegNo, TxtTTCode,
                        TxtVdName, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode,
                        LueVdCode1, TxtIdentityNo1, MeeAddress1, TxtVilCode1, TxtSDCode1, TxtCityCode1, TxtProvCode1,
                        LueVdCode2, TxtIdentityNo2, MeeAddress2, TxtVilCode2, TxtSDCode2, TxtCityCode2, TxtProvCode2
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    BtnQueueNo.Enabled = BtnVdCode.Enabled = BtnVdCode1.Enabled = BtnVdCode2.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueVdCode1, LueVdCode2 }, false);
                    BtnQueueNo.Enabled = BtnQueueNo.Enabled = BtnVdCode.Enabled = BtnVdCode1.Enabled = BtnVdCode2.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 2, 3, 4, 5, 6 });
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mLegalDocVerifyDocNo = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtQueueNo, TxtVehicleRegNo, TxtTTCode,
                TxtVdName, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode, 
                LueVdCode1, TxtIdentityNo1, MeeAddress1, TxtVilCode1, TxtSDCode1, TxtCityCode1, TxtProvCode1, 
                LueVdCode2, TxtIdentityNo2, MeeAddress2, TxtVilCode2, TxtSDCode2, TxtCityCode2, TxtProvCode2
            });
            mVdCode = mVdCode1 = mVdCode2 = string.Empty;
            ClearGrd(ref Grd1);
            ClearGrd(ref Grd2);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtQueueNo, TxtVehicleRegNo, TxtTTCode,
                TxtVdName, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode, 
                LueVdCode1, TxtIdentityNo1, MeeAddress1, TxtVilCode1, TxtSDCode1, TxtCityCode1, TxtProvCode1, 
                LueVdCode2, TxtIdentityNo2, MeeAddress2, TxtVilCode2, TxtSDCode2, TxtCityCode2, TxtProvCode2
            });
            mVdCode = mVdCode1 = mVdCode2 = string.Empty;
            ClearGrd(ref Grd1);
            ClearGrd(ref Grd2);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetGrd(ref iGrid Grd)
        {
            Grd.Cols.Count = 7;
            Sm.GrdHdrWithColWidth(
                Grd,
                new string[] { 
                    //0
                    "Code", 
                    
                    //1-5
                    "Document Legalitas", "Nomor Dokumen", "Tgl.Dokumen", "Volume", "Jumlah", 
                    
                    //6
                    "Keterangan" 
                },
                new int[] 
                { 
                    //0
                    0, 

                    //1-5
                    250, 130, 100, 100, 100,
                    
                    //6
                    350 
                }
            );
            Sm.GrdFormatDec(Grd, new int[] { 4, 5 }, 0);
            Sm.GrdFormatDate(Grd, new int[] { 3 });
        }

        private void ClearGrd(ref iGrid Grd)
        {
            Sm.ClearGrd(Grd, true);
            Sm.SetGrdNumValueZero(ref Grd, 0, new int[] { 4, 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLegalDocVerifyUpdateFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueVdCode(ref LueVdCode1, string.Empty);
                SetLueVdCode(ref LueVdCode2, string.Empty);
                tabControl1.SelectTab("tabPage1");
                BtnQueueNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void GrdRequestEdit(ref iGrid Grd, ref DXE.LookUpEdit LueDLCode, ref DXE.DateEdit DteDLDocDt, object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd, LueDLCode, ref fCell, ref fAccept, e);
                if (e.ColIndex == 3) Sm.DteRequestEdit(Grd, DteDLDocDt, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 4, 5 });
            }
        }

        private void GrdKeyDown(ref iGrid Grd, object sender, KeyEventArgs e)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select LegalDocVerifyDocNo From TblRawMaterialVerify Where LegalDocVerifyDocNo=@DocNo ",
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already verified.");
            }
            else
            {
                //if (TxtDocNo.Text.Length == 0)
                Sm.GrdRemoveRow(Grd, e, BtnSave);
                Sm.GrdKeyDown(Grd, e, BtnFind, BtnSave);
            }
        }

        private void GrdAfterCommitEdit(ref iGrid Grd, object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd, new int[] { 2, 6 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd, new int[] { 4, 5 }, e);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "LegalDocVerifyUpdate", "TblLegalDocVerifyUpdateHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveLegalDocVerifyUpdateHdr(DocNo));

            int DNo = 0;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveLegalDocVerifyUpdateDtl("1", ref Grd1, ref DNo, DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveLegalDocVerifyUpdateDtl("2", ref Grd2, ref DNo, DocNo, Row));

            cml.Add(SaveLegalDocVerifyDtl(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtQueueNo, "Nomor antrian", false) ||
                Sm.IsTxtEmpty(TxtVehicleRegNo, "Nomor polisi", false) ||
                Sm.IsTxtEmpty(TxtVdName, "Vendor", false) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords(ref Grd1) ||
                IsGrdExceedMaxRecords(ref Grd2) ||
                IsGrdValueNotValid(ref Grd1) ||
                IsGrdValueNotValid(ref Grd2) ||
                IsDoubleEntry(ref Grd1) ||
                IsDoubleEntry(ref Grd2) ||
                IsPurchaseInvoiceAlreadyProcessed()
                ;
        }

        private bool IsPurchaseInvoiceAlreadyProcessed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("Inner Join TblRawMaterialVerify B On A.RawMaterialVerifyDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr C On B.LegalDocVerifyDocNo=C.DocNo And C.DocNo=@DocNo ");
            SQL.AppendLine("Where A.CanceLind='N';");
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", mLegalDocVerifyDocNo);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed to Purchase invoice.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1 && Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Kedua data dokumen log dan balok masih kosong.");
                return true;
            }

            if (mVdCode1.Length!=0 && Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Data dokumen log masih kosong.");
                return true;
            }

            if (mVdCode1.Length == 0 && Grd1.Rows.Count > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Vendor log masih kosong.");
                return true;
            }

            if (mVdCode2.Length != 0 && Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Data dokumen balok masih kosong.");
                return true;
            }

            if (mVdCode2.Length == 0 && Grd2.Rows.Count > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Vendor balok masih kosong.");
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords(ref iGrid Grd)
        {
            if (Grd.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid(ref iGrid Grd)
        {
            if (Grd.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd, Row, 1, false, "Document is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsDoubleEntry(ref iGrid Grd)
        {
            if (Grd.Rows.Count > 1)
            {
                for (int Row1 = 0; Row1 < Grd.Rows.Count - 1; Row1++)
                {
                    if (Sm.GetGrdStr(Grd, Row1, 0).Length != 0)
                    {
                        for (int Row2 = 0; Row2 < Grd.Rows.Count - 1; Row2++)
                        {
                            if (Row1 != Row2 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd, Row1, 0), Sm.GetGrdStr(Grd, Row2, 0)))
                            {
                                Sm.StdMsg(mMsgType.Warning,
                                    "Dokumen : " + Sm.GetGrdStr(Grd, Row2, 1) + Environment.NewLine + Environment.NewLine +
                                    "Dokumen dimasukkan lebih dari 1 kali. ");
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }


        private MySqlCommand SaveLegalDocVerifyUpdateHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLegalDocVerifyUpdateHdr(DocNo, DocDt, LegalDocVerifyDocNo, QueueNo, VehicleRegNo, VdCode, VdCode1, VdCode2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LegalDocVerifyDocNo, @QueueNo, @VehicleRegNo, @VdCode, @VdCode1, @VdCode2, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblLegalDocVerifyHdr Set ");
            SQL.AppendLine("    VehicleRegNo=@VehicleRegNo, ");
            SQL.AppendLine("    VdCode=@VdCode, ");
            SQL.AppendLine("    VdCode1=@VdCode1, ");
            SQL.AppendLine("    VdCode2=@VdCode2 ");
            SQL.AppendLine("Where DocNo=@LegalDocVerifyDocNo; ");

            SQL.AppendLine("Delete From TblLegalDocVerifyDtl Where DocNo=@LegalDocVerifyDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@QueueNo", TxtQueueNo.Text);
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", mLegalDocVerifyDocNo);
            Sm.CmParam<String>(ref cm, "@VehicleRegNo", TxtVehicleRegNo.Text);
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm, "@VdCode1", mVdCode1);
            Sm.CmParam<String>(ref cm, "@VdCode2", mVdCode2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveLegalDocVerifyUpdateDtl(string DLType, ref iGrid Grd, ref int DNo, string DocNo, int Row)
        {
            DNo += 1;
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblLegalDocVerifyUpdateDtl(DocNo, DNo, DLType, DLCode, DLDocNo, DLDocDt, Volume, Qty, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @DLType, @DLCode, @DLDocNo, @DLDocDt, @Volume, @Qty, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DLType", DLType);
            Sm.CmParam<String>(ref cm, "@DLCode", Sm.GetGrdStr(Grd, Row, 0));
            Sm.CmParam<String>(ref cm, "@DLDocNo", Sm.GetGrdStr(Grd, Row, 2));
            Sm.CmParamDt(ref cm, "@DLDocDt", Sm.GetGrdDate(Grd, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Volume", Sm.GetGrdDec(Grd, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd, Row, 5));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveLegalDocVerifyDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLegalDocVerifyDtl ");
            SQL.AppendLine("(DocNo, DNo, DLType, DLCode, DLDocNo, DLDocDt, Volume, Qty, Remark, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select @LegalDocVerifyDocNo, DNo, DLType, DLCode, DLDocNo, DLDocDt, Volume, Qty, Remark, CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From TblLegalDocVerifyUpdateDtl ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", mLegalDocVerifyDocNo);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowLegalDocVerifyUpdateHdr(DocNo);
                ShowVdInfo(mVdCode, TxtVdName, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode);
                ShowVdInfo2(mVdCode1, LueVdCode1, TxtIdentityNo1, MeeAddress1, TxtVilCode1, TxtSDCode1, TxtCityCode1, TxtProvCode1);
                ShowVdInfo2(mVdCode2, LueVdCode2, TxtIdentityNo2, MeeAddress2, TxtVilCode2, TxtSDCode2, TxtCityCode2, TxtProvCode2);
                ShowLegalDocVerifyUpdateDtl("1", ref Grd1, DocNo);
                ShowLegalDocVerifyUpdateDtl("2", ref Grd2, DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                tabControl1.SelectTab("tabPage1");
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLegalDocVerifyUpdateHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.LegalDocVerifyDocNo, A.QueueNo, A.VehicleRegNo, A.VdCode, A.VdCode1, A.VdCode2, C.TTName " +
                    "From TblLegalDocVerifyUpdateHdr A " +
                    "Left Join TblLoadingQueue B On A.QueueNo=B.DocNo " +
                    "Left Join TblTransportType C On B.TTCode=C.TTCode " +
                    "Where A.DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "LegalDocVerifyDocNo", "QueueNo", "VehicleRegNo", "TTName",  
                        
                        //6-8
                        "VdCode", "VdCode1", "VdCode2"  
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        mLegalDocVerifyDocNo = Sm.DrStr(dr, c[2]);
                        TxtQueueNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtVehicleRegNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtTTCode.EditValue = Sm.DrStr(dr, c[5]);
                        mVdCode = Sm.DrStr(dr, c[6]);
                        mVdCode1 = Sm.DrStr(dr, c[7]);
                        mVdCode2 = Sm.DrStr(dr, c[8]);
                    }, true
                );
        }

        private void ShowLegalDocVerifyUpdateDtl(string DLType, ref iGrid grd, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DLCode, B.DLName, A.DLDocNo, A.DLDocDt, A.Volume, A.Qty, A.Remark ");
            SQL.AppendLine("From TblLegalDocVerifyUpdateDtl A ");
            SQL.AppendLine("Left Join TblDocLegality B On A.DLCode=B.DLCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DLType=@DLType ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DLType", DLType);
            Sm.ShowDataInGrid(
                ref grd, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "DLCode", 
                    "DLName", "DLDocNo", "DLDocDt", "Volume", "Qty", 
                    "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref grd, grd.Rows.Count - 1, new int[] { 4, 5 });
            Sm.FocusGrd(grd, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mGlobalVendorRMP = Sm.GetParameter("GlobalVendorRMP");
        }

        private void SetLueVdCode(ref DXE.LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select VdCode As Col1, VdName As Col2 From TblVendor Where ");
            if (Code.Length > 0)
                SQL.AppendLine("VdCode=@Code;");
            else
                SQL.AppendLine("0=1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }


        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ShowVdInfo(
            string VdCode, DXE.TextEdit TxtVdName, 
            DXE.TextEdit TxtIdentityNo, DXE.MemoExEdit MeeAddress, DXE.TextEdit TxtVilCode, 
            DXE.TextEdit TxtSDCode, DXE.TextEdit TxtCityCode, DXE.TextEdit TxtProvCode
            )
        {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtVdName, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode
                });

            if (VdCode.Length == 0) return;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.VdName, A.IdentityNo, A.Address, B.VilName, C.SDName, D.CityName, E.ProvName ");
            SQL.AppendLine("From TblVendor A ");
            SQL.AppendLine("Left Join TblVillage B On A.VilCode=B.VilCode ");
            SQL.AppendLine("Left Join TblSubDistrict C On A.SDCode=C.SDCode ");
            SQL.AppendLine("Left Join TblCity D On A.CityCode=D.CityCode ");
            SQL.AppendLine("Left Join TblProvince E On D.ProvCode=E.ProvCode ");
            SQL.AppendLine("Where A.VdCode=@VdCode; ");

            try
            {
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

                Sm.ShowDataInCtrl(
                        ref cm,
                        SQL.ToString(),
                        new string[] 
                        { 
                            //0
                            "VdName",

                            //1-5
                            "IdentityNo", "Address", "VilName", "SDName", "CityName", 
                            
                            //6
                            "ProvName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtVdName.EditValue = Sm.DrStr(dr, c[0]);
                            TxtIdentityNo.EditValue = Sm.DrStr(dr, c[1]);
                            MeeAddress.EditValue = Sm.DrStr(dr, c[2]);
                            TxtVilCode.EditValue = Sm.DrStr(dr, c[3]);
                            TxtSDCode.EditValue = Sm.DrStr(dr, c[4]);
                            TxtCityCode.EditValue = Sm.DrStr(dr, c[5]);
                            TxtProvCode.EditValue = Sm.DrStr(dr, c[6]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ShowVdInfo2(
            string VdCode, DXE.LookUpEdit LueVdCode, 
            DXE.TextEdit TxtIdentityNo, DXE.MemoExEdit MeeAddress, DXE.TextEdit TxtVilCode,
            DXE.TextEdit TxtSDCode, DXE.TextEdit TxtCityCode, DXE.TextEdit TxtProvCode
            )
        {
            SetLueVdCode(ref LueVdCode, string.Empty);
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    LueVdCode, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode
                });

            if (VdCode.Length == 0) return;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.IdentityNo, A.Address, B.VilName, C.SDName, D.CityName, E.ProvName ");
            SQL.AppendLine("From TblVendor A ");
            SQL.AppendLine("Left Join TblVillage B On A.VilCode=B.VilCode ");
            SQL.AppendLine("Left Join TblSubDistrict C On A.SDCode=C.SDCode ");
            SQL.AppendLine("Left Join TblCity D On A.CityCode=D.CityCode ");
            SQL.AppendLine("Left Join TblProvince E On D.ProvCode=E.ProvCode ");
            SQL.AppendLine("Where A.VdCode=@VdCode; ");

            try
            {
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

                Sm.ShowDataInCtrl(
                        ref cm,
                        SQL.ToString(),
                        new string[] 
                        { 
                            //0
                            "IdentityNo", 

                            //1-5
                            "Address", "VilName", "SDName", "CityName", "ProvName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            SetLueVdCode(ref LueVdCode, VdCode);
                            TxtIdentityNo.EditValue = Sm.DrStr(dr, c[0]);
                            MeeAddress.EditValue = Sm.DrStr(dr, c[1]);
                            TxtVilCode.EditValue = Sm.DrStr(dr, c[2]);
                            TxtSDCode.EditValue = Sm.DrStr(dr, c[3]);
                            TxtCityCode.EditValue = Sm.DrStr(dr, c[4]);
                            TxtProvCode.EditValue = Sm.DrStr(dr, c[5]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsQueueNoNotValid()
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo ");
                SQL.AppendLine("From TblLoadingQueue A ");
                SQL.AppendLine("Left Join TblLegalDocVerifyHdr B On A.DocNo=B.QueueNo And B.CancelInd='N' ");
                SQL.AppendLine("Where A.DocNo=@DocNo And B.QueueNo Is Null");
                SQL.AppendLine("Limit 1");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtQueueNo.Text);

                if (Sm.IsDataExist(cm)) return false;

                Sm.StdMsg(mMsgType.Warning,
                    "Nomor Antrian : " + TxtQueueNo.Text + Environment.NewLine + Environment.NewLine +
                    "Nomor antrian ini tidak benar."
                    );
                TxtQueueNo.EditValue = null;
                TxtQueueNo.Focus();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return true;
        }

        private void SetVehicleRegNo()
        {
            try
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select LicenceNo From TblLoadingQueue Where DocNo=@DocNo Limit 1"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtQueueNo.Text);

                var VehicleRegNo = Sm.GetValue(cm);
                TxtVehicleRegNo.EditValue = VehicleRegNo;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public void ShowLegalDocVerifyInfo()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData2();
                ShowLegalDocVerifyHdr();
                ShowVdInfo(mVdCode, TxtVdName, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode);
                ShowVdInfo2(mVdCode1, LueVdCode1, TxtIdentityNo1, MeeAddress1, TxtVilCode1, TxtSDCode1, TxtCityCode1, TxtProvCode1);
                ShowVdInfo2(mVdCode2, LueVdCode2, TxtIdentityNo2, MeeAddress2, TxtVilCode2, TxtSDCode2, TxtCityCode2, TxtProvCode2);
                ShowLegalDocVerifyDtl("1", ref Grd1);
                ShowLegalDocVerifyDtl("2", ref Grd2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLegalDocVerifyHdr()
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", mLegalDocVerifyDocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.QueueNo, A.VehicleRegNo, ");
            SQL.AppendLine("A.VdCode, A.VdCode1, A.VdCode2, C.TTName ");
            SQL.AppendLine("From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("Left Join TblLoadingQueue B On A.QueueNo=B.DocNo ");
            SQL.AppendLine("Left Join TblTransportType C On B.TTCode=C.TTCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "QueueNo", "VehicleRegNo", "TTName",  "VdCode", "VdCode1", 
                        
                        //6
                        "VdCode2"  
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        mLegalDocVerifyDocNo = Sm.DrStr(dr, c[0]);
                        TxtQueueNo.EditValue = Sm.DrStr(dr, c[1]);
                        TxtVehicleRegNo.EditValue = Sm.DrStr(dr, c[2]);
                        TxtTTCode.EditValue = Sm.DrStr(dr, c[3]);
                        mVdCode = Sm.DrStr(dr, c[4]);
                        mVdCode1 = Sm.DrStr(dr, c[5]);
                        mVdCode2 = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowLegalDocVerifyDtl(string DLType, ref iGrid grd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DLCode, B.DLName, A.DLDocNo, A.DLDocDt, A.Volume, A.Qty, A.Remark ");
            SQL.AppendLine("From TblLegalDocVerifyDtl A ");
            SQL.AppendLine("Left Join TblDocLegality B On A.DLCode=B.DLCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DLType=@DLType ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", mLegalDocVerifyDocNo);
            Sm.CmParam<String>(ref cm, "@DLType", DLType);
            Sm.ShowDataInGrid(
                ref grd, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "DLCode", 
                    "DLName", "DLDocNo", "DLDocDt", "Volume", "Qty", 
                    "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref grd, grd.Rows.Count - 1, new int[] { 4, 5 });
            Sm.FocusGrd(grd, 0, 1);
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnQueueNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmLegalDocVerifyUpdateDlg(this));
        }

        private void BtnVdCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmLegalDocVerifyUpdateDlg2(this, 0));
        }

        private void BtnVdCode1_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmLegalDocVerifyUpdateDlg2(this, 1));
        }

        private void BtnVdCode2_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmLegalDocVerifyUpdateDlg2(this, 2));
        }

        #endregion

        #region Misc Control Event

        private void LueDLCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDLCode1, new Sm.RefreshLue1(Sl.SetLueDLCode));
        }

        private void LueDLCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDLCode2, new Sm.RefreshLue1(Sl.SetLueDLCode));
        }

        private void LueDLCode1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDLCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueDLCode1_Leave(object sender, EventArgs e)
        {
            if (LueDLCode1.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueDLCode1).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value =
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueDLCode1);
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueDLCode1.GetColumnValue("Col2");
                }
                LueDLCode1.Visible = false;
            }
        }

        private void DteDLDocDt1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteDLDocDt1_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDLDocDt1, ref fCell, ref fAccept);
        }

        private void LueDLCode2_Leave(object sender, EventArgs e)
        {
            if (LueDLCode2.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueDLCode2).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 0].Value =
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueDLCode2);
                    Grd2.Cells[fCell.RowIndex, 1].Value = LueDLCode2.GetColumnValue("Col2");
                }
                LueDLCode2.Visible = false;
            }
        }

        private void DteDLDocDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void DteDLDocDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDLDocDt2, ref fCell, ref fAccept);
        }

        
        private void LueVdCode1_Validated(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode1, new Sm.RefreshLue2(SetLueVdCode), mVdCode1);
            if (Sm.GetLue(LueVdCode1).Length == 0) mVdCode1 = string.Empty;
            ShowVdInfo2(mVdCode1, LueVdCode1, TxtIdentityNo1, MeeAddress1, TxtVilCode1, TxtSDCode1, TxtCityCode1, TxtProvCode1);
        }

        private void LueVdCode2_Validated(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode2, new Sm.RefreshLue2(SetLueVdCode), mVdCode2);
            if (Sm.GetLue(LueVdCode2).Length == 0) mVdCode2 = string.Empty;
            ShowVdInfo2(mVdCode2, LueVdCode2, TxtIdentityNo2, MeeAddress2, TxtVilCode2, TxtSDCode2, TxtCityCode2, TxtProvCode2);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdRequestEdit(ref Grd1, ref LueDLCode1, ref DteDLDocDt1, sender, e);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            GrdKeyDown(ref Grd1, sender, e);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdAfterCommitEdit(ref Grd1, sender, e);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdRequestEdit(ref Grd2, ref LueDLCode2, ref DteDLDocDt2, sender, e);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdAfterCommitEdit(ref Grd2, sender, e);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            GrdKeyDown(ref Grd2, sender, e);
        }

        #endregion

        #endregion
    }
}
