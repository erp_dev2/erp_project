﻿namespace RunSystem
{
    partial class FrmPrintGL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.LblCOAAc = new System.Windows.Forms.Label();
            this.ChkProfitCenterCode = new DevExpress.XtraEditors.CheckEdit();
            this.LblMultiProfitCenterCode = new System.Windows.Forms.Label();
            this.CcbAcNo = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.ChkAcNo = new DevExpress.XtraEditors.CheckEdit();
            this.LueCOAAc = new DevExpress.XtraEditors.LookUpEdit();
            this.CcbProfitCenterCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.TxtAcNo1 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo4 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo5 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo6 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo7 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo8 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo9 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo10 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo11 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo12 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo13 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo14 = new DevExpress.XtraEditors.TextEdit();
            this.ChkAcNo2 = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCOAAc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(400, 0);
            this.panel1.Size = new System.Drawing.Size(82, 75);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Size = new System.Drawing.Size(82, 33);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkAcNo2);
            this.panel2.Controls.Add(this.TxtAcNo1);
            this.panel2.Controls.Add(this.TxtAcNo2);
            this.panel2.Controls.Add(this.TxtAcNo3);
            this.panel2.Controls.Add(this.TxtAcNo4);
            this.panel2.Controls.Add(this.TxtAcNo5);
            this.panel2.Controls.Add(this.TxtAcNo6);
            this.panel2.Controls.Add(this.TxtAcNo7);
            this.panel2.Controls.Add(this.TxtAcNo8);
            this.panel2.Controls.Add(this.TxtAcNo9);
            this.panel2.Controls.Add(this.TxtAcNo10);
            this.panel2.Controls.Add(this.TxtAcNo11);
            this.panel2.Controls.Add(this.TxtAcNo12);
            this.panel2.Controls.Add(this.TxtAcNo13);
            this.panel2.Controls.Add(this.TxtAcNo14);
            this.panel2.Controls.Add(this.CcbProfitCenterCode);
            this.panel2.Controls.Add(this.LueCOAAc);
            this.panel2.Controls.Add(this.CcbAcNo);
            this.panel2.Controls.Add(this.ChkAcNo);
            this.panel2.Controls.Add(this.ChkProfitCenterCode);
            this.panel2.Controls.Add(this.LblMultiProfitCenterCode);
            this.panel2.Controls.Add(this.LblCOAAc);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Size = new System.Drawing.Size(400, 75);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(62, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(201, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 4;
            this.label3.Text = "-";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(216, 9);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(97, 20);
            this.DteDocDt2.TabIndex = 5;
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(101, 9);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(97, 20);
            this.DteDocDt1.TabIndex = 3;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // LblCOAAc
            // 
            this.LblCOAAc.AutoSize = true;
            this.LblCOAAc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOAAc.ForeColor = System.Drawing.Color.Red;
            this.LblCOAAc.Location = new System.Drawing.Point(6, 34);
            this.LblCOAAc.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblCOAAc.Name = "LblCOAAc";
            this.LblCOAAc.Size = new System.Drawing.Size(89, 14);
            this.LblCOAAc.TabIndex = 6;
            this.LblCOAAc.Text = "COA\'s Account";
            this.LblCOAAc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkProfitCenterCode
            // 
            this.ChkProfitCenterCode.Location = new System.Drawing.Point(378, 75);
            this.ChkProfitCenterCode.Name = "ChkProfitCenterCode";
            this.ChkProfitCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProfitCenterCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProfitCenterCode.Properties.Caption = " ";
            this.ChkProfitCenterCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProfitCenterCode.Size = new System.Drawing.Size(18, 22);
            this.ChkProfitCenterCode.TabIndex = 27;
            this.ChkProfitCenterCode.ToolTip = "Remove filter";
            this.ChkProfitCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProfitCenterCode.ToolTipTitle = "Run System";
            this.ChkProfitCenterCode.CheckedChanged += new System.EventHandler(this.ChkProfitCenterCode_CheckedChanged);
            // 
            // LblMultiProfitCenterCode
            // 
            this.LblMultiProfitCenterCode.AutoSize = true;
            this.LblMultiProfitCenterCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMultiProfitCenterCode.ForeColor = System.Drawing.Color.Black;
            this.LblMultiProfitCenterCode.Location = new System.Drawing.Point(18, 78);
            this.LblMultiProfitCenterCode.Name = "LblMultiProfitCenterCode";
            this.LblMultiProfitCenterCode.Size = new System.Drawing.Size(77, 14);
            this.LblMultiProfitCenterCode.TabIndex = 25;
            this.LblMultiProfitCenterCode.Tag = "";
            this.LblMultiProfitCenterCode.Text = "Profit Center";
            // 
            // CcbAcNo
            // 
            this.CcbAcNo.Location = new System.Drawing.Point(101, 97);
            this.CcbAcNo.Name = "CcbAcNo";
            this.CcbAcNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbAcNo.Properties.DropDownRows = 30;
            this.CcbAcNo.Size = new System.Drawing.Size(271, 20);
            this.CcbAcNo.TabIndex = 8;
            this.CcbAcNo.EditValueChanged += new System.EventHandler(this.CcbAcNo_EditValueChanged);
            // 
            // ChkAcNo
            // 
            this.ChkAcNo.Location = new System.Drawing.Point(378, 96);
            this.ChkAcNo.Name = "ChkAcNo";
            this.ChkAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAcNo.Properties.Appearance.Options.UseFont = true;
            this.ChkAcNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAcNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAcNo.Properties.Caption = " ";
            this.ChkAcNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAcNo.Size = new System.Drawing.Size(20, 22);
            this.ChkAcNo.TabIndex = 9;
            this.ChkAcNo.ToolTip = "Remove filter";
            this.ChkAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAcNo.ToolTipTitle = "Run System";
            this.ChkAcNo.CheckedChanged += new System.EventHandler(this.ChkAcNo_CheckedChanged_1);
            // 
            // LueCOAAc
            // 
            this.LueCOAAc.EnterMoveNextControl = true;
            this.LueCOAAc.Location = new System.Drawing.Point(101, 31);
            this.LueCOAAc.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.LueCOAAc.Name = "LueCOAAc";
            this.LueCOAAc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCOAAc.Properties.Appearance.Options.UseFont = true;
            this.LueCOAAc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueCOAAc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCOAAc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueCOAAc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCOAAc.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCOAAc.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCOAAc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCOAAc.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCOAAc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueCOAAc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueCOAAc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCOAAc.Properties.DropDownRows = 30;
            this.LueCOAAc.Properties.NullText = "[Empty]";
            this.LueCOAAc.Properties.PopupWidth = 500;
            this.LueCOAAc.Size = new System.Drawing.Size(294, 20);
            this.LueCOAAc.TabIndex = 7;
            this.LueCOAAc.ToolTip = "F4 : Show/hide list";
            this.LueCOAAc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // CcbProfitCenterCode
            // 
            this.CcbProfitCenterCode.Location = new System.Drawing.Point(101, 75);
            this.CcbProfitCenterCode.Name = "CcbProfitCenterCode";
            this.CcbProfitCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbProfitCenterCode.Properties.DropDownRows = 30;
            this.CcbProfitCenterCode.Size = new System.Drawing.Size(271, 20);
            this.CcbProfitCenterCode.TabIndex = 26;
            this.CcbProfitCenterCode.EditValueChanged += new System.EventHandler(this.CcbProfitCenterCode_EditValueChanged);
            // 
            // TxtAcNo1
            // 
            this.TxtAcNo1.EnterMoveNextControl = true;
            this.TxtAcNo1.Location = new System.Drawing.Point(101, 53);
            this.TxtAcNo1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo1.Name = "TxtAcNo1";
            this.TxtAcNo1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo1.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo1.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo1.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo1.Properties.MaxLength = 50;
            this.TxtAcNo1.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo1.TabIndex = 10;
            this.TxtAcNo1.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo2
            // 
            this.TxtAcNo2.EnterMoveNextControl = true;
            this.TxtAcNo2.Location = new System.Drawing.Point(122, 53);
            this.TxtAcNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo2.Name = "TxtAcNo2";
            this.TxtAcNo2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo2.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo2.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo2.Properties.MaxLength = 50;
            this.TxtAcNo2.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo2.TabIndex = 11;
            this.TxtAcNo2.Validated += new System.EventHandler(this.TxtAcNo2_Validated);
            // 
            // TxtAcNo3
            // 
            this.TxtAcNo3.EnterMoveNextControl = true;
            this.TxtAcNo3.Location = new System.Drawing.Point(143, 53);
            this.TxtAcNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo3.Name = "TxtAcNo3";
            this.TxtAcNo3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo3.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo3.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo3.Properties.MaxLength = 50;
            this.TxtAcNo3.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo3.TabIndex = 12;
            this.TxtAcNo3.Validated += new System.EventHandler(this.TxtAcNo3_Validated);
            // 
            // TxtAcNo4
            // 
            this.TxtAcNo4.EnterMoveNextControl = true;
            this.TxtAcNo4.Location = new System.Drawing.Point(164, 53);
            this.TxtAcNo4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo4.Name = "TxtAcNo4";
            this.TxtAcNo4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo4.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo4.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo4.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo4.Properties.MaxLength = 50;
            this.TxtAcNo4.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo4.TabIndex = 13;
            this.TxtAcNo4.Validated += new System.EventHandler(this.TxtAcNo4_Validated);
            // 
            // TxtAcNo5
            // 
            this.TxtAcNo5.EnterMoveNextControl = true;
            this.TxtAcNo5.Location = new System.Drawing.Point(185, 53);
            this.TxtAcNo5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo5.Name = "TxtAcNo5";
            this.TxtAcNo5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo5.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo5.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo5.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo5.Properties.MaxLength = 50;
            this.TxtAcNo5.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo5.TabIndex = 14;
            this.TxtAcNo5.Validated += new System.EventHandler(this.TxtAcNo5_Validated);
            // 
            // TxtAcNo6
            // 
            this.TxtAcNo6.EnterMoveNextControl = true;
            this.TxtAcNo6.Location = new System.Drawing.Point(206, 53);
            this.TxtAcNo6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo6.Name = "TxtAcNo6";
            this.TxtAcNo6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo6.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo6.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo6.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo6.Properties.MaxLength = 50;
            this.TxtAcNo6.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo6.TabIndex = 15;
            this.TxtAcNo6.Validated += new System.EventHandler(this.TxtAcNo6_Validated);
            // 
            // TxtAcNo7
            // 
            this.TxtAcNo7.EnterMoveNextControl = true;
            this.TxtAcNo7.Location = new System.Drawing.Point(227, 53);
            this.TxtAcNo7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo7.Name = "TxtAcNo7";
            this.TxtAcNo7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo7.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo7.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo7.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo7.Properties.MaxLength = 50;
            this.TxtAcNo7.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo7.TabIndex = 16;
            this.TxtAcNo7.Validated += new System.EventHandler(this.TxtAcNo7_Validated);
            // 
            // TxtAcNo8
            // 
            this.TxtAcNo8.EnterMoveNextControl = true;
            this.TxtAcNo8.Location = new System.Drawing.Point(248, 53);
            this.TxtAcNo8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo8.Name = "TxtAcNo8";
            this.TxtAcNo8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo8.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo8.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo8.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo8.Properties.MaxLength = 50;
            this.TxtAcNo8.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo8.TabIndex = 17;
            this.TxtAcNo8.Validated += new System.EventHandler(this.TxtAcNo8_Validated);
            // 
            // TxtAcNo9
            // 
            this.TxtAcNo9.EnterMoveNextControl = true;
            this.TxtAcNo9.Location = new System.Drawing.Point(265, 53);
            this.TxtAcNo9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo9.Name = "TxtAcNo9";
            this.TxtAcNo9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo9.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo9.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo9.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo9.Properties.MaxLength = 50;
            this.TxtAcNo9.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo9.TabIndex = 18;
            this.TxtAcNo9.Validated += new System.EventHandler(this.TxtAcNo9_Validated);
            // 
            // TxtAcNo10
            // 
            this.TxtAcNo10.EnterMoveNextControl = true;
            this.TxtAcNo10.Location = new System.Drawing.Point(282, 53);
            this.TxtAcNo10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo10.Name = "TxtAcNo10";
            this.TxtAcNo10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo10.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo10.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo10.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo10.Properties.MaxLength = 50;
            this.TxtAcNo10.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo10.TabIndex = 19;
            this.TxtAcNo10.Validated += new System.EventHandler(this.TxtAcNo10_Validated);
            // 
            // TxtAcNo11
            // 
            this.TxtAcNo11.EnterMoveNextControl = true;
            this.TxtAcNo11.Location = new System.Drawing.Point(303, 53);
            this.TxtAcNo11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo11.Name = "TxtAcNo11";
            this.TxtAcNo11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo11.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo11.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo11.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo11.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo11.Properties.MaxLength = 50;
            this.TxtAcNo11.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo11.TabIndex = 20;
            this.TxtAcNo11.Validated += new System.EventHandler(this.TxtAcNo11_Validated);
            // 
            // TxtAcNo12
            // 
            this.TxtAcNo12.EnterMoveNextControl = true;
            this.TxtAcNo12.Location = new System.Drawing.Point(320, 53);
            this.TxtAcNo12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo12.Name = "TxtAcNo12";
            this.TxtAcNo12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo12.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo12.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo12.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo12.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo12.Properties.MaxLength = 50;
            this.TxtAcNo12.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo12.TabIndex = 21;
            this.TxtAcNo12.Validated += new System.EventHandler(this.TxtAcNo12_Validated);
            // 
            // TxtAcNo13
            // 
            this.TxtAcNo13.EnterMoveNextControl = true;
            this.TxtAcNo13.Location = new System.Drawing.Point(337, 53);
            this.TxtAcNo13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo13.Name = "TxtAcNo13";
            this.TxtAcNo13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo13.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo13.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo13.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo13.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo13.Properties.MaxLength = 50;
            this.TxtAcNo13.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo13.TabIndex = 22;
            this.TxtAcNo13.Validated += new System.EventHandler(this.TxtAcNo13_Validated);
            // 
            // TxtAcNo14
            // 
            this.TxtAcNo14.EnterMoveNextControl = true;
            this.TxtAcNo14.Location = new System.Drawing.Point(354, 53);
            this.TxtAcNo14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo14.Name = "TxtAcNo14";
            this.TxtAcNo14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAcNo14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo14.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo14.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo14.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo14.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo14.Properties.MaxLength = 50;
            this.TxtAcNo14.Size = new System.Drawing.Size(17, 20);
            this.TxtAcNo14.TabIndex = 23;
            this.TxtAcNo14.Validated += new System.EventHandler(this.TxtAcNo14_Validated);
            // 
            // ChkAcNo2
            // 
            this.ChkAcNo2.Location = new System.Drawing.Point(378, 53);
            this.ChkAcNo2.Name = "ChkAcNo2";
            this.ChkAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAcNo2.Properties.Appearance.Options.UseFont = true;
            this.ChkAcNo2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAcNo2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAcNo2.Properties.Caption = " ";
            this.ChkAcNo2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAcNo2.Size = new System.Drawing.Size(20, 22);
            this.ChkAcNo2.TabIndex = 24;
            this.ChkAcNo2.ToolTip = "Remove filter";
            this.ChkAcNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAcNo2.ToolTipTitle = "Run System";
            this.ChkAcNo2.CheckedChanged += new System.EventHandler(this.ChkAcNo2_CheckedChanged);
            // 
            // FrmPrintGL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 75);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = false;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmPrintGL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCOAAc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private System.Windows.Forms.Label LblCOAAc;
        private DevExpress.XtraEditors.CheckEdit ChkProfitCenterCode;
        private System.Windows.Forms.Label LblMultiProfitCenterCode;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbAcNo;
        private DevExpress.XtraEditors.CheckEdit ChkAcNo;
        private DevExpress.XtraEditors.LookUpEdit LueCOAAc;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbProfitCenterCode;
        private DevExpress.XtraEditors.CheckEdit ChkAcNo2;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo1;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo2;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo3;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo4;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo5;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo6;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo7;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo8;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo9;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo10;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo11;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo12;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo13;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo14;
    }
}