﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmRptItemSalesTargetDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptItemSalesTarget mFrmParent;
        private string mSQL = string.Empty;
        private string mItCode, mSpCode, mYr = string.Empty;
        private string mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptItemSalesTargetDlg(FrmRptItemSalesTarget FrmParent, string ItCode, string SpCode, string Yr)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mItCode = ItCode;
            mSpCode = SpCode;
            mYr = Yr;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetFormControl(mState.View);
            SetGrd(); 
            ShowData();
        }

        private void SetFormControl(RunSystem.mState state)
        {

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItCode, TxtItName, TxtSalesPerson
                    }, true);
                    TxtItCode.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtItCode, TxtItName, TxtSalesPerson
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "Document",

                        //1-5
                        "",
                        "Item Code", 
                        "Item Name",
                        "Quantity",
                        "Uom",
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5 });
        }

        private void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

        #endregion

        #region Show data
        public void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Select * from ( ");
                SQL.AppendLine("    Select A.DocNo,  Left(A.DocDt, 4) As Yr, I.SpCode, B.ItCode, G.ItName, ");
                SQL.AppendLine("    SUM(B.Qty) Qty, G.InventoryUomCode ");
                SQL.AppendLine("    from TblDOCt2Hdr A  ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl B On A.DocNo = B.DocNo  ");
                SQL.AppendLine("    Inner Join TblSalesInvoiceDtl C on B.DocNo = C.DOCtDocNo And B.Dno = C.DOCtDno  ");
                SQL.AppendLine("    Inner Join TblIncomingPaymentDtl D On C.DocNo = D.InvoiceDocNo  ");
                SQL.AppendLine("    Inner Join TblIncomingPaymentHdr E On D.DocNo = E.DocNo  ");
                SQL.AppendLine("    Inner Join TblVoucherRequestHdr F On E.VoucherrequestDocno = F.Docno And F.VoucherDocno Is null  ");
                SQL.AppendLine("    Inner Join TblItem G On B.ItCode = G.ItCode  ");
                SQL.AppendLine("    INner JOin TblItemCategory H On G.ItCtCode = H.ItCtCode "); 
                SQL.AppendLine("    Left Join (   ");
                SQL.AppendLine("        Select Distinct A.DocNo, ifnull(C.SpCode, D.SpCode) As SpCode from TblDrHdr A   ");
                SQL.AppendLine("        Inner Join TblDrDtl B On A.DocNo = b.DocNo  ");
                SQL.AppendLine("        Inner Join TblSOHdr  C On B.SODocNo = C.DocnO  ");
                SQL.AppendLine("        Inner Join TblCtQthdr D On C.CtQtDocNo = D.DocNO  ");
                SQL.AppendLine("    )I On A.DRDocNo = I.DocNo   ");
                SQL.AppendLine("    Left Join (  ");
                SQL.AppendLine("        Select Distinct A.DocNo, ifnull(C.SpCode, D.SpCode) As SpCode   ");
                SQL.AppendLine("        from TblPLHdr A   ");
                SQL.AppendLine("        Inner Join TblPLDtl B On A.DocNo = b.DocNo  ");
                SQL.AppendLine("        Inner Join TblSOHdr  C On B.SODocNo = C.DocnO  ");
                SQL.AppendLine("        Inner Join TblCtQthdr D On C.CtQtDocNo = D.DocNo  ");
                SQL.AppendLine("    )J On A.PLDocNo = J.DocNo  ");
                SQL.AppendLine("    Group BY A.DocNo,  B.ItCode, G.ItName, G.InventoryUomCode   ");
                SQL.AppendLine(")X ");
               
                mSQL = SQL.ToString();

                string Filter = "Where X.ItCode = '" + mItCode + "' And X.Yr = '" + mYr + "' And X.SpCode = '"+mSpCode+"' ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ",
                        new string[]
                        {
                            //0
                            "DocNo",
                            //1-5
                            "ItCode", "ItName", "Qty", "InventoryUomCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);

                        }, false, false, true, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                mMenuCode = mFrmParent.mMenuCode;
                e.DoDefault = false;
                var f = new FrmDOCt2(mMenuCode);
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }
        }


        private void Grd1_EllipsisButtonClick_1(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                mMenuCode = mFrmParent.mMenuCode;
                var f = new FrmDOCt2(mMenuCode);
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }
        }
        #endregion   

        #endregion
    }
}
