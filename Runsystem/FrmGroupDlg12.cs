﻿#region Update
/*
    18/05/2020 [WED/YK] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGroupDlg12 : RunSystem.FrmBase4
    {
        #region Field

        private FrmGroup mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmGroupDlg12(FrmGroup FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo, Concat(AcNo, ' - ', AcDesc) AcDesc, Alias, ");
            SQL.AppendLine("Case AcType When 'C' Then 'Credit' When 'D' Then 'Debit' End As AcType ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where ActInd = 'Y' ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "COA#",
                    "Account",
                    "Alias",
                    "Type"
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 180, 200, 150, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And AcNo Not In (" + mFrmParent.GetSelectedCOA() + ") ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "AcNo", "AcDesc", "Alias" });
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL() + Filter + " Order By AcNo;",
                    new string[] 
                    { 
                        "AcNo", "AcDesc", "Alias", "AcType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            //int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                string AcNo = string.Empty;
                //mFrmParent.Grd12.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsSiteCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        //Row1 = mFrmParent.Grd12.Rows.Count - 1;
                        //Row2 = Row;

                        if (AcNo.Length > 0) AcNo += ",";
                        AcNo += Sm.GetGrdStr(Grd1, Row, 2);

                        //Sm.CopyGrdValue(mFrmParent.Grd12, Row1, 1, Grd1, Row2, 2);
                        //Sm.CopyGrdValue(mFrmParent.Grd12, Row1, 2, Grd1, Row2, 3);

                        //mFrmParent.Grd12.Rows.Add();
                    }
                }
                //mFrmParent.Grd12.EndUpdate();
                if (AcNo.Length > 0)
                {
                    mFrmParent.ShowCOAInfo(AcNo);
                    mFrmParent.IsCOAChanged = true;
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 COA.");
        }

        private bool IsSiteCodeAlreadyChosen(int Row)
        {
            if (mFrmParent.Grd12.Rows.Count > 1)
            {
                for (int Index = 0; Index < mFrmParent.Grd12.Rows.Count - 1; Index++)
                    if (Sm.CompareStr(
                        Sm.GetGrdStr(mFrmParent.Grd12, Index, 1),
                        Sm.GetGrdStr(Grd1, Row, 2)
                        )) return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        #endregion

        #endregion

    }
}
