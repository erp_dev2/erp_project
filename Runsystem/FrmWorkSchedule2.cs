﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWorkSchedule2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mWSCode = string.Empty;
        internal FrmWorkSchedule2Find FrmFind;

        #endregion

        #region Constructor

        public FrmWorkSchedule2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Work Schedule (Weekly)";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);

                if (mWSCode.Length != 0)
                {
                    ShowData(mWSCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();

        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWSCode, TxtWSName, ChkActInd, MeeRemark,
                        ChkD1HolidayInd, TmeD1In1, TmeD1Out1, TmeD1In2, TmeD1Out2, 
                        ChkD2HolidayInd, TmeD2In1, TmeD2Out1, TmeD2In2, TmeD2Out2, 
                        ChkD3HolidayInd, TmeD3In1, TmeD3Out1, TmeD3In2, TmeD3Out2, 
                        ChkD4HolidayInd, TmeD4In1, TmeD4Out1, TmeD4In2, TmeD4Out2, 
                        ChkD5HolidayInd, TmeD5In1, TmeD5Out1, TmeD5In2, TmeD5Out2, 
                        ChkD6HolidayInd, TmeD6In1, TmeD6Out1, TmeD6In2, TmeD6Out2, 
                        ChkD7HolidayInd, TmeD7In1, TmeD7Out1, TmeD7In2, TmeD7Out2
                    }, true);
                    TxtWSCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWSCode, TxtWSName, ChkActInd, MeeRemark,
                        ChkD1HolidayInd, TmeD1In1, TmeD1Out1, TmeD1In2, TmeD1Out2, 
                        ChkD2HolidayInd, TmeD2In1, TmeD2Out1, TmeD2In2, TmeD2Out2, 
                        ChkD3HolidayInd, TmeD3In1, TmeD3Out1, TmeD3In2, TmeD3Out2, 
                        ChkD4HolidayInd, TmeD4In1, TmeD4Out1, TmeD4In2, TmeD4Out2, 
                        ChkD5HolidayInd, TmeD5In1, TmeD5Out1, TmeD5In2, TmeD5Out2, 
                        ChkD6HolidayInd, TmeD6In1, TmeD6Out1, TmeD6In2, TmeD6Out2, 
                        ChkD7HolidayInd, TmeD7In1, TmeD7Out1, TmeD7In2, TmeD7Out2
                    }, false);
                    TxtWSCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWSName, ChkActInd, MeeRemark,
                        ChkD1HolidayInd, TmeD1In1, TmeD1Out1, TmeD1In2, TmeD1Out2, 
                        ChkD2HolidayInd, TmeD2In1, TmeD2Out1, TmeD2In2, TmeD2Out2, 
                        ChkD3HolidayInd, TmeD3In1, TmeD3Out1, TmeD3In2, TmeD3Out2, 
                        ChkD4HolidayInd, TmeD4In1, TmeD4Out1, TmeD4In2, TmeD4Out2, 
                        ChkD5HolidayInd, TmeD5In1, TmeD5Out1, TmeD5In2, TmeD5Out2, 
                        ChkD6HolidayInd, TmeD6In1, TmeD6Out1, TmeD6In2, TmeD6Out2, 
                        ChkD7HolidayInd, TmeD7In1, TmeD7Out1, TmeD7In2, TmeD7Out2
                    }, false);
                    TxtWSName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtWSCode, TxtWSName, MeeRemark,
                TmeD1In1, TmeD1Out1, TmeD1In2, TmeD1Out2, 
                TmeD2In1, TmeD2Out1, TmeD2In2, TmeD2Out2, 
                TmeD3In1, TmeD3Out1, TmeD3In2, TmeD3Out2, 
                TmeD4In1, TmeD4Out1, TmeD4In2, TmeD4Out2, 
                TmeD5In1, TmeD5Out1, TmeD5In2, TmeD5Out2, 
                TmeD6In1, TmeD6Out1, TmeD6In2, TmeD6Out2, 
                TmeD7In1, TmeD7Out1, TmeD7In2, TmeD7Out2
            });
            ChkActInd.Checked = false;
            ChkD1HolidayInd.Checked = false;
            ChkD2HolidayInd.Checked = false;
            ChkD3HolidayInd.Checked = false;
            ChkD4HolidayInd.Checked = false; 
            ChkD5HolidayInd.Checked = false;
            ChkD6HolidayInd.Checked = false;
            ChkD7HolidayInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWorkSchedule2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWSCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWSCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblWorkSchedule2 Where WSCode=@WSCode" };
                Sm.CmParam<String>(ref cm, "@WSCode", TxtWSCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                bool IsInsert = !TxtWSCode.Properties.ReadOnly;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblWorkSchedule2 ");
                SQL.AppendLine("(WSCode, WSName, ActInd, ");
                SQL.AppendLine("D1HolidayInd, D1In1, D1Out1, D1In2, D1Out2, ");
                SQL.AppendLine("D2HolidayInd, D2In1, D2Out1, D2In2, D2Out2, ");
                SQL.AppendLine("D3HolidayInd, D3In1, D3Out1, D3In2, D3Out2, ");
                SQL.AppendLine("D4HolidayInd, D4In1, D4Out1, D4In2, D4Out2, ");
                SQL.AppendLine("D5HolidayInd, D5In1, D5Out1, D5In2, D5Out2, ");
                SQL.AppendLine("D6HolidayInd, D6In1, D6Out1, D6In2, D6Out2, ");
                SQL.AppendLine("D7HolidayInd, D7In1, D7Out1, D7In2, D7Out2, ");
                SQL.AppendLine("Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@WSCode, @WSName, @ActInd, ");
                SQL.AppendLine("@D1HolidayInd, @D1In1, @D1Out1, @D1In2, @D1Out2, ");
                SQL.AppendLine("@D2HolidayInd, @D2In1, @D2Out1, @D2In2, @D2Out2, ");
                SQL.AppendLine("@D3HolidayInd, @D3In1, @D3Out1, @D3In2, @D3Out2, ");
                SQL.AppendLine("@D4HolidayInd, @D4In1, @D4Out1, @D4In2, @D4Out2, ");
                SQL.AppendLine("@D5HolidayInd, @D5In1, @D5Out1, @D5In2, @D5Out2, ");
                SQL.AppendLine("@D6HolidayInd, @D6In1, @D6Out1, @D6In2, @D6Out2, ");
                SQL.AppendLine("@D7HolidayInd, @D7In1, @D7Out1, @D7In2, @D7Out2, ");
                SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update WSName=@WSName, ActInd=@ActInd, ");
                SQL.AppendLine("   D1HolidayInd=@D1HolidayInd, D1In1=@D1In1, D1Out1=@D1Out1, D1In2=@D1In2, D1Out2=@D1Out2, ");
                SQL.AppendLine("   D2HolidayInd=@D2HolidayInd, D2In1=@D2In1, D2Out1=@D2Out1, D2In2=@D2In2, D2Out2=@D2Out2, ");
                SQL.AppendLine("   D3HolidayInd=@D3HolidayInd, D3In1=@D3In1, D3Out1=@D3Out1, D3In2=@D3In2, D3Out2=@D3Out2, ");
                SQL.AppendLine("   D4HolidayInd=@D4HolidayInd, D4In1=@D4In1, D4Out1=@D4Out1, D4In2=@D4In2, D4Out2=@D4Out2, ");
                SQL.AppendLine("   D5HolidayInd=@D5HolidayInd, D5In1=@D5In1, D5Out1=@D5Out1, D5In2=@D5In2, D5Out2=@D5Out2, ");
                SQL.AppendLine("   D6HolidayInd=@D6HolidayInd, D6In1=@D6In1, D6Out1=@D6Out1, D6In2=@D6In2, D6Out2=@D6Out2, ");
                SQL.AppendLine("   D7HolidayInd=@D7HolidayInd, D7In1=@D7In1, D7Out1=@D7Out1, D7In2=@D7In2, D7Out2=@D7Out2, ");
                SQL.AppendLine("   Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@WSCode", TxtWSCode.Text);
                Sm.CmParam<String>(ref cm, "@WSName", TxtWSName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");

                Sm.CmParam<String>(ref cm, "@D1HolidayInd", ChkD1HolidayInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@D1In1", Sm.GetTme(TmeD1In1));
                Sm.CmParam<String>(ref cm, "@D1Out1", Sm.GetTme(TmeD1Out1));
                Sm.CmParam<String>(ref cm, "@D1In2", Sm.GetTme(TmeD1In2));
                Sm.CmParam<String>(ref cm, "@D1Out2", Sm.GetTme(TmeD1Out2));

                Sm.CmParam<String>(ref cm, "@D2HolidayInd", ChkD2HolidayInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@D2In1", Sm.GetTme(TmeD2In1));
                Sm.CmParam<String>(ref cm, "@D2Out1", Sm.GetTme(TmeD2Out1));
                Sm.CmParam<String>(ref cm, "@D2In2", Sm.GetTme(TmeD2In2));
                Sm.CmParam<String>(ref cm, "@D2Out2", Sm.GetTme(TmeD2Out2));

                Sm.CmParam<String>(ref cm, "@D3HolidayInd", ChkD3HolidayInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@D3In1", Sm.GetTme(TmeD3In1));
                Sm.CmParam<String>(ref cm, "@D3Out1", Sm.GetTme(TmeD3Out1));
                Sm.CmParam<String>(ref cm, "@D3In2", Sm.GetTme(TmeD3In2));
                Sm.CmParam<String>(ref cm, "@D3Out2", Sm.GetTme(TmeD3Out2));

                Sm.CmParam<String>(ref cm, "@D4HolidayInd", ChkD4HolidayInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@D4In1", Sm.GetTme(TmeD4In1));
                Sm.CmParam<String>(ref cm, "@D4Out1", Sm.GetTme(TmeD4Out1));
                Sm.CmParam<String>(ref cm, "@D4In2", Sm.GetTme(TmeD4In2));
                Sm.CmParam<String>(ref cm, "@D4Out2", Sm.GetTme(TmeD4Out2));

                Sm.CmParam<String>(ref cm, "@D5HolidayInd", ChkD5HolidayInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@D5In1", Sm.GetTme(TmeD5In1));
                Sm.CmParam<String>(ref cm, "@D5Out1", Sm.GetTme(TmeD5Out1));
                Sm.CmParam<String>(ref cm, "@D5In2", Sm.GetTme(TmeD5In2));
                Sm.CmParam<String>(ref cm, "@D5Out2", Sm.GetTme(TmeD5Out2));

                Sm.CmParam<String>(ref cm, "@D6HolidayInd", ChkD6HolidayInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@D6In1", Sm.GetTme(TmeD6In1));
                Sm.CmParam<String>(ref cm, "@D6Out1", Sm.GetTme(TmeD6Out1));
                Sm.CmParam<String>(ref cm, "@D6In2", Sm.GetTme(TmeD6In2));
                Sm.CmParam<String>(ref cm, "@D6Out2", Sm.GetTme(TmeD6Out2));

                Sm.CmParam<String>(ref cm, "@D7HolidayInd", ChkD7HolidayInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@D7In1", Sm.GetTme(TmeD7In1));
                Sm.CmParam<String>(ref cm, "@D7Out1", Sm.GetTme(TmeD7Out1));
                Sm.CmParam<String>(ref cm, "@D7In2", Sm.GetTme(TmeD7In2));
                Sm.CmParam<String>(ref cm, "@D7Out2", Sm.GetTme(TmeD7Out2));

                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                if (IsInsert)
                    BtnInsertClick(sender, e);
                else
                    ShowData(TxtWSCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string WSCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@WSCode", WSCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select *  From TblWorkSchedule2 Where WSCode=@WSCode;",
                        new string[] 
                        {
                            "WSCode", "WSName", "ActInd", "Remark",
                            
                            "D1HolidayInd", "D1In1", "D1Out1", "D1In2", "D1Out2",
 
                            "D2HolidayInd", "D2In1", "D2Out1", "D2In2", "D2Out2",

                            "D3HolidayInd", "D3In1", "D3Out1", "D3In2", "D3Out2",
                            
                            "D4HolidayInd", "D4In1", "D4Out1", "D4In2", "D4Out2",

                            "D5HolidayInd", "D5In1", "D5Out1", "D5In2", "D5Out2",

                            "D6HolidayInd", "D6In1", "D6Out1", "D6In2", "D6Out2",

                            "D7HolidayInd", "D7In1", "D7Out1", "D7In2", "D7Out2"
                            
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtWSCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtWSName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                            MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                            
                            ChkD1HolidayInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                            Sm.SetTme(TmeD1In1, Sm.DrStr(dr, c[5]));
                            Sm.SetTme(TmeD1Out1, Sm.DrStr(dr, c[6]));
                            Sm.SetTme(TmeD1In2, Sm.DrStr(dr, c[7]));
                            Sm.SetTme(TmeD1Out2, Sm.DrStr(dr, c[8]));

                            ChkD2HolidayInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[9]), "Y");
                            Sm.SetTme(TmeD2In1, Sm.DrStr(dr, c[10]));
                            Sm.SetTme(TmeD2Out1, Sm.DrStr(dr, c[11]));
                            Sm.SetTme(TmeD2In2, Sm.DrStr(dr, c[12]));
                            Sm.SetTme(TmeD2Out2, Sm.DrStr(dr, c[13]));

                            ChkD3HolidayInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[14]), "Y");
                            Sm.SetTme(TmeD3In1, Sm.DrStr(dr, c[15]));
                            Sm.SetTme(TmeD3Out1, Sm.DrStr(dr, c[16]));
                            Sm.SetTme(TmeD3In2, Sm.DrStr(dr, c[17]));
                            Sm.SetTme(TmeD3Out2, Sm.DrStr(dr, c[18]));

                            ChkD4HolidayInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[19]), "Y");
                            Sm.SetTme(TmeD4In1, Sm.DrStr(dr, c[20]));
                            Sm.SetTme(TmeD4Out1, Sm.DrStr(dr, c[21]));
                            Sm.SetTme(TmeD4In2, Sm.DrStr(dr, c[22]));
                            Sm.SetTme(TmeD4Out2, Sm.DrStr(dr, c[23]));

                            ChkD5HolidayInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[24]), "Y");
                            Sm.SetTme(TmeD5In1, Sm.DrStr(dr, c[25]));
                            Sm.SetTme(TmeD5Out1, Sm.DrStr(dr, c[26]));
                            Sm.SetTme(TmeD5In2, Sm.DrStr(dr, c[27]));
                            Sm.SetTme(TmeD5Out2, Sm.DrStr(dr, c[28]));

                            ChkD6HolidayInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[29]), "Y");
                            Sm.SetTme(TmeD6In1, Sm.DrStr(dr, c[30]));
                            Sm.SetTme(TmeD6Out1, Sm.DrStr(dr, c[31]));
                            Sm.SetTme(TmeD6In2, Sm.DrStr(dr, c[32]));
                            Sm.SetTme(TmeD6Out2, Sm.DrStr(dr, c[33]));

                            ChkD7HolidayInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[34]), "Y");
                            Sm.SetTme(TmeD7In1, Sm.DrStr(dr, c[35]));
                            Sm.SetTme(TmeD7Out1, Sm.DrStr(dr, c[36]));
                            Sm.SetTme(TmeD7In2, Sm.DrStr(dr, c[37]));
                            Sm.SetTme(TmeD7Out2, Sm.DrStr(dr, c[38]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWSCode, "Work schedule code", false) ||
                Sm.IsTxtEmpty(TxtWSName, "Work schedule name", false) ||

                (!ChkD1HolidayInd.Checked && Sm.IsTmeEmpty(TmeD1In1, "Day 1-Check in")) ||
                (!ChkD1HolidayInd.Checked && Sm.IsTmeEmpty(TmeD1Out1, "Day 1-Check out")) ||
                (!ChkD1HolidayInd.Checked && Sm.IsTmeEmpty(TmeD1In2, "Day 1-Break in")) ||
                (!ChkD1HolidayInd.Checked && Sm.IsTmeEmpty(TmeD1Out2, "Day 1-Break out")) ||

                (!ChkD2HolidayInd.Checked && Sm.IsTmeEmpty(TmeD2In1, "Day 2-Check in")) ||
                (!ChkD2HolidayInd.Checked && Sm.IsTmeEmpty(TmeD2Out1, "Day 2-Check out")) ||
                (!ChkD2HolidayInd.Checked && Sm.IsTmeEmpty(TmeD2In2, "Day 2-Break in")) ||
                (!ChkD2HolidayInd.Checked && Sm.IsTmeEmpty(TmeD2Out2, "Day 2-Break out")) ||

                (!ChkD3HolidayInd.Checked && Sm.IsTmeEmpty(TmeD3In1, "Day 3-Check in")) ||
                (!ChkD3HolidayInd.Checked && Sm.IsTmeEmpty(TmeD3Out1, "Day 3-Check out")) ||
                (!ChkD3HolidayInd.Checked && Sm.IsTmeEmpty(TmeD3In2, "Day 3-Break in")) ||
                (!ChkD3HolidayInd.Checked && Sm.IsTmeEmpty(TmeD3Out2, "Day 3-Break out")) ||

                (!ChkD4HolidayInd.Checked && Sm.IsTmeEmpty(TmeD4In1, "Day 4-Check in")) ||
                (!ChkD4HolidayInd.Checked && Sm.IsTmeEmpty(TmeD4Out1, "Day 4-Check out")) ||
                (!ChkD4HolidayInd.Checked && Sm.IsTmeEmpty(TmeD4In2, "Day 4-Break in")) ||
                (!ChkD4HolidayInd.Checked && Sm.IsTmeEmpty(TmeD4Out2, "Day 4-Break out")) ||

                (!ChkD5HolidayInd.Checked && Sm.IsTmeEmpty(TmeD5In1, "Day 5-Check in")) ||
                (!ChkD5HolidayInd.Checked && Sm.IsTmeEmpty(TmeD5Out1, "Day 5-Check out")) ||
                (!ChkD5HolidayInd.Checked && Sm.IsTmeEmpty(TmeD5In2, "Day 5-Break in")) ||
                (!ChkD5HolidayInd.Checked && Sm.IsTmeEmpty(TmeD5Out2, "Day 5-Break out")) ||

                (!ChkD6HolidayInd.Checked && Sm.IsTmeEmpty(TmeD6In1, "Day 6-Check in")) ||
                (!ChkD6HolidayInd.Checked && Sm.IsTmeEmpty(TmeD6Out1, "Day 6-Check out")) ||
                (!ChkD6HolidayInd.Checked && Sm.IsTmeEmpty(TmeD6In2, "Day 6-Break in")) ||
                (!ChkD6HolidayInd.Checked && Sm.IsTmeEmpty(TmeD6Out2, "Day 6-Break out")) ||

                (!ChkD7HolidayInd.Checked && Sm.IsTmeEmpty(TmeD7In1, "Day 7-Check in")) ||
                (!ChkD7HolidayInd.Checked && Sm.IsTmeEmpty(TmeD7Out1, "Day 7-Check out")) ||
                (!ChkD7HolidayInd.Checked && Sm.IsTmeEmpty(TmeD7In2, "Day 7-Break in")) ||
                (!ChkD7HolidayInd.Checked && Sm.IsTmeEmpty(TmeD7Out2, "Day 7-Break out")) ||

                IsWSCodeExisted();
        }

        private bool IsWSCodeExisted()
        {
            if (!TxtWSCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select WSCode From TblWorkSchedule2 Where WSCode=@WSCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@WSCode", TxtWSCode.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Work schedule code ( " + TxtWSCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        private void TxtWSCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWSCode);
        }

        private void TxtWSName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWSName);
        }

        #endregion
    }
}
