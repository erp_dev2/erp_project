﻿#region Update
/*
    09/08/2018 [TKG] tambah status do
    30/09/2018 [TKG] menghilangkan angka di belakang koma
    12/08/2019 [TKG] tambah informasi packaging unit
    04/11/2019 [DITA/IMS] tambah informasi Specifications
    07/01/2020 [DITA/SIER] Tambah parameter IsFilterByItCt
    23/09/2020 [DITA/SIER] Tambah kolom remark detail + auto height nya
    18/01/2021 [DITA/IMS] tambah kolom specification berdasarkan param : IsBOMShowSpecifications
    23/02/2021 [IBL/SIER] tambah informasi Customer Category berdasarkan parameter IsCustomerComboBasedOnCategory
    10/11/2021 [YOG/AMKA] Membuat field Customer, warehouse, dan item di menu DO to Customer terfirlter berdasarkan grup
    19/01/2022 [TRI/SIER] BUG, data belum terfilter berdasar item category
    29/03/2022 [ISD/SIER] kolom customer category terfilter berdasarkan group dengan parameter IsFilterByCtCt
    10/08/2022 [MAU/SIER] Penambahan kolom Process dan Total Amount dan Foreign Name serta default hide untuk Local Code, Spesification, Property, Source, Lot, Bin, Quantity2, UoM2, Quantity3, UoM3 saat FIND pada menu DO to Customer
    11/08/2022 [MAU/SIER] Penambahan field filter Customer Category berdasarkan group setting di Header saat FIND pada menu DO to Customer
    24/08/2022 [MAU/SIER] Feedback , Penambahan field filter Customer Category berdasarkan group setting di Header saat FIND pada menu DO to Customer
    26/08/2022 [IBL/PHT] Kolom ProcessInd dimunculkan berdasarkan param IsDOCtUseProcessInd
    07/09/2022 [ICA/SIER] ketika param isbomshowspecifications = Y, kolom Specification defaultnya di hide akan ke unhide kalo di uncheck
    16/09/2022 [MAU/SIER] Feedback penambahan kolom Total Amount dengan parameter IsDoToCustomerShowTotalAmount
    05/10/2022 [VIN/SIER] BUG: Total Amount belum decimal
    02/11/2022 [VIN/SIER] BUG Query: kolom Total Amount tidak bisa pakai format(x, 2)-> hasilnya string -> rounding nya saat show data
    04/05/2023 [BRI/MNET] Bug ChkExcludedCancelledItem
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;
using System.Net;

#endregion

namespace RunSystem
{
    public partial class FrmDOCtFind : RunSystem.FrmBase2
    {
        #region Field
        private iGRichTextManager fManager;
        private FrmDOCt mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsInventoryShowTotalQty = false;
        internal bool
            mIsFilterByItCt = false,
            //mIsDOCtAmtRounded = false,
            mIsDoToCustomerShowTotalAmount = false;
        private string
            mDoToCustomerFindFormat = string.Empty,
            mLueCtCtCode = string.Empty;
            

        #endregion

        #region Constructor

        public FrmDOCtFind(FrmDOCt FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueCtCode(ref LueCtCode, string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                base.FrmLoad(sender, e);

                ChkExcludedCancelledItem.Checked = true;

                if (mFrmParent.mIsShowCustomerCategory)
                {
                    if (mFrmParent.mIsCustomerComboBasedOnCategory)
                    {
                        Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");
                        //if (Sm.GetLue(LueCtCtCode).Length > 0)
                        //{
                        //    Sl.SetLueCtCodeBasedOnCategory(ref LueCtCode, string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N", Sm.GetLue(LueCtCtCode));
                            

                        //}
                        //else
                        //{
                            
                        //    Sl.SetLueCtCode(ref LueCtCode, string.Empty, string.Empty);
                        //}
                    }
                    else
                    {
                        Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, string.Empty);
                    }
                }
                else
                {
                    LblCtCtCode.Visible = false;
                    LueCtCtCode.Visible = false;
                    ChkCtCtCode.Visible = false;
                    Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, string.Empty);
                    Sl.SetLueCtCode(ref LueCtCtCode, string.Empty, string.Empty);
                }


            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("B.CancelInd, A.DocNoInternal, C.WhsName, E.CtName, ");
            SQL.AppendLine("B.ItCode, D.ItCodeInternal, D.ItName, D.ForeignName, D.ItGrpCode, ");
            SQL.AppendLine("F.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.QtyPackagingUnit, B.PackagingUnitUomCode, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, ");
            SQL.AppendLine("A.CurCode, B.UPrice, A.Remark, B.Remark RemarkDtl, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            SQL.AppendLine("A.KBRegistrationNo, A.KBRegistrationDt, A.KBContractNo, A.KBContractDt, A.KBPLNo, A.KBPLDt, D.Specification, G.CtCtName,  ");

            SQL.AppendLine("case when A.ProcessInd = 'F' then 'Final' when A.ProcessInd = 'D' then 'Draft' END AS ProcessInd, IFNULL(H.TotalAmt, 0.00) AS TotalAmt ");

            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblCustomer E On A.CtCode=E.CtCode ");
            SQL.AppendLine("Left Join TblProperty F On B.PropCode=F.PropCode ");
            SQL.AppendLine("Left Join TblCustomerCategory G On E.CtCtCode = G.CtCtCode ");

            // Total Amount

            
            
            SQL.AppendLine(" LEFT JOIN (");
            SQL.AppendLine("    SELECT A.DocNo, SUM(B.QTY * B.UPRICE) AS TotalAmt ");
            SQL.AppendLine("    From TblDOCtHdr A ");
            SQL.AppendLine("    Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    GROUP BY A.DocNO ");
            SQL.AppendLine(" ) H ON A.DocNo = H.DocNo ");
                
            
            SQL.AppendLine(" Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterByCtCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("    Where CtCtCode=E.CtCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode)) ");
            }
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 47;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Status",
                        "Cancel", 
                        "Local"+Environment.NewLine+"Document#",

                        //6-10
                        "Warehouse",
                        "Customer",
                        "Item's Code",
                        "Local Code",
                        "Item's Name",

                        //11-15
                        "Foreign Name",
                        "Group",
                        "Property",
                        "Batch#",
                        "Source",

                        //16-20
                        "Lot",
                        "Bin",
                        "Quantity"+Environment.NewLine+"(Packaging)",
                        "UoM"+Environment.NewLine+"(Packaging)",
                        "Quantity",

                        //21-25
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        
                        //26-30
                        "Currency",
                        "Unit Price",
                        "Total",
                        "Registration#",
                        "Registration Date",

                        //31-35
                        "Contract#",
                        "Contract Date",
                        "Packing List#",
                        "Packing List Date",
                        "Remark",

                        //36-40
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 

                        //41-45
                        "Last"+Environment.NewLine+"Updated Time",
                        "Specification",
                        "Remark Detail",
                        "Customer Category",
                        "Process",

                        //46
                        "Total Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 80, 50, 130, 
                        
                        //6-10
                        200, 200, 100, 100, 200, 
                        
                        //11-15
                        150, 150, 100, 120, 150, 
                        
                        //16-20
                        200, 200, 200, 200, 100, 
                        
                        //21-25
                        100, 100, 100, 100, 100, 
                        
                        //26-30
                        80, 120, 130, 120, 120, 
                        
                        //31-35
                        120, 120, 120, 120, 300, 
                        
                        //36-40
                        120, 120, 120, 120, 120, 
                        
                        //41-45
                        120, 300, 300, 180, 80,

                        //46
                        130
                    }
                );

            fManager = new iGRichTextManager(Grd1);
            //Grd1.Cols[43].DefaultCellValue = @"{\rtf1\ansi\ansicpg1251\deff0\deflang1049{\fonttbl{\f0\fswiss\fcharset0 Lucida Console;}}{\colortbl ;\red255\green0\blue0;}{\*\generator Msftedit 5.41.15.1507;}\viewkind4\uc1\pard\lang1033\f0\fs20 This \i is\i0  a \b rich \cf1\b0 text \cf0\ul format\ulnone .\lang1049\f1\par}";
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 20, 22, 24, 27, 28, 46 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 30, 32, 34, 37, 40 });
            Sm.GrdFormatTime(Grd1, new int[] { 38, 41 });
            Sm.GrdColInvisible(Grd1, new int[] { 36, 37, 38, 39, 40, 41, 42 }, false);
            if (!mFrmParent.mIsDOCtApprovalActived) Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            if (!mFrmParent.mIsShowLocalDocNo) Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            if (!mFrmParent.mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 11 }, false);
            if (!mFrmParent.mIsItGrpCodeShow)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 12 }, false);
            }
            if (!mFrmParent.mIsKawasanBerikatEnabled) Sm.GrdColInvisible(Grd1, new int[] { 29, 30, 31, 32, 33, 34 }, false);
            if (!mFrmParent.mIsDOCtShowPackagingUnit) Sm.GrdColInvisible(Grd1, new int[] { 18, 19 }, false);
            //if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 42 },false);
            if (!mFrmParent.mIsDOCtShowRemarkDetail) Sm.GrdColInvisible(Grd1, new int[] { 43 });
            if (!mFrmParent.mIsCustomerComboBasedOnCategory) Sm.GrdColInvisible(Grd1, new int[] { 44 });
            if (!mFrmParent.mIsDOCtUseProcessInd) Sm.GrdColInvisible(Grd1, new int[] { 45 });
            Grd1.Cols[42].Move(12);
            Grd1.Cols[43].Move(37);
            Grd1.Cols[44].Move(7);
            Grd1.Cols[4].CellStyle.ImageAlign = iGContentAlignment.TopCenter;
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[45].Move(3);
            Grd1.Cols[46].Move(33); //
            if (!mIsDoToCustomerShowTotalAmount) Sm.GrdColInvisible(Grd1, new int[] { 46 }, false);
            if (mDoToCustomerFindFormat == "2") Sm.GrdColInvisible(Grd1, new int[] { 9, 13, 15, 16, 17 }, false);
            if (mFrmParent.mNumberOfInventoryUomCode == 1) Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25 }, false);
            if (mFrmParent.mNumberOfInventoryUomCode == 2) Sm.GrdColInvisible(Grd1, new int[] { 22, 23 }, false);
            //if (mFrmParent.mNumberOfInventoryUomCode == 3) Sm.GrdColInvisible(Grd1, new int[] {  }, false);
            
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  36, 37, 38, 39, 40, 41}, !ChkHideInfoInGrd.Checked);
            if (mDoToCustomerFindFormat == "2") Sm.GrdColInvisible(Grd1, new int[] { 9, 13, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
            if (mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 42 }, !ChkHideInfoInGrd.Checked);
            if (mFrmParent.mNumberOfInventoryUomCode == 1) Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25 }, !ChkHideInfoInGrd.Checked);
            if (mFrmParent.mNumberOfInventoryUomCode == 2) Sm.GrdColInvisible(Grd1, new int[] { 22, 23 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 22, 23 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25 }, true);
        }

        

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                
                
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And (B.CancelInd='N' Or A.Status<>'C') ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.DocNoInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "D.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCtCode), "G.CtCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItCodeInternal", "D.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "B.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "B.Bin", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "DocNoInternal", "WhsName", 
                            
                            //6-10
                            "CtName", "ItCode", "ItCodeInternal", "ItName", "ForeignName", 
                            
                            //11-15
                            "ItGrpCode", "PropName", "BatchNo", "Source", "Lot", 
                            
                            //16-20
                            "Bin", "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", "InventoryUomCode", 

                            //21-25
                            "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CurCode", 

                            //26-30
                            "UPrice", "KBRegistrationNo", "KBRegistrationDt", "KBContractNo", "KBContractDt", 
                            
                            //31-35
                            "KBPLNo", "KBPLDt", "Remark", "CreateBy", "CreateDt", 
                            
                            //36-40
                            "LastUpBy", "LastUpDt", "Specification", "RemarkDtl", "CtCtName",

                            //41-42
                            "ProcessInd", "TotalAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 26);
                            if (mFrmParent.mIsDOCtAmtRounded)
                                Grd.Cells[Row, 28].Value = decimal.Truncate(Sm.GetGrdDec(Grd1, Row, 20) * Sm.GetGrdDec(Grd1, Row, 27));
                            else
                                Grd.Cells[Row, 28].Value = Sm.GetGrdDec(Grd1, Row, 20) * Sm.GetGrdDec(Grd1, Row, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 30, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 29);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 34, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 33);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 34);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 37, 35);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 38, 35);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 36);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 40, 37);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 41, 37);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 38);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 39);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 44, 40);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 45, 41);
                            if (mFrmParent.mIsDOCtAmtRounded)
                                Grd.Cells[Row, 46].Value = decimal.Truncate(dr.GetDecimal(c[42]));
                            else
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 46, 42);

                            //Sm.GrdFormatDec


                        }, true, false, false, false
                    );
                if (mFrmParent.mIsDOCtShowRemarkDetail) Grd1.Rows.AutoHeight();
                if (mIsInventoryShowTotalQty)
                {
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 18, 20, 22, 24, 27, 28, 46 });
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void GetParameter()
        {
            mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine(" Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In ( ");

            SQL.AppendLine(" 'DoToCustomerFindFormat','IsDOCtAmtRounded', 'IsDoToCustomerShowTotalAmount' ");

            SQL.AppendLine(" );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean



                            //string
                            case "DoToCustomerFindFormat" : mDoToCustomerFindFormat = ParValue; break;
                            //case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            case "IsDoToCustomerShowTotalAmount": mIsDoToCustomerShowTotalAmount = ParValue == "Y"; break;

                        }
                    }
                }
                dr.Close();
            }
            
        }
        //private void ComputeAmt()
        //{
        //    decimal Amt = 0m;
        //    for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
        //        if (Sm.GetGrdStr(Grd1, row, 24).Length != 0 && !Sm.GetGrdBool(Grd1, row, 1))
        //            Amt += Sm.GetGrdDec(Grd1, row, 24);
        //    if (mIsDOCtAmtRounded)
        //        //TxtAmt.EditValue = Sm.FormatNum(Math.Ceiling(Amt),0); 
        //        mFrmParent.TxtAmt.EditValue = Sm.FormatNum(decimal.Truncate(Amt), 0);
        //    else
        //        mFrmParent.TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        //}


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeFilterByItCt), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }
        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue3(Sl.SetLueCtCtCode), string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
            //Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");

            if (Sm.GetLue(LueCtCtCode).Length > 0)
                {
                    Sl.SetLueCtCodeBasedOnCategory(ref LueCtCode, string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N", Sm.GetLue(LueCtCtCode));
                }
                else
                {
                    Sl.SetLueCtCode(ref LueCtCode, string.Empty, string.Empty);
                }
            

            
        }

        private void ChkCtCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Asset's category");
        }

        #endregion

        #endregion
    }
}
