﻿#region Update
/*
    17/07/2018 [TKG] tambah cost center saat journal
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    12/03/2019 [TKG] Bug saat proses journal cancel
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvDept : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmRecvDeptFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string mDocType = "06";
        private bool 
            mIsSystemUseCostCenter = false,
            mIsJournalCostCenterEnabled = false,
            mIsAutoJournalActived = false;

        #endregion

        #region Constructor

        public FrmRecvDept(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueDeptCode(ref LueDeptCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "DO#",
                        "DO"+Environment.NewLine+"D Number",

                        //6-10
                        "",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Batch#",
                        
                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Available DO"+Environment.NewLine+"Quantity",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Available DO"+Environment.NewLine+"Quantity",
                        "Quantity",
                        "UoM",
                        "Available DO"+Environment.NewLine+"Quantity",
                        
                        //21-25
                        "Quantity",
                        "UoM",
                        "DO's Remark",
                        "Remark",
                        "Stock",

                        //26-29
                        "Stock",
                        "Stock",
                        "Cost Center Code",
                        "Cost Center"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 20, 150, 0, 
                        
                        //6-10
                        20, 80, 20, 250, 200, 
                        
                        //11-15
                        130, 100, 80, 100, 80,
                        
                        //16-20
                        80, 100, 80, 80, 100, 
                        
                        //21-25
                        80, 80, 300, 300, 0,

                        //26-29
                        0, 0, 0, 200
                    }
                );

            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 6, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 6, 7, 8, 11, 12, 13, 17, 18, 19, 20, 21, 22, 25, 26, 27, 28 }, false);
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueWhsCode, LueDeptCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29  });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueDeptCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 15, 18, 21, 24 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueDeptCode, MeeRemark
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21, 25, 26, 27 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvDeptFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode' ");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "RecvDeptHdr", "RecvDeptDtl" };

            var l = new List<RecvDeptHdr>();
            var ldtl = new List<RecvDeptDtl>();

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
            SQL.AppendLine("A.DocNo,Date_Format(A.DocDt,'%d %M %Y') As DocDt,B.WhsName,C.DeptName,A.Remark ");
            SQL.AppendLine("From TblRecvDeptHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "Company",
                         //1-5
                         "Address",
                         "Phone",
                         "DocNo",
                         "DocDt",
                         "WhsName",
                         //6-8
                         "DeptName",
                         "Remark",
                         "CompanyLogo",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RecvDeptHdr()
                        {
                            Company = Sm.DrStr(dr, c[0]),
                            Address = Sm.DrStr(dr, c[1]),
                            Phone = Sm.DrStr(dr, c[2]),
                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            WhsName = Sm.DrStr(dr, c[5]),
                            DeptName = Sm.DrStr(dr, c[6]),
                            Remark = Sm.DrStr(dr, c[7]),
                            CompanyLogo = Sm.DrStr(dr, c[8]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                        });
                    }
                }

                dr.Close();
            }
            myLists.Add(l);

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;


                SQLDtl.AppendLine("Select A.DODeptDocNo,Date_Format(D.DocDt,'%d %M %Y') As DocDt,B.ItCode,C.ItName,B.BatchNo,B.Lot,B.Bin,A.Qty,A.Qty2,A.Qty3,C.InventoryUOMCode,C.InventoryUOMCode2,C.InventoryUOMCode3,A.Remark ");
                SQLDtl.AppendLine("From TblRecvDeptDtl  A ");
                SQLDtl.AppendLine("Inner Join TblDODeptDtl B On Concat(A.DODeptDocNo,A.DODeptDNo)=Concat(B.DocNo,B.DNo) ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode= C.ItCode ");
                SQLDtl.AppendLine("Inner Join TblDODeptHdr D On A.DODeptDocNo= D.DocNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DODeptDocNo",
                         //1-5
                         "ItCode",
                         "ItName",
                         "BatchNo",
                         "Lot",
                         "Bin",
                         //6-10
                         "Qty",
                         "Qty2",
                         "Qty3",
                         "InventoryUOMCode",
                         "InventoryUOMCode2",
                         //11-13
                         "InventoryUOMCode3",
                         "DocDt",
                         "Remark"



                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new RecvDeptDtl()
                        {
                            DODeptDocNo = Sm.DrStr(drDtl, cDtl[0]),
                            ItCode = Sm.DrStr(drDtl, cDtl[1]),
                            ItName = Sm.DrStr(drDtl, cDtl[2]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),
                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[10]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            DocDt = Sm.DrStr(drDtl, cDtl[12]),
                            Remark = Sm.DrStr(drDtl, cDtl[13])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            int a = int.Parse(ParValue);
            if (a == 1)
            {
                Sm.PrintReport("RecvDept1", myLists, TableName, false);
            }
            else if (a == 2)
            {
                Sm.PrintReport("RecvDept2", myLists, TableName, false);
            }
            else
            {
                Sm.PrintReport("RecvDept3", myLists, TableName, false);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvDeptDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueDeptCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 15, 18, 21, 24 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDODeptWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }


            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && !Sm.IsLueEmpty(LueDeptCode, "Department") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmRecvDeptDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueDeptCode)));

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmDODeptWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }


            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 18, 21 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 24 }, e);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string 
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvDept", "TblRecvDeptHdr"),
                JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);
                
            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvDeptHdr(DocNo, JournalDocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveRecvDeptDtl(DocNo, Row));

            cml.Add(SaveStockMovement(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 11).Length > 0)
                    cml.Add(SaveStockSummary(Row));

            if (mIsSystemUseCostCenter) cml.Add(SaveJournal(DocNo, JournalDocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            //ReComputeRemainingQty();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                (mIsJournalCostCenterEnabled && IsCCCodeInvalid()) ;
        }

        private bool IsCCCodeInvalid()
        {
            string CCCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                CCCode = Sm.GetGrdStr(Grd1, 0, 28);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 4).Length>0 && 
                        !Sm.CompareStr(CCCode, Sm.GetGrdStr(Grd1, r, 28)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO# : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                            "Cost center : " + Sm.GetGrdStr(Grd1, r, 29) + Environment.NewLine + Environment.NewLine +
                            "Invalid cost center." + Environment.NewLine +
                            "All DO should have the same cost center.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "DO to department's document number is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 11, false, "Source is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 15, true, "Quantity (1) is empty.") ||
                    (Sm.GetGrdStr(Grd1, Row, 19).Length > 0 && mNumberOfInventoryUomCode>=2 && Sm.IsGrdValueEmpty(Grd1, Row, 18, true, "Quantity (2) is empty.")) ||
                    (Sm.GetGrdStr(Grd1, Row, 22).Length > 0 && mNumberOfInventoryUomCode==3 && Sm.IsGrdValueEmpty(Grd1, Row, 21, true, "Quantity (3) is empty."))
                    ) return true;

                if (((Sm.GetGrdStr(Grd1, Row, 15).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 15).Length != 0 && Sm.GetGrdDec(Grd1, Row, 15) == 0m)) &&
                    ((Sm.GetGrdStr(Grd1, Row, 18).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 18).Length != 0 && Sm.GetGrdDec(Grd1, Row, 18) == 0m)) &&
                    ((Sm.GetGrdStr(Grd1, Row, 21).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 21).Length != 0 && Sm.GetGrdDec(Grd1, Row, 21) == 0m)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "DO to department's document number : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                        "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                        "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                        "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                        "Quantity should be greater than 0.");
                    return true;
                }

                if (IsRecvQtyBiggerThanRemainingQty(Row)) return true;
            }
            return false;
        }

        private bool IsRecvQtyBiggerThanRemainingQty(int Row)
        {
            decimal
                Remaining1 = Sm.GetGrdDec(Grd1, Row, 14),
                Recv1 = Sm.GetGrdDec(Grd1, Row, 15),
                Remaining2 = Sm.GetGrdDec(Grd1, Row, 17),
                Recv2 = Sm.GetGrdDec(Grd1, Row, 18),
                Remaining3 = Sm.GetGrdDec(Grd1, Row, 20),
                Recv3 = Sm.GetGrdDec(Grd1, Row, 21);

            if (Recv1 > Remaining1)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO to department's document number : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                    "Received quantity 1 (" + Sm.FormatNum(Recv1, 0) + ") is greater than remaining quantity (" + Sm.FormatNum(Remaining1, 0) + ")."
                    );
                return true;
            }

            if (Recv2 > Remaining2)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO to department# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                    "Received quantity 2 (" + Sm.FormatNum(Recv2, 0) + ") is greater than remaining quantity (" + Sm.FormatNum(Remaining2, 0) + ")."
                    );
                return true;
            }

            if (Recv3 > Remaining3)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO to department# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                    "Received quantity 3 (" + Sm.FormatNum(Recv3, 0) + ") is greater than remaining quantity (" + Sm.FormatNum(Remaining3, 0) + ")."
                    );
                return true;
            }

            return false;
        }

        private MySqlCommand SaveRecvDeptHdr(string DocNo, string JournalDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRecvDeptHdr(DocNo, DocDt, WhsCode, DeptCode, Remark, JournalDocNo, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @DeptCode, @Remark, Case When @JournalDocNo='' Then Null Else @JournalDocNo End, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", (mIsSystemUseCostCenter) ? JournalDocNo : "");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvDeptDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRecvDeptDtl(DocNo, DNo, CancelInd, DODeptDocNo, DODeptDNo, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @DODeptDocNo, @DODeptDNo, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DODeptDocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@DODeptDNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockMovement(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, C.Source, 'N', '' As Source2, ");
            SQL.AppendLine("A.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvDeptHdr A ");
            SQL.AppendLine("Inner Join TblRecvDeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDODeptDtl C On B.DODeptDocNo=C.DocNo And B.DODeptDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockSummary ");
            SQL.AppendLine("(WhsCode, Lot, Bin, Source, ItCode, BatchNo, PropCode, Qty, Qty2, Qty3, CreateBy, CreateDt) ");
            SQL.AppendLine("Select WhsCode, Lot, Bin, Source, ItCode, BatchNo, PropCode, ");
            SQL.AppendLine("Qty, Qty2, Qty3, CreateBy, CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select @WhsCode As WhsCode, @Lot As Lot, @Bin As Bin, @Source As Source, ");
            SQL.AppendLine("    @ItCode As ItCode, @BatchNo As BatchNo, '-' As PropCode, ");
            SQL.AppendLine("    0.00 As Qty, 0.00 As Qty2, 0.00 As Qty3, @UserCode As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine(") T Where Not Exists(");
            SQL.AppendLine("    Select 1 From TblStockSummary ");
            SQL.AppendLine("    Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source ");
            SQL.AppendLine("    ) Limit 1; ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WHsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, string JournalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Department : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@CCCode, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select F.AcNo, 0 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
            SQL.AppendLine("        From TblStockMovement A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.ItCode=B.ItCode And A.BatchNo=B.BatchNo And A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblRecvDeptDtl C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblDODeptHdr D On C.DODeptDocNo=D.DocNo ");
            SQL.AppendLine("        Inner Join TblItemCostCategory E On A.ItCode=E.ItCode And D.CCCode=E.CCCode ");
            SQL.AppendLine("        Inner Join TblCostCategory F On E.CCtCode=F.CCtCode And E.CCCode=F.CCCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DocType=@DocType And A.CancelInd='N' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.Excrate As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblStockMovement A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.ItCode=B.ItCode And A.BatchNo=B.BatchNo And A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DocType=@DocType And A.CancelInd='N' ");
            SQL.AppendLine("    ) Tbl Group By AcNo  ");
            SQL.AppendLine(") T;  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetGrdStr(Grd1, 0, 28));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsCancelledDataNotValid(DNo)
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelRecvDeptDtl(DNo));

            cml.Add(SaveStockMovement2(TxtDocNo.Text, DNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (
                    Sm.GetGrdBool(Grd1, Row, 1) && 
                    !Sm.GetGrdBool(Grd1, Row, 2) && 
                    Sm.GetGrdStr(Grd1, Row, 11).Length > 0
                    )
                    cml.Add(SaveStockSummary2(Row));

            if (mIsAutoJournalActived) cml.Add(SaveJournal2(TxtDocNo.Text, DNo));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblRecvDeptDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo) || 
                IsGrdValueNotValid2();
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 DO.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid2()
        {
            string Msg = string.Empty;

            ReComputeStock();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd1, Row, 11).Length > 0 &&
                    Sm.GetGrdBool(Grd1, Row, 1) && 
                    !Sm.GetGrdBool(Grd1, Row, 2)
                    )
                {
                    Msg =
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                        "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                        "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                        "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                    if (Sm.GetGrdDec(Grd1, Row, 15) > Sm.GetGrdDec(Grd1, Row, 25))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Cancelled Quantity should not be greater than existing stock.");
                        return true;
                    }

                    if (Grd1.Cols[18].Visible)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 18) > Sm.GetGrdDec(Grd1, Row, 26))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Cancelled Quantity (2) should not be greater than existing stock (2).");
                            return true;
                        }
                    }

                    if (Grd1.Cols[21].Visible)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 21) > Sm.GetGrdDec(Grd1, Row, 27))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Cancelled Quantity (3) should not be greater than existing stock (3).");
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private MySqlCommand CancelRecvDeptDtl(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvDeptDtl Set ");
            SQL.AppendLine("CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            
            //SQL.AppendLine("Insert Into TblStockMovement ");
            //SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            //SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, ");
            //SQL.AppendLine("Qty, Qty2, Qty3, ");
            //SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, C.Source, 'Y', '' As Source2, ");
            //SQL.AppendLine("A.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
            //SQL.AppendLine("B.Qty*-1, B.Qty2*-1, B.Qty3*-1, ");
            //SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            //SQL.AppendLine("From TblRecvDeptHdr A ");
            //SQL.AppendLine("Inner Join TblRecvDeptDtl B ");
            //SQL.AppendLine("    On A.DocNo=B.DocNo ");
            //SQL.AppendLine("    And Position(Concat('##', B.DNo, '##') In @DNo)>0; ");
            //SQL.AppendLine("Inner Join TblDODeptDtl C On B.DODeptDocNo=C.DocNo And B.DODeptDNo=C.DNo ");
            //SQL.AppendLine("Where A.DocNo=@DocNo; ");
            
            
            //SQL.AppendLine("Update TblStockSummary As T1 ");
            //SQL.AppendLine("Inner Join ( ");
            //SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.BatchNo, A.Source, ");
            //SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            //SQL.AppendLine("    From TblStockMovement A ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, ItCode, BatchNo, Source ");
            //SQL.AppendLine("        From TblStockMovement ");
            //SQL.AppendLine("        Where DocType=@DocType ");
            //SQL.AppendLine("        And DocNo=@DocNo ");
            //SQL.AppendLine("        And CancelInd='Y' ");
            //SQL.AppendLine("        And Position(Concat('##', DNo, '##') In @DNo)>0 ");
            //SQL.AppendLine("    ) B ");
            //SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            //SQL.AppendLine("        And A.Lot=B.Lot ");
            //SQL.AppendLine("        And A.Bin=B.Bin ");
            //SQL.AppendLine("        And A.ItCode=B.ItCode ");
            //SQL.AppendLine("        And A.BatchNo=B.BatchNo ");
            //SQL.AppendLine("        And A.Source=B.Source ");
            //SQL.AppendLine("    Where (A.Qty<>0 Or A.Qty2<>0 Or A.Qty3<>0) ");
            //SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.ItCode, A.BatchNo, A.Source ");
            //SQL.AppendLine(") T2 ");
            //SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            //SQL.AppendLine("    And T1.Lot=T2.Lot ");
            //SQL.AppendLine("    And T1.Bin=T2.Bin ");
            //SQL.AppendLine("    And T1.ItCode=T2.ItCode ");
            //SQL.AppendLine("    And T1.BatchNo=T2.BatchNo ");
            //SQL.AppendLine("    And T1.Source=T2.Source ");
            //SQL.AppendLine("Set ");
            //SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            //SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            //SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            //SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            //SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            //if (mIsSystemUseCostCenter)
            //{
            //    SQL.AppendLine("Set @row:=0; ");
            //    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, CreateBy, CreateDt) ");
            //    SQL.AppendLine("Values(@JournalDocNo, @DocDt, Concat('Cancelling Receiving Item From Department : ', @DocNo), @UserCode, CurrentDateTime()); ");

            //    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            //    SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            //    SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            //    SQL.AppendLine("From ( ");
            //    SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            //    SQL.AppendLine("        Select F.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0 As CAmt ");
            //    SQL.AppendLine("        From TblStockMovement A ");
            //    SQL.AppendLine("        Inner Join TblStockPrice B On A.ItCode=B.ItCode And A.BatchNo=B.BatchNo And A.Source=B.Source ");
            //    SQL.AppendLine("        Inner Join TblRecvDeptDtl C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            //    SQL.AppendLine("        Inner Join TblDODeptHdr D On C.DODeptDocNo=D.DocNo ");
            //    SQL.AppendLine("        Inner Join TblItemCostCategory E On A.ItCode=E.ItCode And D.CCCode=E.CCCode ");
            //    SQL.AppendLine("        Inner Join TblCostCategory F On E.CCtCode=F.CCtCode And E.CCCode=F.CCCode ");
            //    SQL.AppendLine("        Where A.DocNo=@DocNo And A.DocType=@DocType ");
            //    SQL.AppendLine("        And Position(Concat('##', A.DNo, '##') In @DNo)>0 And A.CancelInd='N' ");
            //    SQL.AppendLine("        Union All ");
            //    SQL.AppendLine("        Select D.AcNo, 0 As DAmt, A.Qty*B.UPrice*B.Excrate As CAmt ");
            //    SQL.AppendLine("        From TblStockMovement A ");
            //    SQL.AppendLine("        Inner Join TblStockPrice B On A.ItCode=B.ItCode And A.BatchNo=B.BatchNo And A.Source=B.Source ");
            //    SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            //    SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");
            //    SQL.AppendLine("        Where A.DocNo=@DocNo And A.DocType=@DocType ");
            //    SQL.AppendLine("        And Position(Concat('##', A.DNo, '##') In @DNo)>0 And A.CancelInd='N' ");
            //    SQL.AppendLine("    ) Tbl Group By AcNo  ");
            //    SQL.AppendLine(") T;  ");
            //}

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockMovement2(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, C.Source, 'Y', '' As Source2, ");
            SQL.AppendLine("A.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
            SQL.AppendLine("B.Qty*-1, B.Qty2*-1, B.Qty3*-1, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvDeptHdr A ");
            SQL.AppendLine("Inner Join TblRecvDeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Position(Concat('##', B.DNo, '##') In @DNo)>0 ");
            SQL.AppendLine("Inner Join TblDODeptDtl C On B.DODeptDocNo=C.DocNo And B.DODeptDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary2(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WHsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);
    
            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblRecvDeptDtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Cancelling Receiving Item From Department : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("CCCode, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select F.AcNo, 0 As CAmt, A.Qty*B.UPrice*B.ExcRate As DAmt ");
            SQL.AppendLine("        From TblStockMovement A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.ItCode=B.ItCode And A.BatchNo=B.BatchNo And A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblRecvDeptDtl C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblDODeptHdr D On C.DODeptDocNo=D.DocNo ");
            SQL.AppendLine("        Inner Join TblItemCostCategory E On A.ItCode=E.ItCode And D.CCCode=E.CCCode ");
            SQL.AppendLine("        Inner Join TblCostCategory F On E.CCtCode=F.CCtCode And E.CCCode=F.CCCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DocType=@DocType And A.CancelInd='N' ");
            SQL.AppendLine("        And Position(Concat('##', A.DNo, '##') In @DNo)>0 ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.Excrate As CAmt, 0 As DAmt ");
            SQL.AppendLine("        From TblStockMovement A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.ItCode=B.ItCode And A.BatchNo=B.BatchNo And A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DocType=@DocType And A.CancelInd='N' ");
            SQL.AppendLine("        And Position(Concat('##', A.DNo, '##') In @DNo)>0 ");
            SQL.AppendLine("    ) Tbl Group By AcNo  ");
            SQL.AppendLine(") T;  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowRecvDeptHdr(DocNo);
                ShowRecvDeptDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvDeptHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WhsCode, DeptCode, Remark From TblRecvDeptHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "WhsCode", "DeptCode", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowRecvDeptDtl(string DocNo)
        {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DNo, A.CancelInd, A.DODeptDocNo, A.DODeptDNo, ");
                SQL.AppendLine("B.ItCode, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin, A.Qty, A.Qty2, A.Qty3, ");
                SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, A.Remark, B.Remark As DORemark, E.CCCode, E.CCName, ");

                SQL.AppendLine("B.Qty-IfNull(( ");
                SQL.AppendLine("    Select Sum(Qty) From TblRecvDeptDtl ");
                SQL.AppendLine("    Where DODeptDocNo= A.DODeptDocNo And DODeptDNo= A.DODeptDNo ");
                SQL.AppendLine("    And CancelInd='N' And Concat(DocNo, DNo)<>Concat(A.DocNo, A.DNo) ");
                SQL.AppendLine("), 0) As RemainingQty, ");

                SQL.AppendLine("B.Qty2-IfNull(( ");
                SQL.AppendLine("    Select Sum(Qty2) From TblRecvDeptDtl ");
                SQL.AppendLine("    Where DODeptDocNo= A.DODeptDocNo And DODeptDNo= A.DODeptDNo ");
                SQL.AppendLine("    And CancelInd='N' And Concat(DocNo, DNo)<>Concat(A.DocNo, A.DNo) ");
                SQL.AppendLine("), 0) As RemainingQty2, ");

                SQL.AppendLine("B.Qty3-IfNull(( ");
                SQL.AppendLine("    Select Sum(Qty3) From TblRecvDeptDtl ");
                SQL.AppendLine("    Where DODeptDocNo= A.DODeptDocNo And DODeptDNo= A.DODeptDNo ");
                SQL.AppendLine("    And CancelInd='N' And Concat(DocNo, DNo)<>Concat(A.DocNo, A.DNo) ");
                SQL.AppendLine("), 0) As RemainingQty3 ");

                SQL.AppendLine("From TblRecvDeptDtl A ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DODeptDocNo=B.DocNo And A.DODeptDNo=B.DNo ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("Inner Join TblDODeptHdr D On A.DODeptDocNo=D.DocNo ");
                SQL.AppendLine("Left Join TblCostCenter E On D.CCCode=E.CCCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "CancelInd", "DODeptDocNo", "DODeptDNo", "ItCode", "ItName", 
                        
                        //6-10
                        "BatchNo", "Source", "Lot", "Bin", "RemainingQty",  
                        
                        //11-15
                        "Qty", "InventoryUomCode", "RemainingQty2", "Qty2", "InventoryUomCode2", 
                        
                        //16-20
                        "RemainingQty3", "Qty3", "InventoryUomCode3", "DORemark", "Remark",

                        //21-22
                        "CCCode", "CCName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                        Grd.Cells[Row, 25].Value = 0m;
                        Grd.Cells[Row, 26].Value = 0m;
                        Grd.Cells[Row, 27].Value = 0m;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                    }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 25, 26, 27 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsSystemUseCostCenter = Sm.GetParameterBoo("IsSystemUseCostCenter");
            mIsJournalCostCenterEnabled = Sm.GetParameterBoo("IsJournalCostCenterEnabled");
            SetNumberOfInventoryUomCode();
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedDODept()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL += ((SQL.Length != 0?", ":"") + "'" + Sm.GetGrdStr(Grd1, Row, 4) + Sm.GetGrdStr(Grd1, Row, 5) + "'");
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void ReComputeRemainingQty()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T1.DocNo, T1.DNo, ");
                SQL.AppendLine("T1.Qty-IfNull(T2.Qty, 0) As OutstandingQty1, T1.Qty2-IfNull(T2.Qty2, 0) As OutstandingQty2, T1.Qty3-IfNull(T2.Qty3, 0) As OutstandingQty3 ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select B.DocNo, B.DNo, B.Qty, B.Qty2, B.Qty3 ");
                SQL.AppendLine("    From TblDODeptHdr A, TblDODeptDtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo And B.CancelInd='N' And A.WhsCode=@WhsCode And A.DeptCode=@DeptCode ");
                SQL.AppendLine("    And Concat(B.DocNo, B.DNo) In (" + GetSelectedDODept() + ") ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select B.DODeptDocNo, B.DODeptDNo, Sum(B.Qty) As Qty, Sum(B.Qty2) As Qty2, Sum(B.Qty3) As Qty3 ");
                SQL.AppendLine("    From TblRecvDeptHdr A, TblRecvDeptDtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo And B.CancelInd='N' And A.WhsCode=@WhsCode And A.DeptCode=@DeptCode And Concat(B.DODeptDocNo, B.DODeptDNo) In (" + GetSelectedDODept() + ") ");
                SQL.AppendLine("    Group By B.DODeptDocNo, B.DODeptDNo ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.DODeptDocNo And  T1.DNo=T2.DODeptDNo ");

                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "DocNo", 
                        "DNo", "OutstandingQty1", "OutstandingQty2", "OutstandingQty3"
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 4), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 2);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 4);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 11);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 12), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 13), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 25, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 26, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 27, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.ClearGrd(Grd1, true);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #endregion
    }

    #region Report Class

    class RecvDeptHdr
    {
        public string Company { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string WhsName { get; set; }
        public string DeptName { get; set; }
        public string Remark { get; set; }
        public string CompanyLogo { get; set; }
        public string PrintBy { get; set; }

    }

    class RecvDeptDtl
    {
        public string DODeptDocNo { get; set; }
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string BatchNo { get; set; }
        public string Lot { get; set; }
        public string Bin { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public string InventoryUomCode { get; set; }
        public string InventoryUomCode2 { get; set; }
        public string InventoryUomCode3 { get; set; }
        public string DocDt { get; set; }
        public string Remark { get; set; }
    }

    #endregion
}
