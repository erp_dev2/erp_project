﻿namespace RunSystem
{
    partial class FrmEntity2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEntity2));
            this.TxtEntName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtEntCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LueParent = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtEntLogo = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtEntPhone = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtEntFax = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.MeeEntAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtAcDesc1 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo1 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo1 = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtAcDesc2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo2 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtAcDesc3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo3 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo3 = new DevExpress.XtraEditors.SimpleButton();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtAcDesc4 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo4 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo4 = new DevExpress.XtraEditors.SimpleButton();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtAcDesc6 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo6 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo6 = new DevExpress.XtraEditors.SimpleButton();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtAcDesc5 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo5 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo5 = new DevExpress.XtraEditors.SimpleButton();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.ChkAutoCreate = new DevExpress.XtraEditors.CheckEdit();
            this.ChkConsolidate = new DevExpress.XtraEditors.CheckEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtShortCode = new DevExpress.XtraEditors.TextEdit();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.TxtAcDesc8 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo8 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo8 = new DevExpress.XtraEditors.SimpleButton();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtAcDesc7 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo7 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo7 = new DevExpress.XtraEditors.SimpleButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtEntCode2 = new DevExpress.XtraEditors.TextEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntLogo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEntAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoCreate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkConsolidate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 473);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // TxtEntName
            // 
            this.TxtEntName.EnterMoveNextControl = true;
            this.TxtEntName.Location = new System.Drawing.Point(260, 8);
            this.TxtEntName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEntName.Name = "TxtEntName";
            this.TxtEntName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEntName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEntName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEntName.Properties.Appearance.Options.UseFont = true;
            this.TxtEntName.Properties.MaxLength = 80;
            this.TxtEntName.Size = new System.Drawing.Size(429, 20);
            this.TxtEntName.TabIndex = 13;
            this.TxtEntName.Validated += new System.EventHandler(this.TxtEntName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(182, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Entity Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEntCode
            // 
            this.TxtEntCode.EnterMoveNextControl = true;
            this.TxtEntCode.Location = new System.Drawing.Point(82, 8);
            this.TxtEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEntCode.Name = "TxtEntCode";
            this.TxtEntCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEntCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEntCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEntCode.Properties.MaxLength = 16;
            this.TxtEntCode.Size = new System.Drawing.Size(95, 20);
            this.TxtEntCode.TabIndex = 11;
            this.TxtEntCode.Validated += new System.EventHandler(this.TxtEntCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(7, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Entity Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(44, 17);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 14);
            this.label8.TabIndex = 15;
            this.label8.Text = "Parent";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueParent
            // 
            this.LueParent.EnterMoveNextControl = true;
            this.LueParent.Location = new System.Drawing.Point(91, 14);
            this.LueParent.Margin = new System.Windows.Forms.Padding(5);
            this.LueParent.Name = "LueParent";
            this.LueParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.Appearance.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParent.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParent.Properties.DropDownRows = 30;
            this.LueParent.Properties.MaxLength = 40;
            this.LueParent.Properties.NullText = "[Empty]";
            this.LueParent.Properties.PopupWidth = 350;
            this.LueParent.Size = new System.Drawing.Size(319, 20);
            this.LueParent.TabIndex = 16;
            this.LueParent.ToolTip = "F4 : Show/hide list";
            this.LueParent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParent.EditValueChanged += new System.EventHandler(this.LueParent_EditValueChanged);
            // 
            // TxtEntLogo
            // 
            this.TxtEntLogo.EnterMoveNextControl = true;
            this.TxtEntLogo.Location = new System.Drawing.Point(91, 36);
            this.TxtEntLogo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEntLogo.Name = "TxtEntLogo";
            this.TxtEntLogo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEntLogo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEntLogo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEntLogo.Properties.Appearance.Options.UseFont = true;
            this.TxtEntLogo.Properties.MaxLength = 40;
            this.TxtEntLogo.Size = new System.Drawing.Size(319, 20);
            this.TxtEntLogo.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(18, 39);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Logo Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(37, 105);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 23;
            this.label4.Text = "Address";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEntPhone
            // 
            this.TxtEntPhone.EnterMoveNextControl = true;
            this.TxtEntPhone.Location = new System.Drawing.Point(91, 58);
            this.TxtEntPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEntPhone.Name = "TxtEntPhone";
            this.TxtEntPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEntPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEntPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEntPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtEntPhone.Properties.MaxLength = 40;
            this.TxtEntPhone.Size = new System.Drawing.Size(319, 20);
            this.TxtEntPhone.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(45, 61);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 14);
            this.label5.TabIndex = 19;
            this.label5.Text = "Phone";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEntFax
            // 
            this.TxtEntFax.EnterMoveNextControl = true;
            this.TxtEntFax.Location = new System.Drawing.Point(91, 80);
            this.TxtEntFax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEntFax.Name = "TxtEntFax";
            this.TxtEntFax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEntFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEntFax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEntFax.Properties.Appearance.Options.UseFont = true;
            this.TxtEntFax.Properties.MaxLength = 40;
            this.TxtEntFax.Size = new System.Drawing.Size(319, 20);
            this.TxtEntFax.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(62, 83);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Fax";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeEntAddress
            // 
            this.MeeEntAddress.EnterMoveNextControl = true;
            this.MeeEntAddress.Location = new System.Drawing.Point(91, 102);
            this.MeeEntAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEntAddress.Name = "MeeEntAddress";
            this.MeeEntAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEntAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeEntAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEntAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEntAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEntAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEntAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEntAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEntAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEntAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEntAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEntAddress.Properties.MaxLength = 400;
            this.MeeEntAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEntAddress.Properties.ShowIcon = false;
            this.MeeEntAddress.Size = new System.Drawing.Size(513, 20);
            this.MeeEntAddress.TabIndex = 24;
            this.MeeEntAddress.ToolTip = "F4 : Show/hide text";
            this.MeeEntAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEntAddress.ToolTipTitle = "Run System";
            // 
            // TxtAcDesc1
            // 
            this.TxtAcDesc1.EnterMoveNextControl = true;
            this.TxtAcDesc1.Location = new System.Drawing.Point(221, 33);
            this.TxtAcDesc1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc1.Name = "TxtAcDesc1";
            this.TxtAcDesc1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc1.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc1.Properties.MaxLength = 255;
            this.TxtAcDesc1.Properties.ReadOnly = true;
            this.TxtAcDesc1.Size = new System.Drawing.Size(513, 20);
            this.TxtAcDesc1.TabIndex = 17;
            // 
            // TxtAcNo1
            // 
            this.TxtAcNo1.EnterMoveNextControl = true;
            this.TxtAcNo1.Location = new System.Drawing.Point(221, 10);
            this.TxtAcNo1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo1.Name = "TxtAcNo1";
            this.TxtAcNo1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo1.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo1.Properties.MaxLength = 80;
            this.TxtAcNo1.Properties.ReadOnly = true;
            this.TxtAcNo1.Size = new System.Drawing.Size(166, 20);
            this.TxtAcNo1.TabIndex = 15;
            // 
            // BtnAcNo1
            // 
            this.BtnAcNo1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo1.Appearance.Options.UseBackColor = true;
            this.BtnAcNo1.Appearance.Options.UseFont = true;
            this.BtnAcNo1.Appearance.Options.UseForeColor = true;
            this.BtnAcNo1.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo1.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo1.Image")));
            this.BtnAcNo1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo1.Location = new System.Drawing.Point(390, 9);
            this.BtnAcNo1.Name = "BtnAcNo1";
            this.BtnAcNo1.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo1.TabIndex = 16;
            this.BtnAcNo1.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo1.ToolTipTitle = "Run System";
            this.BtnAcNo1.Click += new System.EventHandler(this.BtnAcNo1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(6, 13);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(208, 14);
            this.label7.TabIndex = 14;
            this.label7.Text = "COA\'s Account (Uninvoice Purchase)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc2
            // 
            this.TxtAcDesc2.EnterMoveNextControl = true;
            this.TxtAcDesc2.Location = new System.Drawing.Point(221, 79);
            this.TxtAcDesc2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc2.Name = "TxtAcDesc2";
            this.TxtAcDesc2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc2.Properties.MaxLength = 255;
            this.TxtAcDesc2.Properties.ReadOnly = true;
            this.TxtAcDesc2.Size = new System.Drawing.Size(513, 20);
            this.TxtAcDesc2.TabIndex = 21;
            // 
            // TxtAcNo2
            // 
            this.TxtAcNo2.EnterMoveNextControl = true;
            this.TxtAcNo2.Location = new System.Drawing.Point(221, 56);
            this.TxtAcNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo2.Name = "TxtAcNo2";
            this.TxtAcNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo2.Properties.MaxLength = 80;
            this.TxtAcNo2.Properties.ReadOnly = true;
            this.TxtAcNo2.Size = new System.Drawing.Size(166, 20);
            this.TxtAcNo2.TabIndex = 19;
            // 
            // BtnAcNo2
            // 
            this.BtnAcNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo2.Appearance.Options.UseBackColor = true;
            this.BtnAcNo2.Appearance.Options.UseFont = true;
            this.BtnAcNo2.Appearance.Options.UseForeColor = true;
            this.BtnAcNo2.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo2.Image")));
            this.BtnAcNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo2.Location = new System.Drawing.Point(390, 55);
            this.BtnAcNo2.Name = "BtnAcNo2";
            this.BtnAcNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo2.TabIndex = 20;
            this.BtnAcNo2.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo2.ToolTipTitle = "Run System";
            this.BtnAcNo2.Click += new System.EventHandler(this.BtnAcNo2_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(28, 59);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(186, 14);
            this.label9.TabIndex = 18;
            this.label9.Text = "COA\'s Account (Uninvoice Sales)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc3
            // 
            this.TxtAcDesc3.EnterMoveNextControl = true;
            this.TxtAcDesc3.Location = new System.Drawing.Point(221, 125);
            this.TxtAcDesc3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc3.Name = "TxtAcDesc3";
            this.TxtAcDesc3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc3.Properties.MaxLength = 255;
            this.TxtAcDesc3.Properties.ReadOnly = true;
            this.TxtAcDesc3.Size = new System.Drawing.Size(513, 20);
            this.TxtAcDesc3.TabIndex = 25;
            // 
            // TxtAcNo3
            // 
            this.TxtAcNo3.EnterMoveNextControl = true;
            this.TxtAcNo3.Location = new System.Drawing.Point(221, 102);
            this.TxtAcNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo3.Name = "TxtAcNo3";
            this.TxtAcNo3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo3.Properties.MaxLength = 80;
            this.TxtAcNo3.Properties.ReadOnly = true;
            this.TxtAcNo3.Size = new System.Drawing.Size(166, 20);
            this.TxtAcNo3.TabIndex = 23;
            // 
            // BtnAcNo3
            // 
            this.BtnAcNo3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo3.Appearance.Options.UseBackColor = true;
            this.BtnAcNo3.Appearance.Options.UseFont = true;
            this.BtnAcNo3.Appearance.Options.UseForeColor = true;
            this.BtnAcNo3.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo3.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo3.Image")));
            this.BtnAcNo3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo3.Location = new System.Drawing.Point(390, 101);
            this.BtnAcNo3.Name = "BtnAcNo3";
            this.BtnAcNo3.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo3.TabIndex = 24;
            this.BtnAcNo3.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo3.ToolTipTitle = "Run System";
            this.BtnAcNo3.Click += new System.EventHandler(this.BtnAcNo3_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(96, 105);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(118, 14);
            this.label10.TabIndex = 22;
            this.label10.Text = "COA\'s Account (AP)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc4
            // 
            this.TxtAcDesc4.EnterMoveNextControl = true;
            this.TxtAcDesc4.Location = new System.Drawing.Point(221, 171);
            this.TxtAcDesc4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc4.Name = "TxtAcDesc4";
            this.TxtAcDesc4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc4.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc4.Properties.MaxLength = 255;
            this.TxtAcDesc4.Properties.ReadOnly = true;
            this.TxtAcDesc4.Size = new System.Drawing.Size(513, 20);
            this.TxtAcDesc4.TabIndex = 29;
            // 
            // TxtAcNo4
            // 
            this.TxtAcNo4.EnterMoveNextControl = true;
            this.TxtAcNo4.Location = new System.Drawing.Point(221, 148);
            this.TxtAcNo4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo4.Name = "TxtAcNo4";
            this.TxtAcNo4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo4.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo4.Properties.MaxLength = 80;
            this.TxtAcNo4.Properties.ReadOnly = true;
            this.TxtAcNo4.Size = new System.Drawing.Size(166, 20);
            this.TxtAcNo4.TabIndex = 27;
            // 
            // BtnAcNo4
            // 
            this.BtnAcNo4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo4.Appearance.Options.UseBackColor = true;
            this.BtnAcNo4.Appearance.Options.UseFont = true;
            this.BtnAcNo4.Appearance.Options.UseForeColor = true;
            this.BtnAcNo4.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo4.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo4.Image")));
            this.BtnAcNo4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo4.Location = new System.Drawing.Point(390, 147);
            this.BtnAcNo4.Name = "BtnAcNo4";
            this.BtnAcNo4.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo4.TabIndex = 28;
            this.BtnAcNo4.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo4.ToolTipTitle = "Run System";
            this.BtnAcNo4.Click += new System.EventHandler(this.BtnAcNo4_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(96, 151);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 14);
            this.label11.TabIndex = 26;
            this.label11.Text = "COA\'s Account (AR)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc6
            // 
            this.TxtAcDesc6.EnterMoveNextControl = true;
            this.TxtAcDesc6.Location = new System.Drawing.Point(220, 263);
            this.TxtAcDesc6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc6.Name = "TxtAcDesc6";
            this.TxtAcDesc6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc6.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc6.Properties.MaxLength = 255;
            this.TxtAcDesc6.Properties.ReadOnly = true;
            this.TxtAcDesc6.Size = new System.Drawing.Size(513, 20);
            this.TxtAcDesc6.TabIndex = 37;
            // 
            // TxtAcNo6
            // 
            this.TxtAcNo6.EnterMoveNextControl = true;
            this.TxtAcNo6.Location = new System.Drawing.Point(220, 240);
            this.TxtAcNo6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo6.Name = "TxtAcNo6";
            this.TxtAcNo6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo6.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo6.Properties.MaxLength = 80;
            this.TxtAcNo6.Properties.ReadOnly = true;
            this.TxtAcNo6.Size = new System.Drawing.Size(166, 20);
            this.TxtAcNo6.TabIndex = 35;
            // 
            // BtnAcNo6
            // 
            this.BtnAcNo6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo6.Appearance.Options.UseBackColor = true;
            this.BtnAcNo6.Appearance.Options.UseFont = true;
            this.BtnAcNo6.Appearance.Options.UseForeColor = true;
            this.BtnAcNo6.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo6.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo6.Image")));
            this.BtnAcNo6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo6.Location = new System.Drawing.Point(389, 239);
            this.BtnAcNo6.Name = "BtnAcNo6";
            this.BtnAcNo6.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo6.TabIndex = 36;
            this.BtnAcNo6.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo6.ToolTipTitle = "Run System";
            this.BtnAcNo6.Click += new System.EventHandler(this.BtnAcNo6_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(84, 243);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(130, 14);
            this.label12.TabIndex = 34;
            this.label12.Text = "COA\'s Account (Sales)";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc5
            // 
            this.TxtAcDesc5.EnterMoveNextControl = true;
            this.TxtAcDesc5.Location = new System.Drawing.Point(220, 217);
            this.TxtAcDesc5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc5.Name = "TxtAcDesc5";
            this.TxtAcDesc5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc5.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc5.Properties.MaxLength = 255;
            this.TxtAcDesc5.Properties.ReadOnly = true;
            this.TxtAcDesc5.Size = new System.Drawing.Size(513, 20);
            this.TxtAcDesc5.TabIndex = 33;
            // 
            // TxtAcNo5
            // 
            this.TxtAcNo5.EnterMoveNextControl = true;
            this.TxtAcNo5.Location = new System.Drawing.Point(220, 194);
            this.TxtAcNo5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo5.Name = "TxtAcNo5";
            this.TxtAcNo5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo5.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo5.Properties.MaxLength = 80;
            this.TxtAcNo5.Properties.ReadOnly = true;
            this.TxtAcNo5.Size = new System.Drawing.Size(166, 20);
            this.TxtAcNo5.TabIndex = 31;
            // 
            // BtnAcNo5
            // 
            this.BtnAcNo5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo5.Appearance.Options.UseBackColor = true;
            this.BtnAcNo5.Appearance.Options.UseFont = true;
            this.BtnAcNo5.Appearance.Options.UseForeColor = true;
            this.BtnAcNo5.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo5.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo5.Image")));
            this.BtnAcNo5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo5.Location = new System.Drawing.Point(389, 193);
            this.BtnAcNo5.Name = "BtnAcNo5";
            this.BtnAcNo5.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo5.TabIndex = 32;
            this.BtnAcNo5.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo5.ToolTipTitle = "Run System";
            this.BtnAcNo5.Click += new System.EventHandler(this.BtnAcNo5_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(80, 197);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(134, 14);
            this.label13.TabIndex = 30;
            this.label13.Text = "COA\'s Account (COGS)";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(15, 289);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(199, 14);
            this.label14.TabIndex = 38;
            this.label14.Text = "COA\'s Account (Depreciation Cost)";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 38);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(772, 435);
            this.Tc1.TabIndex = 15;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.ChkAutoCreate);
            this.Tp1.Controls.Add(this.ChkConsolidate);
            this.Tp1.Controls.Add(this.label16);
            this.Tp1.Controls.Add(this.TxtShortCode);
            this.Tp1.Controls.Add(this.LueParent);
            this.Tp1.Controls.Add(this.label8);
            this.Tp1.Controls.Add(this.label3);
            this.Tp1.Controls.Add(this.TxtEntLogo);
            this.Tp1.Controls.Add(this.label4);
            this.Tp1.Controls.Add(this.label5);
            this.Tp1.Controls.Add(this.TxtEntPhone);
            this.Tp1.Controls.Add(this.label6);
            this.Tp1.Controls.Add(this.TxtEntFax);
            this.Tp1.Controls.Add(this.MeeEntAddress);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(766, 407);
            this.Tp1.Text = "General";
            // 
            // ChkAutoCreate
            // 
            this.ChkAutoCreate.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkAutoCreate.Location = new System.Drawing.Point(419, 36);
            this.ChkAutoCreate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkAutoCreate.Name = "ChkAutoCreate";
            this.ChkAutoCreate.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkAutoCreate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAutoCreate.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkAutoCreate.Properties.Appearance.Options.UseBackColor = true;
            this.ChkAutoCreate.Properties.Appearance.Options.UseFont = true;
            this.ChkAutoCreate.Properties.Appearance.Options.UseForeColor = true;
            this.ChkAutoCreate.Properties.Caption = "Auto Create";
            this.ChkAutoCreate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAutoCreate.Size = new System.Drawing.Size(185, 22);
            this.ChkAutoCreate.TabIndex = 28;
            // 
            // ChkConsolidate
            // 
            this.ChkConsolidate.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkConsolidate.Location = new System.Drawing.Point(419, 14);
            this.ChkConsolidate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkConsolidate.Name = "ChkConsolidate";
            this.ChkConsolidate.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkConsolidate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkConsolidate.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkConsolidate.Properties.Appearance.Options.UseBackColor = true;
            this.ChkConsolidate.Properties.Appearance.Options.UseFont = true;
            this.ChkConsolidate.Properties.Appearance.Options.UseForeColor = true;
            this.ChkConsolidate.Properties.Caption = "Consolidated Parent Entity";
            this.ChkConsolidate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkConsolidate.Size = new System.Drawing.Size(185, 22);
            this.ChkConsolidate.TabIndex = 27;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(18, 126);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 14);
            this.label16.TabIndex = 25;
            this.label16.Text = "Short Code";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtShortCode
            // 
            this.TxtShortCode.EnterMoveNextControl = true;
            this.TxtShortCode.Location = new System.Drawing.Point(91, 123);
            this.TxtShortCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortCode.Name = "TxtShortCode";
            this.TxtShortCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortCode.Properties.Appearance.Options.UseFont = true;
            this.TxtShortCode.Properties.MaxLength = 40;
            this.TxtShortCode.Size = new System.Drawing.Size(319, 20);
            this.TxtShortCode.TabIndex = 26;
            this.TxtShortCode.Validated += new System.EventHandler(this.TxtShortCode_Validated);
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.TxtAcDesc8);
            this.Tp2.Controls.Add(this.TxtAcNo8);
            this.Tp2.Controls.Add(this.BtnAcNo8);
            this.Tp2.Controls.Add(this.label15);
            this.Tp2.Controls.Add(this.TxtAcDesc7);
            this.Tp2.Controls.Add(this.TxtAcNo7);
            this.Tp2.Controls.Add(this.BtnAcNo7);
            this.Tp2.Controls.Add(this.TxtAcNo1);
            this.Tp2.Controls.Add(this.label7);
            this.Tp2.Controls.Add(this.label14);
            this.Tp2.Controls.Add(this.TxtAcDesc6);
            this.Tp2.Controls.Add(this.BtnAcNo1);
            this.Tp2.Controls.Add(this.TxtAcNo6);
            this.Tp2.Controls.Add(this.TxtAcDesc1);
            this.Tp2.Controls.Add(this.BtnAcNo6);
            this.Tp2.Controls.Add(this.label9);
            this.Tp2.Controls.Add(this.label12);
            this.Tp2.Controls.Add(this.BtnAcNo2);
            this.Tp2.Controls.Add(this.TxtAcDesc5);
            this.Tp2.Controls.Add(this.TxtAcNo2);
            this.Tp2.Controls.Add(this.TxtAcNo5);
            this.Tp2.Controls.Add(this.TxtAcDesc2);
            this.Tp2.Controls.Add(this.BtnAcNo5);
            this.Tp2.Controls.Add(this.label10);
            this.Tp2.Controls.Add(this.label13);
            this.Tp2.Controls.Add(this.BtnAcNo3);
            this.Tp2.Controls.Add(this.TxtAcDesc4);
            this.Tp2.Controls.Add(this.TxtAcNo3);
            this.Tp2.Controls.Add(this.TxtAcNo4);
            this.Tp2.Controls.Add(this.TxtAcDesc3);
            this.Tp2.Controls.Add(this.BtnAcNo4);
            this.Tp2.Controls.Add(this.label11);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 167);
            this.Tp2.Text = "COA\'s Account Informations";
            // 
            // TxtAcDesc8
            // 
            this.TxtAcDesc8.EnterMoveNextControl = true;
            this.TxtAcDesc8.Location = new System.Drawing.Point(219, 355);
            this.TxtAcDesc8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc8.Name = "TxtAcDesc8";
            this.TxtAcDesc8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc8.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc8.Properties.MaxLength = 255;
            this.TxtAcDesc8.Properties.ReadOnly = true;
            this.TxtAcDesc8.Size = new System.Drawing.Size(513, 20);
            this.TxtAcDesc8.TabIndex = 45;
            // 
            // TxtAcNo8
            // 
            this.TxtAcNo8.EnterMoveNextControl = true;
            this.TxtAcNo8.Location = new System.Drawing.Point(219, 332);
            this.TxtAcNo8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo8.Name = "TxtAcNo8";
            this.TxtAcNo8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo8.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo8.Properties.MaxLength = 80;
            this.TxtAcNo8.Properties.ReadOnly = true;
            this.TxtAcNo8.Size = new System.Drawing.Size(166, 20);
            this.TxtAcNo8.TabIndex = 43;
            // 
            // BtnAcNo8
            // 
            this.BtnAcNo8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo8.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo8.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo8.Appearance.Options.UseBackColor = true;
            this.BtnAcNo8.Appearance.Options.UseFont = true;
            this.BtnAcNo8.Appearance.Options.UseForeColor = true;
            this.BtnAcNo8.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo8.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo8.Image")));
            this.BtnAcNo8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo8.Location = new System.Drawing.Point(388, 331);
            this.BtnAcNo8.Name = "BtnAcNo8";
            this.BtnAcNo8.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo8.TabIndex = 44;
            this.BtnAcNo8.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo8.ToolTipTitle = "Run System";
            this.BtnAcNo8.Click += new System.EventHandler(this.BtnAcNo8_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(47, 335);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(166, 14);
            this.label15.TabIndex = 42;
            this.label15.Text = "COA\'s Account (Inter Office)";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc7
            // 
            this.TxtAcDesc7.EnterMoveNextControl = true;
            this.TxtAcDesc7.Location = new System.Drawing.Point(219, 309);
            this.TxtAcDesc7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc7.Name = "TxtAcDesc7";
            this.TxtAcDesc7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc7.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc7.Properties.MaxLength = 255;
            this.TxtAcDesc7.Properties.ReadOnly = true;
            this.TxtAcDesc7.Size = new System.Drawing.Size(513, 20);
            this.TxtAcDesc7.TabIndex = 41;
            // 
            // TxtAcNo7
            // 
            this.TxtAcNo7.EnterMoveNextControl = true;
            this.TxtAcNo7.Location = new System.Drawing.Point(219, 286);
            this.TxtAcNo7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo7.Name = "TxtAcNo7";
            this.TxtAcNo7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo7.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo7.Properties.MaxLength = 80;
            this.TxtAcNo7.Properties.ReadOnly = true;
            this.TxtAcNo7.Size = new System.Drawing.Size(166, 20);
            this.TxtAcNo7.TabIndex = 39;
            // 
            // BtnAcNo7
            // 
            this.BtnAcNo7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo7.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo7.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo7.Appearance.Options.UseBackColor = true;
            this.BtnAcNo7.Appearance.Options.UseFont = true;
            this.BtnAcNo7.Appearance.Options.UseForeColor = true;
            this.BtnAcNo7.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo7.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo7.Image")));
            this.BtnAcNo7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo7.Location = new System.Drawing.Point(388, 285);
            this.BtnAcNo7.Name = "BtnAcNo7";
            this.BtnAcNo7.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo7.TabIndex = 40;
            this.BtnAcNo7.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo7.ToolTipTitle = "Run System";
            this.BtnAcNo7.Click += new System.EventHandler(this.BtnAcNo7_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtEntCode2);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.TxtEntCode);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtEntName);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 38);
            this.panel3.TabIndex = 9;
            // 
            // TxtEntCode2
            // 
            this.TxtEntCode2.EnterMoveNextControl = true;
            this.TxtEntCode2.Location = new System.Drawing.Point(82, 8);
            this.TxtEntCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEntCode2.Name = "TxtEntCode2";
            this.TxtEntCode2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEntCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEntCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEntCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtEntCode2.Properties.MaxLength = 16;
            this.TxtEntCode2.Size = new System.Drawing.Size(95, 20);
            this.TxtEntCode2.TabIndex = 15;
            this.TxtEntCode2.Visible = false;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(696, 7);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(65, 22);
            this.ChkActInd.TabIndex = 14;
            // 
            // FrmEntity2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmEntity2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntLogo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEntAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoCreate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkConsolidate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.Tp2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtEntName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtEntCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LueParent;
        private DevExpress.XtraEditors.TextEdit TxtEntLogo;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtEntFax;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtEntPhone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.MemoExEdit MeeEntAddress;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc1;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo1;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo1;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc2;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo2;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo2;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc4;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo4;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo4;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc3;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo3;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo3;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc6;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo6;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo6;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc5;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo5;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc7;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo7;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo7;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc8;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo8;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo8;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.TextEdit TxtShortCode;
        private DevExpress.XtraEditors.CheckEdit ChkAutoCreate;
        private DevExpress.XtraEditors.CheckEdit ChkConsolidate;
        internal DevExpress.XtraEditors.TextEdit TxtEntCode2;
    }
}