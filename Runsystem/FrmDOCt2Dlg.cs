﻿#region Update
/*
    16/08/2017 [WED] tambah informasi item jasa dari DR
    05/09/2018 [HAR] PL dengan Shipment Planning dengan status planning bisa keambil
    15/06/2020 [IBL/IMS] Tambah kolom SO Contract's Remark berdasarkan parameter IsSalesTransactionShowSOContractRemark
 *  01/07/2020 [HAR/IMS] feedback remark so contract ambil dari detail item SO Contract
    25/09/2020 [DITA/IMS] tambah informasi No dari SO Contract berdasarkan parameter IsDetailShowColumnNumber
    24/02/2021 [IBL/SIER] narik informasi customer category ke layar utama
    16/11/2021 [ICA/SIER] Filter customer belum berfungsi
 * 
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt2 mFrmParent;
        string mSQL = string.Empty;
        byte mDocType = 1;

        #endregion

        #region Constructor

        public FrmDOCt2Dlg(FrmDOCt2 FrmParent, byte DocType)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocType = DocType;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text += mDocType == 1 ? " Delivery Request" : " Packing List";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            if (mDocType == 1)
            {
                SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.CtCode, B.CtName, ");
                SQL.AppendLine("A.SAName, C.VdName, A.ExpDriver, A.ExpPlatNo, D.ItName As LogisticItName, ");
                SQL.AppendLine("t1.Remark SOContractRemark, T1.SOContractNo, E.CtCtName ");
                SQL.AppendLine("From TblDRHdr A ");
                if (mFrmParent.mIsSalesTransactionShowSOContractRemark)
                {
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    //SQL.AppendLine("    SELECT A.DocNo, C.Remark FROM tbldrhdr A ");
                    //SQL.AppendLine("    INNER JOIN tbldrdtl B ON A.DocNo = B.DocNo ");
                    //SQL.AppendLine("    INNER JOIN tblsocontracthdr C ON B.SODocNo = C.DocNo ");
                    //SQL.AppendLine("    GROUP BY C.DocNo ");
                    SQL.AppendLine("SELECT A.DocNo, group_concat(distinct C.Remark separator '#' ) As remark, C.No SOContractNo ");
                    SQL.AppendLine("FROM tbldrhdr A ");
                    SQL.AppendLine("INNER JOIN tbldrdtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("INNER JOIN tblsocontractDtl C ON B.SODocNo = C.DocNo And B.SODNo = C.DNo ");
                    SQL.AppendLine("GROUP BY C.DocNo ");
                    SQL.AppendLine(")t1 ON A.DocNo = t1.DocNo ");
                }
                else
                {
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT NULL as DocNo, NULL as Remark, NULL as SOContractNo ");
                    SQL.AppendLine(")t1 ON 0 = 0 ");
                }
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                SQL.AppendLine("Left Join TblVendor C On A.ExpVdCode=C.VdCode ");
                SQL.AppendLine("Left Join TblItem D On A.ItCode = D.ItCode ");
                SQL.AppendLine("Left Join TblCustomerCategory E On B.CtCtCode = E.CtCtCode ");
                SQL.AppendLine("Where A.CancelInd='N' ");
                SQL.AppendLine("And A.ProcessInd Not In ('C', 'M', 'F') ");
            }
            else
            {
                SQL.AppendLine("Select Distinct A.DocNo, A.DocDt, A.LocalDocNo, C.CtCode, D.CtName, ");
                SQL.AppendLine("E.SectionNo, F.CtCtName, ");

                SQL.AppendLine("Case E.SectionNo ");
                for (int i=1;i<=25;i++)
                    SQL.AppendLine("    When '"+i.ToString()+"' Then Cnt"+i.ToString());
                SQL.AppendLine(" End As Cnt, ");

                SQL.AppendLine("Case E.SectionNo ");
                for (int i = 1;i<=25;i++)
                    SQL.AppendLine("    When '" + i.ToString() + "' Then Seal" + i.ToString());
                SQL.AppendLine(" End As Seal ");

                SQL.AppendLine("From TblPLHdr A ");
                SQL.AppendLine("Inner Join TblSIHdr B On A.SIDocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblSP C ");
                SQL.AppendLine("    On B.SPDocNo=C.DocNo ");
                SQL.AppendLine("    And C.Status In ('R', 'F', 'P') ");
                SQL.AppendLine("Inner Join TblCustomer D On C.CtCode=D.CtCode ");
                SQL.AppendLine("Inner Join TblPLDtl E On A.DocNo=E.DocNo And E.ProcessInd<>'F' ");
                SQL.AppendLine("Left Join TblCustomerCategory F On D.CtCtCode = F.CtCtCode ");
                SQL.AppendLine("Where 0=0 ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            if (mDocType == 1)
            {
                Grd1.Cols.Count = 15;
                Grd1.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "DR#", 
                        "",
                        "Date",
                        "Local#",
                        "Customer Code",
                        
                        //6-10
                        "Customer",
                        "Shipping",
                        "Expedition",
                        "Driver",
                        "Vehicle Plat#",

                        //11-14
                        "Logistic",
                        "SO Contract's Remark",
                        "SO Contract's"+Environment.NewLine+"No",
                        "Customer Category"
                    },
                    new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        140, 20, 80, 130, 0, 
                    
                        //6-10
                        250, 250, 250, 150, 150, 

                        //11-14
                        250, 300, 100, 0
                    }
                );
                Sm.GrdColButton(Grd1, new int[] { 2 });
                Sm.GrdFormatDate(Grd1, new int[] { 3 });
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                Sm.GrdColInvisible(Grd1, new int[] { 2, 14 }, false);

                if (!mFrmParent.mIsSalesTransactionShowSOContractRemark)
                    Sm.GrdColInvisible(Grd1, new int[] { 12 }, false);
                if (!mFrmParent.mIsDetailShowColumnNumber) Sm.GrdColInvisible(Grd1, new int[] { 13 });

                Grd1.Cols[13].Move(1);
            }
            else
            {
                Grd1.Cols.Count = 12;
                Grd1.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "PL#", 
                        "",
                        "Date",
                        "Local#",
                        "Customer Code",
                        
                        //6-10
                        "Customer Category",
                        "Customer",
                        "Container",
                        "Seal",
                        "Section#"
                    },
                    new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        140, 20, 80, 130, 0, 
                    
                        //6-9
                        0, 250, 200, 200, 0
                    }
                );
                Sm.GrdColButton(Grd1, new int[] { 2 });
                Sm.GrdFormatDate(Grd1, new int[] { 3 });
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
                Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 11 }, false);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            if (mDocType == 1)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 2, 11 }, !ChkHideInfoInGrd.Checked);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            }
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                if (mDocType == 1)
                {
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                }
                else
                {
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);
                }

                if (mDocType == 1)
                {
                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                            new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "LocalDocNo", "CtCode", "CtName", "SAName", 
                            
                            //6-10
                            "VdName", "ExpDriver", "ExpPlatNo", "LogisticItName", "SOContractRemark",

                            //11-12
                            "SOContractNo", "CtCtName"
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            }, true, false, false, false
                        );
                }
                else
                {
                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                            new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "LocalDocNo", "CtCode", "CtCtName", "CtName",

                            //6-8
                            "Cnt", "Seal", "SectionNo"
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            }, true, false, false, false
                        );
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    mFrmParent.ClearData2();
                    int Row = Grd1.CurRow.Index;
                    string DocNo = Sm.GetGrdStr(Grd1, Row, 1);

                    if (mDocType == 1)
                    {
                        mFrmParent.TxtDRDocNo.EditValue = DocNo;
                        if (mFrmParent.mIsDOCtCopyLocalDocNoFromDRPL)
                            mFrmParent.TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                        mFrmParent.mCtCode = Sm.GetGrdStr(Grd1, Row, 5);
                        mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                        mFrmParent.TxtSAName.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                        mFrmParent.TxtExpVdCode.EditValue = Sm.GetGrdStr(Grd1, Row, 8);
                        mFrmParent.TxtExpDriver.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                        mFrmParent.TxtExpPlatNo.EditValue = Sm.GetGrdStr(Grd1, Row, 10);
                        mFrmParent.TxtCtCtCode.EditValue = Sm.GetGrdStr(Grd1, Row, 14);
                    }
                    else
                    {
                        mFrmParent.TxtPLDocNo.EditValue = DocNo;
                        if (mFrmParent.mIsDOCtCopyLocalDocNoFromDRPL)
                            mFrmParent.TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                        mFrmParent.mCtCode = Sm.GetGrdStr(Grd1, Row, 5);
                        mFrmParent.TxtCtCtCode.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                        mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                        mFrmParent.TxtCnt.EditValue = Sm.GetGrdStr(Grd1, Row, 8);
                        mFrmParent.TxtSeal.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                        mFrmParent.mSectionNo = Sm.GetGrdStr(Grd1, Row, 10);
                    }
                    mFrmParent.ShowSOInfo();
                    mFrmParent.ShowItemInfo();
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                if (mDocType == 1)
                {
                    var f = new FrmDR(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmPL(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (mDocType == 1)
                {
                    var f = new FrmDR(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmPL(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion

    }
}
