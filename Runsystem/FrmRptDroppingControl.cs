﻿#region Update
/*
    12/11/2019 [WED/VIR] new apps
    30/12/2019 [WED/YK] Requested Amount ambil dari Dropping Request yg masih draft
    03/02/2020 [WED/YK] judul kolom 'Payment Amount' diganti jadi 'Payment Priority'
    13/04/2020 [WED/YK] Request amount ambil dari Draft Amount
    23/06/2020 [DITA/YK] Ubah rumus outstanding amt untuk yk, param = mOutstandingAmtFormulaForDroppingControl
    10/07/2020 [VIN/YK] Hasil sinkronisasi "Description" VR dengan project code bisa tampil di Reporting Dropping Control
    13/07/2020 [VIN/YK] Feedback outstanding amount dikurangi dari VR tipe manual
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDroppingControl : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mDroppingControlRequestAmountCode = string.Empty,  // 1 : VIR, 2 : YK
            mOutstandingAmtFormulaForDroppingControl = string.Empty // 1 : VIR, 2 : YK
            ;

        #endregion

        #region Constructor

        public FrmRptDroppingControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueSiteCode(ref LueSiteCode);
                GetParameter();
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.ProjectName, IFNULL(C.ProjectCode2, C.ProjectCode) ProjectCode, F.SiteName, G.CtName, ");
            SQL.AppendLine("IFNULL(H.RequestAmt, 0.00) RequestAmt, IFNULL(H.RequestApprovedAmt, 0.00) RequestApprovedAmt, IFNULL(I.PaymentAmt, 0.00) PaymentAmt, ");
            SQL.AppendLine("IFNULL(I.VCPaymentAmt, 0.00)+IFNULL(J.VRAmt, 0.000) VCPaymentAmt,  ");
            if(mOutstandingAmtFormulaForDroppingControl == "2")
                SQL.AppendLine("(IFNULL(H.RequestApprovedAmt, 0.00) - (IFNULL(I.VCPaymentAmt, 0.00)+IFNULL(J.VRAmt, 0.000))) OutstandingAmt ");
            else
                SQL.AppendLine("(IFNULL(H.RequestAmt, 0.00) - (IFNULL(I.VCPaymentAmt, 0.00)+IFNULL(J.VRAmt, 0.000))) OutstandingAmt ");
            SQL.AppendLine("FROM TblLOPHdr A ");
            SQL.AppendLine("INNER JOIN TblBOQHdr B ON A.DocNo = B.LOPDocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractHdr C ON B.DocNo = C.BOQDocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractRevisionHdr D ON C.DocNo = D.SOCDocNo ");
            SQL.AppendLine("INNER JOIN TblProjectImplementationHdr E ON D.DocNo = E.SOContractDocNo AND E.Status = 'A' AND E.CancelInd = 'N' ");
            SQL.AppendLine("LEFT JOIN TblSite F ON A.SiteCode = F.SiteCode ");
            SQL.AppendLine("INNER JOIN TblCustomer G ON A.CtCode = G.CtCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    SELECT T.PRJIDocNo, SUM(T.Amt) RequestAmt, SUM(T.ApprovedAmt) RequestApprovedAmt ");
	        SQL.AppendLine("    FROM ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        SELECT PRJIDocNo, DraftAmt Amt, 0.00 AS ApprovedAmt ");
		    SQL.AppendLine("        FROM TblDroppingRequestHdr ");
		    SQL.AppendLine("        WHERE CancelInd = 'N' AND STATUS IN ('O', 'A') ");
            SQL.AppendLine("        And PRJIDocNo Is not Null ");
            //if (mDroppingControlRequestAmountCode == "2")
            //    SQL.AppendLine("        And ProcessInd = 'D' ");
		    SQL.AppendLine("        UNION ALL  ");
		    SQL.AppendLine("        SELECT PRJIDocNo, 0.00 As Amt, Amt AS ApprovedAmt ");
		    SQL.AppendLine("        FROM TblDroppingRequestHdr ");
		    SQL.AppendLine("        WHERE CancelInd = 'N' AND STATUS = 'A' ");
            SQL.AppendLine("        And PRJIDocNo Is not Null ");
            SQL.AppendLine("   ) T ");
            SQL.AppendLine("   GROUP BY T.PRJIDocNo ");
            SQL.AppendLine(") H ON E.DocNo = H.PRJIDocNo ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T1.PRJIDocNo, SUM(T2.Amt) PaymentAmt, IFNULL(SUM(T3.Amt), 0.00) VCPaymentAmt ");
            SQL.AppendLine("    FROM TblDroppingRequestHdr T1 ");
            SQL.AppendLine("    INNER JOIN TblDroppingPaymentHdr T2 ON T1.DocNo = T2.DRQDocNo AND T2.CancelInd = 'N' AND T2.Status = 'A' ");
            SQL.AppendLine("    LEFT JOIN TblVoucherHdr T3 ON T2.VoucherRequestDocNo = T3.VoucherRequestDocNo AND T3.CancelInd = 'N' ");
            SQL.AppendLine("    GROUP BY T1.PRJIDocNo ");
            SQL.AppendLine(") I ON E.DocNo = I.PRJIDocNo ");
            SQL.AppendLine("LEFT JOIN  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    SELECT A2.Description, SUM(A2.Amt) VRAmt ");
            SQL.AppendLine("    FROM  tblvoucherrequesthdr A1  ");
            SQL.AppendLine("    LEFT JOIN tblvoucherrequestdtl A2 ON A1.DocNo=A2.DocNo ");
            SQL.AppendLine("    WHERE A1.DocType='01' AND A1.VoucherDocNo IS NOT NULL ");
            SQL.AppendLine("    GROUP By A2.Description ");
            SQL.AppendLine(") J ON J.Description=C.ProjectCode2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Project Name",
                    "Project Code", 
                    "Site",
                    "Customer", 
                    
                    //6-10
                    "Requested Amount",
                    "Requested Amount"+Environment.NewLine+"(Approved)",
                    "Payment Priority",
                    "Payment Priority"+Environment.NewLine+"(Voucher)",
                    "Outstanding Amount"
                },
                new int[] 
                {
                    //0
                    20, 

                    //1-5
                    130, 250, 100, 150, 200,  
                    
                    //6-10
                    150, 150, 150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9, 10 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where A.Status = 'A' And A.CancelInd = 'N' ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", true);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, new string[] { "A.ProjectName", "C.ProjectCode", "C.ProjectCode2" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo",  
                        //1-5
                        "ProjectName", "ProjectCode", "SiteName", "CtName", "RequestAmt", 
                        //6-9
                        "RequestApprovedAmt", "PaymentAmt", "VCPaymentAmt", "OutstandingAmt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    }, true, false, false, false
                );
                Grd1.GroupObject.Add(4);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 7, 8, 9, 10 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mDroppingControlRequestAmountCode = Sm.GetParameter("DroppingControlRequestAmountCode");
            mOutstandingAmtFormulaForDroppingControl = Sm.GetParameter("OutstandingAmtFormulaForDroppingControl");

            if (mDroppingControlRequestAmountCode.Length == 0) mDroppingControlRequestAmountCode = "1";
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "LOP#");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        #endregion

        #endregion
    }
}
