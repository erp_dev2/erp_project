﻿#region Update
/*
    14/02/2018 [HAR] ambil data dari sales invoice yang baru, invoicetype = 3
    07/08/2018 [TKG] tambah informasi remark DO
    07/09/2018 [HAR] tambah ambil data dari sales invoice project doctype = 5 
    13/09/2019 [TKG] difilter berdasarkan otorisasi site
    25/09/2019 [WED/YK] Project Implementation ambil dari detail sales invoice
    21/01/2020 [TKG/SIER] berdasarkan parameter IsIncomingPaymentProjectSystemEnabled, incoming payment digunakan untuk modul project system
    23/09/2020 [VIN/SIER] bug: List of DO pada Sales Invoice based on DO tidak muncul
    22/01/2021 [VIN/KSM] lup ke SLI based on DOCt based DR-SC
 *  16/04/2021 [BRI/SIER] menambahkan field total amt based on parameter IsIncomingPaymentAmtUseCOAAmt
 *  21/06/2021 [VIN/SIER] menambahkan parameter IsDOCtAmtRounded
 *  09/08/2022 [MAU/SIER] Default unhide kolom Date dan loop
 *  09/08/2022 [SET/SIER] penyesuaian ChooseData() untuk Description VR
 *  18/08/2022 [TYO/PRODUCT] filter department berdasarkan parameter IsFilterByDept
 *  25/08/2022 [TYO/SIER] feedback menambahkan Or statement di query filter by dept
 *  13/09/2022 [VIN/SIER] BUG Filter Department
 *  03/01/2023 [ICA/MNET] mengurangi outstanding amount dengan amount PI yg di tarik di Net Of Payment
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIncomingPaymentDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmIncomingPayment mFrmParent;
        private string mSQL = string.Empty, mCtCode = string.Empty, mDocTypeSLI = string.Empty;
        private string mMInd = "N";
        private decimal mActivePeriod = 0;
        private string OnlineCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmIncomingPaymentDlg(FrmIncomingPayment FrmParent, string CtCode, string MInd, decimal ActivePeriod)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
            mMInd = MInd;
            mActivePeriod = ActivePeriod;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -180);
                SetGrd();
                SetSQL();
                OnlineCtCode = Sm.GetParameter("OnlineCtCode");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Document#",
                    "Date",
                    "", 
                    "Type",

                    //6-10
                    "Type",
                    "Currency",    
                    "Outstanding"+Environment.NewLine+"Amount",
                    "Due Date",
                    "Local Document",

                    //11-15
                    "CBD",
                    "DO's Remark",
                    "Project Code",
                    "Project Name",
                    "Customer PO#"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 130, 80, 20, 0, 
                    
                    //6-10
                    150, 60, 130, 100, 100,

                    //11-15
                    0, 400, 100, 200, 120
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1, 11 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 9 });
            Sm.GrdColInvisible(Grd1, new int[] {  5, 11 }, false);
            if (!mFrmParent.mIsIncomingPaymentShowDORemark)
                Sm.GrdColInvisible(Grd1, new int[] { 12 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From (");

            SQL.AppendLine("    Select A.DocNo, A.DocDt, '1' As InvoiceType, "); 
            SQL.AppendLine("case  ");
	        SQL.AppendLine("     when A.DocNo NOT LIKE '%SLIDO%' then 'Sales Invoice'  ");
            SQL.AppendLine("     ELSE 'Sales Invoice Based on DO (Based On Delivery Request-Sales Contract)' ");
	        SQL.AppendLine("end As InvoiceTypeDesc,  ");
            SQL.AppendLine("    A.CurCode, A.Amt-IfNull(B.Amt, 0)-IfNull(E.ARSAmt, 0) As Amt, A.DueDt, A.LocalDocNo, A.CBDInd, F.DOCtRemark, ");
            if (mFrmParent.mIsBOMShowSpecifications)
                SQL.AppendLine("    G.ProjectCode, G.ProjectName, G.PONo ");
            else
                SQL.AppendLine("    null as ProjectCode, null as ProjectName, null as PONo ");
	        SQL.AppendLine("    From TblSalesInvoiceHdr A ");
	        SQL.AppendLine("    Left Join ( ");
		    SQL.AppendLine("        Select T.InvoiceDocNo As DocNo, Sum(T.Amt) Amt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select T2.InvoiceDocNo, T2.Amt ");
			SQL.AppendLine("            From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("            Inner Join TblSalesInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo And T3.CtCode=@CtCode And IfNull(T3.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("            And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("            And T1.CtCode=@CtCode ");
            SQL.AppendLine("            Union ALL ");
            SQL.AppendLine("            Select T2.InvoiceDocNo, T2.Amt ");
            SQL.AppendLine("            FROM TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("            INNER JOIN TblNetOffPaymentDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            INNER JOIN TblPartnerEntity T3 ON T1.PartnerEntityCode = T3.PartnerEntityCode ");
            SQL.AppendLine("            Inner Join TblSalesInvoiceHdr T4 On T2.InvoiceDocNo=T4.DocNo AND T4.CtCode=@CtCode And IFNULL(T4.ProcessInd, 'O')<>'F'  ");
            SQL.AppendLine("            WHERE T1.CancelInd = 'N' ");
            SQL.AppendLine("                AND IFNULL(T1.Status, 'O') <> 'C' ");
            SQL.AppendLine("                AND T3.CtCode = @CtCode ");
            SQL.AppendLine("        )T Group By T.InvoiceDocNo ");
	        SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");
            if (mFrmParent.mIsUseActivePeriod && mCtCode == OnlineCtCode)
            {
                SQL.AppendLine("        Left Join TblSOHdr C On A.SODocNo = C.DocNo ");
                SQL.AppendLine("        Left Join MsiWeb.order_summaries D On C.LocalDocno = D.id  ");
            }
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select  A.SalesInvoiceDocNo, Sum(A.Amt) As ARSAmt ");
            SQL.AppendLine("        From TblARSHdr A  ");
            SQL.AppendLine("        Where A.CancelInd = 'N' ");
            SQL.AppendLine("        Group by A.SalesInvoiceDocNo ");
            SQL.AppendLine("    ) E On A.DocNo=E.SalesInvoiceDocNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T3.Remark Order By T2.DNo Separator ' ') As DOCtRemark ");
            SQL.AppendLine("    From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T3 On T2.DOCtDocNo=T3.DocNo And T3.Remark Is Not Null ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.ProcessInd, 'O')<>'F' ");
            if (mFrmParent.mIsUseMInd) SQL.AppendLine("    And T1.MInd=@MInd ");
            SQL.AppendLine("    And T1.CtCode=@CtCode ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine("    ) F On A.DocNo=F.DocNo ");

            if (mFrmParent.mIsBOMShowSpecifications)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct IfNull(T10.ProjectCode, T7.ProjectCode2)) ProjectCode, Group_Concat(Distinct IfNull(T10.ProjectName, T9.ProjectName)) ProjectName, Group_Concat(Distinct T7.PONo) PONo ");
                SQL.AppendLine("    From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl2 T3 On T2.DOCtDocNo = T3.DocNo And T2.DOCtDNo = T3.DNo ");
                SQL.AppendLine("    Inner Join TblDOCt2Hdr T4 On T3.DocNo = T4.DocNo ");
                SQL.AppendLine("    Inner Join TblDRHdr T5 On T4.DRDocNo = T5.DocNo ");
                SQL.AppendLine("    Inner Join TblDRDtl T6 On T4.DRDocNo = T6.DocNo And T3.DRDNo = T6.DNo ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T7 On T6.SODocNo = T7.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr T8 On T7.BOQDocNo = T8.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr T9 On T8.LOPDocNo = T9.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T10 On T9.PGCode = T10.PGCode ");
                SQL.AppendLine("    Group By T1.DocNo ");
                SQL.AppendLine(") G On A.DocNo = G.DocNo ");
            }

	        SQL.AppendLine("    Where A.CancelInd='N' ");
	        SQL.AppendLine("    And IfNull(A.ProcessInd, 'O')<>'F' ");
            if (mFrmParent.mIsUseMInd)
                SQL.AppendLine("    And A.MInd=@MInd ");
            if (mFrmParent.mIsUseActivePeriod && mCtCode == OnlineCtCode)
                SQL.AppendLine("    And time_to_sec(timediff(@Now, FROM_UNIXTIME(D.created))) <= @ActivePeriod ");
            SQL.AppendLine("    And A.CtCode=@CtCode ");

            if (mFrmParent.mIsFilterByDept)
            {
                //SQL.AppendLine("And Exists( ");
                //SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                //SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                //SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                //SQL.AppendLine("    ) OR A.DeptCode IS NULL ");
                SQL.AppendLine("And (A.DeptCode Is Null ");
                SQL.AppendLine("    Or (A.DeptCode Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        )))) ");
            }    

            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select A.DocNo, A.DocDt, '2' As InvoiceType, 'Sales Return Invoice' As InvoiceTypeDesc, ");
            SQL.AppendLine("    A.CurCode, A.TotalAmt-IfNull(B.Amt, 0) As Amt, Null As DueDt, Null As LocalDocNo, 'N' As CBDInd, Null As DOCtRemark, ");
            SQL.AppendLine("    null as ProjectCode, null as ProjectName, null as PONo ");
            SQL.AppendLine("    From TblSalesReturnInvoiceHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T.InvoiceDocNo As DocNo, Sum(T.Amt) Amt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select T2.InvoiceDocNo, T2.Amt ");
            SQL.AppendLine("            From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            SQL.AppendLine("            Inner Join TblSalesReturnInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo And T3.CtCode=@CtCode And IfNull(T3.IncomingPaymentInd, 'O')<>'F' ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("            And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("                And T1.CtCode=@CtCode ");
            SQL.AppendLine("            Union ALL ");
            SQL.AppendLine("            Select T2.InvoiceDocNo, T2.Amt ");
            SQL.AppendLine("            From TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("            INNER JOIN TblNetOffPaymentDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            INNER JOIN TblPartnerEntity T3 ON T1.PartnerEntityCode = T3.PartnerEntityCode ");
            SQL.AppendLine("            Inner Join TblSalesReturnInvoiceHdr T4 On T2.InvoiceDocNo=T4.DocNo And T4.CtCode=@CtCode And IfNull(T4.IncomingPaymentInd, 'O')<>'F' ");
            SQL.AppendLine("            WHERE T1.CancelInd = 'N' ");
            SQL.AppendLine("                AND IFNULL(T1.Status, 'O') <> 'C' ");
            SQL.AppendLine("                AND T3.CtCode = @CtCode ");
            SQL.AppendLine("        )T Group By T.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.IncomingPaymentInd<>'F' And A.CancelInd='N' ");
            SQL.AppendLine("    And A.CtCode=@CtCode ");

            if (mFrmParent.mIsFilterByDept)
            {
                //SQL.AppendLine("And Exists( ");
                //SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                //SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                //SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                //SQL.AppendLine("    ) OR A.DeptCode IS NULL ");
                SQL.AppendLine("And (A.DeptCode Is Null ");
                SQL.AppendLine("    Or (A.DeptCode Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        )))) ");
            }

            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select A.DocNo, A.DocDt, '3' As InvoiceType, 'Sales Invoice 2' As InvoiceTypeDesc, ");
            SQL.AppendLine("    A.CurCode, A.Amt-IfNull(B.Amt, 0)-IfNull(E.ARSAmt, 0) As Amt, A.DueDt, A.LocalDocNo, 'N' CBDInd, Null As DOCtRemark, ");
            SQL.AppendLine("    null as ProjectCode, null as ProjectName, null as PONo ");
            SQL.AppendLine("    From TblSalesInvoice2Hdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T.InvoiceDocNo As DocNo, Sum(T.Amt) Amt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select T2.InvoiceDocNo, T2.Amt ");
            SQL.AppendLine("            From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("            Inner Join TblSalesInvoice2Hdr T3 On T2.InvoiceDocNo=T3.DocNo And T3.CtCode=@CtCode And IfNull(T3.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("                And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("                And T1.CtCode=@CtCode ");
            SQL.AppendLine("            Union ALL ");
            SQL.AppendLine("            Select T2.InvoiceDocNo, T2.Amt ");
            SQL.AppendLine("            From TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblNetOffPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("            Inner Join TblSalesInvoice2Hdr T3 On T2.InvoiceDocNo=T3.DocNo And T3.CtCode=@CtCode And IfNull(T3.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("                And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("        )T Group By T.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select  A.SalesInvoiceDocNo, Sum(A.Amt) As ARSAmt ");
            SQL.AppendLine("        From TblARSHdr A  ");
            SQL.AppendLine("        Where A.CancelInd = 'N' ");
            SQL.AppendLine("        Group by A.SalesInvoiceDocNo ");
            SQL.AppendLine("    ) E On A.DocNo=E.SalesInvoiceDocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.ProcessInd, 'O')<>'F' ");
            if (mFrmParent.mIsUseActivePeriod && mCtCode == OnlineCtCode)
                SQL.AppendLine("    And time_to_sec(timediff(@Now, FROM_UNIXTIME(D.created))) <= @ActivePeriod ");
            SQL.AppendLine("    And A.CtCode=@CtCode ");

            SQL.AppendLine("Union ALL ");

            SQL.AppendLine("Select A.DocNo, A.DocDt, '5' As InvoiceType, 'Sales project' As InvoiceTypeDesc, "); 
            SQL.AppendLine("A.CurCode, A.Amt-IfNull(G.Amt, 0.00)-IfNull(H.ARSAmt, 0.00) As Amt, A.DueDt, A.LocalDocNo, 'N' CBDInd, Null As DOCtRemark, ");
            SQL.AppendLine("null as ProjectCode, null as ProjectName, null as PONo ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct T2.ProjectImplementationDocNo, T1.DocNo, T1.DocDt, T1.CurCode,  ");
            SQL.AppendLine("    T1.Amt, T1.DueDt, T1.LocalDocNo, T1.CancelInd, T1.ProcessInd, T1.CtCode ");
            SQL.AppendLine("    From TblSalesInvoice5Hdr T1 ");
            SQL.AppendLine("    Inner Join TblSalesInvoice5Dtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine(") A ");
            if (mFrmParent.mIsFilterBySite)
            {
                if (mFrmParent.mIsIncomingPaymentProjectSystemEnabled)
                {
                    SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.ProjectImplementationDocNo=B.DocNo ");
                    SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
                    SQL.AppendLine("Inner Join TblSOContractHdr D On C.SOCDocNo=D.DocNo ");
                    SQL.AppendLine("Inner Join TblBOQHdr E On D.BOQDocNo=E.DocNo ");
                    SQL.AppendLine("Inner Join TblLOPHdr F On E.LOPDocNo=F.DocNo ");
                    SQL.AppendLine("And (F.SiteCode Is Null Or ( ");
                    SQL.AppendLine("    F.SiteCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=IfNull(F.SiteCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine(")) ");
                }
            }
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.InvoiceDocNo As DocNo, Sum(T.Amt) Amt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo, T2.Amt ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='5' ");
            SQL.AppendLine("        Inner Join TblSalesInvoice5Hdr T3 On T2.InvoiceDocNo=T3.DocNo  And T3.CtCode=@CtCode And IfNull(T3.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("            And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("            And T1.CtCode=@CtCode ");
            SQL.AppendLine("        Union ALL ");
            SQL.AppendLine("        Select T2.InvoiceDocNo, T2.Amt ");
            SQL.AppendLine("        From TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblNetOffPaymentDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        INNER JOIN TblPartnerEntity T3 ON T1.PartnerEntityCode = T3.PartnerEntityCode ");
            SQL.AppendLine("        Inner Join TblSalesInvoice2Hdr T4 On T2.InvoiceDocNo=T4.DocNo AND T4.CtCode=@CtCode And IFNULL(T4.ProcessInd, 'O')<>'F'  ");
            SQL.AppendLine("        WHERE T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND IFNULL(T1.Status, 'O') <> 'C' ");
            SQL.AppendLine("            AND T3.CtCode = @CtCode ");
            SQL.AppendLine("    )T Group By T.InvoiceDocNo ");
            SQL.AppendLine(") G On A.DocNo=G.DocNo ");
            SQL.AppendLine("Left Join (  ");
	        SQL.AppendLine("    Select  A.SalesInvoiceDocNo, Sum(A.Amt) As ARSAmt  ");
	        SQL.AppendLine("    From TblARSHdr A   ");
	        SQL.AppendLine("    Where A.CancelInd = 'N'  ");
	        SQL.AppendLine("    Group by A.SalesInvoiceDocNo  ");
            SQL.AppendLine(") H On A.DocNo=H.SalesInvoiceDocNo  ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And IfNull(A.ProcessInd, 'O')<>'F' And A.CtCode=@CtCode ");

            SQL.AppendLine(") T Where Amt>0 And Locate(Concat('##', T.DocNo, T.InvoiceType, '##'), @SelectedInvoice)<1 ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SelectedInvoice", mFrmParent.GetSelectedInvoice());
                Sm.CmParam<String>(ref cm, "@MInd", mMInd);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@Now", Sm.GetValue("Select Concat(curdate(), ' ', Curtime()) "));
                Sm.CmParam<decimal>(ref cm, "@ActivePeriod", mActivePeriod);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By InvoiceType, DocDt, DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo",
                        //1-5
                        "DocDt", "InvoiceType", "InvoiceTypeDesc",  "CurCode", "Amt", 
                        //6-10
                        "DueDt", "LocalDocNo", "CBDInd", "DOCtRemark", "ProjectCode",
                        //11-13
                        "ProjectName", "PONo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        //Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        if (mFrmParent.mIsDOCtAmtRounded)
                            Grd.Cells[Row, 8].Value = decimal.Truncate(dr.GetDecimal(c[5]));
                        else
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);

                        if(mFrmParent.mVRDescForIncomingPaymentDetail.Length > 0)
                            mFrmParent.Grd1.Cells[Row1, 11].Value = mFrmParent.mVRDescForIncomingPaymentDetail + " " + Sm.GetGrdStr(Grd1, Row, 2);
                        else
                            mFrmParent.Grd1.Cells[Row1, 11].Value = null;

                        mFrmParent.ComputeGiroAmt();
                        mFrmParent.ComputeAmt();
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 9 });
                        Sm.SetGrdBoolValueFalse(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 12 });
                        mFrmParent.ComputeAmtSI();
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 sales invoice or sales return invoice document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 5);
            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(key,
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd1, Index, 5)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    mDocTypeSLI = Sm.GetValue("SELECT doctype FROM tblsalesinvoicedtl WHERE docno = @Param limit 1;", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
                if (mDocTypeSLI == "1")
                {
                    var f1 = new FrmSalesInvoice(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
                else
                {
                    string SLIDO = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    if (!SLIDO.Contains("SLIDO"))
                    {
                        var f1 = new FrmSalesInvoice3(mFrmParent.mMenuCode);
                        f1.Tag = mFrmParent.mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                        f1.ShowDialog();
                    }
                    else
                    {
                        var f1 = new FrmSalesInvoice8(mFrmParent.mMenuCode);
                        f1.Tag = mFrmParent.mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                        f1.ShowDialog();
                    }
                }
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "5"))
                {
                    var f1 = new FrmSalesInvoice5(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                //if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "2"))
                //{
                //    var f2 = new FrmSalesReturnInvoice(mFrmParent.mMenuCode);
                //    f2.Tag = mFrmParent.mMenuCode;
                //    f2.WindowState = FormWindowState.Normal;
                //    f2.StartPosition = FormStartPosition.CenterScreen;
                //    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                //    f2.ShowDialog();
                //}
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)

            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    mDocTypeSLI = Sm.GetValue("SELECT doctype FROM tblsalesinvoicedtl WHERE docno = @Param limit 1;", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
                    if (mDocTypeSLI == "1")
                    {
                        var f1 = new FrmSalesInvoice(mFrmParent.mMenuCode);
                        f1.Tag = mFrmParent.mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                        f1.ShowDialog();
                    }
                    else
                    {
                        string SLIDO = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                        if (!SLIDO.Contains("SLIDO"))
                        {
                            var f1 = new FrmSalesInvoice3(mFrmParent.mMenuCode);
                            f1.Tag = mFrmParent.mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                            f1.ShowDialog();
                        }
                        else
                        {
                            var f1 = new FrmSalesInvoice8(mFrmParent.mMenuCode);
                            f1.Tag = mFrmParent.mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                            f1.ShowDialog();
                        }

                    }
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "5"))
                {
                    var f1 = new FrmSalesInvoice5(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }


                //if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "2"))
                //{
                //    var f2 = new FrmSalesReturnInvoice(mFrmParent.mMenuCode);
                //    f2.Tag = mFrmParent.mMenuCode;
                //    f2.WindowState = FormWindowState.Normal;
                //    f2.StartPosition = FormStartPosition.CenterScreen;
                //    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                //    f2.ShowDialog();
                //}
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        #endregion

        #endregion
    }
}
