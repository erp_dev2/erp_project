﻿#region Update
/*
    16/12/2020 [WED/IMS] new apps (PI with approval)
    30/12/2020 [WED/IMS] tambah informasi Local Code, Specification, Local DocNo
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using System.Xml;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoice3Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmPurchaseInvoice3 mFrmParent;
        private string mSQL = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private string mDocTitle = string.Empty;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmPurchaseInvoice3Find(FrmPurchaseInvoice3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueVdCode(ref LueVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, K.TIN, K.VdName, K.Address, A.VdInvNo, Q.VdDONo, ");
            SQL.AppendLine("Case When A.TaxCode1 = 'PPN2' Then 'TaxCode1' ");
            SQL.AppendLine("When A.TaxCode2 = 'PPN2' Then 'TaxCode2' ");
            SQL.AppendLine("When A.TaxCode3 = 'PPN2' Then 'TaxCode3' ");
            SQL.AppendLine("ENd As WAPUFrom, ");
            SQL.AppendLine("Case When A.TaxCode1 = 'PPN2' Then ifnull((A.Amt*L.TaxRate*0.01), 0) ");
            SQL.AppendLine("When A.TaxCode2 = 'PPN2' Then ifnull((A.Amt*M.TaxRate*0.01), 0) ");
            SQL.AppendLine("When A.TaxCode3 = 'PPN2' Then ifnull((A.Amt*N.TaxRate*0.01), 0) ");
            SQL.AppendLine("ENd As WAPU, ");
            SQL.AppendLine("Case When A.TaxCode1 = 'PPN3' Then 'TaxCode1' ");
            SQL.AppendLine("When A.TaxCode2 = 'PPN3' Then 'TaxCode2' ");
            SQL.AppendLine("When A.TaxCode3 = 'PPN3' Then 'TaxCode3' ");
            SQL.AppendLine("ENd As NWAPUFrom, ");
            SQL.AppendLine("Case When A.TaxCode1 = 'PPN3' Then ifnull((A.Amt*L.TaxRate*0.01), 0) ");
            SQL.AppendLine("When A.TaxCode2 = 'PPN3' Then ifnull((A.Amt*M.TaxRate*0.01), 0) ");
            SQL.AppendLine("When A.TaxCode3 = 'PPN3' Then ifnull((A.Amt*N.TaxRate*0.01), 0) ");
            SQL.AppendLine("ENd As NWAPU, ");
            SQL.AppendLine("L.TaxName TaxName1, ifnull((A.Amt*L.TaxRate*0.01), 0) As PITaxAmt1, ");
            SQL.AppendLine("M.TaxName TaxName2, ifnull((A.Amt*M.TaxRate*0.01), 0) As PITaxAmt2, ");
            SQL.AppendLine("N.TaxName TaxName3, ifnull((A.Amt*N.TaxRate*0.01), 0) As PITaxAmt3, ");
            if (mFrmParent.mIsPITotalWithoutTaxInclDownpaymentEnabled)
                SQL.AppendLine("A.Amt+A.TaxAmt As Amt, ");
            else
                SQL.AppendLine("A.Amt+A.TaxAmt-A.Downpayment As Amt, ");
            SQL.AppendLine("B.RecvVdDocNo, G.ItCode, J.ItName, ");
            SQL.AppendLine("C.QtyPurchase, J.PurchaseUomCode, ");
            SQL.AppendLine("C.Qty2, J.InventoryUomCode2, ");
            SQL.AppendLine("C.Qty3, J.InventoryUomCode3, ");
            SQL.AppendLine("A.CurCode, I.UPrice, E.Discount, E.DiscountAmt, E.RoundingValue, O.DeptName, ");
            SQL.AppendLine("P.SiteName, ");
            SQL.AppendLine("Cast((((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue )*Case When IfNull(L.TaxRate, 0)=0 Then 0 Else L.TaxRate/100 End)AS DECIMAL(18,6)) As TaxAmt1, ");
            SQL.AppendLine("Cast((((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue )*Case When IfNull(M.TaxRate, 0)=0 Then 0 Else M.TaxRate/100 End)AS DECIMAL(18,6)) As TaxAmt2, ");
            SQL.AppendLine("Cast((((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue )*Case When IfNull(N.TaxRate, 0)=0 Then 0 Else N.TaxRate/100 End)AS DECIMAL(18,6)) As TaxAmt3,  ");
            SQL.AppendLine("Cast(( ");
            SQL.AppendLine("    ((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue) + ");
            SQL.AppendLine("    (((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue)*Case When IfNull(L.TaxRate, 0)=0 Then 0 Else L.TaxRate/100 End) + ");
            SQL.AppendLine("    (((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue)*Case When IfNull(M.TaxRate, 0)=0 Then 0 Else M.TaxRate/100 End) + ");
            SQL.AppendLine("    (((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue)*Case When IfNull(N.TaxRate, 0)=0 Then 0 Else N.TaxRate/100 End)  ");
            SQL.AppendLine(")AS DECIMAL(18,6)) As Total, A.TaxRateAmt, A.PaymentDt,  ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, J.ForeignName, A.TaxCode1, A.TaxCode2, A.TaxCode3, B.FileName, ");
            if(mFrmParent.mIsBOMShowSpecifications)
                SQL.AppendLine("R.ProjectCode, R.ProjectName, R.PONo ");
            else
                SQL.AppendLine("Null As ProjectCode,Null As ProjectName, Null As NTPDocNo, Null As PONo ");
            SQL.AppendLine(", Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' Else '' End As StatusDesc, J.ItCodeInternal, J.Specification, A.LocalDocNo ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblRecvVdDtl C On B.RecvVdDocNo=C.DocNo And B.RecvVdDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblPOHdr D On C.PODocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl E On C.PODocNo=E.DocNo And C.PODNo=E.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl F On E.PORequestDocNo=F.DocNo And E.PORequestDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl G On F.MaterialRequestDocNo=G.DocNo And F.MaterialRequestDNo=G.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr H On F.QtDocNo=H.DocNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("    And H.PtCode Is Not Null ");
                SQL.AppendLine("    And H.PtCode In (");
                SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblQtDtl I On F.QtDocNo=I.DocNo And F.QtDNo=I.DNo ");
            SQL.AppendLine("Inner Join TblItem J On G.ItCode=J.ItCode ");
            SQL.AppendLine("Inner Join TblVendor K On A.VdCode=K.VdCode ");
            SQL.AppendLine("Left Join TblTax L On A.TaxCode1=L.TaxCode ");
            SQL.AppendLine("Left Join TblTax M On A.TaxCode2=M.TaxCode ");
            SQL.AppendLine("Left Join TblTax N On A.TaxCode3=N.TaxCode ");
            SQL.AppendLine("Left Join TblDepartment O On A.DeptCode=O.DeptCode ");
            SQL.AppendLine("Left Join TblSite P On A.SiteCode=P.SiteCode ");
            SQL.AppendLine("Inner Join TblRecvVdHdr Q On B.RecvVdDocNo=Q.DocNo ");
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("Select T0.DocNo, Group_Concat(Distinct IFNULL(T11.ProjectCode, T12.ProjectCode2)) ProjectCode,  ");
                SQL.AppendLine(" Group_Concat(Distinct IFNULL(T11.ProjectName, T10.ProjectName)) ProjectName,  Group_Concat(Distinct T12.PONo)PONo ");
                SQL.AppendLine("From TblPurchaseInvoiceHdr T0 ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl T1 ON T1.DocNo=T0.DocNo  ");
                SQL.AppendLine("Inner Join TblRecvVdDtl T2 ON T1.RecvVdDocNo=T2.DocNo AND T1.RecvVdDNo=T2.DNo  ");
                SQL.AppendLine("Inner Join TblPOHdr T3 ON T2.PODocNo=T3.DocNo  ");
                SQL.AppendLine("Inner Join TblPODtl T4 ON T2.PODocNo=T4.DocNo AND T2.PODNo=T4.DNo  ");
                SQL.AppendLine("Inner Join TblPORequestDtl T5 ON T4.PORequestDocNo=T5.DocNo AND T4.PORequestDNo=T5.DNo  ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl T6 ON T5.MaterialRequestDocNo=T6.DocNo AND T5.MaterialRequestDNo=T6.DNo  ");
                SQL.AppendLine("Inner Join TblBOMRevisionDtl T7 ON T7.DocNo = T6.BOMRDocNo AND T7.DNo = T6.BOMRDNo ");
                SQL.AppendLine("Inner JOIN TblBOMRevisionHdr T8 ON T7.DocNo = T8.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr T9 ON T8.BOQDocNo = T9.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr T10 ON T9.LOPDocNo = T10.DocNo  ");
                SQL.AppendLine("Left Join TblProjectGroup T11 ON T10.PGCode = T11.PGCode ");
                SQL.AppendLine("Left Join TblSOContractHdr T12 ON T12.BOQDocNo = T9.DocNo ");
                SQL.AppendLine("Group By T0.DocNo  ");    	
                SQL.AppendLine(")R On R.DocNo = A.DocNo ");
                            
            }
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or (A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 58;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "NPWP",
                        "Vendor",
                        //6-10
                        "Address",
                        "Vendor's"+Environment.NewLine+"Invoice#",
                        "WAPU Code",
                        "PPN"+Environment.NewLine+"WAPU",
                        "NWAPU Code",
                        //11-15
                        "PPN"+Environment.NewLine+"Non WAPU",
                        "Tax 1",
                        "Invoice's"+Environment.NewLine+"Tax 1",
                        "Tax 2",
                        "Invoice's"+Environment.NewLine+"Tax 2",
                        //16-20
                        "Tax 3",
                        "Invoice's"+Environment.NewLine+"Tax 3",
                        "Invoice"+Environment.NewLine+"Amount",
                        "Received#",
                        "Vendor's DO#",
                        //21-25
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Foreign Name",
                        "Quantity",
                        "UoM",
                        //26-30
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Currency",
                        //31-35
                        "Unit Price",
                        "Discount"+Environment.NewLine+"%",
                        "Discount"+Environment.NewLine+"Amount",
                        "Rounding"+Environment.NewLine+"Value",
                        "Item's"+Environment.NewLine+"Tax 1",
                        //36-40
                        "Item's"+Environment.NewLine+"Tax 2",
                        "Item's"+Environment.NewLine+"Tax 3",
                        "Amount",
                        "Payment Date",
                        "Department",
                        //41-45
                        "Site",
                        "",
                        "File Name",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        //46-50 
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",
                        "Tax"+Environment.NewLine+"Rate", 

                        //51-55
                        "Project Code",
                        "Project Name",
                        "Customer PO#",
                        "Status",
                        "Local Code",

                        //56-57
                        "Specification",
                        "Local Document#"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        150, 80, 80, 130, 200, 
                        //6-10
                        250, 130, 80, 130, 80, 
                        //11-15
                        130, 80, 130, 80, 130, 
                        //16-20
                        80, 130, 150, 130, 130, 
                        //21-25
                        80, 150, 130, 100, 80, 
                        //26-30
                        100, 80, 100, 80, 80,
                        //31-35
                        130, 100, 100, 100, 100, 
                        //36-40
                        100, 100, 100, 130, 120, 
                        //41-45
                        130, 20, 180, 100, 100,  
                        //46-50
                        100, 100, 100, 100, 100,
                        //51-55
                        120, 200, 120, 100, 120,
                        //56-57
                        250, 120
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 42 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11,13, 15, 17, 18, 24, 26, 28, 31, 32, 33, 34, 35, 36, 37, 38, 50 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 39, 45, 48 });
            Sm.GrdFormatTime(Grd1, new int[] { 46, 49 });
            if (mDocTitle == "KIM")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 8, 10, 23, 26, 27, 28, 29, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 }, false);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 23, 26, 27, 28, 29, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 }, false);
            }
            if (mFrmParent.mIsSiteMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 41 }, true);
            if (!mFrmParent.mIsShowForeignName)
                Grd1.Cols[23].Visible = true;
            if (mFrmParent.mIsPIAllowToUploadFile)
                Sm.GrdColInvisible(Grd1, new int[] { 42, 43 }, true);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 51, 52, 53 }, false);
            if (!mFrmParent.mIsPurchaseInvoiceUseApproval) Sm.GrdColInvisible(Grd1, new int[] { 54 });
            ShowInventoryUomCode();
            Grd1.Cols[57].Move(23);
            Grd1.Cols[56].Move(23);
            Grd1.Cols[55].Move(22);
            Grd1.Cols[54].Move(4);
            Grd1.Cols[50].Move(19);
            Grd1.Cols[51].Move(44);
            Grd1.Cols[52].Move(45);
            Grd1.Cols[53].Move(46);
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27 }, true);
                
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 28, 29 }, true);                
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  44, 45, 46, 47, 48, 49 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "G.ItCode", "J.ItName", "J.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, TxtRecvDocNo.Text, "B.RecvVdDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtVdDONo.Text, "Q.VdDONo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CancelInd", "TIN", "VdName", "Address",
                        //6-10
                        "VdInvNo","WAPUFROM", "WAPU", "NWAPUFROM","NWAPU", 
                        //11-15
                        "TaxName1", "PITaxAmt1", "TaxName2", "PITaxAmt2", "TaxName3", 
                        //16-20
                        "PITaxAmt3",  "Amt", "RecvVdDocNo",  "VdDONo",   "ItCode", 
                        //21-25
                        "ItName", "ForeignName", "QtyPurchase", "PurchaseUomCode", "Qty2", 
                        //26-30
                        "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CurCode", "UPrice",
                        //31-35
                        "Discount", "DiscountAmt", "RoundingValue", "TaxAmt1", "TaxAmt2", 
                        //36-40
                        "TaxAmt3", "Total", "DeptName", "SiteName", "CreateBy", 
                        //41-45
                        "CreateDt", "LastUpBy", "LastUpDt", "TaxCode1", "TaxCode2", 
                        //46-50
                        "TaxCode3", "TaxRateAmt", "PaymentDt", "FileName", "ProjectCode",
                        //51-55
                        "ProjectName", "PONo", "StatusDesc", "ItCodeInternal", "Specification",
                        //56
                        "LocalDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);

                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        if (mDocTitle == "KIM")
                        {
                            if (Sm.DrStr(dr, c[44]) == "PPN2" || Sm.DrStr(dr, c[44]) == "PPN3")
                            {
                                Grd1.Cells[Row, 12].Value = string.Empty;
                                Grd1.Cells[Row, 13].Value = Sm.FormatNum(0,0);
                            }
                            else
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            }
                            if (Sm.DrStr(dr, c[45]) == "PPN2" || Sm.DrStr(dr, c[45]) == "PPN3")
                            {
                                Grd1.Cells[Row, 14].Value = string.Empty;
                                Grd1.Cells[Row, 15].Value = Sm.FormatNum(0, 0);
                            }
                            else
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            }
                            if (Sm.DrStr(dr, c[46]) == "PPN2" || Sm.DrStr(dr, c[46]) == "PPN3")
                            {
                                Grd1.Cells[Row, 16].Value = string.Empty;
                                Grd1.Cells[Row, 17].Value = Sm.FormatNum(0, 0);
                            }
                            else
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            }
                        }
                        else
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                        }
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);

                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 25);

                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 30);

                        Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 31);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 32);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 33);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 34);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 35);

                        Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 36); 
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 37);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 38);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 39);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 44, 40);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 45, 41);

                        Sm.SetGrdValue("T", Grd, dr, c, Row, 46, 41);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 47, 42);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 48, 43);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 49, 43);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 50, 47);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 39, 48);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 49);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 51, 50);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 52, 51);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 53, 52);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 54, 53);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 55, 54);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 56, 55);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 57, 56);
                    }, true, false, true, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void GetParameter()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length != 0) mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mDocTitle = Sm.GetParameter("DocTitle");
        }

        private void DownloadFileKu(string TxtFile)
        {
            DownloadFile(mFrmParent.mHostAddrForFTPClient, mFrmParent.mPortForFTPClient, TxtFile, mFrmParent.mUsernameForFTPClient, mFrmParent.mPasswordForFTPClient, mFrmParent.mSharedFolderForFTPClient);
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (TxtFile.Length > 0)
            {
                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    //Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

               
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
               

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                reader.Close();
                memStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }


        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 42 && Sm.GetGrdStr(Grd1, e.RowIndex, 43).Length != 0)
            {
                DownloadFileKu(Sm.GetGrdStr(Grd1, e.RowIndex, 43));
            }
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkRecvDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Received#");
        }

        private void TxtRecvDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtVdDONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdDONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor's DO#");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local#");
        }

        #endregion       

        #endregion
       
    }
}
