﻿#region Update
/*
    04/19/2017 [HAR] perubahan di printout dan aturan additinal discount 
    jika ada account kepala 4 yang normalnya di debet dia tidak berlaku rule debet di debet = +,  
    01/07/2017 [TKG] tambah kolom CBDInd pada saat save.
    03/11/2017 [WED] insert ke tabel baru, untuk menyimpan curcode dan excrate saat ada downpayment (hanya saat insert)
    03/11/2017 [WED] update ke tabel summary2 saat edit
    15/03/2021 [HAR] Mind default N berdasarkan parameter : MenuCodeForDocWithMInd
    12/04/2021 [ICA/KSM] tambah informasi customer category di setluectcode berdasarkan parameter IsCustomerComboShowCategory
    09/04/2021 [ICA/SIER] menambah kondisi di setluebankaccode yg muncul hanya bank active
    13/08/2021 [MYA/ALL] tambah pilihan tax berdasarkan parameter SalesInvoiceTaxCalculationFormula
    20/08/2021 [MYA/KSM] Minta penambahan field "Departement" pada menu Sales Invoice based on DO to Customer (Based on DR-SC)
    08/10/2021 [HAR/ALL] BUG saat function computeamt, kolom amt yg diambil tdk sesuai (harusnya 31 tapi 27 )
    18/02/2022 [TKG/GSS] merubah GetParameter() dan proses Insert Data
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
           mMenuCode = string.Empty, mAccessInd = string.Empty, mMInd = "N",
           mDocNo = string.Empty, //if this application is called from other application;
           mSalesInvoiceTaxCalculationFormula = string.Empty,
           mSLI2TaxCalculationFormat = string.Empty;
        internal FrmSalesInvoice2Find FrmFind;
        internal string mIsFormPrintOutInvoice;
        private bool 
            mIsAutoJournalActived = false,
            mIsDOCtAmtRounded = false,
            mIsSalesInvoice2TaxEnabled = false,
            mIsSalesInvoice3COANonTaxable = false,
            mIsSalesInvoice2UseDepartment = false,
            mIsCustomerComboShowCategory = false;
        private string
            mMainCurCode = string.Empty,
            mDueDtSIValue = string.Empty,
            mTaxCodeSIValue = string.Empty,
            mEmpCodeSI = string.Empty,
            mEmpCodeTaxCollector = string.Empty,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mTaxInvDocument = string.Empty,
            mDOTaxInfo = string.Empty
            ;
        internal bool
            mIsCustomerItemNameMandatory = false, 
            mIsEntityMandatory = false,
            mIsBOMShowSpecifications = false
            ;
        private decimal mAdditionalCostDiscAmt = 0m;
        iGCell fCell;
        bool fAccept;
        
        
        #endregion

        #region Constructor

        public FrmSalesInvoice2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Sales Invoice";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sl.SetLueTaxCode(ref LueTaxCode1);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);
                TcSalesInvoice3.SelectedTabPage = Tp1;
                TcSalesInvoice3.SelectedTabPage = Tp2;
                TcSalesInvoice3.SelectedTabPage = Tp3;
                TcSalesInvoice3.SelectedTabPage = Tp4;
                SetLueSPCode(ref LueSPCode);
                SetLueOptionCode(ref LueOption);
                LueOption.Visible = false;
                mIsFormPrintOutInvoice = Sm.GetParameter("FormPrintOutInvoice");
                SetGrd();

                if (mSalesInvoiceTaxCalculationFormula == "1") TcSalesInvoice3.TabPages.Remove(Tp4);

                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (!mIsSalesInvoice2UseDepartment)
                {
                    lblDeptCode.Visible = false;
                    LueDeptCode.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1
            Grd1.Cols.Count = 35;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "SO#",

                    //1-5
                    "SO DNo",
                    "SO Date",
                    "",
                    "Customer Quotation",
                    "",

                    //6-10
                    "Item's"+Environment.NewLine+"Code",
                    "",
                    "Item's Name",
                    "Packaging",
                    "Quantity"+Environment.NewLine+"Packaging",

                    //11-15
                    "Quantity",
                    "UoM",
                    "Price"+Environment.NewLine+"(Price List)",
                    "Discount"+Environment.NewLine+"%",
                    "Discount"+Environment.NewLine+"Amount",

                    //16-20   
                    "Price After"+Environment.NewLine+"Discount",
                    "Promo"+Environment.NewLine+"%",
                    "Price"+Environment.NewLine+"Before Tax",
                    "Tax"+Environment.NewLine+"%",
                    "Tax"+Environment.NewLine+"Amount",

                    //21-25   
                    "Price"+Environment.NewLine+"After Tax",
                    "Total",
                    "Delivery"+Environment.NewLine+"Date",
                    "Currency",
                    "Remark",

                    //26-30
                    "Payment day",
                    "Tax 1",
                    "Tax 2",
                    "Tax 3",
                    "Tax Amount"+Environment.NewLine+"(Sales)",

                    //31-34
                    "Amount"+Environment.NewLine+"(Before Tax)",
                    "TaxAmt1",
                    "TaxAmt2",
                    "TaxAmt3",
                },
                new int[] 
                {
                    //0
                    150, 

                    //1-5
                    20, 80, 20, 150, 20,   
                    
                    //6-10
                    100, 20, 200, 150, 100,   
                    
                    //11-15
                    80, 80, 100, 80, 100,  
                    
                    //16-20
                    100, 80, 100, 80, 100,     
                   
                    //21-25
                    100, 120, 100, 80, 400,

                    //26-30
                    80, 60, 60, 60, 150, 
                    
                    //31-34
                    150, 0, 0, 0 
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 3, 5, 7 });
            Sm.GrdColCheck(Grd1, new int[] { 27, 28, 29 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 23 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 30, 31, 32, 33, 34 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 6, 7, 14, 17, 19, 23, 26 }, false);

            if (mSalesInvoiceTaxCalculationFormula == "1") Sm.GrdColInvisible(Grd1, new int[] { 27, 28, 29, 30, 31, 32, 33, 34 });
            #endregion

            #region Grd2

            Grd2.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(Grd2, new string[] { "Currency", "Deposit Amount" }, new int[] { 100, 200 });
            Sm.GrdFormatDec(Grd2, new int[] { 1 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 11;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "",
                        "Debit"+Environment.NewLine+"Amount",
                        "",
                        //6-10
                        "Credit"+Environment.NewLine+"Amount",
                        "OptCode",
                        "Option",
                        "Remark",
                        ""
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 20, 100, 20, 
                        //6-10
                        100, 50, 150, 400, 20
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 3, 5, 10 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 7, 10 }, false);

            #endregion
        }


        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 14, 17, 19, 23 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, DteDocDt, LueCtCode, DteDueDt, TxtLocalDocNo, LueSPCode, TxtSODocNo,
                        LueTaxCode1, LueTaxCode2, LueTaxCode3, TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3,
                        TxtTaxInvDocument, DteTaxInvoiceDt, 
                        TxtTaxInvDocument2, LueCurCode, TxtDownpayment, LueBankAcCode, LueDeptCode, MeeRemark 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 27, 28, 29 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, DteDueDt, TxtLocalDocNo, TxtTaxInvDocument, 
                        LueTaxCode1, LueTaxCode2, LueTaxCode3,
                        LueCurCode, TxtDownpayment, MeeRemark,  LueBankAcCode, LueSPCode, LueDeptCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 27, 28, 29 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 8, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, LueCtCode, DteDueDt, TxtLocalDocNo, TxtSODocNo,
                 LueTaxCode1, LueTaxCode2, LueTaxCode3, 
                 TxtTaxInvDocument, LueCurCode, MeeRemark, LueSPCode,  LueBankAcCode,
                 LueOption, TxtTaxInvDocument2, DteTaxInvoiceDt, LueDeptCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { 
                TxtTotalAmt, TxtTotalTax, TxtDownpayment, TxtAmt,
                TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        #region Clear Grid

        private void ClearGrd()
        {
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
        }

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 30, 31, 32, 33, 34 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 1 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 6 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesInvoice2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                SetLueCtCode(ref LueCtCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Order by CreateDt limit 1"));
                ComputeAmtFromTax();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", "") == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string 
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesInvoice", "TblSalesInvoiceHdr"),
                JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalesInvoiceHdr(DocNo));
            cml.Add(SaveSalesInvoiceDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveSalesInvoiceDtl(DocNo, Row));
            if (Grd3.Rows.Count > 1)
            {
                cml.Add(SaveSalesInvoiceDtl2(DocNo));
                //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveSalesInvoiceDtl2(DocNo, Row));
            }

            if (decimal.Parse(TxtDownpayment.Text) != 0)
            {
                var lR = new List<Rate>();
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                decimal DP = 0m;

                SQL.AppendLine("Select CtCode, CurCode, ExcRate, Amt ");
                SQL.AppendLine("From TblCustomerDepositSummary2 ");
                SQL.AppendLine("Where CtCode = @CtCode ");
                SQL.AppendLine("And CurCode = @CurCode ");
                SQL.AppendLine("And Amt > 0 Order By CreateDt Asc; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                    Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CtCode",

                         //1-3
                         "CurCode",
                         "ExcRate",
                         "Amt"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lR.Add(new Rate()
                            {
                                CtCode = Sm.DrStr(dr, c[0]),

                                CurCode = Sm.DrStr(dr, c[1]),
                                ExcRate = Sm.DrDec(dr, c[2]),
                                Amt = Sm.DrDec(dr, c[3])
                            });
                        }
                    }
                    dr.Close();
                }

                if (lR.Count > 0)
                {
                    DP = decimal.Parse(TxtDownpayment.Text);
                    for (int i = 0; i < lR.Count; i++)
                    {
                        if (DP > 0)
                        {
                            cml.Add(SaveExcRate(ref lR, i, DocNo, DP));
                            DP = DP - lR[i].Amt;
                        }
                    }
                }

                cml.Add(SaveCustomerDeposit(DocNo));
            }

            //di remark karna update status SO CBD ada di DR
            //cml.Add(UpdateSOHDRProcessInd(DocNo, "N"));
            //cml.Add(UpdateSODTLProcessInd(DocNo, "N"));
           

            //cml.Add(SaveJournal(DocNo, JournalDocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsDteEmpty(DteDueDt, "Due date") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                (mIsSalesInvoice2UseDepartment && Sm.IsLueEmpty(LueDeptCode, "Department")) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsSOAlreadyCancelled() ||
                IsSOAlreadyFulfilled() ||
                IsCurrencyNotValid() ||
                IsDownpaymentNotValid() ||
                IsFacturNumberNotValid() ||
                IsTaxInvDocumentNotValid() ||
                IsJournalAmtNotBalanced();
        }

        private bool IsFacturNumberNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 20) > 0)
                    if (Sm.IsTxtEmpty(TxtTaxInvDocument, "Factur number", false)) return true;       
            }
            return false;
        }


        private bool IsTaxInvDocumentNotValid()
        {
            bool IsTaxable = TxtTaxInvDocument.Text.Length > 0;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                {
                    if (IsTaxable && Sm.GetGrdDec(Grd1, Row, 19) <= 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, 
                            "SO# : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + 
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine + 
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                            "Tax Amount : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 19), 0) + Environment.NewLine + Environment.NewLine +
                            "You can't choose nontaxable SO#."
                            );
                        return true;
                    }

                    if (!IsTaxable && Sm.GetGrdDec(Grd1, Row, 19) > 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "SO# : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine +
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                            "Tax Amount : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 19), 0) + Environment.NewLine + Environment.NewLine +
                            "You can't choose taxable DO#."
                            );
                        return true;
                    }
                }
            }
            return false;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 SO.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "SO list entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd3.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Additional cost, discount information entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                var AcType = string.Empty;

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd3, Row, 4) == 0m && Sm.GetGrdDec(Grd3, Row, 6) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount can't be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd3, Row, 4) != 0m && Sm.GetGrdDec(Grd3, Row, 6) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount both can't be bigger than 0.");
                        return true;
                    }
                    //if (Sm.GetGrdBool(Grd3, Row, 3))
                    //{
                    //    if (AcType.Length == 0) AcType = "D";
                    //}
                    //if (Sm.GetGrdBool(Grd3, Row, 5))
                    //{
                    //    if (AcType.Length == 0) AcType = "C";
                    //}
                }

                //if (AcType == "D")
                //{
                //    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                //    {
                //        if (Sm.GetGrdDec(Grd3, Row, 4) > 0m && !Sm.GetGrdBool(Grd3, Row, 3))
                //        {
                //            Sm.StdMsg(mMsgType.Warning,
                //                "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                //                "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                //                "You need to tick this account number (Debit).");
                //            return true;
                //        }
                //        if (Sm.GetGrdBool(Grd3, Row, 5))
                //        {
                //            Sm.StdMsg(mMsgType.Warning,
                //                "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                //                "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                //                "You need to un-tick this account number (Credit).");
                //            return true;
                //        }
                //    }
                //}

                //if (AcType == "C")
                //{
                //    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                //    {
                //        if (Sm.GetGrdDec(Grd3, Row, 6) > 0m && !Sm.GetGrdBool(Grd3, Row, 5))
                //        {
                //            Sm.StdMsg(mMsgType.Warning,
                //                "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                //                "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                //                "You need to tick this account number (Credit).");
                //            return true;
                //        }
                //        if (!Sm.GetGrdBool(Grd3, Row, 5))
                //        {
                //            Sm.StdMsg(mMsgType.Warning,
                //                "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                //                "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                //                "You need to un-tick this account number (Debit).");
                //            return true;
                //        }
                //    }
                //}
            }
            return false;
        }

        private bool IsSOAlreadyCancelled()
        {
            string Msg = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0)
                    {
                        Msg =
                            "SO# : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine + 
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine + Environment.NewLine;

                        if (IsSOAlreadyCancelled(Row))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Some items already cancelled.");
                            Sm.FocusGrd(Grd1, Row, 0);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsSOAlreadyCancelled(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From (Select A.DocNo, B.Qty ");
            SQL.AppendLine("From TblSOHdr A ");
            SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo) T Where Qty<Cast(@Qty As Decimal(12, 4))");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            return Sm.IsDataExist(cm);
        }

        private bool IsSOAlreadyFulfilled()
        {
            string Msg = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0)
                    {
                        Msg =
                            "SO# : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine + Environment.NewLine;

                        if (IsSOAlreadyFulfilled(Row))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "This document already fulfilled.");
                            Sm.FocusGrd(Grd1, Row, 2);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsSOAlreadyFulfilled(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblSODtl ");
            SQL.AppendLine("Where ProcessInd5='F' And DocNo=@DocNo And DNo=@DNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 1));
            return Sm.IsDataExist(cm);
        }

        private bool IsSOAlreadyProcessedDR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.SODocNo ");
            SQL.AppendLine("from TblDRhdr A ");
            SQL.AppendLine("Inner Join tblDrDtl B On A.DocNo = b.DocNo ");
            SQL.AppendLine("Where B.SoDocNo = @SODocNo And A.cancelInd = 'N' ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", TxtSODocNo.Text);
            

            string Msg = string.Empty;
            
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "SO have been processed to Delivery Request.");
                return true;
            }
            return false;
        }


        private bool IsCurrencyNotValid()
        {
            string CurCode = Sm.GetLue(LueCurCode);
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, Row, 24)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "One sales invoice# only allowed 1 currency type.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDownpaymentNotValid()
        {
            decimal Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (Downpayment == 0m) return false;

            //Recompute Deposit
            ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));

            //Get Currency
            decimal Deposit = 0m;
            string CurCode = Sm.GetLue(LueCurCode);

            //Get Deposit Amount Based on currency
            if (Grd2.Rows.Count > 0)
            {
                for (int row = 0; row < Grd2.Rows.Count - 1; row++)
                {
                    if (Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, row, 0)))
                    {
                        Deposit = Sm.GetGrdDec(Grd2, row, 1);
                        break;
                    }
                }
            }

            if (Downpayment > Deposit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Currency : " + CurCode + Environment.NewLine +
                    "Deposit Amount: " + Sm.FormatNum(Deposit, 0) + Environment.NewLine +
                    "Downpayment Amount: " + Sm.FormatNum(Downpayment, 0) + Environment.NewLine + Environment.NewLine +
                    "Downpayment is bigger than existing deposit."
                    );
                return true;
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) Debit += Sm.GetGrdDec(Grd3, Row, 4);
                if (Sm.GetGrdStr(Grd3, Row, 6).Length > 0) Credit += Sm.GetGrdDec(Grd3, Row, 6);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveCustomerDeposit(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, ");
            SQL.AppendLine("(Case @CancelInd When 'Y' Then '04' Else '03' End) As DocType, ");
            SQL.AppendLine("DocDt, CtCode, CurCode, (Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesInvoiceHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @CtCode, @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+((Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt), LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoiceHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSalesInvoiceHdr(DocNo, DocDt, CancelInd, ProcessInd, " +
                    "CBDInd, CtCode, DueDt, LocalDocNo, TaxInvDocument, CurCode, TotalAmt, " +
                    "TaxCode1, TaxCode2, TaxCode3, TaxAmt1, TaxAmt2, TaxAmt3, " + 
                    "TotalTax, Downpayment, Amt, MInd, BankAcCode, SalesName, JournalDocNo, " +
                    "JournalDocNo2, SODocNo, Remark, DeptCode, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', 'O', 'Y', @CtCode, @DueDt, @LocalDocNo, " +
                    "@TaxInvDocument, @CurCode, @TotalAmt, @TaxCode1, @TaxCode2, @TaxCode3, " +
                    "@TaxAmt1, @TaxAmt2, @TaxAmt3, @TotalTax, @Downpayment, @Amt, " +
                    "@MInd, @BankAcCode, (Select SpName From tblSalesPerson Where SpCode=@SPCode), " +
                    "Null, Null, @SODocNo, @Remark, @DeptCode, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvDocument", TxtTaxInvDocument.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", decimal.Parse(TxtTotalTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@Downpayment", decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "SODocNo", TxtSODocNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", decimal.Parse(TxtTaxAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoiceDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Invoice (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalesInvoiceDtl(DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ItCode, QtyPackagingUnit, Qty, UPriceBeforeTax, TaxRate, TaxAmt, UPriceAfterTax, ");
                        if (mSalesInvoiceTaxCalculationFormula != "1") SQL.AppendLine("TaxInd1, TaxInd2, TaxInd3, TaxAmt1, TaxAmt2, TaxAmt3, TaxAmtTotal, ");
                        SQL.AppendLine("CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", '1', @DOCtDocNo_" + r.ToString() +
                        ", @DOCtDNo_" + r.ToString() +
                        ", 'O', @ItCode_" + r.ToString() +
                        ", @QtyPackagingUnit_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @UPriceBeforeTax_" + r.ToString() +
                        ", @TaxRate_" + r.ToString() +
                        ", @TaxAmt_" + r.ToString() +
                        ", @UPriceAfterTax_" + r.ToString());

                    if (mSalesInvoiceTaxCalculationFormula != "1")
                        SQL.AppendLine(
                            ", @TaxInd1_" + r.ToString() +
                            ", @TaxInd2_" + r.ToString() +
                            ", @TaxInd3_" + r.ToString() +
                            ", @TaxAmt1_" + r.ToString() +
                            ", @TaxAmt2_" + r.ToString() +
                            ", @TaxAmt3_" + r.ToString() +
                            ", @TaxAmtTotal_" + r.ToString());

                    SQL.AppendLine(", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@DOCtDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                    Sm.CmParam<String>(ref cm, "@DOCtDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 11));
                    Sm.CmParam<Decimal>(ref cm, "@UPriceBeforeTax_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
                    Sm.CmParam<Decimal>(ref cm, "@TaxRate_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 19));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 20));
                    Sm.CmParam<Decimal>(ref cm, "@UPriceAfterTax_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 21));

                    if (mSalesInvoiceTaxCalculationFormula != "1")
                    {
                        Sm.CmParam<String>(ref cm, "@TaxInd1_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 27) ? "Y" : "N");
                        Sm.CmParam<String>(ref cm, "@TaxInd2_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 28) ? "Y" : "N");
                        Sm.CmParam<String>(ref cm, "@TaxInd3_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 29) ? "Y" : "N");
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt1_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 32));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 33));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 34));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmtTotal_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 30));
                    }
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "1");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;

        }

        //private MySqlCommand SaveSalesInvoiceDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSalesInvoiceDtl(DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ItCode, QtyPackagingUnit, Qty, UPriceBeforeTax, TaxRate, TaxAmt, UPriceAfterTax, ");

        //    if (mSalesInvoiceTaxCalculationFormula != "1")
        //        SQL.AppendLine("TaxInd1, TaxInd2, TaxInd3, TaxAmt1, TaxAmt2, TaxAmt3, TaxAmtTotal, ");

        //    SQL.AppendLine("CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @DocType, @DOCtDocNo, @DOCtDNo, 'O', @ItCode, @QtyPackagingUnit, @Qty, @UPriceBeforeTax, @TaxRate, @TaxAmt, @UPriceAfterTax, ");

        //    if (mSalesInvoiceTaxCalculationFormula != "1")
        //        SQL.AppendLine("@TaxInd1, @TaxInd2, @TaxInd3, @TaxAmt1, @TaxAmt2, @TaxAmt3, @TaxAmtTotal, ");

        //    SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@DocType", "1");
        //    Sm.CmParam<String>(ref cm, "@DOCtDocNo", Sm.GetGrdStr(Grd1, Row, 0));
        //    Sm.CmParam<String>(ref cm, "@DOCtDNo", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
        //    Sm.CmParam<Decimal>(ref cm, "@UPriceBeforeTax", Sm.GetGrdDec(Grd1, Row, 18));
        //    Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd1, Row, 19));
        //    Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd1, Row, 20));
        //    Sm.CmParam<Decimal>(ref cm, "@UPriceAfterTax", Sm.GetGrdDec(Grd1, Row, 21));

        //    if (mSalesInvoiceTaxCalculationFormula != "1")
        //    {
        //        Sm.CmParam<String>(ref cm, "@TaxInd1", Sm.GetGrdBool(Grd1, Row, 27) ? "Y" : "N");
        //        Sm.CmParam<String>(ref cm, "@TaxInd2", Sm.GetGrdBool(Grd1, Row, 28) ? "Y" : "N");
        //        Sm.CmParam<String>(ref cm, "@TaxInd3", Sm.GetGrdBool(Grd1, Row, 29) ? "Y" : "N");
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", Sm.GetGrdDec(Grd1, Row, 32));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", Sm.GetGrdDec(Grd1, Row, 33));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", Sm.GetGrdDec(Grd1, Row, 34));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmtTotal", Sm.GetGrdDec(Grd1, Row, 30));
        //    }
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveSalesInvoiceDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Invoice (Dtl2) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalesInvoiceDtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc, AcInd, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @OptAcDesc_" + r.ToString() +
                        ", @AcInd_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 6));
                    Sm.CmParam<String>(ref cm, "@OptAcDesc_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 7));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 9));
                    Sm.CmParam<String>(ref cm, "@AcInd_" + r.ToString(), Sm.GetGrdBool(Grd3, r, 10) ? "Y" : "N");
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveSalesInvoiceDtl2(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblSalesInvoiceDtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc,  AcInd,  Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @OptAcDesc,  @AcInd, @Remark, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 4));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@OptAcDesc", Sm.GetGrdStr(Grd3, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@AcInd", Sm.GetGrdBool(Grd3, Row, 10) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateSODTLProcessInd(string DocNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSODtl A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DOCtDocNo ");
            SQL.AppendLine("    And A.DNo=B.DOCtDNo ");
            SQL.AppendLine("    And B.DocType='1' ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("Set A.ProcessInd5=@ProcessIndNew ");
            SQL.AppendLine("Where A.ProcessInd5=@ProcessIndOld; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProcessIndOld", CancelInd=="N"?"O":"F");
            Sm.CmParam<String>(ref cm, "@ProcessIndNew", CancelInd=="N"?"F":"O");
            return cm;
        }

        private MySqlCommand UpdateSOHDRProcessInd(string DocNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOHdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr B ");
            SQL.AppendLine("    On A.DocNo=B.SODocNo ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("Set A.Status=@ProcessIndNew ");
            SQL.AppendLine("Where A.Status=@ProcessIndOld; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProcessIndOld", CancelInd == "N" ? "O" : "F");
            Sm.CmParam<String>(ref cm, "@ProcessIndNew", CancelInd == "N" ? "F" : "O");
            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, string JournalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Sales Invoice : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select C.AcNo, A.Amt As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
            SQL.AppendLine("        Inner Join TblCOA C On Concat(B.ParValue, A.CtCode)=C.AcNo ");
            SQL.AppendLine("        Where Exists(");
            SQL.AppendLine("            Select T1.DocNo From TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr T2 ");
            SQL.AppendLine("                On T1.DOCtDocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.PLDocNo Is Not Null ");
            SQL.AppendLine("            Where T1.DocNo=@DocNo Limit 1 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.AcNo, A.Amt As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
            SQL.AppendLine("        Inner Join TblCOA C On Concat(B.ParValue, A.CtCode)=C.AcNo ");
            SQL.AppendLine("        Where Exists(");
            SQL.AppendLine("            Select T1.DocNo From  TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr T2 ");
            SQL.AppendLine("                On T1.DOCtDocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.DRDocNo Is Not Null ");
            SQL.AppendLine("            Where T1.DocNo=@DocNo Limit 1 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.AcNo, 0 As DAmt, A.Amt As CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
            SQL.AppendLine("        Inner Join TblCOA C On Concat(B.ParValue, A.CtCode)=C.AcNo ");
            SQL.AppendLine("        Where Exists(");
            SQL.AppendLine("            Select T1.DocNo From TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr T2 ");
            SQL.AppendLine("                On T1.DOCtDocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.PLDocNo Is Not Null ");
            SQL.AppendLine("            Where T1.DocNo=@DocNo Limit 1 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.AcNo, 0 As DAmt, A.Amt As CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
            SQL.AppendLine("        Inner Join TblCOA C On Concat(B.ParValue, A.CtCode)=C.AcNo ");
            SQL.AppendLine("        Where Exists(");
            SQL.AppendLine("            Select T1.DocNo From TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr T2 ");
            SQL.AppendLine("                On T1.DOCtDocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.DRDocNo Is Not Null ");
            SQL.AppendLine("            Where T1.DocNo=@DocNo Limit 1 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.DocNo=@DocNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select AcNo, DAmt, CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceDtl2 Where DocNo=@DocNo ");

            SQL.AppendLine("    ) Tbl Group By AcNo  ");
            SQL.AppendLine(") T;  ");

            SQL.AppendLine("Update TblSalesInvoiceHdr Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where Exists( ");
            SQL.AppendLine("    Select DocNo From TblJournalHdr ");
            SQL.AppendLine("    Where DocNo=@JournalDocNo ");
            SQL.AppendLine(");");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            
            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesInvoiceHdr());

            if (decimal.Parse(TxtDownpayment.Text) != 0)
            {
                var lR = new List<Rate>();
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select B.CtCode, A.CurCode, A.ExcRate, A.Amt ");
                SQL.AppendLine("From TblSalesInvoiceDtl3 A ");
                SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("Where A.DocNo = @DocNo And B.CtCode = @CtCode ");
                SQL.AppendLine("And A.CurCode = @CurCode ");
                SQL.AppendLine("And A.Amt > 0 Order By A.CreateDt Desc; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                    Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CtCode",

                         //1-3
                         "CurCode",
                         "ExcRate",
                         "Amt"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lR.Add(new Rate()
                            {
                                CtCode = Sm.DrStr(dr, c[0]),

                                CurCode = Sm.DrStr(dr, c[1]),
                                ExcRate = Sm.DrDec(dr, c[2]),
                                Amt = Sm.DrDec(dr, c[3])
                            });
                        }
                    }
                    dr.Close();
                }

                if (lR.Count > 0)
                {
                    for (int i = 0; i < lR.Count; i++)
                    {
                        cml.Add(UpdateSummary2(ref lR, i));

                    }
                }

                cml.Add(SaveCustomerDeposit(TxtDocNo.Text));
            }

            //di remark karna update status SO CBD ada di DR
            //cml.Add(UpdateSOHDRProcessInd(TxtDocNo.Text, "Y"));
            //cml.Add(UpdateSODTLProcessInd(TxtDocNo.Text, "Y"));
            //cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToIncomingPayment() ||
                IsVoucherRequestPPNExisted()||
                IsSOAlreadyProcessedDR()
                ;
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblSalesInvoiceHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyProcessedToIncomingPayment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo From tblIncomingPaymentHdr A ");
            SQL.AppendLine("inner Join tblIncomingPaymentDtl B On A.DocNo = B.Docno ");
            SQL.AppendLine("Where B.InvoiceDocno = @DocNo And ");
            SQL.AppendLine("A.cancelInd = 'N'  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to incoming payment.");
                return true;
            }
            return false;
        }

        private bool IsVoucherRequestPPNExisted()
        {
            if (Sm.IsDataExist(
                "SELECT DocNo FROM TblSalesInvoiceHdr " +
                "WHERE DocNo=@Param AND VoucherRequestPPNDocNo IS NOT NULL;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Voucher Request PPN.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditSalesInvoiceHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Cancelling Sales Invoice : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select C.AcNo, 0 As DAmt, A.Amt As CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
            SQL.AppendLine("        Inner Join TblCOA C On Concat(B.ParValue, A.CtCode)=C.AcNo ");
            SQL.AppendLine("        Where Exists(");
            SQL.AppendLine("            Select T1.DocNo From TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr T2 ");
            SQL.AppendLine("                On T1.DOCtDocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.PLDocNo Is Not Null ");
            SQL.AppendLine("            Where T1.DocNo=@DocNo Limit 1 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.AcNo, 0 As DAmt, A.Amt As CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
            SQL.AppendLine("        Inner Join TblCOA C On Concat(B.ParValue, A.CtCode)=C.AcNo ");
            SQL.AppendLine("        Where Exists(");
            SQL.AppendLine("            Select T1.DocNo From  TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr T2 ");
            SQL.AppendLine("                On T1.DOCtDocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.DRDocNo Is Not Null ");
            SQL.AppendLine("            Where T1.DocNo=@DocNo Limit 1 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.AcNo, A.Amt As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
            SQL.AppendLine("        Inner Join TblCOA C On Concat(B.ParValue, A.CtCode)=C.AcNo ");
            SQL.AppendLine("        Where Exists(");
            SQL.AppendLine("            Select T1.DocNo From TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr T2 ");
            SQL.AppendLine("                On T1.DOCtDocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.PLDocNo Is Not Null ");
            SQL.AppendLine("            Where T1.DocNo=@DocNo Limit 1 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.AcNo, A.Amt As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
            SQL.AppendLine("        Inner Join TblCOA C On Concat(B.ParValue, A.CtCode)=C.AcNo ");
            SQL.AppendLine("        Where Exists(");
            SQL.AppendLine("            Select T1.DocNo From TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr T2 ");
            SQL.AppendLine("                On T1.DOCtDocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.DRDocNo Is Not Null ");
            SQL.AppendLine("            Where T1.DocNo=@DocNo Limit 1 ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.DocNo=@DocNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select AcNo, CAmt, DAmt ");
            SQL.AppendLine("        From TblSalesInvoiceDtl2 Where DocNo=@DocNo ");

            SQL.AppendLine("    ) Tbl Group By AcNo  ");
            SQL.AppendLine(") T;  ");

            SQL.AppendLine("Update TblSalesInvoiceHdr Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where Exists( ");
            SQL.AppendLine("    Select DocNo From TblJournalHdr ");
            SQL.AppendLine("    Where DocNo=@JournalDocNo ");
            SQL.AppendLine(");");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSalesInvoiceHdr(DocNo);
                ShowSalesInvoiceDtl(DocNo);
                ShowSalesInvoiceDtl2(DocNo);
                ShowSODtl(DocNo);
                ComputeAmt();
                ComputeAmtFromTax();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesInvoiceHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, CtCode, DueDt, LocalDocNo, TaxInvDocument, " +
                    "CurCode, TotalAmt, TotalTax, DownPayment, Amt, BankAcCode, SalesName, SODocNo, Remark, " +
                    "TaxCode1, TaxCode2, TaxCode3, DeptCode " +
                    "From TblSalesInvoiceHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt",  "CancelInd", "CtCode", "DueDt", "LocalDocNo", 
                        
                        //6-10
                        "TaxInvDocument", "CurCode", "TotalAmt", "TotalTax", "DownPayment", 
                        
                        //11-15
                        "Amt", "BankAcCode", "SalesName", "SODocNo", "Remark",
 
                        //16-19
                        "TaxCode1", "TaxCode2", "TaxCode3", "DeptCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y") ? true : false;
                        SetLueCtCode(ref LueCtCode);
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[4]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        TxtTaxInvDocument.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[7]));
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtTotalTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtDownpayment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                        Sl.SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[12]));
                        Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Where SPname  = '" + Sm.DrStr(dr, c[13]) + "'"));
                        TxtSODocNo.EditValue = Sm.DrStr(dr, c[14]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                        Sl.SetLueTaxCode(ref LueTaxCode1);
                        Sl.SetLueTaxCode(ref LueTaxCode2);
                        Sl.SetLueTaxCode(ref LueTaxCode3);
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[18]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[19]));

                    }, true
                );
        }

        private void ShowSalesInvoiceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.Dno, B.Doctype, B.DoctDocno, B.DOCtDno, D.DocDt, B.ItCode, C.ItCodeInternal, C.Specification, ");
            SQL.AppendLine("C.ItName, B.Qty QtyPackagingUnit, C.SalesuomCode As PackagingUnitUomCode, ");
            SQL.AppendLine("B.Qty, C.SalesUomCode As PriceUomCode, B.UpriceBeforeTax, ");
            if (mSalesInvoiceTaxCalculationFormula != "1")
                SQL.AppendLine("B.TaxInd1, B.TaxInd2, B.TaxInd3, B.TaxAmt1, B.TaxAmt2, B.TaxAmt3, B.TaxAmtTotal, ");
            else
                SQL.AppendLine("'N' As TaxInd1, 'N' As TaxInd2, 'N' As TaxInd3, 0.00 As TaxAmt1, 0.00 As TaxAmt2, 0.00 As TaxAmt3, 0.00 As TaxAmtTotal, ");
            SQL.AppendLine("B.TaxRate, B.TaxAmt, B.UPriceAfterTax, D.DocNoInternal, E.Remark, E.Qty2 ");
            SQL.AppendLine("From TblSalesInvoicehdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblDOCtHdr D On B.DOCtDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblDoctDtl E ON D.DocNo = E.DocNo AND B.DOCtDNo = E.DNo");
            SQL.AppendLine("Where B.DocType = '3' And A.DocNo=@DocNo; ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "DOCtDocNo", "DOCtDNo", "DocDt", "ItCode", "ItName", 
                    
                    //6-10
                    "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", "PriceUomCode", "UPriceBeforeTax", 
                    
                    //11-15
                    "TaxRate", "TaxAmt", "UPriceAfterTax", "DocType", "DocNoInternal",

                    //16-20
                    "ItCodeInternal", "Specification", "Remark", "TaxInd1", "TaxInd2", 
                    
                    //21-25
                    "TaxInd3", "TaxAmtTotal", "TaxAmt1", "TaxAmt2", "TaxAmt3",
                    
                    //26
                    "Qty2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    //Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 4);
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 5);
                    //Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 6);
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 7);
                    //Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 8);
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 9);
                    //Grd.Cells[Row, 22].Value = Sm.GetLue(LueCurCode);
                    //Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 10);
                    //Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 11);
                    //Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 12);
                    //Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 13);
                    //if (mIsDOCtAmtRounded)
                    //    Grd.Cells[Row, 27].Value = decimal.Truncate(dr.GetDecimal(c[8]) * dr.GetDecimal(c[13]));
                    //else
                    //    Grd.Cells[Row, 27].Value = dr.GetDecimal(c[8]) * dr.GetDecimal(c[13]);
                    //Grd.Cells[Row, 28].Value = 0m;
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 14);
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 15);
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 16);
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 17);
                    //Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 18);
                    if (mSalesInvoiceTaxCalculationFormula != "1")
                    {
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 27, 19);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 28, 20);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 29, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 22);
                        //if (mSalesBasedDOQtyIndex == "1")
                        //    Grd.Cells[Row, 42].Value = dr.GetDecimal(c[8]) * dr.GetDecimal(c[10]);
                        //else
                        //    Grd.Cells[Row, 42].Value = dr.GetDecimal(c[26]) * dr.GetDecimal(c[10]);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 23);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 25);
                    }
                }, false, false, true, false
            );
            //Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 18, 20, 23, 24, 25, 26, 27, 28 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSalesInvoiceDtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, A.OptAcDesc, C.OptDesc, A.Remark ");
            SQL.AppendLine("From TblSalesInvoiceDtl2 A ");
            SQL.AppendLine("Inner Join TblCOA B ");
            SQL.AppendLine("Left Join TblOption C On A.OptAcDesc = C.OptCode And OptCat='AccountDescriptionOnSalesInvoice'  ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "AcInd", "DAmt", "CAmt", "OptAcDesc",
                    "OptDesc", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    if (Sm.GetGrdBool(Grd3, Row, 10).ToString() == "True")
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        }
                        else
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 2);
                        }
                    }
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowCustomerDepositSummary(string CtCode)
        {
            ClearGrd2();

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select CurCode, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary ");
            SQL.AppendLine("Where CtCode=@CtCode Order By CurCode;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        public void ShowSODtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, (UPriceBefore+TaxAmt) As UPriceAfterTax, (UPriceBefore+TaxAmt)*Qty As Total ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("   Select A.DocNo, B.Dno, A.DocDt, A.CtQtDocNo, E.ItCode, G.ItName, ");
            SQL.AppendLine("   B.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty, D.PriceUomCode, ");
            SQL.AppendLine("   Round(E.UPrice, 2) UPrice, C.Discount, ifnull((E.UPrice*C.Discount *0.01), 0) As DiscountAmt, ");
            SQL.AppendLine("   Round(C.UPrice, 2) As UPriceAfterDiscount, ");
            SQL.AppendLine("   IfNull(F.DiscRate, 0) As PromoRate,  ");
            SQL.AppendLine("   Round((C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0))), 2) As UPriceBefore, ");
            SQL.AppendLine("   B.TaxRate,  ");
            SQL.AppendLine("   Round(((C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0)))*0.01*B.TaxRate), 2) As TaxAmt, ");
            SQL.AppendLine("   B.DeliveryDt, B.Remark, A.CurCode, I.PtDay, ");
            if (TxtDocNo.Text.Length > 0 && mSalesInvoiceTaxCalculationFormula != "1")
                SQL.AppendLine("   J.TaxInd1, J.TaxInd2, J.TaxInd3, J.TaxAmtTotal, J.TaxAmt1, J.TaxAmt2, J.TaxAmt3 ");
            else
                SQL.AppendLine("   'N' As TaxInd1, 'N' As TaxInd2, 'N' As TaxInd3, 0.00 As TaxAmtTotal, 0.00 TaxAmt1, 0.00 TaxAmt2, 0.00 TaxAmt3 ");
            SQL.AppendLine("   From TblSOHdr A   ");
            SQL.AppendLine("   Inner Join TblSODtl B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("   Inner Join TblCtQtHdr B2 On A.CtQtDocNo=B2.DocNo  ");
            SQL.AppendLine("   Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
            SQL.AppendLine("   Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DocNo ");
            SQL.AppendLine("   Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DocNo And C.ItemPriceDNo=E.DNo ");
            SQL.AppendLine("   Left Join TblSOQuotPromoItem F On A.SOQuotPromoDocNo=F.DocNo And E.ItCode=F.ItCode  ");
            SQL.AppendLine("   Inner Join TblItem G On E.ItCode=G.ItCode ");
            SQL.AppendLine("   Left Join TblAgent H On B.AgtCode=H.AgtCode ");
            SQL.AppendLine("   Left Join TblPaymentTerm I On B2.PtCode=I.PtCode ");
            if (TxtDocNo.Text.Length > 0)
            {
                SQL.AppendLine("   Inner Join TblSalesInvoiceDtl J On B.DocNo = J.DoCtDocNo And B.Dno = J.DOCtDno ");
                SQL.AppendLine("   Where J.DocNo = @DocNo  ");
            }
            else
            {
                SQL.AppendLine("   Where A.DocNo = @DocNo  ");
                SQL.AppendLine("   And Concat(B.DocNo, B.Dno) Not In (Select Concat(DOctDocNo,DOCtDno) As keyDocNo From TblSalesInvoicehdr A Inner Join TblSalesInvoiceDtl B On A.DocNo=B.DocNo Where A.SODocNo Is Not Null And CancelInd = 'N') ");
            }
            SQL.AppendLine(") T Order By DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    //1-5
                    "Dno", "DocDt", "CtQtDocNo", "ItCode", "ItName",  
                    //6-10
                    "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", "PriceUomCode", "UPrice",  
                    //11-15
                    "Discount", "DiscountAmt", "UPriceAfterDiscount", "PromoRate",  "UPriceBefore",  
                    //16-20
                    "TaxRate" , "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt", 
                    //21-25
                    "CurCode", "Remark", "PtDay", "TaxInd1", "TaxInd2", 
                    //26-30
                    "TaxInd3", "TaxAmtTotal", "TaxAmt1", "TaxAmt2", "TaxAmt3",

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);

                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);

                    if (mSalesInvoiceTaxCalculationFormula != "1")
                    {
                        Grd.Cells[Row, 27].Value = Sm.DrStr(dr, c[24]) == "Y";
                        Grd.Cells[Row, 28].Value = Sm.DrStr(dr, c[25]) == "Y";
                        Grd.Cells[Row, 29].Value = Sm.DrStr(dr, c[26]) == "Y";
                        Grd.Cells[Row, 30].Value = Sm.DrDec(dr, c[27]);
                        Grd.Cells[Row, 31].Value = Sm.DrDec(dr, c[8]) * Sm.DrDec(dr, c[15]);
                        Grd.Cells[Row, 32].Value = Sm.DrDec(dr, c[28]);
                        Grd.Cells[Row, 33].Value = Sm.DrDec(dr, c[29]);
                        Grd.Cells[Row, 34].Value = Sm.DrDec(dr, c[30]);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 30, 31, 32, 33, 34 });
            Sm.FocusGrd(Grd1, 0, 1);
          
        }
   
        #endregion

        #region Additional Method

        private decimal GetTaxRate(string TaxCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select TaxRate from TblTax Where TaxCode=@TaxCode;"
            };
            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);
            string TaxRate = Sm.GetValue(cm);

            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }


        private void GetParameter()
        {
            string MenuCodeForDocWithMInd = string.Empty;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");            
            SQL.AppendLine("'SalesInvoiceTaxCalculationFormula', 'SLI3TaxCalculationFormat', 'MenuCodeForDocWithMInd', 'IsAutoJournalActived', 'IsBOMShowSpecifications', ");
            SQL.AppendLine("'IsDOCtAmtRounded', 'IsCustomerComboShowCategory', 'IsSalesInvoice2UseDepartment', 'IsCustomerItemNameMandatory', 'IsEntityMandatory', ");
            SQL.AppendLine("'IsSalesInvoice3COANonTaxable', 'IsSalesInvoice2TaxEnabled' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsSalesInvoice3COANonTaxable": mIsSalesInvoice3COANonTaxable = ParValue == "Y"; break;
                            case "IsSalesInvoice2TaxEnabled": mIsSalesInvoice2TaxEnabled = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsCustomerItemNameMandatory": mIsCustomerItemNameMandatory = ParValue == "Y"; break;
                            case "IsSalesInvoice2UseDepartment": mIsSalesInvoice2UseDepartment = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsCustomerComboShowCategory": mIsCustomerComboShowCategory = ParValue == "Y"; break;
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;

                            //string
                            case "SalesInvoiceTaxCalculationFormula": mSalesInvoiceTaxCalculationFormula = ParValue; break;
                            case "SLI3TaxCalculationFormat": mSLI2TaxCalculationFormat = ParValue; break;
                            case "MenuCodeForDocWithMInd": MenuCodeForDocWithMInd = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }


            if (mSLI2TaxCalculationFormat.Length == 0) mSLI2TaxCalculationFormat = "1";
            if (mSalesInvoiceTaxCalculationFormula.Length == 0) mSalesInvoiceTaxCalculationFormula = "1";
            if (MenuCodeForDocWithMInd.Length > 0)
                mMInd = MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1 ? "Y" : "N";
            
        }

        private MySqlCommand SaveExcRate(ref List<Rate> lR, int i, string DocNo, decimal DP)
        {
            var SQL1 = new StringBuilder();

            decimal x = ((DP - lR[i].Amt) >= 0) ? lR[i].Amt : DP;

            SQL1.AppendLine("Select @DP:=(Case @CancelInd When 'Y' Then -1 Else 1 End * " + x + "); ");

            SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt-@DP ");
            SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            SQL1.AppendLine("Insert Into TblSalesInvoiceDtl3(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @DocNo, @CurCode, @ExcRate, @DP, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt-@DP, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
            Sm.CmParam<String>(ref cm1, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand UpdateSummary2(ref List<Rate> lR, int i)
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt + @Amt, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
            Sm.CmParam<Decimal>(ref cm1, "@Amt", lR[i].Amt);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union ALL Select 'All' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void LueRequestEdit(
        iGrid Grd,
        DevExpress.XtraEditors.LookUpEdit Lue,
        ref iGCell fCell,
        ref bool fAccept,
        TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueCtCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct  C.CtCode As Col1, ");
                if (mIsCustomerComboShowCategory)
                    SQL.AppendLine("Concat(C.Ctname, ' [', IfNull(D.CtCtName, ''), '] ')  As Col2 ");
                else
                    SQL.AppendLine("C.Ctname  As Col2 ");
                SQL.AppendLine("From TblSOHdr A ");
                SQL.AppendLine("Inner Join TblCtQthdr B On A.CtQtDocNo = b.DoCno ");
                SQL.AppendLine("And B.PtCode = (Select ParValue From TblParameter Where ParCode ='CBDCode') ");
                SQL.AppendLine("Inner Join TblCustomer C on A.CtCode = C.CtCode ");
                SQL.AppendLine("Left Join TblCustomerCategory D On C.CtCtCode = D.CtCtCode ");
                SQL.AppendLine("Where A.Status = 'O' And A.CancelInd = 'N' ");
                SQL.AppendLine("Order By C.CtName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueOptionCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='AccountDescriptionOnSalesInvoice' ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeAmtFromTax()
        {
            ComputeAmt();
            //ComputeTotalWithTax(); // remark by wed. kalkulasi tax masuk ke ComputeAmt() dengan method baru ComputeTax(Amt), dimiripkan dengan SalesInvoice ComputeAmt()
        }

        private void ComputeTaxPerDetail(bool IsFromDetail, int Row)
        {
            if (IsFromDetail) // perubahan dari detail yg di tick/untick
            {
                ComputeTaxPerDetail2(Row); 
            }
            else
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    ComputeTaxPerDetail2(i);
                }
            }

            ComputeAmt();
        }

        private void ComputeTaxPerDetail2(int Row)
        {
            decimal TaxAmt = 0m;
            decimal Qty = Sm.GetGrdDec(Grd1, Row, 11);
            decimal UPriceBeftax = Sm.GetGrdDec(Grd1, Row, 18);

            Grd1.Cells[Row, 32].Value = 0m;
            Grd1.Cells[Row, 33].Value = 0m;
            Grd1.Cells[Row, 34].Value = 0m;

            //if (Sm.GetGrdBool(Grd1, Row, 27) && Sm.GetLue(LueTaxCode1).Length > 0)
            if (Sm.GetGrdBool(Grd1, Row, 27) && Sm.GetLue(LueTaxCode1).Length > 0)
            {
                decimal Amt = (Qty * UPriceBeftax) * GetTaxRate(Sm.GetLue(LueTaxCode1)) * 0.01m;
                TaxAmt += Amt;
                Grd1.Cells[Row, 32].Value = Amt;
            }
            if (Sm.GetGrdBool(Grd1, Row, 28) && Sm.GetLue(LueTaxCode2).Length > 0)
            {
                decimal Amt = (Qty * UPriceBeftax) * GetTaxRate(Sm.GetLue(LueTaxCode2)) * 0.01m;
                TaxAmt += Amt;
                Grd1.Cells[Row, 33].Value = Amt;
            }
            if (Sm.GetGrdBool(Grd1, Row, 29) && Sm.GetLue(LueTaxCode3).Length > 0)
            {
                decimal Amt = (Qty * UPriceBeftax) * GetTaxRate(Sm.GetLue(LueTaxCode3)) * 0.01m;
                TaxAmt += Amt;
                Grd1.Cells[Row, 34].Value = Amt;
            }

            Grd1.Cells[Row, 30].Value = Sm.FormatNum(TaxAmt, 0);
        }

        private void ComputeTax(decimal Amt)
        {
            string
                TaxCode1 = Sm.GetLue(LueTaxCode1),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3);
            decimal
                TaxAmt1 = 0m,
                TaxAmt2 = 0m,
                TaxAmt3 = 0m;

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3 }, 0);

            if (mSalesInvoiceTaxCalculationFormula == "1")
            {
                if (TaxCode1.Length != 0) TaxAmt1 = GetTaxRate(TaxCode1) * 0.01m * Amt;
                if (TaxCode2.Length != 0) TaxAmt2 = GetTaxRate(TaxCode2) * 0.01m * Amt;
                if (TaxCode3.Length != 0) TaxAmt3 = GetTaxRate(TaxCode3) * 0.01m * Amt;
            }
            else
            {
                if (TaxCode1.Length != 0) TaxAmt1 = GetTaxAmtPerDetail(1);
                if (TaxCode2.Length != 0) TaxAmt2 = GetTaxAmtPerDetail(2);
                if (TaxCode3.Length != 0) TaxAmt3 = GetTaxAmtPerDetail(3);
            }

            if (mIsDOCtAmtRounded)
            {
                TaxAmt1 = Decimal.Truncate(TaxAmt1);
                TaxAmt2 = Decimal.Truncate(TaxAmt2);
                TaxAmt3 = Decimal.Truncate(TaxAmt3);
            }

            TxtTaxAmt1.Text = Sm.FormatNum(TaxAmt1, 0);
            TxtTaxAmt2.Text = Sm.FormatNum(TaxAmt2, 0);
            TxtTaxAmt3.Text = Sm.FormatNum(TaxAmt3, 0);
        }

        private decimal GetTaxAmtPerDetail(byte TaxNo)
        {
            decimal TaxAmt = 0m;
            int TaxCol = 0;

            if (TaxNo == 1) TaxCol = 27;
            if (TaxNo == 2) TaxCol = 28;
            if (TaxNo == 3) TaxCol = 29;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                //decimal UPriceBefTax = Sm.GetGrdDec(Grd1, i, 23);
                //decimal Qty = Sm.GetGrdDec(Grd1, i, 20);

                //if (Sm.GetGrdBool(Grd1, i, TaxCol))
                //    TaxAmt += (Qty * UPriceBefTax) * TaxRate * 0.01m;

                if (Sm.GetGrdBool(Grd1, i, TaxCol))
                    TaxAmt += Sm.GetGrdDec(Grd1, i, (TaxCol + 5)); // hitungnya ini ada di ComputeTaxPerDetail2
            }

            return TaxAmt;
        }


        internal void ComputeAmt()
        {
            decimal COAAmt = 0m, Amt = 0m, TotalTax = 0m, Downpayment = 0m;

            if (TxtDownpayment.Text.Length > 0) Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (mSLI2TaxCalculationFormat == "1")
            {
                #region Default
                var CustomerAcNoAR =
                Sm.GetValue(
                        "Select (Select Concat(ParValue, A.CtCode) From TblParameter Where ParCode='CustomerAcNoAR' Limit 1) As AcNo " +
                        "From TblCustomer A, TblCustomerCategory B " +
                        "Where A.CtCtCode is Not Null " +
                        "And A.CtCtCode=B.CtCtCode " +
                        "And A.CtCode='" + Sm.GetLue(LueCtCode) + "' Limit 1; "
                        );
                //if (CustomerAcNoAR.Length > 0)
                //{
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    //if (Sm.CompareStr(CustomerAcNoAR, Sm.GetGrdStr(Grd3, Row, 1)))
                    //{
                    string AcType = Sm.GetValue(
                        "Select AcType From TblCoa Where AcNo = '" + Sm.GetGrdStr(Grd3, Row, 1) + "';");
                    if (Sm.GetGrdBool(Grd3, Row, 3))
                    {
                        if (AcType == "D")
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                    }
                    if (Sm.GetGrdBool(Grd3, Row, 5))
                    {
                        if (AcType == "C")
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 6);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 6);
                    }
                }
                #endregion
            }
            else //SRN, pilihan additional cost & discount nya ada yg masuk hitungan atau enggak
            {
                #region SRN

                mAdditionalCostDiscAmt = 0m;
                string CtCode = Sm.GetLue(LueCtCode);

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    string AcNo = Sm.GetGrdStr(Grd3, Row, 1);
                    if (!AcNo.Contains(string.Concat(".", CtCode))) // kalau bukan COA customer
                    {
                        //kalau dicentang di debit, (-)
                        if (Sm.GetGrdBool(Grd3, Row, 3))
                        {
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                        }
                        //kalau dicentang di kredit, (+)
                        if (Sm.GetGrdBool(Grd3, Row, 5))
                        {
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 6);
                        }

                        //kalau ga di centang baik debit dan kredit, masuk ke AdditionalCostDiscAmt
                        if (!Sm.GetGrdBool(Grd3, Row, 3) && !Sm.GetGrdBool(Grd3, Row, 5))
                        {
                            mAdditionalCostDiscAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                            mAdditionalCostDiscAmt += Sm.GetGrdDec(Grd3, Row, 6);
                        }
                    }
                }

                #endregion
            }

            Amt = COAAmt;

            if (mIsSalesInvoice2TaxEnabled)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    decimal Qty = Sm.GetGrdDec(Grd1, Row, 11);
                    decimal UPriceBefTax = Sm.GetGrdDec(Grd1, Row, 18);

                    Amt += (Qty * UPriceBefTax);
                }

                if (mSLI2TaxCalculationFormat == "1")
                {
                    if (mIsSalesInvoice3COANonTaxable)
                        ComputeTax(Amt - COAAmt);
                    else
                        ComputeTax(Amt);
                }
                else
                    ComputeTax(Amt);

                TotalTax = Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text);

                TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
                TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
                TxtAmt.EditValue = Sm.FormatNum(Amt + TotalTax - Downpayment, 0);
            }
            else
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 31) > 0)
                    {
                        decimal Amount = Sm.GetGrdDec(Grd1, Row, 31);
                        Amt += Amount;
                    }
                }

                //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                //{
                //    if (Sm.GetGrdStr(Grd1, Row, 25).Length > 0)
                //    {
                //        decimal Qty = Sm.GetGrdDec(Grd1, Row, 20);
                //        decimal TaxAmt = Sm.GetGrdDec(Grd1, Row, 25);
                //        TotalTax += (Qty * TaxAmt);
                //    }
                //}
                ComputeTax(Amt);
                TotalTax = Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text);

                if (mIsDOCtAmtRounded)
                {
                    Amt = decimal.Truncate(Amt);
                    TotalTax = decimal.Truncate(TotalTax);
                    Downpayment = decimal.Truncate(Downpayment);
                }
                TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
                TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
                TxtAmt.EditValue = Sm.FormatNum(Amt + TotalTax - Downpayment, 0);
            }

            if (mSLI2TaxCalculationFormat != "1")
            {
                // ditambahkan nilai additional cost discount yg ga dicentang
                TxtAmt.EditValue = Sm.FormatNum((Decimal.Parse(TxtAmt.Text) + mAdditionalCostDiscAmt), 0);
            }
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };



        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            decimal DP = Decimal.Parse(TxtDownpayment.Text);

            if (Sm.GetParameter("DocTitle") == "IOK")
            {
                var l = new List<InvoiceHdr>();
                var ldtl = new List<InvoiceDtl>();
                var ldtl2 = new List<InvoiceDtl2>();

                string[] TableName = { "InvoiceHdr", "InvoiceDtl", "InvoiceDtl2" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header
                var SQL = new StringBuilder();
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
                SQL.AppendLine("A.LocalDocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, ");
                SQL.AppendLine("A.CtCode, if(C.Doctype = '1',  Concat(F.SAName, '\n', F.SAAddress), '') As DesAddress, B.CtName, B.Address, DATE_FORMAT(A.DueDt,'%d %M %Y') As DueDt, ");
                SQL.AppendLine("A.CurCode, Round(A.TotalAmt, 2) As totalAmt, A.TotalTax, A.DownPayment, if(A.DownPayment = 0, round(A.Amt, 2), (Round(A.TotalAmt, 2)+A.TotalTax)) As Amt, A.SalesName, if(C.Doctype = '1', G.VdName, '') As ExpVd, ");
                SQL.AppendLine("if(C.Doctype = '1', D.ExpPlatNo, '') As ExpPlatNo, A.TaxInvDocument, H.bankAcNo, I.BankName As BankAcNm, A.Remark, A.cancelInd ");
                SQL.AppendLine("From TblSalesInvoiceHdr A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL.AppendLine("Inner Join TblSalesInvoiceDtl C On A.DocNo = C.DocNo ");
                SQL.AppendLine("Left Join TblDoct2Hdr D On C.DOCtDocNo = D.DocNo ");
                SQL.AppendLine("Left Join TblPlHdr E On D.PLDOcNo = E.DocNo ");
                SQL.AppendLine("Left Join TblDRHdr F On D.DrDOcNo = F.DocNo ");
                SQL.AppendLine("Left Join tblVendor G On F.ExpVdCode = G.VdCode ");
                SQL.AppendLine("Left Join TblBankAccount H On A.BankAcCode = H.BankAcCode ");
                SQL.AppendLine("Left Join TblBank I On H.BankCode = I.BankCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "LocalDocNo",
                         "DocDt",
                         "CtName",
                         "Address",
                         "DesAddress",

                         //11-15 
                         "CurCode",
                         "TotalAmt",
                         "TotalTax", 
                         "DownPayment", 
                         "Amt",

                         //16-20
                         "SalesName",
                         "ExpVd",
                         "ExpPlatNo",
                         "TaxInvDocument",
                         "BankAcNm",

                         //21-24
                         "BAnkAcNo",
                         "Remark",
                         "CancelInd",
                         "CompLocation2"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new InvoiceHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressFull = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),

                                LocalDocNo = Sm.DrStr(dr, c[6]),
                                DocDt = Sm.DrStr(dr, c[7]),
                                CtName = Sm.DrStr(dr, c[8]),
                                Address = Sm.DrStr(dr, c[9]),
                                DesAddress = Sm.DrStr(dr, c[10]),

                                CurCode = Sm.DrStr(dr, c[11]),
                                TotalAmt = Sm.DrDec(dr, c[12]),
                                TotalTax = Sm.DrDec(dr, c[13]),
                                DownPayment = Sm.DrDec(dr, c[14]),
                                Amt = Sm.DrDec(dr, c[15]),

                                SalesName = Sm.DrStr(dr, c[16]),
                                ExpVd = Sm.DrStr(dr, c[17]),
                                ExpPlatNo = Sm.DrStr(dr, c[18]),
                                TaxInvNo = Sm.DrStr(dr, c[19]),
                                BankAcNm = Sm.DrStr(dr, c[20]),
                                BankAcNo = Sm.DrStr(dr, c[21]),
                                Remark = Sm.DrStr(dr, c[22]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[15])),
                                Terbilang2 = Convert(Sm.DrDec(dr, c[15])),
                                CancelInd = Sm.DrStr(dr, c[23]),
                                CompLocation2 = Sm.DrStr(dr, c[24]),


                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Detail 1
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select  ");
                    SQLDtl.AppendLine("A.DOCtDocNo, A.DOCtDNo, ");
                    SQLDtl.AppendLine("A.ItCode, ");
                    SQLDtl.AppendLine("If(A.DocType='1', K1.ForeignName, K2.ForeignName) As ItName,  ");
                    SQLDtl.AppendLine("A.QtyPackagingUnit, ");
                    SQLDtl.AppendLine("If(A.DocType='1', F1.PackagingUnitUomCode, F2.PackagingUnitUomCode) As PackagingUnitUomCode, ");
                    SQLDtl.AppendLine("A.Qty, ");
                    SQLDtl.AppendLine("If(A.DocType='1', I1.PriceUomCode, I2.PriceUomCode) As PriceUomCode, ");
                    SQLDtl.AppendLine("A.UPriceBeforeTax, ");
                    SQLDtl.AppendLine("A.TaxRate, ");
                    SQLDtl.AppendLine("A.TaxAmt, ");
                    SQLDtl.AppendLine("A.UPriceAfterTax, ");
                    SQLDtl.AppendLine("If(A.DocType='1', L1.PtName, L2.PtName) As PtName, ");
                    SQLDtl.AppendLine("Round((A.Qty * A.UPriceBeforeTax), 2) As Amt, ");
                    SQLDtl.AppendLine("if(A.Qty =0, 0, if(A.Doctype='1', ((N1.Qty2*A.Qty)/N1.Qty), ((N2.Qty2*A.Qty)/N2.Qty))) As Qty2,  ");
                    SQLDtl.AppendLine("T1.AddCost, T2.AddDisc ");
                    SQLDtl.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");

                    SQLDtl.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl.AppendLine("Left Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtHdr G1 On E1.CtQtDocNo=G1.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblCtQtDtl H1 On E1.CtQtDocNo=H1.DocNo And F1.CtQtDNo=H1.DNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceHdr I1 On H1.ItemPriceDocNo=I1.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblItemPriceDtl J1 On H1.ItemPriceDocNo=J1.DocNo And H1.ItemPriceDNo=J1.DNo ");
                    SQLDtl.AppendLine("Left Join TblItem K1 On J1.ItCode=K1.ItCode ");
                    SQLDtl.AppendLine("Left Join TblPaymentTerm L1 On G1.PtCode=L1.PtCode ");
                    SQLDtl.AppendLine("Left Join TblDRHdr M1 On B.DRDocNo=M1.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit N1 On F1.PackagingUnitUomCode = N1.UomCOde And A.ItCode = N1.ItCode ");

                    SQLDtl.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtHdr G2 On E2.CtQtDocNo=G2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtDtl H2 On E2.CtQtDocNo=H2.DocNo And F2.CtQtDNo=H2.DNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceHdr I2 On H2.ItemPriceDocNo=I2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceDtl J2 On H2.ItemPriceDocNo=J2.DocNo And H2.ItemPriceDNo=J2.DNo ");
                    SQLDtl.AppendLine("Left Join TblItem K2 On J2.ItCode=K2.ItCode ");
                    SQLDtl.AppendLine("Left Join TblPaymentTerm L2 On G2.PtCode=L2.PtCode ");
                    SQLDtl.AppendLine("Left Join TblPLHdr M2 On B.PLDocNo=M2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit N2 On F2.PackagingUnitUomCode = N2.UomCOde And A.ItCode = N2.ItCode ");

                    SQLDtl.AppendLine("Left Join ( ");
                    SQLDtl.AppendLine("     Select if(X.Actype='D'&& X.Damt=0, X.Camt, X.Damt) As AddCost, 0 As AddCost1, X.DocNo ");
                    SQLDtl.AppendLine("     From ");
                    SQLDtl.AppendLine("     ( ");
                    SQLDtl.AppendLine("     Select A.Damt, A.Camt, A.DocNo, A.AcNo, B.AcType, B.AcDesc  ");
                    SQLDtl.AppendLine("     From TblsalesInvoiceDtl2 A ");
                    SQLDtl.AppendLine("     Inner Join TblCoa B On A.AcNo = B.AcNo ");
                    SQLDtl.AppendLine("     Where A.AcNo = '1.14.11' ");
                    SQLDtl.AppendLine("     Or A.AcNo = '1.14.12' ");
                    SQLDtl.AppendLine("     And DocNo=@DocNo  ");
                    SQLDtl.AppendLine("     )X ");
                    SQLDtl.AppendLine(") T1 On A.DocNo = T1.DocNo  ");
                    SQLDtl.AppendLine("Left Join ( ");
                    SQLDtl.AppendLine("         Select if(X.AcType = 'C' && X.Camt !=0, X.Camt, X.Damt) As AddDisc, X.DocNo ");
                    SQLDtl.AppendLine("         From (  ");
                    SQLDtl.AppendLine("             Select A.DocNo, A.AcNo, A.Damt, A.Camt, B.AcType  ");
                    SQLDtl.AppendLine("             From TblsalesInvoiceDtl2 A  ");
                    SQLDtl.AppendLine("             Inner Join TblCoa B On A.AcNo = B.AcNo  ");
                    SQLDtl.AppendLine("             Where left(A.Acno, 5)='4.2.1' ");
                    SQLDtl.AppendLine("         )X  ");
                    SQLDtl.AppendLine("     ) T2 On A.DocNo = T2.DocNo ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo ");
                    SQLDtl.AppendLine("Group By A.DOCtDocNo, A.DOCtDNo ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    //Sm.CmParam<String>(ref cmDtl, "@CtCode", Sm.GetLue(LueCtCode));

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ItCode" ,

                     //1-5
                     "ItName" ,
                     "QtyPackagingUnit",
                     "PackagingUnitUomCode",
                     "Qty",
                     "PriceUomCode",

                     //6-10
                     "UPriceBeforeTax",
                     "TaxRate",
                     "TaxAmt",
                     "UPriceAfterTax",
                     "PtName",

                     //11-15
                     "Amt",
                     "Qty2", 
                     "AddCost",
                     "AddDisc",
                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new InvoiceDtl()
                            {
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                QtyPackagingUnit = Sm.DrDec(drDtl, cDtl[2]),
                                PackagingUnitUomCode = Sm.DrStr(drDtl, cDtl[3]),
                                Qty = Sm.DrDec(drDtl, cDtl[4]),
                                PriceUomCode = Sm.DrStr(drDtl, cDtl[5]),
                                UPriceBeforeTax = Sm.DrDec(drDtl, cDtl[6]),
                                TaxRate = Sm.DrDec(drDtl, cDtl[7]),
                                TaxAmt = Sm.DrDec(drDtl, cDtl[8]),
                                UPriceAfterTax = Sm.DrDec(drDtl, cDtl[9]),
                                PtName = Sm.DrStr(drDtl, cDtl[10]),
                                Amt = Sm.DrDec(drDtl, cDtl[11]),
                                CurCode = Sm.GetLue(LueCurCode),

                                Qty2 = Sm.DrDec(drDtl, cDtl[12]),
                                AddCost = Sm.DrDec(drDtl, cDtl[13]),
                                AddDisc = Sm.DrDec(drDtl, cDtl[14]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Detail 2
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select ");
                    SQLDtl2.AppendLine("If(A.DocType='1', Group_Concat(Distinct E1.CtPONo SEPARATOR ','), Group_Concat(Distinct E2.CtPONo SEPARATOR ',')) As PONumber, ");
                    SQLDtl2.AppendLine("If(A.DocType='1', Group_concat(Distinct E1.LocalDocno separator ', '), Group_concat(Distinct E2.LocalDocno separator ', ')) As SODocNo ");
                    SQLDtl2.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl2.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");
                    SQLDtl2.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl2.AppendLine("Left Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl2.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl2.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl2.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl2.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl2.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl2.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl2.AppendLine("Where A.DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Group BY A.DocNo ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "PONumber" ,

                     //1-5
                     "SODocNo" ,
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new InvoiceDtl2()
                            {
                                PONumber = Sm.DrStr(drDtl2, cDtl2[0]),
                                SODocNo = Sm.DrStr(drDtl2, cDtl2[1]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                if (Decimal.Parse(TxtTotalTax.Text) > 0)
                {
                    Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);
                    //Sm.PrintReport("Invoice", myLists, TableName, false);
                    //Sm.PrintReport("FakturPenjualan", myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport(string.Concat(mIsFormPrintOutInvoice + '2'), myLists, TableName, false);
                    //Sm.PrintReport("InvoiceNoPPN", myLists, TableName, false);
                    //Sm.PrintReport("FakturPenjualan", myLists, TableName, false);
                }

            }

            else if (Sm.GetParameter("DocTitle") == "MSI")
            {
                var ListH = new List<InvoiceHdr2>();
                var ListD = new List<InvoiceDtl3>();
                var ListD2 = new List<InvoiceDtl4>();
                var ListD3 = new List<InvoiceDtl5>();

                string[] TableName = { "InvoiceHdr2", "InvoiceDtl3", "InvoiceDtl4", "InvoiceDtl5" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region MSI

                #region Header2 MSI

                var cm2 = new MySqlCommand();
                var SQL2 = new StringBuilder();
                SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyEmail', ");
                SQL2.AppendLine("Concat(Left(A.DocDt,4),'.',substring(A.DocDt,5,2),'.', Left(A.DocNo,4))As Doc,");
                SQL2.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.CtName, B.Address, B.Phone, A.SalesName,");
                SQL2.AppendLine("H.SAName, H.SAAddress, A.DownPayment, K.Phone As SAPhone, A.Curcode, if(C.Doctype = '1', G.VdName, '') As ExpVd,");
                SQL2.AppendLine("DATE_FORMAT(A.DueDt,'%d %M %Y') As DueDt, Round(A.TotalAmt, 2) As totalAmt, A.TotalTax,  ");
                SQL2.AppendLine("if(A.DownPayment = 0, round(A.Amt, 2), (Round(A.TotalAmt, 2)+A.TotalTax)) As Amt,  A.Remark, A.cancelInd, I.CityName As SACityName, J.CityName As CtCityName ");
                SQL2.AppendLine("From TblSalesInvoiceHdr A ");
                SQL2.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL2.AppendLine("Inner Join TblSalesInvoiceDtl C On A.DocNo = C.DocNo ");
                SQL2.AppendLine("Left Join TblDoct2Hdr D On C.DOCtDocNo = D.DocNo ");
                SQL2.AppendLine("Left Join TblPlHdr E On D.PLDOcNo = E.DocNo ");
                SQL2.AppendLine("Left Join TblDRHdr F On D.DrDOcNo = F.DocNo ");
                SQL2.AppendLine("Left Join tblVendor G On F.ExpVdCode = G.VdCode ");
                SQL2.AppendLine("Inner Join TblSOhdr H On A.SODocNo = H.DocNo ");
                SQL2.AppendLine("Left Join TblCity I On H.SAcityCode = I.CityCode ");
                SQL2.AppendLine("Left Join TblCity J On B.CityCode = J.CityCode ");
                SQL2.AppendLine("Left Join TblCustomerShipAddress K On H.SAname = K.Name And H.SAAddress = K.Address ");
                SQL2.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyEmail",

                         //6-10
                         "Doc",
                         "DocNo",
                         "DocDt",
                         "CtName",
                         "Address",
                        

                         //11-15
                         "Phone",
                         "SalesName",
                         "SAName",
                         "SAAddress",
                         "DownPayment",

                         //16-20
                         "SaPhone",
                         "Curcode",
                         "DueDt",
                         "TotalAmt",
                         "TotalTax",

                         //21-25
                         "Amt",
                         "Remark",
                         "Cancelind",
                         "ExpVd",
                         "SACityName",
                         "CtCityName",
                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            ListH.Add(new InvoiceHdr2()
                            {
                                CompanyLogo = Sm.DrStr(dr2, c2[0]),

                                CompanyName = Sm.DrStr(dr2, c2[1]),
                                CompanyAddress = Sm.DrStr(dr2, c2[2]),
                                CompanyAddressFull = Sm.DrStr(dr2, c2[3]),
                                CompanyPhone = Sm.DrStr(dr2, c2[4]),
                                CompanyEmail = Sm.DrStr(dr2, c2[5]),

                                Doc = Sm.DrStr(dr2, c2[6]),
                                DocNo = Sm.DrStr(dr2, c2[7]),
                                DocDt = Sm.DrStr(dr2, c2[8]),
                                CtName = Sm.DrStr(dr2, c2[9]),
                                Address = Sm.DrStr(dr2, c2[10]),
                                Phone = Sm.DrStr(dr2, c2[11]),

                                SalesName = Sm.DrStr(dr2, c2[12]),
                                SAName = Sm.DrStr(dr2, c2[13]),
                                SAAddress = Sm.DrStr(dr2, c2[14]),
                                Downpayment = Sm.DrDec(dr2, c2[15]),

                                SaPhone = Sm.DrStr(dr2, c2[16]),
                                Curcode = Sm.DrStr(dr2, c2[17]),
                                DueDt = Sm.DrStr(dr2, c2[18]),
                                TotalAmt = Sm.DrDec(dr2, c2[19]),
                                TotalTax = Sm.DrDec(dr2, c2[20]),

                                //21-23
                                Amt = Sm.DrDec(dr2, c2[21]),
                                Remark = Sm.DrStr(dr2, c2[22]),
                                Cancelind = Sm.DrStr(dr2, c2[23]),
                                ExpVd = Sm.DrStr(dr2, c2[24]),
                                SACityName = Sm.DrStr(dr2, c2[25]),
                                CtCityName = Sm.DrStr(dr2, c2[26]),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                UserCode = Sm.GetValue("Select UserName From tblUser Where UserCode='" + Gv.CurrentUserCode + "'")
                            });
                        }
                    }
                    dr2.Close();
                }
                myLists.Add(ListH);
                #endregion

                #region Detail 3 MSI
                var cmDtl3 = new MySqlCommand();

                var SQLDtl3 = new StringBuilder();
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;

                    SQLDtl3.AppendLine("Select A.DocNo, A.ItCode, K.ItName,A.Qty, I.PriceUomCode, M.UPrice, A.TaxAmt, ");
                    SQLDtl3.AppendLine("A.UPriceAfterTax, ifnull(H.Discount,0) As Discount, Round((A.Qty * (ifnull(M.UPrice,0)-(ifnull(M.UPrice,0)*ifnull(H.discount, 0)/100))), 2) As SubTotal, ");
                    SQLDtl3.AppendLine("L.PTName, F.DocNo As DocNoSO, Round((A.Qty*ifnull(M.UPrice,0)*ifnull(H.discount, 0)/100),2) As DiscAmt, Round ((A.Qty*ifnull(M.UPrice,0)),2) As Amt ");
                    SQLDtl3.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl3.AppendLine("Inner Join TblSalesInvoiceHdr B On A.DocNo = b.DocNo And B.SoDocNo is not null");
                    SQLDtl3.AppendLine("Left Join TblSOHdr E On B.SODocNo=E.DocNo ");
                    SQLDtl3.AppendLine("Left Join TblSODtl F On A.DOctDocNo=F.DocNo And A.DOCtDNo=F.DNo ");
                    SQLDtl3.AppendLine("Left Join TblCtQtHdr G On E.CtQtDocNo=G.DocNo ");
                    SQLDtl3.AppendLine("left Join TblCtQtDtl H On E.CtQtDocNo=H.DocNo And F.CtQtDNo=H.DNo ");
                    SQLDtl3.AppendLine("Left Join TblItemPriceHdr I On H.ItemPriceDocNo=I.DocNo ");
                    SQLDtl3.AppendLine("Left Join TblItem K On A.ItCode=K.ItCode ");
                    SQLDtl3.AppendLine("Left Join TblPaymentTerm L On G.PtCode=L.PtCode ");
                    SQLDtl3.AppendLine("Left Join tblitempricedtl M On H.ItemPriceDocno=M.DocNo And H.ItemPriceDNo=M.DNo ");
                    SQLDtl3.AppendLine("Where A.DocNo=@DocNo And B.cancelInd = 'N' ");

                    cmDtl3.CommandText = SQLDtl3.ToString();

                    Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                    // Sm.CmParam<String>(ref cmDtl, "@CtCode", Sm.GetLue(LueCtCode));

                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                    {
                     //0
                     "DocNo" ,

                     //1-5
                     "ItCode" ,
                     "ItName",
                     "Qty",
                     "PriceUomCode",
                     "Uprice",
                     
                     //6-10
                     "TaxAmt",
                     "UPriceAfterTax",
                     "Discount",
                     "SubTotal",
                     "PTName",
                     "DocNoSO",

                     //11-12
                     "DiscAmt",
                     "Amt"
          
                    });
                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {
                            ListD.Add(new InvoiceDtl3()
                            {
                                DocNo = Sm.DrStr(drDtl3, cDtl3[0]),
                                ItCode = Sm.DrStr(drDtl3, cDtl3[1]),
                                ItName = Sm.DrStr(drDtl3, cDtl3[2]),
                                Qty = Sm.DrDec(drDtl3, cDtl3[3]),
                                PriceUomCode = Sm.DrStr(drDtl3, cDtl3[4]),
                                Uprice = Sm.DrDec(drDtl3, cDtl3[5]),
                                TaxAmt = Sm.DrDec(drDtl3, cDtl3[6]),
                                UPriceAfterTax = Sm.DrDec(drDtl3, cDtl3[7]),
                                Discount = Sm.DrDec(drDtl3, cDtl3[8]),
                                SubTotal = Sm.DrDec(drDtl3, cDtl3[9]),
                                PTName = Sm.DrStr(drDtl3, cDtl3[10]),
                                DocNoSO = Sm.DrStr(drDtl3, cDtl3[11]),
                                DiscAmt = Sm.DrDec(drDtl3, cDtl3[12]),
                                Amt = Sm.DrDec(drDtl3, cDtl3[13]),

                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ListD);
                #endregion

                #region Detail 4 MSI
                var cmDtl4 = new MySqlCommand();

                var SQLDtl4 = new StringBuilder();
                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl4.Open();
                    cmDtl4.Connection = cnDtl4;

                    if(DP == 0)
                    {
                        SQLDtl4.AppendLine("Select A.Docno, A.TotalAmt, A.TotalTax, B.DAmt, B.CAmt, ");
                        SQLDtl4.AppendLine("(A.TotalAmt+ A.TotalTax+ ifnull(if(B.Damt!=0,B.DAmt,B.CAmt),0))As Total ");
                        SQLDtl4.AppendLine("from  tblsalesinvoicehdr A ");
                        SQLDtl4.AppendLine("Left  join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo And Acno=(Select Parvalue From Tblparameter Where parCode = 'TOPFreightAcNo') ");
                        SQLDtl4.AppendLine("where A.DocNo=@DocNo");
                    }
                    else
                    {
                        SQLDtl4.AppendLine("Select A.Docno, A.TotalAmt, A.TotalTax, B.DAmt, B.CAmt, ");
                        SQLDtl4.AppendLine("(A.TotalAmt+ A.TotalTax+ ifnull(if(B.Damt!=0,B.DAmt,B.CAmt),0)- A.DownPayment) As Total ");
                        SQLDtl4.AppendLine("from  tblsalesinvoicehdr A ");
                        SQLDtl4.AppendLine("Left  join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo And Acno=(Select Parvalue From Tblparameter Where parCode = 'TOPFreightAcNo') ");
                        SQLDtl4.AppendLine("where A.DocNo=@DocNo");
                    }
                    cmDtl4.CommandText = SQLDtl4.ToString();

                    Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);

                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                    {
                     //0
                     "DocNo",

                     //1-5
                     "TotalAmt",
                     "TotalTax",
                     "DAmt",
                     "CAmt",
                     "Total",

                    });
                    if (drDtl4.HasRows)
                    {
                        while (drDtl4.Read())
                        {
                            ListD2.Add(new InvoiceDtl4()
                            {
                                DocNo = Sm.DrStr(drDtl4, cDtl4[0]),

                                TotalAmt = Sm.DrDec(drDtl4, cDtl4[1]),
                                TotalTax = Sm.DrDec(drDtl4, cDtl4[2]),
                                DAmt = Sm.DrDec(drDtl4, cDtl4[3]),
                                CAmt = Sm.DrDec(drDtl4, cDtl4[4]),
                                Total = Sm.DrDec(drDtl4, cDtl4[5]),

                                Terbilang = Sm.Terbilang(Sm.DrDec(drDtl4, cDtl4[5])),
                                Terbilang2 = Convert(Sm.DrDec(drDtl4, cDtl4[5])),

                            });
                        }
                    }
                    drDtl4.Close();
                }
                myLists.Add(ListD2);
                #endregion

                #region Detail 5 MSI
                var cmDtl5 = new MySqlCommand();

                var SQLDtl5 = new StringBuilder();
                using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl5.Open();
                    cmDtl5.Connection = cnDtl5;


                    SQLDtl5.AppendLine("Select X.Acno, X.AcDesc, X.Actype, X.Damt, X.Camt,");
                    SQLDtl5.AppendLine("case  ");
                    SQLDtl5.AppendLine("When (X.Actype ='C' && X.CAmt>0) Then X.CAmt  ");
                    SQLDtl5.AppendLine("When (X.Actype ='C' && X.CAmt=0) Then (X.DAmt*-1) ");
                    SQLDtl5.AppendLine("When (X.Actype ='D' && X.DAmt>0) Then X.DAmt  ");
                    SQLDtl5.AppendLine("When (X.Actype ='D' && X.DAmt=0) Then (X.CAmt*-1) ");
                    SQLDtl5.AppendLine("End As AmtAcNo, ");
                    SQLDtl5.AppendLine("case  ");
                    SQLDtl5.AppendLine("When (X.Actype ='C' && CAmt>0) Then '1'  ");
                    SQLDtl5.AppendLine("When (X.Actype ='C' && CAmt=0) Then '0' ");
                    SQLDtl5.AppendLine("When (X.Actype ='D' && DAmt>0) Then '1'  ");
                    SQLDtl5.AppendLine("When (X.Actype ='D' && DAmt=0) Then '0' ");
                    SQLDtl5.AppendLine("End As TypeInd, ifnull(X.OptDesc, '-') As OptDesc, ");
                    SQLDtl5.AppendLine("if((X.Actype = 'D' && Left(X.AcNo, 1) = '4' && DAmt>0), -1, 1) As 'UniqInd', X.Remark ");
                    SQLDtl5.AppendLine("From ");
                    SQLDtl5.AppendLine("( ");
                    SQLDtl5.AppendLine("    Select A.DoCNo, A.Acno, b.AcDesc, B.Actype, A.Damt, A.Camt, C.OptDesc, A.Remark   ");
                    SQLDtl5.AppendLine("    From TblSalesInvoiceDtl2 A ");
                    SQLDtl5.AppendLine("    Inner Join TblCOA B On A.AcNo = B.Acno ");
                    SQLDtl5.AppendLine("    Left Join TblOption C On A.OptAcDesc = C.OptCode And C.OptCat = 'AccountDescriptionOnSalesInvoice' ");
                    SQLDtl5.AppendLine("    Where A.AcInd = 'N' ");
                    SQLDtl5.AppendLine(")X Where X.DocNo=@DocNo ");
                    SQLDtl5.AppendLine("union All ");
                    SQLDtl5.AppendLine("Select ' ', ' ', ' ', 0, 0, 0, ' ', ' ', 0, ' ' ");

                    cmDtl5.CommandText = SQLDtl5.ToString();

                    Sm.CmParam<String>(ref cmDtl5, "@DocNo", TxtDocNo.Text);

                    var drDtl5 = cmDtl5.ExecuteReader();
                    var cDtl5 = Sm.GetOrdinal(drDtl5, new string[] 
                    {
                     //0
                     "AcNo",

                     //1-5
                     "AcDesc",
                     "AcType",
                     "DAmt",
                     "CAmt",
                     "AmtAcNo",
                     //6-8
                     "OptDesc",
                     "Remark",
                     "UniqInd"
                    });
                    if (drDtl5.HasRows)
                    {
                        while (drDtl5.Read())
                        {
                            ListD3.Add(new InvoiceDtl5()
                            {
                                AcNo = Sm.DrStr(drDtl5, cDtl5[0]),

                                AcDesc = Sm.DrStr(drDtl5, cDtl5[1]),
                                AcType = Sm.DrStr(drDtl5, cDtl5[2]),
                                DAmt = Sm.DrDec(drDtl5, cDtl5[3]),
                                CAmt = Sm.DrDec(drDtl5, cDtl5[4]),
                                AmtAcNo = Sm.DrDec(drDtl5, cDtl5[5]),
                                OptDesc = Sm.DrStr(drDtl5, cDtl5[6]),
                                Remark = Sm.DrStr(drDtl5, cDtl5[7]),
                                UniqInd = Sm.DrDec(drDtl5, cDtl5[8]),
                            });
                        }
                    }
                    drDtl5.Close();
                }
                myLists.Add(ListD3);
                #endregion


                #endregion


                if (Decimal.Parse(TxtTotalTax.Text) > 0)
                {
                    Sm.PrintReport(string.Concat(mIsFormPrintOutInvoice, "CBD"), myLists, TableName, false);
                    //Sm.PrintReport("Invoice", myLists, TableName, false);
                    //Sm.PrintReport("FakturPenjualan", myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport(string.Concat(mIsFormPrintOutInvoice + '2' +"CBD"), myLists, TableName, false);
                    //Sm.PrintReport("InvoiceNoPPN", myLists, TableName, false);
                    //Sm.PrintReport("FakturPenjualan", myLists, TableName, false);
                }
            }



        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            DteDueDt.EditValue = null;
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            DteDueDt.EditValue = null;
            ClearGrd();
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));
            if (Sm.GetLue(LueCtCode).Length > 0)
                ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtDownpayment_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDownpayment, 0);
            ComputeAmt();
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void TxtTaxInvDocument_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTaxInvDocument);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue1(SetLueOptionCode));
        }

        private void LueOption_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value =
                    Grd3.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueOption);
                    Grd3.Cells[fCell.RowIndex, 8].Value = LueOption.GetColumnValue("Col2");
                }
                LueOption.Visible = false;
            }
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mSalesInvoiceTaxCalculationFormula == "2") ComputeTaxPerDetail(false, 0);
                else ComputeAmt();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mSalesInvoiceTaxCalculationFormula == "2") ComputeTaxPerDetail(false, 0);
                else ComputeAmt();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mSalesInvoiceTaxCalculationFormula == "2") ComputeTaxPerDetail(false, 0);
                else ComputeAmt();
            }
        }

        private void TxtTaxAmt1_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTaxAmt1, 0);
        }

        private void TxtTaxAmt2_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTaxAmt2, 0);
        }

        private void TxtTaxAmt3_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTaxAmt3, 0);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }


        #endregion

        #region Button Event

        private void BtnSO_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSalesInvoice2Dlg(this, Sm.GetLue(LueCtCode)));
        }

        private void BtnDueDt_Click(object sender, EventArgs e)
        {
            DteDueDt.EditValue = null;
            var DocDt = Sm.GetDte(DteDocDt);
            if (DocDt.Length > 0 && Grd1.Rows.Count > 1)
            {
                decimal TOP = Sm.GetGrdDec(Grd1, 0, 26);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    {
                        if (TOP > Sm.GetGrdDec(Grd1, Row, 26))
                            TOP = Sm.GetGrdDec(Grd1, Row, 26);
                    }
                }
                
                DteDueDt.EditValue = new DateTime(
                   Int32.Parse(DocDt.Substring(0, 4)),
                   Int32.Parse(DocDt.Substring(4, 2)),
                   Int32.Parse(DocDt.Substring(6, 2)),
                   0, 0, 0).AddDays((double)TOP);
            
            }

        }

        private void BtnLocalDocNo_Click(object sender, EventArgs e)
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 29).Length > 0)
                {
                    TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 29);
                    break;
                }
            }
        }

        #endregion

        #region Grid

        #region Grd1

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (mSalesInvoiceTaxCalculationFormula != "1")
            {
                if (e.ColIndex == 27 || e.ColIndex == 28 || e.ColIndex == 29)
                {
                    ComputeTaxPerDetail(true, e.RowIndex);
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmCtQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            ComputeAmt();
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmCtQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdDec(Grd3, e.RowIndex, 4) > 0)
            {
                Grd3.Cells[e.RowIndex, 6].Value = 0;
            }

            if (e.ColIndex == 6 && Sm.GetGrdDec(Grd3, e.RowIndex, 6) > 0)
            {
                Grd3.Cells[e.RowIndex, 4].Value = 0;
            }

            if (e.ColIndex == 3 || e.ColIndex == 5)
                Grd3.Cells[e.RowIndex, 10].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6 }, e.ColIndex))
                ComputeAmt();
            
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesInvoice2Dlg2(this));
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LueOption, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                SetLueOptionCode(ref LueOption);
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmSalesInvoice2Dlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);

        }

        #endregion

        #endregion

        #endregion

        #region Class

        #region Report Class

        private class InvoiceHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string LocalDocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string DesAddress { get; set; }
            public string Address { get; set; }
            public string CurCode { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal TotalTax { get; set; }
            public decimal DownPayment { get; set; }
            public decimal Amt { get; set; }
            public string SalesName { get; set; }
            public string ExpVd { get; set; }
            public string ExpPlatNo { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string TaxInvNo { get; set; }
            public string BankAcNm { get; set; }
            public string BankAcNo { get; set; }
            public string CancelInd { get; set; }
            public string CompLocation2 { get; set; }
        }

        private class InvoiceHdr2
        {
            public string CompanyLogo { set; get; }

            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyEmail { get; set; }

            public string Doc { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }

            public string SalesName { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string PrintBy { get; set; }
            public string CompLocation2 { get; set; }
            public decimal Downpayment { get; set; }

            public string SaPhone { get; set; }
            public string Curcode { get; set; }
            public string UserCode { get; set; }
            public string DueDt { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal TotalTax { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
            public string Cancelind { get; set; }
            public string ExpVd { get; set; }
            public string SACityName { get; set; }
            public string CtCityName { get; set; }
        }

        private class InvoiceDtl
        {
            public decimal UPriceBeforeTax { get; set; }
            public decimal UPriceAfterTax { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public string PackagingUnitUomCode { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public string CurCode { get; set; }
            public string CTPoNo { get; set; }
            public string PriceUomCode { get; set; }
            public decimal Discount { get; set; }
            public decimal DiscAmt { get; set; }
            public decimal TaxRate { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal AmtDisc { get; set; }
            public decimal AmtTax { get; set; }
            public decimal AmtCost { get; set; }
            public string PtName { get; set; }
            public decimal Qty2 { get; set; }
            public string SODocNo { get; set; }
            public decimal AddCost { get; set; }
            public decimal AddDisc { get; set; }

        }

        private class InvoiceDtl2
        {
            public string PONumber { get; set; }
            public string SODocNo { get; set; }
        }

        private class InvoiceDtl3
        {
            public string DocNo { get; set; }

            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PriceUomCode { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal Uprice { get; set; }

            public decimal UPriceAfterTax { get; set; }
            public decimal Discount { get; set; }
            public decimal SubTotal { get; set; }
            public string PTName { get; set; }
            public string DocNoSO { get; set; }
            public decimal DiscAmt { get; set; }
            public decimal Amt { get; set; }

        }

        private class InvoiceDtl4
        {
            public string DocNo { get; set; }

            public decimal TotalAmt { get; set; }
            public decimal TotalTax { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal Total { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }

        }

        private class InvoiceDtl5
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal AmtAcNo { get; set; }
            public string OptDesc { get; set; }
            public string Remark { get; set; }
            public decimal UniqInd { get; set; }
        }

        #endregion

        #region Rate Class

        class Rate
        {
            public decimal Downpayment { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string CtCode { get; set; }
        }

        #endregion

        #endregion
       
    }
}
