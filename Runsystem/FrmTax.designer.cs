﻿namespace RunSystem
{
    partial class FrmTax
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTax));
            this.TxtTaxName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtTaxCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTaxRate = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnAcNo1 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcDesc1 = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnAcNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcDesc2 = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtAcNo1 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo2 = new DevExpress.XtraEditors.TextEdit();
            this.ChkTaxInvoiceInd = new DevExpress.XtraEditors.CheckEdit();
            this.LueTaxGrpCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblTaxGrpCode = new System.Windows.Forms.Label();
            this.ChkWapuInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtLevel = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LueParent = new DevExpress.XtraEditors.LookUpEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ChkServiceInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInvoiceInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxGrpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWapuInd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkServiceInd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(993, 0);
            this.panel1.Size = new System.Drawing.Size(70, 417);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.LblTaxGrpCode);
            this.panel2.Controls.Add(this.LueTaxGrpCode);
            this.panel2.Controls.Add(this.TxtTaxRate);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtTaxName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtTaxCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(993, 417);
            // 
            // TxtTaxName
            // 
            this.TxtTaxName.EnterMoveNextControl = true;
            this.TxtTaxName.Location = new System.Drawing.Point(109, 30);
            this.TxtTaxName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxName.Name = "TxtTaxName";
            this.TxtTaxName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxName.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxName.Properties.MaxLength = 80;
            this.TxtTaxName.Size = new System.Drawing.Size(374, 20);
            this.TxtTaxName.TabIndex = 12;
            this.TxtTaxName.Validated += new System.EventHandler(this.TxtTaxName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(41, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Tax Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxCode
            // 
            this.TxtTaxCode.EnterMoveNextControl = true;
            this.TxtTaxCode.Location = new System.Drawing.Point(109, 8);
            this.TxtTaxCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxCode.Name = "TxtTaxCode";
            this.TxtTaxCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxCode.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxCode.Properties.MaxLength = 16;
            this.TxtTaxCode.Size = new System.Drawing.Size(152, 20);
            this.TxtTaxCode.TabIndex = 10;
            this.TxtTaxCode.Validated += new System.EventHandler(this.TxtTaxCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(44, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Tax Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxRate
            // 
            this.TxtTaxRate.EnterMoveNextControl = true;
            this.TxtTaxRate.Location = new System.Drawing.Point(109, 52);
            this.TxtTaxRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxRate.Name = "TxtTaxRate";
            this.TxtTaxRate.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxRate.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxRate.Size = new System.Drawing.Size(152, 20);
            this.TxtTaxRate.TabIndex = 14;
            this.TxtTaxRate.Validated += new System.EventHandler(this.TxtTaxRate_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(47, 55);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "Tax Rate";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo1
            // 
            this.BtnAcNo1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo1.Appearance.Options.UseBackColor = true;
            this.BtnAcNo1.Appearance.Options.UseFont = true;
            this.BtnAcNo1.Appearance.Options.UseForeColor = true;
            this.BtnAcNo1.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo1.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo1.Image")));
            this.BtnAcNo1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo1.Location = new System.Drawing.Point(330, 7);
            this.BtnAcNo1.Name = "BtnAcNo1";
            this.BtnAcNo1.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo1.TabIndex = 19;
            this.BtnAcNo1.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo1.ToolTipTitle = "Run System";
            this.BtnAcNo1.Click += new System.EventHandler(this.BtnAcNo1_Click);
            // 
            // TxtAcDesc1
            // 
            this.TxtAcDesc1.EnterMoveNextControl = true;
            this.TxtAcDesc1.Location = new System.Drawing.Point(112, 30);
            this.TxtAcDesc1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc1.Name = "TxtAcDesc1";
            this.TxtAcDesc1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc1.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc1.Properties.MaxLength = 255;
            this.TxtAcDesc1.Properties.ReadOnly = true;
            this.TxtAcDesc1.Size = new System.Drawing.Size(374, 20);
            this.TxtAcDesc1.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(19, 11);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 14);
            this.label4.TabIndex = 17;
            this.label4.Text = "Input Tax COA";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo2
            // 
            this.BtnAcNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo2.Appearance.Options.UseBackColor = true;
            this.BtnAcNo2.Appearance.Options.UseFont = true;
            this.BtnAcNo2.Appearance.Options.UseForeColor = true;
            this.BtnAcNo2.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo2.Image")));
            this.BtnAcNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo2.Location = new System.Drawing.Point(331, 52);
            this.BtnAcNo2.Name = "BtnAcNo2";
            this.BtnAcNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo2.TabIndex = 23;
            this.BtnAcNo2.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo2.ToolTipTitle = "Run System";
            this.BtnAcNo2.Click += new System.EventHandler(this.BtnAcNo2_Click);
            // 
            // TxtAcDesc2
            // 
            this.TxtAcDesc2.EnterMoveNextControl = true;
            this.TxtAcDesc2.Location = new System.Drawing.Point(112, 74);
            this.TxtAcDesc2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc2.Name = "TxtAcDesc2";
            this.TxtAcDesc2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc2.Properties.MaxLength = 255;
            this.TxtAcDesc2.Properties.ReadOnly = true;
            this.TxtAcDesc2.Size = new System.Drawing.Size(374, 20);
            this.TxtAcDesc2.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(9, 54);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 14);
            this.label5.TabIndex = 21;
            this.label5.Text = "Output Tax COA";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcNo1
            // 
            this.TxtAcNo1.EnterMoveNextControl = true;
            this.TxtAcNo1.Location = new System.Drawing.Point(112, 8);
            this.TxtAcNo1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo1.Name = "TxtAcNo1";
            this.TxtAcNo1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo1.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo1.Properties.MaxLength = 40;
            this.TxtAcNo1.Properties.ReadOnly = true;
            this.TxtAcNo1.Size = new System.Drawing.Size(214, 20);
            this.TxtAcNo1.TabIndex = 18;
            // 
            // TxtAcNo2
            // 
            this.TxtAcNo2.EnterMoveNextControl = true;
            this.TxtAcNo2.Location = new System.Drawing.Point(112, 52);
            this.TxtAcNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo2.Name = "TxtAcNo2";
            this.TxtAcNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo2.Properties.MaxLength = 40;
            this.TxtAcNo2.Properties.ReadOnly = true;
            this.TxtAcNo2.Size = new System.Drawing.Size(214, 20);
            this.TxtAcNo2.TabIndex = 22;
            // 
            // ChkTaxInvoiceInd
            // 
            this.ChkTaxInvoiceInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTaxInvoiceInd.Location = new System.Drawing.Point(111, 96);
            this.ChkTaxInvoiceInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTaxInvoiceInd.Name = "ChkTaxInvoiceInd";
            this.ChkTaxInvoiceInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTaxInvoiceInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTaxInvoiceInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTaxInvoiceInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTaxInvoiceInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTaxInvoiceInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTaxInvoiceInd.Properties.Caption = "Is Tax Invoice Mandatory ?";
            this.ChkTaxInvoiceInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTaxInvoiceInd.ShowToolTips = false;
            this.ChkTaxInvoiceInd.Size = new System.Drawing.Size(179, 22);
            this.ChkTaxInvoiceInd.TabIndex = 25;
            // 
            // LueTaxGrpCode
            // 
            this.LueTaxGrpCode.EnterMoveNextControl = true;
            this.LueTaxGrpCode.Location = new System.Drawing.Point(109, 74);
            this.LueTaxGrpCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxGrpCode.Name = "LueTaxGrpCode";
            this.LueTaxGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxGrpCode.Properties.Appearance.Options.UseFont = true;
            this.LueTaxGrpCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxGrpCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxGrpCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxGrpCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxGrpCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxGrpCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxGrpCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxGrpCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxGrpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxGrpCode.Properties.DropDownRows = 12;
            this.LueTaxGrpCode.Properties.NullText = "[Empty]";
            this.LueTaxGrpCode.Properties.PopupWidth = 500;
            this.LueTaxGrpCode.Size = new System.Drawing.Size(374, 20);
            this.LueTaxGrpCode.TabIndex = 16;
            this.LueTaxGrpCode.ToolTip = "F4 : Show/hide list";
            this.LueTaxGrpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxGrpCode.EditValueChanged += new System.EventHandler(this.LueTaxGrpCode_EditValueChanged);
            // 
            // LblTaxGrpCode
            // 
            this.LblTaxGrpCode.AutoSize = true;
            this.LblTaxGrpCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTaxGrpCode.ForeColor = System.Drawing.Color.Black;
            this.LblTaxGrpCode.Location = new System.Drawing.Point(63, 77);
            this.LblTaxGrpCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTaxGrpCode.Name = "LblTaxGrpCode";
            this.LblTaxGrpCode.Size = new System.Drawing.Size(40, 14);
            this.LblTaxGrpCode.TabIndex = 15;
            this.LblTaxGrpCode.Text = "Group";
            this.LblTaxGrpCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkWapuInd
            // 
            this.ChkWapuInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkWapuInd.Location = new System.Drawing.Point(111, 119);
            this.ChkWapuInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkWapuInd.Name = "ChkWapuInd";
            this.ChkWapuInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkWapuInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkWapuInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkWapuInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkWapuInd.Properties.Appearance.Options.UseFont = true;
            this.ChkWapuInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkWapuInd.Properties.Caption = "WAPU";
            this.ChkWapuInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkWapuInd.ShowToolTips = false;
            this.ChkWapuInd.Size = new System.Drawing.Size(179, 22);
            this.ChkWapuInd.TabIndex = 27;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Grd1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 146);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(993, 271);
            this.panel3.TabIndex = 27;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(993, 271);
            this.Grd1.TabIndex = 28;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtLevel);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.LueParent);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(993, 146);
            this.panel4.TabIndex = 28;
            // 
            // TxtLevel
            // 
            this.TxtLevel.EnterMoveNextControl = true;
            this.TxtLevel.Location = new System.Drawing.Point(109, 118);
            this.TxtLevel.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel.Name = "TxtLevel";
            this.TxtLevel.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLevel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLevel.Properties.MaxLength = 2;
            this.TxtLevel.Size = new System.Drawing.Size(117, 20);
            this.TxtLevel.TabIndex = 22;
            this.TxtLevel.Validated += new System.EventHandler(this.TxtLevel_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(68, 120);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 14);
            this.label15.TabIndex = 21;
            this.label15.Text = "Level";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(60, 98);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 14);
            this.label8.TabIndex = 19;
            this.label8.Text = "Parent";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueParent
            // 
            this.LueParent.EnterMoveNextControl = true;
            this.LueParent.Location = new System.Drawing.Point(109, 96);
            this.LueParent.Margin = new System.Windows.Forms.Padding(5);
            this.LueParent.Name = "LueParent";
            this.LueParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.Appearance.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParent.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParent.Properties.DropDownRows = 30;
            this.LueParent.Properties.MaxLength = 40;
            this.LueParent.Properties.NullText = "[Empty]";
            this.LueParent.Properties.PopupWidth = 500;
            this.LueParent.Size = new System.Drawing.Size(374, 20);
            this.LueParent.TabIndex = 20;
            this.LueParent.ToolTip = "F4 : Show/hide list";
            this.LueParent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParent.EditValueChanged += new System.EventHandler(this.LueParent_EditValueChanged);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.ChkServiceInd);
            this.panel5.Controls.Add(this.TxtAcNo1);
            this.panel5.Controls.Add(this.ChkWapuInd);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.TxtAcNo2);
            this.panel5.Controls.Add(this.TxtAcDesc1);
            this.panel5.Controls.Add(this.ChkTaxInvoiceInd);
            this.panel5.Controls.Add(this.BtnAcNo1);
            this.panel5.Controls.Add(this.BtnAcNo2);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.TxtAcDesc2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(498, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(495, 146);
            this.panel5.TabIndex = 17;
            // 
            // ChkServiceInd
            // 
            this.ChkServiceInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkServiceInd.Location = new System.Drawing.Point(301, 96);
            this.ChkServiceInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkServiceInd.Name = "ChkServiceInd";
            this.ChkServiceInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkServiceInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkServiceInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkServiceInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkServiceInd.Properties.Appearance.Options.UseFont = true;
            this.ChkServiceInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkServiceInd.Properties.Caption = "Is Tax Service Mandatory ?";
            this.ChkServiceInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkServiceInd.ShowToolTips = false;
            this.ChkServiceInd.Size = new System.Drawing.Size(179, 22);
            this.ChkServiceInd.TabIndex = 26;
            // 
            // FrmTax
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 417);
            this.Name = "FrmTax";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInvoiceInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxGrpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWapuInd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkServiceInd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtTaxRate;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtTaxName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtTaxCode;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo2;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo1;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc2;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc1;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo1;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo2;
        private DevExpress.XtraEditors.CheckEdit ChkTaxInvoiceInd;
        private System.Windows.Forms.Label LblTaxGrpCode;
        private DevExpress.XtraEditors.LookUpEdit LueTaxGrpCode;
        private DevExpress.XtraEditors.CheckEdit ChkWapuInd;
        private System.Windows.Forms.Panel panel3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtLevel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LueParent;
        private DevExpress.XtraEditors.CheckEdit ChkServiceInd;
    }
}