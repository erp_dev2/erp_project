﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemPropertyDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmItemProperty mFrmParent;
        private string mSQL;

        #endregion

        #region Constructor

        public FrmItemPropertyDlg(FrmItemProperty FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Item's Source";
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetSQL();
            SetGrd();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 32;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code", 
                        "Item's Name",
                        "Foreign Name",
                        "Local Code",
                        "Active",

                        //6-10
                        "Category",
                        "Sub Category",
                        "Group",
                        "HS Code",
                        "UoM",

                        //11-15
                        "Length",
                        "UoM",
                        "Height",
                        "UoM",
                        "Width",
                       
                        //16-20
                        "UoM",
                        "Diameter",
                        "UoM",
                        "Volume",
                        "UoM",
                        
                        //21-25
                        "Inventory",
                        "Sales",
                        "Purchase",
                        "Fixed"+Environment.NewLine+"Asset",
                        "Planning",

                        //26-30
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",

                        //31
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 300, 200, 100, 50, 
                        
                        //6-10
                        200, 150, 120, 150, 100,   
                        
                        //11-15
                        80, 100, 80, 100, 80,   

                        //16-20
                        100, 80, 100, 80, 100,   

                        //21-25
                        100, 100, 100, 100, 100, 

                        //26-30
                        100, 100, 100, 100, 100, 
                        
                        //31
                        100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 5, 21, 22, 23, 24, 25 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 15, 17, 19 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 });
            Sm.GrdFormatDate(Grd1, new int[] { 27, 30 });
            Sm.GrdFormatTime(Grd1, new int[] { 28, 31 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.ForeignName, A.ItCodeInternal, A.ActInd, B.ItCtName, ");
            SQL.AppendLine("C.ItScName, D.ItGrpName, A.InventoryUomCode, ");
            SQL.AppendLine("A.Length, A.LengthUomCode, A.Height, A.HeightUomCode, A.Width, A.WidthUomCode, ");
            SQL.AppendLine("A.Diameter, A.DiameterUomCode, A.Volume, A.VolumeUomCode, A.HSCode, ");
            SQL.AppendLine("A.InventoryItemInd, A.SalesItemInd, A.PurchaseItemInd, A.FixedItemInd, A.PlanningItemInd, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Left Join TblItemSubCategory C On A.ItScCode=C.ItScCode ");
            SQL.AppendLine("Left Join TblItemGroup D On A.ItGrpCode=D.ItGrpCode ");


            mSQL = SQL.ToString();
        }
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ItCodeInternal" });


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ItName;",
                        new string[]
                        {
                            //0
                            "ItCode", 
                                
                            //1-5
                            "ItName", "ForeignName", "ItCodeInternal", "ActInd", "ItCtName",
                            
                            //6-10
                             "ItScName", "ItGrpName", "HSCode",  "InventoryUomCode", "Length", 
                            
                            //11-15
                            "LengthUomCode", "Height",  "HeightUomCode", "Width", "WidthUomCode",  
                            
                            //16-20
                            "Diameter", "DiameterUomCode", "Volume", "VolumeUomCode", "InventoryItemInd", 
                            
                            //21-25
                            "SalesItemInd", "PurchaseItemInd", "FixedItemInd", "PlanningItemInd", "CreateBy",  
                            
                            //26-28
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 30, 28);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 31, 28);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 2))
            {
                //mFrmParent.ClearData();
                mFrmParent.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtItName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);

            }
            this.Hide();
        }


        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event
        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItem_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion


    }
}


