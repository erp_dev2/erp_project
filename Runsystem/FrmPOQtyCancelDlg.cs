﻿#region Update
/* 
    11/05/2018 [ARI] tambah filter departmen dan klom department
    29/05/2018 [TKG] tambah status PO.
    05/01/2022 [RIS/PHT] Membuat terfilter berdasarkan department dan item category dengan param IsFilterByDept dan IsFilterByItCt
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPOQtyCancelDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPOQtyCancel mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPOQtyCancelDlg(FrmPOQtyCancel FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e); this.Text = "List Of PO";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
                //Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDept ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "No",

                        //1-5
                        "",
                        "PO#",
                        "PO"+Environment.NewLine+"DNo",
                        "Date",
                        "Item's"+Environment.NewLine+"Code",

                        //6-10
                        "",
                        "Item's Name",    
                        "PO"+Environment.NewLine+"Quantity",
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "UoM",

                        //11-12
                        "Vendor",
                        "Department"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        //1-5
                        20, 100, 20, 100, 100, 
                        //6-10
                        20, 150, 150, 100, 80, 
                        //11-12
                        150, 170 
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColButton(Grd1, new int[] { 1, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 6 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.DocNo, B.DNo, A.DocDt, D.ItCode, E.ItName, F.VdName, E.PurchaseUomCode, ");
            SQL.AppendLine("    B.Qty As QtyPO, Round(B.Qty-IfNull(G.RecvVdQty, 0)-IfNull(H.POQtyCancelQty, 0)+IfNull(I.DOVdQty, 0), 4) As QtyOutstanding, J.DeptName ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd<>'F' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr D2 On D.DocNo=D2.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (D2.SiteCode Is Null Or ( ");
                SQL.AppendLine("    D2.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(D2.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=E.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Inner Join TblVendor F On A.VdCode=F.VdCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo, T1.PODNo, Sum(T1.QtyPurchase) As RecvVdQty ");
            SQL.AppendLine("        From TblRecvVdDtl T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Inner Join TblPOHdr T3 On T2.DocNo=T3.DocNo And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) G On A.DocNo=G.PODocNo And B.DNo=G.PODNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo, T1.PODNo, Sum(T1.Qty) As POQtyCancelQty ");
            SQL.AppendLine("        From TblPOQtyCancel T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Inner Join TblPOHdr T3 On T2.DocNo=T3.DocNo And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) H On A.DocNo=H.PODocNo And B.DNo=H.PODNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T4.DocNo, T4.DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As DOVdQty ");
            SQL.AppendLine("        From TblDOVdHdr T1 ");
            SQL.AppendLine("        Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo And T3.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblPODtl T4 On T3.PODocNo=T4.DocNo And T3.PODNo=T4.DNo And T4.CancelInd='N' And T4.ProcessInd<>'F' ");
            SQL.AppendLine("        Inner Join TblPOHdr T5 On T4.DocNo=T5.DocNo And T5.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where T1.ReplacedItemInd='Y' ");
            SQL.AppendLine("        Group By T4.DocNo, T4.DNo ");
            SQL.AppendLine("    ) I On A.DocNo=I.DocNo And B.DNo=I.DNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("    Inner Join TblQtHdr J On C.QtDocNo=J.DocNo ");
                SQL.AppendLine("    And (J.PtCode Is Null Or ( ");
                SQL.AppendLine("    And J.PtCode Is Not Null ");
                SQL.AppendLine("    And J.PtCode In (");
                SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("    INNER Join TblDepartment J On D2.DeptCode=J.DeptCode ");
                SQL.AppendLine("        And Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupDepartment ");
                SQL.AppendLine("            Where DeptCode=J.DeptCode ");
                SQL.AppendLine("            And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("            ) ");
            }
            else
            {
                SQL.AppendLine("    Left Join TblDepartment J On D2.DeptCode=J.DeptCode ");
            }
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.Status='A' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "E.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "D2.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + ") T WHere QtyOutstanding>0 Order By T.DocDt, T.DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo",  

                            //1-5
                            "DNo", "DocDt", "ItCode", "ItName", "QtyPO", 

                            //6-9
                            "QtyOutStanding", "PurchaseUomCode", "VdName", "DeptName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtPODocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtPODNo.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtItName.EditValue= Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.TxtQtyPO.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8), 0);
                mFrmParent.TxtVdCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11);
                mFrmParent.TxtOutstandingQtyPO.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9), 0);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDept ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion

    }
}
