﻿#region Update
/*
    03/07/2018 [HAR] tambah sum
   13/02/2019 [MEY] Menambahkan kolom Machine disamping SFC#
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptMaterialConsumptionVSReceivingMaterial2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterByDept = false;

        #endregion

        #region Constructor

        public FrmRptMaterialConsumptionVSReceivingMaterial2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                SetLueDeptCode(ref LueDeptCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("select Z.Wc, Z3.DocName, Z.SfcDocNo, Z.DocDt, Z.EmpCode, Z2.EmpName, Z2.DeptCode, ");
            SQL.AppendLine("SUM(if(Z.Kode = '1', QtyBom, 0)) As BOMQty,");
            SQL.AppendLine("SUM(if(Z.Kode = '2', QtyBom, 0)) As SfcQty, Z5.AssetName ");
            SQL.AppendLine("From (");
            SQL.AppendLine(" Select G.DocNo As SfcDocNo, G.DocDt As DocDt, A.DocNo As PPDocNo, E.WorkCenterDocNo As Wc, ");
            SQL.AppendLine("        F.DocCode As EmpCode, F.Qty As QtyBom, '1' as kode ");
            SQL.AppendLine("        From TblPPHdr A  ");
            SQL.AppendLine("        Inner Join TblPPDtl B On A.DocNo = B.DocNo   ");
            SQL.AppendLine("        Inner Join tblProductionOrderHdr C On B.ProductionOrderDocNo = C.DocnO ");
            SQL.AppendLine("        Inner Join TblproductionorderDtl D On C.DocNo = D.DocNo  ");
            SQL.AppendLine("        Inner Join tblProductionRoutingDtl E On C.ProductionRoutingDocNo = E.DocNo ");
            SQL.AppendLine("        And D.ProductionRoutingDNo = E.Dno   ");
            SQL.AppendLine("        Inner Join TblBomDtl F On D.BomDOcNo = F.Docno And F.DocType='3' ");
            SQL.AppendLine("        Left Join TblShopFloorControlHdr G ");
            SQL.AppendLine("            On A.DocNo = G.PPDocNo ");
            SQL.AppendLine("            And E.WorkcenterDocno=G.WorkcenterDocno ");
            SQL.AppendLine("            And G.CancelInd='N' ");
            SQL.AppendLine("            And (G.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        Where A.CancelInd = 'N'  ");
            SQL.AppendLine("        union All ");
            SQL.AppendLine("        Select A.DocNo As SFCDocNo, A.DocDt, A.PPDocNo, A.WOrkcenterDocNo, B.BomCode, B.Qty, '2' as kode ");
            SQL.AppendLine("        From TblShopFloorControlHdr A ");
            SQL.AppendLine("        Inner Join TblShopFloorCOntrol2Dtl B On A.DocNo = B.DocNo And B.BomType='3' ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine(") Z ");
            SQL.AppendLine("Inner Join TblEmployee Z2 On Z.EmpCode = Z2.EmpCode ");
            SQL.AppendLine("Inner Join TblWorkcenterHdr Z3 On Z.Wc = Z3.DocNo ");
            SQL.AppendLine("Inner Join TblShopFloorControlHdr Z4 On Z4.DocNo = Z.SfcDocNo ");
            SQL.AppendLine("Left Join TblAsset Z5 On Z5.AssetCode=Z4.MachineCode ");        
            SQL.AppendLine("where Z.SfcDocNo is not null ");
            SQL.AppendLine("Group By Z.SfcDocNo, Z.DocDt, Z.PPDocNo, Z.Wc, Z.EmpCode ");
            SQL.AppendLine(")X ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Work center#", 
                        "Work center",
                        "",
                        "SFC#",
                        "",

                        //6-10
                        "Machine",
                        "Date",
                        "Labour"+Environment.NewLine+"Code",
                        "",
                        "Labour",
                       

                        //11
                         "Quantity"+Environment.NewLine+"(BOM)",
                        "Quantity"+Environment.NewLine+"(SFC)",
                    },
                    new int[]
                    {
                        40, 
                        130, 200, 20, 150, 20,
                        200, 100, 80, 20, 200,
                         100, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3, 5, 9 });
            Sm.GrdFormatDec(Grd1, new int[] {  11, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 6, 7, 8, 10, 11, 12 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 8, 9 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 8, 9}, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 1=1 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterName.Text, new string[]{ "X.WC", "X.DocName"});
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "X.DeptCode", true);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Group By X.WC, X.DocName, X.SfcDocNo, X.DocDt, X.EmpCode, X.EmpName",
                        new string[]
                        {
                            //0
                            "WC", 

                            //1-5
                            "DocName", "SfcDocNo", "AssetName", "DocDt", "EmpCode", 
                            
                            //6-10
                            "EmpName", "BOMQty", "SFCQty", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 12});
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        #endregion

        #region Additional Method

        private void SetLueDeptCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            SQL.AppendLine("Where T.ActInd = 'Y' ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By DeptName; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkWorkCenterName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work center");
        }

        private void TxtWorkCenterName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion
    }
}
