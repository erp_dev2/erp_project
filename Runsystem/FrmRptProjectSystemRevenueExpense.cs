﻿#region Update
/*
    27/02/2023 [WED/MNET] new reporting
    06/03/2023 [WED/MNET] customer belum terfilter
    05/04/2023 [WED/MNET] tmabah Revenue Paid
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectSystemRevenueExpense : RunSystem.FrmBase6
    {
        #region Field

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        internal List<FrmRptProjectBudgetRealization.DroppingRealization> ml2 = null;

        #endregion

        #region Constructor

        public FrmRptProjectSystemRevenueExpense(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                Sm.SetDteCurrentDate(DteAchievementDt);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueCtCode(ref LueCtCode);
                ml2 = new List<FrmRptProjectBudgetRealization.DroppingRealization>();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            //mItGrpCodeForRemuneration = Sm.GetParameter("ItGrpCodeForRemuneration");
            //mItGrpCodeForDirectCost = Sm.GetParameter("ItGrpCodeForDirectCost");
            //mItGrpCodeForIndirectCost = Sm.GetParameter("ItGrpCodeForIndirectCost");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.DocNo, E.ProjectName, F.CtName, C.BOQDocNo, D.EstRevAmt, D.EstResAmt, A.TotalResource, ");
            SQL.AppendLine("IfNull(G.PlannedTask, 0.00) PlannedTask, IfNull(G.SettledTask, 0.00) SettledTask, ");
            SQL.AppendLine("IfNull(G.PlannedRevAmt, 0.00) PlannedRevAmt, IfNull(G.SettledRevAmt, 0.00) SettledRevAmt, ");
            SQL.AppendLine("(IfNull(G.PlannedTask, 0.00) * A.RemunerationAmt * 0.01) PlannedRemunerationAmt, (IfNull(G.SettledTask, 0.00) * A.RemunerationAmt * 0.01) SettledRemunerationAmt, ");
            SQL.AppendLine("(IfNull(G.PlannedTask, 0.00) * A.DirectCostAmt * 0.01) PlannedDirectCostAmt, (IfNull(G.SettledTask, 0.00) * A.DirectCostAmt * 0.01) SettledDirectCostAmt, ");
            SQL.AppendLine("(IfNull(G.PlannedTask, 0.00) * A.IndirectCostAmt * 0.01) PlannedIndirectCostAmt, (IfNull(G.SettledTask, 0.00) * A.IndirectCostAmt * 0.01) SettledIndirectCostAmt, ");
            SQL.AppendLine("H.VCIncomingDocNo, IfNull(H.VCIncomingAmt, 0.00) VCIncomingAmt ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("    And A.Status = 'A' And A.ProcessInd = 'F' And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.SOCDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHDr D On C.BOQDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer F On E.CtCode = F.CtCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, Sum(T2.BobotPercentage) PlannedTask, IfNull(T3.SettledTask, 0.00) SettledTask, ");
            SQL.AppendLine("    Sum(T2.Amt) PlannedRevAmt, IfNull(T4.SettledRevAmt, 0.00) SettledRevAmt ");
            SQL.AppendLine("    From TblProjectImplementationHdr T1 ");
            SQL.AppendLine("    Inner Join TblProjectImplementationDtl5 T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.Status = 'A' And T1.ProcessInd = 'F' And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And T2.PlanEndDt Is Not Null And T2.PlanEndDt <= @AchievementDt ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X1.DocNo, Sum(X2.BobotPercentage) SettledTask ");
            SQL.AppendLine("        From TblProjectImplementationHdr X1 ");
            SQL.AppendLine("        Inner Join TblProjectImplementationDtl5 X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("            And X1.Status = 'A' And X1.ProcessInd = 'F' And X1.CancelInd = 'N' ");
            SQL.AppendLine("            And X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            And X2.SettledInd = 'Y' ");
            SQL.AppendLine("            And X2.PlanEndDt Is Not Null And X2.PlanEndDt <= @AchievementDt ");
            SQL.AppendLine("        Group By X1.DocNo ");
            SQL.AppendLine("    ) T3 On T1.DocNo = T3.DocNo ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X1.DocNo, Sum(X2.SettledRevAmt) SettledRevAmt ");
            SQL.AppendLine("        From TblProjectImplementationHdr X1 ");
            SQL.AppendLine("        Inner Join TblProjectImplementationDtl5 X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("            And X1.Status = 'A' And X1.ProcessInd = 'F' And X1.CancelInd = 'N' ");
            SQL.AppendLine("            And X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            And X2.SettledInd = 'Y' ");
            SQL.AppendLine("            And X2.PlanEndDt Is Not Null And X2.PlanEndDt <= @AchievementDt ");
            SQL.AppendLine("        Group By X1.DocNo ");
            SQL.AppendLine("    ) T4 On T1.DocNo = T4.DocNo ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") G On A.DocNo = G.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Group_Concat(Distinct A.DocNo) VCIncomingDocNo, Sum(A.Amt) VCIncomingAmt, E.ProjectImplementationDocNo ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentHdr B On A.VoucherRequestDocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("        And A.CancelInd = 'N' ");
            SQL.AppendLine("        And B.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl C On B.DocNo = C.DocNo ");
            SQL.AppendLine("    Inner Join TblSalesInvoice5Hdr D On C.InvoiceDocNo = D.DocNo ");
            SQL.AppendLine("        And D.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblSalesInvoice5Dtl E On C.InvoiceDocNo = E.DocNo ");
            SQL.AppendLine("    Inner Join TblProjectImplementationHdr F On E.ProjectImplementationDocNo = F.DocNo ");
            SQL.AppendLine("        And F.Status = 'A' And F.ProcessInd = 'F' And F.CancelInd = 'N' ");
            SQL.AppendLine("        And F.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By E.ProjectImplementationDocNo ");
            SQL.AppendLine(") H On A.DocNo = H.ProjectImplementationDocNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 41;
            Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No.";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 80;
            Grd1.Header.Cells[0, 1].Value = "Date";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 180;
            Grd1.Header.Cells[0, 2].Value = "Project Implementation#";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 20;
            Grd1.Header.Cells[0, 3].Value = "";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 120;
            Grd1.Header.Cells[0, 4].Value = "Project Name";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Cols[5].Width = 120;
            Grd1.Header.Cells[0, 5].Value = "Customer";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Grd1.Cols[6].Width = 180;
            Grd1.Header.Cells[0, 6].Value = "BOQ#";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;

            Grd1.Cols[7].Width = 20;
            Grd1.Header.Cells[0, 7].Value = "";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;

            Grd1.Cols[8].Width = 150;
            Grd1.Header.Cells[0, 8].Value = "Revenue";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 2;

            Grd1.Cols[9].Width = 150;
            Grd1.Header.Cells[0, 9].Value = "COGS Amount";
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 9].SpanRows = 2;

            Grd1.Cols[10].Width = 120;
            Grd1.Header.Cells[0, 10].Value = "Plan Achievement"+Environment.NewLine+"( % )";
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 10].SpanRows = 2;

            Grd1.Cols[11].Width = 120;
            Grd1.Header.Cells[0, 11].Value = "Settled Achievement" + Environment.NewLine + "( % )";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 2;

            Grd1.Cols[12].Width = 120;
            Grd1.Header.Cells[0, 12].Value = "Deviation" + Environment.NewLine + "( % )";
            Grd1.Header.Cells[0, 12].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 12].SpanRows = 2;

            Grd1.Cols[13].Width = 150;
            Grd1.Header.Cells[0, 13].Value = "Revenue Planned";
            Grd1.Header.Cells[0, 13].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 13].SpanRows = 2;

            Grd1.Cols[14].Width = 150;
            Grd1.Header.Cells[0, 14].Value = "Revenue Recorded";
            Grd1.Header.Cells[0, 14].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 14].SpanRows = 2;

            Grd1.Cols[15].Width = 150;
            Grd1.Header.Cells[0, 15].Value = "Revenue Deviation";
            Grd1.Header.Cells[0, 15].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 15].SpanRows = 2;

            Grd1.Cols[16].Width = 150;
            Grd1.Header.Cells[0, 16].Value = "Revenue Paid";
            Grd1.Header.Cells[0, 16].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 16].SpanRows = 2;

            Grd1.Cols[17].Width = 20;
            Grd1.Header.Cells[0, 17].Value = "";
            Grd1.Header.Cells[0, 17].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 17].SpanRows = 2;

            //Grd1.Header.Cells[1, 16].Value = "Revenue Paid";
            //Grd1.Header.Cells[1, 16].TextAlign = iGContentAlignment.TopCenter;
            //Grd1.Header.Cells[1, 16].SpanCols = 2;
            //Grd1.Header.Cells[0, 16].Value = "Amount";
            //Grd1.Header.Cells[0, 17].Value = "";
            //Grd1.Cols[16].Width = 150;
            //Grd1.Cols[17].Width = 20;

            Grd1.Cols[18].Width = 150;
            Grd1.Header.Cells[0, 18].Value = "COGS Planned";
            Grd1.Header.Cells[0, 18].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 18].SpanRows = 2;

            Grd1.Cols[19].Width = 150;
            Grd1.Header.Cells[0, 19].Value = "COGS Settled";
            Grd1.Header.Cells[0, 19].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 19].SpanRows = 2;

            Grd1.Cols[20].Width = 150;
            Grd1.Header.Cells[0, 20].Value = "COGS Realized";
            Grd1.Header.Cells[0, 20].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 20].SpanRows = 2;

            Grd1.Cols[21].Width = 20;
            Grd1.Header.Cells[0, 21].Value = "";
            Grd1.Header.Cells[0, 21].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 21].SpanRows = 2;

            Grd1.Header.Cells[1, 22].Value = "Planned Profit Margin";
            Grd1.Header.Cells[1, 22].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 22].SpanCols = 2;
            Grd1.Header.Cells[0, 22].Value = "Profit Margin" + Environment.NewLine + "(Revenue Planned - COGS Planned)";
            Grd1.Header.Cells[0, 23].Value = "Profit Margin" + Environment.NewLine + "( % )";
            Grd1.Cols[22].Width = 150;
            Grd1.Cols[23].Width = 150;

            Grd1.Header.Cells[1, 24].Value = "Realized Profit Margin";
            Grd1.Header.Cells[1, 24].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 24].SpanCols = 4;
            Grd1.Header.Cells[0, 24].Value = "Profit Margin" + Environment.NewLine + "(Revenue Recorded - COGS Settled)";
            Grd1.Header.Cells[0, 25].Value = "Profit Margin" + Environment.NewLine + "( % )";
            Grd1.Header.Cells[0, 26].Value = "Profit Margin" + Environment.NewLine + "(Revenue Recorded - COGS Realized)";
            Grd1.Header.Cells[0, 27].Value = "Profit Margin" + Environment.NewLine + "( % )";
            Grd1.Cols[24].Width = 150;
            Grd1.Cols[25].Width = 150;
            Grd1.Cols[26].Width = 150;
            Grd1.Cols[27].Width = 150;

            Grd1.Header.Cells[1, 28].Value = "COGS Breakdown";
            Grd1.Header.Cells[1, 28].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 28].SpanCols = 9;
            Grd1.Header.Cells[0, 28].Value = "Remuneration Planned";
            Grd1.Header.Cells[0, 29].Value = "Remuneration Settled";
            Grd1.Header.Cells[0, 30].Value = "Remuneration Realized";
            Grd1.Header.Cells[0, 31].Value = "Direct Cost Planned";
            Grd1.Header.Cells[0, 32].Value = "Direct Cost Settled";
            Grd1.Header.Cells[0, 33].Value = "Direct Cost Realized";
            Grd1.Header.Cells[0, 34].Value = "Indirect Cost Planned";
            Grd1.Header.Cells[0, 35].Value = "Indirect Cost Settled";
            Grd1.Header.Cells[0, 36].Value = "Indirect Cost Realized";
            Grd1.Cols[28].Width = 150;
            Grd1.Cols[29].Width = 150;
            Grd1.Cols[30].Width = 150;
            Grd1.Cols[31].Width = 150;
            Grd1.Cols[32].Width = 150;
            Grd1.Cols[33].Width = 150;
            Grd1.Cols[34].Width = 150;
            Grd1.Cols[35].Width = 150;
            Grd1.Cols[36].Width = 150;

            Grd1.Cols[37].Width = 0;
            Grd1.Header.Cells[0, 37].Value = "Resource Amount";
            Grd1.Header.Cells[0, 37].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 37].SpanRows = 2;

            Grd1.Cols[38].Width = 0;
            Grd1.Header.Cells[0, 38].Value = "Realization DocNo";
            Grd1.Header.Cells[0, 38].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 38].SpanRows = 2;

            Grd1.Cols[39].Width = 0;
            Grd1.Header.Cells[0, 39].Value = "Dropping Request DocType";
            Grd1.Header.Cells[0, 39].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 39].SpanRows = 2;

            Grd1.Cols[40].Width = 0;
            Grd1.Header.Cells[0, 40].Value = "VC Incoming#";
            Grd1.Header.Cells[0, 40].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 40].SpanRows = 2;

            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;

            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3, 7, 17, 21 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 });
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColInvisible(Grd1, new int[] { 38, 39, 40 });
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                if (Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                    Sm.IsDteEmpty(DteAchievementDt, "Achievement Date")
                ) return;

                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = " Where 0 = 0 ";

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "E.CtCode", true);

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParamDt(ref cm, "@AchievementDt", Sm.GetDte(DteAchievementDt));

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt, A.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocDt",  
                        //1-5
                        "DocNo", "ProjectName", "CtName", "BOQDocNo", "EstRevAmt",
                        //6-10
                        "EstResAmt", "PlannedTask", "SettledTask", "PlannedRevAmt", "SettledRevAmt",
                        //11-15
                        "PlannedRemunerationAmt", "SettledRemunerationAmt", "PlannedDirectCostAmt", "SettledDirectCostAmt", "PlannedIndirectCostAmt",
                        //16-19
                        "SettledIndirectCostAmt", "TotalResource", "VCIncomingDocNo", "VCIncomingAmt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0); // date
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1); // prji docno
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2); // project name
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3); // customer name
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4); // boq docno
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5); // estimated revenue amount [revenue]
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6); // estimated resource amount [cogs]
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7); // plan achievement (%)
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8); // settled achievement (%)
                        Grd.Cells[Row, 12].Value = Sm.DrDec(dr, c[7]) - Sm.DrDec(dr, c[8]); // deviation achievement (%)
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9); // revenue planned
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10); // revenue recorded
                        Grd.Cells[Row, 15].Value = Sm.DrDec(dr, c[9]) - Sm.DrDec(dr, c[10]); // revenue deviation
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 19); // Revenue Paid
                        Grd.Cells[Row, 18].Value = Sm.DrDec(dr, c[17]) * Sm.DrDec(dr, c[7]) * 0.01m; // cogs planned
                        Grd.Cells[Row, 19].Value = Sm.DrDec(dr, c[17]) * Sm.DrDec(dr, c[8]) * 0.01m; // cogs settled
                        Grd.Cells[Row, 20].Value = 0m; // cogs realized

                        //Profit
                        Grd.Cells[Row, 22].Value = Sm.GetGrdDec(Grd, Row, 13) - Sm.GetGrdDec(Grd, Row, 18);
                        Grd.Cells[Row, 23].Value = 0m;
                        if (Sm.GetGrdDec(Grd, Row, 22) != 0m && Sm.GetGrdDec(Grd, Row, 13) != 0m) Grd.Cells[Row, 23].Value = (Sm.GetGrdDec(Grd, Row, 22) / Sm.GetGrdDec(Grd, Row, 13)) * 100m;
                        
                        Grd.Cells[Row, 24].Value = Sm.GetGrdDec(Grd, Row, 14) - Sm.GetGrdDec(Grd, Row, 19);
                        Grd.Cells[Row, 25].Value = 0m;
                        if (Sm.GetGrdDec(Grd, Row, 24) != 0m && Sm.GetGrdDec(Grd, Row, 14) != 0m) Grd.Cells[Row, 25].Value = (Sm.GetGrdDec(Grd, Row, 24) / Sm.GetGrdDec(Grd, Row, 14)) * 100m;

                        Grd.Cells[Row, 26].Value = Grd.Cells[Row, 27].Value = 0m;

                        //COGS Breakdown
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 16);
                        Grd.Cells[Row, 30].Value = Grd.Cells[Row, 33].Value = Grd.Cells[Row, 36].Value = 0m;

                        Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 17);
                        Grd.Cells[Row, 38].Value = Grd.Cells[Row, 39].Value = string.Empty;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 18);
                    }, true, false, false, false
                );
                if (Grd1.Rows.Count > 0) ProcessRealizationDocs();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length > 0)
            {
                var f = new FrmProjectImplementation3(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length > 0)
            {
                var f = new FrmBOQ3(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 38).Length > 0)
            {
                var f = new FrmRptProjectSystemRevenueExpenseDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 2), ref ml2);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 40).Length > 0)
            {
                var f = new FrmRptProjectSystemRevenueExpenseDlg2(this, Sm.GetGrdStr(Grd1, e.RowIndex, 40));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void CalculateRealization(int Row)
        {
            Grd1.Cells[Row, 26].Value = Sm.GetGrdDec(Grd1, Row, 14) - Sm.GetGrdDec(Grd1, Row, 20);
            Grd1.Cells[Row, 27].Value = 0m;
            if (Sm.GetGrdDec(Grd1, Row, 26) != 0m && Sm.GetGrdDec(Grd1, Row, 14) != 0m) Grd1.Cells[Row, 27].Value = (Sm.GetGrdDec(Grd1, Row, 26) / Sm.GetGrdDec(Grd1, Row, 14)) * 100m; 
        }

        private void CalculateCOGSBreakdown(int Row, ref List<FrmRptProjectBudgetRealization.DroppingRealization> l2)
        {
            var l3 = l2.GroupBy(g => new { g.ProjectImplementationDocNo, g.DirectCostInd, g.RemunerationInd, g.IndirectCostInd })
                .Select(t => new FrmRptProjectBudgetRealization.DroppingRealization()
                {
                    ProjectImplementationDocNo = t.Key.ProjectImplementationDocNo,
                    RemunerationInd = t.Key.RemunerationInd,
                    DirectCostInd = t.Key.DirectCostInd,
                    IndirectCostInd = t.Key.IndirectCostInd,
                    RealizationAmt = t.Sum(s => s.RealizationAmt)
                }).ToList();

            foreach (var x in l3.Where(w => w.ProjectImplementationDocNo == Sm.GetGrdStr(Grd1, Row, 2)))
            {
                if (x.RemunerationInd.Length > 0) Grd1.Cells[Row, 30].Value = x.RealizationAmt;
                if (x.DirectCostInd.Length > 0) Grd1.Cells[Row, 33].Value = x.RealizationAmt;
                if (x.IndirectCostInd.Length > 0) Grd1.Cells[Row, 36].Value = x.RealizationAmt;
            }

            l3.Clear();
        }

        private void ProcessRealizationDocs()
        {
            var l2 = new List<FrmRptProjectBudgetRealization.DroppingRealization>();
            var l3 = new List<FrmRptProjectBudgetRealization.DroppingRealization>();

            l2.Clear(); l3.Clear();

            string PRJIDocNo = string.Empty;
            string DroppingRequestDocNo = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    if (PRJIDocNo.Length > 0) PRJIDocNo += ",";
                    PRJIDocNo += Sm.GetGrdStr(Grd1, Row, 2);
                }
            }

            PrepDroppingRequest(ref l2, PRJIDocNo, ref DroppingRequestDocNo);
            GetRealizationDocs(ref l2, ref l3, ref DroppingRequestDocNo);
            DisplayRealizationDocs(ref l2, ref l3);

            ml2.Clear();
            ml2 = l2;
        }

        private void DisplayRealizationDocs(ref List<FrmRptProjectBudgetRealization.DroppingRealization> l2, ref List<FrmRptProjectBudgetRealization.DroppingRealization> l3)
        {
            for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    foreach (var x in l3.Where(w => w.ProjectImplementationDocNo == Sm.GetGrdStr(Grd1, Row, 2)))
                    {
                        Grd1.Cells[Row, 20].Value = x.RealizationAmt;
                        Grd1.Cells[Row, 38].Value = x.RealizationDocNo;

                        CalculateRealization(Row);
                        CalculateCOGSBreakdown(Row, ref l2);
                    }
                }
            }
        }

        private void GetRealizationDocs(
            ref List<FrmRptProjectBudgetRealization.DroppingRealization> l2,
            ref List<FrmRptProjectBudgetRealization.DroppingRealization> l3,
            ref string DroppingRequestDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.* From ( ");

            SQL.AppendLine(Sm.QueryRealizationDocs());

            SQL.AppendLine(") T ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DroppingRequestDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "DocNo",

                    //1-4
                    "Realization",
                    "DocType",
                    "RealizationDocDt",
                    "RealizationAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in l2.Where(w =>
                            w.DroppingRequestDocNo == Sm.DrStr(dr, c[0]) &&
                            w.DocType == Sm.DrStr(dr, c[2]))
                            )
                        {
                            x.RealizationDocNo = Sm.DrStr(dr, c[1]);
                            x.RealizationDocDt = Sm.DrStr(dr, c[3]);
                            x.RealizationAmt = Sm.DrDec(dr, c[4]);
                        }
                    }
                }
                dr.Close();
            }

            if (l2.Count > 0)
            {
                l3 = l2.GroupBy(g => new { g.ProjectImplementationDocNo })
                    .Select(t => new FrmRptProjectBudgetRealization.DroppingRealization()
                    {
                        ProjectImplementationDocNo = t.Key.ProjectImplementationDocNo,
                        DocType = string.Empty,
                        RemunerationInd = string.Join(string.Empty, t.Select(i => i.RemunerationInd).ToArray()),
                        DirectCostInd = string.Join(string.Empty, t.Select(i => i.DirectCostInd).ToArray()),
                        IndirectCostInd = string.Join(string.Empty, t.Select(i => i.IndirectCostInd).ToArray()),
                        RealizationDocNo = string.Join(",", t.Select(i => i.RealizationDocNo).ToArray()),
                        RealizationDocDt = string.Join(",", t.Select(i => i.RealizationDocDt).ToArray()),
                        RealizationAmt = t.Sum(s => s.RealizationAmt)
                    }).ToList();
            }
        }

        private void PrepDroppingRequest(
            ref List<FrmRptProjectBudgetRealization.DroppingRealization> l2,
            string PRJIDocNo, ref string DroppingRequestDocNo
            )
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct A.DocNo, F.DocNo DroppingRequestDocNo, F.DocType DroppingRequestType, ");
            SQL.AppendLine("H.ParValue As RemunerationInd, I.ParValue As DirectCostInd, J.ParValue As IndirectCostInd ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.DocNo = B.PRJIDocNo ");
            SQL.AppendLine("    And A.Status = 'A' And A.ProcessInd = 'F' And A.CancelInd = 'N' ");
            SQL.AppendLine("    And Find_In_Set(A.DocNo, @PRJIDocNo) ");
            SQL.AppendLine("Inner Join TblProjectImplementationDtl2 C On A.DocNo = C.DocNo And B.ResourceItCode = C.ResourceItCode ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPDtl D On B.DocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblDroppingRequestDtl E On D.DocNo = E.PRBPDocNo And D.DNo = E.PRBPDNo ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr F On E.DocNo = F.DocNo And F.CancelInd = 'N' And F.Status = 'A' And F.ProcessInd = 'F' ");
            SQL.AppendLine("Inner Join TblItem G On B.ResourceItCode = G.ItCode ");
            SQL.AppendLine("Left Join TblParameter H On H.ParCode = 'ItGrpCodeForRemuneration' And H.ParValue Is Not Null And Find_In_Set(G.ItGrpCode, H.ParValue) ");
            SQL.AppendLine("Left Join TblParameter I On I.ParCode = 'ItGrpCodeForDirectCost' And I.ParValue Is Not Null And Find_In_Set(G.ItGrpCode, I.ParValue) ");
            SQL.AppendLine("Left Join TblParameter J On J.ParCode = 'ItGrpCodeForIndirectCost' And J.ParValue Is Not Null And Find_In_Set(G.ItGrpCode, J.ParValue) ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DroppingRequestDocNo", "DroppingRequestType", "RemunerationInd", "DirectCostInd", "IndirectCostInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new FrmRptProjectBudgetRealization.DroppingRealization()
                        {
                            ProjectImplementationDocNo = Sm.DrStr(dr, c[0]),
                            DroppingRequestDocNo = Sm.DrStr(dr, c[1]),
                            DocType = Sm.DrStr(dr, c[2]),
                            RealizationDocNo = string.Empty,
                            RealizationDocDt = string.Empty,
                            RealizationAmt = 0m,
                            RemunerationInd = Sm.DrStr(dr, c[3]),
                            DirectCostInd = Sm.DrStr(dr, c[4]),
                            IndirectCostInd = Sm.DrStr(dr, c[5]),
                        });

                        if (DroppingRequestDocNo.Length > 0) DroppingRequestDocNo += ",";
                        DroppingRequestDocNo += Sm.DrStr(dr, c[1]);
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
