﻿#region Update
/*
    29/12/2021 [TKG/GSS] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSalaryDetailDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptEmpSalaryDetail mFrmParent;
        private string mSQL = string.Empty, mPayrunCode = string.Empty, mEmpCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptEmpSalaryDetailDlg(
            FrmRptEmpSalaryDetail FrmParent, 
            String PayrunCode, 
            string EmpCode
            )
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPayrunCode = PayrunCode;
            mEmpCode = EmpCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                base.FrmLoad(sender, e);
                
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 60;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.RowHeader.Visible = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Date", 
                        "Grade"+Environment.NewLine+"Level", 
                        "Department", 
                        "Paid Date", 
                        "Period", 

                        //6-10
                        "Group", 
                        "Site", 
                        "Process", 
                        "Status", 
                        "Working"+Environment.NewLine+"Day", 
                        
                        //11-15
                        "Schedule", 
                        "Holiday"+Environment.NewLine+"Index",
                        "Public"+Environment.NewLine+"Holiday", 
                        "Actual"+Environment.NewLine+"Time (In)", 
                        "Actual"+Environment.NewLine+"Time (Out)", 
                        
                        //16-20
                        "Work"+Environment.NewLine+"In", 
                        "Work"+Environment.NewLine+"Out",
                        "Break"+Environment.NewLine+"In", 
                        "Break"+Environment.NewLine+"Out",
                        "Routine"+Environment.NewLine+"OT In",
                        
                        //21-25
                        "Routine"+Environment.NewLine+"OT Out",
                        "Shift"+Environment.NewLine+"1/2",
                        "Late",
                        "Working"+Environment.NewLine+"Date (In)",
                        "Working"+Environment.NewLine+"Date (Out)",
                        
                        //26-30
                        "Working"+Environment.NewLine+"Duration",
                        "Monthly"+Environment.NewLine+"Salary",
                        "Daily"+Environment.NewLine+"Salary",
                        "Type",  
                        "Leave",
                        
                        //31-35
                        "Working"+Environment.NewLine+"Time (In)",
                        "Working"+Environment.NewLine+"Time (Out)",
                        "Duration",
                        "Paid Leave"+Environment.NewLine+"Day",
                        "Paid Leave"+Environment.NewLine+"Hour",
                        
                        //36-40
                        "Paid Leave"+Environment.NewLine+"Amount",
                        "Processed Paid"+Environment.NewLine+"Leave Amount",
                        "Unpaid Leave"+Environment.NewLine+"Day",
                        "Unpaid Leave"+Environment.NewLine+"Hour",
                        "UnPaid Leave"+Environment.NewLine+"Amount",
                        
                        //41-45
                        "Processed Unpaid"+Environment.NewLine+"Leave Amount",
                        "Actual"+Environment.NewLine+"Salary", 
                        "OT 1"+Environment.NewLine+"(Hour)",
                        "OT 1"+Environment.NewLine+"(Amount)",
                        "OT 2"+Environment.NewLine+"(Hour)",
                        
                        //46-50
                        "OT 2"+Environment.NewLine+"(Amount)",
                        "OT Holiday"+Environment.NewLine+"(Hour)",
                        "OT Holiday"+Environment.NewLine+"(Amount)",
                        "OT To"+Environment.NewLine+"Leave",
                        "Holiday",
                        
                        //51-55
                        "Meal",
                        "Transport",
                        "Leave"+Environment.NewLine+"Deduction", 
                        "Actual"+Environment.NewLine+"Date (In)",
                        "Actual"+Environment.NewLine+"Date (Out)",
                        
                        //56-59
                        "Type", 
                        "Paid",
                        "Start"+Environment.NewLine+"Time",
                        "End"+Environment.NewLine+"Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 130, 150, 150, 150, 
                        
                        //6-10
                        150, 150, 90, 130, 100,
                        
                        //11-15
                        150, 100, 100, 100, 100,

                        //16-20
                        100, 100, 100, 100, 100,

                        //21-25
                        100, 100, 100, 100, 100,

                        //26-30
                        100, 100, 100, 100, 100,

                        //31-35
                        100, 130, 100, 100, 100,
                        //
                        //36-40
                        150, 150, 100, 100, 100,

                        //41-45
                        150, 100, 100, 100, 100, 
                        
                        //46-50
                        100, 100, 100, 100, 100, 
                        
                        //51-55
                        100, 100, 100, 100, 100, 
                        
                        //56-60
                        100, 60, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1, 4, 24, 25, 54, 55 });
            Sm.GrdFormatTime(Grd1, new int[] { 14, 15, 16, 17, 18, 19, 20, 21, 31, 32, 58, 59 });
            Sm.GrdColCheck(Grd1, new int[] { 8, 13, 22, 23, 30, 49, 50, 57 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 26, 27, 28, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 51, 52, 53 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 10, 12, 13, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29, 31, 32, 33, 34, 35, 36, 38, 39, 40 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, ");
            SQL.AppendLine("A.EmpSalary As BSalary1, A.EmpSalary2 As BSalary2, ");
            SQL.AppendLine("B.WSName, C.LeaveName, D.DeptName, ");
            SQL.AppendLine("E.OptDesc As EmploymentStatusDesc, ");
            SQL.AppendLine("F.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("G.GrdLvlName, ");
            SQL.AppendLine("H.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("I.PGName, J.SiteName, K.OptDesc As LeaveTypeDesc ");
            SQL.AppendLine("From TblPayrollProcess2 A ");
            SQL.AppendLine("Left Join TblWorkSchedule B on A.WSCode = B.WSCode ");
            SQL.AppendLine("Left Join TblLeave C on A.LeaveCode = C.LeaveCode ");
            SQL.AppendLine("Left Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On A.EmploymentStatus=E.OptCode And E.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblOption F On A.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr G On A.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption H On A.PayrunPeriod=H.OptCode And H.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On A.PGCode=I.PGCode ");
            SQL.AppendLine("Left Join TblSite J On A.SiteCode=J.SiteCode ");
            SQL.AppendLine("Left Join TblOption K On A.LeaveType=K.OptCode And K.OptCat='LeaveType' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Order By A.Dt; ");

            return SQL.ToString();
        }

        private void ShowData()
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCode);
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(),
                    new string[]
                    {
                        //0
                        "Dt",

                        //1-5
                        "GrdLvlName",
                        "DeptName",
                        "LatestPaidDt",
                        "PayrunPeriodDesc",
                        "PGName",

                        //6-10
                        "SiteName",
                        "ProcessInd",
                        "EmploymentStatusDesc",
                        "WorkingDay",
                        "WSName",

                        //11-15
                        "HolidayIndex",
                        "HolInd",
                        "ActualIn",
                        "ActualOut",
                        "WSIn1",
                        
                        //16-20
                        "WSOut1",
                        "WSIn2",
                        "WSOut2",
                        "WSIn3",
                        "WSOut3",
                        
                        //21-25
                        "OneDayInd",
                        "LateInd",
                        "WorkingIn",
                        "WorkingOut",
                        "WorkingDuration",
                        
                        //26-30
                        "BSalary1",
                        "BSalary2",
                        "SystemTypeDesc",
                        "LeaveName",
                        "LeaveDuration",
                        
                        //31-35
                        "PLDay",
                        "PLHr",
                        "PLAmt",
                        "ProcessPLAmt",
                        "UPLDay",
                        
                        //36-40
                        "UPLHr",
                        "UPLAmt",
                        "ProcessUPLAmt",
                        "Salary",
                        "OT1Hr",
                        
                        //41-45
                        "OT1Amt",
                        "OT2Hr",
                        "OT2Amt",
                        "OTHolidayHr",
                        "OTHolidayAmt",
                        
                        //46-50
                        "OTToLeaveInd",
                        "WSHolidayInd",
                        "Meal",
                        "Transport",
                        "DedProdLeave",
                        
                        //51-55
                        "LeaveTypeDesc",
                        "PaidLeaveInd",
                        "LeaveStartTm",
                        "LeaveEndTm"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 23);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 25);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 26);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 31, 23);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 32, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 30);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 31);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 32);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 33);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 34);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 35);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 36);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 37);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 38);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 39);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 40);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 41);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 42);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 46, 43);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 44);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 48, 45);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 49, 46);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 50, 47);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 51, 48);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 52, 49);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 53, 50);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 54, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 55, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 56, 51);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 57, 52);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 58, 53);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 59, 54);
                    }, false, false, false, false
                );
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 12, 26, 27, 28, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 51, 52, 53 });           

            
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 12, 26, 27, 28, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 51, 52, 53 });
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion

        #region Event

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 10, 12, 13, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29, 31, 32, 33, 34, 35, 36, 38, 39, 40 }, !ChkHideInfoInGrd.Checked);
        }
        
        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion
    }
}
