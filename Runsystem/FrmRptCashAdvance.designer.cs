﻿namespace RunSystem
{
    partial class FrmRptCashAdvance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtDocNoVR = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkDocNoVR = new DevExpress.XtraEditors.CheckEdit();
            this.TxtLocalDocNoVR = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkLocalDocNoVR = new DevExpress.XtraEditors.CheckEdit();
            this.TxtDocNoCAS = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.ChkDocNoCAS = new DevExpress.XtraEditors.CheckEdit();
            this.TxtDocNoVC = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkDocNoVC = new DevExpress.XtraEditors.CheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtDroppingPaymentDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkDroppingPayment = new DevExpress.XtraEditors.CheckEdit();
            this.LblDroppingPayment = new System.Windows.Forms.Label();
            this.LblExcludeUnsettledCA = new System.Windows.Forms.Label();
            this.ChkExcludeUnsettledCA = new DevExpress.XtraEditors.CheckEdit();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.ChkSiteCode = new DevExpress.XtraEditors.CheckEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.LueEntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.ChkEntCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkDeptCode = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.LueVRType = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkVRType = new DevExpress.XtraEditors.CheckEdit();
            this.LblVRType = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoVR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNoVR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNoVR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLocalDocNoVR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoCAS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNoCAS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoVC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNoVC.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDroppingPaymentDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDroppingPayment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkExcludeUnsettledCA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVRType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVRType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(882, 0);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LblVRType);
            this.panel2.Controls.Add(this.LueVRType);
            this.panel2.Controls.Add(this.ChkVRType);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.TxtDocNoVC);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.ChkDocNoVC);
            this.panel2.Controls.Add(this.TxtDocNoCAS);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.ChkDocNoCAS);
            this.panel2.Controls.Add(this.TxtLocalDocNoVR);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ChkLocalDocNoVR);
            this.panel2.Controls.Add(this.TxtDocNoVR);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.ChkDocNoVR);
            this.panel2.Size = new System.Drawing.Size(882, 144);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ReadOnly = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(882, 329);
            this.Grd1.TabIndex = 35;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 144);
            this.panel3.Size = new System.Drawing.Size(882, 329);
            // 
            // TxtDocNoVR
            // 
            this.TxtDocNoVR.EnterMoveNextControl = true;
            this.TxtDocNoVR.Location = new System.Drawing.Point(163, 4);
            this.TxtDocNoVR.Name = "TxtDocNoVR";
            this.TxtDocNoVR.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNoVR.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNoVR.Properties.MaxLength = 250;
            this.TxtDocNoVR.Size = new System.Drawing.Size(223, 20);
            this.TxtDocNoVR.TabIndex = 11;
            this.TxtDocNoVR.Validated += new System.EventHandler(this.TxtDocNoVR_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(39, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 14);
            this.label4.TabIndex = 10;
            this.label4.Text = "VR (Cash Advance)#";
            // 
            // ChkDocNoVR
            // 
            this.ChkDocNoVR.Location = new System.Drawing.Point(392, 2);
            this.ChkDocNoVR.Name = "ChkDocNoVR";
            this.ChkDocNoVR.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocNoVR.Properties.Appearance.Options.UseFont = true;
            this.ChkDocNoVR.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocNoVR.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocNoVR.Properties.Caption = " ";
            this.ChkDocNoVR.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocNoVR.Size = new System.Drawing.Size(19, 22);
            this.ChkDocNoVR.TabIndex = 12;
            this.ChkDocNoVR.ToolTip = "Remove filter";
            this.ChkDocNoVR.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocNoVR.ToolTipTitle = "Run System";
            this.ChkDocNoVR.CheckedChanged += new System.EventHandler(this.ChkDocNoVR_CheckedChanged);
            // 
            // TxtLocalDocNoVR
            // 
            this.TxtLocalDocNoVR.EnterMoveNextControl = true;
            this.TxtLocalDocNoVR.Location = new System.Drawing.Point(163, 26);
            this.TxtLocalDocNoVR.Name = "TxtLocalDocNoVR";
            this.TxtLocalDocNoVR.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNoVR.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNoVR.Properties.MaxLength = 250;
            this.TxtLocalDocNoVR.Size = new System.Drawing.Size(223, 20);
            this.TxtLocalDocNoVR.TabIndex = 14;
            this.TxtLocalDocNoVR.Validated += new System.EventHandler(this.TxtLocalDocNoVR_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(98, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Local VR#";
            // 
            // ChkLocalDocNoVR
            // 
            this.ChkLocalDocNoVR.Location = new System.Drawing.Point(392, 24);
            this.ChkLocalDocNoVR.Name = "ChkLocalDocNoVR";
            this.ChkLocalDocNoVR.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLocalDocNoVR.Properties.Appearance.Options.UseFont = true;
            this.ChkLocalDocNoVR.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkLocalDocNoVR.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkLocalDocNoVR.Properties.Caption = " ";
            this.ChkLocalDocNoVR.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLocalDocNoVR.Size = new System.Drawing.Size(19, 22);
            this.ChkLocalDocNoVR.TabIndex = 15;
            this.ChkLocalDocNoVR.ToolTip = "Remove filter";
            this.ChkLocalDocNoVR.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkLocalDocNoVR.ToolTipTitle = "Run System";
            this.ChkLocalDocNoVR.CheckedChanged += new System.EventHandler(this.ChkLocalDocNoVR_CheckedChanged);
            // 
            // TxtDocNoCAS
            // 
            this.TxtDocNoCAS.EnterMoveNextControl = true;
            this.TxtDocNoCAS.Location = new System.Drawing.Point(163, 48);
            this.TxtDocNoCAS.Name = "TxtDocNoCAS";
            this.TxtDocNoCAS.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNoCAS.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNoCAS.Properties.MaxLength = 250;
            this.TxtDocNoCAS.Size = new System.Drawing.Size(223, 20);
            this.TxtDocNoCAS.TabIndex = 17;
            this.TxtDocNoCAS.Validated += new System.EventHandler(this.TxtTxtDocNoCAS_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 14);
            this.label2.TabIndex = 16;
            this.label2.Text = "Cash Advance Settlement#";
            // 
            // ChkDocNoCAS
            // 
            this.ChkDocNoCAS.Location = new System.Drawing.Point(392, 46);
            this.ChkDocNoCAS.Name = "ChkDocNoCAS";
            this.ChkDocNoCAS.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocNoCAS.Properties.Appearance.Options.UseFont = true;
            this.ChkDocNoCAS.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocNoCAS.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocNoCAS.Properties.Caption = " ";
            this.ChkDocNoCAS.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocNoCAS.Size = new System.Drawing.Size(19, 22);
            this.ChkDocNoCAS.TabIndex = 18;
            this.ChkDocNoCAS.ToolTip = "Remove filter";
            this.ChkDocNoCAS.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocNoCAS.ToolTipTitle = "Run System";
            this.ChkDocNoCAS.CheckedChanged += new System.EventHandler(this.ChkTxtDocNoCAS_CheckedChanged);
            // 
            // TxtDocNoVC
            // 
            this.TxtDocNoVC.EnterMoveNextControl = true;
            this.TxtDocNoVC.Location = new System.Drawing.Point(163, 70);
            this.TxtDocNoVC.Name = "TxtDocNoVC";
            this.TxtDocNoVC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNoVC.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNoVC.Properties.MaxLength = 250;
            this.TxtDocNoVC.Size = new System.Drawing.Size(223, 20);
            this.TxtDocNoVC.TabIndex = 20;
            this.TxtDocNoVC.Validated += new System.EventHandler(this.TxtTxtDocNoVC_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(98, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 14);
            this.label3.TabIndex = 19;
            this.label3.Text = "Voucher#";
            // 
            // ChkDocNoVC
            // 
            this.ChkDocNoVC.Location = new System.Drawing.Point(392, 68);
            this.ChkDocNoVC.Name = "ChkDocNoVC";
            this.ChkDocNoVC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocNoVC.Properties.Appearance.Options.UseFont = true;
            this.ChkDocNoVC.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocNoVC.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocNoVC.Properties.Caption = " ";
            this.ChkDocNoVC.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocNoVC.Size = new System.Drawing.Size(19, 22);
            this.ChkDocNoVC.TabIndex = 21;
            this.ChkDocNoVC.ToolTip = "Remove filter";
            this.ChkDocNoVC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocNoVC.ToolTipTitle = "Run System";
            this.ChkDocNoVC.CheckedChanged += new System.EventHandler(this.ChkTxtDocNoVC_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtDroppingPaymentDocNo);
            this.panel4.Controls.Add(this.ChkDroppingPayment);
            this.panel4.Controls.Add(this.LblDroppingPayment);
            this.panel4.Controls.Add(this.LblExcludeUnsettledCA);
            this.panel4.Controls.Add(this.ChkExcludeUnsettledCA);
            this.panel4.Controls.Add(this.LueSiteCode);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.ChkSiteCode);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.DteDocDt2);
            this.panel4.Controls.Add(this.LueEntCode);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.ChkEntCode);
            this.panel4.Controls.Add(this.LueDeptCode);
            this.panel4.Controls.Add(this.ChkDeptCode);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.DteDocDt1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(451, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(427, 140);
            this.panel4.TabIndex = 22;
            // 
            // TxtDroppingPaymentDocNo
            // 
            this.TxtDroppingPaymentDocNo.EnterMoveNextControl = true;
            this.TxtDroppingPaymentDocNo.Location = new System.Drawing.Point(154, 93);
            this.TxtDroppingPaymentDocNo.Name = "TxtDroppingPaymentDocNo";
            this.TxtDroppingPaymentDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDroppingPaymentDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDroppingPaymentDocNo.Properties.MaxLength = 250;
            this.TxtDroppingPaymentDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtDroppingPaymentDocNo.TabIndex = 48;
            // 
            // ChkDroppingPayment
            // 
            this.ChkDroppingPayment.Location = new System.Drawing.Point(400, 91);
            this.ChkDroppingPayment.Name = "ChkDroppingPayment";
            this.ChkDroppingPayment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDroppingPayment.Properties.Appearance.Options.UseFont = true;
            this.ChkDroppingPayment.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDroppingPayment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDroppingPayment.Properties.Caption = " ";
            this.ChkDroppingPayment.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDroppingPayment.Size = new System.Drawing.Size(19, 22);
            this.ChkDroppingPayment.TabIndex = 49;
            this.ChkDroppingPayment.ToolTip = "Remove filter";
            this.ChkDroppingPayment.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDroppingPayment.ToolTipTitle = "Run System";
            // 
            // LblDroppingPayment
            // 
            this.LblDroppingPayment.AutoSize = true;
            this.LblDroppingPayment.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDroppingPayment.Location = new System.Drawing.Point(43, 96);
            this.LblDroppingPayment.Name = "LblDroppingPayment";
            this.LblDroppingPayment.Size = new System.Drawing.Size(108, 14);
            this.LblDroppingPayment.TabIndex = 47;
            this.LblDroppingPayment.Text = "Dropping Payment";
            // 
            // LblExcludeUnsettledCA
            // 
            this.LblExcludeUnsettledCA.AutoSize = true;
            this.LblExcludeUnsettledCA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblExcludeUnsettledCA.Location = new System.Drawing.Point(210, 118);
            this.LblExcludeUnsettledCA.Name = "LblExcludeUnsettledCA";
            this.LblExcludeUnsettledCA.Size = new System.Drawing.Size(173, 14);
            this.LblExcludeUnsettledCA.TabIndex = 46;
            this.LblExcludeUnsettledCA.Text = "Exclude Settled Cash Advance";
            // 
            // ChkExcludeUnsettledCA
            // 
            this.ChkExcludeUnsettledCA.EditValue = true;
            this.ChkExcludeUnsettledCA.Location = new System.Drawing.Point(400, 114);
            this.ChkExcludeUnsettledCA.Name = "ChkExcludeUnsettledCA";
            this.ChkExcludeUnsettledCA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkExcludeUnsettledCA.Properties.Appearance.Options.UseFont = true;
            this.ChkExcludeUnsettledCA.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkExcludeUnsettledCA.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkExcludeUnsettledCA.Properties.Caption = " ";
            this.ChkExcludeUnsettledCA.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkExcludeUnsettledCA.Size = new System.Drawing.Size(19, 22);
            this.ChkExcludeUnsettledCA.TabIndex = 45;
            this.ChkExcludeUnsettledCA.ToolTip = "Remove filter";
            this.ChkExcludeUnsettledCA.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkExcludeUnsettledCA.ToolTipTitle = "Run System";
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(154, 71);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(242, 20);
            this.LueSiteCode.TabIndex = 33;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(123, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 14);
            this.label7.TabIndex = 32;
            this.label7.Text = "Site";
            // 
            // ChkSiteCode
            // 
            this.ChkSiteCode.Location = new System.Drawing.Point(400, 69);
            this.ChkSiteCode.Name = "ChkSiteCode";
            this.ChkSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSiteCode.Properties.Appearance.Options.UseFont = true;
            this.ChkSiteCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSiteCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSiteCode.Properties.Caption = " ";
            this.ChkSiteCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSiteCode.Size = new System.Drawing.Size(19, 22);
            this.ChkSiteCode.TabIndex = 34;
            this.ChkSiteCode.ToolTip = "Remove filter";
            this.ChkSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSiteCode.ToolTipTitle = "Run System";
            this.ChkSiteCode.CheckedChanged += new System.EventHandler(this.ChkSiteCode_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(78, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 14);
            this.label9.TabIndex = 26;
            this.label9.Text = "Department";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(282, 5);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(114, 20);
            this.DteDocDt2.TabIndex = 25;
            // 
            // LueEntCode
            // 
            this.LueEntCode.EnterMoveNextControl = true;
            this.LueEntCode.Location = new System.Drawing.Point(154, 49);
            this.LueEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntCode.Name = "LueEntCode";
            this.LueEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntCode.Properties.DropDownRows = 30;
            this.LueEntCode.Properties.NullText = "[Empty]";
            this.LueEntCode.Properties.PopupWidth = 300;
            this.LueEntCode.Size = new System.Drawing.Size(242, 20);
            this.LueEntCode.TabIndex = 30;
            this.LueEntCode.ToolTip = "F4 : Show/hide list";
            this.LueEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEntCode.EditValueChanged += new System.EventHandler(this.LueEntCode_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(112, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 14);
            this.label8.TabIndex = 29;
            this.label8.Text = "Entity";
            // 
            // ChkEntCode
            // 
            this.ChkEntCode.Location = new System.Drawing.Point(400, 47);
            this.ChkEntCode.Name = "ChkEntCode";
            this.ChkEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkEntCode.Properties.Appearance.Options.UseFont = true;
            this.ChkEntCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkEntCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkEntCode.Properties.Caption = " ";
            this.ChkEntCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkEntCode.Size = new System.Drawing.Size(19, 22);
            this.ChkEntCode.TabIndex = 31;
            this.ChkEntCode.ToolTip = "Remove filter";
            this.ChkEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkEntCode.ToolTipTitle = "Run System";
            this.ChkEntCode.CheckedChanged += new System.EventHandler(this.ChkEntCode_CheckedChanged);
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(154, 27);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(242, 20);
            this.LueDeptCode.TabIndex = 27;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // ChkDeptCode
            // 
            this.ChkDeptCode.Location = new System.Drawing.Point(400, 25);
            this.ChkDeptCode.Name = "ChkDeptCode";
            this.ChkDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDeptCode.Properties.Appearance.Options.UseFont = true;
            this.ChkDeptCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDeptCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDeptCode.Properties.Caption = " ";
            this.ChkDeptCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDeptCode.Size = new System.Drawing.Size(19, 22);
            this.ChkDeptCode.TabIndex = 28;
            this.ChkDeptCode.ToolTip = "Remove filter";
            this.ChkDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDeptCode.ToolTipTitle = "Run System";
            this.ChkDeptCode.CheckedChanged += new System.EventHandler(this.ChkDeptCode_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(270, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 14);
            this.label5.TabIndex = 44;
            this.label5.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(110, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "Period";
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(154, 5);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(114, 20);
            this.DteDocDt1.TabIndex = 24;
            // 
            // LueVRType
            // 
            this.LueVRType.EnterMoveNextControl = true;
            this.LueVRType.Location = new System.Drawing.Point(163, 93);
            this.LueVRType.Margin = new System.Windows.Forms.Padding(5);
            this.LueVRType.Name = "LueVRType";
            this.LueVRType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVRType.Properties.Appearance.Options.UseFont = true;
            this.LueVRType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVRType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVRType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVRType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVRType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVRType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVRType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVRType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVRType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVRType.Properties.DropDownRows = 30;
            this.LueVRType.Properties.MaxLength = 16;
            this.LueVRType.Properties.NullText = "[Empty]";
            this.LueVRType.Properties.PopupWidth = 300;
            this.LueVRType.Size = new System.Drawing.Size(223, 20);
            this.LueVRType.TabIndex = 48;
            this.LueVRType.ToolTip = "F4 : Show/hide list";
            this.LueVRType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // ChkVRType
            // 
            this.ChkVRType.Location = new System.Drawing.Point(392, 91);
            this.ChkVRType.Name = "ChkVRType";
            this.ChkVRType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkVRType.Properties.Appearance.Options.UseFont = true;
            this.ChkVRType.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkVRType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkVRType.Properties.Caption = " ";
            this.ChkVRType.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkVRType.Size = new System.Drawing.Size(19, 22);
            this.ChkVRType.TabIndex = 49;
            this.ChkVRType.ToolTip = "Remove filter";
            this.ChkVRType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkVRType.ToolTipTitle = "Run System";
            // 
            // LblVRType
            // 
            this.LblVRType.AutoSize = true;
            this.LblVRType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVRType.Location = new System.Drawing.Point(94, 96);
            this.LblVRType.Name = "LblVRType";
            this.LblVRType.Size = new System.Drawing.Size(62, 14);
            this.LblVRType.TabIndex = 50;
            this.LblVRType.Text = "VR\'s Type";
            // 
            // FrmRptCashAdvance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 473);
            this.Name = "FrmRptCashAdvance";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoVR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNoVR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNoVR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLocalDocNoVR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoCAS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNoCAS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoVC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNoVC.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDroppingPaymentDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDroppingPayment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkExcludeUnsettledCA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVRType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVRType.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtDocNoVR;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkDocNoVR;
        private DevExpress.XtraEditors.TextEdit TxtDocNoVC;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckEdit ChkDocNoVC;
        private DevExpress.XtraEditors.TextEdit TxtDocNoCAS;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.CheckEdit ChkDocNoCAS;
        private DevExpress.XtraEditors.TextEdit TxtLocalDocNoVR;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkLocalDocNoVR;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.LookUpEdit LueEntCode;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.CheckEdit ChkEntCode;
        internal DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private DevExpress.XtraEditors.CheckEdit ChkDeptCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.CheckEdit ChkSiteCode;
        private System.Windows.Forms.Label LblExcludeUnsettledCA;
        private DevExpress.XtraEditors.CheckEdit ChkExcludeUnsettledCA;
        private System.Windows.Forms.Label LblVRType;
        private DevExpress.XtraEditors.LookUpEdit LueVRType;
        private DevExpress.XtraEditors.CheckEdit ChkVRType;
        private DevExpress.XtraEditors.TextEdit TxtDroppingPaymentDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkDroppingPayment;
        private System.Windows.Forms.Label LblDroppingPayment;
    }
}