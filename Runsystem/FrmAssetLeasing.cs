﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAssetLeasing : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmAssetLeasingFind FrmFind;

        #endregion

        #region Constructor

        public FrmAssetLeasing(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Accident";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);

            base.FrmLoad(sender, e);
            Sl.SetLueVdCode(ref LueVdCode);
            Sl.SetLueEntCode(ref LueEntCode);
            SetLueStatus(ref LueStatus);
            Sl.SetLueCurCode(ref LueCurCode);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        # region Standart Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, ChkCancelInd, TxtAssetCode, TxtAssetName, TxtDisplayName, LueStatus,
                        LueEntCode, LueVdCode, TxtInsurancePolicy, DteStart, DteEnd, LueCurCode, TxtInsuredValue, MeeRemark, MeeCancelReason 
                    }, true);
                    BtnAssetCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueEntCode, LueVdCode, TxtInsurancePolicy, DteStart, DteEnd, LueCurCode, TxtInsuredValue, MeeRemark
                    }, false);
                    BtnAssetCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       MeeCancelReason, LueStatus
                    }, false);
                    ChkCancelInd.Focus();
                    break;

            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo,DteDocDt, TxtAssetCode, TxtAssetName, TxtDisplayName, MeeCancelReason, LueStatus,
                LueEntCode, LueVdCode, TxtInsurancePolicy, DteStart, DteEnd, LueCurCode, TxtInsuredValue, MeeRemark 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtInsuredValue }, 0);
            ChkCancelInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAssetLeasingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetLue(LueStatus, "O");
            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {

        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AssetLeasing", "TblAssetLeasing");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveAssetLeasing(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);

        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsTxtEmpty(TxtAssetCode, "Asset", false) ||
                Sm.IsLueEmpty(LueEntCode, "Entity") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                Sm.IsDteEmpty(DteStart, "Date Start") ||
                Sm.IsDteEmpty(DteEnd, "Date End") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsTxtEmpty(TxtInsuredValue, "Insured Value", true)
                ;
        }

        private MySqlCommand SaveAssetLeasing(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into tblassetleasing(DocNo, DocDt, Status, CancelInd, AssetCode, EntCode, VdCode, InsurancePolicy, StartDt, EndDt, CurCode, InsuredValue, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', 'N', @AssetCode, @EntCode, @VdCode, @InsurancePolicy, @StartDt, @EndDt, @CurCode, @InsuredValue, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@InsurancePolicy", TxtInsurancePolicy.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStart));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEnd));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@InsuredValue", decimal.Parse(TxtInsuredValue.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditAssetLeasing());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready()||
                IsDataFulfilledAlready()
                ;
        }

       

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblAssetLeasing " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataFulfilledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblAssetLeasing " +
                    "Where Status='F' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already fulfilled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditAssetLeasing()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAssetLeasing Set ");
            SQL.AppendLine("   Status=@Status, CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowAssetLeasing(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAssetLeasing(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt,  A.Status, A.cancelReason, A.CancelInd, A.AssetCode, B.AssetName, B.DisplayName, A.EntCode, A.VdCode, ");
            SQL.AppendLine("A.InsurancePolicy, A.StartDt, A.EndDt, A.CurCode, A.InsuredValue, A.Remark ");
            SQL.AppendLine("From TblAssetLeasing A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "Status", "CancelReason", "CancelInd", "AssetCode",  
                        //6-10
                        "AssetName", "DisplayName", "EntCode", "VdCode", "InsurancePolicy",  
                        //11-15
                        "StartDt", "EndDt", "CurCode", "InsuredValue", "Remark"

                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = DocNo;
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sm.SetLue(LueStatus, Sm.DrStr(dr, c[2]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                    TxtAssetCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtAssetName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtDisplayName.EditValue = Sm.DrStr(dr, c[7]);
                    Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[8]));
                    Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[9]));
                    TxtInsurancePolicy.EditValue = Sm.DrStr(dr, c[10]);
                    Sm.SetDte(DteStart, Sm.DrStr(dr, c[11]));
                    Sm.SetDte(DteEnd, Sm.DrStr(dr, c[12]));
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[13]));
                    TxtInsuredValue.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                }, true
        );

        }

        #endregion

        #region Additional Method
        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'O' As Col1, 'Outstanding' As Col2 Union All ");
            SQL.AppendLine("Select 'F', 'Fulfilled' ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }
        #endregion


        #endregion
     
        #region Event
        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
        }

        private void BtnAssetCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAssetLeasingDlg(this));
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtInsuredValue_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtInsuredValue, 0);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        #endregion

       
    }
}
