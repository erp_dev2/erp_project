﻿#region update
/* 
    19/02/2018 [ARI] tambah filter pension date, kolom grade, kolom level
    01/11/2018 [TKG] bug filter department
    11/03/2020 [VIN/SIER] tambah kolom pre & post pensiun
    25/11/2020 [VIN/PHT] tambah parameter PensionCalculationFormat
    26/11/2020 [VIN/PHT] tambah informasi dari tblempseverance
    14/04/2021 [TKG/PHT] meggunakan parameter IsRptPensionUseSSRetiredMaxAge2
    23/06/2021 [BRI/PHT] menambah filter site menggunakan parameter IsRptPensionUseSiteFilter
    10/03/2022 [VIN/PHT] Level Kosong
    02/08/2022 [MAU/PHT] Reporting Pensiun tervalidasi otorisasi group user
    01/09/2022 [HPH/PRODUCT] Penambahan parameter RetiredMaxAge dan IsRptPensionUseRetiredMaxAge untuk pension
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptPension : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mSSRetiredMaxAge = string.Empty,
            mSSRetiredMaxAge2 = string.Empty,
            mNoOfDayPrePeriodPension = string.Empty,
            mNoOfDayPostPeriodPension = string.Empty,
            mPensionCalculationFormat = string.Empty,
            mRetiredMaxAge = string.Empty;
        private bool
            mIsRptPensionShowPrePostPeriod = false,
            mIsRptPensionShowSeveranceInformation= false,
            mIsRptPensionUseSSRetiredMaxAge2 = false,
            mIsRptPensionUseSiteFilter = false,
            mIsEmployeeTabAdditionalEnabled = false,
            mIsFilterBySite = false,
            mIsRptPensionUseRetiredMaxAge = false;

        #endregion

        #region Constructor

        public FrmRptPension(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                
                GetParameter();
                SetGrd();
                SetSQL();
                if (!mIsRptPensionUseSiteFilter)
                {
                    LblSiteCode.Visible = false;
                    LueSiteCode.Visible = false;
                    ChkSiteCode.Visible = false;
                }
                Sm.SetDefaultPeriod(ref DtePensionDt1, ref DtePensionDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode(ref LueSiteCode);
                SetLueStatus(ref LueStatus);
                Sm.SetLue(LueStatus, "1");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.DivisionName, A.DeptCode, C.DeptName, D.PosName, E.SiteCode, E.SiteName, A.JoinDt, A.ResignDt, ");
            SQL.AppendLine("A.IDNumber, F.OptDesc As Gender, G.OptDesc As EmploymentStatus, A.BirthDt, H.GrdLvlName, I.LevelName, ");
            SQL.AppendLine("sum(J.basicSalary) basicSalary, sum(J.RetirementBenefit) RetirementBenefit, sum(J.SevAmt) SevAmt, ");
            SQL.AppendLine("sum(J.AccruedExp) AccruedExp, sum(J.RecvPay) RecvPay, sum(AmtAfTax) AmtAfTax, ");
            if (mPensionCalculationFormat != "2")
            {
                
                SQL.AppendLine("Date_Format(Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d')- INTERVAL @NoOfDayPrePeriodPension DAY,'%Y%m%d') AS RetirmentPreparationPeriod,   ");
                SQL.AppendLine("Date_Format(Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d')+ INTERVAL @NoOfDayPostPeriodPension DAY,'%Y%m%d') AS PostRetirmentPeriod,    ");

                if (mIsRptPensionUseRetiredMaxAge)
                {
                    SQL.AppendLine("Date_Format(A.BirthDt+INTERVAL @RetiredMaxAge Year, '%Y%m%d')As Pension,");
                    SQL.AppendLine("DATEDIFF(Date_Format(A.BirthDt+INTERVAL @RetiredMaxAge Year, '%Y%m%d'), date_format(curdate(),'%Y%m%d'))As Remaining ");
                }
                else
                {
                    SQL.AppendLine("Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d')As Pension,");
                    SQL.AppendLine("DATEDIFF(Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d'), date_format(curdate(),'%Y%m%d'))As Remaining ");
                }
            }
            else
            {
                if (mIsRptPensionUseRetiredMaxAge)
                {
                    SQL.AppendLine("case ");
                    SQL.AppendLine("    when Right(A.BirthDt, 2)='01' then Date_Format(A.BirthDt+INTERVAL @RetiredMaxAge Year, '%Y%m%d') ");
                    SQL.AppendLine("    ELSE concat(Date_Format(A.BirthDt + INTERVAL '1' MONTH + INTERVAL @RetiredMaxAge Year, '%Y%m'), '01') ");
                    SQL.AppendLine("END AS Pension, ");

                    SQL.AppendLine("case ");
                    SQL.AppendLine("   when Right(A.BirthDt, 2)='01' then DATEDIFF(Date_Format(A.BirthDt+INTERVAL @RetiredMaxAge Year, '%Y%m%d'), date_format(curdate(),'%Y%m%d')) ");
                    SQL.AppendLine("   ELSE DATEDIFF(concat(Date_Format(A.BirthDt + INTERVAL '1' MONTH + INTERVAL @RetiredMaxAge Year, '%Y%m'), '01'), date_format(curdate(),'%Y%m%d')) ");
                    SQL.AppendLine("End As Remaining, ");
                }
                else
                {
                    SQL.AppendLine("case ");
                    SQL.AppendLine("    when Right(A.BirthDt, 2)='01' then Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d') ");
                    SQL.AppendLine("    ELSE concat(Date_Format(A.BirthDt + INTERVAL '1' MONTH + INTERVAL @SSRetiredMaxAge Year, '%Y%m'), '01') ");
                    SQL.AppendLine("END AS Pension, ");

                    SQL.AppendLine("case ");
                    SQL.AppendLine("   when Right(A.BirthDt, 2)='01' then DATEDIFF(Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d'), date_format(curdate(),'%Y%m%d')) ");
                    SQL.AppendLine("   ELSE DATEDIFF(concat(Date_Format(A.BirthDt + INTERVAL '1' MONTH + INTERVAL @SSRetiredMaxAge Year, '%Y%m'), '01'), date_format(curdate(),'%Y%m%d')) ");
                    SQL.AppendLine("End As Remaining, ");
                }

                SQL.AppendLine("case  ");
                SQL.AppendLine("    when Right(A.BirthDt, 2)='01' then Date_Format(Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d')- INTERVAL @NoOfDayPrePeriodPension DAY,'%Y%m%d')  ");
                SQL.AppendLine("    ELSE Date_Format(concat(Date_Format(A.BirthDt + INTERVAL '1' MONTH + INTERVAL @SSRetiredMaxAge Year, '%Y%m'), '01')- INTERVAL @NoOfDayPrePeriodPension DAY,'%Y%m%d') ");
                SQL.AppendLine("END AS RetirmentPreparationPeriod, ");

                SQL.AppendLine("case  ");
                SQL.AppendLine("    when Right(A.BirthDt, 2)='01' then Date_Format(Date_Format(A.BirthDt+INTERVAL @SSRetiredMaxAge Year, '%Y%m%d')+ INTERVAL @NoOfDayPostPeriodPension DAY,'%Y%m%d')  ");
                SQL.AppendLine("    ELSE Date_Format(concat(Date_Format(A.BirthDt + INTERVAL '1' MONTH + INTERVAL @SSRetiredMaxAge Year, '%Y%m'), '01')+ INTERVAL @NoOfDayPostPeriodPension DAY,'%Y%m%d') ");
                SQL.AppendLine("END AS PostRetirmentPeriod ");
                

            }
            SQL.AppendLine("from tblemployee A ");
            SQL.AppendLine("Left Join tbldivision B On A.DivisionCode=B.DivisionCode ");
            SQL.AppendLine("Left Join tbldepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join tblposition D On A.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join tblsite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join tbloption F On F.OptCat='Gender' and A.Gender=F.OptCode ");
            SQL.AppendLine("Left Join tbloption G On G.OptCat='EmploymentStatus' and A.EmploymentStatus=G.OptCode");
            SQL.AppendLine("Left Join TblGradeLevelHdr H On A.GrdLvlCode=H.GrdLvlCode ");
            if(mIsEmployeeTabAdditionalEnabled)
                SQL.AppendLine("Left Join TblLevelHdr I On A.LevelCode=I.LevelCode ");
            else
                SQL.AppendLine("Left Join TblLevelHdr I On H.LevelCode=I.LevelCode ");

            SQL.AppendLine("LEFT JOIN tblempseverance J ON A.EmpCode=J.EmpCode  AND J.CancelInd='N' AND J.`Status`='A'  ");
            SQL.AppendLine("GROUP BY A.EmpCode ");
            SQL.AppendLine(")T Where 1=1 ");

            //SQL.AppendLine();

            if (mIsFilterBySite)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("Select 1 From tblgroupsite U ");
                SQL.AppendLine("WHERE T.SiteCode = U.SiteCode ");
                SQL.AppendLine("And GrpCode In(SELECT V.GrpCode From tbluser V WHERE V.UserCode = @UserCode) ");
                SQL.AppendLine(" ) ");
            }



            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee#",
                        "Employee Name", 
                        "Old Code",
                        "Division",
                        "Department",
                        
                        //6-10 
                        "Position",
                        "Site",
                        "Join Date",
                        "Resign Date",
                        "ID Number",
                       
                        //11-15
                        "Gender",
                        "Employment"+Environment.NewLine+"Status",
                        "Birth Date",
                        "Grade", 
                        "Level",

                        //16-20
                        "Pension",
                        "Remaining"+Environment.NewLine+"Days",
                        "Retirment"+Environment.NewLine+"Preparation Period",
                        "Post"+Environment.NewLine+"Retirement Period",
                        "Salary",

                        //21-25
                        "Retirement"+Environment.NewLine+"Benefits",
                        "Severance"+Environment.NewLine+"Pay",
                        "Accrued"+Environment.NewLine+"Expenses",
                        "Received"+Environment.NewLine+"Pay",
                        "Payment"+Environment.NewLine+"Received Directly"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 200, 130, 150, 200,  
                        
                        //6-10
                        200, 200, 100, 100, 150,   
                        
                        //11-15
                        100, 100, 100, 140, 100,
                        
                        //16-20
                        100, 80, 150, 150, 150,

                        //21-25
                        150, 150, 150, 150, 150

                    }
                );
            
            Sm.GrdFormatDate(Grd1, new int[] { 8, 9, 13, 16, 18, 19 });
            Grd1.ReadOnly = true;
            //Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            if (!mIsRptPensionShowPrePostPeriod)
                Sm.GrdColInvisible(Grd1, new int[] { 18,19 });
            if(!mIsRptPensionShowSeveranceInformation)
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23, 24, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 20, 21, 22, 23, 24, 25 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();
                switch (Sm.GetLue(LueStatus))
                {
                    case "1":
                        Filter = " And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@CurrentDate)) ";
                        break;
                    case "2":
                        Filter = " And ResignDt Is Not Null And ResignDt<=@CurrentDate ";
                        break;
                }
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.CmParam<string>(ref cm, "@RetiredMaxAge", mRetiredMaxAge);
                Sm.CmParam<string>(ref cm, "@SSRetiredMaxAge", mIsRptPensionUseSSRetiredMaxAge2 ? mSSRetiredMaxAge2 : mSSRetiredMaxAge);
                Sm.CmParam<string>(ref cm, "@NoOfDayPostPeriodPension", mNoOfDayPostPeriodPension);
                Sm.CmParam<string>(ref cm, "@NoOfDayPrePeriodPension", mNoOfDayPrePeriodPension);
                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "EmpCode", "EmpCodeOld", "EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "SiteCode", true);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DtePensionDt1), Sm.GetDte(DtePensionDt2), "Pension");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By SiteName, DeptName, EmpName;",
                        new string[]
                        {
                            //0
                            "EmpCode",

                            //1-5
                            "EmpName", "EmpCodeOld", "DivisionName", "DeptName", "PosName", 
                            
                            //6-10
                             "SiteName", "JoinDt", "ResignDt", "IDNumber", "Gender",   
                            
                            //11-15
                            "EmploymentStatus", "BirthDt", "GrdLvlName", "LevelName", "Pension", 
                            
                            //16-20
                            "Remaining", "RetirmentPreparationPeriod", "PostRetirmentPeriod", "basicSalary", "RetirementBenefit", 

                            //21-24
                            "SevAmt", "AccruedExp", "RecvPay", "AmtAfTax"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);

                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 20, 21, 22, 23, 24, 25 });
                Grd1.Rows.CollapseAll();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }
        
        private void GetParameter()
        {
            //mSSRetiredMaxAge = Sm.GetParameter("SSRetiredMaxAge");
            //mSSRetiredMaxAge2 = Sm.GetParameter("SSRetiredMaxAge2");
            //mNoOfDayPrePeriodPension = Sm.GetParameter("NoOfDayPrePeriodPension");
            //mNoOfDayPostPeriodPension = Sm.GetParameter("NoOfDayPostPeriodPension");
            //mPensionCalculationFormat = Sm.GetParameter("PensionCalculationFormat");
            //if (mPensionCalculationFormat.Length == 0) mPensionCalculationFormat = "1";
            //mIsRptPensionShowPrePostPeriod = Sm.GetParameterBoo("IsRptPensionShowPrePostPeriod");
            //mIsRptPensionShowSeveranceInformation = Sm.GetParameterBoo("IsRptPensionShowSeveranceInformation");
            //mIsRptPensionUseSSRetiredMaxAge2 = Sm.GetParameterBoo("IsRptPensionUseSSRetiredMaxAge2");
            mIsRptPensionUseSiteFilter = Sm.GetParameterBoo("IsRptPensionUseSiteFilter");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'SSRetiredMaxAge', 'RetiredMaxAge', 'SSRetiredMaxAge2', 'IsRptPensionShowPrePostPeriod', 'NoOfDayPrePeriodPension', 'NoOfDayPostPeriodPension', ");
            SQL.AppendLine("'PensionCalculationFormat', 'IsRptPensionShowSeveranceInformation', 'IsRptPensionUseSSRetiredMaxAge2', 'IsEmployeeTabAdditionalEnabled', 'IsFilterBySite', 'IsRptPensionUseRetiredMaxAge' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptPensionShowPrePostPeriod": mIsRptPensionShowPrePostPeriod = ParValue == "Y"; break;
                            case "IsRptPensionShowSeveranceInformation": mIsRptPensionShowSeveranceInformation = ParValue == "Y"; break;
                            case "IsRptPensionUseSSRetiredMaxAge2": mIsRptPensionUseSSRetiredMaxAge2 = ParValue == "Y"; break;
                            case "IsEmployeeTabAdditionalEnabled": mIsEmployeeTabAdditionalEnabled = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsRptPensionUseRetiredMaxAge": mIsRptPensionUseRetiredMaxAge = ParValue == "Y"; break;

                            //string
                            case "SSRetiredMaxAge": 
                                mSSRetiredMaxAge = ParValue;
                                if (mSSRetiredMaxAge.Length == 0) mSSRetiredMaxAge = "56";
                                break;
                            case "SSRetiredMaxAge2": 
                                mSSRetiredMaxAge2 = ParValue;
                                if (mSSRetiredMaxAge2.Length == 0) mSSRetiredMaxAge2 = "56";
                                break;
                            case "RetiredMaxAge":
                                mRetiredMaxAge = ParValue;
                                if (mRetiredMaxAge.Length == 0) mRetiredMaxAge = "56";
                                break;
                            case "NoOfDayPrePeriodPension": mNoOfDayPrePeriodPension = ParValue; break;
                            case "NoOfDayPostPeriodPension": mNoOfDayPostPeriodPension = ParValue; break;
                            case "PensionCalculationFormat": 
                                mPensionCalculationFormat = ParValue;
                                if (mPensionCalculationFormat.Length == 0) mPensionCalculationFormat = "1";
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            //if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Birth date");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employee's status");
        }

        private void DtePensionDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DtePensionDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkPensionDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Pension date");
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union All " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

    }
}
