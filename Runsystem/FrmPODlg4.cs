﻿#region Update
/*
    22/09/2019 [TKG/IMS] layar dialog untuk split estimated received date 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPODlg4 : RunSystem.FrmBase15
    {
        #region Field

        private string mPORequestDNo = string.Empty;
        private int mRow = -1;
        private FrmPO mFrmParent;
        private bool mIsInsert;

        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmPODlg4(FrmPO FrmParent, int r, bool IsInsert)
        {
            InitializeComponent();
            mRow = r;
            mFrmParent = FrmParent;
            mIsInsert = IsInsert;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                SetGrd();
                ShowData();
                if (!mIsInsert)
                {
                    Grd1.ReadOnly = true;
                    BtnSave.Enabled = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "Quantity", "Estimated"+Environment.NewLine+"Received Date" },
                    new int[]{ 130, 150 }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 0 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 0 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowData()
        {
            TxtPORequestDocNo.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 7);
            mPORequestDNo = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 8);
            TxtItCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 12);
            TxtItName.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 15);
            TxtQty.EditValue = Sm.FormatNum(Sm.GetGrdDec(mFrmParent.Grd1, mRow, 19), 0);
            TxtPurchaseUomCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 20);

            if (mFrmParent.mlEstRecvDt.Count > 0)
            {
                int r = 0;
                Grd1.BeginUpdate();
                foreach (var x in mFrmParent.mlEstRecvDt.Where(w =>
                    Sm.CompareStr(w.PORequestDocNo, TxtPORequestDocNo.Text) &&
                    Sm.CompareStr(w.PORequestDNo, mPORequestDNo)))
                {
                    Grd1.Rows.Add();
                    Grd1.Cells[r, 0].Value = Sm.FormatNum(x.Qty, 0);
                    if (x.Dt.Length>0) Grd1.Cells[r, 1].Value = Sm.ConvertDate(x.Dt);
                    r++;
                }
            }
            Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0 });
            Grd1.EndUpdate();
        }

        #endregion

        #region Button Methods

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            if (IsGrdValueNotValid() ||
                IsQtyNotValid()) return;
            try
            {
                if (mFrmParent.mlEstRecvDt.Count > 0)
                {
                    mFrmParent.mlEstRecvDt.RemoveAll(x =>
                        Sm.CompareStr(x.PORequestDocNo, TxtPORequestDocNo.Text) &&
                        Sm.CompareStr(x.PORequestDNo, mPORequestDNo)
                        );
                }
                var MaxDt = string.Empty;
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdDec(Grd1, r, 0) != 0m)
                    {
                        if (MaxDt.Length == 0)
                            MaxDt = Sm.GetGrdDate(Grd1, r, 1);
                        else
                        { 
                            if (Sm.CompareDtTm(MaxDt, Sm.GetGrdDate(Grd1, r, 1))<0)
                                MaxDt = Sm.GetGrdDate(Grd1, r, 1);
                        }
                        mFrmParent.mlEstRecvDt.Add(new FrmPO.EstRecvDt()
                        {
                            PORequestDocNo = TxtPORequestDocNo.Text,
                            PORequestDNo = mPORequestDNo,
                            Qty = Sm.GetGrdDec(Grd1, r, 0),
                            Dt = Sm.GetGrdDate(Grd1, r, 1).Substring(0, 8)
                        });
                    }
                }
                if (MaxDt.Length > 0) mFrmParent.Grd1.Cells[mRow, 31].Value = Sm.ConvertDate(MaxDt);
                this.Close();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsGrdValueNotValid()
        {
            for (int r= 0; r< Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 0, true, "Quantity is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Estimated received date is empty."))
                    return true;
            }
            return false;
        }

        private bool IsQtyNotValid()
        {
            if (Grd1.Rows.Count <= 1) return false;
            decimal Qty = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                Qty += Sm.GetGrdDec(Grd1, r, 0);
            if (decimal.Parse(TxtQty.Text) != Qty)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid quantity.");
                return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1) Sm.DteRequestEdit(Grd1, DteEstRecvDt, ref fCell, ref fAccept, e);
            Sm.GrdRequestEdit(Grd1, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0 });
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 0 }, e);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteEstRecvDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEstRecvDt, ref fCell, ref fAccept);
        }

        private void DteEstRecvDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        #endregion

        #endregion
    }
}
