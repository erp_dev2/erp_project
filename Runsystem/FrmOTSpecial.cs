﻿#region Update
/*
    31/10/2019 [HAR/SRN]  
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOTSpecial : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal bool mIsSiteMandatory = false, mIsApprovalBySiteMandatory = false, mIsFilterBySite = false;
        internal string mOTSpecialSource = string.Empty;
        internal bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;
        iGCell fCell;
        bool fAccept;

        internal FrmOTSpecialFind FrmFind;

        #endregion

        #region Constructor

        public FrmOTSpecial(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "OT Request Special";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, TxtStatus }, true);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueSourceCode(ref LueSourceCode);
                LueSourceCode.Visible = false;
                Sl.SetLueDivisionCode(ref LueDivCode);
                if (mIsSiteMandatory)
                    LblSiteCode.ForeColor = Color.Red;
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-4
                        "Code",
                        ""+ mOTSpecialSource == "1" ? "Level": "Position"+"",           
                        "OT Amount",
                        "Meal Amount",
                    },
                     new int[] 
                    {
                        //0
                        20, 
                        
                        //1-4
                        80, 150, 150, 150 
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1 }, false);

            #endregion

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueDeptCode, LueSiteCode, LueDivCode, DteDocDt, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LueSiteCode, LueDivCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 3, 4 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtStatus, DteDocDt, LueDeptCode, LueSiteCode, LueDivCode,
                MeeRemark, 
            });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOTSpecialFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                  InsertData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 2)
                    {
                        LueRequestEdit(Grd1, LueSourceCode, ref fCell, ref fAccept, e, 3);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        SetLueSourceCode(ref LueSourceCode);
                    }
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3,4 });
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
           
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 && Sm.IsGrdColSelected(new int[] { 3, 4 }, e.ColIndex))
            {
                var Amt = Sm.GetGrdStr(Grd1, 0, e.ColIndex);
                for (int Row = 1; Row < Grd1.Rows.Count - 1; Row++)
                    Grd1.Cells[Row, e.ColIndex].Value = Amt;
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {

        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OTSpecial", "TblOTSpecialHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveOTSpecialHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveOTSpecialDtl(DocNo, Row));
                    cml.Add(SaveOTSpecialSummary(DocNo, Row));
                }
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LueDivCode, "Division") ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords();
        }

 
        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data on grid.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveOTSpecialHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblOTSpecialHdr(DocNo, DocDt, Status, DeptCode, SiteCode, DivisionCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @DeptCode, @SiteCode, @DivisionCode, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='OTSpecial' ");
            if (mIsApprovalBySiteMandatory)
                SQL.AppendLine("And IfNull(SiteCode, '')=@SiteCode ");
            SQL.AppendLine("And DeptCode=@DeptCode; ");

            SQL.AppendLine("Update TblOTSpecialHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='OTSpecial' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DivisionCode", Sm.GetLue(LueDivCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveOTSpecialDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblOTSpecialDtl(DocNo, DNo, SpecialIndCode, SourceCode, OTAmt, MealAmt, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @SpecialIndCode, @SourceCode, @OTAmt, @MealAmt, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SpecialIndCode", mOTSpecialSource);
            Sm.CmParam<String>(ref cm, "@SourceCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@OTAmt", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@MealAmt", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOTSpecialSummary(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblOTSpecialSummary(DeptCode, SiteCode, DivisionCode, SpecialIndCode, SourceCode, OTAmt, MealAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DeptCode, @SiteCode, @DivisionCode, @SpecialIndCode, @SourceCode, @OTAmt, @MealAmt, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("ON DUPLICATE KEY ");
            SQL.AppendLine("    Update DeptCode = @DeptCode, SiteCode=@SiteCode, DivisionCode=@DivisionCode, SpecialIndCode=@SpecialIndCode, SourceCode=@SourceCode, OTAmt=@OTAmt, MealAmt=@MealAmt ");    

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DivisionCode", Sm.GetLue(LueDivCode));
            Sm.CmParam<String>(ref cm, "@SpecialIndCode", mOTSpecialSource);
            Sm.CmParam<String>(ref cm, "@SourceCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@OTAmt", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@MealAmt", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }


        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowOTSpecialHdr(DocNo);
                ShowOTSpecialDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowOTSpecialHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ");
            SQL.AppendLine("Case Status ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("DeptCode, SiteCode, DivisionCode, Remark ");
            SQL.AppendLine("From TblOTSpecialHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                       "DocNo", 
                       
                       //1-5
                       "DocDt", "StatusDesc", "DeptCode", "SiteCode", "DivisionCode",  
                       
                       //6-7
                       "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[3]), string.Empty);
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[4]), string.Empty);
                        Sl.SetLueDivisionCode(ref LueDivCode);
                        Sm.SetLue(LueDivCode, Sm.DrStr(dr, c[5]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
            );
        }

        private void ShowOTSpecialDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.SourceCode, if(A.SpecialIndCode = '1', C.Levelname, B.PositionStatusName) As Source, ");
            SQL.AppendLine("A.OTAmt, A.MealAmt ");
            SQL.AppendLine("From TblOTSpecialDtl A ");
            SQL.AppendLine("Left Join TblPositionStatus B On A.SourceCode = B.PositionStatusCode ");
            SQL.AppendLine("Left Join Tbllevelhdr C On A.SourceCode = C.levelCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    
                    //1-5
                    "SourceCode", "Source", "OTAmt", "MealAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }


        private void ShowOTSpecialInfo()
        {
            int No = 0;
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SpecialIndCode", mOTSpecialSource);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DivisionCode", Sm.GetLue(LueDivCode));

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SourceCode, if(A.SpecialIndCode = '1', C.Levelname, B.PositionStatusName) As Source, ");
            SQL.AppendLine("A.OTAmt, A.MealAmt  ");
            SQL.AppendLine("From TblOTSpecialSummary A  ");
            SQL.AppendLine("Left Join TblPositionStatus B On A.SourceCode = B.PositionStatusCode  ");
            SQL.AppendLine("Left Join Tbllevelhdr C On A.SourceCode = C.levelCode   ");
            SQL.AppendLine("Where A.SpecialIndCode=@SpecialIndCode");
            if (Sm.GetLue(LueDeptCode).Length > 0)
                SQL.AppendLine("And A.DeptCode = @DeptCode ");
            if (Sm.GetLue(LueSiteCode).Length > 0)
                SQL.AppendLine("And A.SiteCode = @SiteCode ");
            if (Sm.GetLue(LueDivCode).Length > 0)
                SQL.AppendLine("And A.DivisionCode = @DivisionCode ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "SourceCode", 
                    
                    //1-5
                    "Source", "OTAmt", "MealAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }



        #endregion

        #region Additional Method

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowOTSpecialInfo();
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
            mOTSpecialSource = Sm.GetParameter("OTSpecialSource");
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e, int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueSourceCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (mOTSpecialSource == "1")
            {
                SQL.AppendLine("Select T.LevelCode As Col1, T.LevelName As Col2 ");
                SQL.AppendLine("From tbllevelhdr T ");
                SQL.AppendLine("Order By T.LevelName;");
            }
            else
            {
                SQL.AppendLine("Select T.PositionStatusCode As Col1, T.PositionStatusName As Col2 ");
                SQL.AppendLine("From TblPositionStatus T ");
                SQL.AppendLine("Order By T.PositionStatusName;");
            }

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        #endregion


        #endregion

        #region Event

        private void LueSourceCode_Leave(object sender, EventArgs e)
        {
            if (LueSourceCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueSourceCode).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueSourceCode);
                    Grd1.Cells[fCell.RowIndex, 2].Value = LueSourceCode.Text;
                }
                LueSourceCode.Visible = false;
            }

        }

        private void LueSourceCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                ClearGrd();
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                ClearGrd();
            }
        }

        private void LueDivCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDivCode, new Sm.RefreshLue1(Sl.SetLueDivisionCode));
        }

        private void LueSourceCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDivCode, new Sm.RefreshLue1(SetLueSourceCode));
        }

        #endregion
    }
}
