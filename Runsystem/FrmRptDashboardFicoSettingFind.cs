﻿#region Update
/*
    20/09/2022 [SET/Yk] New Menu find
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDashboardFicoSettingFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmRptDashboardFicoSetting mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptDashboardFicoSettingFind(FrmRptDashboardFicoSetting FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.RptDashboardCode, F.OptDesc RptDashboardName, C.SiteName, B.SettingCode, B.SettingDesc, B.Sequence, B.AcNo, B.Formula, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblRptDashboardFicoSettingHdr A ");
            SQL.AppendLine("Left Join TblRptDashboardFicoSettingDtl B On A.RptDashboardCode=B.RptDashboardCode ");
            SQL.AppendLine("Left Join TblSite C On A.SiteCode=C.SiteCode ");
            //if (mFrmParent.mIsFilterBySite)
            //{
            //    SQL.AppendLine("And (C.SiteCode Is Null Or ");
            //    SQL.AppendLine("(C.SiteCode Is Not Null ");
            //    SQL.AppendLine("And Exists( ");
            //    SQL.AppendLine("    Select 1 From TblGroupSite ");
            //    SQL.AppendLine("    Where SiteCode=C.SiteCode ");
            //    SQL.AppendLine("    And GrpCode In ( ");
            //    SQL.AppendLine("        Select GrpCode From TblUser ");
            //    SQL.AppendLine("        Where UserCode=@UserCode ");
            //    SQL.AppendLine("    ) ");
            //    SQL.AppendLine("))) ");
            //}   
            //SQL.AppendLine("Left Join TblProfitCenter D On C.ProfitCenterCode=D.ProfitCenterCode ");
            //SQL.AppendLine("Left Join TblEntity E On D.EntCode=E.EntCode ");
            SQL.AppendLine("Left Join TblOption F On A.RptDashboardType=F.OptCode And F.OptCat = 'RptDashboardType' ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Report Code", 
                        "Report Name",
                        "Site",
                        "Setting Code",
                        "Setting Description",

                        //6-10
                        "Sequence",
                        "COA's Account#",
                        "Formula",
                        "Created"+Environment.NewLine+"By",  
                        "Created"+Environment.NewLine+"Date",
                        
                        //11-15
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 250, 200, 100, 200, 
                        
                        //6-10
                        100, 300, 150, 100, 100, 
                        
                        //11-15
                        100, 100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 10, 13 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtRptFicoCode.Text, new string[] { "A.RptDashboardCode", "F.OptDesc" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By A.RptDashboardCode, F.OptDesc;",
                        new string[]
                        {
                            //0
                            "RptDashboardCode", 
                            
                            //1-5
                            "RptDashboardName", "SiteName", "SettingCode", "SettingDesc", "Sequence", 
                            
                            //6-10
                            "AcNo", "Formula", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //11-12
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            //Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtRptFicoCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRptFicoCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Report");
        }

        #endregion

        #endregion
    }
}
