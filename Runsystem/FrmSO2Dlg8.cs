﻿#region Update
/* 18/11/2021 [ISD/IOK] dialog baru
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSO2Dlg8 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSO2 mFrmParent;
        private string mSQL = string.Empty;
        internal bool mIsFilterByItCt = false;
        #endregion

        #region Constructor

        public FrmSO2Dlg8(FrmSO2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            //GetParameter();
            SetGrd();
            SetSQL();
            Sl.SetLueItCtCode(ref LueItCtCode);
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 35;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code", 
                        "Item's Name",
                        "Foreign Name",
                        "Local Code",
                        "Old Code",
                        
                        //6-10
                        "Active",
                        "Category",
                        "Sub Category",
                        "Group",
                        "HS Code",
                        
                        //11-15
                        "UoM",
                        "Length",
                        "UoM",
                        "Height",
                        "UoM",
                        
                        //16-20
                        "Width",
                        "UoM",
                        "Diameter",
                        "UoM",
                        "Volume",
                        
                        //21-25
                        "UoM",
                        "Inventory",
                        "Sales",
                        "Purchase",
                        "Fixed"+Environment.NewLine+"Asset",
                        
                        //26-30
                        "Planning",
                        "Specification",
                        "Pricing Group",
                        "Created By",
                        "Created Date",
                        
                        //31-34
                        "Created Time", 
                        "Last Updated By", 
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        80, 300, 200, 100, 130, 
                        
                        //6-10
                        60, 200, 150, 120, 150, 
                        
                        //11-15
                        100, 80, 100, 80, 100, 

                        //16-20
                        80, 100, 80, 100, 80, 

                        //21-25
                        100, 100, 100, 100, 100, 

                        //26-30
                        100, 200, 200, 130, 130, 
                        
                        //31-34
                        130, 130, 130, 130
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34 });
            Sm.GrdColCheck(Grd1, new int[] { 6, 22, 23, 24, 25, 26 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16, 18, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 30, 33 });
            Sm.GrdFormatTime(Grd1, new int[] { 31, 34 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 29, 30, 31, 32, 33, 34 }, false);
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.ForeignName, A.ItCodeInternal, A.ItCodeOld, A.ActInd, B.ItCtName, ");
            SQL.AppendLine("C.ItScName, D.ItGrpName, A.InventoryUomCode, ");
            SQL.AppendLine("A.Length, A.LengthUomCode, A.Height, A.HeightUomCode, A.Width, A.WidthUomCode, ");
            SQL.AppendLine("A.Diameter, A.DiameterUomCode, A.Volume, A.VolumeUomCode, A.HSCode, ");
            SQL.AppendLine("A.InventoryItemInd, A.SalesItemInd, A.PurchaseItemInd, A.FixedItemInd, A.PlanningItemInd, A.Specification, E.PGName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Left Join TblItemSubCategory C On A.ItScCode=C.ItScCode ");
            SQL.AppendLine("Left Join TblItemGroup D On A.ItGrpCode=D.ItGrpCode ");
            SQL.AppendLine("Left Join TblPricingGroup E On A.PGCode=E.PGCode ");
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine("And A.ActInd='Y' ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (mFrmParent.mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ItName;",
                        new string[]
                        {
                            //0
                            "ItCode", 
                                
                            //1-5
                            "ItName", "ForeignName", "ItCodeInternal", "ItCodeOld", "ActInd", 
                            
                            //6-10
                             "ItCtName", "ItScName", "ItGrpName", "HSCode",  "InventoryUomCode", 
                            
                            //11-15
                            "Length", "LengthUomCode", "Height",  "HeightUomCode", "Width", 
                            
                            //16-20
                            "WidthUomCode", "Diameter", "DiameterUomCode", "Volume", "VolumeUomCode", 
                            
                            //21-25
                            "InventoryItemInd", "SalesItemInd", "PurchaseItemInd", "FixedItemInd", "PlanningItemInd", 
                            
                            //26-30
                            "Specification", "PGName", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //31
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 31, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 34, 31);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                Row1 = mFrmParent.Grd1.CurCell.RowIndex;
                Row2 = Grd1.CurRow.Index;

                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 32, Grd1, Row2, 1);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 2);

                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        #endregion

        #endregion

    }
}
