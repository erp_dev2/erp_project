﻿#region Update
// 12/11/2018 [HAR] tambah parameter buat nentuin format koneksi FTP nya
// 13/02/2019 [HAR] Bug fixing waktu insert
// 28/03/2019 [HAR] tambah dialog untuk ambil shipment planning document
// 05/04/2019 [HAR] ubah kondisi save (save data kemudian upload file)
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmAttachmentFile : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty,
          mStatusInput = "0";//0=view, 1=insert, 2=edit
        internal FrmAttachmentFileFind FrmFind;


        private byte[] downloadedData;


        #endregion

        #region Constructor

        public FrmAttachmentFile(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #region standard method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtDocNo, DteDocDt, 
                       TxtFile1, TxtFile2, TxtFile3, TxtFile4, TxtFile5, TxtFile6, TxtFile7, TxtFile8, TxtFile9, TxtFile10,
                       TxtFile11, TxtFile12, TxtFile13, TxtFile14, TxtFile15, TxtFile16, TxtFile17, TxtFile18, TxtFile19, TxtFile20,
                    }, true);
                    ChkFile1.Enabled = ChkFile2.Enabled = ChkFile3.Enabled = ChkFile4.Enabled = ChkFile5.Enabled = 
                    ChkFile6.Enabled = ChkFile7.Enabled = ChkFile8.Enabled = ChkFile9.Enabled = ChkFile10.Enabled =
                    ChkFile11.Enabled = ChkFile12.Enabled = ChkFile13.Enabled = ChkFile14.Enabled = ChkFile15.Enabled =
                    ChkFile16.Enabled = ChkFile17.Enabled = ChkFile18.Enabled = ChkFile19.Enabled = ChkFile20.Enabled = false;
                    BtnUpload1.Enabled = BtnUpload2.Enabled = BtnUpload3.Enabled = BtnUpload4.Enabled = BtnUpload5.Enabled =
                    BtnUpload6.Enabled = BtnUpload7.Enabled = BtnUpload8.Enabled = BtnUpload9.Enabled = BtnUpload10.Enabled = false;
                    BtnUpload11.Enabled = BtnUpload12.Enabled = BtnUpload13.Enabled = BtnUpload14.Enabled = BtnUpload15.Enabled =
                    BtnUpload16.Enabled = BtnUpload17.Enabled = BtnUpload18.Enabled = BtnUpload19.Enabled = BtnUpload20.Enabled = false;
                    BtnDownload1.Enabled = BtnDownload2.Enabled = BtnDownload3.Enabled = BtnDownload4.Enabled = BtnDownload5.Enabled =
                    BtnDownload6.Enabled = BtnDownload7.Enabled = BtnDownload8.Enabled = BtnDownload9.Enabled = BtnDownload10.Enabled = true;
                    BtnDownload11.Enabled = BtnDownload12.Enabled = BtnDownload13.Enabled = BtnDownload14.Enabled = BtnDownload15.Enabled =
                    BtnDownload16.Enabled = BtnDownload17.Enabled = BtnDownload18.Enabled = BtnDownload19.Enabled = BtnDownload20.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> {  DteDocDt, }, false);
                    ChkFile1.Enabled = ChkFile2.Enabled = ChkFile3.Enabled = ChkFile4.Enabled = ChkFile5.Enabled =
                    ChkFile6.Enabled = ChkFile7.Enabled = ChkFile8.Enabled = ChkFile9.Enabled = ChkFile10.Enabled = true;
                    ChkFile11.Enabled = ChkFile12.Enabled = ChkFile13.Enabled = ChkFile14.Enabled = ChkFile15.Enabled =
                    ChkFile16.Enabled = ChkFile17.Enabled = ChkFile18.Enabled = ChkFile19.Enabled = ChkFile20.Enabled = true;
                    BtnUpload1.Enabled = BtnUpload2.Enabled = BtnUpload3.Enabled = BtnUpload4.Enabled = BtnUpload5.Enabled =
                    BtnUpload6.Enabled = BtnUpload7.Enabled = BtnUpload8.Enabled = BtnUpload9.Enabled = BtnUpload10.Enabled =
                    BtnUpload11.Enabled = BtnUpload12.Enabled = BtnUpload13.Enabled = BtnUpload14.Enabled = BtnUpload15.Enabled =
                    BtnUpload16.Enabled = BtnUpload17.Enabled = BtnUpload18.Enabled = BtnUpload19.Enabled = BtnUpload20.Enabled = true;
                    BtnDownload1.Enabled = BtnDownload2.Enabled = BtnDownload3.Enabled = BtnDownload4.Enabled = BtnDownload5.Enabled =
                    BtnDownload6.Enabled = BtnDownload7.Enabled = BtnDownload8.Enabled = BtnDownload9.Enabled = BtnDownload10.Enabled = false;
                    BtnDownload11.Enabled = BtnDownload12.Enabled = BtnDownload13.Enabled = BtnDownload14.Enabled = BtnDownload15.Enabled =
                    BtnDownload16.Enabled = BtnDownload17.Enabled = BtnDownload18.Enabled = BtnDownload19.Enabled = BtnDownload20.Enabled = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    TxtDocNo.Focus();
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, }, false);
                    ChkFile1.Enabled = ChkFile2.Enabled = ChkFile3.Enabled = ChkFile4.Enabled = ChkFile5.Enabled =
                    ChkFile6.Enabled = ChkFile7.Enabled = ChkFile8.Enabled = ChkFile9.Enabled = ChkFile10.Enabled = true;
                    ChkFile11.Enabled = ChkFile12.Enabled = ChkFile13.Enabled = ChkFile14.Enabled = ChkFile15.Enabled =
                    ChkFile16.Enabled = ChkFile17.Enabled = ChkFile18.Enabled = ChkFile19.Enabled = ChkFile20.Enabled = true;
                    BtnUpload1.Enabled = BtnUpload2.Enabled = BtnUpload3.Enabled = BtnUpload4.Enabled = BtnUpload5.Enabled =
                    BtnUpload6.Enabled = BtnUpload7.Enabled = BtnUpload8.Enabled = BtnUpload9.Enabled = BtnUpload10.Enabled =
                    BtnUpload11.Enabled = BtnUpload12.Enabled = BtnUpload13.Enabled = BtnUpload14.Enabled = BtnUpload15.Enabled =
                    BtnUpload16.Enabled = BtnUpload17.Enabled = BtnUpload18.Enabled = BtnUpload19.Enabled = BtnUpload20.Enabled = true;
                    BtnDownload1.Enabled = BtnDownload2.Enabled = BtnDownload3.Enabled = BtnDownload4.Enabled = BtnDownload5.Enabled =
                    BtnDownload6.Enabled = BtnDownload7.Enabled = BtnDownload8.Enabled = BtnDownload9.Enabled = BtnDownload10.Enabled = false;
                    BtnDownload11.Enabled = BtnDownload12.Enabled = BtnDownload13.Enabled = BtnDownload14.Enabled = BtnDownload15.Enabled =
                    BtnDownload16.Enabled = BtnDownload17.Enabled = BtnDownload18.Enabled = BtnDownload19.Enabled = BtnDownload20.Enabled = false;
                    DteDocDt.Focus();
                    break;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, 
                TxtFile1, TxtFile2, TxtFile3, TxtFile4, TxtFile5, TxtFile6, TxtFile7, TxtFile8, TxtFile9, TxtFile10,
                TxtFile11, TxtFile12, TxtFile13, TxtFile14, TxtFile15, TxtFile16, TxtFile17, TxtFile18, TxtFile19, TxtFile20
            });
            ChkFile1.Checked = ChkFile2.Checked = ChkFile3.Checked = ChkFile4.Checked = ChkFile5.Checked =
            ChkFile6.Checked = ChkFile7.Checked = ChkFile8.Checked = ChkFile9.Checked = ChkFile10.Checked = false;
            ChkFile11.Checked = ChkFile12.Checked = ChkFile13.Checked = ChkFile14.Checked = ChkFile15.Checked =
            ChkFile16.Checked = ChkFile17.Checked = ChkFile18.Checked = ChkFile19.Checked = ChkFile20.Checked = false;
            PbUpload1.Value = PbUpload2.Value = PbUpload3.Value = PbUpload4.Value = PbUpload5.Value =
            PbUpload6.Value = PbUpload7.Value = PbUpload8.Value = PbUpload9.Value = PbUpload10.Value = 0;
            PbUpload11.Value = PbUpload12.Value = PbUpload13.Value = PbUpload14.Value = PbUpload15.Value =
            PbUpload16.Value = PbUpload17.Value = PbUpload18.Value = PbUpload19.Value = PbUpload20.Value = 0;
        }
        #endregion 

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAttachmentFileFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                mStatusInput = "1";
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            mStatusInput = "2";
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                string DocNo = string.Empty;
                Cursor.Current = Cursors.WaitCursor;
                if (Sm.StdMsgYN("Question",
                 "Do you want to save this data ?"
                 ) == DialogResult.No ||
             IsUploadFileNotValid()) return;

                if (mStatusInput == "2")
                {
                    DocNo = TxtDocNo.Text;
                    if (TxtFile1.Text.Length > 1 && ChkFile1.Checked == true)
                        UploadFile(DocNo, TxtFile1, PbUpload1, "1");
                    if (TxtFile2.Text.Length > 1 && ChkFile2.Checked == true)
                        UploadFile(DocNo, TxtFile2, PbUpload2, "2");
                    if (TxtFile3.Text.Length > 1 && ChkFile3.Checked == true)
                        UploadFile(DocNo, TxtFile3, PbUpload3, "3");
                    if (TxtFile4.Text.Length > 1 && ChkFile4.Checked == true)
                        UploadFile(DocNo, TxtFile4, PbUpload4, "4");
                    if (TxtFile5.Text.Length > 1 && ChkFile5.Checked == true)
                        UploadFile(DocNo, TxtFile5, PbUpload5, "5");

                    if (TxtFile6.Text.Length > 1 && ChkFile6.Checked == true)
                        UploadFile(DocNo, TxtFile6, PbUpload6, "6");
                    if (TxtFile7.Text.Length > 1 && ChkFile7.Checked == true)
                        UploadFile(DocNo, TxtFile7, PbUpload7, "7");
                    if (TxtFile8.Text.Length > 1 && ChkFile8.Checked == true)
                        UploadFile(DocNo, TxtFile8, PbUpload8, "8");
                    if (TxtFile9.Text.Length > 1 && ChkFile9.Checked == true)
                        UploadFile(DocNo, TxtFile9, PbUpload9, "9");
                    if (TxtFile10.Text.Length > 1 && ChkFile10.Checked == true)
                        UploadFile(DocNo, TxtFile10, PbUpload5, "10");

                    if (TxtFile11.Text.Length > 1 && ChkFile11.Checked == true)
                        UploadFile(DocNo, TxtFile11, PbUpload11, "11");
                    if (TxtFile12.Text.Length > 1 && ChkFile12.Checked == true)
                        UploadFile(DocNo, TxtFile12, PbUpload12, "12");
                    if (TxtFile13.Text.Length > 1 && ChkFile13.Checked == true)
                        UploadFile(DocNo, TxtFile13, PbUpload13, "13");
                    if (TxtFile14.Text.Length > 1 && ChkFile14.Checked == true)
                        UploadFile(DocNo, TxtFile14, PbUpload14, "14");
                    if (TxtFile15.Text.Length > 1 && ChkFile15.Checked == true)
                        UploadFile(DocNo, TxtFile15, PbUpload15, "15");

                    if (TxtFile16.Text.Length > 1 && ChkFile16.Checked == true)
                        UploadFile(DocNo, TxtFile16, PbUpload16, "16");
                    if (TxtFile17.Text.Length > 1 && ChkFile17.Checked == true)
                        UploadFile(DocNo, TxtFile17, PbUpload17, "17");
                    if (TxtFile18.Text.Length > 1 && ChkFile18.Checked == true)
                        UploadFile(DocNo, TxtFile18, PbUpload18, "18");
                    if (TxtFile19.Text.Length > 1 && ChkFile19.Checked == true)
                        UploadFile(DocNo, TxtFile19, PbUpload19, "19");
                    if (TxtFile20.Text.Length > 1 && ChkFile20.Checked == true)
                        UploadFile(DocNo, TxtFile20, PbUpload20, "20");

                    ShowData(DocNo);
                }
                else
                {
                    var cml = new List<MySqlCommand>();
                    cml.Add(SaveAttachmentFile());
                    Sm.ExecCommands(cml);
                    SaveUploadFile();
                    
                    ShowData(TxtDocNo.Text);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            mStatusInput = "0";
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
    
        }



        #endregion 

        #region Save Data

        private void SaveUploadFile()
        {
            

            if (TxtFile1.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile1, PbUpload1, "1");
            if (TxtFile2.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile2, PbUpload2, "2");
            if (TxtFile3.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile3, PbUpload3, "3");
            if (TxtFile4.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile4, PbUpload4, "4");
            if (TxtFile5.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile5, PbUpload5, "5");
            if (TxtFile6.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile6, PbUpload6, "6");
            if (TxtFile7.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile7, PbUpload7, "7");
            if (TxtFile8.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile8, PbUpload8, "8");
            if (TxtFile9.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile9, PbUpload9, "9");
            if (TxtFile10.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile10, PbUpload10, "10");
            if (TxtFile11.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile11, PbUpload11, "11");
            if (TxtFile12.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile12, PbUpload12, "12");
            if (TxtFile13.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile13, PbUpload13, "13");
            if (TxtFile14.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile14, PbUpload14, "14");
            if (TxtFile15.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile15, PbUpload15, "15");
            if (TxtFile16.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile16, PbUpload16, "16");
            if (TxtFile17.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile17, PbUpload17, "17");
            if (TxtFile18.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile18, PbUpload18, "18");
            if (TxtFile19.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile19, PbUpload19, "19");
            if (TxtFile20.Text.Length > 1)
                UploadFile(TxtDocNo.Text, TxtFile20, PbUpload20, "20");
        }

        private bool IsUploadFileNotValid2()
        {
            return
                IsFileSizeNotvalid(TxtFile1) ||
                IsFileSizeNotvalid(TxtFile2) ||
                IsFileSizeNotvalid(TxtFile3) ||
                IsFileSizeNotvalid(TxtFile4) ||
                IsFileSizeNotvalid(TxtFile5) ||
                IsFileSizeNotvalid(TxtFile6) ||
                IsFileSizeNotvalid(TxtFile7) ||
                IsFileSizeNotvalid(TxtFile8) ||
                IsFileSizeNotvalid(TxtFile9) ||
                IsFileSizeNotvalid(TxtFile10) 
                //IsFileNameAlreadyExisted(TxtFile1) ||
                //IsFileNameAlreadyExisted(TxtFile2) ||
                //IsFileNameAlreadyExisted(TxtFile3) ||
                //IsFileNameAlreadyExisted(TxtFile4) ||
                //IsFileNameAlreadyExisted(TxtFile5) ||
                //IsFileNameAlreadyExisted(TxtFile6) ||
                //IsFileNameAlreadyExisted(TxtFile7) ||
                //IsFileNameAlreadyExisted(TxtFile8) ||
                //IsFileNameAlreadyExisted(TxtFile9) ||
                //IsFileNameAlreadyExisted(TxtFile10)
             ;
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Local Document", false)||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsUploadFileNotValid2()||
                IsLocalExisted()
                ;
            ;
        }


        private bool IsLocalExisted()
        {
            if (Sm.IsDataExist("Select LocalDocNo From TblAttachmentFile Where LocalDocNo=@LocalDocNo;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Local Document ( " + TxtDocNo.Text + " ) already existed.");
                return true;
            }
            return false;
        }
        private bool IsFTPClientDataNotValid()
        {

            if (mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(DevExpress.XtraEditors.TextEdit TxtFile)
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(DevExpress.XtraEditors.TextEdit TxtFile)
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblAttachmentFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveAttachmentFile()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAttachmentFile(LocalDocNo, DocDt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@LocalDocNo, @DocDt, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateAttachmentFile(string LocalDocNo, string FileName, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAttachmentFile Set ");
            SQL.AppendLine("    FileName"+Code+"=@FileName ");
            SQL.AppendLine("Where LocalDocNo=@LocalDocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        #endregion 

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowAttachmentFileHdr(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAttachmentFileHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@LocalDocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select LocalDocNo, DocDt, FileName1, FileName2, FileName3, FileName4, FileName5, " +
                    "FileName6, FileName7, FileName8, FileName9, FileName10, " +
                    "FileName11, FileName12, FileName13, FileName14, FileName15, "+
                    "FileName16, FileName17, FileName18, FileName19, FileName20 " +
                    "From TblAttachmentFile Where LocalDocNo=@LocalDocNo;",
                    new string[] 
                    { 
                        "LocalDocNo", 
                        "DocDt", "FileName1", "FileName2", "FileName3",  "FileName4", 
                        "FileName5", "FileName6", "FileName7", "FileName8", "FileName9", 
                        "FileName10", "FileName11", "FileName12", "FileName13",  "FileName14", 
                        "FileName15", "FileName16", "FileName17", "FileName18", "FileName19", 
                        "FileName20", 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtFile1.EditValue = Sm.DrStr(dr, c[2]);
                        if (TxtFile1.Text.Length > 0)
                            ChkFile1.Checked = false;
                        TxtFile2.EditValue = Sm.DrStr(dr, c[3]);
                        if (TxtFile2.Text.Length > 0)
                            ChkFile2.Checked = false;
                        TxtFile3.EditValue = Sm.DrStr(dr, c[4]);
                        if (TxtFile3.Text.Length > 0)
                            ChkFile3.Checked = false;
                        TxtFile4.EditValue = Sm.DrStr(dr, c[5]);
                        if (TxtFile4.Text.Length > 0)
                            ChkFile4.Checked = false;
                        TxtFile5.EditValue = Sm.DrStr(dr, c[6]);
                        if (TxtFile5.Text.Length > 0)
                            ChkFile5.Checked = false;
                        TxtFile6.EditValue = Sm.DrStr(dr, c[7]);
                        if (TxtFile6.Text.Length > 0)
                            ChkFile6.Checked = false;
                        TxtFile7.EditValue = Sm.DrStr(dr, c[8]);
                        if (TxtFile7.Text.Length > 0)
                            ChkFile7.Checked = false;
                        TxtFile8.EditValue = Sm.DrStr(dr, c[9]);
                        if (TxtFile8.Text.Length > 0)
                            ChkFile8.Checked = false;
                        TxtFile9.EditValue = Sm.DrStr(dr, c[10]);
                        if (TxtFile9.Text.Length > 0)
                            ChkFile9.Checked = false;
                        TxtFile10.EditValue = Sm.DrStr(dr, c[11]);
                        if (TxtFile10.Text.Length > 0)
                            ChkFile10.Checked = false;
                        TxtFile11.EditValue = Sm.DrStr(dr, c[12]);
                        if (TxtFile11.Text.Length > 0)
                            ChkFile11.Checked = false;
                        TxtFile12.EditValue = Sm.DrStr(dr, c[13]);
                        if (TxtFile12.Text.Length > 0)
                            ChkFile12.Checked = false;
                        TxtFile13.EditValue = Sm.DrStr(dr, c[14]);
                        if (TxtFile13.Text.Length > 0)
                            ChkFile13.Checked = false;
                        TxtFile14.EditValue = Sm.DrStr(dr, c[15]);
                        if (TxtFile14.Text.Length > 0)
                            ChkFile14.Checked = false;
                        TxtFile15.EditValue = Sm.DrStr(dr, c[16]);
                        if (TxtFile15.Text.Length > 0)
                            ChkFile15.Checked = false;
                        TxtFile16.EditValue = Sm.DrStr(dr, c[17]);
                        if (TxtFile16.Text.Length > 0)
                            ChkFile16.Checked = false;
                        TxtFile17.EditValue = Sm.DrStr(dr, c[18]);
                        if (TxtFile17.Text.Length > 0)
                            ChkFile17.Checked = false;
                        TxtFile18.EditValue = Sm.DrStr(dr, c[19]);
                        if (TxtFile18.Text.Length > 0)
                            ChkFile18.Checked = false;
                        TxtFile19.EditValue = Sm.DrStr(dr, c[20]);
                        if (TxtFile19.Text.Length > 0)
                            ChkFile19.Checked = false;
                        TxtFile20.EditValue = Sm.DrStr(dr, c[21]);
                        if (TxtFile20.Text.Length > 0)
                            ChkFile20.Checked = false;
                    }, true
                );
        }


        #endregion

        #region Additional method

        private void GetParameter()
        {
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mStatusInput = "0";
        }

        private void AddFile(DevExpress.XtraEditors.TextEdit TxtFile, DevExpress.XtraEditors.CheckEdit ChkFile)
        {
            try
            {
                ChkFile.Checked = true;
                OD1.InitialDirectory = "c:";
                OD1.Filter = "PDF files (*.pdf)|*.pdf";
                OD1.FilterIndex = 2;
                OD1.ShowDialog();

                TxtFile.Text = OD1.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void UploadFile(string DocNo, DevExpress.XtraEditors.TextEdit TxtFile, System.Windows.Forms.ProgressBar PbUpload, string Code)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            
            if (mFormatFTPClient == "1")
            {
                #region type 1
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

                Stream ftpStream = request.GetRequestStream();
                FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

                int length = 1024;
                byte[] buffer = new byte[length];
                int bytesRead = 0;

                do
                {
                    bytesRead = file.Read(buffer, 0, length);
                    ftpStream.Write(buffer, 0, bytesRead);

                    PbUpload.Invoke(
                        (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                    byte[] buffers = new byte[10240];
                    int read;
                    while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                    {
                        ftpStream.Write(buffers, 0, read);
                        PbUpload.Invoke(
                            (MethodInvoker)delegate
                            {
                                PbUpload.Value = (int)file.Position;
                            });
                    }
                }
                while (bytesRead != 0);

                file.Close();
                ftpStream.Close();
                #endregion
            }
            else
            {
                #region type 2
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

                Stream ftpStream = request.GetRequestStream();
                FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

                int length = 1024;
                byte[] buffer = new byte[length];
                int bytesRead = 0;

                do
                {
                    bytesRead = file.Read(buffer, 0, length);
                    ftpStream.Write(buffer, 0, bytesRead);

                    PbUpload.Invoke(
                        (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                    byte[] buffers = new byte[10240];
                    int read;
                    while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                    {
                        ftpStream.Write(buffers, 0, read);
                        PbUpload.Invoke(
                            (MethodInvoker)delegate
                            {
                                PbUpload.Value = (int)file.Position;
                            });
                    }
                }
                while (bytesRead != 0);

                file.Close();
                ftpStream.Close();
                #endregion
            }
            

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateAttachmentFile(DocNo, toUpload.Name, Code));
            Sm.ExecCommands(cml);
        }

        private void DownloadFileKu(DevExpress.XtraEditors.TextEdit TxtFile, System.Windows.Forms.ProgressBar PbUpload)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, PbUpload);
            SFD1.FileName = TxtFile.Text;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {
            
                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared, System.Windows.Forms.ProgressBar PbUpload)
        {
            downloadedData = new byte[0];

            try
            {
                //this.Text = "Connecting...";
                Application.DoEvents();

                if (mFormatFTPClient == "1")
                {
                    #region type 1
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    //this.Text = "Retrieving Information...";
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = "Downloading File...";
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Set up progress bar
                    PbUpload.Value = 0;
                    PbUpload.Maximum = dataLength;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            PbUpload.Value = PbUpload.Maximum;

                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);

                            if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                            {
                                PbUpload.Value += bytesRead;

                                PbUpload.Refresh();
                                Application.DoEvents();
                            }
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
                else
                {
                    #region  type 2
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    //this.Text = "Retrieving Information...";
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = "Downloading File...";
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Set up progress bar
                    PbUpload.Value = 0;
                    PbUpload.Maximum = dataLength;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            PbUpload.Value = PbUpload.Maximum;

                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);

                            if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                            {
                                PbUpload.Value += bytesRead;

                                PbUpload.Refresh();
                                Application.DoEvents();
                            }
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

       
        #endregion

        #region Event

        #region Chk
        private void ChkFile1_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile1.Checked == false)
            {
                TxtFile1.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        private void ChkFile4_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile4.Checked == false)
            {
                TxtFile4.EditValue = string.Empty;
            }
        }

        private void ChkFile5_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile5.Checked == false)
            {
                TxtFile5.EditValue = string.Empty;
            }
        }

        private void ChkFile6_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile6.Checked == false)
            {
                TxtFile6.EditValue = string.Empty;
            }
        }

        private void ChkFile7_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile7.Checked == false)
            {
                TxtFile7.EditValue = string.Empty;
            }
        }

        private void ChkFile8_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile8.Checked == false)
            {
                TxtFile8.EditValue = string.Empty;
            }
        }

        private void ChkFile9_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile9.Checked == false)
            {
                TxtFile9.EditValue = string.Empty;
            }
        }

        private void ChkFile10_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile10.Checked == false)
            {
                TxtFile10.EditValue = string.Empty;
            }
        }

        private void ChkFile11_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile11.Checked == false)
            {
                TxtFile11.EditValue = string.Empty;
            }
        }

        private void ChkFile12_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile12.Checked == false)
            {
                TxtFile12.EditValue = string.Empty;
            }
        }

        private void ChkFile13_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile13.Checked == false)
            {
                TxtFile13.EditValue = string.Empty;
            }
        }

        private void ChkFile14_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile14.Checked == false)
            {
                TxtFile14.EditValue = string.Empty;
            }
        }

        private void ChkFile15_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile15.Checked == false)
            {
                TxtFile15.EditValue = string.Empty;
            }
        }

        private void ChkFile16_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile16.Checked == false)
            {
                TxtFile16.EditValue = string.Empty;
            }
        }

        private void ChkFile17_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile17.Checked == false)
            {
                TxtFile17.EditValue = string.Empty;
            }
        }

        private void ChkFile18_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile18.Checked == false)
            {
                TxtFile18.EditValue = string.Empty;
            }
        }

        private void ChkFile19_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile19.Checked == false)
            {
                TxtFile19.EditValue = string.Empty;
            }
        }

        private void ChkFile20_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile20.Checked == false)
            {
                TxtFile20.EditValue = string.Empty;
            }
        }
        #endregion

        #region Btn
        private void BtnUpload1_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile1, ChkFile1);
        }

        private void BtnUpload2_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile2, ChkFile2);
        }

        private void BtnUpload3_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile3, ChkFile3);
        }

        private void BtnUpload4_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile4, ChkFile4);
        }

        private void BtnUpload5_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile5, ChkFile5);
        }

        private void BtnUpload6_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile6, ChkFile6);
        }

        private void BtnUpload7_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile7, ChkFile7);
        }

        private void BtnUpload8_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile8, ChkFile8);
        }

        private void BtnUpload9_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile9, ChkFile9);
        }

        private void BtnUpload10_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile10, ChkFile10);
        }

        private void BtnUpload11_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile11, ChkFile11);
        }

        private void BtnUpload12_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile12, ChkFile12);
        }

        private void BtnUpload13_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile13, ChkFile13);
        }

        private void BtnUpload14_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile14, ChkFile14);
        }

        private void BtnUpload15_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile15, ChkFile15);
        }

        private void BtnUpload16_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile16, ChkFile16);
        }

        private void BtnUpload17_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile17, ChkFile17);
        }

        private void BtnUpload18_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile18, ChkFile18);
        }

        private void BtnUpload19_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile19, ChkFile19);
        }

        private void BtnUpload20_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile20, ChkFile20);
        }


        private void BtnDownload1_Click(object sender, EventArgs e)
        {
            if (TxtFile1.Text.Length > 0)
                DownloadFileKu(TxtFile1, PbUpload1);
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            if (TxtFile2.Text.Length > 0)
                DownloadFileKu(TxtFile2, PbUpload2);
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            if (TxtFile3.Text.Length > 0)
                DownloadFileKu(TxtFile3, PbUpload3);
        }

        private void BtnDownload4_Click(object sender, EventArgs e)
        {
            if (TxtFile4.Text.Length > 0)
                DownloadFileKu(TxtFile4, PbUpload4);
        }

        private void BtnDownload5_Click(object sender, EventArgs e)
        {
            if (TxtFile5.Text.Length > 0)
                DownloadFileKu(TxtFile5, PbUpload5);
        }

        private void BtnDownload6_Click(object sender, EventArgs e)
        {
            if (TxtFile6.Text.Length > 0)
                DownloadFileKu(TxtFile6, PbUpload6);
        }

        private void BtnDownload7_Click(object sender, EventArgs e)
        {
            if (TxtFile7.Text.Length > 0)
                DownloadFileKu(TxtFile7, PbUpload7);
        }

        private void BtnDownload8_Click(object sender, EventArgs e)
        {
            if (TxtFile8.Text.Length > 0)
                DownloadFileKu(TxtFile8, PbUpload8);
        }

        private void BtnDownload9_Click(object sender, EventArgs e)
        {
            if (TxtFile9.Text.Length > 0)
                DownloadFileKu(TxtFile9, PbUpload9);
        }

        private void BtnDownload10_Click(object sender, EventArgs e)
        {
            if (TxtFile10.Text.Length > 0)
                DownloadFileKu(TxtFile10, PbUpload10);
        }

        private void BtnDownload11_Click(object sender, EventArgs e)
        {
            if (TxtFile11.Text.Length > 0)
                DownloadFileKu(TxtFile11, PbUpload11);
        }

        private void BtnDownload12_Click(object sender, EventArgs e)
        {
            if (TxtFile12.Text.Length > 0)
                DownloadFileKu(TxtFile12, PbUpload12);
        }

        private void BtnDownload13_Click(object sender, EventArgs e)
        {
            if (TxtFile13.Text.Length > 0)
                DownloadFileKu(TxtFile13, PbUpload13);

        }

        private void BtnDownload14_Click(object sender, EventArgs e)
        {
            if (TxtFile14.Text.Length > 0)
                DownloadFileKu(TxtFile14, PbUpload14);
        }

        private void BtnDownload15_Click(object sender, EventArgs e)
        {
            if (TxtFile15.Text.Length > 0)
                DownloadFileKu(TxtFile15, PbUpload15);
        }

        private void BtnDownload16_Click(object sender, EventArgs e)
        {
            if (TxtFile16.Text.Length > 0)
                DownloadFileKu(TxtFile16, PbUpload16);
        }

        private void BtnDownload17_Click(object sender, EventArgs e)
        {
            if (TxtFile17.Text.Length > 0)
                DownloadFileKu(TxtFile17, PbUpload17);
        }

        private void BtnDownload18_Click(object sender, EventArgs e)
        {
            if (TxtFile18.Text.Length > 0)
                DownloadFileKu(TxtFile18, PbUpload18);
        }

        private void BtnDownload19_Click(object sender, EventArgs e)
        {
            if (TxtFile19.Text.Length > 0)
                DownloadFileKu(TxtFile19, PbUpload19);
        }

        private void BtnDownload20_Click(object sender, EventArgs e)
        {
            if (TxtFile20.Text.Length > 0)
                DownloadFileKu(TxtFile20, PbUpload20);
        }

        private void BtnSP_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAttachmentFileDlg(this));
        }

        #endregion 

        




        #endregion

        

      
    }
}
