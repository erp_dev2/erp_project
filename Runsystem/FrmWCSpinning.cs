﻿#region
/*
    01/11/2017 [TKG] Aplikasi baru
    13/11/2017 [TKG] menggunakan button untuk memilih work center
    16/03/2018 [TKG] menambah formula non item
    06/06/2018 [TKG] total machine menjadi 2 decimal point
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWCSpinning : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmWCSpinningFind FrmFind;

        #endregion

        #region Constructor

        public FrmWCSpinning(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueProdFormula, "ProdFormula");
                SetLueFSNICode(ref LueFSNICode1, "N");
                SetLueFSNICode(ref LueFSNICode2, "Y");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }


        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Code",

                        //1-4
                        "Efficiency Variable",
                        "Used",
                        "Formula",
                        "Default Value"
                    },
                     new int[] 
                    {
                        0,
                        200, 80, 80, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 2, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 3);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1 });
        }

        private void ShowGrdData()
        {
            var cm = new MySqlCommand();

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select OptCode, OptDesc From TblOption " +
                    "Where OptCat='ProdEfficiencyVar' Order By OptDesc;",

                    new string[] 
                       { 
                           "OptCode", "OptDesc"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Row, new int[] { 2, 3 });
                        Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 4 });
                    }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWCName, TxtTotalMachine, TxtSeqNo, LueProdFormula, MeeRemark,
                        ChkActInd, ChkUnitInd, ChkPercentInd, ChkProdDataPerItemInd, LueFSNICode1,
                        LueFSNICode2
                    }, true);
                    BtnWCCode.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2, 3, 4 });
                    TxtWCCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWCName, TxtTotalMachine, TxtSeqNo, LueProdFormula, MeeRemark,
                        ChkUnitInd, ChkPercentInd, ChkProdDataPerItemInd, LueFSNICode1, LueFSNICode2
                    }, false);
                    BtnWCCode.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 3, 4 });
                    TxtWCCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWCName, TxtTotalMachine, TxtSeqNo, LueProdFormula, MeeRemark,
                        ChkActInd, ChkUnitInd, ChkPercentInd, ChkProdDataPerItemInd, LueFSNICode1, 
                        LueFSNICode2
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 3, 4 });
                    TxtWCCode.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtWCCode, TxtWCName, LueFSNICode1, LueFSNICode2, MeeRemark });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtTotalMachine, TxtSeqNo }, 255);
            ChkActInd.Checked = false;
            ChkUnitInd.Checked = false; 
            ChkPercentInd.Checked = false;
            ChkProdDataPerItemInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 2, 3 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWCSpinningFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                ShowGrdData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWCCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                    IsDataNotValid()) return;
                
                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveWCSpinning());
                if (Grd1.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                        if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                            cml.Add(SaveWCSpinningEffVar(r));
                }

                Sm.ExecCommands(cml);

                ShowData(TxtWCCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 4) Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 4 }, e);
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Show Data

        public void ShowData(string WCCode)
        {
            try
            {
                ClearData();
                ShowWCSpinning(WCCode);
                ShowWCSpinningEffVar(WCCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowWCSpinning(string WCCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WCCode", WCCode);

            Sm.ShowDataInCtrl(
                 ref cm,
                  "Select WCCode, WCName, ProdFormula, TotalMachine, SeqNo, " +
                  "ActInd, UnitInd, PercentInd, ProdDataPerItemInd, FSNICode1, FSNICode2, Remark " +
                  "From TblWCSpinning Where WCCode=@WCCode;",
                 new string[] 
                   {
                      //0
                      "WCCode",

                      //1-5
                      "WCName", "ProdFormula", "TotalMachine", "SeqNo", "ActInd",

                      //6-10
                      "UnitInd", "PercentInd", "ProdDataPerItemInd", "FSNICode1", "FSNICode2", 
                      
                      //11
                      "Remark"
                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtWCCode.EditValue = Sm.DrStr(dr, c[0]);
                     TxtWCName.EditValue = Sm.DrStr(dr, c[1]);
                     Sm.SetLue(LueProdFormula, Sm.DrStr(dr, c[2]));
                     TxtTotalMachine.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 2);
                     TxtSeqNo.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 255);
                     ChkActInd.Checked = Sm.DrStr(dr, c[5]) == "Y";
                     ChkUnitInd.Checked = Sm.DrStr(dr, c[6]) == "Y";
                     ChkPercentInd.Checked = Sm.DrStr(dr, c[7]) == "Y";
                     ChkProdDataPerItemInd.Checked = Sm.DrStr(dr, c[8]) == "Y";
                     Sm.SetLue(LueFSNICode1, Sm.DrStr(dr, c[9]));
                     Sm.SetLue(LueFSNICode2, Sm.DrStr(dr, c[10]));
                     MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                 }, true
             );
        }

        private void ShowWCSpinningEffVar(string WCCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WCCode", WCCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.ProdEfficiencyVar, B.OptDesc, A.UseInd, A.UseFormulaInd, A.DefaultValue " +
                    "From TblWCSpinningEffVar A " +
                    "Left Join TblOption B On B.OptCat='ProdEfficiencyVar' And A.ProdEfficiencyVar=B.OptCode " +
                    "Where A.WCCode=@WCCode Order By A.ProdEfficiencyVar;",

                    new string[] 
                       { 
                           //0
                           "ProdEfficiencyVar",

                           //1-4
                           "OptDesc", "UseInd", "UseFormulaInd", "DefaultValue"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWCCode, "Work center code", false) ||
                Sm.IsTxtEmpty(TxtWCName, "Work center name", false) ||
                Sm.IsTxtEmpty(TxtSeqNo, "Sequence#", true) ||
                Sm.IsLueEmpty(LueProdFormula, "Production formula") ||
                IsWCCodeExisted();
        }

        private bool IsWCCodeExisted()
        {
            if (!TxtWCCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select 1 From TblWCSpinning Where WCCode=@Param;", TxtWCCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Work center code ( " + TxtWCCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveWCSpinning()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWCSpinning ");
            SQL.AppendLine("(WCCode, WCName, ActInd, UnitInd, PercentInd, ProdDataPerItemInd, TotalMachine, SeqNo, ProdFormula, FSNICode1, FSNICode2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@WCCode, @WCName, @ActInd, @UnitInd, @PercentInd, @ProdDataPerItemInd, @TotalMachine, @SeqNo, @ProdFormula, @FSNICode1, @FSNICode2, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update WCName=@WCName, ActInd=@ActInd, UnitInd=@UnitInd, PercentInd=@PercentInd, ProdDataPerItemInd=@ProdDataPerItemInd, TotalMachine=@TotalMachine, SeqNo=@SeqNo, ProdFormula=@ProdFormula, FSNICode1=@FSNICode1, FSNICode2=@FSNICode2, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (TxtWCCode.Properties.ReadOnly)
                SQL.AppendLine("Delete From TblWCSpinningEffVar Where WCCode=@WCCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WCCode", TxtWCCode.Text);
            Sm.CmParam<String>(ref cm, "@WCName", TxtWCName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@UnitInd", ChkUnitInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@PercentInd", ChkPercentInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@ProdDataPerItemInd", ChkProdDataPerItemInd.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@SeqNo", decimal.Parse(TxtSeqNo.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalMachine", decimal.Parse(TxtTotalMachine.Text));
            Sm.CmParam<String>(ref cm, "@ProdFormula", Sm.GetLue(LueProdFormula));
            Sm.CmParam<String>(ref cm, "@FSNICode1", Sm.GetLue(LueFSNICode1));
            Sm.CmParam<String>(ref cm, "@FSNICode2", Sm.GetLue(LueFSNICode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWCSpinningEffVar(int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblWCSpinningEffVar(WCCode, ProdEfficiencyVar, UseInd, UseFormulaInd, DefaultValue, CreateBy, CreateDt) " +
                    "Values(@WCCode, @ProdEfficiencyVar, @UseInd, @UseFormulaInd, @DefaultValue, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@WCCode", TxtWCCode.Text);
            Sm.CmParam<String>(ref cm, "@ProdEfficiencyVar", Sm.GetGrdStr(Grd1, r, 0));
            Sm.CmParam<String>(ref cm, "@UseInd", Sm.GetGrdBool(Grd1, r, 2) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UseFormulaInd", Sm.GetGrdBool(Grd1, r, 3) ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@DefaultValue", Sm.GetGrdDec(Grd1, r, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void SetLueFSNICode(ref LookUpEdit Lue, string TotalInd)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select FSNICode As Col1, FSNIName As Col2 ");
            SQL.AppendLine("From TblFormulaSpinningNonItem ");
            SQL.AppendLine("Where TotalInd=@TotalInd ");
            SQL.AppendLine("Order By FSNIName;");

            Sm.CmParam<String>(ref cm, "@TotalInd", TotalInd);
            cm.CommandText = SQL.ToString();
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Misc Event

        #region Misc Control Event

        private void TxtWCCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtWCCode);
        }

        private void TxtWCName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtWCName);
        }

        private void LueProdFormula_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueProdFormula, new Sm.RefreshLue2(Sl.SetLueOption), "ProdFormula");
        }

        private void TxtTotalMachine_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTotalMachine, 2);
        }

        private void TxtSeqNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtSeqNo, 255);
        }

        private void LueFSNICode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueFSNICode1, new Sm.RefreshLue2(SetLueFSNICode), "N");
        }

        private void LueFSNICode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueFSNICode2, new Sm.RefreshLue2(SetLueFSNICode), "Y");
        }

        #endregion

        private void BtnWCCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmWCSpinningDlg(this));
        }

        #endregion
    }
}
