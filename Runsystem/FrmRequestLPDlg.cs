﻿#region Update
/*
    03/08/2018 [WED] validasi berdasarkan ID Number, alamat. Kalau ada yang belum lunasin pinjamannya, nggak bisa ngajuin pinjaman.
    14/08/2018 [WED] BUG saat ShowDataHdr dan format tanggal BirthDt nya
    18/09/2018 [WED] TblSurveyDtl diganti jadi TblLoanSummary
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRequestLPDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRequestLP mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRequestLPDlg(FrmRequestLP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.ReadOnly = true;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Partner"+Environment.NewLine+"Code", 
                    "Partner Name",
                    "Active",
                    "Address",
                    "City",
                    
                    //6-10
                    "Province",
                    "Postal Code",
                    "Identity#",
                    "Gender",
                    "Birth Date",
                    
                    //11-15
                    "Manpower",
                    "Asset",
                    "Turnover",
                    "Business Category",
                    "Business Sector",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 200, 80, 200, 180, 
                    
                    //6-10
                    180, 100, 130, 100, 100, 

                    //11-15
                    100, 100, 100, 200, 200
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 11 }, 11);
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PnCode, A.PnName, A.ActInd, A.Address, B.CityName, C.ProvName, A.PostalCd,  ");
            SQL.AppendLine("A.IdentityNo, D.OptDesc, A.BirthDt, A.Manpower, A.Asset, A.TurnOver, E.BCName, F.BSName ");
            SQL.AppendLine("From (Select * From TblPartner Where ActInd = 'Y') A ");
            SQL.AppendLine("Left Join TblCity B On A.CityCode=B.CityCode ");
            SQL.AppendLine("Left Join TblProvince C On A.ProvCode=C.ProvCode ");
            SQL.AppendLine("Left Join TblOption D On A.Gender=D.OptCode And D.OptCat='Gender' ");
            SQL.AppendLine("Left Join TblBusinessCategory E On A.BCCode=E.BCCode ");
            SQL.AppendLine("Left Join TblBusinessSector F On A.BSCode=F.BSCode ");
            SQL.AppendLine("Where 0 = 0 ");
            if (TxtPnName.Text.Length > 0)
            {
                SQL.AppendLine("And (A.PnCode Like @PnCode Or A.PnName Like @PnCode) ");
            }
            //SQL.AppendLine("Where ( ");
            //SQL.AppendLine("    Not Exists ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select 1 From TblSurveyDtl T1 ");
            //SQL.AppendLine("        Inner Join TblSurvey T2 On T1.DocNo = T2.DocNo And T2.Status <> 'C' And T2.CancelInd = 'N' ");
            //SQL.AppendLine("            And T1.PaymentInd = 'O' ");
            //SQL.AppendLine("        Inner Join TblRequestLP T3 On T2.RQLPDocNo = T3.DocNo And T3.PnCode = A.PnCode ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("    Or ");
            //SQL.AppendLine("    Not Exists ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select 1 From TblSurveyDtl T1 ");
            //SQL.AppendLine("        Inner Join TblSurvey T2 On T1.DocNo = T2.DocNo And T2.Status <> 'C' And T2.CancelInd = 'N' ");
            //SQL.AppendLine("            And T1.PaymentInd = 'O' ");
            //SQL.AppendLine("        Inner Join TblRequestLP T3 On T2.RQLPDocNo = T3.DocNo ");
            //SQL.AppendLine("        Inner Join TblPartner T4 On T3.PnCode = T4.PnCode And (T4.IDNumber = A.IDNumber Or T4.Address = A.Address) ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine(") ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();                
                var l = new List<Partner>();
                var l2 = new List<OutstandingPartnerLoan>();

                PrepData(ref l);
                if (l.Count > 0)
                {
                    PrepOutstandingLoan(ref l2);
                    if(l2.Count > 0)
                        ComparePartnerData(ref l, ref l2);
                    ShowDataHdr(ref l);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    Sm.ClearGrd(Grd1, false);
                    return;
                }

                l.Clear();
                l2.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            mFrmParent.TxtPnCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
            mFrmParent.ShowPartner(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));

            this.Close();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void ShowDataHdr(ref List<Partner> l)
        {
            Sm.ClearGrd(Grd1, false);
            int No = 0;
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].VisibleInd == "Y")
                {
                    Grd1.Rows.Add();
                    Grd1.Cells[No, 0].Value = No + 1;
                    Grd1.Cells[No, 1].Value = l[i].PnCode;
                    Grd1.Cells[No, 2].Value = l[i].PnName;
                    Grd1.Cells[No, 3].Value = (string.Compare(l[i].ActInd, "Y") == 0);
                    Grd1.Cells[No, 4].Value = l[i].Address;
                    Grd1.Cells[No, 5].Value = l[i].CityName;
                    Grd1.Cells[No, 6].Value = l[i].ProvName;
                    Grd1.Cells[No, 7].Value = l[i].PostalCd;
                    Grd1.Cells[No, 8].Value = l[i].IdentityNo;
                    Grd1.Cells[No, 9].Value = l[i].OptDesc;
                    if (l[i].BirthDt.Length <= 0) Grd1.Cells[No, 10].Value = string.Empty;
                    else Grd1.Cells[No, 10].Value = Sm.ConvertDate(l[i].BirthDt);
                    //Grd1.Cells[No, 10].Value = (l[i].BirthDt.Length > 0) ? Sm.ConvertDate(l[i].BirthDt).ToString() : string.Empty;
                    Grd1.Cells[No, 11].Value = l[i].ManPower;
                    Grd1.Cells[No, 12].Value = l[i].Asset;
                    Grd1.Cells[No, 13].Value = l[i].TurnOver;
                    Grd1.Cells[No, 14].Value = l[i].BCName;
                    Grd1.Cells[No, 15].Value = l[i].BSName;
                    No += 1;
                }
            }
        }

        private void ComparePartnerData(ref List<Partner> l, ref List<OutstandingPartnerLoan> l2)
        {
            for (int i = 0; i < l.Count; i++)
            {
                for (int j = 0; j < l2.Count; j++)
                {
                    if (l[i].PnCode == l2[j].PnCode)
                    {
                        l[i].VisibleInd = "N";
                        break;
                    }

                    if (l[i].IdentityNo == l2[j].IdentityNo)
                    {
                        l[i].VisibleInd = "N";
                        break;
                    }

                    if (l[i].Address == l2[j].Address)
                    {
                        l[i].VisibleInd = "N";
                        break;
                    }
                }
            }
        }

        private void PrepOutstandingLoan(ref List<OutstandingPartnerLoan> l2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct C.PnCode, D.IdentityNo, D.Address ");
            SQL.AppendLine("From (Select * From TblSurvey Where CancelInd = 'N' And Status <> 'C') A ");
            SQL.AppendLine("Inner Join TblLoanSummary B On A.DocNo = B.SurveyDocNo And B.PaymentInd = 'O' ");
            SQL.AppendLine("Inner Join TblRequestLP C On A.RQLPDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblPartner D On C.PnCode = D.PnCode ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select Distinct B.PnCode, B.IdentityNo, B.Address ");
            SQL.AppendLine("From (Select * From TblRequestLP Where CancelInd = 'N' And Status <> 'C' And ProcessInd = 'O') A ");
            SQL.AppendLine("Inner Join TblPartner B On A.PnCode = B.PnCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "PnCode", 
                        
                    //1-2
                    "IdentityNo", "Address"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new OutstandingPartnerLoan()
                        {
                            PnCode = Sm.DrStr(dr, c[0]),
                            IdentityNo = Sm.DrStr(dr, c[1]),
                            Address = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepData(ref List<Partner> l)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = GetSQL()
                };
                Sm.CmParam<String>(ref cm, "@PnCode", string.Concat("%", TxtPnName.Text, "%"));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "PnCode", 
                        
                    //1-5
                    "PnName", "ActInd", "Address", "CityName", "ProvName", 

                    //6-10
                    "PostalCd",  "IdentityNo", "OptDesc", "BirthDt", "Manpower", 

                    //11-14
                    "Asset", "TurnOver", "BCName", "BSName"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Partner() 
                        {
                            PnCode = Sm.DrStr(dr, c[0]),
                            PnName = Sm.DrStr(dr, c[1]),
                            ActInd = Sm.DrStr(dr, c[2]),
                            Address = Sm.DrStr(dr, c[3]),
                            CityName = Sm.DrStr(dr, c[4]),
                            ProvName = Sm.DrStr(dr, c[5]),
                            PostalCd = Sm.DrStr(dr, c[6]),
                            IdentityNo = Sm.DrStr(dr, c[7]),
                            OptDesc = Sm.DrStr(dr, c[8]),
                            BirthDt = Sm.DrStr(dr, c[9]),
                            ManPower = Sm.DrDec(dr, c[10]),
                            Asset = Sm.DrDec(dr, c[11]),
                            TurnOver = Sm.DrDec(dr, c[12]),
                            BCName = Sm.DrStr(dr, c[13]),
                            BSName = Sm.DrStr(dr, c[14]),
                            VisibleInd = "Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Events

        #region misc control events

        private void TxtPnName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPnName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Partner");
        }

        #endregion

        #endregion

        #region Class

        private class Partner
        {
            public string PnCode { get; set; }
            public string PnName { get; set; }
            public string ActInd { get; set; }
            public string Address { get; set; }
            public string CityName { get; set; }
            public string ProvName { get; set; }
            public string PostalCd { get; set; }
            public string IdentityNo { get; set; }
            public string OptDesc { get; set; }
            public string BirthDt { get; set; }
            public decimal ManPower { get; set; }
            public decimal Asset { get; set; }
            public decimal TurnOver { get; set; }
            public string BCName { get; set; }
            public string BSName { get; set; }
            public string VisibleInd { get; set; }
        }

        private class OutstandingPartnerLoan
        {
            public string PnCode { get; set; }
            public string IdentityNo { get; set; }
            public string Address { get; set; }
        }

        #endregion

    }
}
