﻿#region Update
/*
    06/12/2020 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt10Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt10 mFrmParent;
        private string mWhsCode = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOCt10Dlg3(FrmDOCt10 FrmParent, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter1, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.ItCode, T2.ItCodeInternal, T2.ItName, T2.ForeignName, ");
            SQL.AppendLine("T7.PropCode, T8.PropName, T7.BatchNo, T7.Source, T7.Lot, T7.Bin, ");
            SQL.AppendLine("If(T1.Qty < T7.Qty, T1.Qty, T7.Qty) - IfNull(T9.DOQty, 0.00) Qty, T2.InventoryUomCode, ");
            SQL.AppendLine("If(T1.Qty < T7.Qty, T1.Qty, T7.Qty) - IfNull(T9.DOQty, 0.00) Qty2, T2.InventoryUomCode2, ");
            SQL.AppendLine("If(T1.Qty < T7.Qty, T1.Qty, T7.Qty) - IfNull(T9.DOQty, 0.00) Qty3, T2.InventoryUomCode3, ");
            SQL.AppendLine("T1.SOContractDocNo, T1.SOContractDNo, T1.No ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select D.ItCode, C.SOContractDocNo, B.SOContractDNo, D.No, D.Qty ");
            SQL.AppendLine("    From TblRecvCtDtl A ");
            SQL.AppendLine("    Inner Join TblDOCtDtl B On A.DOCtDocNo = B.DocNo And A.DOCtDNo = B.DNo ");
            SQL.AppendLine("        And A.DocNo = @RecvCtDocNo ");
            SQL.AppendLine("    Inner Join TblDOCtHdr C On B.DocNo = C.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl D ON C.SOContractDocNo = D.DocNo And B.SOContractDNo = D.DNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select C.ItCode, B.SOContractDocNo, B.SOContractDNo, C.No, C.Qty ");
            SQL.AppendLine("    From TblRecvCtDtl A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl B On A.DOCtDocNo = B.DocNo And A.DOCtDNo = B.DNo ");
            SQL.AppendLine("        And A.DocNo = @RecvCtDocNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl C On B.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblItem T2 On T1.ItCode = T2.ItCode And T2.ServiceItemInd = 'N' " + Filter2);
            SQL.AppendLine("Inner Join TblSOContractHdr T3 ON T1.SOContractDocNo = T3.DOcNo ");
            SQL.AppendLine("Inner Join TblBOQHdr T4 On T3.BOQDocNo = T4.DOcNo ");
            SQL.AppendLine("Inner Join TblLOPHdr T5 ON T4.LOPDocNo = T5.DocNo ");
            SQL.AppendLine("Inner Join TblProjectGroup T6 ON T5.PGCode = T6.PGCode ");
            SQL.AppendLine("Inner Join TblStockSummary T7 On T1.ItCode = T7.ItCode And T7.BatchNo = T6.ProjectCode ");
            SQL.AppendLine("    And T7.WhsCode = @WhsCode ");
            SQL.AppendLine("    And T7.Qty > 0 ");
            SQL.AppendLine("Left Join TblProperty T8 On T7.PropCode = T8.PropCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.SOContractDocNo, B.SOContractDNo, Sum(B.Qty) DOQty ");
            SQL.AppendLine("    From TblDOCt3Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt3Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And A.CancelInd = 'N' ");
            SQL.AppendLine("        And A.RecvCtDocNo = @RecvCtDocNo ");
            SQL.AppendLine("        And B.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        And B.SOContractDNo Is Not Null ");
            SQL.AppendLine("    Group By B.SOContractDocNo, B.SOContractDNo ");
            SQL.AppendLine(") T9 On T1.SOContractDocNo = T9.SOContractDocNo And T1.SOContractDNo = T9.SOContractDNo ");
            SQL.AppendLine("Where Not Find_In_Set(Concat(T1.SOContractDocNo, T1.SOContractDNo), @SelectedSOContract) ");
            SQL.AppendLine("Order By T1.SOContractDocNo, Right(Concat('0000000000', IfNull(T1.No, '0')), 10); ");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Local Code", 
                        "Item's Name", 

                        //6-10
                        "Foreign Name",
                        "Property",
                        "Property",
                        "Batch#",
                        "Source",
                        
                        //11-15
                        "Lot",
                        "Bin", 
                        "Stock",
                        "UoM",
                        "Stock",

                        //16-20
                        "UoM",
                        "Stock",
                        "UoM",
                        "SOContract#",
                        "SOContract D#",

                        //21
                        "SO Contract's"+Environment.NewLine+"No"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 80, 20, 80, 250, 

                        //6-10
                        180, 0, 0, 200, 180, 

                        //11-15
                        60, 60, 100, 80, 100, 

                        //16-20
                        80, 100, 80, 180, 0,

                        //21
                        100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 7, 8, 10, 11, 12 }, false);
            Grd1.Cols[21].Move(2);
            Grd1.Cols[19].Move(2);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter1 = " ", Filter2 = string.Empty, Filter3 = string.Empty ;

                var cm = new MySqlCommand();

                if (mFrmParent.Grd2.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd2.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(mFrmParent.Grd2, r, 1).Length != 0)
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += "(T2.ItCode=@ItCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), Sm.GetGrdStr(mFrmParent.Grd2, r, 1));
                        }
                    }
                }

                if (Filter2.Length != 0)
                    Filter2 = " And ( " + Filter2 + ") ";
                else
                    Filter2 = " And 1=0 ";

                Sm.GenerateSQLFilterForInventory(ref cm, ref Filter3, "T7", ref mFrmParent.Grd1, 11, 12, 13);
                Filter2 = Filter2 + ((Filter3.Length > 0) ? " And (" + Filter3 + ") " : " ");

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@RecvCtDocNo", mFrmParent.TxtRecvCtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@SelectedSOContract", mFrmParent.SelectedSOContract());
                Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "T2.ItCode", "T2.ItCodeInternal", "T2.ItName" });
                Sm.FilterStr(ref Filter1, ref cm, TxtBatchNo.Text, "T7.BatchNo", false);
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter1, Filter2),
                    new string[] 
                    { 
                        //0
                        "ItCode",

                        //1-5
                        "ItCodeInternal", "ItName", "ForeignName", "PropCode", "PropName", 
                        
                        //6-10
                        "BatchNo", "Source", "Lot", "Bin", "Qty", 
                        
                        //11-15
                        "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3",

                        //16-18
                        "SOContractDocNo", "SOContractDNo", "No"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 21);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 15, 18, 21 });                        

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
                    }
                }
            }
            if (!IsChoose)
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            else
                mFrmParent.RecomputeSummary();
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string Key =
                Sm.GetGrdStr(Grd1, Row, 10) +
                Sm.GetGrdStr(Grd1, Row, 11) +
                Sm.GetGrdStr(Grd1, Row, 12) +
                Sm.GetGrdStr(Grd1, Row, 19) +
                Sm.GetGrdStr(Grd1, Row, 20);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Key,
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 11) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 12) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 13) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 25) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 26)
                    ))
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        #endregion

        #endregion
    }
}
