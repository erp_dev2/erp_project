﻿#region Update
/*
    03/05/2017 [TKG] menampilkan data dengan qty > 0 untuk filter back date
    10/07/2018 [WED] tambah filter multi warehouse
    15/05/2019 [WED] tambah fitur tampilkan 0 stock, berdasarkan parameter IsStockSummaryShowZeroStock
    22/05/2019 [WED] IsStockSummaryShowZeroStock juga menampilkan selain 0
    06/08/2019 [TKG] bug proses multi warehouse
    23/03/2020 [TKG/IMS] berdasarkan parameter IsInvTrnShowItSpec, menampilkan specifikasi item
    28/12/2020 [VIN/SRN] mengganti label kolom, qty --> qty1, uom --> uom1
    11/03/2021 [TKG/IMS] menambah heat number
*/
#endregion

#region Namespace


using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;
using System.Text.RegularExpressions;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private string[] mCols = null;
        private bool
            mIsStockSummaryShowOldDataNoFilter = false,
            mIsItGrpCode = false,
            mIsShowForeignName = false,
            mIsInventoryRptFilterByGrpWhs = false,
            mIsFilterByItCt = false,
            mIsStockSummaryShowZeroStock = false,
            mIsInvTrnShowItSpec = false,
            mIsStockSummaryHeatNumberEnabled = false;

        #endregion

        #region Constructor

        public FrmRptStockSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (!mIsStockSummaryShowZeroStock)
                {
                    LblZeroStock.Visible = false;
                    ChkZeroStock.Visible = false;
                }
                SetGrd();
                SetCols();
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCatCode, mIsFilterByItCt ? "Y" : "N");
                if (mIsInventoryRptFilterByGrpWhs)
                    Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                else
                    Sl.SetLueWhsCode(ref LueWhsCode);
                SetCcbWhsName(ref CcbWhsName, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsStockSummaryShowOldDataNoFilter = Sm.GetParameterBoo("IsStockSummaryShowOldDataNoFilter");
            mIsItGrpCode = Sm.GetParameter("IsItGrpCodeShow") == "N";
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsInventoryRptFilterByGrpWhs = Sm.GetParameterBoo("IsInventoryRptFilterByGrpWhs");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsStockSummaryShowZeroStock = Sm.GetParameterBoo("IsStockSummaryShowZeroStock");
            mIsInvTrnShowItSpec = Sm.GetParameterBoo("IsInvTrnShowItSpec");
            mIsStockSummaryHeatNumberEnabled = Sm.GetParameterBoo("IsStockSummaryHeatNumberEnabled");
        }

        private string SetSQL(string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Summary */ ");
            SQL.AppendLine("Select B.WhsName, A.Lot, A.Bin, A.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, ");
            SQL.AppendLine("0 As AvailableStock, A.Qty, C.InventoryUomCode ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine(", A.Qty2, C.InventoryUomCode2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine(", A.Qty3, C.InventoryUomCode3 ");
            SQL.AppendLine(",C.ItGrpCOde, D.ItGrpName, C.ActInd, E.ItScName, C.Specification, B.WhsCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.WhsCode, T1.Lot, T1.Bin, T1.ItCode, ");
            SQL.AppendLine("    Sum(T1.Qty) As Qty ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , Sum(T1.Qty2) As Qty2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , Sum(T1.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockSummary T1 ");
            SQL.AppendLine("    Where 0 = 0 ");
            if (!ChkZeroStock.Checked) SQL.AppendLine("    And (T1.Qty<>0) ");
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=T1.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("     " + Filter.Replace("X.", "T1."));
            SQL.AppendLine("    Group By T1.WhsCode, T1.Lot, T1.Bin, T1.ItCode ");         
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            //if (mIsStockSummaryShowZeroStock && ChkZeroStock.Checked) SQL.AppendLine("And A.Qty = 0 ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode " + Filter2.Replace("X.", "C."));
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblItemGroup D On C.ItGrpCode = D.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubcategory E On C.ItScCode = E.ItScCode ");
            SQL.AppendLine("Order By B.WhsName, C.ItName; ");

            return SQL.ToString();
        }

        private string SetSQL2(string DocDt, string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Summary 2 */ ");
            SQL.AppendLine("Select B.WhsName, A.Lot, A.Bin, A.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, ");
            SQL.AppendLine("0 As AvailableStock, A.Qty, C.InventoryUomCode ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine(", A.Qty2, C.InventoryUomCode2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine(", A.Qty3, C.InventoryUomCode3 ");
            SQL.AppendLine(", C.ItGrpCode, D.ItGrpName, C.ActInd, E.ItScName, C.Specification, B.WhsCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.WhsCode, T1.Lot, T1.Bin, T1.ItCode, ");
            SQL.AppendLine("    Sum(T1.Qty) As Qty  ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , Sum(T1.Qty2) As Qty2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , Sum(T1.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement T1 ");
            SQL.AppendLine("    Inner Join TblItem T2 On T1.ItCode=T2.ItCode " + Filter2.Replace("X.", "T2."));
            SQL.AppendLine("    Where T1.DocDt<=" + DocDt);
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=T1.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (!ChkZeroStock.Checked) SQL.AppendLine("    And (T1.Qty<>0) ");
            SQL.AppendLine("     " + Filter.Replace("X.", "T1."));
            SQL.AppendLine("Group By T1.WhsCode, T1.Lot, T1.Bin, T1.ItCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            
            if (mIsStockSummaryShowZeroStock && ChkZeroStock.Checked) 
                SQL.AppendLine("And (A.Qty = 0 Or A.Qty > 0) ");
            else 
                SQL.AppendLine("And A.Qty>0 ");
            
            SQL.AppendLine("Inner Join TblItem C On A.ItCode= C.ItCode " + Filter2.Replace("X.", "C."));
            
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("Left Join TblItemGroup D On C.ItGrpCode = D.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubcategory E On C.ItScCode = E.ItScCode ");
            SQL.AppendLine("Order By B.WhsName, C.ItName; ");
            
            return SQL.ToString();
        }

        private string SetSQL3(string Filter, string Filter2, string Filter3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Summary 3 */ ");

            SQL.AppendLine("Select T1.WhsCode, T6.WhsName, T1.Lot, T1.Bin, T1.ItCode, T5.ItCodeInternal, T5.ItName, T5.ForeignName, ");
            SQL.AppendLine("IfNull(T2.Qty, 0)-IfNull(T3.Qty2, 0)+IfNull(T4.Qty3, 0) As AvailableStock, ");
            SQL.AppendLine("IfNull(T2.Qty, 0) As Qty, T5.InventoryUomCode  ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , T2.Qty2, T5.InventoryUomCode2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , T2.Qty3, T5.InventoryUomCode3 ");
            SQL.AppendLine(", T5.ItGrpCOde, T7.ItGrpName, T5.ActInd, T8.ItScName, T5.Specification, T6.WhsCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct WhsCode, Lot, Bin, ItCode From ( ");
	        SQL.AppendLine("    Select Distinct A.WhsCode, A.Lot, A.Bin, A.ItCode  ");
	        SQL.AppendLine("    From TblStockSummary A ");
	        SQL.AppendLine("    Inner Join TblItem B On A.ItCode=B.ItCode " + Filter.Replace("X.", "B."));
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Where (A.Qty>0 ");
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    ) " + Filter2.Replace("X.", "A.") + Filter3.Replace("X.", "A."));
	        SQL.AppendLine("    Union All ");
	        SQL.AppendLine("    Select Distinct A.WhsCode, B.Lot, B.Bin, B.ItCode ");
	        SQL.AppendLine("    From TblDOWhsHdr A ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' " + Filter3.Replace("X.", "B."));
            SQL.AppendLine("    Inner Join TblItem C On B.ItCode=C.ItCode " + Filter.Replace("X.", "C."));
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Where 0=0 " + Filter2.Replace("X.", "A."));
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select WhsCode, Lot, Bin, ItCode From (");
	        SQL.AppendLine("        Select Distinct A.WhsCode2 As WhsCode, B.Lot, B.Bin, B.ItCode ");
	        SQL.AppendLine("        From TblDOWhsHdr A ");
            SQL.AppendLine("        Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' " + Filter3.Replace("X.", "B."));
            SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode " + Filter.Replace("X.", "C."));
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    ) T Where 0=0 " + Filter2.Replace("X.", "T."));
            SQL.AppendLine(") X ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.ItCode, Sum(A.Qty) As Qty ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , Sum(A.Qty2) As Qty2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockSummary A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode=B.ItCode " + Filter.Replace("X.", "B."));
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Where (A.Qty>0 ");
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    ) " + Filter2.Replace("X.", "A.") + Filter3.Replace("X.", "A."));
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.ItCode ");
            SQL.AppendLine(") T2 On T1.WhsCode=T2.WhsCode And T1.Lot=T2.Lot And T1.Bin=T2.Bin And T1.ItCode=T2.ItCode ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select A.WhsCode, B.Lot, B.Bin, B.ItCode, Sum(B.Qty) As Qty2 ");
	        SQL.AppendLine("    From TblDOWhsHdr A ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' " + Filter3.Replace("X.", "B."));
            SQL.AppendLine("    Inner Join TblItem C On B.ItCode=C.ItCode " + Filter.Replace("X.", "C."));
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Where 0=0 " + Filter2.Replace("X.", "A."));
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Group By A.WhsCode, B.Lot, B.Bin, B.ItCode ");
            SQL.AppendLine(") T3 On T1.WhsCode=T3.WhsCode And T1.Lot=T3.Lot And T1.Bin=T3.Bin And T1.ItCode=T3.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select WhsCode, Lot, Bin, ItCode, Qty3 From (");
	        SQL.AppendLine("        Select A.WhsCode2 As WhsCode, B.Lot, B.Bin, B.ItCode, Sum(B.Qty) As Qty3 ");
	        SQL.AppendLine("        From TblDOWhsHdr A ");
            SQL.AppendLine("        Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' " + Filter3.Replace("X.", "B."));
            SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode " + Filter.Replace("X.", "C."));
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("        Group By A.WhsCode, B.Lot, B.Bin, B.ItCode ");
            SQL.AppendLine("    ) T Where 0=0 " + Filter2.Replace("X.", "T."));
            SQL.AppendLine(") T4 On T1.WhsCode=T4.WhsCode And T1.Lot=T4.Lot And T1.Bin=T4.Bin And T1.ItCode=T4.ItCode ");
            SQL.AppendLine("Inner Join TblItem T5 On T1.ItCode=T5.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T5.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblWarehouse T6 On T1.WhsCode=T6.WhsCode ");
            SQL.AppendLine("Left Join TblItemGroup T7 On T5.ItGrpCode = T7.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubcategory T8 On T5.ItScCode = T8.ItScCode ");
            SQL.AppendLine("Where (IfNull(T2.Qty, 0)-IfNull(T3.Qty2, 0)+IfNull(T4.Qty3, 0)>0 ");
            SQL.AppendLine(" Or IfNull(T2.Qty, 0)>0 ");
            SQL.AppendLine(") Order By T6.WhsName, T5.ItName;");

            return SQL.ToString();
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode"); 
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }
        
        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Warehouse", 
                        "Lot",
                        "Bin",   
                        "Item's Code", 
                        "Local Code",
                        
                        //6-10
                        "Item's Name",
                        "Foreign Name",
                        "Available" + Environment.NewLine + "Stock", 
                        "Quantity 1", 
                        "UoM 1",
                        
                        //11-15
                        "Quantity 2", 
                        "UoM 2",
                        "Quantity 3", 
                        "UoM 3",
                        "Item Group"+Environment.NewLine+"Code",

                        //16-20
                        "Item Group"+Environment.NewLine+"Name",
                        "Active",
                        "Sub-Category",
                        "Specification",
                        "Heat Number",

                        //21
                        "Warehouse Code"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 60, 60, 100, 100,  
                        
                        //6-10
                        250, 230, 100, 100, 70,
                        
                        //11-15
                        100, 70, 100, 70, 100,

                        //16-20
                        150, 70, 150, 200, 200,

                        //21
                        0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 17 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 11, 13}, 0);
            if (mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 8, 11, 12, 13, 14, 18, 19 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 8, 11, 12, 13, 14, 18, 19 }, false);
            if (mIsItGrpCode) Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, false);
            Grd1.Cols[15].Move(8);
            Grd1.Cols[16].Move(9);
            Grd1.Cols[17].Move(10);
            Grd1.Cols[18].Move(11);
            if (mIsInvTrnShowItSpec)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, mIsInvTrnShowItSpec);
                Grd1.Cols[19].Move(8);
            }
            if (mIsStockSummaryHeatNumberEnabled)
                Grd1.Cols[20].Move(9);
            else
                Grd1.Cols[20].Visible = false;
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 18 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14 }, true);
        }

        private void SetCols()
        {
            if (mNumberOfInventoryUomCode == 1)
                mCols = new string[] { 
                    //0
                    "WhsName", 

                    //1-5
                    "Lot", "Bin", "ItCode", "ItCodeInternal", "ItName",

                    //6-10
                    "ForeignName", "AvailableStock", "Qty", "InventoryUomCode", "ItGrpCode",
                    
                    //11-15
                    "ItGrpName", "ActInd", "ItScName", "Specification", "WhsCode"
                };

            if (mNumberOfInventoryUomCode == 2)
                mCols = new string[] { 
                    //0
                    "WhsName", 
                    
                    //1-5
                    "Lot", "Bin", "ItCode", "ItCodeInternal", "ItName",
                    
                    //6-10
                    "ForeignName", "AvailableStock", "Qty", "InventoryUomCode", "Qty2",
                    
                    //11-15
                    "InventoryUOMCode2", "ItGrpCode", "ItGrpName", "ActInd",  "ItScName",

                    //16-17
                    "Specification", "WhsCode"
                };

            if (mNumberOfInventoryUomCode == 3)
                mCols = new string[] { 
                    //0
                    "WhsName", 
                    
                    //1-5
                    "Lot", "Bin", "ItCode", "ItCodeInternal", "ItName",
                    
                    //6-10
                    "ForeignName", "AvailableStock", "Qty", "InventoryUomCode", "Qty2",
                    
                    //11-15
                    "InventoryUOMCode2", "Qty3", "InventoryUOMCode3", "ItGrpCode", "ItGrpName",
                    
                    //16-19
                    "ActInd", "ItScName", "Specification", "WhsCode"
                };
        }

        override protected void ShowData()
        {
            try
            {
                if(ChkWhsName.Checked && ChkWhsCode.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "Please just use Multi Warehouse or Warehouse filter, instead of using both of them.");
                    CcbWhsName.Focus();
                    return;
                }

                if (ChkZeroStock.Checked && ChkAvailableStock.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "Please just use Available Stock or Zero Stock, instead of using both of them.");
                    ChkAvailableStock.Focus();
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;
                string SQL = string.Empty, Filter = " And 1=1 ", Filter2 = " And 1=1 ";
                string DocDt = (ChkDocDt.Checked) ? Sm.GetDte(DteDocDt).Substring(0, 8) : "";
                
                var cm = new MySqlCommand();

                if (ChkAvailableStock.Checked)
                {
                    if (ChkDocDt.Checked && decimal.Parse(DocDt) < decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid filter (Date).");
                        return;
                    }
                    string Filter3 = " ";

                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCatCode), "X.ItCtCode", true);
                    Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, "X.ItCode", true);
                    Sm.FilterStr(ref Filter, ref cm, TxtItName.Text, "X.ItName", false);
                    if(ChkWhsName.Checked)
                        FilterStr(ref Filter2, ref cm, ProcessCcb(Sm.GetCcb(CcbWhsName)), "X.WhsCode", "_2");
                    else
                        Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueWhsCode), "X.WhsCode", true);
                    Sm.FilterStr(ref Filter3, ref cm, TxtLot.Text, "X.Lot", false);
                    Sm.FilterStr(ref Filter3, ref cm, TxtBin.Text, "X.Bin", false);

                    SQL = SetSQL3(Filter, Filter2, Filter3);
                }
                else
                {
                    if (!mIsStockSummaryShowOldDataNoFilter)
                    {
                        if (!(
                            !(ChkDocDt.Checked && decimal.Parse(DocDt) <
                            decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8))) ||
                            (
                            (ChkDocDt.Checked && decimal.Parse(DocDt) <
                            decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8))) &&
                            ChkItCode.Checked &&
                            !ChkWhsCode.Checked &&
                            !ChkItCatCode.Checked &&
                            !ChkItName.Checked &&
                            !ChkLot.Checked &&
                            !ChkBin.Checked
                            )))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Invalid filter (Date).");
                            return;
                        }
                    }

                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    if (ChkWhsName.Checked)
                        FilterStr(ref Filter, ref cm, ProcessCcb(Sm.GetCcb(CcbWhsName)), "X.WhsCode", "_2");
                    else
                        Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "X.WhsCode", true);
                    Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "X.Lot", false);
                    Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "X.Bin", false);
                    Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueItCatCode), "X.ItCtCode", true);
                    Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, "X.ItCode", true);
                    Sm.FilterStr(ref Filter2, ref cm, TxtItName.Text, "X.ItName", false);
               
                    SQL =
                        ((ChkDocDt.Checked && decimal.Parse(DocDt) < 
                        decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8))) ?
                        SetSQL2(DocDt, Filter, Filter2) :
                        SetSQL(Filter, Filter2));
                }

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL, mCols,
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdVal("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdVal("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 10, 9);
                        if (mNumberOfInventoryUomCode == 1)
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 17, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                        }
                        if (mNumberOfInventoryUomCode == 2)
                        {
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                        }
                        if (mNumberOfInventoryUomCode == 3)
                        {
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                        }
                        Grd.Cells[Row, 20].Value = null;
                    }, true, false, false, false
                );
                if (mIsStockSummaryHeatNumberEnabled)
                    SetHeatNumber();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private void SetHeatNumber()
        {
            var l = new List<StockHeatNumber>();

            SetHeatNumber(ref l);

            if (l.Count > 0)
            {
                foreach(var x in l.OrderBy(o=>o.WhsCode).OrderBy(o=>o.ItCode))
                {
                    for (int i = 0; i < Grd1.Rows.Count; i++)
                    {

                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, i, 21), x.WhsCode) &&
                            Sm.CompareStr(Sm.GetGrdStr(Grd1, i, 2), x.Lot) &&
                            Sm.CompareStr(Sm.GetGrdStr(Grd1, i, 3), x.Bin) &&
                            Sm.CompareStr(Sm.GetGrdStr(Grd1, i, 4), x.ItCode))
                        {
                            Grd1.Cells[i, 20].Value = x.HeatNumber;
                            break;
                        }
                    }
                }
                l.Clear();
            }
        }

        private void SetHeatNumber(ref List<StockHeatNumber> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, WhsCode = string.Empty, Lot = string.Empty, Bin = string.Empty, ItCode = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                WhsCode = Sm.GetGrdStr(Grd1, i, 21);
                if (WhsCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += 
                        "(A.WhsCode=@WhsCode0" + i.ToString() + 
                        " And A.Lot=@Lot0" + i.ToString() + 
                        " And A.Bin=@Bin0" + i.ToString() + 
                        " And A.ItCode=@ItCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@WhsCode0" + i.ToString(), WhsCode);
                    Sm.CmParam<String>(ref cm, "@Lot0" + i.ToString(), Sm.GetGrdStr(Grd1, i, 2));
                    Sm.CmParam<String>(ref cm, "@Bin0" + i.ToString(), Sm.GetGrdStr(Grd1, i, 3));
                    Sm.CmParam<String>(ref cm, "@ItCode0" + i.ToString(), Sm.GetGrdStr(Grd1, i, 4));
                }
            }

            if (Filter.Length > 0)
                Filter = " Where A.Qty>0.00 And (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select A.WhsCode, A.Lot, A.Bin, A.ItCode, Group_Concat(Distinct B.Value2 Order By B.Value2 Separator ', ') As HeatNumber ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblSourceInfo B On A.Source=B.Source And B.Value2 Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Group By A.WhsCode, A.Lot, A.Bin, A.ItCode;");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    "WhsCode", 
                    "Lot", "Bin", "ItCode", "HeatNumber" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new StockHeatNumber()
                        {
                            WhsCode = Sm.DrStr(dr, c[0]),
                            Lot = Sm.DrStr(dr, c[1]),
                            Bin = Sm.DrStr(dr, c[2]),
                            ItCode = Sm.DrStr(dr, c[3]),
                            HeatNumber = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetCcbWhsName(ref CheckedComboBoxEdit Ccb, string WhsName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.WhsName As Col From TblWarehouse T ");
            SQL.AppendLine("Where 0 = 0 ");
            if (WhsName.Length > 0)
            {
                SQL.AppendLine("And T.WhsName = @WhsName ");
            }
            else
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=T.WhsCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T.WhsName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsName", WhsName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetCcb(ref Ccb, cm);
        }

        private string ProcessCcb(string Value)
        {
            if (Value.Length != 0)
            {
                Value = GetWhsCode(Value);
                Value = "#" + Value.Replace(", ", "# #") + "#";
                Value = Value.Replace("#", @"""");
            }
            return Value;
        }

        private string GetWhsCode(string Value)
        {
            if(Value.Length != 0)
            {
                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(T.WhsCode Separator ', ') WhsCode ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select WhsCode ");
                SQL.AppendLine("    From TblWarehouse ");
                SQL.AppendLine("    Where Find_In_Set(WhsName, @Param) ");
                SQL.AppendLine(")T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
            }

            return Value;
        }

        private void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, string Param)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 + Param + Index.ToString();
                        //Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), "%" + s + "%");
                        Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), s);
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            int c = e.ColIndex;
            if (Sm.IsGrdColSelected(new int[] { 8, 9, 11, 13 }, c))
            {
                decimal Total = 0m;
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, c).Length != 0) Total += Sm.GetGrdDec(Grd1, r, c);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void ChkItCatCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item's code");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item's name");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueItCatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCatCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit2(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit2(this, sender, "Date");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void ChkAvailableStock_CheckedChanged(object sender, EventArgs e)
        {
            Grd1.Cols[8].Visible = ChkAvailableStock.Checked;
        }

        private void CcbWhsName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkWhsName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Warehouse");
        }

        private class StockHeatNumber
        {
            public string WhsCode { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string ItCode { get; set; }
            public string HeatNumber { get; set; }
        }

        #endregion
    }
}
