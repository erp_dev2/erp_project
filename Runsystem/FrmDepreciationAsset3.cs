﻿#region Update
/*
    20/01/2022 [ISD/PRODUCT] tambah field Residual Value
    20/01/2022 [ISD/PRODUCT] Validasi untuk depreciation date dan add information pop-up based on parameter DepreciationDateRuleFormat
    21/01/2022 [ISD/PRODUCT] penyesuaian header menu dan add information pop-up based on parameter EconomicLifeYearRuleFormat
    21/01/2022 [ISD/PRODUCT] Validasi untuk Economic Life Asset maks 30 tahun dengan parameter MaxEconomicLifeYear 
    25/01/2022 [ICA/PRODUCT] Tidak membentuk journal ketika cancel data
    27/01/2022 [ICA/PRODUCT] Penyesuaian tampilan grid (menambah beberapa kolom) dan menghubungkan dengan recondition asset
    03/02/2022 [DITA/PRODUCT] saat choose data setelah find muncul warning
    03/02/2022 [ICA/PRODUCT] save AccDeprValue dan NBV ke database 
    07/02/2022 [ICA/PRODUCT] Lue Depreciation Code tidak menampilkan depreciation method non depreciable
    08/02/2022 [WED/PRODUCT] analis salah rumus
                            NBV = Asset Depreciable Value - Acc. Depr. Value => NBV = Total Asset Value - Acc. Depr. Value
                            Depreciation Value (garis lurus) = Asset Value (header) / Eco Life Month (header) => Depreciation Value (garis lurus) = Asset Depreciable Value / Economic Life Month (header)
    16/02/2022 [ICA/PRODUCT] Menyertakan AccDeprValue sebelum recondisi (case bila sudah recondisi)
    24/02/2022 [WED/PRODUCT] perbaikan rumus NBV untuk baris kedua langsung ganti tahun
    02/03/2022 [WED/PRODUCT] perbaikan rumus jika ada rekondisi
    09/03/2022 [WED/PRODUCT] perbaikan set ecolifemonth di grid. kalau ada rekoondisi pun, jumlah eco life month di grid menyesuaikan di header nya, bukan dari dokumen RoA
    02/06/2022 [DITA/PHT] tambah parameter IsFicoUseMultiProfitCenterFilter
    30/06/2022 [RDA/PRODUCT] perubahan di beberapa field ketika menarik item dari Initial Master Asset (pembeda menggunakan kolom AssetSource)
    04/07/2022 [SET/PRODUCT] menyesuaikan InsertDataNotValid(), IsGrdEmpty(), ShowAssetInfo() ketika asset yang dipilih Non-Depreciable
    06/07/2022 [IBL/PRODUCT] economic life yr & economic life mth ambil dari remaining economic life yr & mth jika yg dipilih adalah Initial Asset
    13/07/2022 [IBL/PRODUCT] Kolom NBV row terahir nilainya dijadikan 0 atau 1 berdasarkan parameter DepreciationSalvageValue
                             DBA atas master asset : asset value on acquisition date -> asset value - total recondition asset; recondition value -> total recondition; recondition doc -> assetcode & ngeloop masster asset
                             DBA atas initial master asset : asset value on acquisition date -> (asset value - acc depre opening balance) - total recondition; recondition value --> total recondition; recondition doc -> assetcode & ngeloop masster asset
                             Jika yg dipilih adalah Initial master, maka eco life yg diupdate adalah yg remaining ecolife
    19/07/2022 [IBL/PRODUCT] DBA atas initial master asset:
                             - Asset Value header ambil dari Asset Value initial asset;
                             - kolom asset value on acquisition date -> asset value - total recondition asset.
                             - perubahan rumus :
                               kolom Acc. Depr. Value SEBELUM ada Recondition, row pertama ditambah dengan Acc. Depr. Opening Balance dr Initial Asset
    20/07/2022 [IBL/PRODUCT] DBA atas initial master asset:
                             kolom Acc. Depr. Value SETELAH ada Recondition, row pertama ditambah dengan Asset's Acc. Depr. on Current Date dr Recondition Asset
                             Jika terdapat Recondition maka kolom Depreciation Value (garis lurus) = Asset's NBV on Recondition Date / Economic Life Month (header)
    21/07/2022 [IBL/PRODUCT] Kolom Depreciation Value baris terahir disamakan dengan kolom NBV lastRow - 1 agar Acc.Depr Value row terahir akan sama dengan Asset Value
    25/07/2022 [IBL/PRODUCT] Initial Master Asset : Depreciation Value (garis lurus) = (AssetValue - AccDeprOpeningBalance - ResidualValue) / Economic Life Month (header)
    26/07/2022 [IBL/PRODUCT] Bug: Saldo menurun, depreciation value row terahir belum sama dg NBV lastRow - 1.
    04/08/2022 [IBL/PRODUCT] Feedback DBA yg tercancel akibat adanya rekondisi seharusnya tidak tertambah dengan nilai recondition value
                             Nilai NBV pada baris terakhir = Residual Value
    18/08/2022 [MYA/PRODUCT] Membuat validasi Item category berdasarkan apa yang telah terpasang di Group (system tools)
    30/08/2022 [IBL/PRODUCT] Bug: Saat show data kolom recondition value masih belum sesuai. (Blm di sum semua recondition value)
    07/09/2022 [IBL/PRODUCT] Penyesuaian depreciation value dr master asset after recondition (garis lurus). Depre Val = AssetNBV On Recondition Dt / EcoLifeMth
    13/09/2022 [IBL/PRODUCT] Penyesuaian depreciation value dr master asset after recondition (garis lurus). Depre Val = AssetNBV On Recondition Dt - ResidualValue / EcoLifeMth
    13/09/2022 [IBL/PRODUCT] EcoLife Month di header tidak boleh decimal.
    29/09/2022 [IBL/PRODUCT] EcoLife Month di header masih blm sesuai setelah dilakukan rekondisi.
    14/04/2023 [SET/PHT] add parameter isDBAHideActive
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmDepreciationAsset3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmDepreciationAsset3Find FrmFind;
        bool mIsAutoJournalActived = false,
             mIsJournalValidationDepreciationAssetEnabled = false;
        internal bool
            mIsDBACCFilteredByGroup = false,
            mIsNeedCalculate = false,
            mIsFicoUseMultiProfitCenterFilter = false,
            mIsFilterByItCt = false,
            IsInsert = false,
            mIsDBAHideActive = false;
        internal string mDepreciationTypeGarisLurus = string.Empty,
                        mDepreciationDateRuleFormat = string.Empty,
                        mEconomicLifeYearRuleFormat = string.Empty,
                        mMaxEconomicLifeYear = string.Empty,
                        mDepreciationMethodForNonDepreciable = string.Empty;
        internal string mDepreciationTypeSaldoMenurun = string.Empty;
        internal string LastYr = string.Empty;
        internal string mDepreciationSalvageValue = string.Empty;
        internal string mAssetSource = string.Empty;
        #endregion

        #region Constructor

        public FrmDepreciationAsset3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Depreciation Asset";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetLueDepreciationCode(ref LueDepreciationCode);
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "DNo",
                        
                    //1-5 
                    "Active",
                    "Old Active",
                    "Month"+Environment.NewLine+"(Purchase)",
                    "Month",
                    "Year",

                    //6-10
                    "Asset's Value On "+Environment.NewLine+"Acquisition Date",
                    "Recondition Value",
                    "Recondition Document#",
                    "",
                    "Total Asset Value", 

                    //11-15
                    "Residual Value",
                    "Asset Depreciable"+Environment.NewLine+"Value",
                    "Depreciation"+Environment.NewLine+"Value",
                    "Acc. Depr. Value",
                    "NBV",

                    //16-20
                    "Journal#",
                    "Site",
                    "Profit Center",
                    "Costcenter Code",
                    "Cost Center",

                    //21-25
                    "Balance",
                    "Annual Depreciation Value",
                    "Asset NBV on Recondition Date",
                    "Asset Code",
                    "",
                },
                new int[]
                {
                    //0
                    20,
                        
                    //1-5
                    60, 40, 100, 80, 80, 

                    //6-10
                    150, 150, 200, 20, 150,

                    //11-15
                    150, 150, 150, 150, 150,

                    //16-20
                    150, 200, 200, 200, 200,

                    //21-25
                    150, 150, 0, 150, 20,
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 10, 11, 12, 13, 14, 15, 21, 22, 23 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 9, 25 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 8, 9, 19, 23 }, false);
            Grd1.Cols[24].Move(8);
            Grd1.Cols[25].Move(9);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtAssetCode, TxtAssetName, TxtDisplayName, TxtEcoLifeYr, TxtEcoLife, TxtItCode, 
                        TxtItName, DteDepreciationDt, TxtAssetValue, LueDepreciationCode, TxtPercentageAnnualDepreciation,
                        TxtCC, MeeRemark, TxtResidualValue, TxtStatus
                    }, true);
                    BtnAssetCode.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
                    ChkCancelInd.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, TxtEcoLifeYr, LueDepreciationCode, TxtResidualValue, TxtStatus
                    }, false);
                    BtnAssetCode.Enabled = true;
                    break;
                case mState.Edit:
                    ChkCancelInd.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtAssetCode, TxtAssetName, TxtDisplayName, 
                TxtItCode, TxtItName, DteDepreciationDt, LueDepreciationCode, TxtCC, 
                MeeRemark, TxtResidualValue, TxtStatus
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtEcoLifeYr, TxtEcoLife, TxtAssetValue, TxtPercentageAnnualDepreciation, TxtResidualValue }, 0);

            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        public void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDepreciationAsset3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                IsInsert = true;
                ClearData();
                SetFormControl(mState.Insert);
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            IsInsert = false;
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            IsInsert = false;
            mIsNeedCalculate = false;
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f1 = new FrmReconditionAsset(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f1.ShowDialog();
            }

            if (e.ColIndex == 25 && Sm.GetGrdStr(Grd1, e.RowIndex, 24).Length != 0)
            {
                var f1 = new FrmAsset2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 24);
                f1.ShowDialog();
            }
        }
        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f1 = new FrmReconditionAsset(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f1.ShowDialog();
            }

            if (e.ColIndex == 25 && Sm.GetGrdStr(Grd1, e.RowIndex, 24).Length != 0)
            {
                var f1 = new FrmAsset2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 24);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DepreciationAsset", "TblDepreciationAssetHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDepAssetHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) 
                    cml.Add(SaveDepAssetDtl(DocNo, Row));
            cml.Add(UpdateAsset());

            //if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtAssetCode, "Asset", false) ||
                Sm.IsDteEmpty(DteDepreciationDt, "Depreciation Date") ||
                Sm.IsTxtEmpty(TxtAssetValue, "Asset value", true) ||
                //Sm.IsTxtEmpty(TxtEcoLifeYr, "Economic Life Year", true) ||
                IsTxtEcoLife(TxtEcoLifeYr, "Economic Life Year", true, Sm.GetLue(LueDepreciationCode)) ||
                IsTxtEcoLife(TxtEcoLife, "Economic Life Month", true, Sm.GetLue(LueDepreciationCode)) ||
                //Sm.IsTxtEmpty(TxtEcoLife, "Economic Life Month", true) ||
                IsEcoLifeInvalid() ||
                Sm.IsLueEmpty(LueDepreciationCode, "Depreciation Code")||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDepreciationDt)) ||
                IsJournalSettingInvalid() ||
                IsGrdEmpty()
                ;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsJournalValidationDepreciationAssetEnabled) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Table
            if (IsJournalSettingInvalid_Asset(Msg)) return true;
            if (IsJournalSettingInvalid_ItemCategory(Msg)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_Asset(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Asset = string.Empty;

            SQL.AppendLine("Select Concat(AssetCode, ' - ', AssetName) ");
            SQL.AppendLine("From TblAsset ");
	        SQL.AppendLine("Where AcNo Is Null ");
	        SQL.AppendLine("And ActiveInd = 'Y' ");
	        SQL.AppendLine("And AssetCode = @AssetCode; ");

            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);

            cm.CommandText = SQL.ToString();
            Asset = Sm.GetValue(cm);
            if (Asset.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Asset's COA account# (" + Asset + ") is empty.");
                return true;
            }

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ItCtName = string.Empty;

            SQL.AppendLine("Select C.ItCtName ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode And A.ActiveInd = 'Y' ");
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode = C.ItCtCode And C.AcNo Is Null ");
            SQL.AppendLine("    And A.AssetCode = @AssetCode; ");

            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }

            return false;
        }
        
        private bool IsTxtEcoLife(TextEdit Txt, string Message, bool IsNumeric, string DepreciationCode)
        {
            bool Result = true;
            if (Txt.EditValue != null && Txt.Text.Length != 0)
            {
                if (IsNumeric)
                {
                    if (DepreciationCode != "3")
                    {
                        if (decimal.Parse(Txt.Text) != 0)
                            Result = false;
                        else
                        {
                            Sm.StdMsg(mMsgType.Warning, Message + " is zero.");
                            Txt.Focus();
                        }
                    }
                    else
                        Result = false;

                }
                else
                    Result = false;
            }
            else
            {
                if (Message.Length == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                    Sm.StdMsg(mMsgType.Warning, Message + " is empty.");
                Txt.Focus();
            }
            return Result;
        }

        private bool IsEcoLifeInvalid()
        {
            decimal EcoLifeAcquisitionDt = 0m;
            decimal EcolifeMth = Decimal.Parse(TxtEcoLife.Text);
            string ReconditionDocNo = Sm.GetGrdStr(Grd1, 0, 8);

            if (ReconditionDocNo.Length > 0)
            {
                EcoLifeAcquisitionDt = Sm.GetValueDec("Select EcoLife1 From TblReconditionAssetHdr " +
                    "Where DocNo = @Param And CancelInd = 'N' And Status = 'A';", ReconditionDocNo);

                if (EcolifeMth > EcoLifeAcquisitionDt)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Economic Life Month cannot more than Economic Life on Acquisition Date." + Environment.NewLine +
                        "Recondition Document# : " + ReconditionDocNo + Environment.NewLine +
                        "Economic Life (Months) on Acquisition Date : " + Sm.FormatNum(EcoLifeAcquisitionDt, 0));
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Sm.GetLue(LueDepreciationCode) != "3")
            {
                if (Grd1.Rows.Count == 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "No depreciation value.");
                    return true;
                }
                return false;
            }
            else
                return false;
        }

        private MySqlCommand SaveDepAssetHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDepreciationAssetHdr(DocNo, DocDt, CancelInd, AssetCode, AssetValue, ResidualValue, DepreciationDt, EcoLifeYr, EcoLife, DepreciationCode, PercentageAnnualDepreciation, Status, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @CancelInd, @AssetCode, @AssetValue, @ResidualValue, @DepreciationDt, @EcoLifeYr, @EcoLife, @DepreciationCode, @PercentageAnnualDepreciation, @Status, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N"); 
            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@AssetValue", Decimal.Parse(TxtAssetValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@ResidualValue", Decimal.Parse(TxtResidualValue.Text));
            Sm.CmParamDt(ref cm, "@DepreciationDt", Sm.GetDte(DteDepreciationDt));
            Sm.CmParam<Decimal>(ref cm, "@EcoLifeYr", Decimal.Parse(TxtEcoLifeYr.Text)); 
            Sm.CmParam<Decimal>(ref cm, "@EcoLife", Decimal.Parse(TxtEcoLife.Text));
            Sm.CmParam<String>(ref cm, "@DepreciationCode", Sm.GetLue(LueDepreciationCode)); 
            Sm.CmParam<Decimal>(ref cm, "@PercentageAnnualDepreciation", Decimal.Parse(TxtPercentageAnnualDepreciation.Text));
            Sm.CmParam<String>(ref cm, "@Status", TxtStatus.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateAsset()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblAsset Set ResidualValue=@ResidualValue," +
                    "EcoLifeYr = IF(AssetCode = '1', @EcoLifeYr, EcoLifeYr), EcoLife = IF(AssetCode = '1', @EcoLife, EcoLife),  " +
                    "RemEcoLifeYr = IF(AssetCode = '2', @EcoLifeYr, RemEcoLifeYr), RemEcoLifeMth = IF(AssetCode = '2', @EcoLife, RemEcoLifeMth),  " +
                    "DepreciationCode=@DepreciationCode, PercentageAnnualDepreciation=@PercentageAnnualDepreciation, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where AssetCode=@AssetCode;"
            };
            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@ResidualValue", Decimal.Parse(TxtResidualValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@EcoLifeYr", Decimal.Parse(TxtEcoLifeYr.Text));
            Sm.CmParam<Decimal>(ref cm, "@EcoLife", Decimal.Parse(TxtEcoLife.Text));
            Sm.CmParam<String>(ref cm, "@DepreciationCode", Sm.GetLue(LueDepreciationCode));
            Sm.CmParam<Decimal>(ref cm, "@PercentageAnnualDepreciation", Decimal.Parse(TxtPercentageAnnualDepreciation.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDepAssetDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDepreciationAssetDtl(DocNo, DNo, ActiveInd, MthPurchase, Mth, Yr, DepreciationValue, CCCode, CostValue, AnnualCostValue, ReconditionDocNo, AccDeprValue, NBV, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ActiveInd, @MthPurchase, @Mth, @Yr, @DepreciationValue, @CCCode, @CostValue, @AnnualCostValue, @ReconditionDocNo, @AccDeprValue, @NBV, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ActiveInd", "Y");
            Sm.CmParam<String>(ref cm, "@MthPurchase", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@ReconditionDocNo", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@DepreciationValue", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@AccDeprValue", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@NBV", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetGrdStr(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@CostValue", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@AnnualCostValue", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblDepreciationAssetHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, IfNull(A.DepreciationDt, B.AssetDt) As JournalDt, ");
            SQL.AppendLine("Concat('Depreciation of Business Assets : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.AssetCode = B.AssetCode; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt, EntCode From (");
            SQL.AppendLine("        Select B.AcNo, 'D' As ACtype, B.AssetValue As DAmt, 0 As CAmt, D.EntCode ");
            SQL.AppendLine("        From TblDepreciationAssetHdr A ");
            SQL.AppendLine("        Inner Join TblAsset B ");
            SQL.AppendLine("        On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("        And B.AcNo Is Not Null ");
            SQL.AppendLine("        And B.ActiveInd='Y' ");
            SQL.AppendLine("        Left Join TblCostCenter C On B.CCCode = C.CCCode ");
            SQL.AppendLine("        Left Join TblProfitCenter D On C.ProfitCenterCode = D.ProfitCenterCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.AcNo, 'C' As ACtype, 0 As DAmt, B.AssetValue As CAmt, F.EntCode ");
            SQL.AppendLine("        From TblDepreciationAssetHdr A ");
            SQL.AppendLine("        Inner Join TblAsset B On A.AssetCode=B.AssetCode And B.ActiveInd='Y' ");
            SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Left Join TblCostCenter E On B.CCCode = E.CCCode ");
            SQL.AppendLine("        Left Join TblProfitCenter F On E.ProfitCenterCode = F.ProfitCenterCode  ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo, Actype ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDepreciationDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            
            var cml = new List<MySqlCommand>();
            cml.Add(EditDepAssetHdr());
            //if (mIsAutoJournalActived && IsJournalDataExisted()) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);
            
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            var Query = new StringBuilder();

            Query.AppendLine("Select IfNull(A.DepreciationDt, B.AssetDt) As DocDt ");
            Query.AppendLine("From TblDepreciationAssetHdr A ");
            Query.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            Query.AppendLine("Where A.DocNo=@Param ");
            Query.AppendLine("And A.AssetCode = B.AssetCode; ");

            var DocDt = Sm.GetValue(Query.ToString(), TxtDocNo.Text);

            return
                Sm.IsTxtEmpty(TxtDocNo, "Depreciation", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, DocDt) ||
                IsJournalSettingInvalid() ||
                IsCancelIndEditedAlready();
        }

        private bool IsCancelIndEditedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblDepreciationAssetHdr Where DocNo=@DocNo And CancelInd='Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsJournalDataExisted()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblDepreciationAssetHdr " +
                    "Where JournalDocNo Is Not Null And DocNo=@DocNo Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            return Sm.IsDataExist(cm);
        }

        private MySqlCommand EditDepAssetHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblDepreciationAssetHdr Set CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var Query = new StringBuilder();

            Query.AppendLine("Select IfNull(A.DepreciationDt, B.AssetDt) As DocDt ");
            Query.AppendLine("From TblDepreciationAssetHdr A ");
            Query.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            Query.AppendLine("Where A.DocNo=@Param ");
            Query.AppendLine("And A.AssetCode = B.AssetCode; ");

            var DocDt = Sm.GetValue(Query.ToString(), TxtDocNo.Text);
            var CurrentDt = Sm.ServerCurrentDate();
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblDepreciationAssetHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, ");
            SQL.AppendLine("JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblDepreciationAssetHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblDepreciationAssetHdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                mIsNeedCalculate = false;
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDepAssetHdr(DocNo);
                ShowDepAssetDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDepAssetHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.AssetCode, B.Assetname, B.DisplayName, B.ItCode, C.Itname, ");
            SQL.AppendLine("A.DepreciationDt, IfNull(A.AssetValue, B.AssetValue)As AssetValue, IFnull(A.EcolifeYr, B.EcoLifeYr) As EcoLifeYr, ");
            SQL.AppendLine("ifnull(A.Ecolife, B.Ecolife) As EcoLife, ifnull(A.DepreciationCode, B.DepreciationCode) DepreciationCode, ");
            SQL.AppendLine("ifnull(A.PercentageAnnualDepreciation, B.PercentageAnnualDepreciation) PercentageAnnualDepreciation, E.CCname, A.remark, ifnull(A.ResidualValue, B.ResidualValue) As ResidualValue, A.Status "); 
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode = B.AssetCode ");
            SQL.AppendLine("left Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join TblOption D On B.DepreciationCode = D.OptCode And D.OptCat = 'DepreciationMethod'  ");
            SQL.AppendLine("Left Join TblCostCenter E On B.CCCode = E.CCCode ");
            SQL.AppendLine("Where DocNo=@DocNo;"); 

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt",  "CancelInd", "AssetCode", "AssetName", "DisplayName",   
                        //6-10
                        "ItCode", "ItName", "DepreciationDt", "AssetValue", "EcoLifeYr",    
                        //11-15
                        "EcoLife", "DepreciationCode", "PercentageAnnualdepreciation", "CCname", "Remark", 
                        //16-17
                        "ResidualValue", "Status" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtAssetCode.EditValue =  Sm.DrStr(dr, c[3]);
                        TxtAssetName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtDisplayName.EditValue = Sm.DrStr(dr, c[5]);
                        TxtItCode.EditValue =  Sm.DrStr(dr, c[6]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[7]); 
                        Sm.SetDte(DteDepreciationDt, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueDepreciationCode, Sm.DrStr(dr, c[12]));
                        TxtAssetValue.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[9]), 0);
                        TxtEcoLifeYr.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                        TxtEcoLife.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[11]), 0);
                        TxtPercentageAnnualDepreciation.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[13]), 0);
                        TxtCC.EditValue =  Sm.DrStr(dr, c[14]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                        TxtResidualValue.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[16]), 0);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[17]);
                    }, true
                );
        }

        private void ShowDepAssetDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.ActiveInd, A.MthPurchase, A.Mth, A.Yr, IfNull(C.AssetValue, 0.00) - IfNull(I.ReconditionValue, 00) As AssetValue, B.ResidualValue, ");
            SQL.AppendLine("A.depreciationvalue, A.CCCode, D.CCName, E.SiteName, G.ProfitCenterName, A.CostValue, ");
            SQL.AppendLine("Round(A.AnnualCostValue, 2) As AnnualCostValue, A.JournalDocNo, A.ReconditionDocNo, ");
            SQL.AppendLine("IfNull(B.AssetValue, 0.00) - (IfNull(C.AssetValue, 0.00) - IfNull(I.ReconditionValue, 0.00)) ReconditionValue, B.AssetCode ");
            SQL.AppendLine("From TblDepreciationAssetDtl A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblAsset C ON B.AssetCode = C.AssetCode ");
            SQL.AppendLine("Left Join TblCostCenter D On A.CCCode = D.CCCode ");
            SQL.AppendLine("Left Join TblSite E ON C.SiteCode = E.SiteCode ");
            SQL.AppendLine("Left Join TblCostCenter F ON C.CCCode = F.CCCode ");
            SQL.AppendLine("Left Join TblProfitCenter G ON F.ProfitCenterCode = G.ProfitCenterCode ");
            SQL.AppendLine("Left Join tblreconditionassethdr H ON A.ReconditionDocNo = H.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select AssetCode, Sum(ReconditionValue) As ReconditionValue ");
            SQL.AppendLine("    From TblReconditionAssetHdr ");
            SQL.AppendLine("    Where AssetCode = @AssetCode ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine(")I On B.AssetCode = I.AssetCode ");
            //SQL.AppendLine("	AND CONCAT(A.Yr, A.Mth, '01') BETWEEN Concat(LEFT(H.ReconditionDt, 6), '01') AND Date_Add(Concat(LEFT(H.ReconditionDt, 6), '31'), INTERVAL H.EcoLifeAfterRecondition MONTH ) ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ActiveInd", "MthPurchase", "Mth", "Yr", "AssetValue", 
                    
                    //6-10
                    "ReconditionValue", "ReconditionDocNo", "ResidualValue", "depreciationvalue", "JournalDocNo", 

                    //11-15
                    "SiteName", "ProfitCenterName", "CCCode", "CCName", "CostValue", 

                    //16-17
                    "AnnualCostValue", "AssetCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Grd1.Cells[Row, 10].Value = Sm.GetGrdDec(Grd1, Row, 6) + Sm.GetGrdDec(Grd1, Row, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Grd1.Cells[Row, 12].Value = Sm.GetGrdDec(Grd1, Row, 10) - Sm.GetGrdDec(Grd1, Row, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    if (Sm.GetGrdDec(Grd1, Row, 3) % 12 == 1)
                        Grd1.Rows[Row].BackColor = Color.Aqua;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            //ClearGrd();
            //SetMthGrd();
            //SetCostValue();
            //for(int Row = 0; Row < Grd1.Rows.Count; ++Row)
            //    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
            SetAccDeprValueAndNBV();

            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowAssetInfo(string AssetCode)
        {
            mIsNeedCalculate = false;
            string mDepreciationCode = string.Empty;
            decimal mInitialAssetValue = 0m;
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.AssetCode, A.AssetName, A.DisplayName, A.ItCode, B.Itname, "+
                    "A.AssetDt, A.AssetValue, A.EcoLifeYr, A.Ecolife, A.DepreciationCode, A.PercentageAnnualdepreciation, D.CCname, A.ResidualValue," +
                    "A.AssetSource, A.AccDepr, A.OpeningBalanceDt, RemEcoLifeYr, RemEcoLifeMth " +
                    "from TblAsset A "+
                    "left Join TblItem B On A.ItCode = B.ItCode "+
                    "Left Join TblOption C On A.DepreciationCOde = C.OptCode And C.OptCat = 'DepreciationMethod' "+
                    "Left Join TblCostCenter D On A.CCCode = D.CCCode "+
                    "Where A.AssetCode = '"+AssetCode+"' ",
                    new string[] 
                    { 
                        //0
                        "AssetCode", 
                        //1-5
                        "AssetName", "DisplayName", "ItCode", "ItName", "AssetDt",
                        //6-10
                        "AssetValue", "EcoLifeYr", "EcoLife", "DepreciationCode", "PercentageAnnualdepreciation", 
                        //11-15
                        "CCName", "ResidualValue", "AssetSource", "AccDepr", "OpeningBalanceDt",
                        //16-17
                        "RemEcoLifeYr", "RemEcoLifeMth"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        decimal test = Sm.DrDec(dr, c[8]);
                        mDepreciationCode = Sm.DrStr(dr, c[9]);
                        mAssetSource = Sm.DrStr(dr, c[13]);
                        mInitialAssetValue = decimal.Parse(Sm.DrStr(dr, c[6])) - decimal.Parse(Sm.DrStr(dr, c[14]));

                        TxtAssetCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtAssetName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtDisplayName.EditValue = Sm.DrStr(dr, c[2]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetDte(DteDepreciationDt, mAssetSource == "2" ? Sm.DrStr(dr, c[15]) : Sm.DrStr(dr, c[5]) );
                        TxtAssetValue.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[6]), 0);
                        if (Sm.DrStr(dr, c[9]) == "3")
                            SetLueDepreciationCode(ref LueDepreciationCode, Sm.DrStr(dr, c[9]));
                        else
                            Sm.SetLue(LueDepreciationCode, Sm.DrStr(dr, c[9]));
                        TxtEcoLifeYr.EditValue = mAssetSource == "2" ? Sm.FormatNum(Sm.DrDec(dr, c[16]), 0) : Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        TxtEcoLife.EditValue = mAssetSource == "2" ? Sm.FormatNum(Sm.DrDec(dr, c[17]), 0) : Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtPercentageAnnualDepreciation.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                        TxtCC.EditValue = Sm.DrStr(dr, c[11]);
                        TxtResidualValue.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[12]), 0);
                    }, true
                );

            mIsNeedCalculate = true;
            if (mAssetSource == "2")
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtResidualValue, TxtEcoLifeYr, LueDepreciationCode, DteDepreciationDt}, true);
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtResidualValue, TxtEcoLifeYr, LueDepreciationCode, DteDepreciationDt}, false);
            if (mDepreciationCode == "3")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtResidualValue, TxtEcoLifeYr, LueDepreciationCode, DteDepreciationDt }, true);
                //Sm.SetControlNumValueZero(new List<DXE.TextEdit>
                //{ TxtEcoLifeYr, TxtEcoLife }, 0);
            }
        }


        #endregion

        #endregion

        #region Additional Method

        public void SetLueDepreciationCode(ref LookUpEdit Lue)
        {
            string Filter = string.Empty;
            Filter = " And OptCode Not In ('" + mDepreciationMethodForNonDepreciable + "') ";

            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption " +
                "Where OptCat = 'DepreciationMethod' " + Filter + " Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        
        public void SetLueDepreciationCode(ref LookUpEdit Lue, string DepreciationCode)
        {
            //Sm.SetLue2(
            //    ref Lue,
            //    "Select OptCode As Col1, OptDesc As Col2 From TblOption " +
            //    "Where OptCat = 'DepreciationMethod' AND OptCode = '" + DepreciationCode + "' " +
            //    "Order By OptDesc ",
            //    0, 35, false, true, "Code", "Name", "Col2", "Col1");

            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode Col1, OptDesc Col2 ");
                SQL.AppendLine("From TblOption ");
                if (DepreciationCode.Length > 0)
                    SQL.AppendLine("Where OptCat = 'DepreciationMethod' AND OptCode = @Code ");
                SQL.AppendLine("ORDER BY OptDesc; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (DepreciationCode.Length > 0)
                    Sm.CmParam<String>(ref cm, "@Code", DepreciationCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (DepreciationCode.Length > 0) Sm.SetLue(Lue, DepreciationCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mDepreciationTypeGarisLurus = Sm.GetParameter("DepreciationTypeGarisLurus");
            mDepreciationTypeSaldoMenurun = Sm.GetParameter("DepreciationTypeSaldoMenurun");
            mDepreciationSalvageValue = Sm.GetParameter("DepreciationSalvageValue");
            mIsJournalValidationDepreciationAssetEnabled = Sm.GetParameterBoo("IsJournalValidationDepreciationAssetEnabled");
            mIsDBACCFilteredByGroup = Sm.GetParameterBoo("IsDBACCFilteredByGroup");
            mDepreciationDateRuleFormat = Sm.GetParameter("DepreciationDateRuleFormat");
            mEconomicLifeYearRuleFormat = Sm.GetParameter("EconomicLifeYearRuleFormat");
            mMaxEconomicLifeYear = Sm.GetParameter("MaxEconomicLifeYear");
            mDepreciationMethodForNonDepreciable = Sm.GetParameter("DepreciationMethodForNonDepreciable");
            mIsFicoUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiProfitCenterFilter");
            if (mMaxEconomicLifeYear.Length == 0) mMaxEconomicLifeYear = "30";
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsDBAHideActive = Sm.GetParameterBoo("IsDBAHideActive");
        }

        public void SetMthGrd()
        {
            ClearGrd();
            try
            {
                decimal 
                    StartMth = 0m,
                    Year = 0m,
                    EndMth = 0m,  
                    ReconditionEndMth = 0m;
                string CCName = TxtCC.Text,
                    CCCode = string.Empty,
                    DepMethod = string.Empty,
                    ReconditionDocNo = string.Empty,
                    ReconditionStartMth = string.Empty, 
                    ReconditionYr = string.Empty;

                DepMethod = Sm.GetValue("Select DepreciationCode From TblAsset Where AssetCode ='" + TxtAssetCode.Text + "'");

                var SQL = new StringBuilder();

                SQL.AppendLine("/*DepreciationAsset3 - SetMthGrd*/ ");
                SQL.AppendLine("Select A.CCCode, A.DepreciationCode, B.DocNo ReconditionDocNo, SUBSTRING(B.ReconditionDt, 5, 2) ReconditionStartMth, SUBSTRING(B.ReconditionDt, 1, 4) ReconditionYr, ");
                SQL.AppendLine("B.EcoLife3 ReconditionEndMth ");
                SQL.AppendLine("From TblAsset A ");
                SQL.AppendLine("Left Join TblReconditionAssetHdr B On A.AssetCode = B.AssetCode ");
                SQL.AppendLine("   And B.CancelInd = 'N' ");
                SQL.AppendLine("Where A.AssetCode = @AssetCode ");
                SQL.AppendLine("Order By B.CreateDt Desc Limit 1; ");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);

                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    {
                        "CCCode", 
                        "DepreciationCode", "ReconditionDocNo", "ReconditionStartMth", "ReconditionYr", "ReconditionEndMth"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        CCCode = Sm.DrStr(dr, c[0]);
                        DepMethod = Sm.DrStr(dr, c[1]);
                        ReconditionDocNo = Sm.DrStr(dr, c[2]);
                        ReconditionStartMth = Sm.DrStr(dr, c[3]);
                        ReconditionYr = Sm.DrStr(dr, c[4]);
                        ReconditionEndMth = Sm.DrDec(dr, c[5]);
                    }, true
                );

                if(ReconditionDocNo.Length > 0)
                {
                    if (ReconditionStartMth.Length > 0)
                    {
                        StartMth = decimal.Parse(ReconditionStartMth) - 1;
                        Year = decimal.Parse(ReconditionYr);
                    }

                    //if (mDepreciationTypeGarisLurus == "1" && DepMethod == "1")
                    //{
                    //    EndMth = ReconditionEndMth;
                    //}
                    //else if (mDepreciationTypeSaldoMenurun == "1" && DepMethod == "2")
                    //{
                    //    EndMth = ReconditionEndMth;
                    //}
                    //else
                    //{
                    //    EndMth = 12 - StartMth;
                    //    if ((ReconditionEndMth/12) >= 1)
                    //        EndMth += ((ReconditionEndMth/12 - 1) * 12);
                    //}
                }
                else
                {
                    if (Sm.GetDte(DteDepreciationDt).Length > 0)
                    {
                        StartMth = decimal.Parse(Sm.GetDte(DteDepreciationDt).Substring(4, 2)) - 1;
                        Year = decimal.Parse(Sm.GetDte(DteDepreciationDt).Substring(0, 4));
                    }
                }

                if (mDepreciationTypeGarisLurus == "1" && DepMethod == "1")
                {
                    EndMth = decimal.Parse(TxtEcoLife.Text);
                }
                else if (mDepreciationTypeSaldoMenurun == "1" && DepMethod == "2")
                {
                    EndMth = decimal.Parse(TxtEcoLife.Text);
                }
                else
                {
                    EndMth = 12 - StartMth;
                    if (TxtEcoLifeYr.Text.Length > 0)
                        EndMth += ((decimal.Parse(TxtEcoLifeYr.Text) - 1) * 12);
                }

                for (int x = 0; x < EndMth; x++)
                {
                    Grd1.Rows.Add();
                    Grd1.Cells[x, 3].Value = x + 1;
                    if (StartMth == 12)
                    {
                        StartMth = 1;
                        Year = Year + 1;
                    }
                    else
                    {
                        StartMth += 1;
                    }
                    Grd1.Cells[x, 1].Value = true;
                    if (StartMth.ToString().Length > 1)
                    {
                        Grd1.Cells[x, 4].Value = StartMth;
                    }
                    else
                    {
                        Grd1.Cells[x, 4].Value = String.Concat('0', StartMth);
                    }
                    Grd1.Cells[x, 5].Value = Year;
                    Grd1.Cells[x, 19].Value = CCCode;
                    Grd1.Cells[x, 20].Value = CCName;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        public void  SetCostValue()
        {
            decimal
                StartMth = 0m,
                CostValue = 0m,
                Prosen = 0m,
                EcoLife = 0m,
                ResidualMonth = 0m,
                TotalAsset = 0m,
                AssetDepreciableValue = 0m,
                DepreciationValue = 0m,
                AccDeprValue = 0m,
                ResidualValue = 0m,
                NBV = 0m,
                Balance = 0m,
                AnnualDepreciationValue = 0m,
                NBVValue2 = 0m
                ;

            string Year = string.Empty, DepMethod = string.Empty, ReconditionDocNo = string.Empty;

            if (Sm.GetDte(DteDepreciationDt).Length>0) 
            {
                StartMth = decimal.Parse(Sm.GetDte(DteDepreciationDt).Substring(4, 2)) - 1;
                Year = Sm.GetDte(DteDepreciationDt).Substring(0, 4);
            }

            if (TxtAssetValue.Text.Length > 0) 
                CostValue = Decimal.Parse(TxtAssetValue.Text);
            if (TxtPercentageAnnualDepreciation.Text.Length > 0) 
                Prosen = Decimal.Parse(TxtPercentageAnnualDepreciation.Text);

            EcoLife = 12 - StartMth;
            ResidualMonth = 12 - StartMth;
            ResidualValue = decimal.Parse(TxtResidualValue.Text);

            if (TxtEcoLifeYr.Text.Length > 0) 
                EcoLife += ((decimal.Parse(TxtEcoLifeYr.Text) - 1) * 12);

            if (mDepreciationTypeGarisLurus == "1")
            {
                if (TxtEcoLife.Text.Length > 0)
                    EcoLife = decimal.Parse(TxtEcoLife.Text);
                else
                    EcoLife = 0;
            }

            //var cm = new MySqlCommand()
            //{ CommandText = "Select DepreciationCode From TblAsset Where AssetCode =@AssetCode ;"};
            //Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);

            //DepMethod = Sm.GetValue(cm);

            DepMethod = Sm.GetLue(LueDepreciationCode);

            #region depmethod = 2 saldo menurun
            if (DepMethod == "2")
            {
                #region deptype = 0  saldo menurun Runsystem
                if (mDepreciationTypeSaldoMenurun == "0")
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 4) % 12 == 0)
                        {
                            Grd1.Rows[Row].BackColor = Color.Aqua;
                        }

                        if (Row == 0)
                        {
                            Grd1.Cells[Row, 21].Value = Sm.FormatNum(Math.Round(CostValue, 2), 0);
                            Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round((Prosen * CostValue / 100), 2), 0);
                            Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, Row, 22) / ResidualMonth, 2), 0);
                        }
                        else
                        {
                            #region Old

                            if (Row == (EcoLife - 1))
                            {
                                if (mDepreciationSalvageValue == "0")
                                {
                                    Grd1.Cells[Row, 21].Value = Sm.FormatNum(0m, 0);
                                    Grd1.Cells[Row, 22].Value = Sm.FormatNum(0m, 0);
                                    Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 21)) - (Sm.GetGrdDec(Grd1, (Row - 1), 6)), 2), 0);
                                }
                                else
                                {
                                    Grd1.Cells[Row, 21].Value = Sm.FormatNum(1m, 0);
                                    Grd1.Cells[Row, 22].Value = Sm.FormatNum(1m, 0);
                                    Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round(((Sm.GetGrdDec(Grd1, (Row - 1), 21)) - (Sm.GetGrdDec(Grd1, (Row - 1), 6)))-1, 2), 0);
                                }
                            }
                            else
                            {
                                if (Sm.GetGrdStr(Grd1, Row, 5) == Year)
                                {
                                    if (Row == 0)
                                    {
                                        Grd1.Cells[Row, 21].Value = Sm.FormatNum(Math.Round(CostValue, 2), 0);
                                    }
                                    else
                                    {
                                        if (Row == 0)
                                            Grd1.Cells[Row, 21].Value = Sm.FormatNum(0m, 0);
                                        else
                                            Grd1.Cells[Row, 21].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 21)) - (Sm.GetGrdDec(Grd1, (Row - 1), 13)), 2), 0);
                                    }
                                    Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round((Prosen * CostValue / 100), 2), 0);
                                    Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, Row, 22) / CekRow(Sm.GetGrdStr(Grd1, Row, 5)), 2), 0);
                                }
                                else
                                {
                                    Year = Sm.GetGrdStr(Grd1, Row, 5);
                                    if (Row == 0)
                                        CostValue = 0;
                                    else
                                        CostValue = (Sm.GetGrdDec(Grd1, (Row - 1), 21) - Sm.GetGrdDec(Grd1, (Row - 1), 13));

                                    if (Row == 0)
                                        Grd1.Cells[Row, 21].Value = Sm.FormatNum(0m, 0);
                                    else
                                        Grd1.Cells[Row, 21].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 21)) - (Sm.GetGrdDec(Grd1, (Row - 1), 13)), 2), 0);
                                    Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round((Prosen * CostValue / 100), 2), 0);
                                    Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round(Sm.GetGrdDec(Grd1, Row, 22) / CekRow(Sm.GetGrdStr(Grd1, Row, 5)), 2), 0);
                                }
                            }
                            #endregion
                        }
                    }
                }
                #endregion

                #region deptype = 1 saldo menurun Non runsystem
                else
                {
                    int CountData = 0;
                    int lastRow = Grd1.Rows.Count - 2;

                    if (Grd1.Rows.Count > 1)
                    {
                        LastYr = Sm.GetGrdStr(Grd1, lastRow, 5);
                        if (Sm.GetGrdStr(Grd1, 0, 8).Length > 0) Year = Sm.GetGrdStr(Grd1, 0, 5);
                    }

                    string CurrYr = Year;
                    bool IsFirst = true;
                    

                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        AssetDepreciableValue = Sm.GetGrdDec(Grd1, Row, 12);
                        TotalAsset = Sm.GetGrdDec(Grd1, Row, 10);
                        ReconditionDocNo = Sm.GetGrdStr(Grd1, Row, 8);
                        NBVValue2 = Sm.GetGrdDec(Grd1, Row, 23);

                        #region Old
                        //if (Row == 0)
                        //{
                        //DepreciationValue = (((CountData * (Prosen / 100) * CostValue) / 12) / CountData);
                        //Balance = CostValue - (((CountData * (Prosen / 100) * CostValue) / 12) / CountData);
                        //AnnualDepreciationValue = (((CountData * (Prosen / 100) * CostValue) / 12));
                        //}
                        //else
                        //{
                        //if (Row == Grd1.Rows.Count - 2)
                        //{
                        //    if (mDepreciationSalvageValue == "0")
                        //    {
                        //        DepreciationValue = Sm.GetGrdDec(Grd1, (Row - 1), 13);
                        //        Balance = 0m;
                        //        AnnualDepreciationValue = Sm.GetGrdDec(Grd1, (Row - 1), 22);
                        //    }
                        //    else
                        //    {
                        //        DepreciationValue = Math.Round(Sm.GetGrdDec(Grd1, (Row - 1), 13), 2) - 1;
                        //        Balance = 1m;
                        //        AnnualDepreciationValue = Math.Round(Sm.GetGrdDec(Grd1, (Row - 1), 22), 2) - 1;
                        //    }
                        //}
                        //else
                        //{
                        //    if (Sm.GetGrdStr(Grd1, Row, 5) == Sm.GetGrdStr(Grd1, (Row - 1), 5))
                        //    {
                        //        DepreciationValue = Sm.GetGrdDec(Grd1, (Row - 1), 13);
                        //        Balance = (Sm.GetGrdDec(Grd1, Row - 1, 21) - Sm.GetGrdDec(Grd1, Row - 1, 13));
                        //        AnnualDepreciationValue = Sm.GetGrdDec(Grd1, Row - 1, 22);
                        //    }
                        //    else
                        //    {
                        //        if (LastYr == Sm.GetGrdStr(Grd1, Row, 5) && Row != Grd1.Rows.Count - 2)
                        //        {
                        //            DepreciationValue = ((Sm.GetGrdDec(Grd1, (Row - 1), 21)) / CountData);
                        //            Balance = (Sm.GetGrdDec(Grd1, Row - 1, 21) - ((Sm.GetGrdDec(Grd1, (Row - 1), 21)) / CountData));
                        //            if (Balance <= 0m) Balance = 0m;
                        //            AnnualDepreciationValue = Sm.GetGrdDec(Grd1, (Row - 1), 21);
                        //        }
                        //        else
                        //        {
                        //            DepreciationValue = (Sm.GetGrdDec(Grd1, (Row - 1), 21) * (Prosen / 100) / CountData);
                        //            Balance = Math.Round(Sm.GetGrdDec(Grd1, (Row - 1), 21) - (Sm.GetGrdDec(Grd1, (Row - 1), 21) * (Prosen / 100) / CountData), 2);
                        //            if (Balance <= 0m) Balance = 0m;
                        //            AnnualDepreciationValue = (Sm.GetGrdDec(Grd1, (Row - 1), 21) * (Prosen / 100));
                        //        }
                        //    }
                        //}
                        //}
                        #endregion

                        if (Sm.GetGrdStr(Grd1, Row, 5) == CurrYr && CurrYr == Year) // tahun awal
                        {
                            if (IsFirst)
                            {
                                if (ReconditionDocNo.Length > 0) AnnualDepreciationValue = NBVValue2 * (Prosen / 100);
                                else AnnualDepreciationValue = AssetDepreciableValue * (Prosen / 100);
                                
                                IsFirst = false;
                            }
                            DepreciationValue = AnnualDepreciationValue / 12;
                        }
                        else // tahun setelahnya
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 5) != CurrYr)
                            {
                                CountData = CekRow(Sm.GetGrdStr(Grd1, Row, 5));
                                if (Sm.GetGrdStr(Grd1, Row, 5) != LastYr)
                                    AnnualDepreciationValue = (NBV - ResidualValue) * (Prosen / 100);
                                else
                                    AnnualDepreciationValue = (NBV - ResidualValue);
                            }

                            DepreciationValue = AnnualDepreciationValue / CountData;
                        }

                        Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round(DepreciationValue, 2), 0);
                        Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round(AnnualDepreciationValue, 2), 0);

                        SetAccDeprValueAndNBVRow(Row);
                        AccDeprValue = Sm.GetGrdDec(Grd1, Row, 14);
                        //if (Row != 0) 
                            NBV = Sm.GetGrdDec(Grd1, Row, 15);

                        CurrYr = Sm.GetGrdStr(Grd1, Row, 5);

                        Balance = TotalAsset - AccDeprValue;                        
                        Grd1.Cells[Row, 21].Value = Sm.FormatNum(Math.Round(Balance, 2), 0);

                    }
                }
                #endregion
            }
            #endregion

            #region depmethod = 1 garis lurus
            else
            {
                #region depttype = 1 garis lurus non runsystem 
                if (mDepreciationTypeGarisLurus == "1")
                {
                    int CountData = 0;
                    int lastRow = Grd1.Rows.Count - 2;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        AssetDepreciableValue = Sm.GetGrdDec(Grd1, Row, 12);
                        NBVValue2 = Sm.GetGrdDec(Grd1, Row, 23);
                        Year = Sm.GetGrdStr(Grd1, Row, 5);
                        CountData = CekRow(Year);
                            
                        if (Row == 0)
                        {
                            Grd1.Cells[Row, 21].Value = Sm.FormatNum(Math.Round((CostValue - (CostValue / EcoLife)), 2), 0);
                        }
                        else if (Row == lastRow)
                        {
                            if (mDepreciationSalvageValue == "0")
                                Grd1.Cells[Row, 21].Value = Sm.FormatNum(0m, 0);
                            else
                                Grd1.Cells[Row, 21].Value = Sm.FormatNum(1m, 0);
                        }
                        else
                        {
                            Grd1.Cells[Row, 21].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 21)) - (Sm.GetGrdDec(Grd1, (Row - 1), 13)), 2), 0);
                        } 
                        if (Row == lastRow)
                        {
                            if (mDepreciationSalvageValue == "0")
                            {
                                //Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round((CostValue / EcoLife), 2), 0);
                                //Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round(((CostValue / EcoLife) * CountData), 2), 0);
                                Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round(((AssetDepreciableValue / EcoLife) * CountData), 2), 0);
                            }
                            else
                            {
                                //Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round((CostValue / EcoLife) - 1, 2), 0);
                                //Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round(((CostValue / EcoLife) * CountData) - 1, 2), 0);
                                Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round(((AssetDepreciableValue / EcoLife) * CountData) - 1, 2), 0);
                            }
                        }
                        else
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                            {
                                Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round(((NBVValue2 - ResidualValue) / EcoLife), 2), 0);
                                Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round((((NBVValue2 - ResidualValue) / EcoLife) * CountData), 2), 0);
                            }
                            else
                            {
                                //Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round((CostValue / EcoLife), 2), 0);
                                //Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round(((CostValue / EcoLife) * CountData), 2), 0);
                                Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round((AssetDepreciableValue / EcoLife), 2), 0);
                                Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round(((AssetDepreciableValue / EcoLife) * CountData), 2), 0);
                            }
                        }
                    }
                }
                #endregion 

                #region depttype = 0 garis lurus runsystem
                else
                {
                    int CountData = 0;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 5) == Year)
                        {
                            if (Row == 0)
                            {
                                CountData = CekRow(Sm.GetGrdStr(Grd1, Row, 5));
                                Grd1.Cells[Row, 21].Value = Sm.FormatNum(Math.Round(CostValue, 2), 0);
                            }
                            else
                            {
                                Grd1.Cells[Row, 21].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 21)) - (Sm.GetGrdDec(Grd1, (Row - 1), 13)), 2), 0);
                            }

                            Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round((CostValue - ((CostValue / EcoLife) * CountData)), 2), 0);
                            Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round((CostValue / EcoLife), 2), 0);
                        }
                        else
                        {
                            CountData = CountData + CekRow(Sm.GetGrdStr(Grd1, Row, 5));
                            if (Row == 0)
                            {
                                    Grd1.Cells[Row, 21].Value = Sm.FormatNum(0m, 0);
                            }
                            else
                            {
                                Grd1.Cells[Row, 21].Value = Sm.FormatNum(Math.Round((Sm.GetGrdDec(Grd1, (Row - 1), 21)) - (Sm.GetGrdDec(Grd1, (Row - 1), 13)), 2), 0);
                            }
                            Grd1.Cells[Row, 22].Value = Sm.FormatNum(Math.Round((CostValue - ((CostValue / EcoLife) * CountData)), 2), 0);
                            Grd1.Cells[Row, 13].Value = Sm.FormatNum(Math.Round((CostValue / EcoLife), 2), 0);
                        }
                    }
                }
                #endregion
            }
            #endregion

            if (!(DepMethod == "2" && mDepreciationTypeSaldoMenurun == "1")) SetAccDeprValueAndNBV();
        }

        internal void SetDepAssetDtl(string AssetCode)
        {
            string
                SiteName = string.Empty,
                ProfitCenterName = string.Empty,
                ReconditionDocNo = string.Empty,
                ReconditionStartMth = string.Empty,
                ReconditionEndMth = string.Empty,
                YrMth = string.Empty;
            decimal 
                ResidualValue = 0m,
                AssetValue = 0m,
                ReconditionValue = 0m,
                TotalAssetValue = 0m,
                AssetDepreciableValue = 0m,
                NBVValue2 = 0m,
                AccDeprOB = 0m
                ;

            ResidualValue = Decimal.Parse(TxtResidualValue.Text);

            var SQL = new StringBuilder();

            SQL.AppendLine("/*DepreciationAsset3 - SetDepAssetDtl*/ ");
            SQL.AppendLine("Select B.SiteName, D.ProfitCenterName, ifnull(A.ResidualValue, 0.00) ResidualValue, ");
            SQL.AppendLine("IfNull(A.AssetValue, 0.00) - IfNull(F.ReconditionValue, 0.00) AssetValue, ");
            SQL.AppendLine("E.DocNo ReconditionDocNo, E.ReconditionStartMth, IfNull(F.ReconditionValue, 0.00) ReconditionValue,");
            SQL.AppendLine("IfNull(E.NBVValue2, 0.00) NBVValue2, E.ReconditionEndMth, If(A.AssetSource = '2', IfNull(AccDepr, 0.00), 0.00) As AccDeprOB ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode = B.SiteCode ");
            SQL.AppendLine("Left Join TblCostCenter C On A.CCCode = C.CCCode ");
            SQL.AppendLine("Left Join TblProfitCenter D On C.ProfitCenterCode = D.ProfitCenterCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select DocNo, AssetCode, Left(ReconditionDt, 6) ReconditionStartMth, NBVValue2, ");
            SQL.AppendLine("	Left(Date_Format(Date_Add(ReconditionDt, Interval (EcoLife3-1) Month), '%Y%m%d'), 6) ReconditionEndMth, AssetValue2 ");
            SQL.AppendLine("	From TblReconditionAssetHdr ");
            SQL.AppendLine("	Where AssetCode = @AssetCode And CancelInd = 'N' And Status = 'A' ");
            SQL.AppendLine("	Order By CreateDt Desc Limit 1 ");
            SQL.AppendLine(")E On A.AssetCode = E.AssetCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select AssetCode, Sum(ReconditionValue) As ReconditionValue ");
            SQL.AppendLine("	From TblReconditionAssetHdr ");
            SQL.AppendLine("	Where AssetCode = @AssetCode And CancelInd = 'N' And Status = 'A' ");
            SQL.AppendLine("	Group By AssetCode ");
            SQL.AppendLine(")F On A.AssetCode = F.AssetCode ");
            SQL.AppendLine("Where A.AssetCode = @AssetCode ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    "SiteName",
                    "ProfitCenterName", "ResidualValue", "AssetValue", "ReconditionStartMth", "ReconditionValue",
                    "ReconditionEndMth", "ReconditionDocNo", "NBVValue2", "AccDeprOB"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    SiteName = Sm.DrStr(dr, c[0]);
                    ProfitCenterName = Sm.DrStr(dr, c[1]);
                    AssetValue = Sm.DrDec(dr, c[3]);
                    ReconditionStartMth = Sm.DrStr(dr, c[4]);
                    ReconditionValue = Sm.DrDec(dr, c[5]);
                    ReconditionEndMth = Sm.DrStr(dr, c[6]);
                    ReconditionDocNo = Sm.DrStr(dr, c[7]);
                    NBVValue2 = Sm.DrDec(dr, c[8]);
                    AccDeprOB = Sm.DrDec(dr, c[9]);
                }, true
            );

            for (int row = 0; row < Grd1.Rows.Count - 1; row++)
            {
                YrMth = Sm.GetGrdStr(Grd1, row, 5) + Sm.GetGrdStr(Grd1, row, 4);
                Grd1.Cells[row, 6].Value = AssetValue;
                Grd1.Cells[row, 11].Value = ResidualValue;
                Grd1.Cells[row, 17].Value = SiteName;
                Grd1.Cells[row, 18].Value = ProfitCenterName;
                Grd1.Cells[row, 7].Value = 0m;
                Grd1.Cells[row, 23].Value = 0m;
                if (ReconditionDocNo.Length > 0)
                {
                    if (Decimal.Parse(YrMth) >= Decimal.Parse(ReconditionStartMth) && Decimal.Parse(YrMth) <= Decimal.Parse(ReconditionEndMth))
                    {
                        Grd1.Cells[row, 7].Value = ReconditionValue;
                        Grd1.Cells[row, 8].Value = ReconditionDocNo;
                        Grd1.Cells[row, 23].Value = NBVValue2;
                        Grd1.Cells[row, 24].Value = AssetCode;
                    }
                }

                TotalAssetValue = AssetValue + Sm.GetGrdDec(Grd1, row, 7);
                AssetDepreciableValue = TotalAssetValue - AccDeprOB - ResidualValue; //jika sumbernya initial asset maka Depreciable Value = AssetVal - AccDeprOB - ResidualVal (validasi di query)

                Grd1.Cells[row, 10].Value = TotalAssetValue;
                Grd1.Cells[row, 12].Value = AssetDepreciableValue;
            }
        }

        private void SetAccDeprValueAndNBVRow(int row)
        {
            decimal NBV = 0m, AccDeprValue = 0m, PrevAccDeprValue = 0m, TotalAssetValue = 0m, DepreciationValue = 0m, AccDeprValueRoA = 0m;

            int lastRow = Grd1.Rows.Count - 2;
            DepreciationValue = Sm.GetGrdDec(Grd1, row, 13);
            TotalAssetValue = Sm.GetGrdDec(Grd1, row, 10);

            if (row != 0) PrevAccDeprValue = Sm.GetGrdDec(Grd1, row - 1, 14);

            if (row == 0)
            {
                if (Sm.GetGrdStr(Grd1, row, 8).Length > 0)
                    AccDeprValueRoA = Sm.GetValueDec("Select AssetValue2 From TblReconditionAssetHdr Where DocNo = @Param", Sm.GetGrdStr(Grd1, row, 8));
                else
                    AccDeprValueRoA = Sm.GetValueDec("Select If(AssetSource = '2', AccDepr, 0.00) From TblAsset Where AssetCode = @Param", TxtAssetCode.Text);
                AccDeprValue = AccDeprValueRoA + DepreciationValue;
            }
            else
            {
                if (row == lastRow)
                {
                    DepreciationValue = Sm.GetGrdDec(Grd1, row - 1, 15) - Convert.ToDecimal(TxtResidualValue.Text);
                    Grd1.Cells[row, 13].Value = DepreciationValue;
                }
                AccDeprValue = PrevAccDeprValue + DepreciationValue;
            }

            NBV = TotalAssetValue - AccDeprValue;

            Grd1.Cells[row, 14].Value = AccDeprValue;

            //Grd1.Cells[row, 15].Value = Sm.GetGrdDec(Grd1, row, 12) - Sm.GetGrdDec(Grd1, row, 14);
            if (row == lastRow)
                Grd1.Cells[row, 15].Value = Sm.FormatNum(TxtResidualValue.Text, 0);
            else
                Grd1.Cells[row, 15].Value = NBV;
        }

        private void SetAccDeprValueAndNBV()
        {
            decimal AccDeprValueRoA = 0m;
            int lastRow = Grd1.Rows.Count - 2;

            for (int row = 0; row < Grd1.Rows.Count - 1; ++row)
            {
                if (row == 0)
                {
                    if (Sm.GetGrdStr(Grd1, row, 8).Length > 0)
                    {
                        AccDeprValueRoA = Sm.GetValueDec("Select AssetValue2 From TblReconditionAssetHdr Where DocNo = @Param", Sm.GetGrdStr(Grd1, row, 8));
                    }
                    else
                    {
                        AccDeprValueRoA = Sm.GetValueDec("Select If(AssetSource = '2', AccDepr, 0.00) From TblAsset Where AssetCode = @Param", TxtAssetCode.Text);
                    }
                    Grd1.Cells[row, 14].Value = AccDeprValueRoA + Sm.GetGrdDec(Grd1, row, 13);
                }
                else
                {
                    if (row == lastRow)
                        Grd1.Cells[row, 13].Value = Sm.GetGrdDec(Grd1, row - 1, 15) - Convert.ToDecimal(TxtResidualValue.Text);
                    Grd1.Cells[row, 14].Value = Sm.GetGrdDec(Grd1, row - 1, 14) + Sm.GetGrdDec(Grd1, row, 13);
                }

                //Grd1.Cells[row, 15].Value = Sm.GetGrdDec(Grd1, row, 12) - Sm.GetGrdDec(Grd1, row, 14);
                if (row == lastRow)
                    Grd1.Cells[row, 15].Value = Sm.FormatNum(TxtResidualValue.Text, 0);
                else
                    Grd1.Cells[row, 15].Value = Sm.GetGrdDec(Grd1, row, 10) - Sm.GetGrdDec(Grd1, row, 14);
            }
        }

        public void ComputeAnnualDepreciationRate()
        {
            if (!IsInsert || mAssetSource == "2") return;
            decimal year = decimal.Parse(TxtEcoLifeYr.Text);
            string DepMethod = string.Empty;
            DepMethod = Sm.GetLue(LueDepreciationCode);

            if (year != 0)
            {
                if (year <= decimal.Parse(mMaxEconomicLifeYear))
                {
                    decimal month = year * 12;
                    TxtEcoLife.Text = Sm.FormatNum(Math.Floor(month), 0);
                }
                else
                {
                    Sm.StdMsg(mMsgType.Warning, "Economic Life cannot be more than "+ mMaxEconomicLifeYear + " years");
                    TxtEcoLifeYr.Focus();
                }

                if (DepMethod == "1")
                {
                    decimal PercentageDepreciation = 100 / year; // 100 is equal to 100%
                    TxtPercentageAnnualDepreciation.Text = (Math.Round(PercentageDepreciation, 2)).ToString();
                }
                else if (DepMethod == "2")
                {
                    decimal PercentageDepreciation = (100 / year) * 2; // 100 is equal to 100%
                    TxtPercentageAnnualDepreciation.Text = (Math.Round(PercentageDepreciation, 2)).ToString();
                }
            }

            if (year == 0)
            {
                Sm.SetControlNumValueZero(new List<DXE.TextEdit>
                { TxtPercentageAnnualDepreciation, TxtEcoLife }, 0);
            }
        }

        private int CekRow(string Year)
        {
            int Count=0;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 5) == Year)
                {
                    Count = Count + 1;
                }
            }
            return Count;
        }

        private int CekRow2(int Row)
        {
            int Count = 0;
            if (Row % 12 == 0)
            {
                Count = 12;
            }
            else
            {
                Count = Row % 12;
            }
            return Count;
        }

        #endregion

        #region Event

        private void BtnAssetCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDepreciationAsset3Dlg(this));
            //Sm.SetControlReadOnly(new List<DXE.BaseEdit> {
            //    DteDepreciationDt //TxtAssetValue 
            //}, false);
            TxtAssetValue.Focus();
        }

        private void TxtEcoLife_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtEcoLife, 0);
            //ClearGrd();
            if (TxtAssetCode.Text.Length > 0 && mIsNeedCalculate)
            {
                SetMthGrd();
                SetDepAssetDtl(TxtAssetCode.Text);
                SetCostValue();
            }
        }

        private void TxtAssetValue_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAssetValue, 0);
            //ClearGrd();
            if (TxtAssetCode.Text.Length > 0 && mIsNeedCalculate)
            {
                SetMthGrd();
                SetDepAssetDtl(TxtAssetCode.Text);
                SetCostValue();
            }
        }

        private void DteDepreciationDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                string Dt1 = Sm.GetValue("Select case when AssetSource = '1' then AssetDt when AssetSource = '2' then OpeningBalanceDt END AS AssetDt From TblAsset Where AssetCode ='" + TxtAssetCode.Text + "'");
                string Dt2 = Sm.GetDte(DteDepreciationDt);
                if (Sm.GetDte(DteDepreciationDt).Length > 0)
                {
                    if (Dt2.Substring(0, 6) != Dt1.Substring(0, 6) || decimal.Parse(Dt2.Substring(6, 2)) < decimal.Parse(Dt1.Substring(6, 2)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "The day should not be less than the previous, and always keep the same month and year.");
                        Sm.SetDte(DteDepreciationDt, Dt1);
                    }
                }
            }
            //ClearGrd();
            if (TxtAssetCode.Text.Length > 0 && mIsNeedCalculate)
            {
                SetMthGrd();
                SetDepAssetDtl(TxtAssetCode.Text);
                SetCostValue();
            }
        }

        private void TxtEcoLifeYr_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtEcoLifeYr, 0);
            ComputeAnnualDepreciationRate();
            //ClearGrd();
            if (TxtAssetCode.Text.Length > 0 && mIsNeedCalculate)
            {
                SetMthGrd();
                SetDepAssetDtl(TxtAssetCode.Text);
                SetCostValue();
            }
        }

        private void TxtResidualValue_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtResidualValue, 0); 
            //ClearGrd();
            if (TxtAssetCode.Text.Length > 0 && mIsNeedCalculate)
            {
                SetMthGrd();
                SetDepAssetDtl(TxtAssetCode.Text);
                SetCostValue();
            }
        }

        private void EconomicLifeYearRule_Click(object sender, EventArgs e)
        {
            Sm.StdMsg(mMsgType.Info, mEconomicLifeYearRuleFormat);
        }

        private void DepreciationDateRule_Click(object sender, EventArgs e)
        {
            Sm.StdMsg(mMsgType.Info, mDepreciationDateRuleFormat);
        }

        private void LueDepreciationCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDepreciationCode, new Sm.RefreshLue1(SetLueDepreciationCode));
            if (Sm.GetLue(LueDepreciationCode).Length > 0)
            {
                ComputeAnnualDepreciationRate();
                // ClearGrd();
                if (TxtAssetCode.Text.Length > 0 && mIsNeedCalculate)
                {
                    SetMthGrd();
                    SetDepAssetDtl(TxtAssetCode.Text);
                    SetCostValue();
                }
            }
        }

        private void TxtPercentageAnnualDepreciation_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPercentageAnnualDepreciation, 0);
            //ClearGrd();
            if (TxtAssetCode.Text.Length > 0 && mIsNeedCalculate)
            {
                SetMthGrd();
                SetDepAssetDtl(TxtAssetCode.Text);
                SetCostValue();
            }
        }

        #endregion 
           
    }
}
