﻿#region Update
/*
    06/12/2017 [TKG] aplikasi baru
    06/02/2018 [HAR] tambah save ke journal
    15/02/2022 [DITA/PHT] closing journal dibuat based on profit center -> param : IsClosingJournalBasedOnMultiProfitCenter
    21/02/2022 [DITA/PHT] tidak bisa save closing date yg sama untuk profit center terfilter dengan yg sudah ada
    01/03/2022 [TKG/PHT] ubah GetParameter()
    02/03/2022 [TKG/PHT] Berdasarkan parameter ClosingJournalProfitCenterLevelToBeValidated, profit center yg bisa dipilih hanya dengan level tertentu saja.
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmClosingJournal : RunSystem.FrmBase1
    {
        #region Field, Property
        private List<String> mlProfitCenter = null;
        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mMainCurCode = string.Empty, mProfitCenterCode = string.Empty;
        internal FrmClosingJournalFind FrmFind;

        private bool
            mIsAutoJournalActived = false,
            mIsCOAAssetUseStartYr = false,
            mIsEntityMandatory = false,
            mIsReportingFilterByEntity = false,
            mIsAllProfitCenterSelected = false;

        private string
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mCOAAssetStartYr = string.Empty,
            mCOAAssetAcNo = string.Empty,
            mAccountingRptStartFrom = string.Empty;

        internal bool mIsClosingJournalBasedOnMultiProfitCenter = false;

        internal decimal mClosingJournalProfitCenterLevelToBeValidated = 0m;

        #endregion

        #region Constructor

        public FrmClosingJournal(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Monthly Journal Entries's Closing";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                if (!mIsClosingJournalBasedOnMultiProfitCenter)
                {
                    LblMultiProfitCenterCode.Visible = false;
                    CcbProfitCenterCode.Visible = false;
                    panel4.Visible = false;
                }
                else
                {
                    LblJournalDocNo.Visible = false;
                    TxtJournalDocNo.Visible = false;
                    LblMultiProfitCenterCode.Location = new System.Drawing.Point(12, 81);
                    CcbProfitCenterCode.Location = new System.Drawing.Point(120, 78);
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);

                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, ChkCancelInd, DteClosingDt, TxtJournalDocNo,  MeeRemark, CcbProfitCenterCode, MeeCancelReason }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, DteClosingDt, MeeRemark, CcbProfitCenterCode }, false);
                    DteClosingDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { MeeCancelReason }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtDocNo, DteDocDt, DteClosingDt, TxtJournalDocNo, MeeRemark, CcbProfitCenterCode, MeeCancelReason });
            ChkCancelInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmClosingJournalFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            var ClosingDt = Sm.GetDte(DteClosingDt);
            var Mth = ClosingDt.Substring(4, 2);
            string DocNo = string.Empty;
            var cml = new List<MySqlCommand>();

            mProfitCenterCode = GetCcbProfitCenterCode();

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            //ProcessData();
            if (mIsClosingJournalBasedOnMultiProfitCenter)
            {
                var l = new List<ClosingJournal>();

                string[] mProfitCenterCodes = mProfitCenterCode.Split(',');
                int index = 0;

                foreach (var x in mProfitCenterCodes)
                {

                    index += 1;
                    l.Add(new ClosingJournal()
                    {
                        DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ClosingJournal", "TblClosingJournal", index.ToString()),
                        ProfitCenterCode = x
                    });

                }
                foreach (var y in l)
                    cml.Add(SaveClosingJournal(y.DocNo, y.ProfitCenterCode));

                l.Clear();
                ClearData();
            }
            else
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ClosingJournal", "TblClosingJournal");

                cml.Add(SaveClosingJournal(DocNo, string.Empty));
            }

            //if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            
            Sm.ExecCommands(cml);

           if(!mIsClosingJournalBasedOnMultiProfitCenter) ShowData(DocNo);
        }

        private MySqlCommand SaveClosingJournal(string DocNo, string ProfitCenterCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblClosingJournal(DocNo, CancelInd, DocDt, ClosingDt, ProfitCenterCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, 'N', @DocDt, @ClosingDt, @ProfitCenterCode ,@Remark, @UserCode, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@ClosingDt", Sm.GetDte(DteClosingDt));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveCOAOpeningBalance(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCOAOpeningBalanceHdr ");
            SQL.AppendLine("(DocNo, DocDt, Yr, CancelInd, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, 'N', @EntCode, @Remark, @CreateBy, @CreateDt); ");

            SQL.AppendLine("Insert Into TblCOAOpeningBalanceDtl(DocNo, AcNo, Amt, CreateBy, CreateDt) ");

            var cm = new MySqlCommand()
            {
                CommandText =
                        "Insert Into TblClosingJournal(DocNo, CancelInd, DocDt, ClosingDt, Remark, CreateBy, CreateDt) " +
                        "Values(@DocNo, 'N', @DocDt, @ClosingDt, @Remark, @UserCode, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@ClosingDt", Sm.GetDte(DteClosingDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteClosingDt, "Closing date") ||
                mIsClosingJournalBasedOnMultiProfitCenter && IsProfitCenterInvalid()||
                IsClosingDtInvalid();
        }

        private bool IsClosingDtInvalid()
        {
            if (!mIsClosingJournalBasedOnMultiProfitCenter)
            {
                return Sm.IsDataExist(
                    "Select 1 From TblClosingJournal Where CancelInd='N' And ClosingDt>@Param;",
                    Sm.Left(Sm.GetDte(DteClosingDt), 8),
                    "Invalid closing date.");
            }
            else
            {
                if (Sm.IsDataExist("Select 1 From TblClosingJournal Where CancelInd='N' And ClosingDt>=@Param1 And Find_In_Set(ProfitCenterCode, @Param2) ;",
                        Sm.Left(Sm.GetDte(DteClosingDt), 8), mProfitCenterCode, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, "One or more selected profit center(s) is/are already in active document.");
                    return true;
                }
                return false;
            }

        }

        #region Process Opening Journal

        private void ProcessData()
        {
            var lCOA = new List<COA>();

            var ClosingDt = Sm.GetDte(DteClosingDt);
            var Yr = Sm.Left(ClosingDt, 4);
            var Mth = ClosingDt.Substring(4, 2);

            var StartFrom = Sm.GetValue("Select Max(Yr) From TblCOAOpeningBalanceHdr Where CancelInd='N' And Yr<=@Param;", Yr);
            if (StartFrom.Length == 0) StartFrom = Yr;

            Process1(ref lCOA);
            
            if (lCOA.Count > 0)
            {
                if (StartFrom.Length > 0)
                {
                    Process2(ref lCOA, StartFrom);
                    Process3(ref lCOA, Yr, Mth, StartFrom);
                }
                else
                {
                    Process2(ref lCOA, Yr);
                    Process3(ref lCOA, Yr, Mth, string.Empty);
                }
                Process4(ref lCOA, Yr, Mth);
                // Process5(ref lCOA); TIDAK DIGUNAKAN
                Process6(ref lCOA);
                Process7(ref lCOA);
                Process9(ref lCOA);

                lCOA.RemoveAll(w => !(Sm.Left(w.AcNo, 1) == "1" || Sm.Left(w.AcNo, 1) == "2" || Sm.Left(w.AcNo, 1) == "3"));

                lCOA.Clear();
            }
        }

        private void Process1(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Left Join TblCOADtl B On A.AcNo=B.AcNo ");
                SQL.AppendLine("Where A.ActInd='Y'  ");
                SQL.AppendLine("Order By A.AcNo; ");

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A, TblCOAOpeningBalanceDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            SQL.AppendLine("And B.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Yr=@Yr ");
            SQL.AppendLine("Order By B.AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A, TblJournalDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("And Left(A.DocDt, 6)<@YrMth ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("And Left(A.DocDt, 6)<@YrMth ");
            }
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By AcNo;");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0)
                Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<COA> lCOA, string Yr, string Mth)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A, TblJournalDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
            SQL.AppendLine("And Substring(A.DocDt, 5, 2)=@Mth ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            SQL.AppendLine("Group By B.AcNo;");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process6(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt +
                    lCOA[i].CurrentMonthDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt +
                    lCOA[i].CurrentMonthCAmt;
            }
        }

        private void Process7(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process9(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].AcType == "D")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceDAmt +
                        lCOA[i].MonthToDateDAmt -
                        lCOA[i].MonthToDateCAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthDAmt -
                        lCOA[i].CurrentMonthCAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
                if (lCOA[i].AcType == "C")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateCAmt -
                        lCOA[i].MonthToDateDAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthCAmt -
                        lCOA[i].CurrentMonthDAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                }
            }
        }

        #endregion

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string JournalDocNo = Sm.GetValue("Select JournalDocNo From tblClosingJournal Where DocNo = '"+TxtDocNo.Text+"' ");

            var cml = new List<MySqlCommand>();

            if(JournalDocNo.Length>0)
                cml.Add(DeleteJournal(JournalDocNo));
            cml.Add(EditClosingJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private MySqlCommand DeleteJournal(string JournalDocNo)
        {
            string[] strArr = null;
            char[] splitchar = { '#' };
            strArr = JournalDocNo.Split(splitchar);
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From tbljournalHdr Where DocNo = '"+ strArr[0]+"' ; "+
                    "Delete From tbljournalDtl Where DocNo = '" + strArr[0] + "' ; " +
                    "Delete From tbljournalHdr Where DocNo = '" + strArr[1] + "'; "+
                    "Delete From tbljournalHdr Where DocNo = '"+ strArr[1]+"'; "
            };
            return cm;
        }

        private MySqlCommand EditClosingJournal()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblClosingJournal Set CancelInd='Y', CancelReason=@CancelReason ,JournalDocNo = NULL, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And CancelInd='N';"
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private bool IsEditedDataNotValid()
        {
            return 
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                mIsClosingJournalBasedOnMultiProfitCenter && Sm.IsMeeEmpty(MeeCancelReason, "Cancel Reason") ||
                IsDocAlreadyCancelled();
        }

      
        private bool IsDocAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblClosingJournal Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowClosingJournal(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }


        private void ShowClosingJournal(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.CancelInd, A.DocDt, A.ClosingDt, A.JournalDocNo, A.Remark ");
            if (mIsClosingJournalBasedOnMultiProfitCenter) SQL.AppendLine(", A.CancelReason, B.ProfitCenterName ");
            else SQL.AppendLine(", Null As CancelReason, Null As ProfitCenterName ");
            SQL.AppendLine("From TblClosingJournal A ");
            if(mIsClosingJournalBasedOnMultiProfitCenter) SQL.AppendLine("Inner Join TblProfitCenter B On A.ProfitCenterCode = B.ProfitCenterCode ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
                ref cm,SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "CancelInd", "DocDt", "ClosingDt", "JournalDocNo", "Remark",

                    //6-7
                    "CancelReason", "ProfitCenterName"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[1])=="Y";
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    Sm.SetDte(DteClosingDt, Sm.DrStr(dr, c[3]));
                    TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[6]);
                    CcbProfitCenterCode.EditValue = Sm.DrStr(dr, c[7]);
                }, true
            );
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsEntityMandatory', 'IsCOAAssetUseStartYr', 'IsReportingFilterByEntity', 'IsClosingJournalBasedOnMultiProfitCenter', 'IsAutoJournalActived', ");
            SQL.AppendLine("'MainCurCode', 'AcNoForCurrentEarning', 'AcNoForIncome', 'AcNoForCost', 'COAAssetStartYr', ");
            SQL.AppendLine("'COAAssetAcNo', 'AccountingRptStartFrom', 'ClosingJournalProfitCenterLevelToBeValidated' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsReportingFilterByEntity": mIsReportingFilterByEntity = ParValue == "Y"; break;
                            case "IsCOAAssetUseStartYr": mIsCOAAssetUseStartYr = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;

                            //string
                            case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                            case "COAAssetAcNo": mCOAAssetAcNo = ParValue; break;
                            case "COAAssetStartYr": mCOAAssetStartYr = ParValue; break;
                            case "AcNoForCost": mAcNoForCost = ParValue; break;
                            case "AcNoForIncome": mAcNoForIncome = ParValue; break;
                            case "AcNoForCurrentEarning": mAcNoForCurrentEarning = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;

                            //decimal
                            case "ClosingJournalProfitCenterLevelToBeValidated":
                                if (ParValue.Length > 0)
                                    mClosingJournalProfitCenterLevelToBeValidated = decimal.Parse(ParValue);
                                break;

                        }
                    }
                }
                dr.Close();
            }
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            string CloseDate = Sm.GetValue("Select MAX(ClosingDt) As ClosingDt From TblClosingJournal Where DocNo != '" + TxtDocNo.Text + "' And CancelInd = 'N' ");


            #region Journal 1

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo1, DocDt, Concat('Closing Journal(1) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblClosingJournal Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, Dno, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo1, Y.Dno, Y.AcNo, if(Y.Acno='3.3', ifnull(X.DAmt, 0), 0) As DAmt,");
            if (CloseDate.Length == 0)
            {
                SQL.AppendLine("ifnull(ifnull(Y.DAmt, 0)-ifnull(Y.CAmt, 0), 0) CAmt, ");
            }
            else
            {
                SQL.AppendLine("ifnull(ifnull(Z.Amt, 0)+ifnull(Y.DAmt, 0)-ifnull(Y.CAmt, 0), 0) CAmt, ");
            }
            SQL.AppendLine("NULL, NULL, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.AcNo, A.Dno, SUM(B.DAmt) As DAmt, SUM(B.CAmt) As CAmt ");
            SQL.AppendLine("    From TblAccountClosingJournal A ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        Select B.AcNo,  ifnull(SUM(B.DAmt), 0) As DAmt, ifnull(SUM(B.CAmt), 0) As CAmt ");  
	        SQL.AppendLine("        from  TblJournalHdr A   ");
	        SQL.AppendLine("        Left Join TblJournalDtl B On A.Docno = B.DocNo ");
            SQL.AppendLine("        Where A.MenuCode != '" + mMenuCode + "' ");
            if (CloseDate.Length > 0)
            {
                SQL.AppendLine("    And A.DocDt between " + CloseDate + " And " + Sm.GetDte(DteClosingDt).Substring(0, 8) + "");
            }
            
	        SQL.AppendLine("        Group BY B.AcNo ");
            SQL.AppendLine("    )B On A.AcNo = B.AcNo ");
            SQL.AppendLine("    Where A.ActiveInd  = 'Y' And A.JNGroup = '1' ");
            SQL.AppendLine("    Group BY A.AcNo, A.Dno ");
            SQL.AppendLine(")Y ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.AcNo, Sum(T2.Amt) As Amt  ");
            SQL.AppendLine("    From TblCOAOpeningBalanceHdr T1, TblCOAOpeningBalanceDtl T2  ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo  ");
            SQL.AppendLine("    And T1.CancelInd='N'  ");
            SQL.AppendLine("    And T1.Yr='" + Sm.GetDte(DteClosingDt).Substring(0, 4) + "'  ");
            SQL.AppendLine("    Group By T2.AcNo  ");
            SQL.AppendLine(")Z On Y.AcNo = Z.AcNo ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select (select ParValue From TblParameter  ");
            SQL.AppendLine("    Where ParCode = 'AcNoForCurrentEarning') As Acno, SUM(X.DAmt)-SUM(X.CAmt) As DAmt  ");
            SQL.AppendLine("    From   ");
            SQL.AppendLine("    (  ");
            SQL.AppendLine("        Select A.AcNo, A.Dno, SUM(B.DAmt) As DAmt, SUM(B.CAmt) As CAmt ");
            SQL.AppendLine("        From TblAccountClosingJournal A ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select B.AcNo,  ifnull(SUM(B.DAmt), 0) As DAmt, ifnull(SUM(B.CAmt), 0) As CAmt ");
            SQL.AppendLine("            from  TblJournalHdr A   ");
            SQL.AppendLine("            Left Join TblJournalDtl B On A.Docno = B.DocNo ");
            SQL.AppendLine("            Where A.MenuCode != '" + mMenuCode + "' ");
            if (CloseDate.Length > 0)
            {
                SQL.AppendLine("        And A.DocDt between " + CloseDate + " And " + Sm.GetDte(DteClosingDt).Substring(0, 8) + "");
            }
            SQL.AppendLine("            Group BY B.AcNo ");
            SQL.AppendLine("        )B On A.AcNo = B.AcNo ");
            SQL.AppendLine("        Where A.ActiveInd  = 'Y' And A.JNGroup = '1' ");
            SQL.AppendLine("        Group BY A.AcNo, A.Dno ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine(")X On Y.AcNo = X.AcNo ");
            SQL.AppendLine("Order BY Y.Dno ; ");
            #endregion 

            #region journal 2
            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo2, DocDt, Concat('Closing Journal(2) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblClosingJournal Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, Dno, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo2, Y.Dno, Y.AcNo, if(Y.Acno='3.3', ifnull(X.DAmt, 0), 0) As DAmt, ");
            if (CloseDate.Length == 0)
            {
                SQL.AppendLine("ifnull(ifnull(Y.DAmt, 0)-ifnull(Y.CAmt, 0), 0) CAmt, ");
            }
            else
            {
                SQL.AppendLine("ifnull(ifnull(Z.Amt, 0)+ifnull(Y.DAmt, 0)-ifnull(Y.CAmt, 0), 0) CAmt, ");
            }
            SQL.AppendLine("NULL, NULL, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.AcNo, A.Dno, ifnull(B.DAmt, 0) As DAmt, ifnull(B.CAmt, 0) As CAmt ");
            SQL.AppendLine("    From TblAccountClosingJournal A ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        Select B.AcNo,  ifnull(SUM(B.DAmt), 0) As DAmt, ifnull(SUM(B.CAmt), 0) As CAmt  "); 
	        SQL.AppendLine("        from  TblJournalHdr A   "); 
	        SQL.AppendLine("        Left Join TblJournalDtl B On A.Docno = B.DocNo ");
	        SQL.AppendLine("        Where A.MenuCode != '"+mMenuCode+"' ");
            if (CloseDate.Length > 0)
            {
                SQL.AppendLine("    And A.DocDt between " + CloseDate + " And " + Sm.GetDte(DteClosingDt).Substring(0, 8) + "");
            }
	        SQL.AppendLine("        Group BY B.AcNo ");
            SQL.AppendLine("    )B On A.AcNo = B.AcNo ");
            SQL.AppendLine("    Where A.ActiveInd  = 'Y' And A.JNGroup = '2' ");
            SQL.AppendLine("    Group BY A.AcNo, A.Dno ");
            SQL.AppendLine(")Y ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.AcNo, Sum(T2.Amt) As Amt  ");
            SQL.AppendLine("    From TblCOAOpeningBalanceHdr T1, TblCOAOpeningBalanceDtl T2  ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo  ");
            SQL.AppendLine("    And T1.CancelInd='N'  ");
            SQL.AppendLine("    And T1.Yr='" + Sm.GetDte(DteClosingDt).Substring(0, 4) + "'  ");
            SQL.AppendLine("    Group By T2.AcNo  ");
            SQL.AppendLine(")Z On Y.AcNo = Z.AcNo ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select (select ParValue From TblParameter  ");
            SQL.AppendLine("    Where ParCode = 'AcNoForCurrentEarning') As Acno, SUM(X.DAmt)-SUM(X.CAmt) As DAmt  ");
            SQL.AppendLine("    From   ");
            SQL.AppendLine("    (  ");
            SQL.AppendLine("        Select A.AcNo, A.Dno, SUM(B.DAmt) As DAmt, SUM(B.CAmt) As CAmt   ");
            SQL.AppendLine("        From TblAccountClosingJournal A   ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select B.AcNo,  ifnull(SUM(B.DAmt), 0) As DAmt, ifnull(SUM(B.CAmt), 0) As CAmt  ");
            SQL.AppendLine("            from  TblJournalHdr A   ");
            SQL.AppendLine("            Left Join TblJournalDtl B On A.Docno = B.DocNo ");
            SQL.AppendLine("            Where A.MenuCode != '" + mMenuCode + "' ");
            if (CloseDate.Length > 0)
            {
                SQL.AppendLine("        And A.DocDt between " + CloseDate + " And " + Sm.GetDte(DteClosingDt).Substring(0, 8) + "");
            }
            SQL.AppendLine("            Group BY B.AcNo ");
            SQL.AppendLine("        )B On A.AcNo = B.AcNo ");
            SQL.AppendLine("        Where A.ActiveInd  = 'Y' And A.JNGroup = '2'   ");
            SQL.AppendLine("     Group BY A.AcNo, A.Dno   ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine(")X On Y.AcNo = X.AcNo ");
            SQL.AppendLine("Order BY Y.Dno ; ");
            #endregion

            SQL.AppendLine("Update TblClosingJournal Set JournalDocNo=Concat(@JournalDocNo1, '#', @JournalDocNo2) Where DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo1", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 2));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Multi profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }


        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T1.ProfitCenterName As Col, T1.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T1 ");
            SQL.AppendLine("    WHERE Exists(  ");
            SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
            SQL.AppendLine("        Where ProfitCenterCode=T1.ProfitCenterCode  ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            if (mClosingJournalProfitCenterLevelToBeValidated>0m)
                SQL.AppendLine("    And Level=@ClosingJournalProfitCenterLevelToBeValidated ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<decimal>(ref cm, "@ClosingJournalProfitCenterLevelToBeValidated", mClosingJournalProfitCenterLevelToBeValidated);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        #endregion

        #endregion


        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        public class ClosingJournal
        {
            public string ProfitCenterCode { get; set; }
            public string DocNo { get; set; }
        }

        #endregion
    }
}
