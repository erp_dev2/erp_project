﻿#region Update
/*
  26/07/2019 [DITA] Master baru
 **/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemProperty : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmItemPropertyFind FrmFind;

        #endregion

        #region Constructor

        public FrmItemProperty(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItCode, TxtItName, TxtPropCode, TxtPropName
                    }, true);
                    TxtItCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItCode, TxtItName, TxtPropCode, TxtPropName
                    }, true);
                    TxtItCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItCode, TxtItName, TxtPropCode, TxtPropName
                    }, true);
                    TxtItCode.Focus();
                    break;
                default:
                    break;
            }
        }

       public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtItCode, TxtItName, TxtPropCode, TxtPropName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemPropertyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblItemProperty(ItCode, PropCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ItCode, @PropCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ItCode=@ItCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
                Sm.CmParam<String>(ref cm, "@PropCode", TxtPropCode.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtItCode.Text, TxtPropCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ItCode, string PropCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
                Sm.CmParam<String>(ref cm, "@PropCode", PropCode);
                Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.ItCode, A.PropCode, C.PropName, B.ItName From TblItemProperty A "+
                    "Inner Join TblItem B On A.ItCode = B.ItCode "+
                    "Inner Join TblProperty C On A.PropCode = C.PropCode "+
                    " Where A.ItCode=@ItCode And A.PropCode= @PropCode ",
                    new string[] 
                    {
                        "ItCode", "ItName", "PropCode", "PropName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtItCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtPropCode.EditValue = Sm.DrStr(dr, c[2]);
                        TxtPropName.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItCode, "Item Code", false) ||
                Sm.IsTxtEmpty(TxtPropCode, "Property Code", false) ||
                IsItCodeExisted();
                IsPropCodeExisted();
        }

        private bool IsItCodeExisted()
        {
            if (Sm.IsDataExist("Select ItCode From TblItemProperty Where ItCode=@Param Limit 1;", TxtItCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Item code ( " + TxtItCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsPropCodeExisted()
        {
            if (Sm.IsDataExist("Select PropCode From TblItemProperty Where PropCode=@Param Limit 1;", TxtPropCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Property code ( " + TxtPropCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItCode);
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItName);
        }

        private void TxtPropCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPropCode);
        }

        private void TxtPropName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPropName);
        }

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmItemPropertyDlg(this));
            }
        }
        private void BtnPropCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmItemPropertyDlg2(this));
            }
        }

        #endregion

       

        #endregion

        

       

    }
}
