﻿namespace RunSystem
{
    partial class FrmRptPartnerDemographic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPartnerCode = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.ChkProvCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkCityCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkPartnerCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueCityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueProvCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPartnerCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProvCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPartnerCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProvCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LueProvCode);
            this.panel2.Controls.Add(this.LueCityCode);
            this.panel2.Controls.Add(this.ChkPartnerCode);
            this.panel2.Controls.Add(this.ChkCityCode);
            this.panel2.Controls.Add(this.ChkProvCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtPartnerCode);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Size = new System.Drawing.Size(772, 70);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 403);
            this.Grd1.TabIndex = 16;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 70);
            this.panel3.Size = new System.Drawing.Size(772, 403);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(33, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "City";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(7, 26);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Province";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPartnerCode
            // 
            this.TxtPartnerCode.EnterMoveNextControl = true;
            this.TxtPartnerCode.Location = new System.Drawing.Point(65, 44);
            this.TxtPartnerCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPartnerCode.Name = "TxtPartnerCode";
            this.TxtPartnerCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPartnerCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPartnerCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPartnerCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPartnerCode.Properties.MaxLength = 300;
            this.TxtPartnerCode.Size = new System.Drawing.Size(194, 20);
            this.TxtPartnerCode.TabIndex = 15;
            this.TxtPartnerCode.Validated += new System.EventHandler(this.TxtPartnerCode_Validated);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(13, 47);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(47, 14);
            this.label26.TabIndex = 14;
            this.label26.Text = "Partner";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkProvCode
            // 
            this.ChkProvCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkProvCode.Location = new System.Drawing.Point(261, 22);
            this.ChkProvCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkProvCode.Name = "ChkProvCode";
            this.ChkProvCode.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkProvCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProvCode.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkProvCode.Properties.Appearance.Options.UseBackColor = true;
            this.ChkProvCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProvCode.Properties.Appearance.Options.UseForeColor = true;
            this.ChkProvCode.Properties.Caption = "";
            this.ChkProvCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProvCode.Size = new System.Drawing.Size(22, 22);
            this.ChkProvCode.TabIndex = 13;
            this.ChkProvCode.CheckedChanged += new System.EventHandler(this.ChkProvCode_CheckedChanged);
            // 
            // ChkCityCode
            // 
            this.ChkCityCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCityCode.Location = new System.Drawing.Point(261, 1);
            this.ChkCityCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCityCode.Name = "ChkCityCode";
            this.ChkCityCode.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCityCode.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCityCode.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCityCode.Properties.Appearance.Options.UseFont = true;
            this.ChkCityCode.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCityCode.Properties.Caption = "";
            this.ChkCityCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCityCode.Size = new System.Drawing.Size(22, 22);
            this.ChkCityCode.TabIndex = 10;
            this.ChkCityCode.CheckedChanged += new System.EventHandler(this.ChkCityCode_CheckedChanged);
            // 
            // ChkPartnerCode
            // 
            this.ChkPartnerCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPartnerCode.Location = new System.Drawing.Point(261, 43);
            this.ChkPartnerCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPartnerCode.Name = "ChkPartnerCode";
            this.ChkPartnerCode.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPartnerCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPartnerCode.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPartnerCode.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPartnerCode.Properties.Appearance.Options.UseFont = true;
            this.ChkPartnerCode.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPartnerCode.Properties.Caption = "";
            this.ChkPartnerCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPartnerCode.Size = new System.Drawing.Size(22, 22);
            this.ChkPartnerCode.TabIndex = 16;
            this.ChkPartnerCode.CheckedChanged += new System.EventHandler(this.ChkPartnerCode_CheckedChanged);
            // 
            // LueCityCode
            // 
            this.LueCityCode.EnterMoveNextControl = true;
            this.LueCityCode.Location = new System.Drawing.Point(65, 2);
            this.LueCityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCityCode.Name = "LueCityCode";
            this.LueCityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.Appearance.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCityCode.Properties.DropDownRows = 30;
            this.LueCityCode.Properties.MaxLength = 16;
            this.LueCityCode.Properties.NullText = "[Empty]";
            this.LueCityCode.Properties.PopupWidth = 350;
            this.LueCityCode.Size = new System.Drawing.Size(194, 20);
            this.LueCityCode.TabIndex = 9;
            this.LueCityCode.ToolTip = "F4 : Show/hide list";
            this.LueCityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCityCode.EditValueChanged += new System.EventHandler(this.LueCityCode_EditValueChanged);
            // 
            // LueProvCode
            // 
            this.LueProvCode.EnterMoveNextControl = true;
            this.LueProvCode.Location = new System.Drawing.Point(65, 23);
            this.LueProvCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProvCode.Name = "LueProvCode";
            this.LueProvCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvCode.Properties.Appearance.Options.UseFont = true;
            this.LueProvCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProvCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProvCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProvCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProvCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProvCode.Properties.DropDownRows = 30;
            this.LueProvCode.Properties.MaxLength = 16;
            this.LueProvCode.Properties.NullText = "[Empty]";
            this.LueProvCode.Properties.PopupWidth = 350;
            this.LueProvCode.Size = new System.Drawing.Size(194, 20);
            this.LueProvCode.TabIndex = 12;
            this.LueProvCode.ToolTip = "F4 : Show/hide list";
            this.LueProvCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProvCode.EditValueChanged += new System.EventHandler(this.LueProvCode_EditValueChanged);
            // 
            // FrmRptPartnerDemographic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptPartnerDemographic";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtPartnerCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProvCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPartnerCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProvCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtPartnerCode;
        private System.Windows.Forms.Label label26;
        private DevExpress.XtraEditors.CheckEdit ChkPartnerCode;
        private DevExpress.XtraEditors.CheckEdit ChkCityCode;
        private DevExpress.XtraEditors.CheckEdit ChkProvCode;
        private DevExpress.XtraEditors.LookUpEdit LueProvCode;
        private DevExpress.XtraEditors.LookUpEdit LueCityCode;
    }
}