﻿#region Update
/*
    11/09/2020 [TKG/KSM] New Reporting
    05/01/2021 [IBL/KSM] Menambahkan garis border pada printout
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMontlyReportingWeavingComb : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMontlyReportingWeavingComb(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.Both;
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetLueWhsCode(ref LueWhsCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code",
                        "Item's Name",
                        "Local Code",
                        "Beginning",
                        "In",
                       
                        //6-8
                        "Out",
                        "Return In",
                        "Ending"
                    },
                     new int[] 
                    {
                        50,
                        120, 200, 120, 130, 130, 
                        130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            if (Sm.IsLueEmpty(LueMth, "Month")) return;
            if (Sm.IsLueEmpty(LueWhsCode, "Warehouse")) return;

            Cursor.Current = Cursors.WaitCursor;

            var l1 = new List<Data1>();
            var l2 = new List<Data2>();
            var l3 = new List<Data2>();
            var l4 = new List<Data2>();
            var l5 = new List<Data2>();

            var Dt = string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth), "01");
            var WhsCode = Sm.GetLue(LueWhsCode);

            Sm.ClearGrd(Grd1, false);

            try
            {
                Process1(ref l1, WhsCode);
                if (l1.Count > 0)
                {
                    Process2(ref l2, WhsCode, Dt, "And DocDt>=@Dt ");
                    Process2(ref l3, WhsCode, Dt, "And Left(DocDt, 6)=Left(@Dt, 6) And Qty>0.00 And DocType Not In ('16') ");
                    Process2(ref l4, WhsCode, Dt, "And Left(DocDt, 6)=Left(@Dt, 6) And Qty<0.00 And DocType Not In ('16') ");
                    Process2(ref l5, WhsCode, Dt, "And Left(DocDt, 6)=Left(@Dt, 6) And DocType='16' ");
                    Process3(ref l1, ref l2, ref l3, ref l4, ref l5);

                    l2.Clear();
                    l3.Clear();
                    l4.Clear();
                    l5.Clear();

                    Process4(ref l1);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l1.Clear();
                l2.Clear();
                l3.Clear();
                l4.Clear();
                l5.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void SetLueWhsCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WhsCode As Col1, A.WhsName As Col2 ");
            SQL.AppendLine("From TblWarehouse A ");
            SQL.AppendLine("Left Join TblParameter B ");
            SQL.AppendLine("    On B.ParCode='RptWeavingCombWhs' ");
            SQL.AppendLine("    And B.ParValue Is Not Null ");
            SQL.AppendLine("Where Find_In_Set(IfNull(A.WhsCode, ''), IfNull(B.ParValue, '')) ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("    Where WhsCode=A.WhsCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Order By A.WhsName;");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void Process1(ref List<Data1> l, string WhsCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
            
            SQL.AppendLine("Select A.ItCode, B.ItName, B.ItCodeInternal, A.Stock ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select ItCode, Sum(Qty) As Stock ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where WhsCode=@WhsCode ");
            SQL.AppendLine("    Group By ItCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "ItCode", "ItName", "ItCodeInternal", "Stock" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data1()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            ItName = Sm.DrStr(dr, c[1]),
                            ItCodeInternal = Sm.DrStr(dr, c[2]),
                            Stock = Sm.DrDec(dr, c[3]),
                            Beginning = 0m,
                            In = 0m,
                            Out = 0m,
                            ReturnIn = 0m,
                            Ending = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Data2> l, string WhsCode, string Dt, string subQuery)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
            Sm.CmParamDt(ref cm, "@Dt", Dt);

            SQL.AppendLine("Select ItCode, Sum(Qty) Qty ");
            SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine(subQuery);
            SQL.AppendLine("Group By ItCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "Qty" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data2()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            Qty = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(
            ref List<Data1> l1, 
            ref List<Data2> l2, 
            ref List<Data2> l3, 
            ref List<Data2> l4, 
            ref List<Data2> l5)
        {
            foreach (var i in l1.OrderBy(o=>o.ItCode))
            {
                foreach (var j in l2.Where(w => Sm.CompareStr(w.ItCode, i.ItCode)))
                    i.Beginning = i.Stock - j.Qty;

                foreach (var k in l3.Where(w => Sm.CompareStr(w.ItCode, i.ItCode)))
                    i.In = k.Qty;

                foreach (var l in l4.Where(w => Sm.CompareStr(w.ItCode, i.ItCode)))
                    i.Out = l.Qty;

                foreach (var m in l5.Where(w => Sm.CompareStr(w.ItCode, i.ItCode)))
                    i.ReturnIn = m.Qty;

                i.Ending = i.Beginning + i.In + i.Out + i.ReturnIn;
            }
        }

        private void Process4(ref List<Data1> l)
        {
            bool IsExisted = false;
            iGRow r;
            int n = 0;

            Grd1.BeginUpdate();
            foreach (var x in l.Where(w=>
                w.Beginning != 0m || w.In != 0m || w.Out != 0m || w.ReturnIn != 0m || w.Ending != 0m
            ).OrderBy(o => o.ItName))
            {
                r = Grd1.Rows.Add();
                n++;
                r.Cells[0].Value = n;
                r.Cells[1].Value = x.ItCode;
                r.Cells[2].Value = x.ItName;
                r.Cells[3].Value = x.ItCodeInternal;
                r.Cells[4].Value = x.Beginning;
                r.Cells[5].Value = x.In;
                r.Cells[6].Value = Math.Abs(x.Out);
                r.Cells[7].Value = Math.Abs(x.ReturnIn);
                r.Cells[8].Value = x.Ending;
                IsExisted = true;
            }

            if (IsExisted)
            {
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 4, 5, 6, 7, 8 });
            }
            else
                Sm.StdMsg(mMsgType.NoData, string.Empty);

            Sm.SetGrdProperty(Grd1, false);

            Grd1.EndUpdate();
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_Validated(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(SetLueWhsCode));
        }

        #endregion

        #endregion

        #region Class

        private class Data1
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ItCodeInternal { get; set; }
            public decimal Stock { get; set; }
            public decimal Beginning { get; set; }
            public decimal In { get; set; }
            public decimal Out { get; set; }
            public decimal ReturnIn { get; set; }
            public decimal Ending { get; set; }
        }

        private class Data2
        {
            public string ItCode { get; set; }
            public decimal Qty { get; set; }
        }

        #endregion
    }
}
