﻿#region Update
/*
    30/08/2018 [WED] Saat save tender, yang di update ke MRDtl adalah TenderDocNo nya, bukan ProcessInd nya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTender : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mMRDocNo = string.Empty,
            mMRDNo = string.Empty;
        internal bool mIsPORequestFilterByDept = false;
        internal FrmTenderFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmTender(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmTender");
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueSector(ref LueSector);
                LueSector.Visible = false;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 2;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] { "SectorCode", "Sector" },
                new int[] { 0, 200 }
            );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] {  }, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtMaterialRequestDocNo, TxtItCode, TxtItName, 
                        TxtCurCode, TxtEstPrice, TxtQty, TxtStatus, MeeRemark, MeeTenderName, DteExpiredDt
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    BtnMRDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, MeeTenderName, DteExpiredDt
                    }, false);
                    DteDocDt.Focus();
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    BtnMRDocNo.Enabled = true;
                    break;
                case mState.Edit:
                    if (Sm.IsDataExist("Select DocNo From TblTender Where DocNo = @Param And Status = 'O' And ActInd = 'Y'; ", TxtDocNo.Text))
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteExpiredDt }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mMRDocNo = string.Empty;
            mMRDNo = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtMaterialRequestDocNo, TxtItCode, TxtItName, TxtCurCode, 
                TxtStatus, MeeRemark, MeeTenderName, DteExpiredDt
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtEstPrice, TxtQty }, 0);
            ChkActInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTenderFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkActInd.Checked = true;
                TxtStatus.EditValue = "Open";
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Cancel", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Methods

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd1, LueSector, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Tender", "TblTender");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveTender(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    cml.Add(SaveTenderDtl(DocNo, Row));

            cml.Add(UpdateMRProcessInd(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteExpiredDt, "Expired Tender Date") ||
                IsDateInvalid() ||
                Sm.IsMeeEmpty(MeeTenderName, "Tender Name") ||
                Sm.IsTxtEmpty(TxtMaterialRequestDocNo, "MR#", false) ||
                Sm.IsTxtEmpty(TxtItCode, "Item", false) ||
                IsGrdValueInvalid();
        }

        private bool IsGrdValueInvalid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Sector is empty.")) return true;
                }

                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    for (int j = i; j < Grd1.Rows.Count - 1; j++)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 0) == Sm.GetGrdStr(Grd1, j + 1, 0))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Duplicate sector : " + Sm.GetGrdStr(Grd1, j+1, 1));
                            Sm.FocusGrd(Grd1, j + 1, 1);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsDateInvalid()
        {
            string mDocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);
            string mExpiredDt = Sm.Left(Sm.GetDte(DteExpiredDt), 8);

            if (Sm.CompareDtTm(mDocDt, mExpiredDt) == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Expired Tender Date should be bigger than Document Date.");
                DteExpiredDt.Focus();
                return true;
            }

            return false;
        }

        private MySqlCommand SaveTender(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTender ");
            SQL.AppendLine("(DocNo, DocDt, ExpiredDt, ActInd, Status, TenderName, MaterialRequestDocNo, MaterialRequestDNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @ExpiredDt, 'Y', 'O', @TenderName, @MaterialRequestDocNo, @MaterialRequestDNo, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@ExpiredDt", Sm.GetDte(DteExpiredDt));
            Sm.CmParam<String>(ref cm, "@TenderName", MeeTenderName.Text);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", mMRDocNo);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDNo", mMRDNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTenderDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTenderDtl ");
            SQL.AppendLine("(DocNo, DNo, SectorCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @SectorCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SectorCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateMRProcessInd(string TenderDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestDtl ");
            SQL.AppendLine("    Set TenderDocNo = @DocNo, Remark = @Remark, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @MaterialRequestDocNo And DNo = @MaterialRequestDNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TenderDocNo);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", mMRDocNo);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDNo", mMRDNo);
            Sm.CmParam<String>(ref cm, "@Remark", (TenderDocNo.Length > 0) ? string.Concat("[Tender : " + TenderDocNo + "] \r\n", MeeRemark.Text) : MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditTender());

            if (!ChkActInd.Checked)
                cml.Add(UpdateMRProcessInd(string.Empty));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataIncativeAlready() ||
                Sm.IsDteEmpty(DteExpiredDt, "Expired Tender Date") ||
                IsDateInvalid() ||
                (!ChkActInd.Checked && IsDataAlreadyProcessToQt());
        }

        private bool IsDataAlreadyProcessToQt()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.Status='A' And A.CancelInd = 'N' And A.TenderDocNo = '" + TxtDocNo.Text + "' And B.ActInd = 'Y' Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (Sm.IsDataExist(cm))
            {
                string QtDocNos = Sm.GetValue(SQL.ToString());

                Sm.StdMsg(mMsgType.Warning, "This data is already process to vendor quotation : " + QtDocNos + ".");
                return true;
            }

            return false;
        }

        private bool IsDataNotDeactivated()
        {
            if (ChkActInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to deactivate this document.");
                return true;
            }
            return false;
        }

        private bool IsDataIncativeAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblTender ");
            SQL.AppendLine("Where (ActInd='N' Or Status = 'C') And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already inactive or closed.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditTender()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTender Set ");
            SQL.AppendLine("    ExpiredDt = @ExpiredDt, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And ActInd='Y' AND Status='O'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParamDt(ref cm, "@ExpiredDt", Sm.GetDte(DteExpiredDt));
            Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowTender(DocNo);
                ShowTenderDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowTender(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.ActInd, A.TenderName, A.MaterialRequestDocNo, A.MaterialRequestDNo, B.ItCode, C.ItName, B.Qty, B.CurCode, B.EstPrice, A.Remark, ");
            SQL.AppendLine("CASE A.Status When 'O' Then 'Open' When 'C' Then 'Closed' End As Status, A.ExpiredDt ");
            SQL.AppendLine("FROM TblTender A ");
            SQL.AppendLine("INNER JOIN TblMaterialRequestDtl B ON A.MaterialRequestDocNo = B.DocNo And A.MaterialRequestDNo = B.DNo ");
            SQL.AppendLine("INNER JOIN TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("WHERE A.DocNo=@DocNo;");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "ActInd", "Status", "MaterialRequestDocNo", "MaterialRequestDNo", 

                    //6-10
                    "ItCode", "ItName", "Qty", "CurCode", "EstPrice", 
                    
                    //11-13
                    "Remark", "TenderName", "ExpiredDt"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                     TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                     TxtMaterialRequestDocNo.EditValue = Sm.DrStr(dr, c[4]);
                     mMRDocNo = Sm.DrStr(dr, c[4]);
                     mMRDNo = Sm.DrStr(dr, c[5]);
                     TxtItCode.EditValue = Sm.DrStr(dr, c[6]);
                     TxtItName.EditValue = Sm.DrStr(dr, c[7]);
                     TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                     TxtCurCode.EditValue = Sm.DrStr(dr, c[9]);
                     TxtEstPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                     MeeTenderName.EditValue = Sm.DrStr(dr, c[12]);
                     Sm.SetDte(DteExpiredDt, Sm.DrStr(dr, c[13]));
                 }, true
             );
        }

        private void ShowTenderDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.SectorCode, B.SectorName ");
            SQL.AppendLine("From TblTenderDtl A ");
            SQL.AppendLine("Inner Join TblSector B On A.SectorCode = B.SectorCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "SectorCode", "SectorName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mIsPORequestFilterByDept = Sm.GetParameterBoo("IsPORequestFilterByDept");
        }

        private void SetLueSector(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SectorCode As Col1, SectorName As Col2 ");
            SQL.AppendLine("From TblSector ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By SectorName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region events

        #region Button Click

        private void BtnMRDocNo_Click(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmTenderDlg(this));
            }
        }

        #endregion

        #region Misc Control events

        private void LueSector_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSector, new Sm.RefreshLue1(SetLueSector));
        }

        private void LueSector_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueSector_Leave(object sender, EventArgs e)
        {
            if (LueSector.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueSector).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value =
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueSector);
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueSector.GetColumnValue("Col2");
                }
                LueSector.Visible = false;
            }
        }

        private void TxtQty_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtQty, 0);
            }
        }

        private void TxtEstPrice_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtEstPrice, 0);
            }
        }

        #endregion

        #endregion

    }
}
