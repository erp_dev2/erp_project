﻿#region Update
/*
    08/11/2019 [WED/VIR] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptOperationCashflow : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mSQL = string.Empty, mTwoYearsAgo = string.Empty,
            mOneYearAgo = string.Empty, mThisYear = string.Empty,
            mThisMth = string.Empty, mNextMth = string.Empty,
            mMthDesc = string.Empty, mTerminDesc = string.Empty;
        private decimal 
            mAchievementDeviderForOperationCashflow = 0m,
            mRAPDeviderForOperationCashflow = 0m;
        private bool mIsFilterBySite = false;
        private int[] mGrdColButton = { 66, 69, 72, 76, 79, 82, 86, 89, 92, 96, 99, 102 };
        private int[] mGrdFormatDec = { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 
                26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 
                46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 67, 
                70, 73, 74, 77, 80, 83, 84, 87, 90, 93, 94, 97, 100, 103, 104, 105, 106, 107, 108, 
                109, 110, 111, 112 };
        private int[] mGrdColReadOnly = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
                61, 63, 64, 65, 67, 68, 70, 71, 73, 74, 75, 77, 78, 80, 81, 83, 84, 87, 88, 90, 
                91, 93, 94, 95, 97, 98, 100, 101, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112 };

        private string[] mArrMthDesc = { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" };

        #endregion

        #region Constructor

        public FrmRptOperationCashflow(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueSiteCode(ref LueSiteCode);
                SetTimeStampVariable();
                SetGrd();
                SetGrdColumnHdr();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.ReadOnly = false;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 113;
            Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 120;
            Grd1.Header.Cells[0, 1].Value = "Kode Proyek";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Nama Proyek";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 200;
            Grd1.Header.Cells[0, 3].Value = "Wilayah";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 180;
            Grd1.Header.Cells[0, 4].Value = "Waktu Proyek";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Cols[5].Width = 150;
            Grd1.Header.Cells[0, 5].Value = "Kontrak";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Grd1.Cols[6].Width = 150;
            Grd1.Header.Cells[0, 6].Value = "Prestasi";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;

            Grd1.Cols[7].Width = 150;
            //Grd1.Header.Cells[0, 7].Value = "Prestasi " + mTwoYearsAgo;
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;

            Grd1.Cols[8].Width = 150;
            //Grd1.Header.Cells[0, 8].Value = "Prestasi s.d " + mTwoYearsAgo;
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 2;

            Grd1.Cols[9].Width = 150;
            //Grd1.Header.Cells[0, 9].Value = "Prestasi " + mOneYearAgo;
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 9].SpanRows = 2;

            Grd1.Cols[10].Width = 150;
            //Grd1.Header.Cells[0, 10].Value = "Prestasi s.d " + mOneYearAgo;
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 10].SpanRows = 2;

            Grd1.Cols[11].Width = 150;
            //Grd1.Header.Cells[0, 11].Value = "Prestasi " + mThisYear;
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 2;

            Grd1.Cols[12].Width = 150;
            //Grd1.Header.Cells[0, 12].Value = "Prestasi s.d " + mThisYear;
            Grd1.Header.Cells[0, 12].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 12].SpanRows = 2;

            Grd1.Cols[13].Width = 150;
            Grd1.Header.Cells[0, 13].Value = " % ";
            Grd1.Header.Cells[0, 13].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 13].SpanRows = 2;

            Grd1.Header.Cells[1, 14].Value = "Prestasi bulan berjalan";
            Grd1.Header.Cells[1, 14].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 14].SpanCols = 2;
            //Grd1.Header.Cells[0, 14].Value = "Prestasi" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            //Grd1.Header.Cells[0, 15].Value = "Prestasi s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Cols[14].Width = 150;
            Grd1.Cols[15].Width = 150;

            Grd1.Cols[16].Width = 150;
            Grd1.Header.Cells[0, 16].Value = "RAP";
            Grd1.Header.Cells[0, 16].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 16].SpanRows = 2;

            Grd1.Cols[17].Width = 150;
            Grd1.Header.Cells[0, 17].Value = " % ";
            Grd1.Header.Cells[0, 17].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 17].SpanRows = 2;

            Grd1.Cols[18].Width = 150;
            //Grd1.Header.Cells[0, 18].Value = "RAP " + mTwoYearsAgo;
            Grd1.Header.Cells[0, 18].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 18].SpanRows = 2;

            Grd1.Cols[19].Width = 150;
            //Grd1.Header.Cells[0, 19].Value = "RAP s.d" + Environment.NewLine + mTwoYearsAgo;
            Grd1.Header.Cells[0, 19].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 19].SpanRows = 2;

            Grd1.Cols[20].Width = 150;
            //Grd1.Header.Cells[0, 20].Value = "RAP " + mOneYearAgo;
            Grd1.Header.Cells[0, 20].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 20].SpanRows = 2;

            Grd1.Cols[21].Width = 150;
            //Grd1.Header.Cells[0, 21].Value = "RAP s.d" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 21].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 21].SpanRows = 2;

            Grd1.Cols[22].Width = 150;
            Grd1.Header.Cells[0, 22].Value = " % ";
            Grd1.Header.Cells[0, 22].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 22].SpanRows = 2;

            Grd1.Cols[23].Width = 150;
            //Grd1.Header.Cells[0, 23].Value = "RAP " + mThisYear;
            Grd1.Header.Cells[0, 23].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 23].SpanRows = 2;

            Grd1.Cols[24].Width = 150;
            Grd1.Header.Cells[0, 24].Value = " % ";
            Grd1.Header.Cells[0, 24].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 24].SpanRows = 2;

            Grd1.Cols[25].Width = 150;
            //Grd1.Header.Cells[0, 25].Value = "RAP s.d" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 25].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 25].SpanRows = 2;

            Grd1.Cols[26].Width = 150;
            Grd1.Header.Cells[0, 26].Value = " % ";
            Grd1.Header.Cells[0, 26].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 26].SpanRows = 2;

            Grd1.Cols[27].Width = 150;
            //Grd1.Header.Cells[0, 27].Value = "RAP" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 27].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 27].SpanRows = 2;

            Grd1.Cols[28].Width = 150;
            Grd1.Header.Cells[0, 28].Value = " % ";
            Grd1.Header.Cells[0, 28].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 28].SpanRows = 2;

            Grd1.Cols[29].Width = 150;
            //Grd1.Header.Cells[0, 29].Value = "RAP s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 29].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 29].SpanRows = 2;

            Grd1.Header.Cells[1, 30].Value = "Realisasi Prestasi";
            Grd1.Header.Cells[1, 30].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 30].SpanCols = 10;
            //Grd1.Header.Cells[0, 30].Value = mTwoYearsAgo;
            Grd1.Header.Cells[0, 31].Value = " % ";
            //Grd1.Header.Cells[0, 32].Value = mOneYearAgo;
            Grd1.Header.Cells[0, 33].Value = " % ";
            //Grd1.Header.Cells[0, 34].Value = mMthDesc + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 35].Value = " % ";
            Grd1.Header.Cells[0, 36].Value = "Total";
            Grd1.Header.Cells[0, 37].Value = " % ";
            Grd1.Header.Cells[0, 38].Value = "Sisa Prestasi" + Environment.NewLine + "(To Go)";
            Grd1.Header.Cells[0, 39].Value = " % ";
            Grd1.Cols[30].Width = 120;
            Grd1.Cols[31].Width = 120;
            Grd1.Cols[32].Width = 120;
            Grd1.Cols[33].Width = 120;
            Grd1.Cols[34].Width = 120;
            Grd1.Cols[35].Width = 120;
            Grd1.Cols[36].Width = 120;
            Grd1.Cols[37].Width = 120;
            Grd1.Cols[38].Width = 120;
            Grd1.Cols[39].Width = 120;

            Grd1.Header.Cells[1, 40].Value = "Cost To Date";
            Grd1.Header.Cells[1, 40].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 40].SpanCols = 10;
            //Grd1.Header.Cells[0, 40].Value = "Cost To"+ Environment.NewLine +"Date " + mTwoYearsAgo;
            Grd1.Header.Cells[0, 41].Value = " % ";
            //Grd1.Header.Cells[0, 42].Value = "Cost To" + Environment.NewLine + "Date " + mOneYearAgo;
            Grd1.Header.Cells[0, 43].Value = " % ";
            //Grd1.Header.Cells[0, 44].Value = "Cost To" + Environment.NewLine + "Date " + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 45].Value = " % ";
            //Grd1.Header.Cells[0, 46].Value = "Cost To" + Environment.NewLine + "Date s.d " + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 47].Value = " % ";
            Grd1.Header.Cells[0, 48].Value = "Sisa Cost To" + Environment.NewLine + "Date (To Go)";
            Grd1.Header.Cells[0, 49].Value = " % ";
            Grd1.Cols[40].Width = 120;
            Grd1.Cols[41].Width = 120;
            Grd1.Cols[42].Width = 120;
            Grd1.Cols[43].Width = 120;
            Grd1.Cols[44].Width = 120;
            Grd1.Cols[45].Width = 120;
            Grd1.Cols[46].Width = 120;
            Grd1.Cols[47].Width = 120;
            Grd1.Cols[48].Width = 120;
            Grd1.Cols[49].Width = 120;

            Grd1.Header.Cells[1, 50].Value = "Cash Flow";
            Grd1.Header.Cells[1, 50].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 50].SpanCols = 3;
            Grd1.Header.Cells[0, 50].Value = "Inflow";
            Grd1.Header.Cells[0, 51].Value = "Outflow";
            Grd1.Header.Cells[0, 52].Value = "Plus/Minus";
            Grd1.Cols[50].Width = 120;
            Grd1.Cols[51].Width = 120;
            Grd1.Cols[52].Width = 120;

            Grd1.Cols[53].Width = 150;
            Grd1.Header.Cells[0, 53].Value = "BMHD";
            Grd1.Header.Cells[0, 53].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 53].SpanRows = 2;

            Grd1.Cols[54].Width = 150;
            Grd1.Header.Cells[0, 54].Value = "Uang Muka";
            Grd1.Header.Cells[0, 54].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 54].SpanRows = 2;

            Grd1.Cols[55].Width = 150;
            Grd1.Header.Cells[0, 55].Value = "Potongan"+ Environment.NewLine +"Uang Muka";
            Grd1.Header.Cells[0, 55].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 55].SpanRows = 2;

            Grd1.Cols[56].Width = 150;
            //Grd1.Header.Cells[0, 56].Value = "Termin s.d"+ Environment.NewLine + mTerminDesc;
            Grd1.Header.Cells[0, 56].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 56].SpanRows = 2;

            Grd1.Cols[57].Width = 150;
            Grd1.Header.Cells[0, 57].Value = "Total Termin";
            Grd1.Header.Cells[0, 57].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 57].SpanRows = 2;

            Grd1.Cols[58].Width = 150;
            Grd1.Header.Cells[0, 58].Value = " % ";
            Grd1.Header.Cells[0, 58].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 58].SpanRows = 2;

            Grd1.Cols[59].Width = 150;
            //Grd1.Header.Cells[0, 59].Value = "Tagihan Brutto"+ Environment.NewLine + string.Concat(mThisYear, " s.d ", mMthDesc);
            Grd1.Header.Cells[0, 59].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 59].SpanRows = 2;

            Grd1.Cols[60].Width = 150;
            //Grd1.Header.Cells[0, 60].Value = "Total Tagihan Brutto" + Environment.NewLine + "s.d " + mMthDesc;
            Grd1.Header.Cells[0, 60].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 60].SpanRows = 2;

            Grd1.Cols[61].Width = 150;
            //Grd1.Header.Cells[0, 61].Value = "Tagihan Brutto" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 61].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 61].SpanRows = 2;

            Grd1.Cols[62].Width = 150;
            //Grd1.Header.Cells[0, 62].Value = "Tagihan Brutto" + Environment.NewLine + "s.d " + mOneYearAgo;
            Grd1.Header.Cells[0, 62].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 62].SpanRows = 2;

            Grd1.Cols[63].Width = 150;
            //Grd1.Header.Cells[0, 63].Value = "Tagihan Brutto" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 63].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 63].SpanRows = 2;

            Grd1.Cols[64].Width = 150;
            Grd1.Header.Cells[0, 64].Value = "Gaji Januari";
            Grd1.Header.Cells[0, 64].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 64].SpanRows = 2;

            Grd1.Cols[65].Width = 0;
            Grd1.Header.Cells[0, 65].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 65].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 65].SpanRows = 2;

            Grd1.Cols[66].Width = 20;
            Grd1.Header.Cells[0, 66].Value = "";
            Grd1.Header.Cells[0, 66].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 66].SpanRows = 2;

            Grd1.Cols[67].Width = 150;
            Grd1.Header.Cells[0, 67].Value = "Gaji Februari";
            Grd1.Header.Cells[0, 67].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 67].SpanRows = 2;

            Grd1.Cols[68].Width = 0;
            Grd1.Header.Cells[0, 68].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 68].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 68].SpanRows = 2;

            Grd1.Cols[69].Width = 20;
            Grd1.Header.Cells[0, 69].Value = "";
            Grd1.Header.Cells[0, 69].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 69].SpanRows = 2;

            Grd1.Cols[70].Width = 150;
            Grd1.Header.Cells[0, 70].Value = "Gaji Maret";
            Grd1.Header.Cells[0, 70].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 70].SpanRows = 2;

            Grd1.Cols[71].Width = 0;
            Grd1.Header.Cells[0, 71].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 71].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 71].SpanRows = 2;

            Grd1.Cols[72].Width = 20;
            Grd1.Header.Cells[0, 72].Value = "";
            Grd1.Header.Cells[0, 72].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 72].SpanRows = 2;

            Grd1.Cols[73].Width = 150;
            Grd1.Header.Cells[0, 73].Value = "Triwulan I";
            Grd1.Header.Cells[0, 73].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 73].SpanRows = 2;

            Grd1.Cols[74].Width = 150;
            Grd1.Header.Cells[0, 74].Value = "Gaji April";
            Grd1.Header.Cells[0, 74].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 74].SpanRows = 2;

            Grd1.Cols[75].Width = 0;
            Grd1.Header.Cells[0, 75].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 75].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 75].SpanRows = 2;

            Grd1.Cols[76].Width = 20;
            Grd1.Header.Cells[0, 76].Value = "";
            Grd1.Header.Cells[0, 76].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 76].SpanRows = 2;

            Grd1.Cols[77].Width = 150;
            Grd1.Header.Cells[0, 77].Value = "Gaji Mei";
            Grd1.Header.Cells[0, 77].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 77].SpanRows = 2;

            Grd1.Cols[78].Width = 0;
            Grd1.Header.Cells[0, 78].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 78].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 78].SpanRows = 2;

            Grd1.Cols[79].Width = 20;
            Grd1.Header.Cells[0, 79].Value = "";
            Grd1.Header.Cells[0, 79].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 79].SpanRows = 2;

            Grd1.Cols[80].Width = 150;
            Grd1.Header.Cells[0, 80].Value = "Gaji Juni";
            Grd1.Header.Cells[0, 80].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 80].SpanRows = 2;

            Grd1.Cols[81].Width = 0;
            Grd1.Header.Cells[0, 81].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 81].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 81].SpanRows = 2;

            Grd1.Cols[82].Width = 20;
            Grd1.Header.Cells[0, 82].Value = "";
            Grd1.Header.Cells[0, 82].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 82].SpanRows = 2;

            Grd1.Cols[83].Width = 150;
            Grd1.Header.Cells[0, 83].Value = "Triwulan II";
            Grd1.Header.Cells[0, 83].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 83].SpanRows = 2;

            Grd1.Cols[84].Width = 150;
            Grd1.Header.Cells[0, 84].Value = "Gaji Juli";
            Grd1.Header.Cells[0, 84].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 84].SpanRows = 2;

            Grd1.Cols[85].Width = 0;
            Grd1.Header.Cells[0, 85].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 85].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 85].SpanRows = 2;

            Grd1.Cols[86].Width = 20;
            Grd1.Header.Cells[0, 86].Value = "";
            Grd1.Header.Cells[0, 86].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 86].SpanRows = 2;

            Grd1.Cols[87].Width = 150;
            Grd1.Header.Cells[0, 87].Value = "Gaji Agustus";
            Grd1.Header.Cells[0, 87].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 87].SpanRows = 2;

            Grd1.Cols[88].Width = 0;
            Grd1.Header.Cells[0, 88].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 88].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 88].SpanRows = 2;

            Grd1.Cols[89].Width = 20;
            Grd1.Header.Cells[0, 89].Value = "";
            Grd1.Header.Cells[0, 89].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 89].SpanRows = 2;

            Grd1.Cols[90].Width = 150;
            Grd1.Header.Cells[0, 90].Value = "Gaji September";
            Grd1.Header.Cells[0, 90].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 90].SpanRows = 2;

            Grd1.Cols[91].Width = 0;
            Grd1.Header.Cells[0, 91].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 91].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 91].SpanRows = 2;

            Grd1.Cols[92].Width = 20;
            Grd1.Header.Cells[0, 92].Value = "";
            Grd1.Header.Cells[0, 92].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 92].SpanRows = 2;

            Grd1.Cols[93].Width = 150;
            Grd1.Header.Cells[0, 93].Value = "Triwulan III";
            Grd1.Header.Cells[0, 93].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 93].SpanRows = 2;

            Grd1.Cols[94].Width = 150;
            Grd1.Header.Cells[0, 94].Value = "Gaji Oktober";
            Grd1.Header.Cells[0, 94].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 94].SpanRows = 2;

            Grd1.Cols[95].Width = 0;
            Grd1.Header.Cells[0, 95].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 95].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 95].SpanRows = 2;

            Grd1.Cols[96].Width = 20;
            Grd1.Header.Cells[0, 96].Value = "";
            Grd1.Header.Cells[0, 96].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 96].SpanRows = 2;

            Grd1.Cols[97].Width = 150;
            Grd1.Header.Cells[0, 97].Value = "Gaji November";
            Grd1.Header.Cells[0, 97].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 97].SpanRows = 2;

            Grd1.Cols[98].Width = 0;
            Grd1.Header.Cells[0, 98].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 98].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 98].SpanRows = 2;

            Grd1.Cols[99].Width = 20;
            Grd1.Header.Cells[0, 99].Value = "";
            Grd1.Header.Cells[0, 99].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 99].SpanRows = 2;

            Grd1.Cols[100].Width = 150;
            Grd1.Header.Cells[0, 100].Value = "Gaji Desember";
            Grd1.Header.Cells[0, 100].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 100].SpanRows = 2;

            Grd1.Cols[101].Width = 0;
            Grd1.Header.Cells[0, 101].Value = "Dropping Request#";
            Grd1.Header.Cells[0, 101].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 101].SpanRows = 2;

            Grd1.Cols[102].Width = 20;
            Grd1.Header.Cells[0, 102].Value = "";
            Grd1.Header.Cells[0, 102].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 102].SpanRows = 2;

            Grd1.Cols[103].Width = 150;
            Grd1.Header.Cells[0, 103].Value = "Triwulan IV";
            Grd1.Header.Cells[0, 103].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 103].SpanRows = 2;

            Grd1.Cols[104].Width = 150;
            Grd1.Header.Cells[0, 104].Value = "Total Gaji s.d" + Environment.NewLine + "saat ini";
            Grd1.Header.Cells[0, 104].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 104].SpanRows = 2;

            Grd1.Header.Cells[1, 105].Value = "Terhadap Pengajuan";
            Grd1.Header.Cells[1, 105].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 105].SpanCols = 4;
            //Grd1.Header.Cells[0, 105].Value = "Sisa RAP s.d" + Environment.NewLine + mOneYearAgo;
            //Grd1.Header.Cells[0, 106].Value = "Sisa RAP s.d" + Environment.NewLine + mThisYear;
            //Grd1.Header.Cells[0, 107].Value = "Sisa RAP s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 108].Value = "Sisa RAP" + Environment.NewLine + "Total";
            Grd1.Cols[105].Width = 120;
            Grd1.Cols[106].Width = 120;
            Grd1.Cols[107].Width = 120;
            Grd1.Cols[108].Width = 120;

            Grd1.Header.Cells[1, 109].Value = "Terhadap Realisasi (Outflow)";
            Grd1.Header.Cells[1, 109].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 109].SpanCols = 4;
            //Grd1.Header.Cells[0, 109].Value = "Sisa RAP s.d" + Environment.NewLine + mOneYearAgo;
            //Grd1.Header.Cells[0, 110].Value = "Sisa RAP s.d" + Environment.NewLine + mTwoYearsAgo;
            //Grd1.Header.Cells[0, 111].Value = "Sisa RAP s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 112].Value = "Sisa RAP" + Environment.NewLine + "Total";
            Grd1.Cols[109].Width = 120;
            Grd1.Cols[110].Width = 120;
            Grd1.Cols[111].Width = 120;
            Grd1.Cols[112].Width = 120;

            Sm.GrdFormatDec(Grd1, mGrdFormatDec, 0);
            Sm.GrdColReadOnly(true, false, Grd1, mGrdColReadOnly);
            Sm.GrdColButton(Grd1, mGrdColButton);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        #region SQL

        private string SQL1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT IFNULL(C.ProjectCode2, C.ProjectCode) ProjectCode, A.ProjectName, F.SiteName, ");
            SQL.AppendLine("If(A.StartDt IS NULL, '',  ");
            SQL.AppendLine("CONCAT( ");
            SQL.AppendLine("IFNULL(Date_Format(A.StartDt, '%d %b %Y'), ''), If(A.EndDt is NULL, '', CONCAT(' - ', DATE_FORMAT(A.EndDt, '%d %b %Y'))), ");
            SQL.AppendLine("' (', TIMESTAMPDIFF(MONTH, A.StartDt, A.EndDt), ' bulan)' ");
            SQL.AppendLine(") ");
            SQL.AppendLine(") AS Period, D.Amt ContractAmt, (D.Amt / (@AchievementDeviderForOperationCashflow / 100)) AS AchTotal, IFNULL(E.DocNo, '') AS PRJIDocNo, ");
            SQL.AppendLine("IFNULL(E.Achievement, 0) AS AchPercentage, IFNULL(E.TotalResource, 0) AS RAPAmt ");
            SQL.AppendLine("FROM TblLOPHdr A ");
            SQL.AppendLine("INNER JOIN TblBOQHdr B ON A.DocNo = B.LOPDocNo ");
            SQL.AppendLine("    AND A.CancelInd = 'N' AND A.Status = 'A' ");
            SQL.AppendLine("    AND B.ActInd = 'Y' AND B.Status = 'A' ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=A.SiteCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (Sm.GetLue(LueSiteCode).Length > 0)
                SQL.AppendLine("    And A.SiteCode = @SiteCode ");
            SQL.AppendLine("INNER JOIN TblSOContractHdr C ON B.DocNo = C.BOQDocNo ");
            SQL.AppendLine("    AND C.CancelInd = 'N' AND C.Status = 'A' ");
            if (TxtProjectCode.Text.Length > 0)
                SQL.AppendLine("    And (C.ProjectCode2 Like @ProjectCode Or C.ProjectCode Like @ProjectCode Or A.ProjectName Like @ProjectCode) ");
            SQL.AppendLine("INNER JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T1.SOCRDocNo, T1.SOCDocNo, T3.Amt ");
            SQL.AppendLine("    FROM ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        SELECT SOCDocNo, MAX(DocNo) SOCRDocNo  ");
		    SQL.AppendLine("         FROM TblSOContractRevisionHdr  ");
		    SQL.AppendLine("         GROUP BY SOCDocNo ");
	        SQL.AppendLine("     ) T1 ");
	        SQL.AppendLine("     INNER JOIN TblSOContractHdr T2 ON T1.SOCDocNo = T2.DocNo AND T2.CancelInd = 'N' AND T2.Status = 'A' ");
	        SQL.AppendLine("     INNER JOIN TblSOContractRevisionHdr T3 ON T1.SOCRDocNo = T3.DocNo ");
            SQL.AppendLine(") D ON C.DocNo = D.SOCDocNo ");
            SQL.AppendLine("LEFT JOIN TblProjectImplementationHdr E ON D.SOCRDocNo = E.SOContractDocNo ");
            SQL.AppendLine("    AND E.CancelInd = 'N' AND E.Status = 'A' ");
            SQL.AppendLine("LEFT JOIN TblSite F ON A.SiteCode = F.SiteCode ");

            return SQL.ToString();
        }

        private string SQL2(byte DocType)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, LEFT(A.SettleDt, 6) SettleDt, SUM(A.SettledAmt) SettledAmt ");
            SQL.AppendLine("FROM TblProjectImplementationDtl A ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.DocNo, @PRJIDocNo) ");
            SQL.AppendLine("AND A.SettledInd = 'Y' AND A.SettleDt IS NOT NULL ");
            if (DocType == 1)
                SQL.AppendLine("And A.InvoicedInd = 'N' ");
            SQL.AppendLine("GROUP BY A.DocNo, LEFT(A.SettleDt, 6) ");
            SQL.AppendLine("ORDER BY A.DocNo, LEFT(A.SettleDt, 6); ");

            return SQL.ToString();
        }

        private string SQL3()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.PRJIDocNo, Sum(A.Downpayment) DPAmt, LEFT(E.DocDt, 6) DocDt, Sum(IfNull(E.Amt, 0.00)) VCAmt ");
            SQL.AppendLine("FROM TblSalesInvoice5Hdr A ");
            SQL.AppendLine("INNER JOIN  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct DocNo, ProjectImplementationDocNo PRJIDocNo ");
	        SQL.AppendLine("     From TblSalesInvoice5Dtl ");
            SQL.AppendLine("    WHERE FIND_IN_SET(ProjectImplementationDocNo, @PRJIDocNo) ");
            SQL.AppendLine(") B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
            SQL.AppendLine("LEFT JOIN TblIncomingPaymentDtl C ON A.DocNo = C.InvoiceDocNo ");
            SQL.AppendLine("LEFT JOIN TblIncomingPaymentHdr D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("LEFT JOIN TblVoucherHdr E ON D.VoucherRequestDocNo = E.VoucherRequestDocNo AND E.CancelInd = 'N' ");
            SQL.AppendLine("GROUP BY B.PRJIDocNo, LEFT(E.DocDt, 6); ");

            return SQL.ToString();
        }

        private string SQL4()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.PRJIDocNo, LEFT(C.DocDt, 6) DocDt, Group_Concat(Distinct A.DocNo) DRQDocNo, SUM(C.Amt) Amt ");
            SQL.AppendLine("FROM TblDroppingRequestHdr A ");
            SQL.AppendLine("INNER JOIN TblDroppingPaymentHdr B ON A.DocNo = B.DRQDocNo ");
            SQL.AppendLine("    AND FIND_IN_SET(A.PRJIDocNo, @PRJIDocNo) ");
            SQL.AppendLine("INNER JOIN TblVoucherHdr C ON B.VoucherRequestDocNo = C.VoucherRequestDocNo AND C.CancelInd = 'N' ");
            SQL.AppendLine("GROUP BY A.PRJIDocNo, LEFT(C.DocDt, 6); ");

            return SQL.ToString();
        }

        private string SQL5()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.PRJIDocNo, LEFT(A.DocDt, 6) DocDt, SUM(A.Amt) Amt ");
            SQL.AppendLine("FROM TblDroppingRequestHdr A ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.PRJIDocNo, @PRJIDocNo) ");
            SQL.AppendLine("AND A.Status = 'A' AND A.CancelInd = 'N' ");
            SQL.AppendLine("GROUP BY A.PRJIDocNo, LEFT(A.DocDt, 6); ");

            return SQL.ToString();
        }

        private string SQL6()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo PRJIDocNo, SUM(D.Amt) Amt ");
            SQL.AppendLine("FROM TblProjectImplementationHdr A ");
            SQL.AppendLine("INNER JOIN TblSOContractRevisionHdr B ON A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("    AND FIND_IN_SET(A.DocNo, @PRJIDocNo) ");
            SQL.AppendLine("INNER JOIN TblARDownpayment C ON B.SOCDocNo = C.SODocNo ");
            SQL.AppendLine("INNER JOIN TblVoucherHdr D ON C.VoucherRequestDocNo = D.DocNo ");
            SQL.AppendLine("GROUP BY A.DocNo; ");

            return SQL.ToString();
        }

        #endregion

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                SetTimeStampVariable();
                SetGrdColumnHdr();
                string mPRJIDocNo = string.Empty;

                var l = new List<OperationCashFlow>();
                var l2 = new List<Achievement>();
                var l3 = new List<SalesInvoice>();
                var l4 = new List<DroppingPayment>();
                var l5 = new List<DroppingRequest>();
                var l6 = new List<ARDP>();
                var l7 = new List<SettledUninvoiced>();

                Process1(ref l);
                if (l.Count > 0)
                {
                    for (int i = 0; i < l.Count; ++i)
                    {
                        if (l[i].PRJIDocNo.Length > 0)
                        {
                            if (mPRJIDocNo.Length > 0) mPRJIDocNo += ",";
                            mPRJIDocNo += l[i].PRJIDocNo;
                        }
                    }

                    if (mPRJIDocNo.Length > 0)
                    {
                        Process2(ref l2, mPRJIDocNo);
                        Process3(ref l3, mPRJIDocNo);
                        Process4(ref l4, mPRJIDocNo);
                        Process5(ref l5, mPRJIDocNo);
                        Process6(ref l6, mPRJIDocNo);
                        Process7(ref l7, mPRJIDocNo);
                    }

                    if (l2.Count > 0) Process2_1(ref l, ref l2);
                    if (l3.Count > 0) Process3_1(ref l, ref l3);
                    if (l4.Count > 0) Process4_1(ref l, ref l4);
                    if (l6.Count > 0) Process6_1(ref l, ref l6);
                    ProcessPlusMinus(ref l);
                    if (l7.Count > 0) Process7_1(ref l, ref l7);
                    if (l5.Count > 0) Process5_1(ref l, ref l5);

                    Process98(ref l); // RAP, Realisasi Prestasi, Cost To Date
                    Process99(ref l);
                }
                else Sm.StdMsg(mMsgType.NoData, string.Empty);

                Grd1.GroupObject.Add(3);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, mGrdFormatDec);

                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear(); l5.Clear(); l6.Clear(); l7.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(mGrdColButton, e.ColIndex))
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, (e.ColIndex - 1)).Length > 0)
                {
                    string[] s = Sm.GetGrdStr(Grd1, e.RowIndex, (e.ColIndex - 1)).Split(',');
                    foreach (string d in s)
                    {
                        var f = new FrmDroppingRequest(mMenuCode);
                        f.Tag = mMenuCode;
                        f.Text = Sm.GetMenuDesc("FrmDroppingRequest");
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = d;
                        f.ShowDialog();
                    }
                }
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mAchievementDeviderForOperationCashflow = Sm.GetParameterDec("AchievementDeviderForOperationCashflow");
            mRAPDeviderForOperationCashflow = Sm.GetParameterDec("RAPDeviderForOperationCashflow");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void SetTimeStampVariable()
        {
            mThisMth = string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth));
            if (Sm.GetLue(LueMth) == "12") mNextMth = string.Concat((Int32.Parse(Sm.GetLue(LueYr)) + 1).ToString(), "01");
            else mNextMth = string.Concat(Sm.GetLue(LueYr), Sm.Right(string.Concat("00", (Int32.Parse(Sm.GetLue(LueMth)) + 1).ToString()), 2));
            mThisYear = Sm.GetLue(LueYr);
            mOneYearAgo = (Int32.Parse(mThisYear) - 1).ToString();
            mTwoYearsAgo = (Int32.Parse(mThisYear) - 2).ToString();
            mMthDesc = mArrMthDesc[Int32.Parse(Sm.GetLue(LueMth)) - 1];
            if (Sm.GetLue(LueMth) == "01") mTerminDesc = string.Concat(mArrMthDesc[11], " ", mOneYearAgo);
            else mTerminDesc = string.Concat(mArrMthDesc[Int32.Parse(Sm.GetLue(LueMth)) - 2], " ", mThisYear);
        }

        private void SetGrdColumnHdr()
        {
            Grd1.Header.Cells[0, 7].Value = "Prestasi " + mTwoYearsAgo;
            Grd1.Header.Cells[0, 8].Value = "Prestasi s.d " + mTwoYearsAgo;
            Grd1.Header.Cells[0, 9].Value = "Prestasi " + mOneYearAgo;
            Grd1.Header.Cells[0, 10].Value = "Prestasi s.d " + mOneYearAgo;
            Grd1.Header.Cells[0, 11].Value = "Prestasi " + mThisYear;
            Grd1.Header.Cells[0, 12].Value = "Prestasi s.d " + mThisYear;
            Grd1.Header.Cells[0, 14].Value = "Prestasi" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 15].Value = "Prestasi s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 18].Value = "RAP " + mTwoYearsAgo;
            Grd1.Header.Cells[0, 19].Value = "RAP s.d" + Environment.NewLine + mTwoYearsAgo;
            Grd1.Header.Cells[0, 20].Value = "RAP " + mOneYearAgo;
            Grd1.Header.Cells[0, 21].Value = "RAP s.d" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 23].Value = "RAP " + mThisYear;
            Grd1.Header.Cells[0, 25].Value = "RAP s.d" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 27].Value = "RAP" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 29].Value = "RAP s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 30].Value = mTwoYearsAgo;
            Grd1.Header.Cells[0, 32].Value = mOneYearAgo;
            Grd1.Header.Cells[0, 34].Value = mMthDesc + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 40].Value = "Cost To" + Environment.NewLine + "Date " + mTwoYearsAgo;
            Grd1.Header.Cells[0, 42].Value = "Cost To" + Environment.NewLine + "Date " + mOneYearAgo;
            Grd1.Header.Cells[0, 44].Value = "Cost To" + Environment.NewLine + "Date " + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 46].Value = "Cost To" + Environment.NewLine + "Date s.d " + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 56].Value = "Termin s.d" + Environment.NewLine + mTerminDesc;
            Grd1.Header.Cells[0, 59].Value = "Tagihan Brutto" + Environment.NewLine + string.Concat(mThisYear, " s.d ", mMthDesc);
            Grd1.Header.Cells[0, 60].Value = "Total Tagihan Brutto" + Environment.NewLine + "s.d " + mMthDesc;
            Grd1.Header.Cells[0, 61].Value = "Tagihan Brutto" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 62].Value = "Tagihan Brutto" + Environment.NewLine + "s.d " + mOneYearAgo;
            Grd1.Header.Cells[0, 63].Value = "Tagihan Brutto" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 105].Value = "Sisa RAP s.d" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 106].Value = "Sisa RAP s.d" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 107].Value = "Sisa RAP s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 109].Value = "Sisa RAP s.d" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 110].Value = "Sisa RAP s.d" + Environment.NewLine + mTwoYearsAgo;
            Grd1.Header.Cells[0, 111].Value = "Sisa RAP s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
        }

        #region Process

        private void Process1(ref List<OperationCashFlow> l)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@ProjectCode", string.Concat("%", TxtProjectCode.Text, "%"));
                Sm.CmParam<Decimal>(ref cm, "@AchievementDeviderForOperationCashflow", mAchievementDeviderForOperationCashflow);

                cm.CommandText = SQL1();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "ProjectCode", 
                    "ProjectName", "SiteName", "Period", "ContractAmt", "AchTotal", 
                    "PRJIDocNo", "AchPercentage", "RAPAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OperationCashFlow()
                        {
                            ProjectCode = Sm.DrStr(dr, c[0]),
                            ProjectName = Sm.DrStr(dr, c[1]),
                            SiteName = Sm.DrStr(dr, c[2]),
                            Period = Sm.DrStr(dr, c[3]),
                            ContractAmt = Sm.DrDec(dr, c[4]),
                            AchTotal = Sm.DrDec(dr, c[5]),
                            PRJIDocNo = Sm.DrStr(dr, c[6]),
                            Ach2 = 0m,
                            AchTo2 = 0m,
                            Ach1 = 0m,
                            AchTo1 = 0m,
                            AchNow = 0m,
                            AchToNow = 0m,
                            AchPercentage = Sm.DrDec(dr, c[7]),
                            AchMth = 0m,
                            AchToMth = 0m,
                            RAPAmt = Sm.DrDec(dr, c[8]),
                            RAPPercentage = Sm.DrDec(dr, c[5]) == 0m ? 0m : ((Sm.DrDec(dr, c[8]) / Sm.DrDec(dr, c[5])) * (mRAPDeviderForOperationCashflow / 100)) * 100,
                            RAP2 = 0m,
                            RAPTo2 = 0m,
                            RAP1 = 0m,
                            RAPTo1 = 0m,
                            RAPTo1Percentage = 0m,
                            RAPNow = 0m,
                            RAPNowPercentage = 0m,
                            RAPToNow = 0m,
                            RAPToNowPercentage = 0m,
                            RAPMth = 0m,
                            RAPMthPercentage = 0m,
                            RAPToMth = 0m,
                            R2 = 0m,
                            R2Percentage = 0m,
                            R1 = 0m,
                            R1Percentage = 0m,
                            RMth = 0m,
                            RMthPercentage = 0m,
                            RTotal = 0m,
                            RTotalPercentage = 0m,
                            RToGo = 0m,
                            RToGoPercentage = 0m,
                            CD2 = 0m,
                            CD2Percentage = 0m,
                            CD1 = 0m,
                            CD1Percentage = 0m,
                            CDMth = 0m,
                            CDMthPercentage = 0m,
                            CDToMth = 0m,
                            CDToMthPercentage = 0m,
                            CDToGo = 0m,
                            CDToGoPercentage = 0m,
                            Inflow = 0m,
                            Outflow = 0m,
                            PlusMinus = 0m,
                            BMHD = 0m,
                            DPAmt = 0m,
                            DPDiscountAmt = 0m,
                            TerminToNow = 0m,
                            TotalTermin = 0m,
                            TotalTerminPercentage = 0m,
                            BruttoToMth = 0m,
                            TotalBruttoToNow = 0m,
                            Brutto1 = 0m,
                            BruttoTo1 = 0m,
                            BruttoNow = 0m,
                            Salary1 = 0m,
                            DRQDocNo1 = string.Empty,
                            Salary2 = 0m,
                            DRQDocNo2 = string.Empty,
                            Salary3 = 0m,
                            DRQDocNo3 = string.Empty,
                            Q1 = 0m,
                            Salary4 = 0m,
                            DRQDocNo4 = string.Empty,
                            Salary5 = 0m,
                            DRQDocNo5 = string.Empty,
                            Salary6 = 0m,
                            DRQDocNo6 = string.Empty,
                            Q2 = 0m,
                            Salary7 = 0m,
                            DRQDocNo7 = string.Empty,
                            Salary8 = 0m,
                            DRQDocNo8 = string.Empty,
                            Salary9 = 0m,
                            DRQDocNo9 = string.Empty,
                            Q3 = 0m,
                            Salary10 = 0m,
                            DRQDocNo10 = string.Empty,
                            Salary11 = 0m,
                            DRQDocNo11 = string.Empty,
                            Salary12 = 0m,
                            DRQDocNo12 = string.Empty,
                            Q4 = 0m,
                            TotalSalary = 0m,
                            IORAP1 = Sm.DrDec(dr, c[8]),
                            IORAPNow = Sm.DrDec(dr, c[8]),
                            IORAPMth = Sm.DrDec(dr, c[8]),
                            IORAPTotal = Sm.DrDec(dr, c[8]),
                            OORAP1 = Sm.DrDec(dr, c[8]),
                            OORAPNow = Sm.DrDec(dr, c[8]),
                            OORAPMth = Sm.DrDec(dr, c[8]),
                            OORAPTotal = Sm.DrDec(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Achievement> l2, string PRJIDocNo)
        {
            var cm = new MySqlCommand();            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);

                cm.CommandText = SQL2(0);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocNo", "SettleDt", "SettledAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Achievement()
                        {
                            PRJIDocNo = Sm.DrStr(dr, c[0]),
                            SettleDt = Sm.DrStr(dr, c[1]),
                            SettledAmt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<SalesInvoice> l3, string PRJIDocNo)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);

                cm.CommandText = SQL3();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "PRJIDocNo", "DocDt", "DPAmt", "VCAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new SalesInvoice()
                        {
                            PRJIDocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            DPAmt = Sm.DrDec(dr, c[2]),
                            VCAmt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<DroppingPayment> l4, string PRJIDocNo)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);

                cm.CommandText = SQL4();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "PRJIDocNo", "DocDt", "DRQDocNo", "Amt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new DroppingPayment()
                        {
                            PRJIDocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            DRQDocNo = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process5(ref List<DroppingRequest> l5, string PRJIDocNo)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);

                cm.CommandText = SQL5();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "PRJIDocNo", "DocDt", "Amt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l5.Add(new DroppingRequest()
                        {
                            PRJIDocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref List<ARDP> l6, string PRJIDocNo)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);

                cm.CommandText = SQL6();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "PRJIDocNo", "Amt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l6.Add(new ARDP()
                        {
                            PRJIDocNo = Sm.DrStr(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process7(ref List<SettledUninvoiced> l7, string PRJIDocNo)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);

                cm.CommandText = SQL2(1);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocNo", "SettleDt", "SettledAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l7.Add(new SettledUninvoiced()
                        {
                            PRJIDocNo = Sm.DrStr(dr, c[0]),
                            SettleDt = Sm.DrStr(dr, c[1]),
                            SettledAmt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2_1(ref List<OperationCashFlow> l, ref List<Achievement> l2)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = 0; j < l2.Count; ++j)
                {
                    if (l[i].PRJIDocNo == l2[j].PRJIDocNo)
                    {
                        //s.d dua tahun lalu
                        if (Int32.Parse(Sm.Left(l2[j].SettleDt, 4)) < Int32.Parse(mOneYearAgo))
                            l[i].AchTo2 += l2[j].SettledAmt;

                        //s.d tahun lalu
                        if (Int32.Parse(Sm.Left(l2[j].SettleDt, 4)) < Int32.Parse(mThisYear))
                            l[i].AchTo1 += l2[j].SettledAmt;

                        //s.d tahun berjalan
                        if (Int32.Parse(Sm.Left(l2[j].SettleDt, 4)) < (Int32.Parse(mThisYear) + 1))
                            l[i].AchToNow += l2[j].SettledAmt;

                        //s.d bulan berjalan
                        if(Int32.Parse(l2[j].SettleDt) < Int32.Parse(mNextMth))
                            l[i].AchToMth += l2[j].SettledAmt;

                        //dua tahun lalu
                        if (Int32.Parse(Sm.Left(l2[j].SettleDt, 4)) == Int32.Parse(mTwoYearsAgo))
                            l[i].Ach2 += l2[j].SettledAmt;

                        //tahun lalu
                        if (Int32.Parse(Sm.Left(l2[j].SettleDt, 4)) == Int32.Parse(mOneYearAgo))
                            l[i].Ach1 += l2[j].SettledAmt;

                        //tahun ini
                        if (Int32.Parse(Sm.Left(l2[j].SettleDt, 4)) == Int32.Parse(mThisYear))
                            l[i].AchNow += l2[j].SettledAmt;

                        //tahun bulan berjalan
                        if (Int32.Parse(l2[j].SettleDt) == Int32.Parse(mThisMth))
                            l[i].AchMth += l2[j].SettledAmt;
                    }
                }
            }
        }

        private void Process3_1(ref List<OperationCashFlow> l, ref List<SalesInvoice> l3)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = 0; j < l3.Count; ++j)
                {
                    if (l[i].PRJIDocNo == l3[j].PRJIDocNo)
                    {
                        l[i].Inflow += l3[j].VCAmt;
                        l[i].DPDiscountAmt += l3[j].DPAmt;
                        l[i].TotalTermin += l3[j].VCAmt;

                        if (l3[j].DocDt.Length > 0)
                        {
                            if (Sm.Left(l3[j].DocDt, 4) == mThisYear)
                            {
                                if (Int32.Parse(Sm.Left(l3[j].DocDt, 6)) < Int32.Parse(mThisMth))
                                    l[i].TerminToNow += l3[j].VCAmt;
                            }
                        }
                    }
                }
            }
        }

        private void Process4_1(ref List<OperationCashFlow> l, ref List<DroppingPayment> l4)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = 0; j < l4.Count; ++j)
                {
                    if (l[i].PRJIDocNo == l4[j].PRJIDocNo)
                    {
                        // Outflow
                        l[i].Outflow += l4[j].Amt;

                        // Terhadap Realisasi
                        l[i].OORAPTotal -= l4[j].Amt;

                        if (l4[j].DocDt.Length > 0)
                        {
                            if (Sm.Left(l4[j].DocDt, 4) == mOneYearAgo)
                                l[i].OORAP1 -= l4[j].Amt;

                            if (Sm.Left(l4[j].DocDt, 4) == mThisYear)
                                l[i].OORAPNow -= l4[j].Amt;

                            if (Int32.Parse(Sm.Left(l4[j].DocDt, 6)) < Int32.Parse(mNextMth))
                                l[i].OORAPMth -= l4[j].Amt;

                            // Salary
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "01"))
                            {
                                l[i].DRQDocNo1 = l4[j].DRQDocNo;
                                l[i].Salary1 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "02"))
                            {
                                l[i].DRQDocNo2 = l4[j].DRQDocNo;
                                l[i].Salary2 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "03"))
                            {
                                l[i].DRQDocNo3 = l4[j].DRQDocNo;
                                l[i].Salary3 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "04"))
                            {
                                l[i].DRQDocNo4 = l4[j].DRQDocNo;
                                l[i].Salary4 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "05"))
                            {
                                l[i].DRQDocNo5 = l4[j].DRQDocNo;
                                l[i].Salary5 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "06"))
                            {
                                l[i].DRQDocNo6 = l4[j].DRQDocNo;
                                l[i].Salary6 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "07"))
                            {
                                l[i].DRQDocNo7 = l4[j].DRQDocNo;
                                l[i].Salary7 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "08"))
                            {
                                l[i].DRQDocNo8 = l4[j].DRQDocNo;
                                l[i].Salary8 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "09"))
                            {
                                l[i].DRQDocNo9 = l4[j].DRQDocNo;
                                l[i].Salary9 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "10"))
                            {
                                l[i].DRQDocNo10 = l4[j].DRQDocNo;
                                l[i].Salary10 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "11"))
                            {
                                l[i].DRQDocNo11 = l4[j].DRQDocNo;
                                l[i].Salary11 = l4[j].Amt;
                            }
                            if (Sm.Left(l4[j].DocDt, 6) == string.Concat(mThisYear, "12"))
                            {
                                l[i].DRQDocNo12 = l4[j].DRQDocNo;
                                l[i].Salary12 = l4[j].Amt;
                            }
                        }
                    }
                }
            }
        }

        private void Process6_1(ref List<OperationCashFlow> l, ref List<ARDP> l6)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = 0; j < l6.Count; ++j)
                {
                    if (l[i].PRJIDocNo == l6[j].PRJIDocNo)
                    {
                        l[i].Inflow += l6[j].Amt;
                        l[i].DPAmt += l6[i].Amt;
                    }
                }
            }
        }

        private void ProcessPlusMinus(ref List<OperationCashFlow> l)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                l[i].PlusMinus = l[i].Inflow - l[i].Outflow;
            }
        }

        private void Process7_1(ref List<OperationCashFlow> l, ref List<SettledUninvoiced> l7)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = 0; j < l7.Count; ++j)
                {
                    if (l[i].PRJIDocNo == l7[j].PRJIDocNo)
                    {
                        if (Sm.Left(l7[j].SettleDt, 4) == mThisYear)
                        {
                            if (Int32.Parse(Sm.Left(l7[j].SettleDt, 6)) < Int32.Parse(mThisMth))
                            {
                                l[i].BruttoToMth += l7[j].SettledAmt;
                            }
                            l[i].BruttoNow += l7[j].SettledAmt;
                        }

                        if (Int32.Parse(Sm.Left(l7[j].SettleDt, 6)) < Int32.Parse(mNextMth))
                        {
                            l[i].TotalBruttoToNow += l7[j].SettledAmt;
                        }

                        if (Sm.Left(l7[j].SettleDt, 4) == mOneYearAgo)
                        {
                            l[i].Brutto1 += l7[j].SettledAmt;
                        }

                        if (Int32.Parse(Sm.Left(l7[j].SettleDt, 4)) < Int32.Parse(mThisYear))
                        {
                            l[i].BruttoTo1 += l7[j].SettledAmt;
                        }
                    }
                }
            }
        }

        private void Process5_1(ref List<OperationCashFlow> l, ref List<DroppingRequest> l5)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = 0; j < l5.Count; ++j)
                {
                    if (l[i].PRJIDocNo == l5[j].PRJIDocNo)
                    {
                        l[i].IORAPTotal -= l5[j].Amt;
                        if (l5[j].DocDt.Length > 0)
                        {
                            if (Sm.Left(l5[j].DocDt, 4) == mOneYearAgo) l[i].IORAP1 -= l5[j].Amt;
                            if (Sm.Left(l5[j].DocDt, 4) == mThisYear) l[i].IORAPNow -= l5[j].Amt;
                            if (Int32.Parse(Sm.Left(l5[j].DocDt, 6)) < Int32.Parse(mNextMth)) l[i].IORAPMth -= l5[j].Amt;
                        }
                    }
                }
            }
        }

        private void Process98(ref List<OperationCashFlow> l) // RAP, Realisasi Prestasi, Cost To Date
        {
            for (int i = 0; i < l.Count; ++i)
            {
                // RAP
                if (l[i].RAPPercentage != 0)
                {
                    l[i].RAP2 = l[i].Ach2 * l[i].RAPPercentage;
                    l[i].RAPTo2 = l[i].AchTo2 * l[i].RAPPercentage;
                    l[i].RAP1 = l[i].Ach1 * l[i].RAPPercentage;
                    l[i].RAPTo1 = l[i].AchTo1 * l[i].RAPPercentage;
                    l[i].RAPNow = l[i].AchNow * l[i].RAPPercentage;
                    l[i].RAPToNow = l[i].AchToNow * l[i].RAPPercentage;
                    l[i].RAPMth = l[i].AchMth * l[i].RAPPercentage;
                    l[i].RAPToMth = l[i].AchToMth * l[i].RAPPercentage;
                }

                if (l[i].AchTotal != 0)
                {
                    l[i].RAPTo1Percentage = (l[i].RAPTo1 / l[i].AchTotal) * 100;
                    l[i].RAPToNowPercentage = (l[i].RAPToNow / l[i].AchTotal) * 100;
                }

                if (l[i].AchMth != 0) l[i].RAPMthPercentage = (l[i].RAPMth / l[i].AchMth) * 100;
                if (l[i].AchNow != 0) l[i].RAPNowPercentage = (l[i].RAPNow / l[i].AchNow) * 100;

                //Realisasi Prestasi
                l[i].R2 = l[i].AchTo2;
                l[i].R1 = l[i].AchTo1;
                l[i].RMth = l[i].AchMth;
                l[i].RTotal = l[i].R2 + l[i].R1 + l[i].RMth;
                l[i].RToGo = l[i].AchTotal - l[i].RTotal;

                if (l[i].AchTotal != 0)
                {
                    l[i].R2Percentage = l[i].R2 / l[i].AchTotal;
                    l[i].R1Percentage = l[i].R1 / l[i].AchTotal;
                    l[i].RMthPercentage = l[i].RMth / l[i].AchTotal;
                    l[i].RTotalPercentage = l[i].RTotal / l[i].AchTotal;
                    l[i].RToGoPercentage = l[i].RToGo / l[i].AchTotal;
                }

                //Cost To Date
                if (l[i].AchTotal != 0)
                {
                    l[i].CD2 = (l[i].AchTo2 / l[i].AchTotal) * l[i].RAPAmt;
                    l[i].CD2Percentage = (l[i].CD2 / l[i].AchTotal) * 100;
                    l[i].CD1 = (l[i].AchTo1 / l[i].AchTotal) * l[i].RAPAmt;
                    l[i].CD1Percentage = (l[i].CD1 / l[i].AchTotal) * 100;
                    l[i].CDMth = (l[i].AchMth / l[i].AchTotal) * l[i].RAPAmt;
                    l[i].CDMthPercentage = (l[i].CDMth / l[i].AchTotal) * 100;
                    l[i].CDToMth = l[i].CD2 + l[i].CD1 + l[i].CDMth;
                    l[i].CDToMthPercentage = (l[i].CDToMth / l[i].AchTotal) * 100;
                    l[i].CDToGo = l[i].RAPAmt - l[i].CDToMth;
                    l[i].CDToGoPercentage = (l[i].CDToGo / l[i].AchTotal) * 100;
                }

                l[i].BMHD = l[i].CDToMth - l[i].Outflow;
                if (l[i].AchTotal != 0) l[i].TotalTerminPercentage = (l[i].TotalTermin / l[i].AchTotal) * 100;
                l[i].Q1 = l[i].Salary1 + l[i].Salary2 + l[i].Salary3;
                l[i].Q2 = l[i].Salary4 + l[i].Salary5 + l[i].Salary6;
                l[i].Q3 = l[i].Salary7 + l[i].Salary8 + l[i].Salary9;
                l[i].Q4 = l[i].Salary10 + l[i].Salary11 + l[i].Salary12;
                if (mThisMth == string.Concat(mThisYear, "01")) l[i].TotalSalary = l[i].Salary1;
                if (mThisMth == string.Concat(mThisYear, "02")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2;
                if (mThisMth == string.Concat(mThisYear, "03")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2 + l[i].Salary3;
                if (mThisMth == string.Concat(mThisYear, "04")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2 + l[i].Salary3 + l[i].Salary4;
                if (mThisMth == string.Concat(mThisYear, "05")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2 + l[i].Salary3 + l[i].Salary4 + l[i].Salary5;
                if (mThisMth == string.Concat(mThisYear, "06")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2 + l[i].Salary3 + l[i].Salary4 + l[i].Salary5 + l[i].Salary6;
                if (mThisMth == string.Concat(mThisYear, "07")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2 + l[i].Salary3 + l[i].Salary4 + l[i].Salary5 + l[i].Salary6 + l[i].Salary7;
                if (mThisMth == string.Concat(mThisYear, "08")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2 + l[i].Salary3 + l[i].Salary4 + l[i].Salary5 + l[i].Salary6 + l[i].Salary7 + l[i].Salary8;
                if (mThisMth == string.Concat(mThisYear, "09")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2 + l[i].Salary3 + l[i].Salary4 + l[i].Salary5 + l[i].Salary6 + l[i].Salary7 + l[i].Salary8 + l[i].Salary9;
                if (mThisMth == string.Concat(mThisYear, "10")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2 + l[i].Salary3 + l[i].Salary4 + l[i].Salary5 + l[i].Salary6 + l[i].Salary7 + l[i].Salary8 + l[i].Salary9 + l[i].Salary10;
                if (mThisMth == string.Concat(mThisYear, "11")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2 + l[i].Salary3 + l[i].Salary4 + l[i].Salary5 + l[i].Salary6 + l[i].Salary7 + l[i].Salary8 + l[i].Salary9 + l[i].Salary10 + l[i].Salary11;
                if (mThisMth == string.Concat(mThisYear, "12")) l[i].TotalSalary = l[i].Salary1 + l[i].Salary2 + l[i].Salary3 + l[i].Salary4 + l[i].Salary5 + l[i].Salary6 + l[i].Salary7 + l[i].Salary8 + l[i].Salary9 + l[i].Salary10 + l[i].Salary11 + l[i].Salary12;
            }
        }

        private void Process99(ref List<OperationCashFlow> l)
        {
            Sm.ClearGrd(Grd1, false);
            int No = 0;
            for (int i = 0; i < l.Count; i++)
            {
                Grd1.Rows.Add();
                Grd1.Cells[No, 0].Value = No + 1;
                Grd1.Cells[No, 1].Value = l[i].ProjectCode;
                Grd1.Cells[No, 2].Value = l[i].ProjectName;
                Grd1.Cells[No, 3].Value = l[i].SiteName;
                Grd1.Cells[No, 4].Value = l[i].Period;
                Grd1.Cells[No, 5].Value = l[i].ContractAmt;
                Grd1.Cells[No, 6].Value = l[i].AchTotal;
                Grd1.Cells[No, 7].Value = l[i].Ach2;
                Grd1.Cells[No, 8].Value = l[i].AchTo2;
                Grd1.Cells[No, 9].Value = l[i].Ach1;
                Grd1.Cells[No, 10].Value = l[i].AchTo1;
                Grd1.Cells[No, 11].Value = l[i].AchNow;
                Grd1.Cells[No, 12].Value = l[i].AchToNow;
                Grd1.Cells[No, 13].Value = l[i].AchPercentage;
                Grd1.Cells[No, 14].Value = l[i].AchMth;
                Grd1.Cells[No, 15].Value = l[i].AchToMth;
                Grd1.Cells[No, 16].Value = l[i].RAPAmt;
                Grd1.Cells[No, 17].Value = l[i].RAPPercentage;
                Grd1.Cells[No, 18].Value = l[i].RAP2;
                Grd1.Cells[No, 19].Value = l[i].RAPTo2;
                Grd1.Cells[No, 20].Value = l[i].RAP1;
                Grd1.Cells[No, 21].Value = l[i].RAPTo1;
                Grd1.Cells[No, 22].Value = l[i].RAPTo1Percentage;
                Grd1.Cells[No, 23].Value = l[i].RAPNow;
                Grd1.Cells[No, 24].Value = l[i].RAPNowPercentage;
                Grd1.Cells[No, 25].Value = l[i].RAPToNow;
                Grd1.Cells[No, 26].Value = l[i].RAPToNowPercentage;
                Grd1.Cells[No, 27].Value = l[i].RAPMth;
                Grd1.Cells[No, 28].Value = l[i].RAPMthPercentage;
                Grd1.Cells[No, 29].Value = l[i].RAPToMth;
                Grd1.Cells[No, 30].Value = l[i].R2;
                Grd1.Cells[No, 31].Value = l[i].R2Percentage;
                Grd1.Cells[No, 32].Value = l[i].R1;
                Grd1.Cells[No, 33].Value = l[i].R1Percentage;
                Grd1.Cells[No, 34].Value = l[i].RMth;
                Grd1.Cells[No, 35].Value = l[i].RMthPercentage;
                Grd1.Cells[No, 36].Value = l[i].RTotal;
                Grd1.Cells[No, 37].Value = l[i].RTotalPercentage;
                Grd1.Cells[No, 38].Value = l[i].RToGo;
                Grd1.Cells[No, 39].Value = l[i].RToGoPercentage;
                Grd1.Cells[No, 40].Value = l[i].CD2;
                Grd1.Cells[No, 41].Value = l[i].CD2Percentage;
                Grd1.Cells[No, 42].Value = l[i].CD1;
                Grd1.Cells[No, 43].Value = l[i].CD1Percentage;
                Grd1.Cells[No, 44].Value = l[i].CDMth;
                Grd1.Cells[No, 45].Value = l[i].CDMthPercentage;
                Grd1.Cells[No, 46].Value = l[i].CDToMth;
                Grd1.Cells[No, 47].Value = l[i].CDToMthPercentage;
                Grd1.Cells[No, 48].Value = l[i].CDToGo;
                Grd1.Cells[No, 49].Value = l[i].CDToGoPercentage;
                Grd1.Cells[No, 50].Value = l[i].Inflow;
                Grd1.Cells[No, 51].Value = l[i].Outflow;
                Grd1.Cells[No, 52].Value = l[i].PlusMinus;
                Grd1.Cells[No, 53].Value = l[i].BMHD;
                Grd1.Cells[No, 54].Value = l[i].DPAmt;
                Grd1.Cells[No, 55].Value = l[i].DPDiscountAmt;
                Grd1.Cells[No, 56].Value = l[i].TerminToNow;
                Grd1.Cells[No, 57].Value = l[i].TotalTermin;
                Grd1.Cells[No, 58].Value = l[i].TotalTerminPercentage;
                Grd1.Cells[No, 59].Value = l[i].BruttoToMth;
                Grd1.Cells[No, 60].Value = l[i].TotalBruttoToNow;
                Grd1.Cells[No, 61].Value = l[i].Brutto1;
                Grd1.Cells[No, 62].Value = l[i].BruttoTo1;
                Grd1.Cells[No, 63].Value = l[i].BruttoNow;
                Grd1.Cells[No, 64].Value = l[i].Salary1;
                Grd1.Cells[No, 65].Value = l[i].DRQDocNo1;
                Grd1.Cells[No, 67].Value = l[i].Salary2;
                Grd1.Cells[No, 68].Value = l[i].DRQDocNo2;
                Grd1.Cells[No, 70].Value = l[i].Salary3;
                Grd1.Cells[No, 71].Value = l[i].DRQDocNo3;
                Grd1.Cells[No, 73].Value = l[i].Q1;
                Grd1.Cells[No, 74].Value = l[i].Salary4;
                Grd1.Cells[No, 75].Value = l[i].DRQDocNo4;
                Grd1.Cells[No, 77].Value = l[i].Salary5;
                Grd1.Cells[No, 78].Value = l[i].DRQDocNo5;
                Grd1.Cells[No, 80].Value = l[i].Salary6;
                Grd1.Cells[No, 81].Value = l[i].DRQDocNo6;
                Grd1.Cells[No, 83].Value = l[i].Q2;
                Grd1.Cells[No, 84].Value = l[i].Salary7;
                Grd1.Cells[No, 85].Value = l[i].DRQDocNo7;
                Grd1.Cells[No, 87].Value = l[i].Salary8;
                Grd1.Cells[No, 88].Value = l[i].DRQDocNo8;                
                Grd1.Cells[No, 90].Value = l[i].Salary9;
                Grd1.Cells[No, 91].Value = l[i].DRQDocNo9;
                Grd1.Cells[No, 93].Value = l[i].Q3;
                Grd1.Cells[No, 94].Value = l[i].Salary10;
                Grd1.Cells[No, 95].Value = l[i].DRQDocNo10;
                Grd1.Cells[No, 97].Value = l[i].Salary11;
                Grd1.Cells[No, 98].Value = l[i].DRQDocNo11;
                Grd1.Cells[No, 100].Value = l[i].Salary12;
                Grd1.Cells[No, 101].Value = l[i].DRQDocNo12;
                Grd1.Cells[No, 103].Value = l[i].Q4;
                Grd1.Cells[No, 104].Value = l[i].TotalSalary;
                Grd1.Cells[No, 105].Value = l[i].IORAP1;
                Grd1.Cells[No, 106].Value = l[i].IORAPNow;
                Grd1.Cells[No, 107].Value = l[i].IORAPMth;
                Grd1.Cells[No, 108].Value = l[i].IORAPTotal;
                Grd1.Cells[No, 109].Value = l[i].OORAP1;
                Grd1.Cells[No, 110].Value = l[i].OORAPNow;
                Grd1.Cells[No, 111].Value = l[i].OORAPMth;
                Grd1.Cells[No, 112].Value = l[i].OORAPTotal;
                No += 1;
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        #endregion

        #endregion

        #region Class

        private class OperationCashFlow
        {
            public string No { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string SiteName { get; set; }
            public string Period { get; set; }
            public string PRJIDocNo { get; set; }
            public decimal ContractAmt { get; set; }
            public decimal AchTotal { get; set; }
            public decimal Ach2 { get; set; }
            public decimal AchTo2 { get; set; }
            public decimal Ach1 { get; set; }
            public decimal AchTo1 { get; set; }
            public decimal AchNow { get; set; }
            public decimal AchToNow { get; set; }
            public decimal AchPercentage { get; set; }
            public decimal AchMth { get; set; }
            public decimal AchToMth { get; set; }
            public decimal RAPAmt { get; set; }
            public decimal RAPPercentage { get; set; }
            public decimal RAP2 { get; set; }
            public decimal RAPTo2 { get; set; }
            public decimal RAP1 { get; set; }
            public decimal RAPTo1 { get; set; }
            public decimal RAPTo1Percentage { get; set; }
            public decimal RAPNow { get; set; }
            public decimal RAPNowPercentage { get; set; }
            public decimal RAPToNow { get; set; }
            public decimal RAPToNowPercentage { get; set; }
            public decimal RAPMth { get; set; }
            public decimal RAPMthPercentage { get; set; }
            public decimal RAPToMth { get; set; }
            public decimal R2 { get; set; }
            public decimal R2Percentage { get; set; }
            public decimal R1 { get; set; }
            public decimal R1Percentage { get; set; }
            public decimal RMth { get; set; }
            public decimal RMthPercentage { get; set; }
            public decimal RTotal { get; set; }
            public decimal RTotalPercentage { get; set; }
            public decimal RToGo { get; set; }
            public decimal RToGoPercentage { get; set; }
            public decimal CD2 { get; set; }
            public decimal CD2Percentage { get; set; }
            public decimal CD1 { get; set; }
            public decimal CD1Percentage { get; set; }
            public decimal CDMth { get; set; }
            public decimal CDMthPercentage { get; set; }
            public decimal CDToMth { get; set; }
            public decimal CDToMthPercentage { get; set; }
            public decimal CDToGo { get; set; }
            public decimal CDToGoPercentage { get; set; }
            public decimal Inflow { get; set; }
            public decimal Outflow { get; set; }
            public decimal PlusMinus { get; set; }
            public decimal BMHD { get; set; }
            public decimal DPAmt { get; set; }
            public decimal DPDiscountAmt { get; set; }
            public decimal TerminToNow { get; set; }
            public decimal TotalTermin { get; set; }
            public decimal TotalTerminPercentage { get; set; }
            public decimal BruttoToMth { get; set; }
            public decimal TotalBruttoToNow { get; set; }
            public decimal Brutto1 { get; set; }
            public decimal BruttoTo1 { get; set; }
            public decimal BruttoNow { get; set; }
            public decimal Salary1 { get; set; }
            public string DRQDocNo1 { get; set; }
            public decimal Salary2 { get; set; }
            public string DRQDocNo2 { get; set; }
            public decimal Salary3 { get; set; }
            public string DRQDocNo3 { get; set; }
            public decimal Q1 { get; set; }
            public decimal Salary4 { get; set; }
            public string DRQDocNo4 { get; set; }
            public decimal Salary5 { get; set; }
            public string DRQDocNo5 { get; set; }
            public decimal Salary6 { get; set; }
            public string DRQDocNo6 { get; set; }
            public decimal Q2 { get; set; }
            public decimal Salary7 { get; set; }
            public string DRQDocNo7 { get; set; }
            public decimal Salary8 { get; set; }
            public string DRQDocNo8 { get; set; }
            public decimal Salary9 { get; set; }
            public string DRQDocNo9 { get; set; }
            public decimal Q3 { get; set; }
            public decimal Salary10 { get; set; }
            public string DRQDocNo10 { get; set; }
            public decimal Salary11 { get; set; }
            public string DRQDocNo11 { get; set; }
            public decimal Salary12 { get; set; }
            public string DRQDocNo12 { get; set; }
            public decimal Q4 { get; set; }
            public decimal TotalSalary { get; set; }
            public decimal IORAP1 { get; set; }
            public decimal IORAPNow { get; set; }
            public decimal IORAPMth { get; set; }
            public decimal IORAPTotal { get; set; }
            public decimal OORAP1 { get; set; }
            public decimal OORAPNow { get; set; }
            public decimal OORAPMth { get; set; }
            public decimal OORAPTotal { get; set; }
        }

        private class Achievement
        {
            public string PRJIDocNo { get; set; }
            public string SettleDt { get; set; }
            public decimal SettledAmt { get; set; }
        }

        private class SalesInvoice
        {
            public string PRJIDocNo { get; set; }
            public string DocDt { get; set; }
            public decimal DPAmt { get; set; }
            public decimal VCAmt { get; set; }
        }

        private class DroppingPayment
        {
            public string PRJIDocNo { get; set; }
            public string DocDt { get; set; }
            public string DRQDocNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class DroppingRequest
        {
            public string PRJIDocNo { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class ARDP
        {
            public string PRJIDocNo { get; set; }
            public decimal Amt { get; set; }
        }

        public class SettledUninvoiced
        {
            public string PRJIDocNo { get; set; }
            public string SettleDt { get; set; }
            public decimal SettledAmt { get; set; }
        }

        #endregion

    }
}
