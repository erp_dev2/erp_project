﻿#region Update 
/*
    01/08/2018 [HAR] tambah training di header, detail menjadi class berdasarkan master training
    15/08/2018 [WED] ShowTraining() dan ShowTraining(string DocNo) query nya dirubah, karena Tm di TrainingDtl boleh null
    16/08/2018 [HAR] inputan detail scheduled jadi input time
    14/12/2018 [HAR] BUg : Show data detail
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Globalization;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTrainingSchedule : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "";
        internal FrmTrainingScheduleFind FrmFind;
        private string
           mWSCodeForNationalHoliday = string.Empty,
           mWSCodesForNationalHoliday = string.Empty,
           mWSCodesForHoliday = string.Empty;
        private bool
            mIsNationalHolidayAutoUpdateWorkSchedule = false;
        internal bool mIsFilterBySiteHR = false;
        private int mCol = -1;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmTrainingSchedule(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Training Schedule";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                GetParameter();
                SetFormControl(mState.View);
               
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mWSCodeForNationalHoliday = Sm.GetParameter("WSCodeForNationalHoliday");
            mWSCodesForNationalHoliday = Sm.GetParameter("WSCodesForNationalHoliday");
            mWSCodesForHoliday = Sm.GetParameter("WSCodesForHoliday");
            mIsNationalHolidayAutoUpdateWorkSchedule = Sm.GetParameterBoo("IsNationalHolidayAutoUpdateWorkSchedule");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 2;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Training"+Environment.NewLine+"Code",
                        
                        //1
                        "Training",
                    },
                     new int[] 
                    {
                        //0
                        100, 

                        //1-5
                        210
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 0});
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueTrainingCodeHdr,
                        DteStartDt, DteEndDt,  MeeRemark 
                    }, true);
                    BtnRefreshData.Enabled = false;
                    BtnClearData.Enabled = false;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueTrainingCodeHdr,
                        DteStartDt, DteEndDt,  MeeRemark 
                    }, false);
                    BtnRefreshData.Enabled = true;
                    BtnClearData.Enabled = true;
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, LueTrainingCodeHdr,
                TxtStatus, DteDocDt, DteStartDt, DteEndDt,  MeeRemark, 
            });
           ClearGrd1();
        }

        private void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
            Grd1.Cols.Count = 2;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTrainingScheduleFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                DteStartDt.DateTime = DteDocDt.DateTime;
                DteEndDt.DateTime = DteDocDt.DateTime;
                Sl.SetLueTrainingCode(ref LueTrainingCodeHdr, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            NationalHolidayAutoUpdateWorkSchedule();

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TrainingSch", "TblTrainingSchHdr");
            var l = new List<TrainingSch>();

            PrepareData(ref l);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveTrainingSchDtl(DocNo, ref l));

            cml.Add(SaveTrainingSchHdr(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            if (
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start date") ||
                Sm.IsDteEmpty(DteEndDt, "End date") ||
                Sm.IsLueEmpty(LueTrainingCodeHdr, "Training")||
                IsGrdEmpty())
                return true;
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 class");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveTrainingSchHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTrainingSchHdr(DocNo, DocDt, Status, StartDt, EndDt, TrainingCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'A', @StartDt, @EndDt, @TrainingCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetLue(LueTrainingCodeHdr));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTrainingSchDtl(string DocNo, ref List<TrainingSch> l)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into tblTrainingSchDtl(DocNo, TrainingCode, TrainingDNo, Dt, SchInd, Tm, CreateBy, CreateDt) ");
            SQL.AppendLine("Values  ");

            int x = 0;
            var cm = new MySqlCommand();

            l.ForEach(i =>
            {
                x += 1;
                if (x != 1)
                    SQL.AppendLine(", ");
                SQL.AppendLine("(@DocNo" + x + ", @TrainingCode" + x + ", @TrainingDNo" + x + ", @Dt" + x + ",  @SchInd" + x + ", @Tm"+x+", @UserCode, @CurrentDateTime)");

                Sm.CmParam<String>(ref cm, "@DocNo" + x, DocNo);
                Sm.CmParam<String>(ref cm, "@TrainingCode" + x, Sm.GetLue(LueTrainingCodeHdr));
                Sm.CmParam<String>(ref cm, "@TrainingDNo" + x, i.TrainingDno);
                Sm.CmParam<String>(ref cm, "@Dt" + x, i.Dt);
                Sm.CmParam<String>(ref cm, "@SchInd" + x, i.SchInd.Length>0?"Y":"N");
                Sm.CmParam<String>(ref cm, "@Tm" + x, i.Tm.Replace(":", ""));
            });
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CurrentDateTime", Sm.Left(Sm.ServerCurrentDateTime(), 12));
            cm.CommandText = SQL.ToString();
            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowTrainingSchHdr(DocNo);
                ShowTrainingSchDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTrainingSchHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ");
            SQL.AppendLine("(Case Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("Else '' End) As StatusDesc, ");
            SQL.AppendLine("StartDt, EndDt, TrainingCode, Remark ");
            SQL.AppendLine("From TblTrainingSchHdr Where DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "StatusDesc", "StartDt", "EndDt", "TrainingCode", 
                        //6
                        "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[3]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[4]));
                        Sl.SetLueTrainingCode(ref LueTrainingCodeHdr, Sm.DrStr(dr, c[5]), string.Empty);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowTrainingSchDtl(string DocNo)
        {
            ShowTraining(DocNo);
            ShowDate();
            ShowHoliday();

            var l = new List<TrainingSch>();

            GetData(DocNo, ref l);
            if (l.Count > 0)
            {
                ShowData(ref l);
            }
        }

        private void ShowTraining(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DNo, Case When A.Tm Is Null Then ");
            SQL.AppendLine("    A.Class ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Concat(A.Class, IfNull(Concat(' : ', Left(A.Tm, 2), ':', Right(A.Tm, 2)), '')) ");
            SQL.AppendLine("End As Class ");
            SQL.AppendLine("From TblTrainingDtl A ");
            SQL.AppendLine("Where Exists( ");
            SQL.AppendLine("    Select 1 From TblTrainingSchDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And TrainingCode=A.TrainingCode ");
            SQL.AppendLine(") Order By A.DNo;");

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] { "Dno", "Class" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    }, true, false, false, false
                );
            if (Grd1.Rows.Count >= 1 && Sm.GetGrdStr(Grd1, 0, 0).Length > 0) Grd1.Rows.Add();
        }


        private void GetData(string DocNo, ref List<TrainingSch> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.TrainingDno, A.Dt, ifnull(A.SchInd, '-') As SchInd, B.TrainingName, if(A.SchInd = 'Y', 'Scheduled', '') As SchName, IfNull(Concat(Left(A.Tm, 2), ':', Right(A.Tm, 2)), '') As Tm ");
            SQL.AppendLine("From TblTrainingSchDtl A ");
            SQL.AppendLine("Inner Join  TblTraining B On A.TrainingCode=B.TrainingCode ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.Dt, A.TrainingDno ;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "TrainingDno", "Dt", "SchInd", "TrainingName", "SchName", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TrainingSch()
                        {
                            TrainingDno = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            SchInd = Sm.DrStr(dr, c[2]),
                            //SchName = Sm.DrStr(dr, c[4]),
                            Tm = Sm.DrStr(dr, c[5]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowData(ref List<TrainingSch> l)
        {
            if (Grd1.Cols.Count <= 2) return;

            string TrainingDno = string.Empty;
            int r = 0;
            int c = 2;
            Grd1.BeginUpdate();
            l.ForEach(i =>
            {
                if (!Sm.CompareStr(TrainingDno, i.TrainingDno))
                {
                    TrainingDno = i.TrainingDno;
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), i.TrainingDno))
                        {
                            r = Row;
                            break;
                        }
                    }
                    c = 2;
                    while (c <= Grd1.Cols.Count)
                    {
                        if (Sm.CompareStr(i.Dt, Grd1.Header.Cells[0, c].Value.ToString()))
                        {

                            Grd1.Cells[r, c].Value = i.SchInd;
                            Grd1.Cells[r, c + 1].Value = i.Tm;
                            Grd1.Cells[r, c + 1].ForeColor = i.IsHoliday ? Color.Red : Color.Black;
                            Grd1.Cells[r, c + 1].BackColor = i.SchInd == "Y" ? Color.Yellow : Grd1.Header.Cells[0, c + 1].BackColor;
                            break;
                        }
                        c += 2;
                    }
                }
                else
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), i.TrainingDno))
                        {
                            r = Row;
                            break;
                        }
                    }
                    c = 2;
                    while (c <= Grd1.Cols.Count)
                    {
                        if (Sm.CompareStr(i.Dt, Grd1.Header.Cells[0, c].Value.ToString()))
                        {

                            Grd1.Cells[r, c].Value = i.SchInd;
                            Grd1.Cells[r, c + 1].Value = i.Tm;
                            Grd1.Cells[r, c + 1].ForeColor = i.IsHoliday ? Color.Red : Color.Black;
                            Grd1.Cells[r, c + 1].BackColor = i.SchInd == "Y" ? Color.Yellow : Grd1.Header.Cells[0, c + 1].BackColor;
                            break;
                        }
                        c += 2;
                    }
                }

            });
            Grd1.EndUpdate();
        }

        #endregion

        #region Additional Method

        private void PrepareData(ref List<TrainingSch> l)
        {
            int Col = 2;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
                    Grd1.Cols.Count > 1)
                {
                    Col = 2;
                    while (Col < Grd1.Cols.Count)
                    {
                        l.Add(new TrainingSch()
                        {
                            TrainingDno = Sm.GetGrdStr(Grd1, Row, 0),
                            Dt = Grd1.Header.Cells[0, Col].Value.ToString(),
                            SchInd = Sm.GetGrdStr(Grd1, Row, Col),
                            Tm = Sm.GetGrdStr(Grd1, Row, Col)
                        });
                        Col += 2;
                    }
                }
            }
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, mCol - 1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void NationalHolidayAutoUpdateWorkSchedule()
        {
            if (!mIsNationalHolidayAutoUpdateWorkSchedule ||
                mWSCodeForNationalHoliday.Length == 0 ||
                mWSCodesForNationalHoliday.Length == 0 ||
                mWSCodesForHoliday.Length == 0) return;

            NationalHolidayAutoUpdateWorkSchedule(
                mWSCodesForNationalHoliday.Split('#').Union(mWSCodesForHoliday.Split('#')).ToArray());
        }

        private void NationalHolidayAutoUpdateWorkSchedule(string[] AllHolidays)
        {
            bool IsNeedUpdate = true;
            var cm = new MySqlCommand();
            cm.CommandText = "Select WSName from TblWorkSchedule Where WSCode=@WSCode;";
            Sm.CmParam<String>(ref cm, "@WSCode", mWSCodeForNationalHoliday);
            var WSNameForNationalHoliday = Sm.GetValue(cm);

            for (int i = 3; i < Grd1.Cols.Count - 1; i++)
            {
                if (Grd1.Header.Cells[0, i + 1].BackColor == Color.Pink)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                        {
                            IsNeedUpdate = true;
                            foreach (string h in AllHolidays)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, i), h))
                                {
                                    IsNeedUpdate = false;
                                    break;
                                }
                            }
                            if (IsNeedUpdate)
                            {
                                Grd1.Cells[r, i].Value = mWSCodeForNationalHoliday;
                                Grd1.Cells[r, i + 1].Value = WSNameForNationalHoliday;
                                Grd1.Cells[r, i + 1].ForeColor = Color.Red;
                                Grd1.Cells[r, i + 1].BackColor = Color.Pink;
                            }
                        }
                    }
                }
            }
        }

        private void ShowTraining()
        {
           
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Dno, concat(Class, IfNull(Concat(' : ', Left(Tm, 2), ':', Right(Tm, 2)), '')) As Class ");
            SQL.AppendLine("From tbltrainingDtl ");
            SQL.AppendLine("Where TrainingCode=@TrainingCode ");

            Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetLue(LueTrainingCodeHdr));

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString() + " Order By Dno ;",
                    new string[]
                        {
                            //0
                            "Dno", 

                            //1
                            "Class"
                        },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    }, true, false, false, false
                );
            if (Grd1.Rows.Count >= 1 && Sm.GetGrdStr(Grd1, 0, 0).Length > 0) Grd1.Rows.Add();
        }

        private void ShowDate()
        {
            var DtMin = Sm.GetDte(DteStartDt);
            var DtMax = Sm.GetDte(DteEndDt);

            if (DtMin.Length >= 8 &&
                DtMax.Length >= 8 &&
                decimal.Parse(DtMin) <= decimal.Parse(DtMax))
            {
                DateTime Dt1 = new DateTime(
                       Int32.Parse(DtMin.Substring(0, 4)),
                       Int32.Parse(DtMin.Substring(4, 2)),
                       Int32.Parse(DtMin.Substring(6, 2)),
                       0, 0, 0);

                DateTime Dt2 = new DateTime(
                    Int32.Parse(DtMax.Substring(0, 4)),
                    Int32.Parse(DtMax.Substring(4, 2)),
                    Int32.Parse(DtMax.Substring(6, 2)),
                    0, 0, 0);

                DateTime TempDt = Dt1;

                var TotalDays = (Dt2 - Dt1).Days;

                var s = new List<string>();

                s.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );

                for (int i = 0; i < TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);
                    s.Add(
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                    );
                }

                Grd1.Cols.Count = 2 + ((TotalDays + 1) * 2);
                int Col = 2;

                Grd1.BeginUpdate();
                s.ForEach(i =>
                {
                    Grd1.Header.Cells[0, Col].Value = i;
                    Grd1.Cols[Col].Width = 100;
                    Grd1.Cols[Col].Visible = false;
                    Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.MiddleCenter;

                    Grd1.Header.Cells[0, Col + 1].Value =
                        Sm.Right(i, 2) + "-" +
                        CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(
                            int.Parse(i.Substring(4, 2))
                            ) + "-" +
                        Sm.Left(i, 4) + Environment.NewLine +
                        TempDt.DayOfWeek.ToString();
                    Grd1.Cols[Col + 1].Width = 100;
                    Grd1.Header.Cells[0, Col + 1].TextAlign = iGContentAlignment.MiddleCenter;

                    if ((int)TempDt.DayOfWeek == 0)
                    {
                        for (int r = 0; r < Grd1.Rows.Count; r++)
                        {
                            Grd1.Cells[r, Col + 1].BackColor = Color.LightSalmon;
                            Grd1.Header.Cells[0, Col + 1].BackColor = Color.LightSalmon;
                        }
                    }

                    TempDt = TempDt.AddDays(1);
                    Col += 2;
                });
                Grd1.EndUpdate();
            }
        }

        private void ShowHoliday()
        {
            var SQL = new StringBuilder();
            var l = new List<Holiday>();

            SQL.AppendLine("Select HolDt ");
            SQL.AppendLine("From TblHoliday ");
            SQL.AppendLine("Where HolDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("Order By HolDt;");

            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "HolDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Holiday()
                        {
                            HolDt = Sm.DrStr(dr, c[0])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0 && Grd1.Cols.Count > 3)
            {
                l.ForEach(h =>
                {
                    for (int i = 3; i < Grd1.Cols.Count; i++)
                    {
                        if (Sm.CompareStr(h.HolDt, Grd1.Header.Cells[0, i].Value.ToString()))
                        {
                            Grd1.Header.Cells[0, i + 1].Value += (Environment.NewLine + "HOLIDAY");
                            Grd1.Header.Cells[0, i + 1].BackColor = Color.Pink;
                            for (int r = 0; r < Grd1.Rows.Count; r++)
                                Grd1.Cells[r, i + 1].BackColor = Color.Pink;
                        }
                    }
                });
            }
            Grd1.Rows.AutoHeight();
        }

        private void SetLueTrainingSchedule(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

           
            SQL.AppendLine("Select 'Y' Col1, 'Scheduled' As Col2 ; ");
            cm.CommandText = SQL.ToString();
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion 

        #region Event

        #region Misc Control Event

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            DteEndDt.EditValue = DteStartDt.EditValue;
            ClearGrd1();
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd1();
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0 && e.ColIndex > 2)
            {
                mCol = e.ColIndex;
            }
            else
                e.DoDefault = false;
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Grd1.CurCell != null && Grd1.CurCell.ColIndex > 2 && e.KeyCode == Keys.Enter && Grd1.CurCell.RowIndex != Grd1.Rows.Count - 1)
            {
                if (Grd1.CurCell.ColIndex == Grd1.Cols.Count - 1)
                    Sm.FocusGrd(Grd1, Grd1.CurCell.RowIndex + 1, 8);
                else
                    Sm.FocusGrd(Grd1, Grd1.CurCell.RowIndex, Grd1.CurCell.ColIndex + 2);
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            var Value = string.Empty;
            if (e.ColIndex != 0 || e.ColIndex != 1)
            {
                Value = Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex);
                if (Value.Length == 4 || Value.Length == 3)
                {
                    SetTime(Grd1, e.RowIndex, e.ColIndex-1, e.ColIndex);
                    Grd1.Cells[e.RowIndex, e.ColIndex].BackColor = Color.Yellow;
                }
                else
                {
                    if (!(Value.Length == 5 && Value.Substring(2, 1) == ":"))
                    {
                        Grd1.Cells[e.RowIndex, e.ColIndex].Value = Grd1.Cells[e.RowIndex, e.ColIndex-1].Value = string.Empty;
                    }
                }
                Sm.FocusGrd(Grd1, e.RowIndex, e.ColIndex);
            }
        }

        #endregion

        #region Button Event

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            try
            {
                ClearGrd1();

                if (
                    Sm.IsLueEmpty(LueTrainingCodeHdr, "Training")||
                    Sm.IsDteEmpty(DteStartDt, "Start date") ||
                    Sm.IsDteEmpty(DteEndDt, "End date")
                    ) return;

                Cursor.Current = Cursors.WaitCursor;

                ShowTraining();
                ShowDate();
                ShowHoliday();

                var l = new List<TrainingSch>();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnClearData_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to clear all schedule ?") == DialogResult.No) return;

            if (Grd1.Rows.Count <= 1) return;

            try
            {
                Grd1.BeginUpdate();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    for (int Col = 2; Col < Grd1.Cols.Count; Col++)
                    {
                        Grd1.Cells[Row, Col].Value = null;
                        Grd1.Cells[Row, Col].ForeColor = Color.Black;
                        Grd1.Cells[Row, Col].BackColor = Color.Transparent;
                    }
                }

                Grd1.EndUpdate();

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void BtnExcel_Click_1(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        private void LueTrainingCodeHdr_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingCodeHdr, new Sm.RefreshLue1(Sl.SetLueTrainingCode));
        }

        private void LueTrainingSchedule_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        public void SetTime(iGrid GrdXXX, int RowXXX, int ColActual, int ColEdit)
        {
            try
            {
                if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 4 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2)) <= 59)
                {
                    GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat(Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                    GrdXXX.Cells[RowXXX, ColEdit].Value = Sm.GetGrdStr(GrdXXX, RowXXX, ColActual);
                }
                if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 3 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2)) <= 59)
                {
                    GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat("0", Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 1), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                    GrdXXX.Cells[RowXXX, ColEdit].Value = Sm.GetGrdStr(GrdXXX, RowXXX, ColActual);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #endregion

        #region Class

        private class TrainingSch
        {
            public string TrainingDno { get; set; }
            public string Dt { get; set; }
            public string SchInd { get; set; }
            public string SchName { get; set; }
            public string Tm { get; set; }
            public bool IsHoliday { get; set; }
        }

        private class WorkSchedulePattern
        {
            public string Pattern { get; set; }
            public Color PatternColor { get; set; }
        }

        private class Holiday
        {
            public string HolDt { get; set; }
        }

        #endregion
    }
}
