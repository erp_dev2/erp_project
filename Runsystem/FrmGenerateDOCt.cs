﻿#region Update
/*
    30/01/2020 [WED/SIER] new apps
    06/02/2020 [WED/SIER] whs bisa milih
    25/02/2020 [VIN/SIER] tambah kolom grand total dan round up
    17/03/2020 [WED/SIER] isian angka nya ambil dari menu master, bukan dari parameter
    19/03/2020 [WED/SIER] data nilai yg tadi nya dari parameter, ambil dari master nya
    18/06/2020 [DITA/SIER] Tambah inputan item dan customer category
    17/09/2021 [TKG/ALL] tambah validasi setting journal
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGenerateDOCt : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmGenerateDOCtFind FrmFind;
        private string
            mGenerateDOCtWhsCode = string.Empty,
            mGenerateDOCtItCode = string.Empty,
            mGenerateDOCtLWBPHour = string.Empty,
            mGenerateDOCtWBPHour = string.Empty,
            mAcNoForInitialStockOpeningBalance = string.Empty,
            mEntCode = string.Empty;
        private decimal
            mGenerateDOCtKAP = 0m,
            mGenerateDOCtKVAQuota = 0m,
            mGenerateDOCtLWBP = 0m,
            mGenerateDOCtWBP = 0m,
            mGenerateDOCtPJUPercentage = 0m,
            mGenerateDOCtUsageLimitMultiplier = 0m,
            mInventoryUomCodeConvert12 = 0m,
            mInventoryUomCodeConvert13 = 0m;
        private bool
            mIsCheckCOAJournalNotExists = false,
            mIsBatchNoUseDocDtIfEmpty = false,
            mIsAutoJournalActived = false,
            mIsMovingAvgEnabled = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDOCtAmtRounded = false;

        #endregion

        #region Constructor

        public FrmGenerateDOCt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Generate DO To Customer (Electricity)";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                SetLueWhsCode(ref LueWhsCode, mGenerateDOCtWhsCode);
                Sl.SetLueCtCtCode(ref LueCtCtCode);
                SetGrd();
                base.FrmLoad(sender, e);
                //if this application is called from other application

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",
                    //1-5
                    "Customer's Code", "DNo", "Customer's Name", "Shipping's Address", "KVA", 
                    //6-10
                    "Usage Limit", "LWBP Usage"+Environment.NewLine+"(kWH)", "LWBP Usage"+Environment.NewLine+"(IDR)", "WBP Usage"+Environment.NewLine+"(kWH)", "WBP Usage"+Environment.NewLine+"(IDR)", 
                    //11-15
                    "KAP Rent"+Environment.NewLine+"(IDR)", "Amount", "BPJU", "Total Bills", "PPN (10%)", 
                    //16-20
                    "Total After Tax", "Object", "Target", "Bill", "DO#",
                    //21-22
                    "Round Up", "Grand Total"
                },
                new int[] 
                { 
                    20, 
                    120, 0, 200, 400, 100, 
                    120, 120, 120, 120, 120, 
                    120, 120, 120, 120, 120, 
                    120, 80, 80, 120, 130,
                    120, 120
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 15, 16, 17, 18, 19, 20 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,21, 22 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 19, 21, 22 }, 0);
            
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 20 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueWhsCode,LueCtCtCode
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnItem.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueWhsCode, LueCtCtCode
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnItem.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 7, 9, 17, 18, 21 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueWhsCode
                    }, false);
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mEntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, MeeRemark, LueCtCtCode, TxtItCode, TxtItName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtKVAQuota, TxtLWBP, TxtLWBPHour, TxtWBP, TxtWBPHour,
                TxtKAP, TxtPJUPercentage
            }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 19, 21, 22 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGenerateDOCtFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            try
            {
                SetFormControl(mState.Edit);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtKVAQuota.EditValue = Sm.FormatNum(mGenerateDOCtKVAQuota, 0);
                TxtLWBP.EditValue = Sm.FormatNum(mGenerateDOCtLWBP, 0);
                TxtLWBPHour.EditValue = mGenerateDOCtLWBPHour;
                TxtWBP.EditValue = Sm.FormatNum(mGenerateDOCtWBP, 0);
                TxtWBPHour.EditValue = mGenerateDOCtWBPHour;
                TxtKAP.EditValue = Sm.FormatNum(mGenerateDOCtKAP, 0);
                TxtPJUPercentage.EditValue = Sm.FormatNum(mGenerateDOCtPJUPercentage, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Methods

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.IsGrdColSelected(new int[] { 5, 7, 9, 21 }, e.ColIndex))
                {
                    ComputeDetail(e.RowIndex);
                }
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCtCtCode, "Customer Category"))
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmGenerateDOCtDlg(this));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0&& !Sm.IsLueEmpty(LueCtCtCode, "Customer Category"))
                {
                    Sm.FormShowDialog(new FrmGenerateDOCtDlg(this));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "GenerateDOCt", "TblGenerateDOCtHdr");
            string StockInitialDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "StockInitial", "TblStockInitialHdr");
            mEntCode = Sm.GetValue("Select C.EntCode " +
                            "From TblWarehouse A " +
                            "Inner Join TblCostCenter B on A.CCCode = B.CCCode  " +
                            "INner Join TblProfitCenter C on B.ProfitCenterCode = C.ProfitCenterCode " +
                            "Where A.WhsCode = '" + Sm.GetLue(LueWhsCode) + "' limit 1");

            var cml = new List<MySqlCommand>();
            var l = new List<GenerateDOCtHdr>();
            var ldtl = new List<GenerateDOCtDtl>();
            var l2 = new List<StockInitialHdr>();
            var l2dtl = new List<StockInitialDtl>();
            var l3 = new List<DOCtHdr>();
            var l3dtl = new List<DOCtDtl>();

            ProcessHdr(ref l, DocNo);
            ProcessDtl(ref ldtl, DocNo, StockInitialDocNo);
            ProcessDtl2(ref ldtl);
            ProcessStockInitial(ref l2, ref l2dtl, StockInitialDocNo, DocNo, ldtl.Count);
            ProcessDOCt(ref l3, ref l3dtl, ref ldtl);

            foreach (var i in l) { cml.Add(SaveGenerateDOCtHdr(i)); }
            foreach (var i in ldtl) { cml.Add(SaveGenerateDOCtDtl(i)); }

            //Save Stock Initial
            for (int i = 0; i < l2.Count; ++i)
            {
                cml.Add(SaveStockInitialHdr(ref l2, i));
                for (int j = 0; j < l2dtl.Count; ++j)
                {
                    if (l2[i].DocNo == l2dtl[j].DocNo)
                    {
                        cml.Add(SaveStockInitialDtl(ref l2dtl, j));
                    }
                }
                cml.Add(SaveStock(ref l2, i));
                if (mIsAutoJournalActived)
                {
                    if (IsJournalSettingInvalid(ref l2dtl))
                    {
                        cml.Clear();
                        return;
                    }
                    cml.Add(SaveJournal(ref l2, i));
                }
            }

            //Save DO To Customer
            for (int i = 0; i < l3.Count; ++i)
            {
                cml.Add(SaveDOCtHdr(ref l3, i));
                for (int j = 0; j < l3dtl.Count; ++j)
                {
                    if (l3[i].DocNo == l3dtl[j].DocNo)
                    {
                        cml.Add(SaveDOCtDtl(ref l3dtl, j));
                    }
                }
                cml.Add(SaveStock2(ref l3, i));
                if (mIsAutoJournalActived)
                {
                    if (IsJournalSettingInvalid(ref l3dtl))
                    {
                        cml.Clear();
                        return;
                    }
                    cml.Add(SaveJournal2(ref l3, i));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);

            l.Clear(); ldtl.Clear(); l2.Clear(); l2dtl.Clear(); l3.Clear(); l3dtl.Clear();
        }

        private bool IsJournalSettingInvalid(ref List<StockInitialDtl> l)
        {
            if (!mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter

            if (mAcNoForInitialStockOpeningBalance.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForInitialStockOpeningBalance is empty.");
                return true;
            }

            //Table
            if (IsJournalSettingInvalid_ItemCategory(Msg, ref l)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, ref List<StockInitialDtl> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCtName = string.Empty;
            int i = 0;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B.AcNo Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            foreach (var x in l)
            {
                if (x.ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + i.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + i.ToString(), x.ItCode);
                    i++;
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid(ref List<DOCtDtl> l)
        {
            if (!mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter

            if (!mIsAcNoForSaleUseItemCategory)
            {
                if (Sm.GetParameter("AcNoForCOGS").Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForCOGS is empty.");
                    return true;
                }
                if (Sm.GetParameter("AcNoForSaleOfFinishedGoods").Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForSaleOfFinishedGoods is empty.");
                    return true;
                }
            }
            if (Sm.GetParameter("CustomerAcNoNonInvoice").Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                return true;
            }


            //Table
            if (IsJournalSettingInvalid_ItemCategory(Msg, ref l)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, ref List<DOCtDtl> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCtName = string.Empty;
            int i = 0;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("And (B.AcNo Is Null ");
            if (mIsAcNoForSaleUseItemCategory)
                SQL.AppendLine(" Or B.AcNo4 Is Null Or B.AcNo5 Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.ItCode In (");
            foreach (var x in l)
            {
                if (x.ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + i.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + i.ToString(), x.ItCode);
                    i++;
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueCtCtCode, "Customer's category") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecord() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsGrdValueNotValid()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.IsGrdValueEmpty(Grd1, i, 14, true, "Total Bills is zero.")) return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to insert at least 1 customer data.");
                Sm.FocusGrd(Grd1, 0, 1);
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecord()
        {
            if (Grd1.Rows.Count > 999)
            {
                Sm.StdMsg(mMsgType.Warning, "Data exceeds maximum amount of records (1000).");
                Sm.FocusGrd(Grd1, 0, 1);
                return true;
            }

            return false;
        }

        private MySqlCommand SaveGenerateDOCtHdr(GenerateDOCtHdr l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGenerateDOCtHdr ");
            SQL.AppendLine("(DocNo, DocDt, WhsCode, KVAQuota, LWBP, LWBPHour, WBP, WBPHour, KAP, ");
            SQL.AppendLine("PJUPercentage, Remark, CtCtCode, ItCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @WhsCode, @KVAQuota, @LWBP, @LWBPHour, @WBP, @WBPHour, @KAP, ");
            SQL.AppendLine("@PJUPercentage, @Remark, @CtCtCode, @ItCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", l.DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", l.DocDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", l.WhsCode);
            Sm.CmParam<Decimal>(ref cm, "@KVAQuota", l.KVAQuota);
            Sm.CmParam<Decimal>(ref cm, "@LWBP", l.LWBP);
            Sm.CmParam<String>(ref cm, "@LWBPHour", l.LWBPHour);
            Sm.CmParam<Decimal>(ref cm, "@WBP", l.WBP);
            Sm.CmParam<String>(ref cm, "@WBPHour", l.WBPHour);
            Sm.CmParam<Decimal>(ref cm, "@KAP", l.KAP);
            Sm.CmParam<Decimal>(ref cm, "@PJUPercentage", l.PJUPercentage);
            Sm.CmParam<String>(ref cm, "@Remark", l.Remark);
            Sm.CmParam<String>(ref cm, "@CtCtCode", l.CtCtCode);
            Sm.CmParam<String>(ref cm, "@ItCode", l.ItCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveGenerateDOCtDtl(GenerateDOCtDtl l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGenerateDOCtDtl( ");
            SQL.AppendLine("DocNo, DNo, DOCtDocNo, DOCtDNo, CtCode, CtShipAddressDNo,  ");
            SQL.AppendLine("KVA, UsageLimit, LWBPUsageKWH, LWBPUsageIDR, WBPUsageKWH, WBPUsageIDR, ");
            SQL.AppendLine("KAPRent, Amt, BPJU, TotalBills, TaxAmt, TotalAfterTax, ");
            SQL.AppendLine("Object, Target, Bill, Roundup, GrandTotal, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values( ");
            SQL.AppendLine("@DocNo, @DNo, @DOCtDocNo, @DOCtDNo, @CtCode, @CtShipAddressDNo,  ");
            SQL.AppendLine("@KVA, @UsageLimit, @LWBPUsageKWH, @LWBPUsageIDR, @WBPUsageKWH, @WBPUsageIDR, ");
            SQL.AppendLine("@KAPRent, @Amt, @BPJU, @TotalBills, @TaxAmt, @TotalAfterTax, ");
            SQL.AppendLine("@Object, @Target, @Bill, @Roundup, @GrandTotal, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", l.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", l.DNo);
            Sm.CmParam<String>(ref cm, "@DOCtDocNo", l.DOCtDocNo);
            Sm.CmParam<String>(ref cm, "@DOCtDNo", l.DOCtDNo);
            Sm.CmParam<String>(ref cm, "@CtCode", l.CtCode);
            Sm.CmParam<String>(ref cm, "@CtShipAddressDNo", l.CtShipAddressDNo);
            Sm.CmParam<Decimal>(ref cm, "@KVA", l.KVA);
            Sm.CmParam<Decimal>(ref cm, "@UsageLimit", l.UsageLimit);
            Sm.CmParam<Decimal>(ref cm, "@LWBPUsageKWH", l.LWBPUsageKWH);
            Sm.CmParam<Decimal>(ref cm, "@LWBPUsageIDR", l.LWBPUsageIDR);
            Sm.CmParam<Decimal>(ref cm, "@WBPUsageKWH", l.WBPUsageKWH);
            Sm.CmParam<Decimal>(ref cm, "@WBPUsageIDR", l.WBPUsageIDR);
            Sm.CmParam<Decimal>(ref cm, "@KAPRent", l.KAPRent);
            Sm.CmParam<Decimal>(ref cm, "@Amt", l.Amt);
            Sm.CmParam<Decimal>(ref cm, "@BPJU", l.BPJU);
            Sm.CmParam<Decimal>(ref cm, "@TotalBills", l.TotalBills);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", l.TaxAmt);
            Sm.CmParam<Decimal>(ref cm, "@TotalAfterTax", l.TotalAfterTax);
            Sm.CmParam<String>(ref cm, "@Object", l.Object);
            Sm.CmParam<String>(ref cm, "@Target", l.Target);
            Sm.CmParam<Decimal>(ref cm, "@Bill", l.Bill);
            Sm.CmParam<Decimal>(ref cm, "@Roundup", l.Roundup);
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", l.GrandTotal);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #region Stock Initial

        private MySqlCommand SaveStockInitialHdr(ref List<StockInitialHdr> l, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblStockInitialHdr(DocNo, DocDt, WhsCode, CurCode, ExcRate, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @CurCode, @ExcRate, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", l[Row].DocDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", l[Row].WhsCode);
            Sm.CmParam<String>(ref cm, "@CurCode", l[Row].CurCode);
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", l[Row].ExcRate);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveStockInitialDtl(ref List<StockInitialDtl> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockInitialDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @ItCode, @PropCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @UPrice, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", l[Row].DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", l[Row].ItCode);
            Sm.CmParam<String>(ref cm, "@PropCode", l[Row].PropCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", l[Row].BatchNo);
            Sm.CmParam<String>(ref cm, "@Lot", l[Row].Lot);
            Sm.CmParam<String>(ref cm, "@Bin", l[Row].Bin);
            Sm.CmParam<String>(ref cm, "@Source", l[Row].Source);
            Sm.CmParam<Decimal>(ref cm, "@Qty", l[Row].Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", l[Row].Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", l[Row].Qty3);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", l[Row].UPrice);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(ref List<StockInitialHdr> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.BatchNo, B.Source, A.CurCode, B.UPrice, A.ExcRate, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "04");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(ref List<StockInitialHdr> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblStockInitialHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Stock Initial : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, B.RemarkJournal, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt, Remark As RemarkJournal From (");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select D.AcNo, B.Qty*IfNull(E.MovingAvgPrice, 0.00) As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='Y' And D.AcNo Is Not Null ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, 0.00 As DAmt, B.Qty*IfNull(E.MovingAvgPrice, 0.00) As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, 0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), (Row + 1)));
            Sm.CmParam<String>(ref cm, "@AcNoForInitialStockOpeningBalance", mAcNoForInitialStockOpeningBalance);
            Sm.CmParam<String>(ref cm, "@MenuCode", Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmStockInitial' Limit 1;"));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].DocNo);
            return cm;
        }

        #endregion

        #region DO To Customer

        private MySqlCommand SaveDOCtHdr(ref List<DOCtHdr> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCtHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, WhsCode, CtCode, CurCode, ");
            SQL.AppendLine("SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, ");
            SQL.AppendLine("SAPhone, SAFax, SAEmail, SAMobile, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @DocDt, 'A', @WhsCode, @CtCode, @CurCode, ");
            SQL.AppendLine("B.Name, IfNull(B.Address, A.Address), IfNull(B.CityCode, A.CityCode), IfNull(B.CntCode, A.CntCode), IfNull(B.PostalCd, A.PostalCd), ");
            SQL.AppendLine("IfNull(B.Phone, A.Phone), IfNull(B.Fax, A.Fax), IfNull(B.Email, A.Email), IfNull(B.Mobile, A.Mobile), @Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblCustomer A ");
            SQL.AppendLine("Inner Join TblCustomerShipAddress B On A.CtCode = B.CtCode ");
            SQL.AppendLine("    And A.CtCode = @CtCode ");
            SQL.AppendLine("    And B.DNo = @CtShipAddressDNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", l[Row].DocDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", l[Row].WhsCode);
            Sm.CmParam<String>(ref cm, "@CtCode", l[Row].CtCode);
            Sm.CmParam<String>(ref cm, "@CtShipAddressDNo", l[Row].CtShipAddressDNo);
            Sm.CmParam<String>(ref cm, "@CurCode", l[Row].CurCode);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDOCtDtl(ref List<DOCtDtl> l, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, ProcessInd, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @ItCode, @PropCode, @BatchNo, 'O', @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @UPrice, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", l[Row].DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", l[Row].ItCode);
            Sm.CmParam<String>(ref cm, "@PropCode", l[Row].PropCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", l[Row].BatchNo);
            Sm.CmParam<String>(ref cm, "@Lot", l[Row].Lot);
            Sm.CmParam<String>(ref cm, "@Bin", l[Row].Bin);
            Sm.CmParam<String>(ref cm, "@Source", l[Row].Source);
            Sm.CmParam<Decimal>(ref cm, "@Qty", l[Row].Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", l[Row].Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", l[Row].Qty3);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", l[Row].UPrice);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock2(ref List<DOCtHdr> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A'; ");

            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where (A.Qty<>0) ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "07");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(ref List<DOCtHdr> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine(" Where DocNo=@DocNo And Status='A'; ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Generate DO To Customer : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A'; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty)) As DAmt, ");
                else
                    SQL.AppendLine("        Sum(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Group By E.AcNo5 ");
            }
            else
            {
                SQL.AppendLine("        Select D.ParValue As AcNo, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                else
                    SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            if (mIsDOCtAmtRounded)
                SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
            else
                SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblDOCtHdr A ");
            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
            if (mIsDOCtAmtRounded)
                SQL.AppendLine("        Sum(Floor(X1.Amt)) As DAmt, ");
            else
                SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
            SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
            SQL.AppendLine("            From TblDOCtHdr A ");
            SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Left Join ( ");
            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ) C On 0=0 ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) X1 ");
            SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
            SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
            SQL.AppendLine("        Union All ");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select X.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(X.Amt)) As CAmt ");
                else
                    SQL.AppendLine("        Sum(X.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 ");
                SQL.AppendLine("            Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty ");
                SQL.AppendLine("            As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 0=0 ");
                SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X ");
                SQL.AppendLine("        Group By X.AcNo ");
            }
            else
            {
                SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(X1.Amt)) As CAmt ");
                else
                    SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
                SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 1=1 ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X1 ");
                SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                SQL.AppendLine("        Group By X2.ParValue ");
            }

            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblDOCtHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmDOCt' limit 1;"));
            Sm.CmParam<String>(ref cm, "@MainCurCode", "IDR");
            Sm.CmParamDt(ref cm, "@DocDt", l[Row].DocDt);
            Sm.CmParam<String>(ref cm, "@CurCode", l[Row].CurCode);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(l[Row].WhsCode));

            return cm;
        }

        #endregion

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowGenerateDOCtHdr(DocNo);
                ShowGenerateDOCtDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowGenerateDOCtHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.WhsCode, A.KVAQuota, A.LWBP, A.LWBPHour, A.WBP, A.WBPHour, A.KAP, A.PJUPercentage, A.Remark, ");
            SQL.AppendLine("A.CtCtCode, A.ItCode, B.ItName ");
            SQL.AppendLine("FROM TblGenerateDOCtHdr A ");
            SQL.AppendLine("Left Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DocDt", "WhsCode", "KVAQuota", "LWBP", "LWBPHour",  
                    
                    //6-10
                    "WBP", "WBPHour", "KAP", "PJUPercentage", "Remark",

                    //11-13
                    "CtCtCode", "ItCode", "ItName"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[2]));
                    TxtKVAQuota.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                    TxtLWBP.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                    TxtLWBPHour.EditValue = Sm.DrStr(dr, c[5]);
                    TxtWBP.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    TxtWBPHour.EditValue = Sm.DrStr(dr, c[7]);
                    TxtKAP.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    TxtPJUPercentage.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                    Sm.SetLue(LueCtCtCode, Sm.DrStr(dr, c[11]));
                    TxtItCode.EditValue = Sm.DrStr(dr, c[12]);
                    TxtItName.EditValue = Sm.DrStr(dr, c[13]);
                }, true
            );
        }

        private void ShowGenerateDOCtDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT B.CtCode, B.CtShipAddressDNo, D.`Name`, IFNULL(D.Address, C.Address) SAAddress, B.KVA, ");
            SQL.AppendLine("B.UsageLimit, B.LWBPUsageKWH, B.LWBPUsageIDR, B.WBPUsageKWH, B.WBPUsageIDR, B.KAPRent, B.Amt, ");
            SQL.AppendLine("B.BPJU, B.TotalBills, B.TaxAmt, B.TotalAfterTax, B.Object, B.Target, B.Bill, B.Roundup, B.GrandTotal, B.DOCtDocNo ");
            SQL.AppendLine("FROM TblGenerateDOCtHdr A ");
            SQL.AppendLine("INNER JOIN TblGenerateDOCtDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND A.DocNo = @DocNo ");
            SQL.AppendLine("INNER JOIN TblCustomer C ON B.CtCode = C.CtCode ");
            SQL.AppendLine("INNER JOIN TblCustomerShipAddress D ON C.CtCode = D.CtCode ");
            SQL.AppendLine("    AND B.CtShipAddressDNo = D.DNo ");
            SQL.AppendLine("ORDER BY B.DNo; ");


            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "CtCode",
                    //1-5
                    "CtShipAddressDNo", "Name", "SAAddress", "KVA", "UsageLimit", 
                    //6-10
                    "LWBPUsageKWH", "LWBPUsageIDR", "WBPUsageKWH", "WBPUsageIDR", "KAPRent",
                    //11-15
                    "Amt", "BPJU", "TotalBills", "TaxAmt", "TotalAfterTax",
                    //16-20
                    "Object", "Target", "Bill", "DOCtDocNo", "Roundup",
                    //21
                    "GrandTotal"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                }, false, false, false, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'IsCheckCOAJournalNotExists', 'IsMovingAvgEnabled', 'GenerateDOCtWhsCode', 'GenerateDOCtItCode', ");
            SQL.AppendLine("'AcNoForInitialStockOpeningBalance', 'IsBatchNoUseDocDtIfEmpty', 'IsAcNoForSaleUseItemCategory', 'IsDOCtAmtRounded', 'GenerateDOCtUsageLimitMultiplier' ");            
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                            case "IsBatchNoUseDocDtIfEmpty": mIsBatchNoUseDocDtIfEmpty = ParValue == "Y"; break;
                            case "IsAcNoForSaleUseItemCategory": mIsAcNoForSaleUseItemCategory = ParValue == "Y"; break;
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            
                            //string
                            case "GenerateDOCtWhsCode": mGenerateDOCtWhsCode = ParValue; break;
                            case "GenerateDOCtItCode": mGenerateDOCtItCode = ParValue; break;
                            case "AcNoForInitialStockOpeningBalance": mAcNoForInitialStockOpeningBalance = ParValue; break;

                            //decimal
                            case "GenerateDOCtUsageLimitMultiplier":
                                if (ParValue.Length == 0)
                                    mGenerateDOCtUsageLimitMultiplier = 1m;
                                else
                                    mGenerateDOCtUsageLimitMultiplier = decimal.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
            if (mGenerateDOCtItCode.Length > 0)
            {
                mInventoryUomCodeConvert12 = Decimal.Parse(Sm.GetValue("Select IfNull(InventoryUomCodeConvert12, 1) From TblItem Where ItCode=@Param ", mGenerateDOCtItCode));
                mInventoryUomCodeConvert13 = Decimal.Parse(Sm.GetValue("Select IfNull(InventoryUomCodeConvert13, 1) From TblItem Where ItCode=@Param ", mGenerateDOCtItCode));
            }
        }

        private void CheckDataMaster()
        {
            mGenerateDOCtLWBPHour = string.Empty;
            mGenerateDOCtWBPHour = string.Empty;
            mGenerateDOCtKAP = 0m;
            mGenerateDOCtKVAQuota = 0m;
            mGenerateDOCtLWBP = 0m;
            mGenerateDOCtWBP = 0m;
            mGenerateDOCtPJUPercentage = 0m;

            if (Sm.GetLue(LueWhsCode).Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select KVAQuota, LWBP, LWBPHour, WBP, WBPHour, ");
                SQL.AppendLine("KAP, PJUPercentage ");
                SQL.AppendLine("From TblGenerateDOCtMaster ");
                SQL.AppendLine("Where DocType = @DocType And WhsCode = @WhsCode; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocType", "1");
                    Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "KVAQuota", 
                        "LWBP", "LWBPHour", "WBP", "WBPHour", "KAP", 
                        "PJUPercentage" 
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            mGenerateDOCtKVAQuota = Sm.DrDec(dr, c[0]);
                            mGenerateDOCtLWBP = Sm.DrDec(dr, c[1]);
                            mGenerateDOCtLWBPHour = Sm.DrStr(dr, c[2]);
                            mGenerateDOCtWBP = Sm.DrDec(dr, c[3]);
                            mGenerateDOCtWBPHour = Sm.DrStr(dr, c[4]);
                            mGenerateDOCtKAP = Sm.DrDec(dr, c[5]);
                            mGenerateDOCtPJUPercentage = Sm.DrDec(dr, c[6]);
                        }
                    }
                    dr.Close();
                }
            }

            TxtKVAQuota.EditValue = Sm.FormatNum(mGenerateDOCtKVAQuota, 0);
            TxtLWBP.EditValue = Sm.FormatNum(mGenerateDOCtLWBP, 0);
            TxtLWBPHour.EditValue = mGenerateDOCtLWBPHour;
            TxtWBP.EditValue = Sm.FormatNum(mGenerateDOCtWBP, 0);
            TxtWBPHour.EditValue = mGenerateDOCtWBPHour;
            TxtKAP.EditValue = Sm.FormatNum(mGenerateDOCtKAP, 0);
            TxtPJUPercentage.EditValue = Sm.FormatNum(mGenerateDOCtPJUPercentage, 0);

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; ++Row)
                {
                    ComputeDetail(Row);
                }
            }
        }

        private void ComputeDetail(int Row)
        {
            decimal mKVA = 0m, mUsageLimit = 0m, mUsageLWBPkWH = 0m, mUsageLWBPIDR = 0m,
                mUsageWBPkWH = 0m, mUsageWBPIDR = 0m, mKAPRent = 0m, mAmount = 0m,
                mBPJU = 0m, mTotalBills = 0m, mPPN = 0m, mTotalAfterTax = 0m,
                mRoundup=0m, mGrandTotal = 0m;

            
            mKVA = Sm.GetGrdDec(Grd1, Row, 5);
            mUsageLWBPkWH = Sm.GetGrdDec(Grd1, Row, 7);
            mUsageWBPkWH = Sm.GetGrdDec(Grd1, Row, 9);
            mRoundup = Sm.GetGrdDec(Grd1, Row, 21);

            mUsageLimit = mKVA * mGenerateDOCtUsageLimitMultiplier * mGenerateDOCtLWBP;
            mUsageLWBPIDR = mUsageLWBPkWH * mGenerateDOCtLWBP;
            mUsageWBPIDR = mUsageWBPkWH * mGenerateDOCtWBP;
            if (mGenerateDOCtKVAQuota != 0) mKAPRent = (mKVA / mGenerateDOCtKVAQuota) * mGenerateDOCtKAP;
            mAmount = mKAPRent + mUsageWBPIDR + mUsageLWBPIDR + mUsageLimit;
            mBPJU = mAmount * mGenerateDOCtPJUPercentage * 0.01m;
            mTotalBills = mAmount + mBPJU;
            mPPN = mTotalBills * 0.1m;
            mTotalAfterTax = mTotalBills + mPPN;
            mGrandTotal = mRoundup + mTotalBills;

            Grd1.Cells[Row, 6].Value = Sm.FormatNum(mUsageLimit, 0);
            Grd1.Cells[Row, 8].Value = Sm.FormatNum(mUsageLWBPIDR, 0);
            Grd1.Cells[Row, 10].Value = Sm.FormatNum(mUsageWBPIDR, 0);
            Grd1.Cells[Row, 11].Value = Sm.FormatNum(mKAPRent, 0);
            Grd1.Cells[Row, 12].Value = Sm.FormatNum(mAmount, 0);
            Grd1.Cells[Row, 13].Value = Sm.FormatNum(mBPJU, 0);
            Grd1.Cells[Row, 14].Value = Sm.FormatNum(mTotalBills, 0);
            Grd1.Cells[Row, 15].Value = Sm.FormatNum(mPPN, 0);
            Grd1.Cells[Row, 16].Value = Sm.FormatNum(mTotalAfterTax, 0);
            Grd1.Cells[Row, 19].Value = Sm.FormatNum(mTotalAfterTax, 0);
            Grd1.Cells[Row, 22].Value = Sm.FormatNum(mGrandTotal, 0);
        }

        private void ProcessHdr(ref List<GenerateDOCtHdr> l, string DocNo)
        {
            l.Add(new GenerateDOCtHdr()
            {
                DocNo = DocNo,
                DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                WhsCode = Sm.GetLue(LueWhsCode),
                KVAQuota = Decimal.Parse(TxtKVAQuota.Text),
                LWBP = Decimal.Parse(TxtLWBP.Text),
                LWBPHour = TxtLWBPHour.Text,
                WBP = Decimal.Parse(TxtWBP.Text),
                WBPHour = TxtWBPHour.Text,
                KAP = Decimal.Parse(TxtKAP.Text),
                PJUPercentage = Decimal.Parse(TxtPJUPercentage.Text),
                Remark = MeeRemark.Text,
                CtCtCode = Sm.GetLue(LueCtCtCode),
                ItCode = TxtItCode.Text
            });
        }

        private void ProcessDtl(ref List<GenerateDOCtDtl> l, string DocNo, string StockInitialDocNo)
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                l.Add(new GenerateDOCtDtl()
                {
                    DocNo = DocNo,
                    DNo = Sm.Right(string.Concat("000", (i + 1)), 3),
                    DOCtDocNo = "1",
                    DOCtDNo = "1",
                    StockInitialDocNo = StockInitialDocNo,
                    CtCode = Sm.GetGrdStr(Grd1, i, 1),
                    CtShipAddressDNo = Sm.GetGrdStr(Grd1, i, 2),
                    KVA = Sm.GetGrdDec(Grd1, i, 5),
                    UsageLimit = Sm.GetGrdDec(Grd1, i, 6),
                    LWBPUsageKWH = Sm.GetGrdDec(Grd1, i, 7),
                    LWBPUsageIDR = Sm.GetGrdDec(Grd1, i, 8),
                    WBPUsageKWH = Sm.GetGrdDec(Grd1, i, 9),
                    WBPUsageIDR = Sm.GetGrdDec(Grd1, i, 10),
                    KAPRent = Sm.GetGrdDec(Grd1, i, 11),
                    Amt = Sm.GetGrdDec(Grd1, i, 12),
                    BPJU = Sm.GetGrdDec(Grd1, i, 13),
                    TotalBills = Sm.GetGrdDec(Grd1, i, 14),
                    TaxAmt = Sm.GetGrdDec(Grd1, i, 15),
                    TotalAfterTax = Sm.GetGrdDec(Grd1, i, 16),
                    Object = Sm.GetGrdStr(Grd1, i, 17),
                    Target = Sm.GetGrdStr(Grd1, i, 18),
                    Bill = Sm.GetGrdDec(Grd1, i, 19),
                    Roundup = Sm.GetGrdDec(Grd1, i, 21),
                    GrandTotal = Sm.GetGrdDec(Grd1, i, 22),
                });
            }
        }

        private void ProcessDtl2(ref List<GenerateDOCtDtl> l)
        {
            string 
                mDOCtDocNo = string.Empty, 
                mDOCtDNo = string.Empty, 
                mCtCode = string.Empty,
                mCtShipAddressDNo = string.Empty;

            int mDocNoIndex = 0, mDNoIndex = 1;

            foreach(var x in l.OrderBy(o => o.CtCode).ThenBy(p => p.CtShipAddressDNo))
            {
                if (mCtCode == x.CtCode && mCtShipAddressDNo == x.CtShipAddressDNo)
                {
                    mDNoIndex += 1;
                }
                else
                {
                    mCtCode = x.CtCode;
                    mCtShipAddressDNo = x.CtShipAddressDNo;
                    mDocNoIndex += 1;
                    mDNoIndex = 1;

                    mDOCtDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt", "TblDOCtHdr", mDocNoIndex.ToString());
                }

                mDOCtDNo = Sm.Right(string.Concat("000", mDNoIndex.ToString()), 3);

                x.DOCtDocNo = mDOCtDocNo;
                x.DOCtDNo = mDOCtDNo;
            }
        }

        private void ProcessStockInitial(ref List<StockInitialHdr> l2, ref List<StockInitialDtl> l2dtl, string StockInitialDocNo, string DocNo, int Count)
        {
            l2.Add(new StockInitialHdr()
            {
                DocNo = StockInitialDocNo,
                DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                WhsCode = Sm.GetLue(LueWhsCode),
                CurCode = "IDR",
                ExcRate = 1m,
                Remark = string.Concat(MeeRemark.Text, " [Auto created by #", DocNo, "]")
            });

            l2dtl.Add(new StockInitialDtl()
            {
                DocNo = StockInitialDocNo,
                DNo = "001",
                ItCode = mGenerateDOCtItCode,
                PropCode = "-",
                BatchNo = mIsBatchNoUseDocDtIfEmpty ? Sm.Left(Sm.GetDte(DteDocDt), 8) : "-",
                Source = string.Concat("04*", StockInitialDocNo, "*001"),
                Lot = "-",
                Bin = "-",
                Qty = 1m * Count,
                Qty2 = 1m * Count * mInventoryUomCodeConvert12,
                Qty3 = 1m * Count * mInventoryUomCodeConvert13,
                UPrice = 0m,
                Remark = string.Concat("Auto created by #", DocNo)
            });
        }

        private void ProcessDOCt(ref List<DOCtHdr> l3, ref List<DOCtDtl> l3dtl, ref List<GenerateDOCtDtl> ldtl)
        {
            string mDocNo = string.Empty;

            foreach (var x in ldtl.OrderBy(o => o.DOCtDocNo).ThenBy(p => p.DOCtDNo))
            {
                l3dtl.Add(new DOCtDtl()
                {
                    DocNo = x.DOCtDocNo,
                    DNo = x.DOCtDNo,
                    ItCode = mGenerateDOCtItCode,
                    PropCode = "-",
                    BatchNo = mIsBatchNoUseDocDtIfEmpty ? Sm.Left(Sm.GetDte(DteDocDt), 8) : "-",
                    Source = string.Concat("04*", x.StockInitialDocNo, "*001"),
                    Lot = "-",
                    Bin = "-",
                    Qty = 1m,
                    Qty2 = 1m * mInventoryUomCodeConvert12,
                    Qty3 = 1m * mInventoryUomCodeConvert13,
                    UPrice = x.GrandTotal,
                    Remark = string.Concat("Auto created by #", x.DocNo)
                    
                });

                if (x.DOCtDocNo != mDocNo)
                {
                    l3.Add(new DOCtHdr()
                    {
                        DocNo = x.DOCtDocNo,
                        DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                        WhsCode = Sm.GetLue(LueWhsCode),
                        CtCode = x.CtCode,
                        CtShipAddressDNo = x.CtShipAddressDNo,
                        CurCode = "IDR",
                        Remark = string.Concat(MeeRemark.Text, " [Auto created by #", x.DocNo, "]")
                    });

                    mDocNo = x.DOCtDocNo;
                }
            }
        }

        private void SetLueWhsCode(ref DXE.LookUpEdit Lue, string WhsCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WhsCode Col1, WhsName Col2 ");
            SQL.AppendLine("From TblWarehouse ");
            if (WhsCode.Length>0)
                SQL.AppendLine("Where Find_In_Set(WhsCode, @Param) ");
            else
                SQL.AppendLine("Where 1=0 ");
            SQL.AppendLine("Order By WhsName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Param", WhsCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(SetLueWhsCode), mGenerateDOCtWhsCode);
                CheckDataMaster();
            }
        }
        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(Sl.SetLueCtCtCode));
        }

        private void BtnItem_Click(object sender, EventArgs e)
        {
            if (BtnItem.Enabled)
            {
                Sm.FormShowDialog(new FrmGenerateDOCtDlg2(this));
            }
        }

        #endregion

        #endregion

        #region Class

        private class GenerateDOCtHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public decimal KVAQuota { get; set; }
            public decimal LWBP { get; set; }
            public string LWBPHour { get; set; }
            public decimal WBP { get; set; }
            public string WBPHour { get; set; }
            public decimal KAP { get; set; }
            public decimal PJUPercentage { get; set; }
            public string Remark { get; set; }
            public string CtCtCode { get; set; }
            public string ItCode { get; set; }
        }

        private class GenerateDOCtDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DOCtDocNo { get; set; }
            public string DOCtDNo { get; set; }
            public string StockInitialDocNo { get; set; }
            public string CtCode { get; set; }
            public string CtShipAddressDNo { get; set; }
            public decimal KVA { get; set; }
            public decimal UsageLimit { get; set; }
            public decimal LWBPUsageKWH { get; set; }
            public decimal LWBPUsageIDR { get; set; }
            public decimal WBPUsageKWH { get; set; }
            public decimal WBPUsageIDR { get; set; }
            public decimal KAPRent { get; set; }
            public decimal Amt { get; set; }
            public decimal BPJU { get; set; }
            public decimal TotalBills { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal TotalAfterTax { get; set; }
            public string Object { get; set; }
            public string Target { get; set; }
            public decimal Bill { get; set; }
            public decimal Roundup { get; set; }
            public decimal GrandTotal { get; set; }
        }

        private class StockInitialHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string Remark { get; set; }
        }

        private class StockInitialDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public decimal UPrice { get; set; }
            public string Remark { get; set; }
        }

        public class DOCtHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string CtCode { get; set; }
            public string CtShipAddressDNo { get; set; }
            public string CurCode { get; set; }
            public string Remark { get; set; }
        }

        public class DOCtDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public decimal UPrice { get; set; }
            public string Remark { get; set; }
        }

        #endregion
    }
}
