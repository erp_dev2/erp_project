﻿#region Update
/*
    19/01/2021 [VIN/IMS] Tambah kolom ItCodeInternal, Spec berdasarkan parameter IsBOMShowSpecifications
 * 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmReorderPointFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmReorderPoint mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmReorderPointFind(FrmReorderPoint FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-1);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                SetGrd();
                SetSQL();
                
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T1.* FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  SELECT T.* FROM ");
            SQL.AppendLine("  ( ");
            SQL.AppendLine("    SELECT a.DocNo, a.DocDt, b.ItCode, c.ItCodeInternal, c.Specification, c.ForeignName, c.ItName, d.ItCtCode, d.ItCtName, ");
            SQL.AppendLine("    b.MinStock, b.ReOrderPoint, a.CreateBy, a.CreateDt, a.LastUpBy, a.LastUpDt ");
            SQL.AppendLine("    FROM tblreorderpointhdr a ");
            SQL.AppendLine("    INNER JOIN tblreorderpointdtl b ON a.DocNo = b.DocNo ");
            SQL.AppendLine("    INNER JOIN tblitem c ON b.ItCode = c.ItCode ");
            SQL.AppendLine("    LEFT JOIN tblitemcategory d ON c.ItCtCode = d.ItCtCode ");
            SQL.AppendLine("    WHERE a.DocDt IN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("      SELECT DocDt FROM TblReorderPointHdr WHERE DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("  )T ");
            SQL.AppendLine("  WHERE T.MinStock IN ");
            SQL.AppendLine("  ( ");
            SQL.AppendLine("    SELECT MinStock FROM TblReorderPointDtl WHERE MinStock > 0 OR MinStock < 0 ");
            SQL.AppendLine("  ) ");
            SQL.AppendLine("  OR T.ReOrderPoint IN ");
            SQL.AppendLine("  ( ");
            SQL.AppendLine("    SELECT ReOrderPoint FROM TblReorderPointDtl WHERE ReOrderPoint > 0 OR ReOrderPoint < 0 ");
            SQL.AppendLine("  ) ");
            SQL.AppendLine(")T1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Item's Code",
                        "Item's"+Environment.NewLine+"Local Code",
                        "Foreign Name",

                        //6-10
                        "Item's Name",
                        "Item's"+Environment.NewLine+"Category Name",
                        "Minimum Stock",
                        "Reorder Point",
                        "Created"+Environment.NewLine+"By",
                        
                        //11-15
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time", 

                        //16
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 100, 100, 200, 
                        
                        //6-10
                        300, 250, 100, 100, 100, 
                        
                        //11-15
                        100, 100, 100, 100, 100,

                        //16
                        150
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 10, 11, 12, 13, 14, 15 }, false);
                
            Sm.SetGrdProperty(Grd1, false);
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                Sm.GrdColInvisible(Grd1, new int[] {4, 16 }, true);
                Grd1.Cols[16].Move(7);
                Grd1.Cols[4].Move(5);
            }
        }

        override protected void HideInfoInGrd()
        {
            if (!mFrmParent.mIsBOMShowSpecifications)
                Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);

        }

        override protected void ShowData()
        {
            try
            {
                if (IsFilterByDateInvalid()) return;

                Cursor.Current = Cursors.WaitCursor;
                string Filter = " WHERE 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T1.DocNo", false);
                //Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T1.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "T1.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T1.ItCode", "T1.ItName", "T1.ItCodeInternal", "T1.ForeignName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ORDER BY T1.DocNo; ",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "ItCode", "ItCodeInternal", "ForeignName", "ItName", 
                            
                            //6-10
                            "ItCtName", "MinStock", "ReOrderPoint", "CreateBy", "CreateDt",
                            
                            //11-13
                            "LastUpBy", "LastUpDt", "Specification"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private bool IsFilterByDateInvalid()
        {
            if (Sm.IsDteEmpty(DteDocDt1, "Start date")) return true;
            if (Sm.IsDteEmpty(DteDocDt2, "End date")) return true;

            var DocDt1 = Sm.GetDte(DteDocDt1);
            var DocDt2 = Sm.GetDte(DteDocDt2);

            if (Decimal.Parse(DocDt1) > Decimal.Parse(DocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0)
                DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item Category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
