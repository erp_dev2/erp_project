﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TenTec.Windows.iGridLib;
using Sm = RunSystem.StdMtd;

namespace RunSystem
{
    class ChartHeader
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string ChartTitle1 { get; set; }
        public string ChartTitle2 { get; set; }
        public string ChartTitle3 { get; set; }
        public string CreateBy { get; set; }
        public string PrintBy { get; set; }
    }

    class ChartDetail
    {
        public string AxisYTitle { set; get; }
        public decimal AxisYValue { get; set; }
        public string SeriesTitle { set; get; }
        public int SeriesNumber { set; get; }
        public string AxisXTitle { set; get; }
        public decimal AxisXValue { get; set; }
    }

    class ChartClass
    {
        public static List<ChartDetail> GetChartDetail(iGrid Grd, int AxisXCol, int[] AxisYCol, bool Percentage)
        {
            decimal YValue = 0;
            List<ChartDetail> chartdtl = new List<ChartDetail>();
            for (int iRow = 0; iRow < Grd.Rows.Count; iRow++)
            {
                for (int iColY = 0; iColY <= AxisYCol.Length - 1; iColY++)
                for (int iCol = 0; iCol < Grd.Cols.Count; iCol++)
                    if ((AxisYCol[iColY] == iCol) && (Grd.Rows[iRow].Tag != "Subtotal")) 
                    {
                        YValue = Decimal.Parse(Grd.Cells[iRow, AxisYCol[iColY]].Text);
                        chartdtl.Add(new ChartDetail()
                        {
                            AxisXTitle = (Percentage ? Sm.GetGrdStr(Grd, iRow, AxisXCol) + " %": Sm.GetGrdStr(Grd, iRow, AxisXCol)),
                            AxisXValue = iRow,
                            SeriesNumber = iColY + 1,
                            SeriesTitle = Grd.Cols[AxisYCol[iColY]].Text.ToString().Replace(Environment.NewLine, " "),
                            AxisYValue = (Percentage ? YValue * 100 : YValue)
                        });
                    }
            }
            return chartdtl;
                
        }
    }

    

}
