﻿#region Update
/*
    05/12/2020 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptOperationalBudgetMonitoringDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptOperationalBudgetMonitoring mFrmParent;
        private string
            mSQL = string.Empty,
            mJournalDocNo = string.Empty,
            mAcNo = string.Empty
            ;

        #endregion

        #region Constructor

        public FrmRptOperationalBudgetMonitoringDlg(
            FrmRptOperationalBudgetMonitoring FrmParent, 
            string JournalDocNo, 
            string AcNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mJournalDocNo = JournalDocNo;
            mAcNo = AcNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Date",
                    "Month",
                    "",
                    "Journal#",
                    "COA#",

                    //6-9
                    "COA",
                    "Value",
                    "Remark"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    80, 100, 20, 180, 150,

                    //6-8
                    200, 150, 200
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, Substring(A.DocDt, 5, 2) Mth, A.DocNo, B.AcNo, C.AcDesc, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.DAmt - B.CAmt Else B.CAmt - B.DAmt End As Amt, ");
            SQL.AppendLine("A.Remark ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And Find_In_Set(A.DocNo, @DocNo) ");
            SQL.AppendLine("    And B.AcNo = @AcNo ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");

            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {

        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.CmParam<string>(ref cm, "@DocNo", mJournalDocNo);
                Sm.CmParam<string>(ref cm, "@AcNo", mAcNo);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL() + " Order By A.DocDt, A.DocNo; ",
                    new string[] 
                    { 
                        //0
                        "DocDt", 
                        //1-5
                        "Mth", "DocNo", "AcNo", "AcDesc", "Amt",
                        //6
                        "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    }, true, false, false, false
                );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Methods

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length > 0)
            {
                var f = new FrmJournal(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

    }
}
