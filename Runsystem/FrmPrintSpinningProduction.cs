﻿#region Update
/*
    03/01/2019 [TKG] Print Out Spinning Production
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPrintSpinningProduction : RunSystem.FrmBase11
    {
        #region Field

        private string mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmPrintSpinningProduction(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.SetDteCurrentDate(DteDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        protected override void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsDteEmpty(DteDt, "Date")) return;

            var Dt = Sm.GetDte(DteDt).Substring(0, 8);
            var Dt1 = string.Concat(Sm.Left(Dt, 6), "01");
            var Dt2 = Dt;
            var WorkingDays = Decimal.Parse(Sm.Right(Dt, 2));

            var lWCSpinning = new List<WCSpinning>();

            var lDataMesin = new List<DataMesin>();
            var lDataProduksi1 = new List<DataProduksi1>();
            var lProdEfficiencyVar = new List<ProdEfficiencyVar>();
            var lDataEfisiensi1 = new List<DataEfisiensi>();
            var lFormulaSpinningNonItem = new List<FormulaSpinningNonItem>();
            var lDataProduksi2 = new List<DataProduksi2>();
            var lDataEfisiensi2 = new List<DataEfisiensi>();
            var lDataProduksi2Total = new List<DataProduksi2Total>();

            var lWCGrd1 = new List<WCGrd>();
            var lWCGrd2 = new List<WCGrd>();
            var lPEVGrd = new List<PEVGrd>();

            var lData1 = new List<Data1>();
            var lData2a = new List<Data2>();
            var lData3a = new List<Data3>();
            var lData4a = new List<Data4>();
            var lData2b = new List<Data2>();
            var lData3b = new List<Data3>();
            var lData4b = new List<Data4>();
            var lData5 = new List<Data5>();
            var lData6 = new List<Data6>();

            try
            {
                Process1(ref lWCSpinning);
                Process2(ref lProdEfficiencyVar);
                Process3(ref lFormulaSpinningNonItem);

                Process11(Dt, ref lDataMesin);
                Process12(Dt, ref lDataProduksi1);
                Process13(Dt, ref lDataEfisiensi1);
                Process14(WorkingDays, ref lWCSpinning, ref lDataProduksi1, ref lFormulaSpinningNonItem, ref lProdEfficiencyVar, ref lDataEfisiensi1);
                Process15(Dt, ref lDataProduksi2, ref lDataEfisiensi2, ref lProdEfficiencyVar, ref lWCSpinning);
                Process16(ref lDataProduksi2Total, ref lWCSpinning, ref lDataProduksi2, ref lDataEfisiensi2);

                Process21(ref lData1, ref lWCSpinning);
                Process22(ref lData2a, ref lWCSpinning, ref lDataMesin);
                Process23(ref lData3a, ref lWCSpinning, ref lDataProduksi1);
                Process24(ref lData3a, ref lWCSpinning, ref lDataProduksi2);
                Process25(ref lData4a, ref lWCSpinning, ref lDataProduksi2Total);
                Process26(ref lData5, ref lProdEfficiencyVar, ref lWCSpinning, ref lDataEfisiensi1, ref lDataEfisiensi2);

                lDataMesin.Clear();
                lDataProduksi1.Clear();
                lDataEfisiensi1.Clear();
                lDataProduksi2.Clear();
                lDataEfisiensi2.Clear();
                lDataProduksi2Total.Clear();

                Process31(Dt1, Dt2, ref lDataMesin);
                Process32(Dt1, Dt2, ref lDataProduksi1);
                Process33(Dt1, Dt2, ref lDataEfisiensi1);
                Process34(WorkingDays, ref lWCSpinning, ref lDataProduksi1, ref lFormulaSpinningNonItem, ref lProdEfficiencyVar, ref lDataEfisiensi1);
                Process35(Dt1, Dt2, ref lDataProduksi2, ref lDataEfisiensi2, ref lProdEfficiencyVar, ref lWCSpinning);
                Process36(ref lDataProduksi2Total, ref lWCSpinning, ref lDataProduksi2, ref lDataEfisiensi2);

                Process22(ref lData2b, ref lWCSpinning, ref lDataMesin);
                Process23(ref lData3b, ref lWCSpinning, ref lDataProduksi1);
                Process24(ref lData3b, ref lWCSpinning, ref lDataProduksi2);
                Process25(ref lData4b, ref lWCSpinning, ref lDataProduksi2Total);

                ParPrint(
                    ref lData1,
                    ref lData2a,
                    ref lData2b,
                    ref lData3a,
                    ref lData3b,
                    ref lData4a,
                    ref lData4b,
                    ref lData5,
                    ref lData6);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lWCSpinning.Clear();
                lDataMesin.Clear();
                lDataProduksi1.Clear();
                lProdEfficiencyVar.Clear();
                lDataEfisiensi1.Clear();
                lFormulaSpinningNonItem.Clear();
                lDataProduksi2.Clear();
                lDataEfisiensi2.Clear();
                lDataProduksi2Total.Clear();
                lWCGrd1.Clear();
                lWCGrd2.Clear();
                lPEVGrd.Clear();

                lData1.Clear();
                lData2a.Clear();
                lData3a.Clear();
                lData4a.Clear();
                lData2b.Clear();
                lData3b.Clear();
                lData4b.Clear();
                lData5.Clear();
                lData6.Clear();
            }
        }

        private void Process1(ref List<WCSpinning> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, WCName, UnitInd, PercentInd, ProdDataPerItemInd, ");
            SQL.AppendLine("TotalMachine, SeqNo, ProdFormula, FSNICode1, FSNICode2 ");
            SQL.AppendLine("From TblWCSpinning Where ActInd='Y' Order By SeqNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "WCCode", 
                    "WCName", "UnitInd", "PercentInd", "ProdDataPerItemInd", "TotalMachine", 
                    "SeqNo", "ProdFormula", "FSNICode1", "FSNICode2"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WCSpinning()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            WCName = Sm.DrStr(dr, c[1]),
                            UnitInd = Sm.DrStr(dr, c[2]) == "Y",
                            PercentInd = Sm.DrStr(dr, c[3]) == "Y",
                            ProdDataPerItemInd = Sm.DrStr(dr, c[4]) == "Y",
                            TotalMachine = Sm.DrDec(dr, c[5]),
                            SeqNo = Sm.DrStr(dr, c[6]),
                            ProdFormula = Sm.DrStr(dr, c[7]),
                            FSNICode1 = Sm.DrStr(dr, c[8]),
                            FSNICode2 = Sm.DrStr(dr, c[9])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<ProdEfficiencyVar> l)
        {
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select OptCode, OptDesc From TblOption Where OptCat='ProdEfficiencyVar' Order By OptCode;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptCode", "OptDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProdEfficiencyVar()
                        {
                            PEVCode = Sm.DrStr(dr, c[0]),
                            PEVDesc = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<FormulaSpinningNonItem> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WCCode, A.PercentInd, A.TotalMachine, Case When B.Query Is Not Null Then B.TotalInd Else C.TotalInd End As TotalInd, ");
            SQL.AppendLine("B.Query As Query_1, B.Param1 As Param1_1, B.Param2 As Param2_1, B.Param3 As Param3_1, B.Param4 As Param4_1, B.Param5 As Param5_1, B.Param6 As Param6_1, ");
            SQL.AppendLine("C.Query As Query_2, B.Param1 As Param1_2, B.Param2 As Param2_2, B.Param3 As Param3_2, B.Param4 As Param4_2, B.Param5 As Param5_2, B.Param6 As Param6_2 ");
            SQL.AppendLine("From TblWCSpinning A ");
            SQL.AppendLine("Left Join TblFormulaSpinningNonItem B On A.FSNICode1=B.FSNICode And B.ActInd='Y' ");
            SQL.AppendLine("Left Join TblFormulaSpinningNonItem C On A.FSNICode2=C.FSNICode And C.ActInd='Y' ");
            SQL.AppendLine("Where ProdDataPerItemInd='N' ");
            SQL.AppendLine("Order By SeqNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "WCCode", 

                    //1-5
                    "PercentInd", "TotalMachine", "TotalInd", "Query_1", "Param1_1", 

                    //6-10
                    "Param2_1", "Param3_1", "Param4_1", "Param5_1", "Param6_1", 

                    //11-15
                    "Query_2", "Param1_2", "Param2_2", "Param3_2", "Param4_2", 

                    //16-17
                    "Param5_2", "Param6_2"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new FormulaSpinningNonItem()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            PercentInd = Sm.DrStr(dr, c[1]) == "Y",
                            TotalMachine = Sm.DrDec(dr, c[2]),
                            TotalInd = Sm.DrStr(dr, c[3]) == "Y",
                            Query_1 = Sm.DrStr(dr, c[4]),
                            Param1_1 = Sm.DrStr(dr, c[5]),
                            Param2_1 = Sm.DrStr(dr, c[6]),
                            Param3_1 = Sm.DrStr(dr, c[7]),
                            Param4_1 = Sm.DrStr(dr, c[8]),
                            Param5_1 = Sm.DrStr(dr, c[9]),
                            Param6_1 = Sm.DrStr(dr, c[10]),
                            Query_2 = Sm.DrStr(dr, c[11]),
                            Param1_2 = Sm.DrStr(dr, c[12]),
                            Param2_2 = Sm.DrStr(dr, c[13]),
                            Param3_2 = Sm.DrStr(dr, c[14]),
                            Param4_2 = Sm.DrStr(dr, c[15]),
                            Param5_2 = Sm.DrStr(dr, c[16]),
                            Param6_2 = Sm.DrStr(dr, c[17])
                        });
                    }
                }
                dr.Close();
            }
        }


        private void Process11(string Dt, ref List<DataMesin> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, TotalMachine MesinTotalUnit, Sum(RunningMachine) MesinJalanUnit ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, A.TotalMachine, B.RunningMachine ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblSpinningProduction1Hdr B ");
            SQL.AppendLine("        On A.WCCode=B.WCCode ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.ProdDt=@ProdDt ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, A.TotalMachine, B.RunningMachine ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblSpinningProduction2Hdr B ");
            SQL.AppendLine("        On A.WCCode=B.WCCode ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.ProdDt=@ProdDt ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By WCCode, TotalMachine ");
            SQL.AppendLine("Having Sum(RunningMachine)>0.00 ");
            SQL.AppendLine("Order By SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt", Dt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WCCode", "MesinTotalUnit", "MesinJalanUnit" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataMesin()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            MesinTotalUnit = Sm.DrDec(dr, c[1]),
                            MesinJalanUnit = Sm.DrDec(dr, c[2]),
                            MesinTotalPersen = 100m,
                            MesinJalanPersen = 0m
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                l.ForEach(x =>
                {
                    if (x.MesinTotalUnit == 0m)
                        x.MesinJalanPersen = 0m;
                    else
                        x.MesinJalanPersen = Math.Round((x.MesinJalanUnit / x.MesinTotalUnit) * 100m, 0);
                });
            }
        }

        private void Process12(string Dt, ref List<DataProduksi1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, Sum(RunningMachine) Unit, Sum(Qty) Kg ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, B.RunningMachine, B.Qty ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblSpinningProduction1Hdr B ");
            SQL.AppendLine("        On A.WCCode=B.WCCode ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.ProdDt=@ProdDt ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By WCCode ");
            SQL.AppendLine("Having Sum(RunningMachine)>0.00 Or Sum(Qty)>0.00 ");
            SQL.AppendLine("Order By SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt", Dt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WCCode", "Unit", "Kg" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataProduksi1()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            Unit = Sm.DrDec(dr, c[1]),
                            Kg = Sm.DrDec(dr, c[2]),
                            Persen = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process13(string Dt, ref List<DataEfisiensi> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, PEVCode, PEVDesc, Sum(Value) Value ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, B.OptCode As PEVCode, B.OptDesc As PEVDesc, IfNull(D.Value, 0.000) As Value ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblOption B On B.OptCat='ProdEfficiencyVar' ");
            SQL.AppendLine("    Left Join TblSpinningProduction1Hdr C ");
            SQL.AppendLine("        On A.WCCode=C.WCCode ");
            SQL.AppendLine("        And C.CancelInd='N' ");
            SQL.AppendLine("        And C.ProdDt=@ProdDt ");
            SQL.AppendLine("    Left Join TblSpinningProduction1Dtl D ");
            SQL.AppendLine("        On C.DocNo=D.DocNo ");
            SQL.AppendLine("        And B.OptCode=D.ProdEfficiencyVar ");
            SQL.AppendLine("    Where A.ProdDataPerItemInd='N' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By WCCode, PEVCode, PEVDesc ");
            SQL.AppendLine("Order By SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt", Dt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WCCode", "PEVCode", "PEVDesc", "Value" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataEfisiensi()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            PEVCode = Sm.DrStr(dr, c[1]),
                            PEVDesc = Sm.DrStr(dr, c[2]),
                            Value = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process14(
            decimal WorkingDays,
            ref List<WCSpinning> l,
            ref List<DataProduksi1> l2,
            ref List<FormulaSpinningNonItem> l3,
            ref List<ProdEfficiencyVar> l4,
            ref List<DataEfisiensi> l5
            )
        {
            var lFormulaParam = new List<FormulaParam>();

            foreach (var i in l.Where(w => !w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                foreach (var j in l2.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                {
                    foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode) && w.PercentInd && !w.TotalInd))
                    {
                        lFormulaParam.Clear();
                        for (int x = 1; x <= 6; x++)
                        {
                            lFormulaParam.Add(new FormulaParam
                            {
                                ParamNo = "Param" + x.ToString(),
                                ParamDesc = k.Param1_1,
                                Value = 0m
                            });
                        }

                        foreach (var f in lFormulaParam)
                        {
                            if (f.ParamDesc.Length > 0)
                            {
                                if (Sm.CompareStr(f.ParamDesc, "Qty"))
                                    f.Value = j.Kg;
                                else
                                {
                                    if (Sm.CompareStr(f.ParamDesc, "Total Machine"))
                                        f.Value = i.TotalMachine;
                                    else
                                    {
                                        if (Sm.CompareStr(f.ParamDesc, "Working Days"))
                                            f.Value = WorkingDays;
                                        else
                                        {
                                            foreach (var o in l4.Where(w => Sm.CompareStr(f.ParamDesc, w.PEVDesc)))
                                            {
                                                foreach (var e in l5.Where(w =>
                                                        Sm.CompareStr(i.WCCode, w.WCCode) &&
                                                        Sm.CompareStr(o.PEVCode, w.PEVCode)))
                                                    f.Value = e.Value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        j.Persen = ComputePersen1(k.Query_1, ref lFormulaParam);
                    }
                }
            }
            lFormulaParam.Clear();
        }

        private void Process15(
            string Dt,
            ref List<DataProduksi2> l,
            ref List<DataEfisiensi> l2,
            ref List<ProdEfficiencyVar> l3,
            ref List<WCSpinning> l4)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WCCode, A.ProdFormula, A.UnitInd, A.PercentInd, ");
            SQL.AppendLine("C.ItCode, D.ItName, C.RunningMachine, C.Qty, ");
            SQL.AppendLine("RSRPM, RSTPI, RSNE, RSAvgSPL, RSMFR, ");
            SQL.AppendLine("WSpeed, WNE, WDelMC, WDelRun, WMFR, WDel ");
            SQL.AppendLine("From TblWCSpinning A ");
            SQL.AppendLine("Inner Join TblSpinningProduction2Hdr B ");
            SQL.AppendLine("    On A.WCCode=B.WCCode ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.ProdDt=@ProdDt ");
            SQL.AppendLine("Inner Join TblSpinningProduction2Dtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblItemSpinning D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Order By A.SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt", Dt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "WCCode", 

                    //1-5
                    "ProdFormula", "UnitInd", "PercentInd", "ItCode", "ItName", 
                    
                    //6-10
                    "RunningMachine", "Qty", "RSRPM", "RSTPI", "RSNE", 
                    
                    //11-15
                    "RSAvgSPL", "RSMFR", "WSpeed", "WNE", "WDelMC", 
                    
                    //16-18
                    "WDelRun", "WMFR", "WDel"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataProduksi2()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            ProdFormula = Sm.DrStr(dr, c[1]),
                            UnitInd = Sm.DrStr(dr, c[2]) == "Y",
                            PercentInd = Sm.DrStr(dr, c[3]) == "Y",
                            ItCode = Sm.DrStr(dr, c[4]),
                            ItName = Sm.DrStr(dr, c[5]),
                            Unit = Sm.DrDec(dr, c[6]),
                            Kg = Sm.DrDec(dr, c[7]),
                            Persen = 0m,
                            RSRPM = Sm.DrDec(dr, c[8]),
                            RSTPI = Sm.DrDec(dr, c[9]),
                            RSNE = Sm.DrDec(dr, c[10]),
                            RSAvgSPL = Sm.DrDec(dr, c[11]),
                            RSMFR = Sm.DrDec(dr, c[12]),
                            WSpeed = Sm.DrDec(dr, c[13]),
                            WNE = Sm.DrDec(dr, c[14]),
                            WDelMC = Sm.DrDec(dr, c[15]),
                            WDelRun = Sm.DrDec(dr, c[16]),
                            WMFR = Sm.DrDec(dr, c[17]),
                            WDel = Sm.DrDec(dr, c[18])
                        });
                    }
                }
                dr.Close();
            }
            if (l.Count > 0)
            {
                var WCCodeTemp = string.Empty;

                foreach (var x in l.OrderBy(o => o.WCCode))
                {
                    if (x.ProdFormula == "02" && x.PercentInd)
                    {
                        if ((x.Unit * x.RSMFR) != 0m)
                            x.Persen = 100m * (x.Kg / (x.Unit * x.RSMFR));
                    }

                    if (x.ProdFormula == "03" && x.PercentInd)
                    {
                        if ((x.WDel) != 0m) x.Persen = 100m * (x.Kg / x.WDel);
                    }

                    if (!Sm.CompareStr(x.WCCode, WCCodeTemp))
                    {
                        if (x.ProdFormula == "02")
                        {
                            if (x.UnitInd && x.PercentInd)
                            {
                                var s = new List<string>(new string[] { "02", "03", "06" });

                                for (int i = 0; i <= 2; i++)
                                {
                                    l2.Add(new DataEfisiensi
                                    {
                                        WCCode = x.WCCode,
                                        PEVCode = s[i],
                                        PEVDesc = l3.FirstOrDefault(v => v.PEVCode == s[i]).PEVDesc,
                                        Value = 0m
                                    });
                                }
                            }
                        }

                        if (x.ProdFormula == "03")
                        {
                            l2.Add(new DataEfisiensi
                            {
                                WCCode = x.WCCode,
                                PEVCode = "06",
                                PEVDesc = l3.FirstOrDefault(v => v.PEVCode == "06").PEVDesc,
                                Value = 0m
                            });
                        }
                    }
                    WCCodeTemp = x.WCCode;
                }

                if (l2.Count > 0)
                {
                    decimal ValueTemp = 0m, UnitTemp = 0m, KgTemp = 0m;
                    foreach (var i in l2)
                    {
                        foreach (var j in l4.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                        {
                            if (j.ProdFormula == "02" && j.PercentInd)
                            {
                                ValueTemp = 0m;
                                UnitTemp = 0m;
                                if (i.PEVCode == "02") //RPM
                                {
                                    foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                    {
                                        ValueTemp += (x.RSRPM * x.Unit);
                                        UnitTemp += x.Unit;
                                    }
                                }

                                if (i.PEVCode == "03") //TPI (CD)
                                {
                                    foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                    {
                                        ValueTemp += (x.RSTPI * x.Unit);
                                        UnitTemp += x.Unit;
                                    }
                                }

                                if (i.PEVCode == "06") //Average Count Riil
                                {
                                    foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                    {
                                        ValueTemp += (x.RSNE * x.Unit);
                                        UnitTemp += x.Unit;
                                    }
                                }

                                if (UnitTemp != 0m) i.Value = ValueTemp / UnitTemp;
                            }

                            if (j.ProdFormula == "03")
                            {
                                ValueTemp = 0m;
                                KgTemp = 0m;

                                foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                {
                                    ValueTemp += (x.WNE * x.Kg);
                                    KgTemp += x.Kg;
                                }

                                if (KgTemp != 0m) i.Value = ValueTemp / KgTemp;
                            }
                        }

                    }
                }
            }


        }

        private void Process16(
            ref List<DataProduksi2Total> l,
            ref List<WCSpinning> l2,
            ref List<DataProduksi2> l3,
            ref List<DataEfisiensi> l4)
        {
            decimal UnitTemp = 0m, KgTemp = 0m, PersenTemp = 0m;
            foreach (var i in l2.Where(w => w.ProdDataPerItemInd))
            {
                UnitTemp = 0m;
                KgTemp = 0m;
                PersenTemp = 0m;

                foreach (var j in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                {
                    UnitTemp += j.Unit;
                    KgTemp += j.Kg;
                }

                if (i.ProdFormula == "02")
                {
                    //Ring Spinning

                    decimal RPM = 0m, TPI = 0m, AverageCountRiil = 0m, RSAvgSPL = 0m;

                    foreach (var k in l4.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                    {
                        if (k.PEVCode == "02") RPM = k.Value;
                        if (k.PEVCode == "03") TPI = k.Value;
                        if (k.PEVCode == "06") AverageCountRiil = k.Value;
                    }

                    foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode) && w.RSAvgSPL != 0m))
                    {
                        RSAvgSPL = k.RSAvgSPL;
                        break;
                    }

                    if ((840m * 36m * TPI * AverageCountRiil * 400m) != 0)
                    {
                        if ((((RPM * 60m * 24m * RSAvgSPL * UnitTemp) / (840m * 36m * TPI * AverageCountRiil * 400m)) * 181.44m) != 0)
                            PersenTemp = (KgTemp / (((RPM * 60m * 24m * RSAvgSPL * UnitTemp) / (840m * 36m * TPI * AverageCountRiil * 400m)) * 181.44m)) * 100m;
                    }

                    l.Add(new DataProduksi2Total
                    {
                        WCCode = i.WCCode,
                        ProdFormula = "02",
                        RingSpinningInd = true,
                        WinderInd = false,
                        PackingInd = false,
                        Unit = UnitTemp,
                        Kg = KgTemp,
                        Persen = PersenTemp
                    });
                }

                if (i.ProdFormula == "03")
                {
                    //Winder

                    foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                    {
                        PersenTemp += (k.Kg * k.Persen);
                    }

                    if (KgTemp != 0)
                        PersenTemp = PersenTemp / KgTemp;
                    else
                        PersenTemp = 0m;

                    l.Add(new DataProduksi2Total
                    {
                        WCCode = i.WCCode,
                        ProdFormula = "03",
                        RingSpinningInd = false,
                        WinderInd = true,
                        PackingInd = false,
                        Unit = UnitTemp,
                        Kg = KgTemp,
                        Persen = PersenTemp
                    });
                }

                if (i.ProdFormula == "01")
                {
                    //Packing

                    string WCCodeRingSpinning = string.Empty;

                    foreach (var k in l2.Where(w => w.ProdDataPerItemInd && w.ProdFormula == "02"))
                    {
                        WCCodeRingSpinning = k.WCCode;
                        break;
                    }

                    if (WCCodeRingSpinning.Length > 0)
                    {
                        foreach (var k in l2.Where(w => w.ProdDataPerItemInd && w.ProdFormula == "02"))
                        {
                            WCCodeRingSpinning = k.WCCode;
                            break;
                        }

                        foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, WCCodeRingSpinning)))
                        {
                            PersenTemp += (k.Unit * k.RSMFR);
                            break;
                        }
                    }

                    if (PersenTemp != 0m)
                        PersenTemp = (KgTemp / PersenTemp) * 100m;

                    l.Add(new DataProduksi2Total
                    {
                        WCCode = i.WCCode,
                        ProdFormula = "01",
                        RingSpinningInd = false,
                        WinderInd = false,
                        PackingInd = true,
                        Unit = 0m,
                        Kg = KgTemp,
                        Persen = PersenTemp
                    });
                }
            }
        }

        private void Process21(ref List<Data1> l, ref List<WCSpinning> l2)
        {
            //Judul work center
            var Title = new string[10];
            int i = 0;
            foreach (var x in l2.OrderBy(o => o.SeqNo))
            {
                Title[i] = x.WCName;
                i++;
            }
            l.Add(new Data1()
            {
                Title1 = Title[0],
                Title2 = Title[1],
                Title3 = Title[2],
                Title4 = Title[3],
                Title5 = Title[4],
                Title6 = Title[5],
                Title7 = Title[6],
                Title8 = Title[7],
                Title9 = Title[8],
                Title10 = Title[9]
            });
        }

        private void Process22(ref List<Data2> l, ref List<WCSpinning> l2, ref List<DataMesin> l3)
        {
            //Data mesin
            l.Add(new Data2()
            {
                id = "1",
                Remark = "Jumlah Mesin Total"
            });

            l.Add(new Data2()
            {
                id = "2",
                Remark = "Jumlah Mesin Berjalan"
            });

            foreach (var i in l2.OrderBy(o => o.SeqNo))
            {
                foreach (var j in l3.Where(w => Sm.CompareStr(i.WCCode, w.WCCode)))
                {
                    if (i.SeqNo == "1")
                    {
                        l[0].Unit1 = j.MesinTotalUnit;
                        l[0].Persen1 = j.MesinTotalPersen;
                        l[1].Unit1 = j.MesinJalanUnit;
                        l[1].Persen1 = j.MesinJalanPersen;
                    }
                    if (i.SeqNo == "2")
                    {
                        l[0].Unit2 = j.MesinTotalUnit;
                        l[0].Persen2 = j.MesinTotalPersen;
                        l[1].Unit2 = j.MesinJalanUnit;
                        l[1].Persen2 = j.MesinJalanPersen;
                    }
                    if (i.SeqNo == "3")
                    {
                        l[0].Unit3 = j.MesinTotalUnit;
                        l[0].Persen3 = j.MesinTotalPersen;
                        l[1].Unit3 = j.MesinJalanUnit;
                        l[1].Persen3 = j.MesinJalanPersen;
                    }
                    if (i.SeqNo == "4")
                    {
                        l[0].Unit4 = j.MesinTotalUnit;
                        l[0].Persen4 = j.MesinTotalPersen;
                        l[1].Unit4 = j.MesinJalanUnit;
                        l[1].Persen4 = j.MesinJalanPersen;
                    }
                    if (i.SeqNo == "5")
                    {
                        l[0].Unit5 = j.MesinTotalUnit;
                        l[0].Persen5 = j.MesinTotalPersen;
                        l[1].Unit5 = j.MesinJalanUnit;
                        l[1].Persen5 = j.MesinJalanPersen;
                    }

                    if (i.SeqNo == "6")
                    {
                        l[0].Unit6 = j.MesinTotalUnit;
                        l[0].Persen6 = j.MesinTotalPersen;
                        l[1].Unit6 = j.MesinJalanUnit;
                        l[1].Persen6 = j.MesinJalanPersen;
                    }
                    if (i.SeqNo == "7")
                    {
                        l[0].Unit7 = j.MesinTotalUnit;
                        l[0].Persen7 = j.MesinTotalPersen;
                        l[1].Unit7 = j.MesinJalanUnit;
                        l[1].Persen7 = j.MesinJalanPersen;
                    }
                    if (i.SeqNo == "8")
                    {
                        l[0].Unit8 = j.MesinTotalUnit;
                        l[0].Persen8 = j.MesinTotalPersen;
                        l[1].Unit8 = j.MesinJalanUnit;
                        l[1].Persen8 = j.MesinJalanPersen;
                    }
                    if (i.SeqNo == "9")
                    {
                        l[0].Unit9 = j.MesinTotalUnit;
                        l[0].Persen9 = j.MesinTotalPersen;
                        l[1].Unit9 = j.MesinJalanUnit;
                        l[1].Persen9 = j.MesinJalanPersen;
                    }
                    if (i.SeqNo == "10")
                    {
                        l[0].Unit10 = j.MesinTotalUnit;
                        l[0].Persen10 = j.MesinTotalPersen;
                        l[1].Unit10 = j.MesinJalanUnit;
                        l[1].Persen10 = j.MesinJalanPersen;
                    }
                }    
            }
        }

        private void Process23(ref List<Data3> l, ref List<WCSpinning> l2, ref List<DataProduksi1> l3)
        {
            //Data produksi

            if (l3.Count > 0)
            {
                l.Add(new Data3()
                {
                    ItemCode = "<None>",
                    ItemName = string.Empty
                });

                foreach (var i in l2.Where(w => !w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
                {
                    foreach (var j in l3.Where(w => Sm.CompareStr(i.WCCode, w.WCCode)))
                    {
                        if (i.SeqNo == "1")
                        {
                            l[0].Unit1 = j.Unit;
                            l[0].Kg1 = j.Kg;
                            l[0].Persen1 = j.Persen;
                        }
                        if (i.SeqNo == "2")
                        {
                            l[0].Unit2 = j.Unit;
                            l[0].Kg2 = j.Kg;
                            l[0].Persen2 = j.Persen;
                        }
                        if (i.SeqNo == "3")
                        {
                            l[0].Unit3 = j.Unit;
                            l[0].Kg3 = j.Kg;
                            l[0].Persen3 = j.Persen;
                        }
                        if (i.SeqNo == "4")
                        {
                            l[0].Unit4 = j.Unit;
                            l[0].Kg4 = j.Kg;
                            l[0].Persen4 = j.Persen;
                        }
                        if (i.SeqNo == "5")
                        {
                            l[0].Unit5 = j.Unit;
                            l[0].Kg5 = j.Kg;
                            l[0].Persen5 = j.Persen;
                        }

                        if (i.SeqNo == "6")
                        {
                            l[0].Unit6 = j.Unit;
                            l[0].Kg6 = j.Kg;
                            l[0].Persen6 = j.Persen;
                        }
                        if (i.SeqNo == "7")
                        {
                            l[0].Unit7 = j.Unit;
                            l[0].Kg7 = j.Kg;
                            l[0].Persen7 = j.Persen;
                        }
                        if (i.SeqNo == "8")
                        {
                            l[0].Unit8 = j.Unit;
                            l[0].Kg8 = j.Kg;
                            l[0].Persen8 = j.Persen;
                        }
                        if (i.SeqNo == "9")
                        {
                            l[0].Unit9 = j.Unit;
                            l[0].Kg9 = j.Kg;
                            l[0].Persen9 = j.Persen;
                        }
                        if (i.SeqNo == "10")
                        {
                            l[0].Unit10 = j.Unit;
                            l[0].Kg10 = j.Kg;
                            l[0].Persen10 = j.Persen;
                        }
                    }
                }
            }
        }

        private void Process24(ref List<Data3> l, ref List<WCSpinning> l2, ref List<DataProduksi2> l3)
        {
            //Data total produksi

            if (l3.Count > 0)
            {
                foreach (var i in l3.OrderBy(o => o.ItName))
                {
                    l.Add(new Data3()
                    {
                        ItemCode = i.ItCode,
                        ItemName = i.ItName 
                    });
                }

                foreach (var i in l2.Where(w =>w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
                {
                    foreach (var j in l3.Where(w => Sm.CompareStr(i.WCCode, w.WCCode)))
                    {
                        foreach (var k in l.Where(w => Sm.CompareStr(j.ItCode, w.ItemCode)))
                        {
                            if (i.SeqNo == "1")
                            {
                                k.Unit1 = j.Unit;
                                k.Kg1 = j.Kg;
                                k.Persen1 = j.Persen;
                            }
                            if (i.SeqNo == "2")
                            {
                                k.Unit2 = j.Unit;
                                k.Kg2 = j.Kg;
                                k.Persen2 = j.Persen;
                            }
                            if (i.SeqNo == "3")
                            {
                                k.Unit3 = j.Unit;
                                k.Kg3 = j.Kg;
                                k.Persen3 = j.Persen;
                            }
                            if (i.SeqNo == "4")
                            {
                                k.Unit4 = j.Unit;
                                k.Kg4 = j.Kg;
                                k.Persen4 = j.Persen;
                            }
                            if (i.SeqNo == "5")
                            {
                                k.Unit5 = j.Unit;
                                k.Kg5 = j.Kg;
                                k.Persen5 = j.Persen;
                            }
                            if (i.SeqNo == "6")
                            {
                                k.Unit6 = j.Unit;
                                k.Kg6 = j.Kg;
                                k.Persen6 = j.Persen;
                            }
                            if (i.SeqNo == "7")
                            {
                                k.Unit7 = j.Unit;
                                k.Kg7 = j.Kg;
                                k.Persen7 = j.Persen;
                            }
                            if (i.SeqNo == "8")
                            {
                                k.Unit8 = j.Unit;
                                k.Kg8 = j.Kg;
                                k.Persen8 = j.Persen;
                            }
                            if (i.SeqNo == "9")
                            {
                                k.Unit9 = j.Unit;
                                k.Kg9 = j.Kg;
                                k.Persen9 = j.Persen;
                            }
                            if (i.SeqNo == "10")
                            {
                                k.Unit10 = j.Unit;
                                k.Kg10 = j.Kg;
                                k.Persen10 = j.Persen;
                            }
                        }
                    }
                }
            }
        }

        private void Process25(ref List<Data4> l, ref List<WCSpinning> l2, ref List<DataProduksi2Total> l3)
        {
            //Data total produksi

            l.Add(new Data4()
            {
                Unit1 = 0m,
                Kg1 = 0m,
                Persen1 = 0m,
                Unit2 = 0m,
                Kg2 = 0m,
                Persen2 = 0m,
                Unit3 = 0m,
                Kg3 = 0m,
                Persen3 = 0m,
                Unit4 = 0m,
                Kg4 = 0m,
                Persen4 = 0m,
                Unit5 = 0m,
                Kg5 = 0m,
                Persen5 = 0m,
                Unit6 = 0m,
                Kg6 = 0m,
                Persen6 = 0m,
                Unit7 = 0m,
                Kg7 = 0m,
                Persen7 = 0m,
                Unit8 = 0m,
                Kg8 = 0m,
                Persen8 = 0m,
                Unit9 = 0m,
                Kg9 = 0m,
                Persen9 = 0m,
                Unit10 = 0m,
                Kg10 = 0m,
                Persen10 = 0m
            });

            if (l3.Count > 0)
            {
                foreach (var i in l2.Where(w =>!w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
                {
                    foreach (var j in l3.Where(w => Sm.CompareStr(i.WCCode, w.WCCode)))
                    {
                        if (i.SeqNo == "1")
                        {
                            l[0].Unit1 = j.Unit;
                            l[0].Kg1 = j.Kg;
                            l[0].Persen1 = j.Persen;
                        }
                        if (i.SeqNo == "2")
                        {
                            l[0].Unit2 = j.Unit;
                            l[0].Kg2 = j.Kg;
                            l[0].Persen2 = j.Persen;
                        }
                        if (i.SeqNo == "3")
                        {
                            l[0].Unit3 = j.Unit;
                            l[0].Kg3 = j.Kg;
                            l[0].Persen3 = j.Persen;
                        }
                        if (i.SeqNo == "4")
                        {
                            l[0].Unit4 = j.Unit;
                            l[0].Kg4 = j.Kg;
                            l[0].Persen4 = j.Persen;
                        }
                        if (i.SeqNo == "5")
                        {
                            l[0].Unit5 = j.Unit;
                            l[0].Kg5 = j.Kg;
                            l[0].Persen5 = j.Persen;
                        }

                        if (i.SeqNo == "6")
                        {
                            l[0].Unit6 = j.Unit;
                            l[0].Kg6 = j.Kg;
                            l[0].Persen6 = j.Persen;
                        }
                        if (i.SeqNo == "7")
                        {
                            l[0].Unit7 = j.Unit;
                            l[0].Kg7 = j.Kg;
                            l[0].Persen7 = j.Persen;
                        }
                        if (i.SeqNo == "8")
                        {
                            l[0].Unit8 = j.Unit;
                            l[0].Kg8 = j.Kg;
                            l[0].Persen8 = j.Persen;
                        }
                        if (i.SeqNo == "9")
                        {
                            l[0].Unit9 = j.Unit;
                            l[0].Kg9 = j.Kg;
                            l[0].Persen9 = j.Persen;
                        }
                        if (i.SeqNo == "10")
                        {
                            l[0].Unit10 = j.Unit;
                            l[0].Kg10 = j.Kg;
                            l[0].Persen10 = j.Persen;
                        }
                    }
                }
            }
        }

        private void Process26(
            ref List<Data5> l, 
            ref List<ProdEfficiencyVar> l2, 
            ref List<WCSpinning> l3, 
            ref List<DataEfisiensi> l4, 
            ref List<DataEfisiensi> l5)
        { 
            //Data efficiency

            foreach (var x in l2.OrderBy(o => o.PEVCode))
            {
                l.Add(new Data5()
                {
                    Id = x.PEVCode,
                    Remark = x.PEVDesc,
                    Value1 = 0m,
                    Value2 = 0m,
                    Value3 = 0m,
                    Value4 = 0m,
                    Value5 = 0m,
                    Value6 = 0m,
                    Value7 = 0m,
                    Value8 = 0m,
                    Value9 = 0m,
                    Value10 = 0m
                });
            }

            if (l4.Count > 0)
            {
                foreach (var i in l3.Where(w => !w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
                {
                    foreach (var j in l4.Where(w => Sm.CompareStr(i.WCCode, w.WCCode)))
                    {
                        if (i.SeqNo == "1")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value1 = j.Value;
                        }
                        if (i.SeqNo == "2")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value2 = j.Value;
                        }
                        if (i.SeqNo == "3")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value3 = j.Value;
                        }
                        if (i.SeqNo == "4")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value4 = j.Value;
                        }
                        if (i.SeqNo == "5")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value5 = j.Value;
                        }
                        if (i.SeqNo == "6")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value6 = j.Value;
                        }
                        if (i.SeqNo == "7")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value7 = j.Value;
                        }
                        if (i.SeqNo == "8")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value8 = j.Value;
                        }
                        if (i.SeqNo == "9")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value9 = j.Value;
                        }
                        if (i.SeqNo == "10")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value10 = j.Value;
                        }
                    }
                }
            }

            if (l5.Count > 0)
            {
                foreach (var i in l3.Where(w => w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
                {
                    foreach (var j in l5.Where(w => Sm.CompareStr(i.WCCode, w.WCCode)))
                    {
                        if (i.SeqNo == "1")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value1 = j.Value;
                        }
                        if (i.SeqNo == "2")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value2 = j.Value;
                        }
                        if (i.SeqNo == "3")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value3 = j.Value;
                        }
                        if (i.SeqNo == "4")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value4 = j.Value;
                        }
                        if (i.SeqNo == "5")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value5 = j.Value;
                        }
                        if (i.SeqNo == "6")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value6 = j.Value;
                        }
                        if (i.SeqNo == "7")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value7 = j.Value;
                        }
                        if (i.SeqNo == "8")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value8 = j.Value;
                        }
                        if (i.SeqNo == "9")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value9 = j.Value;
                        }
                        if (i.SeqNo == "10")
                        {
                            foreach (var k in l.Where(w => Sm.CompareStr(j.PEVCode, w.Id)))
                                k.Value10 = j.Value;
                        }
                    }
                }
            }

        }

        private void Process31(string Dt1, string Dt2, ref List<DataMesin> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, TotalMachine MesinTotalUnit, Sum(RunningMachine) MesinJalanUnit ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, A.TotalMachine, B.RunningMachine ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblSpinningProduction1Hdr B ");
            SQL.AppendLine("        On A.WCCode=B.WCCode ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.ProdDt>=@ProdDt1 ");
            SQL.AppendLine("        And B.ProdDt<=@ProdDt2 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, A.TotalMachine, B.RunningMachine ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblSpinningProduction2Hdr B ");
            SQL.AppendLine("        On A.WCCode=B.WCCode ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.ProdDt>=@ProdDt1 ");
            SQL.AppendLine("        And B.ProdDt<=@ProdDt2 ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By WCCode, TotalMachine ");
            SQL.AppendLine("Having Sum(RunningMachine)>0.00 ");
            SQL.AppendLine("Order By SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt1", Dt1);
            Sm.CmParamDt(ref cm, "@ProdDt2", Dt2);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WCCode", "MesinTotalUnit", "MesinJalanUnit" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataMesin()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            MesinTotalUnit = Sm.DrDec(dr, c[1]),
                            MesinJalanUnit = Sm.DrDec(dr, c[2]),
                            MesinTotalPersen = 100m,
                            MesinJalanPersen = 0m
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                l.ForEach(x =>
                {
                    if (x.MesinTotalUnit == 0m)
                        x.MesinJalanPersen = 0m;
                    else
                        x.MesinJalanPersen = Math.Round((x.MesinJalanUnit / x.MesinTotalUnit) * 100m, 0);
                });
            }
        }

        private void Process32(string Dt1, string Dt2, ref List<DataProduksi1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, Sum(RunningMachine) Unit, Sum(Qty) Kg ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, B.RunningMachine, B.Qty ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblSpinningProduction1Hdr B ");
            SQL.AppendLine("        On A.WCCode=B.WCCode ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.ProdDt>=@ProdDt1 ");
            SQL.AppendLine("        And B.ProdDt<=@ProdDt2 ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By WCCode ");
            SQL.AppendLine("Having Sum(RunningMachine)>0.00 Or Sum(Qty)>0.00 ");
            SQL.AppendLine("Order By SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt1", Dt1);
            Sm.CmParamDt(ref cm, "@ProdDt2", Dt2);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WCCode", "Unit", "Kg" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataProduksi1()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            Unit = Sm.DrDec(dr, c[1]),
                            Kg = Sm.DrDec(dr, c[2]),
                            Persen = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process33(string Dt1, string Dt2, ref List<DataEfisiensi> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, PEVCode, PEVDesc, Sum(Value) Value ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, B.OptCode As PEVCode, B.OptDesc As PEVDesc, IfNull(D.Value, 0.000) As Value ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblOption B On B.OptCat='ProdEfficiencyVar' ");
            SQL.AppendLine("    Left Join TblSpinningProduction1Hdr C ");
            SQL.AppendLine("        On A.WCCode=C.WCCode ");
            SQL.AppendLine("        And C.CancelInd='N' ");
            SQL.AppendLine("        And C.ProdDt>=@ProdDt1 ");
            SQL.AppendLine("        And C.ProdDt<=@ProdDt2 ");
            SQL.AppendLine("    Left Join TblSpinningProduction1Dtl D ");
            SQL.AppendLine("        On C.DocNo=D.DocNo ");
            SQL.AppendLine("        And B.OptCode=D.ProdEfficiencyVar ");
            SQL.AppendLine("    Where A.ProdDataPerItemInd='N' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By WCCode, PEVCode, PEVDesc ");
            SQL.AppendLine("Order By SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt1", Dt1);
            Sm.CmParamDt(ref cm, "@ProdDt2", Dt2);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WCCode", "PEVCode", "PEVDesc", "Value" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataEfisiensi()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            PEVCode = Sm.DrStr(dr, c[1]),
                            PEVDesc = Sm.DrStr(dr, c[2]),
                            Value = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process34(
            decimal WorkingDays,
            ref List<WCSpinning> l,
            ref List<DataProduksi1> l2,
            ref List<FormulaSpinningNonItem> l3,
            ref List<ProdEfficiencyVar> l4,
            ref List<DataEfisiensi> l5
            )
        {
            var lFormulaParam = new List<FormulaParam>();

            foreach (var i in l.Where(w => !w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                foreach (var j in l2.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                {
                    foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode) && w.PercentInd && !w.TotalInd))
                    {
                        lFormulaParam.Clear();
                        for (int x = 1; x <= 6; x++)
                        {
                            lFormulaParam.Add(new FormulaParam
                            {
                                ParamNo = "Param" + x.ToString(),
                                ParamDesc = k.Param1_1,
                                Value = 0m
                            });
                        }

                        foreach (var f in lFormulaParam)
                        {
                            if (f.ParamDesc.Length > 0)
                            {
                                if (Sm.CompareStr(f.ParamDesc, "Qty"))
                                    f.Value = j.Kg;
                                else
                                {
                                    if (Sm.CompareStr(f.ParamDesc, "Total Machine"))
                                        f.Value = i.TotalMachine;
                                    else
                                    {
                                        if (Sm.CompareStr(f.ParamDesc, "Working Days"))
                                            f.Value = WorkingDays;
                                        else
                                        {
                                            foreach (var o in l4.Where(w => Sm.CompareStr(f.ParamDesc, w.PEVDesc)))
                                            {
                                                foreach (var e in l5.Where(w =>
                                                        Sm.CompareStr(i.WCCode, w.WCCode) &&
                                                        Sm.CompareStr(o.PEVCode, w.PEVCode)))
                                                    f.Value = e.Value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        j.Persen = ComputePersen1(k.Query_1, ref lFormulaParam);
                    }
                }
            }
            lFormulaParam.Clear();
        }

        private void Process35(
            string Dt1,
            string Dt2,
            ref List<DataProduksi2> l,
            ref List<DataEfisiensi> l2,
            ref List<ProdEfficiencyVar> l3,
            ref List<WCSpinning> l4)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WCCode, A.ProdFormula, A.UnitInd, A.PercentInd, ");
            SQL.AppendLine("C.ItCode, D.ItName, C.RunningMachine, C.Qty, ");
            SQL.AppendLine("RSRPM, RSTPI, RSNE, RSAvgSPL, RSMFR, ");
            SQL.AppendLine("WSpeed, WNE, WDelMC, WDelRun, WMFR, WDel ");
            SQL.AppendLine("From TblWCSpinning A ");
            SQL.AppendLine("Inner Join TblSpinningProduction2Hdr B ");
            SQL.AppendLine("    On A.WCCode=B.WCCode ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.ProdDt>=@ProdDt1 ");
            SQL.AppendLine("    And B.ProdDt<=@ProdDt2 ");
            SQL.AppendLine("Inner Join TblSpinningProduction2Dtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblItemSpinning D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Order By A.SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt1", Dt1);
            Sm.CmParamDt(ref cm, "@ProdDt2", Dt2);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "WCCode", 

                    //1-5
                    "ProdFormula", "UnitInd", "PercentInd", "ItCode", "ItName", 
                    
                    //6-10
                    "RunningMachine", "Qty", "RSRPM", "RSTPI", "RSNE", 
                    
                    //11-15
                    "RSAvgSPL", "RSMFR", "WSpeed", "WNE", "WDelMC", 
                    
                    //16-18
                    "WDelRun", "WMFR", "WDel"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataProduksi2()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            ProdFormula = Sm.DrStr(dr, c[1]),
                            UnitInd = Sm.DrStr(dr, c[2]) == "Y",
                            PercentInd = Sm.DrStr(dr, c[3]) == "Y",
                            ItCode = Sm.DrStr(dr, c[4]),
                            ItName = Sm.DrStr(dr, c[5]),
                            Unit = Sm.DrDec(dr, c[6]),
                            Kg = Sm.DrDec(dr, c[7]),
                            Persen = 0m,
                            RSRPM = Sm.DrDec(dr, c[8]),
                            RSTPI = Sm.DrDec(dr, c[9]),
                            RSNE = Sm.DrDec(dr, c[10]),
                            RSAvgSPL = Sm.DrDec(dr, c[11]),
                            RSMFR = Sm.DrDec(dr, c[12]),
                            WSpeed = Sm.DrDec(dr, c[13]),
                            WNE = Sm.DrDec(dr, c[14]),
                            WDelMC = Sm.DrDec(dr, c[15]),
                            WDelRun = Sm.DrDec(dr, c[16]),
                            WMFR = Sm.DrDec(dr, c[17]),
                            WDel = Sm.DrDec(dr, c[18])
                        });
                    }
                }
                dr.Close();
            }
            if (l.Count > 0)
            {
                var WCCodeTemp = string.Empty;

                foreach (var x in l.OrderBy(o => o.WCCode))
                {
                    if (x.ProdFormula == "02" && x.PercentInd)
                    {
                        if ((x.Unit * x.RSMFR) != 0m)
                            x.Persen = 100m * (x.Kg / (x.Unit * x.RSMFR));
                    }

                    if (x.ProdFormula == "03" && x.PercentInd)
                    {
                        if ((x.WDel) != 0m) x.Persen = 100m * (x.Kg / x.WDel);
                    }

                    if (!Sm.CompareStr(x.WCCode, WCCodeTemp))
                    {
                        if (x.ProdFormula == "02")
                        {
                            if (x.UnitInd && x.PercentInd)
                            {
                                var s = new List<string>(new string[] { "02", "03", "06" });

                                for (int i = 0; i <= 2; i++)
                                {
                                    l2.Add(new DataEfisiensi
                                    {
                                        WCCode = x.WCCode,
                                        PEVCode = s[i],
                                        PEVDesc = l3.FirstOrDefault(v => v.PEVCode == s[i]).PEVDesc,
                                        Value = 0m
                                    });
                                }
                            }
                        }

                        if (x.ProdFormula == "03")
                        {
                            l2.Add(new DataEfisiensi
                            {
                                WCCode = x.WCCode,
                                PEVCode = "06",
                                PEVDesc = l3.FirstOrDefault(v => v.PEVCode == "06").PEVDesc,
                                Value = 0m
                            });
                        }
                    }
                    WCCodeTemp = x.WCCode;
                }

                if (l2.Count > 0)
                {
                    decimal ValueTemp = 0m, UnitTemp = 0m, KgTemp = 0m;
                    foreach (var i in l2)
                    {
                        foreach (var j in l4.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                        {
                            if (j.ProdFormula == "02" && j.PercentInd)
                            {
                                ValueTemp = 0m;
                                UnitTemp = 0m;
                                if (i.PEVCode == "02") //RPM
                                {
                                    foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                    {
                                        ValueTemp += (x.RSRPM * x.Unit);
                                        UnitTemp += x.Unit;
                                    }
                                }

                                if (i.PEVCode == "03") //TPI (CD)
                                {
                                    foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                    {
                                        ValueTemp += (x.RSTPI * x.Unit);
                                        UnitTemp += x.Unit;
                                    }
                                }

                                if (i.PEVCode == "06") //Average Count Riil
                                {
                                    foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                    {
                                        ValueTemp += (x.RSNE * x.Unit);
                                        UnitTemp += x.Unit;
                                    }
                                }

                                if (UnitTemp != 0m) i.Value = ValueTemp / UnitTemp;
                            }

                            if (j.ProdFormula == "03")
                            {
                                ValueTemp = 0m;
                                KgTemp = 0m;

                                foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                {
                                    ValueTemp += (x.WNE * x.Kg);
                                    KgTemp += x.Kg;
                                }

                                if (KgTemp != 0m) i.Value = ValueTemp / KgTemp;
                            }
                        }

                    }
                }
            }


        }

        private void Process36(
            ref List<DataProduksi2Total> l,
            ref List<WCSpinning> l2,
            ref List<DataProduksi2> l3,
            ref List<DataEfisiensi> l4)
        {
            decimal UnitTemp = 0m, KgTemp = 0m, PersenTemp = 0m;
            foreach (var i in l2.Where(w => w.ProdDataPerItemInd))
            {
                UnitTemp = 0m;
                KgTemp = 0m;
                PersenTemp = 0m;

                foreach (var j in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                {
                    UnitTemp += j.Unit;
                    KgTemp += j.Kg;
                }

                if (i.ProdFormula == "02")
                {
                    //Ring Spinning

                    decimal RPM = 0m, TPI = 0m, AverageCountRiil = 0m, RSAvgSPL = 0m;

                    foreach (var k in l4.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                    {
                        if (k.PEVCode == "02") RPM = k.Value;
                        if (k.PEVCode == "03") TPI = k.Value;
                        if (k.PEVCode == "06") AverageCountRiil = k.Value;
                    }

                    foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode) && w.RSAvgSPL != 0m))
                    {
                        RSAvgSPL = k.RSAvgSPL;
                        break;
                    }

                    if ((840m * 36m * TPI * AverageCountRiil * 400m) != 0)
                    {
                        if ((((RPM * 60m * 24m * RSAvgSPL * UnitTemp) / (840m * 36m * TPI * AverageCountRiil * 400m)) * 181.44m) != 0)
                            PersenTemp = (KgTemp / (((RPM * 60m * 24m * RSAvgSPL * UnitTemp) / (840m * 36m * TPI * AverageCountRiil * 400m)) * 181.44m)) * 100m;
                    }

                    l.Add(new DataProduksi2Total
                    {
                        WCCode = i.WCCode,
                        ProdFormula = "02",
                        RingSpinningInd = true,
                        WinderInd = false,
                        PackingInd = false,
                        Unit = UnitTemp,
                        Kg = KgTemp,
                        Persen = PersenTemp
                    });
                }

                if (i.ProdFormula == "03")
                {
                    //Winder

                    foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                    {
                        PersenTemp += (k.Kg * k.Persen);
                    }

                    if (KgTemp != 0)
                        PersenTemp = PersenTemp / KgTemp;
                    else
                        PersenTemp = 0m;

                    l.Add(new DataProduksi2Total
                    {
                        WCCode = i.WCCode,
                        ProdFormula = "03",
                        RingSpinningInd = false,
                        WinderInd = true,
                        PackingInd = false,
                        Unit = UnitTemp,
                        Kg = KgTemp,
                        Persen = PersenTemp
                    });
                }

                if (i.ProdFormula == "01")
                {
                    //Packing

                    string WCCodeRingSpinning = string.Empty;

                    foreach (var k in l2.Where(w => w.ProdDataPerItemInd && w.ProdFormula == "02"))
                    {
                        WCCodeRingSpinning = k.WCCode;
                        break;
                    }

                    if (WCCodeRingSpinning.Length > 0)
                    {
                        foreach (var k in l2.Where(w => w.ProdDataPerItemInd && w.ProdFormula == "02"))
                        {
                            WCCodeRingSpinning = k.WCCode;
                            break;
                        }

                        foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, WCCodeRingSpinning)))
                        {
                            PersenTemp += (k.Unit * k.RSMFR);
                            break;
                        }
                    }

                    if (PersenTemp != 0m)
                        PersenTemp = (KgTemp / PersenTemp) * 100m;

                    l.Add(new DataProduksi2Total
                    {
                        WCCode = i.WCCode,
                        ProdFormula = "01",
                        RingSpinningInd = false,
                        WinderInd = false,
                        PackingInd = true,
                        Unit = 0m,
                        Kg = KgTemp,
                        Persen = PersenTemp
                    });
                }
            }
        }

        private void ParPrint(
            ref List<Data1> l1, 
            ref List<Data2> l2h, 
            ref List<Data2> l2a, 
            ref List<Data3> l3h, 
            ref List<Data3> l3a, 
            ref List<Data4> l4h, 
            ref List<Data4> l4a, 
            ref List<Data5> l5, 
            ref List<Data6> l6 
            )
        {
            var l = new List<DataHdr>();
            
            string[] TableName = { "DataHdr", "Data1", "Data2h", "Data2a", "Data3h", "Data3a", "Data4h", "Data4a", "Data5", "Data6" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper5') As 'CompanyEmail',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
            SQL.AppendLine("DocNO, IfNull(DATE_FORMAT(A.DocDt, '%d %M %Y'), '') As DocDt   ");
            SQL.AppendLine("From tblwastespinninghdr A ");
            SQL.AppendLine("Where A.Docno = '0002/KSM//05/18' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyEmail",
                         
                         //6-10
                         "CompanyFax",
                         "Shipper1",
                         "Shipper2",
                         "Shipper3",
                         "Shipper4",
                         
                         //11-15
                         "CompLocation2",
                         "DocNo",
                         "DocDt",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyEmail = Sm.DrStr(dr, c[5]),

                            CompanyFax = Sm.DrStr(dr, c[6]),
                            Shipper1 = Sm.DrStr(dr, c[7]),
                            Shipper2 = Sm.DrStr(dr, c[8]),
                            Shipper3 = Sm.DrStr(dr, c[9]),
                            Shipper4 = Sm.DrStr(dr, c[10]),

                            CompLocation2 = Sm.DrStr(dr, c[11]),
                            DocNo = Sm.DrStr(dr, c[12]),
                            DocDt = Sm.DrStr(dr, c[13]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region data 1
            myLists.Add(l1);
            #endregion

            #region data 2 harian
            myLists.Add(l2h);
            #endregion

            #region data 2 akumulasi
            myLists.Add(l2a);
            #endregion

            #region data 3 harian
            myLists.Add(l3h);
            #endregion

            #region data 3 akumulasi
            myLists.Add(l3a);
            #endregion

            #region data 4 harian
            myLists.Add(l4h);
            #endregion

            #region data 4 akumulasi
            myLists.Add(l4a);
            #endregion

            #region data 5
            myLists.Add(l5);
            #endregion

            #region data 6

            myLists.Add(l6);
            
            #endregion

            Sm.PrintReport("Spinning2", myLists, TableName, false);
        }

        private decimal ComputePersen1(string SQL, ref List<FormulaParam> l)
        {
            var cm = new MySqlCommand() { CommandText = string.Concat("Select ", SQL, ";") };
            foreach (var x in l)
                if (x.ParamDesc.Length > 0) Sm.CmParam<Decimal>(ref cm, x.ParamNo, x.Value);

            return Sm.GetValueDec(cm);
        }

        #endregion

        #region New

        #region For Print

        private class Data1 // Judul Work Center (Carding, Drawing Breaker, dll)
        {
            //digunakan di harian dan akumulasi
            public string Title1 { get; set; } // Carding
            public string Title2 { get; set; } // Drawing Breaker
            public string Title3 { get; set; } // Omega Lap
            public string Title4 { get; set; }
            public string Title5 { get; set; }
            public string Title6 { get; set; }
            public string Title7 { get; set; }
            public string Title8 { get; set; }
            public string Title9 { get; set; }
            public string Title10 { get; set; }
        }

        private class Data2 // Data mesin
        {
            //digunakan di harian dan akumulasi
            public string id { get; set; } //1=Jumlah Mesin Total 2=Jumlah Mesin Jalan
            public string Remark { get; set; } //untuk id 1 isinya "Jumlah Mesin Total" untuk id 2 isinya "Jumlah Mesin Jalan"

            public decimal Unit1 { get; set; }
            public decimal Persen1 { get; set; }

            public decimal Unit2 { get; set; }
            public decimal Persen2 { get; set; }

            public decimal Unit3 { get; set; }
            public decimal Persen3 { get; set; }

            public decimal Unit4 { get; set; }
            public decimal Persen4 { get; set; }

            public decimal Unit5 { get; set; }
            public decimal Persen5 { get; set; }

            public decimal Unit6 { get; set; }
            public decimal Persen6 { get; set; }

            public decimal Unit7 { get; set; }
            public decimal Persen7 { get; set; }

            public decimal Unit8 { get; set; }
            public decimal Persen8 { get; set; }

            public decimal Unit9 { get; set; }
            public decimal Persen9 { get; set; }

            public decimal Unit10 { get; set; }
            public decimal Persen10 { get; set; }
        }

        private class Data3 // Data Produksi
        {
            //digunakan di harian dan akumulasi
            public string ItemCode { get; set; }
            public string ItemName { get; set; }

            public decimal Unit1 { get; set; }
            public decimal Kg1 { get; set; }
            public decimal Persen1 { get; set; }

            public decimal Unit2 { get; set; }
            public decimal Kg2 { get; set; }
            public decimal Persen2 { get; set; }

            public decimal Unit3 { get; set; }
            public decimal Kg3 { get; set; }
            public decimal Persen3 { get; set; }

            public decimal Unit4 { get; set; }
            public decimal Kg4 { get; set; }
            public decimal Persen4 { get; set; }

            public decimal Unit5 { get; set; }
            public decimal Kg5 { get; set; }
            public decimal Persen5 { get; set; }

            public decimal Unit6 { get; set; }
            public decimal Kg6 { get; set; }
            public decimal Persen6 { get; set; }

            public decimal Unit7 { get; set; }
            public decimal Kg7 { get; set; }
            public decimal Persen7 { get; set; }

            public decimal Unit8 { get; set; }
            public decimal Kg8 { get; set; }
            public decimal Persen8 { get; set; }

            public decimal Unit9 { get; set; }
            public decimal Kg9 { get; set; }
            public decimal Persen9 { get; set; }

            public decimal Unit10 { get; set; }
            public decimal Kg10 { get; set; }
            public decimal Persen10 { get; set; }
        }

        private class Data4 // Data Produksi (Total)
        {
            //digunakan di harian dan akumulasi
            public decimal Unit1 { get; set; }
            public decimal Kg1 { get; set; }
            public decimal Persen1 { get; set; }

            public decimal Unit2 { get; set; }
            public decimal Kg2 { get; set; }
            public decimal Persen2 { get; set; }

            public decimal Unit3 { get; set; }
            public decimal Kg3 { get; set; }
            public decimal Persen3 { get; set; }

            public decimal Unit4 { get; set; }
            public decimal Kg4 { get; set; }
            public decimal Persen4 { get; set; }

            public decimal Unit5 { get; set; }
            public decimal Kg5 { get; set; }
            public decimal Persen5 { get; set; }

            public decimal Unit6 { get; set; }
            public decimal Kg6 { get; set; }
            public decimal Persen6 { get; set; }

            public decimal Unit7 { get; set; }
            public decimal Kg7 { get; set; }
            public decimal Persen7 { get; set; }

            public decimal Unit8 { get; set; }
            public decimal Kg8 { get; set; }
            public decimal Persen8 { get; set; }

            public decimal Unit9 { get; set; }
            public decimal Kg9 { get; set; }
            public decimal Persen9 { get; set; }

            public decimal Unit10 { get; set; }
            public decimal Kg10 { get; set; }
            public decimal Persen10 { get; set; }
        }

        private class Data5 // Data Efisiensi
        {
            public string Id { get; set; } //untuk mengurutkan
            public string Remark { get; set; } //Speed (CD), RPM, TPI (CD), dll
            public decimal Value1 { get; set; }
            public decimal Value2 { get; set; }
            public decimal Value3 { get; set; }
            public decimal Value4 { get; set; }
            public decimal Value5 { get; set; }
            public decimal Value6 { get; set; }
            public decimal Value7 { get; set; }
            public decimal Value8 { get; set; }
            public decimal Value9 { get; set; }
            public decimal Value10 { get; set; }
        }

        private class Data6 // Keterangan dokumen paling bawah (Produksi 100% RSF, AKM PRODUKSI  RSF, dll)
        {
            public string Id { get; set; } //untuk mengurutkan
            public string Remark { get; set; } //Produksi 100% RSF, AKM PRODUKSI  RSF, dll
            public decimal Value { get; set; } //nilainya
            public string Uom { get; set; } //Satuan
        }

        private class DataHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyEmail { get; set; }
            public string CompanyFax { get; set; }
            public string CompLocation2 { get; set; }
            public string Shipper1 { get; set; }
            public string Shipper2 { get; set; }
            public string Shipper3 { get; set; }
            public string Shipper4 { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string PrintBy { get; set; }
        }

        #endregion

        #region For Process

        private class WCSpinning
        {
            public string WCCode { get; set; }
            public string WCName { get; set; }
            public bool UnitInd { get; set; }
            public bool PercentInd { get; set; }
            public bool ProdDataPerItemInd { get; set; }
            public decimal TotalMachine { get; set; }
            public string SeqNo { get; set; }
            public string ProdFormula { get; set; }
            public string FSNICode1 { get; set; }
            public string FSNICode2 { get; set; }
        }

        private class DataMesin
        {
            public string WCCode { get; set; }
            public decimal MesinTotalUnit { get; set; }
            public decimal MesinTotalPersen { get; set; }
            public decimal MesinJalanUnit { get; set; }
            public decimal MesinJalanPersen { get; set; }
        }

        private class DataProduksi1
        {
            public string WCCode { get; set; }
            public decimal Unit { get; set; }
            public decimal Kg { get; set; }
            public decimal Persen { get; set; }
        }

        private class ProdEfficiencyVar
        {
            public string PEVCode { get; set; }
            public string PEVDesc { get; set; }
        }

        private class DataEfisiensi
        {
            public string WCCode { get; set; }
            public string PEVCode { get; set; }
            public string PEVDesc { get; set; }
            public decimal Value { get; set; }
        }

        private class FormulaSpinningNonItem
        {
            public string WCCode { get; set; }
            public bool PercentInd { get; set; }
            public decimal TotalMachine { get; set; }
            public bool TotalInd { get; set; }
            public string Query_1 { get; set; }
            public string Param1_1 { get; set; }
            public string Param2_1 { get; set; }
            public string Param3_1 { get; set; }
            public string Param4_1 { get; set; }
            public string Param5_1 { get; set; }
            public string Param6_1 { get; set; }
            public string Query_2 { get; set; }
            public string Param1_2 { get; set; }
            public string Param2_2 { get; set; }
            public string Param3_2 { get; set; }
            public string Param4_2 { get; set; }
            public string Param5_2 { get; set; }
            public string Param6_2 { get; set; }
        }

        private class FormulaParam
        {
            public string ParamNo { get; set; }
            public string ParamDesc { get; set; }
            public decimal Value { get; set; }
        }

        private class DataProduksi2
        {
            public string WCCode { get; set; }
            public string ProdFormula { get; set; }
            public bool UnitInd { get; set; }
            public bool PercentInd { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Unit { get; set; }
            public decimal Kg { get; set; }
            public decimal Persen { get; set; }
            public decimal RSRPM { get; set; }
            public decimal RSTPI { get; set; }
            public decimal RSNE { get; set; }
            public decimal RSAvgSPL { get; set; }
            public decimal RSMFR { get; set; }
            public decimal WSpeed { get; set; }
            public decimal WNE { get; set; }
            public decimal WDelMC { get; set; }
            public decimal WDelRun { get; set; }
            public decimal WMFR { get; set; }
            public decimal WDel { get; set; }
        }

        private class DataProduksi2Total
        {
            public string WCCode { get; set; }
            public string ProdFormula { get; set; }
            public bool RingSpinningInd { get; set; }
            public bool WinderInd { get; set; }
            public bool PackingInd { get; set; }
            public decimal Unit { get; set; }
            public decimal Kg { get; set; }
            public decimal Persen { get; set; }
        }

        private class WCGrd
        {
            public string WCCode { get; set; }
            public int No { get; set; }
        }

        private class PEVGrd
        {
            public string PEVCode { get; set; }
            public int No { get; set; }
        }

        #endregion

        #endregion
    }
}
