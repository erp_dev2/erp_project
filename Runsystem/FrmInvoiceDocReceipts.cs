﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvoiceDocReceipts : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmInvoiceDocReceiptsFind FrmFind;

        #endregion

        #region Constructor

        public FrmInvoiceDocReceipts(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetGrd();
            SetLueVdCode(ref LueVdCode, "");
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Purchase"+Environment.NewLine+"Invoice Number",
                        "",

                        //6-10
                        "Purchase Invoice"+Environment.NewLine+"Invoice Date",
                        "Vendor's"+Environment.NewLine+"Invoice Number",
                        "Vendor's"+Environment.NewLine+"Invoice Date",
                        "Currency",
                        "Amount"+Environment.NewLine+"Without Tax",
                        
                        //11-14
                        "Tax"+Environment.NewLine+"Invoice Number",
                        "Currency",
                        "Tax",
                        "Amount"+Environment.NewLine+"With Tax"
                    },
                     new int[] 
                    {
                       
                        //0
                        0, 

                        //1-5
                        50, 0, 20, 130, 20,  
                        
                        //6-10
                        100, 130, 100, 70, 120,

                        //11-14
                        120, 70, 80, 120 
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1,2 });
            Sm.GrdColButton(Grd1, new int[] { 3, 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 13, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 6, 12, 13 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueVdCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueVdCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueVdCode, MeeRemark
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 12, 13, 14 });
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmInvoiceDocReceiptsDlg(this, Sm.GetLue(LueVdCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 13, 14 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
                Sm.FormShowDialog(new FrmInvoiceDocReceiptsDlg(this, Sm.GetLue(LueVdCode)));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInvoiceDocReceiptsFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            SetLueVdCode(ref LueVdCode, "");
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<InvDocRecHdr>();
            var ldtl = new List<InvDocRecDtl>();
            var ldtl2 = new List<InvDocRecDtl2>();
            var ldtl3 = new List<InvDocRecDtl3>();
            var ldtl4 = new List<InvDocRecDtl4>();

            string[] TableName = { "InvDocRecHdr", "InvDocRecDtl", "InvDocRecDtl2", "InvDocRecDtl3", "InvDocRecDtl4" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='IsPOSplitBasedOnTax') As 'Kop', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.VdName, B.Address, A.Remark, C.UserName As CreateBy ");
            SQL.AppendLine("From TblInvoiceDocReceiptsHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("Inner Join TblUser C On A.CreateBy = C.UserCode");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",
                         "VdName",

                         //6-10
                         "Address" ,
                         "Remark",
                         "CreateBy",
                         "CompanyLogo",
                         "Kop"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new InvDocRecHdr()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),
                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyPhone = Sm.DrStr(dr, c[2]),

                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            VdName = Sm.DrStr(dr, c[5]),
                            VdAddress = Sm.DrStr(dr, c[6]),
                            Remark = Sm.DrStr(dr, c[7]),
                            CreateBy = Sm.DrStr(dr, c[8]),
                            CompanyLogo = Sm.DrStr(dr, c[9]),
                            Kop = Sm.DrStr(dr, c[10]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select T1.DNo, T1.CancelInd, T1.PurchaseInvoiceDocNo, DATE_FORMAT(T2.DocDt,'%d %M %Y') As DocDt, T2.VdInvNo, DATE_FORMAT(T2.VdInvDt, '%d %M %Y') As VdInvDt, DATE_FORMAT(T2.DueDt,'%d %M %Y') As DueDt, ");
                SQLDtl.AppendLine("T2.Amt, T2.TaxInvoiceNo, T2.Remark, T2.TaxCurCode, T2.Amt As Total,   ");
                SQLDtl.AppendLine("( ");
                SQLDtl.AppendLine("    Select E.CurCode ");
                SQLDtl.AppendLine("    From TblPurchaseInvoiceDtl A ");
                SQLDtl.AppendLine("    Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
                SQLDtl.AppendLine("    Inner Join TblPODtl C On B.PODocNo=C.DocNo And B.PODNo=C.DNo ");
                SQLDtl.AppendLine("    Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
                SQLDtl.AppendLine("    Inner Join TblQtHdr E On D.QtDocNo=E.DocNo  ");
                SQLDtl.AppendLine("    Where A.DocNo=T1.PurchaseInvoiceDocNo ");
                SQLDtl.AppendLine("    Limit 1 ");
                SQLDtl.AppendLine(") As CurCode, ");
                SQLDtl.AppendLine("( ");
                SQLDtl.AppendLine("	Select K.DtName ");
                SQLDtl.AppendLine("    From TblPurchaseInvoiceDtl A ");
                SQLDtl.AppendLine("    Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
                SQLDtl.AppendLine("    Inner Join TblPODtl C On B.PODocNo=C.DocNo And B.PODNo=C.DNo ");
                SQLDtl.AppendLine("    Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
                SQLDtl.AppendLine("    Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
                SQLDtl.AppendLine("    Left Join TblDeliveryType K On E.DTCode = K.DtCode ");
                SQLDtl.AppendLine("    Where A.DocNo=T1.PurchaseInvoiceDocNo ");
                SQLDtl.AppendLine("    Limit 1 ");
                SQLDtl.AppendLine(") As DtName, T2.TaxCode1 ");
                SQLDtl.AppendLine("From TblInvoiceDocReceiptsDtl T1 ");
                SQLDtl.AppendLine("Inner Join TblPurchaseinvoiceHdr T2 On T1.PurchaseInvoiceDocNo=T2.DocNo ");
                SQLDtl.AppendLine("Where T1.DocNo=@DocNo ");


                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo" ,

                         //1-5
                         "VdInvNo",
                         "VdInvDt" ,
                         "DueDt",
                         "TaxInvoiceNo" ,
                         "Remark" ,
                         //6-9
                         "TaxCurCode",
                         "Total" ,
                         "DtName",
                         "TaxCode1",
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new InvDocRecDtl()
                        {
                            Invoice = Sm.DrStr(drDtl, cDtl[1]),
                            InvDt = Sm.DrStr(drDtl, cDtl[2]),
                            DueDt = Sm.DrStr(drDtl, cDtl[3]),
                            InvTax = Sm.DrStr(drDtl, cDtl[4]),
                            Remark = Sm.DrStr(drDtl, cDtl[5]),
                            CurCode = Sm.DrStr(drDtl, cDtl[6]),
                            Total = Sm.DrDec(drDtl, cDtl[7]),
                            DtName = Sm.DrStr(drDtl, cDtl[8]),
                            TaxCode1 = Sm.DrStr(drDtl, cDtl[9]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail data 2
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select D.TaxCurCode, (D.TaxAmt*D.TaxRateAmt) As TotalTax  ");
                SQLDtl2.AppendLine("From TblInvoiceDocReceiptsHdr A ");
                SQLDtl2.AppendLine("Inner Join TblInvoiceDocReceiptsDtl C On A.DocNo = C.DocNo ");
                SQLDtl2.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
                SQLDtl2.AppendLine("Inner Join TblPurchaseInvoiceHdr D On C.PurchaseInvoiceDocNo = D.DocNo And D.CancelInd = 'N' ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0-1
                         "TaxCurCode" ,
                         "TotalTax",
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new InvDocRecDtl2()
                        {
                            TaxCurCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            TotalTax = Sm.DrDec(drDtl2, cDtl2[1]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);

            #endregion

            #region Detail data 3
            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select * From( ");
                SQLDtl3.AppendLine("Select Distinct C.DNo, D.VdInvNo, ");
                SQLDtl3.AppendLine("DATE_FORMAT(D.VdInvDt,'%d %M %Y') As VdInvDt, DATE_FORMAT(D.DueDt,'%d %M %Y') As DueDt, ");
                SQLDtl3.AppendLine("D.TaxInvoiceNo, D.Remark, D.TaxCurCode, T1.CurCode As AmtCurCode, ");
                SQLDtl3.AppendLine("D.Amt As TotalAmt, (D.TaxAmt*D.TaxRateAmt) As TotalTax, D.DocNo, C.CancelInd, T1.DtName, D.TaxCode1  ");
                SQLDtl3.AppendLine("From TblInvoiceDocReceiptsHdr A ");
                SQLDtl3.AppendLine("Inner Join TblInvoiceDocReceiptsDtl C On A.DocNo = C.DocNo ");
                SQLDtl3.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
                SQLDtl3.AppendLine("Inner Join TblPurchaseInvoiceHdr D On C.PurchaseInvoiceDocNo = D.DocNo ");
                SQLDtl3.AppendLine("Left Join ");
                SQLDtl3.AppendLine(" (Select IfNull(G.CurCode, '') As CurCode, A.DocNo, I.DtName ");
                SQLDtl3.AppendLine(" From TblPurchaseInvoiceDtl A  ");
                SQLDtl3.AppendLine("  Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
                SQLDtl3.AppendLine("  Left Join TblPOHdr C On B.PODocNo=C.DocNo  ");
                SQLDtl3.AppendLine("  Left Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo ");
                SQLDtl3.AppendLine("  Left Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
                SQLDtl3.AppendLine("  Left Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo  ");
                SQLDtl3.AppendLine("  Left Join TblQtHdr G On E.QtDocNo=G.DocNo ");
                SQLDtl3.AppendLine("  Left Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
                SQLDtl3.AppendLine("  Left join TblDeliveryType I On G.DtCode = I.DtCode ");
                SQLDtl3.AppendLine("  Where A.DocNo In (Select PurchaseInvoiceDocNo From ");
                SQLDtl3.AppendLine("  TblInvoiceDocReceiptsDtl Where DocNo=@DocNo) ");
                SQLDtl3.AppendLine("  ) T1 On T1.DocNo = D.DocNo ");
                SQLDtl3.AppendLine(") G1 ");
                SQLDtl3.AppendLine("Where G1.DocNo In (Select PurchaseInvoiceDocNo From ");
                SQLDtl3.AppendLine("TblInvoiceDocReceiptsDtl Where DocNo=@DocNo And CancelInd = 'N') Group By DocNo ");

                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                        {
                         //0
                         "DNo" ,

                         //1-5
                         "VdInvNo",
                         "VdInvDt" ,
                         "DueDt",
                         "TaxInvoiceNo" ,
                         "Remark" ,
                         //6-10
                         "TaxCurCode",
                         "AmtCurCode",
                         "TotalAmt",
                         "TotalTax",
                         "DtName",
                         //11
                         "TaxCode1"
                        });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new InvDocRecDtl3()
                        {
                            Invoice = Sm.DrStr(drDtl3, cDtl3[1]),
                            InvDt = Sm.DrStr(drDtl3, cDtl3[2]),
                            DueDt = Sm.DrStr(drDtl3, cDtl3[3]),
                            InvTax = Sm.DrStr(drDtl3, cDtl3[4]),
                            Remark = Sm.DrStr(drDtl3, cDtl3[5]),
                            TaxCurCode = Sm.DrStr(drDtl3, cDtl3[6]),
                            AmtCurCode = Sm.DrStr(drDtl3, cDtl3[7]),
                            TotalAmt = Sm.DrDec(drDtl3, cDtl3[8]),
                            TotalTax = Sm.DrDec(drDtl3, cDtl3[9]),
                            DtName = Sm.DrStr(drDtl3, cDtl3[10]),
                            TaxCode1 = Sm.DrStr(drDtl3, cDtl3[11])
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);

            #endregion

            #region Detail data 4
            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine("Select IFNull(A.TaxCurCode, '') As TaxCurCode, G1.CurCode As AmtCurCode ");
                SQLDtl4.AppendLine("From TblPurchaseInvoiceHdr A  ");
                SQLDtl4.AppendLine("Inner Join ");
                SQLDtl4.AppendLine("(Select IfNull(G.CurCode, '') As CurCode, A.DocNo ");
                SQLDtl4.AppendLine("From TblPurchaseInvoiceDtl A ");
                SQLDtl4.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
                SQLDtl4.AppendLine("Left Join TblPOHdr C On B.PODocNo=C.DocNo ");
                SQLDtl4.AppendLine("Left Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo ");
                SQLDtl4.AppendLine("Left Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
                SQLDtl4.AppendLine("Left Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
                SQLDtl4.AppendLine("Left Join TblQtHdr G On E.QtDocNo=G.DocNo ");
                SQLDtl4.AppendLine("Left Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
                SQLDtl4.AppendLine("Where A.DocNo In (Select PurchaseInvoiceDocNo From TblInvoiceDocReceiptsDtl ");
                SQLDtl4.AppendLine("Where DocNo = @DocNo) Limit 1 ) G1 On G1.DocNo = A.DocNo ");
                SQLDtl4.AppendLine("Where A.DocNo In (Select PurchaseInvoiceDocNo From TblInvoiceDocReceiptsDtl ");
                SQLDtl4.AppendLine("Where DocNo = @DocNo) And A.CancelInd = 'N' Limit 1 ");

                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "TaxCurCode" ,

                         //1-5
                         "AmtCurCode",
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {
                        ldtl4.Add(new InvDocRecDtl4()
                        {
                            TaxCurCode = Sm.DrStr(drDtl4, cDtl4[0]),
                            AmtCurCode = Sm.DrStr(drDtl4, cDtl4[1]),
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            Sm.PrintReport("InvoiceDocReceipts", myLists, TableName, false);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "InvoiceDocReceipts", "TblInvoiceDocReceiptsHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveInvoiceDocReceiptsHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveInvoiceDocReceiptsDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor")||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 purchase invoice document number.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveInvoiceDocReceiptsHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblInvoiceDocReceiptsHdr(DocNo, DocDt, VdCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @VdCode, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveInvoiceDocReceiptsDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblInvoiceDocReceiptsDtl(DocNo, DNo, CancelInd, PurchaseInvoiceDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @PurchaseInvoiceDocNo, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PurchaseInvoiceDocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            UpdateCancelledInvoiceDocReceiptsDocNo();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (IsCancelledDataNotValid(DNo) || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelInvoiceDocReceiptsDtl(TxtDocNo.Text, DNo));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledInvoiceDocReceiptsDocNo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, CancelInd ");
            SQL.AppendLine("From TblInvoiceDocReceiptsDtl  ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By DNo");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                IsCancelledInvoiceDocReceiptsNotExisted(DNo)||
                IsDataAlreadyCancelled(DNo); 
        }

        private bool IsCancelledInvoiceDocReceiptsNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 purchase invoice document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select PurchaseInvoiceDocNo ");
            SQL.AppendLine("From TblInvoiceDocReceiptsDtl ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo And DNo In (" + DNo + ") ");
            SQL.AppendLine("Limit 1 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "PurchaseInvoiceDocNo" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Sm.StdMsg(mMsgType.Warning,
                             "Purchase Invoice Document Number : " + Sm.DrStr(dr, 0) + Environment.NewLine + Environment.NewLine +
                             "This data already cancelled.");
                        return true;
                    }
                }
                dr.Close();
            }
            return false;
        }

        private MySqlCommand CancelInvoiceDocReceiptsDtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblInvoiceDocReceiptsDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo In (" + DNo + "); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowInvoiceDocReceiptsHdr(DocNo);
                ShowInvoiceDocReceiptsDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowInvoiceDocReceiptsHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, VdCode, Remark From TblInvoiceDocReceiptsHdr Where DocNo=@DocNo",
                    new string[] { "DocNo", "DocDt", "VdCode", "Remark" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        SetLueVdCode(ref LueVdCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[2]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowInvoiceDocReceiptsDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DNo, T1.CancelInd, T1.PurchaseInvoiceDocNo, T2.DocDt, T2.VdInvNo, T2.VdInvDt, ");
            SQL.AppendLine("T2.Amt, T2.TaxInvoiceNo, T2.TaxCurCode, (T2.TaxRateAmt*T2.TaxAmt) As TaxAmt, T2.Amt+T2.TaxAmt As AmtWithTax, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select E.CurCode ");
            SQL.AppendLine("    From TblPurchaseInvoiceDtl A ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblPODtl C On B.PODocNo=C.DocNo And B.PODNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
            SQL.AppendLine("    Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
            SQL.AppendLine("    Where A.DocNo=T1.PurchaseInvoiceDocNo ");
            SQL.AppendLine("    Limit 1 ");
            SQL.AppendLine(") As CurCode ");
            SQL.AppendLine("From TblInvoiceDocReceiptsDtl T1  ");
            SQL.AppendLine("Inner Join TblPurchaseinvoiceHdr T2 On T1.PurchaseInvoiceDocNo=T2.DocNo ");
            SQL.AppendLine("Where T1.DocNo=@DocNo ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "CancelInd", "PurchaseInvoiceDocNo", "DocDt", "VdInvNo", "VdInvDt", 
                    
                    //6-10
                    "CurCode", "Amt", "TaxInvoiceNo", "TaxCurCode", "TaxAmt", 
                    
                    //11
                    "AmtWithTax"  
                    
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 11);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 13, 14 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        internal string GetSelectedGetSelectedInvoiceDocReceiptsDocNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 4) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void SetLueVdCode(ref LookUpEdit Lue, string VdCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct VdCode As Col1, VdName As Col2 From ( ");
            SQL.AppendLine("    Select Distinct A.VdCode, B.VdName ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("    Inner Join TblVendor B On A.VdCode=B.VdCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocNo Not In ( ");
            SQL.AppendLine("        Select PurchaseInvoiceDocNo ");
            SQL.AppendLine("        From TblInvoiceDocReceiptsDtl ");
            SQL.AppendLine("        Where CancelInd='N') ");
            if (VdCode.Length != 0)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select VdCode, VdName From TblVendor Where VdCode='" + VdCode + "'");
            }
            SQL.AppendLine(") T Order By VdName ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        
        #endregion

        #endregion

        #region Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode), "");
            ClearGrd();
        }

        #endregion
    }

    #region Report Class

    class InvDocRecHdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string VdName { get; set; }
        public string VdAddress { get; set; }
        public string Remark { get; set; }
        public string PrintBy { get; set; }
        public string CreateBy { get; set; }
        public string Kop { get; set; }
    }

    class InvDocRecDtl
    {
        public string Invoice { get; set; }
        public string InvDt { get; set; }
        public string DueDt { get; set; }
        public string InvTax { get; set; }
        public string Remark { get; set; }
        public string CurCode { get; set; }
        public decimal Total { get; set; }
        public string DtName { get; set; }
        public string TaxCode1 { get; set; }
    }

    class InvDocRecDtl2
    {
        public string TaxCurCode { get; set; }
        public decimal TotalTax { get; set; }

    }

    class InvDocRecDtl3
    {
        public string Invoice { get; set; }
        public string InvDt { get; set; }
        public string DueDt { get; set; }
        public string InvTax { get; set; }
        public string Remark { get; set; }
        public string TaxCurCode { get; set; }
        public string AmtCurCode { get; set; }
        public decimal TotalAmt { get; set; }
        public decimal TotalTax { get; set; }
        public string DtName { get; set; }
        public string TaxCode1 { get; set; }

    }

    class InvDocRecDtl4
    {
        public string TaxCurCode { get; set; }
        public string AmtCurCode { get; set; }

    }
    
    #endregion
}
