﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmBudget mFrmParent;
        private string mBudgetBasedOn = string.Empty;
        private bool mIsFilterBySite = false;
        private bool mIsFilterByDept = false;
        
        #endregion

        #region Constructor

        public FrmBudgetFind(FrmBudget FrmParent, string BudgetBasedOn, bool IsFilterBySite, bool IsFilterByDept)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mBudgetBasedOn = BudgetBasedOn;
            mIsFilterBySite = IsFilterBySite;
            mIsFilterByDept = IsFilterByDept;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueYr(LueYr, "");
                Sl.SetLueMth(LueMth);
                if (mBudgetBasedOn == "1")
                    SetLueDeptCode(ref LueDeptCode);
                if (mBudgetBasedOn == "2")
                {
                    LblDeptCode.Text = "Site";
                    SetLueSiteCode(ref LueDeptCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Year",
                        "Code",
                        (mBudgetBasedOn=="1")?"Department":"Site",
                        "Month",
                        "Requested"+Environment.NewLine+"Amount", 

                        //6-10
                        "Budget"+Environment.NewLine+"Amount",
                        "Approved By",
                        "Approved Date",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
 
                        //11-14
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 8, 10, 13 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 8, 9, 10, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1 , true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "A.Yr", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueMth), "A.Mth", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                SQL.AppendLine("Select A.*, B.Amt As ReqAmt, ");
                if (mBudgetBasedOn=="1")
                    SQL.AppendLine("C.DeptName, ");
                if (mBudgetBasedOn == "2")
                    SQL.AppendLine("C.SiteName As DeptName, ");
                SQL.AppendLine("D.UserName ");
                SQL.AppendLine("From TblBudget A ");
                SQL.AppendLine("Inner Join TblBudgetRequest B On A.Yr=B.Yr And A.DeptCode=B.DeptCode And A.Mth=B.Mth ");
                if (mBudgetBasedOn == "1")
                    SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
                if (mBudgetBasedOn == "2")
                    SQL.AppendLine("Inner Join TblSite C On A.DeptCode=C.SiteCode ");
                SQL.AppendLine("Left Join TblUser D On A.UserCode=D.UserCode");
                SQL.AppendLine(Filter);

                if (mBudgetBasedOn == "1" && mIsFilterByDept)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='"+Gv.CurrentUserCode+"' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }

                if (mBudgetBasedOn == "2" && mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=A.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mBudgetBasedOn == "1")
                    SQL.AppendLine(" Order By A.Yr, C.DeptName, A.Mth;");
                if (mBudgetBasedOn == "2")
                    SQL.AppendLine(" Order By A.Yr, C.SiteName, A.Mth;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            "Yr", 
                            "DeptCode", "DeptName", "Mth", "ReqAmt", "Amt", 
                            "UserName", "ApprovalDt", "CreateBy", "CreateDt", "LastUpBy", 
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 11);
                        }, true, true, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 1, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                SQL.AppendLine("Where (T.ActInd='Y') ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (mIsFilterBySite) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        #endregion

        #region Event

        #region Misc Control Event

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkYr_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Year");
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkMth_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Month");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mBudgetBasedOn == "1")
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
            if (mBudgetBasedOn == "2")
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, mBudgetBasedOn == "1"?"Department":"Site");
        }

        #endregion

        #endregion
    }
}
