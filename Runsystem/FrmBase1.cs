﻿#region Update
/*
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
    20/04/2023 [WED/ALL] activity log di simpan berdasarkan parameter IsActivityLogStored
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
//using System.Runtime.InteropServices;

using Sm = RunSystem.StdMtd;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;

#endregion

namespace RunSystem
{
    public partial class FrmBase1 : Form
    {
        //[Flags]
        //enum AnimateWindowFlags
        //{
        //    AW_HOR_POSITIVE = 0x00000001,
        //    AW_HOR_NEGATIVE = 0x00000002,
        //    AW_VER_POSITIVE = 0x00000004,
        //    AW_VER_NEGATIVE = 0x00000008,
        //    AW_CENTER = 0x00000010,
        //    AW_HIDE = 0x00010000,
        //    AW_ACTIVATE = 0x00020000,
        //    AW_SLIDE = 0x00040000,
        //    AW_BLEND = 0x00080000
        //}

        //[DllImport("user32.dll")]
        //static extern bool AnimateWindow(IntPtr hWnd, int time, AnimateWindowFlags flags);

        #region Constructor

        public FrmBase1()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Form Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
        }

        virtual protected void FrmClosing(object sender, FormClosingEventArgs e)
        {

        }

        #endregion

        #region Button Method

        virtual protected void BtnFindClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnInsertClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnEditClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnDeleteClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnSaveClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnCancelClick(object sender, EventArgs e)
        {
        }

        virtual protected void BtnPrintClick(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion

        #region Event

        #region Form Event

        private void FrmBase1_Load(object sender, EventArgs e)
        {
            //var r = new Random();
            //switch (r.Next(0, 4))
            //{
            //    case 0:
            //        AnimateWindow(this.Handle, 300, AnimateWindowFlags.AW_ACTIVATE | AnimateWindowFlags.AW_HOR_POSITIVE);
            //        break;
            //    case 1:
            //        AnimateWindow(this.Handle, 300, AnimateWindowFlags.AW_SLIDE | AnimateWindowFlags.AW_VER_POSITIVE);
            //        break;
            //    case 2:
            //        AnimateWindow(this.Handle, 300, AnimateWindowFlags.AW_SLIDE | AnimateWindowFlags.AW_VER_NEGATIVE);
            //        break;
            //    case 3:
            //        AnimateWindow(this.Handle, 300, AnimateWindowFlags.AW_ACTIVATE | AnimateWindowFlags.AW_HOR_POSITIVE);
            //        break;
            //}
            FrmLoad(sender, e);
        }

        private void FrmBase1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnSave.Enabled &&
                Sm.StdMsgYN("Question",
                    "Data not been saved." + Environment.NewLine +
                    "Do you want to close application ?") == DialogResult.No)
                e.Cancel = true;
            else
                FrmClosing(sender, e);
        }

        #endregion

        #region Button Event

        private void BtnFind_Click(object sender, EventArgs e)
        {
            BtnFindClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "F";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            BtnInsertClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "I";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            BtnEditClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "E";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            BtnDeleteClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "D";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            BtnSaveClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "S";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            BtnCancelClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "C";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            BtnPrintClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "N";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        #endregion

        #endregion
    }
}
