﻿#region Update
/*
    25/05/2018 [ARI] tambah printout BOQ
    27/08/2018 [WED] ubah nama kolom : Month --> Duration, Amount --> Unit Price, Total --> Total Price
    10/09/2018 [TKG] ganti label dari total price jadi total unit, tambah total price
    09/10/2018 [HAR] 1 customer bisa banyak BOQ
    17/10/2018 [WED] Total Price di parent bisa muncul sebelum di save
    17/10/2018 [WED] parent BoQ tidak mengisi resource type, resource name, quantity, duration, uom --> default : 1
                     Total Unit = Qty * Duration
                     Parent Unit Price = Parent Total Price
                     Child Total Price = Total Unit * Unit Price
                     Parent Total Price = Sum(child Total Price)
    23/10/2018 [MEY] ubah label di print out nya, "DAFTAR KUANTITAS HARGA" --> "RINCIAN PENAWARAN HARGA"
    24/10/2018 [MEY] penomoran item BOQ saat print out dibuat urut
    08/11/2018 [HAR] Bug saat save boq dtl dan printout waktu konversi ke excel nilainya ke hide
    12/03/2019 [WED] tambah subtotal, tax, grandtotal, taxInd, parameter BOQTaxPercentage
    13/03/2019 [WED] tax di printout BOQ mengikuti tax yang ada di transaksi nya
    13/03/2019 [WED] nama dirut di print out BOQ berdasarkan parameter BOQPrintOutBODName
    27/03/2019 [HAR] print out yodya
    28/03/2019 [HAR] print out yodya untuk user diatas 10M tidak dimunculin
    04/04/2019 [MEY] Settingan aproval berdasarkan nominal angka. Misal Aproval diatas 10 Milyar
    04/04/2019 [MEY] tambah inputan direct cost, indirect cost, dan policy
    05/04/2019 [MEY] ubah posisi direct cost,indirect cost dan policy dan tambah subtotal rbps
    08/04/2019 [WED] perubahan form tanda tangan di printout BOQ
                     Kota tanda tangan, ambil dari Address master Site
                     Nama di bold & underline
                     tanggal di tiap nama penandatangan dihilangkan
                     Created By --> Dibuat oleh, Approved By --> Disetujui oleh
    16/04/2019 [DITA] Perubahan nama Subtotal RBPS & Subtotal pada BOQ
    21/05/2019 [DITA] Perubahan Print OUT BOQ (tidak menampilkan signature untuk created user)
    21/05/2019 [WED] BUG : Nominal Total di header belum ikut ter update saat Edit
    18/06/2019 [MEY] ganti field Policy menjadi Management Risk
    18/06/2019 [MEY] tambah kolom baru yaitu Percentage di sebelah grand total
    18/06/2019 [MEY] saat insert, menampilkan item yang aktif saja 
    16/07/2019 [WED] tambah validasi hanya LOP yang masih aktif yang bisa di save
    22/07/2019 [WED] tambah fasilitas Draft dan Final
    30/07/2019 [WED] BUG saat Edit Data
    31/07/2019 [DITA] tambah Bank Guarantee Indicator di BOQ
    31/07/2019 [TKG] tambah parameter IsFilterBySite
    13/08/2019 [TKG] bug saat menampilkan data contact person kosong.
    24/09/2019 [WED/IMS] validasi, kalau LOP sudah dibuat di BOQ lain, tidak boleh di simpan
    24/09/2019 [WED/YK] doctype untuk BOQ biasa adalah 1
    02/10/2019 [DITA/YK] ProcessInd = Final masih bisa edit Active Indicator
    23/10/2019 [WED/YK] tambah parameter IsBOQExcludeCancelledData
    07/11/2019 [VIN/YK] tambah parameter BOQStatusDisplayedInFind
    08/11/2019 [WED/YK] item BOQ yang di Load adalah : item aktif, atau, item non-aktif tapi terpakai
    16/12/2019 [WED/YK] Tax bisa di klik selama masih draft
    22/12/2021 [ISD/YK] tambah field approval's remark based on param IsPSModuleShowApproverRemarkInfo
    18/02/2022 [TRI/YK] Bug ketika edit kemudian save ada warning qtmth truncated
    22/03/2022 [VIN/YK] BUG show hdr belum di group by
    21/04/2022 [RIS/PRODUCT] menambah lue tax
    31/05/2022 [DITA/YK] Masih muncul warning di boq ketika param PPNDefaultSetting kosong -> ComputeTax
    13/07/2022 [RIS/PRODUCT] Menambah tab approval information
    13/07/2022 [SET/PRODUCT] Field Customer Contact Person tidak mandatory
    06/09/2022 [VIN/ALL] Bug validasi Field Customer Contact Person tidak mandatory
    06/10/2022 [IBL/PRODUCT] Kolom Qty, Duration, Total Unit untuk parentnya otomatis terkalkulasi dari childnya berdasarkan parameter ItemBOQCalculationFormat;
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBOQ : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mCtCode = string.Empty;
        internal FrmBOQFind FrmFind;
        private decimal mBOQTaxPercentage = 10m;
        private string
            mBOQPrintOutBODName = string.Empty,
            mRptName = string.Empty,
            mBOQDocType = "1",
            mPPNDefaultSetting = string.Empty,
            mItemBOQCalculationFormat = string.Empty;
        internal string mBOQStatusDisplayedInFind = String.Empty;
        private bool
            mIsBOQPrintOutShowCreatedUser = false,
            mIsPSModuleShowApproverRemarkInfo = false,
            isinsert = false,
            mIsCustomerContactPersonNonMandatory = false;
        internal bool mIsFilterBySite = false, mIsBOQExcludeCancelledData = false; 
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmBOQ(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmBOQ");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueRingCode(ref LueRingCode);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLuePtCode(ref LuePtCode);
                SetLueSPCode(ref LueSPCode);
                Sl.SetLueAgingAP(ref LueAgingAP);
                Sl.SetLueOption(ref LueProcessInd, "ProjectImplementationProcessInd");
                Sl.SetLueOption(ref LuePPN, "PPNForProject");

                TcBOQ.SelectTab("tabPage2");
                SetGrd2();
                TcBOQ.SelectTab("tabPage1");
                SetGrd();
                SetLueResourceName(ref LueResourceName);
                LueResourceName.Visible = false;
                Sl.SetLueOption(ref LueResourceType, "ResourceType");
                LueResourceType.Visible = false;


                if (!mIsPSModuleShowApproverRemarkInfo) 
                {
                    int ypoint = 21;
                    label26.Visible = MeeApprovalRemark.Visible = false;
                    label24.Top = label24.Top - ypoint; LueProcessInd.Top = LueProcessInd.Top - ypoint;
                    label9.Top = label9.Top - ypoint; TxtLOPDocNo.Top = TxtLOPDocNo.Top - ypoint; BtnLOPDocNo.Top = BtnLOPDocNo.Top - ypoint;
                    label3.Top = label3.Top - ypoint; LueCtCode.Top = LueCtCode.Top - ypoint;
                    label5.Top = label5.Top - ypoint; LueCtContactPersonName.Top = LueCtContactPersonName.Top - ypoint; BtnCtContactPersonName.Top = BtnCtContactPersonName.Top - ypoint;
                    label6.Top = label6.Top - ypoint; LueRingCode.Top = LueRingCode.Top - ypoint;
                    label16.Top = label16.Top - ypoint; LueSPCode.Top = LueSPCode.Top - ypoint;
                    label8.Top = label8.Top - ypoint; DteQtStartDt.Top = DteQtStartDt.Top - ypoint;
                    label7.Top = label7.Top - ypoint; TxtQtMth.Top = TxtQtMth.Top - ypoint;
                    label10.Top = label10.Top - ypoint; LuePtCode.Top = LuePtCode.Top - ypoint;
                    Grd2.Height = Grd2.Height + ypoint;
                }    
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mCtCode.Length != 0)
                {
                    if (IsUserAbleToInsert())
                    {
                        BtnInsertClick();
                        Sm.SetLue(LueCtCode, mCtCode);
                        Sm.SetControlReadOnly(LueCtCode, true);
                        SetLueCtPersonCode(ref LueCtContactPersonName, mCtCode, string.Empty);
                    }
                    else
                    {
                        BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                    }
                }

                if (mIsCustomerContactPersonNonMandatory)
                    label5.ForeColor = Color.Black;

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd2.Cols.Count = 23;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd2, new string[] 
                {
                    //0
                    "Item BOQ",

                    //1-5
                    "Item BOQ#",
                    "Required",
                    "ResourceTypeCode",
                    "Resource Type",
                    "ResourceNameCode",

                    //6-10
                    "Resource Name",
                    "Quantity",
                    "UoM",
                    "Duration",
                    "Total" + Environment.NewLine + "Unit",
                    
                    //11-15
                    "Unit Price",
                    "Allow",
                    "Amt",
                    "Previous Amount",
                    "Remark",

                    //16-20
                    "TotalAmt",
                    "TotalAmt2",
                    "Total Price",
                    "ParentInd",
                    "Quantity Tmp",

                    //21-22
                    "Duration Tmp",
                    "Total Unit Tmp"
                },
                new int[] 
                {
                    400,
                    150, 100, 0, 150, 0, 
                    200, 100, 100, 80, 100, 
                    150, 20, 150, 150, 200,
                    200, 200, 130, 0, 150,
                    150,150
                }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 7, 9, 10, 11, 13, 14, 16, 17, 18, 20, 21, 22 }, 0);
            Sm.GrdColCheck(Grd2, new int[] { 2 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 3, 5, 10, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22 });
            Sm.GrdColInvisible(Grd2, new int[] { 1, 12, 13, 14, 16, 17, 19, 20, 21, 22 });

        }

        private void SetGrd2()
        {
            Grd3.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd3, new int[] { 3 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 1, 2, 3, 4 });
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueCtCode, LueCtContactPersonName,LueRingCode, LueSPCode, DteQtStartDt, TxtQtMth, LuePtCode,
                        LueAgingAP, LueCurCode, TxtCreditLimit, TxtCtReplace, MeeRemark, TxtLOPDocNo, TxtDirectCost, TxtIndirectCost, TxtPolicy,
                        LueProcessInd, MeeApprovalRemark, LuePPN
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    ChkBankGuaranteeInd.Properties.ReadOnly = true;
                    ChkPrintSignatureInd.Properties.ReadOnly = true;
                    ChkTaxInd.Properties.ReadOnly = true;
                    BtnCtContactPersonName.Enabled = false;
                    BtnCtReplace.Enabled = false;
                    BtnCtReplace2.Enabled = true;
                    BtnLOPDocNo.Enabled = false;
                    Grd2.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueSPCode, DteQtStartDt, TxtQtMth, LuePtCode, LueRingCode,
                        LueAgingAP, LueCurCode, TxtCreditLimit, MeeRemark, TxtDirectCost, TxtIndirectCost, TxtPolicy
                    }, false);
                    DteDocDt.Focus();
                    TxtQtMth.EditValue = '0';
                    ChkActInd.Checked = true;
                    ChkTaxInd.Properties.ReadOnly = false;
                    ChkBankGuaranteeInd.Properties.ReadOnly = false;
                    ChkPrintSignatureInd.Properties.ReadOnly = false;
                    ChkPrintSignatureInd.Checked = true;
                    BtnCtContactPersonName.Enabled = true;
                    BtnCtReplace.Enabled = true;
                    BtnCtReplace2.Enabled = true;
                    BtnLOPDocNo.Enabled = true;
                    //Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 13 });
                    Grd2.ReadOnly = false;
                    break;
                case mState.Edit:
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblBOQHdr ");
                    SQL.AppendLine("Where DocNo = @Param ");
                    SQL.AppendLine("And ProcessInd = 'D'; ");


                    if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueCtContactPersonName, LueSPCode, DteQtStartDt, TxtQtMth, LuePtCode, LueRingCode, LueProcessInd,
                            LueAgingAP, LueCurCode, TxtCreditLimit, MeeRemark, TxtDirectCost, TxtIndirectCost, TxtPolicy
                        }, false);
                        Grd2.ReadOnly = false;
                        BtnCtContactPersonName.Enabled = true;
                        ChkBankGuaranteeInd.Properties.ReadOnly = false;
                        ChkTaxInd.Properties.ReadOnly = false;
                        if(ChkTaxInd.Checked)
                        {
                            LuePPN.Properties.ReadOnly = false;
                            LuePPN.BackColor = Color.FromArgb(255, 255, 255);
                        }
                    }
                    else ChkBankGuaranteeInd.Properties.ReadOnly = true;

                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    BtnCtReplace.Enabled = false;
                    BtnCtReplace2.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            ChkTaxInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtQtMth, LueCtCode, LueCtContactPersonName,LueRingCode, LueSPCode, DteQtStartDt,LuePtCode,
                LueAgingAP, LueCurCode, TxtCtReplace, MeeRemark, TxtLOPDocNo, TxtStatus, LueProcessInd, MeeApprovalRemark, LuePPN
            });
            ClearGrd();
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>
            {
                TxtCreditLimit, TxtSubTotal, TxtTax, TxtGrandTotal, TxtDirectCost, TxtIndirectCost, TxtPolicy, TxtSubTotalRBPS, TxtPercentage
            }, 0);
            ChkPrintSignatureInd.Checked = false;
            ChkActInd.Checked = false;
            ChkBankGuaranteeInd.Checked = false;
            //Sm.SetLue(LuePPN, Sm.GetParameter("PPNDefaultSetting"));
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 7, 9, 10, 11, 13, 14, 16, 17, 18, 20, 21, 22 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBOQFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteQtStartDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
                Sm.SetLue(LueProcessInd, "D");
                Sm.SetLue(LuePPN, mPPNDefaultSetting);
                isinsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            isinsert = true;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            isinsert = false;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 11 }, e.ColIndex))
            {
                
            }

            if(e.ColIndex == 2)
            {
                TickGrdCols(e.RowIndex, e.ColIndex);
            }

            if (Sm.IsGrdColSelected(new int[] { 7, 9 }, e.ColIndex))
            {
                decimal mTotal = 0m, mQty = 0m, mMth = 0m;
                if(Sm.GetGrdBool(Grd2, e.RowIndex, 2))
                {
                    mQty = Sm.GetGrdDec(Grd2, e.RowIndex, 7);
                    mMth = Sm.GetGrdDec(Grd2, e.RowIndex, 9);
                    mTotal = mQty * mMth;
                }
                Grd2.Cells[e.RowIndex, 10].Value = mTotal;

            }

            if (Sm.IsGrdColSelected(new int[] { 7, 9, 11 }, e.ColIndex))
            {
                InputAmount(e.RowIndex, e.ColIndex);
                ComputeTotalPrice(e.RowIndex);
                InputAmount2(e.RowIndex, e.ColIndex);
                CalculateParentAmt(e.RowIndex, e.ColIndex);
                ComputeSubTotal();
                ComputePercentage();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 4) LueRequestEdit(Grd2, LueResourceType, ref fCell, ref fAccept, e);
                if (e.ColIndex == 6) LueRequestEdit(Grd2, LueResourceName, ref fCell, ref fAccept, e);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            //Sm.GrdRemoveRow(Grd2, e, BtnSave);
            //Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
            if (e.KeyCode == Keys.Enter && Grd2.CurRow.Index != Grd2.Rows.Count - 1)
                Sm.FocusGrd(Grd2, Grd2.CurRow.Index + 1, Grd2.CurCell.Col.Index);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            //if (Grd2.Rows.Count > 0)
            //{
            //    decimal mQty = Sm.GetGrdDec(Grd2, 0, 7);
            //    string mUoM = Sm.GetGrdStr(Grd2, 0, 8);
            //    decimal mMth = Sm.GetGrdDec(Grd2, 0, 9);

            //    if (e.ColIndex == 2)
            //    {
            //        bool IsRequired = Sm.GetGrdBool(Grd2, 0, 2);
            //        for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            //            Grd2.Cells[Row, 2].Value = IsRequired;
            //    }

            //if (e.ColIndex == 7)
            //{
            //    for (int r = 0; r < Grd2.Rows.Count; r++)
            //    {
            //        if (Sm.GetGrdBool(Grd2, r, 2))
            //        {
            //            Grd2.Cells[r, 7].Value = mQty;
            //            if (Sm.GetGrdStr(Grd2, r, 9).Length > 0)
            //                Grd2.Cells[r, 10].Value = mQty * Sm.GetGrdDec(Grd2, r, 9);
            //            ComputeTotalPrice(r);
            //        }
            //    }
            //}

            //    if (e.ColIndex == 8)
            //    {
            //        for (int r = 0; r < Grd2.Rows.Count; r++)
            //        {
            //            if (Sm.GetGrdBool(Grd2, r, 2))
            //            {
            //                Grd2.Cells[r, 8].Value = mUoM;
            //            }
            //        }
            //    }

            //if (e.ColIndex == 9)
            //{
            //    for (int r = 0; r < Grd2.Rows.Count; r++)
            //    {
            //        if (Sm.GetGrdBool(Grd2, r, 2))
            //        {
            //            Grd2.Cells[r, 9].Value = mMth;
            //            if (Sm.GetGrdStr(Grd2, r, 7).Length > 0)
            //                Grd2.Cells[r, 10].Value = mMth * Sm.GetGrdDec(Grd2, r, 7);
            //            ComputeTotalPrice(r);
            //        }
            //    }
            //}


            //    if (Sm.IsGrdColSelected(new int[] { 4, 6 }, e.ColIndex))
            //    {
            //        if (BtnSave.Enabled)
            //        {
            //            if (e.ColIndex == 4)
            //            {
            //                string mResourceType = Sm.GetGrdStr(Grd2, 0, 3);

            //                for (int x = 0; x < Grd2.Rows.Count; x++)
            //                {
            //                    if(Sm.GetGrdBool(Grd2, x, 2))
            //                    {
            //                        Grd2.Cells[x, 3].Value = mResourceType;
            //                        Grd2.Cells[x, 4].Value = LueResourceType.GetColumnValue("Col2");
            //                    }
            //                }
            //            }

            //            if (e.ColIndex == 6)
            //            {
            //                string mResourceName = Sm.GetGrdStr(Grd2, 0, 5);

            //                for (int Row = 1; Row < Grd2.Rows.Count; Row++)
            //                {
            //                    if (Sm.GetGrdBool(Grd2, Row, 2))
            //                    {
            //                        Grd2.Cells[Row, 5].Value = mResourceName;
            //                        Grd2.Cells[Row, 6].Value = LueResourceName.GetColumnValue("Col2");
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid() 
               ) return;

            if (decimal.Parse(TxtDirectCost.Text) == 0 || decimal.Parse(TxtIndirectCost.Text) == 0 || decimal.Parse(TxtPolicy.Text) == 0)
            {
                if (
                Sm.StdMsgYN("Question",
                       "Direct Cost: " + TxtDirectCost.Text + Environment.NewLine +
                       "Indirect Cost: " + TxtIndirectCost.Text + Environment.NewLine +
                       "Policy: " + TxtPolicy.Text + Environment.NewLine +
                       "Do you want to save this data ?"
                      ) == DialogResult.No
                    ) return;
            }

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BOQ", "TblBOQHdr");

            var cml = new List<MySqlCommand>();

            //ComputeTotalAmt();

            cml.Add(SaveBOQHdr(DocNo));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveBOQDtl(DocNo, Row));
            }

            if (TxtCtReplace.Text.Length != 0)
                cml.Add(CancelBOQReplacement(TxtCtReplace.Text));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtLOPDocNo, "LOP#", false) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                //IsLueCtContactPersonNonMandatory() ||
                (!mIsCustomerContactPersonNonMandatory && Sm.IsLueEmpty(LueCtContactPersonName, "Contact Person")) ||
                Sm.IsLueEmpty(LueSPCode, "Sales Person") ||
                Sm.IsDteEmpty(DteQtStartDt, "Quotation Start Date") ||
                Sm.IsTxtEmpty(TxtQtMth, "Quotation Month", true) ||
                Sm.IsLueEmpty(LuePtCode, "Payment Term") ||
                Sm.IsLueEmpty(LueCurCode, "Currency Code") ||
                Sm.IsTxtEmpty(TxtCreditLimit, "Credit Limit", true) ||
                IsLOPAlreadyProceed() ||
                IsLOPCancelled() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsLOPAlreadyProceed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblBOQHdr ");
            SQL.AppendLine("Where LOPDocNo = @Param ");
            SQL.AppendLine("And ActInd = 'Y' ");
            SQL.AppendLine("And Status In ('O', 'A') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtLOPDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This LOP data has been proceed in BOQ#" + Sm.GetValue(SQL.ToString(), TxtLOPDocNo.Text));
                return true;
            }

            return false;
        }
        
        private bool IsLueCtContactPersonNonMandatory()
        {
            if (!mIsCustomerContactPersonNonMandatory)
            {
                Sm.IsLueEmpty(LueCtContactPersonName, "Contact Person");
                return true;
            }
            else
                return false;
        }

        private bool IsLOPCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblLOPHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C' Or ProcessInd = 'C') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtLOPDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This LOP document is cancelled.");
                TxtLOPDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if(Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                {
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 3).Length != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The resource type should be empty due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 4);
                        return true;
                    }
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 5).Length != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The resource name should be empty due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 6);
                        return true;
                    }
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdDec(Grd2, Row, 7) != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The quantity should be zero due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 7);
                        return true;
                    }
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 8).Length != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The uom should be empty due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 8);
                        return true;
                    }
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdDec(Grd2, Row, 9) != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The duration should be zero due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 9);
                        return true;
                    }
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdDec(Grd2, Row, 11) != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The unit price should be zero due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 11);
                        return true;
                    }
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 12) == "Y" && Sm.IsGrdValueEmpty(Grd2, Row, 4, false, "Resource Type should not be empty.")) return true;
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 12) == "Y" && Sm.IsGrdValueEmpty(Grd2, Row, 6, false, "Resource Name should not be empty.")) return true;
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.IsGrdValueEmpty(Grd2, Row, 7, true, "Quantity should not be zero.")) return true;
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.IsGrdValueEmpty(Grd2, Row, 8, false, "UoM should not be empty.")) return true;
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.IsGrdValueEmpty(Grd2, Row, 9, true, "Duration should not be zero.")) return true;
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.IsGrdValueEmpty(Grd2, Row, 10, true, "Total Unit should not be zero.")) return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "There is no BOQ item listed.");
                return true;
            }
            
            return false;
        }

        private MySqlCommand SaveBOQHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBOQHdr(DocNo, DocDt, DocType, ActInd, Status, ProcessInd, LOPDocNo, CtCode, CtContactPersonName, RingCode, SPCode, QtStartDt, QtMth, ");
            SQL.AppendLine("PtCode, AgingAp, CurCode, CreditLimit, PrintSignatureInd, CtQtDocNoReplace, SubTotal, Tax, TaxInd, GrandTotal, DirectCost, IndirectCost, Policy, SubTotalRBPS, Remark, CreateBy, CreateDt, BankGuaranteeInd, TaxCode) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocType, @ActInd, 'O', 'D', @LOPDocNo, @CtCode, @CtContactPersonName, @RingCode, @SPCode, @QtStartDt, @QtMth, ");
            SQL.AppendLine("@PtCode, @AgingAp, @CurCode, @CreditLimit, @PrintSignatureInd, @CtQtDocNoReplace, @SubTotal, @Tax, @TaxInd, @GrandTotal, @DirectCost, @IndirectCost, @Policy, @SubTotalRBPS, @Remark, @UserCode, CurrentDateTime(), @BankGuaranteeInd, @TaxCode); ");

            //SQL.AppendLine("Update TblBOQHdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            //SQL.AppendLine("Where CtCode=@CtCode And DocNo<>@DocNo; ");

            //SQL.AppendLine("Insert Into TblBOQDtl(DocNo, ItBOQCode, Amt, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select @DocNo, A.ItBOQCode, 0, @UserCode, CurrentDateTime() ");
            //SQL.AppendLine("From TblItemBOQ A ");
            //SQL.AppendLine("Where A.ActInd = 'Y' ");
            //SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mBOQDocType);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCtContactPersonName));
            Sm.CmParam<String>(ref cm, "@RingCode", Sm.GetLue(LueRingCode));
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParamDt(ref cm, "@QtStartDt", Sm.GetDte(DteQtStartDt));
            Sm.CmParam<String>(ref cm, "@QtMth", TxtQtMth.Text);
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@AgingAp", Sm.GetLue(LueAgingAP));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", Decimal.Parse(TxtCreditLimit.Text));
            Sm.CmParam<String>(ref cm, "@PrintSignatureInd", ChkPrintSignatureInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtQtDocNoReplace", TxtCtReplace.Text);
            Sm.CmParam<Decimal>(ref cm, "@SubTotal", Decimal.Parse(TxtSubTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Decimal.Parse(TxtTax.Text));
            Sm.CmParam<String>(ref cm, "@TaxInd", ChkTaxInd.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", Decimal.Parse(TxtGrandTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCost", Decimal.Parse(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCost", Decimal.Parse(TxtIndirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@Policy", Decimal.Parse(TxtPolicy.Text));
            Sm.CmParam<Decimal>(ref cm, "@SubTotalRBPS", Decimal.Parse(TxtSubTotalRBPS.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankGuaranteeInd", ChkBankGuaranteeInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LuePPN));
            return cm;
        }

        private MySqlCommand SaveBOQDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBOQDtl(DocNo, ItBOQCode, RequiredInd, Amt, ");
            SQL.AppendLine("ResourceTypeCode, ResourceNameCode, Qty, UoM, Mth, Total, TotalAmt, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @ItBOQCode, @RequiredInd, @Amt, ");
            SQL.AppendLine("@ResourceTypeCode, @ResourceNameCode, @Qty, @UoM, @Mth, @Total, @TotalAmt, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()) ");
            
            SQL.AppendLine("On Duplicate Key Update ");

            SQL.AppendLine("    RequiredInd = @RequiredInd, Amt = @Amt, ");
            SQL.AppendLine("    ResourceTypeCode = @ResourceTypeCode, ResourceNameCode = @ResourceNameCode, ");
            SQL.AppendLine("    Qty = @Qty, UoM = @UoM, ");
            SQL.AppendLine("    Mth = @Mth, Total = @Total, ");
            SQL.AppendLine("    TotalAmt = @TotalAmt, Remark = @Remark, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");

            //SQL.AppendLine("Update TblBOQDtl ");
            //SQL.AppendLine("    Set RequiredInd = @RequiredInd, Amt = @Amt, ");
            //SQL.AppendLine("    ResourceTypeCode = @ResourceTypeCode, ResourceNameCode = @ResourceNameCode, ");
            //SQL.AppendLine("    Qty = @Qty, UoM = @UoM, ");
            //SQL.AppendLine("    Mth = @Mth, Total = @Total, ");
            //SQL.AppendLine("    TotalAmt = @TotalAmt, Remark = @Remark ");
            //SQL.AppendLine("Where DocNo = @DocNo And ItBOQCode = @ItBOQCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ItBOQCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@RequiredInd", Sm.GetGrdBool(Grd2, Row, 2) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ResourceTypeCode", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@ResourceNameCode", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@UoM", Sm.GetGrdStr(Grd2, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Mth", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Total", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Sm.GetGrdDec(Grd2, Row, 18));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand CancelBOQReplacement(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBOQHdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsActIndDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblBOQHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And ProcessInd = 'F'; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                cml.Add(UpdateBOQFinal(TxtDocNo.Text));
            }
            else cml.Add(UpdateBOQHdr(TxtDocNo.Text));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveBOQDtl(TxtDocNo.Text, Row));
            }

            cml.Add(AddApprovalData());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                Sm.IsTxtEmpty(TxtLOPDocNo, "LOP#", false) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                //IsLueCtContactPersonNonMandatory() ||
                (!mIsCustomerContactPersonNonMandatory && Sm.IsLueEmpty(LueCtContactPersonName, "Contact Person")) ||
                Sm.IsLueEmpty(LueSPCode, "Sales Person") ||
                Sm.IsDteEmpty(DteQtStartDt, "Quotation Start Date") ||
                Sm.IsTxtEmpty(TxtQtMth, "Quotation Month", true) ||
                Sm.IsLueEmpty(LuePtCode, "Payment Term") ||
                Sm.IsLueEmpty(LueCurCode, "Currency Code") ||
                Sm.IsTxtEmpty(TxtCreditLimit, "Credit Limit", true) ||
                IsLOPCancelled() ||
                IsActIndEditedAlready() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsActIndEditedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblBOQHdr Where DocNo=@DocNo And (ActInd='N' Or Status = 'C'); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already inactive.");
                return true;
            }
            return false;
        }
        private MySqlCommand UpdateBOQFinal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblBOQHdr Set ");
            SQL.AppendLine("    ActInd = @ActInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo And ActInd = 'Y'; ");

            SQL.AppendLine("Delete From TblBOQDtl Where DocNo = @DocNo And Exists (Select DocNo From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");
            
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateBOQHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblBOQHdr Set ");
            SQL.AppendLine("    ProcessInd = @ProcessInd, ");
            SQL.AppendLine("    LOPDocNo = @LOPDocNo, CtContactPersonName = @CtContactPersonName, ");
            SQL.AppendLine("    RingCode = @RingCode, SPCode = @SPCode, QtStartDt = @QtStartDt, ");
            SQL.AppendLine("    QtMth = @QtMth, PtCode = @PtCode, AgingAp = @AgingAp,  ");
            SQL.AppendLine("    CurCode = @CurCode, CreditLimit = @CreditLimit,  ");
            SQL.AppendLine("    PrintSignatureInd = @PrintSignatureInd, Remark = @Remark, ");
            SQL.AppendLine("    ActInd = @ActInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime(), BankGuaranteeInd=@BankGuaranteeInd ");
            SQL.AppendLine("Where DocNo = @DocNo And ActInd = 'Y'; ");

            //if (ChkActInd.Checked) // remarked by WED. di remark dulu untuk sementara, sampai ada clear bispro nya kek gimana
            //{
                SQL.AppendLine("Update TblBOQHdr Set  ");
                SQL.AppendLine("    SubTotal = @SubTotal, Tax = @Tax, TaxInd = @TaxInd, GrandTotal = @GrandTotal, ");
                SQL.AppendLine("    DirectCost = @DirectCost, IndirectCost = @IndirectCost, Policy = @Policy, SubTotalRBPS = @SubTotalRBPS, TaxCode = @TaxCode ");
                SQL.AppendLine("Where DocNo = @DocNo And ActInd = 'Y'; ");

                SQL.AppendLine("Delete From TblBOQDtl Where DocNo = @DocNo And Exists (Select DocNo From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");

                //SQL.AppendLine("Insert Into TblBOQDtl(DocNo, ItBOQCode, Amt, CreateBy, CreateDt) ");
                //SQL.AppendLine("Select @DocNo, A.ItBOQCode, 0, @UserCode, CurrentDateTime() ");
                //SQL.AppendLine("From TblItemBOQ A ");
                ////SQL.AppendLine("Where A.ActInd = 'Y' ");
                //SQL.AppendLine("; ");
            //}

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@TaxInd", (ChkTaxInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<Decimal>(ref cm, "@SubTotal", Decimal.Parse(TxtSubTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Decimal.Parse(TxtTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", Decimal.Parse(TxtGrandTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCost", Decimal.Parse(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCost", Decimal.Parse(TxtIndirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@Policy", Decimal.Parse(TxtPolicy.Text));
            Sm.CmParam<Decimal>(ref cm, "@SubTotalRBPS", Decimal.Parse(TxtSubTotalRBPS.Text));
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCtContactPersonName));
            Sm.CmParam<String>(ref cm, "@RingCode", Sm.GetLue(LueRingCode));
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParamDt(ref cm, "@QtStartDt", Sm.GetDte(DteQtStartDt));
            Sm.CmParam<String>(ref cm, "@QtMth", TxtQtMth.Text.Replace(",","."));
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@AgingAp", Sm.GetLue(LueAgingAP));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", Decimal.Parse(TxtCreditLimit.Text));
            Sm.CmParam<String>(ref cm, "@PrintSignatureInd", ChkPrintSignatureInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankGuaranteeInd", ChkBankGuaranteeInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LuePPN));

            return cm;
        }

        private MySqlCommand AddApprovalData()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='BOQ' ");
                SQL.AppendLine("And T.SiteCode In ( Select SiteCode From TblLOPHdr Where DocNo = @LOPDocNo ) ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblBOQHdr ");
                SQL.AppendLine("    Where DocNo = @DocNo ");
                SQL.AppendLine("    And ActInd = 'Y' ");
                SQL.AppendLine("    And ProcessInd = 'F' ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.GrandTotal*1 ");
                SQL.AppendLine("    From TblBOQHdr A ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)); ");
            }

            SQL.AppendLine("Update TblBOQHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And ProcessInd = 'F' ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'BOQ' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand UpdateBOQDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblBOQDtl ");
        //    SQL.AppendLine("    Set RequiredInd = @RequiredInd, Amt = @Amt, ResourceTypeCode = @ResourceTypeCode, ResourceNameCode = @ResourceNameCode, ");
        //    SQL.AppendLine("    Qty = @Qty, UoM = @UoM, Mth = @Mth, Total = @Total, ");
        //    SQL.AppendLine("    TotalAmt = @TotalAmt, Remark = @Remark ");
        //    SQL.AppendLine("Where DocNo = @DocNo And ItBOQCode = @ItBOQCode ");
        //    SQL.AppendLine("And Exists ");
        //    SQL.AppendLine("(Select DocNo From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@ItBOQCode", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@RequiredInd", Sm.GetGrdBool(Grd2, Row, 2) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@ResourceTypeCode", Sm.GetGrdStr(Grd2, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@ResourceNameCode", Sm.GetGrdStr(Grd2, Row, 5));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@UoM", Sm.GetGrdStr(Grd2, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Mth", Sm.GetGrdDec(Grd2, Row, 9));
        //    Sm.CmParam<Decimal>(ref cm, "@Total", Sm.GetGrdDec(Grd2, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 11));
        //    Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Sm.GetGrdDec(Grd2, Row, 18));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 15));

        //    return cm;
        //}

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBOQHdr(DocNo);
                LoadAllItemBOQ();
                Sm.ShowDocApproval(DocNo, "BOQ", ref Grd3);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBOQHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ActInd, A.CtCode, A.CtContactPersonName, A.RingCode, A.SPCode, A.QtStartDt, ");
            SQL.AppendLine("A.QtMth, A.PtCode, A.AgingAP, A.CurCode, A.CreditLimit, A.PrintSignatureInd, A.CtQtDocNoReplace, A.Remark, ");
            SQL.AppendLine("A.LOPDocNo, A.Status, A.SubTotal, A.Tax, A.GrandTotal, A.TaxInd, A.DirectCost, A.IndirectCost, A.Policy, ");
            SQL.AppendLine("A.SubTotalRBPS, A.ProcessInd, A.BankGuaranteeInd, A.TaxCode ");
            if (mIsPSModuleShowApproverRemarkInfo)
                SQL.AppendLine(", C.Remark as ApprovalRemark ");
            else
                SQL.AppendLine(", Null as ApprovalRemark ");
            SQL.AppendLine("From TblBOQHdr A ");
            if (mIsPSModuleShowApproverRemarkInfo)
            {
                SQL.AppendLine("Left Join (   ");
                SQL.AppendLine("	Select Max(T2.`Level`) LV, T1.DocNo   ");
                SQL.AppendLine("	From TblDocApproval T1   ");
                SQL.AppendLine("	Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo   ");
                SQL.AppendLine("	Where T1.DocType = 'BOQ' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '') ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine("	Group By T1.DocNo ");
                SQL.AppendLine(") B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("Left Join (   ");
                SQL.AppendLine("   Select T2.`Level` As LV, T1.DocNo, T1.Remark, T3.EmpName, T1.UserCode ");
                SQL.AppendLine("   From TblDocApproval T1   ");
                SQL.AppendLine("   Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo ");
                SQL.AppendLine("	Inner Join TblEmployee T3 On T1.UserCode = T3.UserCode  ");
                SQL.AppendLine("   Where T1.DocType = 'BOQ' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '')   ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine(") C On B.DocNo = C.DocNo And B.LV = C.LV ");
            }
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "ActInd", "CtCode", "CtContactPersonName", "RingCode", 
                    
                    //6-10
                    "SPCode", "QtStartDt", "QtMth", "PtCode", "AgingAP", 

                    //11-15
                    "CurCode", "CreditLimit", "PrintSignatureInd", "CtQtDocNoReplace", "Remark",

                    //16-20
                    "LOPDocNo", "Status", "SubTotal", "Tax", "GrandTotal", 
                    
                    //21-25
                    "TaxInd", "DirectCost", "IndirectCost", "Policy", "SubTotalRBPS",
                    
                    //26-28
                    "ProcessInd", "BankGuaranteeInd", "ApprovalRemark", "TaxCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.DrStr(dr, c[3]), Sm.DrStr(dr, c[4]));
                    //Sm.SetLue(LueCtContactPersonName, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LueRingCode, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[6]));
                    Sm.SetDte(DteQtStartDt, Sm.DrStr(dr, c[7]));
                    TxtQtMth.EditValue = Sm.DrStr(dr, c[8]);
                    
                    Sm.SetLue(LuePtCode, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueAgingAP, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[11]));
                    TxtCreditLimit.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    ChkPrintSignatureInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[13]), "Y");
                    TxtCtReplace.EditValue = Sm.DrStr(dr, c[14]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                    TxtLOPDocNo.EditValue = Sm.DrStr(dr, c[16]);
                    string mStatusDesc = string.Empty;
                    if (Sm.DrStr(dr, c[17]) == "A") mStatusDesc = "Approved";
                    if (Sm.DrStr(dr, c[17]) == "C") mStatusDesc = "Cancelled";
                    if (Sm.DrStr(dr, c[17]) == "O") mStatusDesc = "Outstanding";
                    TxtStatus.EditValue = mStatusDesc;
                    TxtSubTotal.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                    TxtTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                    TxtGrandTotal.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0);
                    ChkTaxInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[21]), "Y");
                    TxtDirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 0);
                    TxtIndirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[23]), 0);
                    TxtPolicy.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 0);
                    TxtSubTotalRBPS.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                    Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[26]));
                    ChkBankGuaranteeInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[27]), "Y");
                    MeeApprovalRemark.EditValue = Sm.DrStr(dr, c[28]);
                    if (Sm.DrStr(dr, c[29]) == "")
                        Sm.SetLue(LuePPN, mPPNDefaultSetting);
                    else
                        Sm.SetLue(LuePPN, Sm.DrStr(dr, c[29]));

                }, true
            );
            ComputePercentage();
        }

        #endregion

        #region Additional Method


        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mBOQTaxPercentage = Sm.GetParameterDec("BOQTaxPercentage");
            mBOQPrintOutBODName = Sm.GetParameter("BOQPrintOutBODName");
            mRptName = Sm.GetParameter("FormPrintBOQ");
            mIsBOQPrintOutShowCreatedUser = Sm.GetParameterBoo("IsBOQPrintOutShowCreatedUser");
            mIsBOQExcludeCancelledData = Sm.GetParameterBoo("IsBOQExcludeCancelledData");
            mBOQStatusDisplayedInFind = Sm.GetParameter("BOQStatusDisplayedInFind");
            mIsPSModuleShowApproverRemarkInfo = Sm.GetParameterBoo("IsPSModuleShowApproverRemarkInfo");
            mPPNDefaultSetting = Sm.GetParameter("PPNDefaultSetting");
            mIsCustomerContactPersonNonMandatory = Sm.GetParameterBoo("IsCustomerContactPersonNonMandatory");
            mItemBOQCalculationFormat = Sm.GetParameter("ItemBOQCalculationFormat");
            if (mItemBOQCalculationFormat.Length == 0) mItemBOQCalculationFormat = "1";
        }

        private void ComputeSubTotal()
        {
            decimal mSubtotal = 0m;

            if (Grd2.Rows.Count > 1)
            {
                for (int i = 0; i < Grd2.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd2, i, 0).Length > 0 && 
                        Sm.GetGrdBool(Grd2, i, 2) && // item tersebut required
                        Sm.GetGrdStr(Grd2, i, 19) == "Y") // item tersebut merupakan parent
                    {
                        mSubtotal += Sm.GetGrdDec(Grd2, i, 18);
                    }
                }
            }

            TxtSubTotal.EditValue = Sm.FormatNum(mSubtotal, 0);
            ComputeTax();
        }

        private void ComputeTax()
        {
            decimal mTax = 0m, mSubtotal = 0m, tax = 0m;
            mSubtotal = Decimal.Parse(TxtSubTotal.Text);


            var taxcode = Sm.GetLue(LuePPN);

            if (ChkTaxInd.Checked)
            {
                tax = taxcode.Length > 0 ? Decimal.Parse(Sm.GetValue("select taxrate from tbltax where taxcode = @param ", taxcode)) : 0m;
            }

            //if (ChkTaxInd.Checked) mTax = (mBOQTaxPercentage * 0.01m) * mSubtotal;
            if (ChkTaxInd.Checked) mTax = (tax * 0.01m) * mSubtotal;

            TxtTax.EditValue = Sm.FormatNum(mTax, 0);
            ComputeGrandTotal();
        }

        private void ComputeGrandTotal()
        {
            decimal mGrandTotal = 0m, mSubtotal = 0m, mTax = 0m;
            mSubtotal = Decimal.Parse(TxtSubTotal.Text);
            mTax = Decimal.Parse(TxtTax.Text);
            mGrandTotal = mSubtotal + mTax;
            TxtGrandTotal.EditValue = Sm.FormatNum(mGrandTotal, 0);
        }

        private void ComputePercentage()
        {
            decimal mGrandTotal = 0m, mCreditLimit = 0m, mPercentage = 0m;
            mGrandTotal = decimal.Parse(TxtGrandTotal.Text);
            mCreditLimit = decimal.Parse(TxtCreditLimit.Text);
            if (mCreditLimit > 0)
            {
                mPercentage = (mGrandTotal / mCreditLimit) * 100 ;
            }
            else
                mPercentage = 0;
            TxtPercentage.EditValue = Sm.FormatNum(mPercentage, 0);
        }
        private void ComputeSubTotalRBPS()
        {
            decimal mSubTotalRBPS = 0m, mDirectCost = 0m, mPolicy = 0m, mIndirectCost = 0m;
            mDirectCost = Decimal.Parse(TxtDirectCost.Text);
            mIndirectCost = Decimal.Parse(TxtIndirectCost.Text);
            mPolicy = Decimal.Parse(TxtPolicy.Text);
            mSubTotalRBPS = mDirectCost + mIndirectCost + mPolicy;
            TxtSubTotalRBPS.EditValue = Sm.FormatNum(mSubTotalRBPS, 0);
        }

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'BOQ' And SiteCode Is Not Null; ");
        }

        private void ComputeTotalAmt()
        {
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                {
                    Grd2.Cells[Row, 16].Value = Sm.GetGrdDec(Grd2, Row, 10) * Sm.GetGrdDec(Grd2, Row, 11);

                    decimal Amt = Sm.GetGrdDec(Grd2, Row, 17);
                    string Ac = Sm.GetGrdStr(Grd2, Row, 1);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if(Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < Row; y++)
                            {
                                if(Sm.GetGrdStr(Grd2, y, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, Row, 16));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 16));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd2.Cells[y, 16].Value = Qty;
                                    Grd2.Cells[y, 17].Value = Qty;
                                    Grd2.Cells[Row, 17].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ComputeTotalPrice(int r)
        {
            decimal Qty = 0m, Mth = 0m, Amt = 0m;

            Grd2.BeginUpdate();

            Qty = Sm.GetGrdDec(Grd2, r, 7);
            Mth = Sm.GetGrdDec(Grd2, r, 9);
            Amt = Sm.GetGrdDec(Grd2, r, 11);

            Grd2.Cells[r, 18].Value = Qty * Mth * Amt;

            //string AcNo1 = string.Empty, AcNo2 = string.Empty;
            //decimal Amt1 = 0m, Amt2 = 0m;
            //bool IsFirst = false;
            //int Temp = 0;
            //for (var i = 0; i < r; i++)
            //{
            //    IsFirst = false;
            //    AcNo1 = Sm.GetGrdStr(Grd2, i, 1);
            //    Amt1 = Sm.GetGrdDec(Grd2, i, 18);
            //    for (var j = Temp; j < Grd2.Rows.Count; j++)
            //    {
            //        AcNo2 = Sm.GetGrdStr(Grd2, j, 1);
            //        Amt2 = Sm.GetGrdDec(Grd2, j, 18);
            //        if (
            //            //AcNo1.Count(x => x == '.') == AcNo2.Count(x => x == '.') && Sm.CompareStr(AcNo1, AcNo2) ||
            //            AcNo1.Count(x => x == '.') != AcNo2.Count(x => x == '.') && AcNo1.StartsWith(string.Concat(AcNo2, '.'))
            //            )
            //        {
            //            if (!IsFirst)
            //            {
            //                IsFirst = true;
            //                Temp = j;
            //            }
            //            Grd2.Cells[j, 18].Value = Amt1 + Amt2;
            //            if (string.Compare(AcNo1, AcNo2) == 0)
            //                break;
            //        }
            //    }
            //}
   
            Grd2.EndUpdate();
        }

        private void InputAmount(int RowIndex, int ColIndex)
        {
            #region Old Code
            //if (ColIndex == 11)
            //{
            //    decimal Amt = Sm.GetGrdDec(Grd2, RowIndex, 13);
            //    if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y") // allowed to edit & required
            //    {
            //        string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
            //        string ccc = string.Empty;
            //        for (int i = 0; i < Ac.Length; i++)
            //        {
            //            if (Ac[i] == '.')
            //            {
            //                ccc = Ac.Substring(0, i);
            //                for (int y = 0; y < RowIndex; y++)
            //                {
            //                    if (Sm.GetGrdStr(Grd2, y, 1) == ccc)
            //                    {
            //                        decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 11));
            //                        decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 11));
            //                        decimal Qty = QtyRow + QtyRow2 - Amt;
            //                        Grd2.Cells[y, 11].Value = Qty;
            //                        Grd2.Cells[y, 13].Value = Qty;
            //                        Grd2.Cells[RowIndex, 13].Value = QtyRow;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        Sm.StdMsg(mMsgType.Info, "Can't input amount for this BOQ item");
            //        //Grd2.Cells[RowIndex, 7].Value = 0;
            //    }
            //}
            #endregion

            if (ColIndex == 7 || ColIndex == 9 || ColIndex == 11)
            {
                decimal Amt = Sm.GetGrdDec(Grd2, RowIndex, 13);
                if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y") // allowed to edit & required
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < RowIndex; y++)
                            {
                                if (Sm.GetGrdStr(Grd2, y, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 11));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 11));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd2.Cells[y, 11].Value = Qty;
                                    Grd2.Cells[y, 13].Value = Qty;
                                    Grd2.Cells[RowIndex, 13].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "Can't input amount for this BOQ item");
                    //Grd2.Cells[RowIndex, 7].Value = 0;
                }
            }
        }

        private void InputAmount2(int RowIndex, int ColIndex)
        {
            if (ColIndex == 7 || ColIndex == 9 || ColIndex == 11)
            {
                decimal Amt = Sm.GetGrdDec(Grd2, RowIndex, 16);
                if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y") // allowed to edit & required
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < RowIndex; y++)
                            {
                                if (Sm.GetGrdStr(Grd2, y, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 18));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 18));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd2.Cells[y, 18].Value = Qty;
                                    Grd2.Cells[y, 16].Value = Qty;
                                    Grd2.Cells[y, 11].Value = Qty;
                                    Grd2.Cells[RowIndex, 16].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "Can't input amount for this BOQ item");
                }
            }
        }

        private void CalculateParentAmt(int RowIndex, int ColIndex)
        {
            if (mItemBOQCalculationFormat == "1") return;

            if (ColIndex == 7)
            {
                decimal QtyTmp = Sm.GetGrdDec(Grd2, RowIndex, 20);
                if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y")
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
                    string ccc = string.Empty;

                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int j = 0; j < RowIndex; j++)
                            {
                                if (Sm.GetGrdStr(Grd2, j, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 7));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, j, 7));
                                    decimal Qty = QtyRow + QtyRow2 - QtyTmp;
                                    Grd2.Cells[j, 7].Value = Qty;
                                    Grd2.Cells[j, 20].Value = Qty;
                                    Grd2.Cells[RowIndex, 20].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
            }

            if (ColIndex == 9)
            {
                decimal QtyTmp = Sm.GetGrdDec(Grd2, RowIndex, 21);
                if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y")
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
                    string ccc = string.Empty;

                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int j = 0; j < RowIndex; j++)
                            {
                                if (Sm.GetGrdStr(Grd2, j, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 9));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, j, 9));
                                    decimal Qty = QtyRow + QtyRow2 - QtyTmp;
                                    Grd2.Cells[j, 9].Value = Qty;
                                    Grd2.Cells[j, 21].Value = Qty;
                                    Grd2.Cells[RowIndex, 21].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
            }

            if (ColIndex == 7 || ColIndex == 9 || ColIndex == 10)
            {
                decimal QtyTmp = Sm.GetGrdDec(Grd2, RowIndex, 22);
                if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y")
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
                    string ccc = string.Empty;

                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int j = 0; j < RowIndex; j++)
                            {
                                if (Sm.GetGrdStr(Grd2, j, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 10));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, j, 10));
                                    decimal Qty = QtyRow + QtyRow2 - QtyTmp;
                                    Grd2.Cells[j, 10].Value = Qty;
                                    Grd2.Cells[j, 22].Value = Qty;
                                    Grd2.Cells[RowIndex, 22].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void TickGrdCols(int RowIndex, int ColIndex)
        {
            try
            {
                if (ColIndex != 2) return;

                int ColAmt = 11;
                int ColAmt2 = 13;
                int Len = Sm.GetGrdStr(Grd2, RowIndex, 1).Length;
                bool Value = Sm.GetGrdBool(Grd2, RowIndex, ColIndex);
                string MenuCode = Sm.GetGrdStr(Grd2, RowIndex, 1);

                for (int Row = RowIndex + 1; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length <= Len)
                    {
                        //Grd2.Cells[Row-1, ColAmt].ReadOnly = iGBool.True;
                        //Grd2.Cells[Row-1, ColAmt].BackColor = Color.FromArgb(224, 224, 224);
                        //Grd2.Cells[Row - 1, ColAmt2].Value = 0;
                        //InputAmount(Row-1, ColAmt);
                        break;
                    }
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > Len)
                    {
                        Grd2.Cells[Row, ColIndex].Value = Value;
                        //Grd2.Cells[Row-1, ColIndex].ReadOnly = iGBool.True;
                        //Grd2.Cells[Row-1, ColIndex].BackColor = Color.FromArgb(224, 224, 224);
                        //InputAmount(Row-1, ColAmt);
                    }

                }

                for (int Row = RowIndex - 1; Row >= 0; Row--)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length < Len &&
                        ("X" + MenuCode).IndexOf("X" + Sm.GetGrdStr(Grd2, Row, 1)) >= 0 &&
                        (Value || (!Value && IsNotHaveThickedChildNode(Row, ColIndex, Sm.GetGrdStr(Grd2, Row, 1).Length))))
                        Grd2.Cells[Row, ColIndex].Value = Value;

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsNotHaveThickedChildNode(int CurRow, int CurCol, int Len)
        {
            for (int Row = CurRow + 1; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > Len)
                {
                    if (Sm.GetGrdBool(Grd2, Row, CurCol))
                        return false;
                }
                else
                    return true;
            }
            return true;
        }

        private void SetLueResourceName(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'INTERNAL' As Col1, 'INTERNAL' As Col2 ");
	        SQL.AppendLine("Union All ");
	        SQL.AppendLine("Select X.Col1, X.Col2 ");
	        SQL.AppendLine("From ");
	        SQL.AppendLine("( ");
		    SQL.AppendLine("    Select VdCode As Col1, VdName As Col2 ");
		    SQL.AppendLine("    From TblVendor "); 
		    SQL.AppendLine("    Where 0 = 0 ");
            if (TxtDocNo.Text.Length == 0)
		        SQL.AppendLine("    And ActInd = 'Y' ");
		    SQL.AppendLine("    Order By VdName ");
	        SQL.AppendLine(")X; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void LoadAllItemBOQ()
        {
            var SQL = new StringBuilder();
            int Level = 0;
            string PrevMenuCode = string.Empty;
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (TxtDocNo.Text.Length == 0)
                {
                    SQL.AppendLine("Select A.ItBOQCode, A.ActInd, Concat(A.ItBOQCode, ' ', A.ItBOQName) As ItemBOQ, 0 As Amt, If(B.ItBOQCode Is Null, 'N', 'Y') As Allow, ");
                    SQL.AppendLine("null as ResourceTypeCode, null as ResourceTypeDesc, null as ResourceNameCode, null As ResourceNameDesc, 'Y' As RequiredInd, ");
                    SQL.AppendLine("0 as Qty, null as UoM, 0 as Mth, 0 As Total, null as Remark, 0 as TotalAmt, If(A.Parent is Null, 'Y', 'N') As ParentInd ");
                    SQL.AppendLine("From TblItemBOQ A ");
                    SQL.AppendLine("Left Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select ItBOQCode ");
                    SQL.AppendLine("    From TblItemBOQ ");
                    SQL.AppendLine("    Where ItBOQCode Not In ");
                    SQL.AppendLine("    ( Select Parent From TblItemBOQ Where Parent Is Not Null ) ");
                    SQL.AppendLine(") B On A.ItBOQCode = B.ItBOQCode ");
                    SQL.AppendLine("Where A.ActInd = 'Y' ");
                    SQL.AppendLine("Order By A.ItBOQCode; ");
                }
                else
                {
                    SQL.AppendLine("Select B.ItBOQCode, B.ActInd, Concat(B.ItBOQCode, ' ', B.ItBOQName) As ItemBOQ, ");
                    SQL.AppendLine("IfNull(A.Amt, 0) As Amt, If(C.ItBOQCode Is Null, 'N', 'Y') As Allow, A.ResourceTypeCode, D.OptDesc As ResourceTypeDesc, ");
                    SQL.AppendLine("A.ResourceNameCode, IfNull(If(A.ResourceNameCode = 'INTERNAL', 'INTERNAL', E.VdName), '') As ResourceNameDesc, ");
                    SQL.AppendLine("IfNull(A.RequiredInd, 'N') As RequiredInd, A.Qty, A.UoM, A.Mth, A.Total, A.Remark, A.TotalAmt, If(B.Parent is Null, 'Y', 'N') As ParentInd ");
                    SQL.AppendLine("From TblItemBOQ B ");
                    SQL.AppendLine("Left Join TblBOQDtl A On A.ItBOQCode=B.ItBOQCode And A.DocNo=@DocNo ");
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("   Select ItBOQCode From TblItemBOQ ");
                    SQL.AppendLine("   Where ItBOQCode Not In ");
                    SQL.AppendLine("   (Select Parent From TblItemBOQ Where Parent Is Not Null) ");
                    SQL.AppendLine(") C On B.ItBOQCode = C.ItBOQCode ");
                    SQL.AppendLine("Left Join TblOption D On A.ResourceTypeCode = D.OptCode And D.OptCat = 'ResourceType' ");
                    SQL.AppendLine("Left Join TblVendor E On A.ResourceNameCode = E.VdCode ");
                    SQL.AppendLine("Order By B.ItBOQCode; ");

                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                }

                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, 
                    new string[] 
                    { 
                        "ItBOQCode", 
                        "ItemBOQ", "Amt", "Allow", "ResourceTypeCode", "ResourceTypeDesc", 
                        "ResourceNameCode", "ResourceNameDesc", "RequiredInd", "Qty", "UoM",
                        "Mth", "Total", "Remark", "TotalAmt", "ParentInd", 
                        "ActInd"
                    });
                if (dr.HasRows)
                {
                    int Row = 0;
                    Grd2.Rows.Count = 0;
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        // kalau item tersebut aktif, atau, non-aktif tapi terpakai
                        if (Sm.DrStr(dr, c[16]) == "Y" || (Sm.DrStr(dr, c[16]) == "N" && Sm.DrStr(dr, c[8]) == "Y"))
                        {
                            Grd2.Rows.Add();
                            Level = Sm.DrStr(dr, 0).Length - ((Sm.DrStr(dr, 0).Length + 1) / 2);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 1);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("B", Grd2, dr, c, Row, 2, 8);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 4);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 5);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 6);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 6, 7);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 7, 9);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 8, 10);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 9, 11);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 10, 12);
                            if (TxtDocNo.Text.Length == 0)
                            {
                                Grd2.Cells[Row, 11].Value = 0m;
                                Grd2.Cells[Row, 13].Value = 0m;
                                Grd2.Cells[Row, 14].Value = 0m;
                            }
                            else
                            {
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 11, 2);
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 13, 2);
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 14, 2);
                            }
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 12, 3);
                            if (Sm.GetGrdStr(Grd2, Row, 12) == "N")
                            {
                                Sm.GrdColReadOnly(false, true, Grd2, new int[] { 2 });
                                Grd2.Cells[Row, 3].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 4].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 5].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 6].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 7].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 8].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 9].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 10].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 11].ReadOnly = iGBool.True;
                                //Grd2.Cells[Row, 7].ReadOnly = iGBool.True;
                                //Grd2.Cells[Row, 9].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 3].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 4].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 5].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 6].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 7].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 8].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 9].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 10].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 11].BackColor = Color.FromArgb(224, 224, 224);

                                Grd2.Cells[Row, 3].Value = string.Empty;
                                Grd2.Cells[Row, 4].Value = string.Empty;
                                Grd2.Cells[Row, 5].Value = string.Empty;
                                Grd2.Cells[Row, 6].Value = string.Empty;
                                Grd2.Cells[Row, 8].Value = "Unit";
                                if (mItemBOQCalculationFormat == "2")
                                {
                                    if (TxtDocNo.Text.Length == 0)
                                    {
                                        Grd2.Cells[Row, 7].Value = 0m;
                                        Grd2.Cells[Row, 9].Value = 0m;
                                        Grd2.Cells[Row, 10].Value = 0m;
                                        Grd2.Cells[Row, 20].Value = 0m;
                                        Grd2.Cells[Row, 21].Value = 0m;
                                        Grd2.Cells[Row, 22].Value = 0m;
                                    }
                                }
                                else
                                {
                                    Grd2.Cells[Row, 7].Value = 1m;
                                    Grd2.Cells[Row, 9].Value = 1m;
                                    Grd2.Cells[Row, 10].Value = 1m;
                                }
                            }

                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 19, 15);

                            Grd2.Rows[Row].Level = Level;
                            if (Row > 0)
                            {
                                Grd2.Rows[Row - 1].TreeButton =
                                    (PrevMenuCode.Length >= Sm.DrStr(dr, 0).Length) ?
                                        iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                            }
                            PrevMenuCode = Sm.DrStr(dr, 0);
                            Row++;
                        }
                    }
                    //if (Row > 0)
                    Grd2.Rows[Grd2.Rows.Count - 1].TreeButton =
                        (PrevMenuCode.Length >= Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 1).Length) ?
                            iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                }
                Grd2.TreeLines.Visible = true;

                //for(int r = 0; r<Grd2.Rows.Count;r++)
                //    ComputeTotalPrice(r);
                Grd2.EndUpdate();
                dr.Close();
            }

        }

        private bool IsUserAbleToInsert()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblGroupMenu A, TblMenu B, TblUser C ");
            SQL.AppendLine("Where A.MenuCode=B.MenuCode ");
            SQL.AppendLine("And Left(A.AccessInd, 1)='Y' ");
            SQL.AppendLine("And B.Param='FrmBOQ' ");
            SQL.AppendLine("And A.GrpCode=C.GrpCode ");
            SQL.AppendLine("And C.UserCode=@Param;");

            return (Sm.IsDataExist(SQL.ToString(), Gv.CurrentUserCode));
        }

        private void BtnInsertClick()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                ChkActInd.Checked = true;
                Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
                Sm.SetLue(LueProcessInd, "D");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueSPCode(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union ALL Select 'All' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void SetLueCtPersonCode(ref DXE.LookUpEdit Lue, string CtCode, string ContactPersonName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Col1 From ( ");
            SQL.AppendLine("Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode=@CtCode ");
            if (ContactPersonName.Length > 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select @ContactPersonName As Col1 ");
            }
            SQL.AppendLine(") T Order By Col1;");

            var cm = new MySqlCommand();
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ContactPersonName", ContactPersonName);

            Sm.SetLue1(ref Lue, ref cm, "Contact Person");
            if (ContactPersonName.Length > 0) Sm.SetLue(Lue, ContactPersonName);

        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void ParPrint()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No || Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

            string[] TableName = { "BOQ", "BOQDtl", "BOQTot", "BOQDtl2", "BOQDtl3" };

            var l = new List<BOQ>();
            var l2 = new List<BOQDtl>();
            var l3 = new List<BOQTot>();
            var l4 = new List<BOQDtl2>();
            var l5 = new List<BOQDtl3>();
            var lI = new List<ItemBOQ>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y')As DocDt, B.ProjectName, C.Address As SignedCity ");
            SQL.AppendLine("From TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblLOPHdr B On A.LOPDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblSite C On B.SiteCode = C.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "DocNo", 
                         "DocDt",
                         //6-7
                         "ProjectName",
                         "SignedCity"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BOQ()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),
                            ProjectName = Sm.DrStr(dr, c[6]),
                            BOQPrintOutBODName = mBOQPrintOutBODName,
                            SignedCity = Sm.DrStr(dr, c[7]),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region Detail

            if(Grd2.Rows.Count > 0)
            {
                for (int Row1 = 0; Row1 < Grd2.Rows.Count; Row1++)
                {
                    if(Sm.GetGrdBool(Grd2, Row1, 2) && Sm.GetGrdStr(Grd2, Row1, 12) == "N")
                    {
                        l2.Add(new BOQDtl()
                        {
                            DocNo = TxtDocNo.Text,
                            ItBOQCode = Sm.GetGrdStr(Grd2, Row1, 1),
                            Amt = Sm.GetGrdDec(Grd2, Row1, 16),
                            Allow = Sm.GetGrdStr(Grd2, Row1, 12),
                            ResourceTypeCode = Sm.GetGrdStr(Grd2, Row1, 3),
                            ResourceTypeDesc = Sm.GetGrdStr(Grd2, Row1, 4),
                            ResourceNameCode = Sm.GetGrdStr(Grd2, Row1, 5),
                            ResourceNameDesc = Sm.GetGrdStr(Grd2, Row1, 6),
                            Qty = Sm.GetGrdDec(Grd2, Row1, 7),
                            Uom = Sm.GetGrdStr(Grd2, Row1, 8),
                            Mth = Sm.GetGrdDec(Grd2, Row1, 9),
                            Total = Sm.GetGrdDec(Grd2, Row1, 10),
                        });
                    }
                }
            }

            var cmI = new MySqlCommand();

            using (var cnI = new MySqlConnection(Gv.ConnectionString))
            {
                cnI.Open();
                cmI.Connection = cnI;
                cmI.CommandText = "Select ItBOQCode, ItBOQName, Parent, Level From TblItemBOQ; ";
                var drI = cmI.ExecuteReader();
                var cI = Sm.GetOrdinal(drI, new string[] { "ItBOQCode", "ItBOQName", "Parent", "Level" });
                if (drI.HasRows)
                {
                    while (drI.Read())
                    {
                        lI.Add(new ItemBOQ()
                        {
                            ItBOQCode = Sm.DrStr(drI, cI[0]),
                            ItBOQName = Sm.DrStr(drI, cI[1]),
                            Parent = Sm.DrStr(drI, cI[2]),
                            Level = Int32.Parse(Sm.DrStr(drI, cI[3])),
                            Indent = ((Sm.DrInt(drI, cI[3]) - 1) * 1) * 0.75F * 19F,
                            Index1 = string.Empty,
                            Index2 = string.Empty,
                        });
                    }
                }
                drI.Close();
            }

            if (l2.Count > 0 && lI.Count > 0)
            {
                for (int i = 0; i < l2.Count; i++)
                {
                    for (int j = 0; j < lI.Count; j++)
                    {
                        if(l2[i].ItBOQCode == lI[j].ItBOQCode)
                        {
                            l2[i].ItBOQName = lI[j].ItBOQName;
                            l2[i].Level = lI[j].Level;
                            l2[i].Indent = lI[j].Indent;
                            break;
                        }
                    }
                }

                for (int i = 0; i < l2.Count; i++)
                {
                    for (int j = 0; j < lI.Count; j++)
                    {
                        if (Sm.Left(l2[i].ItBOQCode, 1) == Sm.Left(lI[j].ItBOQCode, 1) && 
                            l2[i].Level == lI[j].Level && 
                            lI[j].Index1 == string.Empty)
                        {
                            l2[i].ItBOQCode2 = lI[j].ItBOQCode;
                            lI[j].Index1 = "Y";
                            break;
                        }
                    }
                }
            }

            myLists.Add(l2);
            #endregion

            #region Detail Total

            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();

            #region Old Code
            //SQL3.AppendLine("Select A.DocNo, Sum(A.TotalAmt)Amt, truncate(Sum(A.TotalAmt),0)AmtBulat, Truncate((Sum(A.TotalAmt)*10)/100,0) As PPn, ");
            //SQL3.AppendLine("Truncate(Sum(A.TotalAmt),0)+Truncate((Sum(A.TotalAmt)*10)/100,0) As TotalAmt ");
            //SQL3.AppendLine("From TblItemBOQ B ");
            //SQL3.AppendLine("Left Join TblBOQDtl A On A.ItBOQCode=B.ItBOQCode And A.DocNo=@DocNo ");
            //SQL3.AppendLine("Where A.RequiredInd='Y' And B.Parent is Null ");
            //SQL3.AppendLine("Group By A.DocNo; ");
            #endregion 

            SQL3.AppendLine("Select A.DocNo, A.SubTotal Amt, Truncate(A.SubTotal, 0) AmtBulat, Truncate(A.Tax, 0) PPn, ");
            SQL3.AppendLine("Truncate(A.GrandTotal, 0) TotalAmt ");
            SQL3.AppendLine("From TblBOQHdr A ");
            SQL3.AppendLine("Where A.DocNo = @DocNo; ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-4
                         "Amt",
                         "AmtBulat",
                         "PPn", 
                         "TotalAmt",

                        });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new BOQTot()
                        {
                            DocNo = Sm.DrStr(dr3, c3[0]),

                            Amt = Sm.DrDec(dr3, c3[1]),
                            AmtBulat = Sm.DrDec(dr3, c3[2]),
                            PPn = Sm.DrDec(dr3, c3[3]),
                            TotalAmt = Sm.DrDec(dr3, c3[4]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr3, c3[4])),
                        });
                    }
                }
                dr3.Close();
            }

            myLists.Add(l3);
            #endregion

            #region Detail 2

            if (Grd2.Rows.Count > 0)
            {
                for (int Row1 = 0; Row1 < Grd2.Rows.Count; Row1++)
                {
                    if (Sm.GetGrdBool(Grd2, Row1, 2))
                    {
                        l4.Add(new BOQDtl2()
                        {
                            ItBOQCode = Sm.GetGrdStr(Grd2, Row1, 1),
                            Amt = Sm.GetGrdDec(Grd2, Row1, 16),
                            Allow = Sm.GetGrdStr(Grd2, Row1, 12),
                            Qty = Sm.GetGrdDec(Grd2, Row1, 7),
                            Uom = Sm.GetGrdStr(Grd2, Row1, 8),
                            Mth = Sm.GetGrdDec(Grd2, Row1, 9),
                            Total = Sm.GetGrdDec(Grd2, Row1, 10),
                            TotalAmt = Sm.GetGrdDec(Grd2, Row1, 16)
                        });
                    }
                }
            }

            if (l4.Count > 0 && lI.Count > 0)
            {
                for (int i = 0; i < l4.Count; i++)
                {
                    for (int j = 0; j < lI.Count; j++)
                    {
                        if (l4[i].ItBOQCode == lI[j].ItBOQCode)
                        {
                            l4[i].ItBOQName = lI[j].ItBOQName;
                            l4[i].Level = lI[j].Level;
                            l4[i].Indent = lI[j].Indent;
                            l4[i].Parent = lI[j].Parent;
                            break;
                        }
                    }
                }

                int mCurrLevel = 1;
                string mLastParent = string.Empty;
                bool mFlag = false;

                for (int i = 0; i < l4.Count; i++)
                {
                    for (int j = 0; j < lI.Count; j++)
                    {
                        if (Sm.Left(l4[i].ItBOQCode, 1) == Sm.Left(lI[j].ItBOQCode, 1) &&
                            //l4[i].Level == lI[j].Level &&
                            //l4[i].Parent == lI[j].Parent &&
                            lI[j].Index2 == string.Empty //&&
                            //l4[i].Allow == "N"
                            )
                        {
                            if (l4[i].Level == lI[j].Level)
                            {
                                if (mFlag)
                                {
                                    if (mLastParent == lI[j].Parent)
                                    {
                                        l4[i].ItBOQCode2 = lI[j].ItBOQCode;
                                        mLastParent = l4[i].Parent;
                                        mCurrLevel = l4[i].Level;
                                        mFlag = false;
                                        lI[j].Index2 = "Y";
                                        break;
                                    }
                                    else if (l4[i].Parent == lI[j].Parent)
                                    {
                                        l4[i].ItBOQCode2 = lI[j].ItBOQCode;
                                        mLastParent = l4[i].Parent;
                                        mCurrLevel = l4[i].Level;
                                        mFlag = false;
                                        lI[j].Index2 = "Y";
                                        break;
                                    }
                                    lI[j].Index2 = "Y";
                                }
                                else
                                {
                                    mFlag = false;
                                    if (mCurrLevel > l4[i].Level) mFlag = true;

                                    l4[i].ItBOQCode2 = lI[j].ItBOQCode;
                                    mLastParent = l4[i].ItBOQCode2;
                                    mCurrLevel = l4[i].Level;
                                    lI[j].Index2 = "Y";
                                    break;
                                }
                            }
                            lI[j].Index2 = "Y";
                        }
                    }
                }
            }

            myLists.Add(l4);
            #endregion

            #region Detail Signature

            var cm5 = new MySqlCommand();

            var SQL5 = new StringBuilder();
            using (var cn5 = new MySqlConnection(Gv.ConnectionString))
            {
                cn5.Open();
                cm5.Connection = cn5;

                if(IsNeedApproval())
                {
                    SQL5.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature,  ");
                    SQL5.AppendLine("T1.UserCode, T1.UserName, T3.PosName,  ");
                    SQL5.AppendLine("T1.DNo, T1.Seq As Level, @Space As Space,  ");
                    SQL5.AppendLine("T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt  ");
                    SQL5.AppendLine("From (  ");
                    SQL5.AppendLine("    Select UserCode, UserName, DNo, Seq, Title, LastUpDt From (  ");
                    SQL5.AppendLine("        Select Distinct B.UserCode, C.UserName,  ");
			        SQL5.AppendLine("            B.ApprovalDNo As DNo, 100+D.Level As Seq,  ");
			        SQL5.AppendLine("            'Disetujui oleh' As Title, ");
			        SQL5.AppendLine("            Left(B.LastUpDt, 8) As LastUpDt  ");
			        SQL5.AppendLine("            From TblBOQHdr A  ");
			        SQL5.AppendLine("            Inner Join TblDocApproval B On B.DocType='BOQ' And A.DocNo=B.DocNo  ");
			        SQL5.AppendLine("            Inner Join TblUser C On B.UserCode=C.UserCode  ");
                    SQL5.AppendLine("            Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType='BOQ' And D.StartAmt < 10000000000 ");
			        SQL5.AppendLine("            Where A.Status='A'  ");
			        SQL5.AppendLine("            And A.DocNo=@DocNo ");
			        SQL5.AppendLine("            Order By D.Level Desc ");
                    SQL5.AppendLine("        Limit 1  ");
                    SQL5.AppendLine("    ) Tbl  ");

                    if (mIsBOQPrintOutShowCreatedUser)
                    {
                        SQL5.AppendLine("    Union All  ");
                        SQL5.AppendLine("    Select Distinct  ");
                        SQL5.AppendLine("    A.CreateBy As UserCode, B.UserName,  ");
                        SQL5.AppendLine("    '1' As DNo, 0 As Seq, 'Dibuat oleh' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                        SQL5.AppendLine("    From TblBOQHdr A  ");
                        SQL5.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode  ");
                        SQL5.AppendLine("    Where A.Status='A'  ");
                        SQL5.AppendLine("    And A.DocNo=@DocNo  ");
                    }
                    //SQL5.AppendLine("    Union All ");
                    	
                    //SQL5.AppendLine("    Select Distinct D.UserCode, E.UserName, ");
                    //SQL5.AppendLine("    D.ApprovalDNo As DNo, 1 As Seq, 'Approved By' As Title, Left(D.LastUpDt, 8) As LastUpDt ");
                    //SQL5.AppendLine("    From TblBOQHdr A ");
                    //SQL5.AppendLine("    Inner Join TblDocApproval D On D.DocType='BOQ' And A.DocNo=D.DocNo ");
                    //SQL5.AppendLine("    Inner Join TblUser E On D.UserCode=E.UserCode ");
                    //SQL5.AppendLine("    Inner Join TblDocApprovalSetting F On D.ApprovalDNo=F.DNo And F.DocType='BOQ' And F.StartAmt < 10000000000 ");
                    //SQL5.AppendLine("    Where A.Status='A' ");
                    //SQL5.AppendLine("    And A.DocNo=@DocNo  ");
                    SQL5.AppendLine(") T1  ");
                    SQL5.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode  ");
                    SQL5.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode  ");
                    SQL5.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature'  ");
                    SQL5.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName  ");
                    SQL5.AppendLine("Order By T1.Seq;  ");
                }
                else
                {
                    SQL5.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature,   ");
                    SQL5.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt   ");
                    SQL5.AppendLine("From (   ");
                    SQL5.AppendLine("    Select Distinct D.UserCode, E.UserName,  ");
	                SQL5.AppendLine("    D.ApprovalDNo As DNo, F.Level As level, 'Approved By' As Title, Left(D.LastUpDt, 8) As LastUpDt  ");
	                SQL5.AppendLine("    From TblBOQHdr A  ");
	                SQL5.AppendLine("    Inner Join TblDocApproval D On D.DocType='BOQ' And A.DocNo=D.DocNo  ");
	                SQL5.AppendLine("    Inner Join TblUser E On D.UserCode=E.UserCode  ");
                    SQL5.AppendLine("    Inner Join TblDocApprovalSetting F On D.ApprovalDNo=F.DNo And F.DocType='BOQ' And F.StartAmt < 10000000000 ");
	                SQL5.AppendLine("    Where A.Status='A'  ");
                    SQL5.AppendLine("   	And A.DocNo=@DocNo   ");
                    if (mIsBOQPrintOutShowCreatedUser)
                    {
                        SQL5.AppendLine("    Union All   ");
                        SQL5.AppendLine("    Select Distinct   ");
                        SQL5.AppendLine("    A.CreateBy As UserCode, B.UserName,   ");
                        SQL5.AppendLine("    '1' As DNo, 0 As level, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt   ");
                        SQL5.AppendLine("    From TblBOQHdr A   ");
                        SQL5.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode   ");
                        SQL5.AppendLine("    Where A.Status='A'   ");
                        SQL5.AppendLine("    And A.DocNo=@DocNo   ");
                    }
                    SQL5.AppendLine(") T1   ");
                    SQL5.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode   ");
                    SQL5.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode   ");
                    SQL5.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature'   ");
                    SQL5.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName   ");
                    SQL5.AppendLine("Order By T1.Level;   ");
                    
                }
                cm5.CommandText = SQL5.ToString();
                Sm.CmParam<String>(ref cm5, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cm5, "@DocNo", TxtDocNo.Text);
                var dr5 = cm5.ExecuteReader();
                var c5 = Sm.GetOrdinal(dr5, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                if (dr5.HasRows)
                {
                    while (dr5.Read())
                    {

                        l5.Add(new BOQDtl3()
                        {
                            Signature = Sm.DrStr(dr5, c5[0]),
                            UserName = Sm.DrStr(dr5, c5[1]),
                            PosName = Sm.DrStr(dr5, c5[2]),
                            Space = Sm.DrStr(dr5, c5[3]),
                            DNo = Sm.DrStr(dr5, c5[4]),
                            Title = Sm.DrStr(dr5, c5[5]),
                            LastUpDt = Sm.DrStr(dr5, c5[6])
                        });
                    }
                }
                dr5.Close();
            }
            myLists.Add(l5);
            #endregion

            if (mRptName.Length > 0)
            {
                if (mRptName == "YK")
                {
                    Sm.PrintReport("BOQ" + mRptName, myLists, TableName, false);
                    Sm.PrintReport("BOQ2" + mRptName, myLists, TableName, false);
                }
            }
            else
            {
                Sm.PrintReport("BOQ", myLists, TableName, false);
                Sm.PrintReport("BOQ2", myLists, TableName, false);
            }
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnLOPDocNo_Click(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmBOQDlg2(this));
            }
        }

        private void BtnCtContactPersonName_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mMenuCode = "RSYYNNN";
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode), string.Empty);
                }
            }
        }

        private void BtnCtReplace_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer") &&
                !Sm.IsLueEmpty(LueCurCode, "Currency"))
                Sm.FormShowDialog(new FrmBOQDlg(this, Sm.GetLue(LueCurCode)));
        }

        private void BtnCtReplace2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                if (TxtCtReplace.EditValue != null && TxtCtReplace.Text.Length != 0)
                {
                    var f = new FrmBOQ(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtCtReplace.Text;
                    f.ShowDialog();
                }                
            }
        }

        #endregion

        #region Misc Control events

        private void LueResourceType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueResourceType, new Sm.RefreshLue2(Sl.SetLueOption), "ResourceType");
            }
        }

        private void LueResourceType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueResourceType_Leave(object sender, EventArgs e)
        {
            if (LueResourceType.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LueResourceType).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 3].Value =
                    Grd2.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueResourceType);
                    Grd2.Cells[fCell.RowIndex, 4].Value = LueResourceType.GetColumnValue("Col2");
                }
                LueResourceType.Visible = false;
            }
        }

        private void LueResourceName_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueResourceName, new Sm.RefreshLue1(SetLueResourceName));
            }
        }

        private void LueResourceName_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueResourceName_Leave(object sender, EventArgs e)
        {
            if (LueResourceName.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (Sm.GetLue(LueResourceName).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 5].Value =
                    Grd2.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueResourceName);
                    Grd2.Cells[fCell.RowIndex, 6].Value = LueResourceName.GetColumnValue("Col2");
                }
                LueResourceName.Visible = false;
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                if (Sm.GetLue(LueCtCode).Length == 0)
                {
                    LueCtContactPersonName.EditValue = null;
                    Sm.SetControlReadOnly(LueCtContactPersonName, true);
                }
                else
                {
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode), string.Empty);
                    Sm.SetControlReadOnly(LueCtContactPersonName, false);
                }
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtCtReplace  
                });
                
                Sm.ClearGrd(Grd2, true);
                LoadAllItemBOQ();
            }
        }

        private void LueCtContactPersonName_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueCtContactPersonName, new Sm.RefreshLue3(SetLueCtPersonCode), Sm.GetLue(LueCtCode), string.Empty);
        }

        private void LueRingCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueRingCode, new Sm.RefreshLue1(Sl.SetLueRingCode));
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LueAgingAP_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueAgingAP, new Sm.RefreshLue1(Sl.SetLueAgingAP));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
                TxtCtReplace.EditValue = null;
                Sm.ClearGrd(Grd2, true);
                if (Sm.GetLue(LueCurCode).Length > 0 && Sm.GetLue(LueCtCode).Length > 0)
                    LoadAllItemBOQ();
            }
        }

        private void TxtCreditLimit_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtCreditLimit, 0);
                ComputePercentage();
            }
        }


        private void TxtDirectCost_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDirectCost, 0);
                ComputeSubTotalRBPS();
            }
        }

        private void TxtIndirectCost_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtIndirectCost, 0);
                ComputeSubTotalRBPS();
            }
        }

        private void TxtPolicy_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtPolicy, 0);
                ComputeSubTotalRBPS();
            }
        }


        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void ChkTaxInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ComputeTax();
                if (ChkTaxInd.Checked)
                {
                    LuePPN.Properties.ReadOnly = false;
                    LuePPN.BackColor = Color.FromArgb(255, 255, 255);
                }
                else
                {
                    LuePPN.Properties.ReadOnly = true;
                    LuePPN.BackColor = Color.FromArgb(195, 195, 195);
                }
            }
        }

        private void LuePPN_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePPN, new Sm.RefreshLue2(Sl.SetLueOption), "PPNForProject");
                if (Sm.GetLue(LuePPN) == "" && isinsert)
                    Sm.SetLue(LuePPN, mPPNDefaultSetting);
                if (ChkTaxInd.Checked)
                {
                    ComputeTax();
                }
            }
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectImplementationProcessInd");
            }
        }

        #endregion

        #endregion

        #region Class

        private class BOQ
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }

            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string ProjectName { get; set; }
            public string BOQPrintOutBODName { get; set; }
            public string SignedCity { get; set; }
        }

        private class BOQDtl
        {
            public string DocNo { get; set; }
            public string ItBOQCode { get; set; }

            public string ItemBOQ { get; set; }
            public string ItBOQName { get; set; }
            public decimal Amt { get; set; }
            public string Allow { get; set; }
            public string ResourceTypeCode { get; set; }

            public string ResourceTypeDesc { get; set; }
            public string ResourceNameCode { get; set; }
            public string ResourceNameDesc { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }

            public decimal Mth { get; set; }
            public decimal Total { get; set; }
            public int Level { get; set; }
            public float Indent { get; set; }

            public string ItBOQCode2 { get; set; }
        }

        private class ItemBOQ
        {
            public string ItBOQCode { get; set; }
            public string ItBOQName { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public float Indent { get; set; }
            public string Index1 { get; set; }
            public string Index2 { get; set; }
        }

        private class BOQTot
        {
            public string DocNo { get; set; }
            public decimal Amt { get; set; }
            public decimal AmtBulat { get; set; }
            public decimal PPn { get; set; }
            public decimal TotalAmt { get; set; }
            public string Terbilang { get; set; }
        }

        private class BOQDtl2
        {
            public string ItBOQCode { get; set; }
            public string ItBOQCode2 { get; set; }

            public string ItemBOQ { get; set; }
            public string ItBOQName { get; set; }
            public string Parent { get; set; }
            public decimal Amt { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }

            public decimal Mth { get; set; }
            public decimal Total { get; set; }
            public int Level { get; set; }
            public float Indent { get; set; }
            public decimal TotalAmt { get; set; }
            public string Allow { get; set; }
        }

        private class BOQDtl3
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }
        #endregion         

    }
}
