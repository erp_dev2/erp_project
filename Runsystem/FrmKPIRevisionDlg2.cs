﻿#region Update
/*
    23/08/2017 [HAR] tambah source KPI, edit cuma bisa menonaktifkan document
    20/02/2019 [MEY] dialog untuk menu baru master kpi revision 
    04/03/2019 [TKG] menghilang validasi performance review.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmKPIRevisionDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmKPIRevision mFrmParent;
        private string mSQL=string.Empty;

        #endregion

        #region Constructor

        public FrmKPIRevisionDlg2(FrmKPIRevision FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List Of KPI";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "KPI#", 
                        "KPI Name",
                        "Site",
                        "Year",
                        "PIC Code",
                      
                        //6
                        "PIC Name",
                        "Respons by"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 200, 200, 80, 80,
                        //6-7
                       150, 100

                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] {0, 1, 2, 3, 4, 5, 6, 7 } );
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            string CurrentDateTime2 = Sm.ServerCurrentDateTime();

            SQL.AppendLine("Select * from (  ");
            SQL.AppendLine("Select A.Docno, A.DocDt, A.ActInd, A.KPIName, A.PICCode, C.EmpName, B.KPIName As KPIParent, E.PosName, C.SiteCode, D.SiteName, left(A.DocDt, 4)  as  Yr ");
            SQL.AppendLine("From TblKPIHdr A ");
            SQL.AppendLine("Inner Join TblEmployee C On A.PICCode = C.EmpCode");
            SQL.AppendLine("Left Join TblKPIHdr B ON A.KPIParent = B.DocNo ");
            SQL.AppendLine("Inner Join TblSite D On C.SiteCode=D.SiteCode ");
            SQL.AppendLine("Inner Join TblPosition E On E.PosCode=C.PosCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            // SQL.AppendLine("And A.DocNo Not In (Select KPIDocNo From TblPerformanceReviewHdr) ");
            SQL.AppendLine(")T ");

            //if (Sm.GetLue(LueYr).Length > 0)
            //    SQL.AppendLine("and Left(A.DocDt, 4) ='"+Sm.GetLue(LueYr).+"' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsLueEmpty(LueYr, "Year")) return;
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0=0  ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtKPIDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtKPIName.Text, new string[] { "T.KPIName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "T.Yr", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.DocNo;",
                        new string[] 
                        { 
                             //0
                            "DocNo",
                            //1-5
                            "KPIName",  "SiteName", "PICCode",  "EmpName", "PosName", "Yr"                          
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Row, 1), 1);
                mFrmParent.TxtKPIDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }
      
        #endregion

        #region Additional Method

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (mFrmParent.mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By T.SiteName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkKPIDocNo_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "KPI#");
        }

        private void ChkKPIName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "KPI Name");
        }

        private void TxtKPIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtKPIName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }
        #endregion    
    }
}
