﻿#region Update
/*
    06/12/2017 [TKG] Customized GL
    12/04/2018 [TKG] bug perhitungan opening balance
    07/07/2018 [TKG] tambah filter document#, account#, account description
    12/07/2018 [TKG] tambah journal description, menu code, menu description
    15/10/2019 [WED/IMS] filter COA Alias
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptGL2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptGL2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, 0);
                Sl.SetLueAcNo(ref LueCOAAc);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Account#",
                    "Description",
                    "Opening"+Environment.NewLine+"Balance",
                    "Document#",
                    "Date",
                   
                    //6-10
                    "Journal"+Environment.NewLine+"Description",
                    "Menu Code",
                    "Menu Description",
                    "Debit",
                    "Credit",

                    //11-12
                    "Closing"+Environment.NewLine+"Balance",
                    "Alias"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 200, 130, 130, 80,
                    
                    //6-10
                    200, 100, 200, 130, 130, 
                    
                    //11-12
                    130, 130
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 9, 10, 11 }, 0);
            Grd1.Cols[12].Move(2);
            Sm.SetGrdProperty(Grd1, false);         
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            var l1 = new List<Data1>();
            var l2 = new List<Data2>();
            var l3 = new List<Data2>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref l1);
                if (l1.Count > 0)
                {
                    Process2(ref l2);
                    Process3(ref l1, ref l2);
                    Process4(ref l2, ref l3);
                    Process5(ref l3);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l1.Clear();
                l2.Clear();
                l3.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void Process1(ref List<Data1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Dt = Sm.GetDte(DteDocDt1);
            var Yr = Sm.Left(Dt, 4);
            string Filter = " ";

            Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "A.AcDesc", false);
            
            if (ChkAcNo.Checked)
                Filter += " And A.AcNo Like '" + TxtAcNo.Text.Replace("'", "") + "%' ";

            if (ChkCOAAc.Checked)
                Filter += " And A.AcNo Like '" + Sm.GetLue(LueCOAAc).Replace("'", "") + "%' ";

            if (ChkAlias.Checked)
                Filter += " And A.Alias Like '" + TxtAlias.Text.Replace("'", "") + "%' ";
            
            SQL.AppendLine("Select A.AcNo, A.Alias, A.AcDesc, IfNull(B.Amt, 0.00)-IfNull(C.Amt, 0.00) As Amt ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.AcNo, Sum(T2.Amt) As Amt ");
            SQL.AppendLine("    From TblCOAOpeningBalanceHdr T1, TblCOAOpeningBalanceDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@Yr ");
            SQL.AppendLine("    Group By T2.AcNo ");
            SQL.AppendLine(") B On A.AcNo=B.AcNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.AcNo, Sum(T2.DAmt-T2.CAmt) As Amt ");
            SQL.AppendLine("    From TblJournalHdr T1, TblJournalDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T1.DocDt<@Dt ");
            SQL.AppendLine("    And Left(T1.DocDt, 4)=@Yr ");
            SQL.AppendLine("    Group By T2.AcNo ");
            SQL.AppendLine(") C On A.AcNo=C.AcNo ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("And A.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.AcNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParamDt(ref cm, "@Dt", Dt);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Amt", "Alias" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data1()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            OpeningBalance = Sm.DrDec(dr, c[2]),
                            Alias = Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Data2> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var AcNoTemp = string.Empty;
            var AcNoPrev = string.Empty;
            int i = 0;
            string Filter = " ";

            Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
            Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "C.AcDesc", false);
            
            if (ChkAcNo.Checked)
                Filter += " And C.AcNo Like '" + TxtAcNo.Text.Replace("'", "") + "%' ";

            if (ChkCOAAc.Checked)
                Filter += " And C.AcNo Like '" + Sm.GetLue(LueCOAAc).Replace("'", "") + "%' ";

            if (ChkAlias.Checked)
                Filter += " And C.Alias Like '" + TxtAlias.Text.Replace("'", "") + "%' ";

            SQL.AppendLine("Select B.AcNo, C.Alias, C.AcDesc, A.DocNo, A.DocDt, A.JnDesc, A.MenuCode, A.MenuDesc, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo And (B.DAmt<>0.00 Or B.CAmt<>0.00) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By B.AcNo, A.DocDt, A.DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    "AcNo", 
                    "AcDesc", "DocNo", "DocDt", "JnDesc", "MenuCode", 
                    "MenuDesc", "DAmt", "CAmt", "Alias"
                    //"Remark"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        AcNoTemp = Sm.DrStr(dr, c[0]);
                        if (Sm.CompareStr(AcNoTemp, AcNoPrev))
                            i += 1;
                        else
                            i = 1;
                        l.Add(new Data2()
                        {
                            AcNo = AcNoTemp,
                            AcDesc = Sm.DrStr(dr, c[1]),
                            SeqNo = i,
                            DocNo = Sm.DrStr(dr, c[2]),
                            DocDt = Sm.DrStr(dr, c[3]),
                            JnDesc = Sm.DrStr(dr, c[4]),
                            MenuCode = Sm.DrStr(dr, c[5]),
                            MenuDesc = Sm.DrStr(dr, c[6]),
                            DAmt = Sm.DrDec(dr, c[7]),
                            CAmt = Sm.DrDec(dr, c[8]),
                            Alias = Sm.DrStr(dr, c[9]),
                            OpeningBalance = 0m,
                            Opening = 0m,
                            Balance = 0m
                        });
                        AcNoPrev = AcNoTemp;
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Data1> l1, ref List<Data2> l2)
        {
            int x = 0, v = 0;
            bool IsFind = false;
            for (int i = 0; i < l1.Count; i++)
            {
                x = v;
                IsFind = false;
                for (int j = x; j < l2.Count; j++)
                {
                    if (Sm.CompareStr(l1[i].AcNo, l2[j].AcNo))
                    {
                        l2[j].OpeningBalance = l1[i].OpeningBalance;
                        v = j;
                        IsFind = true;
                    }
                    else
                    {
                        if (IsFind) break;
                    }
                }
                l2.Add(new Data2()
                {
                    AcNo = l1[i].AcNo,
                    Alias = l1[i].Alias,
                    AcDesc = l1[i].AcDesc,
                    SeqNo = 0,
                    DocNo = string.Empty,
                    DocDt = string.Empty,
                    JnDesc = string.Empty,
                    MenuCode = string.Empty,
                    MenuDesc = string.Empty,
                    DAmt = 0m,
                    CAmt = 0m,
                    OpeningBalance = l1[i].OpeningBalance,
                    Opening = 0m,
                    Balance = 0m
                });
            }
        }

        private void Process4(ref List<Data2> l1, ref List<Data2> l2)
        {
            var Amt = 0m;
            foreach (var x in l1.OrderBy(o1 => o1.AcNo).ThenBy(o2 => o2.SeqNo))
            {
                if (x.SeqNo == 0)
                    Amt = x.OpeningBalance;
                else
                {
                    x.Opening = Amt;
                    x.Balance = x.Opening + x.DAmt - x.CAmt;
                    Amt = x.Balance;
                }
                l2.Add(new Data2()
                {
                    AcNo = x.AcNo,
                    Alias = x.Alias,
                    AcDesc = x.AcDesc,
                    SeqNo = x.SeqNo,
                    DocNo = x.DocNo,
                    DocDt = x.DocDt,
                    JnDesc = x.JnDesc,
                    MenuCode = x.MenuCode,
                    MenuDesc = x.MenuDesc,
                    OpeningBalance = x.OpeningBalance,
                    Opening = x.Opening,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt,
                    Balance = x.Balance
                    //Remark = x.Remark
                });
            }
        }

        private void Process5(ref List<Data2> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {

                    r = Grd1.Rows.Add();
                    r.Cells[0].Value = i + 1;
                    r.Cells[1].Value = l[i].AcNo;
                    r.Cells[2].Value = l[i].AcDesc;
                    if (l[i].DAmt == 0 && l[i].CAmt == 0)
                        r.Cells[3].Value = l[i].OpeningBalance;
                    else
                        r.Cells[3].Value = 0m;
                    r.Cells[4].Value = l[i].DocNo;
                    if (l[i].DocDt.Length > 0) r.Cells[5].Value = Sm.ConvertDate(l[i].DocDt);
                    r.Cells[6].Value = l[i].JnDesc;
                    r.Cells[7].Value = l[i].MenuCode;
                    r.Cells[8].Value = l[i].MenuDesc;
                    r.Cells[9].Value = l[i].DAmt;
                    r.Cells[10].Value = l[i].CAmt;
                    r.Cells[11].Value = l[i].Balance;
                    r.Cells[12].Value = l[i].Alias;
            }
            Grd1.GroupObject.Add(1);
            Grd1.Group();
            Sm.SetGrdAlwaysShowSubTotal(Grd1);
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10 });
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Journal#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account#");
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account Description");
        }

        private void LueCOAAc_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCOAAc, new Sm.RefreshLue1(Sl.SetLueAcNo));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCOAAc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "COA's account");
        }

        private void ChkAlias_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Alias");
        }

        private void TxtAlias_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

        #region Class

        private class Data1
        {
            public string AcNo { get; set; }
            public string Alias { get; set; }
            public string AcDesc { get; set; }
            public decimal OpeningBalance { get; set; }
        }

        private class Data2
        {
            public string AcNo { get; set; }
            public string Alias { get; set; }
            public string AcDesc { get; set; }
            public decimal OpeningBalance { get; set; }
            public int SeqNo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string JnDesc { get; set; }
            public string MenuCode { get; set; }
            public string MenuDesc { get; set; }
            public decimal Opening { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal Balance { get; set; }
            //public string Remark { get; set; }
        }

        #endregion
    }
}
