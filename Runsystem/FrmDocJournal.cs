﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion


namespace RunSystem
{


    public partial class FrmDocJournal : RunSystem.FrmBase5
    {
        #region Field

        internal string
            mMenuCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        private string mSQL = string.Empty;
        private int TotalSelected;
        List<JournalList> ListOfJournal = new List<JournalList>();

        #endregion

        #region Constructor

        public FrmDocJournal(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                ListOfJournal.Clear();
                ListOfJournal.Add(new JournalList()
                {
                    TrnNo = 0,
                    TrnName = "DO to Department",
                    TableName = "tbldodepthdr",
                    SQL = "Select Distinct 0 As TrnType, 'DO To Depatment' As TrnName, "+
                          "H1.DocNo, '' As DNo, H1.DocDt, concat(D1.Dtl, '\n', ifnull(H1.Remark, '')) As Remark from tbldodepthdr H1 " +
                          "Left Join "+
                          "(Select DD.DocNo, group_concat(' ', concat(round(DD.Qty, 2), ' ', II.InventoryUOMCode, ' ', II.ItName)) As Dtl " +
                          "from tbldodeptdtl DD "+
                          "inner join tblitem II on DD.ItCode=II.ItCode "+
                          "where CancelInd='N' "+
                          "group by DocNo) D1 on D1.DocNo=H1.DocNo "+
                          "Where ifnull(H1.JournalDocNo, '') = ''"
                });
                
                ListOfJournal.Add(new JournalList()
                {
                    TrnNo = 1,
                    TrnName = "DO to Department (Return)",
                    TableName = "tbldodeptdtl",
                    SQL = "Select Distinct 1 As TrnType, 'DO to Department (Return)'  As TrnName, "+
                          "DD.DocNo, DD.DNo, HH.DocDt, concat(II.ItName, '. ', ifnull(DD.Remark, '')) As Remark from tbldodeptdtl DD " +
                          "Inner Join tbldodepthdr HH On DD.DocNo=HH.DocNo " +
                          "Inner Join tblitem II On DD.ItCode=II.ItCode " +
                          "Where CancelInd='Y' And ifnull(DD.JournalDocNo, '') = ''"
                });

                base.FrmLoad(sender, e);

                SetGrd();
                SetSQL();
                

                string SQLLue = string.Empty;
                for (int intX = 0; intX < ListOfJournal.Count; intX++) 
                {
                    if (SQLLue != string.Empty) SQLLue = SQLLue + " union all ";
                    SQLLue = SQLLue + "Select " + ListOfJournal[intX].TrnNo.ToString() + " As Col1, '" + ListOfJournal[intX].TrnName + "' As Col2 "; 

                }

                Sm.SetLue2(ref LueTrnCode, "Select A.* from (" + SQLLue + ") A Order By A.Col1",
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "Posting",

                        //1-5
                        "Document"+Environment.NewLine+"Type",
                        "Document"+Environment.NewLine+"Type",
                        "Document Date",
                        "Document"+Environment.NewLine+"Number",
                        "Document"+Environment.NewLine+"Detail No",

                        //6
                        "Notes",
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdColHdrAutoAlign(Grd1);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            StringBuilder SQL = new StringBuilder();

            for (int intX = 0; intX < ListOfJournal.Count; intX++)
            {
                if (SQL.ToString() != string.Empty) SQL.AppendLine(" Union All ");
                SQL.AppendLine(ListOfJournal[intX].SQL);
            }

            mSQL = "Select A.* from (" + SQL.ToString() + ") A ";
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                lblProgress.Text = string.Empty;
                PG.Value = 0;
                TotalSelected = 0;
                Application.DoEvents();
                DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);


                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                if (Sm.GetLue(LueTrnCode).Length != 0)
                    Filter += (Filter == string.Empty ? " Where " : " And ") + " A.TrnType = " + Sm.GetLue(LueTrnCode);


                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.TrnType, A.DocDt, A.DocNo;",
                        new string[] { "TrnType", "TrnName", "DocDt", "DocNo", "DNo", "Remark" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = ChkAutoChoose.Checked;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                        }, true, false, true, true
                    );
                if (Grd1.Rows.Count > 1) Grd1.Rows.RemoveAt(Grd1.Rows.Count - 1);
                //Grd1.Rows.AutoHeight();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private MySqlCommand UpdateJournalDocNo(string TableName, string DocNo, string DNo, string JournalDocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Update " + TableName + " Set JournalDocNo='" + JournalDocNo + "' ");
            SQL.AppendLine("Where DocNo='" + DocNo + "' ");
            if (DNo != string.Empty)
                SQL.AppendLine("And DNo='" + DNo + "' ");
            var cm = new MySqlCommand(){ CommandText = SQL.ToString()};
            return cm;
        }

        private int IsPeriodClosed()
        {
            List<string> OpenPeriod = new List<string> ();
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var dr = new MySqlCommand("Select Period from tblaccountperiod where Status='O'", cn).ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Period") };
                    while (dr.Read())
                        OpenPeriod.Add(Sm.DrStr(dr, c[0]));
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
            }
            finally
            {
            }

            int tSelected = 0;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 && (Sm.GetGrdBool(Grd1, Row, 0)))
                {
                    bool PrdClosed = true;
                    for (int iPrd = 0; iPrd < OpenPeriod.Count; iPrd++)
                        if (OpenPeriod[iPrd] == Sm.GetGrdDate(Grd1, Row, 3).Substring(0, 6))
                            PrdClosed = false;
                    if (PrdClosed)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Unable to Post One or more data. Period " + Sm.GetGrdDate(Grd1, Row, 3).Substring(4, 2) + "/" + Sm.GetGrdDate(Grd1, Row, 3).Substring(0, 4) + " is closed !");
                        return -1;
                    }
                    else
                        tSelected++;
                }
            }
            return tSelected;
        }



        private void BGWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var Worker = sender as BackgroundWorker;
            int CurrentRow = 0;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 && (Sm.GetGrdBool(Grd1, Row, 0)))
                {
                    CurrentRow++;
                    Worker.ReportProgress((CurrentRow * 100) / TotalSelected);
                    //lblProgress.Text = "Processing " + CurrentRow.ToString() + " of " + TotalSelected.ToString() + " Rows";
                    string JournalDocNo = string.Empty;
                    string DocNo = Sm.GetGrdStr(Grd1, Row, 4);
                    string DNo = Sm.GetGrdStr(Grd1, Row, 5);

                    switch (Sm.GetGrdInt(Grd1, Row, 1))
                    {
                        case 0:
                            string DocDt = Sm.GetValue("Select DocDt from tbldodepthdr where DocNo='" + DocNo + "' and ifnull(JournalDocNo, '') = ''");
                            if (DocDt != string.Empty)
                            {
                                JournalDocNo = Sm.GenerateDocNoJournal(DocDt, 1);

                                //var cml = new List<MySqlCommand>();

                                //var fDODept = new FrmDODept2(mMenuCode);
                                //cml.Add(fDODept.SaveJournal(DocNo, DocDt, JournalDocNo));
                                //cml.Add(UpdateJournalDocNo("tbldodepthdr", DocNo, DNo, JournalDocNo));
                                //Sm.ExecCommands(cml);
                            }
                            break;
                    }
                }
            }
            
        }

        private void BGWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            PG.Value = e.ProgressPercentage;

        }

        private void BGWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // TODO: do something with final calculation.
            
            Sm.StdMsg(mMsgType.Info, TotalSelected.ToString() + " number of rows are successfully processed. "+ Environment.NewLine + "Please click on refresh button to retrieve other data.");
            Sm.ClearGrd(Grd1, false);
        }

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Question", "Are you sure you want to do Journal Posting on selected rows ? " + Environment.NewLine + "This process cannot be Undone.") == DialogResult.No) return;
                
                TotalSelected = IsPeriodClosed();

                if (TotalSelected <= 0) 
                {
                    if (TotalSelected == 0) 
                        Sm.StdMsg(mMsgType.NoData, "");
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;

                PG.Value = 0;
                lblProgress.Text = "Processing "+TotalSelected.ToString() + " Rows";
                BGWorker.RunWorkerAsync();

                
                

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                e.DoDefault = false;
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                string DocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                switch (Sm.GetGrdInt(Grd1, e.RowIndex, 1))
                {
                    case 0:
                        var fDODept = new FrmDODeptWO(mMenuCode);
                        fDODept.WindowState = FormWindowState.Normal;
                        fDODept.StartPosition = FormStartPosition.CenterScreen;
                        fDODept.TopMost = true;
                        fDODept.Show();
                        fDODept.ShowData(DocNo);
                        break;
                }
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if ((e.ColIndex == 0 || e.ColIndex == 1))
            {
                if (Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex))
                    Grd1.Cells[e.RowIndex, (e.ColIndex == 0) ? 1 : 0].Value = false;
            }
        }

        private void ChkAutoChoose_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkAutoChoose.Checked) ChkAutoChoose.Text = "Untick All Document";
            else ChkAutoChoose.Text = "Tick All Document";
            for (int row = 0; row < Grd1.Rows.Count; row++)
                if (Sm.GetGrdStr(Grd1, row, 4).Length != 0)
                    Grd1.Cells[row, 0].Value = ChkAutoChoose.Checked;

        }
        #endregion

        #region Misc Control Event

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueTrnCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);

        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Transaction Type");
        }
        #endregion
    }

    class JournalList
    {
        public int TrnNo { set; get; }
        public string TrnName { set; get; }
        public string TableName { get; set; }
        public string SQL { get; set; }
    }
}
