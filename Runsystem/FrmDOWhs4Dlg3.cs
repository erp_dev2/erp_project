﻿#region Update
/*
    30/09/2020 [WED/IMS] new apps
    15/12/2020 [WED/IMS] tarik data Transfer Request Project yg aktif dan approved detail nya
    16/04/2021 [WED/IMS] Project Code nya ngikut dari PGCode di TransferRequestProjectHdr, bukan ngikut SO Contract nya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOWhs4Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOWhs4 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOWhs4Dlg3(FrmDOWhs4 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -2);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.WhsCode, C.WhsName, A.PGCode, F.ProjectCode, F.ProjectName, E.PONo, A.SOContractDocNo, A.ItCode, D.ItName, A.Remark ");
            SQL.AppendLine("From TblTransferRequestProjectHdr A ");
            SQL.AppendLine("Inner Join "); // transfer request project yg baru itu whscode di detail nya null.
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct T1.DocNo ");
            SQL.AppendLine("    From TblTransferRequestProjectHdr T1 ");
            SQL.AppendLine("    Inner Join TblTransferRequestProjectDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T2.WhsCode Is Null ");
            SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("        And T2.Status = 'A' ");
            SQL.AppendLine("        And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And T2.ProcessInd In ('O', 'P') ");
            SQL.AppendLine(") B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
            SQL.AppendLine("Left Join TblItem D On A.ItCode = D.ItCode ");
            SQL.AppendLine("Left Join TblSOContractHdr E On A.SOContractDocNo = E.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup F On A.PGCode = F.PGCode ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T1.DocNo SOContractDocNo, T4.PGCode, T4.ProjectName, T4.ProjectCode, T1.PONo ");
            //SQL.AppendLine("    From TblSOContractHdr T1 ");
            //SQL.AppendLine("    Inner Join TblBOQHDr T2 On T1.BOQDocNo = T2.DocNo And T1.CancelInd = 'N' ");
            //SQL.AppendLine("    Inner Join TblLOPHdr T3 On T2.LOPDocNo = T3.DocNo ");
            //SQL.AppendLine("    Left Join TblProjectGroup T4 On T3.PGCode = T4.PGCode ");
            //SQL.AppendLine(") E On A.SOContractDocNo = E.SOContractDocNo And A.PGCode = E.PGCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "",
                    "Date",
                    "Local",
                    "Warehouse Code",

                    //6-10
                    "Warehouse (To)",
                    "Project Code",
                    "Project Name",
                    "Customer PO#",
                    "SO Contract#",

                    //11-14
                    "Item's Code",
                    "Item's Name",
                    "Remark",
                    "PGCode"
                },
                new int[]
                {
                    //
                    50,

                    //1-5
                    180, 20, 80, 120, 100, 

                    //6-10
                    200, 120, 200, 180, 180, 

                    //11-14
                    100, 200, 200, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 11, 14 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc; ",
                    new string[]
                    {
                        "DocNo", 
                        "DocDt", "LocalDocNo", "WhsCode", "WhsName", "ProjectCode", 
                        "ProjectName","PONo", "SOContractDocNo", "ItCode", "ItName", 
                        "Remark", "PGCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtTransferRequestProjectDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                Sm.SetLue(mFrmParent.LueWhsCode2, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5));
                mFrmParent.TxtProjectCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.TxtProjectName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                mFrmParent.TxtSOCDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10);
                Sm.SetLue(mFrmParent.LueItCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11));
                mFrmParent.mPGCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 14);

                mFrmParent.ClearGrd();

                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmTransferRequestProject2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmTransferRequestProject2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse To");
        }

        #endregion

        #endregion

    }
}
