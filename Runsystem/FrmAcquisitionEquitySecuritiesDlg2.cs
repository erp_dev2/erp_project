﻿#region Update

/*
    11/04/2022 [SET/PRODUCT] Menu dialog baru
    11/05/2022 [IBL/PRODUCT] Menambahkan choose data dengan double click
    17/05/2022 [SET/PRODUCT] Feedback merubah & tambah source Investment Code -> PortofolioId
    30/05/2022 [SET/PRODUCT] menyesuaikan choose data InvestmentCode & InvestmentName
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAcquisitionEquitySecuritiesDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmAcquisitionEquitySecurities mFrmParent;
        string mSQL = string.Empty;
        private int mRow = 0;

        #endregion

        #region Constructor

        public FrmAcquisitionEquitySecuritiesDlg2(FrmAcquisitionEquitySecurities FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.PortofolioName InvestmentName, A.PortofolioId InvestmentCode, C.InvestmentCtCode, C.InvestmentCtName, A.UomCode, B.CurCode, A.InvestmentEquityCode ");
            SQL.AppendLine("FROM tblinvestmentitemEquity A ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolio B ON A.PortofolioId = B.PortofolioId ");
            SQL.AppendLine("INNER JOIN tblinvestmentcategory C ON B.InvestmentCtCode = C.InvestmentCtCode ");
            SQL.AppendLine("WHERE A.ActInd = 'Y' ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.ReadOnly = true;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Investment Name",
                        "Investment Code",
                        "InvestmentCtCode",
                        "Category",
                        "UoM",

                        //6-7
                        "CurCode",
                        "InvestmentEquityCode"

                    },
                     new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 150, 100, 100, 100,

                        //6-7
                        25, 100
                    }
                );
            //Sm.GrdColButton(Grd1, new int[] { 2 });
            //Sm.GrdFormatDate(Grd1, new int[] { 3, 5 });
            //Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            //if (
            //    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
            //    Sm.IsDteEmpty(DteDocDt2, "End date") ||
            //    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
            //    ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtInvestment.Text, new string[] { "PortofolioName", "A.PortofolioId" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt;",
                        new string[]
                        {
                            //0
                            "InvestmentName", 

                            //1-5
                            "InvestmentCode", "InvestmentCtCode", "InvestmentCtName", "UOMCode", "CurCode",

                            //6
                            "InvestmentEquityCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.Grd1.Cells[mRow, 1].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.Grd1.Cells[mRow, 2].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.Grd1.Cells[mRow, 5].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.Grd1.Cells[mRow, 6].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                mFrmParent.Grd1.Cells[mRow, 8].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.Grd1.Cells[mRow, 13].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.TxtCurCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                this.Close();
            }
        }

        #endregion

        #region Event

        private void ChkInvestment_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment");
        }

        private void TxtInvestment_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

    }
}
