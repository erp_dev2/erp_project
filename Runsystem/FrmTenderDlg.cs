﻿#region Update
/*
    30/08/2018 [WED] bug LueVendor nggak bisa di klik
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTenderDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTender mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmTenderDlg(FrmTender FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of MR Item";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsPORequestFilterByDept ? "Y" : "N");
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Document#",
                        "DNo", 
                        "Date",
                        "Department",
                        "Item Code",
                        
                        //6-10
                        "Item Name",
                        "Quantity",
                        "Usage Date",
                        "Currency",
                        "Estimated"+Environment.NewLine+"Price",

                        //11
                        "Remark"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        120, 0, 80, 160, 100, 

                        //6-10
                        180, 80, 100, 80, 120, 

                        //11
                        200
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, B.DocDt, D.DeptName, A.ItCode, C.ItName, A.Qty, A.UsageDt, A.CurCode, A.EstPrice, A.Remark ");
            SQL.AppendLine("From  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select * ");
	        SQL.AppendLine("    From TblMaterialRequestDtl T1 ");
	        SQL.AppendLine("    Where T1.Status = 'A' ");
	        SQL.AppendLine("    And T1.CancelInd = 'N' ");
	        SQL.AppendLine("    And T1.ProcessInd = 'O' ");
            SQL.AppendLine("    And Concat(T1.DocNo, T1.DNo) Not In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("         Select Concat(T.MaterialRequestDocNo, T.MaterialRequestDNo) ");
            SQL.AppendLine("         From TblTender T ");
            SQL.AppendLine("         Where T.ActInd = 'Y' And T.Status = 'O' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr B ON A.DocNo = B.DocNo And (B.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode And C.ActInd = 'Y' ");
            SQL.AppendLine("Inner Join TblDepartment D On B.DeptCode = D.DeptCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "C.ItName", "C.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.DocDt, A.DocNo, C.ItName;",
                        new string[] 
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "DNo", "DocDt", "DeptName", "ItCode", "ItName",

                            //6-10
                            "Qty", "UsageDt", "CurCode", "EstPrice", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtMaterialRequestDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.mMRDocNo = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.mMRDNo = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtItName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtQty.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 7), 0);
                mFrmParent.TxtCurCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);
                mFrmParent.TxtEstPrice.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 10), 0);
                this.Hide();
            }
            else
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least one item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsPORequestFilterByDept ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
