﻿namespace RunSystem
{
    partial class FrmEmployeeUpdateApprovalDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label48 = new System.Windows.Forms.Label();
            this.TxtDisplayName = new DevExpress.XtraEditors.TextEdit();
            this.TxtEmpCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgGeneral = new System.Windows.Forms.TabPage();
            this.ChkActingOfficial = new DevExpress.XtraEditors.CheckEdit();
            this.DteContractDt = new DevExpress.XtraEditors.DateEdit();
            this.LblContractDt = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.LuePositionStatusCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtRegEntCode = new DevExpress.XtraEditors.TextEdit();
            this.label53 = new System.Windows.Forms.Label();
            this.DteTGDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.LueEntityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPOH = new DevExpress.XtraEditors.TextEdit();
            this.label69 = new System.Windows.Forms.Label();
            this.LblDivisionCode = new System.Windows.Forms.Label();
            this.LueDivisionCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.LueWorkGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.label64 = new System.Windows.Forms.Label();
            this.LueSection = new DevExpress.XtraEditors.LookUpEdit();
            this.label63 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.MeeDomicile = new DevExpress.XtraEditors.MemoExEdit();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtRTRW = new DevExpress.XtraEditors.TextEdit();
            this.LueMaritalStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.TxtVillage = new DevExpress.XtraEditors.TextEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.TxtSubDistrict = new DevExpress.XtraEditors.TextEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.LblEmploymentStatus = new System.Windows.Forms.Label();
            this.TxtBarcodeCode = new DevExpress.XtraEditors.TextEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.TxtShortCode = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.LueEmploymentStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label71 = new System.Windows.Forms.Label();
            this.LueCostGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.label67 = new System.Windows.Forms.Label();
            this.LueEducationType = new DevExpress.XtraEditors.LookUpEdit();
            this.label68 = new System.Windows.Forms.Label();
            this.LueShoeSize = new DevExpress.XtraEditors.LookUpEdit();
            this.label61 = new System.Windows.Forms.Label();
            this.LueClothesSize = new DevExpress.XtraEditors.LookUpEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtLevelCode = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.DteLeaveStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LblPGCode = new System.Windows.Forms.Label();
            this.LuePGCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label62 = new System.Windows.Forms.Label();
            this.LueBloodType = new DevExpress.XtraEditors.LookUpEdit();
            this.LblSystemType = new System.Windows.Forms.Label();
            this.LueSystemType = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMother = new DevExpress.XtraEditors.TextEdit();
            this.label57 = new System.Windows.Forms.Label();
            this.LblPayrunPeriod = new System.Windows.Forms.Label();
            this.LuePayrunPeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.DteWeddingDt = new DevExpress.XtraEditors.DateEdit();
            this.label59 = new System.Windows.Forms.Label();
            this.TxtBankAcName = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.LblGrdLvlCode = new System.Windows.Forms.Label();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.LueGrdLvlCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtBankBranch = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtBirthPlace = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtBankAcNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueReligion = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.LuePTKP = new DevExpress.XtraEditors.LookUpEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.LuePayrollType = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtNPWP = new DevExpress.XtraEditors.TextEdit();
            this.LueGender = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueCity = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtIdNumber = new DevExpress.XtraEditors.TextEdit();
            this.DteResignDt = new DevExpress.XtraEditors.DateEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.DteJoinDt = new DevExpress.XtraEditors.DateEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtUserCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtPostalCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LblPosCode = new System.Windows.Forms.Label();
            this.LuePosCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblDeptCode = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgEmployeeFamily = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnFamily = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TpgGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActingOfficial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePositionStatusCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRegEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDivisionCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWorkGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDomicile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRTRW.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaritalStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubDistrict.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBarcodeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEducationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShoeSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueClothesSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSystemType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMother.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrunPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReligion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePTKP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrollType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            this.TpgEmployeeFamily.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(902, 0);
            this.panel1.Size = new System.Drawing.Size(70, 616);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(902, 616);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.TxtEmpName);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtEmpCode);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(902, 50);
            this.panel3.TabIndex = 10;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label48);
            this.panel7.Controls.Add(this.TxtDisplayName);
            this.panel7.Controls.Add(this.TxtEmpCodeOld);
            this.panel7.Controls.Add(this.label66);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(552, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(346, 46);
            this.panel7.TabIndex = 18;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(32, 6);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(57, 14);
            this.label48.TabIndex = 12;
            this.label48.Text = "Old Code";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDisplayName
            // 
            this.TxtDisplayName.EnterMoveNextControl = true;
            this.TxtDisplayName.Location = new System.Drawing.Point(93, 24);
            this.TxtDisplayName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDisplayName.Name = "TxtDisplayName";
            this.TxtDisplayName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDisplayName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDisplayName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDisplayName.Properties.Appearance.Options.UseFont = true;
            this.TxtDisplayName.Properties.MaxLength = 13;
            this.TxtDisplayName.Size = new System.Drawing.Size(239, 20);
            this.TxtDisplayName.TabIndex = 17;
            // 
            // TxtEmpCodeOld
            // 
            this.TxtEmpCodeOld.EnterMoveNextControl = true;
            this.TxtEmpCodeOld.Location = new System.Drawing.Point(93, 3);
            this.TxtEmpCodeOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCodeOld.Name = "TxtEmpCodeOld";
            this.TxtEmpCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCodeOld.Properties.MaxLength = 50;
            this.TxtEmpCodeOld.Size = new System.Drawing.Size(239, 20);
            this.TxtEmpCodeOld.TabIndex = 13;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(11, 27);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(78, 14);
            this.label66.TabIndex = 16;
            this.label66.Text = "Display Name";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(103, 24);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 40;
            this.TxtEmpName.Size = new System.Drawing.Size(354, 20);
            this.TxtEmpName.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(4, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Employee Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(103, 3);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 50;
            this.TxtEmpCode.Size = new System.Drawing.Size(354, 20);
            this.TxtEmpCode.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(7, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Employee Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.tabControl1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 50);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(902, 566);
            this.panel4.TabIndex = 11;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgGeneral);
            this.tabControl1.Controls.Add(this.TpgEmployeeFamily);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(902, 566);
            this.tabControl1.TabIndex = 21;
            // 
            // TpgGeneral
            // 
            this.TpgGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgGeneral.Controls.Add(this.ChkActingOfficial);
            this.TpgGeneral.Controls.Add(this.DteContractDt);
            this.TpgGeneral.Controls.Add(this.LblContractDt);
            this.TpgGeneral.Controls.Add(this.label60);
            this.TpgGeneral.Controls.Add(this.LuePositionStatusCode);
            this.TpgGeneral.Controls.Add(this.TxtRegEntCode);
            this.TpgGeneral.Controls.Add(this.label53);
            this.TpgGeneral.Controls.Add(this.DteTGDt);
            this.TpgGeneral.Controls.Add(this.label4);
            this.TpgGeneral.Controls.Add(this.label70);
            this.TpgGeneral.Controls.Add(this.LueEntityCode);
            this.TpgGeneral.Controls.Add(this.TxtPOH);
            this.TpgGeneral.Controls.Add(this.label69);
            this.TpgGeneral.Controls.Add(this.LblDivisionCode);
            this.TpgGeneral.Controls.Add(this.LueDivisionCode);
            this.TpgGeneral.Controls.Add(this.label65);
            this.TpgGeneral.Controls.Add(this.LueWorkGroup);
            this.TpgGeneral.Controls.Add(this.label64);
            this.TpgGeneral.Controls.Add(this.LueSection);
            this.TpgGeneral.Controls.Add(this.label63);
            this.TpgGeneral.Controls.Add(this.label58);
            this.TpgGeneral.Controls.Add(this.MeeDomicile);
            this.TpgGeneral.Controls.Add(this.LblSiteCode);
            this.TpgGeneral.Controls.Add(this.LueSiteCode);
            this.TpgGeneral.Controls.Add(this.TxtRTRW);
            this.TpgGeneral.Controls.Add(this.LueMaritalStatus);
            this.TpgGeneral.Controls.Add(this.label56);
            this.TpgGeneral.Controls.Add(this.TxtVillage);
            this.TpgGeneral.Controls.Add(this.label55);
            this.TpgGeneral.Controls.Add(this.TxtSubDistrict);
            this.TpgGeneral.Controls.Add(this.label54);
            this.TpgGeneral.Controls.Add(this.LblEmploymentStatus);
            this.TpgGeneral.Controls.Add(this.TxtBarcodeCode);
            this.TpgGeneral.Controls.Add(this.label52);
            this.TpgGeneral.Controls.Add(this.TxtShortCode);
            this.TpgGeneral.Controls.Add(this.label50);
            this.TpgGeneral.Controls.Add(this.LueEmploymentStatus);
            this.TpgGeneral.Controls.Add(this.panel5);
            this.TpgGeneral.Controls.Add(this.label9);
            this.TpgGeneral.Controls.Add(this.LueCity);
            this.TpgGeneral.Controls.Add(this.TxtIdNumber);
            this.TpgGeneral.Controls.Add(this.DteResignDt);
            this.TpgGeneral.Controls.Add(this.label10);
            this.TpgGeneral.Controls.Add(this.label11);
            this.TpgGeneral.Controls.Add(this.DteJoinDt);
            this.TpgGeneral.Controls.Add(this.label6);
            this.TpgGeneral.Controls.Add(this.TxtUserCode);
            this.TpgGeneral.Controls.Add(this.TxtPostalCode);
            this.TpgGeneral.Controls.Add(this.label3);
            this.TpgGeneral.Controls.Add(this.label15);
            this.TpgGeneral.Controls.Add(this.MeeAddress);
            this.TpgGeneral.Controls.Add(this.label8);
            this.TpgGeneral.Controls.Add(this.LblPosCode);
            this.TpgGeneral.Controls.Add(this.LuePosCode);
            this.TpgGeneral.Controls.Add(this.LblDeptCode);
            this.TpgGeneral.Controls.Add(this.LueDeptCode);
            this.TpgGeneral.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgGeneral.Location = new System.Drawing.Point(4, 26);
            this.TpgGeneral.Name = "TpgGeneral";
            this.TpgGeneral.Size = new System.Drawing.Size(894, 536);
            this.TpgGeneral.TabIndex = 0;
            this.TpgGeneral.Text = "General";
            this.TpgGeneral.UseVisualStyleBackColor = true;
            // 
            // ChkActingOfficial
            // 
            this.ChkActingOfficial.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActingOfficial.Location = new System.Drawing.Point(387, 189);
            this.ChkActingOfficial.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActingOfficial.Name = "ChkActingOfficial";
            this.ChkActingOfficial.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActingOfficial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActingOfficial.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActingOfficial.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActingOfficial.Properties.Appearance.Options.UseFont = true;
            this.ChkActingOfficial.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActingOfficial.Properties.Caption = "Acting Official (PLT)";
            this.ChkActingOfficial.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActingOfficial.ShowToolTips = false;
            this.ChkActingOfficial.Size = new System.Drawing.Size(140, 22);
            this.ChkActingOfficial.TabIndex = 77;
            // 
            // DteContractDt
            // 
            this.DteContractDt.EditValue = null;
            this.DteContractDt.EnterMoveNextControl = true;
            this.DteContractDt.Location = new System.Drawing.Point(287, 317);
            this.DteContractDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteContractDt.Name = "DteContractDt";
            this.DteContractDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteContractDt.Properties.Appearance.Options.UseFont = true;
            this.DteContractDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteContractDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteContractDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteContractDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteContractDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteContractDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteContractDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteContractDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteContractDt.Properties.MaxLength = 16;
            this.DteContractDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteContractDt.Size = new System.Drawing.Size(95, 20);
            this.DteContractDt.TabIndex = 70;
            // 
            // LblContractDt
            // 
            this.LblContractDt.AutoSize = true;
            this.LblContractDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblContractDt.ForeColor = System.Drawing.Color.Black;
            this.LblContractDt.Location = new System.Drawing.Point(224, 320);
            this.LblContractDt.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblContractDt.Name = "LblContractDt";
            this.LblContractDt.Size = new System.Drawing.Size(54, 14);
            this.LblContractDt.TabIndex = 69;
            this.LblContractDt.Text = "Contract";
            this.LblContractDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(37, 215);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(84, 14);
            this.label60.TabIndex = 40;
            this.label60.Text = "PositionStatus";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePositionStatusCode
            // 
            this.LuePositionStatusCode.EnterMoveNextControl = true;
            this.LuePositionStatusCode.Location = new System.Drawing.Point(125, 212);
            this.LuePositionStatusCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePositionStatusCode.Name = "LuePositionStatusCode";
            this.LuePositionStatusCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.Appearance.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePositionStatusCode.Properties.DropDownRows = 30;
            this.LuePositionStatusCode.Properties.MaxLength = 16;
            this.LuePositionStatusCode.Properties.NullText = "[Empty]";
            this.LuePositionStatusCode.Properties.PopupWidth = 300;
            this.LuePositionStatusCode.Size = new System.Drawing.Size(257, 20);
            this.LuePositionStatusCode.TabIndex = 41;
            this.LuePositionStatusCode.ToolTip = "F4 : Show/hide list";
            this.LuePositionStatusCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtRegEntCode
            // 
            this.TxtRegEntCode.EnterMoveNextControl = true;
            this.TxtRegEntCode.Location = new System.Drawing.Point(125, 107);
            this.TxtRegEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRegEntCode.Name = "TxtRegEntCode";
            this.TxtRegEntCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRegEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRegEntCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRegEntCode.Properties.Appearance.Options.UseFont = true;
            this.TxtRegEntCode.Properties.MaxLength = 80;
            this.TxtRegEntCode.Properties.ReadOnly = true;
            this.TxtRegEntCode.Size = new System.Drawing.Size(257, 20);
            this.TxtRegEntCode.TabIndex = 33;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(54, 110);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(67, 14);
            this.label53.TabIndex = 32;
            this.label53.Text = "Entity (SS)";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTGDt
            // 
            this.DteTGDt.EditValue = null;
            this.DteTGDt.EnterMoveNextControl = true;
            this.DteTGDt.Location = new System.Drawing.Point(125, 317);
            this.DteTGDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTGDt.Name = "DteTGDt";
            this.DteTGDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTGDt.Properties.Appearance.Options.UseFont = true;
            this.DteTGDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTGDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTGDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTGDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTGDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTGDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTGDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTGDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTGDt.Properties.MaxLength = 16;
            this.DteTGDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTGDt.Size = new System.Drawing.Size(98, 20);
            this.DteTGDt.TabIndex = 51;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(8, 320);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 14);
            this.label4.TabIndex = 50;
            this.label4.Text = "Training Graduation";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(82, 89);
            this.label70.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(39, 14);
            this.label70.TabIndex = 30;
            this.label70.Text = "Entity";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntityCode
            // 
            this.LueEntityCode.EnterMoveNextControl = true;
            this.LueEntityCode.Location = new System.Drawing.Point(125, 86);
            this.LueEntityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntityCode.Name = "LueEntityCode";
            this.LueEntityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntityCode.Properties.DropDownRows = 30;
            this.LueEntityCode.Properties.MaxLength = 16;
            this.LueEntityCode.Properties.NullText = "[Empty]";
            this.LueEntityCode.Properties.PopupWidth = 300;
            this.LueEntityCode.Size = new System.Drawing.Size(257, 20);
            this.LueEntityCode.TabIndex = 31;
            this.LueEntityCode.ToolTip = "F4 : Show/hide list";
            this.LueEntityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtPOH
            // 
            this.TxtPOH.EnterMoveNextControl = true;
            this.TxtPOH.Location = new System.Drawing.Point(125, 275);
            this.TxtPOH.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPOH.Name = "TxtPOH";
            this.TxtPOH.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPOH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPOH.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPOH.Properties.Appearance.Options.UseFont = true;
            this.TxtPOH.Properties.MaxLength = 40;
            this.TxtPOH.Size = new System.Drawing.Size(257, 20);
            this.TxtPOH.TabIndex = 45;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(46, 278);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(75, 14);
            this.label69.TabIndex = 44;
            this.label69.Text = "Point of Hire";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblDivisionCode
            // 
            this.LblDivisionCode.AutoSize = true;
            this.LblDivisionCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDivisionCode.ForeColor = System.Drawing.Color.Black;
            this.LblDivisionCode.Location = new System.Drawing.Point(75, 152);
            this.LblDivisionCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDivisionCode.Name = "LblDivisionCode";
            this.LblDivisionCode.Size = new System.Drawing.Size(46, 14);
            this.LblDivisionCode.TabIndex = 36;
            this.LblDivisionCode.Text = "Division";
            this.LblDivisionCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDivisionCode
            // 
            this.LueDivisionCode.EnterMoveNextControl = true;
            this.LueDivisionCode.Location = new System.Drawing.Point(125, 149);
            this.LueDivisionCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDivisionCode.Name = "LueDivisionCode";
            this.LueDivisionCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.Appearance.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDivisionCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDivisionCode.Properties.DropDownRows = 30;
            this.LueDivisionCode.Properties.MaxLength = 16;
            this.LueDivisionCode.Properties.NullText = "[Empty]";
            this.LueDivisionCode.Properties.PopupWidth = 300;
            this.LueDivisionCode.Size = new System.Drawing.Size(257, 20);
            this.LueDivisionCode.TabIndex = 36;
            this.LueDivisionCode.ToolTip = "F4 : Show/hide list";
            this.LueDivisionCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(32, 257);
            this.label65.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(89, 14);
            this.label65.TabIndex = 42;
            this.label65.Text = "Working Group";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWorkGroup
            // 
            this.LueWorkGroup.EnterMoveNextControl = true;
            this.LueWorkGroup.Location = new System.Drawing.Point(125, 254);
            this.LueWorkGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueWorkGroup.Name = "LueWorkGroup";
            this.LueWorkGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.Appearance.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWorkGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWorkGroup.Properties.DropDownRows = 30;
            this.LueWorkGroup.Properties.MaxLength = 16;
            this.LueWorkGroup.Properties.NullText = "[Empty]";
            this.LueWorkGroup.Properties.PopupWidth = 300;
            this.LueWorkGroup.Size = new System.Drawing.Size(257, 20);
            this.LueWorkGroup.TabIndex = 43;
            this.LueWorkGroup.ToolTip = "F4 : Show/hide list";
            this.LueWorkGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(73, 236);
            this.label64.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(48, 14);
            this.label64.TabIndex = 41;
            this.label64.Text = "Section";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSection
            // 
            this.LueSection.EnterMoveNextControl = true;
            this.LueSection.Location = new System.Drawing.Point(125, 233);
            this.LueSection.Margin = new System.Windows.Forms.Padding(5);
            this.LueSection.Name = "LueSection";
            this.LueSection.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.Appearance.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSection.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSection.Properties.DropDownRows = 30;
            this.LueSection.Properties.MaxLength = 16;
            this.LueSection.Properties.NullText = "[Empty]";
            this.LueSection.Properties.PopupWidth = 300;
            this.LueSection.Size = new System.Drawing.Size(257, 20);
            this.LueSection.TabIndex = 42;
            this.LueSection.ToolTip = "F4 : Show/hide list";
            this.LueSection.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(70, 467);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(51, 14);
            this.label63.TabIndex = 64;
            this.label63.Text = "Domicile";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(41, 509);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(80, 14);
            this.label58.TabIndex = 69;
            this.label58.Text = "Marital Status";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeDomicile
            // 
            this.MeeDomicile.EnterMoveNextControl = true;
            this.MeeDomicile.Location = new System.Drawing.Point(125, 464);
            this.MeeDomicile.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDomicile.Name = "MeeDomicile";
            this.MeeDomicile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.Appearance.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDomicile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDomicile.Properties.MaxLength = 400;
            this.MeeDomicile.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeDomicile.Properties.ShowIcon = false;
            this.MeeDomicile.Size = new System.Drawing.Size(257, 20);
            this.MeeDomicile.TabIndex = 65;
            this.MeeDomicile.ToolTip = "F4 : Show/hide text";
            this.MeeDomicile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDomicile.ToolTipTitle = "Run System";
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(92, 131);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 34;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(125, 128);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(257, 20);
            this.LueSiteCode.TabIndex = 35;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtRTRW
            // 
            this.TxtRTRW.EnterMoveNextControl = true;
            this.TxtRTRW.Location = new System.Drawing.Point(125, 422);
            this.TxtRTRW.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRTRW.Name = "TxtRTRW";
            this.TxtRTRW.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRTRW.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRTRW.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRTRW.Properties.Appearance.Options.UseFont = true;
            this.TxtRTRW.Properties.MaxLength = 10;
            this.TxtRTRW.Size = new System.Drawing.Size(257, 20);
            this.TxtRTRW.TabIndex = 61;
            // 
            // LueMaritalStatus
            // 
            this.LueMaritalStatus.EnterMoveNextControl = true;
            this.LueMaritalStatus.Location = new System.Drawing.Point(125, 506);
            this.LueMaritalStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueMaritalStatus.Name = "LueMaritalStatus";
            this.LueMaritalStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.Appearance.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMaritalStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMaritalStatus.Properties.DropDownRows = 30;
            this.LueMaritalStatus.Properties.MaxLength = 1;
            this.LueMaritalStatus.Properties.NullText = "[Empty]";
            this.LueMaritalStatus.Properties.PopupWidth = 300;
            this.LueMaritalStatus.Size = new System.Drawing.Size(257, 20);
            this.LueMaritalStatus.TabIndex = 70;
            this.LueMaritalStatus.ToolTip = "F4 : Show/hide list";
            this.LueMaritalStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(75, 425);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(46, 14);
            this.label56.TabIndex = 60;
            this.label56.Text = "RT/RW";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVillage
            // 
            this.TxtVillage.EnterMoveNextControl = true;
            this.TxtVillage.Location = new System.Drawing.Point(125, 401);
            this.TxtVillage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVillage.Name = "TxtVillage";
            this.TxtVillage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVillage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVillage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVillage.Properties.Appearance.Options.UseFont = true;
            this.TxtVillage.Properties.MaxLength = 40;
            this.TxtVillage.Size = new System.Drawing.Size(257, 20);
            this.TxtVillage.TabIndex = 59;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(80, 404);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(41, 14);
            this.label55.TabIndex = 58;
            this.label55.Text = "Village";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSubDistrict
            // 
            this.TxtSubDistrict.EnterMoveNextControl = true;
            this.TxtSubDistrict.Location = new System.Drawing.Point(125, 380);
            this.TxtSubDistrict.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubDistrict.Name = "TxtSubDistrict";
            this.TxtSubDistrict.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSubDistrict.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubDistrict.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubDistrict.Properties.Appearance.Options.UseFont = true;
            this.TxtSubDistrict.Properties.MaxLength = 40;
            this.TxtSubDistrict.Size = new System.Drawing.Size(257, 20);
            this.TxtSubDistrict.TabIndex = 57;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(52, 383);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(69, 14);
            this.label54.TabIndex = 56;
            this.label54.Text = "Sub District";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblEmploymentStatus
            // 
            this.LblEmploymentStatus.AutoSize = true;
            this.LblEmploymentStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEmploymentStatus.ForeColor = System.Drawing.Color.Black;
            this.LblEmploymentStatus.Location = new System.Drawing.Point(7, 68);
            this.LblEmploymentStatus.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEmploymentStatus.Name = "LblEmploymentStatus";
            this.LblEmploymentStatus.Size = new System.Drawing.Size(114, 14);
            this.LblEmploymentStatus.TabIndex = 28;
            this.LblEmploymentStatus.Text = "Employment Status";
            this.LblEmploymentStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBarcodeCode
            // 
            this.TxtBarcodeCode.EnterMoveNextControl = true;
            this.TxtBarcodeCode.Location = new System.Drawing.Point(125, 44);
            this.TxtBarcodeCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBarcodeCode.Name = "TxtBarcodeCode";
            this.TxtBarcodeCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBarcodeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBarcodeCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBarcodeCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBarcodeCode.Properties.MaxLength = 16;
            this.TxtBarcodeCode.Size = new System.Drawing.Size(257, 20);
            this.TxtBarcodeCode.TabIndex = 27;
            this.TxtBarcodeCode.Visible = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(38, 47);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(83, 14);
            this.label52.TabIndex = 26;
            this.label52.Text = "Barcode Code";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtShortCode
            // 
            this.TxtShortCode.EnterMoveNextControl = true;
            this.TxtShortCode.Location = new System.Drawing.Point(125, 23);
            this.TxtShortCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortCode.Name = "TxtShortCode";
            this.TxtShortCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortCode.Properties.Appearance.Options.UseFont = true;
            this.TxtShortCode.Properties.MaxLength = 8;
            this.TxtShortCode.Size = new System.Drawing.Size(257, 20);
            this.TxtShortCode.TabIndex = 25;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(52, 26);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(69, 14);
            this.label50.TabIndex = 24;
            this.label50.Text = "Short Code";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEmploymentStatus
            // 
            this.LueEmploymentStatus.EnterMoveNextControl = true;
            this.LueEmploymentStatus.Location = new System.Drawing.Point(125, 65);
            this.LueEmploymentStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmploymentStatus.Name = "LueEmploymentStatus";
            this.LueEmploymentStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.Appearance.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmploymentStatus.Properties.DropDownRows = 30;
            this.LueEmploymentStatus.Properties.MaxLength = 1;
            this.LueEmploymentStatus.Properties.NullText = "[Empty]";
            this.LueEmploymentStatus.Properties.PopupWidth = 300;
            this.LueEmploymentStatus.Size = new System.Drawing.Size(257, 20);
            this.LueEmploymentStatus.TabIndex = 29;
            this.LueEmploymentStatus.ToolTip = "F4 : Show/hide list";
            this.LueEmploymentStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label71);
            this.panel5.Controls.Add(this.LueCostGroup);
            this.panel5.Controls.Add(this.label67);
            this.panel5.Controls.Add(this.LueEducationType);
            this.panel5.Controls.Add(this.label68);
            this.panel5.Controls.Add(this.LueShoeSize);
            this.panel5.Controls.Add(this.label61);
            this.panel5.Controls.Add(this.LueClothesSize);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.TxtLevelCode);
            this.panel5.Controls.Add(this.label47);
            this.panel5.Controls.Add(this.DteLeaveStartDt);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.LblPGCode);
            this.panel5.Controls.Add(this.LuePGCode);
            this.panel5.Controls.Add(this.label62);
            this.panel5.Controls.Add(this.LueBloodType);
            this.panel5.Controls.Add(this.LblSystemType);
            this.panel5.Controls.Add(this.LueSystemType);
            this.panel5.Controls.Add(this.TxtMother);
            this.panel5.Controls.Add(this.label57);
            this.panel5.Controls.Add(this.LblPayrunPeriod);
            this.panel5.Controls.Add(this.LuePayrunPeriod);
            this.panel5.Controls.Add(this.DteWeddingDt);
            this.panel5.Controls.Add(this.label59);
            this.panel5.Controls.Add(this.TxtBankAcName);
            this.panel5.Controls.Add(this.label49);
            this.panel5.Controls.Add(this.LblGrdLvlCode);
            this.panel5.Controls.Add(this.TxtEmail);
            this.panel5.Controls.Add(this.LueGrdLvlCode);
            this.panel5.Controls.Add(this.DteBirthDt);
            this.panel5.Controls.Add(this.TxtBankBranch);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.TxtBirthPlace);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.TxtBankAcNo);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.LueReligion);
            this.panel5.Controls.Add(this.TxtMobile);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.LueBankCode);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.LuePTKP);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.LuePayrollType);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.TxtNPWP);
            this.panel5.Controls.Add(this.LueGender);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.TxtPhone);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(520, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(370, 532);
            this.panel5.TabIndex = 68;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(44, 5);
            this.label71.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(68, 14);
            this.label71.TabIndex = 71;
            this.label71.Text = "Cost Group";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCostGroup
            // 
            this.LueCostGroup.EnterMoveNextControl = true;
            this.LueCostGroup.Location = new System.Drawing.Point(115, 2);
            this.LueCostGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueCostGroup.Name = "LueCostGroup";
            this.LueCostGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostGroup.Properties.Appearance.Options.UseFont = true;
            this.LueCostGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCostGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCostGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCostGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCostGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCostGroup.Properties.DropDownRows = 25;
            this.LueCostGroup.Properties.MaxLength = 2;
            this.LueCostGroup.Properties.NullText = "[Empty]";
            this.LueCostGroup.Properties.PopupWidth = 500;
            this.LueCostGroup.Size = new System.Drawing.Size(245, 20);
            this.LueCostGroup.TabIndex = 72;
            this.LueCostGroup.ToolTip = "F4 : Show/hide list";
            this.LueCostGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(20, 509);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(93, 14);
            this.label67.TabIndex = 124;
            this.label67.Text = "Education Type";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEducationType
            // 
            this.LueEducationType.EnterMoveNextControl = true;
            this.LueEducationType.Location = new System.Drawing.Point(117, 506);
            this.LueEducationType.Margin = new System.Windows.Forms.Padding(5);
            this.LueEducationType.Name = "LueEducationType";
            this.LueEducationType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationType.Properties.Appearance.Options.UseFont = true;
            this.LueEducationType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEducationType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEducationType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEducationType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEducationType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEducationType.Properties.DropDownRows = 30;
            this.LueEducationType.Properties.MaxLength = 1;
            this.LueEducationType.Properties.NullText = "[Empty]";
            this.LueEducationType.Properties.PopupWidth = 300;
            this.LueEducationType.Size = new System.Drawing.Size(244, 20);
            this.LueEducationType.TabIndex = 125;
            this.LueEducationType.ToolTip = "F4 : Show/hide list";
            this.LueEducationType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(220, 488);
            this.label68.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(60, 14);
            this.label68.TabIndex = 122;
            this.label68.Text = "Shoe Size";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueShoeSize
            // 
            this.LueShoeSize.EnterMoveNextControl = true;
            this.LueShoeSize.Location = new System.Drawing.Point(280, 485);
            this.LueShoeSize.Margin = new System.Windows.Forms.Padding(5);
            this.LueShoeSize.Name = "LueShoeSize";
            this.LueShoeSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShoeSize.Properties.Appearance.Options.UseFont = true;
            this.LueShoeSize.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShoeSize.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShoeSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShoeSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShoeSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShoeSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShoeSize.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShoeSize.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShoeSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShoeSize.Properties.DropDownRows = 30;
            this.LueShoeSize.Properties.MaxLength = 1;
            this.LueShoeSize.Properties.NullText = "[Empty]";
            this.LueShoeSize.Properties.PopupWidth = 300;
            this.LueShoeSize.Size = new System.Drawing.Size(80, 20);
            this.LueShoeSize.TabIndex = 123;
            this.LueShoeSize.ToolTip = "F4 : Show/hide list";
            this.LueShoeSize.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(41, 488);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(72, 14);
            this.label61.TabIndex = 120;
            this.label61.Text = "Clothes Size";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueClothesSize
            // 
            this.LueClothesSize.EnterMoveNextControl = true;
            this.LueClothesSize.Location = new System.Drawing.Point(116, 485);
            this.LueClothesSize.Margin = new System.Windows.Forms.Padding(5);
            this.LueClothesSize.Name = "LueClothesSize";
            this.LueClothesSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClothesSize.Properties.Appearance.Options.UseFont = true;
            this.LueClothesSize.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClothesSize.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueClothesSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClothesSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueClothesSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClothesSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueClothesSize.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClothesSize.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueClothesSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueClothesSize.Properties.DropDownRows = 30;
            this.LueClothesSize.Properties.MaxLength = 1;
            this.LueClothesSize.Properties.NullText = "[Empty]";
            this.LueClothesSize.Properties.PopupWidth = 300;
            this.LueClothesSize.Size = new System.Drawing.Size(104, 20);
            this.LueClothesSize.TabIndex = 121;
            this.LueClothesSize.ToolTip = "F4 : Show/hide list";
            this.LueClothesSize.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(78, 194);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(35, 14);
            this.label51.TabIndex = 91;
            this.label51.Text = "Level";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(51, 89);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 14);
            this.label14.TabIndex = 81;
            this.label14.Text = "Birth Date";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevelCode
            // 
            this.TxtLevelCode.EnterMoveNextControl = true;
            this.TxtLevelCode.Location = new System.Drawing.Point(116, 191);
            this.TxtLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevelCode.Name = "TxtLevelCode";
            this.TxtLevelCode.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevelCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevelCode.Properties.Appearance.Options.UseFont = true;
            this.TxtLevelCode.Properties.MaxLength = 80;
            this.TxtLevelCode.Properties.ReadOnly = true;
            this.TxtLevelCode.Size = new System.Drawing.Size(245, 20);
            this.TxtLevelCode.TabIndex = 92;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label47.Location = new System.Drawing.Point(223, 466);
            this.label47.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(137, 14);
            this.label47.TabIndex = 119;
            this.label47.Text = "(Annual / Long Service)";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteLeaveStartDt
            // 
            this.DteLeaveStartDt.EditValue = null;
            this.DteLeaveStartDt.EnterMoveNextControl = true;
            this.DteLeaveStartDt.Location = new System.Drawing.Point(116, 464);
            this.DteLeaveStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLeaveStartDt.Name = "DteLeaveStartDt";
            this.DteLeaveStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteLeaveStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLeaveStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLeaveStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLeaveStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLeaveStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLeaveStartDt.Properties.MaxLength = 16;
            this.DteLeaveStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLeaveStartDt.Size = new System.Drawing.Size(105, 20);
            this.DteLeaveStartDt.TabIndex = 118;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(16, 467);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 14);
            this.label5.TabIndex = 117;
            this.label5.Text = "Permanent Date";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPGCode
            // 
            this.LblPGCode.AutoSize = true;
            this.LblPGCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPGCode.ForeColor = System.Drawing.Color.Black;
            this.LblPGCode.Location = new System.Drawing.Point(35, 216);
            this.LblPGCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPGCode.Name = "LblPGCode";
            this.LblPGCode.Size = new System.Drawing.Size(78, 14);
            this.LblPGCode.TabIndex = 93;
            this.LblPGCode.Text = "Payroll Group";
            this.LblPGCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePGCode
            // 
            this.LuePGCode.EnterMoveNextControl = true;
            this.LuePGCode.Location = new System.Drawing.Point(116, 212);
            this.LuePGCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePGCode.Name = "LuePGCode";
            this.LuePGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.Appearance.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePGCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePGCode.Properties.DropDownRows = 30;
            this.LuePGCode.Properties.NullText = "[Empty]";
            this.LuePGCode.Properties.PopupWidth = 300;
            this.LuePGCode.Size = new System.Drawing.Size(245, 20);
            this.LuePGCode.TabIndex = 94;
            this.LuePGCode.ToolTip = "F4 : Show/hide list";
            this.LuePGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(44, 111);
            this.label62.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(69, 14);
            this.label62.TabIndex = 83;
            this.label62.Text = "Blood Type";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBloodType
            // 
            this.LueBloodType.EnterMoveNextControl = true;
            this.LueBloodType.Location = new System.Drawing.Point(115, 107);
            this.LueBloodType.Margin = new System.Windows.Forms.Padding(5);
            this.LueBloodType.Name = "LueBloodType";
            this.LueBloodType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.Appearance.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBloodType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBloodType.Properties.DropDownRows = 25;
            this.LueBloodType.Properties.MaxLength = 2;
            this.LueBloodType.Properties.NullText = "[Empty]";
            this.LueBloodType.Properties.PopupWidth = 500;
            this.LueBloodType.Size = new System.Drawing.Size(245, 20);
            this.LueBloodType.TabIndex = 84;
            this.LueBloodType.ToolTip = "F4 : Show/hide list";
            this.LueBloodType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LblSystemType
            // 
            this.LblSystemType.AutoSize = true;
            this.LblSystemType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSystemType.ForeColor = System.Drawing.Color.Black;
            this.LblSystemType.Location = new System.Drawing.Point(34, 153);
            this.LblSystemType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSystemType.Name = "LblSystemType";
            this.LblSystemType.Size = new System.Drawing.Size(79, 14);
            this.LblSystemType.TabIndex = 87;
            this.LblSystemType.Text = "System Type";
            this.LblSystemType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSystemType
            // 
            this.LueSystemType.EnterMoveNextControl = true;
            this.LueSystemType.Location = new System.Drawing.Point(115, 149);
            this.LueSystemType.Margin = new System.Windows.Forms.Padding(5);
            this.LueSystemType.Name = "LueSystemType";
            this.LueSystemType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.Appearance.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSystemType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSystemType.Properties.DropDownRows = 30;
            this.LueSystemType.Properties.NullText = "[Empty]";
            this.LueSystemType.Properties.PopupWidth = 300;
            this.LueSystemType.Size = new System.Drawing.Size(245, 20);
            this.LueSystemType.TabIndex = 88;
            this.LueSystemType.ToolTip = "F4 : Show/hide list";
            this.LueSystemType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtMother
            // 
            this.TxtMother.EnterMoveNextControl = true;
            this.TxtMother.Location = new System.Drawing.Point(115, 128);
            this.TxtMother.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMother.Name = "TxtMother";
            this.TxtMother.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMother.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMother.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMother.Properties.Appearance.Options.UseFont = true;
            this.TxtMother.Properties.MaxLength = 40;
            this.TxtMother.Size = new System.Drawing.Size(245, 20);
            this.TxtMother.TabIndex = 86;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(15, 132);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(98, 14);
            this.label57.TabIndex = 85;
            this.label57.Text = "Biological Mother";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPayrunPeriod
            // 
            this.LblPayrunPeriod.AutoSize = true;
            this.LblPayrunPeriod.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPayrunPeriod.ForeColor = System.Drawing.Color.Black;
            this.LblPayrunPeriod.Location = new System.Drawing.Point(31, 258);
            this.LblPayrunPeriod.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPayrunPeriod.Name = "LblPayrunPeriod";
            this.LblPayrunPeriod.Size = new System.Drawing.Size(82, 14);
            this.LblPayrunPeriod.TabIndex = 97;
            this.LblPayrunPeriod.Text = "Payrun Period";
            this.LblPayrunPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePayrunPeriod
            // 
            this.LuePayrunPeriod.EnterMoveNextControl = true;
            this.LuePayrunPeriod.Location = new System.Drawing.Point(116, 254);
            this.LuePayrunPeriod.Margin = new System.Windows.Forms.Padding(5);
            this.LuePayrunPeriod.Name = "LuePayrunPeriod";
            this.LuePayrunPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.Appearance.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePayrunPeriod.Properties.DropDownRows = 30;
            this.LuePayrunPeriod.Properties.MaxLength = 1;
            this.LuePayrunPeriod.Properties.NullText = "[Empty]";
            this.LuePayrunPeriod.Properties.PopupWidth = 300;
            this.LuePayrunPeriod.Size = new System.Drawing.Size(245, 20);
            this.LuePayrunPeriod.TabIndex = 98;
            this.LuePayrunPeriod.ToolTip = "F4 : Show/hide list";
            this.LuePayrunPeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // DteWeddingDt
            // 
            this.DteWeddingDt.EditValue = null;
            this.DteWeddingDt.EnterMoveNextControl = true;
            this.DteWeddingDt.Location = new System.Drawing.Point(115, 23);
            this.DteWeddingDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteWeddingDt.Name = "DteWeddingDt";
            this.DteWeddingDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteWeddingDt.Properties.Appearance.Options.UseFont = true;
            this.DteWeddingDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteWeddingDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteWeddingDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteWeddingDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteWeddingDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteWeddingDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteWeddingDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteWeddingDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteWeddingDt.Properties.MaxLength = 16;
            this.DteWeddingDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteWeddingDt.Size = new System.Drawing.Size(245, 20);
            this.DteWeddingDt.TabIndex = 74;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(27, 26);
            this.label59.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(86, 14);
            this.label59.TabIndex = 73;
            this.label59.Text = "Wedding Date";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcName
            // 
            this.TxtBankAcName.EnterMoveNextControl = true;
            this.TxtBankAcName.Location = new System.Drawing.Point(116, 359);
            this.TxtBankAcName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcName.Name = "TxtBankAcName";
            this.TxtBankAcName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcName.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcName.Properties.MaxLength = 80;
            this.TxtBankAcName.Size = new System.Drawing.Size(245, 20);
            this.TxtBankAcName.TabIndex = 108;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(25, 363);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(88, 14);
            this.label49.TabIndex = 107;
            this.label49.Text = "Account Name";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblGrdLvlCode
            // 
            this.LblGrdLvlCode.AutoSize = true;
            this.LblGrdLvlCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblGrdLvlCode.ForeColor = System.Drawing.Color.Black;
            this.LblGrdLvlCode.Location = new System.Drawing.Point(74, 174);
            this.LblGrdLvlCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblGrdLvlCode.Name = "LblGrdLvlCode";
            this.LblGrdLvlCode.Size = new System.Drawing.Size(39, 14);
            this.LblGrdLvlCode.TabIndex = 89;
            this.LblGrdLvlCode.Text = "Grade";
            this.LblGrdLvlCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(116, 443);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 80;
            this.TxtEmail.Size = new System.Drawing.Size(245, 20);
            this.TxtEmail.TabIndex = 116;
            // 
            // LueGrdLvlCode
            // 
            this.LueGrdLvlCode.EnterMoveNextControl = true;
            this.LueGrdLvlCode.Location = new System.Drawing.Point(116, 170);
            this.LueGrdLvlCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueGrdLvlCode.Name = "LueGrdLvlCode";
            this.LueGrdLvlCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.Appearance.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGrdLvlCode.Properties.DropDownRows = 30;
            this.LueGrdLvlCode.Properties.NullText = "[Empty]";
            this.LueGrdLvlCode.Properties.PopupWidth = 300;
            this.LueGrdLvlCode.Size = new System.Drawing.Size(245, 20);
            this.LueGrdLvlCode.TabIndex = 90;
            this.LueGrdLvlCode.ToolTip = "F4 : Show/hide list";
            this.LueGrdLvlCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // DteBirthDt
            // 
            this.DteBirthDt.EditValue = null;
            this.DteBirthDt.EnterMoveNextControl = true;
            this.DteBirthDt.Location = new System.Drawing.Point(115, 86);
            this.DteBirthDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBirthDt.Name = "DteBirthDt";
            this.DteBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBirthDt.Properties.MaxLength = 8;
            this.DteBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBirthDt.Size = new System.Drawing.Size(245, 20);
            this.DteBirthDt.TabIndex = 82;
            // 
            // TxtBankBranch
            // 
            this.TxtBankBranch.EnterMoveNextControl = true;
            this.TxtBankBranch.Location = new System.Drawing.Point(116, 338);
            this.TxtBankBranch.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankBranch.Name = "TxtBankBranch";
            this.TxtBankBranch.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankBranch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankBranch.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankBranch.Properties.Appearance.Options.UseFont = true;
            this.TxtBankBranch.Properties.MaxLength = 80;
            this.TxtBankBranch.Size = new System.Drawing.Size(245, 20);
            this.TxtBankBranch.TabIndex = 106;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(79, 446);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 14);
            this.label18.TabIndex = 115;
            this.label18.Text = "Email";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(34, 342);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 14);
            this.label23.TabIndex = 105;
            this.label23.Text = "Branch Name";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBirthPlace
            // 
            this.TxtBirthPlace.EnterMoveNextControl = true;
            this.TxtBirthPlace.Location = new System.Drawing.Point(115, 65);
            this.TxtBirthPlace.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBirthPlace.Name = "TxtBirthPlace";
            this.TxtBirthPlace.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBirthPlace.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBirthPlace.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBirthPlace.Properties.Appearance.Options.UseFont = true;
            this.TxtBirthPlace.Properties.MaxLength = 80;
            this.TxtBirthPlace.Size = new System.Drawing.Size(245, 20);
            this.TxtBirthPlace.TabIndex = 80;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(49, 68);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 14);
            this.label13.TabIndex = 79;
            this.label13.Text = "Birth Place";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcNo
            // 
            this.TxtBankAcNo.EnterMoveNextControl = true;
            this.TxtBankAcNo.Location = new System.Drawing.Point(116, 380);
            this.TxtBankAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcNo.Name = "TxtBankAcNo";
            this.TxtBankAcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcNo.Properties.MaxLength = 80;
            this.TxtBankAcNo.Size = new System.Drawing.Size(245, 20);
            this.TxtBankAcNo.TabIndex = 110;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(213, 47);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 14);
            this.label12.TabIndex = 77;
            this.label12.Text = "Religion";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReligion
            // 
            this.LueReligion.EnterMoveNextControl = true;
            this.LueReligion.Location = new System.Drawing.Point(262, 44);
            this.LueReligion.Margin = new System.Windows.Forms.Padding(5);
            this.LueReligion.Name = "LueReligion";
            this.LueReligion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.Appearance.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReligion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReligion.Properties.DropDownRows = 30;
            this.LueReligion.Properties.MaxLength = 2;
            this.LueReligion.Properties.NullText = "[Empty]";
            this.LueReligion.Properties.PopupWidth = 300;
            this.LueReligion.Size = new System.Drawing.Size(98, 20);
            this.LueReligion.TabIndex = 78;
            this.LueReligion.ToolTip = "F4 : Show/hide list";
            this.LueReligion.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(116, 422);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 80;
            this.TxtMobile.Size = new System.Drawing.Size(245, 20);
            this.TxtMobile.TabIndex = 114;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(51, 383);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(62, 14);
            this.label24.TabIndex = 109;
            this.label24.Text = "Account#";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(45, 320);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 14);
            this.label22.TabIndex = 103;
            this.label22.Text = "Bank Name";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(116, 317);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 30;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(245, 20);
            this.LueBankCode.TabIndex = 104;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(6, 300);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(107, 14);
            this.label21.TabIndex = 101;
            this.label21.Text = "Non-Incoming Tax";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePTKP
            // 
            this.LuePTKP.EnterMoveNextControl = true;
            this.LuePTKP.Location = new System.Drawing.Point(116, 296);
            this.LuePTKP.Margin = new System.Windows.Forms.Padding(5);
            this.LuePTKP.Name = "LuePTKP";
            this.LuePTKP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.Appearance.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePTKP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePTKP.Properties.DropDownRows = 30;
            this.LuePTKP.Properties.NullText = "[Empty]";
            this.LuePTKP.Properties.PopupWidth = 300;
            this.LuePTKP.Size = new System.Drawing.Size(245, 20);
            this.LuePTKP.TabIndex = 102;
            this.LuePTKP.ToolTip = "F4 : Show/hide list";
            this.LuePTKP.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(72, 425);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 14);
            this.label17.TabIndex = 113;
            this.label17.Text = "Mobile";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(40, 237);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 14);
            this.label19.TabIndex = 95;
            this.label19.Text = "Payroll Type";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePayrollType
            // 
            this.LuePayrollType.EnterMoveNextControl = true;
            this.LuePayrollType.Location = new System.Drawing.Point(116, 233);
            this.LuePayrollType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePayrollType.Name = "LuePayrollType";
            this.LuePayrollType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.Appearance.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePayrollType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePayrollType.Properties.DropDownRows = 30;
            this.LuePayrollType.Properties.NullText = "[Empty]";
            this.LuePayrollType.Properties.PopupWidth = 300;
            this.LuePayrollType.Size = new System.Drawing.Size(245, 20);
            this.LuePayrollType.TabIndex = 96;
            this.LuePayrollType.ToolTip = "F4 : Show/hide list";
            this.LuePayrollType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(72, 279);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 14);
            this.label20.TabIndex = 99;
            this.label20.Text = "NPWP";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(66, 47);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 14);
            this.label7.TabIndex = 75;
            this.label7.Text = "Gender";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNPWP
            // 
            this.TxtNPWP.EnterMoveNextControl = true;
            this.TxtNPWP.Location = new System.Drawing.Point(116, 275);
            this.TxtNPWP.Name = "TxtNPWP";
            this.TxtNPWP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNPWP.Properties.Appearance.Options.UseFont = true;
            this.TxtNPWP.Properties.MaxLength = 40;
            this.TxtNPWP.Size = new System.Drawing.Size(245, 20);
            this.TxtNPWP.TabIndex = 100;
            // 
            // LueGender
            // 
            this.LueGender.EnterMoveNextControl = true;
            this.LueGender.Location = new System.Drawing.Point(115, 44);
            this.LueGender.Margin = new System.Windows.Forms.Padding(5);
            this.LueGender.Name = "LueGender";
            this.LueGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.Appearance.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGender.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGender.Properties.DropDownRows = 30;
            this.LueGender.Properties.MaxLength = 1;
            this.LueGender.Properties.NullText = "[Empty]";
            this.LueGender.Properties.PopupWidth = 300;
            this.LueGender.Size = new System.Drawing.Size(98, 20);
            this.LueGender.TabIndex = 76;
            this.LueGender.ToolTip = "F4 : Show/hide list";
            this.LueGender.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(71, 404);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 14);
            this.label16.TabIndex = 111;
            this.label16.Text = "Phone";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(116, 401);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 80;
            this.TxtPhone.Size = new System.Drawing.Size(245, 20);
            this.TxtPhone.TabIndex = 112;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(93, 362);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 14);
            this.label9.TabIndex = 54;
            this.label9.Text = "City";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCity
            // 
            this.LueCity.EnterMoveNextControl = true;
            this.LueCity.Location = new System.Drawing.Point(125, 359);
            this.LueCity.Margin = new System.Windows.Forms.Padding(5);
            this.LueCity.Name = "LueCity";
            this.LueCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.Appearance.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCity.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCity.Properties.DropDownRows = 30;
            this.LueCity.Properties.MaxLength = 16;
            this.LueCity.Properties.NullText = "[Empty]";
            this.LueCity.Properties.PopupWidth = 300;
            this.LueCity.Size = new System.Drawing.Size(257, 20);
            this.LueCity.TabIndex = 55;
            this.LueCity.ToolTip = "F4 : Show/hide list";
            this.LueCity.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtIdNumber
            // 
            this.TxtIdNumber.EnterMoveNextControl = true;
            this.TxtIdNumber.Location = new System.Drawing.Point(125, 485);
            this.TxtIdNumber.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdNumber.Name = "TxtIdNumber";
            this.TxtIdNumber.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIdNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdNumber.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdNumber.Properties.Appearance.Options.UseFont = true;
            this.TxtIdNumber.Properties.MaxLength = 40;
            this.TxtIdNumber.Size = new System.Drawing.Size(257, 20);
            this.TxtIdNumber.TabIndex = 67;
            // 
            // DteResignDt
            // 
            this.DteResignDt.EditValue = null;
            this.DteResignDt.EnterMoveNextControl = true;
            this.DteResignDt.Location = new System.Drawing.Point(287, 296);
            this.DteResignDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteResignDt.Name = "DteResignDt";
            this.DteResignDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDt.Properties.Appearance.Options.UseFont = true;
            this.DteResignDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteResignDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteResignDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteResignDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteResignDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteResignDt.Properties.MaxLength = 16;
            this.DteResignDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteResignDt.Size = new System.Drawing.Size(95, 20);
            this.DteResignDt.TabIndex = 49;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(236, 299);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 14);
            this.label10.TabIndex = 48;
            this.label10.Text = "Resign";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(62, 488);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 14);
            this.label11.TabIndex = 66;
            this.label11.Text = "Identity#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteJoinDt
            // 
            this.DteJoinDt.EditValue = null;
            this.DteJoinDt.EnterMoveNextControl = true;
            this.DteJoinDt.Location = new System.Drawing.Point(125, 296);
            this.DteJoinDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteJoinDt.Name = "DteJoinDt";
            this.DteJoinDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.Appearance.Options.UseFont = true;
            this.DteJoinDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteJoinDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteJoinDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteJoinDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteJoinDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteJoinDt.Properties.MaxLength = 16;
            this.DteJoinDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteJoinDt.Size = new System.Drawing.Size(98, 20);
            this.DteJoinDt.TabIndex = 47;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(63, 299);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 14);
            this.label6.TabIndex = 46;
            this.label6.Text = "Join Date";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUserCode
            // 
            this.TxtUserCode.EnterMoveNextControl = true;
            this.TxtUserCode.Location = new System.Drawing.Point(125, 2);
            this.TxtUserCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUserCode.Name = "TxtUserCode";
            this.TxtUserCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUserCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUserCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUserCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUserCode.Properties.MaxLength = 50;
            this.TxtUserCode.Size = new System.Drawing.Size(257, 20);
            this.TxtUserCode.TabIndex = 23;
            // 
            // TxtPostalCode
            // 
            this.TxtPostalCode.EnterMoveNextControl = true;
            this.TxtPostalCode.Location = new System.Drawing.Point(125, 443);
            this.TxtPostalCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCode.Name = "TxtPostalCode";
            this.TxtPostalCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPostalCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCode.Properties.MaxLength = 16;
            this.TxtPostalCode.Size = new System.Drawing.Size(257, 20);
            this.TxtPostalCode.TabIndex = 63;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(58, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "User Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(50, 446);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 14);
            this.label15.TabIndex = 62;
            this.label15.Text = "Postal Code";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(125, 338);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 80;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(257, 20);
            this.MeeAddress.TabIndex = 53;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(71, 341);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 14);
            this.label8.TabIndex = 52;
            this.label8.Text = "Address";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPosCode
            // 
            this.LblPosCode.AutoSize = true;
            this.LblPosCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPosCode.ForeColor = System.Drawing.Color.Black;
            this.LblPosCode.Location = new System.Drawing.Point(72, 194);
            this.LblPosCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPosCode.Name = "LblPosCode";
            this.LblPosCode.Size = new System.Drawing.Size(49, 14);
            this.LblPosCode.TabIndex = 38;
            this.LblPosCode.Text = "Position";
            this.LblPosCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePosCode
            // 
            this.LuePosCode.EnterMoveNextControl = true;
            this.LuePosCode.Location = new System.Drawing.Point(125, 191);
            this.LuePosCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosCode.Name = "LuePosCode";
            this.LuePosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.Appearance.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosCode.Properties.DropDownRows = 30;
            this.LuePosCode.Properties.MaxLength = 16;
            this.LuePosCode.Properties.NullText = "[Empty]";
            this.LuePosCode.Properties.PopupWidth = 300;
            this.LuePosCode.Size = new System.Drawing.Size(257, 20);
            this.LuePosCode.TabIndex = 39;
            this.LuePosCode.ToolTip = "F4 : Show/hide list";
            this.LuePosCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LblDeptCode
            // 
            this.LblDeptCode.AutoSize = true;
            this.LblDeptCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDeptCode.ForeColor = System.Drawing.Color.Black;
            this.LblDeptCode.Location = new System.Drawing.Point(48, 173);
            this.LblDeptCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDeptCode.Name = "LblDeptCode";
            this.LblDeptCode.Size = new System.Drawing.Size(73, 14);
            this.LblDeptCode.TabIndex = 36;
            this.LblDeptCode.Text = "Department";
            this.LblDeptCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(125, 170);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.MaxLength = 16;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(257, 20);
            this.LueDeptCode.TabIndex = 37;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TpgEmployeeFamily
            // 
            this.TpgEmployeeFamily.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgEmployeeFamily.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgEmployeeFamily.Controls.Add(this.splitContainer2);
            this.TpgEmployeeFamily.Location = new System.Drawing.Point(4, 26);
            this.TpgEmployeeFamily.Name = "TpgEmployeeFamily";
            this.TpgEmployeeFamily.Size = new System.Drawing.Size(894, 536);
            this.TpgEmployeeFamily.TabIndex = 1;
            this.TpgEmployeeFamily.Text = "Familly & Personal Ref.";
            this.TpgEmployeeFamily.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.Grd1);
            this.splitContainer2.Panel1.Controls.Add(this.BtnFamily);
            this.splitContainer2.Panel2Collapsed = true;
            this.splitContainer2.Size = new System.Drawing.Size(890, 532);
            this.splitContainer2.SplitterDistance = 192;
            this.splitContainer2.TabIndex = 41;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 23);
            this.Grd1.Name = "Grd1";
            this.Grd1.ReadOnly = true;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(890, 509);
            this.Grd1.TabIndex = 22;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnFamily
            // 
            this.BtnFamily.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFamily.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnFamily.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFamily.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFamily.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnFamily.Location = new System.Drawing.Point(0, 0);
            this.BtnFamily.Name = "BtnFamily";
            this.BtnFamily.Size = new System.Drawing.Size(890, 23);
            this.BtnFamily.TabIndex = 21;
            this.BtnFamily.Text = "Family";
            this.BtnFamily.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnFamily.UseVisualStyleBackColor = false;
            // 
            // FrmEmployeeUpdateApprovalDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 616);
            this.Name = "FrmEmployeeUpdateApprovalDlg";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.TpgGeneral.ResumeLayout(false);
            this.TpgGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActingOfficial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePositionStatusCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRegEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDivisionCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWorkGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDomicile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRTRW.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaritalStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubDistrict.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBarcodeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEducationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShoeSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueClothesSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSystemType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMother.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrunPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReligion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePTKP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrollType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            this.TpgEmployeeFamily.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.TextEdit TxtDisplayName;
        private System.Windows.Forms.Label label66;
        private DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCodeOld;
        private System.Windows.Forms.Label label48;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgGeneral;
        internal DevExpress.XtraEditors.CheckEdit ChkActingOfficial;
        internal DevExpress.XtraEditors.DateEdit DteContractDt;
        private System.Windows.Forms.Label LblContractDt;
        private System.Windows.Forms.Label label60;
        public DevExpress.XtraEditors.LookUpEdit LuePositionStatusCode;
        internal DevExpress.XtraEditors.TextEdit TxtRegEntCode;
        private System.Windows.Forms.Label label53;
        internal DevExpress.XtraEditors.DateEdit DteTGDt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label70;
        private DevExpress.XtraEditors.LookUpEdit LueEntityCode;
        internal DevExpress.XtraEditors.TextEdit TxtPOH;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label LblDivisionCode;
        private DevExpress.XtraEditors.LookUpEdit LueDivisionCode;
        private System.Windows.Forms.Label label65;
        private DevExpress.XtraEditors.LookUpEdit LueWorkGroup;
        private System.Windows.Forms.Label label64;
        private DevExpress.XtraEditors.LookUpEdit LueSection;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label58;
        private DevExpress.XtraEditors.MemoExEdit MeeDomicile;
        private System.Windows.Forms.Label LblSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        internal DevExpress.XtraEditors.TextEdit TxtRTRW;
        private DevExpress.XtraEditors.LookUpEdit LueMaritalStatus;
        private System.Windows.Forms.Label label56;
        internal DevExpress.XtraEditors.TextEdit TxtVillage;
        private System.Windows.Forms.Label label55;
        internal DevExpress.XtraEditors.TextEdit TxtSubDistrict;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label LblEmploymentStatus;
        internal DevExpress.XtraEditors.TextEdit TxtBarcodeCode;
        private System.Windows.Forms.Label label52;
        internal DevExpress.XtraEditors.TextEdit TxtShortCode;
        private System.Windows.Forms.Label label50;
        private DevExpress.XtraEditors.LookUpEdit LueEmploymentStatus;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label71;
        private DevExpress.XtraEditors.LookUpEdit LueCostGroup;
        private System.Windows.Forms.Label label67;
        private DevExpress.XtraEditors.LookUpEdit LueEducationType;
        private System.Windows.Forms.Label label68;
        private DevExpress.XtraEditors.LookUpEdit LueShoeSize;
        private System.Windows.Forms.Label label61;
        private DevExpress.XtraEditors.LookUpEdit LueClothesSize;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtLevelCode;
        private System.Windows.Forms.Label label47;
        internal DevExpress.XtraEditors.DateEdit DteLeaveStartDt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblPGCode;
        private DevExpress.XtraEditors.LookUpEdit LuePGCode;
        private System.Windows.Forms.Label label62;
        private DevExpress.XtraEditors.LookUpEdit LueBloodType;
        private System.Windows.Forms.Label LblSystemType;
        private DevExpress.XtraEditors.LookUpEdit LueSystemType;
        private DevExpress.XtraEditors.TextEdit TxtMother;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label LblPayrunPeriod;
        private DevExpress.XtraEditors.LookUpEdit LuePayrunPeriod;
        internal DevExpress.XtraEditors.DateEdit DteWeddingDt;
        private System.Windows.Forms.Label label59;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcName;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label LblGrdLvlCode;
        internal DevExpress.XtraEditors.TextEdit TxtEmail;
        private DevExpress.XtraEditors.LookUpEdit LueGrdLvlCode;
        internal DevExpress.XtraEditors.DateEdit DteBirthDt;
        internal DevExpress.XtraEditors.TextEdit TxtBankBranch;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.TextEdit TxtBirthPlace;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcNo;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueReligion;
        internal DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LuePTKP;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.LookUpEdit LuePayrollType;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit TxtNPWP;
        private DevExpress.XtraEditors.LookUpEdit LueGender;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtPhone;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueCity;
        internal DevExpress.XtraEditors.TextEdit TxtIdNumber;
        internal DevExpress.XtraEditors.DateEdit DteResignDt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.DateEdit DteJoinDt;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtUserCode;
        internal DevExpress.XtraEditors.TextEdit TxtPostalCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.MemoExEdit MeeAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LblPosCode;
        public DevExpress.XtraEditors.LookUpEdit LuePosCode;
        private System.Windows.Forms.Label LblDeptCode;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.TabPage TpgEmployeeFamily;
        private System.Windows.Forms.SplitContainer splitContainer2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Button BtnFamily;

    }
}