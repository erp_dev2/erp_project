﻿#region Update
/*
    24/01/2023 [DITA/MNET] New Apps -> based on FrmSOContract
    17/02/2023 [DITA/MNET] pemanggilan compute resource dulu, baru compute total
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContract3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSOContract3 mFrmParent;
        private string mSQL = string.Empty, mCtCode = string.Empty;
        private string mChoosenBOQ = string.Empty;

        #endregion

        #region Constructor

        public FrmSOContract3Dlg(FrmSOContract3 FrmParent, string CtCode)
        {
            InitializeComponent();
            this.Text = "List Bill of Quantity";
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.ProjectName, A.CtCode, B.CtName, A.Remark, A.CtContactPersonName As CtCntCode, D.UserCode PICSales,  ");
            SQL.AppendLine("RemunerationAmt, DirectCost, IndirectCost, TotalCost, CostPerc, RemunerationPerc, DirectCostPerc, IndirectCostPerc, TotalCostPerc ");
            SQL.AppendLine("From TblBOQHdr A  ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode And A.DocType = '1' ");
            SQL.AppendLine("Inner Join TblLOPHdr C On C.DocNo = A.LOPDocNo ");
            SQL.AppendLine("Inner Join TblUser D On C.PICCode = D.UserCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (C.SiteCode Is Null Or ( ");
                SQL.AppendLine("    C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Where A.ActInd = 'Y' And A.Status = 'A'  ");
            SQL.AppendLine("And A.CtCode=@CtCode And A.DocNo not In ( ");
            SQL.AppendLine("  Select BOQDocNo From TblSOContracthdr Where CancelInd = 'N' And Status In ('O', 'A') ");
            SQL.AppendLine(") And C.ProcessInd In ('P', 'L') And C.Status = 'A' And C.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "",
                        "Date",
                        "Customer",
                        "Project name",
                        //6-10
                        "Remark",
                        "Customer Code",
                        "PIC Sales",
                        "RemunerationAmt" ,
                        "DirectCostAmt" ,
                        
                        //11-15
                        "IndirectCostAmt" ,
                        "TotalCost" ,
                        "CostPerc" ,
                        "RemunerationPerc" ,
                        "DirectCostPerc" ,
                        
                        //16-17
                        "IndirectCostPerc" ,
                        "TotalCostPerc"
                    },
                    new int[]
                    {
                        40, 
                        130, 20, 80, 180, 400, 
                        400, 20, 20, 0, 0,
                        0, 0, 0, 0, 0,
                        0, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
               
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo",
                            //1-5
                            "DocDt", "CtName", "ProjectName", "Remark","CtCntCode", 
                            //6-10
                            "PICSales", "RemunerationAmt","DirectCost","IndirectCost","TotalCost",
                            //11-15
                            "CostPerc","RemunerationPerc","DirectCostPerc","IndirectCostPerc","TotalCostPerc"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            string ContactPerson = string.Empty;
            mChoosenBOQ = mFrmParent.TxtBOQDocNo.Text;

            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtBOQDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                Sm.SetLue(mFrmParent.LueCustomerContactperson, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7));
                mFrmParent.TxtProjectName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtPICSales.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                mFrmParent.TxtRemunerationCost.EditValue = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 9, 0);
                mFrmParent.TxtDirectCost.EditValue = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 10, 0);
                mFrmParent.TxtIndirectCost.EditValue = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 11, 0);
                mFrmParent.TxtTotal.EditValue = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 12, 0);
                mFrmParent.TxtCostPerc.EditValue = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 13, 0);
                mFrmParent.TxtRemunerationPerc.EditValue = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 14, 0);
                mFrmParent.TxtDirectPerc.EditValue = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 15, 0);
                mFrmParent.TxtIndirectPerc.EditValue = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 16, 0);
                mFrmParent.TxtTotalPerc.EditValue = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 17, 0);
                mFrmParent.ShowBOQ(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.ShowRevenueItem(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.ShowResource(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.ComputeTotalBOQ();
                mFrmParent.ComputeTotalResource();
                mFrmParent.ComputeContractAmt();
                if (Sm.GetValue("Select ParValue from TblParameter Where Parcode = 'DocTitle'") == "YK" && mChoosenBOQ!=mFrmParent.TxtBOQDocNo.Text)
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ mFrmParent.TxtSAName });
                this.Close();
            }

        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmBOQ(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmBOQ(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        #endregion
    }
}
