﻿#region Update
/*
   
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RuniProbe.GlobalVar;
using Sm = RuniProbe.StdMtd;
using Sl = RuniProbe.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using FastReport;
using FastReport.Data;



#endregion

namespace RuniProbe
{
    public partial class FrmEmpInsPnt2 : RuniProbe.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmEmpInsPnt2Find FrmFind;
        internal bool 
            mIsSiteMandatory = false, 
            mIsFilterBySite = false,
            mIsEmpInsPntBasedOnDept = false,
            mIsEmpInsPntBasedOnSite = false
            ;

        #endregion

        #region Constructor

        public FrmEmpInsPnt2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Incentive/Penalty";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd(); 
                SetFormControl(mState.View);
                SetLuePayment(ref LuePayment);
                Sl.SetLueDeptCode(ref LueDeptCode);
                LblDeptCode.ForeColor = mIsEmpInsPntBasedOnDept ? Color.Red : Color.Black;
                LblSiteCode.ForeColor = mIsEmpInsPntBasedOnSite ? Color.Red : Color.Black;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "",
                        "Old Code",
                        "Employee's Name",
                        "Department"+Environment.NewLine+"Code",

                        //6-10
                        "Department",
                        "Position",
                        "Site",
                        "Amount",
                        "Remark",
                        //11
                        "Payrun"+Environment.NewLine+"Code",
                        
                    },
                     new int[] 
                    {
                        20, 
                        100, 20, 100, 250, 100, 
                        150, 120, 100, 120, 250, 
                        100
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColButton(Grd1, new int[] { 0, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 5 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RuniProbe.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, ChkCancelInd, LueInsPnt, TxtAmt, LueDeptCode, LueSiteCode, LuePayment, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueInsPnt, TxtAmt, LuePayment, LueDeptCode, LueSiteCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 9, 10 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueDeptCode, LueSiteCode, LueInsPnt, LuePayment, 
                MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpInsPnt2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                if (mIsFilterBySite)
                    Sl.SetLueSiteCode(ref LueSiteCode, string.Empty);
                else
                    Sl.SetLueSiteCode(ref LueSiteCode);
                SetLueInsPnt(ref LueInsPnt, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 0 &&
                        (!mIsEmpInsPntBasedOnDept || (mIsEmpInsPntBasedOnDept && !Sm.IsLueEmpty(LueDeptCode, "Department"))) && 
                        (!mIsEmpInsPntBasedOnSite || (mIsEmpInsPntBasedOnSite && !Sm.IsLueEmpty(LueSiteCode, "Site"))) 
                        )
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmEmpInsPnt2Dlg(this, Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode)));
                        
                    }
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && 
                BtnSave.Enabled && 
                (!mIsEmpInsPntBasedOnDept || (mIsEmpInsPntBasedOnDept && !Sm.IsLueEmpty(LueDeptCode, "Department"))) && 
                (!mIsEmpInsPntBasedOnSite || (mIsEmpInsPntBasedOnSite && !Sm.IsLueEmpty(LueSiteCode, "Site"))) &&
                TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmEmpInsPnt2Dlg(this, Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode)));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 9)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    ComputeTotal();
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpInsPnt", "TblEmpInsPntHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpInsPntHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveEmpInsPntDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsEmpInsPntBasedOnDept && Sm.IsLueEmpty(LueDeptCode, "Department")) ||
                (mIsEmpInsPntBasedOnSite && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                Sm.IsLueEmpty(LueInsPnt, "Incentive/Penalty") ||
                Sm.IsTxtEmpty(TxtAmt, "Incentive/Penalty amount", true) ||
                IsGrdEmpty();
                ;

        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpInsPntHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpInsPntHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, DeptCode, SiteCode, InspntCode, AmtInspnt, PaymentType,  Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', @DeptCode, @SiteCode, @InspntCode, @AmtInspnt, @PaymentType, @Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@InspntCode", Sm.GetLue(LueInsPnt));
            Sm.CmParam<Decimal>(ref cm, "@AmtInspnt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePayment));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpInsPntDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpInsPntDtl(DocNo, DNo, EmpCode, AmtInsPnt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @AmtInsPnt, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@AmtInsPnt", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditEmpInsPnt());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsLueEmpty(LueInsPnt, "Insentif/Penalty") ||
                IsDataCancelledAlready();
        }
        private bool IsDataCancelledAlready()
        {
            return 
                Sm.IsDataExist(
                    "Select 1 From TblEmpInsPntHdr Where CancelInd='Y' And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already cancelled.");
        }

        private MySqlCommand EditEmpInsPnt()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpInsPntHdr Set ");
            SQL.AppendLine("CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpInsPntHdr(DocNo);
                ShowEmpInsPntDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpInsPntHdr(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.DeptCode, A.SiteCode, ");
            SQL.AppendLine("A.InspntCode, A.AmtInspnt, A.PaymentType, A.Remark  ");
            SQL.AppendLine("From TblEmpInspnthdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd",  "DeptCode", "SiteCode", "InspntCode", 
                        
                        //6-8
                        "AmtInspnt", "PaymentType", "Remark"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                    Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[4]));
                    SetLueInsPnt(ref LueInsPnt, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueInsPnt, Sm.DrStr(dr, c[5]));
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    Sm.SetLue(LuePayment, Sm.DrStr(dr, c[7]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                }, true
        );

        }

        private void ShowEmpInsPntDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.EmpCode, C.EmpCodeOld, C.Empname, C.DeptCode, D.DeptName, E.Posname, F.SiteName, B.AmtInspnt,  B.Remark, B.PayrunCode  ");
            SQL.AppendLine("From TblEmpInsPntHdr A ");
            SQL.AppendLine("Inner Join TblEmpInsPntDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode = C.EmpCode  ");
            SQL.AppendLine("Inner Join TblDepartment D on C.DeptCode = D.DeptCode ");
            SQL.AppendLine("left Join TblPosition E On C.PosCode = E.PosCode  ");
            
            if(mIsFilterBySite)
                SQL.AppendLine("Inner Join TblSite F on C.SiteCode=F.SiteCode ");
            else
                SQL.AppendLine("Left Join TblSite F on C.SiteCode=F.SiteCode ");
            
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
          
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",
 
                    //1-5
                    "EmpCodeOld", "Empname", "DeptCode", "DeptName",  "PosName", 

                    //6-9
                    "SiteName", "AmtInspnt", "Remark", "PayrunCode", 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }



        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsSiteMandatory = Sm.GetParameter("IsSiteMandatory") == "Y";
            mIsEmpInsPntBasedOnDept = Sm.GetParameter("IsEmpInsPntBasedOnDept") == "Y";
            mIsEmpInsPntBasedOnSite = Sm.GetParameter("IsEmpInsPntBasedOnSite") == "Y";
        }

        private void SetLuePenalty(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select InspntCode As Col1, InspntName As Col2 From TblInspnt;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLuePayment(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='PenaltyPaymentType'",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueInsPnt(ref LookUpEdit Lue, string InsPntCode)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select InsPntCode As Col1, InspntName As Col2 ");
            SQL.AppendLine("From TblInsPnt ");
            if (InsPntCode.Length<=0)
                SQL.AppendLine("Where ActInd='Y' ");
            else
                SQL.AppendLine("Where InsPntCode=@InsPntCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (InsPntCode.Length > 0) Sm.CmParam<String>(ref cm, "@InsPntCode", InsPntCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ComputeTotal()
        {
            decimal amt = 0;
            for (int Row = 0; Row <= Grd1.Rows.Count-1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    amt += Sm.GetGrdDec(Grd1, Row, 9);
                }
            }
            TxtAmt.EditValue = Sm.FormatNum(amt, 0);
        }

        private void ProcessData2()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date")) return;

            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<Result>();

            Grd1.Rows.Clear();
            try
            {
                Process2a(ref lResult);
                if (lResult.Count > 0)
                {
                    Process2b(ref lResult);
                    Process2c();
                    ComputeTotal();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process2a(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;
            var EmpCodeTemp = string.Empty;
            var EmpCodeOldTemp = string.Empty;
            var EmpNameTemp = string.Empty;
            var DeptCodeTemp = string.Empty;
            var DeptNameTemp = string.Empty;
            var PosNameTemp = string.Empty;
            var SiteTemp = string.Empty;
            var AmtTemp = 0m;


            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            if (arr[0].Trim().Length != 8)
                            {
                                EmpCodeTemp = Sm.Right(string.Concat("00000" + arr[0].Trim()), 8);
                            }
                            else
                            {
                                EmpCodeTemp = arr[0].Trim();
                            }
                            string AmtTrim = arr[1].Trim();
                            if (arr[1].Trim().Length > 0)
                                AmtTemp = Decimal.Parse(AmtTrim);
                            else
                                AmtTemp = 0m;

                            l.Add(new Result()
                            {
                                EmpCode = EmpCodeTemp,
                                EmpCodeOld = string.Empty,
                                EmpName = string.Empty,
                                DeptCode = string.Empty,
                                DeptName = string.Empty,
                                PosName = string.Empty,
                                Site = string.Empty,
                                Amt = AmtTemp,
                            });
                        }
                    }
                }
            }
        }

        private void Process2b(ref List<Result> l)
        {
            decimal Amt = 0m;
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[1].Value = l[i].EmpCode;
                r.Cells[3].Value = string.Empty;
                r.Cells[4].Value = string.Empty;
                r.Cells[5].Value = string.Empty;
                r.Cells[6].Value = string.Empty;
                r.Cells[7].Value = string.Empty;
                r.Cells[8].Value = string.Empty;
                r.Cells[9].Value = l[i].Amt;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9 });
            Grd1.EndUpdate();
        }

        private void Process2c()
        {
            Grd1.BeginUpdate();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var EmpCode = string.Empty;
            var Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0<>1 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, A.DeptCode,  ");
            SQL.AppendLine("B.DeptName, C.PosName, D.Sitename ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
            SQL.AppendLine("Where 0=0 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "EmpCodeOld", 
                    "EmpName", 
                    "DeptCode",
                    "DeptName", 
                    "PosName",
                    //6
                    "SiteName"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (EmpCode == Sm.GetGrdStr(Grd1, Row, 1))
                            {
                                //Grd1.Cells[Row, 1].Value = Sm.DrStr(dr, c[0]);
                                Grd1.Cells[Row, 3].Value = Sm.DrStr(dr, c[1]);
                                Grd1.Cells[Row, 4].Value = Sm.DrStr(dr, c[2]);
                                Grd1.Cells[Row, 5].Value = Sm.DrStr(dr, c[3]);
                                Grd1.Cells[Row, 6].Value = Sm.DrStr(dr, c[4]);
                                Grd1.Cells[Row, 7].Value = Sm.DrStr(dr, c[5]);
                                Grd1.Cells[Row, 8].Value = Sm.DrStr(dr, c[6]);
                            }
                        }
                    }
                }
                dr.Close();
            }
            Grd1.EndUpdate();
        }

        #endregion     

        #endregion

        #region Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsFilterBySite)
                    Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
                else
                    Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            }
        }

        private void LueInsPnt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueInsPnt, new Sm.RefreshLue2(SetLueInsPnt), string.Empty);
                //var InsPntCode = Sm.GetLue(LueInsPnt);
                //if (InsPntCode.Length > 0)
                //{
                //    var Amt = Sm.GetValue("Select Amt From TblInsPnt Where InsPntCode=@Param;", InsPntCode);
                //    if (Amt.Length > 0) TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
                //}
            }
        }

        private void LuePayment_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePayment, new Sm.RefreshLue1(SetLuePayment));
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            ProcessData2();
        }
        #endregion

       

        #region Class

        private class Result
        {
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpName { get; set; }
            public string DeptCode { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string Site { get; set; }
            public decimal Amt { get; set; }

        }

        #endregion
        

        
    }
}
