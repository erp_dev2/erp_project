﻿#region Update
/*
    24/01/2018 [TKG] Purchasing Downpayment To Receiving Control Reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchasingDPToRecvControl : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRptPurchasingDPToRecvControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueVdCode(ref LueVdCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, F.VdName, A.CurCode, ");
            SQL.AppendLine("A.Amt-A.TaxAmt As POAmtBeforeTax, A.TaxAmt, A.Amt As POAmt,");
            SQL.AppendLine("B.DocDt As APDownpaymentDocDt, IfNull(B.Amt, 0.00) As APDownpaymentAmt, ");
            SQL.AppendLine("C.DocDt As RecvVdDocDt, IfNull(C.Amt, 0.00) As RecvVdAmt, ");
            SQL.AppendLine("IfNull(D.Amt, 0.00) As DOVdVdAmt, ");
            SQL.AppendLine("IfNull(E.Amt, 0.00) As DODeptVdAmt, ");
            SQL.AppendLine("Case When (B.EarliestDocDt Is Not Null And C.LatestDocDt is Not Null) Then ");
            SQL.AppendLine("        DateDiff(Date_Format(C.LatestDocDt, '%Y%m%d'), Date_Format(B.EarliestDocDt, '%Y%m%d')) ");
            SQL.AppendLine("Else 0.00 End As ApDownpaymentVSRecvVd ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T2.PODocNo As DocNo, ");
            SQL.AppendLine("    Min(T1.DocDt) As EarliestDocDt, ");
            SQL.AppendLine("    Group_Concat(Distinct ");
	        SQL.AppendLine("    Concat(Right(T1.DocDt, 2), '/', Left(MonthName(Str_To_Date(Substring(T1.DocDt, 5, 2), '%m')), 3), '/', Left(T1.DocDt, 4)) ");
	        SQL.AppendLine("    Order By T1.DocDt Separator ', ' ");
	        SQL.AppendLine("    ) As DocDt,  ");
            SQL.AppendLine("    Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblVoucherHdr T1, TblAPDownpayment T2 ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocType='04' ");
            SQL.AppendLine("    And T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
            SQL.AppendLine("    And T2.PODocNo In  ( ");
            SQL.AppendLine("        Select DocNo From TblPOHdr A ");
            SQL.AppendLine("        Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T2.PODocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T2.PODocNo As DocNo, ");
            SQL.AppendLine("    Max(T1.DocDt) As LatestDocDt, ");
            SQL.AppendLine("    Group_Concat(Distinct ");
            SQL.AppendLine("    Concat(Right(T1.DocDt, 2), '/', Left(MonthName(Str_To_Date(Substring(T1.DocDt, 5, 2), '%m')), 3), '/', Left(T1.DocDt, 4)) ");
            SQL.AppendLine("    Order By T1.DocDt Separator ', ' ");
            SQL.AppendLine("    ) As DocDt,  ");
            SQL.AppendLine("    Sum(T2.Qty*T3.UPrice) As Amt ");
            SQL.AppendLine("    From TblRecvVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.Status In ('O', 'A') ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.PODocNo In  ( ");
            SQL.AppendLine("            Select DocNo From TblPOHdr A ");
            SQL.AppendLine("            Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Inner Join TblStockPrice T3 On T2.Source=T3.Source ");
            SQL.AppendLine("    Group By T2.PODocNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T4.PODocNo As DocNo, ");
            SQL.AppendLine("    Min(T1.DocDt) As EarliestDocDt, ");
            SQL.AppendLine("    Group_Concat(Distinct ");
            SQL.AppendLine("    Concat(Right(T1.DocDt, 2), '/', Left(MonthName(Str_To_Date(Substring(T1.DocDt, 5, 2), '%m')), 3), '/', Left(T1.DocDt, 4)) ");
            SQL.AppendLine("    Order By T1.DocDt Separator ', ' ");
            SQL.AppendLine("    ) As DocDt,  ");
            SQL.AppendLine("    Sum(T2.Qty*T3.UPrice) As Amt ");
            SQL.AppendLine("    From TblDOVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblStockPrice T3 On T2.Source=T3.Source ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T4 On T2.RecvVdDocNo=T4.DocNo And T2.RecvVdDNo=T4.DNo  ");
            SQL.AppendLine("        And T4.PODocNo In  ( ");
            SQL.AppendLine("            Select DocNo From TblPOHdr A ");
            SQL.AppendLine("            Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T4.PODocNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T4.PODocNo As DocNo, ");
            SQL.AppendLine("    Sum(T2.Qty*T3.UPrice) As Amt ");
            SQL.AppendLine("    From TblDODeptHdr T1 ");
            SQL.AppendLine("    Inner Join TblDODeptDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblStockPrice T3 On T2.Source=T3.Source ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T4 On T2.Source=T4.Source ");
            SQL.AppendLine("        And T4.PODocNo In  ( ");
            SQL.AppendLine("            Select DocNo From TblPOHdr A ");
            SQL.AppendLine("            Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T4.PODocNo ");
            SQL.AppendLine(") E On A.DocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblVendor F On A.VdCode=F.VdCode ");
            SQL.AppendLine("Where A.DocNo In (Select DocNo from TblPODtl Where CancelInd='N') ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "PO#",
                        "PO"+Environment.NewLine+"Date",
                        "Vendor",
                        "",
                        "Currency",
                        
                        //6-10
                        "PO (Before Tax)",
                        "Tax",
                        "PO (After Tax)",
                        "Downpayment"+Environment.NewLine+"Date",
                        "Downpayment",
                        
                        //11-15
                        "Received"+Environment.NewLine+"Date",
                        "Received",
                        "Returned",
                        "DO To"+Environment.NewLine+"Department",
                        "Downpayment"+Environment.NewLine+"vs Received"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 100, 200, 20, 60, 

                        //6-10
                        130, 130, 130, 100, 130, 

                        //11-15
                        100, 130, 130, 130, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 10, 12, 13, 14, 15 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", 
                            "VdName", 
                            "CurCode", 
                            "POAmtBeforeTax", 
                            "TaxAmt", 
                            
                            //6-10
                            "POAmt", 
                            "APDownpaymentDocDt", 
                            "APDownpaymentAmt", 
                            "RecvVdDocDt", 
                            "RecvVdAmt", 
                            
                            //11-13
                            "DOVdVdAmt", 
                            "DODeptVdAmt", 
                            "ApDownpaymentVSRecvVd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 7, 8, 10, 12, 13, 14, 15 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    var r = e.RowIndex;
                    Sm.FormShowDialog(new FrmRptPurchasingDPToRecvControlDlg(this, Sm.GetGrdStr(Grd1, r, 1), Sm.GetGrdStr(Grd1, r, 5)));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                var r = e.RowIndex;
                Sm.FormShowDialog(new FrmRptPurchasingDPToRecvControlDlg(this, Sm.GetGrdStr(Grd1, r, 1), Sm.GetGrdStr(Grd1, r, 5)));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion

        #endregion
    }
}
