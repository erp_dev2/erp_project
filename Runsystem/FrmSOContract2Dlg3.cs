﻿#region Update
/*
    10/10/2019 [WED/IMS] Delivery Date mandatory berdasarkan parameter IsSOContractItemMultiDeliveryDateEnabled
    06/01/2020 [WED/IMS] tax dimunculkan
    19/02/2020 [DITA/IMS] ganti source dari BOM ke ITEM
    19/02/2020 [DITA/IMS] price list mengambil dari unit price contract, penambahan informasi total setelah PPN 10%
    20/02/2020 [WED/IMS] tambah revisi
    26/10/2020 [DITA/IMS] masih error unequal saat save, ketika tax diisi
    26/10/2020 [DITA/IMS] Price disesuaikan dengan item berdasarkan nomor item nya dan it code nya. note : ada tambahan txtbox baru txtnoitem di sebelah txtitcode posisi visible = false
    22/04/2021 [TKG/IMS] ubah query di ShowItemInfo1()
    29/04/2021 [TKG/IMS] tambah try catch saat tekan tombol add, pengecekan nilai decimal
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmSOContract2Dlg3 : RunSystem.FrmBase8
    {
        #region Field

        private FrmSOContract2 mFrmParent;
        private bool mIsDataExisted = false;
        private bool mIsShowOnly = true;
        private string mItCode = "", mCtCode = string.Empty;
        private int mRow = -1;
        internal string mCtQtDNo = string.Empty;
        internal decimal mVolumePackaging = 0m, mTaxRate = 0m;
        internal string mVolUom = string.Empty;
        internal string mBOQDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmSOContract2Dlg3(FrmSOContract2 FrmParent,
            int Row,
            bool IsShowQty2,
            bool IsDataExisted,
            bool IsShowOnly,
            string CtCode,
            string ItCode,
            string BOQDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
            mIsDataExisted = IsDataExisted;
            mIsShowOnly = IsShowOnly;
            mCtCode = CtCode;
            mItCode = ItCode;
            mBOQDocNo = BOQDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetFormControl(mIsShowOnly);
                if (mIsDataExisted) ShowData(ref mFrmParent.Grd3);
                if (mFrmParent.mIsSOContractItemMultiDeliveryDateEnabled) LblDeliveryDt.ForeColor = Color.Red;
                mVolUom = Sm.GetParameter("ItemVolumeUom");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Button Method

        override protected void BtnAddClick(object sender, EventArgs e)
        {
            try
            {
                if (!IsInsertedDataNotValid()) InsertData(ref mFrmParent.Grd3);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Method

        private void SetFormControl(bool IsShowOnly)
        {
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
            { 
               LuePackaging, TxtQtyPackaging, DteDeliveryDt, MeeRemark
            }, IsShowOnly);
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
            { 
                TxtQty, TxtUPrice, TxtDiscount, 
                TxtDiscountAmt, TxtUPrice1, TxtPromoRate, TxtUPriceBefore,
                TxtTaxRate, TxtTaxAmt, TxtUPriceAfter, TxtTotal
            }, true);
            BtnItem.Enabled = !IsShowOnly;
            BtnAdd.Visible = !IsShowOnly;
            ClearData();
        }

        public void ClearData()
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                  TxtQtyPackaging, TxtQty, TxtUPrice, TxtDiscount, 
                  TxtDiscountAmt, TxtUPrice1, TxtPromoRate, TxtUPriceBefore, TxtTaxRate, 
                  TxtTaxAmt, TxtUPriceAfter, TxtTotal
            }, 0);
            mCtQtDNo = string.Empty;
            mVolumePackaging = 0m;
        }

        public void FillPriceList()
        {
            for (int Row = 0; Row < mFrmParent.Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(mFrmParent.Grd2, Row, 3).Length > 0)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd2, Row, 3) == TxtItCode.Text && Sm.GetGrdStr(mFrmParent.Grd2, Row, 23) == TxtNoItem.Text)
                    {
                        if (mFrmParent.IsInsert)
                        {
                            mFrmParent.Grd3.Rows[mRow].Cells[11].Value = Sm.GetGrdDec(mFrmParent.Grd2, Row, 19);
                            mFrmParent.Grd3.Rows[mRow].Cells[16].Value = Sm.GetGrdDec(mFrmParent.Grd2, Row, 19);
                        }
                        else
                        {
                            mFrmParent.Grd3.Rows[mRow].Cells[11].Value = Sm.GetGrdDec(mFrmParent.Grd2, Row, 21);
                            mFrmParent.Grd3.Rows[mRow].Cells[16].Value = Sm.GetGrdDec(mFrmParent.Grd2, Row, 21);
                        }
                    }
                }
            }

            for (int Row = 0; Row < mFrmParent.Grd6.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(mFrmParent.Grd6, Row, 3).Length > 0)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd6, Row, 3) == TxtItCode.Text && Sm.GetGrdStr(mFrmParent.Grd6, Row, 25) == TxtNoItem.Text)
                    {
                        if (mFrmParent.IsInsert)
                        {
                            mFrmParent.Grd3.Rows[mRow].Cells[11].Value = Sm.GetGrdDec(mFrmParent.Grd6, Row, 21);
                            mFrmParent.Grd3.Rows[mRow].Cells[16].Value = Sm.GetGrdDec(mFrmParent.Grd6, Row, 21);
                        }
                        else
                        {
                            mFrmParent.Grd3.Rows[mRow].Cells[11].Value = Sm.GetGrdDec(mFrmParent.Grd6, Row, 23);
                            mFrmParent.Grd3.Rows[mRow].Cells[16].Value = Sm.GetGrdDec(mFrmParent.Grd6, Row, 23);
                        }
                    }
                }
            }
        }

        private void InsertData(ref iGrid Grd)
        {
            Grd.Rows[mRow].Cells[4].Value = TxtItCode.Text;
            Grd.Rows[mRow].Cells[6].Value = TxtItName.Text;
            Grd.Rows[mRow].Cells[7].Value = Sm.GetLue(LuePackaging);
            Grd.Rows[mRow].Cells[8].Value = TxtQtyPackaging.Text;
            Grd.Rows[mRow].Cells[9].Value = TxtQty.Text;
            Grd.Rows[mRow].Cells[10].Value = TxtUomCode.Text;
            //Grd.Rows[mRow].Cells[11].Value = TxtUPrice.Text;
            Grd.Rows[mRow].Cells[12].Value = TxtDiscount.Text;
            Grd.Rows[mRow].Cells[13].Value = TxtDiscountAmt.Text;
            Grd.Rows[mRow].Cells[14].Value = TxtUPrice1.Text;
            Grd.Rows[mRow].Cells[15].Value = TxtPromoRate.Text;
            //Grd.Rows[mRow].Cells[16].Value = TxtUPriceBefore.Text;
            Grd.Rows[mRow].Cells[17].Value = TxtTaxRate.Text;
            Grd.Rows[mRow].Cells[18].Value = TxtTaxAmt.Text;
            Grd.Rows[mRow].Cells[19].Value = TxtUPriceAfter.Text;
            Grd.Rows[mRow].Cells[20].Value = TxtTotal.Text;
            if (Sm.GetDte(DteDeliveryDt).Length == 0)
                Grd.Rows[mRow].Cells[21].Value = null;
            else
                Grd.Rows[mRow].Cells[21].Value = Sm.SetGrdDate(Sm.GetDte(DteDeliveryDt).Substring(0, 8));
            Grd.Rows[mRow].Cells[22].Value = MeeRemark.Text;
            Grd.Rows[mRow].Cells[23].Value = mCtQtDNo;
            Grd.Rows[mRow].Cells[24].Value = TxtSpecification.Text;
            Grd.Rows[mRow].Cells[25].Value = TxtCtItCode.Text;
            Grd.Rows[mRow].Cells[26].Value = TxtCtItName.Text;
            Grd.Rows[mRow].Cells[27].Value = mVolumePackaging;
            if (TxtQtyPackaging.Text.Length>0)
                Grd.Rows[mRow].Cells[28].Value = Decimal.Parse(TxtQtyPackaging.Text) * mVolumePackaging;
            else
                Grd.Rows[mRow].Cells[28].Value = 0m;
            Grd.Rows[mRow].Cells[29].Value = mVolUom;

            FillPriceList();
            //mFrmParent.ComputeItem();

            if (mRow == Grd.Rows.Count - 1)
            {
                Grd.Rows.Add();
                Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            }
            this.Close();
        }

        private void ShowData(ref iGrid Grd)
        {
            TxtItCode.EditValue = Sm.GetGrdStr(Grd, mRow, 4);
            TxtItName.EditValue = Sm.GetGrdStr(Grd, mRow, 6);
            SetLuePackaging(ref LuePackaging, mItCode, Sm.GetGrdStr(Grd, mRow, 7));
            Sm.SetLue(LuePackaging, Sm.GetGrdStr(Grd, mRow, 7));
            TxtQtyPackaging.EditValue = Sm.GetGrdDec(Grd, mRow, 8, 0);
            TxtQty.EditValue = Sm.GetGrdDec(Grd, mRow, 9, 0);
            TxtUomCode.EditValue = Sm.GetGrdStr(Grd, mRow, 10);
            TxtUPrice.EditValue = Sm.GetGrdDec(Grd, mRow, 11, 0);
            TxtDiscount.EditValue = Sm.GetGrdDec(Grd, mRow, 12, 0);
            TxtDiscountAmt.EditValue = Sm.GetGrdDec(Grd, mRow, 13, 0);
            TxtUPrice1.EditValue = Sm.GetGrdDec(Grd, mRow, 14, 0);
            TxtPromoRate.EditValue = Sm.GetGrdDec(Grd, mRow, 15, 0);
            TxtUPriceBefore.EditValue = Sm.GetGrdDec(Grd, mRow, 16, 0);
            TxtTaxRate.EditValue = Sm.GetGrdDec(Grd, mRow, 17, 0);
            TxtTaxAmt.EditValue = Sm.GetGrdDec(Grd, mRow, 18, 0);
            TxtUPriceAfter.EditValue = Sm.GetGrdDec(Grd, mRow, 19, 0);
            TxtTotal.EditValue = Sm.GetGrdDec(Grd, mRow, 20, 0);
            Sm.SetDte(DteDeliveryDt, Sm.GetGrdDate(Grd, mRow, 21));
            MeeRemark.EditValue = Sm.GetGrdStr(Grd, mRow, 22);
            mCtQtDNo = Sm.GetGrdStr(Grd, mRow, 23);
            TxtSpecification.EditValue = Sm.GetGrdStr(Grd, mRow, 24);
            TxtCtItCode.EditValue = Sm.GetGrdStr(Grd, mRow, 25);
            TxtCtItName.EditValue = Sm.GetGrdStr(Grd, mRow, 26);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItCode, "Item's code", false) ||
                Sm.IsLueEmpty(LuePackaging, "Packaging UOM") ||
                Sm.IsTxtEmpty(TxtQtyPackaging, "Packaging Quantity", true) ||
                Sm.IsTxtEmpty(TxtQty, "Quantity", true) ||
                (mFrmParent.mIsSOContractItemMultiDeliveryDateEnabled && Sm.IsDteEmpty(DteDeliveryDt, "Delivery Date"));
               // IsQtyTaxNotValid();
        }

     
        private bool IsQtyTaxNotValid()
        {
            if (TxtTaxRate.Text.Length > 0)
            {
                if (Decimal.Parse(TxtTaxRate.Text) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Tax rate cannot less than 0.");
                    return true;
                }
            }
            return false;
        }

        private bool IsPackagingAlreadyExist()
        {
            string ItCode = TxtItCode.Text;
            string Packaging = Sm.GetLue(LuePackaging);
            string Uom = TxtUomCode.Text;

            for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.CompareStr(ItCode + Packaging + Uom, Sm.GetGrdStr(mFrmParent.Grd1, Row, 4) + Sm.GetGrdStr(mFrmParent.Grd1, Row, 7) + Sm.GetGrdStr(mFrmParent.Grd1, Row, 10)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                               "Item's Code : " + TxtItCode.Text + Environment.NewLine +
                               "Item's Name : " + TxtItName.Text + Environment.NewLine +
                               "Packaging : " + Sm.GetLue(LuePackaging) + Environment.NewLine +
                               "UoM : " + TxtUomCode.Text + Environment.NewLine +
                               "Already exist.");
                    return true;
                }
            }
            return false;
        }

        internal void ShowItemInfo()
        {
            ShowItemInfo1();
            ShowItemInfo2();
        }


        private void ShowItemInfo1()
        {
            decimal mUPriceContract = 0m;

            for (int i = 0; i < mFrmParent.Grd2.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(mFrmParent.Grd2, i, 3).Length > 0 && Sm.GetGrdStr(mFrmParent.Grd2, i, 23).Length > 0)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd2, i, 3) == TxtItCode.Text && Sm.GetGrdStr(mFrmParent.Grd2, i, 23) == TxtNoItem.Text)
                    {
                        if (mFrmParent.IsInsert)
                        {
                            mUPriceContract = Sm.GetGrdDec(mFrmParent.Grd2, i, 19);
                            break;
                        }
                        else
                        {
                            mUPriceContract = Sm.GetGrdDec(mFrmParent.Grd2, i, 21);
                            break;
                        }
                    }
                }
            }

            for (int Row = 0; Row < mFrmParent.Grd6.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(mFrmParent.Grd6, Row, 3).Length > 0 && Sm.GetGrdStr(mFrmParent.Grd6, Row, 25).Length > 0)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd6, Row, 3) == TxtItCode.Text && Sm.GetGrdStr(mFrmParent.Grd6, Row, 25) == TxtNoItem.Text)
                    {
                        if (mFrmParent.IsInsert)
                        {
                            mUPriceContract = Sm.GetGrdDec(mFrmParent.Grd6, Row, 21);
                            break;
                        }
                        else
                        {
                            mUPriceContract = Sm.GetGrdDec(mFrmParent.Grd6, Row, 23);
                            break;
                        }
                    }
                }
            }

            TxtUomCode.EditValue = null;
            TxtUPrice.EditValue = Sm.FormatNum(mUPriceContract, 0);
            TxtDiscount.EditValue = Sm.FormatNum(0m, 0);
            TxtUPrice1.EditValue = Sm.FormatNum(mUPriceContract, 0);
            TxtPromoRate.EditValue = Sm.FormatNum(0m, 0);
            mVolumePackaging = 0;
            string mGetTaxRate = Sm.GetValue("Select TaxRate From TblTax Where TaxCode = @Param", Sm.GetLue(mFrmParent.LueTaxCode));
            if (mGetTaxRate.Length == 0) mTaxRate = 0m;
            else mTaxRate = decimal.Parse(mGetTaxRate);
            
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.SalesuomCode, IfNull(D.Qty, 0.00) Qty, IFNULL(D.UPriceReal, 0.00) As UPriceReal, IFNULL(D.UPriceAf, 0.00) As UpriceAf, 0 As Discount, ");
            //SQL.AppendLine("0 as DiscRate, '001' DNo, A.Specification, B.CtItCode, B.CtItName, C.Volume, IfNull(D.TaxRate, 0.00) TaxRate, IfNull(D.TaxAmt, 0.00) TaxAmt ");
            //SQL.AppendLine("From TblItem A ");
            //SQL.AppendLine("Inner JOIN ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    SELECT T2.ItCode, T1.Qty, T1.Amt UPriceReal, T1.SPHAmt UPriceAf, ");
            //SQL.AppendLine("    ROUND((((T1.SPHAmt - T1.Amt) / T1.Amt) * 100.00), 4) TaxRate, (T1.SPHAmt - T1.Amt) TaxAmt ");
            //SQL.AppendLine("    FROM TblBOQDtl2 T1, TblItem T2 ");
            //SQL.AppendLine("    Where T1.ItCode = T2.ItCode ");
            //SQL.AppendLine("    AND T1.DocNo = @DocNo ");
            //SQL.AppendLine("    And T1.Amt>0.00 ");
            //SQL.AppendLine("    And T2.ItCode = @ItCode ");
            //SQL.AppendLine("    UNION All ");
            //SQL.AppendLine("    SELECT T2.ItCode, T1.Qty, T1.CostOfGoodsAmt UPriceReal, T1.UPriceSPHAmt UPriceAf, ");
            //SQL.AppendLine("    ROUND((((T1.UPriceSPHAmt - T1.CostOfGoodsAmt) / T1.CostOfGoodsAmt) * 100.00), 4) TaxRate, ");
            //SQL.AppendLine("    (UPriceSPHAmt - CostOfGoodsAmt) TaxAmt ");
            //SQL.AppendLine("    FROM TblBOQDtl3 T1, TblItem T2 ");
            //SQL.AppendLine("    Where T1.ItCode = T2.ItCode ");
            //SQL.AppendLine("    And T1.DocNo = @DocNo ");
            //SQL.AppendLine("    And T1.CostOfGoodsAmt> 0.00 ");
            //SQL.AppendLine("    And T2.ItCode=@ItCode ");
            //SQL.AppendLine(") D ON A.ItCode = D.ItCode ");
            //SQL.AppendLine("Left Join TblCustomerItem B On A.ItCode=B.ItCode And B.CtCode=@CtCode ");
            //SQL.AppendLine("Left Join tblitempackagingunit C On A.ItCode = C.ItCode And A.SalesUomCode = C.UomCode ");
            //SQL.AppendLine("Where A.SalesitemInd = 'Y' ");
            //SQL.AppendLine("And A.ItCode=@ItCode;");

            SQL.AppendLine("Select A.SalesUomCode, A.Specification, B.CtItCode, B.CtItName, IfNull(C.Volume, 0.00) As Volume, ");
            SQL.AppendLine("IfNull(D.Qty, 0.00) Qty, IfNull(D.TaxAmt, 0.00) TaxAmt ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblCustomerItem B On A.ItCode=B.ItCode And B.CtCode=@CtCode ");
            SQL.AppendLine("Left Join TblItemPackagingUnit C On A.ItCode=C.ItCode And A.SalesUomCode=C.UomCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select ItCode, Qty, (SPHAmt-Amt) As TaxAmt ");
            SQL.AppendLine("    FROM TblBOQDtl2 ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And Amt>0.00 ");
            SQL.AppendLine("    And ItCode=@ItCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select ItCode, Qty, (UPriceSPHAmt - CostOfGoodsAmt) As TaxAmt ");
            SQL.AppendLine("    From TblBOQDtl3 ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CostOfGoodsAmt> 0.00 ");
            SQL.AppendLine("    And ItCode=@ItCode ");
            SQL.AppendLine(") D ON A.ItCode = D.ItCode ");
            SQL.AppendLine("Where A.SalesitemInd = 'Y' ");
            SQL.AppendLine("And A.ItCode=@ItCode;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@DocNo", mBOQDocNo);
           
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    //new string[] 
                    //{ 
                    //    //0
                    //    "SalesUomCode", 

                    //    //1-5
                    //    "UPriceReal", "UPriceAf", "Discount", "DiscRate", "DNo",
 
                    //    //6-10
                    //    "Specification", "CtItCode", "CtItName", "Volume", "Qty",

                    //    //11-12
                    //    "TaxRate", "TaxAmt"
                    //},
                    //(MySqlDataReader dr, int[] c) =>
                    //{
                    //    TxtUomCode.EditValue = Sm.DrStr(dr, c[0]);
                    //    //TxtUPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[1]), 0);
                    //    //TxtUPrice1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[1]), 0);
                    //    //TxtDiscount.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                    //    //TxtPromoRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                    //    mCtQtDNo = Sm.DrStr(dr, c[5]);
                    //    TxtSpecification.EditValue = Sm.DrStr(dr, c[6]);
                    //    TxtCtItCode.EditValue = Sm.DrStr(dr, c[7]);
                    //    TxtCtItName.EditValue = Sm.DrStr(dr, c[8]);
                    //    mVolumePackaging = Sm.DrDec(dr, c[9]);
                    //    TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    //    TxtTaxRate.EditValue = (mTaxRate == 0m ? Sm.FormatNum(0m, 0) : Sm.FormatNum(mTaxRate,0));
                    //    TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    //}, false
                    new string[] 
                    { 
                        //0
                        "SalesUomCode", 

                        //1-5
                        "Specification", "CtItCode", "CtItName", "Volume", "Qty", 
                        
                        //6
                        "TaxAmt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        mCtQtDNo = "001";
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtSpecification.EditValue = Sm.DrStr(dr, c[1]);
                        TxtCtItCode.EditValue = Sm.DrStr(dr, c[2]);
                        TxtCtItName.EditValue = Sm.DrStr(dr, c[3]);
                        mVolumePackaging = Sm.DrDec(dr, c[4]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                        TxtTaxRate.EditValue = (mTaxRate == 0m ? Sm.FormatNum(0m, 0) : Sm.FormatNum(mTaxRate,0));
                        TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    }, false

                );
        }

        private void ShowItemInfo2()
        {
            decimal
                Qty = 0m,
                UPrice = 0m,
                Discount = 0m,
                UPrice1 = 0m,
                PromoRate = 0m,
                UPriceBefore = 0m,
                TaxRate = 0m,
                TaxAmt = 0m,
                UPriceAfter = 0m;

            if (TxtQty.Text.Length > 0) Qty = decimal.Parse(TxtQty.Text);
            if (TxtUPrice.Text.Length > 0) UPrice = decimal.Parse(TxtUPrice.Text);
            //if (TxtDiscount.Text.Length > 0) Discount = decimal.Parse(TxtDiscount.Text);
            //TxtDiscountAmt.EditValue = Sm.FormatNum(Discount * 0.01m * UPrice, 0);
            TxtDiscountAmt.EditValue = Sm.FormatNum(0m, 0);
            if (TxtUPrice1.Text.Length > 0) UPrice1 = decimal.Parse(TxtUPrice1.Text);
            if (TxtPromoRate.Text.Length > 0) PromoRate = decimal.Parse(TxtPromoRate.Text);
            TxtUPriceBefore.EditValue = Sm.FormatNum(UPrice1 - (UPrice1 * 0.01m * PromoRate), 0);
            if (TxtUPriceBefore.Text.Length > 0) UPriceBefore = decimal.Parse(TxtUPriceBefore.Text);
            if (TxtTaxRate.Text.Length > 0) TaxRate = decimal.Parse(TxtTaxRate.Text);
            TxtTaxAmt.EditValue = Sm.FormatNum(TaxRate * 0.01m * UPrice, 0);
            if (TxtTaxAmt.Text.Length > 0) TaxAmt = decimal.Parse(TxtTaxAmt.Text);
            TxtUPriceAfter.EditValue = Sm.FormatNum(UPriceBefore + TaxAmt, 0);
            if (TxtUPriceAfter.Text.Length > 0) UPriceAfter = decimal.Parse(TxtUPriceAfter.Text);
            TxtTotal.EditValue = Sm.FormatNum(UPriceAfter * Qty, 0);
        }

        #endregion 

        #region Additional Method

        private decimal GetSalesUomCodeConvert(string ConvertType, string ItCode)
        {
            var cm = new MySqlCommand
            {
                CommandText = "Select SalesUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            return Sm.GetValueDec(cm);
        }

        private decimal ComputeQtyBasedOnConvertionFormula(string ConvertType, decimal Qty, decimal Qty2)
        {
            decimal Convert = GetSalesUomCodeConvert(ConvertType, TxtItCode.Text);
            if (Convert != 0) Qty2 = Convert * Qty;
            
            return Qty2;
        }

        internal void ComputeUomSales()
        {
            decimal QtyPackaging = 0m, QtyUomSales1 = 0m, QtyUomSales2 = 0m;
            string UomSales = string.Empty, UomPackaging = string.Empty, v = string.Empty;

            try
            {
                UomPackaging = Sm.GetLue(LuePackaging);
                if (TxtQtyPackaging.Text.Length>0)
                    QtyPackaging = Decimal.Parse(TxtQtyPackaging.Text);
                if (UomPackaging != string.Empty)
                {
                    v = Sm.GetValue("Select Qty From TblItemPackagingUnit Where ItCode = '" + TxtItCode.Text + "' And UomCode = '" + UomPackaging + "';");
                    if (v.Length>0) QtyUomSales1 = Decimal.Parse(v);
                    v = Sm.GetValue("Select Qty2 From TblItemPackagingUnit Where ItCode = '" + TxtItCode.Text + "' And UomCode = '" + UomPackaging + "';");
                    if (v.Length > 0) QtyUomSales2 = Decimal.Parse(v);

                    UomSales = Sm.GetValue("Select SalesUomCode From TblItem Where ItCode = '" + TxtItCode.Text + "' ");
                    if (UomSales == TxtUomCode.Text)
                        TxtQty.EditValue = Sm.FormatNum((QtyPackaging * QtyUomSales1), 0);
                    else
                        TxtQty.EditValue = Sm.FormatNum((QtyPackaging * QtyUomSales2), 0);
                }
            }               
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public void SetLuePackaging(ref LookUpEdit Lue, string ItCode, string UomCode)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Col1, Col2 From ( ");
            SQL.AppendLine("    Select UomCode As Col1, UomCode As Col2 ");
            SQL.AppendLine("    From TblItemPackagingUnit ");
            SQL.AppendLine("    Where ItCode=@ItCode ");
            if (UomCode.Length != 0)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select UomCode As Col1, UomCode As Col2 ");
                SQL.AppendLine("    From TblUom ");
                SQL.AppendLine("    Where UomCode=@UomCode ");
            }
            SQL.AppendLine(") T Order By Col1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@UomCode", UomCode);

            Sm.SetLue2(
              ref Lue, ref cm,
              0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        private void BtnItem_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSOContract2Dlg4(this, mBOQDocNo));
        }

        private void LuePackaging_EditValueChanged(object sender, EventArgs e)
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtQtyPackaging, TxtQty, TxtTotal }, 0);
            Sm.RefreshLookUpEdit(LuePackaging, new Sm.RefreshLue3(SetLuePackaging), TxtItCode.Text, "");
        }

        private void TxtQtyPackaging_Validated(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LuePackaging, "Packaging UoM")) return;
            try
            {
                Sm.FormatNumTxt(TxtQtyPackaging, 0);
                ComputeUomSales();
                ShowItemInfo2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void TxtTaxRate_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTaxRate, 0);
        }

        private void BtnMasterItem_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtItCode, "Item Code", false))
            {
                try
                {
                    var f = new FrmItem("XXX");
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = TxtItCode.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
            }
        }
        #endregion 
    }
}
