﻿#region Update
/*
    17/01/2018 [TKG] tambah foreign name     
    11/04/2018 [TKG] tambah import indicator dan remark
    27/11/2018 [TKG] tambah function proses excel untuk kolom yg dimulai dengan 0
    19/08/2021 [VIN/IMS] po service ditampilkan
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptRecvVd : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterByItCt = false, mIsShowForeignName = false, mIsItGrpCodeShow = false;
       
        #endregion

        #region Constructor

        public FrmRptRecvVd(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        private void GetParameter()
        {
           mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
           mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
           mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
        }

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ifnull(E2.DocNo, B.PODocNo) PODocNo, K.ImportInd, H.WhsName, G.VdName, A.VdDONo, E.ItCode, F.ItName, F.ForeignName, B.BatchNo, ");
            SQL.AppendLine("B.QtyPurchase, F.PurchaseUomCode, I.ItCtName, F.ItGrpCode, J.ItGrpName, A.Remark As RemarkH, B.Remark As RemarkD ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblPODtl C On B.PODocNo=C.DocNo And B.PODNo=C.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On D.MaterialRequestDocNo=E.DocNo And D.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Left Join TblMaterialRequestDtl E2 On D.MaterialRequestDocNo=E2.DocNo And D.MaterialRequestDNo=E2.DNo And E2.MaterialRequestServiceDocNo Is Not Null  ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblVendor G On A.VdCode=G.VdCode ");
            SQL.AppendLine("Inner Join TblWarehouse H On A.WhsCode=H.WhsCode ");
            SQL.AppendLine("Inner Join TblItemCategory I On F.ItCtCode=I.ItCtCode ");
            SQL.AppendLine("Left join TblItemgroup J On F.ItGrpCode=J.ItGrpCode ");
            SQL.AppendLine("Inner Join TblPOHdr K On C.DocNo=K.DocNo ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Warehouse",
                        "PO#",
                        "Import",

                        //6-10
                        "Vendor",
                        "Vendor's"+Environment.NewLine+"DO#", 
                        "Item's"+Environment.NewLine+"Code",
                        "",
                        "Group Code",
                        
                        //11-15
                        "Item's Name",
                        "Foreign Name",
                        "Category",
                        "Group",
                        "Batch#",
                        
                        //16-19
                        "Quantity",
                        "UoM",
                        "Document's Remark",
                        "Item's Remark"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 200, 150, 60, 
                        
                        //6-10
                        200, 150, 100, 20, 180, 

                        //11-15
                        200, 150, 200, 150, 200, 
                        
                        //16-19
                        100, 80, 250, 250
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdColButton(Grd1, new int[] { 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 16 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 12, 13 }, false);
            if (mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 12 }, true);
            if (mIsItGrpCodeShow)
                Sm.GrdColInvisible(Grd1, new int[] { 10, 13 }, true);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtVdDONo.Text, "A.VdDONo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                //Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, "B.PODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "F.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, new string[] { "E2.DocNo", "B.PODocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "F.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {

                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "WhsName", "PODocNo", "ImportInd", "VdName", 
                            
                            //6-10
                            "VdDONo", "ItCode", "ItGrpCode", "ItName", "ForeignName", 
                            
                            //11-15
                            "ItCtName", "ItGrpName", "BatchNo", "QtyPurchase", "PurchaseUomCode",

                            //16-17
                            "RemarkH", "RemarkD"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        }, true, true, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo("X", Sm.GetGrdStr(Grd1, e.RowIndex, 8));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                Sm.ShowItemInfo("X", Sm.GetGrdStr(Grd1, e.RowIndex, 8));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 7].Value = "'" + Sm.GetGrdStr(Grd1, Row, 7);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 7].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 7), Sm.GetGrdStr(Grd1, Row, 7).Length - 1);
            }
            Grd1.EndUpdate();
        }

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtVdDONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdDONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor's DO#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion
    }
}
