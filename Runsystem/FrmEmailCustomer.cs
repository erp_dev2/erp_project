﻿#region Update
/*
    [ICA/KIM] New Apps 
    24/05/2021 [ICA/KIM] Menambah Attachment saat kirim email
    03/06/2021 [TKG/KIM] tambah save ke TblSalesInvoiceEmail
 *  10/06/2021 [ICA/KIM] menambah Persero di CompanyName
*/

#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using System.Diagnostics;
using FastReport;
using FastReport.Utils;
using System.IO;
using FastReport.Export.Pdf;
using System.Data;

// email
using System.Net;
using System.Net.Security;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Security.Cryptography.X509Certificates;

#endregion

namespace RunSystem
{
    public partial class FrmEmailCustomer : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";

        private string 
            mFormPrintOutInvoice3 = string.Empty,
            mFormPrintOutInvoice3Receipt = string.Empty, 
            mEmpCodeSI = string.Empty;

        private bool mIsDOCtAmtRounded = false;

        internal FrmEmailCustomerFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmailCustomer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            GetParameter();
            SetGrd();
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Customer's Code",
                    "Customer's Name",
                    "Customer's Email",
                    "Sales Invoice#",
                    "Amount",

                    //6
                    "Remark"
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    130, 300, 150, 150, 130,
 
                    //6
                    300
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6 }, true); ;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    TxtDocNo.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt,
            });
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmailCustomerFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            Sm.SetDteCurrentDate(DteDocDt);
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                SentData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmEmailCustomerDlg(this));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmEmailCustomerDlg(this));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmailCustomer", "TblEmailCustomerHdr");

            cml.Add(SaveDataHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveDataDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsDateNotValid() ||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No Customer in the list.");
                return true;
            }
            return false;
        }

        private bool IsDateNotValid()
        {
            decimal currentdt = Decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8));
            decimal DocDt = Decimal.Parse(Sm.GetDte(DteDocDt).Substring(0, 8));
            if (DocDt < currentdt)
            {
                Sm.StdMsg(mMsgType.Warning, "Date is not valid.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveDataHdr(string DocNo)
        {

            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmailCustomerHdr(DocNo, DocDt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDataDtl(string DocNo, int Row)
        {

            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmailCustomerDtl(DocNo, DNo, CtCode, InvoiceDocNo, Email, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @CtCode, @InvoiceDocNo, @Email, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Email", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@InvoiceDocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowDataHdr(DocNo);
                ShowDataDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDataHdr(string DocNo)
        {
            string CntCode = string.Empty;
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select * From TblEmailCustomerHdr Where DocNo=@DocNo;",
                    new string[] 
                    {
                        //0-1
                        "DocNo", "DocDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    }, true
                );
        }

        private void ShowDataDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode, B.CtName, A.Email, A.InvoiceDocNo, C.Amt, C.Remark ");
            SQL.AppendLine("From TblEmailCustomerDtl A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr C On A.InvoiceDocNo=C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.CtName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "CtCode", 

                    //1-5
                    "CtName", "Email", "InvoiceDocNo", "Amt", "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Send Data

        private void SentData()
        {
            if (Sm.StdMsgYN("Process", string.Empty) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string mSLIDocNo = string.Empty;
            var l = new List<EmailCustomer>();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (mSLIDocNo.Length > 0) mSLIDocNo += ",";
                    mSLIDocNo += Sm.GetGrdStr(Grd1, r, 4);
                }
            }

            if (mSLIDocNo.Length > 0)
            {
                GetData(ref l, mSLIDocNo);
                foreach(var x in l)
                {
                    string FileLocation = PrepareAttachment(x.SLIDocNo);
                    x.Files = FileLocation;
                }

                if (l.Count > 0)
                {
                    ProcessSentEmail(ref l);
                    Sm.StdMsg(mMsgType.Info, "Process is completed.");

                    //Delete File 
                    foreach (var x in l)
                    {
                        FileInfo file = new FileInfo(x.Files);
                        file.Delete();
                    }
                }
            }

            InsertData();
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetData(ref List<EmailCustomer> l, string SLIDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, SUBSTRING(A.DocDt, 5, 2) Month, A.CtCode, B.CtName, GROUP_CONCAT(DISTINCT(C.Email)) Email, B.Address ");
            SQL.AppendLine("FROM tblsalesinvoicehdr A ");
            SQL.AppendLine("INNER JOIN tblcustomer B ON A.CtCode = B.CtCode ");
            SQL.AppendLine("INNER JOIN tblcustomercontactperson C ON B.CtCode = C.CtCode And C.Email Is Not Null And Length(C.Email) > 0 ");
            SQL.AppendLine("Where Find_In_Set(A.DocNo, @SLIDocNo) ");
            SQL.AppendLine("GROUP BY A.DocNo, A.CtCode ");

            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SLIDocNo", SLIDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Month", "CtCode", "CtName", "Email", "Address" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmailCustomer()
                        {
                            SLIDocNo = Sm.DrStr(dr, c[0]),
                            Month = Sm.MonthName(Sm.DrStr(dr, c[1])),
                            CtCode = Sm.DrStr(dr, c[2]),
                            CtName = Sm.DrStr(dr, c[3]),
                            Email = Sm.DrStr(dr, c[4]),
                            Address = Sm.DrStr(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessSentEmail(ref List<EmailCustomer> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                var mBody = new StringBuilder();
                string emailFrom = "penagihan@kim.co.id";
                string mEmailTo = l[i].Email.Replace(";", ",").Replace(" ", string.Empty);
                string[] mEmailTos = mEmailTo.Split(',');
                string mEmailPassword = "kim123***!";

                mBody.AppendLine("<!DOCTYPE html>");
                mBody.AppendLine("<html lang='en'>");
                mBody.AppendLine("<head>");
                mBody.AppendLine("<meta charset='utf-8'>");
                mBody.AppendLine("<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>");
                mBody.AppendLine("</head>");
                mBody.AppendLine("<body>");
                mBody.AppendLine("<div class='container'>");
                mBody.AppendLine("<table>");
                mBody.AppendLine("    <tr>");
                mBody.AppendLine("	    <td rowspan='3'>");
                mBody.AppendLine("		    <img src='http://110.232.78.139/runmobile/assets/img/Logo-KIM.jpg' width=75px />");
                mBody.AppendLine("	    </td>");
                mBody.AppendLine("	    <td>");
                mBody.AppendLine("		    <span style='font-size: 20px; font-weight: bold;'>PT.Kawasan Industri Medan (Persero)</span><br />");
                mBody.AppendLine("	    </td>");
                mBody.AppendLine("    </tr>");
                mBody.AppendLine("</table>");
                mBody.AppendLine("<br />");
                mBody.AppendLine("Kepada Yth <br /> " + l[i].CtName + "<br />" + l[i].Address);
                mBody.AppendLine("<br /><br />");
                mBody.AppendLine("Terlampir kami sampaikan tagihan invoice bulan " + l[i].Month + ". Mohon melakukan pembayaran ke virtual account sesuai yang tertera di invoice. Apabila telah melakukan pembayaran diharapkan konfirmasi melalui email ini.");
                mBody.AppendLine("<br /><br />");
                mBody.AppendLine("Demikian kami sampaikan, terima kasih atas kerjasamanya. ");
                mBody.AppendLine("<br /><br />");
                mBody.AppendLine("Hormat kami, <br /> PT Kawasan Industri Medan (Persero) ");

                foreach (string emailTo in mEmailTos)
                {
                    var cml = new List<MySqlCommand>();
                    MailMessage mail = new MailMessage(emailFrom, emailTo);
                    SmtpClient client = new SmtpClient();
                    client.Port = 587;
                    client.Host = "box6066.bluehost.com";
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(emailFrom, mEmailPassword);
                    client.EnableSsl = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s,
                               X509Certificate certificate,
                               X509Chain chain,
                               SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };

                    string mSubject = "Tagihan PT Kawasan Industri Medan (Persero)";
                    mail.Subject = mSubject;
                    mail.IsBodyHtml = true;
                    mail.Body = mBody.ToString();

                    //attachment 
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(l[i].Files);
                    mail.Attachments.Add(attachment);

                    client.Send(mail);
                    SaveSalesInvoiceEmail(l[i].SLIDocNo);
                    mail.Dispose();
                }
            }
        }

        private void SaveSalesInvoiceEmail(string DocNo)
        {
            var cm = new MySqlCommand() 
            { CommandText = "Insert Into TblSalesInvoiceEmail(DocNo, SendDt) Values(@DocNo, CurrentDateTime()); " };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ExecCommand(cm);
        }

        private string DownloadAttachment(string ReportName, List<IList> TableList, string[] TableName, string SLIDocNo)
        {
            Report report = new Report();
            report.Load(Application.StartupPath + @"\\Report\\rep" + ReportName + ".frx");

            //report.RegisterData(dataset1);
            for (int intX = 0; intX < TableList.Count; intX++)
                report.RegisterData(TableList[intX], TableName[intX]);

            report.Report.Prepare();
            PDFExport pdf = new PDFExport();
            string FileName = SLIDocNo.Replace("/", "-")+".pdf";
            report.Export(pdf, FileName);

            string FileLocation = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\" + FileName;

            return FileLocation;
        }

        private string PrepareAttachment(string DocNo)
        {
            string Doctitle = Sm.GetParameter("Doctitle");
            var l2 = new List<InvoiceHdr>();
            var ldtl = new List<InvoiceDtl>();
            var ldtl2 = new List<InvoiceDtl2>();
            var l3 = new List<Employee>();
            var l4 = new List<EmployeeTaxCollector>();

            string[] TableName = { "InvoiceHdr", "InvoiceDtl", "InvoiceDtl2", "Employee", "EmployeeTaxCollector" };

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header KIM
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();
            SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
            SQL2.AppendLine("A.DocNo, ");
            SQL2.AppendLine("Concat(Right(A.DocDt,2),' ',Case substring(A.DocDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
            SQL2.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
            SQL2.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DocDt,4))As DocDt, ");

            SQL2.AppendLine("Concat(Right(A.DueDt,2),' ',Case substring(A.DueDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
            SQL2.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
            SQL2.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DueDt,4))As DueDt, ");

            SQL2.AppendLine("A.CurCode, A.TotalTax As TotalTax, A.TotalAmt As TotalAmt, A.DownPayment As DownPayment, ");
            SQL2.AppendLine("B.CtName, B.Address, D.SAName, D.SAAddress, D.Remark, ");
            SQL2.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1, ");
            SQL2.AppendLine("IfNull(A.TaxCode2, null) As TaxCode2, ");
            SQL2.AppendLine("IfNull(A.TaxCode3, null) As TaxCode3,");

            SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
            SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
            SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxName3, ");
            SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxRate1, ");
            SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxRate2, ");
            SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxRate3, ");
            SQL2.AppendLine("A.Amt, E.CityName, ");
            SQL2.AppendLine("F.CityName As SACityName, B.NPWP, A.Remark As RemarkSI, A.ReceiptNo, ");
            SQL2.AppendLine("G.ParValue As IsDOCtAmtRounded, ");
            SQL2.AppendLine("B.BankVirtualAccount BankVirtualAccountBNI, H.BankACNo BankVirtualAccountMandiri ");

            SQL2.AppendLine("From TblSalesInvoiceHdr A ");
            SQL2.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL2.AppendLine("Inner join ( ");
            SQL2.AppendLine("		Select A.DocNo, B.DOCtDocNo ");
            SQL2.AppendLine("		From TblSalesInvoiceHdr A ");
            SQL2.AppendLine("		Inner join TblSalesInvoicedtl B On A.DocNo=B.DocNo ");
            SQL2.AppendLine("		group by A.DocNo ");
            SQL2.AppendLine("	) C On A.DocNo=C.DocNo ");
            SQL2.AppendLine("Inner Join TblDoctHdr D On C.DOCtDocNo= D.DocNo ");
            SQL2.AppendLine("Left Join TblCity E On B.CityCode= E.CityCode ");
            SQL2.AppendLine("Left Join TblCity F On D.SACityCode= F.CityCode ");
            SQL2.AppendLine("Left Join TblParameter G On G.ParCode = 'IsDOCtAmtRounded' ");
            SQL2.AppendLine("Left Join TblCustomerBankVirtualAccount H On H.CtCode = B.CtCode And H.BankCode = '008' ");
            SQL2.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                {
                //0
                 "CompanyLogo",

                 //1-5
                 "CompanyName",
                 "CompanyAddress",
                 "CompanyAddressFull",
                 "CompanyPhone",
                 "CompanyFax",

                 //6-10
                 "DocNo",
                 "DocDt",
                 "DueDt",
                 "CurCode",
                 "TotalTax",

                 //11-15
                 "TotalAmt",
                 "DownPayment", 
                 "CtName",
                 "Address",
                 "SAName",

                 //16-20
                 "SAAddress",
                 "Remark",
                 "TaxCode1",
                 "TaxCode2",
                 "TaxCode3",

                 //21-25
                 "TaxName1",
                 "TaxName2",
                 "Taxname3",
                 "TaxRate1",
                 "TaxRate2",

                 //26-30
                 "TaxRate3",
                 "Amt",
                 "CityName",
                 "SACityName",
                 "NPWP",

                 //31-35
                 "RemarkSI",
                 "ReceiptNo",
                 "IsDOCtAmtRounded",
                 "BankVirtualAccountBNI",
                 "BankVirtualAccountMandiri"

                });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new InvoiceHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr2, c2[0]),

                            CompanyName = Sm.DrStr(dr2, c2[1]),
                            CompanyAddress = Sm.DrStr(dr2, c2[2]),
                            CompanyAddressFull = Sm.DrStr(dr2, c2[3]),
                            CompanyPhone = Sm.DrStr(dr2, c2[4]),
                            CompanyFax = Sm.DrStr(dr2, c2[5]),

                            DocNo = Sm.DrStr(dr2, c2[6]),
                            DocDt = Sm.DrStr(dr2, c2[7]),
                            DueDt = Sm.DrStr(dr2, c2[8]),
                            CurCode = Sm.DrStr(dr2, c2[9]),
                            TotalTax = Sm.DrDec(dr2, c2[10]),

                            TotalAmt = Sm.DrDec(dr2, c2[11]),
                            DownPayment = Sm.DrDec(dr2, c2[12]),
                            CtName = Sm.DrStr(dr2, c2[13]),
                            Address = Sm.DrStr(dr2, c2[14]),
                            SAName = Sm.DrStr(dr2, c2[15]),

                            SAAddress = Sm.DrStr(dr2, c2[16]),
                            Remark = Sm.DrStr(dr2, c2[17]),
                            TaxCode1 = Sm.DrStr(dr2, c2[18]),
                            TaxCode2 = Sm.DrStr(dr2, c2[19]),
                            TaxCode3 = Sm.DrStr(dr2, c2[20]),

                            TaxName1 = Sm.DrStr(dr2, c2[21]),
                            TaxName2 = Sm.DrStr(dr2, c2[22]),
                            TaxName3 = Sm.DrStr(dr2, c2[23]),
                            TaxRate1 = Sm.DrDec(dr2, c2[24]),
                            TaxRate2 = Sm.DrDec(dr2, c2[25]),

                            TaxRate3 = Sm.DrDec(dr2, c2[26]),
                            Amt = Sm.DrDec(dr2, c2[27]),
                            //Decimal.Parse(TxtAmt.Text), //
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr2, c2[27])),
                            //Sm.Terbilang(Decimal.Parse(TxtAmt.Text)), //
                            CityName = Sm.DrStr(dr2, c2[28]),
                            SACityName = Sm.DrStr(dr2, c2[29]),
                            NPWP = Sm.DrStr(dr2, c2[30]),

                            RemarkSI = Sm.DrStr(dr2, c2[31]),
                            ReceiptNo = Sm.DrStr(dr2, c2[32]),
                            IsDOCtAmtRounded = Sm.DrStr(dr2, c2[33]),
                            BankVirtualAccountBNI = Sm.DrStr(dr2, c2[34]),
                            BankVirtualAccountMandiri = Sm.DrStr(dr2, c2[35]),
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.ItCode, ");
                if (Doctitle == "KIM")
                    SQLDtl.AppendLine("IfNull(B.ForeignName, B.ItName) As ItName, ");
                else
                    SQLDtl.AppendLine("B.ItName, ");
                SQLDtl.AppendLine("A.Qty, A.UPriceAfterTax, ");
                if (mIsDOCtAmtRounded)
                    SQLDtl.AppendLine("Floor(A.Qty*A.UPriceAfterTax) As Amt, ");
                else
                    SQLDtl.AppendLine("A.Qty*A.UPriceAfterTax As Amt, ");
                SQLDtl.AppendLine("B.SalesuomCode As PriceUomCode, C.Remark ");
                SQLDtl.AppendLine("From TblSalesInvoiceDtl A ");
                SQLDtl.AppendLine("Inner join tblitem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine("Inner Join TblDOCthdr C On A.DOCtDocNo = C.DocNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    //0
                    "ItCode" ,

                    //1-5
                    "ItName" ,
                    "Qty",
                    "UPriceAfterTax",
                    "Amt",
                    "PriceUomCode",

                    "Remark"

                });

                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new InvoiceDtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            UPriceAfterTax = Sm.DrDec(drDtl, cDtl[3]),
                            Amt = Sm.DrDec(drDtl, cDtl[4]),
                            PriceUomCode = Sm.DrStr(drDtl, cDtl[5]),

                            Remark = Sm.DrStr(drDtl, cDtl[6]),
                        });
                    }
                }

                drDtl.Close();
            }

            myLists.Add(ldtl);

            #endregion

            #region Detail 2

            var cmDtl2 = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                SQLDtl2.AppendLine("Select A.DocNo, ifnull(sum(B.DAmt),0)As DAmt, ifnull(sum(B.CAmt),0) As CAmt, ifnull(sum(B.DAmt),0)+ ifnull(sum(B.CAmt),0) As TAmt ");
                SQLDtl2.AppendLine("From tblsalesinvoicehdr A ");
                SQLDtl2.AppendLine("Left join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo And B.AcInd='Y' ");
                SQLDtl2.AppendLine("Group by B.Docno ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", DocNo);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                {
                    //0
                    "DocNo" ,

                    //1-2
                    "DAmt" ,
                    "CAmt",
                    "TAmt",

                });

                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new InvoiceDtl2()
                        {
                            DocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                            DAmt = Sm.DrDec(drDtl2, cDtl2[1]),
                            CAmt = Sm.DrDec(drDtl2, cDtl2[2]),
                            TAmt = Sm.DrDec(drDtl2, cDtl2[3]),
                        });
                    }
                }

                drDtl2.Close();
            }

            myLists.Add(ldtl2);

            #endregion

            #region Signature KIM
            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();

            SQL3.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
            SQL3.AppendLine("From TblEmployee A ");
            SQL3.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
            SQL3.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL3.AppendLine("Where A.EmpCode=@EmpCode ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@EmpCode", mEmpCodeSI);
                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                {
                 //0-3
                 "EmpCode",
                 "EmpName",
                 "PosName",
                 "EmpPict"
                
                });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr3, c3[0]),

                            EmpName = Sm.DrStr(dr3, c3[1]),
                            Position = Sm.DrStr(dr3, c3[2]),
                            EmpPict = Sm.DrStr(dr3, c3[3]),
                        });
                    }
                }
                dr3.Close();
            }
            myLists.Add(l3);

            #endregion

            #region Signature2 KIM

            var cm4 = new MySqlCommand();
            var SQL4 = new StringBuilder();
            string SPCode = Sm.GetValue("Select T2.SPCode From TblSalesInvoiceHdr T1 Inner Join TblSalesPerson T2 On T1.SalesName = T2.SPName And T1.DocNo = @Param Limit 1;", DocNo);

            //SQL4.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, A.Mobile ");
            //SQL4.AppendLine("From TblEmployee A ");
            //SQL4.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
            //SQL4.AppendLine("Where A.EmpCode=@EmpCode ");

            SQL4.AppendLine("Select C.EmpCode, C.EmpName, C.Mobile, D.PosName ");
            SQL4.AppendLine("FROM TblSalesPerson A ");
            SQL4.AppendLine("LEFT JOIN TblUser B ON A.UserCode = B.UserCode ");
            SQL4.AppendLine("LEFT JOIN TblEmployee C ON B.UserCode = C.UserCode ");
            SQL4.AppendLine("LEFT JOIN TblPosition D ON C.PosCode = D.PosCode ");
            SQL4.AppendLine("Where A.SPCode = @SPCode; ");

            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQL4.ToString();
                //Sm.CmParam<String>(ref cm4, "@EmpCode", mEmpCodeTaxCollector);
                Sm.CmParam<String>(ref cm4, "@SPCode", SPCode);
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                {
                 //0-3
                 "EmpCode",
                 "EmpName",
                 "PosName",
                 "Mobile"
                
                });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        l4.Add(new EmployeeTaxCollector()
                        {
                            EmpCode = Sm.DrStr(dr4, c4[0]),

                            EmpName = Sm.DrStr(dr4, c4[1]),
                            Position = Sm.DrStr(dr4, c4[2]),
                            Mobile = Sm.DrStr(dr4, c4[3]),
                        });
                    }
                }
                dr4.Close();
            }
            myLists.Add(l4);

            #endregion

            //Sm.PrintReport(mFormPrintOutInvoice3, myLists, TableName, false);

            return DownloadAttachment(mFormPrintOutInvoice3, myLists, TableName, DocNo);
        }

        private void GetParameter()
        {
            //mEmpCodeSI = Sm.GetParameter("EmpCodeSI");
            //mFormPrintOutInvoice3 = Sm.GetParameter("FormPrintOutInvoice3");
            //mFormPrintOutInvoice3Receipt = Sm.GetParameter("FormPrintOutInvoice3Receipt");
            //mIsDOCtAmtRounded = Sm.GetParameterBoo("IsDOCtAmtRounded");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'EmpCodeSI', 'FormPrintOutInvoice3', 'FormPrintOutInvoice3Receipt', 'IsDOCtAmtRounded' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            
                            //string
                            case "EmpCodeSI": mEmpCodeSI = ParValue; break;
                            case "FormPrintOutInvoice3": mFormPrintOutInvoice3 = ParValue; break;
                            case "FormPrintOutInvoice3Receipt": mFormPrintOutInvoice3Receipt = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Class

        private class EmailCustomer
        {
            public string SLIDocNo { get; set; }
            public string CtCode { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string Email { get; set; }
            public string Month { get; set; }
            public string Files { get; set; }
        }

        #region Report Class

        private class InvoiceHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public decimal TotalTax { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal DownPayment { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string Remark { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string SACityName { get; set; }
            public string NPWP { get; set; }
            public string RemarkSI { get; set; }
            public string ReceiptNo { get; set; }
            public string IsDOCtAmtRounded { get; set; }
            public string BankVirtualAccountBNI { get; set; }
            public string BankVirtualAccountMandiri { get; set; }
        }

        class InvoiceDtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public decimal UPriceAfterTax { get; set; }
            public decimal Amt { get; set; }
            public string PriceUomCode { get; set; }
            public string Remark { get; set; }
        }

        class InvoiceDtl2
        {
            public string DocNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal TAmt { get; set; }
        }

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string EmpPict { get; set; }
        }

        private class EmployeeTaxCollector
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Mobile { get; set; }
        }

        #endregion

        #endregion
    }
}
