﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmWorkSchedule2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmWorkSchedule2 mFrmParent;

        #endregion

        #region Constructor

        public FrmWorkSchedule2Find(FrmWorkSchedule2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 45;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "D1-Holiday",
                        "D1-Check In",

                        //6-10
                        "D1-Check Out",
                        "D1-Break In",
                        "D1-Break Out",
                        "D2-Holiday",
                        "D2-Check In",
                        
                        //11-15
                        "D2-Check Out",
                        "D2-Break In",
                        "D2-Break Out",
                        "D3-Holiday",
                        "D3-Check In",
                        
                        //16-20
                        "D3-Check Out",
                        "D3-Break In",
                        "D3-Break Out",
                        "D4-Holiday",
                        "D4-Check In",
                        
                        //21-25
                        "D4-Check Out",
                        "D4-Break In",
                        "D4-Break Out",
                        "D5-Holiday",
                        "D5-Check In",
                        
                        //26-30
                        "D5-Check Out",
                        "D5-Break In",
                        "D5-Break Out",
                        "D6-Holiday",
                        "D6-Check In",
                        
                        //31-35
                        "D6-Check Out",
                        "D6-Break In",
                        "D6-Break Out",
                        "D7-Holiday",
                        "D7-Check In",
                        
                        //36-40
                        "D7-Check Out",
                        "D7-Break In",
                        "D7-Break Out",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        
                        //41-44
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 200, 80, 100, 100, 
                        
                        //6-10
                        100, 100, 100, 100, 100,
                        
                        //11-15
                        100, 100, 100, 100, 100,

                        //16-20
                        100, 100, 100, 100, 100,

                        //21-25
                        100, 100, 100, 100, 100,

                        //26-30
                        100, 100, 100, 100, 100,

                        //31-35
                        100, 100, 100, 100, 100,

                        //36-40
                        100, 100, 100, 100, 100,

                        //41-44
                        100, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 4, 9, 14, 19, 24, 29, 34 });
            Sm.GrdFormatDate(Grd1, new int[] { 40, 43 });
            Sm.GrdFormatTime(Grd1, new int[] { 
                5, 6, 7, 8, 
                10, 11, 12, 13, 
                15, 16, 17, 18, 
                20, 21, 22, 23, 
                25, 26, 27, 28, 
                30, 31, 32, 33,
                35, 36, 37, 38,
                41, 44 });
            Sm.GrdColInvisible(Grd1, new int[] { 39, 40, 41, 42, 43, 44 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 39, 40, 41, 42, 43, 44 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtWSCode.Text, new string[] { "WSCode", "WSName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select * From TblWorkSchedule2 " + Filter + " Order By WSName;",
                        new string[]
                        {
                            "WSCode", "WSName", "ActInd", 
                            
                            "D1HolidayInd", "D1In1", "D1Out1", "D1In2", "D1Out2", 

                            "D2HolidayInd", "D2In1", "D2Out1", "D2In2", "D2Out2", 

                            "D3HolidayInd", "D3In1", "D3Out1", "D3In2", "D3Out2", 
                            
                            "D4HolidayInd", "D4In1", "D4Out1", "D4In2", "D4Out2", 
                            
                            "D5HolidayInd", "D5In1", "D5Out1", "D5In2", "D5Out2", 
                            
                            "D6HolidayInd", "D6In1", "D6Out1", "D6In2", "D6Out2", 
                            
                            "D7HolidayInd", "D7In1", "D7Out1", "D7In2", "D7Out2", 
                            
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);

                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 8, 7);

                            Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 13, 12);

                            Sm.SetGrdValue("B", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 18, 17);

                            Sm.SetGrdValue("B", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 23, 22);

                            Sm.SetGrdValue("B", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 28, 27);

                            Sm.SetGrdValue("B", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 32, 31);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 33, 32);

                            Sm.SetGrdValue("B", Grd, dr, c, Row, 34, 33);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 35, 34);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 36, 35);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 37, 36);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 38, 37);
                            
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 38);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 40, 39);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 41, 39);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 40);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 43, 41);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 44, 41);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Schedule Event

        private void TxtWSCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWSCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work schedule");
        }

        #endregion

        #endregion
    }
}
