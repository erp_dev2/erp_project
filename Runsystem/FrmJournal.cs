﻿#region Update
/*
    30/05/2017 [TKG] tambah remark
    06/07/2017 [HAR] tambah entity berdasar kanparameter di grid nya
    13/07/2017 [TKG] tambah mDocNo agar journal bisa dibuka di aplikasi lain
    08/11/2017 [ARI] tambah printout
    17/07/2018 [TKG] Berdasarkan IsJournalCostCenterEnabled, informasi cost center akan ditampilkan atau tidak.
    02/08/2018 [TKG] Label cost center tetap muncul walaupun parameter tidak menggunakan cost center
    18/09/2018 [TKG] ubah validasi balance
    13/03/2019 [TKG] SaveJournalHdr DocDt kurang tanda '@'
    15/10/2019 [WED/IMS] filter COA Alias
    25/11/2019 [TKG/TWC] tambah fasilitas cancel
    16/01/2020 [VIN/KBN] Printout Journal Transaction
    23/01/2020 [HAR/KBN] BUG Printout Journal Transaction, kurang filter @DocNo di header
    27/02/2020 [WED/KBN] COA bisa dipilih berkali kali berdasarkan parameter IsJournalUseDuplicateCOA
    05/03/2020 [DITA/KBN] Penomoran dokumen 6 digit, reset tiap tahun, dan berdasarkan entity
    19/03/2020 [HAR/KBN] bug printout convert to words untuk nilai 10 tidak muncul
    20/04/2020 [IBL] Menambahkan periode di header
    19/05/2020 [DITA/YK] Filter COA berdasarkan group --> param = IsCOAFilteredByGroup
    07/12/2020 [DITA/IMS] tambah SO Contract di header journal
    03/02/2021 [TKG/PHT] Berdasarkan parameter IsJournalFilterByGroupCC, cost center difilter berdasarkan cost center group
    23/02/2021 [TKG/PHT] mempercepat proses saat show data
    01/03/2021 [WED/IMS] remark detail tidak di hide saat show berdasarkan parameter IsJournalShowRemarkDetail
    05/03/2021 [RDH/PHT] Setting cost center child di journal transaction
    08/03/2021 [RDH/PHT] Membuat parameter untuk memandatory kan cost center di Jurnal Transaction 
    20/05/2021 [TKG/PHT] divalidasi menggunakan otorisasi group thd profit center dan cost center yg muncul adalah cost center yg paling akhir
    08/06/2021 [TKG/PHT] saat cancel journal, informasi cost center tetap diisi dari journal saat diinsert sebelumnya.
    09/06/2021 [BRI/PHT] Ketika Cancel JT ada Cost Center
    30/06/2021 [TKG/PHT] ubah proses save journal, ubah validasi balance, ubah GetParameter(), ubah query untuk cost center berdasarkan profit center, menggunakan parameter IsJournalPeriodEnabled untuk mengaktifkan period, merubah tampilan journal.
    09/09/2021 [MYA/AMKA] Penyesuian Printout Journal Transaction VR Manual
    23/09/2021 [MYA/AMKA] Penyesuian Printout Journal Transaction VR Manual
    13/10/2021 [ISD/AMKA] Penyesuaian Printout Journal Transaction
    15/10/2021 [MYA/ALL] Memasang Parameter agar Journal Transaction tidak bisa menarik akun-akun KAS/BANK
    28/10/2021 [MYA/ALL] Validasi cancel journal transaksi hanya bisa cancel untuk yang menu descriptionnya journal transaksi, kalau yang otomatis2 tidak bisa dicancel dari menu JT
    01/11/2021 [MYA/AMKA] BUG : Muncul warning saat akan print Journal Transaction
    11/11/2021 [MYA/AMKA] BUG : Muncul warning saat akan print Journal Transaction
    11/11/2021 [ISD/AMKA] Membuat field cost center terfilter berdasarkan group user dengan parameter IsJournalTransactionCCFilteredByGroup
    30/11/2021 [TRI/AMKA] BUG : Muncul warning saat print Journal Transaction dengan description Voucher (AP Downpayment)
    06/01/2022 [TKG/GSS] ubah GetParameter dan proses save
    07/01/2022 [DITA/YK] lup button so contract belum mengarah ke socontract nya YK
    19/02/2022 [TKG/PHT] masih bisa tersimpan walaupun tidak balance
    03/03/2022 [TKG/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    23/11/2022 [BRI/TWC] Period melihat closing date
    10/01/2023 [RDA/PHT] perubahan generate journal docno menggunakan Sm.GetNewJournalDocNoWithAddCodes (khusus PHT)
    28/02/2023 [SET/PHT] penyesuaian cancel journal source Exchange Journal
    02/03/2023 [BRI/VIR] bug field Cost Center
    21/03/2023 [WED/PHT] nomor journal untuk tipe monthly tidak sesuai rules baru
    24/03/2023 [BRI/PHT] bug validasi amount
    25/03/2023 [BRI/PHT] bug tidak bisa save
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmJournal : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mJournalCashBankAcNo = string.Empty; //if this application is called from other application;
        internal FrmJournalFind FrmFind;
        internal bool 
            mIsJournalCostCenterEnabled = false,
            mIsCOAUseAlias = false,
            mIsJournalUseDuplicateCOA = false,
            mIsCOAFilteredByGroup = false,
            mIsJournalUseSOContract = false,
            mIsJournalFilterByGroupCC = false,
            mIsJournalShowRemarkDetail = false,
            mIsCostCenterShowChildOnly = false,
            mIsJournalUseProfitCenter = false,
            mIsVRApprovalByType = false,
            mIsJournalTransactionCCFilteredByGroup = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false;
        private string 
            mMainCurCode = string.Empty,
            mFormPrintOutJournal = string.Empty,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mJournalDocNoFormat = "1";
        private bool mIsEntityMandatory = false, mIsAutoJournalActived = false, mIsJournalPeriodEnabled = false;
        iGCell fCell;
        bool fAccept;
        private string[] mSeqNo = new string[100];

        #endregion

        #region Constructor

        public FrmJournal(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Journal Transaction";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                mSeqNo[0] = null;
                for (int i = 1; i < 100; ++i)
                {
                    mSeqNo[i] = Sm.Right(string.Concat("00", (i).ToString()), 2);
                }
                Sl.SetLueCurCode(ref LueCurCode);
                LueEntCode.Visible = false;
                LueCCCode.Visible = mIsJournalCostCenterEnabled;
                LblCCCode.Visible = mIsJournalCostCenterEnabled;
                if (!mIsJournalUseSOContract)
                {
                    LblSOContract.Visible = false;
                    TxtSOContractDocNo.Visible = false;
                    BtnSOContractDocNo.Visible = false;
                    BtnSOContractDocNo2.Visible = false;
                }
                if (mIsJournalPeriodEnabled)
                {
                    LblPeriod.Visible = true;
                    TxtPeriod.Visible = true;
                }

                SetGrd();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Alias",
                        "Description",
                        "Entity's Code",
                        "Entity's Name",

                        //6-8
                        "Debit",
                        "Credit",
                        "Remark",
                    },
                     new int[] 
                    {
                        20,
                        150, 100, 400, 100, 200, 
                        130, 130, 300
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            if (!mIsJournalShowRemarkDetail)
                Sm.GrdColInvisible(Grd1, new int[] { 8 });
            Grd1.Cols[2].Visible = mIsCOAUseAlias;
        }

        override protected void HideInfoInGrd()
        {
            if (!mIsJournalShowRemarkDetail)
                Sm.GrdColInvisible(Grd1, new int[] { 8 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeJnDesc, MeeRemark, LueCCCode, MeeCancelReason, ChkCancelInd, TxtPeriod }, true);
                    BtnSOContractDocNo2.Enabled = true;
                    BtnSOContractDocNo.Enabled = false;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeJnDesc, MeeRemark, LueCCCode, TxtPeriod }, false);
                    BtnSOContractDocNo.Enabled = true;
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDocNo, DteDocDt, MeeJnDesc, MeeRemark, LueCurCode, 
                TxtMenuCode, TxtMenuDesc, LueCCCode, MeeCancelReason,
                TxtPeriod, TxtSOContractDocNo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtDbt, TxtCdt, TxtBalanced }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmJournalFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
                if (mIsJournalUseProfitCenter)
                    Sl.SetLueCCCodeFilterByProfitCenter(ref LueCCCode, string.Empty, "N");
                else
                    Sl.SetLueCCCode(ref LueCCCode, string.Empty, mIsJournalTransactionCCFilteredByGroup ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mEntCode = Sm.GetValue(
                    "Select B.EntCode From TblCostCenter A " +
                    "Inner Join TblProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode " +
                    "And A.CCCode= @Param; ", Sm.GetLue(LueCCCode));

                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Nol",
                "Satu",
                "Dua",
                "Tiga",
                "Empat",
                "Lima",
                "Enam",
                "Tujuh",
                "Delapan",
                "Sembilan"
            };

        private static string[] _teens =
            {
                "Sepuluh",
                "Sebelas",
                "Dua Belas",
                "Tiga Belas",
                "Empat Belas",
                "Lima Belas",
                "Enam Belas",
                "Tujuh Belas",
                "Delapan Belas",
                "Sembilan Belas"
                
            };

        private static string[] _tens =
            {
                "Sepuluh",
                "Sebelas",
                "Dua Puluh",
                "Tiga Puluh",
                "Empat Puluh",
                "Lima Puluh",
                "Enam Puluh",
                "Tujuh Puluh",
                "Delapan Puluh",
                "Sembilan Puluh"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Ribu",
                "Juta",
                "Miliar"
            };



        private static string Convert2(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Ratus ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang(cettt);
            builder.AppendFormat("Rupiah" , (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string Doctitle = Sm.GetParameter("DocTitle");
            string[] TableName = { "JournalHdr", "JournalDtl", "JournalSignAMKA", "JournalHdrKBN" };
            string mMenuCodeJournal = Sm.GetValue("Select MenuCode From TblJournalHdr Where DocNo = @Param", TxtDocNo.Text);
            string mDeptNameQuery = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'DeptCodeJournalTransaction' And OptCode = @Param", mMenuCodeJournal);
            bool IsVoucher = true;
            string DocType = Sm.GetValue(
                "SELECT T1.DocType " +
                "FROM( " +
                "SELECT C.DocType, D.OptDesc " +
                "FROM tbljournalhdr A " +
                "INNER JOIN TblVoucherJournalHdr B ON A.DocNo = B.JournalDocNo " +
                "INNER JOIN tblvoucherhdr C ON B.DocNo = C.VoucherJournalDocNo " +
                "INNER JOIN tbloption D ON C.DocType = D.OptCode AND D.OptCat = 'VoucherDocType' " +
                "WHERE A.DocNo = '" + TxtDocNo.Text + "' " +
                "UNION ALL " +
                "SELECT C.DocType, D.OptDesc " +
                "FROM tbljournalhdr A " +
                "INNER JOIN tblvoucherhdr C ON A.DocNo = C.JournalDocNo " +
                "INNER JOIN tbloption D ON C.DocType = D.OptCode AND D.OptCat = 'VoucherDocType' " +
                "WHERE A.DocNo = '" + TxtDocNo.Text + "' " +
                ") T1 " +
                "LIMIT 1; " 
            );
            string mDeptNameJournal = string.Empty;

            if (mDeptNameQuery.Length != 0) mDeptNameJournal = Sm.GetValue(mDeptNameQuery, TxtDocNo.Text); 

            var l = new List<JournalHdr>();
            var ldtl = new List<JournalDtl>();
            var ldtl2 = new List<JournalDtlAMKA>();
            var lSignAMKA = new List<JournalSignAMKA>();
            var l2 = new List<JournalHdrKBN>();
            

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y')As DocDt, A.JnDesc, A.CurCode, A.MenuDesc, A.Remark, Group_Concat(Distinct(C.EntName)) EntName, D.CCName ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblEntity C On B.EntCode = C.EntCode ");
            SQL.AppendLine("Left Join TblCostCenter D ON A.CCCode = D.CCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo", 
                        
                         //6-10
                         "DocDt", 
                         "JnDesc",
                         "CurCode", 
                         "MenuDesc", 
                         "Remark",

                         //11-12
                         "EntName",
                         "CCName",
                        
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new JournalHdr() 
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            JnDesc = Sm.DrStr(dr, c[7]),
                            CurCode = Sm.DrStr(dr, c[8]),
                            MenuDesc = Sm.DrStr(dr, c[9]),
                            Remark = Sm.DrStr(dr, c[10]),
                            EntName = Sm.DrStr(dr, c[11]),
                            CCName = Sm.DrStr(dr, c[12]),

                            AutoJournal = Sm.GetParameterBoo("IsAutoJournalActived"),
                            DocType = DocType,
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            if (Doctitle == "AMKA" && IsVoucher == true)
            {
                #region Detail AMKA

                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();

                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("SELECT A.*  ");
                    SQLDtl2.AppendLine("FROM (  ");
                    SQLDtl2.AppendLine("	SELECT A.DocNo, E.BankAcNo, A.JnDesc, B.CAmt, B.DAmt, D.DocType, B.ACNo, F.ACDesc ");
                    SQLDtl2.AppendLine("	FROM TblJournalHdr A  ");
                    SQLDtl2.AppendLine("	INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo   ");
                    SQLDtl2.AppendLine("	INNER JOIN TblVoucherJournalHdr C ON A.DocNo = C.JournalDocNo   ");
                    SQLDtl2.AppendLine("	INNER JOIN TblVoucherHdr D ON C.DocNo = D.VoucherJournalDocNo  ");
                    SQLDtl2.AppendLine("	Left JOIN TblBankAccount E ON D.BankAcCode = E.BankAcCode AND B.AcNo = E.COAAcNo AND E.BankAcNo IS NOT NUll  ");
                    SQLDtl2.AppendLine("    Inner Join TblCOA F On B.AcNo = F.ACno ");
                    SQLDtl2.AppendLine("	UNION ALL  ");
                    SQLDtl2.AppendLine("	SELECT A.DocNo, E.BankAcNo, A.JnDesc, B.CAmt, B.DAmt, D.DocType, B.ACNo, F.ACDesc ");
                    SQLDtl2.AppendLine("	FROM TblJournalHdr A  ");
                    SQLDtl2.AppendLine("	INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo  ");
                    SQLDtl2.AppendLine("	INNER JOIN TblVoucherHdr D ON A.DocNo = D.JournalDocNo  ");
                    SQLDtl2.AppendLine("	Left JOIN TblBankAccount E ON D.BankAcCode = E.BankAcCode AND B.AcNo = E.COAAcNo AND E.BankAcNo IS NOT NULL  ");
                    SQLDtl2.AppendLine("    Inner Join TblCOA F On B.AcNo = F.ACno ");
                    SQLDtl2.AppendLine(")A   ");
                    SQLDtl2.AppendLine("WHERE A.DocNo = @DocNo Order By CAmt ASC;  ");

                    cmDtl2.CommandText = SQLDtl2.ToString();
                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl2.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-5
                         "BankAcNo",
                         "JnDesc",
                         "DAmt",
                         "CAmt",
                         "ACNo",
                         "ACDesc"

                        });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl2.Add(new JournalDtlAMKA()
                            {
                                DocNo = Sm.DrStr(drDtl, cDtl[0]),
                                BankAcNo = Sm.DrStr(drDtl, cDtl[1]),
                                JnDesc = Sm.DrStr(drDtl, cDtl[2]),
                                DAmt = Sm.DrDec(drDtl, cDtl[3]),
                                CAmt = Sm.DrDec(drDtl, cDtl[4]),
                                ACNo = Sm.DrStr(drDtl, cDtl[5]),
                                ACDesc = Sm.DrStr(drDtl, cDtl[6])
                            });
                        }
                    }
                    else
                        IsVoucher = false;

                    drDtl.Close();
                }
                if (IsVoucher == true) myLists.Add(ldtl2);

                #endregion
            }
            if ((Doctitle == "AMKA" && IsVoucher == false) || Doctitle != "AMKA")
            {
                #region Detail

                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();

                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select A.Dno, A.ACNo, B.Alias, B.ACDesc, C.EntName, A.DAmt, A.CAmt, A.Remark  ");
                    SQLDtl.AppendLine("From TblJournalDtl A ");
                    SQLDtl.AppendLine("Inner Join TblCOA B On A.AcNo = B.ACno ");
                    SQLDtl.AppendLine("Left Join TblEntity C On A.EntCode = C.EntCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo Order By CAmt Asc, A.AcNo;");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo",

                         //1-5
                         "ACNo",
                         "Alias",
                         "ACDesc",
                         "EntName",
                         "DAmt",

                         //6-7
                         "CAmt",
                         "Remark"

                        });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new JournalDtl()
                            {
                                DNo = Sm.DrStr(drDtl, cDtl[0]),
                                ACNo = Sm.DrStr(drDtl, cDtl[1]),
                                Alias = Sm.DrStr(drDtl, cDtl[2]),
                                ACDesc = Sm.DrStr(drDtl, cDtl[3]),
                                EntName = Sm.DrStr(drDtl, cDtl[4]),
                                DAmt = Sm.DrDec(drDtl, cDtl[5]),
                                CAmt = Sm.DrDec(drDtl, cDtl[6]),
                                Remark = Sm.DrStr(drDtl, cDtl[7])
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);

                #endregion
            }

            #region Signature AMKA

                var cm4 = new MySqlCommand();
                var SQL4 = new StringBuilder();

                if (DocType.Length > 0)
                {

                    SQL4.AppendLine("SELECT T1.EmpPict1, T1.UserName1, Description1, ApproveDt1, T2.EmpPict2, T2.UserName2, T2.Description2, T2.ApproveDt2, ");
                    SQL4.AppendLine("T3.EmpPict3, T3.UserName3, T3.Description3, T3.ApproveDt3, T4.EmpPict4, T4.UserName4, T4.Description4, T4.ApproveDt4 ");
                    SQL4.AppendLine("FROM ");

                    if (mIsVRApprovalByType)
                    {

                        if (DocType == "05" || DocType == "20")
                        {
                            SQL4.AppendLine(GetApproval("TblARDownpayment", "ARDownpayment", "2"));
                        }

                        else if(DocType == "04" || DocType == "19")
                        {
                            SQL4.AppendLine(GetApproval("TblAPDownpayment", "APDownpayment", "2"));
                        }

                        else if (DocType == "02" || DocType == "18")
                        {
                            SQL4.AppendLine(GetApproval("TblIncomingPaymentHdr", "IncomingPayment", "2"));
                        }

                        else if (DocType == "03" || DocType == "17")
                        {
                            SQL4.AppendLine(GetApproval("TblOutgoingPaymentHdr", "OutgoingPayment", "2"));
                        }

                        else if (DocType == "01")
                        {
                            SQL4.AppendLine(GetApproval("TblVoucherRequestHdr", "VoucherRequestManual", "1"));
                        }

                        else if (DocType == "56")
                        {
                            SQL4.AppendLine(GetApproval("TblVoucherRequestHdr", "VoucherRequestCashAdvance", "1"));
                        }

                        else if (DocType == "16")
                        {
                            SQL4.AppendLine(GetApproval("TblVoucherRequestHdr", "VoucherRequestSwitchingBankAcc", "1"));
                        }
                        else
                        {
                            SQL4.AppendLine(GetApproval("TblVoucherRequestHdr", "VoucherRequest", "1"));
                            SQL4.AppendLine("Where 1 != 1 ");
                        }

                    }
                    else
                    {
                        SQL4.AppendLine(GetApproval("TblVoucherRequestHdr", "VoucherRequest", "1"));
                    }


                    using (var cn4 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn4.Open();
                        cm4.Connection = cn4;
                        cm4.CommandText = SQL4.ToString();
                        Sm.CmParam<String>(ref cm4, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm4, "@DocType", DocType);
                        var dr4 = cm4.ExecuteReader();
                        var c4 = Sm.GetOrdinal(dr4, new string[] 
                {
                    //0
                    "EmpPict1",
                    "Description1",
                    "Username1",
                    "ApproveDt1",

                    "EmpPict2",
                    "Description2",
                    "Username2",
                    "ApproveDt2",

                    "EmpPict3",
                    "Description3",
                    "Username3",
                    "ApproveDt3",

                    "EmpPict4",
                    "Description4",
                    "Username4",
                    "ApproveDt4",
                });
                        if (dr4.HasRows)
                        {
                            while (dr4.Read())
                            {
                                lSignAMKA.Add(new JournalSignAMKA()
                                {
                                    EmpPict1 = Sm.DrStr(dr4, c4[0]),
                                    Description1 = Sm.DrStr(dr4, c4[1]),
                                    UserName1 = Sm.DrStr(dr4, c4[2]),
                                    ApproveDt1 = Sm.DrStr(dr4, c4[3]),
                                    EmpPict2 = Sm.DrStr(dr4, c4[4]),
                                    Description2 = Sm.DrStr(dr4, c4[5]),
                                    UserName2 = Sm.DrStr(dr4, c4[6]),
                                    ApproveDt2 = Sm.DrStr(dr4, c4[7]),
                                    EmpPict3 = Sm.DrStr(dr4, c4[8]),
                                    Description3 = Sm.DrStr(dr4, c4[9]),
                                    UserName3 = Sm.DrStr(dr4, c4[10]),
                                    ApproveDt3 = Sm.DrStr(dr4, c4[11]),
                                    EmpPict4 = Sm.DrStr(dr4, c4[12]),
                                    Description4 = Sm.DrStr(dr4, c4[13]),
                                    UserName4 = Sm.DrStr(dr4, c4[14]),
                                    ApproveDt4 = Sm.DrStr(dr4, c4[15]),
                                });
                            }
                        }
                        dr4.Close();
                    }
                    myLists.Add(lSignAMKA);
                }
            

            #endregion

            #region Header KBN

            if (Sm.GetParameter("DocTitle") == "KBN")
            {
                var cm2 = new MySqlCommand();
                var SQL2 = new StringBuilder();

                SQL2.AppendLine("SELECT A.DocNo, DATE_FORMAT(A.DocDt, '%d/%m/%Y') DocDt, A.JnDesc, A.Remark, SUM(B.DAmt) Amt ");
                SQL2.AppendLine("FROM TblJournalHdr A ");
                SQL2.AppendLine("INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                SQL2.AppendLine("Where A.DocNo=@DocNo ");
                SQL2.AppendLine("GROUP BY A.DocNo, A.DocDt, A.JnDesc, A.Remark; ");

                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();

                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-4
                         "DocDt",
                         "JnDesc",
                         "Remark",
                         "Amt",
                         
                        
                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            l2.Add(new JournalHdrKBN()
                            {
                                DocNo = Sm.DrStr(dr2, c2[0]),

                                DocDt = Sm.DrStr(dr2, c2[1]),
                                JnDesc = Sm.DrStr(dr2, c2[2]),
                                Remark = Sm.DrStr(dr2, c2[3]),
                                Amt = Sm.DrDec(dr2, c2[4]),
                                Terbilang = Convert2(Sm.DrDec(dr2, c2[4])),
                                DeptName = (mDeptNameJournal),
                                PrintBy = String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))


                            });
                        }
                    }
                    dr2.Close();
                }

                myLists.Add(l2);
            }

            #endregion 

            if (Doctitle == "AMKA" && IsVoucher == true)
            {
                Sm.PrintReport("JournalAMKA", myLists, TableName, false);
            }
            else
            {
                Sm.PrintReport(mFormPrintOutJournal, myLists, TableName, false);
            }
            
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmJournalDlg(this));
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0 &&
                ((e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 3)) || e.ColIndex != 1))
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                SetLueEntityCode(ref LueEntCode, Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                LueRequestEdit(Grd1, LueEntCode, ref fCell, ref fAccept, e);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 5, 6, 7, 8 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7 });
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 6) Compute(e.RowIndex, 1);
            if (e.ColIndex == 7) Compute(e.RowIndex, 2);
            ComputeDebetCredit(); ComputeBalanced();
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeDebetCredit(); 
                ComputeBalanced();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmJournalDlg(this));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (mIsJournalCostCenterEnabled && Sm.GetLue(LueCCCode).Length==0)
            {
                if (Sm.StdMsgYN("Question", 
                    "Cost center is empty." +Environment.NewLine + 
                    "Do you want to continue saving this data ?", 
                    mMenuCode) == DialogResult.No)
                {
                    LueCCCode.Focus();
                    return;
                }
                if (IsInsertedDataNotValid()) return;
            }
            else
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                    IsInsertedDataNotValid()) return;
            }

            Cursor.Current = Cursors.WaitCursor;


            string DocNo = string.Empty;

            if (mJournalDocNoFormat == "1") //Default
            {
                if (mDocNoFormat == "1")
                    DocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);
                else
                    DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt).ToString(), "Journal", "TblJournalHdr", mEntCode, "1");
            }
            else if (mJournalDocNoFormat == "2") //PHT
            {
                string Code1 = Sm.GetCode1ForJournalDocNo("FrmJournal", string.Empty, string.Empty, mJournalDocNoFormat);
                string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                DocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty));
            }


            var cml = new List<MySqlCommand>();

            cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsMeeEmpty(MeeJnDesc, "Description")||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt))) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                (mIsJournalCostCenterEnabled && Sm.IsLueEmpty(LueCCCode, "Cost Center")) ||
                IsGrdEmpty() || 
                IsGrdValueNotValid() ||
                IsBalancedNotValid() ||
                IsEntityNotValid();
        }

        private string GetProfitCenterCode()
        {
            var Value = Sm.GetLue(LueCCCode);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select ProfitCenterCode From TblCostCenter " +
                    "Where ProfitCenterCode Is Not Null And CCCode=@Param;",
                    Value);
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsBalancedNotValid()
        {
            decimal Debit = 0m, Credit = 0m, Balanced = 0m;

            ComputeDebetCredit(); 
            ComputeBalanced();

            if (TxtDbt.Text.Length != 0m) Debit = decimal.Parse(TxtDbt.Text);
            if (TxtCdt.Text.Length != 0m) Credit = decimal.Parse(TxtCdt.Text);
            if (TxtBalanced.Text.Length != 0m) Balanced = decimal.Parse(TxtBalanced.Text);
            if (Debit!=Credit || Balanced != 0m)
            {
                Sm.StdMsg(mMsgType.Warning, "Debit and credit is not balanced.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "COA's account is empty.")) return true;
                if (Sm.GetGrdDec(Grd1, Row, 6) == 0m && Sm.GetGrdDec(Grd1, Row, 7) == 0m) 
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "Acount# : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                        "Account Description : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                        "Amount must be greater than 0.");
                    return true;
                }
                if (Sm.GetGrdDec(Grd1, Row, 6) < 0m || Sm.GetGrdDec(Grd1, Row, 7) < 0m)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Acount# : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                        "Account Description : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                        "Amount must be greater than 0.");
                    return true;
                }
            }
            return false;
        }

        private bool IsEntityNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 4).Length == 0 && mIsEntityMandatory)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input entity for account " + Sm.GetGrdStr(Grd1, Row, 3) + ".");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            SQL.AppendLine("/* Journal Transaction */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, CurCode, MenuCode, MenuDesc, CCCode, Period, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @JnDesc, @CurCode, ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@CCCode, @Period, @Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblJournalDtl ");
            SQL.AppendLine("(DocNo, DNo, AcNo, EntCode, DAmt, CAmt, Remark, SOContractDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values");
            for (int r = 0; r<Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) 
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @AcNo_" + r.ToString() + ", @EntCode_" + r.ToString() + ", @DAmt_" + r.ToString() + ", @CAmt_" + r.ToString() + ", @Remark_" + r.ToString() + ", @SOContractDocNo, @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@EntCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                }
            }
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt).Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@JnDesc", MeeJnDesc.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Period", TxtPeriod.Text);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }
     
        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            
            var cml = new List<MySqlCommand>();

            cml.Add(CancelJournalHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt))) ||
                //Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready();
                IsJournalAutoGenerate();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this journal.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblJournalHdr Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This journal already cancelled."
                );       
        }

        private bool IsJournalAutoGenerate()
        {
            string JournalType = Sm.GetValue("SELECT MenuCode FROM tbljournalhdr WHERE DocNo = '" + TxtDocNo.Text + "' ;");

            if (JournalType != mMenuCode)
            {
                Sm.StdMsg(mMsgType.Warning, "This Journal can't be cancelled.");
                return true;
            }
            return false;

        }

        private MySqlCommand CancelJournalHdr()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);
            var MenuCodeJournal = Sm.GetValue("Select MenuCode From TblJournalHdr Where DocNo= @Param;", TxtDocNo.Text);
            var MenuCodeExchangeJournal = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmMonthlyFXJournalEntries' Limit 1;");

            SQL.AppendLine("Update TblJournalHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblJournalHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Period, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt && !Sm.CompareStr(MenuCodeJournal, MenuCodeExchangeJournal))
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Left(Replace(CurDate(), '-', ''), 6), ");
            else
                SQL.AppendLine("Period, ");
            SQL.AppendLine("Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, SOContractDocNo, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (IsClosingJournalUseCurrentDt && !Sm.CompareStr(MenuCodeJournal, MenuCodeExchangeJournal))
            {
                if (mJournalDocNoFormat == "1") //Default
                {
                    if (mDocNoFormat == "1")
                        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                    else
                        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
                }
                else if (mJournalDocNoFormat == "2") //PHT
                {
                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmJournal", string.Empty, string.Empty, mJournalDocNoFormat);
                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(CurrentDt, 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
                }

            }
            else
            {
                if (mJournalDocNoFormat == "1") //Default
                {
                    if (mDocNoFormat == "1")
                        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                    else
                        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));
                }
                else if (mJournalDocNoFormat == "2") //PHT
                {
                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmJournal", string.Empty, string.Empty, mJournalDocNoFormat);

                    if (MenuCodeJournal == MenuCodeExchangeJournal)
                    {
                        string[] DocNos = TxtDocNo.Text.Trim().Split('/');
                        if (DocNos.Count() == 7)
                        {
                            Code1 = DocNos[1];
                        }
                    }

                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
                }
            }
            return cm;
        }

        private MySqlCommand SaveGiro()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGiroMovement ");
            SQL.AppendLine("(DocType, DocNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select '02', DocNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblGiroMovement ");
            SQL.AppendLine("Where DocType='01' And DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '06', DocNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblGiroMovement ");
            SQL.AppendLine("Where DocType='05' And DocNo=@DocNo;");

            SQL.AppendLine("Update TblGiroSummary T Set ");
            SQL.AppendLine("    T.ActInd='N', T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblGiroMovement ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And DocType In ('02', '06') ");
            SQL.AppendLine("    And BusinessPartnerCode=T.BusinessPartnerCode ");
            SQL.AppendLine("    And BusinessPartnerType=T.BusinessPartnerType ");
            SQL.AppendLine("    And BankCode=T.BankCode ");
            SQL.AppendLine("    And GiroNo=T.GiroNo ");
            SQL.AppendLine(");");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblVoucherHdr Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Period, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Period, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblVoucherHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, SOContractDocNo, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblVoucherHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
                
            }
            else
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo",Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }

            return cm;
        }

        private MySqlCommand SaveJournalInterOffice2()
        {
            string mJournalDocNo = string.Empty, mJournalDocNo2 = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            if (IsClosingJournalUseCurrentDt)
            {
                if (mDocNoFormat == "1")
                {
                    mJournalDocNo = Sm.GetValue(Sm.GetNewJournalDocNo(CurrentDt, 1));
                    mJournalDocNo2 = Sm.GetValue(Sm.GetNewJournalDocNo(CurrentDt, 2));
                }
                else
                {
                    mJournalDocNo = Sm.GetValue(Sm.GetNewJournalDocNo3(CurrentDt, mEntCode,"1"));
                    mJournalDocNo2 = Sm.GetValue(Sm.GetNewJournalDocNo3(CurrentDt, mEntCode,"2"));
                }

            }
            else
            {
                if (mDocNoFormat == "1")
                {
                    mJournalDocNo = Sm.GetValue(Sm.GetNewJournalDocNo(DocDt, 1));
                    mJournalDocNo2 = Sm.GetValue(Sm.GetNewJournalDocNo(DocDt, 2));
                }
                else
                {
                    mJournalDocNo = Sm.GetValue(Sm.GetNewJournalDocNo3(DocDt, mEntCode, "1"));
                    mJournalDocNo2 = Sm.GetValue(Sm.GetNewJournalDocNo3(DocDt, mEntCode, "2"));
                }
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherHdr Set JournalDocNo2=@JournalDocNo, JournalDocNo4=@JournalDocNo2 ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblVoucherHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblVoucherHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo2, Replace(CurDate(), '-', ''), Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo3 From TblVoucherHdr Where DocNo=@DocNo And JournalDocNo3 Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo2, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo3 From TblVoucherHdr Where DocNo=@DocNo And JournalDocNo3 Is Not Null); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", mJournalDocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo2", mJournalDocNo2);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowJournalHdr(DocNo);
                ShowJournalDtl(DocNo);
                ComputeDebetCredit(); 
                ComputeBalanced();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowJournalHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string CCCode = string.Empty;

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            if (mIsJournalUseSOContract)
            {
                SQL.AppendLine("Select DocNo, DocDt, JnDesc, CurCode, MenuCode, MenuDesc, CCCode, Period, Remark, CancelReason, CancelInd, JournalDocNo, ");
                SQL.AppendLine("(Select SOContractDocNo From TblJournalDtl Where DocNo=T.DocNo And SOContractDocNo Is Not Null Limit 1) As SOContractDocNo ");
                SQL.AppendLine("From TblJournalHdr T ");
                SQL.AppendLine("Where DocNo=@DocNo;");
            }
            else
            { 
                SQL.AppendLine("Select DocNo, DocDt, JnDesc, CurCode, MenuCode, MenuDesc, CCCode, Period, Remark, CancelReason, CancelInd, JournalDocNo, Null As SOContractDocNo ");
                SQL.AppendLine("From TblJournalHdr ");
                SQL.AppendLine("Where DocNo=@DocNo;");
            }

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "JnDesc", "CurCode", "MenuCode", "MenuDesc", 
                        "CCCode", "Remark", "CancelReason", "CancelInd", "JournalDocNo",
                        "Period", "SOContractDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeJnDesc.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[3]));
                        TxtMenuCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtMenuDesc.EditValue = Sm.DrStr(dr, c[5]);
                        CCCode = Sm.DrStr(dr, c[6]);
                        if (CCCode.Length > 0)
                        {
                            if (mIsJournalUseProfitCenter)
                                Sl.SetLueCCCodeFilterByProfitCenter(ref LueCCCode, CCCode, "N");
                            else
                                SetLueCCCode(ref LueCCCode, CCCode);
                        }
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[8]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[9])=="Y";
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPeriod.EditValue = Sm.DrStr(dr, c[11]);
                        TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[12]);
                    }, true
                );
        }

        private void ShowJournalDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.AcNo, B.Alias, B.AcDesc, A.EntCode, C.EntName, A.DAmt, A.CAmt, A.Remark ");
                SQL.AppendLine("From TblJournalDtl A ");
                SQL.AppendLine("Left Join TblCoa B On A.AcNo = B.AcNo ");
                SQL.AppendLine("Left Join TblEntity C On A.EntCode = C.EntCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By CAmt Asc, A.AcNo");

                if (mIsCOAFilteredByGroup)
                {
                    SQL.AppendLine("    And Exists ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select 1 ");
                    SQL.AppendLine("        From TblGroupCOA ");
                    SQL.AppendLine("        Where GrpCode In ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select GrpCode ");
                    SQL.AppendLine("            From TblUser ");
                    SQL.AppendLine("            Where UserCode = @UserCode ");
                    SQL.AppendLine("        ) ");
                    //SQL.AppendLine("        And AcNo = B.AcNo ");
                    SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("; ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "AcNo", 
                        "Alias", "AcDesc", "EntCode", "EntName", "DAmt", "CAmt", 
                        "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7 });
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void SetLookUpEdit(LookUpEdit Lue, object ds)
        {
            //populate data for LookUpEdit control
            try
            {
                Lue.DataBindings.Clear();
                Lue.Properties.DataSource = ds;
                Lue.Properties.PopulateColumns();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Lue.EditValue = null;
            }
        }

        private void SetLueSequence(ref LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, mSeqNo);
        }

        private void SetLueCCCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 From TblCostCenter ");
            SQL.AppendLine("Where 1=1 ");
            if (Code.Length > 0)
                SQL.AppendLine("AND CCCode=@Code;");
            else
            {
                if (mIsCostCenterShowChildOnly)
                    SQL.AppendLine("AND CCCode not in (select distinct parent from tblcostcenter where parent is not null) ");
                SQL.AppendLine("AND ActInd='Y' ");
                if (mIsJournalFilterByGroupCC)
                {
                    SQL.AppendLine("And (CCCode Is Null Or (CCCode Is Not Null And CCCode In ( ");
                    SQL.AppendLine("    Select Distinct CCCode From TblGroupCostCenter ");
                    SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ))) ");
                }
                if (mIsJournalUseProfitCenter)
                {
                    SQL.AppendLine("And NotParentInd='Y' ");
                    SQL.AppendLine("And (CCCode Is Null Or (CCCode Is Not Null And CCCode In ( ");
                    SQL.AppendLine("    Select CCCode ");
                    SQL.AppendLine("    From TblCostCenter ");
                    SQL.AppendLine("    Where ActInd='Y' ");
                    SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("    And ProfitCenterCode In (");
                    SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    )))) ");
                }
                SQL.AppendLine("Order By CCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }


        internal string GetSelectedJournal()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        //private bool IsItCodeEmpty(iGRequestEditEventArgs e)
        //{
        //    if (Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length == 0)
        //    {
        //        e.DoDefault = false;
        //        Sm.StdMsg(mMsgType.Warning, "Account is empty.");
        //        return true;
        //    }
        //    return false;
        //}

        private void Compute(int Row, int ac)
        {
            switch (ac)
            {
                case 1:
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                    {
                        Grd1.Cells[Row, 7].Value = 0m; //Sm.GrdColReadOnly(Grd1, new int[] { 6 });
                    }
                    break;
                case 2:
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                    {
                        Grd1.Cells[Row, 6].Value = 0m; //Sm.GrdColReadOnly(Grd1, new int[] { 5});
                    }
                    break;
            }
        }

        private void ComputeBalanced()
        {
            decimal debit = 0m, credit = 0m;
            
            if (TxtDbt.Text.Length>0) debit = decimal.Parse(TxtDbt.Text);
            if (TxtCdt.Text.Length > 0) credit = decimal.Parse(TxtCdt.Text);
            
            TxtBalanced.EditValue = Sm.FormatNum(debit - credit, 0);
        }

        private void ComputeDebetCredit()
        {
            decimal DebetAmount = 0m, CreditAmount = 0m;

            for (int r= 0; r<Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0 && 
                    Sm.GetGrdStr(Grd1, r, 6).Length>0)
                    DebetAmount += Sm.GetGrdDec(Grd1, r, 6);
            }
            for (int r = 0; r <Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0 && 
                    Sm.GetGrdStr(Grd1, r, 7).Length > 0)
                    CreditAmount += Sm.GetGrdDec(Grd1, r, 7);
            }
                    
            TxtDbt.EditValue = Sm.FormatNum(DebetAmount, 0);
            TxtCdt.EditValue = Sm.FormatNum(CreditAmount, 0); 
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MainCurCode', 'FormPrintOutJournal', 'DocNoFormat', 'IsEntityMandatory', 'IsJournalCostCenterEnabled', ");
            SQL.AppendLine("'IsAutoJournalActived', 'IsCOAUseAlias', 'IsJournalUseDuplicateCOA', 'IsCOAFilteredByGroup', 'IsJournalUseSOContract', ");
            SQL.AppendLine("'IsJournalFilterByGroupCC', 'IsJournalShowRemarkDetail', 'IsCostCenterShowChildOnly', 'IsCOAFilteredByGroup', 'IsJournalUseProfitCenter', 'IsJournalPeriodEnabled', ");
            SQL.AppendLine("'JournalCashBankAcNo', 'IsVRApprovalByType', 'IsJournalTransactionCCFilteredByGroup', 'IsClosingJournalBasedOnMultiProfitCenter','JournalDocNoFormat' );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsVRApprovalByType": mIsVRApprovalByType = ParValue == "Y"; break;
                            case "IsJournalTransactionCCFilteredByGroup": mIsJournalTransactionCCFilteredByGroup = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsJournalCostCenterEnabled": mIsJournalCostCenterEnabled = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsCOAUseAlias": mIsCOAUseAlias = ParValue == "Y"; break;
                            case "IsJournalUseDuplicateCOA": mIsJournalUseDuplicateCOA = ParValue == "Y"; break;
                            case "IsCOAFilteredByGroup": mIsCOAFilteredByGroup = ParValue == "Y"; break;
                            case "IsJournalUseSOContract": mIsJournalUseSOContract = ParValue == "Y"; break;
                            case "IsJournalFilterByGroupCC": mIsJournalFilterByGroupCC = ParValue == "Y"; break;
                            case "IsJournalShowRemarkDetail": mIsJournalShowRemarkDetail = ParValue == "Y"; break;
                            case "IsCostCenterShowChildOnly": mIsCostCenterShowChildOnly = ParValue == "Y"; break;
                            case "IsJournalUseProfitCenter": mIsJournalUseProfitCenter = ParValue == "Y"; break;
                            case "IsJournalPeriodEnabled": mIsJournalPeriodEnabled = ParValue == "Y"; break;

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "FormPrintOutJournal": mFormPrintOutJournal = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "JournalCashBankAcNo": mJournalCashBankAcNo = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }


        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private string GetApproval(string Tbl, string ApprovalDocType, string Type)
        {
            var SQL = new StringBuilder();
            string DocNo = TxtDocNo.Text;
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), D.UserCode, '.JPG') As EmpPict1,  F.UserName AS UserName1,'VP Keuangan' As Description1, DATE_FORMAT(Left(D.LastUpDt, 8),'%d %M %Y') As ApproveDt1 ");
            SQL.AppendLine("FROM TblJournalHdr A ");
            SQL.AppendLine("INNER JOIN TblVoucherHdr B ON A.DocNo = B.JournalDocNo ");
            SQL.AppendLine("INNER JOIN " + Tbl + " C ON B.VoucherRequestDocNo = " + (Type == "1" ? "C.DocNo" : "C.VoucherRequestDocNo"));
            SQL.AppendLine("INNER JOIN TblDocApproval D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    AND D.DocType = '" + ApprovalDocType + "' ");
            SQL.AppendLine("INNER JOIN TblDocApprovalSetting E ON D.DocType = E.DocType ");
            SQL.AppendLine("    AND D.ApprovalDNo = E.DNo AND  E.Level = '1' ");
            SQL.AppendLine("INNER JOIN TblUser F ON D.UserCode = F.UserCode ");
            SQL.AppendLine("LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("WHERE A.DocNo = '" + DocNo + "' ");
            SQL.AppendLine(")T1");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), D.UserCode, '.JPG') As EmpPict2,  F.UserName AS UserName2,'VP Keuangan' As Description2, DATE_FORMAT(Left(D.LastUpDt, 8),'%d %M %Y') As ApproveDt2 ");
            SQL.AppendLine("FROM TblJournalHdr A ");
            SQL.AppendLine("INNER JOIN TblVoucherHdr B ON A.DocNo = B.JournalDocNo ");
            SQL.AppendLine("INNER JOIN " + Tbl + " C ON B.VoucherRequestDocNo = " + (Type == "1" ? "C.DocNo" : "C.VoucherRequestDocNo"));
            SQL.AppendLine("INNER JOIN TblDocApproval D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    AND D.DocType = '" + ApprovalDocType + "' ");
            SQL.AppendLine("INNER JOIN TblDocApprovalSetting E ON D.DocType = E.DocType ");
            SQL.AppendLine("    AND D.ApprovalDNo = E.DNo AND  E.Level = '2' ");
            SQL.AppendLine("INNER JOIN TblUser F ON D.UserCode = F.UserCode ");
            SQL.AppendLine("LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("WHERE A.DocNo = '" + DocNo + "' ");
            SQL.AppendLine(")T2 ON T1.DocNo = T2.DocNo");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), D.UserCode, '.JPG') As EmpPict3,  F.UserName AS UserName3,'VP Keuangan' As Description3, DATE_FORMAT(Left(D.LastUpDt, 8),'%d %M %Y') As ApproveDt3 ");
            SQL.AppendLine("FROM TblJournalHdr A ");
            SQL.AppendLine("INNER JOIN TblVoucherHdr B ON A.DocNo = B.JournalDocNo ");
            SQL.AppendLine("INNER JOIN " + Tbl + " C ON B.VoucherRequestDocNo = " + (Type == "1" ? "C.DocNo" : "C.VoucherRequestDocNo"));
            SQL.AppendLine("INNER JOIN TblDocApproval D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    AND D.DocType = '" + ApprovalDocType + "' ");
            SQL.AppendLine("INNER JOIN TblDocApprovalSetting E ON D.DocType = E.DocType ");
            SQL.AppendLine("    AND D.ApprovalDNo = E.DNo AND  E.Level = '3' ");
            SQL.AppendLine("INNER JOIN TblUser F ON D.UserCode = F.UserCode ");
            SQL.AppendLine("LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("WHERE A.DocNo = '" + DocNo + "' ");
            SQL.AppendLine(")T3 ON T1.DocNo = T3.DocNo");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(G.ParValue, ''), D.UserCode, '.JPG') As EmpPict4,  F.UserName AS UserName4,'VP Keuangan' As Description4, DATE_FORMAT(Left(D.LastUpDt, 8),'%d %M %Y') As ApproveDt4 ");
            SQL.AppendLine("FROM TblJournalHdr A ");
            SQL.AppendLine("INNER JOIN TblVoucherHdr B ON A.DocNo = B.JournalDocNo ");
            SQL.AppendLine("INNER JOIN " + Tbl + " C ON B.VoucherRequestDocNo = " + (Type == "1" ? "C.DocNo" : "C.VoucherRequestDocNo"));
            SQL.AppendLine("INNER JOIN TblDocApproval D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    AND D.DocType = '" + ApprovalDocType + "' ");
            SQL.AppendLine("INNER JOIN TblDocApprovalSetting E ON D.DocType = E.DocType ");
            SQL.AppendLine("    AND D.ApprovalDNo = E.DNo AND  E.Level = '4' ");
            SQL.AppendLine("INNER JOIN TblUser F ON D.UserCode = F.UserCode ");
            SQL.AppendLine("LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("WHERE A.DocNo = '" + DocNo + "' ");
            SQL.AppendLine(")T4 ON T1.DocNo = T4.DocNo");

            return SQL.ToString();

        }

        #endregion

        #endregion

        #region Event

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsJournalUseProfitCenter)
                    Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCodeFilterByProfitCenter), string.Empty, "N");
                else
                    Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsJournalTransactionCCFilteredByGroup ? "Y" : "N");
            }
        }

        private void SetLueEntityCode(ref LookUpEdit Lue, string AcNo)
        {
            var SQL = new StringBuilder();

            if (TxtDocNo.Text.Length == 0)
            {
                SQL.AppendLine("Select B.EntCode As Col1, C.Entname As Col2 ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo = B.AcNo ");
                SQL.AppendLine("Inner Join TblEntity C On B.EntCode = C.EntCode ");
                SQL.AppendLine("Where A.AcNo= '" + AcNo + "' Order By C.EntName  ");
                if (mIsCOAFilteredByGroup)
                {
                    SQL.AppendLine("    And Exists ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select 1 ");
                    SQL.AppendLine("        From TblGroupCOA ");
                    SQL.AppendLine("        Where GrpCode In ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select GrpCode ");
                    SQL.AppendLine("            From TblUser ");
                    SQL.AppendLine("            Where UserCode = @UserCode ");
                    SQL.AppendLine("        ) ");
                    //SQL.AppendLine("        And AcNo = B.AcNo ");
                    SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
                    SQL.AppendLine("    ) ");
                }
            }
            else
            {
                SQL.AppendLine("Select B.EntCode As Col1, C.Entname As Col2 ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo = B.AcNo ");
                SQL.AppendLine("Inner Join TblEntity C On B.EntCode = C.EntCode ");
                SQL.AppendLine("Order By C.EntName  ");
                if (mIsCOAFilteredByGroup)
                {
                    SQL.AppendLine("    And Exists ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select 1 ");
                    SQL.AppendLine("        From TblGroupCOA ");
                    SQL.AppendLine("        Where GrpCode In ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select GrpCode ");
                    SQL.AppendLine("            From TblUser ");
                    SQL.AppendLine("            Where UserCode = @UserCode ");
                    SQL.AppendLine("        ) ");
                    //SQL.AppendLine("        And AcNo = B.AcNo ");
                    SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
                    SQL.AppendLine("    ) ");
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
               ref Lue, SQL.ToString(),
               0, 35, false, true, "Code", "Entity", "Col2", "Col1");
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue2(SetLueEntityCode), "");
        }

        private void LueEntCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueEntCode_Leave(object sender, EventArgs e)
        {
            if (LueEntCode.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueEntCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value =
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueEntCode);
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueEntCode.GetColumnValue("Col2");
                }
                LueEntCode.Visible = false;
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void DteDocDt_DateTimeChanged(object sender, EventArgs e)
        {
            if(DteDocDt.Text.Length != 0)
                TxtPeriod.EditValue = Sm.GetDte(DteDocDt).Substring(0, 6);
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmJournalDlg2(this));
            }
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false))
            {
                bool IsExists = Sm.IsDataExist("Select 1 From TblMenu Where Param = 'FrmSOContract2' Limit 1 ");

                if (IsExists)
                {
                    var f = new FrmSOContract2("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtSOContractDocNo.Text;
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmSOContract("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtSOContractDocNo.Text;
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #region Report Class

        private class JournalHdr
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string JnDesc { get; set; }
            public string CurCode { get; set; }
            public string MenuDesc { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
            public bool AutoJournal { get; set; }
            public string DocType { get; set; }
            public string EntName { get; set; }
            public string CCName { get; set; }
        }

        private class JournalDtl
        {
            public string DNo { get; set; }
            public string ACNo { get; set; }
            public string Alias { get; set; }
            public string ACDesc { get; set; }
            public string EntName { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string Remark { get; set; }
        }

        private class JournalDtlAMKA
        {
            public string DocNo { get; set; }
            public string BankAcNo { get; set; }
            public string JnDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string ACNo { get; set; }
            public string ACDesc { get; set; }
        }

        private class JournalHdrKBN
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string JnDesc { get; set; }
            public string Remark { get; set; }
            public decimal Amt { get; set; }
            public string PrintBy { get; set; }
            public string Terbilang { get; set; }
            public string DeptName { get; set; }
        }

        private class JournalSignAMKA
        {
            public string EmpPict1 { get; set; }
            public string Description1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }
            public string EmpPict2 { get; set; }
            public string Description2 { get; set; }
            public string UserName2 { get; set; }
            public string ApproveDt2 { get; set; }
            public string EmpPict3 { get; set; }
            public string Description3 { get; set; }
            public string UserName3 { get; set; }
            public string ApproveDt3 { get; set; }
            public string EmpPict4 { get; set; }
            public string Description4 { get; set; }
            public string UserName4 { get; set; }
            public string ApproveDt4 { get; set; }
        }
        #endregion
    }
}
