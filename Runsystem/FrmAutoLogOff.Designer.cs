﻿namespace RunSystem
{
    partial class FrmAutoLogOff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtPwd = new DevExpress.XtraEditors.TextEdit();
            this.TxtUserCode = new DevExpress.XtraEditors.TextEdit();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtPwd
            // 
            this.TxtPwd.EnterMoveNextControl = true;
            this.TxtPwd.Location = new System.Drawing.Point(108, 39);
            this.TxtPwd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TxtPwd.Name = "TxtPwd";
            this.TxtPwd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPwd.Properties.Appearance.Options.UseFont = true;
            this.TxtPwd.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.TxtPwd.Properties.MaxLength = 20;
            this.TxtPwd.Properties.PasswordChar = '*';
            this.TxtPwd.Size = new System.Drawing.Size(228, 26);
            this.TxtPwd.TabIndex = 2;
            this.TxtPwd.Validated += new System.EventHandler(this.TxtPwd_Validated);
            // 
            // TxtUserCode
            // 
            this.TxtUserCode.EnterMoveNextControl = true;
            this.TxtUserCode.Location = new System.Drawing.Point(108, 7);
            this.TxtUserCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TxtUserCode.Name = "TxtUserCode";
            this.TxtUserCode.Properties.Appearance.BackColor = System.Drawing.Color.LightGray;
            this.TxtUserCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUserCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUserCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUserCode.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.TxtUserCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtUserCode.Properties.MaxLength = 16;
            this.TxtUserCode.Properties.ReadOnly = true;
            this.TxtUserCode.Size = new System.Drawing.Size(228, 26);
            this.TxtUserCode.TabIndex = 1;
            // 
            // BtnLogin
            // 
            this.BtnLogin.BackColor = System.Drawing.Color.Black;
            this.BtnLogin.FlatAppearance.BorderSize = 0;
            this.BtnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnLogin.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLogin.ForeColor = System.Drawing.Color.White;
            this.BtnLogin.Location = new System.Drawing.Point(270, 70);
            this.BtnLogin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(66, 30);
            this.BtnLogin.TabIndex = 3;
            this.BtnLogin.Text = "&Login";
            this.BtnLogin.UseVisualStyleBackColor = false;
            this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 19);
            this.label2.TabIndex = 6;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(15, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 19);
            this.label1.TabIndex = 5;
            this.label1.Text = "My Login";
            // 
            // FrmAutoLogOff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(358, 107);
            this.Controls.Add(this.TxtPwd);
            this.Controls.Add(this.TxtUserCode);
            this.Controls.Add(this.BtnLogin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "FrmAutoLogOff";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automatic Log Off";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmAutoLogOff_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAutoLogOff_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.TxtPwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtPwd;
        private DevExpress.XtraEditors.TextEdit TxtUserCode;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}