﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmParameterCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmParameterCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmParameterCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Category";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standart Methods

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtParCtCode, TxtParCtName, ChkActInd
                    }, true);
                    TxtParCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtParCtCode, TxtParCtName
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkActInd
                    }, true);
                    TxtParCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtParCtName, ChkActInd
                    }, false);
                    TxtParCtName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtParCtCode, TxtParCtName
                });
        }

        #endregion

        #region Button Methods

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmParameterCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtParCtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtParCtCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblParameterCategory Where ParCtCode=@ParCtCode" };
                Sm.CmParam<String>(ref cm, "@ParCtCode", TxtParCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblParameterCategory(ParCtCode, ParCtName, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ParCtCode, @ParCtName, 'Y', @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ParCtName=@ParCtName, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ParCtCode", TxtParCtCode.Text);
                Sm.CmParam<String>(ref cm, "@ParCtName", TxtParCtName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtParCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ParCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ParCtCode", ParCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "SELECT ParCtCode, ParCtName, ActInd " +
                        "From TblParameterCategory " +
                        "Where ParCtCode=@ParCtCode",
                        new string[] 
                        {
                            "ParCtCode", "ParCtName", "ActInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtParCtCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtParCtName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtParCtCode, "Parameter category code", false) ||
                IsParCtCodeExisted();
        }

        private bool IsParCtCodeExisted()
        {
            if (!TxtParCtCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand();
                cm.CommandText = "Select ParCtCode From TblParameterCategory Where ParCtCode=@ParCtCode;";
                Sm.CmParam<String>(ref cm, "@ParCtCode", TxtParCtCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Parameter category code ( " + TxtParCtCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Events

            #region Misc Control Events

            private void TxtParCtCode_Validated(object sender, EventArgs e)
            {
                Sm.TxtTrim(TxtParCtCode);
            }

            private void TxtParCtName_Validated(object sender, EventArgs e)
            {
                Sm.TxtTrim(TxtParCtName);
            }

            #endregion

        #endregion
    }
}
