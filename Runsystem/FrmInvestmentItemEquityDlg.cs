﻿#region Update
/*
    21/04/2022 [HAR/GSS]menu baru
    30/05/2022 [RDA/PRODUCT] feedback : item yg sudah nonaktif tidak bisa diinsert lagi (seharusnya bisa)
   
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentItemEquityDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmInvestmentItemEquity mFrmParent;
        internal string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmInvestmentItemEquityDlg(FrmInvestmentItemEquity FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion



        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Investment Item's Portofolio";
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetSQL();
            SetGrd();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[]
                    {
                        "No.",

                        //1-5
                        "Investment"+Environment.NewLine+"Code", 
                        "Investment"+Environment.NewLine+"Name",
                        "Investment"+Environment.NewLine+"Category Code",
                        "Investment"+Environment.NewLine+"Category",
                        "Currency",
                        //6 
                        "Stock"+Environment.NewLine+"Exchange",
                        "Sector"
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7});
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PortofolioId, A.PortofolioName, A.InvestmentCtCode, B.InvestmentCtName, A.curCode, A.stockexchange, A.sector From ");
            SQL.AppendLine("TblInvestmentPortofolio A ");
            SQL.AppendLine("Left Join TblInvestmentcategory B On A.InvestmentCtCode = B.InvestmentCtCode ");
            SQL.AppendLine("Where B.investmentCtName like '%equity%' ");
            SQL.AppendLine("And A.portofolioId Not in ( ");
            SQL.AppendLine("    Select PortofolioId From TblInvestmentItemEquity Where ActInd = 'Y' ");
            SQL.AppendLine(")");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 1 = 1 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItem.Text, new string[] { "A.PortofolioId", "A.PortofolioName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.PortofolioId ",
                        new string[]
                        {
                             //0
                            "PortofolioId", 
                            //1-5
                            "PortofolioName", "InvestmentCtCode", "InvestmentCtName", "curCode", "stockexchange", 
                            //6
                            "sector"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        }, true, false, false, true
                        );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 2))
            {
                mFrmParent.TxtPortofolioId.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.ShowData2(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Grid Method

   
        #endregion

        #endregion

        #endregion

        #region Method
        private void ChkItem_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItem_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);

        }
        #endregion
    }
}
