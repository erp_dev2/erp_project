﻿#region Update
/*
    04/09/2018 [TKG] tambah nomor rekening Depreciation Cost Account
    29/10/2018 [MEY] tambah field coa inter office 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmEntityDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmEntity mFrmParent;
        private byte mType = 1;

        #endregion

        #region Constructor

        public FrmEntityDlg(FrmEntity FrmParent, byte type)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mType = type;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetLueAcCode(ref LueAcCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Account#", 
                    "Description",
                    "Parent",
                    "Level",
                    "Type",

                    //6
                    "Entity"
                },
                new int[]
                {
                    50, 
                    120, 200, 200, 80, 80, 
                    150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 11);
            Sm.GrdColReadOnly(Grd1, new int[] {0, 1, 2, 3, 4, 5, 6 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.AcNo, A.AcDesc, D.AcDesc As AcDescParent, A.Level, B.OptDesc, C.EntName ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Left Join TblOption B On A.AcType=B.OptCode And B.OptCat='AccountType' ");
                SQL.AppendLine("Left Join TblEntity C On A.EntCode=C.EntCode ");
                SQL.AppendLine("Left Join TblCOA D On A.Parent=D.AcNo ");
                SQL.AppendLine("Where A.ActInd='Y' ");
                SQL.AppendLine("And A.AcNo Not In (Select Parent From TblCOA Where Parent is Not Null) ");

                string Filter = " ";

                if (Sm.GetLue(LueAcCode).Length>0)
                    Filter = " And A.Parent Like '" + Sm.GetLue(LueAcCode) + "%' ";

                Sm.FilterStr(ref Filter, ref cm, TxtAccount.Text, new string[] { "A.AcNo", "A.AcDesc" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString()+Filter +" Order By A.AcNo;",
                        new string[]
                        {
                            //0
                            "AcNo",

                            //1-5
                            "AcDesc",
                            "AcDescParent",
                            "Level",
                            "OptDesc",
                            "EntName"                           
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value=Row+1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                var r = Grd1.CurRow.Index;

                if (mType == 1)
                {
                    mFrmParent.TxtAcNo1.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                    mFrmParent.TxtAcDesc1.EditValue = Sm.GetGrdStr(Grd1, r, 2);
                }

                if (mType == 2)
                {
                    mFrmParent.TxtAcNo2.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                    mFrmParent.TxtAcDesc2.EditValue = Sm.GetGrdStr(Grd1, r, 2);
                }

                if (mType == 3)
                {
                    mFrmParent.TxtAcNo3.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                    mFrmParent.TxtAcDesc3.EditValue = Sm.GetGrdStr(Grd1, r, 2);
                }

                if (mType == 4)
                {
                    mFrmParent.TxtAcNo4.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                    mFrmParent.TxtAcDesc4.EditValue = Sm.GetGrdStr(Grd1, r, 2);
                }

                if (mType == 5)
                {
                    mFrmParent.TxtAcNo5.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                    mFrmParent.TxtAcDesc5.EditValue = Sm.GetGrdStr(Grd1, r, 2);
                }

                if (mType == 6)
                {
                    mFrmParent.TxtAcNo6.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                    mFrmParent.TxtAcDesc6.EditValue = Sm.GetGrdStr(Grd1, r, 2);
                }

                if (mType == 7)
                {
                    mFrmParent.TxtAcNo7.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                    mFrmParent.TxtAcDesc7.EditValue = Sm.GetGrdStr(Grd1, r, 2);
                }

                if (mType == 8)
                {
                    mFrmParent.TxtAcNo8.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                    mFrmParent.TxtAcDesc8.EditValue = Sm.GetGrdStr(Grd1, r, 2);
                }
                this.Close();
            }
        }

        public static void SetLueAcCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue, "Select AcNo As Col1, AcDesc As Col2 From TblCOA Where Level=1;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void ChkAccount_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        private void ChkAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Group");
        }

        private void TxtAccount_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCode, new Sm.RefreshLue1(SetLueAcCode));
        }

        #endregion
    }
}
