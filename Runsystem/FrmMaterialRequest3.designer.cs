﻿namespace RunSystem
{
    partial class FrmMaterialRequest3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMaterialRequest3));
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueReqType = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtRemainingBudget = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteUsageDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtPOQtyCancelDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnPOQtyCancelDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPOQtyCancelDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label76 = new System.Windows.Forms.Label();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueBCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtDORequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblDORequestDocNo = new System.Windows.Forms.Label();
            this.BtnDORequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDORequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.LblPICCode = new System.Windows.Forms.Label();
            this.LuePICCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.BtnImportCSV = new DevExpress.XtraEditors.SimpleButton();
            this.TxtPONo = new DevExpress.XtraEditors.TextEdit();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.BtnPGCode = new DevExpress.XtraEditors.SimpleButton();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.BtnNTPDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnNTPDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSOCDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSOCDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtNTPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtSOCDocNo = new DevExpress.XtraEditors.TextEdit();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.BtnDroppingRequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtDR_Balance = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtDR_MRAmt = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.MeeDR_Remark = new DevExpress.XtraEditors.MemoExEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtDR_DroppingRequestAmt = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtDR_BCCode = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtDR_PRJIDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtDR_Mth = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtDR_Yr = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtDR_DeptCode = new DevExpress.XtraEditors.TextEdit();
            this.BtnDroppingRequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDroppingRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOQtyCancelDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDORequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNTPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOCDocNo.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Balance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_MRAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDR_Remark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DroppingRequestAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_BCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_PRJIDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Mth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Yr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDroppingRequestDocNo.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(851, 0);
            this.panel1.Size = new System.Drawing.Size(70, 461);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(851, 371);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LueCurCode);
            this.panel3.Controls.Add(this.DteUsageDt);
            this.panel3.Location = new System.Drawing.Point(0, 371);
            this.panel3.Size = new System.Drawing.Size(851, 90);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.DteUsageDt, 0);
            this.panel3.Controls.SetChildIndex(this.LueCurCode, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 439);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(851, 90);
            this.Grd1.TabIndex = 53;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(131, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(52, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(131, 46);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(92, 49);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 18;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(41, 91);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "Request Type";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReqType
            // 
            this.LueReqType.EnterMoveNextControl = true;
            this.LueReqType.Location = new System.Drawing.Point(131, 88);
            this.LueReqType.Margin = new System.Windows.Forms.Padding(5);
            this.LueReqType.Name = "LueReqType";
            this.LueReqType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.Appearance.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReqType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReqType.Properties.DropDownRows = 30;
            this.LueReqType.Properties.NullText = "[Empty]";
            this.LueReqType.Properties.PopupWidth = 300;
            this.LueReqType.Size = new System.Drawing.Size(300, 20);
            this.LueReqType.TabIndex = 23;
            this.LueReqType.ToolTip = "F4 : Show/hide list";
            this.LueReqType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReqType.EditValueChanged += new System.EventHandler(this.LueReqType_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(52, 133);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 26;
            this.label4.Text = "Department";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(131, 130);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(300, 20);
            this.LueDeptCode.TabIndex = 27;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.Validated += new System.EventHandler(this.LueDeptCode_Validated);
            // 
            // TxtRemainingBudget
            // 
            this.TxtRemainingBudget.EnterMoveNextControl = true;
            this.TxtRemainingBudget.Location = new System.Drawing.Point(131, 298);
            this.TxtRemainingBudget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemainingBudget.Name = "TxtRemainingBudget";
            this.TxtRemainingBudget.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemainingBudget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemainingBudget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseFont = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemainingBudget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemainingBudget.Properties.ReadOnly = true;
            this.TxtRemainingBudget.Size = new System.Drawing.Size(166, 20);
            this.TxtRemainingBudget.TabIndex = 49;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(28, 302);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 14);
            this.label11.TabIndex = 48;
            this.label11.Text = "Available Budget";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(131, 319);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 2000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(500, 20);
            this.MeeRemark.TabIndex = 51;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(78, 323);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 50;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteUsageDt
            // 
            this.DteUsageDt.EditValue = null;
            this.DteUsageDt.EnterMoveNextControl = true;
            this.DteUsageDt.Location = new System.Drawing.Point(150, 20);
            this.DteUsageDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUsageDt.Name = "DteUsageDt";
            this.DteUsageDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.Appearance.Options.UseFont = true;
            this.DteUsageDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUsageDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUsageDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUsageDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUsageDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUsageDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUsageDt.Size = new System.Drawing.Size(125, 20);
            this.DteUsageDt.TabIndex = 54;
            this.DteUsageDt.Visible = false;
            this.DteUsageDt.Leave += new System.EventHandler(this.DteUsageDt_Leave);
            this.DteUsageDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteUsageDt_KeyDown);
            // 
            // TxtPOQtyCancelDocNo
            // 
            this.TxtPOQtyCancelDocNo.EnterMoveNextControl = true;
            this.TxtPOQtyCancelDocNo.Location = new System.Drawing.Point(131, 25);
            this.TxtPOQtyCancelDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPOQtyCancelDocNo.Name = "TxtPOQtyCancelDocNo";
            this.TxtPOQtyCancelDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPOQtyCancelDocNo.Properties.MaxLength = 16;
            this.TxtPOQtyCancelDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtPOQtyCancelDocNo.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(25, 28);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 14);
            this.label8.TabIndex = 14;
            this.label8.Text = "Cancellation PO#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPOQtyCancelDocNo2
            // 
            this.BtnPOQtyCancelDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPOQtyCancelDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPOQtyCancelDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPOQtyCancelDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseFont = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnPOQtyCancelDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPOQtyCancelDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPOQtyCancelDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnPOQtyCancelDocNo2.Image")));
            this.BtnPOQtyCancelDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPOQtyCancelDocNo2.Location = new System.Drawing.Point(326, 25);
            this.BtnPOQtyCancelDocNo2.Name = "BtnPOQtyCancelDocNo2";
            this.BtnPOQtyCancelDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnPOQtyCancelDocNo2.TabIndex = 17;
            this.BtnPOQtyCancelDocNo2.ToolTip = "Show Cancellation PO Document";
            this.BtnPOQtyCancelDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPOQtyCancelDocNo2.ToolTipTitle = "Run System";
            this.BtnPOQtyCancelDocNo2.Click += new System.EventHandler(this.BtnPOQtyCancelDocNo2_Click);
            // 
            // BtnPOQtyCancelDocNo
            // 
            this.BtnPOQtyCancelDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPOQtyCancelDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPOQtyCancelDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPOQtyCancelDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseFont = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPOQtyCancelDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPOQtyCancelDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPOQtyCancelDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPOQtyCancelDocNo.Image")));
            this.BtnPOQtyCancelDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPOQtyCancelDocNo.Location = new System.Drawing.Point(299, 25);
            this.BtnPOQtyCancelDocNo.Name = "BtnPOQtyCancelDocNo";
            this.BtnPOQtyCancelDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPOQtyCancelDocNo.TabIndex = 16;
            this.BtnPOQtyCancelDocNo.ToolTip = "Find Cancellation PO Document";
            this.BtnPOQtyCancelDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPOQtyCancelDocNo.ToolTipTitle = "Run System";
            this.BtnPOQtyCancelDocNo.Click += new System.EventHandler(this.BtnPOQtyCancelDocNo_Click);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(131, 67);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 250;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(300, 20);
            this.TxtLocalDocNo.TabIndex = 21;
            this.TxtLocalDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(30, 70);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(95, 14);
            this.label76.TabIndex = 20;
            this.label76.Text = "Local Document";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(97, 112);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 24;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(131, 109);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(300, 20);
            this.LueSiteCode.TabIndex = 25;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.Validated += new System.EventHandler(this.LueSiteCode_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(25, 280);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 14);
            this.label7.TabIndex = 46;
            this.label7.Text = "Budget Category";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBCCode
            // 
            this.LueBCCode.EnterMoveNextControl = true;
            this.LueBCCode.Location = new System.Drawing.Point(131, 277);
            this.LueBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBCCode.Name = "LueBCCode";
            this.LueBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.Appearance.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBCCode.Properties.DropDownRows = 30;
            this.LueBCCode.Properties.NullText = "[Empty]";
            this.LueBCCode.Properties.PopupWidth = 300;
            this.LueBCCode.Size = new System.Drawing.Size(300, 20);
            this.LueBCCode.TabIndex = 47;
            this.LueBCCode.ToolTip = "F4 : Show/hide list";
            this.LueBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBCCode.EditValueChanged += new System.EventHandler(this.LueBCCode_EditValueChanged);
            // 
            // TxtDORequestDocNo
            // 
            this.TxtDORequestDocNo.EnterMoveNextControl = true;
            this.TxtDORequestDocNo.Location = new System.Drawing.Point(131, 256);
            this.TxtDORequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDORequestDocNo.Name = "TxtDORequestDocNo";
            this.TxtDORequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDORequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDORequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDORequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDORequestDocNo.Properties.MaxLength = 250;
            this.TxtDORequestDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDORequestDocNo.TabIndex = 44;
            // 
            // LblDORequestDocNo
            // 
            this.LblDORequestDocNo.AutoSize = true;
            this.LblDORequestDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDORequestDocNo.Location = new System.Drawing.Point(43, 259);
            this.LblDORequestDocNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDORequestDocNo.Name = "LblDORequestDocNo";
            this.LblDORequestDocNo.Size = new System.Drawing.Size(82, 14);
            this.LblDORequestDocNo.TabIndex = 43;
            this.LblDORequestDocNo.Text = "DO Request#";
            this.LblDORequestDocNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDORequestDocNo2
            // 
            this.BtnDORequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDORequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDORequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDORequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDORequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDORequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDORequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDORequestDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDORequestDocNo2.Image")));
            this.BtnDORequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDORequestDocNo2.Location = new System.Drawing.Point(326, 234);
            this.BtnDORequestDocNo2.Name = "BtnDORequestDocNo2";
            this.BtnDORequestDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnDORequestDocNo2.TabIndex = 33;
            this.BtnDORequestDocNo2.ToolTip = "Show DO Request Document";
            this.BtnDORequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDORequestDocNo2.ToolTipTitle = "Run System";
            this.BtnDORequestDocNo2.Click += new System.EventHandler(this.BtnDORequestDocNo2_Click);
            // 
            // BtnDORequestDocNo
            // 
            this.BtnDORequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDORequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDORequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDORequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDORequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDORequestDocNo.Appearance.Options.UseFont = true;
            this.BtnDORequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDORequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDORequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDORequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDORequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnDORequestDocNo.Image")));
            this.BtnDORequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDORequestDocNo.Location = new System.Drawing.Point(298, 255);
            this.BtnDORequestDocNo.Name = "BtnDORequestDocNo";
            this.BtnDORequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnDORequestDocNo.TabIndex = 45;
            this.BtnDORequestDocNo.ToolTip = "Find DO Request Document";
            this.BtnDORequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDORequestDocNo.ToolTipTitle = "Run System";
            this.BtnDORequestDocNo.Visible = false;
            this.BtnDORequestDocNo.Click += new System.EventHandler(this.BtnDORequestDocNo_Click);
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(281, 20);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 14;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 200;
            this.LueCurCode.Size = new System.Drawing.Size(125, 20);
            this.LueCurCode.TabIndex = 55;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            this.LueCurCode.Leave += new System.EventHandler(this.LueCurCode_Leave);
            this.LueCurCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode_KeyDown);
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(54, 23);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(410, 20);
            this.TxtFile.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(24, 27);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 14);
            this.label6.TabIndex = 13;
            this.label6.Text = "File";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(496, 23);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(17, 17);
            this.BtnFile.TabIndex = 16;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(54, 46);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(500, 23);
            this.PbUpload.TabIndex = 18;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(520, 23);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(17, 17);
            this.BtnDownload.TabIndex = 17;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(466, 23);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 15;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // LblPICCode
            // 
            this.LblPICCode.AutoSize = true;
            this.LblPICCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPICCode.ForeColor = System.Drawing.Color.Black;
            this.LblPICCode.Location = new System.Drawing.Point(100, 239);
            this.LblPICCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPICCode.Name = "LblPICCode";
            this.LblPICCode.Size = new System.Drawing.Size(25, 14);
            this.LblPICCode.TabIndex = 41;
            this.LblPICCode.Text = "PIC";
            this.LblPICCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePICCode
            // 
            this.LuePICCode.EnterMoveNextControl = true;
            this.LuePICCode.Location = new System.Drawing.Point(131, 235);
            this.LuePICCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePICCode.Name = "LuePICCode";
            this.LuePICCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.Appearance.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePICCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePICCode.Properties.DropDownRows = 30;
            this.LuePICCode.Properties.NullText = "[Empty]";
            this.LuePICCode.Properties.PopupWidth = 300;
            this.LuePICCode.Size = new System.Drawing.Size(300, 20);
            this.LuePICCode.TabIndex = 42;
            this.LuePICCode.ToolTip = "F4 : Show/hide list";
            this.LuePICCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePICCode.EditValueChanged += new System.EventHandler(this.LuePICCode_EditValueChanged);
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(851, 371);
            this.Tc1.TabIndex = 11;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(845, 343);
            this.Tp1.Text = "General";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.TxtPONo);
            this.panel5.Controls.Add(this.TxtProjectName);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.BtnPGCode);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.TxtProjectCode);
            this.panel5.Controls.Add(this.BtnNTPDocNo2);
            this.panel5.Controls.Add(this.BtnNTPDocNo);
            this.panel5.Controls.Add(this.BtnSOCDocNo2);
            this.panel5.Controls.Add(this.BtnSOCDocNo);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.TxtNTPDocNo);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.TxtSOCDocNo);
            this.panel5.Controls.Add(this.TxtDocNo);
            this.panel5.Controls.Add(this.LblPICCode);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.LuePICCode);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.DteDocDt);
            this.panel5.Controls.Add(this.LueReqType);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.LueDeptCode);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.BtnDORequestDocNo2);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.BtnDORequestDocNo);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.TxtDORequestDocNo);
            this.panel5.Controls.Add(this.TxtRemainingBudget);
            this.panel5.Controls.Add(this.LblDORequestDocNo);
            this.panel5.Controls.Add(this.BtnPOQtyCancelDocNo);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.LueBCCode);
            this.panel5.Controls.Add(this.TxtPOQtyCancelDocNo);
            this.panel5.Controls.Add(this.LblSiteCode);
            this.panel5.Controls.Add(this.BtnPOQtyCancelDocNo2);
            this.panel5.Controls.Add(this.LueSiteCode);
            this.panel5.Controls.Add(this.label76);
            this.panel5.Controls.Add(this.TxtLocalDocNo);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(845, 343);
            this.panel5.TabIndex = 22;
            // 
            // BtnImportCSV
            // 
            this.BtnImportCSV.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnImportCSV.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnImportCSV.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnImportCSV.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnImportCSV.Appearance.Options.UseBackColor = true;
            this.BtnImportCSV.Appearance.Options.UseFont = true;
            this.BtnImportCSV.Appearance.Options.UseForeColor = true;
            this.BtnImportCSV.Appearance.Options.UseTextOptions = true;
            this.BtnImportCSV.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnImportCSV.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnImportCSV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnImportCSV.Location = new System.Drawing.Point(8, 316);
            this.BtnImportCSV.Name = "BtnImportCSV";
            this.BtnImportCSV.Size = new System.Drawing.Size(74, 21);
            this.BtnImportCSV.TabIndex = 52;
            this.BtnImportCSV.Text = "Import CSV";
            this.BtnImportCSV.ToolTip = "Import Data";
            this.BtnImportCSV.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnImportCSV.ToolTipTitle = "Run System";
            this.BtnImportCSV.Click += new System.EventHandler(this.BtnImportCSV_Click);
            // 
            // TxtPONo
            // 
            this.TxtPONo.EnterMoveNextControl = true;
            this.TxtPONo.Location = new System.Drawing.Point(131, 214);
            this.TxtPONo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPONo.Name = "TxtPONo";
            this.TxtPONo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPONo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPONo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPONo.Properties.Appearance.Options.UseFont = true;
            this.TxtPONo.Properties.MaxLength = 30;
            this.TxtPONo.Properties.ReadOnly = true;
            this.TxtPONo.Size = new System.Drawing.Size(300, 20);
            this.TxtPONo.TabIndex = 40;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(298, 193);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 500;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(446, 20);
            this.TxtProjectName.TabIndex = 39;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(32, 217);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(96, 14);
            this.label22.TabIndex = 40;
            this.label22.Text = "Customer\'s PO#";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPGCode
            // 
            this.BtnPGCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPGCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPGCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPGCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPGCode.Appearance.Options.UseBackColor = true;
            this.BtnPGCode.Appearance.Options.UseFont = true;
            this.BtnPGCode.Appearance.Options.UseForeColor = true;
            this.BtnPGCode.Appearance.Options.UseTextOptions = true;
            this.BtnPGCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPGCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPGCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnPGCode.Image")));
            this.BtnPGCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPGCode.Location = new System.Drawing.Point(273, 193);
            this.BtnPGCode.Name = "BtnPGCode";
            this.BtnPGCode.Size = new System.Drawing.Size(24, 19);
            this.BtnPGCode.TabIndex = 38;
            this.BtnPGCode.ToolTip = "Find Project";
            this.BtnPGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPGCode.ToolTipTitle = "Run System";
            this.BtnPGCode.Click += new System.EventHandler(this.BtnPGCode_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(79, 196);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 14);
            this.label23.TabIndex = 36;
            this.label23.Text = "Project";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectCode
            // 
            this.TxtProjectCode.EnterMoveNextControl = true;
            this.TxtProjectCode.Location = new System.Drawing.Point(131, 193);
            this.TxtProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectCode.Name = "TxtProjectCode";
            this.TxtProjectCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectCode.Properties.MaxLength = 30;
            this.TxtProjectCode.Properties.ReadOnly = true;
            this.TxtProjectCode.Size = new System.Drawing.Size(140, 20);
            this.TxtProjectCode.TabIndex = 37;
            // 
            // BtnNTPDocNo2
            // 
            this.BtnNTPDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnNTPDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnNTPDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNTPDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnNTPDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnNTPDocNo2.Appearance.Options.UseFont = true;
            this.BtnNTPDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnNTPDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnNTPDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnNTPDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnNTPDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnNTPDocNo2.Image")));
            this.BtnNTPDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnNTPDocNo2.Location = new System.Drawing.Point(460, 173);
            this.BtnNTPDocNo2.Name = "BtnNTPDocNo2";
            this.BtnNTPDocNo2.Size = new System.Drawing.Size(17, 17);
            this.BtnNTPDocNo2.TabIndex = 35;
            this.BtnNTPDocNo2.ToolTip = "DownloadFile";
            this.BtnNTPDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnNTPDocNo2.ToolTipTitle = "Run System";
            this.BtnNTPDocNo2.Click += new System.EventHandler(this.BtnNTPDocNo2_Click);
            // 
            // BtnNTPDocNo
            // 
            this.BtnNTPDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnNTPDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnNTPDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNTPDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnNTPDocNo.Appearance.Options.UseBackColor = true;
            this.BtnNTPDocNo.Appearance.Options.UseFont = true;
            this.BtnNTPDocNo.Appearance.Options.UseForeColor = true;
            this.BtnNTPDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnNTPDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnNTPDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnNTPDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnNTPDocNo.Image")));
            this.BtnNTPDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnNTPDocNo.Location = new System.Drawing.Point(436, 173);
            this.BtnNTPDocNo.Name = "BtnNTPDocNo";
            this.BtnNTPDocNo.Size = new System.Drawing.Size(17, 17);
            this.BtnNTPDocNo.TabIndex = 34;
            this.BtnNTPDocNo.ToolTip = "DownloadFile";
            this.BtnNTPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnNTPDocNo.ToolTipTitle = "Run System";
            this.BtnNTPDocNo.Click += new System.EventHandler(this.BtnNTPDocNo_Click);
            // 
            // BtnSOCDocNo2
            // 
            this.BtnSOCDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOCDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOCDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOCDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOCDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSOCDocNo2.Appearance.Options.UseFont = true;
            this.BtnSOCDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSOCDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSOCDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOCDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOCDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOCDocNo2.Image")));
            this.BtnSOCDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOCDocNo2.Location = new System.Drawing.Point(460, 153);
            this.BtnSOCDocNo2.Name = "BtnSOCDocNo2";
            this.BtnSOCDocNo2.Size = new System.Drawing.Size(17, 17);
            this.BtnSOCDocNo2.TabIndex = 31;
            this.BtnSOCDocNo2.ToolTip = "DownloadFile";
            this.BtnSOCDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOCDocNo2.ToolTipTitle = "Run System";
            this.BtnSOCDocNo2.Click += new System.EventHandler(this.BtnSOCDocNo2_Click);
            // 
            // BtnSOCDocNo
            // 
            this.BtnSOCDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOCDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOCDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOCDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOCDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOCDocNo.Appearance.Options.UseFont = true;
            this.BtnSOCDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOCDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOCDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOCDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOCDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOCDocNo.Image")));
            this.BtnSOCDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOCDocNo.Location = new System.Drawing.Point(436, 153);
            this.BtnSOCDocNo.Name = "BtnSOCDocNo";
            this.BtnSOCDocNo.Size = new System.Drawing.Size(17, 17);
            this.BtnSOCDocNo.TabIndex = 30;
            this.BtnSOCDocNo.ToolTip = "DownloadFile";
            this.BtnSOCDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOCDocNo.ToolTipTitle = "Run System";
            this.BtnSOCDocNo.Click += new System.EventHandler(this.BtnSOCDocNo_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(6, 175);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(119, 14);
            this.label21.TabIndex = 32;
            this.label21.Text = "Notice To Proceed#";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNTPDocNo
            // 
            this.TxtNTPDocNo.EnterMoveNextControl = true;
            this.TxtNTPDocNo.Location = new System.Drawing.Point(131, 172);
            this.TxtNTPDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNTPDocNo.Name = "TxtNTPDocNo";
            this.TxtNTPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtNTPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNTPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNTPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtNTPDocNo.Properties.MaxLength = 250;
            this.TxtNTPDocNo.Properties.ReadOnly = true;
            this.TxtNTPDocNo.Size = new System.Drawing.Size(300, 20);
            this.TxtNTPDocNo.TabIndex = 33;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(42, 154);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 14);
            this.label17.TabIndex = 28;
            this.label17.Text = "SO Contract#";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOCDocNo
            // 
            this.TxtSOCDocNo.EnterMoveNextControl = true;
            this.TxtSOCDocNo.Location = new System.Drawing.Point(131, 151);
            this.TxtSOCDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOCDocNo.Name = "TxtSOCDocNo";
            this.TxtSOCDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOCDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOCDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOCDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOCDocNo.Properties.MaxLength = 250;
            this.TxtSOCDocNo.Properties.ReadOnly = true;
            this.TxtSOCDocNo.Size = new System.Drawing.Size(300, 20);
            this.TxtSOCDocNo.TabIndex = 29;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel6);
            this.Tp2.Name = "Tp2";
            this.Tp2.PageVisible = false;
            this.Tp2.Size = new System.Drawing.Size(766, 343);
            this.Tp2.Text = "For Dropping Request";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.BtnDroppingRequestDocNo2);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.TxtDR_Balance);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.TxtDR_MRAmt);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.MeeDR_Remark);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.TxtDR_DroppingRequestAmt);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.TxtDR_BCCode);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.TxtDR_PRJIDocNo);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.TxtDR_Mth);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.TxtDR_Yr);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.TxtDR_DeptCode);
            this.panel6.Controls.Add(this.BtnDroppingRequestDocNo);
            this.panel6.Controls.Add(this.TxtDroppingRequestDocNo);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(766, 343);
            this.panel6.TabIndex = 22;
            // 
            // BtnDroppingRequestDocNo2
            // 
            this.BtnDroppingRequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDroppingRequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDroppingRequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDroppingRequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDroppingRequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDroppingRequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDroppingRequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDroppingRequestDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDroppingRequestDocNo2.Image")));
            this.BtnDroppingRequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDroppingRequestDocNo2.Location = new System.Drawing.Point(371, 11);
            this.BtnDroppingRequestDocNo2.Name = "BtnDroppingRequestDocNo2";
            this.BtnDroppingRequestDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnDroppingRequestDocNo2.TabIndex = 15;
            this.BtnDroppingRequestDocNo2.ToolTip = "Find DO Request Document";
            this.BtnDroppingRequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDroppingRequestDocNo2.ToolTipTitle = "Run System";
            this.BtnDroppingRequestDocNo2.Click += new System.EventHandler(this.BtnDroppingRequestDocNo2_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(116, 190);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 14);
            this.label20.TabIndex = 32;
            this.label20.Text = "Balance";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_Balance
            // 
            this.TxtDR_Balance.EnterMoveNextControl = true;
            this.TxtDR_Balance.Location = new System.Drawing.Point(170, 187);
            this.TxtDR_Balance.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_Balance.Name = "TxtDR_Balance";
            this.TxtDR_Balance.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_Balance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_Balance.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_Balance.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_Balance.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDR_Balance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDR_Balance.Properties.ReadOnly = true;
            this.TxtDR_Balance.Size = new System.Drawing.Size(166, 20);
            this.TxtDR_Balance.TabIndex = 33;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(53, 168);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(111, 14);
            this.label19.TabIndex = 30;
            this.label19.Text = "MR\'s Total Amount";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_MRAmt
            // 
            this.TxtDR_MRAmt.EnterMoveNextControl = true;
            this.TxtDR_MRAmt.Location = new System.Drawing.Point(170, 165);
            this.TxtDR_MRAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_MRAmt.Name = "TxtDR_MRAmt";
            this.TxtDR_MRAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_MRAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_MRAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_MRAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_MRAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDR_MRAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDR_MRAmt.Properties.ReadOnly = true;
            this.TxtDR_MRAmt.Size = new System.Drawing.Size(166, 20);
            this.TxtDR_MRAmt.TabIndex = 31;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(117, 212);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 14);
            this.label18.TabIndex = 34;
            this.label18.Text = "Remark";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeDR_Remark
            // 
            this.MeeDR_Remark.EnterMoveNextControl = true;
            this.MeeDR_Remark.Location = new System.Drawing.Point(170, 209);
            this.MeeDR_Remark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDR_Remark.Name = "MeeDR_Remark";
            this.MeeDR_Remark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeDR_Remark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.Appearance.Options.UseBackColor = true;
            this.MeeDR_Remark.Properties.Appearance.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDR_Remark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDR_Remark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDR_Remark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDR_Remark.Properties.MaxLength = 400;
            this.MeeDR_Remark.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeDR_Remark.Properties.ShowIcon = false;
            this.MeeDR_Remark.Size = new System.Drawing.Size(500, 20);
            this.MeeDR_Remark.TabIndex = 35;
            this.MeeDR_Remark.ToolTip = "F4 : Show/hide text";
            this.MeeDR_Remark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDR_Remark.ToolTipTitle = "Run System";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(11, 147);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(153, 14);
            this.label16.TabIndex = 26;
            this.label16.Text = "Dropping Request Amount";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_DroppingRequestAmt
            // 
            this.TxtDR_DroppingRequestAmt.EnterMoveNextControl = true;
            this.TxtDR_DroppingRequestAmt.Location = new System.Drawing.Point(170, 143);
            this.TxtDR_DroppingRequestAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_DroppingRequestAmt.Name = "TxtDR_DroppingRequestAmt";
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDR_DroppingRequestAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDR_DroppingRequestAmt.Properties.ReadOnly = true;
            this.TxtDR_DroppingRequestAmt.Size = new System.Drawing.Size(166, 20);
            this.TxtDR_DroppingRequestAmt.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(64, 124);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 14);
            this.label15.TabIndex = 24;
            this.label15.Text = "Budget Category";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_BCCode
            // 
            this.TxtDR_BCCode.EnterMoveNextControl = true;
            this.TxtDR_BCCode.Location = new System.Drawing.Point(170, 121);
            this.TxtDR_BCCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_BCCode.Name = "TxtDR_BCCode";
            this.TxtDR_BCCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_BCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_BCCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_BCCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_BCCode.Properties.MaxLength = 250;
            this.TxtDR_BCCode.Size = new System.Drawing.Size(300, 20);
            this.TxtDR_BCCode.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(19, 102);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(145, 14);
            this.label14.TabIndex = 22;
            this.label14.Text = "Project Implementation#";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_PRJIDocNo
            // 
            this.TxtDR_PRJIDocNo.EnterMoveNextControl = true;
            this.TxtDR_PRJIDocNo.Location = new System.Drawing.Point(170, 99);
            this.TxtDR_PRJIDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_PRJIDocNo.Name = "TxtDR_PRJIDocNo";
            this.TxtDR_PRJIDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_PRJIDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_PRJIDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_PRJIDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_PRJIDocNo.Properties.MaxLength = 250;
            this.TxtDR_PRJIDocNo.Size = new System.Drawing.Size(300, 20);
            this.TxtDR_PRJIDocNo.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(122, 80);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 14);
            this.label13.TabIndex = 20;
            this.label13.Text = "Month";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_Mth
            // 
            this.TxtDR_Mth.EnterMoveNextControl = true;
            this.TxtDR_Mth.Location = new System.Drawing.Point(170, 77);
            this.TxtDR_Mth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_Mth.Name = "TxtDR_Mth";
            this.TxtDR_Mth.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_Mth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_Mth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_Mth.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_Mth.Properties.MaxLength = 250;
            this.TxtDR_Mth.Size = new System.Drawing.Size(106, 20);
            this.TxtDR_Mth.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(132, 58);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 14);
            this.label12.TabIndex = 18;
            this.label12.Text = "Year";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_Yr
            // 
            this.TxtDR_Yr.EnterMoveNextControl = true;
            this.TxtDR_Yr.Location = new System.Drawing.Point(170, 55);
            this.TxtDR_Yr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_Yr.Name = "TxtDR_Yr";
            this.TxtDR_Yr.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_Yr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_Yr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_Yr.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_Yr.Properties.MaxLength = 250;
            this.TxtDR_Yr.Size = new System.Drawing.Size(106, 20);
            this.TxtDR_Yr.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(91, 36);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 14);
            this.label10.TabIndex = 16;
            this.label10.Text = "Department";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDR_DeptCode
            // 
            this.TxtDR_DeptCode.EnterMoveNextControl = true;
            this.TxtDR_DeptCode.Location = new System.Drawing.Point(170, 33);
            this.TxtDR_DeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDR_DeptCode.Name = "TxtDR_DeptCode";
            this.TxtDR_DeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDR_DeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDR_DeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDR_DeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDR_DeptCode.Properties.MaxLength = 250;
            this.TxtDR_DeptCode.Size = new System.Drawing.Size(300, 20);
            this.TxtDR_DeptCode.TabIndex = 17;
            // 
            // BtnDroppingRequestDocNo
            // 
            this.BtnDroppingRequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDroppingRequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDroppingRequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDroppingRequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseFont = true;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDroppingRequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDroppingRequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDroppingRequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDroppingRequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnDroppingRequestDocNo.Image")));
            this.BtnDroppingRequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDroppingRequestDocNo.Location = new System.Drawing.Point(342, 11);
            this.BtnDroppingRequestDocNo.Name = "BtnDroppingRequestDocNo";
            this.BtnDroppingRequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnDroppingRequestDocNo.TabIndex = 14;
            this.BtnDroppingRequestDocNo.ToolTip = "Find DO Request Document";
            this.BtnDroppingRequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDroppingRequestDocNo.ToolTipTitle = "Run System";
            this.BtnDroppingRequestDocNo.Click += new System.EventHandler(this.BtnDroppingRequestDocNo_Click);
            // 
            // TxtDroppingRequestDocNo
            // 
            this.TxtDroppingRequestDocNo.EnterMoveNextControl = true;
            this.TxtDroppingRequestDocNo.Location = new System.Drawing.Point(170, 11);
            this.TxtDroppingRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDroppingRequestDocNo.Name = "TxtDroppingRequestDocNo";
            this.TxtDroppingRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDroppingRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDroppingRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDroppingRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDroppingRequestDocNo.Properties.MaxLength = 30;
            this.TxtDroppingRequestDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDroppingRequestDocNo.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(50, 14);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 14);
            this.label9.TabIndex = 12;
            this.label9.Text = "Dropping Request#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.panel4);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 343);
            this.Tp3.Text = "Upload File";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.TxtFile);
            this.panel4.Controls.Add(this.ChkFile);
            this.panel4.Controls.Add(this.BtnFile);
            this.panel4.Controls.Add(this.BtnDownload);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.PbUpload);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(766, 343);
            this.panel4.TabIndex = 12;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.BtnImportCSV);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(760, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(85, 343);
            this.panel7.TabIndex = 53;
            // 
            // FrmMaterialRequest3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 461);
            this.Name = "FrmMaterialRequest3";
            this.ShowInTaskbar = false;
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOQtyCancelDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDORequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNTPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOCDocNo.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Balance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_MRAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDR_Remark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DroppingRequestAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_BCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_PRJIDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Mth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_Yr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDR_DeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDroppingRequestDocNo.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtRemainingBudget;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteUsageDt;
        public DevExpress.XtraEditors.SimpleButton BtnPOQtyCancelDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtPOQtyCancelDocNo;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnPOQtyCancelDocNo;
        private DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label76;
        public DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label LblSiteCode;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label7;
        public DevExpress.XtraEditors.LookUpEdit LueBCCode;
        private System.Windows.Forms.Label LblDORequestDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnDORequestDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnDORequestDocNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtDORequestDocNo;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.ProgressBar PbUpload;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.SaveFileDialog SFD;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        private System.Windows.Forms.Label LblPICCode;
        public DevExpress.XtraEditors.LookUpEdit LuePICCode;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel6;
        public DevExpress.XtraEditors.SimpleButton BtnDroppingRequestDocNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtDR_DroppingRequestAmt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtDR_Balance;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtDR_MRAmt;
        public DevExpress.XtraEditors.SimpleButton BtnDroppingRequestDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtDroppingRequestDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtDR_PRJIDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtDR_Mth;
        internal DevExpress.XtraEditors.TextEdit TxtDR_Yr;
        internal DevExpress.XtraEditors.TextEdit TxtDR_DeptCode;
        internal DevExpress.XtraEditors.MemoExEdit MeeDR_Remark;
        internal DevExpress.XtraEditors.TextEdit TxtDR_BCCode;
        internal DevExpress.XtraEditors.LookUpEdit LueReqType;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label21;
        public DevExpress.XtraEditors.SimpleButton BtnNTPDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnNTPDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnSOCDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnSOCDocNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtSOCDocNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtNTPDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label22;
        public DevExpress.XtraEditors.SimpleButton BtnPGCode;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtProjectCode;
        internal DevExpress.XtraEditors.TextEdit TxtPONo;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private System.Windows.Forms.Panel panel4;
        public DevExpress.XtraEditors.SimpleButton BtnImportCSV;
        private System.Windows.Forms.Panel panel7;
    }
}