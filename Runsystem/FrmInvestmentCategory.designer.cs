﻿namespace RunSystem
{
    partial class FrmInvestmentCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInvestmentCategory));
            this.TxtInvestmentCtName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtInvestmentCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.LblCOAStock = new System.Windows.Forms.Label();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.ChkActiveInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtAcDesc2 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo2 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOASales = new System.Windows.Forms.Label();
            this.TxtAcDesc4 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo4 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo4 = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtAcDesc3 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo3 = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtAcDesc6 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo6 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo6 = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtAcDesc5 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo5 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo5 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtAcDesc8 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo8 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo8 = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtAcDesc7 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo7 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo7 = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtAcDesc9 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo9 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo9 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo9.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(836, 0);
            this.panel1.Size = new System.Drawing.Size(70, 276);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtAcDesc9);
            this.panel2.Controls.Add(this.BtnAcNo9);
            this.panel2.Controls.Add(this.TxtAcNo9);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtAcDesc8);
            this.panel2.Controls.Add(this.BtnAcNo8);
            this.panel2.Controls.Add(this.TxtAcNo8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtAcDesc7);
            this.panel2.Controls.Add(this.BtnAcNo7);
            this.panel2.Controls.Add(this.TxtAcNo7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtAcDesc6);
            this.panel2.Controls.Add(this.BtnAcNo6);
            this.panel2.Controls.Add(this.TxtAcNo6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtAcDesc5);
            this.panel2.Controls.Add(this.BtnAcNo5);
            this.panel2.Controls.Add(this.TxtAcNo5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtAcDesc4);
            this.panel2.Controls.Add(this.BtnAcNo4);
            this.panel2.Controls.Add(this.TxtAcNo4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtAcDesc3);
            this.panel2.Controls.Add(this.BtnAcNo3);
            this.panel2.Controls.Add(this.TxtAcNo3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtAcDesc2);
            this.panel2.Controls.Add(this.BtnAcNo2);
            this.panel2.Controls.Add(this.TxtAcNo2);
            this.panel2.Controls.Add(this.LblCOASales);
            this.panel2.Controls.Add(this.ChkActiveInd);
            this.panel2.Controls.Add(this.TxtAcDesc);
            this.panel2.Controls.Add(this.BtnAcNo);
            this.panel2.Controls.Add(this.TxtAcNo);
            this.panel2.Controls.Add(this.LblCOAStock);
            this.panel2.Controls.Add(this.TxtInvestmentCtName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtInvestmentCtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(836, 276);
            // 
            // TxtInvestmentCtName
            // 
            this.TxtInvestmentCtName.EnterMoveNextControl = true;
            this.TxtInvestmentCtName.Location = new System.Drawing.Point(270, 33);
            this.TxtInvestmentCtName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtInvestmentCtName.Name = "TxtInvestmentCtName";
            this.TxtInvestmentCtName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvestmentCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCtName.Properties.MaxLength = 80;
            this.TxtInvestmentCtName.Size = new System.Drawing.Size(526, 20);
            this.TxtInvestmentCtName.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(96, 36);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Investment\'s Category Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentCtCode
            // 
            this.TxtInvestmentCtCode.EnterMoveNextControl = true;
            this.TxtInvestmentCtCode.Location = new System.Drawing.Point(270, 10);
            this.TxtInvestmentCtCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtInvestmentCtCode.Name = "TxtInvestmentCtCode";
            this.TxtInvestmentCtCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvestmentCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCtCode.Properties.MaxLength = 16;
            this.TxtInvestmentCtCode.Size = new System.Drawing.Size(178, 20);
            this.TxtInvestmentCtCode.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(99, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Investment\'s Category Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(800, 53);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(22, 21);
            this.BtnAcNo.TabIndex = 19;
            this.BtnAcNo.ToolTip = "Find COA\'s Account";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(270, 56);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 40;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo.TabIndex = 17;
            // 
            // LblCOAStock
            // 
            this.LblCOAStock.AutoSize = true;
            this.LblCOAStock.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOAStock.ForeColor = System.Drawing.Color.Red;
            this.LblCOAStock.Location = new System.Drawing.Point(126, 59);
            this.LblCOAStock.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOAStock.Name = "LblCOAStock";
            this.LblCOAStock.Size = new System.Drawing.Size(138, 14);
            this.LblCOAStock.TabIndex = 16;
            this.LblCOAStock.Text = "COA\'s Account (Stock) ";
            this.LblCOAStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(427, 55);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 80;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc.TabIndex = 18;
            // 
            // ChkActiveInd
            // 
            this.ChkActiveInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActiveInd.Location = new System.Drawing.Point(454, 9);
            this.ChkActiveInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActiveInd.Name = "ChkActiveInd";
            this.ChkActiveInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActiveInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActiveInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActiveInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActiveInd.Properties.Caption = "Active";
            this.ChkActiveInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActiveInd.ShowToolTips = false;
            this.ChkActiveInd.Size = new System.Drawing.Size(65, 22);
            this.ChkActiveInd.TabIndex = 11;
            // 
            // TxtAcDesc2
            // 
            this.TxtAcDesc2.EnterMoveNextControl = true;
            this.TxtAcDesc2.Location = new System.Drawing.Point(427, 79);
            this.TxtAcDesc2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc2.Name = "TxtAcDesc2";
            this.TxtAcDesc2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc2.Properties.MaxLength = 80;
            this.TxtAcDesc2.Properties.ReadOnly = true;
            this.TxtAcDesc2.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc2.TabIndex = 22;
            // 
            // BtnAcNo2
            // 
            this.BtnAcNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo2.Appearance.Options.UseBackColor = true;
            this.BtnAcNo2.Appearance.Options.UseFont = true;
            this.BtnAcNo2.Appearance.Options.UseForeColor = true;
            this.BtnAcNo2.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo2.Image")));
            this.BtnAcNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo2.Location = new System.Drawing.Point(800, 77);
            this.BtnAcNo2.Name = "BtnAcNo2";
            this.BtnAcNo2.Size = new System.Drawing.Size(22, 21);
            this.BtnAcNo2.TabIndex = 23;
            this.BtnAcNo2.ToolTip = "Find COA\'s Account";
            this.BtnAcNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo2.ToolTipTitle = "Run System";
            this.BtnAcNo2.Click += new System.EventHandler(this.BtnAcNo4_Click);
            // 
            // TxtAcNo2
            // 
            this.TxtAcNo2.EnterMoveNextControl = true;
            this.TxtAcNo2.Location = new System.Drawing.Point(270, 79);
            this.TxtAcNo2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo2.Name = "TxtAcNo2";
            this.TxtAcNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo2.Properties.MaxLength = 40;
            this.TxtAcNo2.Properties.ReadOnly = true;
            this.TxtAcNo2.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo2.TabIndex = 21;
            // 
            // LblCOASales
            // 
            this.LblCOASales.AutoSize = true;
            this.LblCOASales.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOASales.ForeColor = System.Drawing.Color.Red;
            this.LblCOASales.Location = new System.Drawing.Point(128, 81);
            this.LblCOASales.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOASales.Name = "LblCOASales";
            this.LblCOASales.Size = new System.Drawing.Size(134, 14);
            this.LblCOASales.TabIndex = 20;
            this.LblCOASales.Text = "COA\'s Account (Sales) ";
            this.LblCOASales.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc4
            // 
            this.TxtAcDesc4.EditValue = "";
            this.TxtAcDesc4.EnterMoveNextControl = true;
            this.TxtAcDesc4.Location = new System.Drawing.Point(428, 125);
            this.TxtAcDesc4.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc4.Name = "TxtAcDesc4";
            this.TxtAcDesc4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc4.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc4.Properties.MaxLength = 80;
            this.TxtAcDesc4.Properties.ReadOnly = true;
            this.TxtAcDesc4.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc4.TabIndex = 30;
            // 
            // BtnAcNo4
            // 
            this.BtnAcNo4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo4.Appearance.Options.UseBackColor = true;
            this.BtnAcNo4.Appearance.Options.UseFont = true;
            this.BtnAcNo4.Appearance.Options.UseForeColor = true;
            this.BtnAcNo4.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo4.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo4.Image")));
            this.BtnAcNo4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo4.Location = new System.Drawing.Point(800, 123);
            this.BtnAcNo4.Name = "BtnAcNo4";
            this.BtnAcNo4.Size = new System.Drawing.Size(23, 21);
            this.BtnAcNo4.TabIndex = 31;
            this.BtnAcNo4.ToolTip = "Find COA\'s Account";
            this.BtnAcNo4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo4.ToolTipTitle = "Run System";
            this.BtnAcNo4.Click += new System.EventHandler(this.BtnAcNo4_Click_1);
            // 
            // TxtAcNo4
            // 
            this.TxtAcNo4.EnterMoveNextControl = true;
            this.TxtAcNo4.Location = new System.Drawing.Point(271, 125);
            this.TxtAcNo4.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo4.Name = "TxtAcNo4";
            this.TxtAcNo4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo4.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo4.Properties.MaxLength = 40;
            this.TxtAcNo4.Properties.ReadOnly = true;
            this.TxtAcNo4.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo4.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(118, 127);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 14);
            this.label3.TabIndex = 28;
            this.label3.Text = "COA\'s Account (Payable)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc3
            // 
            this.TxtAcDesc3.EnterMoveNextControl = true;
            this.TxtAcDesc3.Location = new System.Drawing.Point(428, 102);
            this.TxtAcDesc3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc3.Name = "TxtAcDesc3";
            this.TxtAcDesc3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc3.Properties.MaxLength = 80;
            this.TxtAcDesc3.Properties.ReadOnly = true;
            this.TxtAcDesc3.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc3.TabIndex = 26;
            // 
            // BtnAcNo3
            // 
            this.BtnAcNo3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo3.Appearance.Options.UseBackColor = true;
            this.BtnAcNo3.Appearance.Options.UseFont = true;
            this.BtnAcNo3.Appearance.Options.UseForeColor = true;
            this.BtnAcNo3.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo3.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo3.Image")));
            this.BtnAcNo3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo3.Location = new System.Drawing.Point(800, 101);
            this.BtnAcNo3.Name = "BtnAcNo3";
            this.BtnAcNo3.Size = new System.Drawing.Size(23, 21);
            this.BtnAcNo3.TabIndex = 27;
            this.BtnAcNo3.ToolTip = "Find COA\'s Account";
            this.BtnAcNo3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo3.ToolTipTitle = "Run System";
            this.BtnAcNo3.Click += new System.EventHandler(this.BtnAcNo3_Click);
            // 
            // TxtAcNo3
            // 
            this.TxtAcNo3.EnterMoveNextControl = true;
            this.TxtAcNo3.Location = new System.Drawing.Point(271, 102);
            this.TxtAcNo3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo3.Name = "TxtAcNo3";
            this.TxtAcNo3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo3.Properties.MaxLength = 40;
            this.TxtAcNo3.Properties.ReadOnly = true;
            this.TxtAcNo3.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo3.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(99, 104);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 14);
            this.label4.TabIndex = 24;
            this.label4.Text = "COA\'s Account (Receivable) ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc6
            // 
            this.TxtAcDesc6.EnterMoveNextControl = true;
            this.TxtAcDesc6.Location = new System.Drawing.Point(427, 171);
            this.TxtAcDesc6.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc6.Name = "TxtAcDesc6";
            this.TxtAcDesc6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc6.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc6.Properties.MaxLength = 80;
            this.TxtAcDesc6.Properties.ReadOnly = true;
            this.TxtAcDesc6.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc6.TabIndex = 38;
            // 
            // BtnAcNo6
            // 
            this.BtnAcNo6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo6.Appearance.Options.UseBackColor = true;
            this.BtnAcNo6.Appearance.Options.UseFont = true;
            this.BtnAcNo6.Appearance.Options.UseForeColor = true;
            this.BtnAcNo6.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo6.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo6.Image")));
            this.BtnAcNo6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo6.Location = new System.Drawing.Point(800, 169);
            this.BtnAcNo6.Name = "BtnAcNo6";
            this.BtnAcNo6.Size = new System.Drawing.Size(23, 21);
            this.BtnAcNo6.TabIndex = 39;
            this.BtnAcNo6.ToolTip = "Find COA\'s Account";
            this.BtnAcNo6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo6.ToolTipTitle = "Run System";
            this.BtnAcNo6.Click += new System.EventHandler(this.BtnAcNo6_Click);
            // 
            // TxtAcNo6
            // 
            this.TxtAcNo6.EnterMoveNextControl = true;
            this.TxtAcNo6.Location = new System.Drawing.Point(270, 171);
            this.TxtAcNo6.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo6.Name = "TxtAcNo6";
            this.TxtAcNo6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo6.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo6.Properties.MaxLength = 40;
            this.TxtAcNo6.Properties.ReadOnly = true;
            this.TxtAcNo6.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo6.TabIndex = 37;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(6, 173);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(251, 14);
            this.label5.TabIndex = 36;
            this.label5.Text = "COA\'s Account (Unrealized Gain/Loss FVOCI)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc5
            // 
            this.TxtAcDesc5.EnterMoveNextControl = true;
            this.TxtAcDesc5.Location = new System.Drawing.Point(427, 148);
            this.TxtAcDesc5.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc5.Name = "TxtAcDesc5";
            this.TxtAcDesc5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc5.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc5.Properties.MaxLength = 80;
            this.TxtAcDesc5.Properties.ReadOnly = true;
            this.TxtAcDesc5.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc5.TabIndex = 34;
            // 
            // BtnAcNo5
            // 
            this.BtnAcNo5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo5.Appearance.Options.UseBackColor = true;
            this.BtnAcNo5.Appearance.Options.UseFont = true;
            this.BtnAcNo5.Appearance.Options.UseForeColor = true;
            this.BtnAcNo5.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo5.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo5.Image")));
            this.BtnAcNo5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo5.Location = new System.Drawing.Point(800, 146);
            this.BtnAcNo5.Name = "BtnAcNo5";
            this.BtnAcNo5.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo5.TabIndex = 35;
            this.BtnAcNo5.ToolTip = "Find COA\'s Account";
            this.BtnAcNo5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo5.ToolTipTitle = "Run System";
            this.BtnAcNo5.Click += new System.EventHandler(this.BtnAcNo5_Click);
            // 
            // TxtAcNo5
            // 
            this.TxtAcNo5.EnterMoveNextControl = true;
            this.TxtAcNo5.Location = new System.Drawing.Point(270, 148);
            this.TxtAcNo5.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo5.Name = "TxtAcNo5";
            this.TxtAcNo5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo5.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo5.Properties.MaxLength = 40;
            this.TxtAcNo5.Properties.ReadOnly = true;
            this.TxtAcNo5.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo5.TabIndex = 33;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(14, 150);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(244, 14);
            this.label6.TabIndex = 32;
            this.label6.Text = "COA\'s Account (Unrealized Gain/Loss FVPL)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc8
            // 
            this.TxtAcDesc8.EnterMoveNextControl = true;
            this.TxtAcDesc8.Location = new System.Drawing.Point(428, 216);
            this.TxtAcDesc8.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc8.Name = "TxtAcDesc8";
            this.TxtAcDesc8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc8.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc8.Properties.MaxLength = 80;
            this.TxtAcDesc8.Properties.ReadOnly = true;
            this.TxtAcDesc8.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc8.TabIndex = 46;
            // 
            // BtnAcNo8
            // 
            this.BtnAcNo8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo8.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo8.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo8.Appearance.Options.UseBackColor = true;
            this.BtnAcNo8.Appearance.Options.UseFont = true;
            this.BtnAcNo8.Appearance.Options.UseForeColor = true;
            this.BtnAcNo8.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo8.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo8.Image")));
            this.BtnAcNo8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo8.Location = new System.Drawing.Point(800, 217);
            this.BtnAcNo8.Name = "BtnAcNo8";
            this.BtnAcNo8.Size = new System.Drawing.Size(22, 21);
            this.BtnAcNo8.TabIndex = 47;
            this.BtnAcNo8.ToolTip = "Find COA\'s Account";
            this.BtnAcNo8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo8.ToolTipTitle = "Run System";
            this.BtnAcNo8.Click += new System.EventHandler(this.BtnAcNo8_Click);
            // 
            // TxtAcNo8
            // 
            this.TxtAcNo8.EnterMoveNextControl = true;
            this.TxtAcNo8.Location = new System.Drawing.Point(271, 216);
            this.TxtAcNo8.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo8.Name = "TxtAcNo8";
            this.TxtAcNo8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo8.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo8.Properties.MaxLength = 40;
            this.TxtAcNo8.Properties.ReadOnly = true;
            this.TxtAcNo8.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo8.TabIndex = 45;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(72, 219);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(192, 14);
            this.label7.TabIndex = 44;
            this.label7.Text = "COA\'s Account (Interest Income)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc7
            // 
            this.TxtAcDesc7.EnterMoveNextControl = true;
            this.TxtAcDesc7.Location = new System.Drawing.Point(428, 193);
            this.TxtAcDesc7.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc7.Name = "TxtAcDesc7";
            this.TxtAcDesc7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc7.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc7.Properties.MaxLength = 80;
            this.TxtAcDesc7.Properties.ReadOnly = true;
            this.TxtAcDesc7.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc7.TabIndex = 42;
            // 
            // BtnAcNo7
            // 
            this.BtnAcNo7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo7.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo7.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo7.Appearance.Options.UseBackColor = true;
            this.BtnAcNo7.Appearance.Options.UseFont = true;
            this.BtnAcNo7.Appearance.Options.UseForeColor = true;
            this.BtnAcNo7.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo7.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo7.Image")));
            this.BtnAcNo7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo7.Location = new System.Drawing.Point(800, 193);
            this.BtnAcNo7.Name = "BtnAcNo7";
            this.BtnAcNo7.Size = new System.Drawing.Size(22, 21);
            this.BtnAcNo7.TabIndex = 43;
            this.BtnAcNo7.ToolTip = "Find COA\'s Account";
            this.BtnAcNo7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo7.ToolTipTitle = "Run System";
            this.BtnAcNo7.Click += new System.EventHandler(this.BtnAcNo7_Click);
            // 
            // TxtAcNo7
            // 
            this.TxtAcNo7.EnterMoveNextControl = true;
            this.TxtAcNo7.Location = new System.Drawing.Point(271, 193);
            this.TxtAcNo7.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo7.Name = "TxtAcNo7";
            this.TxtAcNo7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo7.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo7.Properties.MaxLength = 40;
            this.TxtAcNo7.Properties.ReadOnly = true;
            this.TxtAcNo7.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo7.TabIndex = 41;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(57, 196);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(201, 14);
            this.label8.TabIndex = 40;
            this.label8.Text = "COA\'s Account (Realized Gain/Loss)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc9
            // 
            this.TxtAcDesc9.EnterMoveNextControl = true;
            this.TxtAcDesc9.Location = new System.Drawing.Point(427, 239);
            this.TxtAcDesc9.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc9.Name = "TxtAcDesc9";
            this.TxtAcDesc9.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc9.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc9.Properties.MaxLength = 80;
            this.TxtAcDesc9.Properties.ReadOnly = true;
            this.TxtAcDesc9.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc9.TabIndex = 50;
            // 
            // BtnAcNo9
            // 
            this.BtnAcNo9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo9.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo9.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo9.Appearance.Options.UseBackColor = true;
            this.BtnAcNo9.Appearance.Options.UseFont = true;
            this.BtnAcNo9.Appearance.Options.UseForeColor = true;
            this.BtnAcNo9.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo9.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo9.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo9.Image")));
            this.BtnAcNo9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo9.Location = new System.Drawing.Point(800, 240);
            this.BtnAcNo9.Name = "BtnAcNo9";
            this.BtnAcNo9.Size = new System.Drawing.Size(25, 21);
            this.BtnAcNo9.TabIndex = 51;
            this.BtnAcNo9.ToolTip = "Find COA\'s Account";
            this.BtnAcNo9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo9.ToolTipTitle = "Run System";
            this.BtnAcNo9.Click += new System.EventHandler(this.BtnAcNo9_Click);
            // 
            // TxtAcNo9
            // 
            this.TxtAcNo9.EnterMoveNextControl = true;
            this.TxtAcNo9.Location = new System.Drawing.Point(270, 239);
            this.TxtAcNo9.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo9.Name = "TxtAcNo9";
            this.TxtAcNo9.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo9.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo9.Properties.MaxLength = 40;
            this.TxtAcNo9.Properties.ReadOnly = true;
            this.TxtAcNo9.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo9.TabIndex = 49;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(56, 242);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(208, 14);
            this.label9.TabIndex = 48;
            this.label9.Text = "COA\'s Account (Interest Receivable)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmInvestmentCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 276);
            this.Name = "FrmInvestmentCategory";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo9.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtInvestmentCtName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCtCode;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        private System.Windows.Forms.Label LblCOAStock;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        private DevExpress.XtraEditors.CheckEdit ChkActiveInd;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc2;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo2;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo2;
        private System.Windows.Forms.Label LblCOASales;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc8;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo8;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo8;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc7;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo7;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo7;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc6;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo6;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo6;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc5;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo5;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo5;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc4;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo4;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo4;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc3;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo3;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo3;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc9;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo9;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo9;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.TextEdit TxtAcNo;
    }
}