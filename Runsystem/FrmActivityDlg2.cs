﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmActivityDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmActivity mFrmParent;
        private string mSQL = string.Empty; 

        #endregion

        #region Constructor

        public FrmActivityDlg2(FrmActivity FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-2
                        "Activity"+Environment.NewLine+"Code", 
                        "Activity"+Environment.NewLine+"Name", 
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("ActCtCode, ActCtName ");
            SQL.AppendLine("From TblActivityCategory ");
            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtActCtName.Text, new string[] { "ActCtCode", "ActCtName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By ActCtCode, ActCtName",
                        new string[] 
                        { 
                            //0
                            "ActCtCode",
 
                            //1-5
                            "ActCtName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtActCtCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.LblActCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                this.Close();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion


        #endregion

        #region Event
        private void ChkActCtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Activity Category");
        }

        private void TxtActCtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
