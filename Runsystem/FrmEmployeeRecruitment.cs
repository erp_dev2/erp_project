﻿#region Update
/*
    27/03/2018 [TKG] tambah competence    
    03/04/2018 [TKG] tambah filter site dan level
    25/04/2022 [TYO/HIN] Menambah tab Picture & File based on FrmEmployee
    25/04/2022 [TYO/HIN] Menambah kolom Profession dan Latest Education pada Tab Family & Personal Reference
    25/04/2022 [TYO/HIN] Menambah kolom Training pada tab Training
    25/04/2022 [TYO/HIN] Menambah kolom Field, Faculty, Graduation Year pada tab Education
    26/04/2022 [RIS/HIN] Menambahkan tab stage dengan param IsPersonalInformationUseStage
    10/05/2022 [RIS/HIN] Menambahkan tab Employee Benefit dengan param IsPersonalInformationUseEmployeeBenefit
    11/05/2022 [RIS/HIN] Membuat printout
    16/12/2022 [DITA/HIN] bug saat print karna belum di false
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Drawing.Imaging;
using System.Collections;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeRecruitment : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mEmpCode = string.Empty;
        internal FrmEmployeeRecruitmentFind FrmFind;
        private bool
            mIsEmpCodeAutoGenerate = false, mIsInsert = true, mIsEdit = false,
            mIsEmployeeCOAInfoMandatory = false, mIsPPSActive = false, mIsEmployeeAllowToUploadFile = false,
            mIsEmployeeEducationInputManualData = false;
        private string mEmployeeAcNo = string.Empty, mEmployeeAcDesc = string.Empty, mProfilePicture = string.Empty;
        internal bool 
            //mIsFilterBySite = false,
            mIsFilterBySiteHR = false,
            mIsFilterByLevelHR = false,
            mIsDeptFilterByDivision = false,
            mIsPersonalInformationUseStage = false,
            mIsPersonalInformationUseEmployeeBenefit = false;

        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty;

        private byte[] downloadedData;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmEmployeeRecruitment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                
                tabControl1.SelectTab("TpgGeneral");
                Sl.SetLuePosCode(ref LuePosCode);
                Sl.SetLueGender(ref LueGender);
                Sl.SetLueReligion(ref LueReligion);
                Sl.SetLueCityCode(ref LueCity);
                Sl.SetLueEmployeePayrollType(ref LuePayrollType);
                Sl.SetLuePTKP(ref LuePTKP);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueGrdLvlCode(ref LueGrdLvlCode);
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
                Sl.SetLueOption(ref LueMaritalStatus, "MaritalStatus");
                Sl.SetLueOption(ref LueSystemType, "EmpSystemType");
                Sl.SetLueOption(ref LueBloodType, "BloodType");
                SetLueSite(ref LueSiteCode, string.Empty);
                SetLueSection(ref LueSection);
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
                SetLueWorkGroup(ref LueWorkGroup, Sm.GetLue(LueSection));
                Sl.SetLueDivisionCode(ref LueDivisionCode);
                if (!mIsDeptFilterByDivision)
                    Sl.SetLueDeptCode(ref LueDeptCode);

                tabControl1.SelectTab("TpgEmployeeFamily");
                Sl.SetLueFamilyStatus(ref LueStatus);
                LueStatus.Visible = false;
                Sl.SetLueGender(ref LueFamGender);
                LueFamGender.Visible = false;
                DteFamBirthDt.Visible = false;
               
            //    DtStartWork.Visible = false;
                SetGrd();
                Sl.SetLueOption(ref LueProfessionCode, "Profession");
                LueProfessionCode.Visible = false;
                Sl.SetLueOption(ref LueEducationLevelCode, "EmployeeEducationLevel");
                LueEducationLevelCode.Visible = false;

                SetGrd7();
                SetGrd6();

                tabControl1.SelectTab("TpgEmployeeWorkExp");
                SetGrd2();

                tabControl1.SelectTab("TpgEmployeeEducation");
                Sl.SetLueEmployeeEducationLevel(ref LueLevel);
                Sl.SetLueMajorCode(ref LueMajor);
                Sl.SetLueFacCode(ref LueFacCode, string.Empty);
                Sl.SetLueEmployeeEducationField(ref LueField);
                
                SetGrd3();
                LueLevel.Visible = false;
                LueMajor.Visible = false;
                LueFacCode.Visible = false;
                LueField.Visible = false;

                tabControl1.SelectTab("TpgEmployeeCompetence");
                SetGrd5();
                Sl.SetLueCompetenceCode(ref LueCompetenceCode);
                LueCompetenceCode.Visible = false;

                tabControl1.SelectTab("TpgTraining");
                SetGrd4();
                LueTrainingCode.Visible = false;

                tabControl1.SelectTab("TpgEmployeeSS");
                Sl.SetLueDeptCode(ref LueDeptCode2);
                DteInterviewDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteStartWorkDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());

                tabControl1.SelectTab("TpgStage");
                SetGrd8();
                Sl.SetLueOption(ref LueProgress, "PersonalInformationProgress");
                LueProgress.Visible = false;
                DteStageDt.Visible = false;

                tabControl1.SelectTab("TpgGeneral");

                if (!mIsPersonalInformationUseStage) tabControl1.TabPages.Remove(TpgStage);
                if (!mIsEmployeeAllowToUploadFile) tabControl1.TabPages.Remove(TpPicture);
                if (!mIsPersonalInformationUseEmployeeBenefit) tabControl1.TabPages.Remove(TpgEmployeeBenefit);
                //if this application is called from other application
                if (mEmpCode.Length != 0)
                {
                    ShowData(mEmpCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
             if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region SetGrd1

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Family Name",
                        "Status Code",
                        "Status",
                        "Gender Code",
                        "Gender",
                        
                        //6-10
                        "Birth Date",
                        "ID Card",
                        "National"+Environment.NewLine+"Identity#",
                        "Remark",
                        "Education Level Code",

                        //11-13
                        "Latest Education",
                        "Profession Code",
                        "Proffesion"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        200, 0, 100, 0, 80, 

                        //6-10
                        80, 150, 150, 300, 100,

                        //11-13
                        150, 100, 150
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColInvisible(Grd1, new int[] {0, 2, 4, 10, 12}, false);
            Grd1.Cols[13].Move(7);
            Grd1.Cols[11].Move(8);
            
        }
        #endregion

        #region SetGrd2
        private void SetGrd2()
        {
            Grd2.Cols.Count = 5;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "D No",
                        //1-4
                        "Company",
                        "Position",
                        "Period",
                        "Remark"
                    },
                     new int[] 
                    {
                        0, 150, 150, 150, 250,
                    }
                );

            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);
        }
        #endregion

        #region Setgrd3
        private void SetGrd3()
        {
            Grd3.Cols.Count = 13;
            Grd3.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "School",
                        "Level Code",
                        "Level",
                        "Major Code",
                        "Major",

                        //6-10
                        "Field Code",
                        "Field",
                        "Graduation Year",
                        "Highest",
                        "Remark",

                        //11-12
                        "Faculty Code",
                        "Faculty"
                    },
                     new int[]
                    {
                        //0
                        0, 
                        
                        //1-5
                        250, 0, 180, 0, 180, 
                        
                        //6-10
                        0, 180, 100, 80, 300,

                        //11-12
                        0, 200
                    }
                );
            Sm.GrdColCheck(Grd3, new int[] { 9 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 4, 6, 11 }, false);
            Grd3.Cols[12].Move(8);
        }
        #endregion

        #region Setgrd4

        private void SetGrd4()
        {
            Grd4.Cols.Count = 9;
            Grd4.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd4,
                new string[]
                {
                    //0
                    "DNo",
                    
                    //1-5
                    "TrainingCode",
                    "Training",
                    "Specialization",
                    "Place",
                    "Period",

                    //6-8
                    "Year",
                    "Remark",
                    "Training Evaluation#"
                },
                new int[]
                {
                    0,
                    100, 180, 200, 200, 150,
                    120, 300, 130
                }
            );
            Sm.GrdColInvisible(Grd4, new int[] { 0, 1, 8 }, false);
            
        }
        #endregion

        #region Setgrd5

        private void SetGrd5()
        {
            Grd5.Cols.Count = 5;
            Grd5.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-4
                        "Code",
                        "Name",
                        "Description",
                        "Score"
                    },
                     new int[] 
                    {
                        0, 
                        0, 150, 300, 100
                    }
                );

            Sm.GrdColInvisible(Grd5, new int[] { 0, 1 }, false);
            Sm.GrdFormatDec(Grd5, new int[] { 4 }, 0);
        }

        #endregion

        #region SetGrd6

        private void SetGrd6()
        {
            Grd6.Cols.Count = 5;
            Grd6.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[]
                    {
                        //0
                        "D No",
                        //1-4
                        "File Name",
                        "U",
                        "D",
                        "File Name2"
                    },
                     new int[]
                    {
                        0,
                        250, 20, 20, 250
                    }
                );

            Sm.GrdColInvisible(Grd6, new int[] { 0, 4 }, false);
            Sm.GrdColButton(Grd6, new int[] { 2 }, "1");
            Sm.GrdColButton(Grd6, new int[] { 3 }, "2");
            Sm.GrdColReadOnly(Grd6, new int[] { 0, 1, 4 });
        }

        #endregion

        #region SetGrd7

        private void SetGrd7()
        {
            Grd7.Cols.Count = 5;
            Grd7.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd7,
                    new string[] 
                    {
                        //0
                        "D No",
                        //1-4
                        "Name",
                        "Phone",
                        "Address",
                        "Remark"
                    },
                     new int[] 
                    {
                        0, 
                        150, 150, 250, 250,
                    }
                );

            Sm.GrdColInvisible(Grd7, new int[] { 0 }, false);
        }

        #endregion

        #region SetGrd8

        private void SetGrd8()
        {
            Grd8.Cols.Count = 5;
            Grd8.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd8,
                    new string[]
                    {
                        //0
                        "DNo",
                        
                        //1-3
                        "Progress Code",
                        "Progress",
                        "Date",
                        "Remark",
                    },
                     new int[]
                    {
                        0,
                        0, 150, 150, 250,
                    }
                );
            Sm.GrdFormatDate(Grd8, new int[] { 3 });
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtEmpName, TxtDisplayName, TxtUserCode, LueDeptCode, LuePosCode,
                        DteJoinDt,DteResignDt,TxtIdNumber,LueGender,LueReligion,
                        TxtBirthPlace,DteBirthDt,MeeAddress,LueCity,TxtPostalCode,
                        LueSiteCode, MeeDomicile, LueBloodType, LueSection, LueWorkGroup,
                        TxtPhone,TxtMobile,TxtEmail,LueGrdLvlCode,TxtNPWP,
                        LuePayrollType,LuePTKP,LueBankCode, TxtBankBranch, TxtBankAcName, 
                        TxtBankAcNo, LueStatus,LueFamGender,DteFamBirthDt, TxtEmpCodeOld, 
                        TxtShortCode, TxtBarcodeCode, LuePayrunPeriod, LueEmploymentStatus, TxtSubDistrict, 
                        TxtVillage, TxtRTRW, TxtMother, LueMaritalStatus, DteWeddingDt,
                        LueSystemType, LueDivisionCode, LuePGCode, TxtPOH,TxtInterviewerBy,DteInterviewDt,TxtPlace,TxtGrossSalary,
                        DteStartWorkDt,TxtPlacement,LueDeptCode2,TxtProbation, TxtEmpRequestDocNo, LueProgress, DteStageDt, LueTrainingCode, LueFacCode, LueField,
                        LueProfessionCode, LueEducationLevelCode, TxtJobIncentives, TxtTHP, TxtInsurance, TxtHolidayAllowance, TxtAnnualLeave, TxtBonus, TxtTax, MeeRemark
                    }, true);
                    Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = Grd5.ReadOnly = Grd6.ReadOnly = Grd7.ReadOnly = Grd8.ReadOnly = true;
                    TxtDocNo.Focus();
                    BtnEmpRequest.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtEmpName, TxtDisplayName, LueDeptCode, LuePosCode,LueBloodType,TxtMother,
                        TxtNPWP,TxtSubDistrict,TxtPostalCode,MeeDomicile,TxtIdNumber, 
                        LueGender, LueReligion, TxtBirthPlace,DteBirthDt,MeeAddress,TxtRTRW, TxtVillage, 
                        LueCity, TxtPhone,TxtMobile,LueMaritalStatus, TxtEmail, LueStatus, LueFamGender, DteFamBirthDt, LueLevel,
                        TxtInterviewerBy,DteInterviewDt,TxtPlace,TxtGrossSalary,DteStartWorkDt,TxtPlacement,
                        LueDeptCode2,TxtProbation, LueProgress, DteStageDt, LueTrainingCode, LueFacCode, LueField,
                        LueProfessionCode, LueEducationLevelCode, TxtJobIncentives, TxtTHP, TxtInsurance, TxtHolidayAllowance, TxtAnnualLeave, TxtBonus, TxtTax, MeeRemark
                    }, false);
                    Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = Grd5.ReadOnly = Grd6.ReadOnly = Grd7.ReadOnly = Grd8.ReadOnly = false;
                    DteDocDt.Focus();
                    BtnEmpRequest.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtDocNo, true);

                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEmpName, TxtDisplayName, LueDeptCode, LuePosCode,LueBloodType,TxtMother,
                        TxtNPWP,TxtSubDistrict,TxtPostalCode,MeeDomicile,TxtIdNumber,
                        LueGender, LueReligion, TxtBirthPlace,DteBirthDt,MeeAddress,TxtRTRW, TxtVillage, LueCity, 
                        TxtPhone,TxtMobile,LueMaritalStatus, TxtEmail, LueStatus, LueFamGender, DteFamBirthDt,LueLevel,
                        TxtInterviewerBy,DteInterviewDt,TxtPlace,TxtGrossSalary,DteStartWorkDt,
                        TxtPlacement,LueDeptCode2,TxtProbation, LueProgress, DteStageDt, LueTrainingCode, LueFacCode, LueField,
                        LueProfessionCode, LueEducationLevelCode, TxtJobIncentives, TxtTHP, TxtInsurance, TxtHolidayAllowance, TxtAnnualLeave, TxtBonus, TxtTax, MeeRemark
                    }, false);

                    if (mIsPPSActive)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueDeptCode, LuePosCode, 
                            //LueGrdLvlCode, LueEmploymentStatus, LueSystemType, 
                            //LuePayrunPeriod 
                        }, true);
                    }

                    Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = Grd5.ReadOnly = Grd6.ReadOnly = Grd7.ReadOnly = Grd8.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtEmpName, TxtDisplayName, TxtUserCode, LueDeptCode, LuePosCode,
                DteJoinDt, DteResignDt, TxtIdNumber, LueGender, LueReligion,
                TxtBirthPlace, DteBirthDt, MeeAddress, LueCity, TxtPostalCode,
                LueSiteCode, MeeDomicile, LueBloodType, LueSection, LueWorkGroup,
                TxtPhone, TxtMobile, TxtEmail, LueGrdLvlCode, TxtNPWP,
                LuePayrollType, LuePTKP, LueBankCode, TxtBankBranch, TxtBankAcName, 
                TxtBankAcNo, LueStatus, LueFamGender, DteFamBirthDt, 
                TxtEmpCodeOld, TxtShortCode, TxtBarcodeCode, LuePayrunPeriod, LueEmploymentStatus,
                TxtSubDistrict, TxtVillage, TxtRTRW, TxtMother, LueMaritalStatus,
                DteWeddingDt, LueSystemType, LueDivisionCode, LuePGCode, TxtPOH, TxtInterviewerBy,DteInterviewDt,TxtPlace,TxtGrossSalary,
                DteStartWorkDt,TxtPlacement,LueDeptCode2,TxtProbation, TxtEmpRequestDocNo, LueProgress, DteStageDt, LueTrainingCode, 
                LueFacCode, LueField, LueMajor, LueProfessionCode, LueEducationLevelCode, TxtInsurance, TxtHolidayAllowance, TxtAnnualLeave, TxtBonus, TxtTax, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtGrossSalary, TxtJobIncentives, TxtTHP }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.ClearGrd(Grd2, true);
            ClearGrd3();
            Sm.ClearGrd(Grd4, true);
            ClearGrd5();
            Sm.ClearGrd(Grd6, true);
            Sm.ClearGrd(Grd7, true);
            Sm.ClearGrd(Grd8, true);
            mProfilePicture = string.Empty;

        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd3, 0, new int[] { 9 });
        }

        private void ClearGrd5()
        {
            Grd5.Rows.Clear();
            Grd5.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
               if (FrmFind == null) FrmFind = new FrmEmployeeRecruitmentFind(this);
               Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteInterviewDt);
                Sm.SetDteCurrentDate(DteStartWorkDt);
                mIsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mIsInsert = false;
            mIsEdit = true;
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() 
                { 
                    CommandText =
                        "Delete From TblEmployeeFamilyRecruitment Where DocNo=@DocNo; " +
                        "Delete From TblEmployeeWorkExpRecruitment Where DocNo=@DocNo; " +
                        "Delete From TblEmployeeEducationRecruitment Where DocNo=@DocNo; " +
                        "Delete From TblEmployeeCompetenceRecruitment Where DocNo=@DocNo; " +
                        "Delete From tblemployeeTrainingRecruitment Where DocNo=@DocNo; " +
                      //  "Delete From TblEmployeeSalary Where EmpCode=@EmpCode; " +
                      //  "Delete From TblEmployeeAllowanceDeductionRecruitment Where DocNo=@DocNo; " +
                        "Delete From TblEmployeePersonalReferenceRecruitment Where DocNo=@DocNo; "
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string DocNo = string.Empty;
                if (TxtDocNo.Text.Length == 0)
                {
                    DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmployeeRecruitment", "TblEmployeeRecruitment");
                }
                else
                {
                    DocNo = TxtDocNo.Text;
                }


                var cml = new List<MySqlCommand>();


                cml.Add(SaveEmployeeRecruitment(DocNo));

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveFamilyEmployeeRecruitment(Row, DocNo));

                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveEmployeeWorkExpRecruitment(Row, DocNo));

                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveEmployeeEducationRecruitment(Row, DocNo));

                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0) cml.Add(SaveEmployeeTrainingRecruitment(Row, DocNo));

                for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0) cml.Add(SaveEmployeeCompetenceRecruitment(Row, DocNo));

                for (int Row = 0; Row < Grd7.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd7, Row, 1).Length > 0) cml.Add(SaveEmployeePersonalReferenceRecruitment(Row,DocNo));

                if (mIsPersonalInformationUseStage)
                {
                    for (int Row = 0; Row < Grd8.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd8, Row, 1).Length > 0) cml.Add(SaveEmployeeStageRecruitment(Row, DocNo));
                }

                cml.Add(SaveEmployeeFile(DocNo));

                Sm.ExecCommands(cml);

                for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd6, Row, 1).Length > 0)
                    {
                        if (mIsEmployeeAllowToUploadFile && Sm.GetGrdStr(Grd6, Row, 1).Length > 0 && Sm.GetGrdStr(Grd6, Row, 1) != "openFileDialog1")
                        {
                            if (Sm.GetGrdStr(Grd6, Row, 1) != Sm.GetGrdStr(Grd6, Row, 4))
                            {
                                UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd6, Row, 1));
                            }
                        }
                    }
                }

                ShowData(DocNo);
                
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                    Sm.StdMsgYN("Print", "") == DialogResult.No
                    ) return;

                ParPrint(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmployeeRecruitment(DocNo);
                ShowEmployeeFamilyRecruitment(DocNo);
                ShowEmployeeWorkExpRecruitment(DocNo);
                ShowEmployeeEducationRecruitment(DocNo);
                ShowEmployeeCompetenceRecruitment(DocNo);
                ShowEmployeeTrainingRecruitment(DocNo);
                ShowEmployeePersonalReferenceRecruitment(DocNo);
                ShowEmployeeFile(DocNo);
                if (mIsPersonalInformationUseStage)
                    ShowEmployeeStageRecruitment(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmployeeRecruitment(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

           Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblEmployeeRecruitment Where DocNo=@DocNo;",
                        new string[] 
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt","EmpName", "DisplayName", "UserCode", "DeptCode", 

                            //6-10
                            "PosCode", "JoinDt","ResignDt","IdNumber","Gender",

                            //11-15
                            "Religion", "BirthPlace","BirthDt","Address","CityCode",

                            //16-20
                            "PostalCode", "Phone","Mobile","GrdLvlCode","Email",

                            //21-25
                            "NPWP", "PayrollType","PTKP","BankCode","BankBranch",

                            //26-30
                            "BankAcName", "BankAcNo","EmpCodeOld", "ShortCode", "BarcodeCode",  

                            //31-35
                            "PayrunPeriod","EmploymentStatus","Mother", "SubDistrict", "Village", 

                            //36-40
                             "RTRW", "MaritalStatus","WeddingDt", "SystemType", "SiteCode", 
                            
                            //41-45
                             "Domicile", "BloodType", "SectionCode", "WorkGroupCode", "DivisionCode", 

                             //46-50
                             "PGCode", "POH",  "InterviewerBy", "Place", "InterviewDt",
                            
                             //51-55
                             "GrossSalary", "StartWorkDt", "Placement", "DeptCode2", "Probation",

                             //56-60
                             "EmployeeRequestDocNo", "JobIncentives", "TotalTHP", "Insurance", "HolidayAllowance",

                             //61-64
                             "AnnualLeave", "Bonus", "Tax", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            TxtEmpName.EditValue = Sm.DrStr(dr, c[2]);
                            TxtDisplayName.EditValue = Sm.DrStr(dr, c[3]);
                            TxtUserCode.EditValue = Sm.DrStr(dr, c[4]);
                            Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[5]));
                            Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[6]));
                            Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[7]));
                            Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[8]));
                            TxtIdNumber.EditValue = Sm.DrStr(dr, c[9]);
                            Sm.SetLue(LueGender, Sm.DrStr(dr, c[10]));
                            Sm.SetLue(LueReligion, Sm.DrStr(dr, c[11]));
                            TxtBirthPlace.EditValue = Sm.DrStr(dr, c[12]);
                            Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[13]));
                            MeeAddress.EditValue = Sm.DrStr(dr, c[14]);
                            Sm.SetLue(LueCity, Sm.DrStr(dr, c[15]));
                            TxtPostalCode.EditValue = Sm.DrStr(dr, c[16]);
                            TxtPhone.EditValue = Sm.DrStr(dr, c[17]);
                            TxtMobile.EditValue = Sm.DrStr(dr, c[18]);
                            Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[19]));
                            TxtEmail.EditValue = Sm.DrStr(dr, c[20]);
                            TxtNPWP.EditValue = Sm.DrStr(dr, c[21]);
                            Sm.SetLue(LuePayrollType, Sm.DrStr(dr, c[22]));
                            Sm.SetLue(LuePTKP, Sm.DrStr(dr, c[23]));
                            Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[24]));
                            TxtBankBranch.EditValue = Sm.DrStr(dr, c[25]);
                            TxtBankAcName.EditValue = Sm.DrStr(dr, c[26]);
                            TxtBankAcNo.EditValue = Sm.DrStr(dr, c[27]);
                            TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[28]);
                            TxtShortCode.EditValue = Sm.DrStr(dr, c[29]);
                            TxtBarcodeCode.EditValue = Sm.DrStr(dr, c[30]);
                            Sm.SetLue(LuePayrunPeriod, Sm.DrStr(dr, c[31]));
                            Sm.SetLue(LueEmploymentStatus, Sm.DrStr(dr, c[32]));
                            TxtMother.EditValue = Sm.DrStr(dr, c[33]);
                            TxtSubDistrict.EditValue = Sm.DrStr(dr, c[34]);
                            TxtVillage.EditValue = Sm.DrStr(dr, c[35]);
                            TxtRTRW.EditValue = Sm.DrStr(dr, c[36]);
                            Sm.SetLue(LueMaritalStatus, Sm.DrStr(dr, c[37]));
                            Sm.SetDte(DteWeddingDt, Sm.DrStr(dr, c[38]));
                            Sm.SetLue(LueSystemType, Sm.DrStr(dr, c[39]));
                            SetLueSite(ref LueSiteCode, Sm.DrStr(dr, c[40]));
                            Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[40]));
                            MeeDomicile.EditValue = Sm.DrStr(dr, c[41]);
                            Sm.SetLue(LueBloodType, Sm.DrStr(dr, c[42]));
                            Sm.SetLue(LueSection, Sm.DrStr(dr, c[43]));
                            Sm.SetLue(LueWorkGroup, Sm.DrStr(dr, c[44]));
                            Sm.SetLue(LueDivisionCode, Sm.DrStr(dr, c[45]));
                            if(mIsDeptFilterByDivision)
                                Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[46]));
                            TxtPOH.EditValue = Sm.DrStr(dr, c[47]);
                            TxtInterviewerBy.EditValue = Sm.DrStr(dr, c[48]);
                            TxtPlace.EditValue = Sm.DrStr(dr, c[49]);
                            Sm.SetDte(DteInterviewDt, Sm.DrStr(dr, c[50]));
                            TxtGrossSalary.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[51]), 0);
                            Sm.SetDte(DteStartWorkDt, Sm.DrStr(dr, c[52]));
                            TxtPlacement.EditValue = Sm.DrStr(dr, c[53]);
                            Sm.SetLue(LueDeptCode2, Sm.DrStr(dr, c[54]));
                            TxtProbation.EditValue = Sm.DrStr(dr, c[55]);
                            TxtEmpRequestDocNo.EditValue = Sm.DrStr(dr, c[56]);
                            if (mIsPersonalInformationUseEmployeeBenefit)
                            {
                                TxtJobIncentives.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[57]), 0);
                                TxtTHP.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[58]), 0);
                                TxtInsurance.EditValue = Sm.DrStr(dr, c[59]);
                                TxtHolidayAllowance.EditValue = Sm.DrStr(dr, c[60]);
                                TxtAnnualLeave.EditValue = Sm.DrStr(dr, c[61]);
                                TxtBonus.EditValue = Sm.DrStr(dr, c[62]);
                                TxtTax.EditValue = Sm.DrStr(dr, c[63]);
                                MeeRemark.EditValue = Sm.DrStr(dr, c[64]);
                            }

                        }, true
                    );
            }

        private void ShowEmployeeFamilyRecruitment(string DocNo)
         {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.FamilyName, ");
            SQL.AppendLine("A.Status, B.OptDesc as StatusDesc, A.Gender, C.OptDesc As GenderDesc, ");
            SQL.AppendLine("A.BirthDt, A.IDNo, A.NIN, A.Remark, ");
            SQL.AppendLine("A.EducationLevelCode, D.OptDesc As EducationLevel, A.ProfessionCode, E.OptDesc As Profession ");
            SQL.AppendLine("From TblEmployeeFamilyRecruitment A ");
            SQL.AppendLine("Left Join TblOption B On A.Status=B.OptCode and B.OptCat='FamilyStatus' ");
            SQL.AppendLine("Left Join TblOption C On A.Gender=C.OptCode and C.OptCat='Gender' ");
            SQL.AppendLine("Left Join TblOption D On A.EducationLevelCode=D.OptCode and D.OptCat='EmployeeEducationLevel' ");
            SQL.AppendLine("Left Join TblOption E On A.ProfessionCode=E.OptCode and E.OptCat='Profession' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.FamilyName;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
             Sm.ShowDataInGrid(
                     ref Grd1, ref cm, SQL.ToString(),
                     new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "FamilyName", "Status", "StatusDesc", "Gender", "GenderDesc", 
                        
                        //6-10
                        "BirthDt", "IDNo", "NIN", "Remark", "EducationLevelCode",

                        //11-13
                        "EducationLevel", "ProfessionCode", "Profession"
                    },
                     (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                     {
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                         Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 6);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                     }, false, false, true, false
             );
             Sm.FocusGrd(Grd1, 0, 1);
         }

        private void ShowEmployeeWorkExpRecruitment(string DocNo)
         {
             var cm = new MySqlCommand();

             Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
             Sm.ShowDataInGrid(
                     ref Grd2, ref cm,
                    "select DNo, Company,Position,Period,Remark from  TblEmployeeWorkExpRecruitment Where DocNo=@DocNo Order By Company",

                     new string[] 
                    { 
                        "Dno",
                        "Company", "Position", "Period","Remark"
                    },
                     (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                     {
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 2);
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 3);
                         Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 4);
                     }, false, false, true, false
             );
             Sm.FocusGrd(Grd2, 0, 0);
         }

        private void ShowEmployeeEducationRecruitment(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.School, A.Level as LevelCode, B.OptDesc As Level, ");
            SQL.AppendLine("A.Major, E.MajorName, A.Field As FieldCode, C.OptDesc AS FieldName, A.FacCode, D.FacName, A.GraduationYr, A.HighestInd, A.Remark ");
            SQL.AppendLine("From  TblEmployeeEducationRecruitment A ");
            SQL.AppendLine("Left Join TblOption B On A.Level=B.OptCode and B.OptCat='EmployeeEducationLevel' ");
            SQL.AppendLine("LEFT JOIN tbloption C ON A.Field = C.OptCode AND C.OptCat ='EmployeeEducationField' ");
            SQL.AppendLine("LEFT JOIN TblFaculty D On A.FacCode = D.FacCode ");
            SQL.AppendLine("LEFT JOIN TblMajor E On A.Major = E.MajorCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.HighestInd Desc, A.Level ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                 ref Grd3, ref cm, SQL.ToString(),
                 new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "School", "LevelCode", "Level", "Major", "MajorName", 

                    //6-10
                    "FieldCode", "FieldName", "FacCode", "FacName", "GraduationYr", 

                    //11-12
                    "HighestInd", "Remark"
                },
                 (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                 {
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 10);
                     Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 11);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 12);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);

                 }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
         }

        private void ShowEmployeeCompetenceRecruitment(string DocNo)
        {
            var SQLComp = new StringBuilder();
            SQLComp.AppendLine("Select A.DNo, A.CompetenceCode, B.CompetenceName, A.Description, A.Score ");
            SQLComp.AppendLine("From TblEmployeeCompetenceRecruitment A ");
            SQLComp.AppendLine("Inner Join TblCompetence B On A.CompetenceCode = B.CompetenceCode ");
            SQLComp.AppendLine("Where A.DocNo=@DocNo ");
            SQLComp.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd5, ref cm, SQLComp.ToString(),
                    new string[] 
                    { 
                        "DNo",
                        "CompetenceCode", "CompetenceName", "Description", "Score"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd5, 0, 0);
        }

        private void ShowEmployeeTrainingRecruitment(string DocNo)
         {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DNo, A.TrainingCode, B.TrainingName, A.SpecializationTraining, A.Place, A.Period, A.Yr, A.Remark ");
            SQL.AppendLine("FROM tblemployeetrainingrecruitment A ");
            SQL.AppendLine("LEFT JOIN tbltraining B ON A.TrainingCode = B.TrainingCode ");
            SQL.AppendLine("Where DocNo = @DocNo Order By DNo ");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo",
                    
                    //1-5
                    "TrainingCode",
                    "TrainingName",
                    "SpecializationTraining",
                    "Place",
                    "Period",

                    //6-7
                    "Yr",
                    "Remark"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    //Sm.SetGrdValue("S", Grd4, dr, c, Row, 8, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
         }

        private void ShowEmployeePersonalReferenceRecruitment(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd7, ref cm,
                   "select DNo, Name,Phone,Address,Remark from  TblEmployeePersonalReferenceRecruitment Where DocNo=@DocNo Order By Dno",

                    new string[] 
                    { 
                        "Dno",
                        "name", "Phone", "Address","Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd7, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd7, 0, 0);
        }

        public void ShowEmployeeFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd6, ref cm,
                   "select DNo, FileName from  TblEmployeeRecruitmentFile Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd6, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd6, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd6, dr, c, Row, 4, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ShowEmployeeStageRecruitment(string DocNo)
        {
            var SQLStage = new StringBuilder();
            SQLStage.AppendLine("Select A.DNo, A.ProgressCode, B.OptDesc As Progress, ");
            SQLStage.AppendLine("A.Date, A.Remark ");
            SQLStage.AppendLine("From  TblEmployeeStageRecruitment A ");
            SQLStage.AppendLine("Left Join TblOption B On A.ProgressCode=B.OptCode and B.OptCat='PersonalInformationProgress' ");
            SQLStage.AppendLine("Where A.DocNo=@DocNo ");
            SQLStage.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd8, ref cm, SQLStage.ToString(),

                    new string[]
                    {
                        "DNo",
                        "ProgressCode", "Progress", "Date", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd8, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd8, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd8, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("D", Grd8, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd8, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd8, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtEmpName, "Employee name", false) ||
                IsGrd1ValueNotValid()||
                IsGrd2ValueNotValid()||
                IsGrd3ValueNotValid()||
                IsGrd4ValueNotValid() ||
                IsUploadFileNotValid()
                ;
        }

        private bool IsGrd1ValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Gender is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Familly Name is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsGrd2ValueNotValid()
        {
            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 1, false, "Company is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsGrd3ValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "School is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 2, false, "Level is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsGrd4ValueNotValid()
        {
            if (Grd4.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd4, Row, 1, false, "Specialization Training is empty.")) return true;
                   
                }
            }
            return false;
        }

        private MySqlCommand SaveEmployeeRecruitment(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeRecruitment(DocNo, DocDt, EmpName, DisplayName, UserCode, EmpCodeOld, ShortCode, DeptCode, PosCode, ");
            SQL.AppendLine("JoinDt, ResignDt, IdNumber, MaritalStatus, WeddingDt, Gender, Religion, Mother, BirthPlace, BirthDt, Address, ");
            SQL.AppendLine("CityCode, SubDistrict, Village, RTRW, PostalCode, SiteCode, SectionCode, WorkGroupCode, Domicile, BloodType, Phone, Mobile, Email,GrdLvlCode,NPWP, SystemType, PayrollType, ");
            SQL.AppendLine("PTKP,BankCode,BankAcName,BankAcNo,BankBranch, ");
            SQL.AppendLine("PayrunPeriod, EmploymentStatus, DivisionCode, PGCode, POH, InterviewerBy, Place, InterviewDt, GrossSalary, StartWorkDt, Placement, DeptCode2, Probation, EmployeeRequestDocNo, ");
            SQL.AppendLine("JobIncentives, TotalTHP, Insurance, HolidayAllowance, AnnualLeave, Bonus, Tax, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @EmpName, @DisplayName, @UsCode, @EmpCodeOld, @ShortCode, @DeptCode, @PosCode, ");
            SQL.AppendLine("@JoinDt, @ResignDt, @IdNumber, @MaritalStatus, @WeddingDt, @Gender, @Religion, @Mother, @BirthPlace, @BirthDt, @Address, ");
            SQL.AppendLine("@CityCode, @SubDistrict, @Village, @RTRW, @PostalCode, @SiteCode, @SectionCode, @WorkGroupCode, @Domicile, @BloodType, @Phone, @Mobile, @Email, @GrdLvlCode, @NPWP, @SystemType, @PayrollType,@PTKP,@BankCode,@BankAcName,@BankAcNo,@BankBranch, ");
            SQL.AppendLine("@PayrunPeriod, @EmploymentStatus, @DivisionCode, @PGCode, @POH, @InterviewerBy, @Place,  @InterviewDt, @GrossSalary, @StartWorkDt, @Placement, @DeptCode2, @Probation, @EmployeeRequestDocNo, ");
            SQL.AppendLine("@JobIncentives, @TotalTHP, @Insurance, @HolidayAllowance, @AnnualLeave, @Bonus, @Tax, @Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("    EmpName=@EmpName, DisplayName=@DisplayName, UserCode=@UsCode, EmpCodeOld=@EmpCodeOld, ShortCode=@ShortCode, DeptCode=@DeptCode, PosCode=@PosCode, ");
            SQL.AppendLine("    JoinDt=@JoinDt, ResignDt=@ResignDt, IdNumber=@IdNumber, MaritalStatus=@MaritalStatus, WeddingDt=@WeddingDt, Gender=@Gender, ");
            SQL.AppendLine("    Religion=@Religion, Mother=@Mother, BirthPlace=@BirthPlace, BirthDt=@BirthDt, Address=@Address, ");
            SQL.AppendLine("    CityCode=@CityCode, SubDistrict=@SubDistrict, Village=@Village, RTRW=@RTRW, PostalCode=@PostalCode, SiteCode=@SiteCode, SectionCode=@SectionCode, WorkGroupCode=@WorkGroupCode, Domicile=@Domicile, BloodType=@BloodType, Phone=@Phone, Mobile=@Mobile,Email=@Email, ");
            SQL.AppendLine("    GrdLvlCode=@GrdLvlCode, NPWP=@NPWP, SystemType=@SystemType, PayrollType=@PayrollType, PTKP=@PTKP, BankCode=@BankCode,BankAcName=@BankAcName,BankAcNo=@BankAcNo,BankBranch=@BankBranch, ");
            SQL.AppendLine("    PayrunPeriod=@PayrunPeriod, EmploymentStatus=@EmploymentStatus, DivisionCode=@DivisionCode, PGCode=@PGCode, POH=@POH, InterviewerBy=@InterviewerBy, Place=@Place, InterviewDt=@InterviewDt, GrossSalary=@GrossSalary, StartWorkDt=@StartWorkDt, Placement=@Placement, DeptCode2=@DeptCode2, Probation=@Probation, ");
            SQL.AppendLine("    JobIncentives=@JobIncentives, TotalTHP=@TotalTHP, Insurance=@Insurance, HolidayAllowance=@HolidayAllowance, AnnualLeave=@AnnualLeave, Bonus=@Bonus, Tax=@Tax, Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

           

            if (!mIsInsert)
            {
                SQL.AppendLine("Delete From TblEmployeeFamilyRecruitment Where DocNo=@DocNo; ");
                SQL.AppendLine("Delete From TblEmployeeWorkExpRecruitment Where DocNo=@DocNo; ");
                SQL.AppendLine("Delete From TblEmployeeEducationRecruitment Where DocNo=@DocNo; ");
                SQL.AppendLine("Delete From TblEmployeeTrainingRecruitment Where DocNo=@DocNo; ");
                SQL.AppendLine("Delete From TblEmployeeCompetenceRecruitment Where DocNo=@DocNo; ");
                SQL.AppendLine("Delete From TblEmployeePersonalReferenceRecruitment Where DocNo=@DocNo; ");
                SQL.AppendLine("Delete From TblEmployeeStageRecruitment Where DocNo=@DocNo; ");
            }

            if (TxtEmpRequestDocNo.Text.Length > 0 )
            {
                SQL.AppendLine("Update TblEmployeeRequest A ");
                SQL.AppendLine("SET A.ProcessInd = (Select Case  ");
                SQL.AppendLine("When Count(*) = 0 Then 'O'  ");
                SQL.AppendLine("When Count(*) = A.Amount Then 'F' ");
                SQL.AppendLine("Else 'P' ");
                SQL.AppendLine("End As processInd ");
                SQL.AppendLine("from TblEmployeeRecruitment Where EmployeeRequestDocNo=@EmployeeRequestDocNo  ) ");
                SQL.AppendLine("Where A.DocNo = @EmployeeRequestDocNo ");
            }

           
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpName", TxtEmpName.Text);
            Sm.CmParam<String>(ref cm, "@DisplayName", TxtDisplayName.Text);
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.CmParam<String>(ref cm, "@UsCode", TxtUserCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpCodeOld", TxtEmpCodeOld.Text);
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@SectionCode", Sm.GetLue(LueSection));
            Sm.CmParam<String>(ref cm, "@WorkGroupCode", Sm.GetLue(LueWorkGroup)); 
            Sm.CmParam<String>(ref cm, "@Domicile", MeeDomicile.Text);
            Sm.CmParam<String>(ref cm, "@BloodType", Sm.GetLue(LueBloodType));
            Sm.CmParamDt(ref cm, "@JoinDt", Sm.GetDte(DteJoinDt));
            Sm.CmParamDt(ref cm, "@ResignDt", Sm.GetDte(DteResignDt));
            Sm.CmParam<String>(ref cm, "@IdNumber", TxtIdNumber.Text);
            Sm.CmParam<String>(ref cm, "@MaritalStatus", Sm.GetLue(LueMaritalStatus));
            Sm.CmParamDt(ref cm, "@WeddingDt", Sm.GetDte(DteWeddingDt));
            Sm.CmParam<String>(ref cm, "@Gender", Sm.GetLue(LueGender));
            Sm.CmParam<String>(ref cm, "@Religion", Sm.GetLue(LueReligion));
            Sm.CmParam<String>(ref cm, "@Mother", TxtMother.Text);
            Sm.CmParam<String>(ref cm, "@BirthPlace", TxtBirthPlace.Text);
            Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetDte(DteBirthDt));
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCity));
            Sm.CmParam<String>(ref cm, "@SubDistrict", TxtSubDistrict.Text);
            Sm.CmParam<String>(ref cm, "@Village", TxtVillage.Text);
            Sm.CmParam<String>(ref cm, "@RTRW", TxtRTRW.Text);
            Sm.CmParam<String>(ref cm, "@PostalCode", TxtPostalCode.Text);
            Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@Mobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@Email", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetLue(LueGrdLvlCode));
            Sm.CmParam<String>(ref cm, "@NPWP", TxtNPWP.Text);
            Sm.CmParam<String>(ref cm, "@SystemType", Sm.GetLue(LueSystemType));
            Sm.CmParam<String>(ref cm, "@PayrollType", Sm.GetLue(LuePayrollType));
            Sm.CmParam<String>(ref cm, "@PTKP", Sm.GetLue(LuePTKP));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@BankBranch", TxtBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@BankAcName", TxtBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@BankAcNo", TxtBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", Sm.GetLue(LuePayrunPeriod));
            if (mIsEmployeeCOAInfoMandatory)
            {
                Sm.CmParam<String>(ref cm, "@EmployeeAcNo", mEmployeeAcNo);
                Sm.CmParam<String>(ref cm, "@EmployeeAcDesc", mEmployeeAcDesc);
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DivisionCode", Sm.GetLue(LueDivisionCode));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@POH", TxtPOH.Text);
            Sm.CmParam<String>(ref cm, "@InterviewerBy", TxtInterviewerBy.Text);
            Sm.CmParam<String>(ref cm, "@Place", TxtPlace.Text);
            Sm.CmParamDt(ref cm, "@InterviewDt", Sm.GetDte(DteInterviewDt));
            Sm.CmParam<Decimal>(ref cm, "@GrossSalary", decimal.Parse(TxtGrossSalary.Text));
            Sm.CmParamDt(ref cm, "@StartWorkDt", Sm.GetDte(DteStartWorkDt));
            Sm.CmParam<String>(ref cm, "@Placement", TxtPlacement.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode2", Sm.GetLue(LueDeptCode2));
            Sm.CmParam<String>(ref cm, "@Probation", TxtProbation.Text);
            Sm.CmParam<String>(ref cm, "@EmployeeRequestDocNo", TxtEmpRequestDocNo.Text);
            if(mIsPersonalInformationUseEmployeeBenefit)
            {
                Sm.CmParam<Decimal>(ref cm, "@JobIncentives", decimal.Parse(TxtJobIncentives.Text));
                Sm.CmParam<Decimal>(ref cm, "@TotalTHP", decimal.Parse(TxtTHP.Text));
                Sm.CmParam<String>(ref cm, "@Insurance", TxtInsurance.Text);
                Sm.CmParam<String>(ref cm, "@HolidayAllowance", TxtHolidayAllowance.Text);
                Sm.CmParam<String>(ref cm, "@AnnualLeave", TxtAnnualLeave.Text);
                Sm.CmParam<String>(ref cm, "@Bonus", TxtBonus.Text);
                Sm.CmParam<String>(ref cm, "@Tax", TxtTax.Text);
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            }

            return cm;
        }

        private MySqlCommand SaveFamilyEmployeeRecruitment(int Row, String DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmployeeFamilyRecruitment(DocNo, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, Remark, EducationLevelCode, ProfessionCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @FamilyName, @Status, @Gender, @BirthDt, @IDNo, @NIN, @Remark, @EducationLevelCode, @ProfessionCode, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update FamilyName=@FamilyName, Status=@Status, Gender=@Gender, BirthDt=@BirthDt, IDNo=@IDNo, NIN=@NIN, Remark=@Remark, EducationLevelCode =@EducationLevelCode, ProfessionCode =@ProfessionCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            
            var cm = new MySqlCommand()
            { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@FamilyName", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Gender", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetGrdDate(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@IDNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@NIN", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@EducationLevelCode", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@ProfessionCode", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployeeWorkExpRecruitment(int Row, String DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmployeeWorkExpRecruitment(DocNo, DNo, Company, Position, Period, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) " );
            SQL.AppendLine("Values(@DocNo, @DNo, @Company, @Position, @Period, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()) " );
            SQL.AppendLine("On Duplicate Key " );
            SQL.AppendLine("Update " );
            SQL.AppendLine("Company=@Company, Position=@Position, Period=@Period, ");
            SQL.AppendLine("Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Company", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@Position", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@Period", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand SaveEmployeeEducationRecruitment(int Row, String DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeEducationRecruitment(DocNo, DNo, School, Level, Major, Field, FacCode, GraduationYr, HighestInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @School, @Level, @Major, @Field, @FacCode, @GraduationYr, @HighestInd, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("    School=@School, Level=@Level, Major=@Major, Field = @Field, FacCode = @FacCode, GraduationYr = @GraduationYr, HighestInd=@HighestInd, ");
            SQL.AppendLine("    Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@School", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@Level", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@Major", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@Field", Sm.GetGrdStr(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@FacCode", Sm.GetGrdStr(Grd3, Row, 11));
            Sm.CmParam<String>(ref cm, "@GraduationYr", Sm.GetGrdStr(Grd3, Row, 8));
            Sm.CmParam<String>(ref cm, "@HighestInd", Sm.GetGrdBool(Grd3, Row, 9)?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 10));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployeeCompetenceRecruitment(int Row, String DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeCompetenceRecruitment ");
            SQL.AppendLine("(DocNo, DNo, CompetenceCode, Description, Score, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @CompetenceCode, @Description, @Score, @UserCode, CurrentDateTime());");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd5, Row, 3));
            Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetGrdStr(Grd5, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Score", Sm.GetGrdDec(Grd5, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand SaveEmployeeTrainingRecruitment(int Row, String DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmployeeTrainingRecruitment(DocNo, DNo, TrainingCode, SpecializationTraining, Place, Period, Yr, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @TrainingCode, @SpecializationTraining, @Place, @Period, @Yr, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("    TrainingCode=@TrainingCode, SpecializationTraining=@SpecializationTraining, Place=@Place, Period=@Period, Yr=@Yr, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
                    
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<String>(ref cm, "@SpecializationTraining", Sm.GetGrdStr(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@Place", Sm.GetGrdStr(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@Period", Sm.GetGrdStr(Grd4, Row, 5));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd4, Row, 6));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd4, Row, 7));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployeePersonalReferenceRecruitment(int Row, String DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmployeePersonalReferenceRecruitment(DocNo, DNo, Name, Phone, Address, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Name, @Phone, @Address, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("Name=@Name, Phone=@Phone, Address=@Address, ");
            SQL.AppendLine("Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd7, Row, 1));
            Sm.CmParam<String>(ref cm, "@Phone", Sm.GetGrdStr(Grd7, Row, 2));
            Sm.CmParam<String>(ref cm, "@Address", Sm.GetGrdStr(Grd7, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd7, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand SaveEmployeeFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd6.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd6, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblEmployeeRecruitmentFile(DocNo, EmpUserCode, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @EmpCode, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd6, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtUserCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployeeStageRecruitment(int Row, String DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmployeeStageRecruitment(DocNo, DNo, ProgressCode, Date, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ProgressCode, @Date, @Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("ProgressCode=@ProgressCode, Date=@Date, Remark=@Remark, ");
            SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ProgressCode", Sm.GetGrdStr(Grd8, Row, 1));
            Sm.CmParamDt(ref cm, "@Date", Sm.GetGrdDate(Grd8, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd8, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region FTP

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName)
        {
            
            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateEmployeeFile(DocNo, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid()
        {
            for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
            {
                if(Sm.GetGrdStr(Grd6, Row, 1).Length > 0)
                {
                    if (mIsEmployeeAllowToUploadFile && Sm.GetGrdStr(Grd6, Row, 1).Length > 0 && Sm.GetGrdStr(Grd6, Row, 1) != "openFileDialog1")
                    {
                        if (Sm.GetGrdStr(Grd6, Row, 1) != Sm.GetGrdStr(Grd6, Row, 4))
                        {
                            return IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd6, Row, 1));
                        }
                    }
                }
            }
            return false;

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName);
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsEmployeeAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd6, Row, 1) != Sm.GetGrdStr(Grd6, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblEmployeeRecruitmentFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateEmployeeFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployeerecRuitmentFile Set "); 
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        #endregion

        #region Additional Method

        private void SetLueMajorCode(ref LookUpEdit Lue, string Major)
        {
            var cm = new MySqlCommand();
            cm.CommandText =
                "Select Col1, Col2 From ( " +
                "Select MajorName As Col1, MajorName As Col2 From TblMajor Where ActiveInd='Y' " +
                "Union All " +
                "Select @Major As Col1, @Major As Col2 " +
                ") T Order By Col2;";
            Sm.CmParam<String>(ref cm, "@Major", Major);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueTrainingCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.TrainingCode As Col1, T.TrainingName As Col2 ");
            SQL.AppendLine("From TblTraining T ");
            SQL.AppendLine("Where T.ActiveInd = 'Y' ");
            SQL.AppendLine("Order By T.TrainingName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            var Param = Sm.GetParameter("IsEmpCodeAutoGenerate");
            if (Param.Length != 0) mIsEmpCodeAutoGenerate = Param == "Y";

            mIsEmployeeCOAInfoMandatory = Sm.GetParameterBoo("IsEmployeeCOAInfoMandatory");
            mEmployeeAcNo = Sm.GetParameter("EmployeeAcNo");
            mEmployeeAcDesc = Sm.GetParameter("EmployeeAcDesc");
            mIsPPSActive = Sm.GetParameterBoo("IsPPSActive");
            mIsDeptFilterByDivision = Sm.GetParameterBoo("IsDeptFilterByDivision");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByLevelHR = Sm.GetParameterBoo("IsFilterByLevelHR");
            mIsEmployeeAllowToUploadFile = Sm.GetParameterBoo("IsEmployeeAllowToUploadFile");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
             mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mIsPersonalInformationUseStage = Sm.GetParameterBoo("IsPersonalInformationUseStage");
            mIsEmployeeEducationInputManualData = Sm.GetParameterBoo("IsEmployeeEducationInputManualData");
            mIsPersonalInformationUseEmployeeBenefit = Sm.GetParameterBoo("IsPersonalInformationUseEmployeeBenefit");


            if (mFormatFTPClient.Length == 0) mFormatFTPClient = "1";
        }

        //Untuk input combobox dalam grid
        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueSite(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                SQL.AppendLine("Where (T.ActInd='Y' ");
                if (SiteCode.Length != 0)
                    SQL.AppendLine("Or T.SiteCode='" + SiteCode + "' ");
                SQL.AppendLine(") ");
                if (mIsFilterBySiteHR)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueSection(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select SectionCode As Col1, SectionName As Col2 ");
                SQL.AppendLine("From TblSection Where ActInd='Y' Order By SectionName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueWorkGroup(ref DXE.LookUpEdit Lue, string SectionCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select WorkGroupCode As Col1, WorkGroupName As Col2 ");
                SQL.AppendLine("From TblWorkingGroup Where ActInd='Y' And SectionCode = '" + SectionCode + "' Order By WorkGroupName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueDepartment(ref DXE.LookUpEdit Lue, string DivisionCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select DeptCode As Col1, DeptName As Col2 ");
                SQL.AppendLine("From TblDepartment Where ActInd='Y' And DivisionCode = '" + DivisionCode + "' Order By DeptName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint(string DocNo)
        {
            string mDocTitle = Sm.GetValue("Select ParValue From tblparameter Where ParCode='DocTitle'");
            double mrange = double.Parse(Sm.GetValue("SELECT parvalue FROM tblparameter WHERE parcode = 'RangePersonalInformationStartWork'"));

            var l = new List<Data>();
            var lh = new List<Signature>();
            string[] TableName = { "Data", "Signature" };
            var myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.EmpName, D.OptDesc AS Status, C.LevelName, A.GrossSalary, A.JobIncentives, A.TotalTHP, A.Remark, A.Insurance, ");
            SQL.AppendLine("A.HolidayAllowance, A.AnnualLeave, A.Bonus, A.Tax, A.StartWorkDt ");
            SQL.AppendLine("FROM tblemployeerecruitment A ");
            SQL.AppendLine("INNER JOIN tblemployeerequest B ON A.EmployeeRequestDocNo = B.DocNo ");
            SQL.AppendLine("LEFT JOIN tbllevelhdr C ON B.LevelCode = C.LevelCode ");
            SQL.AppendLine("LEFT JOIN tbloption D ON B.EmploymentStatus = D.OptCode AND optcat = 'EmploymentStatus' ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                        {
                         //0
                         "DocNo",

                         //1-5
                         "DOcDt",
                         "EmpName",
                         "Status",
                         "LevelName",
                         "GrossSalary", 

                         //6-10
                         "JobIncentives",
                         "TotalTHP",
                         "Remark",
                         "Insurance",
                         "HolidayAllowance",

                         //11-14
                         "Bonus",
                         "Tax",
                         "StartWorkDt",
                         "AnnualLeave"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data()
                        {
                            CompanyLogo = Sm.CompanyLogo(),
                            DocNo = Sm.DrStr(dr, c[0]),
                            Date = Sm.ConvertDate(Sm.DrStr(dr, c[1])),
                            Employee = Sm.DrStr(dr, c[2]),
                            Status = Sm.DrStr(dr, c[3]),
                            Level = Sm.DrStr(dr, c[4]),
                            GrossSalary = Sm.DrDec(dr, c[5]),
                            JobIncentives = Sm.DrDec(dr, c[6]),
                            TotalTHP = Sm.DrDec(dr, c[7]),
                            Remark = Sm.DrStr(dr, c[8]),
                            Insurance = Sm.DrStr(dr, c[9]),
                            HolidayAllowance = Sm.DrStr(dr, c[10]),
                            Bonus = Sm.DrStr(dr, c[11]),
                            Tax = Sm.DrStr(dr, c[12]),
                            StartDate = Sm.ConvertDate(Sm.DrStr(dr, c[13])),
                            EndDate = Sm.ConvertDate(Sm.DrStr(dr, c[13])).AddDays(mrange),
                            AnnualLeave = Sm.DrStr(dr, c[14]),
                        });
                    }
                }
                dr.Close();
            }

            #endregion

            #region Signature

            var cm2 = new MySqlCommand();

            var SQL2 = new StringBuilder();

            SQL2.AppendLine("SELECT C.EmpName As Sign1, E.PosName As Pos1, D.EmpName As Sign2 ");
            SQL2.AppendLine("FROM tblparameter A ");
            SQL2.AppendLine("INNER JOIN tblparameter B ON B.ParCode = 'Sign1ForPersonalInformationPrintOut' ");
            SQL2.AppendLine("INNER JOIN TblEmployee C ON A.ParCode = 'Sign2ForPersonalInformationPrintOut' ");
            SQL2.AppendLine("	AND A.Parvalue IS NOT NULL ");
            SQL2.AppendLine("	AND A.Parvalue = C.EmpCode ");
            SQL2.AppendLine("INNER JOIN TblEmployee D ON B.ParValue = D.EmpCode ");
            SQL2.AppendLine("	AND B.parvalue IS NOT null ");
            SQL2.AppendLine("LEFT JOIN tblposition E ON C.PosCode = E.PosCode ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm2.Connection = cn;
                cm2.CommandText = SQL2.ToString();
                var dr = cm2.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                        {
                         //0
                         "Sign1",

                         //1-5
                         "Pos1",
                         "Sign2",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lh.Add(new Signature()
                        {
                            Sign1 = Sm.DrStr(dr, c[0]),
                            Pos1 = Sm.DrStr(dr, c[1]),
                            Sign2 = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }

            #endregion

            myLists.Add(l);
            myLists.Add(lh);

            Sm.PrintReport("EmployeeRecruitment", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtEmpName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEmpName);
        }

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtUserCode);
        }

        private void TxtBirthPlace_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBirthPlace);
        }

        private void TxtPostalCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPostalCode);
        }

        private void TxtPhone_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPhone);
        }

        private void TxtMobile_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMobile);
        }

        private void TxtEmail_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEmail);
        }

        private void LueGrdLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGrdLvlCode, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
        }
       
        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LueGender_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGender, new Sm.RefreshLue1(Sl.SetLueGender));
        }

        private void LueReligion_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueReligion, new Sm.RefreshLue1(Sl.SetLueReligion));
        }

        private void LueCity_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCity, new Sm.RefreshLue1(Sl.SetLueCityCode));
        }

        private void LuePayrollType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrollType, new Sm.RefreshLue1(Sl.SetLueEmployeePayrollType));
        }

        private void LueMaritalStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMaritalStatus, new Sm.RefreshLue2(Sl.SetLueOption), "MaritalStatus");
        }

        private void LuePTKP_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePTKP, new Sm.RefreshLue1(Sl.SetLuePTKP));
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueStatus_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(Sl.SetLueFamilyStatus));
        }

        private void LueStatus_Leave(object sender, EventArgs e)
        {
            if (LueStatus.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueStatus).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 2].Value =
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueStatus);
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueStatus.GetColumnValue("Col2");
                }
                LueStatus.Visible = false;
            }
        }

        private void LueProgress_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd8, ref fAccept, e);
        }

        private void LueProgress_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProgress, new Sm.RefreshLue2(Sl.SetLueOption), "PersonalInformationProgress");
        }

        private void LueProgress_Leave(object sender, EventArgs e)
        {
            if (LueProgress.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueProgress).Length == 0)
                    Grd8.Cells[fCell.RowIndex, 1].Value =
                    Grd8.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd8.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueProgress);
                    Grd8.Cells[fCell.RowIndex, 2].Value = LueProgress.GetColumnValue("Col2");
                }
                LueProgress.Visible = false;
            }
        }

        private void LueFamGender_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueFamGender, new Sm.RefreshLue1(Sl.SetLueGender));

        }

        private void LueFamGender_Leave(object sender, EventArgs e)
        {
            if (LueFamGender.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueFamGender).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value =
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueFamGender);
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueFamGender.GetColumnValue("Col2");
                }
                LueFamGender.Visible = false;
            }
        }

        private void LueFamGender_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void DteFamBirthDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);

        }

        private void DteFamBirthDt_Validated(object sender, EventArgs e)
        {
            DteFamBirthDt.Visible = false;
        }

        private void DteFamBirthDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteFamBirthDt, ref fCell, ref fAccept);
        }

        private void DteStageDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd8, ref fAccept, e);
        }

        private void DteStageDt_Validated(object sender, EventArgs e)
        {
            DteStageDt.Visible = false;
        }

        private void DteStageDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStageDt, ref fCell, ref fAccept);
        }

        private void LueLevel_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue1(Sl.SetLueEmployeeEducationLevel));
        }

        private void LueLevel_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueLevel_Leave(object sender, EventArgs e)
        {
            if (LueLevel.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (LueLevel.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueLevel).Trim();
                    Grd3.Cells[fCell.RowIndex, 3].Value = LueLevel.GetColumnValue("Col2");
                }
            }    
        }

        private void LueLevel_Validated(object sender, EventArgs e)
        {
            LueLevel.Visible = false;
        }

        private void TxtBankBranch_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankBranch);
        }

        private void TxtBankAcNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankAcNo);
        }

        private void TxtBankAcName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBankAcName);
        }

        private void TxtShortCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtShortCode);
        }

        private void LueSystemType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSystemType, new Sm.RefreshLue2(Sl.SetLueOption), "EmpSystemType");
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
        }

        private void TxtSubDistrict_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSubDistrict);
        }

        private void TxtVillage_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVillage);
        }

        private void TxtRTRW_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtRTRW);
        }

        private void TxtMother_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMother);
        }

        private void LueBloodType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBloodType, new Sm.RefreshLue2(Sl.SetLueOption), "BloodType");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(SetLueSite), string.Empty);
        }

        private void FrmEmployee_Activated(object sender, EventArgs e)
        {
            TxtDocNo.Focus();
        }

        private void LueSection_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSection, new Sm.RefreshLue1(SetLueSection));
            if (Sm.GetLue(LueSection).Length > 0)
            {
                SetLueWorkGroup(ref LueWorkGroup, Sm.GetLue(LueSection));
            }
        }

        private void LueWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWorkGroup, new Sm.RefreshLue2(SetLueWorkGroup), Sm.GetLue(LueSection));
        }

        private void LueDivisionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDivisionCode, new Sm.RefreshLue1(Sl.SetLueDivisionCode));
            if (mIsDeptFilterByDivision && Sm.GetLue(LueDivisionCode).Length > 0)
            {
                SetLueDepartment(ref LueDeptCode, Sm.GetLue(LueDivisionCode));
            }
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (!mIsDeptFilterByDivision)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            }
            else
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(SetLueDepartment), Sm.GetLue(LueDivisionCode)); 
            }
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
        }

        private void BtnEmpRequest_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmployeeRecruitmentDlg(this));
        }

        private void BtnShowEmployeeRequest_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtEmpRequestDocNo, "Employee Request", false))
            {
                try
                {
                    var f = new FrmEmployeeRequest(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtEmpRequestDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        private void LueCompetenceCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCompetenceCode, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueCompetenceCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueCompetenceCode_Leave(object sender, EventArgs e)
        {
            if (LueCompetenceCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueCompetenceCode).Length == 0)
                    Grd5.Cells[fCell.RowIndex, 1].Value =
                    Grd5.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueCompetenceCode);
                    Grd5.Cells[fCell.RowIndex, 2].Value = LueCompetenceCode.GetColumnValue("Col2");
                }
                LueCompetenceCode.Visible = false;
                Sm.SetGrdNumValueZero(ref Grd5, (fCell.RowIndex + 1), new int[] { 4 });
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Images|*.png;*.bmp;*.jpg";
            ImageFormat format = ImageFormat.Png;
            sfd.FileName = TxtDocNo.Text;
            if (PicEmployee.Image != null && sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string ext = System.IO.Path.GetExtension(sfd.FileName);
                switch (ext)
                {
                    case ".jpg":
                        format = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        format = ImageFormat.Bmp;
                        break;
                }
                PicEmployee.Image.Save(sfd.FileName, format);
            }
        }

        private void BtnPicture_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document Number", false)) return;

            try
            {
                string DBServer = Sm.GetParameter("DBServer");
                string location = Sm.GetParameter("ImgFileEmployee");
                if (DBServer.Length != 0 && location.Length != 0 && Sm.CompareStr(DBServer, Gv.Server))
                {
                    if (Directory.Exists(location))
                    {
                        string ImageFile = String.Concat(location, "\\", TxtDocNo.Text);
                        foreach (string fe in new string[11]
                                { ".jpg", ".png", ".bmp", ".jpeg", ".gif", ".dib",
                                  ".rle", ".jpe", ".jfif", ".tiff", ".tif" })
                        {
                            if (File.Exists(String.Concat(ImageFile, fe)))
                            {
                                PicEmployee.Image = Image.FromFile(String.Concat(ImageFile, fe));
                                mProfilePicture = String.Concat(location, TxtDocNo.Text, fe);
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void TxtJobIncentives_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtJobIncentives, 0);
        }

        private void TxtTHP_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTHP, 0);
        }

        private void TxtInsurance_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtInsurance);
        }

        private void TxtHolidayAllowance_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtHolidayAllowance);
        }

        private void TxtAnnualLeave_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAnnualLeave);
        }

        private void TxtBonus_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBonus);
        }

        private void TxtTax_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTax);
        }

        #endregion

        #region Grid Event

        #region Grid 1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1, 3, 5, 6, 7, 8, 9, 11, 13 }, e.ColIndex))
            {
                if (e.ColIndex == 3) LueRequestEdit(Grd1, LueStatus, ref fCell, ref fAccept, e);
                if (e.ColIndex == 5) LueRequestEdit(Grd1, LueFamGender, ref fCell, ref fAccept, e);
                if (e.ColIndex == 6) Sm.DteRequestEdit(Grd1, DteFamBirthDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 11) Sm.LueRequestEdit(ref Grd1, ref LueEducationLevelCode, ref fCell, ref fAccept, e);
                if (e.ColIndex == 13) Sm.LueRequestEdit(ref Grd1, ref LueProfessionCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }

            //if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 11, 13 }, e.ColIndex))
            //{
            //    LueRequestEdit(Grd1, LueTrainingCode, ref fCell, ref fAccept, e);
            //    Sm.LueRequestEdit(ref Grd1, ref LueProfessionCode, ref fCell, ref fAccept, e);
            //    Sm.LueRequestEdit(ref Grd1, ref LueEducationLevelCode, ref fCell, ref fAccept, e);
            //    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            //    Sl.SetLueOption(ref LueProfessionCode, "Profession");
            //    Sl.SetLueOption(ref LueEducationLevelCode, "EmployeeEducationLevel");
            //}
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 1, 7, 8, 9 }, e);

        }

        private void LueProfessionCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueProfessionCode, new Sm.RefreshLue2(Sl.SetLueOption), "Profession");
        }

        private void LueProfessionCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueProfessionCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueProfessionCode.Visible && fAccept && fCell.ColIndex == 13)
                {
                    if (Sm.GetLue(LueProfessionCode).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 12].Value =
                        Grd1.Cells[fCell.RowIndex, 13].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 12].Value = Sm.GetLue(LueProfessionCode);
                        Grd1.Cells[fCell.RowIndex, 13].Value = LueProfessionCode.GetColumnValue("Col2");
                    }
                    LueProfessionCode.Visible = false;
                }
            }
        }
        private void LueProfessionCode_Validated(object sender, EventArgs e)
        {
            LueProfessionCode.Visible = false;
        }

        private void LueEducationLevelCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEducationLevelCode, new Sm.RefreshLue2(Sl.SetLueOption), "EmployeeEducationLevel");
        }

        private void LueEducationLevelCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueEducationLevelCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueEducationLevelCode.Visible && fAccept && fCell.ColIndex == 11)
                {
                    if (Sm.GetLue(LueEducationLevelCode).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 10].Value =
                        Grd1.Cells[fCell.RowIndex, 11].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 10].Value = Sm.GetLue(LueEducationLevelCode);
                        Grd1.Cells[fCell.RowIndex, 11].Value = LueEducationLevelCode.GetColumnValue("Col2");
                    }
                    LueEducationLevelCode.Visible = false;
                }
            }
        }


        #endregion

        #region Grid 2

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 3

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 3, 5, 7, 8, 9, 10, 12 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                //Untuk input combobox dalam grid
                if (e.ColIndex == 3)
                {
                    Sm.LueRequestEdit(ref Grd3, ref LueLevel, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                }
                if (e.ColIndex == 5)
                {
                    Sm.LueRequestEdit(ref Grd3, ref LueMajor, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                }
                if (e.ColIndex == 7)
                {
                    if (!mIsEmployeeEducationInputManualData) Sm.LueRequestEdit(ref Grd3, ref LueField, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                }
                if (e.ColIndex == 12)
                {
                    if (!mIsEmployeeEducationInputManualData) Sm.LueRequestEdit(ref Grd3, ref LueFacCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                }
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        private void LueFacCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueFacCode, new Sm.RefreshLue2(Sl.SetLueFacCode), string.Empty);
        }

        private void LueFacCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueFacCode_Leave(object sender, EventArgs e)
        {
            if (LueFacCode.Visible && fAccept && fCell.ColIndex == 12)
            {
                if (Sm.GetLue(LueFacCode).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 11].Value =
                    Grd3.Cells[fCell.RowIndex, 12].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 11].Value = Sm.GetLue(LueFacCode);
                    Grd3.Cells[fCell.RowIndex, 12].Value = LueFacCode.GetColumnValue("Col2");
                }
            }
        }

        private void LueFacCode_Validated(object sender, EventArgs e)
        {
            LueFacCode.Visible = false;
        }

        private void LueField_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueField, new Sm.RefreshLue1(Sl.SetLueEmployeeEducationField));
        }

        private void LueField_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueField_Leave(object sender, EventArgs e)
        {
            if (LueField.Visible && fAccept && fCell.ColIndex == 7)
            {
                if (LueField.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 7].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueField).Trim();
                    Grd3.Cells[fCell.RowIndex, 7].Value = LueField.GetColumnValue("Col2");
                }
            }
        }

        private void LueField_Validated(object sender, EventArgs e)
        {
            LueField.Visible = false;
        }

        private void LueMajor_Validated(object sender, EventArgs e)
        {
           LueMajor.Visible = false;
        }

        private void LueMajor_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueMajor, new Sm.RefreshLue1(Sl.SetLueMajorCode));
        }

        private void LueMajor_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueMajor_Leave(object sender, EventArgs e)
        {
            if (LueMajor.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueMajor.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueMajor).Trim(); 
                    Grd3.Cells[fCell.RowIndex, 5].Value = LueMajor.GetColumnValue("Col2");
                }
            }
        }

       
        #endregion

        #region Grid 4

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd4, e.RowIndex + 1, new int[] { 2 });
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                LueRequestEdit(Grd4, LueTrainingCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                SetLueTrainingCode(ref LueTrainingCode);
            }
        }

        private void LueTrainingCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTrainingCode, new Sm.RefreshLue1(Sl.SetLueTrainingCode));
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdEnter(Grd4, e);
            Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueTrainingCode_Leave(object sender, EventArgs e)
        {
            if (LueTrainingCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueTrainingCode).Length == 0)
                {
                    Grd4.Cells[fCell.RowIndex, 1].Value =
                    Grd4.Cells[fCell.RowIndex, 2].Value = null;
                }
                else
                {
                    Grd4.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueTrainingCode);
                    Grd4.Cells[fCell.RowIndex, 2].Value = LueTrainingCode.GetColumnValue("Col2");
                }
            }
            LueTrainingCode.Visible = false;
        }

        #endregion

        #region Grid 5

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
            {
                if (e.ColIndex == 2)
                    LueRequestEdit(Grd5, LueCompetenceCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
                if (mIsInsert)
                    Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 4 });
                else
                {
                    if ((Sm.GetGrdDec(Grd5, Grd5.Rows.Count - 2, 4) == 0) || (Sm.GetGrdDec(Grd5, Grd5.Rows.Count - 2, 4).ToString().Length == 0))
                        Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 2, new int[] { 4 });
                    Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 4 });
                }
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
            Sm.GrdEnter(Grd5, e);
            Sm.GrdTabInLastCell(Grd5, e, BtnFind, BtnSave);
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd5, new int[] { 4 }, e);
        }


        #endregion

        #region Grid 6

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd6, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd6, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd6, e.RowIndex, 1);
                    SFD.DefaultExt = "jpg";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd6, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" ;
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd6.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
                if (Sm.GetGrdStr(Grd6, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd6, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd6, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd6, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd6_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd6, e, BtnSave);
            Sm.GrdEnter(Grd6, e);
            Sm.GrdTabInLastCell(Grd6, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 7

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd7, e, BtnSave);
            Sm.GrdEnter(Grd7, e);
            Sm.GrdTabInLastCell(Grd7, e, BtnFind, BtnSave);
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
        }

        #endregion

        #region Grid 8

        private void Grd8_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4 }, e.ColIndex))
            {
                if (e.ColIndex == 2) LueRequestEdit(Grd8, LueProgress, ref fCell, ref fAccept, e);
                if (e.ColIndex == 3) Sm.DteRequestEdit(Grd8, DteStageDt, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd8, e.RowIndex);
            }
        }

        private void Grd8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd8, e, BtnSave);
            Sm.GrdKeyDown(Grd8, e, BtnFind, BtnSave);
        }

        #endregion

        #region Company

        private void LueDeptCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode2, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }
        private void TxtGrossSalary_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtGrossSalary, 0);
        }
        #endregion

        #endregion

        #endregion

        #region Class

        private class Data
        {
            public string CompanyLogo { set; get; }
            public string DocNo { set; get; }
            public DateTime Date { set; get; }
            public string Employee { set; get; }
            public string Status { set; get; }
            public string Level { set; get; }
            public decimal GrossSalary { set; get; }
            public decimal JobIncentives { set; get; }
            public decimal TotalTHP { set; get; }
            public string Remark { set; get; }
            public string Insurance { set; get; }
            public string HolidayAllowance { set; get; }
            public string Bonus { set; get; }
            public string Tax { set; get; }
            public DateTime StartDate { set; get; }
            public DateTime EndDate { set; get; }
            public string AnnualLeave { set; get; }
        }

        private class Signature
        {
            public string Sign1 { set; get; }
            public string Sign2 { set; get; }
            public string Pos1 { set; get; }
        }

        #endregion
    }

}
