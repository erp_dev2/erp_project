﻿#region Update
/*
    15/01/2020 [TKG/IMS] New Application
    06/02/2020 [TKG/IMS] BOM ambil data finished good dari informasi so contract bagian BOQ Inventory.
    18/03/2020 [TKG/IMS] barang jasa juga bisa ditarik
    07/06/2020 [TKG/IMS] tambah customer PO#
    19/06/2020 [TKG/IMS] tambah so contract remark
    19/08/2020 [TKG/IMS] menghilangkan kondisi status saat query bom
    24/09/2020 [WED/IMS] quantity di sum berdasarkan item nya, karena di SO Contract ketambahan No & item nya yg sama bisa dipilih berkali kali
    15/11/2020 [TKG/IMS] tambah service
    18/12/2020 [WED/IMS] qty di passed ke variable global di main
    12/01/2021 [TKG/IMS] merubah query
    29/01/2021 [DITA/IMS] tambah item dismantle
    16/02/2021 [DITA/IMS] Bug saat menampilkan quantity dari so contract
    25/02/2021 [WED/IMS] rubah urutan di SetGrd nya
 *  16/06/2021 [VIN/IMS] Penyesuaian qty -> soc service bisa tick service indicator 
 *  22/07/2021 [VIN/IMS] Penyesuaian qty -> berdasarkan qty asli copy ke parent  
 *  13/08/2021 [VIN/IMS] tambah informasi warna: merah -> belum pernah di bom kan 
 *  25/08/2021 [VIN/IMS] Bug Query 
 *  11/11/2021 [VIN/IMS] Bug Filter 
 * 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBom3Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBom3 mFrmParent;
        private string mSQL = string.Empty, mGroupBy = string.Empty;

        #endregion

        #region Constructor

        public FrmBom3Dlg4(FrmBom3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            mGroupBy = string.Empty;

            mGroupBy += " Group By A.DocNo, A.DocDt, E.ProjectCode, E.ProjectName, B.ItCode, B.ItName, G.bomDocNo, ";
            mGroupBy += " B.ItCodeInternal, B.Specification, B.PurchaseUomCode, B.ServiceItemInd, A.PONo, A.ItCodeDismantle ";

            SQL.AppendLine("Select A.DocNo, A.DocDt, E.ProjectCode, E.ProjectName, G.BOMDocNo, ");
            SQL.AppendLine("B.ItCode, B.ItName, B.ItCodeInternal, B.Specification, Sum(A.Qty) Qty, B.PurchaseUomCode, B.ServiceItemInd, A.PONo, Group_Concat(Distinct IfNull(A.Remark, '')) Remark, A.ItCodeDismantle, F.ItName ItNameDismantle ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T2.DocNo, T1.DocDt, T1.BOQDocNo, T2.ItCode, T2.Qty, T1.PONo, T2.Remark, T3.ItCodeDismantle ");
            SQL.AppendLine("    From TblSOContractHdr T1 ");
            SQL.AppendLine("    Inner Join TblSOContractDtl4 T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("        Concat(T2.DocNo, T2.ItCode) In  ( ");
            SQL.AppendLine("            Select Distinct Concat(X2.DocNo, X2.ItCode) ");
            SQL.AppendLine("            From TblSOContractHdr X1 ");
            SQL.AppendLine("            Inner Join TblSOContractDtl4 X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("            Inner Join TblOption X3 On X3.OptCat='BomGroup' ");
            SQL.AppendLine("            Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            And X1.CancelInd='N' ");
            SQL.AppendLine("            And X1.Status='A' ");
            SQL.AppendLine("            And Concat(X2.DocNo, X2.ItCode, X3.OptCode) Not In ( ");
            SQL.AppendLine("                Select Distinct Concat(Y1.SOContractDocNo, Y1.ItCode, Y1.BomGroupCode) ");
            SQL.AppendLine("                From TblBom2Hdr Y1 ");
            SQL.AppendLine("                Inner Join TblSOContractHdr Y2 ");
            SQL.AppendLine("                    On Y1.SOContractDocNo=Y2.DocNo ");
            SQL.AppendLine("                    And Y2.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("                    And Y2.CancelInd='N' ");
            SQL.AppendLine("                    And Y2.Status='A' ");
            SQL.AppendLine("                Where Y1.BomGroupCode Is Not Null ");
            SQL.AppendLine("                And Y1.CancelInd='N' ");
            SQL.AppendLine("                And Y1.Status='A' ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Inner Join TblBOQDtl2 T3 On T2.BOQDocNo = T3.DocNo ");
            SQL.AppendLine("    And T3.ItCode = T2.ItCode And T3.SeqNo = T2.SeqNo ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T2.DocNo, T1.DocDt, T1.BOQDocNo, T2.ItCode, T2.Qty, T1.PONo, T2.Remark, T3.ItCodeDismantle ");
            SQL.AppendLine("    From TblSOContractHdr T1 ");
            SQL.AppendLine("    Inner Join TblSOContractDtl5 T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("        Concat(T2.DocNo, T2.ItCode) In  ( ");
            SQL.AppendLine("            Select Distinct Concat(X2.DocNo, X2.ItCode) ");
            SQL.AppendLine("            From TblSOContractHdr X1 ");
            SQL.AppendLine("            Inner Join TblSOContractDtl5 X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("            Inner Join TblOption X3 On X3.OptCat='BomGroup' ");
            SQL.AppendLine("            Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            And X1.CancelInd='N' ");
            SQL.AppendLine("            And X1.Status='A' ");
            SQL.AppendLine("            And Concat(X2.DocNo, X2.ItCode, X3.OptCode) Not In ( ");
            SQL.AppendLine("                Select Distinct Concat(Y1.SOContractDocNo, Y1.ItCode, Y1.BomGroupCode) ");
            SQL.AppendLine("                From TblBom2Hdr Y1 ");
            SQL.AppendLine("                Inner Join TblSOContractHdr Y2 ");
            SQL.AppendLine("                    On Y1.SOContractDocNo=Y2.DocNo ");
            SQL.AppendLine("                    And Y2.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("                    And Y2.CancelInd='N' ");
            SQL.AppendLine("                    And Y2.Status='A' ");
            SQL.AppendLine("                Where Y1.BomGroupCode Is Not Null ");
            SQL.AppendLine("                And Y1.CancelInd='N' ");
            SQL.AppendLine("                And Y1.Status='A' ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Inner Join TblBOQDtl3 T3 On T2.BOQDocNo = T3.DocNo ");
            SQL.AppendLine("    And T3.ItCode = T2.ItCode And T3.SeqNo = T2.SeqNo ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Inner Join TblBOQHdr C On A.BOQDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr D On C.LOPDocNo=D.DocNO ");
            SQL.AppendLine("Inner Join TblProjectGroup E On D.PGCode=E.PGCode ");
            SQL.AppendLine("Left Join TblItem F On A.ItCodeDismantle = F.ItCode  ");
            SQL.AppendLine("LEFT JOIN   ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    SELECT X1.DocNo , GROUP_CONCAT(Distinct ifnull(X2.DocNo, '')) BOMDocNo, X2.ItCode X   ");
            SQL.AppendLine("    FROM TblSOContractHdr X1   ");
            SQL.AppendLine("    Inner Join TblBOM2Hdr X2 ON X1.DocNo = X2.SOContractDocNo  ");
            SQL.AppendLine("    AND X2.CancelInd='N' AND (X2.Status='O' OR X2.Status='A')   ");
            SQL.AppendLine("    AND X1.DocDt Between @DocDt1 And @DocDt2   ");
            SQL.AppendLine("    GROUP BY X1.DocNo, X2.ItCode   ");
            SQL.AppendLine(")G ON A.Docno = G.DocNo AND A.ItCode = G.X  ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Project's Code",
                    "Project's Name",
                    "Customer's PO#",

                    //6-10
                    "Local Code",
                    "Item's Code",
                    "Item's Name",
                    "Item Dismantle's Code",
                    "Item Dismantle's",

                    //11-15
                    "Specification",
                    "Service",
                    "Quantity",
                    "UoM",
                    "SO Contract's Remark",

                    //16
                    "BOM#"
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    130, 100, 130, 200, 120, 

                    //6-10
                    120, 100, 250, 100, 250,

                    //11-15
                    350, 80, 120, 100, 250, 

                    //16
                    200
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 13 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 9, 16 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 9, 16 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, new string[] { "E.ProjectCode", "E.ProjectName" });
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + mGroupBy + " Order By A.DocDt Desc, A.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "ProjectCode", "ProjectName", "PONo", "ItCodeInternal", 

                        //6-10
                        "ItCode", "ItName", "ItCodeDismantle", "ItNameDismantle", "Specification", 

                        //11-15
                        "ServiceItemInd", "Qty", "PurchaseUomCode", "Remark", "BOMDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        if (Sm.GetGrdStr(Grd1, Row, 16).Length == 0)
                            Grd1.Rows[Row].BackColor = Color.Red;
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int r = Grd1.CurRow.Index;

                mFrmParent.TxtSOContractDocNo.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                mFrmParent.TxtProjectCode.EditValue = Sm.GetGrdStr(Grd1, r, 3);
                mFrmParent.TxtProjectName.EditValue = Sm.GetGrdStr(Grd1, r, 4);
                mFrmParent.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, r, 7);
                mFrmParent.TxtItName.EditValue = Sm.GetGrdStr(Grd1, r, 8);
                mFrmParent.TxtItCodeInternal.EditValue = Sm.GetGrdStr(Grd1, r, 6);
                mFrmParent.MeeSpecification.EditValue = Sm.GetGrdStr(Grd1, r, 11);
                mFrmParent.TxtQty.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 13), 0);
                mFrmParent.TxtQty1.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 13), 0);
                mFrmParent.TxtUomCode.EditValue = Sm.GetGrdStr(Grd1, r, 14);
                mFrmParent.TxtPONo.EditValue = Sm.GetGrdStr(Grd1, r, 5);
                mFrmParent.ChkServiceItemInd.Checked = Sm.GetGrdBool(Grd1, r, 12);
                if (Sm.GetGrdBool(Grd1, r, 12) == true)
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { mFrmParent.ChkServiceItemInd }, true);
                else
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { mFrmParent.ChkServiceItemInd }, false);

                mFrmParent.mInitFGQty = Sm.GetGrdDec(Grd1, r, 13);
                mFrmParent.ClearGrd();
                mFrmParent.ml.Clear();
                mFrmParent.GeneralTypeForQty();
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO contract#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        #endregion

        #endregion
    }
}
