﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDODeptLOPDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDODeptLOP mFrmParent;
        private string mWhsCode = string.Empty, mCCCode = string.Empty; 

        #endregion

        #region Constructor

        public FrmDODeptLOPDlg(FrmDODeptLOP FrmParent, string WhsCode, string CCCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mCCCode = CCCode.Length==0?"XXX":CCCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's Name", 
                        
                        //6-10
                        "Item's Category",
                        "Property Code",
                        "Property",
                        "Batch Number",
                        "Source",

                        //11-15
                        "Lot",
                        "Bin", 
                        "Stock",
                        "UoM",
                        "Stock 2",

                        //16-20
                        "Uom 2",
                        "Stock 3",
                        "Uom 3",
                        "Cost Category",
                        "COA's Account#",

                        //21-22
                        "Group",
                        "Foreign Name"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 20, 100, 250, 
                        
                        //6-10
                        150, 0, 0, 200, 200, 
                        
                        //11-15
                        60, 80, 80, 80, 80, 

                        //16-20
                        80, 80, 80, 200, 200,

                        //21-22
                        100, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            Grd1.Cols[22].Move(6);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 7, 10, 15, 16, 17, 18, 21 }, false);
            if(!mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 22 }, false);
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[21].Visible = true;
                Grd1.Cols[21].Move(5);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 10 }, !ChkHideInfoInGrd.Checked);
            
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string
                    Filter = " And 0=0 ",
                    Filter2 = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedInventory());

                Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "B.ItCodeInternal", "B.ItName", "B.ForeignName" });
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);
                Sm.FilterStr(ref Filter2, ref cm, TxtPropCode.Text, new string[] { "A.PropCode", "F.PropName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "A.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);
                
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.ItCode, B.ItName, B.ForeignName, B.ItCodeInternal, C.ItCtName, A.PropCode, F.PropName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
                SQL.AppendLine("B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, ");
                SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, E.CCtName, C.AcNo, B.ItGrpCode ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select ItCode, PropCode, BatchNo, Source, Lot, Bin, ");
                SQL.AppendLine("    Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
                SQL.AppendLine("    From TblStockSummary A ");
                SQL.AppendLine("    Where (Qty<>0 Or Qty2<>0 Or Qty3<>0) ");
                SQL.AppendLine("    And WhsCode=@WhsCode " + Filter);
                SQL.AppendLine("    Group By ItCode, PropCode, BatchNo, Source, Lot, Bin ");
                SQL.AppendLine(") A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode " + Filter2);
                SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
                SQL.AppendLine("Left Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("Left Join TblCostCategory E On D.CCCode=E.CCCode And D.CCtCode=E.CCtCode ");
                SQL.AppendLine("Left Join TblProperty F On A.PropCode=F.PropCode ");
                SQL.AppendLine("Order By A.ItCode, F.PropName, A.BatchNo, A.Source, A.Lot, A.Bin;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString(),
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItCodeInternal", "ItName", "ItCtName", "PropCode", "PropName",  
                            
                            //6-10
                            "BatchNo", "Source", "Lot", "Bin", "Qty", 
                            
                            //11-15
                            "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", 
                            
                            //16-19
                            "CCtName", "AcNo", "ItGrpCode", "ForeignName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 22);

                        mFrmParent.InsertSummary(
                            Sm.GetGrdStr(Grd1, Row2, 2),
                            Sm.GetGrdStr(Grd1, Row2, 4),
                            Sm.GetGrdStr(Grd1, Row2, 5),
                            Sm.GetGrdStr(Grd1, Row2, 22),
                            Sm.GetGrdStr(Grd1, Row2, 7),
                            Sm.GetGrdStr(Grd1, Row2, 8),
                            Sm.GetGrdStr(Grd1, Row2, 9),
                            Sm.GetGrdStr(Grd1, Row2, 10),
                            Sm.GetGrdStr(Grd1, Row2, 11),
                            Sm.GetGrdStr(Grd1, Row2, 12),
                            Sm.GetGrdStr(Grd1, Row2, 14),
                            Sm.GetGrdStr(Grd1, Row2, 16),
                            Sm.GetGrdStr(Grd1, Row2, 18),
                            Sm.GetGrdDec(Grd1, Row2, 13),
                            Sm.GetGrdDec(Grd1, Row2, 15),
                            Sm.GetGrdDec(Grd1, Row2, 17),
                            Sm.GetGrdStr(Grd1, Row2, 21)
                        );
                        
                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2, 8 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 15, 17, 19 });
                    }
                }
                mFrmParent.ComputeSummary();
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtPropCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPropCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion

    }
}
