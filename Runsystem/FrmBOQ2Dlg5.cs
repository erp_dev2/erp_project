﻿#region Update
/* 
 *  11/11/2020 [ICA/IMS] new App
 * 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBOQ2Dlg5 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBOQ2 mFrmParent;
        private string mSQL = string.Empty;
        private string mItemType = string.Empty; //1=service 2=Inventory
        private int mRow = 0;

        #endregion

        #region Constructor

        public FrmBOQ2Dlg5(FrmBOQ2 FrmParent, string ItemType, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mItemType = ItemType;
            mRow = Row;
            this.Text = "Item Dismantle's List";
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Item Dismantle's Code", 
                    "Item Dismantle's Name",
                    "Local Code",
                    "Purchase UoM",
                    "Specification",

                    //6
                    "Category"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 350, 100, 100, 250, 
                    
                    //6
                    200
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 6 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.ItCodeInternal, A.PurchaseUomCode, A.Specification, B.ItCtName ");
            SQL.AppendLine("From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("And A.ItCtCode=B.ItCtCode ");
            if (mItemType == "1")
                SQL.AppendLine("And A.ServiceItemInd='Y' ");
            else
                SQL.AppendLine("And A.ServiceItemInd='N' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, ItCode = string.Empty;
                var cm = new MySqlCommand();

                if (Filter.Length != 0)
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " ";

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL.ToString() + Filter + " Order By A.ItName; ",
                    new string[] 
                    { 
                        "ItCode", 
                        "ItName", "ItCodeInternal", "PurchaseUomCode", "Specification", "ItCtName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                if (mItemType == "1")
                {
                    Sm.CopyGrdValue(mFrmParent.Grd2, mRow, 23, Grd1, Grd1.CurRow.Index, 1);
                    Sm.CopyGrdValue(mFrmParent.Grd2, mRow, 24, Grd1, Grd1.CurRow.Index, 2);
                }
                if (mItemType == "2")
                {
                    Sm.CopyGrdValue(mFrmParent.Grd3, mRow, 28, Grd1, Grd1.CurRow.Index, 1);
                    Sm.CopyGrdValue(mFrmParent.Grd3, mRow, 29, Grd1, Grd1.CurRow.Index, 2);
                }
            }
            this.Hide();
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }



        #endregion

        #endregion

    }
}
