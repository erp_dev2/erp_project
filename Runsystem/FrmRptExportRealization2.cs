﻿#region Update
/*
    24/01/2019 [WED] THC bukan dari parameter THC
                     if(Size == 40") THC = 145 if(Size == 20") THC = 95
    13/02/2019 [DITA] buat parameter untuk nilai THC feet 20 dan feet 40
    09/04/2019 [MEY] menambah informasi UOM
    12/04/2019 [WED] Amount di subquery dihapus rounding nya
*/
#endregion

#region Namespace


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptExportRealization2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string mTHC20Feet = string.Empty,
        mTHC40Feet = string.Empty;

        #endregion

        #region Constructor

        public FrmRptExportRealization2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetLueStatus(ref LueStatus);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select  ");
            SQL.AppendLine("Case When Y.Status = 'C' Then 'Cancelled' When Y.Status = 'R' Then 'Released' When Y.Status = 'F' Then 'Final' ");
            SQL.AppendLine("When Y.Status = 'P' Then 'Planning' End As Stat, Y.ItGrpName, Y.Vlegal, Y.VlegalDt, Truncate(SUM(Y.QtyInventory), 4) As VolVlegal, ");
            SQL.AppendLine("Round(SUM(Y.FOBAsli), 2) As FOB, ");
            SQL.AppendLine("Y.PEB, Y.PEBDt, ");
            SQL.AppendLine("truncate(SUM(Y.QtyInventory), 4) As VolPEB, ");
            SQL.AppendLine("Y.LocalDocno, Y.PlaceDelivery, ");
            SQL.AppendLine("SUM(Round(Y.Qty, 4)), Y.InventoryUOMCode ");
            SQL.AppendLine("From (  ");
            SQL.AppendLine("    Select *, if(X.FreightTHC>0,  ");
	        SQL.AppendLine("    (X.Qty*X.UPrice)-(X.Qty/X.TotalQty*(X.FreightTHC)), ");
            SQL.AppendLine("    (X.Qty*X.UPrice)) As FOBAsli  ");
            SQL.AppendLine("    From (  ");
            SQL.AppendLine("        Select M.Status,  K.ItGrpName, A2.Dno, A2.SectionNo,   ");
            SQL.AppendLine("        A2.Qty, Round(A2.QtyInventory, 4) As QtyInventory, F.UPrice,   ");
            SQL.AppendLine("        (F.UPrice * A2.Qty) As Amount,  ");
            SQL.AppendLine("        Case   ");
            SQL.AppendLine("        When A2.SectionNo = '1' Then A.Freight1 - Case A.Size1 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '2' Then A.Freight2 - Case A.Size2 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '3' Then A.Freight3 - Case A.Size3 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '4' Then A.Freight4 - Case A.Size4 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '5' Then A.Freight5 - Case A.Size5 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '6' Then A.Freight6 - Case A.Size6 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '7' Then A.Freight7 - Case A.Size7 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '8' Then A.Freight8 - Case A.Size8 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '9' Then A.Freight9 - Case A.Size9 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '10' Then A.Freight10 - Case A.Size10 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '11' Then A.Freight11 - Case A.Size11 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '12' Then A.Freight12 - Case A.Size12 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '13' Then A.Freight13 - Case A.Size13 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '14' Then A.Freight14 - Case A.Size14 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '15' Then A.Freight15 - Case A.Size15 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '16' Then A.Freight16 - Case A.Size16 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '17' Then A.Freight17 - Case A.Size17 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '18' Then A.Freight18 - Case A.Size18 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '19' Then A.Freight19 - Case A.Size19 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '20' Then A.Freight20 - Case A.Size20 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '21' Then A.Freight21 - Case A.Size21 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '22' Then A.Freight22 - Case A.Size22 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '23' Then A.Freight23 - Case A.Size23 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '24' Then A.Freight24 - Case A.Size24 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        When A2.SectionNo = '25' Then A.Freight25 - Case A.Size25 When '40' Then " + mTHC40Feet + " When '20' Then " + mTHC20Feet + " Else 0 End ");
            SQL.AppendLine("        end as FreightTHC,  ");
            SQL.AppendLine("        Case   ");
            SQL.AppendLine("        When A2.SectionNo = '1' Then A.Freight1   ");
            SQL.AppendLine("        When A2.SectionNo = '2' Then A.Freight2   ");
            SQL.AppendLine("        When A2.SectionNo = '3' Then A.Freight3  ");
            SQL.AppendLine("        When A2.SectionNo = '4' Then A.Freight4   ");
            SQL.AppendLine("        When A2.SectionNo = '5' Then A.Freight5   ");
            SQL.AppendLine("        When A2.SectionNo = '6' Then A.Freight6   ");
            SQL.AppendLine("        When A2.SectionNo = '7' Then A.Freight7   ");
            SQL.AppendLine("        When A2.SectionNo = '8' Then A.Freight8   ");
            SQL.AppendLine("        When A2.SectionNo = '9' Then A.Freight9    ");
            SQL.AppendLine("        When A2.SectionNo = '10' Then A.Freight10  "); 
            SQL.AppendLine("        When A2.SectionNo = '11' Then A.Freight11   ");
            SQL.AppendLine("        When A2.SectionNo = '12' Then A.Freight12   ");
            SQL.AppendLine("        When A2.SectionNo = '13' Then A.Freight13   ");
            SQL.AppendLine("        When A2.SectionNo = '14' Then A.Freight14   ");
            SQL.AppendLine("        When A2.SectionNo = '15' Then A.Freight15   ");
            SQL.AppendLine("        When A2.SectionNo = '16' Then A.Freight16  ");
            SQL.AppendLine("        When A2.SectionNo = '17' Then A.Freight17  ");
            SQL.AppendLine("        When A2.SectionNo = '18' Then A.Freight18  ");
            SQL.AppendLine("        When A2.SectionNo = '19' Then A.Freight19  ");
            SQL.AppendLine("        When A2.SectionNo = '20' Then A.Freight20  ");
            SQL.AppendLine("        When A2.SectionNo = '21' Then A.Freight21  ");
            SQL.AppendLine("        When A2.SectionNo = '22' Then A.Freight22  ");
            SQL.AppendLine("        When A2.SectionNo = '23' Then A.Freight23  ");
            SQL.AppendLine("        When A2.SectionNo = '24' Then A.Freight24  ");
            SQL.AppendLine("        When A2.SectionNo = '25' Then A.Freight25  ");
            SQL.AppendLine("        end as Freight,  ");
            SQL.AppendLine("        M.Vlegal, M.VlegalDt, 0 As VolVlegal, ");
            SQL.AppendLine("        M.PEB, IfNull(M.PEBDt, M.DocDt) As PEBDt, M.LocalDocno, M.PlaceDelivery, ");
            SQL.AppendLine("        N.TotalKubik, N.TotalQty, B.InventoryUOMCode ");
            SQL.AppendLine("        From TblSInv X ");
            SQL.AppendLine("        Inner Join TblPlhdr A On A.DocNo = X.PlDocNo ");
            SQL.AppendLine("        Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
            SQL.AppendLine("        Inner Join TblItem B On A2.ItCode = B.ItCode ");
            SQL.AppendLine("        Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno  ");
            SQL.AppendLine("        Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo And D.Status<>'C' ");
            SQL.AppendLine("        Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno   ");
            SQL.AppendLine("        Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo  ");
            SQL.AppendLine("        Inner Join TblUser F3 On F2.SpCode = F3.UserCode  ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo  ");
            SQL.AppendLine("        Left Join TblItemPackagingUnit J On C.PackagingUnitUomCode = J.UomCOde And A2.ItCode = J.ItCode  "); 
            //SQL.AppendLine("        Left Join (Select ParValue As THC From tblParameter Where ParCode = 'THC') I On 0=0   ");
            SQL.AppendLine("        Inner join TblItemGroup K On B.ItGrpCode = K.ItGrpCode  ");
            SQL.AppendLine("        Inner Join TblSIHdr L On A.SIDocNo = L.DocNo  ");
            SQL.AppendLine("        Inner Join TblSP M ON L.SpDocNo = M.DocNo And M.Status<>'C'  ");
            //SQL.AppendLine("-- query untuk mendapatkan total freeight, total harga, total volume perdocument  ");
            SQL.AppendLine("        Inner Join   ");
            SQL.AppendLine("        (  ");
            SQL.AppendLine("             Select A.DocNo, A2.SectionNo, SUM(Round(A2.Qty, 4)) As TotalQty, ");
			SQL.AppendLine("	            SUM(Round(A2.QtyInventory, 4)) As TotalKubik ");
			SQL.AppendLine("	            From TblSInv X   ");
			SQL.AppendLine("	            Inner Join TblPlhdr A On A.DocNo = X.PlDocNo   ");
			SQL.AppendLine("	            Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo   ");
			SQL.AppendLine("	            Inner Join TblItem B On A2.ItCode = B.ItCode    ");
			SQL.AppendLine("	            Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno  ");
            SQL.AppendLine("	            Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo And D.Status<>'C' ");
			SQL.AppendLine("	            Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno  ");
            SQL.AppendLine("	            Group By A.DocNo, A2.SectionNo ");
            SQL.AppendLine("        )N On A.DocNo = N.DocNo And A2.SectionNo = N.SectionNo  ");
            SQL.AppendLine("    )X  ");
            //SQL.AppendLine("    Group by X.Status, X.ItGrpName, X.SectionNo, X.DNo, X.Vlegal, X.VlegalDt,  X.PEB, X.PEBDt, X.LocalDocno, X.PlaceDelivery "); 
            SQL.AppendLine(")Y ");
            SQL.AppendLine("WHERE (Y.PebDt BETWEEN @DocDt1 AND @DocDt2) ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Status",
                        "Jenis"+Environment.NewLine+"Produksi",
                        "V-Legal#", 
                        "Tanggal"+Environment.NewLine+"V-Legal",
                        "Volume"+Environment.NewLine+"V-Legal",

                        //6-10
                        "UoM",
                        "FOB",
                        "PEB#",
                        "Tanggal PEB",
                        "Volume"+Environment.NewLine+"PEB",

                        //11-12
                        "Invoice#",
                        "Negara"+Environment.NewLine+"Tujuan"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 150, 180, 120, 100, 
                        
                        //6-10
                        100, 150, 180, 120, 120, 

                        //11-12
                        120, 250
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 7, 10 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "Y.Status", true);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "Y.LocalDocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter +
                    " Group by Y.Status, Y.ItGrpName, Y.Vlegal, Y.InventoryUoMCode, Y.VlegalDt, Y.PEB, Y.PEBDt, Y.LocalDocno, Y.PlaceDelivery; ",
                   new string[]
                            { 
                                //0
                                "Stat",  

                                //1-5
                                "ItGrpName", "Vlegal", "VlegalDt", "VolVlegal", "InventoryUOMCode", 

                                //6-10
                                "FOB", "PEB", "PEBDt", "VolPEB", "LocalDocno", 

                                //11
                                "PlaceDelivery"
                            },

                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 7, 10 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mTHC20Feet = Sm.GetParameter("THC20Feet");
            mTHC40Feet = Sm.GetParameter("THC40Feet");
        }

        public static void SetLueStatus(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Col1, Col2 From ( " +
                "Select 'P' As Col1, 'Planning' As Col2  " +
                "Union All " +
                "Select 'R' As Col1, 'Released' As Col2  " +
                "Union All " +
                "Select 'C' As Col1, 'Cancelled' As Col2  " +
                "Union All " +
                "Select 'F' As Col1, 'Final' As Col2  " +
                ")T ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 7, 10 });
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local document#");
        }

        #endregion

        #endregion
    }
}
