﻿#region Update
/*
    07/04/2020 [WED/SRN] new apps, CtQt pakai discount rate 5 biji
 */

#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
#endregion

namespace RunSystem
{

    public partial class FrmCtQt4 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mCtCode = string.Empty;
        internal bool
            mIsCtQtUseUPrice2 = false,
            mIsCustomerItemNameMandatory = false,
            mIsCtQtUseItemType = false,
            mIsCtQtUseDiscountAmt = false;
        internal string mCtQtBusinessProcess = "1";
        internal FrmCtQt4Find FrmFind;
        private string mRuleToDeactivateCtQt = string.Empty; // 1 : All, 2 : IOK
        private bool mIsOptionForALLEnabled = false,
            mIsCtQtUseQty = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmCtQt4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Customer Quotation with Discount Rate Level";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueRingCode(ref LueRingCode);
                Sl.SetLueCurCode(ref LueCurCode);
                SetLueDTCode(ref LueShpMCode);
                Sl.SetLuePtCode(ref LuePtCode);
                SetLueSPCode(ref LueSPCode);
                Sl.SetLueAgingAP(ref LueAgingAP);
                Sl.SetLueOption(ref LueCtQtType, "CustomerQuotationType");
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                tabControl1.SelectTab("TpgPort");
                LuePort.Visible = false;

                tabControl1.SelectTab("TpgQuotationItem");
                LueUomCode.Visible = false;
                LueCtQtType.Visible = false;

                tabControl1.SelectTab("TpgAgent");


                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mCtCode.Length != 0)
                {
                    if (IsUserAbleToInsert())
                    {
                        BtnInsertClick();
                        Sm.SetLue(LueCtCode, mCtCode);
                        Sm.SetControlReadOnly(LueCtCode, true);
                        SetLueCtPersonCode(ref LueCtContactPersonName, mCtCode);
                    }
                    else
                    {
                        BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                    }
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 5;
            Sm.GrdHdrWithColWidth
            (
                Grd1,
                new string[]
                {
                    //0
                    "",
 
                    //1-4
                    "City Code", "City Name", "Province", "Country" 
                },
                new int[] { 20, 100, 200, 200, 150 }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4 }, true);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdColButton(Grd1, new int[] { 0 });

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 27;
            Grd2.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "",
                    "Item's Code",
                    "Local Code",
                    "Item's Name",
                    "Price List"+Environment.NewLine+"Document#",

                    //6-10
                    "Price List"+Environment.NewLine+"Dno",
                    "UoM",
                    "Currency",
                    "Previous Price",
                    "Price",

                    //11-15
                    "Price After"+Environment.NewLine+"Discount",
                    "Discount"+Environment.NewLine+"(%)",
                    "Remark",
                    "Specification",
                    "Customer"+Environment.NewLine+"Item's Code",

                    //16-20
                    "Customer"+Environment.NewLine+"Item's Name",
                    "ItemType",
                    "Type",
                    "Quantity",
                    "Amount",

                    //21-25
                    "Discount"+Environment.NewLine+"Amount",
                    "Discount (%)"+Environment.NewLine+"(1)",
                    "Discount (%)"+Environment.NewLine+"(2)",
                    "Discount (%)"+Environment.NewLine+"(3)",
                    "Discount (%)"+Environment.NewLine+"(4)",

                    //26
                    "Discount (%)"+Environment.NewLine+"(5)",
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    20, 100, 150, 250, 150,  

                    //6-10
                    100, 50, 80, 100, 100, 

                    //11-15
                    100, 80, 400, 120, 120,

                    //16-20
                    250, 80, 120, 100, 100,

                    //21-25
                    100, 100, 100, 100, 100, 

                    //26
                    100
                }
            );
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 15, 16, 17, 18, 19, 20 });
            if (mCtQtBusinessProcess!="1")
                Sm.GrdColReadOnly(true, true, Grd2, new int[] { 11 });
            else
                Sm.GrdColReadOnly(true, true, Grd2, new int[] { 12, 21 });
            Sm.GrdFormatDec(Grd2, new int[] { 9, 10, 11, 12,19, 20, 21, 22, 23, 24, 25, 26 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 0, 1 });

            Grd2.Cols[14].Move(5);
            Grd2.Cols[16].Move(6);
            Grd2.Cols[15].Move(6);
            Grd2.Cols[18].Move(16);
            Grd2.Cols[19].Move(8);
            Grd2.Cols[20].Move(15);
            Grd2.Cols[21].Move(18);
            Grd2.Cols[26].Move(16);
            Grd2.Cols[25].Move(16);
            Grd2.Cols[24].Move(16);
            Grd2.Cols[23].Move(16);
            Grd2.Cols[22].Move(16);

            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 5, 6, 12, 14, 17, 21 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd2, new int[] { 16 });
            if (!mIsCtQtUseItemType)
                Sm.GrdColInvisible(Grd2, new int[] { 17, 18 });
            if (!mIsCtQtUseQty)
                Sm.GrdColInvisible(Grd2, new int[] { 19, 20 });
            if (!mIsCtQtUseDiscountAmt)
                Sm.GrdColInvisible(Grd2, new int[] { 21 });

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 3;
            Sm.GrdHdrWithColWidth
            (
                Grd3,
                new string[]
                {
                    "", "Agent Code", "Agent Name"
                },
                new int[] { 20, 100, 200 }
            );
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2 }, true);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, false);
            Sm.GrdColButton(Grd3, new int[] { 0 });

            #endregion

            #region Grid 4

            Grd4.Cols.Count = 2;
            Sm.GrdHdrWithColWidth
            (
                Grd4,
                new string[]
                {
                    "", "Port Name"
                },
                new int[] { 80, 200 }
            );
            Sm.GrdColInvisible(Grd4, new int[] { 0 }, false);

            #endregion

        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 5, 14, }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueCtCode, LueCtContactPersonName,LueRingCode, LueSPCode, DteQtStartDt, TxtQtMth, TxtSalesTarget, LuePtCode,
                        LueAgingAP, LueCurCode, TxtCreditLimit, LueShpMCode, TxtMinDelivery, TxtGoodsReturnDay, MeeWhsCapacity, TxtCtReplace, MeeRemark,
                        LueCtQtType
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    ChkPrintSignatureInd.Properties.ReadOnly = true;
                    BtnCtContactPersonName.Enabled = false;
                    BtnCtReplace.Enabled = false;
                    BtnCtReplace2.Enabled = true;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 13, 18, 19, 20 });
                    if (mCtQtBusinessProcess != "1")
                        Sm.GrdColReadOnly(true, true, Grd2, new int[] { 12, 21 });
                    else
                        Sm.GrdColReadOnly(true, true, Grd2, new int[] { 11 });
                    Grd1.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    Grd4.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, LueSPCode, DteQtStartDt, TxtQtMth, TxtSalesTarget, LuePtCode, LueRingCode,
                        LueAgingAP, LueCurCode, TxtCreditLimit, LueShpMCode, TxtMinDelivery, TxtGoodsReturnDay, MeeWhsCapacity, MeeRemark,
                        LueCtQtType
                    }, false);
                    DteDocDt.Focus();
                    TxtQtMth.EditValue = '0';
                    ChkActInd.Checked = true;
                    ChkPrintSignatureInd.Properties.ReadOnly = false;
                    ChkPrintSignatureInd.Checked = true;
                    BtnCtContactPersonName.Enabled = true;
                    BtnCtReplace.Enabled = true;
                    BtnCtReplace2.Enabled = true;
                    Grd1.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 13, 18, 19 });
                    if (mCtQtBusinessProcess != "1")
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 12, 21 });
                    else
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 11 });
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    BtnCtReplace.Enabled = false;
                    BtnCtReplace2.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtQtMth, LueCtCode, LueCtContactPersonName,LueRingCode, LueSPCode, DteQtStartDt,LuePtCode,
                LueAgingAP, LueCurCode, LueShpMCode, MeeWhsCapacity, TxtCtReplace, LueCtQtType, MeeRemark
            });
            ClearGrd();
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>
            {
                TxtSalesTarget, TxtCreditLimit, TxtMinDelivery, TxtGoodsReturnDay
            }, 0);
            ChkPrintSignatureInd.Checked = false;
            ChkActInd.Checked = false;
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            ClearGrd2();
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd4, true);
        }

        internal void ClearGrd2()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 9, 10, 11, 12, 19, 20, 21, 22, 23, 24, 25, 26 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCtQt4Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteQtStartDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Grid Method

        #region Grid 1
        private void Grd1_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && Sm.GetLue(LueCtCode).Length != 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmCtQt4Dlg(this, Sm.GetLue(LueCtCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && Sm.GetLue(LueCtCode).Length != 0) Sm.FormShowDialog(new FrmCtQt4Dlg(this, Sm.GetLue(LueCtCode)));
        }

        private void Grd1_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 2

        private void Grd2_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmCtQt4Dlg2(
                    this,
                    Sm.GetDte(DteDocDt),
                    Sm.GetLue(LueCurCode),
                    Sm.GetLue(LueCtCode),
                    TxtCtReplace.Text
                    ));

            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd2_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmCtQt4Dlg2(
                            this,
                            Sm.GetDte(DteDocDt),
                            Sm.GetLue(LueCurCode),
                            Sm.GetLue(LueCtCode),
                            TxtCtReplace.Text
                            ));
                }

                if (e.ColIndex == 15)
                {
                    SetLueUomCode(ref LueUomCode, Sm.GetGrdStr(Grd2, e.RowIndex, 2));
                    LueRequestEdit(Grd2, LueUomCode, ref fCell, ref fAccept, e);
                }

                if (e.ColIndex == 18)
                {
                    Sl.SetLueOption(ref LueCtQtType, "CustomerQuotationType");
                    LueRequestEdit(Grd2, LueCtQtType, ref fCell, ref fAccept, e);
                }

                if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
                {
                    e.DoDefault = false;
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_AfterCommitEdit(object sender, TenTec.Windows.iGridLib.iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 11, 12, 21, 22, 23, 24, 25, 26 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 13 }, e);

            if (mCtQtBusinessProcess!="1")
            {
                if (Sm.IsGrdColSelected(new int[] { 21 }, e.ColIndex))
                {
                    ComputePriceAfterDiscount(e.RowIndex);
                    ConvertDiscPercentageToDiscAmt(e.RowIndex);
                    
                }
                else if (Sm.IsGrdColSelected(new int[] { 12 }, e.ColIndex))
                {
                    ComputePriceAfterDiscount(e.RowIndex);
                    ConvertDiscAmtToDiscPercentage(e.RowIndex);
                    
                }
            }
            else
            {
                if (Sm.IsGrdColSelected(new int[] { 11 }, e.ColIndex))
                    ComputeDiscount(e.RowIndex);
            }

            if (Sm.IsGrdColSelected(new int[] { 19 }, e.ColIndex)) ComputeAmt(e.RowIndex);

            if (Sm.IsGrdColSelected(new int[] { 22, 23, 24, 25, 26 }, e.ColIndex))
            {
                ComputeDiscountRateLevel(e.RowIndex);
            }
            
        }

        private void Grd2_ColHdrClick(object sender, iGColHdrClickEventArgs e)
        {
            if (e.ColIndex == 12 && mCtQtBusinessProcess != "1")
            {
                if (Sm.StdMsgYN("Question", "Do you want to copy discount based on first record ?") == DialogResult.Yes)
                {
                    if (Sm.GetGrdStr(Grd2, 0, 12).Length != 0)
                    {
                        var Discount = Sm.GetGrdDec(Grd2, 0, 12);
                        for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0)
                            {
                                Grd2.Cells[Row, 12].Value = Discount;
                                ComputePriceAfterDiscount(Row);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Grid 3

        private void Grd3_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && Sm.GetLue(LueCtCode).Length != 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmCtQt4Dlg3(this, Sm.GetLue(LueCtCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && Sm.GetLue(LueCtCode).Length != 0) Sm.FormShowDialog(new FrmCtQt4Dlg3(this, Sm.GetLue(LueCtCode)));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 4
        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd4, LuePort, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                SetLuePort(ref LuePort);
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
                Sm.GrdEnter(Grd4, e);
                Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CtQt4", "TblCtQtHdr");

            var cml = new List<MySqlCommand>();
            var cml2 = new List<MySqlCommand>();

            cml.Add(SaveCtQtHdr(DocNo));


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveCtQtDtl2(DocNo, Row));
            }

            for (int Row2 = 0; Row2 < Grd2.Rows.Count - 1; Row2++)
            {
                if (Sm.GetGrdStr(Grd2, Row2, 2).Length > 0) cml.Add(SaveCtQtDtl(DocNo, Row2));
            }

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveCtQtDtl3(DocNo, Row));
            }

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0) cml.Add(SaveCtQtDtl4(DocNo, Row));
            }          

            if (TxtCtReplace.Text.Length != 0)
                EditCtQtCancel(TxtCtReplace.Text);

            Sm.ExecCommands(cml);

            if (Sm.GetLue(LueCtCode) == Sm.GetParameter("OnlineCtCode"))
            {
                cml2.Add(SaveToMsiWeb(DocNo));
            }

            Sm.ExecCommands(cml2);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCtContactPersonName, "Contact Person") ||
                Sm.IsLueEmpty(LueSPCode, "Sales Person") ||
                Sm.IsDteEmpty(DteQtStartDt, "Quotation Start Date") ||
                Sm.IsTxtEmpty(TxtQtMth, "Quotation Month", true) ||
                Sm.IsLueEmpty(LuePtCode, "Payment Term") ||
                Sm.IsLueEmpty(LueCurCode, "Currency Code") ||
                Sm.IsTxtEmpty(TxtCreditLimit, "Credit Limit", true) ||
                Sm.IsLueEmpty(LueShpMCode, "Shipping Method") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsQtyRequestNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Item is empty.")) return true;
                if (mIsCtQtUseQty && Sm.IsGrdValueEmpty(Grd2, Row, 19, true, "Quantitiy is 0 ")) return true;
                if (Sm.GetGrdDec(Grd2, Row, 12) < 0m || Sm.GetGrdDec(Grd2, Row, 21) < 0m)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                        "Item's Local Code : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                        "Discount should not be less than 0.");
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 city.");
                return true;
            }
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "City entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            //if (Grd2.Rows.Count > 1000)
            //{
            //    Sm.StdMsg(mMsgType.Warning,
            //        "Item entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
            //    return true;
            //}
            //if (Grd3.Rows.Count > 1000)
            //{
            //    Sm.StdMsg(mMsgType.Warning,
            //        "Agent entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
            //    return true;
            //}
            return false;
        }

        private bool IsQtyRequestNotValid()
        {
            for (int Row = 0; Row <= Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd2, Row, 11) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                            "Price after discount must be greater than 0.");
                        return true;
                    }
                }
            }

            return false;
        }


        private MySqlCommand SaveCtQtHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCtQtHdr(DocNo, DocDt, ActInd, Status, DocType, CtCode, CtContactPersonName, RingCode, SPCode, QtStartDt, QtMth, Salestarget, ");
            SQL.AppendLine("PtCode, AgingAp, CurCode, CreditLimit, ShpMCode, MinDelivery, GoodsReturnDay, WhsCapacity, PrintSignatureInd, CtQtDocNoReplace, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ActInd, 'O', 2, @CtCode, @CtContactPersonName, @RingCode, @SPCode, @QtStartDt, @QtMth, @Salestarget, ");
            SQL.AppendLine("@PtCode, @AgingAp, @CurCode, @CreditLimit, @ShpMCode, @MinDelivery, @GoodsReturnDay, @WhsCapacity, @PrintSignatureInd, @CtQtDocNoReplace, @Remark, @UserCode, CurrentDateTime()); ");

            if (Sm.GetLue(LueCtCode) != Sm.GetParameter("OnlineCtCode"))
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='CtQt'; ");
            }

            if (mRuleToDeactivateCtQt == "2") // IOK RFC
            {
                string mCtQtDocNo = string.Empty;
                mCtQtDocNo = GetCtQtDocNo(DocNo);
                if (mCtQtDocNo.Length > 0)
                {
                    SQL.AppendLine("Update TblCtQtHdr Set ");
                    SQL.AppendLine("    ActInd = 'N', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                    SQL.AppendLine("Where Find_In_Set(DocNo, '" + mCtQtDocNo + "') ");
                    SQL.AppendLine("And Not Exists ( ");
                    SQL.AppendLine("    Select DocNo From TblDocApproval ");
                    SQL.AppendLine("    Where DocType='CtQt' And DocNo=@DocNo ");
                    SQL.AppendLine("); ");
                }
            }
            else // Standard
            {
                SQL.AppendLine("Update TblCtQthdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where CtCode=@CtCode And DocNo<>@DocNo ");
                SQL.AppendLine("And Not Exists ( ");
                SQL.AppendLine("    Select DocNo From TblDocApproval ");
                SQL.AppendLine("    Where DocType='CtQt' And DocNo=@DocNo ");
                SQL.AppendLine("); ");
            }

            SQL.AppendLine("Update TblCtQtHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='CtQt' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCtContactPersonName));
            Sm.CmParam<String>(ref cm, "@RingCode", Sm.GetLue(LueRingCode));
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParamDt(ref cm, "@QtStartDt", Sm.GetDte(DteQtStartDt));
            Sm.CmParam<String>(ref cm, "@QtMth", TxtQtMth.Text);
            Sm.CmParam<Decimal>(ref cm, "@Salestarget", Decimal.Parse(TxtSalesTarget.Text));
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@AgingAp", Sm.GetLue(LueAgingAP));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", Decimal.Parse(TxtCreditLimit.Text));
            Sm.CmParam<String>(ref cm, "@ShpMCode", Sm.GetLue(LueShpMCode));
            Sm.CmParam<Decimal>(ref cm, "@MinDelivery", Decimal.Parse(TxtMinDelivery.Text));
            Sm.CmParam<Decimal>(ref cm, "@GoodsReturnDay", Decimal.Parse(TxtGoodsReturnDay.Text));
            Sm.CmParam<String>(ref cm, "@WhsCapacity", MeeWhsCapacity.Text);
            Sm.CmParam<String>(ref cm, "@PrintSignatureInd", ChkPrintSignatureInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtQtDocNoReplace", TxtCtReplace.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveCtQtDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCtQtDtl2(DocNo, DNo, CityCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @CityCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCtQtDtl3(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCtQtDtl3(DocNo, DNo, AgtCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @AgtCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 6));
            Sm.CmParam<String>(ref cm, "@AgtCode", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCtQtDtl4(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCtQtDtl4(DocNo, DNo, PortCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @PortCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PortCode", Sm.GetGrdStr(Grd4, Row, 0));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCtQtDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCtQtDtl(DocNo, DNo, ItemPriceDocNo, ItemPriceDNo, UPrice, DiscRt1, DiscRt2, DiscRt3, DiscRt4, DiscRt5, Discount, DiscountAmt, CtQtType, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItemPriceDocNo, @ItemPriceDNo, @UPrice, @DiscRt1, @DiscRt2, @DiscRt3, @DiscRt4, @DiscRt5, 0.00, 0.00, @CtQtType, @Qty, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblCtQtDtl A ");
            SQL.AppendLine("Inner Join TblItemPriceDtl B On A.ItemPriceDocNo = B.DocNo ");
            SQL.AppendLine("    And A.ItemPriceDNo = B.DNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("    And A.DNo = @DNo ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("A.DiscountAmt = B.UPrice - A.UPrice, ");
            SQL.AppendLine("A.Discount = ((B.UPrice - A.UPrice) / B.UPrice) * 100 ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.DNo = @DNo ");
            SQL.AppendLine("And B.UPrice != A.UPrice; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 6));
            Sm.CmParam<String>(ref cm, "@ItemPriceDocNo", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@ItemPriceDNo", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt1", Sm.GetGrdDec(Grd2, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt2", Sm.GetGrdDec(Grd2, Row, 23));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt3", Sm.GetGrdDec(Grd2, Row, 24));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt4", Sm.GetGrdDec(Grd2, Row, 25));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt5", Sm.GetGrdDec(Grd2, Row, 26));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 13));
            Sm.CmParam<String>(ref cm, "@CtQtType", Sm.GetGrdStr(Grd2, Row, 17));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 19));

            return cm;
        }

        private MySqlCommand SaveToMsiWeb(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Truncate MsiWeb.Item_summaries; ");
            SQL.AppendLine("Truncate MsiWeb.Item_info; ");
            SQL.AppendLine("Truncate MsiWeb.Item_sub_categories; ");
            SQL.AppendLine("Truncate MsiWeb.Item_Groups; ");

            SQL.AppendLine("Insert into MsiWeb.Item_Summaries(item_code,item_group_code,name,remark,size,color,weight,weight_uom_code,height,");
            SQL.AppendLine("height_uom_code,length,length_uom_code,width,width_uom_code,volume,volume_uom_code,diameter,diameter_uom_code,min_stock,reorder_stock,Avl_stock) ");
            SQL.AppendLine("Select D.itCode, ifnull(E.ItgrpCode, '-') As ItGrpCode, E.Itname, ifnull(E.remark, '-') As Remark, ifnull(G.OptDesc, '-') As Info1, ifnull(H.OptDesc, '-') As Info2, ifnull(I.GW, 0) As Weight, I.UomCode as WeightUomCode, ");
            SQL.AppendLine("ifnull(E.Height, 0) As Height, E.HeightUomCode, ifnull(E.length, 0) As length, E.LengthUomCode, ifnull(E.Width, 0) As Width, E.WidthUomCode, ifnull(E.Volume, 0) As Volume, E.VolumeUomCode,");
            SQL.AppendLine("ifnull(E.Diameter, 0) As Diameter, E.DiameterUomCode, ifnull(E.MinStock, 0) MinStcok, ifnull(E.ReorderStock, 0) As ReorderStock, ifnull(F.Stock, 0) As Stock ");
            SQL.AppendLine("from TblCtQthdr A ");
            SQL.AppendLine("Inner Join TblCtQtDtl B On A.DocNo =B.DocNo ");
            SQL.AppendLine("Inner Join tblItemPricehdr C On B.ItemPriceDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblitemPriceDtl D On B.ItemPriceDocNo = D.DocNo And B.ItemPriceDno = D.Dno ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode = E.ItCode ");
            SQL.AppendLine("Left Join TblOption G On E.Information1 = G.OptCode And G.OptCat = 'ItemInformation1' ");
            SQL.AppendLine("Left Join TblOption H On E.Information2 = H.OptCode And H.OptCat = 'ItemInformation2' ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.ItCode, A.Qty, A.GW, B.PArvalue As UomCode from TblItemPackagingUnit A ");
            SQL.AppendLine("    Inner Join TblParameter B On 0=0 And B.ParCode = 'SIWeightUom' ");
            SQL.AppendLine(")I On D.ItCode = I.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X2.ItCode, X2.StockQty, X2.DRQty, DOWhsQty, (IfNull(X2.StockQty, 0)-IfNull(X2.DRQty, 0)-IfNull(X2.DOWhsQty, 0)) As Stock ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("    Select A.ItCode, IfNull(B.StockQty, 0) As StockQty, IfNull(C.DRQty, 0) As DRQty, IfNull(D.DOWhsQty, 0) As DOWhsQty ");
            SQL.AppendLine("            From TblItem A ");
            SQL.AppendLine("            Left join ( ");
            SQL.AppendLine("                Select ItCode, Sum(Qty) As StockQty ");
            SQL.AppendLine("                From TblStockSummary  ");
            SQL.AppendLine("                Where ItCode In ( ");
            SQL.AppendLine("                    Select T6.ItCode ");
            SQL.AppendLine("                    From TblCtQtDtl T5  ");
            SQL.AppendLine("                    Inner Join TblItemPriceDtl T6 On T5.ItemPriceDocNo=T6.DocNo And T5.ItemPriceDNo=T6.DNo  ");
            SQL.AppendLine("                    Where T5.DocNo=@DocNo ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("                And Qty<>0 ");
            SQL.AppendLine("                Group By ItCode   ");
            SQL.AppendLine("            ) B On A.ItCode=B.ItCode ");
            SQL.AppendLine("            Left join ( ");
            SQL.AppendLine("                Select T6.ItCode, Sum(T2.QtyInventory) As DRQty ");
            SQL.AppendLine("                From TblDRHdr T1 ");
            SQL.AppendLine("                Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo ");
            SQL.AppendLine("                Inner Join TblSODtl T4 On T2.SODocNo=T4.DocNo And T2.SODNo=T4.DNo ");
            SQL.AppendLine("                Inner Join TblCtQtDtl T5 On T3.CtQtDocNo=T5.DocNo And T4.CtQtDNo=T5.DNo ");
            SQL.AppendLine("                Inner Join TblItemPriceDtl T6 On T5.ItemPriceDocNo=T6.DocNo And T5.ItemPriceDNo=T6.DNo  ");
            SQL.AppendLine("                Where T1.CancelInd='N' And T1.ProcessInd='O' ");
            SQL.AppendLine("                Group By T6.ItCode ");
            SQL.AppendLine("            ) C On A.ItCode=C.ItCode ");
            SQL.AppendLine("            Left join ( ");
            SQL.AppendLine("                Select T2.ItCode, Sum(T2.Qty) As DOWhsQty ");
            SQL.AppendLine("                From TblDOWhsHdr T1 ");
            SQL.AppendLine("                Inner Join TblDOWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd='O' ");
            SQL.AppendLine("                Where T1.CancelInd='N'  ");
            SQL.AppendLine("                Group By T2.ItCode ");
            SQL.AppendLine("            ) D On A.ItCode=D.ItCode ");
            SQL.AppendLine("            Where A.ItCode In ( ");
            SQL.AppendLine("                    Select T6.ItCode ");
            SQL.AppendLine("                    From TblCtQtDtl T5  ");
            SQL.AppendLine("                    Inner Join TblItemPriceDtl T6 On T5.ItemPriceDocNo=T6.DocNo And T5.ItemPriceDNo=T6.DNo ");
            SQL.AppendLine("                    Where T5.DocNo=@DocNo ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("    )X2 ");
            SQL.AppendLine(")F On F.ItCode = E.ItCode ");
            SQL.AppendLine("Where CtCode = (Select ParValue from RunsystemMsi.Tblparameter Where parCode='OnlineCtCode') ");
            SQL.AppendLine("And A.ActInd = 'Y' And A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into MsiWeb.Item_Info(ct_Code, Item_code, Qt_Doc_No, Qt_D_No, Cur_Code, unit_price, discount, Tax_rate) ");
            SQL.AppendLine("Select (Select ParValue from RunsystemMsi.Tblparameter Where parCode='OnlineCtCode') As CtCode,");
            SQL.AppendLine("D.itCode, A.DocNo, B.DNo, A.CurCode, D.UPrice, B.Discount, 10 as tax ");
            SQL.AppendLine("from TblCtQthdr A ");
            SQL.AppendLine("Inner Join TblCtQtDtl B On A.DocNo =B.DocNo ");
            SQL.AppendLine("Inner Join tblItemPricehdr C On B.ItemPriceDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblitemPriceDtl D On B.ItemPriceDocNo = D.DocNo And B.ItemPriceDno = D.Dno ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode = E.ItCode ");
            SQL.AppendLine("Where CtCode = (Select ParValue from RunsystemMsi.Tblparameter Where parCode='OnlineCtCode') ");
            SQL.AppendLine("And A.ActInd = 'Y' And A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into MsiWeb.Item_groups(Code, name, sub_cat_Code) ");
            SQL.AppendLine("Select E.ItGrpCode, F.ItGrpname, E.ItScCode ");
            SQL.AppendLine("from TblCtQthdr A ");
            SQL.AppendLine("Inner Join TblCtQtDtl B On A.DocNo =B.DocNo ");
            SQL.AppendLine("Inner Join tblItemPricehdr C On B.ItemPriceDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblitemPriceDtl D On B.ItemPriceDocNo = D.DocNo And B.ItemPriceDno = D.Dno ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode = E.ItCode ");
            SQL.AppendLine("Inner Join TblItemGroup F On E.ItGrpCode = F.ItGrpCode ");
            SQL.AppendLine("Where CtCode = (Select ParValue from RunsystemMsi.Tblparameter Where parCode='OnlineCtCode') ");
            SQL.AppendLine("And A.ActInd = 'Y' And A.DocNo=@DocNo And E.ItGrpCode is not null ");
            SQL.AppendLine("Group By E.ItGrpCode ; "); 
                
            SQL.AppendLine("Insert Into MsiWeb.Item_sub_categories(Code, name, active) ");
            SQL.AppendLine("Select Distinct E.ItScCode, F.ItScname, 'Y' ");
            SQL.AppendLine("from TblCtQthdr A ");
            SQL.AppendLine("Inner Join TblCtQtDtl B On A.DocNo =B.DocNo ");
            SQL.AppendLine("Inner Join tblItemPricehdr C On B.ItemPriceDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblitemPriceDtl D On B.ItemPriceDocNo = D.DocNo And B.ItemPriceDno = D.Dno ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode = E.ItCode ");
            SQL.AppendLine("Inner Join TblItemSubCategory F On E.ItScCode = F.ItScCode ");
            SQL.AppendLine("Where CtCode = (Select ParValue from RunsystemMsi.Tblparameter Where parCode='OnlineCtCode') ");
            SQL.AppendLine("And A.ActInd = 'Y' And A.DocNo=@DocNo And E.ItScCode is not null; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsActIndDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            EditCtQtCancel(TxtDocNo.Text);
            ShowData(TxtDocNo.Text);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Customer Quotation", false) ||
                IsActIndEditedAlready();
        }

        private bool IsActIndEditedAlready()
        {
            var ActInd = ChkActInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblCtQtHdr Where DocNo=@DocNo And ActInd='N' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ActInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already non-active.");
                return true;
            }
            return false;
        }

        private void EditCtQtCancel(string DocNo)
        {
            if (Sm.GetLue(LueCtCode).Length>0)
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Update TblCtQtHdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                        "Where DocNo=@DocNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);
            }
        }

        #region cancel OLD
        //private void EditCtQtCancel2(string DocNoReplace)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Update TblCtQtHdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
        //            "Where DocNo=@DocNo1; " +
        //            "Update TblCtQtHdr Set ActInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
        //            "Where DocNo=@DocNo"
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo1", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNoReplace);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    Sm.ExecCommand(cm);
        //}

        #endregion 

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowCtQtHdr(DocNo);
                //tabControl1.SelectTab("TpgQuotationCity");
                ShowCtQtDtl(DocNo);
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    ComputeAmt(Row);
                }
                //tabControl1.SelectTab("TpgAgent");
                ShowCtQtDtl3(DocNo);
                //tabControl1.SelectTab("TpgQuotationItem");
                ShowCtQtDtl2(DocNo);
                ShowCtQtDtl4(DocNo);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCtQtHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, ActInd, CtCode, CtContactPersonName, RingCode, SPCode, QtStartDt, QtMth, SalesTarget, PtCode, " +
                    "AgingAP, CurCode, CreditLimit, ShpMCode, MinDelivery, GoodsReturnDay, WhsCapacity, PrintSignatureInd, CtQtDocNoReplace, Remark From TblCtQtHdr Where DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "ActInd", "CtCode", "CtContactPersonName", "RingCode", 
                        
                        //6-10
                        "SPCode", "QtStartDt", "QtMth", "SalesTarget", "PtCode",
 
                        //11-15
                        "AgingAP", "CurCode", "CreditLimit", "ShpMCode", "MinDelivery",

                        //16-20
                        "GoodsReturnDay", "WhsCapacity", "PrintSignatureInd", "CtQtDocNoReplace", "Remark"
                        
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                        SetLueCtPersonCode(ref LueCtContactPersonName, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCtContactPersonName, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueRingCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteQtStartDt, Sm.DrStr(dr, c[7]));
                        TxtQtMth.EditValue = Sm.DrStr(dr, c[8]);
                        TxtSalesTarget.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        Sm.SetLue(LuePtCode, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LueAgingAP, Sm.DrStr(dr, c[11]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[12]));
                        TxtCreditLimit.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                        Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[14]));
                        TxtMinDelivery.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0);
                        TxtGoodsReturnDay.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                        MeeWhsCapacity.EditValue = Sm.DrStr(dr, c[17]);
                        ChkPrintSignatureInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[18]), "Y");
                        TxtCtReplace.EditValue = Sm.DrStr(dr, c[19]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                    }, true
                );
        }

        private void ShowCtQtDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CityCode, C.CityName, D.ProvName, E.CntName ");
            SQL.AppendLine("From TblCtQtHdr A ");
            SQL.AppendLine("Inner Join TblCtQtDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblCity C On B.CityCode=C.CityCode ");
            SQL.AppendLine("left Join TblProvince D On C.ProvCode = D.ProvCode ");
            SQL.AppendLine("Left Join TblCountry E On D.CntCode = E.CntCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CityCode", "CityName", "ProvName", "CntName" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowCtQtDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, D.ItCode, E.ItCodeInternal, E.ItName, E.Specification, G.CtItCode, B.ItemPriceDocNo, B.ItemPriceDNo, ");
            SQL.AppendLine("C.PriceUomCode, C.CurCode, IfNull(F.PrevPrice, 0) As PrevPrice, G.CtItName, ");
            SQL.AppendLine("D.UPrice, B.UPrice As PriceAf, B.Discount, B.CtQtType, H.OptDesc As CtQtTypename, B.Remark, B.Qty, B.DiscountAmt, ");
            SQL.AppendLine("B.DiscRt1, B.DiscRt2, B.DiscRt3, B.DiscRt4, B.DiscRt5 ");
            SQL.AppendLine("From TblCtQtHdr A ");
            SQL.AppendLine("Inner Join TblCtQtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblItemPriceHdr C On B.ItemPriceDocNo=C.DocNo And C.CurCode = @CurCode ");
            SQL.AppendLine("Left Join TblItemPriceDtl D On B.ItemPriceDocNo=D.DocNo And B.ItemPriceDNo=D.Dno ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.ItCode, T3.PriceUomCode, T2.UPrice As PrevPrice ");
            SQL.AppendLine("    From TblCtQtHdr T1 ");
            SQL.AppendLine("    Inner Join TblCtQtDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl T4 On T2.ItemPriceDocNo=T4.DocNo And T2.ItemPriceDNo=T4.DNo ");
            SQL.AppendLine("    Inner Join (");
            SQL.AppendLine("        Select X4.ItCode, X3.CurCode, X3.PriceUomCode, Max(Concat(X1.DocDt, X1.DocNo, X2.Dno)) As Key1 ");
            SQL.AppendLine("        From TblCtQtHdr X1 ");
            SQL.AppendLine("        Inner Join TblCtQtDtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr X3 On X2.ItemPriceDocNo=X3.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl X4 On X2.ItemPriceDocNo=X4.DocNo And X2.ItemPriceDNo=X4.DNo ");
            SQL.AppendLine("        Where X1.CtCode=@CtCode ");
            SQL.AppendLine("        And X1.CurCode=@CurCode ");
            SQL.AppendLine("        And X1.DocNo<>@DocNo ");
            SQL.AppendLine("        And X1.DocNo<=@DocNo ");
            SQL.AppendLine("        And X1.DocDt<=@DocDt ");
            SQL.AppendLine("        Group By X4.ItCode, X3.CurCode, X3.PriceUomCode ");
            SQL.AppendLine("        ) T5 ");
            SQL.AppendLine("        On T4.ItCode=T5.ItCode ");
            SQL.AppendLine("        And T1.CurCode=T5.CurCode ");
            SQL.AppendLine("        And T3.PriceUomCode=T5.PriceUomCode ");
            SQL.AppendLine("        And Concat(T1.DocDt, T1.DocNo, T2.Dno)=T5.Key1 ");
            SQL.AppendLine("    Where T1.CtCode=@CtCode ");
            SQL.AppendLine("    And T1.DocNo<>@DocNo ");
            SQL.AppendLine("    And T1.DocDt<=@DocDt ");
            SQL.AppendLine("    And T1.DocNo<=@DocNo ");
            SQL.AppendLine("    And T3.CurCode=@CurCode ");
            SQL.AppendLine(") F On E.ItCode=F.ItCode And C.PriceUomCode=F.PriceUomCode ");
            SQL.AppendLine("LEFT JOIN TblCustomerItem G ON D.ItCode=G.ItCode And G.CtCode=@CtCode ");
            SQL.AppendLine("Left Join TblOption H On B.CtQtType = H.OptCode And H.OptCat = 'CustomerQuotationType' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo; ");          

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItCodeInternal", "ItName", "ItemPriceDocNo", "ItemPriceDNo", "PriceUomCode",  
                    
                    //6-10
                    "CurCode", "PrevPrice", "UPrice", "PriceAf", "Discount",  
                    
                    //11-15
                    "Remark" , "Specification", "CtItCode", "CtItName", "CtQtType",

                    //16-20
                    "CtQtTypename", "Qty", "DiscountAmt", "DiscRt1", "DiscRt2",

                    //21-23
                    "DiscRt3", "DiscRt4", "DiscRt5"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15); 
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 23);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 9, 10, 11, 12, 19, 21, 22, 23, 24, 25, 26 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowCtQtDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AgtCode, B.AgtName ");
            SQL.AppendLine("From TblCtQtDtl3 A ");
            SQL.AppendLine("Left Join TblAgent B On A.AgtCode=B.AgtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.AgtName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "AgtCode", "AgtName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowCtQtDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PortCode, B.PortName ");
            SQL.AppendLine("From TblCtQtDtl4 A ");
            SQL.AppendLine("Left Join TblPort B On A.PortCode=B.PortCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.PortName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "PortCode", "PortName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ComputeDiscountRateLevel(int Row)
        {
            decimal UPrice = Sm.GetGrdDec(Grd2, Row, 10),
                DiscRt1 = Sm.GetGrdDec(Grd2, Row, 22),
                DiscRt2 = Sm.GetGrdDec(Grd2, Row, 23),
                DiscRt3 = Sm.GetGrdDec(Grd2, Row, 24),
                DiscRt4 = Sm.GetGrdDec(Grd2, Row, 25),
                DiscRt5 = Sm.GetGrdDec(Grd2, Row, 26);

            decimal UPriceAfterDiscount = UPrice;

            decimal[] Discs = { DiscRt1, DiscRt2, DiscRt3, DiscRt4, DiscRt5 };

            foreach(var x in Discs.Where(y => y != 0m))
            {
                UPriceAfterDiscount -= (x / 100m) * UPriceAfterDiscount;
            }

            Grd2.Cells[Row, 11].Value = UPriceAfterDiscount;
        }

        internal void ShowReplacementPort(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PortCode, B.PortName ");
            SQL.AppendLine("From TblCtQtDtl4 A ");
            SQL.AppendLine("Left Join TblPort B On A.PortCode=B.PortCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.PortName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "PortCode", "PortName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 1);

        }

        private string GetCtQtDocNo(string DocNo)
        {
            string mCtQtDocNo = string.Empty, mCityCode = string.Empty, mPortCode = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (mCityCode.Length > 0) mCityCode += ",";
                    mCityCode += Sm.GetGrdStr(Grd1, i, 1);
                }
            }

            for (int j = 0; j < Grd4.Rows.Count; j++)
            {
                if (Sm.GetGrdStr(Grd4, j, 0).Length > 0)
                {
                    if (mPortCode.Length > 0) mPortCode += ",";
                    mPortCode += Sm.GetGrdStr(Grd4, j, 0);
                }
            }

            SQL.AppendLine("Select Distinct A.DocNo ");
            SQL.AppendLine("From TblCtQtHdr A ");
            if(mCityCode.Length > 0)
                SQL.AppendLine("Inner Join TblCtQtDtl2 B On A.DocNo = B.DocNo And Find_In_Set(B.CityCode, @CityCode) ");

            if(mPortCode.Length > 0)
                SQL.AppendLine("Inner Join TblCtQtDtl4 C On A.DocNo = C.DocNo And Find_In_Set(C.PortCode, @PortCode) ");

            SQL.AppendLine("Where A.DocNo Not In (@DocNo) ");
            SQL.AppendLine("And A.CtCode = @CtCode ");
            SQL.AppendLine("And A.CurCode = @CurCode ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='CtQt' And DocNo=@DocNo ");
            SQL.AppendLine(") ");

            if (mCityCode.Length <= 0)
            {
                SQL.AppendLine("And Not Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select DocNo From TblCtQtDtl2 ");
                SQL.AppendLine("    Where DocNo = A.DocNo ");
                SQL.AppendLine(") ");
            }

            if (mPortCode.Length <= 0)
            {
                SQL.AppendLine("And Not Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select DocNo From TblCtQtDtl4 ");
                SQL.AppendLine("    Where DocNo = A.DocNo ");
                SQL.AppendLine(") ");
            }

            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@CityCode", mCityCode);
                Sm.CmParam<String>(ref cm, "@PortCode", mPortCode);
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (mCtQtDocNo.Length > 0) mCtQtDocNo += ",";
                        mCtQtDocNo += Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mCtQtDocNo;
        }

        private bool IsUserAbleToInsert()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblGroupMenu A, TblMenu B, TblUser C ");
            SQL.AppendLine("Where A.MenuCode=B.MenuCode ");
            SQL.AppendLine("And Left(A.AccessInd, 1)='Y' ");
            SQL.AppendLine("And B.Param='FrmCtQt' ");
            SQL.AppendLine("And A.GrpCode=C.GrpCode ");
            SQL.AppendLine("And C.UserCode=@Param;");

            return (Sm.IsDataExist(SQL.ToString(), Gv.CurrentUserCode));
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SPCode As Col1, SPName As Col2 From TblSalesPerson ");
            if (mIsOptionForALLEnabled)
                SQL.AppendLine("Union ALL Select 'All' As Col1, 'ALL' As Col2  ");
            SQL.AppendLine("Order By Col2; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void SetLueDTCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DTCode As Col1, DTName As Col2 From TblDeliveryType ");
            if (mIsOptionForALLEnabled)
                SQL.AppendLine("Union ALL Select 'All' As Col1, 'ALL' As Col2 ");
            SQL.AppendLine("Order By Col2; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mIsCtQtUseUPrice2 = Sm.GetParameter("IsCtQtUseUPrice2") == "Y";
            mCtQtBusinessProcess = Sm.GetParameter("CtQtBusinessProcess");
            mIsCustomerItemNameMandatory = Sm.GetParameter("IsCustomerItemNameMandatory") == "Y";
            mIsCtQtUseItemType = Sm.GetParameterBoo("IsCtQtUseItemType");
            mRuleToDeactivateCtQt = Sm.GetParameter("RuleToDeactivateCtQt");
            mIsOptionForALLEnabled = Sm.GetParameterBoo("IsOptionForALLEnabled");
            mIsCtQtUseQty = Sm.GetParameterBoo("IsCtQtUseQty");
            mIsCtQtUseDiscountAmt = Sm.GetParameterBoo("IsCtQtUseDiscountAmt");
        }

        internal void BtnInsertClick()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                ChkActInd.Checked = true;
                Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedCity()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedAgent()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode= '" + CtCode + "' Order By ContactPersonName",
                "Contact Person Name");
        }

        internal void ComputeDiscount(int Row)
        {
            decimal UPrice = Sm.GetGrdDec(Grd2, Row, 10);
            if (UPrice == 0)
                Grd2.Cells[Row, 12].Value = 0m;
            else
                Grd2.Cells[Row, 12].Value = 100 - ((Sm.GetGrdDec(Grd2, Row, 11) * 100) / UPrice);
        }

        internal void ConvertDiscPercentageToDiscAmt(int Row)
        {
            if (Sm.GetGrdDec(Grd2, Row, 12) == 0 && Sm.GetGrdDec(Grd2, Row, 21) == 0)
            {
                Grd2.Cells[Row, 21].Value = 0m;
                Grd2.Cells[Row, 12].Value = 0m;
            }
            else
                Grd2.Cells[Row, 12].Value = (Sm.GetGrdDec(Grd2, Row, 21) / Sm.GetGrdDec(Grd2, Row, 10)) * 100;
            //if (Sm.GetGrdDec(Grd2, Row, 12) == 0 && Sm.GetGrdDec(Grd2, Row, 21) != 0 || Sm.GetGrdDec(Grd2, Row, 12) != 0 && Sm.GetGrdDec(Grd2, Row, 21) != 0) 
               
           
        }

        internal void ConvertDiscAmtToDiscPercentage(int Row)
        {
            //if (Sm.GetGrdDec(Grd2, Row, 12) != 0 && Sm.GetGrdDec(Grd2, Row, 21) == 0 || Sm.GetGrdDec(Grd2, Row, 12) != 0 && Sm.GetGrdDec(Grd2, Row, 21) != 0)
            if (Sm.GetGrdDec(Grd2, Row, 12) == 0 && Sm.GetGrdDec(Grd2, Row, 21) == 0)
            {
                Grd2.Cells[Row, 21].Value = 0m;
                Grd2.Cells[Row, 12].Value = 0m;
            }
            else
                Grd2.Cells[Row, 21].Value = Sm.GetGrdDec(Grd2, Row, 10) - Sm.GetGrdDec(Grd2, Row, 11);
            
        }

        internal void ComputePriceAfterDiscount(int Row)
        {
            decimal UPrice = 0m, Discount = 0m;
            if (Sm.GetGrdStr(Grd2, Row, 10).Length > 0) UPrice = Sm.GetGrdDec(Grd2, Row, 10);
            if (Sm.GetGrdStr(Grd2, Row, 12).Length > 0) Discount = Sm.GetGrdDec(Grd2, Row, 12);
            Grd2.Cells[Row, 11].Value = (((100 - Discount) / 100) * UPrice);
        }

        internal void ComputeAmt(int Row)
        {
            Grd2.Cells[Row, 20].Value = Sm.GetGrdDec(Grd2, Row, 19) * Sm.GetGrdDec(Grd2, Row, 10);
        }

        private void SetLueUomCode(ref LookUpEdit Lue, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct UomCode As Col1, UomCode As Col2 From TblUom ");
            SQL.AppendLine("Where UomCode In ( ");
            SQL.AppendLine("   Select SalesUomCode From TblItem Where ItCode=@ItCode And SalesUomCode Is Not Null ");
            SQL.AppendLine("   Union All ");
            SQL.AppendLine("   Select SalesUomCode2 From TblItem Where ItCode=@ItCode And SalesUomCode2 Is Not Null ");
            SQL.AppendLine(") Order By UomName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.SetLue2(
                ref Lue, ref cm,
                35, 0, true, false, "Code", "Name", "Col2", "Col1");
        }

        private void SetLuePort(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select PortCode As Col1, PortName As Col2 From TblPort ");
            SQL.AppendLine("Where DischargeInd ='Y' ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 100, false, true, "Code", "Port", "Col2", "Col1");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetDefault()
        {
            if (mCtQtBusinessProcess != "1")
                SetDefault2();
            else
                SetDefault1();
        }

        private void SetDefault2()
        {
            ClearGrd2();

            if (
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCurCode, "Currency")
            ) return;

            var SQL = new StringBuilder();

            string CtReplace = TxtCtReplace.Text;

            SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, B.ItCtName, C.PriceUomCode, ");
            SQL.AppendLine("C.DocNo, C.DNo, C.CurCode, IfNull(C.UPrice, 0) As UPrice, C.StartDt, IfNull(D.UPrice, 0) As PrevUPrice, ");
            SQL.AppendLine("IfNull(C.UPrice, 0) As PriceAfterDisc, E.CtItCode, E.CtItName ");
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select C1.DocNo, C2.DNo, C1.CurCode, C1.PriceUomCode, C1.StartDt, C2.ItCode, C2.UPrice ");
            SQL.AppendLine("    From TblItemPriceHdr C1 ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl C2 On C1.DocNo=C2.DocNo ");
            SQL.AppendLine("    Where C1.ActInd='Y' ");
            SQL.AppendLine("    And C1.CurCode=@CurCode ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Distinct T4.ItCode, T1.CurCode, T3.PriceUomCode, T2.UPrice  ");
            SQL.AppendLine("    From TblCtQtHdr T1 ");
            SQL.AppendLine("    Inner Join TblCtQtDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl T4 On T2.ItemPriceDocNo=T4.DocNo And T2.ItemPriceDNo=T4.DNo ");
            SQL.AppendLine("    Where T1.CtCode=@CtCode ");
            SQL.AppendLine("    And T1.CurCode=@CurCode ");
            SQL.AppendLine("    And T1.ActInd='Y' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine(") D ");
            SQL.AppendLine("    On A.ItCode=D.ItCode ");
            SQL.AppendLine("    And C.CurCode=D.CurCode ");
            SQL.AppendLine("    And C.PriceUomCode=D.PriceUomCode ");
            SQL.AppendLine("Left Join TblCustomerItem E ON A.ItCode=E.ItCode And E.CtCode=@CtCode ");
            SQL.AppendLine("Where A.SalesItemInd = 'Y' ");
            SQL.AppendLine("And A.ActInd = 'Y' ");
            SQL.AppendLine("And A.ItCode In ( ");
            SQL.AppendLine("    Select X2.ItCode ");
            SQL.AppendLine("    From TblItemPriceHdr X1 ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("    Where X1.ActInd='Y' ");
            SQL.AppendLine("    And X1.CurCode=@CurCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.ItCode In ( ");
            SQL.AppendLine("    Select X3.ItCode ");
            SQL.AppendLine("    From TblCtQtHdr X1 ");
            SQL.AppendLine("    Inner Join TblCtQtDtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl X3 On X2.ItemPriceDocNo=X3.DocNo And X2.ItemPriceDNo=X3.DNo ");
            SQL.AppendLine("    Where X1.ActInd='Y' ");
            SQL.AppendLine("    And X1.Status='A' ");
            SQL.AppendLine("    And X1.CurCode=@CurCode ");
            SQL.AppendLine("    And X1.CtCode=@CtCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By A.ItName, A.ItCode, C.PriceuomCode;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@CtQtDocNoReplace", CtReplace);

            Sm.ShowDataInGrid(
               ref Grd2, ref cm,
               SQL.ToString(),
               new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItCodeInternal", "ItName", "DocNo", "DNo", "CurCode",   
                    
                    //6-10
                    "PrevUPrice", "UPrice", "PriceAfterDisc", "PriceUomCode", "CtItCode",

                    //11
                    "CtItName"
                },
               (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
               {
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 9);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                   Grd.Cells[Row, 22].Value =
                   Grd.Cells[Row, 23].Value =
                   Grd.Cells[Row, 24].Value =
                   Grd.Cells[Row, 25].Value =
                   Grd.Cells[Row, 26].Value = 0m;
                   ComputeDiscount(Row);
               }, false, false, false, false
           );
            Grd2.Rows.Add();
            Sm.SetGrdNumValueZero(Grd2, Grd2.Rows.Count - 1, new int[] { 9, 10, 11, 12, 21 });
        }

        private void SetDefault1()
        {
            ClearGrd2();

            if (
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCurCode, "Currency")
            ) return;

            var SQL = new StringBuilder();

            string CtReplace = TxtCtReplace.Text;

            SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, B.ItCtName, C.PriceUomCode, F.CtItCode, F.CtItName, ");
            SQL.AppendLine("C.DocNo, C.DNo, C.CurCode, C.UPrice, C.StartDt, IfNull(D.UPrice, 0) As PrevUPrice, ");
            if (CtReplace.Length > 0)
                SQL.AppendLine("IfNull(E.PriceAfterDisc, C.UPrice) As PriceAfterDisc ");
            else
                SQL.AppendLine("IfNull(D.UPrice, C.UPrice) As PriceAfterDisc ");
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select C1.DocNo, C2.DNo, C1.CurCode, C1.PriceUomCode, C1.StartDt, C2.ItCode, C2.UPrice ");
            SQL.AppendLine("    From TblItemPriceHdr C1 ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl C2 On C1.DocNo=C2.DocNo ");
            SQL.AppendLine("    Where C1.ActInd='Y' ");
            SQL.AppendLine("    And C1.CurCode=@CurCode ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.ItCode, T1.CurCode, T3.PriceUomCode, T2.UPrice  ");
            SQL.AppendLine("    From TblCtQtHdr T1 ");
            SQL.AppendLine("    Inner Join TblCtQtDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl T4 On T2.ItemPriceDocNo=T4.DocNo And T2.ItemPriceDNo=T4.DNo ");
            SQL.AppendLine("    Inner Join (");
            SQL.AppendLine("        Select X4.ItCode, X3.CurCode, X3.PriceUomCode, Max(Concat(X1.DocDt, X1.DocNo)) As Key1 ");
            SQL.AppendLine("        From TblCtQtHdr X1 ");
            SQL.AppendLine("        Inner Join TblCtQtDtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr X3 On X2.ItemPriceDocNo=X3.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl X4 On X2.ItemPriceDocNo=X4.DocNo And X2.ItemPriceDNo=X4.DNo ");
            SQL.AppendLine("        Where X1.CtCode=@CtCode ");
            SQL.AppendLine("        And X1.CurCode=@CurCode ");
            SQL.AppendLine("        Group By X4.ItCode, X3.CurCode, X3.PriceUomCode ");
            SQL.AppendLine("        ) T5 ");
            SQL.AppendLine("            On T4.ItCode=T5.ItCode ");
            SQL.AppendLine("            And T1.CurCode=T5.CurCode ");
            SQL.AppendLine("            And T3.PriceUomCode=T5.PriceUomCode ");
            SQL.AppendLine("            And Concat(T1.DocDt, T1.DocNo)=T5.Key1 ");
            SQL.AppendLine(") D ");
            SQL.AppendLine("    On A.ItCode=D.ItCode ");
            SQL.AppendLine("    And C.CurCode=D.CurCode ");
            SQL.AppendLine("    And C.PriceUomCode=D.PriceUomCode ");

            if (CtReplace.Length > 0)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T4.ItCode, T2.UPrice As PriceAfterDisc ");
                SQL.AppendLine("    From TblCtQtHdr T1 ");
                SQL.AppendLine("    Inner Join TblCtQtDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo=T3.DocNo ");
                SQL.AppendLine("    Inner Join TblItemPriceDtl T4 On T2.ItemPriceDocNo=T4.DocNo And T2.ItemPriceDNo=T4.DNo ");
                SQL.AppendLine("    Where T1.DocNo=@CtQtDocNoReplace ");
                SQL.AppendLine(") E On A.ItCode=E.ItCode ");
            }
            SQL.AppendLine("Left Join TblCustomerItem F ON A.ItCode=F.ItCode And F.CtCode=@CtCode ");
            SQL.AppendLine("Where A.SalesItemInd = 'Y' ");
            SQL.AppendLine("And A.ItCode In ( ");
            SQL.AppendLine("    Select Distinct T4.ItCode ");
            SQL.AppendLine("    From TblCtQtHdr T1 ");
            SQL.AppendLine("    Inner Join TblCtQtDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl T4 On T2.ItemPriceDocNo=T4.DocNo And T2.ItemPriceDNo=T4.DNo ");
            SQL.AppendLine("    Where T1.CtCode=@CtCode ");
            SQL.AppendLine("    And T1.CurCode=@CurCode ");

            if (CtReplace.Length == 0)
            {
                SQL.AppendLine("        And Concat(T1.DocDt, T1.DocNo) In ( ");
                SQL.AppendLine("            Select Max(Concat(DocDt, DocNo)) As Key1 ");
                SQL.AppendLine("            From TblCtQtHdr ");
                SQL.AppendLine("            Where CtCode=@CtCode ");
                SQL.AppendLine("            And CurCode=@CurCode ");
                SQL.AppendLine("        ) ");
            }
            else
            {
                SQL.AppendLine("        And T1.DocNo=@CtQtDocNoReplace ");
            }
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Order By A.ItName, A.ItCode, C.PriceuomCode;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@CtQtDocNoReplace", CtReplace);

            Sm.ShowDataInGrid(
               ref Grd2, ref cm,
               SQL.ToString(),
               new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItCodeInternal", "ItName", "DocNo", "DNo", "CurCode",   
                    
                    //6-10
                    "PrevUPrice", "UPrice", "PriceAfterDisc", "PriceUomCode", "CtItCode",

                    //11
                    "CtItName"
                },
               (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
               {
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                   Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 9);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                   Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                   Grd.Cells[Row, 22].Value =
                   Grd.Cells[Row, 23].Value =
                   Grd.Cells[Row, 24].Value =
                   Grd.Cells[Row, 25].Value =
                   Grd.Cells[Row, 26].Value = 0m;
                   ComputeDiscount(Row);
               }, false, false, false, false
           );
            Grd2.Rows.Add();
            Sm.SetGrdNumValueZero(Grd2, Grd2.Rows.Count - 1, new int[] { 9, 10, 11, 12, 21 });
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            string Doctitle = Sm.GetParameter("DocTitle");

            var l = new List<CtQtHdr>();
            var l2 = new List<CtQtHdr2>();
            var ldtl = new List<CtQtDtl>();

            string[] TableName = { "CtQtHdr", "CtQtHdr2", "CtQtDtl" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyEmail',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d-%M-%y')As DocDt, A.CtContactPersonName As CPName, B.CtName, B.Address, ");
            SQL.AppendLine("ifnull(C.SpName, A.SpCode) As SpName, D.PtName, E.DtName, DATE_FORMAT(A.QtStartDt,'%d-%M-%y')As QtStartDt, ");
            SQL.AppendLine("DATE_FORMAT(Date_Add(A.QtStartDt, interval A.QtMth month),'%d-%M-%y')As EndDt, F.UserName, Concat(IfNull(G.ParValue, ''), F.UserCode, '.JPG') As EmpPict ");
            SQL.AppendLine("From TblCtQtHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("Left Join TblSalesPerson C On A.SpCode=C.SpCode ");
            SQL.AppendLine("Left Join TblPaymentTerm D On A.PtCode=D.PtCode ");
            SQL.AppendLine("Left Join TblDeliveryType E On A.ShpMCode=E.DtCode ");
            SQL.AppendLine("Inner Join Tbluser F On A.CreateBy=F.UserCode  ");
            SQL.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyEmail",

                         //6-10
                         "DocNo",
                         "DocDt",
                         "CPName",
                         "CtName",
                         "Address",
                         
                         //11-15
                         "SpName",
                         "PtName",
                         "DtName",
                         "QtStartDt",
                         "EndDt",
                         
                         //16-17
                         "UserName",
                         "EmpPict"

                         
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new CtQtHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyEmail = Sm.DrStr(dr, c[5]),
                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            CPName = Sm.DrStr(dr, c[8]),
                            CtName = Sm.DrStr(dr, c[9]),
                            Address = Sm.DrStr(dr, c[10]),
                            SpName = Sm.DrStr(dr, c[11]),
                            PtName = Sm.DrStr(dr, c[12]),
                            DtName = Sm.DrStr(dr, c[13]),
                            QtStartDt = Sm.DrStr(dr, c[14]),
                            EndDt = Sm.DrStr(dr, c[15]),
                            UserName = Sm.DrStr(dr, c[16]),
                            EmpPict = Sm.DrStr(dr, c[17]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Header2
            var cm1 = new MySqlCommand();
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL1.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd ");
            SQL1.AppendLine("from TblDocApproval A ");
            SQL1.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL1.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL1.AppendLine("Where DocType = 'CtQt' ");
            SQL1.AppendLine("And DocNo =@DocNo ");
            SQL1.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandText = SQL1.ToString();
                Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-3
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd"

                        
                        });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        l2.Add(new CtQtHdr2()
                        {
                            ApprovalDno = Sm.DrStr(dr1, c1[0]),
                            UserCode = Sm.DrStr(dr1, c1[1]),
                            UserName = Sm.DrStr(dr1, c1[2]),
                            EmpPict = Sm.DrStr(dr1, c1[3]),
                            SignInd = Sm.DrStr(dr1, c1[4]),
                        });
                    }
                }

                dr1.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.DocNo, E.ItName, D.UPrice, B.Discount, (D.UPrice)-(D.UPrice)*((B.Discount)/100) As SubTotal, F.CtItName, ");
                SQLDtl.AppendLine("(Select ParValue From TblParameter Where ParCode = 'IsCustomerItemNameMandatory') As IsCustomerItemNameMandatory, G.CurName ");
                SQLDtl.AppendLine("From TblCtQtHdr A ");
                SQLDtl.AppendLine("Inner Join TblCtQtDtl B On A.DocNo=B.DocNo ");
                SQLDtl.AppendLine("Left Join TblItemPriceHdr C On B.ItemPriceDocNo=C.DocNo And C.CurCode = @CurCode ");
                SQLDtl.AppendLine("Left Join TblItemPriceDtl D On B.ItemPriceDocNo=D.DocNo And B.ItemPriceDNo=D.Dno ");
                SQLDtl.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQLDtl.AppendLine("Left Join TblCustomerItem F On A.CtCode = F.CtCode And E.ItCode = F.ItCode ");
                SQLDtl.AppendLine("Left Join TblCurrency G On A.Curcode=G.Curcode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo Order By B.DNo ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "DocNo" ,

                     //1-5
                     "ItName" ,
                     "UPrice",
                     "Discount",
                     "SubTotal",
                     "CtItName",

                     //6-7
                     "IsCustomerItemNameMandatory",
                     "CurName"
                    });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new CtQtDtl()
                        {
                            DocNo = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            UPrice = Sm.DrDec(drDtl, cDtl[2]),
                            Discount = Sm.DrDec(drDtl, cDtl[3]),
                            SubTotal = Sm.DrDec(drDtl, cDtl[4]),
                            CtItName = Sm.DrStr(drDtl, cDtl[5]),
                            IsCustomerItemNameMandatory = Sm.DrStr(drDtl, cDtl[6]),
                            CurName = Sm.DrStr(drDtl, cDtl[7]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion
            if (Doctitle == "MSI")
            {
                Sm.PrintReport("CtQtMSI", myLists, TableName, false);
            }
            else
            {
                Sm.PrintReport("CtQt", myLists, TableName, false);
            }

        }
        
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LblSetDefault_Click(object sender, EventArgs e)
        {
            try
            {
                SetDefault();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                TxtCtReplace.EditValue = null;
                Sm.ClearGrd(Grd2, true);
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                if (Sm.GetLue(LueCtCode).Length == 0)
                {
                    LueCtContactPersonName.EditValue = null;
                    Sm.SetControlReadOnly(LueCtContactPersonName, true);
                }
                else
                {
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode));
                    Sm.SetControlReadOnly(LueCtContactPersonName, false);
                }
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtCtReplace  
                });
                Sm.ClearGrd(Grd1, true);
                Sm.ClearGrd(Grd2, true);
                Sm.ClearGrd(Grd3, true);
            }
        }

        private void BtnCtContactPersonName_Click_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mMenuCode = "RSYYNNN";
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode));
                }
            }
        }

        private void LueCtContactPersonName_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCtContactPersonName, new Sm.RefreshLue2(SetLueCtPersonCode), Sm.GetLue(LueCtCode));
        }

        private void LueRingCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueRingCode, new Sm.RefreshLue1(Sl.SetLueRingCode));
        }

        private void LueSPCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LuePtCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LueAgingAP_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueAgingAP, new Sm.RefreshLue1(Sl.SetLueAgingAP));
        }

        private void LueCurCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
                TxtCtReplace.EditValue = null;
                Sm.ClearGrd(Grd2, true);
            }
        }

        private void TxtSalesTarget_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtSalesTarget, 0);
        }

        private void TxtCreditLimit_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtCreditLimit, 0);
        }

        private void TxtMinDelivery_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtMinDelivery, 0);
        }

        private void TxtGoodsReturnDay_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtGoodsReturnDay, 0);
        }

        private void LueShpMCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueUomCode_Leave(object sender, EventArgs e)
        {
            if (LueUomCode.Visible && fAccept && fCell.ColIndex == 15)
            {
                if (Sm.GetLue(LueUomCode).Length == 0)
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd2.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueUomCode);
            }
        }

        private void LueUomCode_Validated(object sender, EventArgs e)
        {
            LueUomCode.Visible = false;
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd2.Font = new Font(
                    Grd2.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }

        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LuePort_Validated(object sender, EventArgs e)
        {
            LuePort.Visible = false;
        }

        private void LuePort_Leave(object sender, EventArgs e)
        {
            if (LuePort.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LuePort).Length == 0)
                    Grd4.Cells[fCell.RowIndex, 0].Value =
                    Grd4.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LuePort);
                    Grd4.Cells[fCell.RowIndex, 1].Value = LuePort.GetColumnValue("Col2");
                }
                LuePort.Visible = false;
            }
        }

        private void LuePort_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePort, new Sm.RefreshLue1(SetLuePort));
        }

        private void LuePort_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        #endregion

        #region Button Event

        private void BtnCtReplace_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer") &&
                !Sm.IsLueEmpty(LueCurCode, "Currency"))
                Sm.FormShowDialog(new FrmCtQt4Dlg4(this, Sm.GetLue(LueCurCode)));
        }

        private void BtnCtReplace2_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    {
                        if (TxtCtReplace.EditValue != null && TxtCtReplace.Text.Length != 0)
                        {

                            var f = new FrmCtQt4(mMenuCode);
                            f.Tag = mMenuCode;
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = TxtCtReplace.Text;
                            f.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Report Class

        class CtQtHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyEmail { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CPName { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string SpName { get; set; }
            public string PtName { get; set; }
            public string QtStartDt { get; set; }
            public string DtName { get; set; }
            public string EndDt { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string PrintBy { get; set; }
            
        }

        class CtQtHdr2
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
        }

        class CtQtDtl
        {
            public string DocNo { get; set; }
            public string ItName { get; set; }
            public decimal UPrice { get; set; }
            public decimal Discount { get; set; }
            public decimal SubTotal { get; set; }
            public string CtItName { get; set; }
            public string IsCustomerItemNameMandatory { get; set; }
            public string CurName { get; set; }
        }


        #endregion

        private void LueCtQtType_Leave(object sender, EventArgs e)
        {
            if (LueCtQtType.Visible && fAccept && fCell.ColIndex == 18)
            {
                if (Sm.GetLue(LueCtQtType).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 17].Value =
                    Grd2.Cells[fCell.RowIndex, 18].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 17].Value = Sm.GetLue(LueCtQtType);
                    Grd2.Cells[fCell.RowIndex, 18].Value = LueCtQtType.GetColumnValue("Col2");
                }
                LueCtQtType.Visible = false;
            }
        }

        private void LueCtQtType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }
    }
}