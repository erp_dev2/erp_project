﻿#region 
/*
    04/05/2020 [VIN/YK] Tambah kolom COA# dan COA Desc
    24/02/2022 [WED/ALL] format tanggal dan jam di last update masih belum sesuai
    07/03/2022 [TKG/PHT] tambah site group
 */
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSiteFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmSite mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSiteFind(FrmSite FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, B.ProfitCenterName, C.AcDesc, ");
            if (mFrmParent.mIsSiteUseCity)
                SQL.AppendLine("D.CityName, ");
            else
                SQL.AppendLine("Null As CityName, ");
            if (mFrmParent.mIsSiteGroupEnabled)
                SQL.AppendLine("E.OptDesc As SiteGroupDesc ");
            else
                SQL.AppendLine("Null As SiteGroupDesc ");
            SQL.AppendLine("From TblSite A ");
            SQL.AppendLine("Left Join TblProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblCoa C On A.AcNo=C.AcNo ");
            if (mFrmParent.mIsSiteUseCity) SQL.AppendLine("Left Join TblCity D On A.CityCode=D.CityCode ");
            if (mFrmParent.mIsSiteGroupEnabled) SQL.AppendLine("Left Join TblOption E On A.SiteGroup=E.OptCode And E.OptCat='SiteGroup' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Site Code", 
                        "Site Name", 
                        "Active",
                        "Head Office",
                        "Address",

                        //6-10
                        "Profit Center",
                        "POS COA#",
                        "POS COA Description",
                        "City",
                        "Group",
                        
                        //11-15
                        "Remark",
                        "Created By",
                        "Created Date", 
                        "Created Time",
                        "Last Updated By", 

                        //16-17
                        "Last Updated Date", 
                        "Last Updated Time"
                    },
                    new int[]
                    {
                        //
                        50,
                        //1-5
                        150, 250, 80, 100, 200,
                        //6-10
                        200, 200, 250, 200, 200,  
                        //11-15
                        200, 100, 100, 100, 100, 
                        //16-17
                        100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 13, 16 });
            Sm.GrdFormatTime(Grd1, new int[] { 14, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16, 17 }, false);
            if (!mFrmParent.mIsSiteGroupEnabled) Sm.GrdColInvisible(Grd1, new int[] { 10 }, false);
            if (!mFrmParent.mIsSiteUseCity) Sm.GrdColInvisible(Grd1, new int[] { 9 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtSiteCode.Text, new string[] { "SiteCode", "SiteName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By A.SiteName;",
                    new string[]
                    {
                        //0
                        "SiteCode", 
                                
                        //1-5
                        "SiteName", "ActInd", "HOInd", "Address", "ProfitCenterName", 
                            
                        //6-10
                        "AcNo", "AcDesc", "CityName", "SiteGroupDesc", "Remark", 
                        
                        //11-14
                        "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 17, 14);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSiteCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Site");
        }

        #endregion

        #endregion
    }
}
