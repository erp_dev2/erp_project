﻿#region Update
/*
    15/06/2020 [WED/IMS] new apps
    01/07/2020 [WED/IMS] masih ada error saat refresh
    03/07/2020 [WED/IMS] miscellanous berdasarkan parameter MiscItemProjectBudgetControl2Formula
    21/07/2020 [WED/IMS] tambah kolom dan berdasarkan COA item category
    31/08/2020 [DITA/IMS] bug : Project Amt tidak muncul
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectBudgetControl2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mProjectBudgetControlGroupBy = string.Empty,
            mSOContractDocNo = string.Empty,
            mMiscItemProjectBudgetControl2Formula = string.Empty;

        internal bool mIsFilterBySite = false, mIsCustomerItemNameMandatory = false;

        #endregion

        #region Constructor

        public FrmRptProjectBudgetControl2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetLueProjectCode(ref LueProjectCode);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Item's Code",
                    "Item's Name",
                    "Resource"+Environment.NewLine+"COA",
                    "Resource"+Environment.NewLine+"PO",
                    "Finished Good's Code",
                    
                    //6-10
                    "Finished Good's Name",
                    "Implementation",
                    "PO",
                    "COA",
                    "Received From"+Environment.NewLine+"Vendor",

                    //11-14
                    "Received From"+Environment.NewLine+"Warehouse",
                    "Sales",
                    "Contribution Margin",
                    "Deviation"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 250, 100, 100, 130, 
                    
                    //6-10
                    250, 150, 150, 150, 150, 
                    
                    //11-12
                    150, 150, 150, 150
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (Sm.IsLueEmpty(LueProjectCode, "Project")) return;

            Cursor.Current = Cursors.WaitCursor;

            var l1 = new List<Result>();
            var l2 = new List<String>();
            try
            {
                mSOContractDocNo = string.Empty;
                
                if (TxtSOContractDocNo.Text.Length > 0) mSOContractDocNo = TxtSOContractDocNo.Text;
                else mSOContractDocNo = GetSOContractDocNo();

                Process1(ref l1);
                Process2(ref l1);
                if (l1.Count > 0)
                {
                    Process3(ref l1);
                    Process5(ref l1);
                    Process6(ref l1);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l1.Clear();
                l2.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private string GetSOContractDocNo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT GROUP_CONCAT(DISTINCT D.DocNo) SOContractDocNo ");
            SQL.AppendLine("FROM TblProjectGroup A ");
            SQL.AppendLine("INNER JOIN TblLOPHdr B ON A.PGCode = B.PGCode ");
            SQL.AppendLine("    AND A.ProjectCode = @Param ");
            SQL.AppendLine("INNER JOIN TblBOQHdr C ON C.LOPDocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractHdr D ON C.DocNo = D.BOQDocNo ");
            SQL.AppendLine("GROUP BY A.ProjectCode; ");

            return Sm.GetValue(SQL.ToString(), Sm.GetLue(LueProjectCode));
        }

        private void Process1(ref List<Result> l)
        {
            var SQL = new StringBuilder();

            #region Old Code

            //SQL.AppendLine("SELECT X2.ItGrpCode, X3.ItGrpName, Group_Concat(Distinct X1.ItCode) ItCode, X1.FGItCode, X4.ItName FGItName, ");
            //SQL.AppendLine("SUM(X1.ProjectAmt) Amt1, SUM(X1.POAmt) Amt2, SUM(X1.RecvVdAmt) Amt3, 0.00 Amt4 ");
            //SQL.AppendLine("FROM ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    SELECT A.ProjectCode, A.ItCode, A.ItGrpCode, A.FGItCode, A.ProjectAmt, IFNULL(B.POAmt, 0.00) POAmt, IFNULL(B.RecvVdAmt, 0.00) RecvVdAmt ");
            //SQL.AppendLine("    FROM ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        SELECT T7.ProjectCode, T2.ResourceItCode ItCode, T41.ItGrpCode, T2.FGItCode, T2.Amt ProjectAmt ");
            //SQL.AppendLine("        FROM TblProjectImplementationHdr T1 ");
            //SQL.AppendLine("        INNER JOIN TblProjectImplementationDtl2 T2 ON T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("            AND T1.CancelInd = 'N' ");
            //SQL.AppendLine("            AND T1.Status IN ('O', 'A') ");
            //SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr T3 ON T1.SOContractDocNo = T3.DocNo ");
            //SQL.AppendLine("            And Find_In_set(T3.SOCDocNo, @SOContractDocNo) ");
            //SQL.AppendLine("        INNER JOIN TblSOContractHdr T4 ON T3.SOCDocNo = T4.DocNo ");
            //SQL.AppendLine("        Inner join "); 
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select ItCode, ItGrpCode ");
            //SQL.AppendLine("            From TblItem ");
            //SQL.AppendLine("            Where ItCode In ");
            //SQL.AppendLine("            ( ");
            //SQL.AppendLine("                Select Distinct X3.ResourceItCode ");
            //SQL.AppendLine("                From TblProjectImplementationHdr X1 ");
            //SQL.AppendLine("                Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
            //SQL.AppendLine("                    And Find_In_Set(X2.SOCDocNo, @SOContractDocNo) ");
            //SQL.AppendLine("                Inner Join TblProjectImplementationDtl2 X3 On X1.DocNo = X3.DocNo "); 
            //SQL.AppendLine("                    And X1.CancelInd = 'N' ");
            //SQL.AppendLine("                    And X1.Status In ('O', 'A') ");
            //SQL.AppendLine("            ) ");
            //SQL.AppendLine("        ) T41 On T2.ResourceItCode = T41.ItCode ");
            //SQL.AppendLine("        INNER JOIN TblBOQHdr T5 ON T4.BOQDocNo = T5.DocNo ");
            //SQL.AppendLine("        INNER JOIN TblLOPHdr T6 ON T5.LOPDocNo = T6.DocNo ");
            //SQL.AppendLine("        INNER JOIN TblProjectGroup T7 ON T6.PGCode = T7.PGCode AND T7.ProjectCode = @ProjectCode ");
            //SQL.AppendLine("    ) A ");
            //SQL.AppendLine("    LEFT JOIN ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        SELECT T10.ProjectCode, T2.ItCode, T21.ItGrpCode, T3.ItCode FGItCode, (T5.Qty * T6.UPrice) POAmt, (T11.Qty * T6.UPrice) RecvVdAmt ");
            //SQL.AppendLine("        FROM TblMaterialRequestHdr T1 ");
            //SQL.AppendLine("        INNER JOIN TblMaterialRequestDtl T2 ON T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("            AND T1.SOCDocNo IS NOT NULL ");
            //SQL.AppendLine("            And Find_In_set(T1.SOCDocNo, @SOContractDocNo) ");
            //SQL.AppendLine("            AND T2.CancelInd = 'N' ");
            //SQL.AppendLine("            AND T2.Status IN ('O', 'A') ");
            //SQL.AppendLine("            AND T2.BOM2DocNo IS NOT NULL ");
            //SQL.AppendLine("        INner Join ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select ItCode, ItGrpCode ");
            //SQL.AppendLine("            From TblItem ");
            //SQL.AppendLine("            Where ItCode In ");
            //SQL.AppendLine("            ( ");
            //SQL.AppendLine("                Select Distinct X1.ItCode ");
            //SQL.AppendLine("                From TblMaterialRequestDtl X1 ");
            //SQL.AppendLine("                Inner Join TblMaterialRequestHdr X2 On X1.DocNo = X2.DocNo ");
            //SQL.AppendLine("                    And Find_In_Set(X2.SOCDocNo, @SOContractDocNo) ");
            //SQL.AppendLine("                    And X1.CancelInd = 'N' ");
            //SQL.AppendLine("                    And X1.Status In ('O', 'A') ");
            //SQL.AppendLine("                    And X1.BOM2DocNo Is Not null ");
            //SQL.AppendLine("            ) ");
            //SQL.AppendLine("        ) T21 On T2.ItCode = T21.ItCode ");
            //SQL.AppendLine("        INNER JOIN TblBOM2Hdr T3 ON T2.BOM2DocNo = T3.DocNo ");
            //SQL.AppendLine("            AND T3.CancelInd = 'N' ");
            //SQL.AppendLine("            AND T3.Status IN ('O', 'A') ");
            //SQL.AppendLine("        INNER JOIN TblPORequestDtl T4 ON T4.MaterialRequestDocNo = T2.DocNo AND T4.MaterialRequestDNo = T2.DNo ");
            //SQL.AppendLine("        INNER JOIN TblPODtl T5 ON T5.PORequestDocNo = T4.DocNo AND T5.PORequestDNo = T4.DNo ");
            //SQL.AppendLine("            AND T5.CancelInd = 'N' ");
            //SQL.AppendLine("        INNER JOIN TblQtDtl T6 ON T4.QtDocNo = T6.DocNo AND T4.QtDNo = T6.DNo ");
            //SQL.AppendLine("        INNER JOIN TblSOContractHdr T7 ON T1.SOCDocNo = T7.DocNo ");
            //SQL.AppendLine("        INNER JOIN TblBOQHdr T8 ON T7.BOQDocNo = T8.DocNo ");
            //SQL.AppendLine("        INNER JOIN TblLOPHdr T9 ON T8.LOPDocNo = T9.DocNo ");
            //SQL.AppendLine("        INNER JOIN TblProjectGroup T10 ON T9.PGCode = T10.PGCode AND T10.ProjectCode = @ProjectCode ");
            //SQL.AppendLine("        LEFT JOIN TblRecvVdDtl T11 ON T11.PODocNo = T5.DocNo AND T11.PODNo = T5.DNo ");
            //SQL.AppendLine("    ) B ON ");
            //if (mMiscItemProjectBudgetControl2Formula == "1")
            //    SQL.AppendLine("    A.ItCode = B.ItCode AND A.FGItCode = B.FGItCode AND A.ProjectCode = B.ProjectCode ");
            //else
            //    SQL.AppendLine("    A.ItGrpCode = B.ItGrpCode AND A.FGItCode = B.FGItCode AND A.ProjectCode = B.ProjectCode ");
            //SQL.AppendLine(") X1 ");
            //SQL.AppendLine("INNER JOIN TblItem X2 ON X1.ItCode = X2.ItCode ");
            //SQL.AppendLine("LEFT JOIN TblItemGroup X3 ON X2.ItGrpCode = X3.ItGrpCode ");
            //SQL.AppendLine("LEFT JOIN TblItem X4 ON X1.FGItCode = X4.ItCode ");
            //SQL.AppendLine("GROUP BY X2.ItGrpCode, X3.ItGrpName, X1.FGItCode, X4.ItName; ");

            #endregion

            SQL.AppendLine("SELECT X21.ItCtCode, X21.AcNo2, Group_Concat(Distinct X1.ItCode) ItCode, X1.FGItCode, X4.ItName FGItName, ");
            SQL.AppendLine("SUM(X1.ProjectAmt) ProjectAmt, SUM(X1.POAmt) POAmt, SUM(X1.RecvVdAmt) RecvVdAmt, SUM(X1.RecvWhsAmt) RecvWhsAmt, Sum(X5.Amt) SalesAmt, ");
            SQL.AppendLine("X1.ResourcePO, If(X1.ResourcePO = 'Y', 'N', 'Y') ResourceCOA ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.ProjectCode, A.ItCode, A.ItGrpCode, A.ItCtCode, A.FGItCode, A.ProjectAmt, If(B.ProjectCode Is Null, 'N', 'Y') ResourcePO, ");
            SQL.AppendLine("    IFNULL(B.POAmt, 0.00) POAmt, IFNULL(B.RecvVdAmt, 0.00) RecvVdAmt, IfNull(B.RecvWhsAmt, 0.00) RecvWhsAmt ");
            SQL.AppendLine("    FROM ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T7.ProjectCode, T2.ResourceItCode ItCode, T41.ItGrpCode, T41.ItCtCode, T2.FGItCode, T2.Amt ProjectAmt ");
            SQL.AppendLine("        FROM TblProjectImplementationHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblProjectImplementationDtl2 T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND T1.Status IN ('O', 'A') ");
            SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr T3 ON T1.SOContractDocNo = T3.DocNo ");
            SQL.AppendLine("            And Find_In_set(T3.SOCDocNo, @SOContractDocNo) ");
            SQL.AppendLine("        INNER JOIN TblSOContractHdr T4 ON T3.SOCDocNo = T4.DocNo ");
            SQL.AppendLine("        Inner join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select ItCode, ItGrpCode, ItCtCode ");
            SQL.AppendLine("            From TblItem ");
            SQL.AppendLine("            Where ItCode In ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                Select Distinct X3.ResourceItCode ");
            SQL.AppendLine("                From TblProjectImplementationHdr X1 ");
            SQL.AppendLine("                Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
            SQL.AppendLine("                    And Find_In_Set(X2.SOCDocNo, @SOContractDocNo) ");
            SQL.AppendLine("                Inner Join TblProjectImplementationDtl2 X3 On X1.DocNo = X3.DocNo ");
            SQL.AppendLine("                    And X1.CancelInd = 'N' ");
            SQL.AppendLine("                    And X1.Status In ('O', 'A') ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        ) T41 On T2.ResourceItCode = T41.ItCode ");
            SQL.AppendLine("        INNER JOIN TblBOQHdr T5 ON T4.BOQDocNo = T5.DocNo ");
            SQL.AppendLine("        INNER JOIN TblLOPHdr T6 ON T5.LOPDocNo = T6.DocNo ");
            SQL.AppendLine("        INNER JOIN TblProjectGroup T7 ON T6.PGCode = T7.PGCode AND T7.ProjectCode = @ProjectCode ");
            SQL.AppendLine("    ) A ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T10.ProjectCode, T2.ItCode, T21.ItGrpCode, T21.ItCtCode, T3.ItCode FGItCode, ");
            SQL.AppendLine("        (T5.Qty * T6.UPrice) POAmt, (T11.Qty * T6.UPrice) RecvVdAmt, (T12.Qty * T6.UPrice) RecvWhsAmt ");
            SQL.AppendLine("        FROM TblMaterialRequestHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblMaterialRequestDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            AND T1.SOCDocNo IS NOT NULL ");
            SQL.AppendLine("            And Find_In_set(T1.SOCDocNo, @SOContractDocNo) ");
            SQL.AppendLine("            AND T2.CancelInd = 'N' ");
            SQL.AppendLine("            AND T2.Status IN ('O', 'A') ");
            SQL.AppendLine("            AND T2.BOM2DocNo IS NOT NULL ");
            SQL.AppendLine("        INner Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select ItCode, ItGrpCode, ItCtCode ");
            SQL.AppendLine("            From TblItem ");
            SQL.AppendLine("            Where ItCode In ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                Select Distinct X1.ItCode ");
            SQL.AppendLine("                From TblMaterialRequestDtl X1 ");
            SQL.AppendLine("                Inner Join TblMaterialRequestHdr X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("                    And Find_In_Set(X2.SOCDocNo, @SOContractDocNo) ");
            SQL.AppendLine("                    And X1.CancelInd = 'N' ");
            SQL.AppendLine("                    And X1.Status In ('O', 'A') ");
            SQL.AppendLine("                    And X1.BOM2DocNo Is Not null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        ) T21 On T2.ItCode = T21.ItCode ");
            SQL.AppendLine("        INNER JOIN TblBOM2Hdr T3 ON T2.BOM2DocNo = T3.DocNo ");
            SQL.AppendLine("            AND T3.CancelInd = 'N' ");
            SQL.AppendLine("            AND T3.Status IN ('O', 'A') ");
            SQL.AppendLine("        INNER JOIN TblPORequestDtl T4 ON T4.MaterialRequestDocNo = T2.DocNo AND T4.MaterialRequestDNo = T2.DNo ");
            SQL.AppendLine("        INNER JOIN TblPODtl T5 ON T5.PORequestDocNo = T4.DocNo AND T5.PORequestDNo = T4.DNo ");
            SQL.AppendLine("            AND T5.CancelInd = 'N' ");
            SQL.AppendLine("        INNER JOIN TblQtDtl T6 ON T4.QtDocNo = T6.DocNo AND T4.QtDNo = T6.DNo ");
            SQL.AppendLine("        INNER JOIN TblSOContractHdr T7 ON T1.SOCDocNo = T7.DocNo ");
            SQL.AppendLine("        INNER JOIN TblBOQHdr T8 ON T7.BOQDocNo = T8.DocNo ");
            SQL.AppendLine("        INNER JOIN TblLOPHdr T9 ON T8.LOPDocNo = T9.DocNo ");
            SQL.AppendLine("        INNER JOIN TblProjectGroup T10 ON T9.PGCode = T10.PGCode AND T10.ProjectCode = @ProjectCode ");
            SQL.AppendLine("        LEFT JOIN TblRecvVdDtl T11 ON T11.PODocNo = T5.DocNo AND T11.PODNo = T5.DNo And T11.CancelInd = 'N' And T11.Status In ('O', 'A') ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select A.Qty, B.Source, C.DocNo RecvVdDocNo, C.DNo RecvVdDNo ");
            SQL.AppendLine("            From TblRecvWhsDtl A ");
            SQL.AppendLine("            Inner Join TblDOWhsDtl B On A.DOWhsDocNo = B.DocNo And A.DOWhsDNo = B.DNo ");
            SQL.AppendLine("                And A.CancelInd = 'N' ");
            SQL.AppendLine("            Inner Join TblRecvVdDtl C On B.Source = C.Source And C.CancelInd = 'N' And C.Status In ('O', 'A') ");
            SQL.AppendLine("        ) T12 On T11.Source = T12.Source And T11.DocNo = T12.RecvVdDocNo And T11.DNo = T12.RecvVdDNo ");
            SQL.AppendLine("    ) B ON ");
            SQL.AppendLine("    A.ItCtCode = B.ItCtCode AND A.FGItCode = B.FGItCode AND A.ProjectCode = B.ProjectCode ");
            SQL.AppendLine(") X1 ");
            SQL.AppendLine("INNER JOIN TblItem X2 ON X1.ItCode = X2.ItCode ");
            SQL.AppendLine("Inner join TblItemCategory X21 On X2.ItCtCode = X21.ItCtCode ");
            SQL.AppendLine("Inner JOIN TblItem X4 ON X1.FGItCode = X4.ItCode ");
            SQL.AppendLine("Inner Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select ItCode, Sum(Amt) Amt ");
            SQL.AppendLine("    From TblSOContractDtl ");
            SQL.AppendLine("    Where Find_In_Set(DocNo, @SOContractDocNo) ");
            SQL.AppendLine("    Group By ItCode ");
            SQL.AppendLine(") X5 On X5.ItCode = X1.FGItCode ");
            SQL.AppendLine("GROUP BY X2.ItCtCode, X21.AcNo2, X1.FGItCode, X4.ItName, X1.ResourcePO; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ProjectCode", Sm.GetLue(LueProjectCode));
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", mSOContractDocNo);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCtCode", 
                    
                    //1-5
                    "AcNo2", 
                    "FGItCode",
                    "FGItName",
                    "ProjectAmt", 
                    "POAmt",

                    //6-10
                    "RecvVdAmt", 
                    "RecvWhsAmt",
                    "ItCode",
                    "SalesAmt",
                    "ResourcePO",

                    //11
                    "ResourceCOA"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            ItCtCode = Sm.DrStr(dr, c[0]),
                            AcNo2 = Sm.DrStr(dr, c[1]),
                            FGItCode = Sm.DrStr(dr, c[2]),
                            FGItName = Sm.DrStr(dr, c[3]),
                            ProjectAmt = Sm.DrDec(dr, c[4]),
                            POAmt = Sm.DrDec(dr, c[5]),
                            COAAmt = 0m,
                            RecvVdAmt = Sm.DrDec(dr, c[6]),
                            RecvWhsAmt = Sm.DrDec(dr, c[7]),
                            MiscInd = false,
                            OrderByMiscInd = false,
                            ItCode = Sm.DrStr(dr, c[8]),
                            SalesAmt = Sm.DrDec(dr, c[9]),
                            ResourcePO = Sm.DrStr(dr, c[10]) == "Y",
                            ResourceCOA = Sm.DrStr(dr, c[11]) == "Y",
                            ContributionMarginAmt = 0m,
                            DeviationAmt = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result> l)
        {
            var SQL = new StringBuilder();
            string ItCode = string.Empty;

            foreach (var x in l)
            {
                if (ItCode.Length > 0) ItCode += ",";

                if (mMiscItemProjectBudgetControl2Formula == "1")
                    ItCode += x.ItCode;
                else
                    ItCode += x.AcNo2;
            }

            SQL.AppendLine("SELECT T1.ItCode, T1.ItName, T1.FGItCode, T2.ItName FGItName, SUM(T1.POAmt) POAmt, SUM(T1.RecvVdAmt) RecvVdAmt, Sum(T1.RecvWhsAmt) RecvWhsAmt, SUm(T1.SalesAmt) SalesAmt ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT 'MISC' AS ItCode, 'Miscellaneous Items' AS ItName, T3.ItCode FGItCode, ");
            SQL.AppendLine("    (T6.Qty * T5.UPrice) POAmt, (IFNULL(T7.RecvVdQty, 0.00) * T5.UPrice) RecvVdAmt, ");
            SQL.AppendLine("    (IfNull(T7.RecvWhsQty, 0.00) * T5.UPrice) RecvWhsAmt, T8.Amt SalesAmt ");
            SQL.AppendLine("    FROM TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    INNER JOIN TblMaterialRequestDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        AND T1.SOCDocNo IS NOT NULL ");
            SQL.AppendLine("        AND FIND_IN_SET(T1.SOCDocNo, @SOContractDocNo) ");
            SQL.AppendLine("        AND T2.CancelInd = 'N' ");
            SQL.AppendLine("        AND T2.Status IN ('O', 'A') ");
            SQL.AppendLine("        AND T2.BOM2DocNo IS NOT NULL ");
            if (mMiscItemProjectBudgetControl2Formula == "1")
                SQL.AppendLine("        AND NOT FIND_IN_SET(T2.ItCode, @ItCode) ");
            else
            {
                SQL.AppendLine("    Inner join TblItem T21 On T2.ItCode = T21.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory T22 On T21.ItCtCode = T22.ItCtCode ");
                SQL.AppendLine("        And Not Find_In_Set(T22.AcNo2, @ItCode) "); 
            }
            SQL.AppendLine("    INNER JOIN TblBOM2Hdr T3 ON T2.BOM2DocNo = T3.DocNo ");
            SQL.AppendLine("    INNER JOIN TblPORequestDtl T4 ON T4.MaterialRequestDocNo = T2.DocNo AND T4.MaterialRequestDNo = T2.DNo ");
            SQL.AppendLine("    INNER JOIN TblQtDtl T5 ON T4.QtDocNo = T5.DocNo AND T4.QtDNo = T5.DNo ");
            SQL.AppendLine("    INNER JOIN TblPODtl T6 ON T6.PORequestDocNo = T4.DocNo AND T6.PORequestDNo = T4.DNo ");
            SQL.AppendLine("        AND T6.CancelInd = 'N' ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X3.MaterialRequestDocNo, X3.MaterialRequestDNo, SUM(X1.Qty) RecvVdQty, Sum(IfNull(X7.Qty, 0.00)) RecvWhsQty ");
            SQL.AppendLine("        FROM TblRecvVdDtl X1 ");
            SQL.AppendLine("        INNER JOIN TblPODtl X2 ON X1.PODocNo = X2.DocNo AND X1.PODNo = X2.DNo ");
            SQL.AppendLine("            AND X1.CancelInd = 'N' ");
            SQL.AppendLine("            AND X1.Status IN ('O', 'A') ");
            SQL.AppendLine("        INNER JOIN TblPORequestDtl X3 ON X2.PORequestDocNo = X3.DocNo AND X2.PORequestDNo = X3.DNo ");
            SQL.AppendLine("        INNER JOIN TblQtDtl X4 ON X3.QtDocNo = X4.DocNo AND X3.QtDNo = X4.DNo ");
            SQL.AppendLine("        INNER JOIN TblMaterialRequestDtl X5 ON X3.MaterialRequestDocNo = X5.DocNo AND X3.MaterialRequestDNo = X5.DNo ");
            SQL.AppendLine("            AND X5.BOM2DocNo IS NOT NULL ");
            if (mMiscItemProjectBudgetControl2Formula == "1")
                SQL.AppendLine("            AND NOT FIND_IN_SET(X5.ItCode, @ItCode) ");
            else
            {
                SQL.AppendLine("        Inner join TblItem X51 On X5.ItCode = X51.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory X52 ON X51.ItCtCode = X52.ItCtCode ");
                SQL.AppendLine("            And Not Find_In_Set(X52.AcNo2, @ItCode) "); 
            }
            SQL.AppendLine("        INNER JOIN TblMaterialRequestHdr X6 ON X5.DocNo = X6.DocNo ");
            SQL.AppendLine("            AND X6.SOCDocNo IS NOT NULL ");
            SQL.AppendLine("            AND FIND_IN_SET(X6.SOCDocNo, @SOContractDocNo) ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select C.DocNo RecvVdDocNo, C.DNo RecvVdDNo, A.Qty ");
            SQL.AppendLine("            From TblRecvWhsDtl A ");
            SQL.AppendLine("            Inner Join TblDOWhsDtl B On A.DOWhsDocNo = B.DOcNo And A.DOWhsDNo = B.DNo ");
            SQL.AppendLine("                And A.CancelInd = 'N' And B.CancelInd = 'N' ");
            SQL.AppendLine("            Inner Join TblRecvVdDtl C On B.Source = C.Source And C.CancelInd = 'N' And C.Status In ('O', 'A') ");
            SQL.AppendLine("        ) X7 On X1.DocNo = X7.RecvVdDocNo And X1.DNo = X7.RecvVdDNo ");
            SQL.AppendLine("        GROUP BY X3.MaterialRequestDocNo, X3.MaterialRequestDNo ");
            SQL.AppendLine("    ) T7 ON T2.DocNo = T7.MaterialRequestDocNo AND T2.DNo = T7.MaterialRequestDNo ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo, A.ItCode, Sum(A.Amt) Amt ");
            SQL.AppendLine("        From TblSOContractDtl A ");
            SQL.AppendLine("        Inner Join TblSOContractHdr B On A.DocNo = B.DocNo And B.CancelInd = 'N' And B.Status In ('O', 'A') ");
            SQL.AppendLine("        Where Find_In_Set(A.DocNo, @SOContractDocNo) ");  
            SQL.AppendLine("        Group By A.DocNo, A.ItCode ");
            SQL.AppendLine("    ) T8 On T1.SOCDocNo = T8.DocNo And T2.ItCode = T8.ItCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("INNER JOIN TblItem T2 ON T1.FGItCode = T2.ItCode ");
            SQL.AppendLine("GROUP BY T1.FGItCode, T2.ItName; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", mSOContractDocNo);
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode.Length > 0 ? ItCode : "XXX");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 
                    
                    //1-5
                    "ItName", 
                    "FGItCode",
                    "FGItName",
                    "POAmt",
                    "RecvVdAmt",

                    //6-7
                    "RecvWhsAmt",
                    "SalesAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            ItCtCode = Sm.DrStr(dr, c[0]),
                            AcNo2 = Sm.DrStr(dr, c[1]),
                            FGItCode = Sm.DrStr(dr, c[2]),
                            FGItName = Sm.DrStr(dr, c[3]),
                            ProjectAmt = 0m,
                            POAmt = Sm.DrDec(dr, c[4]),
                            RecvVdAmt = Sm.DrDec(dr, c[5]),
                            RecvWhsAmt = Sm.DrDec(dr, c[6]),
                            ResourcePO = true,
                            ResourceCOA = false,
                            DeviationAmt = 0m,
                            SalesAmt = Sm.DrDec(dr, c[7]),
                            ContributionMarginAmt = 0m,
                            COAAmt = 0m,
                            MiscInd = true,
                            OrderByMiscInd = false
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Result> l)
        {
            string mAcNo = string.Empty;

            foreach (var x in l.Where(w => w.AcNo2.Length > 0))
            {
                if (mAcNo.Length > 0) mAcNo += ",";
                mAcNo += x.AcNo2;
            }

            if (mAcNo.Length > 0)
            {
                var lC = new List<COA>();
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select T.AcNo, SUm(T.Amt) Amt ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select A.AcNo, If(C.AcType = 'C', A.CAmt, (A.DAmt * -1)) Amt ");
                SQL.AppendLine("    From TblJournalDtl A ");
                SQL.AppendLine("    Inner Join TblJournalHdr B On A.DocNo = B.DocNo And B.CancelInd = 'N' ");
                SQL.AppendLine("        And Find_In_Set(A.AcNo, @AcNo) ");
                SQL.AppendLine("        And B.MenuCode = (Select IfNull(ParValue, '0102010107') From TblParameter Where ParCode = 'MenuCodeForJournalVoucher') ");
                SQL.AppendLine("    Inner Join TblCOA C On A.AcNo = C.AcNo ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Group By T.AcNo; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@AcNo", mAcNo);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Amt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lC.Add(new COA()
                            {
                                AcNo = Sm.DrStr(dr, c[0]),
                                Amt = Sm.DrDec(dr, c[1]),
                            });
                        }
                    }
                    dr.Close();
                }

                if (lC.Count > 0)
                {
                    foreach (var x in lC)
                    {
                        foreach (var y in l.Where(w => w.AcNo2.Length > 0 && w.AcNo2 == x.AcNo))
                        {
                            y.COAAmt = x.Amt;
                        }
                    }
                }

                lC.Clear();

                Process4(ref l, mAcNo);
            }
        }

        private void Process4(ref List<Result> l, string AcNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Concat(AcNo, ' - ', AcDesc) AcDesc ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where Find_In_Set(AcNo, @AcNo) ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in l.Where(w => w.AcNo2.Length > 0 && w.AcNo2 == Sm.DrStr(dr, c[0])))
                        {
                            x.AcNo2 = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process5(ref List<Result> l)
        {
            foreach(var x in l)
            {
                if (x.ResourceCOA) 
                {
                    x.ContributionMarginAmt = x.SalesAmt - x.COAAmt;
                    x.DeviationAmt = x.ProjectAmt - x.COAAmt;
                }
                else 
                {
                    x.ContributionMarginAmt = x.SalesAmt - x.POAmt;
                    x.DeviationAmt = x.ProjectAmt - x.POAmt;
                }
            }
        }

        private void Process6(ref List<Result> l)
        {
            iGRow r;
            int i = 0;
            Grd1.BeginUpdate();

            foreach (var x in l.Where(w => !w.MiscInd).OrderBy(o => o.OrderByMiscInd).ThenBy(t => t.ItGrpName).ThenBy(t => t.FGItName))
            {
                r = Grd1.Rows.Add();
                i++;
                r.Cells[0].Value = i;
                r.Cells[1].Value = x.ItCtCode;
                r.Cells[2].Value = x.AcNo2;
                r.Cells[3].Value = x.ResourceCOA;
                r.Cells[4].Value = x.ResourcePO;
                r.Cells[5].Value = x.FGItCode;
                r.Cells[6].Value = x.FGItName;
                r.Cells[7].Value = x.ProjectAmt;
                r.Cells[8].Value = x.POAmt;
                r.Cells[9].Value = x.COAAmt;
                r.Cells[10].Value = x.RecvVdAmt;
                r.Cells[11].Value = x.RecvWhsAmt;
                r.Cells[12].Value = x.SalesAmt;
                r.Cells[13].Value = x.ContributionMarginAmt;
                r.Cells[14].Value = x.DeviationAmt;
            }

            foreach (var x in l.Where(w => w.MiscInd == true))
            {
                r = Grd1.Rows.Add();
                i++;
                r.Cells[0].Value = i;
                r.Cells[1].Value = x.ItCtCode;
                r.Cells[2].Value = x.AcNo2;
                r.Cells[3].Value = x.ResourceCOA;
                r.Cells[4].Value = x.ResourcePO;
                r.Cells[5].Value = x.FGItCode;
                r.Cells[6].Value = x.FGItName;
                r.Cells[7].Value = x.ProjectAmt;
                r.Cells[8].Value = x.POAmt;
                r.Cells[9].Value = x.COAAmt;
                r.Cells[10].Value = x.RecvVdAmt;
                r.Cells[11].Value = x.RecvWhsAmt;
                r.Cells[12].Value = x.SalesAmt;
                r.Cells[13].Value = x.ContributionMarginAmt;
                r.Cells[14].Value = x.DeviationAmt;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14 });
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mProjectBudgetControlGroupBy = Sm.GetParameter("ProjectBudgetControlGroupBy");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsCustomerItemNameMandatory = Sm.GetParameterBoo("IsCustomerItemNameMandatory");
            mMiscItemProjectBudgetControl2Formula = Sm.GetParameter("MiscItemProjectBudgetControl2Formula");

            if (mProjectBudgetControlGroupBy.Length == 0) mProjectBudgetControlGroupBy = "1";
            if (mMiscItemProjectBudgetControl2Formula.Length == 0) mMiscItemProjectBudgetControl2Formula = "1";
        }

        private void SetLueProjectCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT IFNULL(A.ProjectCode, A.PGCode) Col1,  ");
            SQL.AppendLine("IFNULL(CONCAT(A.ProjectCode, ' [', A.ProjectName, ']'), CONCAT(A.PGCode, ' [', A.PGName, ']')) Col2 ");
            SQL.AppendLine("FROM TblProjectGroup A ");
            SQL.AppendLine("WHERE A.ActInd = 'Y' ");
            SQL.AppendLine("ORDER BY IfNull(A.ProjectCode, A.PGCode); ");

            Sm.SetLue2(
                ref LueProjectCode, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueProjectCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProjectCode, new Sm.RefreshLue1(SetLueProjectCode));
        }

        private void ChkSOContractDocNo_CheckedChanged(object sender, EventArgs e)
        {

        }

        #endregion

        #region Button Click

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRptProjectBudgetControl2Dlg(this));
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string ItCtCode { get; set; }
            public string AcNo2 { get; set; }
            public string ItGrpCode { get; set; }
            public string ItGrpName { get; set; }
            public string ItCode { get; set; }
            public string FGItCode { get; set; }
            public string FGItName { get; set; }
            public bool ResourcePO { get; set; }
            public bool ResourceCOA { get; set; }
            public decimal ProjectAmt { get; set; }
            public decimal POAmt { get; set; }
            public decimal RecvVdAmt { get; set; }
            public decimal RecvWhsAmt { get; set; }
            public decimal COAAmt { get; set; }
            public decimal DeviationAmt { get; set; }
            public decimal SalesAmt { get; set; }
            public decimal ContributionMarginAmt { get; set; }
            public bool MiscInd { get; set; }
            public bool OrderByMiscInd { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}
