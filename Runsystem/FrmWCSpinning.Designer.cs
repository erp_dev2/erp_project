﻿namespace RunSystem
{
    partial class FrmWCSpinning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWCSpinning));
            this.TxtWCName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtWCCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkUnitInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkPercentInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkProdDataPerItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.LueProdFormula = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtSeqNo = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtTotalMachine = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.BtnWCCode = new DevExpress.XtraEditors.SimpleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.LueFSNICode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueFSNICode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWCName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkUnitInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPercentInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdDataPerItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProdFormula.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeqNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalMachine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFSNICode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFSNICode2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueFSNICode2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueFSNICode1);
            this.panel2.Controls.Add(this.BtnWCCode);
            this.panel2.Controls.Add(this.TxtSeqNo);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.TxtTotalMachine);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.LueProdFormula);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.ChkProdDataPerItemInd);
            this.panel2.Controls.Add(this.ChkPercentInd);
            this.panel2.Controls.Add(this.ChkUnitInd);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.TxtWCName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtWCCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 278);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 278);
            this.panel3.Size = new System.Drawing.Size(772, 195);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Visible = false;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 195);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TxtWCName
            // 
            this.TxtWCName.EnterMoveNextControl = true;
            this.TxtWCName.Location = new System.Drawing.Point(131, 28);
            this.TxtWCName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWCName.Name = "TxtWCName";
            this.TxtWCName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWCName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWCName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWCName.Properties.Appearance.Options.UseFont = true;
            this.TxtWCName.Properties.MaxLength = 40;
            this.TxtWCName.Size = new System.Drawing.Size(400, 20);
            this.TxtWCName.TabIndex = 13;
            this.TxtWCName.Validated += new System.EventHandler(this.TxtWCName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(90, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWCCode
            // 
            this.TxtWCCode.EnterMoveNextControl = true;
            this.TxtWCCode.Location = new System.Drawing.Point(131, 6);
            this.TxtWCCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWCCode.Name = "TxtWCCode";
            this.TxtWCCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtWCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWCCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWCCode.Properties.Appearance.Options.UseFont = true;
            this.TxtWCCode.Properties.MaxLength = 30;
            this.TxtWCCode.Properties.ReadOnly = true;
            this.TxtWCCode.Size = new System.Drawing.Size(194, 20);
            this.TxtWCCode.TabIndex = 11;
            this.TxtWCCode.Validated += new System.EventHandler(this.TxtWCCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(93, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(132, 183);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(206, 22);
            this.ChkActInd.TabIndex = 26;
            // 
            // ChkUnitInd
            // 
            this.ChkUnitInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkUnitInd.Location = new System.Drawing.Point(132, 205);
            this.ChkUnitInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkUnitInd.Name = "ChkUnitInd";
            this.ChkUnitInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkUnitInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkUnitInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkUnitInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkUnitInd.Properties.Appearance.Options.UseFont = true;
            this.ChkUnitInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkUnitInd.Properties.Caption = "Use Unit In Production Data";
            this.ChkUnitInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkUnitInd.Size = new System.Drawing.Size(206, 22);
            this.ChkUnitInd.TabIndex = 27;
            // 
            // ChkPercentInd
            // 
            this.ChkPercentInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPercentInd.Location = new System.Drawing.Point(132, 227);
            this.ChkPercentInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPercentInd.Name = "ChkPercentInd";
            this.ChkPercentInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPercentInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPercentInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPercentInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPercentInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPercentInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPercentInd.Properties.Caption = "Use Percent In Production Data";
            this.ChkPercentInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPercentInd.Size = new System.Drawing.Size(206, 22);
            this.ChkPercentInd.TabIndex = 28;
            // 
            // ChkProdDataPerItemInd
            // 
            this.ChkProdDataPerItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkProdDataPerItemInd.Location = new System.Drawing.Point(132, 248);
            this.ChkProdDataPerItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkProdDataPerItemInd.Name = "ChkProdDataPerItemInd";
            this.ChkProdDataPerItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkProdDataPerItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProdDataPerItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkProdDataPerItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkProdDataPerItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkProdDataPerItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkProdDataPerItemInd.Properties.Caption = "Production Data Per Item";
            this.ChkProdDataPerItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProdDataPerItemInd.Size = new System.Drawing.Size(206, 22);
            this.ChkProdDataPerItemInd.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(79, 54);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 14);
            this.label13.TabIndex = 14;
            this.label13.Text = "Formula";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProdFormula
            // 
            this.LueProdFormula.EnterMoveNextControl = true;
            this.LueProdFormula.Location = new System.Drawing.Point(131, 50);
            this.LueProdFormula.Margin = new System.Windows.Forms.Padding(5);
            this.LueProdFormula.Name = "LueProdFormula";
            this.LueProdFormula.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProdFormula.Properties.Appearance.Options.UseFont = true;
            this.LueProdFormula.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProdFormula.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProdFormula.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProdFormula.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProdFormula.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProdFormula.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProdFormula.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProdFormula.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProdFormula.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProdFormula.Properties.DropDownRows = 30;
            this.LueProdFormula.Properties.MaxLength = 40;
            this.LueProdFormula.Properties.NullText = "[Empty]";
            this.LueProdFormula.Properties.PopupWidth = 400;
            this.LueProdFormula.Size = new System.Drawing.Size(400, 20);
            this.LueProdFormula.TabIndex = 15;
            this.LueProdFormula.ToolTip = "F4 : Show/hide list";
            this.LueProdFormula.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProdFormula.EditValueChanged += new System.EventHandler(this.LueProdFormula_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(131, 160);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(400, 20);
            this.MeeRemark.TabIndex = 25;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(81, 162);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 24;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeqNo
            // 
            this.TxtSeqNo.EnterMoveNextControl = true;
            this.TxtSeqNo.Location = new System.Drawing.Point(131, 138);
            this.TxtSeqNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeqNo.Name = "TxtSeqNo";
            this.TxtSeqNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeqNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeqNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeqNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSeqNo.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeqNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeqNo.Size = new System.Drawing.Size(88, 20);
            this.TxtSeqNo.TabIndex = 23;
            this.TxtSeqNo.Validated += new System.EventHandler(this.TxtSeqNo_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(16, 141);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(112, 14);
            this.label17.TabIndex = 22;
            this.label17.Text = "Report Sequence#";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalMachine
            // 
            this.TxtTotalMachine.EnterMoveNextControl = true;
            this.TxtTotalMachine.Location = new System.Drawing.Point(131, 116);
            this.TxtTotalMachine.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalMachine.Name = "TxtTotalMachine";
            this.TxtTotalMachine.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalMachine.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalMachine.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalMachine.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalMachine.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalMachine.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalMachine.Size = new System.Drawing.Size(88, 20);
            this.TxtTotalMachine.TabIndex = 21;
            this.TxtTotalMachine.Validated += new System.EventHandler(this.TxtTotalMachine_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(45, 119);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 14);
            this.label16.TabIndex = 20;
            this.label16.Text = "Total Machine";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnWCCode
            // 
            this.BtnWCCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnWCCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnWCCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWCCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnWCCode.Appearance.Options.UseBackColor = true;
            this.BtnWCCode.Appearance.Options.UseFont = true;
            this.BtnWCCode.Appearance.Options.UseForeColor = true;
            this.BtnWCCode.Appearance.Options.UseTextOptions = true;
            this.BtnWCCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnWCCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnWCCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnWCCode.Image")));
            this.BtnWCCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnWCCode.Location = new System.Drawing.Point(329, 5);
            this.BtnWCCode.Name = "BtnWCCode";
            this.BtnWCCode.Size = new System.Drawing.Size(24, 21);
            this.BtnWCCode.TabIndex = 12;
            this.BtnWCCode.ToolTip = "Find Item Code";
            this.BtnWCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnWCCode.ToolTipTitle = "Run System";
            this.BtnWCCode.Click += new System.EventHandler(this.BtnWCCode_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(15, 76);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "Non Item Formula I";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFSNICode1
            // 
            this.LueFSNICode1.EnterMoveNextControl = true;
            this.LueFSNICode1.Location = new System.Drawing.Point(131, 72);
            this.LueFSNICode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueFSNICode1.Name = "LueFSNICode1";
            this.LueFSNICode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFSNICode1.Properties.Appearance.Options.UseFont = true;
            this.LueFSNICode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFSNICode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFSNICode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFSNICode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFSNICode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFSNICode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFSNICode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFSNICode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFSNICode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFSNICode1.Properties.DropDownRows = 30;
            this.LueFSNICode1.Properties.MaxLength = 40;
            this.LueFSNICode1.Properties.NullText = "[Empty]";
            this.LueFSNICode1.Properties.PopupWidth = 400;
            this.LueFSNICode1.Size = new System.Drawing.Size(400, 20);
            this.LueFSNICode1.TabIndex = 17;
            this.LueFSNICode1.ToolTip = "F4 : Show/hide list";
            this.LueFSNICode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFSNICode1.EditValueChanged += new System.EventHandler(this.LueFSNICode1_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(11, 98);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 14);
            this.label4.TabIndex = 18;
            this.label4.Text = "Non Item Formula II";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFSNICode2
            // 
            this.LueFSNICode2.EnterMoveNextControl = true;
            this.LueFSNICode2.Location = new System.Drawing.Point(131, 94);
            this.LueFSNICode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueFSNICode2.Name = "LueFSNICode2";
            this.LueFSNICode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFSNICode2.Properties.Appearance.Options.UseFont = true;
            this.LueFSNICode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFSNICode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFSNICode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFSNICode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFSNICode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFSNICode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFSNICode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFSNICode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFSNICode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFSNICode2.Properties.DropDownRows = 30;
            this.LueFSNICode2.Properties.MaxLength = 40;
            this.LueFSNICode2.Properties.NullText = "[Empty]";
            this.LueFSNICode2.Properties.PopupWidth = 400;
            this.LueFSNICode2.Size = new System.Drawing.Size(400, 20);
            this.LueFSNICode2.TabIndex = 19;
            this.LueFSNICode2.ToolTip = "F4 : Show/hide list";
            this.LueFSNICode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFSNICode2.EditValueChanged += new System.EventHandler(this.LueFSNICode2_EditValueChanged);
            // 
            // FrmWCSpinning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmWCSpinning";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWCName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkUnitInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPercentInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdDataPerItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProdFormula.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeqNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalMachine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFSNICode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFSNICode2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtWCCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkProdDataPerItemInd;
        private DevExpress.XtraEditors.CheckEdit ChkPercentInd;
        private DevExpress.XtraEditors.CheckEdit ChkUnitInd;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.LookUpEdit LueProdFormula;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtSeqNo;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtTotalMachine;
        private System.Windows.Forms.Label label16;
        public DevExpress.XtraEditors.SimpleButton BtnWCCode;
        internal DevExpress.XtraEditors.TextEdit TxtWCName;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.LookUpEdit LueFSNICode2;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueFSNICode1;
    }
}