﻿namespace RunSystem
{
    partial class FrmEmailVendorSector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnTenderDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtTenderDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteExpiredDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.MeeTenderName = new DevExpress.XtraEditors.MemoExEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.LueSector = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTenderDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeTenderName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSector.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // LueFontSize
            // 
            this.LueFontSize.EditValue = "9";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.DteExpiredDt);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.MeeTenderName);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.BtnTenderDocNo);
            this.panel2.Controls.Add(this.TxtTenderDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 96);
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 96);
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 377);
            this.Grd1.TabIndex = 14;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnTenderDocNo
            // 
            this.BtnTenderDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnTenderDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnTenderDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTenderDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnTenderDocNo.Appearance.Options.UseBackColor = true;
            this.BtnTenderDocNo.Appearance.Options.UseFont = true;
            this.BtnTenderDocNo.Appearance.Options.UseForeColor = true;
            this.BtnTenderDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnTenderDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnTenderDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnTenderDocNo.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnTenderDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnTenderDocNo.Location = new System.Drawing.Point(262, 4);
            this.BtnTenderDocNo.Name = "BtnTenderDocNo";
            this.BtnTenderDocNo.Size = new System.Drawing.Size(20, 20);
            this.BtnTenderDocNo.TabIndex = 7;
            this.BtnTenderDocNo.ToolTip = "Find Voucher Request";
            this.BtnTenderDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnTenderDocNo.ToolTipTitle = "Run System";
            this.BtnTenderDocNo.Click += new System.EventHandler(this.BtnTenderDocNo_Click);
            // 
            // TxtTenderDocNo
            // 
            this.TxtTenderDocNo.EnterMoveNextControl = true;
            this.TxtTenderDocNo.Location = new System.Drawing.Point(93, 5);
            this.TxtTenderDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTenderDocNo.Name = "TxtTenderDocNo";
            this.TxtTenderDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTenderDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTenderDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTenderDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtTenderDocNo.Properties.MaxLength = 30;
            this.TxtTenderDocNo.Properties.ReadOnly = true;
            this.TxtTenderDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtTenderDocNo.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(32, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "Tender#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteExpiredDt
            // 
            this.DteExpiredDt.EditValue = null;
            this.DteExpiredDt.EnterMoveNextControl = true;
            this.DteExpiredDt.Location = new System.Drawing.Point(93, 68);
            this.DteExpiredDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteExpiredDt.Name = "DteExpiredDt";
            this.DteExpiredDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpiredDt.Properties.Appearance.Options.UseFont = true;
            this.DteExpiredDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpiredDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteExpiredDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteExpiredDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteExpiredDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpiredDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteExpiredDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpiredDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteExpiredDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteExpiredDt.Size = new System.Drawing.Size(99, 20);
            this.DteExpiredDt.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(11, 68);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 14);
            this.label8.TabIndex = 12;
            this.label8.Text = "Expired Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeTenderName
            // 
            this.MeeTenderName.EnterMoveNextControl = true;
            this.MeeTenderName.Location = new System.Drawing.Point(93, 26);
            this.MeeTenderName.Margin = new System.Windows.Forms.Padding(5);
            this.MeeTenderName.Name = "MeeTenderName";
            this.MeeTenderName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeTenderName.Properties.Appearance.Options.UseFont = true;
            this.MeeTenderName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeTenderName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeTenderName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeTenderName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeTenderName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeTenderName.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeTenderName.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeTenderName.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeTenderName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeTenderName.Properties.MaxLength = 400;
            this.MeeTenderName.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeTenderName.Properties.ShowIcon = false;
            this.MeeTenderName.Size = new System.Drawing.Size(489, 20);
            this.MeeTenderName.TabIndex = 9;
            this.MeeTenderName.ToolTip = "F4 : Show/hide text";
            this.MeeTenderName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeTenderName.ToolTipTitle = "Run System";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(6, 26);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 14);
            this.label7.TabIndex = 8;
            this.label7.Text = "Tender Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(93, 47);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(99, 20);
            this.DteDocDt.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(24, 49);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 14);
            this.label6.TabIndex = 10;
            this.label6.Text = "Start Date";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSector
            // 
            this.LueSector.EnterMoveNextControl = true;
            this.LueSector.Location = new System.Drawing.Point(59, 125);
            this.LueSector.Margin = new System.Windows.Forms.Padding(5);
            this.LueSector.Name = "LueSector";
            this.LueSector.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSector.Properties.Appearance.Options.UseFont = true;
            this.LueSector.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSector.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSector.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSector.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSector.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSector.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSector.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSector.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSector.Properties.DropDownRows = 12;
            this.LueSector.Properties.NullText = "[Empty]";
            this.LueSector.Properties.PopupWidth = 500;
            this.LueSector.Size = new System.Drawing.Size(171, 20);
            this.LueSector.TabIndex = 15;
            this.LueSector.ToolTip = "F4 : Show/hide list";
            this.LueSector.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSector.EditValueChanged += new System.EventHandler(this.LueSector_EditValueChanged);
            this.LueSector.Leave += new System.EventHandler(this.LueSector_Leave);
            this.LueSector.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueSector_KeyDown);
            // 
            // FrmEmailVendorSector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Controls.Add(this.LueSector);
            this.Name = "FrmEmailVendorSector";
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.Grd1, 0);
            this.Controls.SetChildIndex(this.LueSector, 0);
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTenderDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeTenderName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSector.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnTenderDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtTenderDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteExpiredDt;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.MemoExEdit MeeTenderName;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueSector;
    }
}