﻿#region Update
/*
    03/09/2021 [IBL/AMKA] New Apps
    18/08/2022 [TYO/PRODUCT] filter coa by group berdasarkan parameter IsGroupCOAActived
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIncomingPayment3Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmIncomingPayment3 mFrmParent;
        private string mSQL = string.Empty, mCtCode = string.Empty, mBankAcCode = string.Empty;
        private iGrid mGrd;
        private bool mIsCOAItemList;

        #endregion

        #region Constructor

        public FrmIncomingPayment3Dlg2(FrmIncomingPayment3 FrmParent, iGrid Grd, bool IsCOAItemList, string CtCode, string BankAcCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mGrd = Grd;
            mIsCOAItemList = IsCOAItemList;
            mCtCode = CtCode;
            mBankAcCode = BankAcCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                
                if (mIsCOAItemList)
                    ShowData2();

                Sl.SetLueAcCtCode(ref LueAcCtCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Account#", 
                        "Description", 
                        "Type",
                        "Category"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 300, 150, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.AcDesc, B.OptDesc As AcTypeDesc, C.AcCtName ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
            SQL.AppendLine("Left Join TblAccountCategory C On Left(A.AcNo, 1)=C.AcCtCode ");
            SQL.AppendLine("Where A.AcNo Not In (Select Parent From TblCOA Where Parent is Not Null) ");
            SQL.AppendLine("And A.ActInd='Y' ");

            if (mFrmParent.mIsGroupCOAActived)
            {
                SQL.AppendLine("And EXISTS");
                SQL.AppendLine("(  ");
                SQL.AppendLine("	SELECT 1   ");
                SQL.AppendLine("	FROM TblGroupCoa  ");
                SQL.AppendLine("	WHERE AcNo =A.AcNo  ");
                SQL.AppendLine("	AND GrpCode IN   ");
                SQL.AppendLine("	(  ");
                SQL.AppendLine("		SELECT GrpCode FROM tbluser  ");
                SQL.AppendLine("		WHERE UserCode = @UserCode  ");
                SQL.AppendLine("	)  ");
                SQL.AppendLine(")  ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();

                if (mGrd.Rows.Count >= 1)
                {
                    for (int r = 0; r < mGrd.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(mGrd, r, 1).Length != 0)
                        {
                            if (mFrmParent.mIsCOACouldBeChosenMoreThanOnce && mIsCOAItemList)
                            {
                                if (Sm.GetGrdStr(mGrd, r, 1) == string.Concat(mFrmParent.mCustomerAcNoAR, mCtCode))
                                {
                                    if (Filter.Length > 0) Filter += " And ";
                                    Filter += " (A.AcNo<>@AcNo0" + r.ToString() + ") ";
                                    Sm.CmParam<String>(ref cm, "@AcNo0" + r.ToString(), Sm.GetGrdStr(mGrd, r, 1));
                                }
                            }
                            else
                            {
                                if (Filter.Length > 0) Filter += " And ";
                                Filter += " (A.AcNo<>@AcNo0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@AcNo0" + r.ToString(), Sm.GetGrdStr(mGrd, r, 1));
                            }
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " ";
                else
                    Filter = " And (" + Filter + ")";

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, "A.AcNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "A.AcDesc", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcCtCode), "C.AcCtCode", true);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.AcNo;",
                        new string[]{ "AcNo", "AcDesc", "AcTypeDesc", "AcCtName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mGrd.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsAcNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mGrd.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mGrd, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mGrd, Row1, 2, Grd1, Row2, 3);
                        Sm.SetGrdNumValueZero(mGrd, Row1, new int[] { 3, 4 });

                        mGrd.Rows.Add();
                        Sm.SetGrdNumValueZero(mGrd, mGrd.Rows.Count - 1, new int[] { 3, 4 });
                    }
                }
                mGrd.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account#.");
        }

        private bool IsAcNoAlreadyChosen(int Row)
        {
            string AcNo = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mGrd.Rows.Count - 1; r++)
            {
                if (mFrmParent.mIsCOACouldBeChosenMoreThanOnce)
                {
                    if (AcNo == string.Concat(mFrmParent.mCustomerAcNoAR, mCtCode))
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(mGrd, r, 1), AcNo)) return true;
                    }
                }
                else
                    if (Sm.CompareStr(Sm.GetGrdStr(mGrd, r, 1), AcNo)) return true;
            }
            return false;
        }

        #region Additional Method

        private void ShowData2()
        {
            try
            {
                if (
                    (mCtCode.Length > 0 && mFrmParent.mIsIncomingPaymentCOAListShowCustomerAccount && mFrmParent.mIncomingPaymentCOAAmtCalculationMethod != "3") || 
                    (mFrmParent.mDefaultCOAListIncomingPayment.Length > 0) ||
                    (mBankAcCode.Length > 0 && mFrmParent.mIncomingPaymentCOAAmtCalculationMethod == "3")
                    )
                {
                    Cursor.Current = Cursors.WaitCursor;

                    string Filter = string.Empty;
                    string Filter2 = string.Empty;
                    string Filter3 = string.Empty;
                    string Filter4 = string.Empty;
                    string CtAcNo = string.Empty;

                    if (mBankAcCode.Length > 0 && mFrmParent.mIncomingPaymentCOAAmtCalculationMethod == "3")
                    {
                        CtAcNo = Sm.GetValue("Select COAAcNo From TblBankAccount Where BankAcCode = @Param; ", mBankAcCode);
                        Filter2 = " A.AcNo = @CtAcNo ";
                    }

                    if (mCtCode.Length > 0 && mFrmParent.mIsIncomingPaymentCOAListShowCustomerAccount && mFrmParent.mIncomingPaymentCOAAmtCalculationMethod != "3")
                    {
                        CtAcNo = Sm.GetValue("Select Concat(ParValue, @Param) From TblParameter Where Parcode = 'CustomerAcNoAR' And ParValue Is Not Null; ", mCtCode);
                        Filter2 = " A.AcNo = @CtAcNo ";
                    }

                    if (mFrmParent.mDefaultCOAListIncomingPayment.Length > 0)
                        Filter3 = " Find_In_Set(A.AcNo, @DefaultCOAListIncomingPayment) ";

                    if (Filter2.Length > 0) Filter4 = Filter2;
                    if (Filter3.Length > 0)
                    {
                        if (Filter4.Length > 0) Filter4 += " Or " + Filter3;
                        else Filter4 = Filter3;
                    }

                    var cm = new MySqlCommand();

                    if (mGrd.Rows.Count >= 1)
                    {
                        for (int r = 0; r < mGrd.Rows.Count; r++)
                        {
                            if (Sm.GetGrdStr(mGrd, r, 1).Length != 0)
                            {
                                if (Filter.Length > 0) Filter += " And ";
                                Filter += " (A.AcNo<>@AcNo0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@AcNo0" + r.ToString(), Sm.GetGrdStr(mGrd, r, 1));
                            }
                        }
                    }
                    if (Filter.Length == 0)
                        Filter = " ";
                    else
                        Filter = " And (" + Filter + ")";

                    Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, "A.AcNo", false);
                    Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "A.AcDesc", false);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcCtCode), "C.AcCtCode", true);
                    Sm.CmParam<String>(ref cm, "@CtAcNo", CtAcNo);
                    Sm.CmParam<String>(ref cm, "@DefaultCOAListIncomingPayment", mFrmParent.mDefaultCOAListIncomingPayment);

                    Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + "And (" + Filter4 + ")" + Filter + " Order By A.AcNo;",
                        new string[] { "AcNo", "AcDesc", "AcTypeDesc", "AcCtName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        }, false, false, false, false
                    );
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                //Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Event

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account#");
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Description");
        }

        private void LueAcCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCtCode, new Sm.RefreshLue1(Sl.SetLueAcCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAcCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }

        #endregion

        #endregion
    }
}
