﻿#region Update
/*
 *  [ICA/AMKA] new apps 
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmGenerateProfitCenterFICOFind : RunSystem.FrmBase2
    {
        #region Field
        string mSQL = string.Empty;
        private FrmGenerateProfitCenterFICO mFrmParent;

        #endregion

        #region Constructor

        public FrmGenerateProfitCenterFICOFind(FrmGenerateProfitCenterFICO FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.ActInd, A.ProfitCenterPackageName, B.CCName, C.ProfitCenterName, ");
            SQL.AppendLine("D.SiteName, E.DeptName, F.CCName as CCName2, G.CCtName, H.WhsName, I.BCName, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("FROM TblGenerateprofitcenterHdr A ");
            SQL.AppendLine("INNER JOIN TblCostCenter B ON A.CCCode = B.CCCode ");
            SQL.AppendLine("INNER JOIN TblProfitCenter C ON A.ProfitCenterCode = C.ProfitCenterCode ");
            SQL.AppendLine("INNER JOIN TblSite D ON A.SiteCode = D.SiteCode ");
            SQL.AppendLine("INNER JOIN TblDepartment E ON A.DeptCode = E.DeptCode ");
            SQL.AppendLine("INNER JOIN TblCostCenter F ON A.CCCode2 = F.CCCode ");
            SQL.AppendLine("INNER JOIN ( ");
            SQL.AppendLine("    Select A.DocNo, Group_Concat(B.CCtName) as CCtName ");
            SQL.AppendLine("    From TblGenerateProfitCenterDtl A ");
            SQL.AppendLine("    Inner Join TblCostCategory B On A.CCtCode = B.CCtCode ");
            SQL.AppendLine("    Group By A.DocNo ");
            SQL.AppendLine(")G On A.DocNo = G.DocNo ");
            SQL.AppendLine("INNER JOIN TblWarehouse H ON A.WhsCode = H.WhsCode ");
            SQL.AppendLine("INNER JOIN ( ");
            SQL.AppendLine("    Select A.DocNo, Group_Concat(B.BCName) as BCName ");
            SQL.AppendLine("    From TblGenerateProfitCenterDtl2 A ");
            SQL.AppendLine("    Inner Join TblBudgetCategory B On A.BCCode = B.BCCode ");
            SQL.AppendLine("    Group By A.DocNo ");
            SQL.AppendLine(")I On A.DocNo = I.DocNo ");
            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5 
                        "Document#",
                        "Date",
                        "Project Name",
                        "Active",
                        "Cost Center"+Environment.NewLine+"Parent",                     
                        
                        //6-10
                        "Profit Center",   
                        "Site",
                        "Department",
                        "Cost Center",
                        "Cost Category",
                        
                        //11-15
                        "Warehouse",
                        "Budget Category",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        
                        //16-18
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 100, 180, 50, 150, 
                        
                        //6-10
                        150, 150, 150, 150, 150, 
                        
                        //11-15
                        150, 150, 100, 100, 100, 
                        
                        //16-18
                        100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 14, 17 });
            Sm.GrdFormatTime(Grd1, new int[] { 15, 18 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 13, 14, 15, 16, 17, 18 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 13, 14, 15, 16, 17, 18 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, TxtProfitCenterPackageName.Text, new string[] { "A.ProfitCenterPackageName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ProfitCenterPackageName;",
                        new string[]
                        {
                              //0
                             "DocNo",
                             
                             //1-5
                             "DocDt",
                             "ProfitCenterPackageName",
                             "ActInd",
                             "CCName",
                             "ProfitCentername",
                             
                             //6-10
                             "SiteName",
                             "DeptName",
                             "CCName2",
                             "CCtName",
                             "WhsName",
                             
                             //11-15
                             "BCName",
                             "CreateBy",
                             "CreateDt",
                             "LastUpBy",
                             "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 15);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event


        private void TxtProfitCenterPackageName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterPackageName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Profit Center Package");
        }

        #endregion

        #endregion
    }
}
