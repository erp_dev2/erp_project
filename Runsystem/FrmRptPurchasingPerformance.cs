﻿#region update
/*
    19/09/2017 [HAR] bug fixing user
    03/10/2017 [WED] bug fixing loop ke master item
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchasingPerformance : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string DocDt = string.Empty;
        private bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptPurchasingPerformance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();   
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                SetLueTransaksi(ref LueTrxCode);
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private void SetSQL(string TypeTrx)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * from ( ");
            // PO
            if (TypeTrx == "1")
            {
                SQL.AppendLine("Select Left(E.DocDt, 6) As MonthFilter, E.DocDt, G.Username, group_Concat(Distinct E.DocNo) As DocNo, ");
                SQL.AppendLine("B.ItCode, F.Itname, F.ForeignName, Right(Last_day(E.DocDt), 2) AS Totalday, Count(B.ItCode)As TotDocument, ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(E.DocDt), 2), 2) AS Avgday, '1' as DocType ");
                SQL.AppendLine("From tblmaterialrequesthdr A ");
                SQL.AppendLine("Inner Join tblmaterialrequestdtl B on B.DocNo=A.DocNo And B.CancelInd='N' And B.Status<>'C' ");
                SQL.AppendLine("Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo ");
                SQL.AppendLine("Inner Join TblPODtl D On C.DocNo=D.PORequestDOcNo And C.DNo=D.PoRequestDNo ");
                SQL.AppendLine("Inner Join TblPOHdr E On D.DocNo=E.DocNo ");
                SQL.AppendLine("Inner Join TblItem F On B.ItCode = F.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser G On E.CreateBy = G.UserCode ");
                SQL.AppendLine("Where D.CancelInd = 'N' And Left(E.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By B.ItCode  ");
            }
            // POR
            if (TypeTrx == "2")
            {
                SQL.AppendLine("Select Left(D.DocDt, 6) As MonthFilter, D.DocDt, F.Username, group_Concat(Distinct D.DocNo) As DocNo, ");
                SQL.AppendLine("B.ItCode, E.Itname, E.ForeignName, Right(Last_day(D.DocDt), 2) AS Totalday, Count(B.ItCode)As TotDocument, ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(D.DocDt), 2), 2) AS Avgday, '2' as DocType ");
                SQL.AppendLine("From tblmaterialrequesthdr A ");
                SQL.AppendLine("Inner Join tblmaterialrequestdtl B on B.DocNo=A.DocNo And B.CancelInd='N' And B.Status<>'C' ");
                SQL.AppendLine("Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo ");
                SQL.AppendLine("Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("Inner JOin TblItem E On B.ItCode = E.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=E.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser F On C.CreateBy = F.UserCode ");
                SQL.AppendLine("Where C.CancelInd = 'N' And Left(D.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By  B.ItCode  ");
            }
            //APDP
            if (TypeTrx == "3")
            {
                SQL.AppendLine("Select Left(F.DocDt, 6) As MonthFilter, F.DocDt, H.Username, group_Concat(Distinct F.DocNo) As DocNo, ");
                SQL.AppendLine("B.ItCode, G.Itname, G.ForeignName, Right(Last_day(F.DocDt), 2) AS Totalday, Count(B.ItCode)As TotDocument, ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(F.DocDt), 2), 2) AS Avgday, '3' as DocType ");
                SQL.AppendLine("From tblmaterialrequesthdr A ");
                SQL.AppendLine("Inner Join tblmaterialrequestdtl B on B.DocNo=A.DocNo And B.CancelInd='N' And B.Status<>'C' ");
                SQL.AppendLine("Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo ");
                SQL.AppendLine("Inner Join TblPODtl D On C.DocNo=D.PORequestDOcNo And C.DNo=D.PoRequestDNo ");
                SQL.AppendLine("Inner Join TblPOHdr E On D.DocNo=E.DocNo ");
                SQL.AppendLine("Inner Join TblAPDownpayment F On E.DocNo=F.PODocNo ");
                SQL.AppendLine("Inner JOin TblItem G On B.ItCode = G.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=G.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser H On F.CreateBy = H.UserCode ");
                SQL.AppendLine("Where F.cancelInd = 'N' And Left(F.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By B.ItCode ");
            }
            //Vendor Qt
            if (TypeTrx == "4")
            {
                SQL.AppendLine("Select Left(A.DocDt, 6) As MonthFilter, A.DocDt, D.Username, group_Concat(Distinct A.DocNo) As DocNo, ");
                SQL.AppendLine("B.ItCode, C.Itname, C.ForeignName, Right(Last_day(A.DocDt), 2) AS Totalday, Count(B.ItCode)As TotDocument, ");
                SQL.AppendLine("Round(Count(B.ItCode) / Right(Last_day(A.DocDt), 2), 2) AS Avgday, '4' as DocType ");
                SQL.AppendLine("from TblQtHdr A ");
                SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode ");
                SQL.AppendLine("Where A.cancelInd = 'N' And Left(A.DocDt, 6) = @MonthFilter ");
                SQL.AppendLine("Group By B.ItCode ");
            }
           
            SQL.AppendLine(")X ");
            SQL.AppendLine("Where X.MonthFilter=@MonthFilter ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code",
                        "",
                        "User", 
                        "Total"+Environment.NewLine+"Document",
                        "Document",
                        
                        //6-8
                        "Item Name",
                        "Foreign Name",
                        "Average Usage Day"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        120, 20, 150, 80, 200,
                        //6-8
                        180, 200, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.GrdColButton(Grd1, new int[] {2}); 
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
            DocDt = string.Concat(Year, Month);

            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
                return;
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
                return;
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
                return;
            }

            if (Sm.IsLueEmpty(LueTrxCode, "Transaction type")) return;
            
            try
            {
                
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@MonthFilter", DocDt);
                Sm.FilterStr(ref Filter, ref cm, TxtUserCode.Text, new string[] { "X.UserName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName", "X.ForeignName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By X.UserName, X.ItName;",
                    new string[] 
                    { 
                        "ItCode", 
                        "UserName", "TotDocument", "DocNo", "ItName", "ForeignName", 
                        "AvgDay" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    }, true, false, false, false
                    );
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 8 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                //f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        public static void SetLueTransaksi(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Purchase Order' As Col2 " +
                "Union All " +
                "Select '2' As Col1, 'PO Request' As Col2 " +
                "Union All " +
                "Select '3' As Col1, 'AP Downpayment' As Col2 " +
                "Union All " +
                "Select '4' As Col1, 'Vendor Quotation' As Col2 ",
               
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void ChkUserCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Username");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueTrxCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrxCode, new Sm.RefreshLue1(SetLueTransaksi));
            if (Sm.GetLue(LueTrxCode).Length > 0)
                SetSQL(Sm.GetLue(LueTrxCode));
        }

       

        #endregion    
    }
}
