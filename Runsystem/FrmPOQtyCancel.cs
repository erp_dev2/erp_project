﻿#region Update
/*      
    04/11/2017 [HAR] 
    otomatis terbuat MR berdasarkan parameter IsAutoGenerateMaterialRequest perubahan tambah save MR
    tambah textbox MR, save PO qtycancel tmbh MRDocNo, Showdata
    23/07/2020 [VIN/SRN] new Printout
    27/20/2020 [ICA/IMS] mengubah Material Request menjadi Purchase Request
    23/04/2021 [VIN/IMS] Bug: Printout 
    05/01/2022 [RIS/PHT] Menambah filter isFilterByDept dan isFilterByItCt
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPOQtyCancel : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            Doctitle = Sm.GetParameter("Doctitle");
        internal FrmPOQtyCancelFind FrmFind;
        internal bool
            mIsFilterBySite = false,
            mIsAutoGeneratePurchaseLocalDocNo = false,
            mIsGroupPaymentTermActived = false,
            mIsFilterByItCt = false,
            mIsFilterByDept = false;
        private List<LocalDocument> mlLocalDocument = null;
        internal bool mIsAutoGenerateMaterialRequest = false;
        private string
             mPOPrintOutCompanyLogo = "1",
             mFormPrintOutPOQtyCancel = string.Empty;


        #endregion

        #region Constructor

        public FrmPOQtyCancel(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Cancellation Purchase Order";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                mlLocalDocument = new List<LocalDocument>();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (Doctitle == "IMS")
                {
                    label3.Text = "Purchase Request#";
                    label3.Location = new System.Drawing.Point(21, 233);
                }
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtLocalDocNo, TxtVdCode, TxtPODocNo, 
                        TxtItCode, TxtItName, TxtOutstandingQtyPO, TxtCancelledQtyPO, TxtQtyPO, 
                        TxtMRDocNo, MeeRemark, ChkNewMRInd
                    }, true);
                    BtnPODocNo.Enabled = false;
                    TxtPODNo.Visible = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtCancelledQtyPO, MeeRemark, ChkNewMRInd 
                    }, false);
                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                    BtnPODocNo.Enabled = true;
                    TxtPODNo.Visible = false;
                    ChkCancelInd.Checked = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, TxtLocalDocNo, TxtVdCode, TxtPODocNo, 
               TxtItCode, TxtItName, TxtMRDocNo, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ChkNewMRInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtOutstandingQtyPO, TxtCancelledQtyPO, TxtQtyPO,
            }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPOQtyCancelFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            string LocalDocNo = mIsAutoGeneratePurchaseLocalDocNo ? string.Empty : TxtLocalDocNo.Text;
            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;

            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                SetLocalDocument(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr
                );
                if (mlLocalDocument.Count == 0) return;
             
                SetRevision(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr,
                    ref Revision
                    );

                if (SeqNo.Length > 0)
                {
                    SetLocalDocNo(
                        "POQtyCancel",
                        ref LocalDocNo,
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );
                }
                mlLocalDocument.Clear();
            }

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "POQtyCancel", "TblPOQtyCancel");
            string MRGenerateDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SavePOCancelQty(
                DocNo,
                LocalDocNo,
                SeqNo,
                DeptCode,
                ItSCCode,
                Mth,
                Yr,
                Revision,
                MRGenerateDocNo
                ));
            cml.Add(UpdatePOProcessInd());

            if (mIsAutoGenerateMaterialRequest && ChkNewMRInd.Checked)
            {
                cml.Add(SaveMR(DocNo, MRGenerateDocNo, TxtPODocNo.Text, TxtPODNo.Text));
            }

            Sm.ExecCommands(cml);
            
            ShowData(DocNo);
        }

        #region Generate Local Document

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblPOQtyCancel ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }


        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblPOHdr Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtPODocNo.Text);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);

                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtPODocNo, "PO#", false) ||
                Sm.IsTxtEmpty(TxtCancelledQtyPO, "Cancelled quantity", true)||
                IsQtyCancelBiggerThanQtyOutstanding()||
                IsAutoGenerateMaterialRequestActive();
        }

        private bool IsAutoGenerateMaterialRequestActive()
        {
            if (mIsAutoGenerateMaterialRequest && ChkNewMRInd.Checked)
            {
                if(Doctitle=="IMS")
                    Sm.StdMsg(mMsgType.Warning, "This document will be automatic create Purchase Request.");
                else
                    Sm.StdMsg(mMsgType.Warning, "This document will be automatic create Material Request.");
            }
            return false;
        }


        private MySqlCommand SavePOCancelQty(
            string DocNo,
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision,
            string MRGenerateDocNo
            )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblPOQtyCancel ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("CancelInd, PODocNo, PODNo, Qty, NewMRInd, ProcessInd, MRDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("'N', @PODocNo, @PODno, @Qty, @NewMRInd, 'O', @MRDocNo, @Remark, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@PODocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@PODNo", TxtPODNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtCancelledQtyPO.Text));
            Sm.CmParam<String>(ref cm, "@NewMRInd", ChkNewMRInd.Checked?"Y":"N");
            if (mIsAutoGenerateMaterialRequest && ChkNewMRInd.Checked)
            {
                Sm.CmParam<String>(ref cm, "@MRDocNo", MRGenerateDocNo);
            }
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdatePOProcessInd()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPODtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, A.DNo, A.Qty, ");
            SQL.AppendLine("    IfNull(B.RecvVdQty, 0)+IfNull(C.POQtyCancelQty, 0)-IfNull(D.DOVdQty, 0) As Qty2  ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(QtyPurchase) As RecvVdQty ");
            SQL.AppendLine("        From TblRecvVdDtl ");
            SQL.AppendLine("        Where PODocNo=@DocNo And PODNo=@DNo ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("        Group By PODocNo, PODNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(Qty) As POQtyCancelQty ");
            SQL.AppendLine("        From TblPOQtyCancel ");
            SQL.AppendLine("        Where PODocNo=@DocNo And PODNo=@DNo ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("        Group By PODocNo, PODNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T3.PODocNo As DocNo, T3.PODNo As DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As DOVdQty ");
            SQL.AppendLine("        From TblDOVdHdr T1 ");
            SQL.AppendLine("        Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl T3 ");
            SQL.AppendLine("            On T2.RecvVdDocNo=T3.DocNo ");
            SQL.AppendLine("            And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("            And T3.CancelInd='N' ");
            SQL.AppendLine("            And T3.PODocNo=@DocNo ");
            SQL.AppendLine("            And T3.PODNo=@DNo ");
            SQL.AppendLine("        Where T1.ReplacedItemInd='Y' ");
            SQL.AppendLine("        Group By T3.PODocNo, T3.PODNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo And A.DNo=@DNo And A.CancelInd='N' ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
            SQL.AppendLine("Set T1.ProcessInd= ");
            SQL.AppendLine("    Case When T2.Qty2=0 Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If((T2.Qty-T2.Qty2)<=0, 'F', 'P') ");
            SQL.AppendLine("    End; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", TxtPODNo.Text);

            return cm;
        }

        private MySqlCommand SaveMR(string POQtyCancelDocNo, string MRGenerateDocNo, string PODocNo, string PODno)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, LocalDocNo, POQtyCancelDocNo, DocDt, SiteCode, DeptCode, ReqType, BCCode, SeqNo, ItScCode, Mth, Yr, Revision, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @MRGenerateDocNo, E.LocalDocNo, @POQtyCancelDocNo, @DocDt, E.SiteCode, E.DeptCode, E.ReqType, E.BCCode, E.SeqNo, E.ItScCode, E.Mth, E.Yr, E.Revision, E.Remark, E.CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblPohdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PoRequestDocNo = C.DocNo And B.PoRequestDno = C.Dno ");
            SQL.AppendLine("Inner Join TblmaterialRequestDtl D ON C.MaterialRequestDocNo = D.DocNo And C.materialRequestDno = D.Dno ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr E On D.DocNo = E.DocNo   ");
            SQL.AppendLine("Where D.CancelInd = 'N' And B.DocNo=@PODocNo And B.Dno = @PODno ; ");


            SQL.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, CancelInd, CancelReason, Status, ProcessInd, ItCode, Qty, UsageDt, QtDocNo, QtDNo, UPrice, Remark, CreateBy, CreateDt)");
            SQL.AppendLine("Select @MRGenerateDocNo, '001', 'N', D.CancelReason, 'A', 'O', D.ItCode, @Qty, @DocDt, D.QtDocNo, D.QtDNo, D.UPrice, D.Remark, D.CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblPohdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PoRequestDocNo = C.DocNo And B.PoRequestDno = C.Dno ");
            SQL.AppendLine("Inner Join TblmaterialRequestDtl D ON C.MaterialRequestDocNo = D.DocNo And C.materialRequestDno = D.Dno ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr E On D.DocNo = E.DocNo   ");
            SQL.AppendLine("Where D.CancelInd = 'N' And B.DocNo=@PODocNo And B.Dno = @PODno ; ");


            SQL.AppendLine("Insert Into TblDocApproval(DocType, ApprovalDNo, DocNo, Dno, UserCode, Status, Remark,  ");
            SQL.AppendLine("CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select F.DocType, F.ApprovalDno, @MRGenerateDocNo, '001', F.UserCode, F.Status, F.Remark, ");
            SQL.AppendLine("F.CreateBy, CurrentDateTime(), F.UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblPohdr A  ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo = B.DocNo   ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PoRequestDocNo = C.DocNo And B.PoRequestDno = C.Dno  ");
            SQL.AppendLine("Inner Join TblmaterialRequestDtl D ON C.MaterialRequestDocNo = D.DocNo And C.materialRequestDno = D.Dno  ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr E On D.DocNo = E.DocNo    ");
            SQL.AppendLine("Inner Join TblDocApproval F On D.DocNo = F.DocNo  And D.DNo = F.DNo And F.DocType = 'MaterialRequest' ");
            SQL.AppendLine("Where D.CancelInd = 'N' And B.DocNo=@PODocNo And B.Dno = @PODno ;  ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@MRGenerateDocNo", MRGenerateDocNo);
            Sm.CmParam<String>(ref cm, "@POQtyCancelDocNo", POQtyCancelDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtCancelledQtyPO.Text));
            Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
            Sm.CmParam<String>(ref cm, "@PODNo", PODno);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(EditPOQtyCancel());
            cml.Add(UpdatePOProcessInd());

            Sm.ExecCommands(cml);
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentAlreadyCancelled() ||
                IsMRReplacementNotValid();
        }

        private bool IsDocumentAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblPOQtyCancel Where DocNo=@DocNo And CancelInd='Y';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsMRReplacementNotValid()
        {
            if (!ChkNewMRInd.Checked || !ChkCancelInd.Checked) return false;

            if (Sm.IsDataExist(
                "Select A.DocNo " +
                "From TblMaterialRequestHdr A " +
                "Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.Status<>'C' " +
                "Where A.POQtyCancelDocNo=@Param Limit 1;",
                TxtDocNo.Text,
                (Doctitle == "IMS") ? "Purchase Request Replacement is invalid." : "Material Request Replacement is invalid."
                )) return true;

            return false;
        }

        private MySqlCommand EditPOQtyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblPOQtyCancel Set " +
                    (ChkCancelInd.Checked?" ProcessInd='O', ":string.Empty) +
                    "   CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowPOQtyCancel(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPOQtyCancel(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.CancelInd, A.PODocNo, A.PODNo, E.ItCode, F.ItName, G.VdName, NewMRInd, ");
            SQL.AppendLine("IfNull(C.Qty, 0) As POQty, A.Qty, A.Remark, ");
            SQL.AppendLine("Round(if(C.CancelInd='N', C.Qty, 0)-IfNull(( ");
            SQL.AppendLine("    Select Sum(QtyPurchase) Qty ");
            SQL.AppendLine("    From TblRecvVdDtl ");
            SQL.AppendLine("    Where PODocNo=A.PODocNo And PODNo=A.PODNo And CancelInd='N' ");   
            SQL.AppendLine("), 0)-IfNull(( ");
            SQL.AppendLine("    Select Sum(Qty) Qty ");
            SQL.AppendLine("    From TblPOQtyCancel ");
            SQL.AppendLine("    Where PODocNo=A.PODocNo And PODNo=A.PODNo And CancelInd='N' And DocNo<>@DocNo ");
            SQL.AppendLine("), 0) ");
            SQL.AppendLine("+IfNull((");
            SQL.AppendLine("    Select Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As Qty ");
            SQL.AppendLine("    From TblDOVdHdr T1, TblDOVdDtl T2, TblRecvVdDtl T3 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.CancelInd='N' ");
            SQL.AppendLine("    And T2.RecvVdDocNo=T3.DocNo ");
            SQL.AppendLine("    And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("    And T3.CancelInd='N' ");
            SQL.AppendLine("    And T3.PODocNo=A.PODocNo ");
            SQL.AppendLine("    And T3.PODNo=A.PODNo ");
            SQL.AppendLine("    And T1.ReplacedItemInd='Y' ");
            SQL.AppendLine("), 0), 4) ");
            SQL.AppendLine("As OutstandingQty, A.MRDocNo ");
            SQL.AppendLine("From TblPOQtyCancel A  ");
            SQL.AppendLine("Inner Join TblPOHdr B On A.PODocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl C On A.PODocNo=C.DocNo And A.PODNo=C.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On D.MaterialRequestDocNo=E.DocNo And D.MaterialRequestDno=E.DNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
            SQL.AppendLine("Inner Join TblVendor G On B.VdCode=G.VdCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] 
               { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "LocalDocNo", "PODocNo", "PODNo", "ItCode", 
                    
                    //6-10
                    "ItName", "VdName", "POQty", "OutstandingQty", "Qty",  
                    
                    //11-13
                    "CancelInd", "Remark", "NewMRInd", "MRDocNo"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    TxtPODocNo.EditValue = Sm.DrStr(dr, c[3]);
                    TxtPODNo.Text = Sm.DrStr(dr, c[4]);
                    TxtItCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtItName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtVdCode.EditValue = Sm.DrStr(dr, c[7]);
                    TxtQtyPO.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    TxtOutstandingQtyPO.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    TxtCancelledQtyPO.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[11]), "Y");
                    MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                    ChkNewMRInd.Checked = Sm.DrStr(dr, c[13])=="Y";
                    TxtMRDocNo.EditValue = Sm.DrStr(dr, c[14]);
                }, true
            );

        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsAutoGeneratePurchaseLocalDocNo = Sm.GetParameterBoo("IsAutoGeneratePurchaseLocalDocNo");
            mIsAutoGenerateMaterialRequest = Sm.GetParameterBoo("IsAutoGenerateMaterialRequest");
            mIsGroupPaymentTermActived = Sm.GetParameterBoo("IsGroupPaymentTermActived");
            mPOPrintOutCompanyLogo = Sm.GetParameter("POPrintOutCompanyLogo");
            mFormPrintOutPOQtyCancel = Sm.GetParameter("FormPrintOutPOQtyCancel");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
        }

        private bool IsQtyCancelBiggerThanQtyOutstanding()
        {
            RecomputeQtyOutstanding();

            decimal QtyOut = Decimal.Parse(TxtOutstandingQtyPO.Text);
            decimal QtyCancel = Decimal.Parse(TxtCancelledQtyPO.Text);
            
            if (QtyCancel > QtyOut)
            {
                Sm.StdMsg(mMsgType.Warning, "Cancelled Quantity ("+Sm.FormatNum(QtyCancel, 0)+") "+Environment.NewLine
                    +"is greather than Outstanding Quantity ("+Sm.FormatNum(QtyOut, 0)+")");
                return true;
            }
            return false;
        }

        private void RecomputeQtyOutstanding()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Qty-IfNull(B.Qty2, 0)-IfNull(C.Qty3, 0)+IfNull(D.Qty4, 0) As Qty ");
            SQL.AppendLine("From TblPODtl A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Sum(QtyPurchase) As Qty2 From TblRecvVdDtl ");
            SQL.AppendLine("    Where CancelInd='N' And PODocNo=@DocNo And PODNo=@DNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Sum(Qty) As Qty3 From TblPOQtyCancel ");
            SQL.AppendLine("    Where CancelInd='N' And PODocNo=@DocNo And PODNo=@DNo ");
            SQL.AppendLine(") C On 0=0 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As Qty4 ");
            SQL.AppendLine("    From TblDOVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 ");
            SQL.AppendLine("        On T2.RecvVdDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("        And T3.CancelInd='N' ");
            SQL.AppendLine("        And T3.PODocNo=@DocNo ");
            SQL.AppendLine("        And T3.PODNo=@DNo ");
            SQL.AppendLine("    Where T1.ReplacedItemInd='Y' ");
            SQL.AppendLine(") D On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DNo=@DNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", TxtPODNo.Text);

            var Qty =  Sm.GetValue(cm);

            if (Qty.Length > 0)
                TxtOutstandingQtyPO.Text = Sm.FormatNum(decimal.Parse(Qty), 0);
            else
                TxtOutstandingQtyPO.Text = Sm.FormatNum(0m, 0);
        }

        #endregion

        #endregion

        private void ParPrint(string DocNo)
        {
            var l = new List<POQtyCancel>();
            var ldtl = new List<PODtl>();
            var ldtl2 = new List<PODtl2>();
            var ldtl3 = new List<PODtl3>();
            var ldtl4 = new List<PODtl4>();
            string[] TableName = { "POQtyCancel", "PODtl", "PODtl2", "PODtl3", "PODtl4" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            int Nomor = 1;

            #region Header
            var SQL = new StringBuilder();

            if (mIsFilterBySite)
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, I.CompanyName, I.CompanyPhone, I.CompanyFax, I.CompanyAddress, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            }
            SQL.AppendLine("If((Select parvalue From TblParameter Where ParCode = 'IsFilterLocalDocNo')='Y',A.LocalDocNo,A1.Docno) As DocNo, DATE_FORMAT(A1.DocDt,'%d %M %Y') As DocDt, ");
            SQL.AppendLine("Concat(RIGHT(A1.DocDt, 2), '/', RIGHT(LEFT(A1.DocDt, 6), 2), '/', RIGHT(LEFT(A1.DocDt, 4), 2))As DocDt2, A.VdContactPerson, A.ShipTo, A.BillTo, ");
            SQL.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1,IfNull(A.TaxCode2, null) As TaxCode2, IfNull(A.TaxCode3, null) As TaxCode3, A.TaxAmt, A.CustomsTaxAmt, ");
            SQL.AppendLine("A.DiscountAmt, A.Amt, A.DownPayment, A.Remark As ARemark, F.VdName, ");
            SQL.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode1  ");
            SQL.AppendLine("INNER JOIN tblpoqtycancel X ON Y.DocNo=X.PODocNo Where X.DocNo= @DocNo) TaxName1, ");
            SQL.AppendLine("(Select Z.TaxName From TblTax Z  ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode  = Y.TaxCode2 ");
            SQL.AppendLine("INNER JOIN tblpoqtycancel X ON Y.DocNo=X.PODocNo Where X.DocNo= @DocNo) TaxName2, ");  
            SQL.AppendLine("(Select  Z.TaxName From TblTax Z  ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode3  ");
            SQL.AppendLine("INNER JOIN tblpoqtycancel X ON Y.DocNo=X.PODocNo Where X.DocNo= @DocNo) TaxName3,   ");
            SQL.AppendLine("Concat(Upper(left(G.UserName,1)),Substring(Lower(G.UserName), 2, Length(G.UserName)))As CreateBy,  ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z  ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode1  ");
            SQL.AppendLine("INNER JOIN tblpoqtycancel X ON Y.DocNo=X.PODocNo Where X.DocNo= @DocNo) TaxRate1,  ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z  ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode2  ");
            SQL.AppendLine("INNER JOIN tblpoqtycancel X ON Y.DocNo=X.PODocNo Where X.DocNo= @DocNo) TaxRate2,  ");
            SQL.AppendLine("(Select  Z.TaxRate From TblTax Z  ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode3  ");
            SQL.AppendLine("INNER JOIN tblpoqtycancel X ON Y.DocNo=X.PODocNo Where X.DocNo= @DocNo) TaxRate3, ");
            SQL.AppendLine("F.Address, F.Phone, F.Fax, F.Email, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', H.ContactNumber, E.CurCode, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='IsPOSplitBasedOnTax') As SplitPO, J.SiteName, A.LocalDocNo, K.PtDay, ");
            SQL.AppendLine("D.QtDocNo, Date_Format(E.DocDt,'%d %M %Y')As QtDocDt , M.PosName, H.Position 'VdPOSName',  ");
            SQL.AppendLine("Concat(RIGHT(N.DocDt, 2), '/', RIGHT(LEFT(N.DocDt, 6), 2), '/', RIGHT(LEFT(N.DocDt, 4), 2))As MRDocDt, O.BankAcNo, P.BankName, Q.VdCtName, GROUP_CONCAT(DISTINCT N.DocNo SEPARATOR ', ') MRDocNo, R.ProjectName ");
            SQL.AppendLine("FROM tblpoqtycancel A1 ");
            SQL.AppendLine("inner join tblpohdr A ON A1.PODocNo=A.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo AND A1.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblVendor F On E.VdCode=F.VdCode ");
            SQL.AppendLine("Inner Join TblUser G On A.CreateBy=G.UserCode ");
            SQL.AppendLine("Left Join TblVendorContactPerson H On A.VdContactPerson=H.ContactPersonName And A.VdCode = H.VdCode ");
            SQL.AppendLine("Left Join TblSite J On A.SiteCode = J.SiteCode ");
            SQL.AppendLine("Left Join TblPaymentTerm K On E.PtCode=K.PtCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                SQL.AppendLine("    From TblPOhdr A  ");
                SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                SQL.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                SQL.AppendLine("    Where A.DocNo=@DocNo And A.SiteCode is not null ");
                SQL.AppendLine(") I On A.DocNo = I.DocNo ");
            }

            SQL.AppendLine("Left Join TblEmployee L On G.UserCode = L.UserCode ");
            SQL.AppendLine("Left Join TblPosition M On L.PosCode = M.PosCode ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr N On D.MaterialRequestDocNo = N.DocNo");
            SQL.AppendLine("Left Join TblVendorBankAccount O On F.VdCode = O.VdCode ");
            SQL.AppendLine("Left Join TblBank P On O.BankCode = P.BankCode ");
            SQL.AppendLine("Left Join TblVendorCategory Q On Q.VdCtCode = F.VdCtCode ");
            SQL.AppendLine("Left JOIN (  ");
            SQL.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(IFNULL(T2.ProjectName, '') Separator '\r\n') ProjectName ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1  ");
            SQL.AppendLine("    Left Join TblProjectGroup T2 ON T1.PGCode = T2.PGCode  ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") R ON R.DocNo = N.DocNo ");
            SQL.AppendLine("Where A1.DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select D.EntLogoName " +
                       "From TblPOhdr A  " +
                       "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                       "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                       "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo(mPOPrintOutCompanyLogo));
                }
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",
                         //1-5
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo",                        
                         //6-10
                         "DocDt",
                         "VdContactPerson",
                         "ShipTo" ,
                         "BillTo" ,
                         "TaxCode1",
                         //11-15
                         "TaxCode2",
                         "TaxCode3" ,
                         "TaxAmt",
                         "CustomsTaxAmt",
                         "DiscountAmt" ,
                         //16-20
                         "Amt" ,
                         "DownPayment" ,
                         "VdName",
                         "CompanyLogo",
                         "ARemark",
                         //21-25
                         "TaxName1",
                         "TaxName2",
                         "Taxname3" ,
                         "CreateBy",
                         "TaxRate1",
                         //26-30
                         "TaxRate2",
                         "TaxRate3",
                         "Address",
                         "Phone",
                         "Fax",
                         //31-35
                         "Email",
                         "ContactNumber",
                         "SplitPO",
                         "SiteName",
                         "LocalDocNo",

                         //36-39
                         "CurCode",
                         "PtDay",
                         "QtDocNo",
                         "QtDocDt",
                         "PosName",

                         //40-45
                         "VdPosName",
                         "MRDocDt",
                         "BankAcNo",
                         "BankName",
                         "VdCtName",

                         //46-48
                         "DocDt2",
                         "MRDocNo",
                         "ProjectName"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new POQtyCancel()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),

                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyLongAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            VdContactPerson = Sm.DrStr(dr, c[7]),
                            ShipTo = Sm.DrStr(dr, c[8]),
                            BillTo = Sm.DrStr(dr, c[9]),
                            TaxCode1 = Sm.DrStr(dr, c[10]),

                            TaxCode2 = Sm.DrStr(dr, c[11]),
                            TaxCode3 = Sm.DrStr(dr, c[12]),
                            TaxAmt = Sm.DrDec(dr, c[13]),
                            CustomsTaxAmt = Sm.DrDec(dr, c[14]),
                            DiscountAmt = Sm.DrDec(dr, c[15]),

                            Amt = Sm.DrDec(dr, c[16]),
                            DownPayment = Sm.DrDec(dr, c[17]),
                            VdName = Sm.DrStr(dr, c[18]),
                            CompanyLogo = Sm.DrStr(dr, c[19]),
                            ARemark = Sm.DrStr(dr, c[20]),

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            TaxName1 = Sm.DrStr(dr, c[21]),
                            TaxName2 = Sm.DrStr(dr, c[22]),
                            TaxName3 = Sm.DrStr(dr, c[23]),
                            CreateBy = Sm.DrStr(dr, c[24]),
                            TaxRate1 = Sm.DrDec(dr, c[25]),

                            TaxRate2 = Sm.DrDec(dr, c[26]),
                            TaxRate3 = Sm.DrDec(dr, c[27]),
                            Address = Sm.DrStr(dr, c[28]),
                            Phone = Sm.DrStr(dr, c[29]),
                            Fax = Sm.DrStr(dr, c[30]),

                            Email = Sm.DrStr(dr, c[31]),
                            ContactNumber = Sm.DrStr(dr, c[32]),
                            SplitPO = Sm.DrStr(dr, c[33]),
                            SiteName = Sm.DrStr(dr, c[34]),
                            LocalDocNo = Sm.DrStr(dr, c[35]),

                            CurCode = Sm.DrStr(dr, c[36]),
                            PtDay = Sm.DrDec(dr, c[37]),
                            QtDocNo = Sm.DrStr(dr, c[38]),
                            QtDocDt = Sm.DrStr(dr, c[39]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[16])),

                            Terbilang2 = Sm.Terbilang2(Sm.DrDec(dr, c[16])),
                            PosName = Sm.DrStr(dr, c[40]),
                            VdPosName = Sm.DrStr(dr, c[41]),
                            MRDocDt = Sm.DrStr(dr, c[42]),
                            BankAcNo = Sm.DrStr(dr, c[43]),

                            BankName = Sm.DrStr(dr, c[44]),
                            VdCtName = Sm.DrStr(dr, c[45]),
                            DocDt2 = Sm.DrStr(dr, c[46]),
                            MRDocNo = Sm.DrStr(dr, c[47]),
                            ProjectName = Sm.DrStr(dr, c[48]),


                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select F.ItCode, I.ItName, B.Qty Qty, A.Qty QtyCancel,  ");
                SQLDtl.AppendLine("(B.Qty-ifnull(M.Qty, 0)) As RevisedQty, I.PurchaseUomCode, K.PtName, G.CurCode, H.UPrice, ");
                SQLDtl.AppendLine("B.Discount, B.DiscountAmt, B.Roundingvalue, Concat(RIGHT(B.EstRecvDt, 2), '/', RIGHT(LEFT(B.EstRecvDt, 6), 2), '/', RIGHT(LEFT(B.EstRecvDt, 4), 2)) As EstRecvDt, B.Remark As DRemark, ");
                SQLDtl.AppendLine("(H.Uprice * (B.Qty-ifnull(M.Qty, 0))) - (H.Uprice*(B.Qty-ifnull(M.Qty, 0))*B.Discount/100) - B.DiscountAmt + B.Roundingvalue  As Total, L.DtName, I.ForeignName, I.ItCodeInternal, I.Specification ");
                SQLDtl.AppendLine("FROM tblpoqtycancel A  ");
                SQLDtl.AppendLine("INNER JOIN tblpodtl B ON A.PODocNo=B.DocNo AND A.PODNo=B.DNo ");
                SQLDtl.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
                SQLDtl.AppendLine("Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
                SQLDtl.AppendLine("Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo ");
                SQLDtl.AppendLine("Inner Join TblQtHdr G On D.QtDocNo=G.DocNo ");
                SQLDtl.AppendLine("Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo ");
                SQLDtl.AppendLine("Inner Join TblItem I On F.ItCode=I.ItCode ");
                SQLDtl.AppendLine("Inner Join TblPaymentTerm K On G.PtCode=K.PtCode ");
                SQLDtl.AppendLine("Left Join TblDeliveryType L On G.DtCode = L.DtCode ");
                SQLDtl.AppendLine("Left join( ");
                SQLDtl.AppendLine("     Select PODocNo, PODNo, Sum(Qty)Qty ");
                SQLDtl.AppendLine("     From TblPOQtyCancel where cancelind='N' ");
                SQLDtl.AppendLine("     Group by PODocNo, PODNo ");
                SQLDtl.AppendLine(")M On B.DocNo=M.PODocNo And B.DNo=M.PODNo ");
                SQLDtl.AppendLine("Where B.CancelInd='N' AND A.DocNo=@DocNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName" ,
                         "Qty",
                         "PurchaseUomCode" ,
                         "PtName",
                         "CurCode" ,

                         //6-10
                         "UPrice" ,
                         "Discount" ,
                         "DiscountAmt",
                         "RoundingValue",
                         "EstRecvDt" ,

                         //11-15
                         "DRemark" ,
                         "Total",
                         "DtName",
                         "ForeignName",
                         "ItCodeInternal",

                         //16-18
                         "Specification",
                         "QtyCancel",
                         "RevisedQty"

                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new PODtl()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),

                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[3]),
                            PtName = Sm.DrStr(drDtl, cDtl[4]),
                            CurCode = Sm.DrStr(drDtl, cDtl[5]),

                            UPrice = Sm.DrDec(drDtl, cDtl[6]),
                            Discount = Sm.DrDec(drDtl, cDtl[7]),
                            DiscountAmt = Sm.DrDec(drDtl, cDtl[8]),
                            RoundingValue = Sm.DrDec(drDtl, cDtl[9]),
                            EstRecvDt = Sm.DrStr(drDtl, cDtl[10]),

                            DRemark = Sm.DrStr(drDtl, cDtl[11]),
                            DTotal = Sm.DrDec(drDtl, cDtl[12]),
                            DtName = Sm.DrStr(drDtl, cDtl[13]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[14]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[15]),

                            Specification = Sm.DrStr(drDtl, cDtl[16]),
                            QtyCancel = Sm.DrDec(drDtl, cDtl[17]),
                            RevisedQty = Sm.DrDec(drDtl, cDtl[18]),


                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Signature

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

               
                SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtl2.AppendLine("From ( ");
                SQLDtl2.AppendLine("    Select Distinct ");
                SQLDtl2.AppendLine("    B.UserCode, C.UserName, "); // Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName,
                SQLDtl2.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                SQLDtl2.AppendLine("    From TblPODtl A ");
                SQLDtl2.AppendLine("    Inner Join TblDocApproval B On B.DocType='PORequest' And A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
                SQLDtl2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'POrequest' ");
                SQLDtl2.AppendLine("    INNER JOIN tblpoqtycancel T ON A.DocNO= T.PODocNo AND A.Dno=T.PODNo ");
                SQLDtl2.AppendLine("    Where A.CancelInd='N' And T.DocNo=@DocNo  ");
                SQLDtl2.AppendLine("    Union All ");
                SQLDtl2.AppendLine("    Select Distinct ");
                SQLDtl2.AppendLine("    A.CreateBy As UserCode, B.UserName, "); //Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName)))
                SQLDtl2.AppendLine("    '1' As DNo, 0 As Level, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                SQLDtl2.AppendLine("    From TblPODtl A ");
                SQLDtl2.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl2.AppendLine("    INNER JOIN tblpoqtycancel T ON A.DocNO= T.PODocNo AND A.Dno=T.PODNo ");
                SQLDtl2.AppendLine("    Where A.CancelInd='N' And T.DocNo=@DocNo  ");
                SQLDtl2.AppendLine(") T1 ");
                SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                SQLDtl2.AppendLine("Order By T1.Level; ");
                
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", DocNo);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {

                        ldtl2.Add(new PODtl2()
                        {
                            Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            PosName = Sm.DrStr(drDtl2, cDtl2[2]),
                            Space = Sm.DrStr(drDtl2, cDtl2[3]),
                            DNo = Sm.DrStr(drDtl2, cDtl2[4]),
                            Title = Sm.DrStr(drDtl2, cDtl2[5]),
                            LastUpDt = Sm.DrStr(drDtl2, cDtl2[6])
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail date
            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select * From (  ");
                SQLDtl3.AppendLine("Select DATE_FORMAT(SUBSTRING(LastUpDt, 1, 8),'%d %M %Y') As LastUpDt, ApprovalDNo As DNo  ");
                SQLDtl3.AppendLine("From TblDocApproval  ");
                SQLDtl3.AppendLine("Where DocType = 'PORequest'  ");
                SQLDtl3.AppendLine("And DocNo In (Select PORequestDocNo From tblpodtl A inner join tblpoqtycancel B ON A.DocNo=B.PODocNo WHERE B.DocNo=@DocNo)  ");
                SQLDtl3.AppendLine("Group By DocNo, ApprovalDNo  ");
                SQLDtl3.AppendLine("UNION  ");
                SQLDtl3.AppendLine("Select DATE_FORMAT(SUBSTRING(A.CreateDt, 1, 8),'%d %M %Y') AS LastUpDt, '1' As DNo  ");
                SQLDtl3.AppendLine("From tblpohdr A ");
                SQLDtl3.AppendLine("INNER JOIN tblpoqtycancel B ON A.DocNo=B.PODocNo ");
                SQLDtl3.AppendLine("WHERE B.DocNo = @DocNo  ");
                SQLDtl3.AppendLine(") G1  ");
                SQLDtl3.AppendLine("Order By G1.DNo ");


                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", DocNo);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                        {
                         //0
                         "LastUpdt", 
                         "DNo"
                        });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new PODtl3()
                        {
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[0]),
                            DNo = Sm.DrStr(drDtl3, cDtl3[1])
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);
            #endregion

            #region Detail note
            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine("Select ParValue From TblParameter Where ParCode='PONotes';");

                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", DocNo);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "ParValue"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {
                        ldtl4.Add(new PODtl4()
                        {
                            Note = Sm.DrStr(drDtl4, cDtl4[0])
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion


            Sm.PrintReport(mFormPrintOutPOQtyCancel, myLists, TableName, false);

        }

        #region Event

        private void BtnPODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPOQtyCancelDlg(this));
        }

        private void TxtQtyPO_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQtyPO, 0);
        }

        private void TxtOutstandingQtyPO_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtOutstandingQtyPO, 0);
        }

        private void TxtCancelledPO_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCancelledQtyPO, 0);
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void BtnMRDocNo_Click(object sender, EventArgs e)
        {
            if (TxtMRDocNo.Text.Length > 0)
            {
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtMRDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;
            ParPrint(TxtDocNo.Text);
        }
        #endregion

        #region Class

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        #endregion

        #region Report Class

        private class POQtyCancel
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyLongAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string VdContactPerson { get; set; }
            public string ShipTo { get; set; }
            public string BillTo { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal CustomsTaxAmt { get; set; }
            public decimal DiscountAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal DownPayment { get; set; }
            public string ARemark { get; set; }
            public string PrintBy { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public string CreateBy { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Email { get; set; }
            public string ContactNumber { get; set; }
            public string SplitPO { get; set; }
            public string SiteName { get; set; }
            public string LocalDocNo { get; set; }
            public string CurCode { get; set; }
            public decimal PtDay { get; set; }
            public string QtDocNo { get; set; }
            public string QtDocDt { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string PosName { get; set; }
            public string VdPosName { get; set; }
            public string MRDocDt { get; set; }
            public string BankAcNo { get; set; }
            public string BankName { get; set; }
            public string VdCtName { get; set; }
            public string DocDt2 { get; set; }
            public string ProjectName { get; set; }
            public string MRDocNo { get; set; }
        }

        private class PODtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string PtName { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Discount { get; set; }
            public decimal DiscountAmt { get; set; }
            public decimal RoundingValue { get; set; }
            public string EstRecvDt { get; set; }
            public string DRemark { get; set; }
            public decimal DTotal { get; set; }
            public string DtName { get; set; }
            public string ForeignName { get; set; }
            public string ItCodeInternal { get; set; }
            public string Specification { get; set; }
            public decimal QtyCancel { get; set; }
            public decimal RevisedQty { get; set; }
        }

        private class PODtl2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class PODtl3
        {
            public string LastUpDt { get; set; }
            public string DNo { get; set; }
        }

        private class PODtl4
        {
            public string Note { get; set; }
        }

        
        #endregion

        


    }
}
