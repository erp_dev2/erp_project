﻿#region Update
/*
    05/07/2018 [TKG] department tidak mandatory.
    04/04/2023 [WED/IOK] tambah Bank Account
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRHAFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmRHA mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRHAFind(FrmRHA FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.HolidayDt, A.VoucherRequestDocNo, C.VoucherDocNo, B.EmpCode, D.EmpName, D.EmpCodeOld, E.PosName, F.DeptName, G.SiteName, ");
            SQL.AppendLine("A.CurCode, B.Tax, B.Amt, H.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            
            SQL.AppendLine("Case K.ParValue When '1' Then ");

            SQL.AppendLine("Trim(Concat( ");
            SQL.AppendLine("Case When J.BankName Is Not Null Then Concat(J.BankName, ' ') Else '' End,  ");
            SQL.AppendLine("Case When I.BankAcNo Is Not Null  ");
            SQL.AppendLine("    Then Concat(I.BankAcNo) ");
            SQL.AppendLine("    Else IfNull(I.BankAcNm, '') End, ");
            SQL.AppendLine("Case When I.Remark Is Not Null Then Concat(' ', '(', I.Remark, ')') Else '' End ");
            SQL.AppendLine(")) ");

            SQL.AppendLine("Else ");
            
            SQL.AppendLine("Trim(Concat( ");
            SQL.AppendLine("Case When I.BankAcNo Is Not Null  ");
            SQL.AppendLine("    Then Concat(I.BankAcNo, ' [', IfNull(I.BankAcNm, ''), ']') ");
            SQL.AppendLine("    Else IfNull(I.BankAcNm, '') End, ");
            SQL.AppendLine("Case When J.BankName Is Not Null Then Concat(' ', J.BankName) Else '' End ");
            SQL.AppendLine(")) ");

            SQL.AppendLine("End As BankAcName ");
            
            SQL.AppendLine("From TblRHAHdr A ");
            SQL.AppendLine("Inner Join TblRHADtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee D On B.EmpCode=D.EmpCode ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And D.GrdLvlCode In ( ");
                SQL.AppendLine("    Select T2.GrdLvlCode ");
                SQL.AppendLine("    From TblPPAHdr T1 ");
                SQL.AppendLine("    Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("Left Join TblSite G On A.SiteCode=G.SiteCode ");
            SQL.AppendLine("Left Join TblOption H On D.PayrunPeriod=H.OptCode And H.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Inner Join TblBankAccount I On A.BankAcCode = I.BankAcCode ");
            SQL.AppendLine("Left Join TblBank J On I.BankCode = J.BankCode ");
            SQL.AppendLine("Left Join TblParameter K On K.ParCode = 'BankAccountFormat' ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Status",
                    "Holiday Date",
                    
                    //6-10
                    "Voucher Request#",
                    "Voucher#",
                    "Employee's"+Environment.NewLine+"Code",
                    "Employee's Name",
                    "Old Code",
                    
                    //11-15
                    "Position",
                    "Department",
                    "Site",
                    "Payrun" + Environment.NewLine + "Period",
                    "Currency",
                    
                    //16-20
                    "Tax",
                    "Amount",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    
                    //21-24
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date",
                    "Last"+Environment.NewLine+"Updated Time",
                    "Bank Account"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 80, 80, 100, 
                    
                    //6-10
                    150, 150, 80, 200, 100, 
                    
                    //11-15
                    150, 150, 150, 150, 80, 

                    //16-20
                    130, 130, 100, 100, 100, 

                    //21-24
                    100, 100, 100, 200
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 16, 17 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 5, 19, 22 });
            Sm.GrdFormatTime(Grd1, new int[] { 20, 23 });
            Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21, 22, 23 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[24].Move(8);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21, 22, 23 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrunPeriod), "D.PayrunPeriod", true);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "D.EmpCode", "D.EmpName", "D.EmpCodeOld" });
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "HolidayDt", "VoucherRequestDocNo", 
                            
                            //6-10
                            "VoucherDocNo", "EmpCode", "EmpName", "EmpCodeOld", "PosName", 
                            
                            //11-15
                            "DeptName", "SiteName", "PayrunPeriodDesc", "CurCode", "Tax", 
                            
                            //16-20
                            "Amt", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",

                            //21
                            "BankAcName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                        }, true, false, false, false
                    );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[]{16, 17});
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 16, 17 });
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrunPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payrun period");
        }

        #endregion

        #endregion

    }
}
