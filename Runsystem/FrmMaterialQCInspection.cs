﻿#region Update
/*
    10/10/2017 [WED] merapikan SetGrd()
    10/10/2017 [WED] tambah parameter NumberOfInventoryUomCode
    12/10/2017 [WED] tambah Qty dan Uom 2 dan 3
    16/10/2017 [WED] pindah posisi Grd1 dan Grd2
    07/10/2017 [TKG] menggunakan data dari master inspector
    17/12/2019 [DITA+VIN/IMS] Print Out baru
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialQCInspection : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mWhsCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal int mStateIndicator = 0; // indikator untuk tombol QC Planning aktif jika dia insert
        internal FrmMaterialQCInspectionFind FrmFind;
        private string mNumberOfInventoryUomCode = string.Empty;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmMaterialQCInspection(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Material QC Inspection";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
               
                SetLueStatus(ref LueStatus);
                base.FrmLoad(sender, e);
                LueStatus.Visible = false;

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    //ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {

            #region Grid 1 (QC Planning's Detail Item)

            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "",
                        "QCPCode",

                        //6-10
                        "Status",
                        "Item's Code",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's Name", 
                        
                        //11-15
                        "Batch Number", 
                        "Lot",
                        "Bin",
                        "Quantity",
                        "UoM",

                        //16-20
                        "Quantity 2",
                        "UoM 2",
                        "Quantity 3",
                        "UoM 3",
                        "Check Quantity"+Environment.NewLine+"Per Bin",

                        //21-23
                        "QC Planning's Remark",
                        "Remark",
                        "QCPlanningDNo"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 20, 20, 0,
                        
                        //6-10
                        100, 80, 20, 100, 250,   
                        
                        //11-15
                        200, 60, 80, 80, 80, 
                        
                        //16-20
                        80, 80, 80, 80, 130, 
                        
                        //21-23
                        200, 200, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18, 20 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 4, 8 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5, 7, 8, 9, 21, 23 }, false);

            if (mNumberOfInventoryUomCode == "1")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19 }, false);
            }

            if (mNumberOfInventoryUomCode == "2")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19 }, false);
            }

            #endregion

            #region Grid 2 (QC Inspector)

            Grd2.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-2
                        "Inspector's Code",
                        "Inspector's Name"
                    },
                     new int[] 
                    {
                        //0
                        20, 
                        
                        //1-2
                        100, 200
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 21 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtQCPlanningDocNo, TxtWhsCode, TmeStartTm, LueStatus, TxtQCParameter,
                         MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0 });
                    BtnQCPlanningDocNo.Enabled = false;
                    BtnQCPlanningDocNo2.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TmeStartTm, MeeRemark, LueStatus
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6, 8, 20, 22 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0 });
                    BtnQCPlanningDocNo.Enabled = true;
                    BtnQCPlanningDocNo2.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtQCPlanningDocNo, TxtWhsCode, TmeStartTm, TxtQCParameter,
                MeeRemark, LueStatus
            });
            mWhsCode = string.Empty;
            mStateIndicator = 0;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 16, 18, 20 });
            Sm.ClearGrd(Grd2, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMaterialQCInspectionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                mStateIndicator = 1;
                SetFormControl(mState.Insert);
                BtnQCPlanningDocNo.Enabled = true;
                Sm.SetDteCurrentDate(DteDocDt);
                BtnQCPlanningDocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "MaterialQC", "MaterialQCDtl" };
            var l = new List<MaterialQC>();
            var ldtl = new List<MaterialQCDtl>();

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("A.DocNo,  DATE_FORMAT(A.DocDt, '%d %M %Y') DocDt, DATE_FORMAT(B.RecvDt, '%d %M %Y' ) RecvDt, B.PODocNo, B.ProjectName, B.VdName ");
            SQL.AppendLine("From TblMaterialQCInspectionHdr A  ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  SELECT X1.DocNo, X5.DocDt RecvDt,  GROUP_CONCAT(DISTINCT X4.PODocNo SEPARATOR ', ') PODocNo, ");
            SQL.AppendLine("  GROUP_CONCAT(DISTINCT IFNULL(X13.ProjectName, X12.ProjectName)) ProjectName, GROUP_CONCAT(DISTINCT X6.VdName SEPARATOR ', ') VdName ");
            SQL.AppendLine("  FROM TblMaterialQCInspectionHdr X1 ");
            SQL.AppendLine("  INNER JOIN TblMaterialQCInspectionDtl X2 ON X1.DocNo = X2.DocNo ");
            SQL.AppendLine("  INNER JOIN TblQCPlanningDtl X3 ON X1.QCPlanningDocNo = X3.DocNo AND X3.DNo = X2.QCPlanningDNo ");
            SQL.AppendLine("  INNER JOIN TblRecvVdDtl X4 ON X3.ItCode = X4.ItCode AND X3.BatchNo = X4.BatchNo AND X3.Lot = X4.Lot AND X3.Bin = X4.Bin ");
            SQL.AppendLine("  INNER JOIN TblRecvVdHdr X5 ON X4.DocNo = X5.DocNo ");
            SQL.AppendLine("  INNER JOIN TblVendor X6 ON X5.VdCode = X6.VdCode ");
            SQL.AppendLine("  INNER JOIN TblPODtl X7 ON X4.PODocNo = X7.DocNo AND X4.PODNo = X7.DNo ");
            SQL.AppendLine("  INNER JOIN TblPORequestDtl X8 ON X7.PORequestDocNo = X8.DocNo AND X7.PORequestDNo = X8.DNo ");
            SQL.AppendLine("  INNER JOIN TblMaterialRequestHdr X9 ON X8.MaterialRequestDocNo = X9.DocNo ");
            SQL.AppendLine("  INNER JOIN TblSOContractHdr X10 ON X9.SOCDocNo = X10.DocNo ");
            SQL.AppendLine("  INNER JOIN TblBOQHdr X11 ON X10.BOQDocNo = X11.DOcNo ");
            SQL.AppendLine("  INNER JOIN TblLOPHdr X12 ON X11.LOPDocNo = X12.DocNo ");
            SQL.AppendLine("  LEFT JOIN TblProjectGroup X13 ON X12.PGCode = X13.PGCode ");
            SQL.AppendLine("   GROUP BY X1.DocNo ");
            SQL.AppendLine(") B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo  ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",
                         //1-5
                         "DocNo",
                         "DocDt",
                         "RecvDt",
                         "PODocNo",
                         "ProjectName",
                         //6
                         "VdName",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new MaterialQC()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            DocNo = Sm.DrStr(dr, c[1]),
                            DocDt = Sm.DrStr(dr, c[2]),
                            RecvDt = Sm.DrStr(dr, c[3]),
                            PODocNo = Sm.DrStr(dr, c[4]),
                            ProjectName = Sm.DrStr(dr, c[5]),
                            VdName = Sm.DrStr(dr, c[6]),
                            
                        });
                    }
                }

                dr.Close();
            }
            myLists.Add(l);

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

               SQLDtl.AppendLine("SELECT C.ItCodeInternal, Sum(A.Qty) Qty ,C.ItName, C.InventoryUomCode, C.Specification, A.Remark, D.QCPassedCount, ");
                SQLDtl.AppendLine("F.QCHoldCount, G.QCRejectedCount  ");
                SQLDtl.AppendLine("From TblMaterialQCInspectionDtl A  ");
                SQLDtl.AppendLine("INNER JOIN TblMaterialQCInspectionHdr E ON A.DocNo = E.DocNo  ");
                SQLDtl.AppendLine("INNER JOIN TblQCPlanningDtl B ON E.QCPlanningDocNo = B.DocNo AND B.DNo = A.QCPlanningDNo  ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode  ");
                SQLDtl.AppendLine(" Left Join  ");
                SQLDtl.AppendLine("  ( ");
	            SQLDtl.AppendLine("     SELECT X2.DocNo, X4.ItCodeInternal, COUNT(*) QCPassedCount ");
	            SQLDtl.AppendLine("     From TblMaterialQCInspectionDtl X1  ");
	            SQLDtl.AppendLine("     INNER JOIN TblMaterialQCInspectionHdr X2 ON X1.DocNo = X2.DocNo  ");
                SQLDtl.AppendLine("     INNER JOIN TblQCPlanningDtl X3 ON X2.QCPlanningDocNo = X3.DocNo AND X3.DNo = X1.QCPlanningDNo  ");
                SQLDtl.AppendLine("     Inner Join TblItem X4 ON X3.ItCode=X4.ItCode  ");
                SQLDtl.AppendLine("     Inner Join TblOption X5 ON X5.OptCat='QCStatus' AND X1.Status= '1'  AND X5.Optcode= X1.Status ");
	            SQLDtl.AppendLine("     WHERE X1.DocNo=@DocNo  ");
                SQLDtl.AppendLine("    GROUP BY X2.DocNo, X4.ItCodeInternal ");
			    SQLDtl.AppendLine("	) D ON D.DocNo = A.DocNo AND C.ItCodeInternal = D.ItCodeInternal ");
			    SQLDtl.AppendLine("	Left Join  ");
                SQLDtl.AppendLine(" ( ");
	            SQLDtl.AppendLine("      SELECT X2.DocNo, X4.ItCodeInternal, COUNT(*) QCHoldCount ");
	            SQLDtl.AppendLine("      From TblMaterialQCInspectionDtl X1  ");
	            SQLDtl.AppendLine("      INNER JOIN TblMaterialQCInspectionHdr X2 ON X1.DocNo = X2.DocNo  ");
                SQLDtl.AppendLine("      INNER JOIN TblQCPlanningDtl X3 ON X2.QCPlanningDocNo = X3.DocNo AND X3.DNo = X1.QCPlanningDNo  ");
                SQLDtl.AppendLine("      Inner Join TblItem X4 ON X3.ItCode=X4.ItCode  ");
                SQLDtl.AppendLine("      Inner Join TblOption X5 ON X5.OptCat='QCStatus' AND X1.Status= '2'  AND X5.Optcode= X1.Status ");
	            SQLDtl.AppendLine("      WHERE X1.DocNo=@DocNo  ");
                SQLDtl.AppendLine("      GROUP BY X2.DocNo, X4.ItCodeInternal ");
		        SQLDtl.AppendLine("  ) F ON F.DocNo = A.DocNo AND C.ItCodeInternal = F.ItCodeInternal ");
		        SQLDtl.AppendLine("	Left Join  ");
                SQLDtl.AppendLine(" ( ");
	            SQLDtl.AppendLine("      SELECT X2.DocNo, X4.ItCodeInternal, COUNT(*) QCRejectedCount ");
	            SQLDtl.AppendLine("      From TblMaterialQCInspectionDtl X1  ");
	            SQLDtl.AppendLine("      INNER JOIN TblMaterialQCInspectionHdr X2 ON X1.DocNo = X2.DocNo  ");
                SQLDtl.AppendLine("      INNER JOIN TblQCPlanningDtl X3 ON X2.QCPlanningDocNo = X3.DocNo AND X3.DNo = X1.QCPlanningDNo  ");
                SQLDtl.AppendLine("      Inner Join TblItem X4 ON X3.ItCode=X4.ItCode  ");
                SQLDtl.AppendLine("      Inner Join TblOption X5 ON X5.OptCat='QCStatus' AND X1.Status= '3'  AND X5.Optcode= X1.Status ");
	            SQLDtl.AppendLine("      WHERE X1.DocNo=@DocNo  ");
                SQLDtl.AppendLine("      GROUP BY X2.DocNo, X4.ItCodeInternal ");
		        SQLDtl.AppendLine(" ) G ON G.DocNo = A.DocNo AND C.ItCodeInternal = G.ItCodeInternal ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo  ");
                SQLDtl.AppendLine("GROUP BY C.ItCodeInternal ");
                SQLDtl.AppendLine("Order By A.DNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCodeInternal",
                         //1-5
                         "Qty",
                         "ItName",
                         "InventoryUomCode",
                         "Specification",
                         "Remark",
                         //6-8
                         "QCPassedCount",
                         "QCHoldCount",
                         "QCRejectedCount",
                         
                        });
                int nomor = 0;
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new MaterialQCDtl()
                        {
                            No = nomor,
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[0]),
                            Qty = Sm.DrDec(drDtl, cDtl[1]),
                            ItName = Sm.DrStr(drDtl, cDtl[2]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[3]),
                            Specification = Sm.DrStr(drDtl, cDtl[4]),
                            Remark = Sm.DrStr(drDtl, cDtl[5]),
                            QCPassedCount = Sm.DrDec(drDtl, cDtl[6]),
                            QCHoldCount = Sm.DrDec(drDtl, cDtl[7]),
                            QCRejectedCount = Sm.DrDec(drDtl, cDtl[8]),
                         
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            Sm.PrintReport("MaterialQCInspection", myLists, TableName, false);

        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (Sm.IsGrdColSelected(new int[] { 3, 4, 6 }, e.ColIndex))
                    {
                        if (e.ColIndex == 6) LueRequestEdit(Grd1, LueStatus, ref fCell, ref fAccept, e);

                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 20 });
                        SetLueStatus(ref LueStatus);
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem("XXX");
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 20)
            {
                if (Sm.GetGrdStr(Grd1, 0, 20).Length != 0)
                {
                    var ChkQtyPerBin = 0m;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        ChkQtyPerBin = Sm.GetGrdDec(Grd1, Row, 20);
                        if (Sm.GetGrdStr(Grd1, Row, 20).Length != 0) Grd1.Cells[Row, 14].Value = ChkQtyPerBin;
                    }
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 0)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ") && !Sm.IsTxtEmpty(TxtQCPlanningDocNo, "Planning Document#", false))
                            Sm.FormShowDialog(new FrmMaterialQCInspectionDlg2(this));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    }
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmMaterialQCInspectionDlg2(this));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MaterialQCInspection", "TblMaterialQCInspectionHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveMaterialQCInspectionHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                {
                    cml.Add(SaveMaterialQCInspectionDtl(DocNo, Row));
                }
            }
            for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                if (Sm.GetGrdStr(Grd2, Row2, 2).Length > 0)
                    cml.Add(SaveMaterialQCInspectionDtl2(DocNo, Row2));
             

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtQCPlanningDocNo, "Planned document#", false) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsQCStatusStillOnHold() ||
                IsGrd2Empty() ||
                IsGrd2ExceedMaxRecords() ||
                IsGrd2ValueNotValid();
        }

        private bool IsQCStatusStillOnHold()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                string QCStatusCode = Sm.GetGrdStr(Grd1, Row, 5);
                if (Sm.CompareStr(QCStatusCode,"2"))
                {
                    Sm.StdMsg(mMsgType.Warning, "QC Status is still On Hold.");
                    Sm.FocusGrd(Grd1, Row, 6);
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No detail planning item is recorded.");
                Grd1.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrd2Empty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 QC Inspector.");
                Grd2.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrd2ExceedMaxRecords()
        {
            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Inspector entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, false, "Status is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 7, false, "Item is empty.")
                    ) return true;
            }
            return false;
        }

        private bool IsGrd2ValueNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Inspector is empty.")
                    ) return true;
            }
            return false;
        }

        private MySqlCommand SaveMaterialQCInspectionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialQCInspectionHdr(DocNo, DocDt, QCPlanningDocNo, StartTm, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @QCPlanningDocNo, @StartTm, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblQCPlanningHdr Set ");
            SQL.AppendLine("  ProcessInd = '1', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @QCPlanningDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@QCPlanningDocNo", TxtQCPlanningDocNo.Text);
            Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStartTm));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveMaterialQCInspectionDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblMaterialQCInspectionDtl(DocNo, DNo, QCPlanningDNo, CancelInd, Status, ChkQtyPerBin, Qty, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @QCPlanningDNo, 'N', @Status, @ChkQtyPerBin, @Qty, @Remark, @CreateBy, CurrentDateTime()); ");

            SQLDtl.AppendLine("UPDATE TblQCPlanningDtl A SET A.Status = @Status, A.CancelInd = 'N', A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime() ");
            SQLDtl.AppendLine("WHERE A.DocNo = @QCPlanningDocNo AND A.DNo = @QCPlanningDNo; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@ChkQtyPerBin", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@QCPlanningDocNo", TxtQCPlanningDocNo.Text);
            Sm.CmParam<String>(ref cm, "@QCPlanningDNo", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMaterialQCInspectionDtl2(string DocNo, int Row)
        {
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Insert Into TblMaterialQCInspectionDtl2(DocNo, DNo, EmpCode, CreateBy, CreateDt) ");
            SQLDtl3.AppendLine("Values(@DocNo, @DNo, @EmpCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl3.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            int rowcount = Grd1.Rows.Count - 1;
            UpdateCancelledMaterialQCInspection();

            string DNo = "'XXX'";
            string QCPDNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                {
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";
                    QCPDNo = QCPDNo + ",'" + Sm.GetGrdStr(Grd1, Row, 23)+ "'";
                }
            }

            if (IsCancelledDataNotValid(DNo) || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelMaterialQCInspectionDtl(DNo, QCPDNo));

            if (AllDataCancelled())
            {
                cml.Add(UpdateQCPlanningProcessInd(TxtQCPlanningDocNo.Text));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool AllDataCancelled()
        {
            bool flag = false;

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) == true)
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                    break;
                }
            }

            return flag;
        }

        private void UpdateCancelledMaterialQCInspection()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, CancelInd ");
            SQL.AppendLine("From TblMaterialQCInspectionDtl ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By DNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Index = 0; Index < Grd1.Rows.Count - 1; Index++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Index, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Index, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Index, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return IsCancelledDataNotExisted(DNo);
        }

        private bool IsCancelledDataNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelMaterialQCInspectionDtl(string DNo, string QCPDNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialQCInspectionDtl Set ");
            SQL.AppendLine("    CancelInd='Y', Status='2', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo In (" + DNo + "); ");

            SQL.AppendLine("UPDATE TblQCPlanningDtl A SET A.CancelInd = 'Y', A.Status = '2', A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("WHERE A.DocNo = @QCPlanningDocNo AND A.DNo IN (" + QCPDNo + "); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@QCPlanningDocNo", TxtQCPlanningDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateQCPlanningProcessInd(string QCPlanningDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblQCPlanningHdr Set ");
            SQL.AppendLine("    ProcessInd = '0', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@QCPlanningDocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.DocNo ");
            SQL.AppendLine("    From TblQCPlanningDtl A ");
            SQL.AppendLine("    Where A.DocNo = @QCPlanningDocNo ");
            SQL.AppendLine("    And A.Status <> '2' ");
            SQL.AppendLine("    Limit 1 ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@QCPlanningDocNo", QCPlanningDocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowMaterialQCInspectionHdr(DocNo);
                ShowMaterialQCInspectionDtl(DocNo);
                ShowMaterialQCInspectionDtl2(DocNo);

                for (int Col = 23; Col < Grd1.Cols.Count; Col++)
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { Col });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void ShowDataPlanning(string PlanningDocNo)
        {
            try
            {
                ClearGrd();
                ShowPlanningDtl(PlanningDocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPlanningDtl(string PlanningDocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PlanningDocNo", PlanningDocNo);

            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("SELECT A.DNo, 'N' AS CancelInd, A.Status, C.OptDesc As StatusDesc, ");
            SQLDtl.AppendLine("A.ItCode, A.BatchNo, A.Lot, A.Bin, A.Qty, A.Qty2, A.Qty3, A.Remark, ");
            SQLDtl.AppendLine("B.ItCodeInternal, B.ItName, B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3 ");
            SQLDtl.AppendLine("FROM TblQCPlanningDtl A ");
            SQLDtl.AppendLine("INNER JOIN TblItem B ON A.ItCode=B.ItCode ");
            SQLDtl.AppendLine("INNER JOIN TblOption C ON C.OptCat='QCStatus' AND A.Status=C.OptCode ");
            SQLDtl.AppendLine("Where A.DocNo=@PlanningDocNo AND A.Status = '2' ");
            SQLDtl.AppendLine("Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "CancelInd", 

                    //1-5
                    "Status", "StatusDesc", "ItCode", "ItCodeInternal", "ItName",  
                    
                    //6-10
                    "BatchNo","Lot", "Bin", "Qty", "InventoryUomCode", 
                    
                    //11-15
                    "Remark", "DNo", "Qty2", "InventoryUomCode2", "Qty3", 
                    
                    //16
                    "InventoryUomCode3"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 11);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 12);
                    Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 20 });
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 16, 18, 20 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowMaterialQCInspectionHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.QCPlanningDocNo, C.WhsName, D.QCPDesc, A.StartTm, A.Remark ");
            SQL.AppendLine("From TblMaterialQCInspectionHdr A ");
            SQL.AppendLine("Inner Join TblQCPlanningHdr B On A.QCPlanningDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On B.WhsCode=C.WhsCode ");
            SQL.AppendLine("INNER JOIN TblQCParameter D ON B.QCPCode = D.QCPCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "QCPlanningDocNo", "WhsName", "StartTm", "QCPDesc",
 
                        //6
                        "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtQCPlanningDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        TxtWhsCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtQCParameter.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetTme(TmeStartTm, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowMaterialQCInspectionDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, B.DNo AS QCPlanningDNo, A.CancelInd, A.Status, D.OptDesc As StatusDesc, ");
            SQL.AppendLine("B.ItCode, B.BatchNo, B.Lot, B.Bin, A.ChkQtyPerBin, A.Qty, B.Qty2, B.Qty3, B.Remark AS PlanningRemark, ");
            SQL.AppendLine("C.ItCodeInternal, C.ItName, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, A.Remark ");
            SQL.AppendLine("From TblMaterialQCInspectionDtl A ");
            SQL.AppendLine("INNER JOIN TblMaterialQCInspectionHdr E ON A.DocNo = E.DocNo ");
            SQL.AppendLine("INNER JOIN TblQCPlanningDtl B ON E.QCPlanningDocNo = B.DocNo AND B.DNo = A.QCPlanningDNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblOption D On D.OptCat='QCStatus' And A.Status=D.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "Status", "StatusDesc", "ItCode", "ItCodeInternal",  
                    
                    //6-10
                    "ItName","BatchNo", "Lot", "Bin", "Qty", 
                    
                    //11-15
                    "InventoryUomCode", "PlanningRemark", "ChkQtyPerBin", "Remark", "QCPlanningDNo",

                    //16-19
                    "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 17);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 18);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 19);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 14);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 15);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 16, 18, 20 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowMaterialQCInspectionDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select A.DNo, A.EmpCode, B.InspectorName ");
            SQLDtl2.AppendLine("From TblMaterialQCInspectionDtl2 A ");
            SQLDtl2.AppendLine("Left Join TblInspector B On A.EmpCode=B.InspectorCode ");
            SQLDtl2.AppendLine("Where A.DocNo=@DocNo ");
            SQLDtl2.AppendLine("Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQLDtl2.ToString(),
                new string[] { 
                    //0
                    "DNo",

                    //1-2
                    "EmpCode", "InspectorName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row2) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row2, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row2, 1, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row2, 2, 2);
                }, false, false, true, false
            );
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mNumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 12) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedInspector()
        {
            var SQLInspector = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row2 = 0; Row2 < Grd2.Rows.Count - 1; Row2++)
                    if (Sm.GetGrdStr(Grd2, Row2, 1).Length != 0)
                        SQLInspector +=
                            "##" +
                            Sm.GetGrdStr(Grd2, Row2, 1) +
                            "##";
            }
            return (SQLInspector.Length == 0 ? "##XXX##" : SQLInspector);
        }

        public static void SetLueStatus(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT OptCode AS Col1, OptDesc AS Col2");
            SQL.AppendLine("FROM TblOption WHERE OptCat='QCStatus' ");
            SQL.AppendLine("ORDER BY OptCode;");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Button Events

        private void BtnQCPlanningDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMaterialQCInspectionDlg(this));
        }

        private void BtnQCPlanningDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtQCPlanningDocNo, "Planning document#", false))
            {
                try
                {
                    var f = new FrmQCPlanning(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtQCPlanningDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        #endregion

        #region Misc Control Event

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LueStatus_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStatus_Leave(object sender, EventArgs e)
        {
            if (LueStatus.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (Sm.GetLue(LueStatus).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 5].Value =
                    Grd1.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueStatus);
                    Grd1.Cells[fCell.RowIndex, 6].Value = LueStatus.GetColumnValue("Col2");
                }
                LueStatus.Visible = false;
            }
        }

        #endregion

        #endregion

        #region Class

        private class MaterialQC
        {
           
            public string CompanyLogo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string RecvDt { get; set; }
            public string PODocNo { get; set; }
            public string ProjectName { get; set; }
            public string VdName { get; set; }

        }

        private class MaterialQCDtl
        {
            public int No { get; set; }
            public string ItCodeInternal { get; set; }
            public decimal Qty { get; set; }
            public string ItName { get; set; }
            public string InventoryUomCode { get; set; }
            public string Specification { get; set; }
            public string Remark { get; set; }
            public decimal QCPassedCount { get; set; }
            public decimal QCHoldCount { get; set; }
            public decimal QCRejectedCount { get; set; }

        }
        #endregion

    }
}
