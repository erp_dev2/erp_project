﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmWODlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmWO mFrmParent;
        private string mSQL = string.Empty, mWORDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmWODlg2(FrmWO FrmParent, string WORDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWORDocNo = WORDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "",
                        "DNo",
                        "Activity",
                        "Breakdown"+Environment.NewLine+"Date",
                        "Breakdown"+Environment.NewLine+"Start",
                        
                        //6
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 0, 250, 100, 100, 
                        
                        //6
                        200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatTime(Grd1, new int[] { 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            mSQL = 
                "Select DNo, Activity, BDDate, BDStart, Remark From TblWORDtl " +
                "Where DocNo=@DocNo And WODocNo Is Null ";
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mWORDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtActivity.Text, "Activity", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DNo Desc;",
                        new string[] 
                        { 
                             //0
                             "DNo", 
                             
                             //1-4
                             "Activity", "BDDate", "BDStart", "Remark"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;
                        if (!IsDataAlreadyChosen(Row))
                        {
                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;

                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 0, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 6);

                            mFrmParent.Grd1.Rows.Add();
                        }
                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 3);
                        mFrmParent.Grd2.Rows.Add();
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 WO request.");
        }

        private bool IsDataAlreadyChosen(int r)
        {
            string DNo = Sm.GetGrdStr(Grd1, r, 2);
            for (int i = 0; i < mFrmParent.Grd1.Rows.Count - 1; i++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, i, 0), DNo)) return true;
            return false;
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtActivity_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkActivity_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Activity");
        }

        #endregion

        #endregion
    }
}
