﻿#region Update
// 21/03/2018 [HAR] tambah karakter untuk tax group name
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using FastReport;

using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTaxGrp : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmTaxGrpFind FrmFind;

        #endregion

        #region Constructor

        public FrmTaxGrp(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtTaxGrpCode, TxtTaxGrpName }, true);
                    BtnAcNo1.Enabled = false;
                    BtnAcNo2.Enabled = false;
                    TxtTaxGrpCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtTaxGrpCode, TxtTaxGrpName }, false);
                    BtnAcNo1.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    TxtTaxGrpCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtTaxGrpName }, false);
                    BtnAcNo1.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    TxtTaxGrpName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            { 
                TxtTaxGrpCode, TxtTaxGrpName, TxtAcNo1, TxtAcDesc1, TxtAcNo2, 
                TxtAcDesc2 
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTaxGrpFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTaxGrpCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTaxGrpCode, string.Empty, false) ||
                Sm.StdMsgYN("Delete", string.Empty) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblTaxGrp Where TaxGrpCode=@TaxGrpCode;" };
                Sm.CmParam<String>(ref cm, "@TaxGrpCode", TxtTaxGrpCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblTaxGrp(TaxGrpCode, TaxGrpName, AcNo1, AcNo2, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@TaxGrpCode, @TaxGrpName, @AcNo1, @AcNo2, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update TaxGrpName=@TaxGrpName, AcNo1=@AcNo1, AcNo2=@AcNo2, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@TaxGrpCode", TxtTaxGrpCode.Text);
                Sm.CmParam<String>(ref cm, "@TaxGrpName", TxtTaxGrpName.Text);
                Sm.CmParam<String>(ref cm, "@AcNo1", TxtAcNo1.Text);
                Sm.CmParam<String>(ref cm, "@AcNo2", TxtAcNo2.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtTaxGrpCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string TaxGrpCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                Sm.CmParam<String>(ref cm, "@TaxGrpCode", TaxGrpCode);

                SQL.AppendLine("Select A.TaxGrpCode, A.TaxGrpName, A.AcNo1, A.AcNo2, B.AcDesc As AcDesc1, C.AcDesc As AcDesc2 ");
                SQL.AppendLine("From TblTaxGrp A ");
                SQL.AppendLine("Left Join TblCOA B On A.AcNo1=B.AcNo ");
                SQL.AppendLine("Left Join TblCOA C On A.AcNo2=C.AcNo ");
                SQL.AppendLine("Where A.TaxGrpCode=@TaxGrpCode;");

                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                        new string[]
                        { 
                            "TaxGrpCode", 
                            "TaxGrpName", "AcNo1", "AcNo2", "AcDesc1", "AcDesc2" 
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtTaxGrpCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtTaxGrpName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtAcNo1.EditValue = Sm.DrStr(dr, c[2]);
                            TxtAcNo2.EditValue = Sm.DrStr(dr, c[3]);
                            TxtAcDesc1.EditValue = Sm.DrStr(dr, c[4]);
                            TxtAcDesc2.EditValue = Sm.DrStr(dr, c[5]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTaxGrpCode, "Code", false) ||
                Sm.IsTxtEmpty(TxtTaxGrpName, "Name", false) ||
                IsTaxGrpCodeExisted();
        }

        private bool IsTaxGrpCodeExisted()
        {
            if (!TxtTaxGrpCode.Properties.ReadOnly && 
                Sm.IsDataExist("Select 1 From TblTaxGrp Where TaxGrpCode=@Param;", TxtTaxGrpCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Tax group code ( " + TxtTaxGrpCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtTaxGrpCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTaxGrpCode);
        }

        private void TxtTaxGrpName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTaxGrpName);
        }

        #endregion

        #region Button Event

        private void BtnAcNo1_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTaxGrpDlg(this, 1));
        }

        private void BtnAcNo2_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTaxGrpDlg(this, 2));
        }

        #endregion

        #endregion

    }
}
