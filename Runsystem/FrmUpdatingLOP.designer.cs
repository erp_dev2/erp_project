﻿namespace RunSystem
{
    partial class FrmUpdatingLOP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUpdatingLOP));
            this.panel3 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtCtName = new DevExpress.XtraEditors.TextEdit();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtQtLetterNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtPIC = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtEstimated = new DevExpress.XtraEditors.TextEdit();
            this.TxtType = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtCityName = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.BtnLOPDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLOPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnLOPDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.BtnBOM = new System.Windows.Forms.Button();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.LuePhaseCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TcULOP = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtLetterNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstimated.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCityName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePhaseCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcULOP)).BeginInit();
            this.TcULOP.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(836, 0);
            this.panel1.Size = new System.Drawing.Size(70, 499);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcULOP);
            this.panel2.Controls.Add(this.Grd1);
            this.panel2.Controls.Add(this.BtnBOM);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(836, 499);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.TxtCtName);
            this.panel3.Controls.Add(this.TxtProjectName);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.BtnLOPDocNo2);
            this.panel3.Controls.Add(this.TxtLOPDocNo);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.BtnLOPDocNo);
            this.panel3.Controls.Add(this.MeeCancelReason);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(836, 140);
            this.panel3.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(82, 112);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 14);
            this.label15.TabIndex = 24;
            this.label15.Text = "Customer";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCtName
            // 
            this.TxtCtName.EnterMoveNextControl = true;
            this.TxtCtName.Location = new System.Drawing.Point(149, 110);
            this.TxtCtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtName.Name = "TxtCtName";
            this.TxtCtName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtCtName.Properties.MaxLength = 16;
            this.TxtCtName.Properties.ReadOnly = true;
            this.TxtCtName.Size = new System.Drawing.Size(304, 20);
            this.TxtCtName.TabIndex = 25;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(149, 89);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 100;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(304, 20);
            this.TxtProjectName.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(60, 92);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 14);
            this.label14.TabIndex = 21;
            this.label14.Text = "Project Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.TxtQtLetterNo);
            this.panel4.Controls.Add(this.TxtPIC);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.TxtEstimated);
            this.panel4.Controls.Add(this.TxtType);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.TxtCityName);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label102);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(469, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(367, 140);
            this.panel4.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(4, 114);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 14);
            this.label3.TabIndex = 56;
            this.label3.Text = "Quotation Letter#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQtLetterNo
            // 
            this.TxtQtLetterNo.EnterMoveNextControl = true;
            this.TxtQtLetterNo.Location = new System.Drawing.Point(116, 111);
            this.TxtQtLetterNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtLetterNo.Name = "TxtQtLetterNo";
            this.TxtQtLetterNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQtLetterNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtLetterNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtLetterNo.Properties.Appearance.Options.UseFont = true;
            this.TxtQtLetterNo.Properties.MaxLength = 16;
            this.TxtQtLetterNo.Properties.ReadOnly = true;
            this.TxtQtLetterNo.Size = new System.Drawing.Size(109, 20);
            this.TxtQtLetterNo.TabIndex = 57;
            // 
            // TxtPIC
            // 
            this.TxtPIC.EnterMoveNextControl = true;
            this.TxtPIC.Location = new System.Drawing.Point(116, 47);
            this.TxtPIC.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPIC.Name = "TxtPIC";
            this.TxtPIC.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPIC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPIC.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPIC.Properties.Appearance.Options.UseFont = true;
            this.TxtPIC.Properties.MaxLength = 16;
            this.TxtPIC.Properties.ReadOnly = true;
            this.TxtPIC.Size = new System.Drawing.Size(244, 20);
            this.TxtPIC.TabIndex = 55;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(78, 6);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 14);
            this.label16.TabIndex = 26;
            this.label16.Text = "Type";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEstimated
            // 
            this.TxtEstimated.EnterMoveNextControl = true;
            this.TxtEstimated.Location = new System.Drawing.Point(116, 69);
            this.TxtEstimated.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEstimated.Name = "TxtEstimated";
            this.TxtEstimated.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEstimated.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEstimated.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEstimated.Properties.Appearance.Options.UseFont = true;
            this.TxtEstimated.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEstimated.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEstimated.Properties.MaxLength = 18;
            this.TxtEstimated.Size = new System.Drawing.Size(198, 20);
            this.TxtEstimated.TabIndex = 54;
            // 
            // TxtType
            // 
            this.TxtType.EnterMoveNextControl = true;
            this.TxtType.Location = new System.Drawing.Point(116, 4);
            this.TxtType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtType.Name = "TxtType";
            this.TxtType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtType.Properties.Appearance.Options.UseFont = true;
            this.TxtType.Properties.MaxLength = 16;
            this.TxtType.Properties.ReadOnly = true;
            this.TxtType.Size = new System.Drawing.Size(244, 20);
            this.TxtType.TabIndex = 27;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(18, 72);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 14);
            this.label9.TabIndex = 53;
            this.label9.Text = "Estimated Value";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(116, 90);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 500;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(244, 20);
            this.MeeRemark.TabIndex = 52;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(66, 94);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 14);
            this.label10.TabIndex = 51;
            this.label10.Text = "Remark";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCityName
            // 
            this.TxtCityName.EnterMoveNextControl = true;
            this.TxtCityName.Location = new System.Drawing.Point(116, 25);
            this.TxtCityName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCityName.Name = "TxtCityName";
            this.TxtCityName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCityName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCityName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCityName.Properties.Appearance.Options.UseFont = true;
            this.TxtCityName.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCityName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCityName.Size = new System.Drawing.Size(109, 20);
            this.TxtCityName.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(88, 51);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 14);
            this.label4.TabIndex = 30;
            this.label4.Text = "PIC";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(86, 29);
            this.label102.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(27, 14);
            this.label102.TabIndex = 28;
            this.label102.Text = "City";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnLOPDocNo2
            // 
            this.BtnLOPDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLOPDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLOPDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLOPDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLOPDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnLOPDocNo2.Appearance.Options.UseFont = true;
            this.BtnLOPDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnLOPDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnLOPDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLOPDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLOPDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnLOPDocNo2.Image")));
            this.BtnLOPDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLOPDocNo2.Location = new System.Drawing.Point(429, 68);
            this.BtnLOPDocNo2.Name = "BtnLOPDocNo2";
            this.BtnLOPDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnLOPDocNo2.TabIndex = 20;
            this.BtnLOPDocNo2.ToolTip = "Show List Of Project";
            this.BtnLOPDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLOPDocNo2.ToolTipTitle = "Run System";
            this.BtnLOPDocNo2.Click += new System.EventHandler(this.BtnLOPDocNo2_Click);
            // 
            // TxtLOPDocNo
            // 
            this.TxtLOPDocNo.EnterMoveNextControl = true;
            this.TxtLOPDocNo.Location = new System.Drawing.Point(149, 68);
            this.TxtLOPDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLOPDocNo.Name = "TxtLOPDocNo";
            this.TxtLOPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtLOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLOPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLOPDocNo.Properties.MaxLength = 16;
            this.TxtLOPDocNo.Properties.ReadOnly = true;
            this.TxtLOPDocNo.Size = new System.Drawing.Size(244, 20);
            this.TxtLOPDocNo.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(56, 71);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 14);
            this.label8.TabIndex = 17;
            this.label8.Text = "List Of Project";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnLOPDocNo
            // 
            this.BtnLOPDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLOPDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLOPDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLOPDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLOPDocNo.Appearance.Options.UseBackColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseFont = true;
            this.BtnLOPDocNo.Appearance.Options.UseForeColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnLOPDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLOPDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLOPDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnLOPDocNo.Image")));
            this.BtnLOPDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLOPDocNo.Location = new System.Drawing.Point(398, 68);
            this.BtnLOPDocNo.Name = "BtnLOPDocNo";
            this.BtnLOPDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnLOPDocNo.TabIndex = 19;
            this.BtnLOPDocNo.ToolTip = "Find List Of Project";
            this.BtnLOPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLOPDocNo.ToolTipTitle = "Run System";
            this.BtnLOPDocNo.Click += new System.EventHandler(this.BtnLOPDocNo_Click);
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(149, 47);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 500;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(244, 20);
            this.MeeCancelReason.TabIndex = 15;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(395, 45);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(67, 22);
            this.ChkCancelInd.TabIndex = 16;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 51);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 14);
            this.label13.TabIndex = 14;
            this.label13.Text = "Reason For Cancellation";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(149, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 50;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(304, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(68, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(108, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(149, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // BtnBOM
            // 
            this.BtnBOM.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnBOM.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnBOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBOM.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBOM.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnBOM.Location = new System.Drawing.Point(0, 140);
            this.BtnBOM.Name = "BtnBOM";
            this.BtnBOM.Size = new System.Drawing.Size(836, 27);
            this.BtnBOM.TabIndex = 36;
            this.BtnBOM.Text = "Historical List Of Project";
            this.BtnBOM.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnBOM.UseVisualStyleBackColor = false;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 167);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(836, 150);
            this.Grd1.TabIndex = 54;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(830, 154);
            this.Grd2.TabIndex = 56;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // LuePhaseCode
            // 
            this.LuePhaseCode.EnterMoveNextControl = true;
            this.LuePhaseCode.Location = new System.Drawing.Point(84, 25);
            this.LuePhaseCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePhaseCode.Name = "LuePhaseCode";
            this.LuePhaseCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePhaseCode.Properties.Appearance.Options.UseFont = true;
            this.LuePhaseCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePhaseCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePhaseCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePhaseCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePhaseCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePhaseCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePhaseCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePhaseCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePhaseCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePhaseCode.Properties.DropDownRows = 20;
            this.LuePhaseCode.Properties.NullText = "[Empty]";
            this.LuePhaseCode.Properties.PopupWidth = 500;
            this.LuePhaseCode.Size = new System.Drawing.Size(171, 20);
            this.LuePhaseCode.TabIndex = 57;
            this.LuePhaseCode.ToolTip = "F4 : Show/hide list";
            this.LuePhaseCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePhaseCode.EditValueChanged += new System.EventHandler(this.LuePhaseCode_EditValueChanged);
            this.LuePhaseCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePhaseCode_KeyDown);
            this.LuePhaseCode.Leave += new System.EventHandler(this.LuePhaseCode_Leave);
            // 
            // TcULOP
            // 
            this.TcULOP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcULOP.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcULOP.Location = new System.Drawing.Point(0, 317);
            this.TcULOP.Name = "TcULOP";
            this.TcULOP.SelectedTabPage = this.Tp1;
            this.TcULOP.Size = new System.Drawing.Size(836, 182);
            this.TcULOP.TabIndex = 58;
            this.TcULOP.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.LuePhaseCode);
            this.Tp1.Controls.Add(this.Grd2);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(830, 154);
            this.Tp1.Text = "List of Project Delivery";
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.Grd3);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(830, 154);
            this.Tp2.Text = "UploadFile";
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(830, 154);
            this.Grd3.TabIndex = 30;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd3_RequestCellToolTipText);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmUpdatingLOP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 499);
            this.Name = "FrmUpdatingLOP";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtLetterNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstimated.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCityName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePhaseCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcULOP)).EndInit();
            this.TcULOP.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtType;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtCtName;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.TextEdit TxtCityName;
        private System.Windows.Forms.Label label102;
        public DevExpress.XtraEditors.SimpleButton BtnLOPDocNo2;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtLOPDocNo;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnLOPDocNo;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Button BtnBOM;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.TextEdit TxtEstimated;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtPIC;
        private DevExpress.XtraEditors.LookUpEdit LuePhaseCode;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtQtLetterNo;
        private DevExpress.XtraTab.XtraTabControl TcULOP;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
    }
}