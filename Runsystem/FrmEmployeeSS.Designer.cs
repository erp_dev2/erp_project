﻿namespace RunSystem
{
    partial class FrmEmployeeSS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.LueEmployeeSSProblem = new DevExpress.XtraEditors.LookUpEdit();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.DteEndDt2 = new DevExpress.XtraEditors.DateEdit();
            this.LueEmployeeFamilySSProblem = new DevExpress.XtraEditors.LookUpEdit();
            this.DteBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.DteStartDt2 = new DevExpress.XtraEditors.DateEdit();
            this.LueSSCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtDeptCode = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPosCode = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtEmpCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmployeeSSProblem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmployeeFamilySSProblem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSSCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Size = new System.Drawing.Size(70, 473);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.xtraTabControl1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 117);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(772, 356);
            this.xtraTabControl1.TabIndex = 36;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.Header.Options.UseTextOptions = true;
            this.xtraTabPage1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.xtraTabPage1.Controls.Add(this.DteEndDt);
            this.xtraTabPage1.Controls.Add(this.LueEmployeeSSProblem);
            this.xtraTabPage1.Controls.Add(this.DteStartDt);
            this.xtraTabPage1.Controls.Add(this.Grd1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(766, 328);
            this.xtraTabPage1.Text = "Employee\'s Social Security";
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(307, 21);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(125, 20);
            this.DteEndDt.TabIndex = 38;
            this.DteEndDt.Leave += new System.EventHandler(this.DteEndDt_Leave);
            this.DteEndDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteEndDt_KeyDown);
            // 
            // LueEmployeeSSProblem
            // 
            this.LueEmployeeSSProblem.EnterMoveNextControl = true;
            this.LueEmployeeSSProblem.Location = new System.Drawing.Point(454, 21);
            this.LueEmployeeSSProblem.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmployeeSSProblem.Name = "LueEmployeeSSProblem";
            this.LueEmployeeSSProblem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmployeeSSProblem.Properties.Appearance.Options.UseFont = true;
            this.LueEmployeeSSProblem.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmployeeSSProblem.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmployeeSSProblem.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmployeeSSProblem.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmployeeSSProblem.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmployeeSSProblem.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmployeeSSProblem.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmployeeSSProblem.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmployeeSSProblem.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmployeeSSProblem.Properties.DropDownRows = 25;
            this.LueEmployeeSSProblem.Properties.NullText = "[Empty]";
            this.LueEmployeeSSProblem.Properties.PopupWidth = 220;
            this.LueEmployeeSSProblem.Size = new System.Drawing.Size(220, 20);
            this.LueEmployeeSSProblem.TabIndex = 39;
            this.LueEmployeeSSProblem.ToolTip = "F4 : Show/hide list";
            this.LueEmployeeSSProblem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmployeeSSProblem.EditValueChanged += new System.EventHandler(this.LueEmployeeSSProblem_EditValueChanged);
            this.LueEmployeeSSProblem.Leave += new System.EventHandler(this.LueEmployeeSSProblem_Leave);
            this.LueEmployeeSSProblem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueEmployeeSSProblem_KeyDown);
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(159, 21);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(125, 20);
            this.DteStartDt.TabIndex = 37;
            this.DteStartDt.Leave += new System.EventHandler(this.DteStartDt_Leave);
            this.DteStartDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteStartDt_KeyDown);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 328);
            this.Grd1.TabIndex = 40;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd1_ColHdrDoubleClick);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.Header.Options.UseTextOptions = true;
            this.xtraTabPage2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.xtraTabPage2.Controls.Add(this.DteEndDt2);
            this.xtraTabPage2.Controls.Add(this.LueEmployeeFamilySSProblem);
            this.xtraTabPage2.Controls.Add(this.DteBirthDt);
            this.xtraTabPage2.Controls.Add(this.DteStartDt2);
            this.xtraTabPage2.Controls.Add(this.LueSSCode);
            this.xtraTabPage2.Controls.Add(this.Grd2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(766, 88);
            this.xtraTabPage2.Text = "Family\'s Social Security";
            // 
            // DteEndDt2
            // 
            this.DteEndDt2.EditValue = null;
            this.DteEndDt2.EnterMoveNextControl = true;
            this.DteEndDt2.Location = new System.Drawing.Point(303, 22);
            this.DteEndDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt2.Name = "DteEndDt2";
            this.DteEndDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt2.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt2.Size = new System.Drawing.Size(125, 20);
            this.DteEndDt2.TabIndex = 39;
            this.DteEndDt2.Leave += new System.EventHandler(this.DteEndDt2_Leave);
            this.DteEndDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteEndDt2_KeyDown);
            // 
            // LueEmployeeFamilySSProblem
            // 
            this.LueEmployeeFamilySSProblem.EnterMoveNextControl = true;
            this.LueEmployeeFamilySSProblem.Location = new System.Drawing.Point(558, 22);
            this.LueEmployeeFamilySSProblem.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmployeeFamilySSProblem.Name = "LueEmployeeFamilySSProblem";
            this.LueEmployeeFamilySSProblem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmployeeFamilySSProblem.Properties.Appearance.Options.UseFont = true;
            this.LueEmployeeFamilySSProblem.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmployeeFamilySSProblem.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmployeeFamilySSProblem.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmployeeFamilySSProblem.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmployeeFamilySSProblem.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmployeeFamilySSProblem.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmployeeFamilySSProblem.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmployeeFamilySSProblem.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmployeeFamilySSProblem.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmployeeFamilySSProblem.Properties.DropDownRows = 25;
            this.LueEmployeeFamilySSProblem.Properties.NullText = "[Empty]";
            this.LueEmployeeFamilySSProblem.Properties.PopupWidth = 220;
            this.LueEmployeeFamilySSProblem.Size = new System.Drawing.Size(187, 20);
            this.LueEmployeeFamilySSProblem.TabIndex = 41;
            this.LueEmployeeFamilySSProblem.ToolTip = "F4 : Show/hide list";
            this.LueEmployeeFamilySSProblem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmployeeFamilySSProblem.EditValueChanged += new System.EventHandler(this.LueEmployeeFamilySSProblem_EditValueChanged);
            this.LueEmployeeFamilySSProblem.Leave += new System.EventHandler(this.LueEmployeeFamilySSProblem_Leave);
            this.LueEmployeeFamilySSProblem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueEmployeeFamilySSProblem_KeyDown);
            // 
            // DteBirthDt
            // 
            this.DteBirthDt.EditValue = null;
            this.DteBirthDt.EnterMoveNextControl = true;
            this.DteBirthDt.Location = new System.Drawing.Point(429, 22);
            this.DteBirthDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBirthDt.Name = "DteBirthDt";
            this.DteBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBirthDt.Size = new System.Drawing.Size(125, 20);
            this.DteBirthDt.TabIndex = 40;
            this.DteBirthDt.Leave += new System.EventHandler(this.DteBirthDt_Leave);
            this.DteBirthDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteBirthDt_KeyDown);
            // 
            // DteStartDt2
            // 
            this.DteStartDt2.EditValue = null;
            this.DteStartDt2.EnterMoveNextControl = true;
            this.DteStartDt2.Location = new System.Drawing.Point(173, 22);
            this.DteStartDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt2.Name = "DteStartDt2";
            this.DteStartDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt2.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt2.Size = new System.Drawing.Size(125, 20);
            this.DteStartDt2.TabIndex = 38;
            this.DteStartDt2.Leave += new System.EventHandler(this.DteStartDt2_Leave);
            this.DteStartDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteStartDt2_KeyDown);
            // 
            // LueSSCode
            // 
            this.LueSSCode.EnterMoveNextControl = true;
            this.LueSSCode.Location = new System.Drawing.Point(58, 22);
            this.LueSSCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSSCode.Name = "LueSSCode";
            this.LueSSCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSCode.Properties.Appearance.Options.UseFont = true;
            this.LueSSCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSSCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSSCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSSCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSSCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSSCode.Properties.DropDownRows = 20;
            this.LueSSCode.Properties.NullText = "[Empty]";
            this.LueSSCode.Properties.PopupWidth = 500;
            this.LueSSCode.Size = new System.Drawing.Size(110, 20);
            this.LueSSCode.TabIndex = 37;
            this.LueSSCode.ToolTip = "F4 : Show/hide list";
            this.LueSSCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSSCode.EditValueChanged += new System.EventHandler(this.LueSSCode_EditValueChanged);
            this.LueSSCode.Leave += new System.EventHandler(this.LueSSCode_Leave);
            this.LueSSCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueSSCode_KeyDown);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 88);
            this.Grd2.TabIndex = 42;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd2_ColHdrDoubleClick);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtDeptCode);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.TxtPosCode);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.TxtEmpCodeOld);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.TxtEmpName);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.TxtEmpCode);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 117);
            this.panel3.TabIndex = 28;
            // 
            // TxtDeptCode
            // 
            this.TxtDeptCode.EnterMoveNextControl = true;
            this.TxtDeptCode.Location = new System.Drawing.Point(126, 90);
            this.TxtDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptCode.Name = "TxtDeptCode";
            this.TxtDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptCode.Properties.MaxLength = 40;
            this.TxtDeptCode.Properties.ReadOnly = true;
            this.TxtDeptCode.Size = new System.Drawing.Size(455, 20);
            this.TxtDeptCode.TabIndex = 35;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(50, 93);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 14);
            this.label9.TabIndex = 34;
            this.label9.Text = "Department";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPosCode
            // 
            this.TxtPosCode.EnterMoveNextControl = true;
            this.TxtPosCode.Location = new System.Drawing.Point(126, 69);
            this.TxtPosCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosCode.Name = "TxtPosCode";
            this.TxtPosCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPosCode.Properties.MaxLength = 40;
            this.TxtPosCode.Properties.ReadOnly = true;
            this.TxtPosCode.Size = new System.Drawing.Size(455, 20);
            this.TxtPosCode.TabIndex = 33;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(74, 72);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 14);
            this.label8.TabIndex = 32;
            this.label8.Text = "Position";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCodeOld
            // 
            this.TxtEmpCodeOld.EnterMoveNextControl = true;
            this.TxtEmpCodeOld.Location = new System.Drawing.Point(126, 48);
            this.TxtEmpCodeOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCodeOld.Name = "TxtEmpCodeOld";
            this.TxtEmpCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCodeOld.Properties.MaxLength = 50;
            this.TxtEmpCodeOld.Properties.ReadOnly = true;
            this.TxtEmpCodeOld.Size = new System.Drawing.Size(328, 20);
            this.TxtEmpCodeOld.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(1, 51);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 14);
            this.label6.TabIndex = 30;
            this.label6.Text = "Employee\'s Old Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(126, 27);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 40;
            this.TxtEmpName.Properties.ReadOnly = true;
            this.TxtEmpName.Size = new System.Drawing.Size(455, 20);
            this.TxtEmpName.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(20, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 14);
            this.label1.TabIndex = 28;
            this.label1.Text = "Employee\'s Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(126, 6);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 50;
            this.TxtEmpCode.Properties.ReadOnly = true;
            this.TxtEmpCode.Size = new System.Drawing.Size(328, 20);
            this.TxtEmpCode.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(23, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 14);
            this.label3.TabIndex = 25;
            this.label3.Text = "Employee\'s Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 431);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 10;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 451);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 11;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // FrmEmployeeSS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmEmployeeSS";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmployeeSSProblem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmployeeFamilySSProblem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSSCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.TextEdit TxtDeptCode;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtPosCode;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCodeOld;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueSSCode;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        internal DevExpress.XtraEditors.DateEdit DteStartDt2;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        internal DevExpress.XtraEditors.DateEdit DteBirthDt;
        private DevExpress.XtraEditors.LookUpEdit LueEmployeeSSProblem;
        private DevExpress.XtraEditors.LookUpEdit LueEmployeeFamilySSProblem;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        internal DevExpress.XtraEditors.DateEdit DteEndDt2;
    }
}