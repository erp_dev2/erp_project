﻿#region Update
/*
    26/07/2018 [HAR] bug update status LOP
    07/08/2018 [WED] ubah logic perubahan status LOP (Progress --> Finished) dengan menambah SettledInd. Status LOP yang berubah berdasarkan Target, dihilangkan
    14/08/2018 [WED] TblLOPHdr.Status --> TblLOPHdr.ProcessInd]
    08/11/2018 [HAR] penambhaan startdate dan enddate di LOP kena di sini juga (tambah end date)
    29/01/2019 [HAR] ceklist di dokumen sebelumnya tidak muncul saat pembuatan dokumen updating baru
    06/05/2020 [IBL/IMS] Tambah informasi Quotation Letter#
    22/09/2022 [VIN/VIR] BUG: city left join krn tidak mandatory
    26/12/2022 [ICA/MNET] Field project name masih bisa di edit selama LOP masih belum settled semua berdasarkan parameter IsProjectNameEditable
    01/02/2023 [RDA/MNET] field project name ketika insert bisa diedit berdasarkan param IsProjectNameEditable
    07/02/2023 [RDA/MNET] field project name tersimpan ke table UpdatingLOPHdr + show + find
    02/03/2023 [WED/MNET] bug fix show data belum dipagerin parameter IsLOPShowPICSales
    04/04/2023 [RDA/MNET] tambah fitur upload file based on mIsUpdatingLOPAllowToUploadFile
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using System.Net;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmUpdatingLOP : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "";
        internal FrmUpdatingLOPFind FrmFind;
        internal bool mIsLOPShowPICSales = false, mIsFilterBySite = false;
        internal bool mIsProjectNameEditable = false;

        private bool mIsUpdatingLOPAllowToUploadFile = false;
        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty;
        private byte[] downloadedData;
        private string mStateIndicator = string.Empty;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmUpdatingLOP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLuePhaseCode(ref LuePhaseCode);
                LuePhaseCode.Visible = false;
                if (!mIsUpdatingLOPAllowToUploadFile) TcULOP.TabPages.Remove(Tp2);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    this.Text = "Updating List Of Project";
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Updating project#",

                        //1-3
                        "",
                        "Date",
                        "Remark",
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-3
                        20, 120, 300
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3 });

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 14;
            Grd2.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Project Document",
                        "Project Dno",
                        "Route",
                        "Previous Target",
                        "Target",
                        
                        //6-10
                        "LOP target",
                        "Start Date",
                        "End Date",
                        "Phase Code",
                        "Phase",

                        //11-13
                        "Settled",
                        "Remark",
                        "PrevSettledInd"
                    },
                    new int[] 
                    {
                        //0
                        10,
 
                        //1-5
                        180, 80, 120, 120, 120,  

                        //6-10
                        100, 120, 120, 150, 200,  
                        
                        //11-13
                        60, 200, 0
                    }
                );
            Sm.GrdColCheck(Grd2, new int[] { 11 });
            Sm.GrdFormatDate(Grd2, new int[] { 7, 8 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 9 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 7, 8,  9, 10, 13 });
            Sm.GrdFormatDec(Grd2, new int[] { 4, 5, 6 }, 0 );
            #endregion

            #region Grid Upload File

            if (mIsUpdatingLOPAllowToUploadFile)
            {
                Grd3.Cols.Count = 7;
                Grd3.FrozenArea.ColCount = 3;

                Sm.GrdHdrWithColWidth(
                        Grd3,
                        new string[]
                        {
                        //0
                        "D No",
                        //1-5
                        "File Name",
                        "",
                        "D",
                        "Upload By",
                        "Date",

                        //6
                        "Time"
                        },
                         new int[]
                        {
                        0,
                        250, 20, 20, 100, 100,
                        100
                        }
                    );

                Sm.GrdColInvisible(Grd3, new int[] { 0 }, false);
                Sm.GrdFormatDate(Grd3, new int[] { 5 });
                Sm.GrdFormatTime(Grd3, new int[] { 6 });
                Sm.GrdColButton(Grd3, new int[] { 2, 3 });
                Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                Grd3.Cols[2].Move(1);
            }

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtLOPDocNo,
                       TxtProjectName, TxtCtName, TxtType, TxtCityName, TxtPIC, TxtEstimated, MeeRemark, TxtQtLetterNo
                    }, true);
                    BtnLOPDocNo.Enabled = false;
                    ChkCancelInd.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
                    if (mIsUpdatingLOPAllowToUploadFile)
                    {
                        Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 5, 6 });

                        for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                        {
                            //if (Sm.GetGrdStr(Grd3, Row, 4).Length == 0)
                            //{
                            //    Grd3.Cells[Row, 2].ReadOnly = iGBool.False;
                            //    Grd3.Cells[Row, 3].ReadOnly = iGBool.True;
                            //}
                            //else
                            //{
                                Grd3.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd3.Cells[Row, 3].ReadOnly = iGBool.False;
                            //}
                        }
                    }

                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark, }, false);
                    if (mIsProjectNameEditable) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtProjectName }, false);
                    BtnLOPDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 5, 10, 11, 12 });
                    if(mIsUpdatingLOPAllowToUploadFile) Sm.GrdColReadOnly(false, true, Grd3, new int[] { 2, 3 }); // for upload file
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    if(mIsProjectNameEditable && IsLOPNotSettled()) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtProjectName }, false);
                    ChkCancelInd.Focus();
                    ChkCancelInd.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtLOPDocNo,
                  TxtProjectName, TxtCtName, TxtType, TxtCityName, TxtPIC, MeeRemark,
                  TxtQtLetterNo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtEstimated
            }, 0);
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);

            mStateIndicator = string.Empty;
            if (mIsUpdatingLOPAllowToUploadFile)
            {
                Sm.ClearGrd(Grd3, true);
                Sm.FocusGrd(Grd3, 0, 0);
            }
        }

        #endregion

        #region Additional Method

        #region Upload File Method
        private MySqlCommand SaveUploadFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Customer's AR Downpayment - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblUpdatingLOPFile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string EmpCode, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateUploadFile(EmpCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsUpdatingLOPAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsUpdatingLOPAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsUpdatingLOPAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsUpdatingLOPAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsUpdatingLOPAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsUpdatingLOPAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != Sm.GetGrdStr(Grd3, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblUpdatingLOPFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateUploadFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblUpdatingLOPFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        #endregion

        private void SetLuePhaseCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select PhaseCode As Col1, PhaseName As Col2  ");
            SQL.AppendLine("From TblPhase ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By Col2 ");

            cm.CommandText = SQL.ToString();
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            //mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            //mIsProjectNameEditable = Sm.GetParameterBoo("IsProjectNameEditable");
            //mIsLOPShowPICSales = Sm.GetParameterBoo("IsLOPShowPICSales");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsFilterBySite', 'IsProjectNameEditable', 'IsLOPShowPICSales', ");
            SQL.AppendLine("'IsUpdatingLOPAllowToUploadFile', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient', 'FileSizeMaxUploadFTPClient' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsProjectNameEditable": mIsProjectNameEditable = ParValue == "Y"; break;
                            case "IsLOPShowPICSales": mIsLOPShowPICSales = ParValue == "Y"; break;
                            case "IsUpdatingLOPAllowToUploadFile": mIsUpdatingLOPAllowToUploadFile = ParValue == "Y"; break;

                            //string
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

        }

        private void LueRequestEdit(
        iGrid Grd,
        DevExpress.XtraEditors.LookUpEdit Lue,
        ref iGCell fCell,
        ref bool fAccept,
        TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmUpdatingLOPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                mStateIndicator = "I";
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mStateIndicator = "E";
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
         
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;
            string HMDocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "UpdatingLOP", "TblUpdatingLOPHdr");
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveUpdatingLOPHdr(DocNo));
            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0)
                {
                    cml.Add(SaveUpdatingLOPDtl(DocNo, r));
                }
                if (Sm.GetGrdBool(Grd2, r, 11))
                {
                    cml.Add(UpdateLOPStatus(r));
                    //cml.Add(UpdateLOPDtl(r, "1"));
                }
            }

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1") //mIsARDPAllowToUploadFile &&
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd3, Row, 1))) return;
                    }
                }
            }

            cml.Add(SaveUploadFile(DocNo));

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                {
                    if (mIsUpdatingLOPAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                    {
                        UploadFile(DocNo, Row, Sm.GetGrdStr(Grd3, Row, 1));
                    }
                }
            }

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtLOPDocNo, "List Of Project", false) ||
                IsGrdEmpty() ||
                (mIsUpdatingLOPAllowToUploadFile && IsUploadFileEmpty()) ||
                IsGrdValueNotValid() ||
                IsLOPAlreadyClosed()
                ;
        }

        private bool IsUploadFileEmpty()
        {
            if (mIsUpdatingLOPAllowToUploadFile && Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No files to upload, please upload at least one file.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 list of project phase.");
                return true;
            }
            return false;
        }

    
        private bool IsGrdValueNotValid()
        {
             for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 7, false, "Phase is empty.")) return true;
                }
            return false;
        }


        private bool IsLOPAlreadyClosed()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblLOPHdr Where DocNo=@Param And CancelInd='N' And ProcessInd = 'L' ;",
                TxtLOPDocNo.Text,
                "Document List Of Project already closed.");
        }


        private MySqlCommand SaveUpdatingLOPHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblUpdatingLOPHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, LOPDocNo, ProjectName, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', @LOPDocNo, @ProjectName, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProjectName", TxtProjectName.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand UpdateLOPDtl(int Row, string Status)
        //{
        //    var SQL = new StringBuilder();
            
        //    if(Status =="1")
        //    {
        //        SQL.AppendLine("Update TblLOPDtl  ");
        //        SQL.AppendLine("SEt Status = @Status, lastUpby = @CreateBy, lastUpDt = CurrentDateTime() ");
        //        SQL.AppendLine("Where DocNo=@LOPDocNo And Dno=@LOPDno  ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("Update tblLOPDtl A  ");
        //        SQL.AppendLine("left Join   ");
        //        SQL.AppendLine("(  ");
        //        SQL.AppendLine("    Select B.LopDocno, B.LOPDno, Max(B.target) As target   ");
        //        SQL.AppendLine("    from tblUpdatingLOpHdr A    ");
        //        SQL.AppendLine("    Inner Join tblUpdatingLOpdtl B on A.DocNO = B.DocNo   ");
        //        SQL.AppendLine("    Where A.CancelInd = 'N' And A.DocNo <> @DocNO And B.LOPDocNo = @LOPDocNo And B.LOPDno = @LOPDNo ");
        //        SQL.AppendLine("    group By B.LOPDocNo, B.LOPDno  ");
        //        SQL.AppendLine(")B On A.DocNo = B.LopDocno And A.Dno = B.LOPDno  ");
        //        SQL.AppendLine("SEt A.Status = @Status  ");
        //        SQL.AppendLine("Where A.target >= B.target  And A.DocNo = @LOPDocNo And A.Dno = @LOPDNo ; ");
              
        //    }

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@LOPDocNo", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@LOPDNo", Sm.GetGrdStr(Grd2, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@Status", Status=="1"?"F":"P");
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveUpdatingLOPDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblUpdatingLOPDtl(DocNo, DNo, LOPDocNo, LOPDno, Route, Target, Dt, Dt2, PhaseCode, SettledInd, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @LOPDocNo, @LOPDno, @Route, @Target, @Dt, @Dt2, @PhaseCode, @SettledInd, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@LOPDocNo", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@LOPDNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@Route", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Target", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetGrdDate(Grd2, Row, 7));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetGrdDate(Grd2, Row, 8));
            Sm.CmParam<String>(ref cm, "@PhaseCode", Sm.GetGrdStr(Grd2, Row, 9));
            Sm.CmParam<String>(ref cm, "@SettledInd", (Sm.GetGrdBool(Grd2, Row, 11)) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateLOPStatus(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblLOPDtl Set ");
            SQL.AppendLine("    Status = @Status, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @LOPDocNo And DNo = @LOPDNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@LOPDocNo", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@LOPDNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@Status", (ChkCancelInd.Checked) ? "P" : "F");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditUpdatingLOP());
            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdBool(Grd2, r, 11))
                {
                    cml.Add(UpdateLOPStatus(r));
                }
            }
           
            Sm.ExecCommands(cml);

            if (mIsUpdatingLOPAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (mIsUpdatingLOPAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd3, Row, 4).Length == 0)
                        {
                            if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd3, Row, 1))) return;
                        }
                    }
                }

                cml.Add(SaveUploadFile(TxtDocNo.Text));

                Sm.ExecCommands(cml);

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (mIsUpdatingLOPAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd3, Row, 4).Length == 0)
                        {
                            UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd3, Row, 1));
                        }
                    }
                }
            }

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||

                //((mIsUpdatingLOPAllowToUploadFile && TxtStatus.Text == "Approved") && Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") || !mIsARDPAllowToUploadFile && Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation")) ||
                //((mIsUpdatingLOPAllowToUploadFile && TxtStatus.Text == "Approved") && IsCancelIndNotTrue() || !mIsARDPAllowToUploadFile && IsCancelIndNotTrue()) ||


                IsUpdatingLOPAlreadyCancelled() 
                ;
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
           
            return false;
        }

        private bool IsUpdatingLOPAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblUpdatingLOPHdr Where DocNo=@Param And CancelInd='Y';",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private MySqlCommand EditUpdatingLOP()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblUpdatingLOPHdr Set ");
            SQL.AppendLine("ProjectName=@ProjectName, CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            if(mIsProjectNameEditable && !ChkCancelInd.Checked)
            {
                SQL.AppendLine("Update TblLOPHdr Set ");
                SQL.AppendLine("ProjectName=@ProjectName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@LOPDocNo; ");
            }
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked == true ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@ProjectName", TxtProjectName.Text);
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsLOPNotSettled()
        {
            bool IsNotSettled = false; 

            for(int row=0; row<Grd2.Rows.Count-1; row++)
            {
                bool cek = Sm.GetGrdBool(Grd2, row, 11);
                if (!Sm.GetGrdBool(Grd2, row, 11))
                {
                    IsNotSettled = true;
                    return true;
                }
            }

            return false;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowUpdatingLOPHdr(DocNo);
                ShowUpdatingLOPDtl(DocNo);
                ShowUpdatingLOPHistory(TxtLOPDocNo.Text);
                if(mIsUpdatingLOPAllowToUploadFile) ShowUploadFile(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowUpdatingLOPHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.LOPDocNo,  ");

            if (mIsProjectNameEditable)
                SQL.AppendLine("A.ProjectName, ");
            else
                SQL.AppendLine("B.ProjectName, ");

            if (mIsLOPShowPICSales) SQL.AppendLine("C.UserName As EmpName, ");
            else SQL.AppendLine("C.EmpName, ");

            SQL.AppendLine(" D.CtName, E.OptDesc As ProjectType, F.CityName, B.PICCode, B.EstValue, A.Remark ");

            SQL.AppendLine("From TblUpdatingLOPHdr A ");
            SQL.AppendLine("Inner Join TblLOPHdr B On A.LOPDocNo=B.DocNo ");
            
            if (mIsLOPShowPICSales) SQL.AppendLine("Inner Join TblUser C On B.PICCode = C.UserCode ");
            else SQL.AppendLine("Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
            
            SQL.AppendLine("Inner Join TblCustomer D On B.CtCode = D.CtCode ");
            SQL.AppendLine("Inner Join TblOption E on B.ProjectType = E.optCode And E.OptCat = 'ProjectType' ");
            SQL.AppendLine("Left Join tblCity F On B.CityCode = F.CityCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "LOPDocNo", "ProjectName",

                    //6-10
                    "CtName", "ProjectType", "CityName", "EmpName", "EstValue", 

                    //11-12
                    "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    TxtLOPDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[5]);
                    TxtCtName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtType.EditValue = Sm.DrStr(dr, c[7]);
                    TxtCityName.EditValue = Sm.DrStr(dr, c[8]);
                    TxtPIC.EditValue = Sm.DrStr(dr, c[9]);
                    TxtEstimated.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                }, true
            );
        }

        internal void ShowUpdatingLOPHistory(string LOPDocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@LOPDocNo", LOPDocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.Remark ");
            SQL.AppendLine("From TblUpdatingLOPHdr A ");
            SQL.AppendLine("Where A.LOPDocNo=@LOPDocNo ");
            SQL.AppendLine("And A.CancelInd='N';");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "DocNo", "DocDt", "Remark" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowUpdatingLOPDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.LOPDocNo, A.LOPDno, A.Route, A.Target, C.target As LOPtarget,  A.Dt, A.Dt2,  A.PhaseCode, B.PhaseName,  A.Remark, ");
            SQL.AppendLine("If(A.SettledInd='Y', 'Y', 'N') As SettledInd ");
            SQL.AppendLine("From TblUpdatingLOPDtl A ");
            SQL.AppendLine("Inner Join TblPhase B On A.phaseCode = B.PhaseCode");
            SQL.AppendLine("Inner Join TblLOPDtl C On A.LOpDocNO = C.DocnO And A.LOpDNO = C.DnO");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString() + "Order By A.Dno ;",
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "LOPDocNo", "LOPDno", "Route", "Target", "LOPtarget", 
                    //6-10
                    "Dt", "Dt2", "PhaseCode", "Phasename","Remark", 
                    //11
                    "SettledInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Grd2.Cells[Row, 4].Value = 0;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 13, 11);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ShowUpdatingLOPDtl2(string LOPDocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@LOPDocNo", LOPDocNo);


            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.DocNo, A.Dno, A.Route, ifnull(C.target, 0) As target, ");
            SQL.AppendLine("A.StartDt, A.EndDt, ");
            SQL.AppendLine("ifnull(C.phaseCode, A.PhaseCode) As PhaseCode, ifnull(C.PhaseName, B.Phasename) As Phasename, ");
            SQL.AppendLine("ifnull(C.Remark, A.Remark) As Remark, A.Target  As tgt, If(A.Status='F', 'Y', 'N') As SettledInd "); //IfNull(C.SettledInd, 'N') As SettledInd ");
            SQL.AppendLine("From TblLOPDtl A ");
            SQL.AppendLine("Inner Join TblPhase B On A.phaseCode = B.PhaseCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.LOPDocNo, B.LOPDNo, B.Target, B.PhaseCode, D.Phasename, B.Remark, B.SettledInd ");
            SQL.AppendLine("    from tblUpdatingLOPHdr A ");
            SQL.AppendLine("    Inner Join tblUpdatingLopDtl B On A.DocNo = b.DocNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select MAX(Concat(A.DocDt, A.DocNo)) AS Keyword ");
            SQL.AppendLine("        From tblUpdatingLOphdr A ");
            SQL.AppendLine("        Where A.cancelInd = 'N' And A.LOPDocNO = @LopDocNo  ");
            SQL.AppendLine("    ) C On Concat(A.DocDt, A.Docno) =  C.keyWord  ");
            SQL.AppendLine("    Inner Join TblPhase D On B.phaseCode = D.PhaseCode");
            SQL.AppendLine(")C On A.DocNo = C.LOPDocNo And A.Dno = C.LOPDNo ");
            SQL.AppendLine("Where A.DocNo=@LOPDocNo ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString() + " ;",
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "DocNo", "Dno", "Route", "Target",  "tgt",  
                    //6-10
                    "StartDt", "EndDt", "PhaseCode", "Phasename","Remark", 
                    //11
                    "SettledInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 13, 11);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
            for (int i = 0; i < Grd2.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 0).Length > 0)
                {
                    if (Sm.GetGrdBool(Grd2, i, 13))
                    {
                        Grd2.Cells[i, 11].ReadOnly = iGBool.True;
                    }
                }
            }
        }

        private void ShowUploadFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                   "select DNo, FileName, CreateBy, CreateDt from  TblUpdatingLOPFile Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName", "CreateBy", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd3, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd3, dr, c, Row, 6, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #endregion

        #region Event
        private void BtnLOPDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmUpdatingLOPDlg(this));
        }

        private void BtnLOPDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtLOPDocNo, "List Of Project#", false))
            {
                var f = new FrmLOP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtLOPDocNo.Text;
                f.ShowDialog();
            }
        }

        private void LuePhaseCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePhaseCode, new Sm.RefreshLue1(SetLuePhaseCode));
        }

        private void LuePhaseCode_Leave(object sender, EventArgs e)
        {
            if (LuePhaseCode.Visible && fAccept && fCell.ColIndex == 10)
            {
                if (Sm.GetLue(LuePhaseCode).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 9].Value =
                    Grd2.Cells[fCell.RowIndex, 10].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 9].Value = Sm.GetLue(LuePhaseCode);
                    Grd2.Cells[fCell.RowIndex, 10].Value = LuePhaseCode.GetColumnValue("Col2");
                }
                LuePhaseCode.Visible = false;
            }
        }

        private void LuePhaseCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex))
            {
                LueRequestEdit(Grd2, LuePhaseCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                SetLuePhaseCode(ref LuePhaseCode);
            }

            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd2.Rows.Count - 1, new int[] { 4, 5 });
            }

        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            //Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
            if(mIsProjectNameEditable && IsLOPNotSettled())
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtProjectName }, ChkCancelInd.Checked);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {

        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {

        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmUpdatingLOP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmUpdatingLOP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        #region Grid Event - Upload File

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|ZIP/RAR files (*.rar;*zip)|*.rar;*zip";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd3.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd3_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            //int SelectedIndex = 0;

            //if (Grd3.SelectedRows.Count > 0)
            //{
            //    for (int Index = Grd3.SelectedRows.Count - 1; Index >= 0; Index--)
            //        SelectedIndex = Grd3.SelectedRows[Index].Index;
            //}

            //if (mStateIndicator == "E") //&& TxtStatus.Text != "Approved"
            //{
            //    if (Sm.GetGrdStr(Grd3, SelectedIndex, 4).Length == 0)
            //    {
            //        Sm.GrdRemoveRow(Grd3, e, BtnSave);
            //        Sm.GrdEnter(Grd3, e);
            //        Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
            //    }
            //}
            if (mStateIndicator == "I") //else 
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                Sm.GrdEnter(Grd3, e);
                Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
            }
        }

        #endregion


        #endregion

    }
}
