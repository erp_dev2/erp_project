﻿#region Update
/*
    18/12/2019 [VIN/VIR] new apps
    19/12/2019 [DITA/VIR] isi data reporting
    15/04/2020 [WED/YK] tambah filter triwulan
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptBalanceSheet2 : RunSystem.FrmBase6
    {
        #region Field
        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mThisYear = string.Empty,
            mOneYearAgo = string.Empty;

        #endregion

        #region Constructor

        public FrmRptBalanceSheet2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetLueMth1(ref LueMth1);
                SetLueMth2(ref LueMth2);
                SetTimeStampVariable();
                SetGrd();
                GetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #region Standar Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("SELECT *FROM TblRptDescription where  doctype = 'BalanceSheet' order by description1 ");
            SQL.AppendLine("Select * ");
            SQL.AppendLine("From TblFicoSettingJournalToCBPHdr ");
            SQL.AppendLine("Where Doctype = 'BalanceSheet' ");
            SQL.AppendLine("And ActInd = 'Y' ");
            SQL.AppendLine("Order By Right(Concat('00000000', Sequence ), 8), Code ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Uraian",
                    "Jenis Asset",
                    "Kategori Asset",
                    "Asset Detail",
                    "RKAP "+ mThisYear,

                    //6-10
                    "RKAP TRIWULAN I"+Environment.NewLine+"TAHUN "+ mThisYear,
                    "TRIWULAN I TAHUN "+ mOneYearAgo,
                    "AUDITED "+ mOneYearAgo, 
                    "TRIWULAN I TAHUN "+ mThisYear,
                    "PERBANDINGAN %",

                    //11-15
                    "RKAP TRIWULAN II"+Environment.NewLine+"TAHUN "+ mThisYear,
                    "TRIWULAN II TAHUN "+ mOneYearAgo,
                    "TRIWULAN II TAHUN "+ mThisYear,
                    "PERBANDINGAN %",
                    "RKAP TRIWULAN III"+Environment.NewLine+"TAHUN "+ mThisYear,

                    //16-20
                    "TRIWULAN III TAHUN "+ mOneYearAgo,
                    "TRIWULAN III TAHUN "+ mThisYear,
                    "PERBANDINGAN %",
                    "RKAP TRIWULAN IV"+Environment.NewLine+"TAHUN "+ mThisYear,
                    "TRIWULAN IV TAHUN "+ mOneYearAgo,

                    //21-24
                    "TRIWULAN IV TAHUN "+ mThisYear,
                    "PERBANDINGAN %",
                    "DocType",
                    "Code"
                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    150, 150, 150, 150, 150,  
                    
                    //6-10
                    150, 200, 200, 150, 200,

                    //11-15
                    150, 200, 200, 150, 150, 

                    //16-20
                    200, 200, 150, 150, 200, 

                    //21-24
                    200, 150, 0, 0
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            Sm.SetGrdProperty(Grd1, false);
        }

        protected override void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth1, "Month")) return;
            try
            {
                var cm = new MySqlCommand();
                Cursor.Current = Cursors.WaitCursor;
                Sm.ClearGrd(Grd1, false);
                SetTimeStampVariable();
                SetGrd();

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(), new string[]
                    {
                        //0
                        "Description1",
                        //1-5
                        "Description2", "Description3", "Description4", "DocType", "Code"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Grd.Cells[Row, 5].Value = 0m;
                        Grd.Cells[Row, 6].Value = 0m;
                        Grd.Cells[Row, 7].Value = 0m;
                        Grd.Cells[Row, 8].Value = 0m;
                        Grd.Cells[Row, 9].Value = 0m;
                        Grd.Cells[Row, 10].Value = 0m;
                        Grd.Cells[Row, 11].Value = 0m;
                        Grd.Cells[Row, 12].Value = 0m;
                        Grd.Cells[Row, 13].Value = 0m;
                        Grd.Cells[Row, 14].Value = 0m;
                        Grd.Cells[Row, 15].Value = 0m;
                        Grd.Cells[Row, 16].Value = 0m;
                        Grd.Cells[Row, 17].Value = 0m;
                        Grd.Cells[Row, 18].Value = 0m;
                        Grd.Cells[Row, 19].Value = 0m;
                        Grd.Cells[Row, 20].Value = 0m;
                        Grd.Cells[Row, 21].Value = 0m;
                        Grd.Cells[Row, 22].Value = 0m;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 5);
                    }, true, false, false, false
                );
                ProcessData();
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void SetLookUpEdit(DXE.LookUpEdit Lue, object ds)
        {
            //populate data for LookUpEdit control
            try
            {
                Lue.DataBindings.Clear();
                Lue.Properties.DataSource = ds;
                Lue.Properties.PopulateColumns();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Lue.EditValue = null;
            }
        }

        private void SetLueMth1(ref DXE.LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { null, "01", "04", "07", "10" });
        }

        private void SetLueMth2(ref DXE.LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { null, "03", "06", "09", "12" });
        }

        private void ProcessData()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<BalanceSheet>();

            SQL.AppendLine("SELECT T.DocType, T.Code, SUM(T.Amt) Amt, SUM(T.Amt1) Amt1, SUM(T.Amt2) Amt2, SUM(T.Amt3) Amt3, SUM(T.Amt4) Amt4, ");
            SQL.AppendLine("SUM(T.JAmt1Now) JAmt1Now, SUM(T.JAmt1Old) JAmt1Old, SUM(T.JAmt2Now) JAmt2Now, SUM(T.JAmt2Old) JAmt2Old, ");
            SQL.AppendLine("SUM(T.JAmt3Now) JAmt3Now, SUM(T.JAmt3Old) JAmt3Old, SUM(T.JAmt4Now) JAmt4Now, SUM(T.JAmt4Old) JAmt4Old, SUM(T.Audited) Audited ");
            SQL.AppendLine("FROM  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, B.Amt, ");
            if (Sm.GetLue(LueMth1) == "01") SQL.AppendLine("    (B.Amt01 + B.Amt02 + B.Amt03) Amt1, "); else SQL.AppendLine("    0.00 Amt1, ");
            if (Sm.GetLue(LueMth1) == "04") SQL.AppendLine("    (B.Amt04 + B.Amt05 + B.Amt06) Amt2, "); else SQL.AppendLine("    0.00 Amt2, ");
            if (Sm.GetLue(LueMth1) == "07") SQL.AppendLine("    (B.Amt07 + B.Amt08 + B.Amt09) Amt3, "); else SQL.AppendLine("    0.00 Amt3, ");
            if (Sm.GetLue(LueMth1) == "10") SQL.AppendLine("    (B.Amt10 + B.Amt11 + B.Amt12) Amt4, "); else SQL.AppendLine("    0.00 Amt4, ");
            SQL.AppendLine("    0.00 JAmt1Now, 0.00 JAmt1Old, 0.00 JAmt2Now, ");
            SQL.AppendLine("    0.00 JAmt2Old, 0.00 JAmt3Now, 0.00 JAmt3Old, 0.00 JAmt4Now, 0.00 JAmt4Old, 0.00 Audited ");
            SQL.AppendLine("    FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND B.Amt != 0 ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'BalanceSheet' ");
            SQL.AppendLine("        AND C.AcNo = D.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");
            SQL.AppendLine("    AND A.Yr = @ThisYear ");

            if (Sm.GetLue(LueMth1) == "01")
            {
                SQL.AppendLine("    UNION ALL ");


                SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 Amt3, 0.00 Amt4, ");
                SQL.AppendLine("    IF(C.AcType = 'C', (B.CAmt - B.DAmt), (B.DAmt - B.CAmt)) JAmt1Now, 0.00 JAmt1Old, 0.00 JAmt2Now, 0.00 JAmt2Old, ");
                SQL.AppendLine("    0.00 JAmt3Now, 0.00 JAmt3Old, 0.00 JAmt4Now, 0.00 JAmt4Old, 0.00 Audited ");
                SQL.AppendLine("    FROM TblJournalHdr A ");
                SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'BalanceSheet' ");
                SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
                SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
                SQL.AppendLine("    Where A.CancelInd = 'N' ");
                SQL.AppendLine("    AND LEFT(A.DocDt, 4) = @ThisYear ");
                SQL.AppendLine("    AND SUBSTR(A.DocDt, 5, 2) IN ('01', '02', '03') ");
            }

            if (Sm.GetLue(LueMth1) == "04")
            {
                SQL.AppendLine("    UNION ALL ");

                SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 Amt3, 0.00 Amt4, ");
                SQL.AppendLine("    0.00 JAmt1Now, 0.00 JAmt1Old, IF(C.AcType = 'C', (B.CAmt - B.DAmt), (B.DAmt - B.CAmt)) JAmt2Now, 0.00 JAmt2Old, ");
                SQL.AppendLine("    0.00 JAmt3Now, 0.00 JAmt3Old, 0.00 JAmt4Now, 0.00 JAmt4Old, 0.00 Audited ");
                SQL.AppendLine("    FROM TblJournalHdr A ");
                SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'BalanceSheet' ");
                SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
                SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
                SQL.AppendLine("    Where A.CancelInd = 'N' ");
                SQL.AppendLine("    AND LEFT(A.DocDt, 4) = @ThisYear ");
                SQL.AppendLine("    AND SUBSTR(A.DocDt, 5, 2) IN ('04', '05', '06') ");
            }

            if (Sm.GetLue(LueMth1) == "07")
            {
                SQL.AppendLine("    UNION ALL ");

                SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 Amt3, 0.00 Amt4, ");
                SQL.AppendLine("    0.00 JAmt1Now, 0.00 JAmt1Old, 0.00 JAmt2Now, 0.00 JAmt2Old, ");
                SQL.AppendLine("    IF(C.AcType = 'C', (B.CAmt - B.DAmt), (B.DAmt - B.CAmt)) JAmt3Now, 0.00 JAmt3Old, 0.00 JAmt4Now, 0.00 JAmt4Old, 0.00 Audited ");
                SQL.AppendLine("    FROM TblJournalHdr A ");
                SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'BalanceSheet' ");
                SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
                SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
                SQL.AppendLine("    Where A.CancelInd = 'N' ");
                SQL.AppendLine("    AND LEFT(A.DocDt, 4) = @ThisYear ");
                SQL.AppendLine("    AND SUBSTR(A.DocDt, 5, 2) IN ('07', '08', '09') ");
            }

            if (Sm.GetLue(LueMth1) == "10")
            {
                SQL.AppendLine("    UNION ALL ");

                SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 Amt3, 0.00 Amt4, ");
                SQL.AppendLine("    0.00 JAmt1Now, 0.00 JAmt1Old, 0.00 JAmt2Now, 0.00 JAmt2Old, ");
                SQL.AppendLine("    0.00 JAmt3Now, 0.00 JAmt3Old, IF(C.AcType = 'C', (B.CAmt - B.DAmt), (B.DAmt - B.CAmt)) JAmt4Now, 0.00 JAmt4Old, 0.00 Audited ");
                SQL.AppendLine("    FROM TblJournalHdr A ");
                SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'BalanceSheet' ");
                SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
                SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
                SQL.AppendLine("    Where A.CancelInd = 'N' ");
                SQL.AppendLine("    AND LEFT(A.DocDt, 4) = @ThisYear ");
                SQL.AppendLine("    AND SUBSTR(A.DocDt, 5, 2) IN ('10', '11', '12') ");
            }

            if (Sm.GetLue(LueMth1) == "01")
            {
                SQL.AppendLine("    UNION ALL ");

                SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 Amt3, 0.00 Amt4, ");
                SQL.AppendLine("    0.00 JAmt1Now, IF(C.AcType = 'C', (B.CAmt - B.DAmt), (B.DAmt - B.CAmt)) JAmt1Old, 0.00 JAmt2Now, 0.00 JAmt2Old, ");
                SQL.AppendLine("    0.00 JAmt3Now, 0.00 JAmt3Old, 0.00 JAmt4Now, 0.00 JAmt4Old, 0.00 Audited ");
                SQL.AppendLine("    FROM TblJournalHdr A ");
                SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'BalanceSheet' ");
                SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
                SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
                SQL.AppendLine("    Where A.CancelInd = 'N' ");
                SQL.AppendLine("    AND LEFT(A.DocDt, 4) = @OneYearAgo");
                SQL.AppendLine("    AND SUBSTR(A.DocDt, 5, 2) IN ('01', '02', '03') ");
            }

            if (Sm.GetLue(LueMth1) == "04")
            {
                SQL.AppendLine("    UNION ALL ");

                SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 Amt3, 0.00 Amt4, ");
                SQL.AppendLine("    0.00 JAmt1Now, 0.00 JAmt1Old, 0.00 JAmt2Now, IF(C.AcType = 'C', (B.CAmt - B.DAmt), (B.DAmt - B.CAmt)) JAmt2Old, ");
                SQL.AppendLine("    0.00 JAmt3Now, 0.00 JAmt3Old, 0.00 JAmt4Now, 0.00 JAmt4Old, 0.00 Audited ");
                SQL.AppendLine("    FROM TblJournalHdr A ");
                SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'BalanceSheet' ");
                SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
                SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
                SQL.AppendLine("    Where A.CancelInd = 'N' ");
                SQL.AppendLine("    AND LEFT(A.DocDt, 4) = @OneYearAgo ");
                SQL.AppendLine("    AND SUBSTR(A.DocDt, 5, 2) IN ('04', '05', '06') ");

            }

            if (Sm.GetLue(LueMth1) == "07")
            {
                SQL.AppendLine("    UNION ALL ");

                SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 Amt3, 0.00 Amt4, ");
                SQL.AppendLine("    0.00 JAmt1Now, 0.00 JAmt1Old, 0.00 JAmt2Now, 0.00 JAmt2Old, ");
                SQL.AppendLine("    0.00 JAmt3Now, IF(C.AcType = 'C', (B.CAmt - B.DAmt), (B.DAmt - B.CAmt)) JAmt3Old, 0.00 JAmt4Now, 0.00 JAmt4Old, 0.00 Audited ");
                SQL.AppendLine("    FROM TblJournalHdr A ");
                SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'BalanceSheet' ");
                SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
                SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
                SQL.AppendLine("    Where A.CancelInd = 'N' ");
                SQL.AppendLine("    AND LEFT(A.DocDt, 4) = @OneYearAgo ");
                SQL.AppendLine("    AND SUBSTR(A.DocDt, 5, 2) IN ('07', '08', '09') ");
            }

            if (Sm.GetLue(LueMth1) == "10")
            {
                SQL.AppendLine("    UNION ALL ");

                SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 Amt3, 0.00 Amt4, ");
                SQL.AppendLine("    0.00 JAmt1Now, 0.00 JAmt1Old, 0.00 JAmt2Now, 0.00 JAmt2Old, ");
                SQL.AppendLine("    0.00 JAmt3Now, 0.00 JAmt3Old, 0.00 JAmt4Now, IF(C.AcType = 'C', (B.CAmt - B.DAmt), (B.DAmt - B.CAmt)) JAmt4Old, 0.00 Audited ");
                SQL.AppendLine("    FROM TblJournalHdr A ");
                SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'BalanceSheet' ");
                SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
                SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
                SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
                SQL.AppendLine("    Where A.CancelInd = 'N' ");
                SQL.AppendLine("    AND LEFT(A.DocDt, 4) = @OneYearAgo ");
                SQL.AppendLine("    AND SUBSTR(A.DocDt, 5, 2) IN ('10', '11', '12') ");
            }

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT D.DocType, D.Code, B.AcNo, 0.00 Amt, 0.00 Amt1, 0.00 Amt2, 0.00 Amt3, 0.00 Amt4, ");
            SQL.AppendLine("    0.00 JAmt1Now, 0.00 JAmt1Old, 0.00 JAmt2Now, 0.00 JAmt2Old, ");
            SQL.AppendLine("    0.00 JAmt3Now, 0.00 JAmt3Old, 0.00 JAmt4Now, 0.00 JAmt4Old, IF(C.AcType = 'C', (B.CAmt - B.DAmt), (B.DAmt - B.CAmt)) Audited ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'BalanceSheet' ");
            SQL.AppendLine("        AND C.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E On D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code And E.ActInd = 'Y' ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");
            SQL.AppendLine("    AND LEFT(A.DocDt, 4) = @OneYearAgo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.DocType, T.Code; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@OneYearAgo", mOneYearAgo);
                Sm.CmParam<String>(ref cm, "@ThisYear", mThisYear);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocType",
                    
                    //1-5
                    "Code",
                    "Amt",
                    "Amt1",
                    "Amt2",
                    "Amt3",

                    //6-10
                    "Amt4",
                    "JAmt1Now",
                    "JAmt1Old",
                    "JAmt2Now",
                    "JAmt2Old",

                    //11-15
                    "JAmt3Now",
                    "JAmt3Old",
                    "JAmt4Now",
                    "JAmt4Old",
                    "Audited"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BalanceSheet()
                        {
                            DocType = Sm.DrStr(dr, c[0]),
                            Code = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt1 = Sm.DrDec(dr, c[3]),
                            Amt2 = Sm.DrDec(dr, c[4]),
                            Amt3 = Sm.DrDec(dr, c[5]),
                            Amt4 = Sm.DrDec(dr, c[6]),
                            JAmt1Now = Sm.DrDec(dr, c[7]),
                            JAmt1Old = Sm.DrDec(dr, c[8]),
                            JAmt2Now = Sm.DrDec(dr, c[9]),
                            JAmt2Old = Sm.DrDec(dr, c[10]),
                            JAmt3Now = Sm.DrDec(dr, c[11]),
                            JAmt3Old = Sm.DrDec(dr, c[12]),
                            JAmt4Now = Sm.DrDec(dr, c[13]),
                            JAmt4Old = Sm.DrDec(dr, c[14]),
                            Audited = Sm.DrDec(dr, c[15])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach (var x in l)
                {
                    for (int i = 0; i < Grd1.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 23) == x.DocType &&
                            Sm.GetGrdStr(Grd1, i, 24) == x.Code)
                        {
                            Grd1.Cells[i, 5].Value = x.Amt;
                            Grd1.Cells[i, 6].Value = x.Amt1;
                            Grd1.Cells[i, 7].Value = x.JAmt1Old;
                            Grd1.Cells[i, 8].Value = x.Audited;
                            Grd1.Cells[i, 9].Value = x.JAmt1Now;
                            Grd1.Cells[i, 10].Value = (x.JAmt1Old != 0m) ? (x.JAmt1Now / x.JAmt1Old) * 100m : 0m;
                            Grd1.Cells[i, 11].Value = x.Amt2;
                            Grd1.Cells[i, 12].Value = x.JAmt2Old;
                            Grd1.Cells[i, 13].Value = x.JAmt2Now;
                            Grd1.Cells[i, 14].Value = (x.JAmt2Old != 0m) ? (x.JAmt2Now / x.JAmt2Old) * 100m : 0m;
                            Grd1.Cells[i, 15].Value = x.Amt3;
                            Grd1.Cells[i, 16].Value = x.JAmt3Old;
                            Grd1.Cells[i, 17].Value = x.JAmt3Now;
                            Grd1.Cells[i, 18].Value = (x.JAmt3Old != 0m) ? (x.JAmt3Now / x.JAmt3Old) * 100m : 0m;
                            Grd1.Cells[i, 19].Value = x.Amt4;
                            Grd1.Cells[i, 20].Value = x.JAmt4Old;
                            Grd1.Cells[i, 21].Value = x.JAmt4Now;
                            Grd1.Cells[i, 22].Value = (x.JAmt4Old != 0m) ? (x.JAmt4Now / x.JAmt4Old) * 100m : 0m;

                            break;
                        }
                    }
                }
            }

            l.Clear();
        }

        private void SetTimeStampVariable()
        {
            mThisYear = Sm.GetLue(LueYr);
            mOneYearAgo = (Int32.Parse(mThisYear) - 1).ToString();
        }

        #endregion

        #region Events

        #region Misc Control Events

        private void LueMth1_EditValueChanged(object sender, EventArgs e)
        {
            LueMth2.EditValue = null;
            if (Sm.GetLue(LueMth1).Length > 0) Sm.SetLue(LueMth2, Sm.Right(string.Concat("00", (Int32.Parse(Sm.GetLue(LueMth1)) + 2).ToString()), 2));
        }

        #endregion

        #endregion

        #endregion

        #region Class

        private class BalanceSheet
        {
            public string DocType { get; set; }
            public string Code { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal Amt4 { get; set; }
            public decimal JAmt1Now { get; set; }
            public decimal JAmt1Old { get; set; }
            public decimal JAmt2Now { get; set; }
            public decimal JAmt2Old { get; set; }
            public decimal JAmt3Now { get; set; }
            public decimal JAmt3Old { get; set; }
            public decimal JAmt4Now { get; set; }
            public decimal JAmt4Old { get; set; }
            public decimal Audited { get; set; }
        }

        #endregion

    }
}
