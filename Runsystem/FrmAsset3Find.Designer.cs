﻿namespace RunSystem
{
    partial class FrmAsset3Find
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkStatus = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkAssetCategoryCode = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueAssetCategoryCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtAssetName = new DevExpress.XtraEditors.TextEdit();
            this.ChkAssetName = new DevExpress.XtraEditors.CheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkProfitCenterCode = new DevExpress.XtraEditors.CheckEdit();
            this.CcbProfitCenterCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetCategoryCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAssetCategoryCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetName.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.ChkStatus);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueStatus);
            this.panel2.Controls.Add(this.ChkAssetCategoryCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LueAssetCategoryCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtAssetName);
            this.panel2.Controls.Add(this.ChkAssetName);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkStatus
            // 
            this.ChkStatus.Location = new System.Drawing.Point(294, 46);
            this.ChkStatus.Name = "ChkStatus";
            this.ChkStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkStatus.Properties.Appearance.Options.UseFont = true;
            this.ChkStatus.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkStatus.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkStatus.Properties.Caption = " ";
            this.ChkStatus.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkStatus.Size = new System.Drawing.Size(20, 22);
            this.ChkStatus.TabIndex = 17;
            this.ChkStatus.ToolTip = "Remove filter";
            this.ChkStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkStatus.ToolTipTitle = "Run System";
            this.ChkStatus.CheckedChanged += new System.EventHandler(this.ChkStatus_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(15, 52);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 15;
            this.label1.Text = "Status";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueStatus
            // 
            this.LueStatus.EnterMoveNextControl = true;
            this.LueStatus.Location = new System.Drawing.Point(61, 48);
            this.LueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueStatus.Name = "LueStatus";
            this.LueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.Appearance.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStatus.Properties.DropDownRows = 30;
            this.LueStatus.Properties.NullText = "[Empty]";
            this.LueStatus.Properties.PopupWidth = 250;
            this.LueStatus.Size = new System.Drawing.Size(229, 20);
            this.LueStatus.TabIndex = 16;
            this.LueStatus.ToolTip = "F4 : Show/hide list";
            this.LueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged);
            // 
            // ChkAssetCategoryCode
            // 
            this.ChkAssetCategoryCode.Location = new System.Drawing.Point(294, 25);
            this.ChkAssetCategoryCode.Name = "ChkAssetCategoryCode";
            this.ChkAssetCategoryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAssetCategoryCode.Properties.Appearance.Options.UseFont = true;
            this.ChkAssetCategoryCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAssetCategoryCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAssetCategoryCode.Properties.Caption = " ";
            this.ChkAssetCategoryCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAssetCategoryCode.Size = new System.Drawing.Size(20, 22);
            this.ChkAssetCategoryCode.TabIndex = 14;
            this.ChkAssetCategoryCode.ToolTip = "Remove filter";
            this.ChkAssetCategoryCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAssetCategoryCode.ToolTipTitle = "Run System";
            this.ChkAssetCategoryCode.CheckedChanged += new System.EventHandler(this.ChkAssetCategoryCode_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(1, 30);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 14);
            this.label5.TabIndex = 12;
            this.label5.Text = "Category";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAssetCategoryCode
            // 
            this.LueAssetCategoryCode.EnterMoveNextControl = true;
            this.LueAssetCategoryCode.Location = new System.Drawing.Point(61, 26);
            this.LueAssetCategoryCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueAssetCategoryCode.Name = "LueAssetCategoryCode";
            this.LueAssetCategoryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCategoryCode.Properties.Appearance.Options.UseFont = true;
            this.LueAssetCategoryCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCategoryCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAssetCategoryCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCategoryCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAssetCategoryCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCategoryCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAssetCategoryCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCategoryCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAssetCategoryCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAssetCategoryCode.Properties.DropDownRows = 30;
            this.LueAssetCategoryCode.Properties.NullText = "[Empty]";
            this.LueAssetCategoryCode.Properties.PopupWidth = 250;
            this.LueAssetCategoryCode.Size = new System.Drawing.Size(229, 20);
            this.LueAssetCategoryCode.TabIndex = 13;
            this.LueAssetCategoryCode.ToolTip = "F4 : Show/hide list";
            this.LueAssetCategoryCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAssetCategoryCode.EditValueChanged += new System.EventHandler(this.LueAssetCategoryCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "Asset ";
            // 
            // TxtAssetName
            // 
            this.TxtAssetName.EnterMoveNextControl = true;
            this.TxtAssetName.Location = new System.Drawing.Point(61, 4);
            this.TxtAssetName.Name = "TxtAssetName";
            this.TxtAssetName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetName.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetName.Properties.MaxLength = 250;
            this.TxtAssetName.Size = new System.Drawing.Size(229, 20);
            this.TxtAssetName.TabIndex = 10;
            this.TxtAssetName.Validated += new System.EventHandler(this.TxtAssetName_Validated);
            // 
            // ChkAssetName
            // 
            this.ChkAssetName.Location = new System.Drawing.Point(294, 2);
            this.ChkAssetName.Name = "ChkAssetName";
            this.ChkAssetName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAssetName.Properties.Appearance.Options.UseFont = true;
            this.ChkAssetName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAssetName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAssetName.Properties.Caption = " ";
            this.ChkAssetName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAssetName.Size = new System.Drawing.Size(20, 22);
            this.ChkAssetName.TabIndex = 11;
            this.ChkAssetName.ToolTip = "Remove filter";
            this.ChkAssetName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAssetName.ToolTipTitle = "Run System";
            this.ChkAssetName.CheckedChanged += new System.EventHandler(this.ChkAssetName_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkProfitCenterCode);
            this.panel4.Controls.Add(this.CcbProfitCenterCode);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(325, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(343, 70);
            this.panel4.TabIndex = 18;
            // 
            // ChkProfitCenterCode
            // 
            this.ChkProfitCenterCode.Location = new System.Drawing.Point(320, 5);
            this.ChkProfitCenterCode.Name = "ChkProfitCenterCode";
            this.ChkProfitCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProfitCenterCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProfitCenterCode.Properties.Caption = " ";
            this.ChkProfitCenterCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProfitCenterCode.Size = new System.Drawing.Size(18, 22);
            this.ChkProfitCenterCode.TabIndex = 21;
            this.ChkProfitCenterCode.ToolTip = "Remove filter";
            this.ChkProfitCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProfitCenterCode.ToolTipTitle = "Run System";
            this.ChkProfitCenterCode.CheckedChanged += new System.EventHandler(this.ChkProfitCenterCode_CheckedChanged);
            // 
            // CcbProfitCenterCode
            // 
            this.CcbProfitCenterCode.Location = new System.Drawing.Point(88, 6);
            this.CcbProfitCenterCode.Name = "CcbProfitCenterCode";
            this.CcbProfitCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbProfitCenterCode.Properties.DropDownRows = 30;
            this.CcbProfitCenterCode.Size = new System.Drawing.Size(229, 20);
            this.CcbProfitCenterCode.TabIndex = 20;
            this.CcbProfitCenterCode.EditValueChanged += new System.EventHandler(this.CcbProfitCenterCode_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(6, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 19;
            this.label3.Text = "Profit Center";
            // 
            // FrmAsset3Find
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmAsset3Find";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetCategoryCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAssetCategoryCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetName.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkStatus;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueStatus;
        private DevExpress.XtraEditors.CheckEdit ChkAssetCategoryCode;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueAssetCategoryCode;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtAssetName;
        private DevExpress.XtraEditors.CheckEdit ChkAssetName;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkProfitCenterCode;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbProfitCenterCode;
        private System.Windows.Forms.Label label3;
    }
}