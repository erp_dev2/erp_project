﻿#region Update
/*
    18/07/2019 [WED] master baru keperluan IMM
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMSeller : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmIMMSellerFind FrmFind;

        #endregion

        #region Constructor

        public FrmIMMSeller(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSellerCode, TxtSellerName
                    }, true);
                    TxtSellerCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSellerCode, TxtSellerName
                    }, false);
                    TxtSellerCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtSellerCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSellerName
                    }, false);
                    TxtSellerName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSellerCode, TxtSellerName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMSellerFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSellerCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblIMMSeller(SellerCode, SellerName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@SellerCode, @SellerName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update SellerName=@SellerName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@SellerCode", TxtSellerCode.Text);
                Sm.CmParam<String>(ref cm, "@SellerName", TxtSellerName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtSellerCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string SellerCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@SellerCode", SellerCode);
                Sm.ShowDataInCtrl(
                    ref cm,
                    "Select SellerCode, SellerName From TblIMMSeller Where SellerCode=@SellerCode",
                    new string[] 
                    {
                        "SellerCode", "SellerName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSellerCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtSellerName.EditValue = Sm.DrStr(dr, c[1]);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSellerCode, "Seller code", false) ||
                Sm.IsTxtEmpty(TxtSellerName, "Seller name", false) ||
                IsSellerCodeExisted();
        }

        private bool IsSellerCodeExisted()
        {
            if (!TxtSellerCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select SellerCode From TblIMMSeller Where SellerCode=@Param Limit 1;", TxtSellerCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Seller code ( " + TxtSellerCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSellerCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSellerCode);
        }

        private void TxtSellerName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSellerName);
        }

        #endregion

        #endregion
    }
}
