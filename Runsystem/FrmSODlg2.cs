﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSODlg2 : RunSystem.FrmBase8
    {
        #region Field

        private FrmSO mFrmParent;
        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSOQuot = string.Empty, mSOQuotPromo = string.Empty;
        private bool IsDataExisted = false;

        #endregion

        #region Constructor

        public FrmSODlg2(FrmSO FrmParent, string SOQuot, string SOQuotPromo, string GridSelect)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mSOQuot = SOQuot;
            mSOQuotPromo = SOQuotPromo;
            mMenuCode = mFrmParent.mMenuCode;
            if (GridSelect.Length!=0) IsDataExisted = true;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            SetFormControl(mState.Insert);
            Sl.SetLueAgtCode(ref LueAgtCode);
            Sm.SetDteCurrentDate(DteDocDt);
            if (IsDataExisted)
                ShowDataFromParent();
            
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            switch (state)
            {
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         LueAgtCode,  TxtQty, TxtTax
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItemName,  TxtUomCode, TxtUPrice, TxtItCode,
                        TxtDiscRate1, TxtDiscRate2, TxtDiscRate3, TxtDiscRate4, TxtDiscRate5,
                        TxtPromoRate, TxtUPriceBefore, TxtUPriceAfter, TxtTotal, TxtAmt
                    }, true);
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                    {
                        TxtQty, TxtUPrice, TxtDiscRate1, TxtDiscRate2, TxtDiscRate3, TxtDiscRate4, TxtDiscRate5,
                        TxtPromoRate, TxtUPriceBefore, TxtUPriceAfter, TxtTax, TxtTotal, TxtAmt
                    }, 0);
                    LueAgtCode.Focus(); 
                    BtnItem.Enabled = true;
                    BtnMasterItem.Enabled = true;
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtItCode, TxtItemName,  TxtUomCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                  TxtQty, TxtUPrice, TxtDiscRate1, TxtDiscRate2, TxtDiscRate3, TxtDiscRate4, TxtDiscRate5,
                  TxtPromoRate, TxtUPriceBefore, TxtUPriceAfter, TxtTax, TxtTotal, TxtAmt
            }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnAddClick(object sender, EventArgs e)
        {
            if (IsInsertedDataNotValid()) return;
                InsertData();
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            try
            {
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[2].Value = Sm.GetLue(LueAgtCode);
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[3].Value = Sm.GetValue("Select AgtName From TblAgent Where AgtCode = '" + Sm.GetLue(LueAgtCode) + "' ");
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[4].Value = TxtItCode.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[5].Value = TxtItemName.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[6].Value = TxtQty.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[7].Value = TxtUomCode.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[8].Value = TxtUPrice.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[9].Value = TxtDiscRate1.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[10].Value = TxtDiscRate2.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[11].Value = TxtDiscRate3.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[12].Value = TxtDiscRate4.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[13].Value = TxtDiscRate5.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[14].Value = TxtPromoRate.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[15].Value = TxtUPriceBefore.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[16].Value = TxtTax.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[17].Value = TxtAmt.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[18].Value = TxtUPriceAfter.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[19].Value = TxtTotal.Text;
                mFrmParent.Grd1.Rows[mFrmParent.Grd1.CurRow.Index].Cells[20].Value = Sm.SetGrdDate(Sm.GetDte(DteDocDt).Substring(0, 8));

                if (!IsDataExisted)
                {
                    mFrmParent.Grd1.Rows.Add();
                    Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
                }
                mFrmParent.ComputeTotal();
                this.Close();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        #region Show Data

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueAgtCode, "Agent") ||
                Sm.IsTxtEmpty(TxtItCode, "Item Code", false) ||
                Sm.IsTxtEmpty(TxtQty, "Quantity", true);
        }


        public void ShowDataDisc()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select C.UPrice, B.DiscRT1, B.DiscRt2, B.DiscRt3, B.DiscRt4, B.DiscRt5 " +
                        "From TblSoQuot A Inner join TblSOQuotItem B On A.DocNo = B.DocNo " +
                        "Inner Join TblItemPriceDtl C On B.ItCode = C.ItCode And B.ItemPriceDocNo = C.DocNo " +
                        "Where A.DocNo = '"+mFrmParent.TxtSOQuot.Text+"' Group By B.ItCode ",
                        new string[] 
                        {
                            "UPrice", "DiscRT1", "DiscRt2", "DiscRt3", "DiscRt4", "DiscRt5"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtUPrice.EditValue = Sm.DrStr(dr, c[0]);
                            TxtDiscRate1.EditValue = Sm.DrStr(dr, c[1]);
                            TxtDiscRate2.EditValue = Sm.DrStr(dr, c[2]);
                            TxtDiscRate3.EditValue = Sm.DrStr(dr, c[3]);
                            TxtDiscRate4.EditValue = Sm.DrStr(dr, c[4]);
                            TxtDiscRate5.EditValue = Sm.DrStr(dr, c[5]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowDataFromParent()
        {
            Sm.SetLue(LueAgtCode, Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 2));
            TxtItCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 3);
            TxtItCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 4);
            TxtItemName.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 5);
            TxtQty.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 6);
            TxtUomCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 7);
            TxtUPrice.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 8);
            TxtDiscRate1.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 9);
            TxtDiscRate2.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 10);
            TxtDiscRate3.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 11);
            TxtDiscRate4.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 12);
            TxtDiscRate5.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 13);
            TxtPromoRate.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 14);
            TxtUPriceBefore.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 15);
            TxtTax.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 16);
            TxtAmt.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 17);
            TxtUPriceAfter.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 18);
            TxtTotal.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 19);
            Sm.SetDte(DteDocDt, Sm.GetGrdDate(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 20));
            //mFrmParent.Grd1.Rows.RemoveAt(mFrmParent.Grd1.CurRow.Index);
        }
        #endregion

        #region Additional Method

        public void ComputePriceBefore()
        {
            decimal disc1, disc2, disc3, disc4, disc5, promo = 0m;
            decimal t1, t2, t3, t4, t5, t6 = 0m;
            decimal up = 0m;
            up = decimal.Parse(TxtUPrice.Text);
            disc1 = decimal.Parse(TxtDiscRate1.Text);
            disc2 = decimal.Parse(TxtDiscRate2.Text);
            disc3 = decimal.Parse(TxtDiscRate3.Text);
            disc4 = decimal.Parse(TxtDiscRate4.Text);
            disc5 = decimal.Parse(TxtDiscRate5.Text);
            promo = decimal.Parse(TxtPromoRate.Text);

            t1 = up - ((up * disc1) / 100);
            t2 = t1 - ((t1 * disc2) / 100);
            t3 = t2 - ((t2 * disc3) / 100);
            t4 = t3 - ((t3 * disc4) / 100);
            t5 = t4 - ((t4 * disc5) / 100);
            t6 = t5 - ((t5 * promo) / 100);

            TxtUPriceBefore.Text = Sm.FormatNum(t6, 0);
        }

        public void ComputeTaxAmt(decimal tax)
        {
            decimal TAmt , UPb= 0m;
            tax = decimal.Parse(TxtTax.Text);
            UPb = decimal.Parse(TxtUPriceBefore.Text);
            TAmt = (UPb * tax) / 100;

            TxtAmt.Text = Sm.FormatNum(TAmt, 0);
            ComputePriceAfter();
        }


        public void ComputePriceAfter()
        {
            decimal TxAmt, UPb = 0m;
            UPb = decimal.Parse(TxtUPriceBefore.Text);
            TxAmt = decimal.Parse(TxtAmt.Text);
            TxtUPriceAfter.Text = Sm.FormatNum(UPb + TxAmt, 0);
            //ComputeTotal();
        }

        public void ComputeTotal()
        {
            decimal Qty, UPa = 0m;
            UPa = decimal.Parse(TxtUPriceAfter.Text);
            Qty = decimal.Parse(TxtQty.Text);
            TxtTotal.Text = Sm.FormatNum(UPa * Qty, 0);
        }

        #endregion

        #endregion

        #region Event
        private void BtnItem_Click(object sender, EventArgs e)
        {
                Sm.FormShowDialog(new FrmSODlg3(this, mSOQuot));
        }

        private void BtnMasterItem_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtItCode, "Item Code", false))
            {
                try
                {
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = TxtItCode.Text;
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }


        private void TxtQty_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQty, 0);
            string a = Sm.ServerCurrentDateTime();
            TxtPromoRate.Text = Sm.FormatNum(Sm.GetValue("Select A.DiscRate From TblSOQuotPromoItem A " +
            "Inner Join TblSOQuotPromoAgt B On A.DocNo = B.DocNo Inner Join TblSOQuotPromo C On A.DocNo = C.DocNo " +
            "Where C.DocNo = '" + mSOQuotPromo + "' And C.ActInd = 'Y' And C.DteEnd >= " + a.Substring(0, 8) + " " +
            "And B.AgtCode = '" + Sm.GetLue(LueAgtCode) + "' And A.ItCode = '" + TxtItCode.Text + "' "), 0);

            ShowDataDisc();
            ComputePriceBefore();
            ComputeTaxAmt(decimal.Parse(TxtTax.Text));
            ComputeTotal();
        }

        private void TxtUPrice_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtUPrice, 0);
        }

        private void TxtDiscRate1_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDiscRate1, 0);
        }

        private void TxtDiscRate2_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDiscRate2, 0);
        }

        private void TxtDiscRate3_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDiscRate3, 0);
        }

        private void TxtDiscRate4_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDiscRate4, 0);
        }

        private void TxtDiscRate5_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDiscRate5, 0);
        }

        private void TxtPromoRate_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPromoRate, 0);
        }

        private void TxtUPriceBefore_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtUPriceBefore, 0);
        }

        private void TxtTax_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTax, 0);
            ComputeTaxAmt(decimal.Parse(TxtTax.Text));
            ComputeTotal();
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void TxtUPriceAfter_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtUPriceAfter, 0);
        }

        private void TxtTotal_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotal, 0);
        }

        private void LueAgtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAgtCode, new Sm.RefreshLue1(Sl.SetLueAgtCode));
            ClearData();
        }

        #endregion

    }
}
