﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmRptMRToVoucherDlg5 : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptMRToVoucher mFrmParent;
        private string mSQL = string.Empty, mDNo = string.Empty, mDocNo = string.Empty, mRecvVdDocNo = string.Empty, mRecvVdDNo = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMRToVoucherDlg5(FrmRptMRToVoucher FrmParent, String DNo, string DocNo, string RecvVdDocNo) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDNo = DNo;
            mDocNo = DocNo;
            mRecvVdDocNo = RecvVdDocNo;
        }

        #endregion

        #region Method

        #region Standard Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                SetFormControl(mState.View);
                SetGrd();
                ShowData();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, TxtStatus, TxtVdName, TxtWhsName, TxtItCode, TxtItName}, true);
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtVdName, TxtWhsName, TxtItCode, TxtItName, TxtStatus
            });
            
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "Approve",

                        //1-3
                        "Status", 
                        "Approve Date",
                        "Remark"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });
        }

        private void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

        #endregion

        #region Show data
        public void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ShowWhsName();
                PrepRecvVdDno();
                ShowStatus();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select B.UserName, ");
                SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' ");
                SQL.AppendLine("When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As Status, A.LastUpDt, A.Remark ");
                SQL.AppendLine("From TblDocApproval A ");
                SQL.AppendLine("Inner Join TblUser B on A.UserCode = B.UserCode ");

                mSQL = SQL.ToString();

                string Filter = "Where A.DocType = 'RecvVd' And A.DocNo = '" + mRecvVdDocNo + "' And A.DNo = '" + mRecvVdDNo + "' ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ",
                        new string[]
                        {
                            //0
                            "UserName",
                            //1-5
                            "Status", "LastUpDt", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                        }, false, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void PrepRecvVdDno()
        {
            var sql = new StringBuilder();

            sql.AppendLine("Select A.DNo ");
            sql.AppendLine("From TblRecvVdDtl A ");
            sql.AppendLine("Inner Join TblPODtl B On A.PODocNo = B.DocNo And A.PODNo = B.DNo And A.DocNo = @Param1 ");
            sql.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo = C.DocNo And B.PORequestDNo = C.DNo ");
            sql.AppendLine("And C.MaterialRequestDocNo = @Param2 And C.MaterialRequestDNo = @Param3; ");

            mRecvVdDNo = Sm.GetValue(sql.ToString(), mRecvVdDocNo, mDocNo, mDNo);
        }

        private void ShowWhsName()
        {
            var sql = new StringBuilder();

            sql.AppendLine("Select B.WhsName ");
            sql.AppendLine("From TblRecvVdHdr A ");
            sql.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            sql.AppendLine("Where A.DocNo = @Param; ");

            TxtWhsName.Text = Sm.GetValue(sql.ToString(), mRecvVdDocNo);
        }

        private void ShowStatus()
        {
            var sql = new StringBuilder();

            sql.AppendLine("Select Case Status ");
            sql.AppendLine("    When 'A' Then 'Approved' ");
            sql.AppendLine("    When 'O' Then 'Outstanding' ");
            sql.AppendLine("    When 'C' Then 'Cancelled' ");
            sql.AppendLine("End As StatusDesc ");
            sql.AppendLine("From TblRecvVdDtl ");
            sql.AppendLine("Where DocNo = @Param1 And DNo = @Param2; ");

            TxtStatus.Text = Sm.GetValue(sql.ToString(), mRecvVdDocNo, mRecvVdDNo, string.Empty);
        }


        #endregion

        #endregion
    }
}
