﻿namespace RunSystem
{
    partial class FrmHoursMeterDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.TxtToDisplay = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtToName = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtToCode = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtMth = new DevExpress.XtraEditors.TextEdit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtToDisplay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtToName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtToCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 98);
            this.panel3.Size = new System.Drawing.Size(842, 375);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(842, 375);
            this.Grd1.TabIndex = 19;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtMth);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtToDisplay);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtToName);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtToCode);
            this.panel2.Size = new System.Drawing.Size(842, 98);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(64, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Display Name";
            // 
            // TxtToDisplay
            // 
            this.TxtToDisplay.EnterMoveNextControl = true;
            this.TxtToDisplay.Location = new System.Drawing.Point(150, 72);
            this.TxtToDisplay.Name = "TxtToDisplay";
            this.TxtToDisplay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtToDisplay.Properties.Appearance.Options.UseFont = true;
            this.TxtToDisplay.Properties.MaxLength = 30;
            this.TxtToDisplay.Size = new System.Drawing.Size(602, 20);
            this.TxtToDisplay.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Technical Object";
            // 
            // TxtToName
            // 
            this.TxtToName.EnterMoveNextControl = true;
            this.TxtToName.Location = new System.Drawing.Point(150, 28);
            this.TxtToName.Name = "TxtToName";
            this.TxtToName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtToName.Properties.Appearance.Options.UseFont = true;
            this.TxtToName.Properties.MaxLength = 30;
            this.TxtToName.Size = new System.Drawing.Size(208, 20);
            this.TxtToName.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Technical Object Code";
            // 
            // TxtToCode
            // 
            this.TxtToCode.EnterMoveNextControl = true;
            this.TxtToCode.Location = new System.Drawing.Point(150, 6);
            this.TxtToCode.Name = "TxtToCode";
            this.TxtToCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtToCode.Properties.Appearance.Options.UseFont = true;
            this.TxtToCode.Properties.MaxLength = 30;
            this.TxtToCode.Size = new System.Drawing.Size(208, 20);
            this.TxtToCode.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(100, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Month";
            // 
            // TxtMth
            // 
            this.TxtMth.EnterMoveNextControl = true;
            this.TxtMth.Location = new System.Drawing.Point(150, 50);
            this.TxtMth.Name = "TxtMth";
            this.TxtMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth.Properties.Appearance.Options.UseFont = true;
            this.TxtMth.Properties.MaxLength = 30;
            this.TxtMth.Size = new System.Drawing.Size(208, 20);
            this.TxtMth.TabIndex = 16;
            // 
            // FrmHoursMeterDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmHoursMeterDlg";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtToDisplay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtToName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtToCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.TextEdit TxtToDisplay;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.TextEdit TxtToName;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.TextEdit TxtToCode;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.TextEdit TxtMth;
    }
}