﻿#region Update
/*
    06/11/2020 [WED/IMS] new apps
    20/12/2020 [WED/IMS] tambah item
    21/01/2021 [WED/IMS] langsung ceplokin Request Type dan Department berdasarkan parameter IsMaterialRequest4CouldDifferentDocument
    05/04/2021 [VIN/IMS] tambah project
    15/06/2021 [VIN/IMS] PR for Service yang semua qty item nya sudah di PO kan tidak dapat di tarik kembali 
    30/07/2021 [VIN/IMS] PR for Service yang sudah cancel dapat ditarik kembali di PO service
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest4Dlg5 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest4 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmMaterialRequest4Dlg5(FrmMaterialRequest4 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -60);
                Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 7;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "PR Service#", 
                    "PR Service D#",
                    "", 
                    "PR Service's"+Environment.NewLine+"Item's Code",
                    
                    //6-10
                    "PR Service's"+Environment.NewLine+"Local Code",
                    "PR Service's"+Environment.NewLine+"Item's Name",
                    "PR Service's"+Environment.NewLine+"Specification",
                    "PR Service's"+Environment.NewLine+"Item's Category",
                    "Item's Code",

                    //11-15
                    "Item's Name",
                    "Minimum Stock",
                    "Reorder Point",
                    "Quantity",
                    "UoM",

                    //16-20
                    "Usage Date",
                    "ItScCode",
                    "Sub-Category",
                    "Project Code",
                    "Project Name"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 180, 100, 20, 100,
                    
                    //6-10
                    180, 200, 250, 180, 120,

                    //11-15
                    200, 120, 120, 120, 120,

                    //16-20
                    120, 0, 150, 100, 150
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 16 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 19, 20 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, B.ItCode, C.ItCodeInternal, C.ItName, G.PGCode, G.PGName, ");
            SQL.AppendLine("C.Specification, D.ItCtName, B.ItCode2, E.ItName ItName2, E.MinStock MinStock2, ");
            SQL.AppendLine("E.ReorderStock ReorderStock2, B.Qty Qty2, E.PurchaseUomCode, B.UsageDt UsageDt2, E.ItScCode ItScCode2, F.ItScName ItScName2 ");
            SQL.AppendLine("From TblMaterialRequestServiceHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestServiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And B.CancelInd = 'N' And B.Status = 'A' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode ");
            SQL.AppendLine("Inner Join TblItem E On B.ItCode2 = E.ItCode "); 
            SQL.AppendLine("Left Join TblItemSubCategory F On E.ItScCode = F.ItScCode ");
            SQL.AppendLine("Left Join TblProjectGroup G On A.PGCode=G.PGCode ");
            SQL.AppendLine("Left JOIN (SELECT A.MaterialRequestServiceDocNo DocNo, A.MaterialRequestServiceDNo DNo, A.Qty ");
            SQL.AppendLine("From TblMaterialRequestDtl A Where A.MaterialRequestServiceDocNo IS NOT NULL and A.CancelInd ='N' ) H ON A.DocNo=H.DocNo AND B.DNo=H.DNo ");
            SQL.AppendLine("Where B.Qty-IfNull(H.Qty, 0.00) > 0 ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "C.ItCode", "C.ItName", "C.ForeignName", "C.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "C.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By A.CreateDt Desc; ",
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DNo", "ItCode", "ItCodeInternal", "ItName", "Specification",   
                        
                        //6-10
                        "ItCtName", "ItCode2", "ItName2", "MinStock2", "ReorderStock2",

                        //11-15
                        "Qty2", "PurchaseUomCode", "UsageDt2", "ItScCode2", "ItScName2", 

                        //16-17
                        "PGCode", "PGName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1))
                {
                    if (IsChoose == false) IsChoose = true;

                    Row1 = mFrmParent.Grd1.Rows.Count - 1;
                    Row2 = Row;

                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 42, Grd1, Row2, 2);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 43, Grd1, Row2, 3);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 44, Grd1, Row2, 7);

                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 10);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 11);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 12);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 13);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 14);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 15);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 16);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 37, Grd1, Row2, 17);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 38, Grd1, Row2, 18);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 46, Grd1, Row2, 19);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 47, Grd1, Row2, 20);

                    mFrmParent.GetReqTypeAndDeptCode();
                    mFrmParent.ComputeTotal(Row1);

                    mFrmParent.Grd1.Rows.Add();
                    Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 25, 26, 27, 28, 29, 30, 31, 32 });
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                string MenuCode = Sm.GetParameter("MenuCodeForPRService");
                if (MenuCode.Length == 0) MenuCode = mFrmParent.mMenuCode;
                var f = new FrmMaterialRequest3(MenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                string MenuCode = Sm.GetParameter("MenuCodeForPRService");
                if (MenuCode.Length == 0) MenuCode = mFrmParent.mMenuCode;
                var f = new FrmMaterialRequest3(MenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }

        #endregion

        #endregion

    }
}
