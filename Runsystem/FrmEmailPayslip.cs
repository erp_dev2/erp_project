﻿#region Update
/*
    30/04/2018 [WED] Format email payslip mengikuti print out payslip
    21/05/2018 [WED] Format email payslip mengikuti print out payslip baru
    02/07/2018 [ARI] tambah jumlah sebelum penyesuaian, ganti tunajangan jadi potongan
    27/02/2020 [HAR/TWC] perubahan password email untuk email sender
    30/03/2020 [TKG/TWC] setting smtp menggunakan parameter
    18/04/2020 [TKG/TWC] 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

// email
using System.Net;
using System.Net.Security;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Security.Cryptography.X509Certificates;

#endregion

namespace RunSystem
{    
    public partial class FrmEmailPayslip : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty, mSQL = string.Empty, mAccessInd = string.Empty;
        private string 
            mEmailPayslipSMTPHost = string.Empty,
            mEmailPayslipSMTPUser = string.Empty,
            mEmailPayslipSMTPPassword = string.Empty;
        private int mEmailPayslipSMTPPort = 0;

        #endregion

        #region Constructor

        public FrmEmailPayslip(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                GetParameter();
                SetGrd();
                base.FrmLoad(sender, e);
                BtnVoucherDocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        private void GetParameter()
        {
            mEmailPayslipSMTPHost = Sm.GetParameter("EmailPayslipSMTPHost");
            var EmailPayslipSMTPPort = Sm.GetParameter("EmailPayslipSMTPPort");
            if (EmailPayslipSMTPPort.Length > 0)
                mEmailPayslipSMTPPort = int.Parse(EmailPayslipSMTPPort);
            mEmailPayslipSMTPUser = Sm.GetParameter("EmailPayslipSMTPUser");
            mEmailPayslipSMTPPassword = Sm.GetParameter("EmailPayslipSMTPPassword");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 51;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Voucher#",
                    "Voucher Request"+Environment.NewLine+"Payroll#",
                    "",
                    "Payrun#",
                    
                    //6-10
                    "Periode",
                    "Employee Code",
                    "Employee Name",
                    "Email",
                    "Site",                    

                    //11-15
                    "Department",
                    "Currency",
                    "Jumlah"+Environment.NewLine+"Pendapatan",
                    "Jumlah"+Environment.NewLine+"Potongan",
                    "Jumlah"+Environment.NewLine+"Bersih",

                    //16-20
                    "Salary",
                    "Functional", 
                    "SSEmployerHealth", 
                    "SSEmployeeHealth", 
                    "SSEmployerEmployment", 

                    //21-25
                    "SSEmployeeEmployment", 
                    "Amt",
                    "SSEmployerPension", 
                    "SSEmployeePension", 
                    "PerformanceValue", 

                    //26-30
                    "AmtAll", 
                    "AmtDed",
                    "CompanyLogo",
                    "Company",
                    "Phone",

                    //31-35
                    "Address",
                    "OT1Amt",
                    "OT2Amt",
                    "OTHolidayAmt",
                    "ADOT",

                    //36-40
                    "TaxAllowance",
                    "Transport",
                    "Meal",
                    "FixAllowance",
                    "ProcessUPLAmt",

                    //41-45
                    "EmpAdvancePayment",
                    "SSEePension",
                    "Tax",
                    "FixDeduction",
                    "Penyesuaian Gaji",

                    //46-50
                    "OT1Hr",
                    "OT2Hr",
                    "OTHolidayHr",
                    "TransportHr",
                    "MealHr"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 120, 150, 20, 100, 
                    
                    //6-10
                    100, 120, 200, 200, 150, 

                    //11-15
                    180, 80, 150, 150, 150, 

                    //16-20
                    0, 0, 0, 0, 0, 

                    //21-25
                    0, 0, 0, 0, 0, 

                    //26-30
                    0, 0, 0, 0, 0,

                    //31-35
                    0, 0, 0, 0, 0,

                    //36-40
                    0, 0, 0, 0, 0,

                    //41-45
                    0, 0, 0, 0, 150,

                    //46-50
                    0, 0, 0, 0, 0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 10, 11, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50 });
            Grd1.Cols[45].Move(15);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 10, 11 }, !ChkHideInfoInGrd.Checked);
        }

        #region Standard Method

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false)
                ) return;

            try
            {
                MeeSendingTo.Text = string.Empty;
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<PayrunList>();
                var lP = new List<PaySlipTWC>();
                var lAD = new List<EmpAllowance>();
                ListPayrunCode(ref l);
                if (l.Count > 0)
                {
                    AddDataHdr(ref l, ref lP);
                    if (lP.Count > 0)
                    {
                        ShowDataHdr(ref lP);
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            if (
                Sm.StdMsgYN("Question", "Do you want to send the payslip informations ?") == DialogResult.No ||
                IsDataNotValid()
                )
                return;

            bool isSent = false;

            try
            {
                var l = new List<PayrunList>();
                var lP = new List<PaySlipTWC>();
                var lAD = new List<EmpAllowance>();
                var lOT = new List<OTAllowance>();
                ListPayrunCode(ref l);
                if (l.Count > 0)
                {
                    AddDataHdr(ref l, ref lP);
                    if (lP.Count > 0)
                    {
                        PrepareAllowanceDeduction(ref lP, ref lAD);
                        PrepareOTAllowance(ref lP, ref lOT);
                    }
                }

                MeeSendingTo.Text = string.Empty;

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 9).Length > 0)
                    {
                        ProcessSendEmail(Row, ref lAD, ref lOT);
                        isSent = true;
                    }
                }

                if (isSent)
                {
                    Sm.StdMsg(mMsgType.Info, "Email processed");
                    lAD.Clear();
                    lP.Clear();
                    l.Clear();
                }
            }
            catch (Exception Ex)
            {
                Sm.ShowErrorMsg(Ex);
                isSent = false;
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        internal void ShowDetail()
        {
            ShowData();
        }

        private void ListPayrunCode(ref List<PayrunList> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select D.PayrunCode ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo And B.DocType = '06' And B.Status = 'A' And B.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollHdr C On B.DocNo = C.VoucherRequestDocNo And C.Status = 'A' And C.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollDtl2 D On C.DocNo = D.DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.DocNo = @DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherDocNo.Text);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "PayrunCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PayrunList()
                        {
                            PayrunCode = Sm.DrStr(dr, c[0])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void AddDataHdr(ref List<PayrunList> l, ref List<PaySlipTWC> lP)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mPayrunCode = string.Empty;

            if (TxtPayrunCode.Text.Length > 0)
                mPayrunCode = TxtPayrunCode.Text;
            else
            {
                mPayrunCode = string.Empty;
                for (int x = 0; x < l.Count; x++)
                {
                    if (mPayrunCode.Length > 0) mPayrunCode += ",";
                    mPayrunCode += l[x].PayrunCode;
                }
            }

            SQL.AppendLine("Select T1.DocNo As VoucherDocNo, T3.DocNo As VRPDocNo, A.Payruncode, If(T1.CurCode = 'IDR', 'Rp', T1.CurCode) As CurCode, ");
            SQL.AppendLine("@CompanyLogo As CompanyLogo, Z.CompanyName As Company2, Z.CompanyPhone As Phone2, Z.CompanyAddress As Address2, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As Phone, ");
            SQL.AppendLine("A.EmpCode, C.EmpName, C.Email, D.DeptName, H.SiteName, A.Salary, A.Functional, A.SSEmployerHealth,  A.SSEmployeeHealth, ");
            SQL.AppendLine("A.SSEmployerEmployment, A.SSEmployeeEmployment, A.Amt, DATE_FORMAT(DATE_ADD(Concat(Left(A.PayrunCode, 6), '01'), Interval 1 MONTH),'%M %Y')As Periode, A.SSEmployerPension, ");
            SQL.AppendLine("A.SSEmployeePension, A.PerformanceValue, IfNull(X.Amt,0)As AmtAll, IfNull(Y.Amt,0)As AmtDed ");
            SQL.AppendLine(" , A.ProcessUPLAmt, A.OT1Amt, A.OT2Amt, A.OTHolidayAmt, A.SalaryAdjustment, A.Transport, A.Meal, A.ADOT, A.FixAllowance, A.FixDeduction, A.EmpAdvancePayment, A.SSEePension, A.tax, A.taxallowance ");
            SQL.AppendLine(" , A.OT1Hr, A.OT2Hr, A.OTHolidayHr, IfNull(O.TransportHr, 0) TransportHr, IfNull(P.MealHr, 0) MealHr ");
            SQL.AppendLine("From TblVoucherHdr T1 ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr T2 On T1.VoucherRequestDocNo = T2.DocNo And T2.DocType = '06' And T2.Status = 'A' And T2.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollHdr T3 On T2.DocNo = T3.VoucherRequestDocNo And T3.Status = 'A' And T3.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollDtl2 T4 On T3.DocNo = T4.DocNo ");
            SQL.AppendLine("Inner Join tblpayrollprocess1 A On T4.PayrunCode = A.PayrunCode ");
            SQL.AppendLine("Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
            SQL.AppendLine("Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
            if(TxtEmpCode.Text.Length > 0)
                SQL.AppendLine("     And (C.EmpCode Like '%" + TxtEmpCode.Text + "%' Or C.EmpName Like '%" + TxtEmpCode.Text + "%' Or C.EmpCodeOld Like '%" + TxtEmpCode.Text + "%') ");
            SQL.AppendLine("Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On C.SystemType=E.OptCode And E.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join tblgradelevelhdr F On C.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("Left Join tblposition G On C.PosCode=G.PosCode ");
            SQL.AppendLine("Left Join tblsite H On C.SiteCode=H.SiteCode ");
            SQL.AppendLine("Left join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("     Select A.PayrunCode, A.EmpCode, C.ADName, Sum(B.Amt)As Amt ");
	        SQL.AppendLine("     From tblpayrollprocess1 A ");
	        SQL.AppendLine("     Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
	        SQL.AppendLine("     Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
	        SQL.AppendLine("     where C.ADType='A' ");
	        SQL.AppendLine("     Group by A.PayrunCode, A.EmpCode ");
            SQL.AppendLine(")X On A.PayrunCode=X.Payruncode And A.EmpCode=X.EmpCode ");
            SQL.AppendLine("Left join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("     Select A.PayrunCode, A.EmpCode, C.ADName, Sum(B.Amt)As Amt ");
	        SQL.AppendLine("     From tblpayrollprocess1 A ");
	        SQL.AppendLine("     Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
	        SQL.AppendLine("     Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
	        SQL.AppendLine("     where C.ADType='D' ");
	        SQL.AppendLine("     Group by A.PayrunCode, A.EmpCode ");
            SQL.AppendLine(")Y On A.PayrunCode=Y.Payruncode And A.EmpCode=Y.EmpCode ");

            SQL.AppendLine(" Left Join ( ");
            SQL.AppendLine("    Select PayrunCode, Empcode, count(transport)As TransportHr ");
            SQL.AppendLine("    From TblPayrollProcess2 ");
            if (TxtPayrunCode.Text.Length > 0)
                SQL.AppendLine("    where Payruncode Like '%" + mPayrunCode + "%' And ProcessInd='Y' And Transport !=0 ");
            else
                SQL.AppendLine("    where Find_In_Set(Payruncode, @PayrunCode) And ProcessInd='Y' And Transport !=0 ");

            SQL.AppendLine("    Group by Payruncode, EmpCode ");
            SQL.AppendLine(" )O On A.PayrunCode=O.PayrunCode And A.EmpCode=O.EmpCode ");

            SQL.AppendLine(" Left Join ( ");
            SQL.AppendLine("    Select PayrunCode, Empcode, count(Meal)As MealHr ");
            SQL.AppendLine("    From TblPayrollProcess2 ");
            if(TxtPayrunCode.Text.Length > 0)
                SQL.AppendLine("    where Payruncode Like '%" + mPayrunCode + "%' And ProcessInd='Y' And Meal !=0 ");
            else
                SQL.AppendLine("    where Find_In_Set(Payruncode, @PayrunCode) And ProcessInd='Y' And Meal !=0 ");

            SQL.AppendLine("    Group by Payruncode, EmpCode ");
            SQL.AppendLine(" )P On A.PayrunCode=P.PayrunCode And A.EmpCode=P.EmpCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select distinct A.PayrunCode, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
            SQL.AppendLine("    From Tblpayrun A  ");
            SQL.AppendLine("    Inner Join TblSite B On A.SiteCode=B.SiteCode  ");
            SQL.AppendLine("    Inner Join TblProfitCenter C On B.ProfitCenterCode=C.ProfitCenterCode  ");
            SQL.AppendLine("    Inner Join TblEntity D On C.EntCode=D.EntCode  ");
            
            if (TxtPayrunCode.Text.Length > 0)
                SQL.AppendLine("    Where A.PayrunCode Like '%" + mPayrunCode + "%' ");
            else
                SQL.AppendLine("    Where Find_In_Set(A.PayrunCode, @PayrunCode) ");
            
            SQL.AppendLine(") Z On A.PayrunCode=Z.PayrunCode ");
            SQL.AppendLine("Where T1.DocNo = @DocNo And T1.CancelInd = 'N' ");
            
            if (TxtPayrunCode.Text.Length > 0)
                SQL.AppendLine("    And T4.PayrunCode Like '%" + mPayrunCode + "%' ");
            else
                SQL.AppendLine("And Find_In_Set(T4.PayrunCode, @PayrunCode) ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString() + " Order By T1.DocNo, A.PayrunCode, H.SiteName, D.DeptName, C.EmpName; ";
                if (TxtPayrunCode.Text.Length <= 0)
                    Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
                Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", "http://approve.borobudurpark.com/assets/img/logo%20pelangi.png");
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "VoucherDocNo", 
                    
                    //1-5
                    "VRPDocNo", "CompanyLogo", "Company", "Address", "Phone",

                    //6-10
                    "Company2", "Address2", "Phone2", "EmpCode", "EmpName",

                    //11-15
                    "Email", "DeptName", "SiteName", "Salary", "Functional", 

                    //16-20
                    "SSEmployerHealth", "SSEmployeeHealth", "SSEmployerEmployment", "SSEmployeeEmployment", "Amt", 

                    //21-25
                    "Periode", "SSEmployerPension", "SSEmployeePension", "Payruncode", "PerformanceValue", 

                    //26-30
                    "AmtAll", "AmtDed", "CurCode", "ProcessUPLAmt", "OT1Amt",

                    //31-35
                    "OT2Amt", "OTHolidayAmt", "SalaryAdjustment", "Transport", "Meal",

                    //36-40
                    "ADOT", "FixAllowance", "FixDeduction", "EmpAdvancePayment", "SSEePension",

                    //41-45
                    "tax", "taxallowance", "OT1Hr", "OT2Hr", "OTHolidayHr", 
                    
                    //46-47
                    "TransportHr", "MealHr"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lP.Add(new PaySlipTWC()
                        {
                            VoucherDocNo = Sm.DrStr(dr, c[0]),
                            VRPDocNo = Sm.DrStr(dr, c[1]),
                            CompanyLogo = Sm.DrStr(dr, c[2]),
                            Company = (Sm.DrStr(dr, c[13]).Length > 0) ? Sm.DrStr(dr, c[6]) : Sm.DrStr(dr, c[3]),
                            Address = (Sm.DrStr(dr, c[13]).Length > 0) ? Sm.DrStr(dr, c[7]) : Sm.DrStr(dr, c[4]),
                            Phone = (Sm.DrStr(dr, c[13]).Length > 0) ? Sm.DrStr(dr, c[8]) : Sm.DrStr(dr, c[5]),
                            EmpCode = Sm.DrStr(dr, c[9]),
                            EmpName = Sm.DrStr(dr, c[10]),
                            Email = Sm.DrStr(dr, c[11]),
                            DeptName = Sm.DrStr(dr, c[12]),
                            SiteName = Sm.DrStr(dr, c[13]),
                            Salary = Sm.DrDec(dr, c[14]),
                            Functional = Sm.DrDec(dr, c[15]),
                            SSEmployerHealth = Sm.DrDec(dr, c[16]),
                            SSEmployeeHealth = Sm.DrDec(dr, c[17]),
                            SSEmployerEmployment = Sm.DrDec(dr, c[18]),
                            SSEmployeeEmployment = Sm.DrDec(dr, c[19]),
                            Amt = Sm.DrDec(dr, c[20]),
                            Periode = Sm.DrStr(dr, c[21]),
                            SSEmployerPension = Sm.DrDec(dr, c[22]),
                            SSEmployeePension = Sm.DrDec(dr, c[23]),
                            Payruncode = Sm.DrStr(dr, c[24]),
                            PerformanceValue = Sm.DrDec(dr, c[25]),
                            AmtAll = Sm.DrDec(dr, c[26]),
                            AmtDed = Sm.DrDec(dr, c[27]),
                            CurCode = Sm.DrStr(dr, c[28]),
                            ProcessUPLAmt = Sm.DrDec(dr, c[29]),
                            OT1Amt = Sm.DrDec(dr, c[30]),
                            OT2Amt = Sm.DrDec(dr, c[31]),
                            OTHolidayAmt = Sm.DrDec(dr, c[32]),
                            SalaryAdjustment = Sm.DrDec(dr, c[33]),
                            Transport = Sm.DrDec(dr, c[34]),
                            Meal = Sm.DrDec(dr, c[35]),
                            ADOT = Sm.DrDec(dr, c[36]),
                            FixAllowance = Sm.DrDec(dr, c[37]),
                            FixDeduction = Sm.DrDec(dr, c[38]),
                            EmpAdvancePayment = Sm.DrDec(dr, c[39]),
                            SSEePension = Sm.DrDec(dr, c[40]),
                            tax = Sm.DrDec(dr, c[41]),
                            taxallowance = Sm.DrDec(dr, c[42]),
                            OT1Hr = Sm.DrDec(dr, c[43]),
                            OT2Hr = Sm.DrDec(dr, c[44]),
                            OTHolidayHr = Sm.DrDec(dr, c[45]),
                            TransportHr = Sm.DrDec(dr, c[46]),
                            MealHr = Sm.DrDec(dr, c[47]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowDataHdr(ref List<PaySlipTWC> lP)
        {
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;

            iGRow r;

            int nos = 1;
            for (int x = 0; x < lP.Count; x++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = nos++;
                r.Cells[2].Value = lP[x].VoucherDocNo;
                r.Cells[3].Value = lP[x].VRPDocNo;
                r.Cells[5].Value = lP[x].Payruncode;
                r.Cells[6].Value = lP[x].Periode;
                r.Cells[7].Value = lP[x].EmpCode;
                r.Cells[8].Value = lP[x].EmpName;
                r.Cells[9].Value = lP[x].Email;
                r.Cells[10].Value = lP[x].SiteName;
                r.Cells[11].Value = lP[x].DeptName;
                r.Cells[12].Value = lP[x].CurCode;
                //r.Cells[13].Value = lP[x].Salary + lP[x].PerformanceValue + lP[x].SSEmployerHealth + lP[x].SSEmployerEmployment + lP[x].SSEmployerPension + lP[x].AmtAll;
                r.Cells[13].Value = lP[x].Salary + lP[x].PerformanceValue + lP[x].OT1Amt + lP[x].OT2Amt + lP[x].OTHolidayAmt + lP[x].ADOT + lP[x].taxallowance + lP[x].Transport + lP[x].Meal + lP[x].FixAllowance;
                //r.Cells[14].Value = lP[x].SSEmployeeHealth + lP[x].SSEmployeeEmployment + lP[x].SSEmployeePension + lP[x].AmtDed;
                r.Cells[14].Value = lP[x].SSEmployeeHealth + lP[x].SSEmployeeEmployment + lP[x].SSEmployeePension + lP[x].ProcessUPLAmt + lP[x].EmpAdvancePayment + lP[x].SSEePension + lP[x].tax + lP[x].FixDeduction;
                //r.Cells[15].Value = lP[x].Salary + lP[x].PerformanceValue + lP[x].SSEmployerHealth + lP[x].SSEmployerEmployment + lP[x].SSEmployerPension + lP[x].AmtAll - lP[x].SSEmployeeHealth - lP[x].SSEmployeeEmployment - lP[x].SSEmployeePension - lP[x].AmtDed;
                r.Cells[15].Value = lP[x].Salary + lP[x].PerformanceValue + lP[x].OT1Amt + lP[x].OT2Amt + lP[x].OTHolidayAmt + lP[x].ADOT + lP[x].taxallowance + lP[x].Transport + lP[x].Meal + lP[x].FixAllowance - lP[x].SSEmployeeHealth - lP[x].SSEmployeeEmployment - lP[x].SSEmployeePension - lP[x].ProcessUPLAmt - lP[x].EmpAdvancePayment - lP[x].SSEePension - lP[x].tax - lP[x].FixDeduction;
                r.Cells[16].Value = lP[x].Salary;
                r.Cells[17].Value = lP[x].Functional;
                r.Cells[18].Value = lP[x].SSEmployerHealth;
                r.Cells[19].Value = lP[x].SSEmployeeHealth;
                r.Cells[20].Value = lP[x].SSEmployerEmployment;
                r.Cells[21].Value = lP[x].SSEmployeeEmployment;
                r.Cells[22].Value = lP[x].Amt;
                r.Cells[23].Value = lP[x].SSEmployerPension;
                r.Cells[24].Value = lP[x].SSEmployeePension;
                r.Cells[25].Value = lP[x].PerformanceValue;
                r.Cells[26].Value = lP[x].AmtAll;
                r.Cells[27].Value = lP[x].AmtDed;
                r.Cells[28].Value = lP[x].CompanyLogo;
                r.Cells[29].Value = lP[x].Company;
                r.Cells[30].Value = lP[x].Phone;
                r.Cells[31].Value = lP[x].Address;

                r.Cells[32].Value = lP[x].OT1Amt;
                r.Cells[33].Value = lP[x].OT2Amt;
                r.Cells[34].Value = lP[x].OTHolidayAmt;
                r.Cells[35].Value = lP[x].ADOT;
                r.Cells[36].Value = lP[x].taxallowance;
                r.Cells[37].Value = lP[x].Transport;
                r.Cells[38].Value = lP[x].Meal;
                r.Cells[39].Value = lP[x].FixAllowance;
                r.Cells[40].Value = lP[x].ProcessUPLAmt;
                r.Cells[41].Value = lP[x].EmpAdvancePayment;
                r.Cells[42].Value = lP[x].SSEePension;
                r.Cells[43].Value = lP[x].tax;
                r.Cells[44].Value = lP[x].FixDeduction;
                r.Cells[45].Value = lP[x].SalaryAdjustment;
                r.Cells[46].Value = lP[x].OT1Hr;
                r.Cells[47].Value = lP[x].OT2Hr;
                r.Cells[48].Value = lP[x].OTHolidayHr;
                r.Cells[49].Value = lP[x].TransportHr;
                r.Cells[50].Value = lP[x].MealHr;
            }

            Grd1.EndUpdate();
        }

        private void PrepareAllowanceDeduction(ref List<PaySlipTWC> lP, ref List<EmpAllowance> lAD)
        {
            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            string mPayrunCode = string.Empty;

            for (int i = 0; i < lP.Count; i++)
            {
                if (mPayrunCode.Length > 0) mPayrunCode += ",";
                mPayrunCode += lP[i].Payruncode;
            }

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.PayrunCode, A.EmpCode, B.ADName, A.Amt, B.ADType, 'Rp' As Curcode ");
                SQLDtl.AppendLine("From Tblpayrollprocessad A ");
                SQLDtl.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.AdCode and B.AmtType='1' ");
                SQLDtl.AppendLine("Where Find_In_Set(A.PayrunCode, @PayrunCode) ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@PayrunCode", mPayrunCode);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "ADType",
                     "CurCode"

                    });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        lAD.Add(new EmpAllowance()
                        {
                            PayrunCode = Sm.DrStr(drDtl, cDtl[0]),
                            EmpCode = Sm.DrStr(drDtl, cDtl[1]),
                            ADName = Sm.DrStr(drDtl, cDtl[2]),
                            Amt = Sm.DrDec(drDtl, cDtl[3]),
                            ADType = Sm.DrStr(drDtl, cDtl[4]),
                            CurCode = Sm.DrStr(drDtl, cDtl[5]),
                        });
                    }
                }
                drDtl.Close();
            }
        }

        private void PrepareOTAllowance(ref List<PaySlipTWC> lP, ref List<OTAllowance> lOT)
        {
            var cmDtl = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();
            string mPayrunCode = string.Empty;

            for (int i = 0; i < lP.Count; i++)
            {
                if (mPayrunCode.Length > 0) mPayrunCode += ",";
                mPayrunCode += lP[i].Payruncode;
            }

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl2.AppendLine("Select A.PayrunCode, A.Empcode, B.ADName, Sum(A.Amt)As Amt, Sum(A.Duration)As Duration ");
                SQLDtl2.AppendLine("From ");
                SQLDtl2.AppendLine("( ");
                SQLDtl2.AppendLine("    Select * ");
                SQLDtl2.AppendLine("    From TblPayrollProcessADOT ");
                SQLDtl2.AppendLine("    Where Find_In_Set(PayrunCode, @PayrunCode) ");
                SQLDtl2.AppendLine(") A ");
                SQLDtl2.AppendLine("Left Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
                SQLDtl2.AppendLine("Group By A.Empcode, B.AdName ");
                SQLDtl2.AppendLine("Order By B.ADName ");

                cmDtl.CommandText = SQLDtl2.ToString();

                Sm.CmParam<String>(ref cmDtl, "@PayrunCode", mPayrunCode);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-4
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "Duration"

                    });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        lOT.Add(new OTAllowance()
                        {
                            PayrunCode = Sm.DrStr(drDtl, cDtl[0]),
                            EmpCode = Sm.DrStr(drDtl, cDtl[1]),
                            ADName = Sm.DrStr(drDtl, cDtl[2]),
                            Amt = Sm.DrDec(drDtl, cDtl[3]),
                            Duration = Sm.DrDec(drDtl, cDtl[4]),
                        });
                    }
                }
                drDtl.Close();
            }
        }

        private void ProcessSendEmail(int Row, ref List<EmpAllowance> lAD, ref List<OTAllowance> lOT)
        {
            var l = new List<EmailPayslip>();
            
            GetEmailPayslip(ref l);

            foreach (var i in l.Where(w=>w.Send<101m))
            {
                ProcessSendEmail2(Row, ref lAD, ref lOT, i);
                UpdateEmailPaySlip(i);
                break;
            }
        }

        private void UpdateEmailPaySlip(EmailPayslip ep)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmailPayslip(Email, Dt, Send) ");
            SQL.AppendLine("Values(@Email, @Dt, 1.00) ");
            SQL.AppendLine("On Duplicate Key Update Send=Send+1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@Email", ep.Email);
            Sm.CmParam<String>(ref cm, "@Dt", ep.Dt);

            Sm.ExecCommand(cm);
        }

        private void ProcessSendEmail2(int Row, ref List<EmpAllowance> lAD, ref List<OTAllowance> lOT, EmailPayslip ep)
        {
            var mBody = new StringBuilder();
            string mEmailTo = Sm.GetGrdStr(Grd1, Row, 9).Replace(";", ",").Replace(" ", string.Empty);
            string[] mEmailTos = mEmailTo.Split(',');
            
            mBody.AppendLine("<!DOCTYPE html>");
            mBody.AppendLine("<html lang='en'>");
            mBody.AppendLine("<head>");
            mBody.AppendLine("<meta charset='utf-8'>");
            mBody.AppendLine("<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>");
            mBody.AppendLine("</head>");
            mBody.AppendLine("<body>");
            mBody.AppendLine("<div class='container'>");
            mBody.AppendLine("<table>");
			mBody.AppendLine("    <tr>");
			mBody.AppendLine("	    <td rowspan='3'>");
			mBody.AppendLine("		    <img src='http://approve.borobudurpark.com/assets/img/logo%20pelangi.png' width=75px />");
			mBody.AppendLine("	    </td>");
			mBody.AppendLine("	    <td>");
			mBody.AppendLine("		    <span style='font-size: 20px; font-weight: bold;'>PT.TAMAN WISATA CANDI</span><br />");
			mBody.AppendLine("		    <span style='font-size: 14px; font-weight: bold;'>Borobudur, Prambanan dan Ratu Boko</span><br />");
            mBody.AppendLine("		    <span style='font-size: 14px; font-weight: bold;'>" + Sm.GetGrdStr(Grd1, Row, 6) + "</span>");
			mBody.AppendLine("	    </td>");
			mBody.AppendLine("    </tr>");
		    mBody.AppendLine("</table>");
	        mBody.AppendLine("<br />");
	        mBody.AppendLine("<table class='table table-bordered table-striped'>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td><strong>Nama</strong></td>");
            mBody.AppendLine("        <td><strong>" + Sm.GetGrdStr(Grd1, Row, 8) + "</strong></td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td><strong>Kode Pegawai</strong></td>");
            mBody.AppendLine("        <td><strong>" + Sm.GetGrdStr(Grd1, Row, 7) + "</strong></td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td><strong>Departemen</strong></td>");
            mBody.AppendLine("        <td><strong>" + Sm.GetGrdStr(Grd1, Row, 11) + "</strong></td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td><strong>Site</strong></td>");
            mBody.AppendLine("        <td><strong>" + Sm.GetGrdStr(Grd1, Row, 10) + "</strong></td>");
		    mBody.AppendLine("    </tr>");
	        mBody.AppendLine("</table>");

	        mBody.AppendLine("<table class='table table-bordered'>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td colspan='3'><strong>Pendapatan</strong></td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td>Gaji Pokok</td>");
			mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 16)) + "</td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td>Tunjangan Kinerja</td>");
			mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 25)) + "</td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Lembur-1 (" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 46)) + " jam)</td>");
			mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 32)) + "</td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Lembur-2 (" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 47)) + " jam)</td>");
			mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 33)) + "</td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Lembur-Hari Libur (" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 48)) + " jam)</td>");
			mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 34)) + "</td>");
		    mBody.AppendLine("    </tr>");
            mBody.AppendLine("        <td>Tunjangan Pajak</td>");
            mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 36)) + "</td>");
            mBody.AppendLine("    </tr>");
            mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Uang Transport (" + String.Format("{0:n0}", Sm.GetGrdDec(Grd1, Row, 49)) + " hari)</td>");
            mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 37)) + "</td>");
            mBody.AppendLine("    </tr>");
            mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Uang Makan (" + String.Format("{0:n0}", Sm.GetGrdDec(Grd1, Row, 50)) + " hari)</td>");
            mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 38)) + "</td>");
            mBody.AppendLine("    </tr>");
            if (lOT.Count > 0)
            {
                for (int x = 0; x < lOT.Count; x++)
                {
                    if (lOT[x].PayrunCode == Sm.GetGrdStr(Grd1, Row, 5) &&
                        lOT[x].EmpCode == Sm.GetGrdStr(Grd1, Row, 7)
                        )
                    {
                        mBody.AppendLine("    <tr>");
                        mBody.AppendLine("        <td>" + lOT[x].ADName + " ( " + String.Format("{0:n}", lOT[x].Duration) + " jam)</td>");
                        mBody.AppendLine("        <td>Rp. </td>");
                        mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", lOT[x].Amt) + "</td>");
                        mBody.AppendLine("    </tr>");
                    }
                }
            }

            if (lAD.Count > 0)
            {
                for (int x = 0; x < lAD.Count; x++)
                {
                    if (lAD[x].PayrunCode == Sm.GetGrdStr(Grd1, Row, 5) &&
                        lAD[x].EmpCode == Sm.GetGrdStr(Grd1, Row, 7) &&
                        lAD[x].ADType == "A")
                    {
                        mBody.AppendLine("    <tr>");
                        mBody.AppendLine("        <td>" + lAD[x].ADName + "</td>");
                        mBody.AppendLine("        <td>" + lAD[x].CurCode + "</td>");
                        mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", lAD[x].Amt) + "</td>");
                        mBody.AppendLine("    </tr>");
                    }
                }
            }
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td><strong>Jumlah Pendapatan</strong></td>");
			mBody.AppendLine("        <td><strong>Rp.</strong></td>");
            mBody.AppendLine("        <td align='right'><strong>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 13)) + "</strong></td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td colspan='3'>&nbsp;</td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td colspan='3'><strong>Potongan</strong></td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td>Potongan BPJS Kesehatan</td>");
			mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 19)) + "</td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Potongan BPJS Ketenagakerjaan</td>");
			mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 21)) + "</td>");
		    mBody.AppendLine("    </tr>");
            mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Potongan BPJS Pensiun</td>");
            mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 42)) + "</td>");
            mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Potongan Pensiun (Jiwasraya)</td>");
			mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 24)) + "</td>");
		    mBody.AppendLine("    </tr>");
            mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Cuti Tidak Dibayar</td>");
            mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 40)) + "</td>");
            mBody.AppendLine("    </tr>");
            mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Pinjaman Karyawan</td>");
            mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 41)) + "</td>");
            mBody.AppendLine("    </tr>");
            mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td>Pajak</td>");
            mBody.AppendLine("        <td>Rp.</td>");
            mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 43)) + "</td>");
            mBody.AppendLine("    </tr>");
            if (lAD.Count > 0)
            {
                for (int x = 0; x < lAD.Count; x++)
                {
                    if (lAD[x].PayrunCode == Sm.GetGrdStr(Grd1, Row, 5) &&
                        lAD[x].EmpCode == Sm.GetGrdStr(Grd1, Row, 7) &&
                        lAD[x].ADType == "D")
                    {
                        mBody.AppendLine("    <tr>");
                        mBody.AppendLine("        <td>" + lAD[x].ADName + "</td>");
                        mBody.AppendLine("        <td>" + lAD[x].CurCode + "</td>");
                        mBody.AppendLine("        <td align='right'>" + String.Format("{0:n}", lAD[x].Amt) + "</td>");
                        mBody.AppendLine("    </tr>");
                    }
                }
            }
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td><strong>Jumlah Potongan</strong></td>");
			mBody.AppendLine("        <td><strong>Rp.</strong></td>");
            mBody.AppendLine("        <td align='right'><strong>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 14)) + "</strong></td>");
		    mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td colspan='3'>&nbsp;</td>");
		    mBody.AppendLine("    </tr>");
            mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td><strong>Jumlah Sebelum Penyesuaian</strong></td>");
            mBody.AppendLine("        <td><strong>Rp.</strong></td>");
            mBody.AppendLine("        <td align='right'><strong>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 15)) + "</strong></td>");
            mBody.AppendLine("    </tr>");
            mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td><strong>Penyesuaian Gaji</strong></td>");
            mBody.AppendLine("        <td><strong>Rp.</strong></td>");
            mBody.AppendLine("        <td align='right'><strong>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 45)) + "</strong></td>");
            mBody.AppendLine("    </tr>");
            mBody.AppendLine("    <tr>");
            mBody.AppendLine("        <td colspan='3'>&nbsp;</td>");
            mBody.AppendLine("    </tr>");
		    mBody.AppendLine("    <tr>");
			mBody.AppendLine("        <td><strong>Jumlah Bersih</strong></td>");
			mBody.AppendLine("        <td><strong>Rp.</strong></td>");
            mBody.AppendLine("        <td align='right'><strong>" + String.Format("{0:n}", Sm.GetGrdDec(Grd1, Row, 15) + Sm.GetGrdDec(Grd1, Row, 45)) + "</strong></td>");
		    mBody.AppendLine("    </tr>");
	        mBody.AppendLine("</table>");
            mBody.AppendLine("</div>");
            mBody.AppendLine("</body>");
            mBody.AppendLine("</html>");

            foreach(string emailTo in mEmailTos)
            {
                MeeSendingTo.Text += emailTo + ";";
                //MailMessage mail = new MailMessage(mEmailPayslipSMTPUser, emailTo);
                MailMessage mail = new MailMessage(ep.Email, emailTo);
                SmtpClient client = new SmtpClient();

                client.Port = mEmailPayslipSMTPPort; //587
                client.Host = mEmailPayslipSMTPHost; //"smtp.gmail.com"; 
                //client.Credentials = new NetworkCredential(mEmailPayslipSMTPUser, mEmailPayslipSMTPPassword);
                client.Credentials = new NetworkCredential(ep.Email, ep.Pwd);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s,
                           X509Certificate certificate,
                           X509Chain chain,
                           SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };

                mail.Subject = Sm.GetParameter("DocTitle").ToUpper() + " PaySlip " + Sm.GetGrdStr(Grd1, Row, 5) + " : " + Sm.GetGrdStr(Grd1, Row, 8) + " (" + Sm.GetGrdStr(Grd1, Row, 6) + ")";
                mail.IsBodyHtml = true;
                mail.Body = mBody.ToString();
                client.Send(mail);
            }
        }

        private bool IsDataNotValid()
        {
            return
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsChosen = false;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (!IsChosen && Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length > 0) IsChosen = true;
            }
            if (!IsChosen)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
                return true;
            }
            return false;
        }

        private void GetEmailPayslip(ref List<EmailPayslip> l)
        {
            l.Clear();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ParValue As Email, B.Dt, IfNull(C.Send, 0.00) AS Send, D.ParValue As Pwd ");
            SQL.AppendLine("From ( "); 
	        SQL.AppendLine("Select '1' As No, ParValue From TblParameter Where ParCode='EmailPayslipSMTPUser' And ParValue Is Not Null ");
	        SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As No, ParValue From TblParameter Where ParCode='EmailPayslipSMTPUser2' And ParValue Is Not Null ");
	        SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '3' As No, ParValue From TblParameter Where ParCode='EmailPayslipSMTPUser3' And ParValue Is Not Null ");
            SQL.AppendLine(") A  ");
            SQL.AppendLine("Inner Join (Select Replace(CurDate(), '-', '') Dt) B On 1=1 ");
            SQL.AppendLine("Left Join TblEmailPayslip C On A.ParValue=C.Email And B.Dt=C.Dt And C.Send<101.00 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select '1' As No, ParValue From TblParameter Where ParCode='EmailPayslipSMTPPassword' And ParValue Is Not Null ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As No, ParValue From TblParameter Where ParCode='EmailPayslipSMTPPassword2' And ParValue Is Not Null ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '3' As No, ParValue From TblParameter Where ParCode='EmailPayslipSMTPPassword3' And ParValue Is Not Null ");
            SQL.AppendLine(") D On A.No=D.No ");
            SQL.AppendLine("Order By A.No Desc;");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Email", "Dt", "Send", "Pwd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new EmailPayslip()
                        {
                            Email = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            Send = Sm.DrDec(dr, c[2]),
                            Pwd = Sm.DrStr(dr, c[3])
                        });
                }
                dr.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
                e.DoDefault = false;

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucherRequestPayroll(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmVoucherRequestPayroll' Limit 1");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmVoucherRequestPayroll(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmVoucherRequestPayroll' Limit 1");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                        Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Events

        #region Button Events

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmailPayslipDlg(this));
        }

        private void BtnVoucherDocNo2_Click(object sender, EventArgs e)
        {
            if(!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmVoucher' Limit 1");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Events

        private void TxtPayrunCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayrunCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun#");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion        

        #endregion

        #region Class

        private class EmailPayslip
        {
            public string Email { get; set; }
            public string Dt { get; set; }
            public decimal Send { get; set; }
            public string Pwd { get; set; }
        }

        private class PayrunList
        {
            public string PayrunCode { get; set; }
        }

        class PaySlipTWC
        {
            public string CompanyLogo { get; set; }
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Email { get; set; }
            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public decimal Salary { get; set; }
            public decimal Functional { get; set; }
            public decimal SSEmployerHealth { get; set; }
            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal Amt { get; set; }
            public string Periode { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal PerformanceValue { get; set; }
            public decimal AmtAll { get; set; }
            public decimal AmtDed { get; set; }
            public string Payruncode { get; set; }
            public string VoucherDocNo { get; set; }
            public string VRPDocNo { get; set; }
            public string CurCode { get; set; }

            public decimal ProcessUPLAmt { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal Transport { get; set; }
            public decimal Meal { get; set; }
            public decimal ADOT { get; set; }
            public decimal FixAllowance { get; set; }
            public decimal FixDeduction { get; set; }
            public decimal EmpAdvancePayment { get; set; }
            public decimal SSEePension { get; set; }
            public decimal tax { get; set; }
            public decimal taxallowance { get; set; }

            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal TransportHr { get; set; }
            public decimal MealHr { get; set; }

        }

        class EmpAllowance
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADName { get; set; }
            public decimal Amt { get; set; }
            public string ADType { get; set; }
            public string CurCode { get; set; }
        }

        class OTAllowance
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADName { get; set; }
            public decimal Amt { get; set; }
            public decimal Duration { get; set; }
        }

        #endregion
    }
}
