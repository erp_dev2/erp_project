﻿#region Update
/*
    23/08/2017 [TKG] TO difilter berdasarkan otorisasi site group.
    30/08/2017 [TKG] approval menggunakan site.
    31/01/2018 [WED] Tambah field informasi Asset Display Name
    22/02/2018 [HAR] tambah warning jika document date dan down date berbeda
    15/03/2018 [ARI] tambah informasi approval
    17/01/2021 [TKG/PHT] ubah GenerateDocNo*
    25/11/2021 [YOG/IOK] menambahkan field Equipment berdasarkan parameter IsWORUseEquipment
    25/01/2022 [YOG/IOK] feedback field Equipment menjadi tidak mandatory
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWOR : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty, mDocNo = string.Empty;
        internal FrmWORFind FrmFind;
        private string mIsWORDocNoUseFormatStandard = string.Empty;
        internal bool
            mIsFilterBySite = false,
            mIsWORUseEquipment = false;
        private bool mIsWORApprovalBySiteMandatory = false;

        #endregion

        #region Constructor

        public FrmWOR(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                SetLueStatus(ref LueStatus);
                SetLueMtcStatusCode(ref LueMtcStatusCode);
                SetLueMtcTypeCode(ref LueMtcTypeCode);
                SetLueSymProblemCode(ref LueSymProblemCode);
                SetLueStatusWo(ref LueWOStatus);
                SetLueEquipmentCode(ref LueEquipmentCode, TxtToCode.Text);
                base.FrmLoad(sender, e);
                SetGrd1();

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    this.Text = "Work Order Request";
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (!mIsWORUseEquipment)
                {
                    int ypoint = 21; 
                    LueEquipmentCode.Visible = false;
                    label16.Visible = false;
                    label15.Top = label15.Top - ypoint;
                    TxtDisplayName.Top = TxtDisplayName.Top - ypoint;
                    label7.Top = label7.Top - ypoint;
                    LueWOStatus.Top = LueWOStatus.Top - ypoint;
                    label9.Top = label9.Top - ypoint;
                    MeeDescription.Top = MeeDescription.Top - ypoint;
                    label14.Top = label14.Top - ypoint;
                    TxtSite.Top = TxtSite.Top - ypoint;
                    label13.Top = label13.Top - ypoint;
                    TxtRecvAssetDocNo.Top = TxtRecvAssetDocNo.Top - ypoint;
                    label10.Top = label10.Top - ypoint;
                    MeeRemark.Top = MeeRemark.Top - ypoint;
                    Grd1.Height = Grd1.Height + ypoint;
                }
               
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd1()
        {
            Grd1.Cols.Count = 5;
            Grd1.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",
                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] 
                    {
                        40, 150, 100, 100, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueMtcStatusCode, LueMtcTypeCode, LueSymProblemCode, TxtToCode, 
                        MeeDescription, MeeRemark, DteDownDt, TmeDown, TxtRecvAssetDocNo, MeeCancelReason, TxtSite,
                        TxtDisplayName, LueEquipmentCode
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnAssetCode.Enabled = false;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueMtcStatusCode, LueMtcTypeCode, LueSymProblemCode, LueEquipmentCode, DteDocDt,  MeeDescription, DteDownDt, TmeDown, MeeRemark
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnAssetCode.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, LueStatus, LueMtcStatusCode, LueMtcTypeCode, LueSymProblemCode, TxtToCode, LueWOStatus,  
                 MeeDescription, MeeRemark, MeeCancelReason, TxtAssetName, DteDownDt, TmeDown, TxtRecvAssetDocNo, TxtSite,
                 TxtDisplayName
            });
            ChkCancelInd.Checked = false;
            Grd1.Rows.Clear();
      
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWORFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteDownDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sm.SetLue(LueStatus, "O");
                Sm.SetLue(LueWOStatus, "O");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Additional Method

        private string GenerateDocNoNonStandard(string DocDt, string DocType, string Tbl)
        {
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                SiteName = TxtSite.Text,
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From " + Tbl);
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((SiteName.Length == 0 ? "XXX" : SiteName)) + "' '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        private void GetParameter()
        {
            mIsWORDocNoUseFormatStandard = Sm.GetParameter("IsWORDocNoUseFormatStandard");
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsWORApprovalBySiteMandatory = Sm.GetParameter("IsWORApprovalBySiteMandatory") == "Y";
            mIsWORUseEquipment = Sm.GetParameterBoo("IsWORUseEquipment");
        }


        private static void SetLueMtcStatusCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
               "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'MaintenanceStatus' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private static void SetLueMtcTypeCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'MaintenanceType' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private static void SetLueSymProblemCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'SymptomProblem' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private static void SetLueStatusWo(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'O' As Col1, 'Open' As Col2 union All");
            SQL.AppendLine("Select 'C','Closed'");

            Sm.SetLue2(
               ref Lue, SQL.ToString(),
               0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'O' As Col1, 'Outstanding' As Col2 Union All ");
            SQL.AppendLine("Select 'C', 'Cancel' Union All ");
            SQL.AppendLine("Select 'A', 'Approve';");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void SetLueEquipmentCode(ref LookUpEdit Lue, string TOCode)
        {
            Sm.SetLue2(
             ref Lue,
             "Select A.EquipmentCode As Col1, A.EquipmentName As Col2 From TblEquipment A " +
             "Inner Join tbltodtl B ON B.EquipmentCode = A.EquipmentCode Where AssetCode = '" + TOCode + "' Order By A.EquipmentCode  ",
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            IsDateDifferent();
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            if (mIsWORDocNoUseFormatStandard == "Y")
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "WOR", "TblWOR");
            }
            else
            {
                DocNo = GenerateDocNoNonStandard(Sm.GetDte(DteDocDt), "WOR", "TblWOR");
            }
            var cml = new List<MySqlCommand>();

            cml.Add(SaveWor(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);

        }

        private bool IsInsertedDataNotValid()
        {
            return
            Sm.IsDteEmpty(DteDocDt, "Date") ||
            Sm.IsLueEmpty(LueMtcStatusCode, "Maintenance status") ||
            // Sm.IsLueEmpty(LueMtcTypeCode, "Maintenance type") ||
            Sm.IsDteEmpty(DteDownDt, "Down Date") ||
            Sm.IsTmeEmpty(TmeDown, "Down Time") ||
            //  Sm.IsLueEmpty(LueSymProblemCode, "Symptom problem") ||
            Sm.IsTxtEmpty(TxtToCode, "Asset Code", false);
            // (mIsWORUseEquipment && IsEquipmentNotValid("Save"))

        }

        /* private bool IsEquipmentNotValid(string type)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EquipmentCode From TblEquipment A ");
            SQL.AppendLine("Inner Join Tbltodtl B On B.EquipmentCode = A.EquipmentCode ");
            SQL.AppendLine("Where B.AssetCode = @assetcode ");
            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@assetCode", TxtToCode.Text);

            if (Sm.GetValue(cm).Length != 0)
            {
                if (type == "Save")
                    return Sm.IsLueEmpty(LueEquipmentCode, "Equipment");
                else if (type == "Show")
                    label16.ForeColor = Color.Red;
            }
            else label16.ForeColor = Color.Black;

            return false;
        } */

        private MySqlCommand SaveWor(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWor(DocNo, DocDt, CancelInd, Status, WOStatus, MtcStatus, MtcType, SymProblem, ToCode, Description, DownDt, DownTm, CloseDt, Remark, ");
            if (mIsWORUseEquipment)
                SQL.AppendLine("EquipmentCode, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @WOStatus, @MtcStatus, @MtcType, @SymProblem, @ToCode, @Description, @DownDt, @DownTm, null, @Remark, ");
            if(mIsWORUseEquipment)
                SQL.AppendLine("@EquipmentCode, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); "); 
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime()  ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='WORequest' ");
            if (mIsWORApprovalBySiteMandatory)
            {
                SQL.AppendLine("And T.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblWOR A");
                SQL.AppendLine("    Inner Join TblTOHdr B On A.TOCOde=B.AssetCode ");
                SQL.AppendLine("    Inner Join TblLocation C On B.LocCode=C.LocCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo And C.SiteCode=T.SiteCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblWOR Set Status='A'  ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists(  ");
            SQL.AppendLine("    Select DocNo From TblDocApproval  ");
            SQL.AppendLine("    Where DocType='WORequest' ");
            SQL.AppendLine("    And DocNo=@DocNo  ");
            SQL.AppendLine("    );  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WOStatus", Sm.GetLue(LueWOStatus));
            Sm.CmParam<String>(ref cm, "@MtcStatus", Sm.GetLue(LueMtcStatusCode));
            Sm.CmParam<String>(ref cm, "@MtcType", Sm.GetLue(LueMtcTypeCode));
            Sm.CmParam<String>(ref cm, "@SymProblem", Sm.GetLue(LueSymProblemCode));
            Sm.CmParam<String>(ref cm, "@ToCode", TxtToCode.Text);
            Sm.CmParam<String>(ref cm, "@Description", MeeDescription.Text);
            Sm.CmParamDt(ref cm, "@DownDt", Sm.GetDte(DteDownDt));
            Sm.CmParam<String>(ref cm, "@DownTm", Sm.GetTme(TmeDown));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            if(mIsWORUseEquipment)
                Sm.CmParam<String>(ref cm, "@EquipmentCode", Sm.GetLue(LueEquipmentCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }


        private void IsDateDifferent()
        {
            if (Sm.GetDte(DteDownDt).Length > 0)
            {
                if (Sm.GetDte(DteDocDt) != Sm.GetDte(DteDownDt))
                {
                    Sm.StdMsg(mMsgType.Info, "Document date and Down date is different");
                }
            }
        }


        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditWor());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToWO();
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWOR Where DocNo=@Param And CancelInd='Y';",
                TxtDocNo.Text,
                "This request already cancelled.");
        }

        private bool IsDataAlreadyProcessedToWO()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWOHdr Where WORDocNo=@Param And CancelInd='N';",
                TxtDocNo.Text,
                "This request already processed to WO.");
        }

        private MySqlCommand EditWor()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWor Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowWor(DocNo);
                ShowDocApproval(DocNo); 
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWor(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.Status, A.WOStatus, ");
            if (mIsWORUseEquipment)
                SQL.AppendLine("A.EquipmentCode, ");
            else
                SQL.AppendLine("Null As EquipmentCode, ");
            SQL.AppendLine("A.MtcStatus, A.MtcType, A.SymProblem, A.TOCode, B.AssetName, A.Description, ");
            SQL.AppendLine("A.DownDt, A.DownTm, A.RecvAssetDocNo, E.SiteName, A.Remark, B.DisplayName ");
            SQL.AppendLine("From TblWOR A");
            SQL.AppendLine("Inner Join TblAsset B On A.ToCode=B.AssetCode");
            SQL.AppendLine("Inner Join TblTOHdr C On A.TOCOde=C.AssetCode ");
            SQL.AppendLine("Left Join TblLocation D On C.LocCode = D.LocCode ");
            SQL.AppendLine("Left Join TblSite E On D.SiteCode = E.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "CancelReason", "Status", "WOStatus",   

                        //6-10
                        "MtcStatus", "MtcType", "SymProblem", "TOCode", "AssetName", 

                        //11-15
                        "Description", "DownDt", "DownTm", "RecvAssetDocNo", "SiteName", 

                        //16-17
                        "Remark", "DisplayName", "EquipmentCode"
                      
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueStatus, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueWOStatus, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueMtcStatusCode, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueMtcTypeCode, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueSymProblemCode, Sm.DrStr(dr, c[8]));
                        TxtToCode.EditValue = Sm.DrStr(dr, c[9]);
                        TxtAssetName.EditValue = Sm.DrStr(dr, c[10]);
                        MeeDescription.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetDte(DteDownDt, Sm.DrStr(dr, c[12]));
                        Sm.SetTme(TmeDown, Sm.DrStr(dr, c[13]));
                        TxtRecvAssetDocNo.EditValue = Sm.DrStr(dr, c[14]);
                        TxtSite.EditValue = Sm.DrStr(dr, c[15]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[16]);
                        TxtDisplayName.EditValue = Sm.DrStr(dr, c[17]);
                        if(mIsWORUseEquipment)
                            Sm.SetLue(LueEquipmentCode, Sm.DrStr(dr, c[18]));
                    }, true
                );
        }

        private void ShowDocApproval(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.ApprovalDNo, B.UserName, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, " +
                    "Case When A.LastUpDt Is Not Null Then  " +
                    "A.LastUpDt " +
                    "Else Null End As LastUpDt, A.Remark " +
                    "From TblDocApproval A " +
                    "Left Join TblUser B On A.UserCode = B.UserCode " +
                    "Where A.DocType='WORequest' " +
                    "And Status In ('A', 'C') " +
                    "And A.DocNo=@DocNo " +
                    "Order By A.ApprovalDNo ",

                    new string[] 
                    { 
                        "ApprovalDNo",
                        "UserName","StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #endregion

        #region Event

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueMtcStatusCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMtcStatusCode, new Sm.RefreshLue1(SetLueMtcStatusCode));
        }

        private void LueEquipmentCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEquipmentCode, new Sm.RefreshLue2(SetLueEquipmentCode), TxtToCode.Text);
        }

        private void TxtToCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsWORUseEquipment)
            {
                SetLueEquipmentCode(ref LueEquipmentCode, TxtToCode.Text);
                //IsEquipmentNotValid("Show");
            }
        }

        private void LueMtcTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMtcTypeCode, new Sm.RefreshLue1(SetLueMtcTypeCode));
        }

        private void LueSymProblemCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSymProblemCode, new Sm.RefreshLue1(SetLueSymProblemCode));
        }

        private void BtnAssetCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmWORDlg(this));
        }

        private void LueWOStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWOStatus, new Sm.RefreshLue1(SetLueStatusWo));
        }

        #endregion 
    }
}
